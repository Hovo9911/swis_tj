<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => '/{lngCode}'], function () {

    Route::get('xml', 'SingleApplication\SingleApplicationController@exportXmlSafData');

    /**
     * Errors (404)
     */
    Route::get('/404', function () {
        return view('errors.404');
    })->name('404');

    Route::get('/', function () {
        return redirect(urlWithLng('login'));
    });

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/open/constructor', function () {
        session(['enable_constructor' => true]);

        return redirect(urlWithLng('dashboard'));
    })->middleware('constructorAuthAccess');

    // Saf Info for UAIS
    Route::get('saf-info/{safNumber}', 'SingleApplication\SingleApplicationSubApplicationController@subApplicationDocs')->middleware('userUrlRoles');
    Route::get('/subapplication/info', ['uses' => 'UserDocuments\UserDocumentsController@subApplicationInfo'])->middleware('userUrlRoles');

    /**
     * Auth Part
     */
    Route::group(['namespace' => 'Auth'], function () {

        Route::get('login', 'LoginController@showLoginForm')->name('login');
        Route::post('login', 'LoginController@login')->name('loginPost');
        Route::post('logout', 'LoginController@logout')->name('logout');
        Route::get('reset-password', 'ResetPasswordController@showResetForm')->name('reset-password');
        Route::post('reset-password', 'ResetPasswordController@sendToken')->name('resetPasswordPost');
        Route::get('reset/{token}', 'ResetPasswordController@reset')->name('reset');
        Route::post('reset', 'ResetPasswordController@setNewPassword')->name('set-new-password');
        Route::get('email/verify/{token}', 'VerifyController@activateEmail');

        Route::get('password/{token}', 'RegisterController@showPasswordForm')->name('set-password');
        Route::post('password', 'RegisterController@Password');

    });

    // G-uploader
    Route::post('/g-uploader/upload', ['uses' => 'GUploaderController@upload']);

    // User Manuals
    Route::get('/help', ['uses' => 'UserManualsController@showUserManuals', 'middleware' => 'userUrlRoles'])->name('help');
    Route::post('/user-manuals/table', ['uses' => 'UserManualsController@table']);

    /**
     * Only Auth Users Routes
     */
    Route::group(['middleware' => ['auth']], function () {

        // Auth
        Route::group(['namespace' => 'Auth'], function () {
            /**
             * Email Verification
             */
            Route::get('email/verify', 'VerifyController@emailVerify')->name('email-verify');
            Route::post('email/verify', 'VerifyController@sendActivationToken');
            Route::post('email/verify-again', 'VerifyController@sendTokenAgain');

            /**
             * Phone Number Verification
             */
            Route::get('phone-number/verify', 'VerifyController@phoneNumberVerify')->name('phone-verify');
            Route::post('phone-number/verify', 'VerifyController@sendActivationSMS');
            Route::post('phone/verify-again', 'VerifyController@sendCodeAgain');
            Route::post('phone-number/verify/code', 'VerifyController@activatePhoneNumber')->name('phone-verify-code');

            /**
             * Password change
             */
            Route::get('user-password-change', 'ChangePasswordController@showPasswordForm');
            Route::post('user-password-change', 'ChangePasswordController@changePassword');
        });

        /**
         *
         * Verified Users Routes
         *
         * User Has Role To access routes
         *
         */
        Route::group(['middleware' => ['verifiedUser', 'userUrlRoles']], function () {

            //
            Route::get('/dashboard', ['uses' => 'DashboardController@index'])->name('dashboard');

            //
            Route::post('/clear-user-session', ['uses' => 'UserController@clearUserRoleSession']);

            // Entity Types
            Route::group(['prefix' => 'entity'], function () {
                Route::post('get-info', ['uses' => 'EntityTypeController@entityTypeInfo'])->name('userTypeInfo');
                Route::post('company-info', ['uses' => 'EntityTypeController@getCompanyInfoByTaxId']);
            });

            // Languages
            Route::group(['prefix' => 'language', 'moduleName' => 'language', 'manager' => \App\Http\Controllers\Core\Language\LanguageManager::class], function () {
                Route::get('', ['uses' => 'LanguagesController@index']);
                Route::post('/table', ['uses' => 'LanguagesController@table', 'log' => ['action' => 'search']]);

                Route::group(['checkAccessForUpdate' => true], function () {
                    Route::get('/create', ['uses' => 'LanguagesController@create']);
                    Route::post('', ['uses' => 'LanguagesController@store']);
                    Route::get('/{id}/{viewType}', ['uses' => 'LanguagesController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+'])->where('viewType', 'view|edit');
                    Route::post('/update/{id}', ['uses' => 'LanguagesController@update']);
                    Route::post('/delete', ['uses' => 'LanguagesController@delete']);
                });
            });

            // Dictionary
            Route::group(['prefix' => 'dictionary', 'moduleName' => 'dictionary', 'manager' => \App\Http\Controllers\Core\Dictionary\DictionaryManager::class], function () {
                Route::get('', ['uses' => 'DictionaryController@index']);
                Route::post('/table', ['uses' => 'DictionaryController@table', 'log' => ['action' => 'search']]);

                Route::group(['checkAccessForUpdate' => true], function () {
                    Route::post('/edit', ['uses' => 'DictionaryController@edit', 'log' => ['action' => 'updateDictionary']]);
                    Route::post('/delete', ['uses' => 'DictionaryController@delete']);
                });
            });

            // Agency
            Route::group(['prefix' => 'agency', 'moduleName' => 'agency', 'manager' => \App\Models\Agency\AgencyManager::class], function () {
                Route::get('', ['uses' => 'AgencyController@index']);
                Route::post('/table', ['uses' => 'AgencyController@table', 'log' => ['action' => 'search']]);
                Route::post('/get-indicators', ['uses' => 'AgencyController@getIndicators']);

                Route::group(['checkAccessForUpdate' => true], function () {
                    Route::get('/create', ['uses' => 'AgencyController@create', 'log' => ['action' => 'create']]);
                    Route::post('', ['uses' => 'AgencyController@store', 'log' => ['action' => 'store']]);
                    Route::get('/{id}/{viewType}', ['uses' => 'AgencyController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+'])->where('viewType', 'view|edit');
                    Route::post('/update/{id}', ['uses' => 'AgencyController@update', 'log' => ['action' => 'update']]);

                    Route::post('/save-indicators', ['uses' => 'AgencyController@saveIndicators']);
                    Route::post('/delete-indicators', ['uses' => 'AgencyController@deleteIndicators']);
                });
            });

            // Regional Office
            Route::group(['prefix' => 'regional-office', 'moduleName' => 'regional-office', 'manager' => \App\Models\RegionalOffice\RegionalOfficeManager::class], function () {

                Route::get('', ['uses' => 'RegionalOfficeController@index']);
                Route::post('/table', ['uses' => 'RegionalOfficeController@table', 'log' => ['action' => 'search']]);
                Route::post('/get-agency-documents', ['uses' => 'RegionalOfficeController@constructorDocumentsOfAgency']);

                Route::group(['checkAccessForUpdate' => true], function () {
                    Route::get('/create', ['uses' => 'RegionalOfficeController@create', 'log' => ['action' => 'create']]);
                    Route::post('', ['uses' => 'RegionalOfficeController@store', 'log' => ['action' => 'store']]);
                    Route::get('/{id}/{viewType}', ['uses' => 'RegionalOfficeController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+'])->where('viewType', 'view|edit');
                    Route::post('/update/{id}', ['uses' => 'RegionalOfficeController@update', 'log' => ['action' => 'update']]);
                });
            });

            // Laboratory
            Route::group(['prefix' => 'laboratory', 'moduleName' => 'laboratory', 'manager' => \App\Models\Laboratory\LaboratoryManager::class], function () {

                Route::get('', ['uses' => 'LaboratoryController@index']);
                Route::post('/table', ['uses' => 'LaboratoryController@table', 'log' => ['action' => 'search']]);
                Route::post('/get-agency-users', ['uses' => 'LaboratoryController@getAgencyUsers']);
                Route::post('/get-lab-data', ['uses' => 'LaboratoryController@getLabData']);
                Route::post('/get-indicators', ['uses' => 'LaboratoryController@getIndicators']);
                Route::post('/save-indicators', ['uses' => 'LaboratoryController@saveIndicators']);
                Route::post('/delete-indicators', ['uses' => 'LaboratoryController@deleteIndicators']);
                Route::post('/get-agency-available-labs', ['uses' => 'LaboratoryController@getAgencyAvailableLabs']);

                Route::group(['checkAccessForUpdate' => true], function () {
                    Route::get('/create', ['uses' => 'LaboratoryController@create', 'log' => ['action' => 'create']]);
                    Route::post('', ['uses' => 'LaboratoryController@store', 'log' => ['action' => 'store']]);
                    Route::get('/{id}/{viewType}', ['uses' => 'LaboratoryController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+'])->where('viewType', 'view|edit');
                    Route::post('/update/{id}', ['uses' => 'LaboratoryController@update', 'log' => ['action' => 'update']]);
                    Route::post('/delete', ['uses' => 'LaboratoryController@delete']);
                });
            });

            // Laboratory Indicators
            Route::group(['prefix' => 'laboratory-indicator', 'moduleName' => 'laboratory-indicator', 'manager' => \App\Models\LaboratoryIndicator\LaboratoryIndicatorManager::class], function () {
                Route::get('', ['uses' => 'LaboratoryIndicatorsController@index']);
                Route::get('/create/{id?}', ['uses' => 'LaboratoryIndicatorsController@create', 'log' => ['action' => 'create']]);
                Route::post('', ['uses' => 'LaboratoryIndicatorsController@store', 'log' => ['action' => 'store']]);
                Route::post('/table', ['uses' => 'LaboratoryIndicatorsController@table', 'log' => ['action' => 'search']]);
                Route::get('/{id}/edit', ['uses' => 'LaboratoryIndicatorsController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+']);
                Route::post('/update/{id}', ['uses' => 'LaboratoryIndicatorsController@update', 'log' => ['action' => 'update']]);
                Route::post('/delete', ['uses' => 'LaboratoryIndicatorsController@delete']);
                Route::post('/autocomplete', ['uses' => 'LaboratoryIndicatorsController@autocomplete']);
            });

            // Broker
            Route::group(['prefix' => 'broker', 'moduleName' => 'broker', 'manager' => \App\Models\Broker\BrokerManager::class], function () {

                Route::get('', ['uses' => 'BrokerController@index']);
                Route::post('/table', ['uses' => 'BrokerController@table', 'log' => ['action' => 'search']]);
                Route::post('/is-broker', ['uses' => 'BrokerController@brokerData'])->name('isBroker');

                Route::group(['checkAccessForUpdate' => true], function () {
                    Route::get('/create', ['uses' => 'BrokerController@create', 'log' => ['action' => 'create']]);
                    Route::post('', ['uses' => 'BrokerController@store', 'log' => ['action' => 'store']]);
                    Route::get('/{id}/{viewType}', ['uses' => 'BrokerController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+'])->where('viewType', 'view|edit');
                    Route::post('/update/{id}', ['uses' => 'BrokerController@update', 'log' => ['action' => 'update']]);
                });
            });

            // User
            Route::group(['prefix' => 'user', 'moduleName' => 'user', 'manager' => \App\Models\User\UserManager::class], function () {

                Route::get('', ['uses' => 'UserController@index']);
                Route::post('/table', ['uses' => 'UserController@table', 'log' => ['action' => 'search']]);
                Route::post('get-company-info', ['uses' => 'UserController@getCompanyInfoByTaxId']);
                Route::post('get-user-info', ['uses' => 'UserController@getUserInfoBySSN']);
                Route::get('service-info', ['uses' => 'UserController@showServiceInfo']);
                Route::post('service-info', ['uses' => 'UserController@serviceInfo']);

                Route::group(['checkAccessForUpdate' => true], function () {
                    Route::get('/create', ['uses' => 'UserController@create', 'log' => ['action' => 'create']]);
                    Route::post('/check/username', ['uses' => 'UserController@checkUsername']);
                    Route::post('', ['uses' => 'UserController@store', 'log' => ['action' => 'store']]);
                    Route::get('/{id}/edit', ['uses' => 'UserController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+'])->where('view', 'edit');
                    Route::get('/{id}/{viewType}', ['uses' => 'UserController@view', 'log' => ['action' => 'view']])->where(['id' => '[0-9]+'])->where('viewType', 'view');
                    Route::post('/update/{id}', ['uses' => 'UserController@update', 'log' => ['action' => 'update']]);
                    Route::post('update-user-info-from-service', ['uses' => 'UserController@updateUserInfoFromService']);
                    Route::post('get-company-head', ['uses' => 'UserController@getCompanyHead'])->name('userTypeInfo');
                    Route::post('email-send-again', ['uses' => 'UserController@emailSendAgain']);
                });
            });

            // User (Custom operator)
            Route::group(['prefix' => 'user_customs_operator', 'moduleName' => 'user', 'manager' => \App\Models\User\UserManager::class], function () {
                Route::get('/create', ['uses' => 'UserCustomsOperator@create', 'log' => ['action' => 'createCustomsOperator']]);
                Route::post('', ['uses' => 'UserCustomsOperator@store', 'log' => ['action' => 'storeCustomsOperator']]);
                Route::get('/{id}/edit', ['uses' => 'UserCustomsOperator@edit', 'log' => ['action' => 'editCustomsOperator']])->where(['id' => '[0-9]+']);
                Route::get('/{id}/view', ['uses' => 'UserCustomsOperator@view', 'log' => ['action' => 'viewCustomsOperator']])->where(['id' => '[0-9]+']);
                Route::post('/update/{id}', ['uses' => 'UserCustomsOperator@update', 'log' => ['action' => 'updateCustomsOperator']]);
            });

            // User Profile
            Route::group(['prefix' => 'profile-settings', 'moduleName' => 'profile-settings', 'manager' => \App\Models\User\UserManager::class], function () {
                Route::get('', ['uses' => 'UserController@profileSettingsShow', 'log' => ['action' => 'open-profile']]);
                Route::post('', ['uses' => 'UserController@profileSettingsStore', 'log' => ['action' => 'store-profile']]);
            });

            // Role Management
            Route::group(['namespace' => 'RoleManagement'], function () {

                // Roles
                Route::group(['prefix' => 'role', 'moduleName' => 'role', 'manager' => \App\Models\Role\RoleManager::class], function () {

                    Route::post('set', ['uses' => 'RolesController@setRole', 'log' => ['action' => 'setRole']])->name('changeRole');
                    Route::view('set', 'role.set-role')->name('roleSet');
                    Route::get('', ['uses' => 'RolesController@index']);
                    Route::post('/table', ['uses' => 'RolesController@table', 'log' => ['action' => 'search']]);

                    // From authorize person get roles/attributes/value for authorize
                    Route::group(['noNeedCheckAccess' => true], function () {
                        Route::post('/get-attributes', ['uses' => 'RolesController@roleAttributes']);
                        Route::post('/get-attributes/values', ['uses' => 'RolesController@roleAttributeValues']);
                        Route::post('/get-payment-and-obligations-attributes', ['uses' => 'RolesController@paymentAndObligationRoleAttributes']);
                        Route::post('/get-payment-and-obligations-attributes/values', ['uses' => 'RolesController@paymentAndObligationRoleAttributesValues']);
                        Route::post('/get-multiple-attributes', ['uses' => 'RolesController@roleMultipleAttribute']);
                    });

                    Route::group(['checkAccessForUpdate' => true], function () {
                        Route::get('/create', ['uses' => 'RolesController@create', 'log' => ['action' => 'create']]);
                        Route::get('/{id}/{viewType}', ['uses' => 'RolesController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+'])->where('viewType', 'view|edit');
                        Route::post('/update/{id}', ['uses' => 'RolesController@update', 'log' => ['action' => 'update']]);
                        Route::post('/delete', ['uses' => 'RolesController@delete']);
                        Route::post('', ['uses' => 'RolesController@store', 'log' => ['action' => 'store']]);
                    });
                });

                // Roles group
                Route::group(['prefix' => 'roles-group', 'moduleName' => 'roles-group', 'manager' => \App\Models\RolesGroup\RolesGroupManager::class], function () {

                    Route::get('', ['uses' => 'RolesGroupController@index']);
                    Route::post('/table', ['uses' => 'RolesGroupController@table', 'log' => ['action' => 'search']]);
                    Route::post('/get-roles', ['uses' => 'RolesGroupController@getMenuRoles']);
                    Route::post('/get-roles-attributes', ['uses' => 'RolesGroupController@getRolesAttributes']);

                    Route::group(['checkAccessForUpdate' => true], function () {
                        Route::get('/create', ['uses' => 'RolesGroupController@create', 'log' => ['action' => 'create']]);
                        Route::get('/{id}/{viewType}', ['uses' => 'RolesGroupController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+'])->where('viewType', 'view|edit');
                        Route::post('', ['uses' => 'RolesGroupController@store', 'log' => ['action' => 'store']]);
                        Route::post('/update/{id}', ['uses' => 'RolesGroupController@update', 'log' => ['action' => 'update']]);
                        Route::post('/delete', ['uses' => 'RolesGroupController@delete']);
                    });
                });
            });

            // Menu
            Route::group(['prefix' => 'menu'], function () {
                Route::post('get-roles', ['uses' => 'MenuController@menuRoles']);
                Route::post('get-module-roles', ['uses' => 'MenuController@getModuleAllRoles']);
            });

            // Authorize person
            Route::group(['prefix' => 'authorize-person', 'moduleName' => 'authorize-person', 'manager' => \App\Models\AuthorizePerson\AuthorizePersonManager::class], function () {

                Route::get('', ['uses' => 'AuthorizePersonController@index']);
                Route::post('/table', ['uses' => 'AuthorizePersonController@table', 'log' => ['action' => 'search']]);
                Route::post('roles-and-menus-search', ['uses' => 'AuthorizePersonController@indexPageSearchMenuAndRole']);

                Route::group(['checkAccessForUpdate' => true], function () {
                    Route::get('/create', ['uses' => 'AuthorizePersonController@create', 'log' => ['action' => 'create']]);
                    Route::post('', ['uses' => 'AuthorizePersonController@store', 'log' => ['action' => 'create']]);
                    Route::get('/{id}/{viewType}', ['uses' => 'AuthorizePersonController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+'])->where('viewType', 'view|edit');
                    Route::post('update/{id}', ['uses' => 'AuthorizePersonController@update', 'log' => ['action' => 'update']]);
                    Route::post('render-table', ['uses' => 'AuthorizePersonController@renderTable']);
                    Route::post('get-person-info', ['uses' => 'AuthorizePersonController@getUserBySSN']);
                });
            });

            // Reference
            Route::group(['namespace' => 'Reference'], function () {

                // Reference Table
                Route::group(['prefix' => 'reference-table', 'moduleName' => 'reference-table', 'manager' => \App\Models\ReferenceTable\ReferenceTableManager::class], function () {
                    Route::get('', ['uses' => 'ReferenceController@index']);
                    Route::post('/table', ['uses' => 'ReferenceController@table', 'log' => ['action' => 'search']]);

                    Route::post('ref', ['uses' => 'ReferenceController@referenceTableData'])->name('refData');
                    Route::post('get-document-type', ['uses' => 'ReferenceController@getDocumentType'])->name('searchDocuments');
                    Route::post('get-ref-data', ['uses' => 'ReferenceController@getRefData'])->name('getRefData');
                    Route::get('{id}/export', ['uses' => 'ReferenceController@exportReferenceTableStructure',]);

                    Route::group(['checkAccessForUpdate' => true], function () {
                        Route::get('/create', ['uses' => 'ReferenceController@create', 'log' => ['action' => 'create']]);
                        Route::post('/render-table', ['uses' => 'ReferenceController@renderTable']);
                        Route::post('/render-table-parameter', ['uses' => 'ReferenceController@renderTableParameter']);
                        Route::post('/render-table-fields', ['uses' => 'ReferenceController@renderTableFields']);
                        Route::post('', ['uses' => 'ReferenceController@store', 'log' => ['action' => 'store']]);
                        Route::get('/{id}/{viewType}', ['uses' => 'ReferenceController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+'])->where('viewType', 'view|edit');
                        Route::get('/{id}/clone', ['uses' => 'ReferenceController@cloneRef', 'log' => ['action' => 'clone']])->where(['id' => '[0-9]+']);
                        Route::post('/update/{id}', ['uses' => 'ReferenceController@update', 'log' => ['action' => 'update']]);
                    });
                });

                // Reference Table Form
                Route::group(['prefix' => 'reference-table-form/{id}', 'moduleName' => 'reference-table-form', 'middleware' => 'referenceFormAccess', 'manager' => \App\Models\ReferenceTableForm\ReferenceTableFormManager::class], function () {
                    Route::get('', ['uses' => 'ReferenceTableFormController@index'])->where(['id' => '[0-9]+']);
                    Route::post('/table', ['uses' => 'ReferenceTableFormController@table', 'log' => ['action' => 'search']]);
                    Route::get('/create', ['uses' => 'ReferenceTableFormController@create', 'log' => ['action' => 'create']])->where(['id' => '[0-9]+']);
                    Route::post('', ['uses' => 'ReferenceTableFormController@store', 'log' => ['action' => 'store']]);
                    Route::get('/{id2}/{viewType}', ['uses' => 'ReferenceTableFormController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+'])->where(['id2' => '[0-9]+'])->where('viewType', 'view|edit');
                    Route::post('/update/{id2}', ['uses' => 'ReferenceTableFormController@update', 'log' => ['action' => 'updateVersion']]);
                    Route::post('autocomplete', ['uses' => 'ReferenceTableFormController@autocompleteReferenceField']);
                    Route::post('/delete', ['uses' => 'ReferenceTableFormController@delete']);
                    Route::get('/export', ['uses' => 'ReferenceTableFormController@export', 'log' => ['action' => 'export']])->where(['id' => '[0-9]+']);
                    Route::get('/import', ['uses' => 'ReferenceTableFormController@importShow'])->where(['id' => '[0-9]+']);
                    Route::post('/import', ['uses' => 'ReferenceTableFormController@import', 'log' => ['action' => 'import']])->where(['id' => '[0-9]+']);
                });

            });

            // Data Model
            Route::group(['prefix' => 'data-model', 'moduleName' => 'data-model', 'manager' => \App\Models\DataModel\DataModelManager::class], function () {
                Route::get('', ['uses' => 'DataModelController@index']);
                Route::post('/table', ['uses' => 'DataModelController@table', 'log' => ['action' => 'search']]);

                Route::group(['checkAccessForUpdate' => true], function () {
                    Route::get('/create', ['uses' => 'DataModelController@create', 'log' => ['action' => 'create']]);
                    Route::get('/{id}/{viewType}', ['uses' => 'DataModelController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+'])->where('viewType', 'view|edit');
                    Route::post('/update/{id}', ['uses' => 'DataModelController@update', 'log' => ['action' => 'update']]);
                    Route::post('', ['uses' => 'DataModelController@store', 'log' => ['action' => 'store']]);
                    Route::post('/reference', ['uses' => 'DataModelController@referenceTableSearch']);
                    Route::post('/autocomplete', ['uses' => 'DataModelController@autocomplete']);
                });
            });

            // Single Application
            Route::group(['namespace' => 'SingleApplication', 'prefix' => 'single-application', 'moduleName' => 'single-application', 'manager' => \App\Models\SingleApplication\SingleApplicationManager::class], function () {

                Route::post('/table', ['uses' => 'SingleApplicationController@table', 'log' => ['action' => 'search']]);
                Route::get('/{type}', ['uses' => 'SingleApplicationController@index']);
                Route::post('product/edit', ['uses' => 'SingleApplicationProductsController@productEdit', 'log' => ['action' => 'editProduct']]);
                Route::get('export-saf/{safNumber}', ['uses' => 'SingleApplicationController@exportXmlSafData']);

                Route::group(['checkAccessForUpdate' => true], function () {

                    // Saf Main
                    Route::get('/create/{type}/{number?}', ['uses' => 'SingleApplicationController@create', 'log' => ['action' => 'create']]);
                    Route::get('/{viewType}/{type}/{number}', ['uses' => 'SingleApplicationController@edit', 'log' => ['action' => 'edit']])->where('type', '[A-Za-z]+')->where('viewType', 'view|edit');
                    Route::post('', ['uses' => 'SingleApplicationController@store', 'log' => ['action' => 'update']]);
                    Route::post('delete', ['uses' => 'SingleApplicationController@delete', 'log' => ['action' => 'delete']]);
                    Route::get('clone/{number?}', ['uses' => 'SingleApplicationController@cloneSaf', 'log' => ['action' => 'clone']]);
                    Route::post('send-message', ['uses' => 'SingleApplicationController@sendMessageToSubApplication']);

                    // Saf Products
                    Route::group(['prefix' => 'product', 'moduleName' => 'product', 'manager' => \App\Models\SingleApplicationProducts\SingleApplicationProductsManager::class], function () {

                        Route::post('store', ['uses' => 'SingleApplicationProductsController@productStore', 'log' => ['action' => 'addProduct']]);
                        Route::post('update', ['uses' => 'SingleApplicationProductsController@productUpdate', 'log' => ['action' => 'updateProduct', 'manager' => \App\Models\SingleApplicationProducts\SingleApplicationProductsManager::class]]);
                        Route::post('delete', ['uses' => 'SingleApplicationProductsController@productDelete', 'log' => ['action' => 'deleteProduct']]);

                        Route::post('render-batch-inputs', ['uses' => 'SingleApplicationProductsController@productBatchesInputRender']);
                        Route::post('total-value-national-currency', ['uses' => 'SingleApplicationProductsController@productTotalValueNationalCurrency']);

                        Route::post('hs-codes', ['uses' => 'SingleApplicationProductsController@productHsCodes']);
                        Route::post('producer-code', ['uses' => 'SingleApplicationProductsController@productProducerCode']);

                        Route::post('clone', ['uses' => 'SingleApplicationProductsController@cloneProduct', 'log' => ['action' => 'cloneProduct']]);
                    });

                    // Sub application
                    Route::post('render-sub-application-inputs', ['uses' => 'SingleApplicationSubApplicationController@renderSubApplicationInputs']);
                    Route::post('get-sub-application-type', ['uses' => 'SingleApplicationSubApplicationController@getSubApplicationType']);
                    Route::post('get-sub-application-agency', ['uses' => 'SingleApplicationSubApplicationController@getSubApplicationAgency']);
                    Route::post('get-sub-application-constructor-documents', ['uses' => 'SingleApplicationSubApplicationController@getSubApplicationConstructorDocuments']);
                    Route::post('render-sub-application-products', ['uses' => 'SingleApplicationSubApplicationController@renderSubApplicationProducts']);
                    Route::post('store-manual-sub-application', ['uses' => 'SingleApplicationSubApplicationController@storeManualSubApplication']);
                    Route::post('delete-manual-sub-application', ['uses' => 'SingleApplicationSubApplicationController@deleteManualSubApplication']);
                    Route::post('send-sub-application', ['uses' => 'SingleApplicationSubApplicationController@sendSubApplication', 'log' => ['action' => 'sendSubApplication']]);

                    // Attached Document TAB
                    Route::post('add-document', ['uses' => 'SingleApplicationDocumentsController@addDocument', 'log' => ['action' => 'addDocument']]);
                    Route::post('render-document-products', ['uses' => 'SingleApplicationDocumentsController@renderDocumentProducts']);
                    Route::post('render-document-sub-applications', ['uses' => 'SingleApplicationDocumentsController@renderDocumentSubApplications']);
                    Route::post('render-document-inputs', ['uses' => 'SingleApplicationDocumentsController@renderDocumentInputs']);
                    Route::post('store-document', ['uses' => 'SingleApplicationDocumentsController@storeDocument']);
                    Route::post('delete-document', ['uses' => 'SingleApplicationDocumentsController@deleteDocument', 'log' => ['action' => 'deleteDocument']]);
                    Route::post('update-document-products', ['uses' => 'SingleApplicationDocumentsController@updateDocumentProducts']);
                    Route::post('update-document-sub-applications', ['uses' => 'SingleApplicationDocumentsController@updateDocumentSubApplications']);

                    // Digital Signature part
                    Route::post('/sign-digital-signature', ['uses' => 'SingleApplicationSubApplicationController@signDigitalSignature']);
                    Route::post('/check-digital-signature', ['uses' => 'SingleApplicationSubApplicationController@checkDigitalSignature']);

                    // Obligations
                    Route::post('pay-obligations-modal', ['uses' => 'SingleApplicationObligationsController@payObligationsModal']);
                    Route::post('pay-obligations', ['uses' => 'SingleApplicationObligationsController@payObligations', 'log' => ['action' => 'payObligation']]);
                    Route::post('delete-free-sum-obligation', ['uses' => 'SingleApplicationObligationsController@deleteFreeSumObligation'])->name('deleteFreeSumObligation');
                    Route::post('store-obligation-receipt', ['uses' => 'SingleApplicationObligationsController@storeObligationReceipt']);
                    Route::post('delete-obligation-receipt', ['uses' => 'SingleApplicationObligationsController@deleteObligationReceipt']);

                    // Laboratory
                    Route::post('/laboratory/create', ['uses' => 'SingleApplicationLabExaminationController@renderLaboratoryExaminationView']);
                    Route::post('/laboratory/get-laboratories', ['uses' => 'SingleApplicationLabExaminationController@renderLaboratoryExaminationGetLaboratories']);
                    Route::post('/laboratory/generate-laboratory-applications', ['uses' => 'SingleApplicationLabExaminationController@generateLaboratoryExaminationLaboratories']);
                    Route::post('/laboratory/update-laboratory-applications', ['uses' => 'SingleApplicationLabExaminationController@updateLaboratoryExaminationLaboratories']);
                    Route::post('/laboratory/get-indicators', ['uses' => 'SingleApplicationLabExaminationController@getProductIndicators']);
                    Route::post('/laboratory/get-laboratories-by-indicators', ['uses' => 'SingleApplicationLabExaminationController@getLaboratoriesByIndicators']);
                    Route::post('/laboratory/delete-lab-examination', ['uses' => 'SingleApplicationLabExaminationController@deleteLabExamination']);
                    Route::post('/laboratory/send-to-lab', ['uses' => 'SingleApplicationLabExaminationController@sendToLab']);
                    Route::post('/laboratory/get-products-view', ['uses' => 'SingleApplicationLabExaminationController@getProductsView']);
                    Route::post('/laboratory/get-lab-result', ['uses' => 'SingleApplicationLabExaminationController@getLabResult']);
                });
            });

            // Documents Cloud
            Route::group(['namespace' => 'DocumentsCloud'], function () {

                // Documents Archive
                Route::group(['prefix' => 'document', 'moduleName' => 'document', 'manager' => \App\Models\Document\DocumentManager::class], function () {
                    Route::get('', ['uses' => 'DocumentController@index']);
                    Route::post('/table', ['uses' => 'DocumentController@table', 'log' => ['action' => 'search']]);
                    Route::post('/ref', ['uses' => 'DocumentController@refTable']);
                    Route::post('/type-of-document', ['uses' => 'DocumentController@getTypeOfDocument']);

                    Route::group(['checkAccessForUpdate' => true], function () {
                        Route::get('/create', ['uses' => 'DocumentController@create', 'log' => ['action' => 'create']]);
                        Route::post('', ['uses' => 'DocumentController@store', 'log' => ['action' => 'store']]);
                        Route::get('/{id}/{viewType}', ['uses' => 'DocumentController@edit', 'log' => ['action' => '{viewType}']])->where(['id' => '[0-9]+'])->where('viewType', 'view|edit');
                        Route::post('/update/{id}', ['uses' => 'DocumentController@update', 'log' => ['action' => 'updateVersion']]);
                        Route::post('/delete', ['uses' => 'DocumentController@delete']);
                    });
                });

                // Folders Archive
                Route::group(['prefix' => 'folder', 'moduleName' => 'folder', 'manager' => \App\Models\Folder\FolderManager::class], function () {
                    Route::get('', ['uses' => 'FolderController@index']);
                    Route::post('/table', ['uses' => 'FolderController@table', 'log' => ['action' => 'search']]);
                    Route::post('ref', ['uses' => 'FolderController@refTable']);
                    Route::post('document-search/{folderId?}', ['uses' => 'FolderController@documentFolderSearch']);
                    Route::post('add-document', ['uses' => 'FolderController@addDocument']);
                    Route::post('render-document-inputs', ['uses' => 'FolderController@renderDocumentInputs']);

                    Route::group(['checkAccessForUpdate' => true], function () {
                        Route::get('/create', ['uses' => 'FolderController@create', 'log' => ['action' => 'create']]);
                        Route::post('', ['uses' => 'FolderController@store', 'log' => ['action' => 'store']]);
                        Route::get('/{id}/{viewType}', ['uses' => 'FolderController@edit', 'log' => ['action' => '{viewType}']])->where(['id' => '[0-9]+'])->where('viewType', 'view|edit');
                        Route::post('/update/{id}', ['uses' => 'FolderController@update', 'log' => ['action' => 'update']]);
                        Route::post('store-document/{id}', ['uses' => 'FolderController@storeDocument', 'log' => ['action' => 'addDocument']]);
                        Route::post('delete-document/{id}', ['uses' => 'FolderController@deleteDocument', 'log' => ['action' => 'deleteDocument']]);
                        Route::post('/delete', ['uses' => 'FolderController@delete', 'log' => ['action' => 'delete']]);
                    });

                });

            });

            // Constructor
            Route::group(['namespace' => 'Constructor', 'middleware' => 'constructorAuthAccess'], function () {

                // Constructor Action
                Route::group(['prefix' => 'constructor-actions', 'moduleName' => 'constructor-actions', 'manager' => \App\Models\ConstructorActions\ConstructorActionsManager::class], function () {
                    Route::get('{id?}', ['uses' => 'ConstructorActionsController@index'])->where(['id' => '[0-9]+']);
                    Route::get('/create/{id}', ['uses' => 'ConstructorActionsController@create', 'log' => ['action' => 'create']])->where(['id' => '[0-9]+']);
                    Route::post('', ['uses' => 'ConstructorActionsController@store', 'log' => ['action' => 'store']]);
                    Route::post('/table', ['uses' => 'ConstructorActionsController@table', 'log' => ['action' => 'search']]);
                    Route::get('/{id}/{viewType}', ['uses' => 'ConstructorActionsController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+'])->where('viewType', 'view|edit');
                    Route::post('/update/{id}', ['uses' => 'ConstructorActionsController@update', 'log' => ['action' => 'update']]);
                    Route::post('/delete', ['uses' => 'ConstructorActionsController@delete']);
                });

                // Data set
                Route::group(['prefix' => 'constructor-data-set', 'moduleName' => 'constructor-data-set', 'manager' => \App\Models\ConstructorDataSet\ConstructorDataSetManager::class], function () {
                    Route::get('', ['uses' => 'ConstructorDataSetController@index', 'log' => ['action' => 'edit']]);
                    Route::post('', ['uses' => 'ConstructorDataSetController@store', 'log' => ['action' => 'update']]);
                    Route::post('search', ['uses' => 'ConstructorDataSetController@search', 'log' => ['action' => 'search']]);
                    Route::post('get-mdm-field', ['uses' => 'ConstructorDataSetController@getMDMField']);
                });

                // Constructor Document
                Route::group(['prefix' => 'constructor-documents', 'moduleName' => 'constructor-documents', 'manager' => \App\Models\ConstructorDocument\ConstructorDocumentManager::class], function () {
                    Route::get('', ['uses' => 'ConstructorDocumentController@index']);
                    Route::get('/create', ['uses' => 'ConstructorDocumentController@create', 'log' => ['action' => 'create']]);
                    Route::post('', ['uses' => 'ConstructorDocumentController@store', 'log' => ['action' => 'store']]);
                    Route::post('/table', ['uses' => 'ConstructorDocumentController@table', 'log' => ['action' => 'search']]);
                    Route::get('/{id}/edit', ['uses' => 'ConstructorDocumentController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+']);
                    Route::post('/update/{id}', ['uses' => 'ConstructorDocumentController@update', 'log' => ['action' => 'update']]);
                    Route::post('/delete', ['uses' => 'ConstructorDocumentController@delete']);
                    Route::get('/export/{id}', ['uses' => 'ConstructorDocumentController@export', 'log' => ['action' => 'export']])->where(['id' => '[0-9]+']);
                });

                // Constructor Lists
                Route::group(['prefix' => 'constructor-lists', 'moduleName' => 'constructor-lists', 'manager' => \App\Models\ConstructorLists\ConstructorListsManager::class], function () {
                    Route::get('{id?}', ['uses' => 'ConstructorListsController@index'])->where(['id' => '[0-9]+']);
                    Route::get('/create/{id}', ['uses' => 'ConstructorListsController@create', 'log' => ['action' => 'create']])->where(['id' => '[0-9]+']);
                    Route::post('/table', ['uses' => 'ConstructorListsController@table', 'log' => ['action' => 'search']]);
                    Route::post('', ['uses' => 'ConstructorListsController@store', 'log' => ['action' => 'store']]);
                    Route::get('/{id}/{viewType}', ['uses' => 'ConstructorListsController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+'])->where('viewType', 'view|edit');
                    Route::post('/update/{id}', ['uses' => 'ConstructorListsController@update', 'log' => ['action' => 'update']]);
                    Route::post('/get-saf-fields', ['uses' => 'ConstructorListsController@getSafFields']);
                });

                // Constructor Mapping
                Route::group(['prefix' => 'constructor-mapping', 'moduleName' => 'constructor-mapping', 'manager' => \App\Models\ConstructorMapping\ConstructorMappingManager::class], function () {
                    Route::get('{id?}', ['uses' => 'ConstructorMappingController@index'])->where(['id' => '[0-9]+']);
                    Route::get('/create/{id}', ['uses' => 'ConstructorMappingController@create', 'log' => ['action' => 'create']])->where(['id' => '[0-9]+']);
                    Route::post('', ['uses' => 'ConstructorMappingController@store', 'log' => ['action' => 'store']]);
                    Route::post('/table', ['uses' => 'ConstructorMappingController@table', 'log' => ['action' => 'search']]);
                    Route::get('/{id}/{viewType}', ['uses' => 'ConstructorMappingController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+'])->where('viewType', 'view|edit');
                    Route::post('/update/{id}', ['uses' => 'ConstructorMappingController@update', 'log' => ['action' => 'update']]);
                    Route::post('/delete', ['uses' => 'ConstructorMappingController@delete']);
                });

                // Constructor Notifications
                Route::group(['prefix' => 'constructor-notifications', 'moduleName' => 'constructor-notifications', 'manager' => \App\Models\NotificationTemplates\NotificationTemplatesManager::class], function () {
                    Route::get('', ['uses' => 'ConstructorNotificationsController@index']);
                    Route::get('/create', ['uses' => 'ConstructorNotificationsController@create', 'log' => ['action' => 'create']]);
                    Route::post('', ['uses' => 'ConstructorNotificationsController@store', 'log' => ['action' => 'store']]);
                    Route::post('/table', ['uses' => 'ConstructorNotificationsController@table', 'log' => ['action' => 'search']]);
                    Route::get('/{id}/{viewType}', ['uses' => 'ConstructorNotificationsController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+'])->where('viewType', 'view|edit');
                    Route::post('/update/{id}', ['uses' => 'ConstructorNotificationsController@update', 'log' => ['action' => 'update']]);
                });

                // Constructor Role
                Route::group(['prefix' => 'constructor-roles'], function () {
                    Route::get('', ['uses' => 'ConstructorRolesController@index']);
                    Route::post('/roles', ['uses' => 'ConstructorRolesController@roles']);
                    Route::post('/get-role-field', ['uses' => 'ConstructorRolesController@getRoleField']);
                    Route::post('', ['uses' => 'ConstructorRolesController@store']);
                });

                // Constructor Rules
                Route::group(['prefix' => 'constructor-rules', 'moduleName' => 'constructor-rules', 'manager' => \App\Models\ConstructorRules\ConstructorRulesManager::class], function () {
                    Route::get('', ['uses' => 'ConstructorRulesController@index']);
                    Route::post('{id}/{type}/store', ['uses' => 'ConstructorRulesController@store', 'log' => ['action' => 'store']])->where(['id' => '[0-9]+']);
                    Route::post('/table', ['uses' => 'ConstructorRulesController@table', 'log' => ['action' => 'search']]);
                    Route::get('{id}/{type?}', ['uses' => 'ConstructorRulesController@index'])->where(['id' => '[0-9]+']);
                    Route::get('{id}/{type}/{id2}/edit', ['uses' => 'ConstructorRulesController@edit'])->where(['id' => '[0-9]+', 'id2' => '[0-9]+']);
                    Route::get('{id}/{type}/create', ['uses' => 'ConstructorRulesController@create', 'log' => ['action' => 'create']])->where(['id' => '[0-9]+']);
                    Route::post('{id}/{type}/update/{id2}', ['uses' => 'ConstructorRulesController@update', 'log' => ['action' => 'update']])->where(['id' => '[0-9]+', 'id2' => '[0-9]+']);
                });

                // Constructor State
                Route::group(['prefix' => 'constructor-states', 'moduleName' => 'constructor-states', 'manager' => \App\Models\ConstructorStates\ConstructorStatesManager::class], function () {
                    Route::get('{id?}', ['uses' => 'ConstructorStatesController@index'])->where(['id' => '[0-9]+']);
                    Route::get('/create/{id}', ['uses' => 'ConstructorStatesController@create', 'log' => ['action' => 'create']])->where(['id' => '[0-9]+']);
                    Route::post('', ['uses' => 'ConstructorStatesController@store', 'log' => ['action' => 'store']]);
                    Route::post('/table', ['uses' => 'ConstructorStatesController@table', 'log' => ['action' => 'search']]);
                    Route::get('/{id}/{viewType}', ['uses' => 'ConstructorStatesController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+'])->where('viewType', 'view|edit');
                    Route::post('/update/{id}', ['uses' => 'ConstructorStatesController@update', 'log' => ['action' => 'update']]);
                    Route::post('/delete', ['uses' => 'ConstructorStatesController@delete']);
                });

                // Constructor Templates
                Route::group(['prefix' => 'constructor-templates', 'moduleName' => 'constructor-templates', 'manager' => \App\Models\ConstructorTemplates\ConstructorTemplatesManager::class], function () {
                    Route::get('', ['uses' => 'ConstructorTemplatesController@index']);
                    Route::get('/create', ['uses' => 'ConstructorTemplatesController@create', 'log' => ['action' => 'create']]);
                    Route::post('', ['uses' => 'ConstructorTemplatesController@store', 'log' => ['action' => 'store']]);
                    Route::post('/table', ['uses' => 'ConstructorTemplatesController@table', 'log' => ['action' => 'search']]);
                    Route::get('/{id}/{viewType}', ['uses' => 'ConstructorTemplatesController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+'])->where('viewType', 'view|edit');
                    Route::post('/update/{id}', ['uses' => 'ConstructorTemplatesController@update', 'log' => ['action' => 'update']])->where(['id' => '[0-9]+']);
                });
            });

            // Global Notifications
            Route::group(['prefix' => 'global-notifications', 'moduleName' => 'global-notifications', 'manager' => \App\Models\NotificationTemplates\NotificationTemplatesManager::class], function () {

                Route::get('', ['uses' => 'NotificationTemplatesController@index']);
                Route::post('/table', ['uses' => 'NotificationTemplatesController@table', 'log' => ['action' => 'search']]);

                Route::group(['checkAccessForUpdate' => true], function () {
                    Route::get('/create', ['uses' => 'NotificationTemplatesController@create', 'log' => ['action' => 'create']]);
                    Route::post('', ['uses' => 'NotificationTemplatesController@store', 'log' => ['action' => 'store']]);
                    Route::get('/{id}/{viewType}', ['uses' => 'NotificationTemplatesController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+'])->where('viewType', 'view|edit');
                    Route::get('/{id}/clone', ['uses' => 'NotificationTemplatesController@cloneTemplate', 'log' => ['action' => 'clone']])->where(['id' => '[0-9]+']);
                    Route::post('/update/{id}', ['uses' => 'NotificationTemplatesController@update', 'log' => ['action' => 'update']]);
                    Route::post('/delete', ['uses' => 'NotificationTemplatesController@delete']);
                    Route::post('/send-global-notification', ['uses' => 'NotificationTemplatesController@sendGlobalNotification']);
                });
            });

            // Notifications
            Route::group(['prefix' => 'notifications', 'moduleName' => 'notifications'], function () {
                Route::post('/getData', ['uses' => 'NotificationsController@getData']);
                Route::post('/update', ['uses' => 'NotificationsController@update']);
                Route::post('/mark-as-read', ['uses' => 'NotificationsController@markAsReadAllUnreadNotifications']);
                Route::get('', ['uses' => 'NotificationsController@index']);
                Route::post('/table', ['uses' => 'NotificationsController@table', 'log' => ['action' => 'search']]);
            });

            // Routing
            Route::group(['prefix' => 'routing', 'moduleName' => 'routing', 'manager' => \App\Models\Routing\RoutingManager::class], function () {

                Route::get('', ['uses' => 'RoutingController@index']);
                Route::post('/table', ['uses' => 'RoutingController@table', 'log' => ['action' => 'search']]);
                Route::post('/get-agency-documents', ['uses' => 'RoutingController@constructorDocumentsOfAgency']);

                Route::group(['checkAccessForUpdate' => true], function () {
                    Route::get('/create', ['uses' => 'RoutingController@create', 'log' => ['action' => 'create']]);
                    Route::post('', ['uses' => 'RoutingController@store', 'log' => ['action' => 'store']]);
                    Route::get('/{id}/{viewType}', ['uses' => 'RoutingController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+'])->where('viewType', 'view|edit');
                    Route::post('/update/{id}', ['uses' => 'RoutingController@update', 'log' => ['action' => 'update']]);
                });
            });

            // User documents
            Route::group(['namespace' => 'UserDocuments', 'prefix' => 'user-documents', 'moduleName' => 'user-documents', 'manager' => \App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications::class], function () {

                Route::get('/{id}', ['uses' => 'UserDocumentsController@index'])->where(['id' => '[0-9]+']);
                Route::post('/{id}/table', ['uses' => 'UserDocumentsController@table', 'log' => ['action' => 'search']]);
                Route::get('/{id}/application/{id2}/{viewType}', ['uses' => 'UserDocumentsController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+'])->where(['id2' => '[0-9]+'])->where('viewType', 'view|edit');
                Route::post('/render-sub-application-products', ['uses' => 'UserDocumentsController@renderSubApplicationProducts']);
                Route::post('/{id}/application/{id2}/store', ['uses' => 'UserDocumentsController@store', 'log' => ['action' => 'update']]);
                Route::post('/{id}/application/{id2}/store-default', ['uses' => 'UserDocumentsController@storeDefault', 'log' => ['action' => 'update']]);
                Route::post('/reference/{reference}', ['uses' => 'UserDocumentsController@reference']);
                Route::post('/{id}/application/{id2}/superscribe/store', ['uses' => 'UserDocumentsSuperscribeController@superscribe']);
                Route::post('/{id}/application/{id2}/superscribe/delete', ['uses' => 'UserDocumentsSuperscribeController@superscribeDelete']);
                Route::post('/{id}/application/{id2}/render-document-inputs', ['uses' => 'UserDocumentsDocumentsController@renderDocumentInputs']);
                Route::post('/{id}/application/{id2}/render-document-products', ['uses' => 'UserDocumentsDocumentsController@renderDocumentProducts']);
                Route::post('/{id}/application/{id2}/store-document', ['uses' => 'UserDocumentsDocumentsController@storeDocument']);
                Route::post('/{id}/application/{id2}/add-document', ['uses' => 'UserDocumentsDocumentsController@addDocument', 'log' => ['action' => 'addDocument']]);
                Route::post('/{id}/application/{id2}/update-document-products', ['uses' => 'UserDocumentsDocumentsController@updateDocumentProducts']);
                Route::post('/{id}/application/{id2}/delete-document', ['uses' => 'UserDocumentsDocumentsController@deleteDocument', 'log' => ['action' => 'deleteDocument']]);
                Route::post('/saf-products', ['uses' => 'UserDocumentsController@safProducts']);
                Route::post('/process', ['uses' => 'UserDocumentsOldLaboratoryController@process']);
                Route::post('/store-indicators', ['uses' => 'UserDocumentsOldLaboratoryController@storeIndicators']);
                Route::post('/edit-indicators/{applicationId}/{documentId}/{id}', ['uses' => 'UserDocumentsOldLaboratoryController@editIndicators'])
                    ->where(['id' => '[0-9]+'])
                    ->where(['applicationId' => '[0-9]+'])
                    ->where(['documentId' => '[0-9]+']);

                Route::post('/update-indicators/{id}', ['uses' => 'UserDocumentsOldLaboratoryController@updateIndicators'])->where(['id' => '[0-9]+']);
                Route::post('/regenerate-access-token', ['uses' => 'UserDocumentsController@reGenerateAccessToken']);

                Route::post('/{id}/application/{id2}/get-obligation-row', ['uses' => 'UserDocumentsObligationController@getObligationRow']);
                Route::post('/{id}/application/{id2}/get-obligation-type', ['uses' => 'UserDocumentsController@getObligationTypeBudgetLine']);
                Route::post('/sign-digital-signature', ['uses' => 'UserDocumentsDigitalSignatureController@signDigitalSignature']);
                Route::post('/check-digital-signature', ['uses' => 'UserDocumentsDigitalSignatureController@checkDigitalSignature']);
                Route::post('/{id}/application/{id2}/render-examination-inputs', ['uses' => 'UserDocumentsExaminationController@renderExaminationInputs']);
                Route::post('/{id}/application/{id2}/store-examination', ['uses' => 'UserDocumentsExaminationController@storeExamination']);
                Route::post('/{id}/application/{id2}/delete-examination', ['uses' => 'UserDocumentsExaminationController@deleteExamination']);
                Route::post('/{id}/application/{id2}/edit-examination', ['uses' => 'UserDocumentsExaminationController@editExamination']);
                Route::post('/{id}/application/{id2}/update-examination', ['uses' => 'UserDocumentsExaminationController@updateExamination']);
                Route::get('/{id}/generate/xls', ['uses' => 'UserDocumentsController@generateXls', 'log' => ['action' => 'xls']]);

                // Laboratory
                Route::post('/laboratory/create', ['uses' => 'UserDocumentsLabExaminationController@renderLaboratoryExaminationView']);
                Route::post('/laboratory/get-laboratories', ['uses' => 'UserDocumentsLabExaminationController@renderLaboratoryExaminationGetLaboratories']);
                Route::post('/laboratory/generate-laboratory-applications', ['uses' => 'UserDocumentsLabExaminationController@generateLaboratoryExaminationLaboratories']);
                Route::post('/laboratory/update-laboratory-applications', ['uses' => 'UserDocumentsLabExaminationController@updateLaboratoryExaminationLaboratories']);
                Route::post('/laboratory/get-indicators', ['uses' => 'UserDocumentsLabExaminationController@getProductIndicators']);
                Route::post('/laboratory/get-laboratories-by-indicators', ['uses' => 'UserDocumentsLabExaminationController@getLaboratoriesByIndicators']);
                Route::post('/laboratory/delete-lab-examination', ['uses' => 'UserDocumentsLabExaminationController@deleteLabExamination']);
                Route::post('/laboratory/send-to-lab', ['uses' => 'UserDocumentsLabExaminationController@sendToLab']);
                Route::post('/laboratory/add-indicator-view', ['uses' => 'UserDocumentsLabExaminationController@addIndicatorView']);
                Route::post('/laboratory/get-indicator-value', ['uses' => 'UserDocumentsLabExaminationController@getIndicatorValue']);
                Route::post('/laboratory/get-products-view', ['uses' => 'UserDocumentsLabExaminationController@getProductsView']);
                Route::post('/laboratory/get-lab-result', ['uses' => 'UserDocumentsLabExaminationController@getLabResult']);

            });

            // User laboratories
//            Route::group(['prefix' => 'user-laboratory', 'moduleName' => 'user-laboratory', 'manager' => \App\Models\UserLaboratories\UserLaboratoriesManager::class], function () {
//                Route::get('/{id}', ['uses' => 'UserLaboratoriesController@index'])->name('user-lab')->where(['id' => '[0-9]+']);
//                Route::post('/{id}/table', ['uses' => 'UserLaboratoriesController@table', 'log' => ['action' => 'search']]);
//                Route::get('/{id}/expertise/{id2}/edit', ['uses' => 'UserLaboratoriesController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+'])->where(['expId' => '[0-9]+']);
//                Route::post('/{id}/expertise/{id2}/render-document-inputs', ['uses' => 'UserLaboratoriesController@renderDocumentInputs']);
//                Route::post('/{id}/expertise/{id2}/update-status', ['uses' => 'UserLaboratoriesController@expertiseStatusUpdate', 'log' => ['action' => '']]);
//                Route::post('/{id}/expertise/{id2}/store-document', ['uses' => 'UserLaboratoriesController@storeDocument', 'log' => ['action' => 'addDocument']]);
//                Route::post('/{id}/expertise/{id2}/delete-document', ['uses' => 'UserLaboratoriesController@deleteDocument', 'log' => ['action' => 'deleteDocument']]);
//                Route::post('/{id}/expertise/{id2}/update', ['uses' => 'UserLaboratoriesController@update']);
//                Route::get('/{id}/expertise/{id2}/view', ['uses' => 'UserLaboratoriesController@view', 'log' => ['action' => 'view']])->where(['id' => '[0-9]+'])->where(['expId' => '[0-9]+']);
//                Route::post('/{id}/expertise/{id2}/add-document', ['uses' => 'UserLaboratoriesController@addDocument', 'log' => ['action' => 'addDocument']]);
//                Route::post('/{id}/expertise/{id2}/receive', ['uses' => 'UserLaboratoriesController@receive', 'log' => ['action' => 'receive']]);
//            });

            // Reports
            Route::group(['namespace' => 'Report'], function () {

                // Reports
                Route::group(['prefix' => 'report', 'moduleName' => 'report', 'manager' => '', 'middleware' => 'reportAccess'], function () {
                    Route::get('{code}', ['uses' => 'ReportsController@index']);
                    Route::post('{code}/generate', ['uses' => 'ReportsController@generate']);
                    Route::post('/document-statuses/{documentId}', ['uses' => 'ReportsController@getDocumentStatus']);
                    Route::post('/document-subdivisions', ['uses' => 'ReportsController@getDocumentSubdivisions']);
                    Route::post('/constructor-documents', ['uses' => 'ReportsController@getConstructorDocuments']);
                });

                // Reports access
                Route::group(['prefix' => 'reports-access', 'moduleName' => 'reports-access', 'manager' => \App\Models\ReportsAccess\ReportsAccessManager::class], function () {
                    Route::get('', ['uses' => 'ReportsAccessController@index']);
                    Route::post('/table', ['uses' => 'ReportsAccessController@table', 'log' => ['action' => 'search']]);

                    Route::group(['checkAccessForUpdate' => true], function () {
                        Route::get('/create', ['uses' => 'ReportsAccessController@create', 'log' => ['action' => 'create']]);
                        Route::post('', ['uses' => 'ReportsAccessController@store', 'log' => ['action' => 'store']]);
                        Route::get('/{id}/{viewType}', ['uses' => 'ReportsAccessController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+'])->where('viewType', 'view|edit');
                        Route::post('/update/{id}', ['uses' => 'ReportsAccessController@update', 'log' => ['action' => 'update']]);
                        Route::post('/delete', ['uses' => 'ReportsAccessController@delete', 'log' => ['action' => 'delete']]);
                    });
                });

            });

            // User Manuals
            Route::group(['prefix' => 'user-manuals', 'moduleName' => 'user-manuals', 'manager' => \App\Models\UserManuals\UserManualsManager::class], function () {
                Route::get('', ['uses' => 'UserManualsController@index'])->name('user-manuals');
                Route::get('/create', ['uses' => 'UserManualsController@create', 'log' => ['action' => 'create']]);

                Route::group(['checkAccessForUpdate' => true], function () {
                    Route::post('', ['uses' => 'UserManualsController@store', 'log' => ['action' => 'store']]);
                    Route::get('/{id}/{viewType}', ['uses' => 'UserManualsController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+'])->where('viewType', 'view|edit');
                    Route::post('/update/{id}', ['uses' => 'UserManualsController@update', 'log' => ['action' => 'update']]);
                    Route::post('/delete', ['uses' => 'UserManualsController@delete', 'log' => ['action' => 'delete']]);
                });
            });

            // Payment And Obligations
            Route::group(['prefix' => 'payment-and-obligations', 'moduleName' => 'payment-and-obligations', 'manager' => ''], function () {
                Route::get('', ['uses' => 'PaymentAndObligationController@index']);
                Route::post('/table', ['uses' => 'PaymentAndObligationController@table', 'log' => ['action' => 'search']]);
                Route::get('/generate/xls', ['uses' => 'PaymentAndObligationController@generateXls', 'log' => ['action' => 'xls']]);

                Route::group(['checkAccessForUpdate' => true], function () {
                    Route::post('/pay-obligations-modal', ['uses' => 'PaymentAndObligationController@payObligationsModal']);
                    Route::post('/pay-obligations', ['uses' => 'PaymentAndObligationController@payObligations']);
                    Route::post('/table-total', ['uses' => 'PaymentAndObligationController@getTableTotalPay']);
                });
            });

            // Risks
            Route::group(['namespace' => 'Risks'], function () {

                // Risk instructions
                Route::group(['prefix' => 'risk-instructions', 'moduleName' => 'risk-instructions', 'manager' => \App\Models\Instructions\InstructionsManager::class], function () {
                    Route::get('', ['uses' => 'RiskInstructionsController@index']);
                    Route::post('/table', ['uses' => 'RiskInstructionsController@table', 'log' => ['action' => 'search']]);

                    Route::group(['checkAccessForUpdate' => true], function () {
                        Route::get('/create', ['uses' => 'RiskInstructionsController@create', 'log' => ['action' => 'create']]);
                        Route::get('{id}/{viewType}', ['uses' => 'RiskInstructionsController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+'])->where('viewType', 'view|edit');
                        Route::post('', ['uses' => 'RiskInstructionsController@store', 'log' => ['action' => 'store']]);
                        Route::post('update/{id}', ['uses' => 'RiskInstructionsController@update', 'log' => ['action' => 'updateVersion']])->where(['id' => '[0-9]+']);
                    });
                });

                // Risk profiles
                Route::group(['prefix' => 'risk-profiles', 'moduleName' => 'risk-profiles', 'manager' => \App\Models\RiskProfile\RiskProfileManager::class], function () {
                    Route::get('', ['uses' => 'RiskProfileController@index']);
                    Route::post('/table', ['uses' => 'RiskProfileController@table', 'log' => ['action' => 'search']]);

                    Route::group(['checkAccessForUpdate' => true], function () {
                        Route::get('/create', ['uses' => 'RiskProfileController@create', 'log' => ['action' => 'create']]);
                        Route::post('', ['uses' => 'RiskProfileController@store', 'log' => ['action' => 'store']]);
                        Route::get('{id}/{viewType}', ['uses' => 'RiskProfileController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+'])->where('viewType', 'view|edit');
                        Route::post('update/{id}', ['uses' => 'RiskProfileController@update', 'log' => ['action' => 'updateRisk']])->where(['id' => '[0-9]+']);
                    });
                });
            });

            // Payment Providers crud
            Route::group(['prefix' => 'payment-providers', 'moduleName' => 'payment-providers', 'manager' => \App\Models\PaymentProviders\PaymentProvidersManager::class], function () {
                Route::get('', ['uses' => 'PaymentProvidersController@index']);
                Route::post('/table', ['uses' => 'PaymentProvidersController@table', 'log' => ['action' => 'search']]);

                Route::group(['checkAccessForUpdate' => true], function () {
                    Route::get('/create', ['uses' => 'PaymentProvidersController@create', 'log' => ['action' => 'create']]);
                    Route::post('', ['uses' => 'PaymentProvidersController@store', 'log' => ['action' => 'store']]);
                    Route::get('/{id}/{viewType}', ['uses' => 'PaymentProvidersController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+'])->where('viewType', 'view|edit');
                    Route::post('/update/{id}', ['uses' => 'PaymentProvidersController@update', 'log' => ['action' => 'update']]);
                    Route::post('/delete', ['uses' => 'PaymentProvidersController@delete', 'log' => ['action' => 'delete']]);
                });
            });

            // Payments
            Route::group(['prefix' => 'payments'], function () {
                Route::get('', ['uses' => 'PaymentController@index']);
                Route::post('/table', ['uses' => 'PaymentController@table', 'log' => ['action' => 'search']]);
                Route::get('/generate/xls', ['uses' => 'PaymentController@paymentsTableExport', 'log' => ['action' => 'search']]);
                Route::post('/getDynamicParams', ['uses' => 'PaymentController@getDynamicParams']);
//                Route::post('/refile', ['uses' => 'PaymentController@refile']);
            });

            // Monitoring
            Route::group(['prefix' => 'monitoring', 'moduleName' => 'monitoring', 'manager' => ''], function () {
                Route::get('', ['uses' => 'MonitoringController@index']);
                Route::post('users/table', ['uses' => 'MonitoringController@usersTable', 'log' => ['action' => 'search']]);
                Route::post('user-roles/table', ['uses' => 'MonitoringController@userRolesTable', 'log' => ['action' => 'search']]);
                Route::post('applications/table', ['uses' => 'MonitoringController@applicationsTable', 'log' => ['action' => 'search']]);
                Route::get('applications/export', ['uses' => 'MonitoringController@applicationsTableExport', 'log' => ['action' => 'search']]);
                Route::post('payments/table', ['uses' => 'MonitoringController@paymentsTable', 'log' => ['action' => 'search']]);
                Route::get('payments/export', ['uses' => 'MonitoringController@paymentsTableExport', 'log' => ['action' => 'search']]);
                Route::post('pays/table', ['uses' => 'MonitoringController@paysTable', 'log' => ['action' => 'search']]);
                Route::get('pays/export', ['uses' => 'MonitoringController@paysTableExport', 'log' => ['action' => 'search']]);
                Route::post('get-document-statues', ['uses' => 'MonitoringController@getDocumentStatues', 'log' => ['action' => 'search']]);
                Route::post('get-agency-subdivisions', ['uses' => 'MonitoringController@getAgencySubdivisions', 'log' => ['action' => 'search']]);
            });

            // Customs Portal
            Route::group(['prefix' => 'customs-portal'], function () {
                Route::get('', ['uses' => 'CustomsPortalController@index']);
                Route::post('/table', ['uses' => 'CustomsPortalController@table']);
                Route::post('/get-agency-documents-subdivisions', ['uses' => 'CustomsPortalController@getAgencyConstructDocumentsAndSubdivisions']);
            });

            // Balance Operations
            Route::group(['prefix' => 'balance-operations'], function () {
                Route::get('', ['uses' => 'BalanceOperationsController@index']);
                Route::post('/table', ['uses' => 'BalanceOperationsController@table']);
                Route::post('/get-company-balance', ['uses' => 'BalanceOperationsController@getBalance']);
                Route::post('/transactions-table', ['uses' => 'BalanceOperationsController@transactionsTable']);
                Route::post('/pays-table', ['uses' => 'BalanceOperationsController@paysTable']);

                Route::group(['checkAccessForUpdate' => true], function () {
                    Route::post('/confirm-refund', ['uses' => 'BalanceOperationsController@confirmRefund']);
                });
            });

            // Calendar
            Route::group(['prefix' => 'calendar', 'moduleName' => 'calendar', 'manager' => \App\Models\Calendar\CalendarManager::class], function () {
                Route::get('', ['uses' => 'CalendarController@edit']);
                Route::post('', ['uses' => 'CalendarController@store', 'log' => ['action' => 'store']]);
            });

            // Customs Portal
            Route::group(['prefix' => 'transport-application', 'manager' => \App\Models\TransportApplication\TransportApplicationManager::class], function () {
                Route::get('', ['uses' => 'TransportApplicationController@index']);
                Route::post('/table', ['uses' => 'TransportApplicationController@table']);

                Route::group(['checkAccessForUpdate' => true], function () {
                    Route::get('/create', ['uses' => 'TransportApplicationController@create', 'log' => ['action' => 'create']]);
                    Route::post('', ['uses' => 'TransportApplicationController@store']);
                    Route::get('/{id}/{viewType}', ['uses' => 'TransportApplicationController@edit', 'log' => ['action' => 'edit']])->where(['id' => '[0-9]+'])->where('viewType', 'view|edit');
                    Route::post('/update/{id}', ['uses' => 'TransportApplicationController@update', 'log' => ['action' => 'update']]);
                    Route::post('/reject/{id}', ['uses' => 'TransportApplicationController@reject', 'log' => ['action' => 'reject']]);
                    Route::post('/invoice/{id}', ['uses' => 'TransportApplicationController@invoice', 'log' => ['action' => 'payment_amount']]);
                });

                Route::get('/print-invoice/{id}', ['uses' => 'TransportApplicationController@printInvoice', 'log' => ['action' => 'print_invoice']]);
                Route::get('/print-permit-doc/{id}', ['uses' => 'TransportApplicationController@printPermitDoc', 'log' => ['action' => 'print_permit_doc']]);
            });
        });

    });

    // Generate default PDF from SAF and My Application
    Route::get('/application/{appId}/{pdf_hash_key}', ['uses' => 'UserDocuments\UserDocumentsController@generateGuestPdf'])->where(['appId' => '[0-9]+']);
    Route::get('/user-documents/{id}/application/{id2}/{pdf_hash_key}/{template}/{products?}', ['uses' => 'UserDocuments\UserDocumentsController@generatePdf'])->where(['id' => '[0-9]+'])->where(['id2' => '[0-9]+']);
    Route::get('/document_archive/get_file/{accessToken}', ['uses' => 'DocumentsCloud\DocumentController@uploadByAccessKey']);

    //Generate PDF for obligations
    Route::get('payment-and-obligations/generatePdf/{id}/{pdf_hash_key}', ['uses' => 'PaymentAndObligationController@generateObligationPDF']);

    // API
    Route::group(['prefix' => 'api/v1'], function () {

        // Payment api
        Route::group(['middleware' => 'checkApiParams'], function () {
            Route::post('payment/add', ['uses' => 'PaymentController@add']);
            Route::post('payment/status', ['uses' => 'PaymentController@status']);
            Route::post('payment/check-tin', ['uses' => 'PaymentController@checkTin']);
        });

        // Get data api
        Route::group(['middleware' => 'CheckGetDataApiToken'], function () {
            Route::post('/subapplication/info', ['uses' => 'ApiController@getSubApplicationInfo']);
            Route::post('/document_archive/info', ['uses' => 'ApiController@getDocumentArchiveInfo']);
        });

        // Functions
        Route::group(['middleware' => 'CheckFunctionApiToken', 'prefix' => 'fn'], function () {
            Route::post('/in-ref', ['uses' => 'ApiFunctionsController@inRef']);
            Route::post('/exchange-rate', ['uses' => 'ApiFunctionsController@exchangeRate']);
            Route::post('/exchange-rate-for-field', ['uses' => 'ApiFunctionsController@exchangeRateForField']);
            Route::post('/hour-spend', ['uses' => 'ApiFunctionsController@hourSpend']);
            Route::post('/hour-left', ['uses' => 'ApiFunctionsController@hourLeft']);
            Route::post('/get-products', ['uses' => 'ApiFunctionsController@getProducts']);
            Route::post('/set-product-values', ['uses' => 'ApiFunctionsController@setProductValues']);
            Route::post('/get-batches', ['uses' => 'ApiFunctionsController@getBatches']);
            Route::post('/get-next-id', ['uses' => 'ApiFunctionsController@getNextId']);
            Route::post('/generate-obligation', ['uses' => 'ApiFunctionsController@generateObligation']);
            Route::post('/is-doc-attached', ['uses' => 'ApiFunctionsController@isDocAttached']);
            Route::post('/not-responding-instruction', ['uses' => 'ApiFunctionsController@notRespondingInstruction']);
            Route::post('/is-identic', ['uses' => 'ApiFunctionsController@isIdentic']);
            Route::post('/unanswered-instruction-type', ['uses' => 'ApiFunctionsController@unansweredInstructionType']);
            Route::post('/negative-answered-instruction-type', ['uses' => 'ApiFunctionsController@negativeAnsweredInstructionType']);
            Route::post('/get-lab-obligations', ['uses' => 'ApiFunctionsController@getLabObligations']);
            Route::post('/unanswered-examination', ['uses' => 'ApiFunctionsController@unansweredExamination']);
        });

        Route::post('alif/add', ['uses' => 'PaymentController@alifAdd']);
    });

    // Rules documentation
    Route::get('/swis-rules-docs', function () {
        return view('swis-rules-docs.index');
    });

});
