<?php

return [
    'document_report' => [
        'allowed_types' => [
            'file' => [
                'application/pdf', //pdf
            ]
        ],

        'max_size' => 100 * 1024 * 1024, // bytes
        'disk' => 'shared',
        'tmp_path' => 'pending/',
        'perm_path' => 'files/uploads/files/',
        'multiple' => false,
    ],

    'transport_application_receipt_file' => [
        'allowed_types' => [
            'file' => [
                'application/pdf', //pdf
                'image/jpeg',
                'image/jpg',
            ]
        ],

        'max_size' => 100 * 1024 * 1024, // bytes
        'disk' => 'shared',
        'tmp_path' => 'pending/',
        'perm_path' => 'files/uploads/files/',
        'multiple' => false,
    ],

    'import_excel' => [
        'allowed_types' => [
            'file' => [
                'text/csv',     //excel
                'text/plain',   //excel
            ]
        ],

        'max_size' => 100 * 1024 * 1024, // bytes
        'disk' => 'shared',
        'tmp_path' => 'pending/',
        'perm_path' => 'files/uploads/imports',
        'multiple' => false,
    ],

    'user_manuals' => [
        'allowed_types' => [
            'file' => [
                'application/pdf', //pdf
                'video/mp4',//video format
                'video/webm',//video format
                'video/ogv',//video format
                'video/3gp' //video format
            ]
        ],

        'max_size' => 1024 * 1024 * 1024, // bytes
        'disk' => 'shared',
        'tmp_path' => 'pending/',
        'perm_path' => 'files/uploads/'.\App\Models\UserManuals\UserManuals::PAGE_TYPE_USER_MANUALS . '/',
        'multiple' => false,
    ],

    'obligation_receipt' => [
        'allowed_types' => [
            'file' => [
                'application/pdf', //pdf
                'image/jpeg',
                'image/jpg',
            ]
        ],

        'max_size' => 100 * 1024 * 1024, // bytes
        'disk' => 'shared',
        'tmp_path' => 'pending/',
        'perm_path' => 'files/uploads/files/',
        'multiple' => false,
    ],

    'agency' => [
        'allowed_types' => [
            'file' => [
                'image/png',
                'image/jpeg',
                'image/jpg',
            ]
        ],

        'max_size' => 100 * 1024 * 1024, // bytes
        'disk' => 'shared',
        'tmp_path' => 'pending/',
        'perm_path' => 'images/' . \App\Models\Agency\Agency::IMG_PATH.'/',
        'multiple' => false,
    ],
];