<?php

return [
    'alif' => [
        'url' => env('PAYMENT_ALIF_URL',''),
        'key' => env('PAYMENT_ALIF_KEY',''),
        'secret' => env('PAYMENT_ALIF_SECRET',''),
        'callback_url' => env('PAYMENT_ALIF_CALLBACK_URL', ''),
        'return_url' => env('PAYMENT_ALIF_RETURN_URL', ''),
        'payment_check_url' => env('PAYMENT_ALIF_CHECK_URL',''),
        'payment_check_time' => env('PAYMENT_ALIF_CHECK_TIME',''),
    ],
    'api' => [
        'add' => '/api/v1/payment/add',
        'token' => env('PAYMENT_API_TOKEN','')
    ]
];
