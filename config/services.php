<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'tax_service' => [
        'url' => env('TAX_SERVICE_URL')
    ],

    'jasper_reports' =>[
        'url' =>env('JASPER_SERVER_URL'),
        'username' =>env('JASPER_SERVER_USERNAME'),
        'password' =>env('JASPER_SERVER_PASSWORD')
    ],

    'sms_gateway' => [
        'host' => env('SMS_GATEWAY_HOST', ''),
        'port' => env('SMS_GATEWAY_PORT', ''),
        'username' => env('SMS_GATEWAY_USERNAME', ''),
        'password' => env('SMS_GATEWAY_PASSWORD', ''),
        'from' => env('SMS_GATEWAY_FROM', ''),
    ]

];
