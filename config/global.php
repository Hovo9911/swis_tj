<?php

return [
    "auth_verify" => [
        "email" => [
            "mode" => true,
            "token_count" => 2,
            "send_again_count" => 4,
            'time_activation' => '24'
        ],
        "phone" => [
            "mode" => false,
            "sms_count" => 3,
            "send_again_count" => 3
        ]
    ],
    "ssn" => [
        "min" => env('SSN_MIN_LENGTH'),
        "max" => env('SSN_MAX_LENGTH'),
    ],
    "tax_id" => [
        "min" => env('TAX_ID_MIN_LENGTH'),
        "max" => env('TAX_ID_MAX_LENGTH')
    ],
    "phone_number" => [
        "required" => true,
    ],

    //
    "block_user_duration" => 30,

    //
    "passport" => [
        "min" => env('PASSPORT_NUMBERS_MIN_LENGTH', 8),
        "max" => env('PASSPORT_NUMBERS_MAX_LENGTH', 9),
    ],

    //
    "digital_signature" => env('DIGITAL_SIGNATURE', false),

    //
    "password_characters" => [
        "min" => env('PASSWORD_CHARACTERS_MIN', 6),
        "max" => env('PASSWORD_CHARACTERS_MAX', 12)
    ],

    "user_login" => [
        "block_time" => env('USER_LOGIN_BLOCK_TIME', 5),
        "failed_count" => env('USER_LOGIN_FAILS_COUNT', 5)
    ],

    "password_expiration" => env('PASSWORD_EXPIRATION', 90),

    //
    "constructor_auth_username" => env('CONSTRUCTOR_AUTH_USERNAME', 'admin'),
    "constructor_auth_password" => env('CONSTRUCTOR_AUTH_PASSWORD'),
];