<?php

return [
    'functions' => [
        'inRef' => env('APP_URL','').'/en/api/v1/fn/in-ref',
        'exchangeRate' => env('APP_URL','').'/en/api/v1/fn/exchange-rate',
        'hourSpend' => env('APP_URL','').'/en/api/v1/fn/hour-spend',
        'hourLeft' => env('APP_URL','').'/en/api/v1/fn/hour-left',
        'getProducts' => env('APP_URL','').'/en/api/v1/fn/get-products',
        'setProductValues' => env('APP_URL','').'/en/api/v1/fn/set-product-values',
        'getBatches' => env('APP_URL','').'/en/api/v1/fn/get-batches',
        'getNextId' => env('APP_URL','').'/en/api/v1/fn/get-next-id',
        'generateObligation' => env('APP_URL','').'/en/api/v1/fn/generate-obligation',
        'isDocAttached' => env('APP_URL','').'/en/api/v1/fn/is-doc-attached',
        'notRespondingInstruction' => env('APP_URL','').'/en/api/v1/fn/not-responding-instruction',
        'isIdentic' => env('APP_URL','').'/en/api/v1/fn/is-identic',
        'unansweredInstructionType' => env('APP_URL','').'/en/api/v1/fn/unanswered-instruction-type',
        'negativeAnsweredInstructionType' => env('APP_URL','').'/en/api/v1/fn/negative-answered-instruction-type',
        'getLabObligations' => env('APP_URL','').'/en/api/v1/fn/get-lab-obligations',
        'unansweredExamination' => env('APP_URL','').'/en/api/v1/fn/unanswered-examination',
    ]
];
