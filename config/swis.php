<?php


return [
    'theme_type' => env('THEME_TYPE', 'green--theme'),

    //
    'date_format' => env('DATE_FORMAT', 'Y-m-d'),
    'date_time_format' => env('DATE_TIME_FORMAT', 'Y-m-d H:i:s'),
    'date_time_format_local' => env('DATE_TIME_FORMAT_LOCAL', 'Y-m-d\TH:i'),
    'date_format_front' => env('DATE_FORMAT_FRONT', 'd/m/Y'),
    'date_time_format_front' => env('DATE_TIME_FORMAT_FRONT', 'd/m/Y H:i:s'),
    'date_time_format_front_without_second' => env('DATE_TIME_FORMAT_FRONT', 'd/m/Y H:i'),
    'date_format_report' => env('DATE_TIME_FORMAT_REPORT', 'd.m.Y'),

    //
    'default_currency' => env('DEFAULT_CURRENCY', 'TJS'),

    //
    'country_mode' => env('COUNTRY_MODE', 'TJ'),
    'country_mode_2' => env('COUNTRY_MODE_2', false),

    //
    'sms_country_phone_number_prefix' => env('SMS_COUNTRY_PHONE_NUMBER_PREFIX'),
    'sms_country_phone_number_count' => env('SMS_COUNTRY_PHONE_NUMBER_COUNT'),
    'sms_send' => env('SMS_SEND' , false),

    //
    'obligation_receipt_mode' => env('OBLIGATION_ENABLE_RECEIPT' , false),

    //
    'working_hours_start' => env('WORKING_HOURS_START',false),
    'working_hours_end' => env('WORKING_HOURS_END',false),
    'lunch_hours_start' => env('LUNCH_HOURS_START',false),
    'lunch_hours_end' => env('LUNCH_HOURS_END',false),

    //
    'email_send' => env('EMAIL_SEND' , true),
    'in_app_notifications' => env('IN_APP_NOTIFICATIONS' , true),
];

