<?php

use Illuminate\Database\Seeder;

class MenuTableLaboratories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $labMenuId = \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::LABORATORY_MENU_NAME);

        $data = [
            [
                'name'=>'laboratory_creating_menu',
                'title'=>'swis.laboratory_creating_menu.title',
                'href'=>'laboratory',
                'parent_id'=>$labMenuId,
                'show_status'=>\App\Models\Menu\Menu::STATUS_ACTIVE
            ],
            [
                'name'=>'laboratory_indicator_menu',
                'title'=>'swis.laboratory_indicator_menu.title',
                'href'=>'laboratory-indicator',
                'parent_id'=>$labMenuId,
                'show_status'=>\App\Models\Menu\Menu::STATUS_ACTIVE
            ],
        ];


        \Illuminate\Support\Facades\DB::table('menu')->insert($data);
    }
}
