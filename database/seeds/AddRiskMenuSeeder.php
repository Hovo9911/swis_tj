<?php

use App\Models\Menu\Menu;
use App\Models\Role\Role;
use App\Models\RoleMenus\RoleMenus;
use Illuminate\Database\Seeder;

class AddRiskMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $paymentMenu = [
            'name' => 'risks_menu',
            'title' => 'swis.risks.title',
            'icon' => '<i class="fa fa-crosshairs"></i>',
            'href' => null,
            'parent_id' => null,
            'sort_order' => 92,
            'show_status' => '1',
            'is_module' => '1'
        ];

        $menu = new Menu($paymentMenu);
        $menu->save();

        $parenMenus = [
            [
                'name' => 'risk_instructions_menu',
                'title' => 'swis.risk_instructions.title',
                'icon' => '',
                'href' => 'risk-instructions',
                'parent_id' => $menu->id,
                'sort_order' => 93,
                'show_status' => '1',
                'is_module' => '0'
            ],
            [
                'name' => 'risk_profiles_menu',
                'title' => 'swis.risk_profiles.title',
                'icon' => '',
                'href' => 'risk-profiles',
                'parent_id' => $menu->id,
                'sort_order' => 94,
                'show_status' => '1',
                'is_module' => '0'
            ]
        ];

        Menu::insert($parenMenus);

        $rolesMenu = [
            [
                'role_id' => Role::HEAD_OF_COMPANY,
                'menu_id' => $menu->id
            ]
        ];
        RoleMenus::insert($rolesMenu);
    }
}
