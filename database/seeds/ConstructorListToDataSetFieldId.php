<?php

use App\Models\ConstructorListToDataSetFromMdm\ConstructorListToDataSetFromMdm;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConstructorListToDataSetFieldId extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ConstructorListToDataSetFromMdm::all();
        if ($data && count($data) > 0) {
            DB::transaction(function () use ($data) {
                foreach ($data as &$item) {
                    $dataSetMdm = App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm::select('field_id')
                        ->where('document_id', $item->relatedList->document_id)
                        ->where('mdm_field_id', $item->mdm_field_id)
                        ->first();

                    if (isset($dataSetMdm)) {
                        ConstructorListToDataSetFromMdm::where('constructor_list_id', $item->constructor_list_id)
                            ->where('mdm_field_id', $item->mdm_field_id)
                            ->update(['constructor_data_set_field_id' => $dataSetMdm->field_id]);
                    }
                }
            });
        }
    }
}
