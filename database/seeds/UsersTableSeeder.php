<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        if(is_null(DB::table('users')->where(['ssn'=>'1903820146'])->first())){
            $user = DB::table('users')->insertGetId([
                'first_name' => 'Super',
                'last_name' => 'Admin',
                'ssn' => '1903820146',
                'login' => 'login',
                'password' => 'admin',
                'allowed_from_ips' => '',
                'registered_at' => \Carbon\Carbon::now(),
                'lastlogin_at' => \Carbon\Carbon::now(),
                'lastlogin_ip' => request()->ip(),
                'email' => 'swadmin@gmail.com',
                'address' => 'adress',
                'is_email_verified' => '1',
                'is_phone_verified' => '1',
                'lng_id' => '1'
            ]);

            DB::table('user_roles')->insert([
                   [
                       'user_id' => $user,
                       'role_id' => \App\Models\Role\Role::SW_ADMIN,
                       'show_status' => \App\Models\Role\Role::STATUS_ACTIVE,
                       'is_local_admin' => '0'
                   ],
                   [
                       'user_id' => $user,
                       'role_id' => \App\Models\Role\Role::NATURAL_PERSON,
                       'show_status' => \App\Models\Role\Role::STATUS_ACTIVE,
                       'is_local_admin' => '0'
                   ],
            ]);
        }



    }
}
