<?php

use App\Models\Menu\Menu;
use App\Models\RoleMenus\RoleMenus;
use Illuminate\Database\Seeder;

class AddPaymentAndObligationMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $paymentMenu = [
            'name' => 'payment_and_obligation',
            'title' => 'swis.payment_and_obligation.title',
            'icon' => '<i class="far fa-credit-card"></i>',
            'href' => 'payment-and-obligations',
            'parent_id' => null,
            'sort_order' => 91,
            'show_status' => '1',
        ];

        $menu = new Menu($paymentMenu);
        $menu->save();

        $rolesMenu = [
            [
                'role_id' => 2,
                'menu_id' => $menu->id
            ],
            [
                'role_id' => 3,
                'menu_id' => $menu->id
            ],
            [
                'role_id' => 1,
                'menu_id' => $menu->id
            ],
            [
                'role_id' => 4,
                'menu_id' => $menu->id
            ],
            [
                'role_id' => 9,
                'menu_id' => $menu->id
            ]
        ];

        RoleMenus::insert($rolesMenu);
    }
}
