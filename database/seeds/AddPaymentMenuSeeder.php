<?php

use Illuminate\Database\Seeder;

class AddPaymentMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $paymentMenu = [
            'name' => 'payments_menu',
            'title' => 'swis.payments.title',
            'icon' => '<i class="far fa-credit-card"></i>',
            'href' => 'payments',
            'parent_id' => null,
            'sort_order' => 90,
            'show_status' => '1',
        ];

        $menu = new \App\Models\Menu\Menu($paymentMenu);
        $menu->save();

        $rolesMenu = [
            [
                'role_id' => 2,
                'menu_id' => $menu->id
            ],
            [
                'role_id' => 3,
                'menu_id' => $menu->id
            ],
        ];
        \App\Models\RoleMenus\RoleMenus::insert($rolesMenu);
    }
}
