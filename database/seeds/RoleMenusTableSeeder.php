<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Role\Role;

class RoleMenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $_SW_ADMIN = [

            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::AGENCY_MENU_NAME),
                'role_id' => Role::SW_ADMIN,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::REGIONAL_OFFICE_MENU_NAME),
                'role_id' => Role::SW_ADMIN,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::LABORATORY_MENU_NAME),
                'role_id' => Role::SW_ADMIN,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::BROKER_MENU_NAME),
                'role_id' => Role::SW_ADMIN,
            ],


            /*[
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::DOCUMENT_CLOUD_MENU_NAME),
                'role_id' => Role::SW_ADMIN,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::DOCUMENT_CLOUD_FOLDER_MENU_NAME),
                'role_id' => Role::SW_ADMIN,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::DOCUMENT_CLOUD_DOCUMENT_MENU_NAME),
                'role_id' => Role::SW_ADMIN,
            ],*/


            /*[
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::SINGLE_APPLICATION_MENU_NAME),
                'role_id' => Role::SW_ADMIN,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::SINGLE_APPLICATION_IMPORT_MENU_NAME),
                'role_id' => Role::SW_ADMIN,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::SINGLE_APPLICATION_EXPORT_MENU_NAME),
                'role_id' => Role::SW_ADMIN,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::SINGLE_APPLICATION_TRANSIT_MENU_NAME),
                'role_id' => Role::SW_ADMIN,
            ],*/


            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::REFERENCE_TABLE_MENU_NAME),
                'role_id' => Role::SW_ADMIN,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::REFERENCE_TABLE_FORM_MENU_NAME),
                'role_id' => Role::SW_ADMIN,
            ],


            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::DATA_MODEL_MENU_NAME),
                'role_id' => Role::SW_ADMIN,
            ],
           /* [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::AUTHORIZE_PERSON_MENU_NAME),
                'role_id' => Role::SW_ADMIN,
            ],*/


            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::USER_MANAGEMENT_MENU_NAME),
                'role_id' => Role::SW_ADMIN,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::ROLE_MENU_NAME),
                'role_id' => Role::SW_ADMIN,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::ROLE_GROUP_MENU_NAME),
                'role_id' => Role::SW_ADMIN,
            ],


            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::ROUTING_MENU_NAME),
                'role_id' => Role::SW_ADMIN,
            ],

        ];

        if (env('COUNTRY_MODE') == 'TJ') {
            $_SW_ADMIN[] = [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::USER_MENU_NAME),
                'role_id' => Role::SW_ADMIN,
            ];
        }

        $_HEAD_OF_COMPANY = [

            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::REGIONAL_OFFICE_MENU_NAME),
                'role_id' => Role::HEAD_OF_COMPANY,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::LABORATORY_MENU_NAME),
                'role_id' => Role::HEAD_OF_COMPANY,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::USER_MANAGEMENT_MENU_NAME),
                'role_id' => Role::HEAD_OF_COMPANY,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::ROLE_MENU_NAME),
                'role_id' => Role::HEAD_OF_COMPANY,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::ROLE_GROUP_MENU_NAME),
                'role_id' => Role::HEAD_OF_COMPANY,
            ],

           /* [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::AUTHORIZE_PERSON_MENU_NAME),
                'role_id' => Role::HEAD_OF_COMPANY,
            ],*/


            /*[
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::DOCUMENT_CLOUD_MENU_NAME),
                'role_id' => Role::HEAD_OF_COMPANY,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::DOCUMENT_CLOUD_FOLDER_MENU_NAME),
                'role_id' => Role::HEAD_OF_COMPANY,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::DOCUMENT_CLOUD_DOCUMENT_MENU_NAME),
                'role_id' => Role::HEAD_OF_COMPANY,
            ],*/


           /* [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::SINGLE_APPLICATION_MENU_NAME),
                'role_id' => Role::HEAD_OF_COMPANY,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::SINGLE_APPLICATION_IMPORT_MENU_NAME),
                'role_id' => Role::HEAD_OF_COMPANY,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::SINGLE_APPLICATION_EXPORT_MENU_NAME),
                'role_id' => Role::HEAD_OF_COMPANY,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::SINGLE_APPLICATION_TRANSIT_MENU_NAME),
                'role_id' => Role::HEAD_OF_COMPANY,
            ],*/


            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::CONSTRUCTOR_MENU_NAME),
                'role_id' => Role::HEAD_OF_COMPANY,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::CONSTRUCTOR_DOCUMENTS_MENU_NAME),
                'role_id' => Role::HEAD_OF_COMPANY,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::CONSTRUCTOR_STATES_MENU_NAME),
                'role_id' => Role::HEAD_OF_COMPANY,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::CONSTRUCTOR_ACTIONS_MENU_NAME),
                'role_id' => Role::HEAD_OF_COMPANY,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::CONSTRUCTOR_DATA_SET_MENU_NAME),
                'role_id' => Role::HEAD_OF_COMPANY,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::CONSTRUCTOR_ROLE_MENU_NAME),
                'role_id' => Role::HEAD_OF_COMPANY,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::CONSTRUCTOR_MAPPING_NAME),
                'role_id' => Role::HEAD_OF_COMPANY,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::CONSTRUCTOR_LISTS_NAME),
                'role_id' => Role::HEAD_OF_COMPANY,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::CONSTRUCTOR_TEMPLATES_NAME),
                'role_id' => Role::HEAD_OF_COMPANY,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::REFERENCE_TABLE_FORM_MENU_NAME),
                'role_id' => Role::HEAD_OF_COMPANY,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::USER_DOCUMENTS_MENU_NAME),
                'role_id' => Role::HEAD_OF_COMPANY,
            ],
        ];


        $_NATURAL_PERSON = [
            /*[
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::DOCUMENT_CLOUD_MENU_NAME),
                'role_id' => Role::NATURAL_PERSON,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::DOCUMENT_CLOUD_FOLDER_MENU_NAME),
                'role_id' => Role::NATURAL_PERSON,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::DOCUMENT_CLOUD_DOCUMENT_MENU_NAME),
                'role_id' => Role::NATURAL_PERSON,
            ],


            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::SINGLE_APPLICATION_MENU_NAME),
                'role_id' => Role::NATURAL_PERSON,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::SINGLE_APPLICATION_IMPORT_MENU_NAME),
                'role_id' => Role::NATURAL_PERSON,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::SINGLE_APPLICATION_EXPORT_MENU_NAME),
                'role_id' => Role::NATURAL_PERSON,
            ],
            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::SINGLE_APPLICATION_TRANSIT_MENU_NAME),
                'role_id' => Role::NATURAL_PERSON,
            ],


            [
                'menu_id' => \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::AUTHORIZE_PERSON_MENU_NAME),
                'role_id' => Role::NATURAL_PERSON,
            ],*/
        ];


        $data = array_merge($_SW_ADMIN, $_HEAD_OF_COMPANY, $_NATURAL_PERSON);

        DB::table('role_menus')->insert($data);
    }
}
