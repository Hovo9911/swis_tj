<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [
            [
                'name'=>'SW admin',
                'level'=>App\Models\Role\Role::DEFAULT_ROLE_LEVEL,
                'show_status'=>App\Models\Role\Role::STATUS_ACTIVE,
                'menu_id'=>null,
                'create_permission'=>'1',
                'read_permission'=>'1',
                'update_permission'=>'1',
                'delete_permission'=>'1',
            ],
            [
                'name'=>'Natural person',
                'level'=>App\Models\Role\Role::DEFAULT_ROLE_LEVEL,
                'show_status'=>App\Models\Role\Role::STATUS_ACTIVE,
                'menu_id'=>null,
                'create_permission'=>'1',
                'read_permission'=>'1',
                'update_permission'=>'1',
                'delete_permission'=>'1',
            ],

            [
                'name'=>'Head of company',
                'level'=>App\Models\Role\Role::DEFAULT_ROLE_LEVEL,
                'show_status'=>App\Models\Role\Role::STATUS_ACTIVE,
                'menu_id'=>null,
                'create_permission'=>'1',
                'read_permission'=>'1',
                'update_permission'=>'1',
                'delete_permission'=>'1',
            ],

            [
                'name'=>'Employee',
                'level'=>App\Models\Role\Role::DEFAULT_ROLE_LEVEL,
                'show_status'=>App\Models\Role\Role::STATUS_ACTIVE,
                'menu_id'=>null,
                'create_permission'=>'1',
                'read_permission'=>'1',
                'update_permission'=>'1',
                'delete_permission'=>'1',
            ] ,
            [
                'name'=>'Auth New Person',
                'level'=>App\Models\Role\Role::DEFAULT_ROLE_LEVEL,
                'show_status'=>App\Models\Role\Role::STATUS_ACTIVE,
                'menu_id'=>\App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::AUTHORIZE_PERSON_MENU_NAME),
                'create_permission'=>'1',
                'read_permission'=>'1',
                'update_permission'=>'1',
                'delete_permission'=>'1',
            ],
            [
                'name'=>'My Applications',
                'level'=>App\Models\Role\Role::DEFAULT_ROLE_LEVEL,
                'show_status'=>App\Models\Role\Role::STATUS_ACTIVE,
                'menu_id'=>null,
                'create_permission'=>'1',
                'read_permission'=>'1',
                'update_permission'=>'1',
                'delete_permission'=>'1',
            ],
            [
                'name'=>'All functionality',
                'level'=>App\Models\Role\Role::DEFAULT_ROLE_LEVEL,
                'show_status'=>App\Models\Role\Role::STATUS_ACTIVE,
                'menu_id'=>null,
                'create_permission'=>'1',
                'read_permission'=>'1',
                'update_permission'=>'1',
                'delete_permission'=>'1',
            ]
        ];


        DB::table('roles')->insert($data);
    }
}
