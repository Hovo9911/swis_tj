<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menus = json_decode(file_get_contents(public_path('menus.json')),true);

        $data = [];
        foreach ($menus as $key=>$menu){
            $data[] = [
                'title'=>$menu['title'],
                'icon'=>$menu['icon'],
                'href'=>@$menu['href'],
            ];
        }

        DB::table('menu')->insert($data);
    }
}
