<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;
use App\Models\Sms\Sms;

class CreateSmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('sms', function(Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('user_id')->default('0');
            $table->string('phone');
            $table->text('message');
            $table->enum('status', [Sms::STATUS_PENDING, Sms::STATUS_FAILED, Sms::STATUS_SENT])->default(Sms::STATUS_PENDING);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms');
    }
}
