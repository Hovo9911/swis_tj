<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use App\Models\RegionalOffice\RegionalOffice;
use App\Models\RegionalOffice\RegionalOfficeLaboratory;

class AddRegionalOfficeCodeToRegionalOfficeLaboratory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('regional_office_laboratory', function (Blueprint $table) {
            $table->string('regional_office_code')->nullable();
            $table->integer('agency_laboratory_id')->nullable();
        });

        $labSubDivisions = RegionalOfficeLaboratory::select('id', 'regional_office_id', 'regional_office_code', 'laboratory_id')->get();
        $subDivisions = RegionalOffice::select('id', 'office_code')->whereIn('id', $labSubDivisions->pluck('regional_office_id')->all())->pluck('office_code', 'id')->all();
        $laboratories = \App\Models\Laboratory\Laboratory::select('laboratory.id', 'agency.id as agency_id')
            ->join('agency', function ($q) {
                $q->on('agency.tax_id', '=', 'laboratory.tax_id')->where('agency.show_status', \App\Models\BaseModel::STATUS_ACTIVE);
            })
            ->whereIn('laboratory.id', $labSubDivisions->pluck('laboratory_id')->all())
            ->pluck('agency_id', 'id')
            ->all();

        foreach ($labSubDivisions as $labSubDivision) {
            $labSubDivision->regional_office_code = $subDivisions[$labSubDivision['regional_office_id']];
            $labSubDivision->agency_laboratory_id = $laboratories[$labSubDivision['laboratory_id']];
            $labSubDivision->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('regional_office_laboratory', function (Blueprint $table) {
            $table->dropColumn('regional_office_code');
        });
    }
}
