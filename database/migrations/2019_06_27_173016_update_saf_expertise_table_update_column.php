<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class UpdateSafExpertiseTableUpdateColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE "saf_expertise" ALTER COLUMN "sampler_code" TYPE TEXT;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE "saf_expertise" ALTER COLUMN "sampler_code" TYPE VARCHAR;');
    }
}
