<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateSafTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('saf', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_tax_id')->default('0');
            $table->unsignedInteger('saf_data_structure_id')->nullable();
            $table->enum('regime',['import','export','transit'])->default('import');
            $table->string('regular_number')->unique();
            $table->unsignedInteger('user_id');
            $table->text('data')->nullable();
            $table->enum('status_type',['draft','send','created','void'])->default('draft');
            $table->adminInfo();
            $table->showStatus();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saf');
    }
}
