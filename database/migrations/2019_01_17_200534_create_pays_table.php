<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;

class CreatePaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ip');
            $table->integer('service_provider_id');
            $table->string('document_id')->unique();
            $table->dateTime('payment_done_date')->nullable();
            $table->float('amount', 8, 2);
            $table->string('beneficiary_name')->nullable();
            $table->string('tax_id')->nullable();
            $table->string('description')->nullable();
            $table->string('status');
            $table->enum('sended_into_balance', ['yes', 'no'])->default('yes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
