<?php

use App\Migration\Blueprint;
use App\Migration\Migration;


class UpdateSafObligationsTableAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_obligations', function (\App\Migration\Blueprint $table) {
            $table->integer('ref_budget_line_id')->default('0')->after('free_sum_budget_line_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_obligations', function (\App\Migration\Blueprint $table) {
            $table->dropColumn('ref_budget_line_id');
        });
    }
}
