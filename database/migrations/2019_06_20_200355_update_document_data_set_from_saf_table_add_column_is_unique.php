<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateDocumentDataSetFromSafTableAddColumnIsUnique extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('documents_dataset_from_saf', function (Blueprint $table) {
            $table->tinyInteger('is_unique')->default(0)->after('show_in_search_results');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('documents_dataset_from_saf', function (Blueprint $table) {
            $table->dropColumn('is_unique');
        });
    }
}
