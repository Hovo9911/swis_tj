<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateNotifySubapplicationExpirationDateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('notify_subapplication_expiration_date', function (Blueprint $table) {
            $table->renameColumn('working_hours_in_requested_status', 'working_minutes_in_requested_status');
            $table->dateTime('date_of_subapplication_end');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('notify_subapplication_expiration_date', function (Blueprint $table) {
            $table->renameColumn('working_minutes_in_requested_status', 'working_hours_in_requested_status');
            $table->dropColumn('date_of_subapplication_end');
        });
    }
}
