<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;

class CreateRolesGroupRolesAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->create('roles_group_roles_attributes', function (Blueprint $table) {
            $table->unsignedInteger('roles_group_id');
            $table->unsignedInteger('role_id');
            $table->string('attribute_id');
            $table->enum('attribute_type',['saf','mdm']);
            $table->index(['roles_group_id','role_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles_group_roles_attributes');
    }
}
