<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateDataModelExcelTableAddNewColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('data_model_excel', function (Blueprint $table) {
            $table->integer('eaeu_total_digits')->nullable()->after('eaeu_max');
            $table->integer('eaeu_fraction_digits')->nullable()->after('eaeu_total_digits');
            $table->integer('decimal_fraction')->nullable()->after('eaeu_fraction_digits');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('data_model_excel', function (Blueprint $table) {
            $table->dropColumn('eaeu_total_digits');
            $table->dropColumn('eaeu_fraction_digits');
            $table->dropColumn('decimal_fraction');
        });
    }
}

