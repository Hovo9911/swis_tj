<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateConstructorActionSuperscribesTableAddEndStateId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_action_superscribes', function (Blueprint $table) {
            $table->string('end_state_id')->after('start_state_id')->unsignedInteger();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_action_superscribes', function (Blueprint $table) {
            $table->dropColumn('end_state_id');
        });
    }
}
