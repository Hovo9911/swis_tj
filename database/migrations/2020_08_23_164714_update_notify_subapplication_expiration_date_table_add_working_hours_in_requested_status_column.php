<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateNotifySubapplicationExpirationDateTableAddWorkingHoursInRequestedStatusColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('notify_subapplication_expiration_date', function (Blueprint $table) {
            $table->integer('working_hours_in_requested_status')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('notify_subapplication_expiration_date', function (Blueprint $table) {
            $table->dropColumn(['working_hours_in_requested_status']);
        });
    }
}
