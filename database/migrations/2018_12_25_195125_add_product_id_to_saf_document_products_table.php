<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddProductIdToSafDocumentProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_document_products', function (Blueprint $table) {
            $table->integer('product_id')->after('products');
            $table->integer('saf_document_id')->after('product_id');
            $table->dropColumn('document_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_document_products', function (Blueprint $table) {
            $table->integer('document_id')->after('products');
            $table->dropColumn(['product_id', 'saf_document_id']);
        });
    }
}
