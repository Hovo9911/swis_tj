<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddCodeForAgencyModule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('agency_lab_indicators', function (Blueprint $table) {
            $table->string('lab_hs_indicator_code')->nullable();
        });

        $schemaBuilder->table('laboratory_indicator', function (Blueprint $table) {
            $table->string('indicator_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('agency_lab_indicators', function (Blueprint $table) {
            $table->dropColumn('lab_hs_indicator_code');
        });

        $schemaBuilder->table('laboratory_indicator', function (Blueprint $table) {
            $table->dropColumn('indicator_code');
        });
    }
}
