<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class UpdateRoutingTableUpdateColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `routing` CHANGE COLUMN `document_id` `constructor_document_id` INT(11) UNSIGNED NOT NULL ;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `routing` CHANGE COLUMN `constructor_document_id` `document_id` INT(11) UNSIGNED NOT NULL ;");
    }
}
