<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddNotificationIdToStoreNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('notifications', function (Blueprint $table) {
            $table->integer('notification_id')->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('notifications', function (Blueprint $table) {
            $table->dropColumn('notification_id');
        });
    }
}
