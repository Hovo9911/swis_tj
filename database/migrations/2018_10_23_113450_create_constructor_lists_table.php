<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateConstructorListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('constructor_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('document_id');
            $table->integer('list_type');
            $table->string('description');
            $table->showStatus();
            $table->adminInfo();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('constructor_lists');
    }
}
