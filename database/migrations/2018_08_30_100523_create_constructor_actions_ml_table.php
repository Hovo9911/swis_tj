<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateConstructorActionsMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('constructor_actions_ml', function (Blueprint $table) {
            $table->unsignedInteger('constructor_actions_id');
            $table->unsignedSmallInteger('lng_id');
            $table->string('action_name');
            $table->text('description');
            $table->showStatus();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('constructor_actions_ml');
    }
}
