<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class ChangeDocumentDatasetFromMdmIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_list_to_data_set_from_mdm', function (Blueprint $table) {
            $table->dropColumn('document_dataset_from_mdm_id');
            $table->integer('mdm_field_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_list_to_data_set_from_mdm', function (Blueprint $table) {
            $table->dropColumn('mdm_field_id');
            $table->integer('document_dataset_from_mdm_id');
        });
    }
}
