<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddMdmDataFieldToSafProductsBatch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_products_batch', function (Blueprint $table) {
            $table->json('mdm_data')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_products_batch', function (Blueprint $table) {
            $table->dropColumn('mdm_data');
        });
    }
}
