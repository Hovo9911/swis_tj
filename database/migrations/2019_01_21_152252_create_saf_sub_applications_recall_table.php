<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSafSubApplicationsRecallTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saf_sub_applications_recall', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_tax_id')->default('0');
            $table->string('saf_number');
            $table->unsignedInteger('sub_application_id');
            $table->unsignedInteger('from_user_id');
            $table->unsignedInteger('answered_user_id')->nullable();
            $table->unsignedInteger('answered_state_id')->nullable();
            $table->tinyInteger('is_answered')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saf_sub_applications_recall');
    }
}
