<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddCountriesLongNameToReferenceCountriesMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('reference_countries_ml', function (Blueprint $table) {
            $table->text('long_name')->after('name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('reference_countries_ml', function (Blueprint $table) {
            $table->dropColumn('long_name');
        });
    }
}
