<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateCalendarTableSetUniqueFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('calendar', function (Blueprint $table) {
            $table->unique(array('agency_id', 'date_from', 'date_to'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
