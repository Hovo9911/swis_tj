<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateUserManualsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('user_manuals');
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('user_manuals', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['user_manuals','videos','technical_specifications','etc'])->nullable();
            $table->string('version')->nullable();
            $table->date('version_date')->nullable();
            $table->string('file')->nullable();
            $table->string('permission')->nullable();
            $table->string('url')->nullable();
            $table->enum('available_without_auth', [0,1])->default(0);
            $table->showStatus();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_manuals');
    }
}