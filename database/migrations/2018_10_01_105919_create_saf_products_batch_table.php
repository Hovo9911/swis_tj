<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;


class CreateSafProductsBatchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('saf_products_batch', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('saf_product_id');
            $table->string('batch_number');
            $table->date('production_date');
            $table->date('expiration_date');
            $table->integer('netto_weight');
            $table->string('quantity');
            $table->integer('measurement_unit')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saf_products_batch');
    }
}
