<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateAgencyLabIndicatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->create('agency_lab_indicators', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('agency_id');
            // related with ref table
            $table->integer('lab_hs_indicator_id');
            $table->double('price')->nullable();
            $table->integer('duration')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agency_lab_indicators');
    }
}
