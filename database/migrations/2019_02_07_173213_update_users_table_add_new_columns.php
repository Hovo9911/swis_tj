<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateUsersTableAddNewColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('users', function (Blueprint $table) {
            $table->string('patronymic_name')->after('last_name')->nullable();
            $table->string('english_first_name')->after('patronymic_name')->nullable();
            $table->string('english_last_name')->after('english_first_name')->nullable();
            $table->string('english_patronymic_name')->after('english_last_name')->nullable();
            $table->enum('genus',['M','F'])->after('english_patronymic_name')->nullable();
            $table->date('birth_date')->after('genus')->nullable();
            $table->string('community')->after('birth_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('users', function (Blueprint $table) {
            $table->dropColumn('patronymic_name');
            $table->dropColumn('english_first_name');
            $table->dropColumn('english_last_name');
            $table->dropColumn('english_patronymic_name');
            $table->dropColumn('genus');
            $table->dropColumn('birth_date');
            $table->dropColumn('community');
        });
    }
}
