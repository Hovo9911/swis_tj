<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class ChangeColumnAttributeIdInConstructorDocumentsRolesAttribute extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_documents_roles_attribute', function (Blueprint $table) {
            $table->dropColumn('attribute_id');
            $table->string('field_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_documents_roles_attribute', function (Blueprint $table) {
            $table->dropColumn('field_id');
            $table->string('attribute_id');
        });
    }
}
