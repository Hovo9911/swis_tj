<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\DB;

class AddSafNumberColumnToTranactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `transactions` MODIFY `payment_id` INTEGER UNSIGNED NULL;');
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('transactions', function (Blueprint $table) {
            $table->string('saf_number')->nullable()->after('payment_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE `transactions` MODIFY `payment_id` INTEGER UNSIGNED NOT NULL;');
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('transactions', function (Blueprint $table) {
            $table->dropColumn('saf_number');
        });
    }
}
