<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languages', function (Blueprint $table) {
            $table->smallInteger('id', true, true);
            $table->string('code', 3);
            $table->string('name');
            $table->string('icon')->default('');
            $table->smallInteger('sort_order')->default(0)->unsigned();
            $table->enum('show_status',['1','2','0'])->default('1');
        });

        $this->seedLanguages();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('languages');
    }

    private function seedLanguages()
    {
        $fileData = File::get(database_path('seeds/resources/helix_all_languages.json'));
        $languages = json_decode($fileData);

        DB::table('languages')->truncate();
        DB::table('languages')->insert([
            'name' => $languages->en->name,
            'code' => 'en'
        ]);

        unset($languages->en);

        $data = [];
        foreach ($languages as $code => $language) {
            $data[$code]['name'] = $language->name;
            $data[$code]['code'] = $code;
        }
        unset($languages);
        DB::table('languages')->insert($data);
    }
}
