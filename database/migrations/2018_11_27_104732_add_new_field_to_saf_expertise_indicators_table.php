<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddNewFieldToSafExpertiseIndicatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_expertise_indicators', function (Blueprint $table) {
            $table->string('actual_result')->nullable();
            $table->string('adequacy')->nullable();
            $table->dropColumn('laboratory_id');
        });

        $schemaBuilder->table('saf_expertise', function (Blueprint $table) {
            $table->unsignedInteger('laboratory_id')->default(0)->after('letter_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_expertise_indicators', function (Blueprint $table) {
            $table->dropColumn('actual_result');
            $table->dropColumn('adequacy');
            $table->unsignedInteger('laboratory_id');
        });

        $schemaBuilder->table('saf_expertise', function (Blueprint $table) {
            $table->dropColumn('laboratory_id');
        });
    }
}
