<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class ChangeSafExpertiseTableColumnAddNew extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_expertise', function (Blueprint $table) {
            $table->integer('saf_product_id')->after('sampler_code')->nullable();
            $table->dropColumn('saf_product_ids');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_expertise', function (Blueprint $table) {
            $table->dropColumn('saf_product_id');
            $table->text('saf_product_ids')->after('sampler_code')->nullable();
        });
    }
}
