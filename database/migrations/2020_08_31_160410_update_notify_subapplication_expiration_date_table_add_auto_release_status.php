<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateNotifySubapplicationExpirationDateTableAddAutoReleaseStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('notify_subapplication_expiration_date', function (Blueprint $table) {
            $table->integer('auto_released_status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('notify_subapplication_expiration_date', function (Blueprint $table) {
            $table->dropColumn('auto_released_status');
        });
    }
}
