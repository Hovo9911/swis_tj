<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;

class AddCodeColumnToconstructorNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_notifications', function (Blueprint $table) {
            $table->string('code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_notifications', function (Blueprint $table) {
            $table->dropColumn('code');
        });
    }
}
