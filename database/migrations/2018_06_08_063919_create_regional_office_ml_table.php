<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateRegionalOfficeMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('regional_office_ml', function (Blueprint $table) {
            $table->unsignedInteger('regional_office_id');
            $table->unsignedSmallInteger('lng_id');
            $table->string('name',255);
            $table->showStatus();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regional_office_ml');
    }
}
