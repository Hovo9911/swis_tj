<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateSafTableAddNewColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf', function (Blueprint $table) {
            $table->tinyInteger('need_reform')->default('0')->after('custom_data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf', function (Blueprint $table) {
            $table->dropColumn('need_reform');
        });
    }
}
