<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateLaboratoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('laboratory', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_tax_id')->default('0');
            $table->unsignedInteger('user_id');
            $table->string('tax_id',100);
            $table->string('laboratory_code',12)->nullable();
            $table->string('certificate_number',255)->nullable();
            $table->date('certification_period_validity')->nullable();
            $table->string('address',45)->nullable();
            $table->char('phone_number',12)->nullable();
            $table->string('email',320)->nullable();
            $table->string('website_url',2048)->nullable();
            $table->showStatus();
            $table->adminInfo();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laboratory');
    }
}
