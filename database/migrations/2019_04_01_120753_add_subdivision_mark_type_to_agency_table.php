<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddSubdivisionMarkTypeToAgencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('agency', function (Blueprint $table) {
            $table->enum('subdivision_mark_type', ['required', 'not_required','optional'])->after('logo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('agency', function (Blueprint $table) {
            $table->dropColumn('subdivision_mark_type');
        });
    }
}
