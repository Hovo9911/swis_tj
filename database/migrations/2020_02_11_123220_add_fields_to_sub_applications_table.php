<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use App\Models\DataModel\DataModel;
use Illuminate\Support\Facades\DB;

class AddFieldsToSubApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_sub_applications', function (Blueprint $table) {
            $table->string('permission_number')->nullable();
            $table->string('expiration_date')->nullable();
            $table->string('rejected_number')->nullable();
            $table->string('rejected_reason')->nullable();
            $table->string('permission_document')->nullable();
            $table->string('total_netto_weight')->nullable();
        });

        $documents = App\Models\ConstructorDocument\ConstructorDocument::where('show_status', '1')->get();

        $permissionNumber = DataModel::getMdmFieldIDByConstant(DataModel::PERMISSION_NUMBER_MDM_ID);
        $expirationDate = DataModel::getMdmFieldIDByConstant(DataModel::EXPIRATION_DATE_MDM_ID);
        $rejectedNumber = DataModel::getMdmFieldIDByConstant(DataModel::REJECTION_NUMBER_MDM_ID);
        $rejectedReason = DataModel::getMdmFieldIDByConstant(DataModel::REJECTION_REASON_MDM_ID);
        $permissionDocument = DataModel::getMdmFieldIDByConstant(DataModel::PERMISSION_DOCUMENT_MDM_ID);
        $totalNettoWeight = DataModel::getMdmFieldIDByConstant(DataModel::TOTAL_NETTO_WEIGHT_MDM_ID);

        foreach ($documents as $document) {
            $subApp = \App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications::select('id', 'custom_data')->where('document_code', $document->document_code)->get();

            if (count($subApp) > 0) {
                $dataSet = \App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm::where('document_id', $document->id)->where('mdm_field_id', $permissionNumber)->first();
                if (!is_null($dataSet)) {
                    \App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications::where('document_code', $document->document_code)->update(['permission_number' => DB::raw("cast(saf_sub_applications.custom_data->>'{$dataSet->field_id}' as varchar)")]);
                }

                $dataSet = \App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm::where('document_id', $document->id)->where('mdm_field_id', $expirationDate)->first();
                if (!is_null($dataSet)) {
                    \App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications::where('document_code', $document->document_code)->update(['expiration_date' => DB::raw("cast(saf_sub_applications.custom_data->>'{$dataSet->field_id}' as varchar)")]);
                }

                $dataSet = \App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm::where('document_id', $document->id)->where('mdm_field_id', $rejectedNumber)->first();
                if (!is_null($dataSet)) {
                    \App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications::where('document_code', $document->document_code)->update(['rejected_number' => DB::raw("cast(saf_sub_applications.custom_data->>'{$dataSet->field_id}' as varchar)")]);
                }

                $dataSet = \App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm::where('document_id', $document->id)->where('mdm_field_id', $rejectedReason)->first();
                if (!is_null($dataSet)) {
                    \App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications::where('document_code', $document->document_code)->update(['rejected_reason' => DB::raw("cast(saf_sub_applications.custom_data->>'{$dataSet->field_id}' as varchar)")]);
                }

                $dataSet = \App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm::where('document_id', $document->id)->where('mdm_field_id', $permissionDocument)->first();
                if (!is_null($dataSet)) {
                    \App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications::where('document_code', $document->document_code)->update(['permission_document' => DB::raw("cast(saf_sub_applications.custom_data->>'{$dataSet->field_id}' as varchar)")]);
                }

                $dataSet = \App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm::where('document_id', $document->id)->where('mdm_field_id', $totalNettoWeight)->first();
                if (!is_null($dataSet)) {
                    \App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications::where('document_code', $document->document_code)->update(['total_netto_weight' => DB::raw("cast(saf_sub_applications.custom_data->>'{$dataSet->field_id}' as varchar)")]);
                }

            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_sub_applications', function (Blueprint $table) {
            $table->dropColumn(['permission_number', 'expiration_date', 'rejected_number', 'rejected_reason', 'permission_document', 'total_netto_weight']);
        });
    }
}