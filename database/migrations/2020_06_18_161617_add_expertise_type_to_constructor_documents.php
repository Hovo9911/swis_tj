<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddExpertiseTypeToConstructorDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_documents', function (Blueprint $table) {
            $table->json('expertise_type')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_documents', function (Blueprint $table) {
            $table->dropColumn('expertise_type');
        });
    }
}
