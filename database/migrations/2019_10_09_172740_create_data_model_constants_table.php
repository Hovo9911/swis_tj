<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;

class CreateDataModelConstantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->create('data_model_constants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('constant');
            $table->unsignedInteger('mdm_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('data_model_constants', function (Blueprint $table) {
            $table->dropColumn(['id','constant','mdm_id']);
        });
    }
}
