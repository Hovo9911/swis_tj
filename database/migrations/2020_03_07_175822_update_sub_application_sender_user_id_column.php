<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateSubApplicationSenderUserIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_sub_applications', function (Blueprint $table) {
            $table->integer('sender_user_id')->nullable();
        });

        $safSubApplications = \App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications::select('id','user_id')->get();
        foreach ($safSubApplications as $safSubApplication){
            \App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications::where('id',$safSubApplication->id)->update(['sender_user_id'=> $safSubApplication->user_id]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_sub_applications', function (Blueprint $table) {
            $table->dropColumn(['sender_user_id']);
        });
    }
}
