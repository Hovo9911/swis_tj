<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;

class CreateSafExpertisingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->create('saf_expertise', function (Blueprint $table) {
            $table->increments('id');
            $table->string('saf_number');
            $table->string('letter_code');
            $table->string('sample_code');
            $table->string('sampling_method_code');
            $table->double('sampling_weight', 8, 2);
            $table->string('sampler_code');
            $table->text('saf_product_id');
            $table->text('saf_product_batches_ids')->nullable();
            $table->string('expertising_method_code')->nullable();
            $table->string('product_group_code')->nullable();
            $table->string('expertising_indicator_code')->nullable();
            $table->string('product_sub_group_code')->nullable();
            $table->tinyInteger('send_to_lab')->default(0);
            $table->integer('total_price')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saf_expertise');
    }
}
