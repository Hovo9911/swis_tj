<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateBrokerMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('broker_ml', function (Blueprint $table) {
            $table->unsignedInteger('broker_id');
            $table->unsignedSmallInteger('lng_id');
            $table->string('name');
            $table->string('legal_entity_name')->nullable();
            $table->text('description')->nullable();
            $table->showStatus();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('broker_ml');
    }
}
