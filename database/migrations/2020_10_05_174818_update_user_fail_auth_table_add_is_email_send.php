<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateUserFailAuthTableAddIsEmailSend extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('user_fail_auth', function (Blueprint $table) {
            $table->tinyInteger('is_email_send')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('user_fail_auth', function (Blueprint $table) {
            $table->dropColumn('is_email_send');
        });
    }
}
