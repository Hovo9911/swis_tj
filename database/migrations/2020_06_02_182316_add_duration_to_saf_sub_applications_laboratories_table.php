<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddDurationToSafSubApplicationsLaboratoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_sub_applications_laboratory', function (Blueprint $table) {
            $table->string('duration')->nullable();
        });

        $schemaBuilder->table('saf_sub_application_products', function (Blueprint $table) {
            $table->string('correspond')->nullable();
            $table->string('total')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_sub_applications_laboratory', function (Blueprint $table) {
            $table->dropColumn('duration');
        });

        $schemaBuilder->table('saf_sub_application_products', function (Blueprint $table) {
            $table->dropColumn('correspond');
            $table->dropColumn('total');
        });
    }
}
