<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddCreatorCompanyTaxIdToSafObligations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_obligations', function (Blueprint $table) {
            $table->string('creator_company_tax_id')->after('company_tax_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_obligations', function (Blueprint $table) {
            $table->dropColumn('creator_company_tax_id');
        });
    }
}
