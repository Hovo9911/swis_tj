<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateLaboratoriesTableAddHeadNameColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('laboratory', function (Blueprint $table) {
          $table->string('head_name')->after('certification_period_validity')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('laboratory', function (Blueprint $table) {
            $table->dropColumn('head_name');
        });
    }
}
