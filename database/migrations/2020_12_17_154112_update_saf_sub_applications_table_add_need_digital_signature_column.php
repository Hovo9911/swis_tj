<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateSafSubApplicationsTableAddNeedDigitalSignatureColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('saf_sub_applications', function (Blueprint $table) {
            $table->smallInteger('need_digital_signature')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_sub_applications', function (Blueprint $table) {
            $table->dropColumn('need_digital_signature');
        });
    }
}
