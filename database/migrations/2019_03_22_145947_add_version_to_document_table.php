<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddVersionToDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('document', function (Blueprint $table) {
            $table->unsignedInteger('version')->after('document_form')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('document', function (Blueprint $table) {
            $table->dropColumn('version');
        });
    }
}
