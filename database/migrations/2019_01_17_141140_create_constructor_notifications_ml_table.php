<?php

use App\Models\BaseModel;
use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateConstructorNotificationsMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('constructor_notifications_ml', function (Blueprint $table) {
            $table->unsignedInteger('constructor_notification_id');
            $table->unsignedSmallInteger('lng_id');
            $table->integer('sub_application_id')->default(0);
            $table->text('email_notification')->nullable();
            $table->text('in_app_notification')->nullable();
            $table->text('sms_notification')->nullable();
            $table->showStatus();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('constructor_notifications_ml');
    }
}