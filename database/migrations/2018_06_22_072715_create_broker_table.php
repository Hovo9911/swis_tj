<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateBrokerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('broker', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tax_id', 100);
            $table->string('certification_number')->nullable();
            $table->date('certification_period_validity')->nullable();
            $table->string('address')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('email')->nullable();
            $table->showStatus();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('broker');
    }
}
