<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConstructorDocumentRolesAttributeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('constructor_documents_roles_attribute', function (Blueprint $table) {
            $table->integer('constructor_documents_roles_id')->unsigned();
            $table->enum('type', ['saf', 'mdm'])->default('saf');
            $table->integer('attribute_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('constructor_documents_roles_attribute');
    }
}
