<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;

class CreateTransportApplicationStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->create('transport_application_states', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('transport_application_id');
            $table->unsignedInteger('user_id');
            $table->string('company_tax_id')->default('0');
            $table->enum('from_status', \App\Models\TransportApplication\TransportApplication::TRANSPORT_APPLICATION_STATUSES)->nullable();
            $table->enum('to_status', \App\Models\TransportApplication\TransportApplication::TRANSPORT_APPLICATION_STATUSES);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transport_application_states');
    }
}
