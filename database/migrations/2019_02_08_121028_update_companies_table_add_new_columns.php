<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateCompaniesTableAddNewColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('company', function (Blueprint $table) {
            $table->string('name_en')->after('name')->nullable();
            $table->string('name_ru')->after('name_en')->nullable();
            $table->string('company_type')->after('name_ru')->nullable();
            $table->string('city_town')->after('company_type')->nullable();
            $table->string('executive_ssn')->after('city_town')->nullable();
            $table->string('street1')->after('executive_ssn')->nullable();
            $table->string('house')->after('street1')->nullable();
            $table->string('apt')->after('house')->nullable();
            $table->string('street2')->after('apt')->nullable();
            $table->string('postcode')->after('street2')->nullable();
            $table->string('community')->after('postcode')->nullable();
            $table->string('phone_number')->after('community')->nullable();
            $table->string('email')->after('phone_number')->nullable();
            $table->string('address')->after('email')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('company', function (Blueprint $table) {
            $table->dropColumn('name_en');
            $table->dropColumn('name_ru');
            $table->dropColumn('company_type');
            $table->dropColumn('city_town');
            $table->dropColumn('executive_ssn');
            $table->dropColumn('street1');
            $table->dropColumn('house');
            $table->dropColumn('apt');
            $table->dropColumn('street2');
            $table->dropColumn('postcode');
            $table->dropColumn('community');
            $table->dropColumn('phone_number');
            $table->dropColumn('email');
            $table->dropColumn('address');
        });
    }
}
