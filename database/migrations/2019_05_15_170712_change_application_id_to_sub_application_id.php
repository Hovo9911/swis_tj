<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeApplicationIdToSubApplicationId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `saf_expertise` CHANGE COLUMN `application_id` `sub_application_id` INT(11) UNSIGNED NOT NULL ;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `saf_expertise` CHANGE COLUMN `sub_application_id` `application_id` DOUBLE(40.0) NOT NULL ;");
    }
}
