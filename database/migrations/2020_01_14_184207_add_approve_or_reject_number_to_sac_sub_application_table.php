<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\DB;

class AddApproveOrRejectNumberToSacSubApplicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_sub_applications', function (Blueprint $table) {
            $table->string('approve_reject_number')->nullable();
        });

        DB::transaction(function () {

            $subApplications = \App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications::select('saf_sub_applications.id','constructor_documents.id as document_id', 'saf_sub_applications.document_code', 'saf_sub_applications.custom_data')
                ->join('constructor_documents', 'constructor_documents.document_code', '=', 'saf_sub_applications.document_code')
                ->orderBy('saf_sub_applications.id', 'desc')
                ->get();

            foreach ($subApplications as &$application) {
                $rejected = \App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm::select('field_id')->where('document_id', $application->document_id)->where('mdm_field_id', 358)->first();
                $approved = \App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm::select('field_id')->where('document_id', $application->document_id)->where('mdm_field_id', 365)->first();

                if ((!is_null($approved) || !is_null($rejected)) && !is_null($application->custom_data)) {
                    $customData = $application->custom_data;

                    if (array_key_exists($approved->field_id, $customData)) {
                        $application->approve_reject_number = $customData[$approved->field_id];
                    } elseif (array_key_exists($rejected->field_id, $customData)) {
                        $application->approve_reject_number = $customData[$rejected->field_id];
                    } else {
                        $application->approve_reject_number = null;
                    }
                    $application->save();
                }
            }

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_sub_applications', function (Blueprint $table) {
            $table->dropColumn('approve_reject_number');
        });
    }
}
