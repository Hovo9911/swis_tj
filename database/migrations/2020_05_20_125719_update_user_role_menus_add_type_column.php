<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;


class UpdateUserRoleMenusAddTypeColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('user_role_menus', function (Blueprint $table) {
            $table->enum('type',['user_role','agency','broker','sw_admin_roles'])->default('user_role');
            $table->dropColumn(['roles']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('user_role_menus', function (Blueprint $table) {
            $table->dropColumn(['type']);
            $table->string('roles')->nullable();
        });
    }
}
