<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddPayerCompanyTaxIdToSafObligations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_obligations', function (Blueprint $table) {
            $table->string('payer_company_tax_id')->after('payer_user_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_obligations', function (Blueprint $table) {
            $table->dropColumn('payer_company_tax_id');
        });
    }
}
