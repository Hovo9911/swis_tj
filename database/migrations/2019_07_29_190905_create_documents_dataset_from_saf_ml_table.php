<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsDatasetFromSafMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents_dataset_from_saf_ml', function (Blueprint $table) {
            $table->integer('documents_dataset_from_saf_Id')->unsigned();
            $table->integer('lng_id');
            $table->string('label')->nullable()->default(null);
            $table->string('tooltip')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents_dataset_from_saf_ml');
    }
}