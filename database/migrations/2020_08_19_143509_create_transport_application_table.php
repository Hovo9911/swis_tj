<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;

class CreateTransportApplicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->create('transport_application', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('company_tax_id')->default('0');
            $table->string('application_number')->unique();
            $table->string('driver_name', 70);
            $table->unsignedInteger('ref_transport_carriers_id');
            $table->string('vehicle_brand', 35);
            $table->string('vehicle_reg_number', 25);
            $table->string('trailer_reg_number', 25)->nullable();
            $table->unsignedInteger('ref_origin_country');
            $table->unsignedInteger('ref_destination_country');
            $table->unsignedInteger('ref_transport_permit_type');
            $table->string('vehicle_carrying_capacity', 18)->nullable();
            $table->string('trailer_carrying_capacity', 18)->nullable();
            $table->string('vehicle_empty_capacity', 18)->nullable();
            $table->string('trailer_empty_capacity', 18)->nullable();
            $table->string('place_of_unloading', 256)->nullable();
            $table->decimal('entry_goods_weight', 16, 6);
            $table->decimal('departure_goods_weight', 16, 6)->nullable();
            $table->string('nature_of_goods', 33)->nullable();
            $table->string('reject_reason', 512)->nullable();
            $table->string('permission_number', 70)->nullable();
            $table->unsignedInteger('ref_issue_country')->nullable();
            $table->date('permit_validity_period_from')->nullable();
            $table->date('permit_validity_period_to')->nullable();
            $table->tinyInteger('period_validity')->nullable();
            $table->unsignedInteger('ref_border_crossing')->nullable();
            $table->date('arrival_date')->nullable();
            $table->date('departure_date')->nullable();
            $table->string('transport_application_receipt_file')->nullable();

            $table->enum('current_status', \App\Models\TransportApplication\TransportApplication::TRANSPORT_APPLICATION_STATUSES)->default(\App\Models\TransportApplication\TransportApplication::TRANSPORT_APPLICATION_STATUS_REGISTERED);
            $table->adminInfo();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transport_application');
    }
}
