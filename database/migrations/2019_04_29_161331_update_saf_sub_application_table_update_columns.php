<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;

class UpdateSafSubApplicationTableUpdateColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_sub_applications', function (Blueprint $table) {
            $table->integer('reference_classificator_id')->after('sub_application_number')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_sub_applications', function (Blueprint $table) {
            $table->dropColumn('reference_classificator_id');
        });
    }
}
