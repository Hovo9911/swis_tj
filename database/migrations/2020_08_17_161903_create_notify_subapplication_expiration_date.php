<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateNotifySubapplicationExpirationDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('notify_subapplication_expiration_date', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('subapplication_id');
            $table->dateTime('date_of_notification');
            $table->integer('is_notified')->default('0');
            $table->adminInfo();
            $table->showStatus();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notify_subapplication_expiration_date');
    }
}
