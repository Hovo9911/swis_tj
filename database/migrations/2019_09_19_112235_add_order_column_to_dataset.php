<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddOrderColumnToDataset extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('documents_dataset_from_mdm', function (Blueprint $table) {
            $table->integer('order')->default(0);
        });

        $schemaBuilder->table('documents_dataset_from_saf', function (Blueprint $table) {
            $table->string('order')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('documents_dataset_from_mdm', function (Blueprint $table) {
            $table->dropColumn('order');
        });

        $schemaBuilder->table('documents_dataset_from_saf', function (Blueprint $table) {
            $table->dropColumn('order');
        });
    }
}
