<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateAgencyMlTableAddAddressField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('agency_ml', function (Blueprint $table) {
            $table->string('address')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('agency_ml', function (Blueprint $table) {
            $table->dropColumn('address');
        });
    }
}
