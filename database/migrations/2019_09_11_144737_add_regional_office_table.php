<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddRegionalOfficeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('regional_office', function (\App\Migration\Blueprint $table) {
            $table->string('region')->nullable();
            $table->string('city')->nullable();
            $table->string('area')->nullable();
            $table->string('street')->nullable();
            $table->string('fax')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('regional_office', function (Blueprint $table) {
            $table->dropColumn('region');
            $table->dropColumn('city');
            $table->dropColumn('area');
            $table->dropColumn('street');
            $table->dropColumn('fax');
        });
    }
}
