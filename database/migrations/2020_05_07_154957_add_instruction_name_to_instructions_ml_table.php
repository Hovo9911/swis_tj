<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddInstructionNameToInstructionsMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('saf_sub_applications', function (Blueprint $table) {
            $table->json('tmp_data')->nullable();
        });

        $schemaBuilder->table('instructions', function (Blueprint $table) {
            $table->string('type_code')->nullable();
        });

        $schemaBuilder->table('instructions_ml', function (Blueprint $table) {
            $table->string('name')->nullable();
        });

        $schemaBuilder->table('instructions_to_feedback', function (Blueprint $table) {
            $table->string('code')->nullable();
        });

        $schemaBuilder->table('saf_sub_application_instruction_feedback', function (Blueprint $table) {
            $table->string('feedback_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('saf_sub_applications', function (Blueprint $table) {
            $table->dropColumn('tmp_data');
        });

        $schemaBuilder->table('instructions', function (Blueprint $table) {
            $table->dropColumn('type_code');
        });

        $schemaBuilder->table('instructions_ml', function (Blueprint $table) {
            $table->dropColumn('name');
        });

        $schemaBuilder->table('instructions_to_feedback', function (Blueprint $table) {
            $table->dropColumn('code');
        });

        $schemaBuilder->table('saf_sub_application_instruction_feedback', function (Blueprint $table) {
            $table->dropColumn('feedback_code');
        });
    }
}
