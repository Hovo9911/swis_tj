<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use App\Models\DataModel\DataModel;

class AddFieldsToSafSubApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_sub_applications', function (Blueprint $table) {
            $table->date('request_date')->nullable();
            $table->date('rejection_date')->nullable();
            $table->date('registration_date')->nullable();
            $table->date('permission_date')->nullable();
            $table->string('registration_number')->nullable();
        });

        $documents = App\Models\ConstructorDocument\ConstructorDocument::where('show_status', '1')->get();

        $requestDate = DataModel::getMdmFieldIDByConstant(DataModel::REQUEST_DATE_MDM_ID);
        $rejectionDate = DataModel::getMdmFieldIDByConstant(DataModel::REJECTION_DATE_MDM_ID);
        $registrationDate = DataModel::getMdmFieldIDByConstant(DataModel::REGISTRATION_DATE_MDM_ID);
        $permissionDate = DataModel::getMdmFieldIDByConstant(DataModel::PERMISSION_DATE_MDM_ID);
        $registrationNumber = DataModel::getMdmFieldIDByConstant(DataModel::REGISTRATION_NUMBER_MDM_ID);

        foreach ($documents as $document) {
            $subApp = \App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications::select('id', 'custom_data')->where('document_code', $document->document_code)->get();

            if (count($subApp) > 0) {
                $dataSet = \App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm::where('document_id', $document->id)->where('mdm_field_id', $requestDate)->first();
                if (!is_null($dataSet)) {
                    \App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications::where('document_code', $document->document_code)->update(['request_date' => DB::raw("cast(saf_sub_applications.custom_data->>'{$dataSet->field_id}' as date)")]);
                }

                $dataSet = \App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm::where('document_id', $document->id)->where('mdm_field_id', $rejectionDate)->first();
                if (!is_null($dataSet)) {
                    \App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications::where('document_code', $document->document_code)->update(['rejection_date' => DB::raw("cast(saf_sub_applications.custom_data->>'{$dataSet->field_id}' as date)")]);
                }

                $dataSet = \App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm::where('document_id', $document->id)->where('mdm_field_id', $registrationDate)->first();
                if (!is_null($dataSet)) {
                    \App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications::where('document_code', $document->document_code)->update(['registration_date' => DB::raw("cast(saf_sub_applications.custom_data->>'{$dataSet->field_id}' as date)")]);
                }

                $dataSet = \App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm::where('document_id', $document->id)->where('mdm_field_id', $permissionDate)->first();
                if (!is_null($dataSet)) {
                    \App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications::where('document_code', $document->document_code)->update(['permission_date' => DB::raw("cast(saf_sub_applications.custom_data->>'{$dataSet->field_id}' as date)")]);
                }

                $dataSet = \App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm::where('document_id', $document->id)->where('mdm_field_id', $registrationNumber)->first();
                if (!is_null($dataSet)) {
                    \App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications::where('document_code', $document->document_code)->update(['registration_number' => DB::raw("cast(saf_sub_applications.custom_data->>'{$dataSet->field_id}' as varchar)")]);
                }

            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_sub_applications', function (Blueprint $table) {
            $table->dropColumn(['request_date', 'rejection_date', 'registration_date', 'permission_date', 'registration_number']);
        });
    }
}
