<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;

class CreateInstructionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->create('instructions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_tax_id');
            $table->integer('document_id');
            $table->integer('type');
            $table->string('name');
            $table->string('code')->index();
            $table->dateTime('active_date_from');
            $table->dateTime('active_date_to');
            $table->showStatus();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instructions');
    }
}
