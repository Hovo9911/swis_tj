<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddTypeToOnlineTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('online_transactions', function (Blueprint $table) {
            $table->integer('user_id')->after('id');
            $table->string('company_tax_id')->after('user_id');
            $table->enum('type', ['alif'])->after('company_tax_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('online_transactions', function (Blueprint $table) {
            $table->dropColumn(['user_id', 'company_tax_id', 'type']);
        });
    }
}
