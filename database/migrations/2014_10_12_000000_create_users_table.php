<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('ssn',32);
            $table->string('email')->nullable();
            $table->string('username')->nullable();
            $table->string('passport')->nullable();
            $table->char('phone_number',12)->nullable();
            $table->string('address',300)->nullable();
            $table->string('registration_address',300)->nullable();
            $table->string('city_village',100)->nullable();
            $table->string('post_address',100)->nullable();
            $table->string('login',130)->nullable();
            $table->string('password',130)->nullable();
            $table->text('allowed_from_ips')->nullable();
            $table->dateTime('registered_at');
            $table->dateTime('lastlogin_at')->nullable();
            $table->string('lastlogin_ip',15)->nullable();
            $table->enum('is_email_verified',['0','1'])->default('0');
            $table->enum('is_phone_verified',['0','1'])->default('0');
            $table->integer('lng_id');
            $table->rememberToken();
            $table->enum('show_status',['1','2','0'])->default('1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
