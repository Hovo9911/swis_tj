<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddNotificationsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('users', function (Blueprint $table) {
            $table->tinyInteger('email_notification')->default(1)->after('is_phone_verified');
            $table->tinyInteger('sms_notification')->default(1)->after('email_notification');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('users', function (Blueprint $table) {
            $table->dropColumn(['email_notification','sms_notification']);
        });
    }
}

