<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateSafDocumentProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_document_products', function (Blueprint $table) {
            $table->dropColumn('id');
            $table->dropColumn('saf_id');
            $table->dropColumn('products');
            $table->dropColumn('state_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
