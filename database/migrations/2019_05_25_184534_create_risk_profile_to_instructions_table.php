<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;

class CreateRiskProfileToInstructionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->create('risk_profile_to_instructions', function (Blueprint $table) {
            $table->integer('risk_profile_id');
            $table->integer('instruction_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('risk_profile_to_instructions');
    }
}
