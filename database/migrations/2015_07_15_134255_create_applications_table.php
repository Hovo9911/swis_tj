<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default('');
        });

        $this->seedTable();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('applications');
    }

    protected function seedTable()
    {
        DB::table('applications')->insert(
            [
                [
                    'name'        => 'adm',
                ],
                [
                    'name'        => 'www',
                ],
                [
                    'name'        => 'api',
                ]
            ]
        );
    }
}
