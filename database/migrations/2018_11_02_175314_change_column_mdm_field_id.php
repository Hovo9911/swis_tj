<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class ChangeColumnMdmFieldId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_list_to_data_set_from_mdm', function (Blueprint $table) {
            $table->string('constructor_data_set_field_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_list_to_data_set_from_mdm', function (Blueprint $table) {
            $table->dropColumn('constructor_data_set_field_id');
        });
    }
}
