<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateTableDocumentAddNewColumnCreatedFromModule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('document', function (Blueprint $table) {
            $table->enum('created_from_module',['document','folder','saf','application'])->after('notes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('document', function (Blueprint $table) {
            $table->dropColumn('created_from_module');
        });
    }
}
