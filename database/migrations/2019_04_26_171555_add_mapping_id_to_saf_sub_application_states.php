<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddMappingIdToSafSubApplicationStates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_sub_application_states', function (Blueprint $table) {
            $table->string('mapping_id')->after('saf_subapplication_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_sub_application_states', function (Blueprint $table) {
            $table->dropColumn('mapping_id');
        });
    }
}
