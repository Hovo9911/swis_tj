<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateFolderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('folder', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_tax_id')->default('0');
            $table->unsignedInteger('user_id');
            $table->string('folder_access_password',16);
            $table->string('folder_name');
            $table->text('folder_description');
            $table->enum('holder_type',['tax_id','id_card','f_citizen'])->default('tax_id');
            $table->string('holder_type_value');
            $table->string('holder_name',250);
            $table->string('holder_country');
            $table->string('holder_community')->nullable();
            $table->string('holder_address',250);
            $table->string('holder_phone_number',50);
            $table->string('holder_email');
            $table->enum('creator_type',['tax_id','id_card','f_citizen'])->default('tax_id');
            $table->string('creator_type_value');
            $table->string('creator_name',250);
            $table->string('creator_country');
            $table->string('creator_community');
            $table->string('creator_address',250);
            $table->string('creator_phone_number',50)->nullable();
            $table->string('creator_email');
            $table->text('notes')->nullable();

            $table->adminInfo();
            $table->showStatus();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('folder');
    }
}
