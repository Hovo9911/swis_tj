<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;


class CreateTableSafExaminations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->create('saf_examinations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('saf_number');
            $table->unsignedInteger('saf_subapplication_id');
            $table->unsignedInteger('document_id');
            $table->unsignedInteger('saf_product_id');
            $table->unsignedInteger('type_of_good')->nullable();
            $table->unsignedInteger('type_of_metal')->nullable();
            $table->double('weight', 10, 2)->nullable();
            $table->double('quantity', 10, 2)->nullable();
            $table->integer('measurement_unit')->nullable();
            $table->string('type_of_stone')->nullable();
            $table->unsignedInteger('piece')->nullable();
            $table->double('carat', 10, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saf_examinations');
    }
}
