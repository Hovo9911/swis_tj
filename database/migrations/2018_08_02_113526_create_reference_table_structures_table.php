<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateReferenceTableStructuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('reference_table_structures', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('reference_table_id');
            $table->string('field_name',255);
            $table->enum('type',\App\Models\ReferenceTable\ReferenceTableStructure::COLUMN_TYPES)->default('text');
            $table->text('parameters');
            $table->enum('required',['0','1'])->default('0');
            $table->enum('searchable',['0','1'])->default('0');
            $table->enum('search_filter',['0','1'])->default('0');
            $table->enum('search_result',['0','1'])->default('0');
            $table->unsignedInteger('sort_order');
            $table->enum('sortable',['0','1'])->default('0');
            $table->enum('has_ml',['0','1'])->default('0');
            $table->showStatus();
            $table->adminInfo();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reference_table_structures');
    }
}
