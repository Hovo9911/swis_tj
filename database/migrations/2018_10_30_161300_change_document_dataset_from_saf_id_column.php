<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class ChangeDocumentDatasetFromSafIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_list_to_data_set_from_saf', function (Blueprint $table) {
            $table->dropColumn('document_dataset_from_saf_id');
            $table->string('saf_field_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_list_to_data_set_from_saf', function (Blueprint $table) {
            $table->dropColumn('saf_field_id');
            $table->string('document_dataset_from_saf_id');
        });
    }
}
