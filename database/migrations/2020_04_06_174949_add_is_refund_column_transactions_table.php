<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use App\Models\Menu\Menu;
use App\Models\Role\Role;
use App\Models\RoleMenus\RoleMenus;

class AddIsRefundColumnTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('transactions', function (Blueprint $table) {
            $table->boolean('is_refund')->default(false);
        });

        $schemaBuilder->table('constructor_states', function (Blueprint $table) {
            $table->boolean('is_countable')->default(true);
        });

        $paymentMenu = [
            'name' => 'balance_operations',
            'title' => 'swis.balance_operations.title',
            'icon' => '<i class="fa fa-credit-card"></i>',
            'href' => 'balance-operations',
            'parent_id' => null,
            'sort_order' => 97,
            'show_status' => '1',
            'is_module' => '1'
        ];

        $menu = new Menu($paymentMenu);
        $menu->save();

        $rolesMenu = [
            [
                'role_id' => Role::SW_ADMIN,
                'menu_id' => $menu->id
            ]
        ];
        RoleMenus::insert($rolesMenu);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('transactions', function (Blueprint $table) {
            $table->dropColumn('is_refund');
        });

        $schemaBuilder->table('constructor_states', function (Blueprint $table) {
            $table->dropColumn('is_countable');
        });
    }
}
