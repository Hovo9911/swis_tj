<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateUserRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('user_roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_tax_id')->default('0');
            $table->unsignedInteger('menu_id')->nullable();
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('role_id')->default('0');
            $table->unsignedInteger('role_group_id')->nullable();
            $table->unsignedInteger('company_id')->nullable();
            $table->unsignedInteger('broker_id')->nullable();
            $table->unsignedInteger('laboratory_id')->nullable();
            $table->integer('attribute_id')->nullable();
            $table->integer('attribute_value')->nullable();
            $table->date('available_at')->nullable();
            $table->date('available_to')->nullable();
            $table->enum('show_only_self',['0','1'])->default('0');
            $table->enum('role_status',['1','2','0'])->default('0');
            $table->enum('show_status',['1','2','0'])->default('1');
            $table->enum('is_local_admin',['0','1'])->default('1');
            $table->unsignedInteger('authorized_by_id')->nullable();
            $table->enum('origin',['natural_person','entity'])->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_roles');
    }
}
