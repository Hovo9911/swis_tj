<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateRegionalOfficeMlTableAddShortNameColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('regional_office_ml', function (Blueprint $table) {
            $table->string('short_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('regional_office_ml', function (Blueprint $table) {
            $table->dropColumn('short_name');
        });
    }
}
