<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddFreeSumObligationTypeToSafObligations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_obligations', function (Blueprint $table) {
            $table->string('free_sum_budget_line_code')->after('obligation_code')->nullable();
            $table->tinyInteger('is_free_sum')->default(false)->after('free_sum_budget_line_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_obligations', function (Blueprint $table) {
            $table->dropColumn(['free_sum_budget_line_code', 'is_free_sum']);
        });
    }
}
