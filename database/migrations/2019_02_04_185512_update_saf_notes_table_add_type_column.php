<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateSafNotesTableAddTypeColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_notes', function (Blueprint $table) {
            $table->enum('type', \App\Models\SingleApplicationNotes\SingleApplicationNotes::NOTE_TYPES)->after('to_saf_holder');
            $table->enum('send_from_module', \App\Models\SingleApplicationNotes\SingleApplicationNotes::NOTE_SEND_MODULES)->after('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_notes', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('send_from_module');
        });
    }
}
