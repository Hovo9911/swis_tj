<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddApplicationIdToSafExpertise extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_expertise', function (Blueprint $table) {
            $table->double('application_id')->nullable()->after('saf_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_expertise', function (Blueprint $table) {
            $table->dropColumn(['application_id']);
        });
    }
}
