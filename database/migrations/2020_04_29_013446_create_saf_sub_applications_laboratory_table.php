<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSafSubApplicationsLaboratoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saf_sub_applications_laboratory', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sub_application_id');
            $table->integer('saf_product_id');
            $table->integer('indicator_id');
            $table->string('found_size');
            $table->integer('found_or_not')->default(2);
            $table->integer('correspond_or_not')->default(2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saf_sub_applications_laboratory');
    }
}
