<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateReferenceTableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('reference_table', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('parent_id')->default('0');
            $table->unsignedInteger('user_id');
            $table->string('table_name')->nullable();
            $table->enum('manually_edit',['0','1'])->default('0');
            $table->text('import_format')->nullable();
            $table->string('api_url')->nullable();
            $table->date('available_at')->nullable();
            $table->date('available_to')->nullable();
            $table->showStatus();
            $table->adminInfo();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reference_table');
    }
}
