<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddListToConstructorMappingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_mapping', function (Blueprint $table) {
            $table->integer('non_visible_list')->after('end_state')->nullable();
            $table->integer('editable_list')->after('non_visible_list')->nullable();
            $table->integer('mandatory_list')->after('editable_list')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_mapping', function (Blueprint $table) {
            $table->dropColumn(['non_visible_list', 'editable_list', 'mandatory_list']);
        });
    }
}
