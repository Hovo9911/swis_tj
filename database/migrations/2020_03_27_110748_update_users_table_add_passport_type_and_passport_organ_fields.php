<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateUsersTableAddPassportTypeAndPassportOrganFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('users', function (Blueprint $table) {
            $table->string('passport_type')->nullable();
            $table->string('passport_issued_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('users', function (Blueprint $table) {
            $table->dropColumn(['passport_type', 'passport_issued_by']);
        });
    }
}
