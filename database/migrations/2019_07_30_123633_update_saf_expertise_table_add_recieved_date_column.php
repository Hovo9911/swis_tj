<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateSafExpertiseTableAddRecievedDateColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_expertise', function (Blueprint $table) {
            $table->date('received_date')->after('total_price')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_expertise', function (Blueprint $table) {
            $table->dropColumn('received_date');
        });
    }
}
