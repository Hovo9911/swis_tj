<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateMenuTableAddShowStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('menu', function (Blueprint $table) {
            $table->enum('show_status', [
                \App\Models\BaseModel::STATUS_ACTIVE,
                \App\Models\BaseModel::STATUS_INACTIVE,
                \App\Models\BaseModel::STATUS_DELETED,
            ])->after('sort_order')->nullable()->default(\App\Models\BaseModel::STATUS_ACTIVE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('menu', function (Blueprint $table) {
            $table->dropColumn('show_status');
        });
    }
}
