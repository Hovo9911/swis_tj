<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferenceTableAgenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reference_table_agencies', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('reference_table_id');
            $table->unsignedInteger('agency_id');
            $table->enum('access',['edit','read'])->default('edit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reference_table_agencies');
    }
}
