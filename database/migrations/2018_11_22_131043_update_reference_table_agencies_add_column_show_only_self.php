<?php


use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateReferenceTableAgenciesAddColumnShowOnlySelf extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('reference_table_agencies', function (Blueprint $table) {
            $table->string('show_only_self')->after('access')->enum('0','1')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('reference_table_agencies', function (Blueprint $table) {
            $table->dropColumn('show_only_self');
        });
    }
}
