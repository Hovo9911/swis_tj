<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;

class UpdateUserRolesTableAddRoleTypeColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('user_roles', function (Blueprint $table) {
            $table->enum('role_type', \App\Models\Role\Role::ROLE_TYPES_ALL)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('user_roles', function (Blueprint $table) {
            $table->dropColumn('role_type');
        });
    }
}
