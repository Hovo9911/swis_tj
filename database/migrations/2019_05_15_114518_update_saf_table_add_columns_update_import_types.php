<?php

use App\Migration\Migration;
use Illuminate\Support\Facades\DB;

class UpdateSafTableAddColumnsUpdateImportTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        DB::statement('ALTER TABLE `saf` MODIFY `importer_type` tinyint UNSIGNED DEFAULT 0;');
        DB::statement('ALTER TABLE `saf` MODIFY `importer_value` VARCHAR(200) DEFAULT 0;');

        DB::statement('ALTER TABLE `saf` MODIFY `exporter_type` tinyint UNSIGNED DEFAULT 0;');
        DB::statement('ALTER TABLE `saf` MODIFY `exporter_value` VARCHAR(200) DEFAULT 0;');

        $schemaBuilder->table('saf', function (\App\Migration\Blueprint $table) {
            $table->tinyInteger('applicant_type')->after('exporter_value');
            $table->string('applicant_value')->default('0')->after('applicant_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
