<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateConstructorMappingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('constructor_mapping', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('document_id');
            $table->integer('start_state');
            $table->integer('role');
            $table->integer('action');
            $table->integer('end_state');
            $table->showStatus();
            $table->adminInfo();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('constructor_mapping');
    }
}
