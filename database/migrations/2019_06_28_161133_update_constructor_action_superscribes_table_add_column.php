<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateConstructorActionSuperscribesTableAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_action_superscribes', function (\App\Migration\Blueprint $table) {
            $table->tinyInteger('is_current')->default(0)->after('end_state_id');
            $table->showStatus();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_action_superscribes', function (\App\Migration\Blueprint $table) {
            $table->dropColumn('is_current');
        });
    }
}
