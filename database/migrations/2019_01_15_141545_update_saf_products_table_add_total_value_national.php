<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateSafProductsTableAddTotalValueNational extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_products', function (Blueprint $table) {
            $table->string('total_value_national')->nullable()->after('total_value_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_products', function (Blueprint $table) {
            $table->dropColumn('total_value_national');
        });
    }
}
