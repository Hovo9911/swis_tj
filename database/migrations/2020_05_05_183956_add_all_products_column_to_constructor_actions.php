<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddAllProductsColumnToConstructorActions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('constructor_actions', function (Blueprint $table) {
            $table->boolean('select_all_products')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('constructor_actions', function (Blueprint $table) {
            $table->dropColumn('select_all_products');
        });
    }
}
