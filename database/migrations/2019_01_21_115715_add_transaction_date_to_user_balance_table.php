<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddTransactionDateToUserBalanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('user_balance', function (Blueprint $table) {
            $table->dateTime('transaction_date')->after('payment_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('user_balance', function (Blueprint $table) {
            $table->dropColumn('transaction_date');
        });
    }
}
