<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\DB;

class UpdateDocumentTableUpdateColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('document', function (Blueprint $table) {
            $table->dropColumn('from_whom');
            $table->string('approve_agency_tax_id')->default('0')->after('subject_for_approvement')->nullable();
            $table->enum('approve_type',['agency','other'])->nullable()->after('approve_agency_tax_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('document', function (Blueprint $table) {
            $table->dropColumn('approve_agency_tax_id');
            $table->dropColumn('approve_type');
            $table->string('from_whom')->nullable();
        });
    }
}
