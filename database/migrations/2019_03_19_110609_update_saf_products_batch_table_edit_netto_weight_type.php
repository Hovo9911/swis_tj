<?php

use App\Migration\Migration;
use Illuminate\Support\Facades\DB;

class UpdateSafProductsBatchTableEditNettoWeightType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `saf_products_batch` MODIFY `netto_weight` varchar(255) NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE `saf_products_batch` MODIFY `netto_weight` varchar(255) NULL;');
    }
}
