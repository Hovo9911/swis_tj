<?php

use App\Migration\Migration;

class UpdateConstructorTemplatesTableNameColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE constructor_templates MODIFY name VARCHAR (512)  NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE constructor_templates MODIFY name VARCHAR (512)  NULL");
    }
}
