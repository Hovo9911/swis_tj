<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddEnableRisksToConstructorMapping extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_mapping', function (Blueprint $table) {
            $table->tinyInteger('enable_risks')->nullable()->after('persons_in_next_state');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_mapping', function (Blueprint $table) {
            $table->dropColumn('enable_risks');
        });
    }
}
