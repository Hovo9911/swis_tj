<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;

class CreateUserBalanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->create('user_balance', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payment_id');
            $table->integer('user_id');
            $table->string('company_tax_id');
            $table->enum('type', ['input', 'output']);
            $table->float('amount', 8, 2);
            $table->float('total_balance', 8, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_balance');
    }
}
