<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddSpecialTypesToConstructorActions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_actions', function (Blueprint $table) {
            $table->integer('special_type')->after('constructor_document_id')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_actions', function (Blueprint $table) {
            $table->dropColumn('special_type');
        });
    }
}
