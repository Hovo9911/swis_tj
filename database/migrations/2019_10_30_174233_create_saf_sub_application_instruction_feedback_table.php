<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSafSubApplicationInstructionFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saf_sub_application_instruction_feedback', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('saf_sub_application_instruction_id');
            $table->integer('instruction_id');
            $table->string('instruction_code');
            $table->integer('feedback_id')->nullable();
            $table->text('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saf_sub_application_instruction_feedback');
    }
}
