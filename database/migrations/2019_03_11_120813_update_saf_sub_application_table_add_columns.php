<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateSafSubApplicationTableAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_sub_applications', function (Blueprint $table) {
//            $table->dropColumn('products');
            $table->unsignedInteger('routing_id')->after('agency_id');
            $table->enum('manual_created',['0','1'])->default('0')->after('note');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_sub_applications', function (Blueprint $table) {
            $table->dropColumn('routing_id');
            $table->dropColumn('manual_created');
//            $table->string('products');
        });
    }
}
