<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateUserRolesTableAddMultipleAttributesColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('user_roles', function (Blueprint $table) {
            $table->json('multiple_attributes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('user_roles', function (Blueprint $table) {
            $table->dropColumn('multiple_attributes');
        });
    }
}
