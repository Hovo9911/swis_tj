<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateRoutingTableAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('routing', function (\App\Migration\Blueprint $table) {
            $table->json('saf_controlled_fields')->after('regime')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('routing', function (Blueprint $table) {
            $table->dropColumn('saf_controlled_fields');
        });
    }
}
