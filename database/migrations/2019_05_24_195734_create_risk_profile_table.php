<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;

class CreateRiskProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->create('risk_profile', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_tax_id');
            $table->integer('document_id');
            $table->string('name');
            $table->text('condition');
            $table->integer('weight');
            $table->integer('frequency');
            $table->dateTime('active_date_from');
            $table->dateTime('active_date_to');
            $table->showStatus();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('risk_profile');
    }
}
