<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddIsContentFieldToDataModel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('data_model_excel', function (Blueprint $table) {
            $table->tinyInteger('is_content')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('data_model_excel', function (Blueprint $table) {
            $table->dropColumn('is_content');
        });
    }
}
