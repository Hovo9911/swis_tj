<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddOrderFiledToConstructorActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('constructor_actions', function (Blueprint $table) {
            $table->integer('order')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('constructor_actions', function (Blueprint $table) {
            $table->dropColumn('order');
        });
    }
}
