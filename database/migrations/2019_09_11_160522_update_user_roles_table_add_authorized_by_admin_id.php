<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateUserRolesTableAddAuthorizedByAdminId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('user_roles', function (Blueprint $table) {
            $table->integer('authorized_by_admin_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('user_roles', function (Blueprint $table) {
            $table->dropColumn('authorized_by_admin_id');
        });
    }
}
