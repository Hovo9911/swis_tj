<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateUserTableAddStatusChangedByUserIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('users', function (Blueprint $table) {
            $table->integer('status_changed_by_user_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('users', function (Blueprint $table) {
            $table->dropColumn('status_changed_by_user_id');
        });
    }
}
