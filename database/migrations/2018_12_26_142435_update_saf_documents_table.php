<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\DB;

class UpdateSafDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_documents', function (Blueprint $table) {
            $table->dropColumn('products');
//            $table->unsignedInteger('subapplication_id')->nullable()->change();
            $table->enum('added_from_module',['saf','application','laboratory'])->after('state_id');
        });

        DB::statement('ALTER TABLE `saf_documents` MODIFY `subapplication_id` INTEGER UNSIGNED NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_documents', function (Blueprint $table) {
            $table->dropColumn('products');
            $table->dropColumn('added_from_module');
        });
        DB::statement('ALTER TABLE `saf_documents` MODIFY `subapplication_id` INTEGER UNSIGNED NOT NULL;');
    }
}
