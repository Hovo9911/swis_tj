<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateSafProductsTableAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_products', function (Blueprint $table) {
            $table->json('saf_controlled_fields')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_products', function (Blueprint $table) {
            $table->dropColumn('saf_controlled_fields');
        });
    }
}
