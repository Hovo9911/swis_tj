<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRoleMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_role_menus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('company_tax_id')->default('0');
            $table->integer('menu_id')->nullable();
            $table->json('roles')->nullable();
            $table->enum('need_update',['0','1']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_role_menus');
    }
}
