<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddEmailSubjectToConstructorNotificationsMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_notifications_ml', function (Blueprint $table) {
            $table->text('email_subject')->nullable()->after('sub_application_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_notifications_ml', function (Blueprint $table) {
            $table->dropColumn('email_subject');
        });
    }
}
