<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateRegionalOfficeMlAddAddressDescColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('regional_office_ml', function (Blueprint $table) {
            $table->string('address')->nullable();
            $table->text('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('regional_office_ml', function (Blueprint $table) {
            $table->dropColumn(['address','description']);
        });
    }
}
