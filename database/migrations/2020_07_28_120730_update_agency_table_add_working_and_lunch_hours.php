<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateAgencyTableAddWorkingAndLunchHours extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('agency', function (Blueprint $table) {
            $table->string('working_hours_start')->default(config('swis.working_hours_start') ?? '');
            $table->string('working_hours_end')->default(config('swis.working_hours_end') ?? '');
            $table->string('lunch_hours_start')->default(config('swis.lunch_hours_start') ?? '');
            $table->string('lunch_hours_end')->default(config('swis.lunch_hours_end') ?? '');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('agency', function (Blueprint $table) {
            $table->dropColumn(['working_hours_start', 'working_hours_end', 'lunch_hours_start', 'lunch_hours_end']);
        });
    }
}
