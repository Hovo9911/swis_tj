<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddTooltipToMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('menu', function (Blueprint $table) {
            $table->string('tooltip')->nullable();
        });

        $menus = \App\Models\Menu\Menu::select('id', 'name', 'title')->whereNotNull('name')->get();

        foreach ($menus as $menu) {
            if (strpos($menu->title, '.title') !== false) {
                $replaceTitle = '.tooltip';

                if (strpos( $menu->title, '.menu') === false) {
                    $replaceTitle = '.menu'.$replaceTitle;
                }

                $menu->tooltip = str_replace('.title', $replaceTitle, $menu->title);

            } elseif (strpos($menu->title, '.label') !== false) {

                $replaceLabel = '.tooltip';

                if (strpos( $menu->title, '.menu') === false) {
                    $replaceLabel = '.menu'.$replaceLabel;
                }

                $menu->tooltip = str_replace('.label', $replaceLabel, $menu->title);
            } else {
                $menu->tooltip = $menu->tooltip = $menu->title.".menu.tooltip";
            }

            $menu->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('menu', function (Blueprint $table) {
            $table->dropColumn('tooltip');
        });
    }
}
