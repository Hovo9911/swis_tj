<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateSafSubtitlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('saf_sub_applications', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('saf_number');
            $table->string('company_tax_id')->default('0');
            $table->string('subtitle_type')->nullable();
            $table->string('document_code')->nullable();
            $table->string('name')->nullable();
            $table->string('products')->nullable();
            $table->enum('current_status',['presented','registered','requested','submitted','recalled','canceled'])->default('presented');
            $table->string('document_number')->unique()->nullable();
            $table->date('status_data')->nullable();
            $table->text('note')->nullable();
            $table->adminInfo();
            $table->showStatus();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saf_sub_applications');
    }
}
