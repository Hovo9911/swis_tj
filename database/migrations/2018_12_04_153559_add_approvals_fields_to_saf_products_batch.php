<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddApprovalsFieldsToSafProductsBatch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_products_batch', function (Blueprint $table) {
            $table->string('approved_quantity')->nullable()->after('measurement_unit');
            $table->string('approved_type')->nullable()->after('approved_quantity');
            $table->string('rejected_quantity')->nullable()->after('approved_type');
            $table->string('rejection_reason')->nullable()->after('rejected_quantity');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_products_batch', function (Blueprint $table) {
            $table->dropColumn(['approved_quantity', 'approved_type', 'rejected_quantity', 'rejection_reason']);
        });

    }
}
