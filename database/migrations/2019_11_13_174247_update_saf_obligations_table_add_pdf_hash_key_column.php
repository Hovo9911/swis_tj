<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateSafObligationsTableAddPdfHashKeyColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_obligations', function (Blueprint $table) {
            $table->string('pdf_hash_key')->nullable()->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->constructor_notifications();
        $schemaBuilder->table('saf_obligations', function (Blueprint $table) {
            $table->dropColumn('pdf_hash_key');
        });
    }
}
