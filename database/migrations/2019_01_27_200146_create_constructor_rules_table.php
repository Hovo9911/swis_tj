<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConstructorRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('constructor_rules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('document_id');
            $table->enum('type', ['obligation','validation','calculation','risk']);
            $table->string('obligation_code', 50)->nullable();
            $table->text('condition')->nullable();
            $table->text('rule')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('constructor_rules');
    }
}
