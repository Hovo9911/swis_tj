<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddAllDataToSafSubApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('saf_sub_applications', function (Blueprint $table) {
            $table->jsonb('all_data')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('saf_sub_applications', function (Blueprint $table) {
            $table->dropColumn('all_data');
        });
    }
}
