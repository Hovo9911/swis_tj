<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateLaboratoryMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('laboratory_ml', function (Blueprint $table) {
            $table->unsignedInteger('laboratory_id');
            $table->unsignedSmallInteger('lng_id');
            $table->string('name',255);
            $table->string('legal_entity_name',255);
            $table->string('head_name',255)->nullable();
            $table->text('description')->nullable();
            $table->showStatus();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laboratory_ml');
    }
}
