<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateConstructorMappingAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_mapping', function (\App\Migration\Blueprint $table) {
            $table->enum('obligation_exist',['0','1'])->default('0')->after('mandatory_list')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_mapping', function (\App\Migration\Blueprint $table) {
            $table->dropColumn('obligation_exist');
        });
    }
}
