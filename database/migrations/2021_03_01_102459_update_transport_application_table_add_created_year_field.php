<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateTransportApplicationTableAddCreatedYearField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('transport_application', function (Blueprint $table) {
            $table->tinyInteger('created_year')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('transport_application', function (Blueprint $table) {
            $table->dropColumn('created_year');
        });
    }
}
