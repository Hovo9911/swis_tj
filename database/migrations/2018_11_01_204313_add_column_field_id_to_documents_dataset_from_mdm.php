<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddColumnFieldIdToDocumentsDatasetFromMdm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('documents_dataset_from_mdm', function (Blueprint $table) {
            $table->string('field_id')->after('id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('documents_dataset_from_mdm', function (Blueprint $table) {
            $table->string('field_id')->after('id')->nullable();
        });
    }
}
