<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddDigitalSignatureToConstructorDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('constructor_documents', function (Blueprint $table) {
            $table->string('digital_signature')->nullable()->default(0);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('constructor_documents', function (Blueprint $table) {
            $table->dropColumn('digital_signature');
        });
    }
}
