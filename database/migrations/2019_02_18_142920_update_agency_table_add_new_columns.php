<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateAgencyTableAddNewColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('agency', function (Blueprint $table) {
            $table->string('legal_entity_name')->after('agency_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('agency', function (Blueprint $table) {
            $table->dropColumn('legal_entity_name');
        });
    }
}
