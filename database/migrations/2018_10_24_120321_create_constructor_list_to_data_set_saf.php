<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateConstructorListToDataSetSaf extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->create('constructor_list_to_data_set_from_saf', function (Blueprint $table) {
            $table->integer('constructor_list_id');
            $table->integer('document_dataset_from_saf_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('constructor_list_to_data_set_from_saf');
    }
}
