<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddTypeColumnToReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('reports', function (Blueprint $table) {
            $table->tinyInteger('type')->default(0);
        });
        \App\Models\Reports\Reports::where('company_tax_id', '0')->where('is_global', 0)->update(['type' => 1]);
        \App\Models\Reports\Reports::where('company_tax_id', '0')->where('is_global', 1)->update(['type' => 2]);
        \App\Models\Reports\Reports::where('company_tax_id', '!=', '0')->where('is_global', 0)->update(['type' => 3]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('reports', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
