<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateSafProductsTableAddSafIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_products', function (Blueprint $table) {
            $table->string('saf_id')->after('saf_number')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_products', function (Blueprint $table) {
            $table->dropColumn('saf_id');
        });
    }
}
