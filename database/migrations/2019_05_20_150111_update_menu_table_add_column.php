<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;

class UpdateMenuTableAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('menu', function (\App\Migration\Blueprint $table) {
            $table->enum('is_module',['0','1'])->after('sort_order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('menu', function (\App\Migration\Blueprint $table) {
            $table->dropColumn('is_module');
        });
    }
}
