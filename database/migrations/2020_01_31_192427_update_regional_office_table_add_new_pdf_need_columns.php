<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateRegionalOfficeTableAddNewPdfNeedColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('regional_office', function (Blueprint $table) {
            $table->string('accreditation_number')->nullable();
            $table->dateTime('accreditation_date_of_issue')->nullable();
            $table->dateTime('accreditation_valid_until')->nullable();
        });

        $schemaBuilder->table('regional_office_ml', function (Blueprint $table) {
            $table->string('supervisor')->nullable();
            $table->string('department_head')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('regional_office', function (Blueprint $table) {
            $table->dropColumn(['accreditation_number','accreditation_date_of_issue','accreditation_valid_until']);
        });

        $schemaBuilder->table('regional_office_ml', function (Blueprint $table) {
            $table->dropColumn(['supervisor','department_head']);
        });
    }
}
