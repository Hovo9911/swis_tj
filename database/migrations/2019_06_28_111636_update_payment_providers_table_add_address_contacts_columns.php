<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdatePaymentProvidersTableAddAddressContactsColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('payment_providers', function (Blueprint $table) {
            $table->string('address')->nullable()->after('description');
            $table->string('contacts')->nullable()->after('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('payment_providers', function (Blueprint $table) {
            $table->dropColumn(['address', 'contacts']);
        });
    }
}
