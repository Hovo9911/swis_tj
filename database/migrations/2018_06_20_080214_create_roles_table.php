<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->string('company_tax_id')->default('0');
            $table->unsignedInteger('menu_id')->nullable();
            $table->string('name',255)->nullable();
            $table->text('description')->nullable();
            $table->enum('level',[1,2])->default('1');
            $table->enum('create_permission',['0','1'])->default('0');
            $table->enum('read_permission',['0','1'])->default('0');
            $table->enum('update_permission',['0','1'])->default('0');
            $table->enum('delete_permission',['0','1'])->default('0');
            $table->showStatus();
            $table->adminInfo();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
