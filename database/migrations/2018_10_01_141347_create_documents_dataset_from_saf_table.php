<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsDatasetFromSafTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents_dataset_from_saf', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('document_id')->unsigned();
            $table->string('saf_field_id');
            $table->tinyInteger('is_attribute')->default(0);
            $table->tinyInteger('is_hidden')->default(0);
            $table->tinyInteger('is_search_param')->default(0);
            $table->tinyInteger('show_in_search_results')->default(0);
            $table->timestamps();
        });

        Schema::table('documents_dataset_from_saf', function (Blueprint $table) {
            $table->foreign('document_id')->references('id')->on('constructor_documents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents_dataset_from_saf', function (Blueprint $table) {
            $table->dropForeign(['document_id']);
        });
        Schema::dropIfExists('documents_dataset_from_saf');
    }
}
