<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;

class CreateSafSubApplicationsDigitalSignatureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->create('saf_sub_applications_digital_signature', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('subapplication_id');
            $table->unsignedInteger('user_id')->nullable();
            $table->string('company_tax_id')->default('0');
            $table->string('file_path');
            $table->string('file_digest');
            $table->text('file_digest_hash')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saf_sub_applications_digital_signature');
    }
}

