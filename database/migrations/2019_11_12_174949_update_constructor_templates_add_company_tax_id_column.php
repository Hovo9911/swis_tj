<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

/**
 * Class UpdateConstructorTemplatesAddCompanyTaxIdColumn
 */
class UpdateConstructorTemplatesAddCompanyTaxIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_templates', function (Blueprint $table) {
            $table->string('company_tax_id')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->constructor_notifications();
        $schemaBuilder->table('constructor_templates', function (Blueprint $table) {
            $table->dropColumn('company_tax_id');
        });
    }
}
