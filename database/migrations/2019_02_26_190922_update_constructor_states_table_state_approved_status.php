<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateConstructorStatesTableStateApprovedStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_states', function (Blueprint $table) {
            $table->unsignedSmallInteger('state_approved_status')->after('state_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_states', function (Blueprint $table) {
            $table->dropColumn('state_approved_status');
        });
    }
}
