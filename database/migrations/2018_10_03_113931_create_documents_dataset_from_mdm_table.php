<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsDatasetFromMdmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents_dataset_from_mdm', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sort_order_id');
            $table->integer('document_id')->unsigned();
            $table->string('mdm_field_id');
            $table->string('group');
            $table->tinyInteger('is_attribute')->default(0);
            $table->tinyInteger('is_hidden')->default(0);
            $table->tinyInteger('is_search_param')->default(0);
            $table->tinyInteger('show_in_search_results')->default(0);
            $table->tinyInteger('is_active')->default(0);
            $table->timestamps();

            $table->foreign('document_id')->references('id')->on('constructor_documents');
        });

        Schema::create('documents_dataset_from_mdm_ml', function (Blueprint $table) {
            $table->integer('documents_dataset_from_mdm_id')->unsigned();
            $table->integer('lng_id');
            $table->string('label');
            $table->string('tooltip')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents_dataset_from_mdm', function (Blueprint $table) {
            $table->dropForeign(['document_id']);
        });
        Schema::dropIfExists('documents_dataset_from_mdm');
        Schema::dropIfExists('documents_dataset_from_mdm_ml');
    }
}
