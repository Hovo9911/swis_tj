<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateUserTableAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('users', function (Blueprint $table) {
            $table->date('passport_issue_date')->after('passport')->nullable();
            $table->date('passport_validity_date')->after('passport_issue_date')->nullable();
            $table->enum('is_dead', ['0', '1'])->after('passport_validity_date')->nullable();
            $table->date('death_date')->after('is_dead')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('users', function (Blueprint $table) {
            $table->dropColumn('passport_issue_date');
            $table->dropColumn('passport_validity_date');
            $table->dropColumn('is_dead');
            $table->dropColumn('death_date');
        });
    }
}
