<?php


use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateSubapplicationTableAddAgencyIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_sub_applications', function (Blueprint $table) {
            $table->unsignedInteger('agency_id')->nullable()->after('company_tax_id');
            $table->string('sub_application_number')->nullable()->after('saf_number');
            $table->string('reference_document_type_code')->nullable()->after('sub_application_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_sub_applications', function (Blueprint $table) {
            $table->dropColumn('agency_id');
            $table->dropColumn('sub_application_number');
            $table->dropColumn('reference_document_type_code');
        });
    }
}
