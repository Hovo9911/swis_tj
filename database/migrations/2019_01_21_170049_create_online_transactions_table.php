<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;

class CreateOnlineTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->create('online_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id');
            $table->string('transaction_id')->nullable();
            $table->string('token');
            $table->float('amount', 16, 2);
            $table->enum('status', ['ok', 'fail', 'pending'])->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('online_transactions');
    }
}
