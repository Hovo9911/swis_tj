<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddReferenceTableCodeToNotesOfReferenceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('notes_of_reference', function (Blueprint $table) {
            $table->string('reference_row_code')->after('reference_row_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('notes_of_reference', function (Blueprint $table) {
            $table->dropColumn('reference_row_code');
        });
    }
}
