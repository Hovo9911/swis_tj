<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;

class AddIsLabColumnAgenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('agency', function (Blueprint $table) {
            $table->boolean('is_lab')->default(false);
        });

        if (Schema::hasTable('reference_agencies_reference')) {
            $schemaBuilder->table('reference_agencies_reference', function (Blueprint $table) {
                $table->boolean('is_lab')->default(false);
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('agency', function (Blueprint $table) {
            $table->dropColumn('is_lab');
        });

        if (Schema::hasTable('reference_agencies_reference')) {
            $schemaBuilder->table('reference_agencies_reference', function (Blueprint $table) {
                $table->dropColumn('is_lab');
            });
        }
    }
}
