<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;

class UpdateUserRolesTableAddNewColumnAuthorizedByIdCurrentRoleColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('user_roles', function (Blueprint $table) {
            $table->enum('authorized_by_id_type' ,['local_admin','employee'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('user_roles', function (Blueprint $table) {
            $table->dropColumn('authorized_by_id_type');
        });
    }
}
