<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateLaboratoriesTableUpdateColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('laboratory', function (Blueprint $table) {
            $table->string('legal_entity_name')->after('address')->nullable();
        });

        $schemaBuilder->table('laboratory_ml', function (Blueprint $table) {
            $table->dropColumn('legal_entity_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('laboratory', function (Blueprint $table) {
            $table->dropColumn('legal_entity_name');
        });

        $schemaBuilder->table('laboratory_ml', function (Blueprint $table) {
            $table->string('legal_entity_name')->after('name')->nullable();
        });
    }
}
