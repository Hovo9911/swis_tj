<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddPrintDefaultToConstructorActions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_actions', function (Blueprint $table) {
            $table->tinyInteger('print_default')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_actions', function (Blueprint $table) {
            $table->dropColumn('print_default');
        });
    }
}
