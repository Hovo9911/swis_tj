<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class RemoveOrderIdFromOnlineTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('online_transactions', function (Blueprint $table) {
            $table->dropColumn('order_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('online_transactions', function (Blueprint $table) {
            $table->integer('order_id');
        });
    }
}
