<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddXpathAndGroupField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('documents_dataset_from_saf', function (Blueprint $table) {
            $table->string('group')->after('saf_field_id')->nullable()->default(null);
        });
        $schemaBuilder->table('documents_dataset_from_mdm', function (Blueprint $table) {
            $table->string('xpath')->after('group')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('documents_dataset_from_saf', function (Blueprint $table) {
            $table->dropColumn('group');
        });
        $schemaBuilder->table('documents_dataset_from_mdm', function (Blueprint $table) {
            $table->dropColumn('xpath');
        });
    }
}
