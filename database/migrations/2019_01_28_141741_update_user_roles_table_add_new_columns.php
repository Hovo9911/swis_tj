<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateUserRolesTableAddNewColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('user_roles', function (Blueprint $table) {
            $table->string('attribute_field_id')->after('attribute_id')->nullable();
            $table->enum('attribute_type',['saf','mdm'])->after('attribute_field_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('user_roles', function (Blueprint $table) {
            $table->dropColumn('attribute_field_id');
            $table->dropColumn('attribute_type');
        });
    }
}
