<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConstructorActionSuperscribesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('constructor_action_superscribes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_tax_id')->default('0');
            $table->unsignedInteger('saf_subapplication_id');
            $table->unsignedInteger('constructor_document_id');
            $table->unsignedInteger('from_user');
            $table->unsignedInteger('to_user');
            $table->unsignedInteger('start_state_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('constructor_action_superscribes');
    }
}
