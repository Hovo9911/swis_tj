<?php

use App\Migration\Migration;

class UpdateRegionalOfficeAddCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('regional_office', function (\App\Migration\Blueprint $table) {
            $table->json('constructor_document')->after('office_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('regional_office', function (\App\Migration\Blueprint $table) {
            $table->dropColumn('constructor_document');
        });
    }
}
