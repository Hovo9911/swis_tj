<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateSafDocumentsTableAddNewColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_documents', function (Blueprint $table) {
            $table->unsignedInteger('expertise_id')->after('state_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_documents', function (Blueprint $table) {
            $table->dropColumn('expertise_id');
        });
    }
}
