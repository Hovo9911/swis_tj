<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;

class CreateSafObligationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->create('saf_obligations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('company_tax_id');
            $table->string('saf_number');
            $table->unsignedInteger('subapplication_id');
            $table->string('type')->nullable();
            $table->string('name')->nullable();
            $table->string('budget_line')->nullable();
            $table->double('obligation', 10, 2)->nullable();
            $table->double('payment', 10, 2)->nullable();
            $table->double('balance', 10, 2)->nullable();
            $table->adminInfo();
            $table->showStatus();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saf_obligations');
    }
}
