<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GlobalUpdateColumnsAddIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*Schema::table('agency', function(Blueprint $table) {$table->index(['tax_id','user_id']);});
        Schema::table('agency_ml', function(Blueprint $table) {$table->index(['agency_id']);});
        Schema::table('broker', function(Blueprint $table) {$table->index(['user_id','tax_id']);});
        Schema::table('broker_ml', function(Blueprint $table) {$table->index(['broker_id']);});
        Schema::table('company', function(Blueprint $table) {$table->index(['tax_id']);});
        Schema::table('constructor_actions', function(Blueprint $table) {$table->index(['user_id','constructor_document_id','action_id'],'u_id_cd_i_ac_id_sh_st');});
        Schema::table('constructor_actions_ml', function(Blueprint $table) {$table->index(['constructor_actions_id'],'c_id_l_id_sh_st');});
        Schema::table('constructor_action_superscribes', function(Blueprint $table) {$table->index(['saf_subapplication_id','constructor_document_id'],'s_id_cd_id');});
        Schema::table('constructor_action_to_template', function(Blueprint $table) {$table->index(['constructor_action_id','constructor_template_id'],'c_action_id_c_tempalte_id');});
        Schema::table('constructor_documents', function(Blueprint $table) {$table->index(['user_id','document_code','company_tax_id','document_type_id'],'u_id_doc_code_c_taxid');});
        Schema::table('constructor_documents_ml', function(Blueprint $table) {$table->index(['constructor_documents_id'],'cd_i_sh_st');});
        Schema::table('constructor_documents_roles', function(Blueprint $table) {$table->index(['document_id','role_id']);});
        Schema::table('constructor_documents_roles_attribute', function(Blueprint $table) {$table->index(['constructor_documents_roles_id','field_id'],'cd_role_id_field_id');});
        Schema::table('constructor_lists', function(Blueprint $table) {$table->index(['document_id','list_type']);});
        Schema::table('constructor_list_to_data_set_from_mdm', function(Blueprint $table) {$table->index(['constructor_list_id','constructor_data_set_field_id'],'c_list_id_field_id');});
        Schema::table('constructor_list_to_data_set_from_saf', function(Blueprint $table) {$table->index(['constructor_list_id','saf_field_id'],'c_list_id_saf_f_id');});
        Schema::table('constructor_mapping', function(Blueprint $table) {$table->index(['document_id','start_state','role','action','end_state'],'dc_id_start_st_role_action');});
        Schema::table('constructor_mapping_ml', function(Blueprint $table) {$table->index(['constructor_mapping_id'],'c_mapping_i_sh_st');});
        Schema::table('constructor_mapping_to_rule', function(Blueprint $table) {$table->index(['constructor_mapping_id','rule_id']);});
        Schema::table('constructor_notifications', function(Blueprint $table) {$table->index(['user_id','company_tax_id']);});
        Schema::table('constructor_notifications_ml', function(Blueprint $table) {$table->index(['constructor_notification_id','sub_application_id'],'c_n_i_sub_id');});
        Schema::table('constructor_roles', function(Blueprint $table) {$table->index(['user_id','constructor_document_id','role_id']);});
        Schema::table('constructor_roles_ml', function(Blueprint $table) {$table->index(['constructor_roles_id'],'cons_i_sh_st');});
        Schema::table('constructor_rules', function(Blueprint $table) {$table->index(['document_id','obligation_code']);});
        Schema::table('constructor_states', function(Blueprint $table) {$table->index(['user_id','constructor_document_id','state_id'],'u_id_c_id_s_id');});
        Schema::table('constructor_states_ml', function(Blueprint $table) {$table->index(['constructor_states_id'],'cons_state_i_sh_st');});
        Schema::table('data_model_excel', function(Blueprint $table) {$table->index(['user_id','reference']);});
        Schema::table('data_model_excel_ml', function(Blueprint $table) {$table->index(['data_model_id']);});
        Schema::table('data_set', function(Blueprint $table) {$table->index(['document_id','field_id']);});
        Schema::table('dictionary_ml', function(Blueprint $table) {$table->index(['dictionary_id']);});
        Schema::table('document', function(Blueprint $table) {$table->index(['user_id','document_type_id'],'u_d_s');});
        Schema::table('documents_dataset_from_mdm', function(Blueprint $table) {$table->index(['field_id','sort_order_id','mdm_field_id'],'field_id_sort_order_mdm_id');});
        Schema::table('documents_dataset_from_mdm_ml', function(Blueprint $table) {$table->index(['documents_dataset_from_mdm_id'],'doc_datase');});
        Schema::table('documents_dataset_from_saf', function(Blueprint $table) {$table->index(['saf_field_id']);});
        Schema::table('documents_notes', function(Blueprint $table) {$table->index(['user_id']);});
        Schema::table('folder', function(Blueprint $table) {$table->index(['user_id','company_tax_id']);});
        Schema::table('folder_documents', function(Blueprint $table) {$table->index(['folder_id','document_id','user_id']);});
        Schema::table('laboratory', function(Blueprint $table) {$table->index(['company_tax_id','tax_id','user_id']);});
        Schema::table('laboratory_indicator', function(Blueprint $table) {$table->index(['company_tax_id','laboratory_id','user_id','indicator_id'],'c_id_l_id_u_id_i_id');});
        Schema::table('laboratory_ml', function(Blueprint $table) {$table->index(['laboratory_id']);});
        Schema::table('laboratory_users', function(Blueprint $table) {$table->index(['laboratory_id','user_id']);});
        Schema::table('notes_of_reference', function(Blueprint $table) {$table->index(['reference_table_id','reference_row_id','reference_row_code'],'r_id_rr_id');});
        Schema::table('notifications', function(Blueprint $table) {$table->index(['user_id','sub_application_id']);});
        Schema::table('online_transactions', function(Blueprint $table) {$table->index(['user_id','company_tax_id','transaction_id']);});
        Schema::table('regional_office', function(Blueprint $table) {$table->index(['user_id','company_tax_id']);});
        Schema::table('regional_office_ml', function(Blueprint $table) {$table->index(['regional_office_id']);});
        Schema::table('reports_ml', function(Blueprint $table) {$table->index(['report_id']);});
        Schema::table('report_agencies', function(Blueprint $table) {$table->index(['report_id','agency_id']);});
        Schema::table('roles', function(Blueprint $table) {$table->index(['user_id','company_tax_id','menu_id']);});
        Schema::table('roles_group', function(Blueprint $table) {$table->index(['user_id','company_tax_id','menu_id']);});
        Schema::table('roles_group_roles', function(Blueprint $table) {$table->index(['roles_group_id','role_id']);});
        Schema::table('role_attributes', function(Blueprint $table) {$table->index(['role_id','attribute_id']);});
        Schema::table('role_menus', function(Blueprint $table) {$table->index(['role_id','menu_id']);});
        Schema::table('routing', function(Blueprint $table) {$table->index(['user_id','constructor_document_id'],'u_id_cd_id_sh');});
        Schema::table('saf', function(Blueprint $table) {$table->index(['company_tax_id','user_id']);});
        Schema::table('saf_documents', function(Blueprint $table) {$table->index(['user_id','company_tax_id','saf_number','document_id','subapplication_id','expertise_id'],'uid_c_tax_saf_n_d_id');});
        Schema::table('saf_document_products', function(Blueprint $table) {$table->index(['user_id','company_tax_id','saf_number','product_id','saf_document_id'],'u_id_c_tax_id_s_number');});
        Schema::table('saf_expertise', function(Blueprint $table) {$table->index(['user_id','company_tax_id','saf_number','application_id','laboratory_id','saf_product_id'],'u_id_comp_tax_saf_number');});
        Schema::table('saf_expertise_documents_products', function(Blueprint $table) {$table->index(['user_id','company_tax_id','saf_expertise_id','document_id'],'u_id_comp_tax_id_exp_id');});
        Schema::table('saf_expertise_indicators', function(Blueprint $table) {$table->index(['saf_expertise_id','user_id','company_tax_id','laboratory_expertise_code'],'se_id_u_id_comp_tax');});
        Schema::table('saf_notes', function(Blueprint $table) {$table->index(['saf_number','sub_application_id','user_id']);});
        Schema::table('saf_obligations', function(Blueprint $table) {$table->index(['user_id','saf_number','state_id','subapplication_id'],'us_id_saf_numb_state_id');});
        Schema::table('saf_products', function(Blueprint $table) {$table->index(['saf_number','user_id']);});
        Schema::table('saf_products_batch', function(Blueprint $table) {$table->index(['saf_product_id','batch_number','measurement_unit','approved_type'],'sp_id_batch_number_m_unit');});
        Schema::table('saf_sub_application_products', function(Blueprint $table) {$table->index(['company_tax_id','user_id','subapplication_id','product_id','saf_number'],'c_tax_user_id_sub');});
        Schema::table('saf_sub_application_states', function(Blueprint $table) {$table->index(['user_id','saf_number','saf_subapplication_id','state_id'],'u_id_saf_numb_sub_app_state_id');});
        Schema::table('notifications', function(Blueprint $table) {$table->index(['user_id','notification_id','sub_application_id','company_tax_id'],'us_id_not_id_sub_id');});
        Schema::table('transactions', function(Blueprint $table) {$table->index(['payment_id','saf_number','user_id','company_tax_id']);});
        Schema::table('users_verify', function(Blueprint $table) {$table->index(['user_id']);});
        Schema::table('user_companies', function(Blueprint $table) {$table->index(['user_id','company_id']);});
        Schema::table('user_manuals_ml', function(Blueprint $table) {$table->index(['user_manual_id']);});
        Schema::table('user_roles', function(Blueprint $table) {$table->index(['user_id','company_tax_id','role_id']);});*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}

/*
 ALTER TABLE `reference_unit_of_measurement_reference` ADD INDEX(`code`);
 ALTER TABLE `reference_unit_of_measurement_reference` ADD INDEX(`company_tax_id`);
 ALTER TABLE `reference_unit_of_measurement_reference_ml` ADD INDEX(`reference_unit_of_measurement_reference_id`);
 ALTER TABLE `reference_hs_code_6` ADD INDEX(`code`);
 ALTER TABLE `reference_hs_code_6` ADD INDEX(`company_tax_id`);
 ALTER TABLE `reference_hs_code_6` ADD INDEX(`unit_of_measurement_1`);
 ALTER TABLE `reference_hs_code_6` ADD INDEX(`unit_of_measurement_2`);
 ALTER TABLE `reference_hs_code_6_ml` ADD INDEX(`reference_hs_code_6_id`);
 ALTER TABLE `reference_hs_code_6_ml` ADD INDEX(`name`);
*/

/*

ALTER TABLE `saf` ADD INDEX(`regular_number`);
ALTER TABLE `saf_sub_applications` ADD INDEX(`saf_number`);
ALTER TABLE `saf_obligations` ADD INDEX(`subapplication_id`);
ALTER TABLE `saf` ADD INDEX(`creator_user_id`);
ALTER TABLE `saf` ADD INDEX(`user_id`);
ALTER TABLE `saf` ADD INDEX(`company_tax_id`);
ALTER TABLE `agency` ADD INDEX(`tax_id`);
ALTER TABLE `saf_sub_applications` ADD INDEX(`company_tax_id`);
ALTER TABLE `agency_ml` ADD INDEX(`agency_id`);
ALTER TABLE `constructor_documents` ADD INDEX(`document_code`);
ALTER TABLE `saf_sub_applications` ADD INDEX(`document_code`);
ALTER TABLE `constructor_documents_ml` ADD INDEX(`constructor_documents_id`);
ALTER TABLE `reference_obligation_budget_line` ADD INDEX(`code`);
ALTER TABLE `saf_obligations` ADD INDEX(`obligation_code`);
ALTER TABLE `saf_obligations` ADD INDEX(`is_free_sum`);
ALTER TABLE `reference_obligation_type` ADD INDEX(`code`);
ALTER TABLE `saf_obligations` ADD INDEX(`free_sum_budget_line_code`);
ALTER TABLE `saf_obligations` ADD INDEX(`budget_line`);
 */

/*
ALTER TABLE `agency` ADD INDEX(`user_id`);
ALTER TABLE `broker` ADD INDEX(`user_id`);
ALTER TABLE `broker` ADD INDEX(`tax_id`);
ALTER TABLE `broker_ml` ADD INDEX(`broker_id`);
ALTER TABLE `company` ADD INDEX(`tax_id`);
ALTER TABLE `constructor_actions` ADD INDEX(`user_id`);
ALTER TABLE `constructor_actions` ADD INDEX(`constructor_document_id`);
ALTER TABLE `constructor_actions` ADD INDEX(`action_id`);
ALTER TABLE `constructor_actions_ml` ADD INDEX(`constructor_actions_id`);
ALTER TABLE `constructor_action_superscribes` ADD INDEX(`company_tax_id`);
ALTER TABLE `constructor_action_superscribes` ADD INDEX(`saf_subapplication_id`);
ALTER TABLE `constructor_action_superscribes` ADD INDEX(`constructor_document_id`);
ALTER TABLE `constructor_documents` ADD INDEX(`user_id`);
ALTER TABLE `constructor_documents` ADD INDEX(`document_code`);
ALTER TABLE `constructor_documents` ADD INDEX(`company_tax_id`);
ALTER TABLE `constructor_documents` ADD INDEX(`document_type_id`);
ALTER TABLE `constructor_documents_ml` ADD INDEX(`constructor_documents_id`);
ALTER TABLE `constructor_documents_roles_attribute` ADD INDEX(`constructor_documents_roles_id`);
ALTER TABLE `constructor_documents_roles_attribute` ADD INDEX(`field_id`);
ALTER TABLE `constructor_lists` ADD INDEX(`document_id`);
ALTER TABLE `constructor_lists` ADD INDEX(`list_type`);
ALTER TABLE `constructor_list_to_data_set_from_mdm` ADD INDEX(`constructor_list_id`);
ALTER TABLE `constructor_list_to_data_set_from_mdm` ADD INDEX(`constructor_data_set_field_id`);
ALTER TABLE `constructor_list_to_data_set_from_saf` ADD INDEX(`constructor_list_id`);
ALTER TABLE `constructor_mapping` ADD INDEX(`document_id`);
ALTER TABLE `constructor_mapping` ADD INDEX(`start_state`);
ALTER TABLE `constructor_mapping` ADD INDEX(`role`);
ALTER TABLE `constructor_mapping` ADD INDEX(`action`);
ALTER TABLE `constructor_mapping` ADD INDEX(`end_state`);
ALTER TABLE `constructor_mapping_ml` ADD INDEX(`constructor_mapping_id`);
ALTER TABLE `constructor_mapping_to_rule` ADD INDEX(`constructor_mapping_id`);
ALTER TABLE `constructor_mapping_to_rule` ADD INDEX(`rule_id`);
ALTER TABLE `constructor_notifications` ADD INDEX(`user_id`);
ALTER TABLE `constructor_notifications` ADD INDEX(`company_tax_id`);
ALTER TABLE `constructor_notifications_ml` ADD INDEX(`constructor_notification_id`);
ALTER TABLE `constructor_notifications_ml` ADD INDEX(`sub_application_id`);
ALTER TABLE `constructor_roles` ADD INDEX(`user_id`);
ALTER TABLE `constructor_roles` ADD INDEX(`constructor_document_id`);
ALTER TABLE `constructor_roles` ADD INDEX(`role_id`);
ALTER TABLE `constructor_roles_ml` ADD INDEX(`constructor_roles_id`);
ALTER TABLE `constructor_rules` ADD INDEX(`type`);
ALTER TABLE `constructor_rules` ADD INDEX(`obligation_code`);
ALTER TABLE `constructor_states` ADD INDEX(`user_id`);
ALTER TABLE `constructor_states` ADD INDEX(`constructor_document_id`);
ALTER TABLE `constructor_states` ADD INDEX(`state_id`);
ALTER TABLE `constructor_states_ml` ADD INDEX(`constructor_states_id`);
ALTER TABLE `documents_dataset_from_mdm` ADD INDEX(`field_id`);
ALTER TABLE `documents_dataset_from_mdm_ml` ADD INDEX(`documents_dataset_from_mdm_id`);
ALTER TABLE `documents_dataset_from_saf` ADD INDEX(`saf_field_id`);
ALTER TABLE `documents_notes` ADD INDEX(`user_id`);
ALTER TABLE `documents_notes` ADD INDEX(`document_uuid`);
ALTER TABLE `folder` ADD INDEX(`company_tax_id`);
ALTER TABLE `folder` ADD INDEX(`user_id`);
ALTER TABLE `folder` ADD INDEX(`creator_type_value`);
ALTER TABLE `folder` ADD INDEX(`holder_type_value`);
ALTER TABLE `folder_documents` ADD INDEX(`folder_id`);
ALTER TABLE `folder_documents` ADD INDEX(`document_id`);
ALTER TABLE `folder_documents` ADD INDEX(`user_id`);
ALTER TABLE `laboratory` ADD INDEX(`company_tax_id`);
ALTER TABLE `laboratory` ADD INDEX(`tax_id`);
ALTER TABLE `laboratory` ADD INDEX(`user_id`);
ALTER TABLE `laboratory_indicator` ADD INDEX(`company_tax_id`);
ALTER TABLE `laboratory_indicator` ADD INDEX(`user_id`);
ALTER TABLE `laboratory_indicator` ADD INDEX(`laboratory_id`);
ALTER TABLE `laboratory_indicator` ADD INDEX(`indicator_id`);
ALTER TABLE `laboratory_ml` ADD INDEX(`laboratory_id`);
ALTER TABLE `laboratory_users` ADD INDEX(`laboratory_id`);
ALTER TABLE `laboratory_users` ADD INDEX(`user_id`);
ALTER TABLE `notes_of_reference` ADD INDEX(`reference_table_id`);
ALTER TABLE `notes_of_reference` ADD INDEX(`reference_row_id`);
ALTER TABLE `online_transactions` ADD INDEX(`user_id`);
ALTER TABLE `online_transactions` ADD INDEX(`company_tax_id`);
ALTER TABLE `payments` ADD INDEX(`tax_id`);
ALTER TABLE `regional_office` ADD INDEX(`company_tax_id`);
ALTER TABLE `regional_office` ADD INDEX(`user_id`);
ALTER TABLE `regional_office_ml` ADD INDEX(`regional_office_id`);
ALTER TABLE `roles` ADD INDEX(`user_id`);
ALTER TABLE `roles` ADD INDEX(`company_tax_id`);
ALTER TABLE `roles` ADD INDEX(`menu_id`);
ALTER TABLE `roles_group` ADD INDEX(`user_id`);
ALTER TABLE `roles_group` ADD INDEX(`company_tax_id`);
ALTER TABLE `roles_group` ADD INDEX(`menu_id`);
ALTER TABLE `roles_group_roles` ADD INDEX(`roles_group_id`);
ALTER TABLE `roles_group_roles` ADD INDEX(`role_id`);
ALTER TABLE `role_menus` ADD INDEX(`role_id`);
ALTER TABLE `role_menus` ADD INDEX(`menu_id`);
ALTER TABLE `routing` ADD INDEX(`user_id`);
ALTER TABLE `routing` ADD INDEX(`company_tax_id`);
ALTER TABLE `routing` ADD INDEX(`constructor_document_id`);
ALTER TABLE `saf_documents` ADD INDEX(`user_id`);
ALTER TABLE `saf_documents` ADD INDEX(`company_tax_id`);
ALTER TABLE `saf_documents` ADD INDEX(`saf_number`);
ALTER TABLE `saf_documents` ADD INDEX(`document_id`);
ALTER TABLE `saf_documents` ADD INDEX(`subapplication_id`);
ALTER TABLE `saf_document_products` ADD INDEX(`user_id`);
ALTER TABLE `saf_document_products` ADD INDEX(`company_tax_id`);
ALTER TABLE `saf_document_products` ADD INDEX(`saf_number`);
ALTER TABLE `saf_document_products` ADD INDEX(`product_id`);
ALTER TABLE `saf_document_products` ADD INDEX(`saf_document_id`);
ALTER TABLE `saf_expertise` ADD INDEX(`company_tax_id`);
ALTER TABLE `saf_expertise` ADD INDEX(`user_id`);
ALTER TABLE `saf_expertise` ADD INDEX(`saf_number`);
ALTER TABLE `saf_expertise` ADD INDEX(`application_id`);
ALTER TABLE `saf_expertise` ADD INDEX(`saf_product_id`);
ALTER TABLE `saf_expertise` ADD INDEX(`laboratory_id`);
ALTER TABLE `saf_products` ADD INDEX(`saf_number`);
ALTER TABLE `saf_products` ADD INDEX(`user_id`);
ALTER TABLE `saf_products` ADD INDEX(`total_value_national`);
ALTER TABLE `saf_products_batch` ADD INDEX(`saf_product_id`);
ALTER TABLE `saf_products_batch` ADD INDEX(`measurement_unit`);
ALTER TABLE `saf_products_batch` ADD INDEX(`batch_number`);
ALTER TABLE `saf_sub_application_products` ADD INDEX(`company_tax_id`);
ALTER TABLE `saf_sub_application_products` ADD INDEX(`user_id`);
ALTER TABLE `saf_sub_application_products` ADD INDEX(`subapplication_id`);
ALTER TABLE `saf_sub_application_products` ADD INDEX(`product_id`);
ALTER TABLE `saf_sub_application_products` ADD INDEX(`saf_number`);
ALTER TABLE `saf_sub_application_states` ADD INDEX(`saf_number`);
ALTER TABLE `saf_sub_application_states` ADD INDEX(`saf_subapplication_id`);
ALTER TABLE `saf_sub_application_states` ADD INDEX(`user_id`);
ALTER TABLE `saf_sub_application_states` ADD INDEX(`state_id`);
ALTER TABLE `saf_notes` ADD INDEX(`saf_number`);
ALTER TABLE `saf_notes` ADD INDEX(`sub_application_id`);
ALTER TABLE `saf_notes` ADD INDEX(`user_id`);
ALTER TABLE `saf_expertise_indicators` ADD INDEX(`saf_expertise_id`);
ALTER TABLE `saf_expertise_indicators` ADD INDEX(`company_tax_id`);
ALTER TABLE `saf_expertise_indicators` ADD INDEX(`user_id`);
ALTER TABLE `reference_document_type_reference` ADD INDEX(`company_tax_id`);
ALTER TABLE `reference_document_type_reference` ADD INDEX(`user_id`);
ALTER TABLE `reference_document_type_reference` ADD INDEX(`according_agency`);
 */

/*
 ALTER TABLE `document` ADD INDEX(`document_form`);
 ALTER TABLE `document` ADD INDEX(`user_id`);
 ALTER TABLE `document` ADD INDEX(`company_tax_id`);
 ALTER TABLE `document` ADD INDEX(`holder_type_value`);
 ALTER TABLE `document` ADD INDEX(`creator_type_value`);
 */

/*
CREATE UNIQUE INDEX regular_number ON saf(regular_number)
 */