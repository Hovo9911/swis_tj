<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;

class CreateUserFailAuthTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->create('user_fail_auth', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username');
            $table->string('hash');
            $table->dateTime('last_increment_date')->nullable();
            $table->tinyInteger('is_blocked')->default(0);
            $table->integer('count')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_fail_auth');
    }
}
