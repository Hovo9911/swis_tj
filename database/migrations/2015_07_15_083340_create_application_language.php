<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationLanguage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_language', function (Blueprint $table) {
            $table->integer('application_id');
            $table->integer('lng_id');
            $table->enum('is_default', ['0', '1'])->default('1');
            $table->enum('show_status',['1','2','0'])->default('1');
            $table->primary(['application_id', 'lng_id']);
        });

        $this->seedFirstLanguage();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('application_language');
    }

    private function seedFirstLanguage()
    {
        DB::table('application_language')->insert(
            [
                [
                    'application_id' => 1,
                    'lng_id'    => 1,
                    'is_default'     => '1',
                    'show_status'    => '1'
                ],
                [
                    'application_id' => 2,
                    'lng_id'    => 1,
                    'is_default'     => '1',
                    'show_status'    => '1'
                ],
                [
                    'application_id' => 3,
                    'lng_id'    => 1,
                    'is_default'     => '1',
                    'show_status'    => '1'
                ]
            ]
        );
    }
}
