<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateConstructorActionToTemplateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->create('constructor_action_to_template', function (Blueprint $table) {
            $table->integer('constructor_action_id');
            $table->integer('constructor_template_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('constructor_action_to_template');
    }
}
