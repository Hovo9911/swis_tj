<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddRiskRunTypeToRiskProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('risk_profile', function (Blueprint $table) {
            $table->integer('risk_run_type')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('risk_profile', function (Blueprint $table) {
            $table->dropColumn('risk_run_type');
        });
    }
}
