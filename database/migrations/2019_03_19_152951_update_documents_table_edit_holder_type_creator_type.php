<?php

use App\Migration\Migration;
use Illuminate\Support\Facades\DB;

class UpdateDocumentsTableEditHolderTypeCreatorType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `document` MODIFY `holder_type` int(10) NOT NULL,MODIFY `creator_type` int(10) NOT NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE `document` MODIFY `holder_type` int(10) NOT NULL,MODIFY `creator_type` int(10) NOT NULL;');
    }
}
