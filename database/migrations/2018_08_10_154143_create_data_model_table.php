<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateDataModelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('data_model', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('excel_id');
            $table->string('name');
            $table->string('reference');
            $table->string('definition')->nullable();
            $table->enum('type',['a', 'n', 'an', 'd', 'dt'])->default('a');
            $table->enum('db_type',['varchar','char','integer','date','datetime'])->default('varchar');
            $table->string('wco_id')->nullable();
            $table->string('wco')->nullable();
            $table->string('eaec_id')->nullable();
            $table->string('eaec')->nullable();
            $table->string('client_min')->nullable();
            $table->string('client_max')->nullable();
            $table->string('pattern')->nullable();
            $table->date('available_at')->nullable();
            $table->date('available_to')->nullable();
            $table->showStatus();
            $table->adminInfo();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_model');
    }
}
