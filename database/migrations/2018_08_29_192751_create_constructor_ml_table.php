<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateConstructorMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('constructor_documents_ml', function (Blueprint $table) {
            $table->unsignedInteger('constructor_documents_id');
            $table->unsignedSmallInteger('lng_id');
            $table->string('document_name');
            $table->text('description')->nullable();
            $table->showStatus();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('constructor_documents_ml');
    }
}
