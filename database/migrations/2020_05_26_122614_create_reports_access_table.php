<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateReportsAccessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('reports_access', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('role_type', [1,2]);
            $table->string('jasper_report_label');
            $table->string('jasper_report_uri');
            $table->showStatus();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports_access');
    }
}
