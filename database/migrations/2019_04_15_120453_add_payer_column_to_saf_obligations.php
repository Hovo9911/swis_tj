<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddPayerColumnToSafObligations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_obligations', function (Blueprint $table) {
            $table->enum('payer_user_type', ['sw_admin', 'agency', 'broker', 'natural_person'])->after('balance')->nullable();
            $table->string('payer_user_id')->after('payer_user_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_obligations', function (Blueprint $table) {
            $table->dropColumn(['payer_user_type', 'payer_user_id']);
        });
    }
}
