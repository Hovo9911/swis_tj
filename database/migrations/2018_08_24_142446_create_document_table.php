<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('document', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('company_tax_id')->default('0');
            $table->string('uuid');
            $table->string('document_access_password',16);
            $table->enum('document_type',['created','registered','confirmed','rejected','suspended'])->default('created');
            $table->string('document_type_id')->default('0');
            $table->string('document_name');
            $table->text('document_description');
            $table->string('document_number',50)->nullable();
            $table->string('document_file')->nullable();
            $table->date('document_release_date')->nullable();
            $table->string('from_whom')->nullable();
            $table->enum('subject_for_approvement',[0,1])->default('0');
            $table->date('confirmation_date')->nullable();
            $table->string('confirming',70)->nullable();
            $table->string('document_status');
            $table->enum('holder_type',['tax_id','id_card','f_citizen'])->default('tax_id');
            $table->string('holder_type_value');
            $table->string('holder_tax_id');
            $table->string('holder_name',250);
            $table->string('holder_country');
            $table->string('holder_community')->nullable();
            $table->string('holder_address',250);
            $table->string('holder_phone_number',50);
            $table->string('holder_email');
            $table->enum('creator_type',['tax_id','id_card','f_citizen'])->default('tax_id');
            $table->string('creator_type_value');
            $table->string('creator_tax_id');
            $table->string('creator_name',250);
            $table->string('creator_country');
            $table->string('creator_address',250);
            $table->string('creator_phone_number',50)->nullable();
            $table->string('creator_email');
            $table->text('notes')->nullable();

            $table->adminInfo();
            $table->showStatus();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document');
    }
}
