<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class SafProductsTableAddPlacesTypeColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_products', function (Blueprint $table) {
            $table->string('places_type')->after('number_of_places')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_products', function (Blueprint $table) {
            $table->dropColumn('places_type');
        });
    }
}
