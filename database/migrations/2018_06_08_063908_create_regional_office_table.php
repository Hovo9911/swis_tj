<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateRegionalOfficeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('regional_office', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_tax_id')->default('0');
            $table->unsignedInteger('user_id');
            $table->string('office_code',12)->nullable();
            $table->string('address',70)->nullable();
            $table->char('phone_number',12)->nullable();
            $table->string('email',320)->nullable();
            $table->adminInfo();
            $table->showStatus();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regional_office');
    }
}
