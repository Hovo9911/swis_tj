<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;

class AddIncrementCountToRiskProfiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('risk_profile', function (Blueprint $table) {
            $table->integer('increment_count')->default(0);
        });

        $schemaBuilder->create('saf_sub_application_instructions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sub_application_id');
            $table->integer('profile_id');
            $table->integer('instruction_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('risk_profile', function (Blueprint $table) {
            $table->dropColumn('increment_count');
        });

        Schema::dropIfExists('saf_sub_application_instructions');
    }
}
