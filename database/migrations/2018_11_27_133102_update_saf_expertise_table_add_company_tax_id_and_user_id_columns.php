<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateSafExpertiseTableAddCompanyTaxIdAndUserIdColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_expertise', function (Blueprint $table) {
            $table->string('company_tax_id')->default('0')->after('id');
            $table->unsignedInteger('user_id')->after('company_tax_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_expertise', function (Blueprint $table) {
            $table->dropColumn('company_tax_id');
            $table->dropColumn('user_id');
        });
    }
}
