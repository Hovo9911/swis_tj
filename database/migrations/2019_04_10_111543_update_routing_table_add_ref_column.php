<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateRoutingTableAddRefColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('routing', function (\App\Migration\Blueprint $table) {
            $table->text('ref_classificator_ids')->after('constructor_document_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('routing', function (Blueprint $table) {
            $table->dropColumn('ref_classificator_ids');
        });
    }
}
