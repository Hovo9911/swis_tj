<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateSafTableAddCustomDataColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf', function (Blueprint $table) {
            $table->text('custom_data')->after('data')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf', function (Blueprint $table) {
            $table->dropColumn('custom_data');
        });
    }
}
