<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;

class CreateTransportApplicationInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->create('transport_application_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('transport_application_id');
            $table->unsignedInteger('user_id');
            $table->string('company_tax_id')->default('0');
            $table->string('tracking_number' ,35)->unique();
            $table->decimal('payment_amount',16, 6);
            $table->string('transaction_number')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transport_application_invoices');
    }
}
