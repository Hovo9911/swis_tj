<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSafNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saf_notes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('saf_number');
            $table->integer('sub_application_id')->nullable();
            $table->integer('user_id');
            $table->string('note');
            $table->tinyInteger('to_saf_holder');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saf_notes');
    }
}
