<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddApprovedRejectedNettoWeightToSafProductsBatch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_products_batch', function (Blueprint $table) {
            $table->double('approved_netto_weight', 8, 2)->nullable();
            $table->double('rejected_netto_weight', 8, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_products_batch', function (Blueprint $table) {
            $table->dropColumn('approved_netto_weight');
            $table->dropColumn('rejected_netto_weight');
        });
    }
}
