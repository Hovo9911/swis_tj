<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateNotificationTemplatesMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->create('notification_templates_ml', function (Blueprint $table) {
            $table->unsignedInteger('notification_template_id');
            $table->unsignedSmallInteger('lng_id');
            $table->integer('sub_application_id')->default(0);
            $table->text('email_notification')->nullable();
            $table->text('in_app_notification')->nullable();
            $table->text('sms_notification')->nullable();
            $table->text('email_subject')->nullable()->after('sub_application_id');
            $table->showStatus();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_templates_ml');
    }
}
