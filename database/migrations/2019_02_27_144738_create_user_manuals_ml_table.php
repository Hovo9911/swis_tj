<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateUserManualsMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->create('user_manuals_ml', function (Blueprint $table) {
            $table->unsignedInteger('user_manual_id');
            $table->unsignedSmallInteger('lng_id');
            $table->text('module')->nullable();
            $table->text('description')->nullable();
            $table->showStatus();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_manuals_ml');
    }
}
