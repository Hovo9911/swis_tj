<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateSafDataStructureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('saf_data_structure', function (Blueprint $table) {
            $table->increments('id');
            $table->string('version');
            $table->text('xml');
            $table->showStatus();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saf_data_structure');
    }
}
