<?php

use App\Models\BaseModel;
use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateConstructorMappingMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('constructor_mapping_ml', function (Blueprint $table) {
            $table->unsignedInteger('constructor_mapping_id');
            $table->unsignedSmallInteger('lng_id');
            $table->text('description');
            $table->showStatus();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('constructor_mapping_ml');
    }
}
