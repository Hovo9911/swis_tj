<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateCompleqsdatasequencesfinalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('ComplexDataSequencesFinal', function (Blueprint $table) {
            $table->string('fractionDigits',255)->after('ref')->nullable();
            $table->string('totalDigits',255)->after('fractionDigits')->nullable();
            $table->string('length',255)->after('totalDigits')->nullable();
            $table->string('minLength',255)->after('length')->nullable();
            $table->string('maxLength',255)->after('minLength')->nullable();
            $table->string('pattern',255)->after('maxLength')->nullable();
            $table->string('minInclusive',255)->after('pattern')->nullable();
            $table->string('enum',255)->after('minInclusive')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('ComplexDataSequencesFinal', function (Blueprint $table) {
            $table->dropColumn('fractionDigits');
            $table->dropColumn('totalDigits');
            $table->dropColumn('length');
            $table->dropColumn('minLength');
            $table->dropColumn('maxLength');
            $table->dropColumn('pattern');
            $table->dropColumn('minInclusive');
            $table->dropColumn('enum');
        });
    }
}
