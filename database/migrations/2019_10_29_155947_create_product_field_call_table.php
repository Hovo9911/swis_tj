<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductFieldCallTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\Models\SingleApplicationSubApplicationInstructions\SingleApplicationSubApplicationInstructions::truncate();
        \App\Models\RiskProfileToInstructions\RiskProfileToInstructions::truncate();

        Schema::create('product_field_call', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sub_application_id');
            $table->string('field_id');
            $table->string('rule_condition');
            $table->integer('profile_id');
        });

        Schema::create('saf_sub_application_instructions_profile', function (Blueprint $table) {
            $table->integer('saf_sub_application_instructions_id');
            $table->integer('profile_id');
        });


        Schema::table('saf_sub_application_instructions', function (Blueprint $table) {
            $table->string('instruction_code')->nullable();
        });

        Schema::table('risk_profile_to_instructions', function (Blueprint $table) {
            $table->string('instruction_code')->nullable();
            $table->dropColumn('instruction_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_field_call');
        Schema::dropIfExists('saf_sub_application_instructions_profile');

        Schema::table('saf_sub_application_instructions', function (Blueprint $table) {
            $table->dropColumn('instruction_code');
        });

        Schema::table('risk_profile_to_instructions', function (Blueprint $table) {
            $table->dropColumn('instruction_code');
            $table->integer('instruction_id')->nullable();
        });
    }
}
