<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateSafTableAddSafControlledFieldColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf', function (Blueprint $table) {
            $table->json('saf_controlled_fields')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf', function (Blueprint $table) {
            $table->dropColumn('saf_controlled_fields');
        });
    }
}
