<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateSafSubApplicationsCertificates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->create('saf_sub_applications_certificates', function (Blueprint $table) {
            $table->unsignedInteger('sub_application_id');
            $table->unsignedInteger('user_id');
            $table->string('file_name');
            $table->enum('sub_application_status',['canceled'])->nulalble();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_sub_applications_certificates', function (Blueprint $table) {
            $table->dropColumn(['sub_application_id','user_id','file_name','sub_application_status']);
        });
    }
}
