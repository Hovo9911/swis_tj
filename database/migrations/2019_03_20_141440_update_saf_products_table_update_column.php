<?php

use Illuminate\Database\Migrations\Migration;use Illuminate\Support\Facades\DB;

class UpdateSafProductsTableUpdateColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `saf_products` MODIFY COLUMN `product_description`  TEXT  NULL ;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `saf_products` MODIFY COLUMN `product_description`  TEXT  NULL ;");
    }
}
