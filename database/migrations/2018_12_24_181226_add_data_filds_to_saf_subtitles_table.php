<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddDataFildsToSafSubtitlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_sub_applications', function (Blueprint $table) {
            $table->json('data')->nullable()->after('document_code');
            $table->json('custom_data')->nullable()->after('data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_sub_applications', function (Blueprint $table) {
            $table->dropColumn(['data', 'custom_data']);
        });
    }
}
