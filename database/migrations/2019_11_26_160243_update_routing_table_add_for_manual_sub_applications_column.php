<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateRoutingTableAddForManualSubApplicationsColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('routing', function (Blueprint $table) {
            $table->tinyInteger('is_manual')->nullable()->default(0);
            $table->integer('agency_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('routing', function (Blueprint $table) {
            $table->dropColumn('is_manual');
            $table->dropColumn('agency_id');
        });
    }
}
