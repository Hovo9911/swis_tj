<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddPdfTypeForConstructorTemplates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE constructor_templates ADD COLUMN pdf_type VARCHAR NOT NULL DEFAULT 'mpdf';");
        DB::table('constructor_templates')->update(['pdf_type' => 'tcpdf']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE constructor_templates DROP COLUMN pdf_type;");
    }
}
