<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;

class CreateReferenceNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->create('notes_of_reference', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('version');
            $table->enum('type', ['reference_table', 'reference_table_formation'])->default('reference_table');
            $table->integer('reference_table_id');
            $table->integer('reference_row_id')->nullable();
            $table->string('note')->default('');
            $table->json('old_data')->nullable();
            $table->json('new_data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notes_of_reference');
    }
}
