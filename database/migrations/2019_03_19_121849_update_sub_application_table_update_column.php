<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class UpdateSubApplicationTableUpdateColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `saf_sub_applications` CHANGE COLUMN `routing_id` `routings` VARCHAR (250)  NULL ;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `saf_sub_applications` CHANGE COLUMN `routings` `routing_id` INT(11) UNSIGNED NOT NULL ;");
    }
}
