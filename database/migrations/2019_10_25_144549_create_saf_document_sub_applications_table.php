<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;

class CreateSafDocumentSubApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->create('saf_document_sub_applications', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('creator_user_id')->nullable();
            $table->string('company_tax_id')->default('0');
            $table->string('saf_number');
            $table->unsignedInteger('sub_application_id');
            $table->unsignedInteger('saf_document_id');
            $table->unsignedInteger('sub_application_agency_id')->nullable();
            $table->unsignedInteger('sub_applicaiton_classificator_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saf_document_sub_applications');
    }
}
