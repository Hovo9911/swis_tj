<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;

class UpdateUserManualsTableAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('user_manuals', function (Blueprint $table) {
            $table->integer('creator_user_id')->nullable()->default(0);
            $table->integer('edited_user_id')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('user_manuals', function (Blueprint $table) {
            $table->dropColumn(['creator_user_id', 'edited_user_id']);
        });
    }
}
