<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;

class CreateSafExpertisingIndicatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->create('saf_expertise_indicators', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('saf_expertise_id');
            $table->string('company_tax_id')->default('0');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('laboratory_id');
            $table->string('laboratory_expertise_code');
            $table->tinyInteger('status_send')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saf_expertise_indicators');
    }
}
