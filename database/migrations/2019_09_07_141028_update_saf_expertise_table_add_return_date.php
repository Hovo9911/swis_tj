<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateSafExpertiseTableAddReturnDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_expertise', function (Blueprint $table) {
            $table->dateTime('return_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_expertise', function (Blueprint $table) {
            $table->dropColumn('return_date');
        });
    }
}
