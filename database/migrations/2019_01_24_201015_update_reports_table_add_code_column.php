<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateReportsTableAddCodeColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('reports', function (Blueprint $table) {
            $table->string('code')->after('company_tax_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('reports', function (Blueprint $table) {
            $table->dropColumn('code');
        });
    }
}
