<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;

class UpdateFewTablesAddUserPassportField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('document', function (Blueprint $table) {
            $table->string('holder_passport')->after('holder_type_value')->nullable();
        });

        $schemaBuilder->table('folder', function (Blueprint $table) {
            $table->string('holder_passport')->after('holder_type_value')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('document', function (Blueprint $table) {
            $table->dropColumn('holder_passport');
        });

        $schemaBuilder->table('folder', function (Blueprint $table) {
            $table->dropColumn('holder_passport');
        });
    }
}
