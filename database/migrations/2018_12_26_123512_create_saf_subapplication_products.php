<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateSafSubapplicationProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->create('saf_sub_application_products', function (Blueprint $table) {
            $table->string('company_tax_id')->default('0');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('subapplication_id');
            $table->unsignedInteger('product_id');
            $table->string('saf_number');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saf_sub_application_products');
    }
}
