<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddIsLabFlowToConstructorDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_documents', function (Blueprint $table) {
            $table->boolean('is_lab_flow')->nullable()->default(false);
        });

        \App\Models\ConstructorDocument\ConstructorDocument::where('company_tax_id', '610007405')->update(['is_lab_flow' => true]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_documents', function (Blueprint $table) {
            $table->dropColumn('regional_office_code');
        });
    }
}
