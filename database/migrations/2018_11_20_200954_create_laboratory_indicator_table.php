<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;

class CreateLaboratoryIndicatorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->create('laboratory_indicator', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_tax_id')->default('0');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('laboratory_id');
            $table->unsignedInteger('indicator_id');
            $table->float('price');
            $table->integer('duration');
            $table->enum('date_time',['hours','days']);
            $table->showStatus();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laboratory_indicator');
    }
}
