<?php

use Illuminate\Database\Migrations\Migration;use Illuminate\Support\Facades\DB;

class UpdateAFewTablesChangeColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `folder` CHANGE COLUMN `created_user_id` `creator_user_id` int(10)   NULL ;");
        DB::statement("ALTER TABLE `document` CHANGE COLUMN `created_user_id` `creator_user_id` int(10)   NULL ;");
        DB::statement("ALTER TABLE `saf_documents` CHANGE COLUMN `created_user_id` `creator_user_id` int(10)   NULL ;");
        DB::statement("ALTER TABLE `saf_document_products` CHANGE COLUMN `created_user_id` `creator_user_id` int(10)   NULL ;");
        DB::statement("ALTER TABLE `saf_obligations` CHANGE COLUMN `created_user_id` `creator_user_id` int(10)   NULL ;");
        DB::statement("ALTER TABLE `saf_notes` CHANGE COLUMN `created_user_id` `creator_user_id` int(10)   NULL ;");
        DB::statement("ALTER TABLE `saf` CHANGE COLUMN `created_user_id` `creator_user_id` int(10)   NULL ;");
        DB::statement("ALTER TABLE `saf_products` CHANGE COLUMN `created_user_id` `creator_user_id` int(10)   NULL ;");
        DB::statement("ALTER TABLE `saf_sub_applications` CHANGE COLUMN `created_user_id` `creator_user_id` int(10)   NULL ;");
        DB::statement("ALTER TABLE `saf_sub_application_products` CHANGE COLUMN `created_user_id` `creator_user_id` int(10)   NULL ;");
        DB::statement("ALTER TABLE `saf_sub_application_states` CHANGE COLUMN `created_user_id` `creator_user_id` int(10)   NULL ;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `folder` CHANGE COLUMN `creator_user_id` `created_user_id` int(10)   NULL ;");
        DB::statement("ALTER TABLE `document` CHANGE COLUMN `creator_user_id` `created_user_id` int(10)   NULL ;");
        DB::statement("ALTER TABLE `saf_documents` CHANGE COLUMN `creator_user_id` `created_user_id` int(10)   NULL ;");
        DB::statement("ALTER TABLE `saf_document_products` CHANGE COLUMN `creator_user_id` `created_user_id` int(10)   NULL ;");
        DB::statement("ALTER TABLE `saf_obligations` CHANGE COLUMN `creator_user_id` `created_user_id` int(10)   NULL ;");
        DB::statement("ALTER TABLE `saf_notes` CHANGE COLUMN `creator_user_id` `created_user_id` int(10)   NULL ;");
        DB::statement("ALTER TABLE `saf` CHANGE COLUMN `creator_user_id` `created_user_id` int(10)   NULL ;");
        DB::statement("ALTER TABLE `saf_products` CHANGE COLUMN `creator_user_id` `created_user_id` int(10)   NULL ;");
        DB::statement("ALTER TABLE `saf_sub_applications` CHANGE COLUMN `creator_user_id` `created_user_id` int(10)   NULL ;");
        DB::statement("ALTER TABLE `saf_sub_application_products` CHANGE COLUMN `creator_user_id` `created_user_id` int(10)   NULL ;");
        DB::statement("ALTER TABLE `saf_sub_application_states` CHANGE COLUMN `creator_user_id` `created_user_id` int(10)   NULL ;");
    }
}
