<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateUserCompaniesTableAddExecPositionColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('user_companies', function (Blueprint $table) {
            $table->string('exec_position')->after('company_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('user_companies', function (Blueprint $table) {
            $table->dropColumn('exec_position');
        });
    }
}
