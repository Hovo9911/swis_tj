<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateDocumentTableAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('document', function (Blueprint $table) {
            $table->unsignedInteger('sub_application_id')->default(0)->after('created_from_module');
            $table->unsignedInteger('state_id')->default(0)->after('sub_application_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('document', function (Blueprint $table) {
            $table->dropColumn(['sub_application_id', 'state_id']);
        });
    }
}
