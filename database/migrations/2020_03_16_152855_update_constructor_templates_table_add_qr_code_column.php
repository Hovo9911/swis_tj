<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateConstructorTemplatesTableAddQrCodeColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('constructor_templates', function (Blueprint $table) {
            $table->smallInteger('qr_code')->nullable()->default(0);
            $table->smallInteger('authorization_without_login')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_templates', function (Blueprint $table) {
            $table->dropColumn(['qr_code', 'authorization_without_login']);
        });
    }
}
