<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateSafResponsibilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('saf_responsibilities', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('saf_number');
            $table->unsignedInteger('subtitle_id');
            $table->string('subtitle_type')->nullable();
            $table->string('document_id')->nullable();
            $table->string('responsibility_type')->nullable();
            $table->string('name')->nullable();
            $table->string('budget_line')->nullable();
            $table->string('responsibility_amd')->nullable();
            $table->string('payment_amd')->nullable();
            $table->string('balance_amd')->nullable();
            $table->adminInfo();
            $table->showStatus();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saf_responsibilities');
    }
}
