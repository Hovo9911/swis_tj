<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddNotificationFieldsToConstructorMappingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_mapping', function (Blueprint $table) {
            $table->integer('applicant')->nullable()->after('mandatory_list');
            $table->integer('importer_exporter')->nullable()->after('applicant');
            $table->integer('agency_admin')->nullable()->after('importer_exporter');
            $table->integer('persons_in_next_state')->nullable()->after('agency_admin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('constructor_mapping', function (Blueprint $table) {
            $table->dropColumn(['applicant','importer_exporter', 'agency_admin', 'persons_in_next_state']);
        });
    }
}
