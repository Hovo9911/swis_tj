<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateCalendarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('calendar', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agency_id')->default('0');
            $table->year('year');
            $table->dateTime('date_from');
            $table->dateTime('date_to');
            $table->adminInfo();
            $table->showStatus();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendar');
    }
}
