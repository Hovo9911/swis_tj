<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateBrokersTableAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('broker', function (Blueprint $table) {
            $table->string('legal_entity_name')->after('certification_period_validity')->nullable();
        });

        $schemaBuilder->table('broker_ml', function (Blueprint $table) {
            $table->dropColumn('legal_entity_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('broker', function (Blueprint $table) {
            $table->dropColumn('legal_entity_name');
        });

        $schemaBuilder->table('broker_ml', function (Blueprint $table) {
            $table->string('legal_entity_name')->after('name')->nullable();
        });
    }
}
