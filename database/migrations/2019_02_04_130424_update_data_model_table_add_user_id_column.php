<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateDataModelTableAddUserIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('data_model_excel', function (Blueprint $table) {
            $table->string('user_id')->nullable()->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('data_model_excel', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
    }
}
