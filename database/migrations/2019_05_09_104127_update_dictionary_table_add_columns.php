<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;

class UpdateDictionaryTableAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('dictionary', function (\App\Migration\Blueprint $table) {
            $table->adminInfo();
            $table->showStatus();
            $table->timestamps();
        });


        $schemaBuilder->table('dictionary_ml', function (\App\Migration\Blueprint $table) {
            $table->adminInfo();
            $table->showStatus();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
