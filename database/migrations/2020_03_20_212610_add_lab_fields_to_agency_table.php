<?php

use Illuminate\Support\Facades\DB;
use App\Migration\Blueprint;
use App\Migration\Migration;

class AddLabFieldsToAgencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('agency', function (Blueprint $table) {
            $table->string('certificate_number',255)->nullable();
            $table->string('laboratory_code',12)->nullable();
            $table->dateTime('certification_period_validity')->nullable();
        });

        DB::table("laboratory_indicator")->truncate();
        DB::table("laboratory")->truncate();
        DB::table("laboratory_ml")->truncate();
        DB::table("laboratory_users")->truncate();

        $schemaBuilder->table('laboratory_indicator', function (Blueprint $table) {
            $table->dropColumn('date_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('agency', function (Blueprint $table) {
            $table->dropColumn('certificate_number');
            $table->dropColumn('laboratory_code');
            $table->dropColumn('certification_period_validity');
        });

        $schemaBuilder->table('laboratory_indicator', function (Blueprint $table) {
            $table->dateTime('date_time')->nullable();
        });
    }
}
