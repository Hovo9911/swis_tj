<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddObligationIdToTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('transactions', function (Blueprint $table) {
            $table->integer('obligation_id')->nullable();
        });

        $schemaBuilder->table('saf_obligations', function (Blueprint $table) {
            $table->boolean('is_paid')->nullable()->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('transactions', function (Blueprint $table) {
            $table->dropColumn('obligation_id');
        });


        $schemaBuilder->table('saf_obligations', function (Blueprint $table) {
            $table->boolean('is_paid')->nullable()->default(false);
        });
    }
}
