<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateUsersTableAddPasswordChangedAtField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('users', function (Blueprint $table) {
            $table->timestamp('password_changed_at')->nullable();
            $table->tinyInteger('password_need_change')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('users', function (Blueprint $table) {
            $table->dropColumn('password_changed_at');
            $table->dropColumn('password_need_change');
        });
    }
}
