<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddLabTypeToTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('saf_sub_applications', function (Blueprint $table) {
            $table->boolean('is_lab')->default(false);
            $table->integer('lab_type')->nullable();
            $table->json('indicators')->nullable();
            $table->integer('from_subapplication_id_for_labs')->nullable();
            $table->string('send_lab_company_tax_id')->nullable();
        });

        $schemaBuilder->table('saf_sub_application_products', function (Blueprint $table) {
            $table->string('sample_quantity')->nullable();
            $table->string('sample_measurement_unit')->nullable();
            $table->string('sample_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('saf_sub_applications', function (Blueprint $table) {
            $table->dropColumn('is_lab');
            $table->dropColumn('lab_type');
            $table->dropColumn('indicators');
            $table->dropColumn('from_subapplication_id_for_labs');
            $table->dropColumn('send_lab_company_tax_id');
        });

        $schemaBuilder->table('saf_sub_application_products', function (Blueprint $table) {
            $table->dropColumn('sample_quantity');
            $table->dropColumn('sample_measurement_unit');
            $table->dropColumn('sample_number');
        });
    }
}
