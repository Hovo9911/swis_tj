<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateSafDocumentsAddCompanyTaxIdAndStateIdColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_documents', function (Blueprint $table) {
            $table->string('company_tax_id')->after('user_id')->default('0');
            $table->unsignedInteger('state_id')->after('products')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_documents', function (Blueprint $table) {
            $table->dropColumn('company_tax_id');
            $table->dropColumn('state_id');
        });
    }
}
