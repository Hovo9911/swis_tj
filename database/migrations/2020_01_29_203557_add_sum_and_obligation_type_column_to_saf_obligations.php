<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddSumAndObligationTypeColumnToSafObligations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_notes', function (Blueprint $table) {
            $table->integer('obligation_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_notes', function (Blueprint $table) {
            $table->dropColumn('obligation_id');
        });
    }
}
