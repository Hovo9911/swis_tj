<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateStoreNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('company_tax_id')->default('0');
            $table->integer('sub_application_id')->default(0);
            $table->text('email_notification')->nullable();
            $table->text('in_app_notification')->nullable();
            $table->text('sms_notification')->nullable();
            $table->boolean('read')->default('0');
            $table->showStatus();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}