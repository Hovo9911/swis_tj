<?php

use App\Migration\Blueprint;
use App\Migration\Migration;
use Illuminate\Support\Facades\Schema;


class CreateNotificationsMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->create('notifications_ml', function (Blueprint $table) {
            $table->unsignedInteger('notification_id')->index();
            $table->unsignedSmallInteger('lng_id');
            $table->text('in_app_notification')->nullable();
            $table->showStatus();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications_ml');
    }
}
