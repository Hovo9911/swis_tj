<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddStateIdToSafObligationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_obligations', function (Blueprint $table) {
            $table->integer('state_id')->nullable()->after('saf_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_obligations', function (Blueprint $table) {
            $table->dropColumn('state_id');
        });
    }
}
