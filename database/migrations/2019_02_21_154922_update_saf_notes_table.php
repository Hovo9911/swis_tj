<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateSafNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_notes', function (Blueprint $table) {
            $table->string('trans_key')->after('creator_user_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf_notes', function (Blueprint $table) {
            $table->dropColumn('trans_key');
        });
    }
}
