<?php

use Illuminate\Support\Facades\Schema;
use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateAgencyMlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->create('agency_ml', function (Blueprint $table) {
            $table->unsignedInteger('agency_id');
            $table->unsignedSmallInteger('lng_id');
            $table->string('name',255);
            $table->text('description')->nullable();
            $table->showStatus();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agency_ml');
    }
}
