<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateNotificationsTableAddNotificationTypeColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('notifications', function (Blueprint $table) {
            $table->string('notification_type')->after('read');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('notifications', function (Blueprint $table) {
            $table->dropColumn('notification_type');
        });
    }
}
