<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateAgencyTableAddAllowSendOtherAgencyColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('agency', function (Blueprint $table) {
            $table->tinyInteger('allow_send_sub_applications_other_agency')->nulalble()->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('agency', function (Blueprint $table) {
            $table->dropColumn('allow_send_sub_applications_other_agency');
        });
    }
}
