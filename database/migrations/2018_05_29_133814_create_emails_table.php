<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emails', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('priority')->default(0);
            $table->string('to');
            $table->string('to_name');
            $table->string('from');
            $table->string('from_name');
            $table->string('reply_to');
            $table->string('reply_to_name');
            $table->text('cc');
            $table->text('bcc');
            $table->string('subject', 1024);
            $table->text('body');
            $table->text('attachments');
            $table->timestamp('add_date');
            $table->timestamp('sent_date')->nullable()->default(null);
            $table->enum('status',[
                App\Mailer\Email::STATUS_PENDING,
                App\Mailer\Email::STATUS_FAILED,
                App\Mailer\Email::STATUS_SENT,
                App\Mailer\Email::STATUS_TRY_LATER
            ])->default(App\Mailer\Email::STATUS_PENDING);
            $table->timestamps();
            $table->index('status');
            $table->text('log')->nullable();
            $table->index('priority');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emails');
    }
}
