<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class CreateRegionalOfficeConstructorDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->create('regional_office_constructor_documents', function (Blueprint $table) {
            $table->unsignedInteger('regional_office_id');
            $table->string('constructor_document_code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('regional_office_constructor_documents', function (Blueprint $table) {
            $table->dropColumn('return_date');
        });
    }
}
