<?php

use App\Migration\Migration;
use Illuminate\Support\Facades\DB;

class ChangeDataColumnTypeToJson extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `saf` MODIFY `data` json;');
        DB::statement('ALTER TABLE `saf` MODIFY `custom_data` json;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE `saf` MODIFY `data`  text;');
        DB::statement('ALTER TABLE `saf` MODIFY `custom_data`  text;');
    }
}
