<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddTransKeyToDocumentsDatasetFromSafTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('documents_dataset_from_saf', function (Blueprint $table) {
            $table->string('xml_field_name')->after('saf_field_id');
            $table->string('title_trans_key')->after('xml_field_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('documents_dataset_from_saf', function (Blueprint $table) {
            $table->dropColumn(['xml_field_name', 'title_trans_key']);
        });
    }
}
