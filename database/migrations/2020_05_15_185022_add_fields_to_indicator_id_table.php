<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class AddFieldsToIndicatorIdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('saf_sub_applications_laboratory', function (Blueprint $table) {
            $table->string('price')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();

        $schemaBuilder->table('saf_sub_applications_laboratory', function (Blueprint $table) {
            $table->dropColumn('price');
        });
    }
}
