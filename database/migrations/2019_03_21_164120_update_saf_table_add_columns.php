<?php

use App\Migration\Blueprint;
use App\Migration\Migration;

class UpdateSafTableAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf', function (Blueprint $table) {
            $table->string('creator_company_tax_id')->nullable()->after('user_id');
            $table->enum('importer_type',['tax_id','id_card','f_citizen'])->nullable()->after('creator_user_id');
            $table->string('importer_value')->nullable()->after('importer_type');
            $table->enum('exporter_type',['tax_id','id_card','f_citizen'])->nullable()->after('importer_value');
            $table->string('exporter_value')->nullable()->after('exporter_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $schemaBuilder = $this->getSchemaBuilder();
        $schemaBuilder->table('saf', function (Blueprint $table) {
            $table->dropColumn(['importer_type', 'importer_value' , 'exporter_type' ,'exporter_value','creator_company_tax_id']);
        });
    }
}
