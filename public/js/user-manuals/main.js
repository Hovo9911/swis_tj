/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        drawCallback: function () {
            $(".btn-remove").click(function () {
                $userManuals.deleteRow([$(this).data('id')])
            });
        },
        // "order": [[0, "desc"], [1, "desc"]],
        "order": [],
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
        lengthChange: true,
        iDisplayLength: 25
    },

    customOptions: {
        afterInitCallback: function () {
        },
        tableDataPath: 'user-manuals/table',
        searchPath: 'user-manuals',
        actions: {
            edit: {
                render: function (row) {
                    if (row.searchByAccessKey !== true && pageType != 'help') {
                        return '<a href="' + $cjs.admPath('user-manuals/' + row.id + '/edit') + '" class="btn btn-edit" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.edit.button') + '"><i class="fa fa-pen"></i></a>'
                    }
                }
            },
            view: {
                render: function (row) {
                    if (row.searchByAccessKey !== true && pageType != 'help') {
                        return '<a href="' + $cjs.admPath('user-manuals/' + row.id + '/view') + '" class="btn btn-view" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.view.button') + '"><i class="fa fa-eye"></i></a>'
                    }
                }
            },
            custom: {
                render: function (row) {
                    return '<button class="btn-remove" data-id="' + row.id + '" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.delete.button') + '"><i class="fa fa-times"></i></button>';
                }
            }
        },
        columnsRender: {
            type: {
                render: function (row) {
                    return $trans('swis.user_manual_type.' + row.type);
                }
            },
            url: {
                render: function (row) {
                    return (row.url) ? '<a target="_blank" href="' + row.url + '" >' + row.url + '</a>' : '';
                }
            },
            file: {
                render: function (row) {
                    return (row.file) ? '<a target="_blank" href="' + row.filePath + '" >' + row.fileName + '</a>' : '';
                }
            },
            creator_user_id: {
                render: function (row) {
                    return (row.creator_user_id != 0) ? row.first_name + ' ' + row.last_name + '(' + $trans('swis.authorized_ssn') + ' ' + row.ssn + ')' : '';
                }
            }
        }
    }
};

/*
 * Options for Form Request
 */
var optionsForm = {
    deletePath: '/user-manuals/delete',
    searchPath: '/user-manuals',
    addPath: '/user-manuals/create',
};

/*
 * Init Datatable, Form
 */
var $userManualsTable = new DataTable('list-data', optionsTable);
var $userManuals = new FormRequest('form-data', optionsForm);

$(document).ready(function () {

    // init
    $userManualsTable.init();
    $userManuals.init();

    // ---------

    radioLoggedIn();

    checkRoleType();
});

function radioLoggedIn() {
    $('input[type=radio][name=available_without_auth]').change(function () {
        if ($(this).val() == 1) {
            $('.roles').hide();

            select2Reset($("#roles"))
            select2Reset($("#agency"))

        } else if ($(this).val() == 0) {
            $('.roles').show();
        }
    });
}

function checkRoleType() {
    $('#roles').change(function (e) {

        var selected = $(e.target).val();
        var roleTypeAgency = '2';

        if ($.inArray(roleTypeAgency, selected) > -1) {
            $('.agencies').removeClass('hide');
        } else {
            $('.agencies').addClass('hide');
        }
    });
}
