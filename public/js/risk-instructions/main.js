/*
 * Options for Form Request
 */
var optionsForm = {
    searchPath: '/risk-instructions',
    addPath: '/risk-instructions/create',
    afterGetErrorResult: function (result) {},
};

/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        drawCallback: function () {},
        "order": [[1, "desc"]],
        lengthChange: true,
        iDisplayLength: 25,
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
    },
    customOptions: {
        afterInitCallback: function () { },
        tableDataPath: 'risk-instructions/table',
        searchPath: 'risk-instructions',
    }
};

/*
 * Init Datatable, Form
 */
var $riskInstructions = new FormRequest('form-data', optionsForm);
var $riskInstructionsTable = new DataTable('list-data', optionsTable);

$(document).ready(function () {

    // init
    $riskInstructions.init();
    $riskInstructionsTable.init();

});