/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching:false,
        drawCallback: function ( data ) {
            if (typeof data.json != 'undefined' && data.json.total) {
                if (parseFloat(data.json.total) > 0) {
                    $('#payAllObligations').prop('disabled', false)
                }
                $('#allTotalPrice').text(data.json.total);
            }
        },
        fnRowCallback: function (nRow) {
            $('td:eq(1)', nRow).addClass('text-center');
            $('td:eq(2)', nRow).addClass('text-center');
        },
        order: [],
        lengthChange: true,
        iDisplayLength: 25
    },
    customOptions: {
        afterInitCallback: function (data) {},
        tableDataPath:'payment-and-obligations/table',
        searchPath:'payment-and-obligations',
        columnsRender: {
            print: {
                render: function (row) {
                    if (row) {
                        return '<a href="' + $cjs.admPath('/payment-and-obligations/generatePdf/' + row.id + '/' + row.pdf_hash_key) +'" target="_blank" ><button class="btn--icon" type="button" data-toggle="tooltip"  title="' + $trans('swis.base.tooltip.print.button') + '"><i class="fas fa-print"></i></button></a>'
                    }
                }
            },
            balance: {
                render: function (row) {
                    if (row) {
                        return parseFloat(row.balance ? row.balance : 0).toFixed(2)
                    }
                }
            },
            sub_application_number: {
                render: function (row) {
                    if (row) {
                        if (row.is_saf_application_link) {
                            return '<a href="' + row.application_link + '" target="_blank">'+row.sub_application_number+'</a>';
                        } else {
                            return '<p>'+row.sub_application_number+'</p>';
                        }
                    }
                }
            },
            saf_number: {
                render: function (row) {
                    if (row) {
                        if (row.is_saf_link) {
                            return '<a href="'+row.saf_link+'" target="_blank">'+row.saf_number+'</a>';
                        } else {
                            return '<p>'+row.saf_number+'</p>';
                        }
                    }
                }
            },
            saf_obligation_receipt: {
                render: function (row) {
                    if (row) {
                        if (row.saf_obligation_receipt) {
                            return '<a target="_blank" href="' + '/files/uploads/files/'+row.saf_obligation_receipt + '" class="btn btn-view" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.view.button') + '"><i class="fa fa-eye"></i></a>'
                        }
                        return '';
                    }
                }
            },
        }
    }
};

/*
 * Options for Form Request
 */
var optionsForm = {
    searchPath: '/payment-and-obligations',
};

/*
 * Init Datatable, Form
 */
var $paymentAndObligationsTable = new DataTable('list-data', optionsTable);
var $paymentAndObligations = new FormRequest('form-data', optionsForm);

//
var payObligationModal = $('#PayObligationModal');

$(document).ready(function () {

    //init
    $paymentAndObligationsTable.init();
    $paymentAndObligations.init();

    // --------

    checkCheckBoxOnTd();

    payObligations();

    disablePayDateField();

    checkDecimalNumber();

    payAllObligations();

    handleSearch();

});

function checkCheckBoxOnTd() {
    $(document).on('click', '.obligation', function () {
        setTotalObligationPrice($(this));
    });
}

function setTotalObligationPrice(element) {
    var amount = parseFloat(element.data('amount'));
    var total = parseFloat($('#totalPrice').data('value')) ? parseFloat($('#totalPrice').data('value')) : 0;

    total = (element.is(':checked')) ? total + amount : total - amount;
    total = parseFloat(total).toFixed(2);

    $('#pay-obligation-modal').prop('disabled', !($('.obligation:checked').length > 0));
    $('#totalPrice').data('value', total).text(total + ' ' + defaultCurrency);
}

function payObligations() {
    $('#pay-obligation-modal').on('click', function (event) {
        event.preventDefault();
        var data = [];
        $('button').prop('disabled', true);

        $('.obligation:checked').each(function (index, value) {
            data.push($(value).data('id'));
        });

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('payment-and-obligations/pay-obligations-modal'),
            data: {data: JSON.stringify(data)},
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    payObligationModal.html('');
                    payObligationModal.append(result.data.html);
                    payObligationModal.modal('show');
                }
                $('button').prop('disabled', false);
            }
        });
    });

    $(document).on('click', '#pay-obligation', function (event) {
        event.preventDefault();
        $('button').prop('disabled', true);

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('payment-and-obligations/pay-obligations'),
            data: {sum: $(this).data('sum'), obligations: $(this).data('obligations')},
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    var backToUrl = result.data.backToUrl;
                    if (backToUrl) {
                        window.location.href = backToUrl;
                        if (backToUrl.indexOf('#') !== -1) {
                            location.reload();
                        }
                    }
                } else {
                    toastr.error($trans.get('core.loading.invalid_data'));
                }
                payObligationModal.modal('hide');
                $('button').prop('disabled', false);
            }
        })
    });

    $(document).on('click', '#refile', function (event) {
        $(this).prop('disabled', true)
        event.preventDefault();

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('/payments/getDynamicParams'),
            data: {order_id: $('#orderId').val(), amount: $('#amountField').val()},
            dataType: 'json',
            success: function (result) {
                if (result.status == 'OK') {
                    $('#amountField').val(parseFloat($('#amountField').val()).toFixed(2))
                    $('#token').val(result.data.token);
                    $('#orderId').val(result.data.orderId);
                    $('#refile').prop('disabled', false);
                    $('#form-data-refile').submit();
                }
            }
        });

    });
}

function disablePayDateField() {
    $('.obligation_pay').on('change', function () {
        if ($(this).val() > 0) {
            $('.payment_date_row').find('input').prop('disabled', false);
            $('.balance').val('').prop('disabled', true);
        } else {
            $('.payment_date_row').find('input').val('').prop('disabled', true);
            $('.balance').prop('disabled', false);
        }

    });
}

function checkDecimalNumber() {
    $('.balance').on('change', function () {
        var v = parseFloat($(this).val());
        $(this).val((isNaN(v)) ? '' : v.toFixed(2));
    })
}

function payAllObligations() {
    $('#payAllObligations').on('click', function () {

        var self = $(this);
        var data = $('#search-filter-main').serializeArray();

        self.prop('disabled', true);

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('payment-and-obligations/table-total'),
            data: data,
            dataType: 'json',
            success: function (result) {

                if (result.status === 'OK') {
                    payObligationModal.html('');
                    payObligationModal.append(result.data.html);
                    payObligationModal.modal('show');
                }

                self.prop('disabled', false);
            }
        });

    });
}

function handleSearch() {
    $('#search-filter').on('submit', function () {
        $('#pay-obligation-modal').prop('disabled', !($('.obligation:checked').length > 0));
        $('#totalPrice').data('value', 0).text( "0.00 " + defaultCurrency);
    });
}