/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
        "order": [[1, "desc"]],
        lengthChange: true,
        iDisplayLength: 25
    },

    customOptions: {
        afterInitCallback: function () {},
        tableDataPath: 'role/table',
        searchPath: 'role',
    }
};

/*
 * Options for Form Request
 */
var optionsForm = {
    searchPath: '/role',
    addPath: '/role/create',
};

/*
 * Init Datatable, Form
 */
var $roleTable = new DataTable('list-data', optionsTable);
var $role = new FormRequest('form-data', optionsForm);

$(document).ready(function () {

    // init
    $roleTable.init();
    $role.init();

});


