/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        drawCallback: function () {
            $(".btn-remove").click(function () {
                $notificationTemplates.deleteRow([$(this).data('id')])
            });
        },
        "order": [[1, "desc"]],
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
    },

    customOptions: {
        afterInitCallback: function () { },
        tableDataPath: 'global-notifications/table',
        searchPath: 'global-notifications',
        actions: {
            edit: {
                render: function (row) {
                    return '<a href="' + $cjs.admPath('global-notifications/' + row.id + '/edit') + '" class="btn btn-edit" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.edit.button') + '"><i class="fa fa-pen"></i></a>'
                }
            },
            view: {
                render: function (row) {
                    return '<a href="' + $cjs.admPath('global-notifications/' + row.id + '/view') + '" class="btn btn-view" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.view.button') + '"><i class="fa fa-eye"></i></a>'
                }
            },
            custom: {
                render: function (row) {
                    if (!row.locked) {
                        return '<a  data-toggle="tooltip" data-id="' + row.id + '" class="btn btn-edit globalNotificationModal"  ' +
                            'title="' + $trans('swis.base.tooltip.send_notification.button') + '"><i class="fa fa-send"></i></a>'
                    }
                }
            },
            clone: {
                render: function (row) {
                    return '<a href="' + $cjs.admPath('global-notifications/' + row.id + '/clone') + '" class="btn btn-success btn-xs btn--icon"  data-toggle="tooltip" title="' + $trans('swis.base.tooltip.clone.button') + '"><i class="fas fa-copy"></i></a>'
                }
            },
            delete: {
                render: function (row) {
                    if (!row.locked) {
                        return '<button class="btn-remove" data-id="' + row.id + '" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.delete.button') + '"><i class="fa fa-times"></i></button>';
                    }
                }
            }
        },
    }
};

/*
 * Options for Form Request
 */
var optionsForm = {
    deletePath: '/global-notifications/delete',
    searchPath: '/global-notifications',
    addPath: '/global-notifications/create',
};

/*
 * Init Datatable, Form
 */
var CK_INSTANCES;
var $notificationTemplatesTable = new DataTable('list-data', optionsTable);
var $notificationTemplates = new FormRequest('form-data', optionsForm);
var $notificationModal = $('#notificationModal');

$(document).ready(function () {

    // Init Pat
    $notificationTemplatesTable.init();
    $notificationTemplates.init();

    // ---------------

    ckInstancedList()

    checkRoleType();

    sendNotification();

    closeModal();

});

function checkRoleType() {
    $('.user_type').change(function () {

        var userType = $("input[type='radio']:checked").val();
        var agencies = $('.agencies');
        var roleTypeAgency = 'agency';

        if (userType == roleTypeAgency) {
            agencies.removeClass('hide');
        } else {
            agencies.addClass('hide');
        }
    });
}

function sendNotification() {

    var notificationTemplateId = '';
    var successAlert = $('#successAlert');

    $(document).on("click", '.globalNotificationModal', function (e) {

        e.preventDefault();

        var self = $(this);

        $notificationModal.modal()
        notificationTemplateId = self.data("id")

    });

    $('.sendNotification').click(function (e) {

        e.preventDefault();

        var self = $(this);
        var userType = $("input[type='radio']:checked").val();
        var agencies = $('#agencies').val();

        successAlert.html('');

        spinnerLoaderForButton(self);

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('global-notifications/send-global-notification'),
            data: {notificationTemplateId: notificationTemplateId, userType: userType, agencies: agencies},
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {

                    successAlert.html('<div class="alert alert-success">' + $trans('swis.global_notifications.send_status.success') +
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
                        '    <span aria-hidden="true">&times;</span>\n' +
                        '  </button>' + '</div> ');

                    spinnerLoaderForButton(self, true);

                }
            }
        });
    });
}

function closeModal() {
    $notificationModal.on('hide.bs.modal', function () {
        var successAlert = $('#successAlert');

        successAlert.html('');

        $('#all_users').prop('checked', true);
        $('#agencies').val('').trigger('change');
        $('.agencies').addClass('hide');

    });
}

function ckInstancedList() {
    if (typeof languageList !== 'undefined') {
        CK_INSTANCES = [];

        $.each(languageList, function (k, v) {
            ClassicEditor
                .create(document.querySelector('#ml_' + v + '_editor'))
                .then(editor => {
                    CK_INSTANCES[editor.sourceElement.getAttribute('name')] = editor;
                })
                .catch(error => {
                    console.error(error);
                });
        })
    }
}




