
/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        drawCallback: function () {
            $(".btn-remove").click(function () {
                $reportsAccess.deleteRow([$(this).data('id')])
            });
        },
        "order": [[2, "desc"]],
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
            $('td:eq(1)', nRow).addClass('text-center');
        },
    },
    customOptions: {
        afterInitCallback: function () {
        },
        tableDataPath: 'reports-access/table',
        searchPath: 'reports-access',
        actions: {
            edit: {
                render: function (row) {
                    if (row.canEdit !== false) {
                        return '<a href="' + $cjs.admPath('reports-access/' + row.id + '/edit') + '" class="btn btn-edit" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.edit.button') + '"><i class="fa fa-pen"></i></a>'
                    }
                }
            },
            view: {
                render: function (row) {
                   return '<a href="' + $cjs.admPath('reports-access/' + row.id + '/view') + '" class="btn btn-view" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.view.button') + '"><i class="fa fa-eye"></i></a>'
                }
            },

            custom: {
                render: function (row) {
                    return '<button class="btn-remove" data-id="' + row.id + '" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.delete.button') + '"><i class="fa fa-times"></i></button>';
                }
            }
        }
    }
};

/*
 * Options for Form Request
 */
var optionsForm = {
    deletePath: '/reports-access/delete',
    searchPath: '/reports-access',
    addPath: '/reports-access/create',
};

/*
 * Init Datatable, Form
 */
var $reportsAccessTable = new DataTable('list-data', optionsTable);
var $reportsAccess = new FormRequest('form-data', optionsForm);

$(document).ready(function () {

    // init
    $reportsAccessTable.init();
    $reportsAccess.init();

    checkRoleType();

});

function checkRoleType() {
    $('input[type=radio][name=role_type]').change(function (e) {

        var selectedroleType = $(e.target).val();


        if (selectedroleType == '2') {
            $('.agencies').removeClass('hide');
        } else {
            $('.agencies').addClass('hide');
        }
    });
}
