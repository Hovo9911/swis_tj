let today = new Date();

/*
 * Options for Form calendar
 */
let options = {
    viewDateOptions: {
        year: currentYear,
        today: today,
        currentYear: today.getFullYear(),
        nonWorkingDays: currentYearNonWorkingDays,
        allWeekends: [],
        language:language
    },
};

/*
 * Options for Form calendar
 */
let options2 = {
    viewDateOptions: {
        year: nextYear,
        today: today,
        currentYear: today.getFullYear(),
        nonWorkingDays: nextYearNonWorkingDays,
        allWeekends: [],
        language:language
    },
};

// Calendar
let calendar = new Calendar('calendar-1', options);
let calendar2 = new Calendar('calendar-2', options2);

$(document).ready(function () {

    // init
    calendar.initCalendar();
    calendar2.initCalendar();

});


