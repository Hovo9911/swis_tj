/*
 * Options for Form Request
 */
var changePasswordBtn = $('.change--password');
var optionsForm = {
    customOptions: {
        onSaveError: function (result) {
            spinnerLoaderForButton(changePasswordBtn, true);
        }
    },
    customSaveFunction: function () {
        spinnerLoaderForButton(changePasswordBtn);
        return true;
    },
};

/*
 * Init Form
 */
var changePassword = new FormRequest('form-data', optionsForm);

$(document).ready(function () {

    // init
    changePassword.init();
});
