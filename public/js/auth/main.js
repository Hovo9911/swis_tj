/*
 * Options for Form Request
 */
var loginBtn = $('.btn--login');
var saveBtn = $('.save--password');
var resetBtn = $('.reset--password');

var optionsForm = {
    customOptions: {
        onSaveError: function (result) {
            spinnerLoaderForButton(loginBtn, true);
            spinnerLoaderForButton(saveBtn, true);
            spinnerLoaderForButton(resetBtn, true);
        }
    },
    customSaveFunction: function () {
        spinnerLoaderForButton(loginBtn);
        spinnerLoaderForButton(saveBtn);
        spinnerLoaderForButton(resetBtn);
        return true;
    },
};

// Init Form
var loginEmail = new FormRequest('form-data', optionsForm);

$(document).ready(function () {

    // init
    loginEmail.initForm();
});