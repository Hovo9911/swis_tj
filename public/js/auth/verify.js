// Get Verification sms
var verify = new FormRequest('form-data', []);

verify.initEditPage = function () {

    verify.initForm();

    // verify.initShowStatusReq();

};

// Activation Code
var $activationCode = new FormRequest('form-data-number', []);

$activationCode.initEditPage = function () {

    $activationCode.initForm();

    // $activationCode.initShowStatusReq();

};

$(document).ready(function () {

    verify.init();

    $activationCode.init();

    sendAgainCode();
});

// Activation code send again
function sendAgainCode() {
    $('#sendAgain').click(function () {

        var url = $(this).data('url');

        $('.btn').addClass('disabled');

        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            success: function (result) {

                toastr.options.preventDuplicates = true;

                if (result.status == 'OK') {

                    var bactToUrl = result.data.backToUrl;

                    if (bactToUrl) {

                        toastr.options.onShown = function () {
                            window.location.href = bactToUrl;
                        };

                        toastr.success($trans.get('core.loading.saved'));
                    }
                } else if (result.status == 'INVALID_DATA') {
                    if (result.errors) {
                        $error.show('form-error', result.errors);
                    }

                    // $.winStatus( $trans.get( 'core.loading.invalid_data' ) , true , null, true  );
                    toastr.error($trans.get('core.loading.invalid_data'));
                }

                $('.btn').removeClass('disabled').removeAttr('disabled');
            }

        });
    });
}