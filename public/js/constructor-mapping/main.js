/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        order: [[1, "desc"]],
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
        lengthChange: true,
        pageLength: 100
    },
    customOptions: {
        afterInitCallback: function () {
        },
        tableDataPath: 'constructor-mapping/table',
        searchPath: 'constructor-mapping',
        columnsRender: {
            non_visible_list: {
                render: function (row) {
                    if (row.non_visible_list) {
                        return '<a target="_blank" href="' + $cjs.admPath('constructor-lists/' + row.non_visible_list + '/edit') + '"><p title="' + row.nonVisibleListToolTip + '">' + row.non_visible_list + '</p></a>';
                    }

                    return '';
                }
            },
            editable_list: {
                render: function (row) {
                    if (row.editable_list) {
                        return '<a target="_blank" href="' + $cjs.admPath('constructor-lists/' + row.editable_list + '/edit') + '"><p title="' + row.editableListToolTip + '">' + row.editable_list + '</p></a>';
                    }

                    return '';
                }
            },
            mandatory_list: {
                render: function (row) {
                    if (row.mandatory_list) {
                        return '<a target="_blank" href="' + $cjs.admPath('constructor-lists/' + row.mandatory_list + '/edit') + '"><p title="' + row.mandatoryListToolTip + '">' + row.mandatory_list + '</p></a>';
                    }

                    return '';
                }
            }

        },
    }
};

/*
 * Options for Form Request
 */
var optionsForm = {
    searchPath: '/constructor-mapping/' + constructorDocumentId,
    addPath: '/constructor-mapping/create/' + constructorDocumentId,
};

/*
 * Init Datatable, Form
 */
var $constructorMappingTable = new DataTable('list-data', optionsTable);
var $constructorMapping = new FormRequest('form-data', optionsForm);

$(document).ready(function () {

    // init
    $constructorMapping.init();
    $constructorMappingTable.init();

    // --------

    constructorDocumentList();

});

function constructorDocumentList() {
    $('.document-list').on('change', function () {
        window.location.href = '/' + $cLng + '/constructor-mapping/' + $(this).val();
    });
}
