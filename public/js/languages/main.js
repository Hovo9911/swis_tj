/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching:false,
        drawCallback:function(){
            $(".btn-remove").click(function () {
                $languages.deleteRow([$(this).data('id')])
            });
        },
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
            $('td:eq(1)', nRow).addClass('text-center');
        },
        "order": [[2, "asc"]],
        lengthChange: true,
        iDisplayLength: 25
    },

    customOptions: {
        afterInitCallback: function () {},
        tableDataPath:'language/table',
        searchPath:'language',
        hiddens: typeof activeLng !='undefined' ? activeLng : ''
    }
};

/*
 * Options for Form Request
 */
var optionsForm = {
    tableId: 'list-data',
    deletePath: '/language/delete',
    searchPath: '/language',
    addPath: '/language/create',
};

/*
 * Init Datatable, Form
 */
var $languagesTable = new DataTable('list-data', optionsTable);
var $languages = new FormRequest('form-data', optionsForm);

$(document).ready(function () {

    // init
    $languagesTable.init();
    $languages.init();

});

