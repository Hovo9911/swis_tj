/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
            $('td:eq(2)', nRow).addClass('text-center');
        },
        "order": [[1, "desc"]],
        lengthChange: true,
        iDisplayLength: 25
    },
    customOptions: {
        afterInitCallback: function () {
        },
        tableDataPath: 'agency/table',
        searchPath: 'agency',
        columnsRender: {
            logo: {
                render: function (row) {
                    if (row.logo) {
                        return '<img style="max-height: 30px;" src="/images/agency/' + row.logo + '" />';
                    }
                    return ''
                }
            },
        }
    }
};

/*
 * Options for Form Request
 */
var optionsForm = {
    searchPath: '/agency',
    addPath: '/agency/create',
};

/*
 * Modal Options , Removing current logo
 */
var optionsModal = {
    headerText: $trans('core.base.delete.empty_message.title'),
    bodyContent: function () {
        return $trans('core.base.delete.confirm_text')
    },
    footerButtons: {
        buttons: {
            remove: {
                click: function (event) {
                    $('.current-logo').slideUp(function () {
                        $('#delete-logo').modal('hide');
                    });

                    $('input[name="logo"]').val('');
                },
                title: $trans('core.base.button.delete'),
                class: 'btn-delete-logo'
            }
        }
    }
};

// Init Datatable, Form, Modal
var $agencyTable = new DataTable('list-data', optionsTable);
var $agency = new FormRequest('form-data', optionsForm);
var $agencyModal = new Modal('delete-logo', optionsModal);

//
var sphereTab = $('.sphere-tab');
var mainIndicatorsTable = $('.mainIndicatorsTable');
var indicatorsTableBlock = $('.indicatorsTableBlock');
var addIndicators = $('.addIndicators');
var addIndicatorsModal = $('#addIndicatorsModal');
var searchSphereForm = $('#searchSphereForm');
var agencyIdField = $('#agencyIdField');

$(document).ready(function () {

    // init
    $agencyTable.init();
    $agency.init();
    $agencyModal.init();

    // ---------

    brokerInfo();

    laboratory();
});

function brokerInfo() {

    var legalEntityName = $('#legalEntityName');
    var address = $('#address');
    var taxIdInput = $('#taxID');

    getCompanyInfoByTaxID(taxIdInput, function () {
        legalEntityName.val('');
        address.val('');
    }, function (result) {
        legalEntityName.val(result.data.company.name);
        address.val(result.data.company.address);
    });
}

function laboratory() {

    addIndicatorsModal.on('hidden.bs.modal', function () {
        indicatorsTableBlock.html('');
        searchSphereForm.trigger("reset");
    });

    //
    $('#isLabCheckbox').on('click', function (e) {
        if ($(this).is(':checked')) {
            sphereTab.show();
        } else {
            sphereTab.hide();
        }
    });

    //
    $('.addNewIndicators').on('click', function (e) {
        e.preventDefault();
        addIndicatorsModal.modal('show');
    });

    //
    $('.searchExpertise').on('click', function (e) {
        e.preventDefault();

        var self = $(this);

        self.prop('disabled', true);
        indicatorsTableBlock.html('');

        var data = searchSphereForm.serializeArray().reduce(function (obj, item) {
            obj[item.name] = item.value;
            return obj;
        }, {});

        data['agency'] = agencyIdField.val();

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('agency/get-indicators'),
            data: data,
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    self.prop('disabled', false);

                    indicatorsTableBlock.append(result.data);
                }
            }
        });

        self.prop('disabled', false);
    });

    //
    addIndicators.on('click', function (e) {
        e.preventDefault();

        var data = [];
        $(".lab_indicator_checkbox:checked").each(function (index, value) {
            data.push($(value).data('id'));
        });

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('agency/save-indicators'),
            data: {indicators: data, agency:  agencyIdField.val()},
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {

                    mainIndicatorsTable.html('');
                    mainIndicatorsTable.append(result.data);

                    indicatorsTableBlock.html('');

                    addIndicatorsModal.modal('hide');
                }
            }
        });
    });

    //
    $(document).on('click', '.lab_indicator_checkbox', function () {
        var is_checked = true;
        $(".lab_indicator_checkbox:checked").each(function (index, value) {
            is_checked = false;
        });

        addIndicators.prop('disabled', is_checked);
    });

    //
    $(document).on('click', '.remove-indicators-btn', function (e) {
        e.preventDefault();

        var self = $(this);

        //agencyId means Laboratory id
        $.ajax({
            type: 'POST',
            url: $cjs.admPath('agency/delete-indicators'),
            data: {indicator: self.data('id'),  agencyId:  agencyIdField.val()},
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    self.closest('tr').remove();
                }
            }
        });
    });
}

