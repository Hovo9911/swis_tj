//
var folderIdVal = $('#folderId').val();

/*
 * Options Form DataTable
 */
var optionsDocumentSearchTable = {
    datatableOptions: {
        searching: false,
        drawCallback: function () {
        },
        initComplete: function (settings, json) {
        },
        "order": [[2, "desc"]],
        fnRowCallback: function (nRow, aData) {
            $('td:eq(1)', nRow).addClass('text-center');
        },
    },
    customOptions: {
        afterInitCallback: function (data) {
        },
        tableDataPath: 'folder/document-search/' + folderIdVal,
        columnsRender: {
            document_file: {
                render: function (row) {
                    if (row.document_file) {
                        return '<a target="_blank" href="' + row.document_file + '" class="btn btn-success btn-xs btn--icon"  title="' + $trans('swis.base.tooltip.view.button') + '" data-toggle="tooltip" ><i class="fas fa-eye"></i></a>'
                    }

                    return ''
                }
            },
            id: {
                render: function (row) {
                    if (row.incrementedId) {
                        return "<span class='document-incremented-real-id'>" + row.incrementedId + "</span>";
                    }
                }
            },
            document_form: {
                render: function (row) {
                    if (row.document_form) {
                        return $trans('swis.document.document_form.' + row.document_form + '.title');
                    }
                }
            },
        }
    }
};

/*
 * Init Datatable
 */
var $documentSearch = new DataTable('search-document', optionsDocumentSearchTable);

//
var initDocumentDataTable = true;
var documentList = $('#documentList');
var deleteCheckedDocumentsButton = $("#deleteCheckedDocuments");
var checkAllDocumentsCheckbox = $("#checkAllDocuments");

$(document).ready(function () {

    documentSearch();

    refAutocomplete();

    addDocument();

    deleteDocument();

    renderDocumentInputs();

    storeDocument();

});

function documentSearch() {

    $('.documentSearch').click(function (e) {

        e.preventDefault();

        var self = $(this);
        var documentSearchFilter = $('#documentSearchFilter');
        var documentSearchDiv = $('.searchDoc');

        self.prop('disabled', true);

        if (initDocumentDataTable) {
            initDocumentDataTable = false;
            $documentSearch.init();
        }

        documentSearchDiv.removeClass('hide');
        $documentSearch.mSearchFilterForm = documentSearchFilter;
        $documentSearch.mOptions.customOptions.animateToTable = true
        $documentSearch.reload();

        $('#search-document').on('xhr.dt', function (e, settings, data, xhr) {
            self.prop('disabled', false);
        })
    })
}

function addDocument() {

    var num = 1;
    var documentErrorInfo = $('#documentErrorInfo');

    $('.addDocument').click(function (e) {

        e.preventDefault();

        var self = $(this);
        var tableTrLength = documentList.find('tbody tr').length + 1;
        var selectedDocs = {};
        var searchDocTable = $('#search-document');

        if (tableTrLength > 1 && num < tableTrLength) {
            num = tableTrLength;
        }

        $.each(searchDocTable.find('tbody input:checked'), function () {
            selectedDocs[$(this).closest('tr').find('.document-incremented-real-id').text()] = $(this).val();
        });

        // searchDocTable.find('tbody td input').prop('checked', false).closest('tr.doc-exist').removeClass('doc-exist');
        documentErrorInfo.html('');

        if (Object.keys(selectedDocs).length > 0) {

            self.prop('disabled', true);

            $.ajax({
                type: 'POST',
                url: $cjs.admPath('folder/add-document'),
                data: {
                    documentItems: selectedDocs,
                    num: num,
                    folder_id: folderIdVal
                },
                dataType: 'json',
                success: function (result) {

                    if (result.status === 'OK') {

                        documentList.find('tbody').append(result.data.html);
                        documentList.closest('.table-responsive').removeClass('hide');

                        if (result.data.newDocumentCount) {
                            animateScrollToElement(documentList.find('tbody tr:last'), 0, 200);
                            num++;
                        }

                        if (typeof result.data.error !== 'undefined') {
                            var dataError = result.data.error;

                            documentErrorInfo.html('<div class="alert alert-info">' + dataError.message + '</div>');

                            $.each(dataError.docIds, function (k, v) {
                                searchDocTable.find('input:checkbox[value="' + v + '"]').closest('tr').addClass('doc-exist');
                            })
                        }

                        if (+folderIdVal) {
                            // Reload document datatable
                            $documentSearch.mOptions.customOptions.animateToTable = false;
                            $documentSearch.reload();
                        } else {
                            searchDocTable.find('tbody input:checked').remove();
                        }

                        deleteCheckedDocumentsButton.removeClass('hide');
                    }

                    self.prop('disabled', false);
                }
            });
        }
    });
}

function renderDocumentInputs() {

    $('.plusMultipleFieldsDocument').click(function () {

        var self = $(this);
        var num = +documentList.find('tbody tr:last-child').data('num') + 1;
        num = isNaN(num) ? 1 : num;

        spinnerLoaderForButton(self);

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('folder/render-document-inputs'),
            data: {num: num},
            dataType: 'json',
            success: function (result) {

                if (result.status === 'OK') {

                    documentList.find('tbody').append(result.data.html);
                    documentList.closest('.table-responsive').removeClass('hide');

                    var newTrRow = documentList.find('tr[data-num="' + num + '"]');

                    refAutocomplete();
                    datePickerInit(newTrRow);

                    $gUploader.init();
                    $('.g-upload').not('.has-uploader').each(function () {
                        $gUploader.onX($(this));
                    });

                    spinnerLoaderForButton(self, true);
                    animateScrollToElement(newTrRow, 100, 200);
                }
            }
        });
    })
}

function storeDocument() {

    $(document).on('click', '.storeDocument', function (e) {

        e.preventDefault();

        var holderInfo = $('#holderInfo :input');
        var documentInfo = $(this).closest('tr').find(':input');
        var data = $.merge(holderInfo, documentInfo);
        var self = $(this);
        var selfTr = self.closest('tr');

        spinnerLoaderForButton(self);

        $error.show('form-error', []);

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('folder/store-document/' + folderIdVal),
            data: data,
            dataType: 'json',
            success: function (result) {

                if (result.status === 'OK') {

                    selfTr.find('input').prop('readonly', true);
                    selfTr.find('.document').val(result.data.docId);
                    selfTr.find('.doc-checkbox').val(result.data.docId).removeClass('hidden');
                    selfTr.find('.access-password').html(result.data.accessPassword);
                    selfTr.find('.document-form').html(result.data.documentForm);
                    selfTr.find('.doc-status').html(result.data.docStatus);
                    selfTr.removeClass('not-saved');

                    var downloadBtn = '';
                    if (result.data.documentFile) {

                        downloadBtn = result.data.documentFileName;
                        var viewBtn = '<a target="_blank" href="' + result.data.documentFile + '" class="btn btn-success btn-xs btn--icon" title="' + $trans('swis.base.tooltip.view.button') + '" data-toggle="tooltip" ><i class="fas fa-eye"></i></a>';

                        selfTr.find('.actions-list').prepend(viewBtn);
                    }

                    selfTr.find('.g-upload-container').html(downloadBtn);
                    selfTr.find('.addProductsDocument').attr('data-update', true);
                    selfTr.find('.delete').removeClass("hide");
                    selfTr.find('.viewPDF').removeClass("hide");

                    self.tooltip('destroy');
                    self.remove();

                    deleteCheckedDocumentsButton.removeClass('hide');
                }

                if (result.errors) {

                    var num = selfTr.data('num');
                    var errors = result.errors;

                    var showSideTabErrors = true;
                    $.each(documentInfo, function (k, v) {
                        var inputName = $(this).attr('name');

                        if (inputName !== undefined && errors[inputName] !== undefined) {
                            showSideTabErrors = false;
                            $('#form-error-' + inputName + '-' + num).addClass('form-error-text').html(errors[inputName]).css({'display': 'block'});
                        }
                    });

                    if (showSideTabErrors) {
                        $error.show('form-error', errors);
                    }

                    spinnerLoaderForButton(self, true);
                    self.tooltip('destroy');
                }
            },
            error: function () {
                spinnerLoaderForButton(self, true);
            }
        })

    })
}

function deleteDocument() {

    checkAllDocumentsCheckbox.on("click", function () {
        $(this).closest('table').find("input[type='checkbox']").not('[disabled]').prop("checked", $(this).prop("checked"));
    });

    $(document).on('click', '.delete', function (e) {
        e.preventDefault();

        var self = $(this);
        var selfTr = self.closest('tr');
        var documentId = self.attr('data-id');

        if (typeof documentId != 'undefined' && documentId !== '') {

            modalConfirmDelete(function (confirm) {
                if (confirm) {

                    loadContent();

                    documentsDeleteAjax([documentId])

                } else {
                    self.prop('disabled', false);
                }
            });

        } else {
            selfTr.remove();

            if (!documentList.find('tbody tr').length) {
                deleteCheckedDocumentsButton.addClass('hide');
                documentList.closest('.table-responsive').addClass('hide');
            }
        }
    });

    deleteCheckedDocumentsButton.click(function () {

        var documentIds = documentList.find("tbody input:checkbox:checked").map(function () {
            if ($(this).val()) {
                return $(this).val();
            }
        }).get();

        if (documentIds.length) {

            var self = $(this);
            modalConfirmDelete(function (confirm) {

                if (confirm) {

                    loadContent();

                    spinnerLoaderForButton(self);

                    if (+folderIdVal) {
                        documentsDeleteAjax(documentIds);
                    } else {
                        reloadDocumentTable(documentIds)
                    }
                }
            });
        }

    });
}

function documentsDeleteAjax(documentIds) {

    $.ajax({
        type: 'POST',
        url: $cjs.admPath('folder/delete-document/' + folderIdVal),
        data: {documentIds: documentIds, folderId: folderIdVal},
        dataType: 'json',
        success: function (result) {

            if (result.status === 'OK') {
                reloadDocumentTable(documentIds);
            }
        }
    })

}

function reloadDocumentTable(documentIds) {

    $.each(documentIds, function (k, v) {
        documentList.find('tbody .document[value="' + v + '"]').closest('tr').remove();
    })

    if (!documentList.find('tbody tr').length) {
        deleteCheckedDocumentsButton.addClass('hide');
        documentList.closest('.table-responsive').addClass('hide');
    }

    checkAllDocumentsCheckbox.prop('checked', false);
    spinnerLoaderForButton(deleteCheckedDocumentsButton, true);

    loadContent();
}
