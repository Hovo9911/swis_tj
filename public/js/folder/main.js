var hash = window.location.hash;
hash && $('ul.nav a[href="' + hash + '"]').tab('show');

/*
 * Options for Form Request
 */
var optionsForm = {
    deletePath: '/folder/delete',
    searchPath: '/folder',
    addPath: '/folder/create',
    customSaveFunction: () => saveData(),
};

/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        drawCallback: function () {
            $(".btn-remove").click(function () {
                $folder.deleteRow([$(this).data('id')])
            });
        },
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
            $('td:eq(1)', nRow).addClass('text-center');
        },
        "order": [[2, "desc"]],
        lengthChange: true,
        iDisplayLength: 25

    },
    customOptions: {
        afterInitCallback: function () {
        },
        tableDataPath: 'folder/table',
        searchPath: 'folder',
        actions: {
            edit: {
                render: function (row) {
                    if (row.canEdit !== false) {
                        return '<a href="' + $cjs.admPath('folder/' + row.id + '/edit') + '" class="btn btn-edit" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.edit.button') + '"><i class="fa fa-pen"></i></a>'
                    }
                }
            },
            view: {
                render: function (row) {

                    var viewByAccessKey = '';
                    if (row.searched_by_access_password && row.canEdit === false) {
                        viewByAccessKey = '?accesskey=' + row.folder_access_password;
                    }

                    return '<a href="' + $cjs.admPath('folder/' + row.id + '/view' + viewByAccessKey) + '" class="btn btn-view" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.view.button') + '"><i class="fa fa-eye"></i></a>'
                }
            },
            custom: {
                render: function (row) {
                    if (row.canEdit !== false) {
                        return '<button class="btn-remove" data-id="' + row.id + '" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.delete.button') + '"><i class="fa fa-times"></i></button>';
                    }
                }
            }
        },

    }
};

//
var $formId = 'form-data';

/*
 * Init Datatable, Form
 */
var $folder = new FormRequest('form-data', optionsForm);
var $folderTable = new DataTable('list-data', optionsTable);

$(document).ready(function () {

    // init
    $folder.init();
    $folderTable.init();

    // ------------------

    disableContentFields();

    authUserIsBroker();

    cancelAction();
});

function saveData() {
    var hasNotSavedDocument = documentOpenedNotSaved();

    if (!hasNotSavedDocument) {
        return true;
    }
}

function cancelAction() {
    $(document).on('click', '.cancelAction', function (e) {

        e.preventDefault();

        var self = $(this);
        var selfTr = self.closest('tr');

        selfTr.find('.rowData').removeClass("hide");
        selfTr.find('.editableData').addClass("hide");
        selfTr.find('.cancelAction').addClass("hide");
    });
}

function documentOpenedNotSaved() {

    var storeDocumentButtons = $('.storeDocument');
    var notSaved = false;

    if (storeDocumentButtons.length) {
        notSaved = true;
        $.each(storeDocumentButtons, function (k, v) {
            $(this).closest('tr').addClass('not-saved');
        });

        $('ul.nav a[href="#tab-documents"]').tab('show')
    }

    return notSaved;
}

function authUserIsBroker() {

    if (typeof $saveMode != "undefined" && !$authUserIsBroker) {

        var holderInfoPanel = $("#holderInfo");

        if ($saveMode === $viewModeAdd) {

            $.ajax({
                type: 'POST',
                url: $cjs.admPath('broker/is-broker'),
                dataType: 'json',
                success: function (result) {

                    if (result.status === 'OK') {

                        disableFormInputs(holderInfoPanel, true);

                        $.each(result.data.userData, function (k, v) {

                            $('input[name="holder_' + k + '"]').val(v);

                            if (k === 'country') {
                                $('select[name="holder_' + k + '"]').find("option[value=" + v + "]").attr('selected', true).trigger('change');
                            }

                            if (k === 'type') {
                                $('select[name="holder_' + k + '"]').find("option[value=" + v + "]").attr('selected', true);
                            }

                        });

                        initTaxIdPassportType();
                    }
                }
            });
        }

        if ($saveMode === $viewModeEdit) {
            disableFormInputs(holderInfoPanel, true);
        }
    }
}

function disableContentFields() {

    if (typeof $saveMode != 'undefined' && $saveMode === 'view') {
        $('.btn-edit').remove();
        $('.input-group-addon').addClass('readonly');
        disableFormInputs($('#' + $formId));
        $('#' + $formId).find('button').remove()
    }

    if (typeof $disabledFields != 'undefined' && $disabledFields) {
        disableFormInputs($('.tabs-container'));
        $('.input-group-addon').addClass('readonly');
    }
}