/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching:false,
        "order": [[1, "desc"]],
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
        drawCallback:function(){},
        lengthChange: true,
        pageLength: 100
    },
    customOptions: {
        afterInitCallback: function () {},
        tableDataPath:'constructor-rules/table',
        searchPath:'constructor-rules',
        actions: {
            edit: {
                render: function (row) {
                    return '<a href="' + $cjs.admPath('constructor-rules/' + constructorDocumentId + '/'+ ruleType+'/'+row.id+'/edit') + '" class="btn btn-edit"  data-toggle="tooltip" title="' + $trans('swis.base.tooltip.edit.button') + '"><i class="fa fa-pen"></i></a>'
                }
            }
        },
    }
};

/*
 * Options for Form Request
 */
var optionsForm = {
    searchPath: '/constructor-rules/' + constructorDocumentId + '/'+ ruleType,
    addPath: '/constructor-rules/' + constructorDocumentId + '/'+ ruleType + '/create'
};

// Init Datatable, Form
var $constructorRulesTable = new DataTable('list-data', optionsTable);
var $constructorRules = new FormRequest('form-data', optionsForm);

$(document).ready(function () {

    // init
    $constructorRulesTable.init();
    $constructorRules.init();

    // ----------

    documentAndTypeRedirect();

    obligationList()

});

function documentAndTypeRedirect(){
    $('.document-list').on('change', function () {
        window.location.href = $cjs.admPath('/constructor-rules/'+$(this).val());
    });

    $('.type-list').on('change', function () {
        window.location.href = $cjs.admPath('/constructor-rules/'+$('.document-list').val()+'/'+$(this).val());
    });
}

function obligationList(){
    $('.obligation-list').on('change', function () {

        var type = $(this).find(':selected').data('type');
        if (type == 'rule') {
            $('.ruleBlock').removeClass('hide');
        } else {
            $('.ruleBlock').addClass('hide');
        }
    });
}