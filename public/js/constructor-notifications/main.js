/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        "order": [[1, "desc"]],
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
        lengthChange: true,
        pageLength: 100
    },

    customOptions: {
        afterInitCallback: function () {

        },
        tableDataPath: 'constructor-notifications/table',
        searchPath: 'constructor-notifications'
    }
};

/*
 * Options for Form Request
 */
var optionsForm = {
    deletePath: '/constructor-notifications/delete',
    searchPath: '/constructor-notifications',
    addPath: '/constructor-notifications/create',
};

/*
 * Init Datatable, Form
 */
var $notificationTemplatesTable = new DataTable('list-data', optionsTable);
var $notificationTemplates = new FormRequest('form-data', optionsForm);
var CK_INSTANCES;

$(document).ready(function () {

    // Init Pat
    $notificationTemplatesTable.init();
    $notificationTemplates.init();

    // ---------------

    ckEditor();

    formSubmit();

});

function formSubmit(){
    $('.mc-nav-button-save').click(function (e) {
        e.preventDefault();
    });
}

function ckEditor(){
    if (typeof languageList !== 'undefined' ) {
        CK_INSTANCES = [];

        $.each(languageList, function (k, v) {
            ClassicEditor
                .create(document.querySelector('#ml_' + v + '_editor'))
                .then(editor => {
                    CK_INSTANCES[editor.sourceElement.getAttribute('name')] = editor;
                })
                .catch(error => {
                    // console.error(error);
                });
        })
    }
}