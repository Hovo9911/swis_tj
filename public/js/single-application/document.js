/*
 * Options Form Document Search DataTable
 */
var optionsDocumentSearchTable = {
    datatableOptions: {
        searching: false,
        drawCallback: function (data, json) {
        },
        initComplete: function (settings, json) {
        },
        "order": [[2, "desc"]],
        fnRowCallback: function (nRow, aData) {
            $('td:eq(1)', nRow).addClass('text-center');
        },
    },
    customOptions: {
        afterInitCallback: function (data) {
        },
        tableDataPath: 'folder/document-search',
        columnsRender: {
            document_file: {
                render: function (row) {
                    if (row.document_file) {
                        return '<a target="_blank" href="' + row.document_file + '" class="btn btn-success btn-xs btn--icon"  title="' + $trans('swis.base.tooltip.view.button') + '" data-toggle="tooltip" ><i class="fas fa-eye"></i></a>'
                    }

                    return ''
                }
            },
            id: {
                render: function (row) {
                    if (row.incrementedId) {
                        return "<span class='document-incremented-real-id'>" + row.incrementedId + "</span>";
                    }
                }
            },
            document_form: {
                render: function (row) {
                    if (row.document_form) {
                        return $trans('swis.document.document_form.' + row.document_form + '.title');
                    }
                }
            },
        }
    }
};

/*
 * Init Datatable
 */
var $documentSearch = new DataTable('search-document', optionsDocumentSearchTable);

//
var documentList = $('#documentList');
var documentProductsModal = $('#documentProductsModal');
var documentSubApplicationsModal = $('#documentSubApplicationsModal');
var initDocumentDataTable = true;
var deleteCheckedDocumentsButton = $("#deleteCheckedDocuments");
var checkAllDocumentsCheckbox = $("#checkAllDocuments");

$(document).ready(function () {

    documentSearch();

    addDocument();

    storeDocument();

    deleteDocument();

    renderDocumentInputs();

    addProductsToDocument();

    addSubApplicationsToDocument();

});

function documentSearch() {

    $('.documentSearch').click(function (e) {

        e.preventDefault();

        var self = $(this);
        var documentSearchFilter = $('#documentSearchFilter');
        var documentSearchDiv = $('.searchDoc');

        self.prop('disabled', true);

        if (initDocumentDataTable) {
            initDocumentDataTable = false;
            $documentSearch.init();
        }

        documentSearchDiv.removeClass('hide');
        $documentSearch.mSearchFilterForm = documentSearchFilter;
        $documentSearch.mOptions.customOptions.animateToTable = true;
        $documentSearch.reload();

        $('#search-document').on('xhr.dt', function (e, settings, data, xhr) {
            self.prop('disabled', false);
        })
    })
}

function addDocument() {

    var num = 1;
    var documentErrorInfo = $('#documentErrorInfo');

    $('.addDocument').click(function (e) {

        e.preventDefault();

        var self = $(this);
        var tableTrLength = documentList.find('tbody tr').length + 1;
        var selectedDocs = {};
        var searchDocTable = $('#search-document');

        if (tableTrLength > 1 && num < tableTrLength) {
            num = tableTrLength;
        }

        $.each(searchDocTable.find('tbody input:checked'), function () {
            selectedDocs[$(this).closest('tr').find('.document-incremented-real-id').text()] = $(this).val();
        });

        searchDocTable.find('input:checkbox').prop('checked', false).closest('tr.doc-exist').removeClass('doc-exist');
        documentErrorInfo.html('');

        checkInputsIsChanged();

        if (Object.keys(selectedDocs).length > 0) {

            self.prop('disabled', true);

            $.ajax({
                type: 'POST',
                url: $cjs.admPath('/single-application/add-document'),
                data: {documentItems: selectedDocs, num: num},
                dataType: 'json',
                success: function (result) {

                    if (result.status === 'OK') {
                        documentList.find('tbody').append(result.data.html);
                        documentList.closest('.table-responsive').removeClass('hide');

                        if (result.data.newDocumentCount) {
                            animateScrollToElement(documentList.find('tbody tr:last'), 0, 200);
                            num++;
                        }

                        if (typeof result.data.error !== 'undefined') {
                            var dataError = result.data.error;

                            documentErrorInfo.html('<div class="alert alert-info">' + dataError.message + '</div>');

                            $.each(dataError.docIds, function (k, v) {
                                searchDocTable.find('input:checkbox[value="' + v + '"]').closest('tr').addClass('doc-exist');
                            })
                        }

                        // Reload document datatable
                        $documentSearch.mOptions.customOptions.animateToTable = false;
                        $documentSearch.reload();
                        deleteCheckedDocumentsButton.removeClass('hide');
                    }

                    self.prop('disabled', false);
                }
            });
        }
    });
}

function renderDocumentInputs() {

    $('.plusMultipleFieldsDocument').click(function () {

        var self = $(this);
        var num = documentList.find('tbody tr:last-child').data('num') + 1;
        num = isNaN(num) ? 1 : num;

        spinnerLoaderForButton(self);

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('single-application/render-document-inputs'),
            data: {num: num},
            dataType: 'json',
            success: function (result) {

                if (result.status === 'OK') {
                    documentList.find('tbody').append(result.data.html);
                    documentList.closest('.table-responsive').removeClass('hide');

                    var newTrRow = documentList.find('tr[data-num="' + num + '"]');

                    refAutocomplete();
                    datePickerInit(newTrRow);

                    // G uploader
                    $gUploader.init();
                    $('.g-upload').not('.has-uploader').each(function () {
                        $gUploader.onX($(this));
                    });

                    spinnerLoaderForButton(self, true);
                    animateScrollToElement(newTrRow, 100, 200);
                }
            }
        });
    })
}

function addProductsToDocument() {

    var addProductsToDocBtn = $('#addProductToDocument');

    $(document).on("click", "#checkboxDocumentProducts", function () {
        $(this).closest('table').find("input[type='checkbox']").not('[readonly]').prop("checked", $(this).prop("checked"));
    });

    $(document).on('click', '.addProductsDocument', function (e) {

        e.preventDefault();

        documentList.find('tr').removeClass('active');
        $(this).closest('tr').addClass('active');

        var selectedProducts = $(this).closest('tr').find('.product-ids').val();
        var self = $(this);

        spinnerLoaderForButton(self);
        addProductsToDocBtn.addClass('hide');

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('single-application/render-document-products'),
            data: {documentId: self.data('document')},
            dataType: 'json',
            success: function (result) {

                if (result.status === 'OK') {

                    documentProductsModal.find('.document-products-list').html(result.data.html);

                    if (result.data.productsCount) {
                        addProductsToDocBtn.removeClass('hide');
                    }

                    var documentProducts = documentProductsModal.find(".pr");
                    if (documentProducts.length > 0 && selectedProducts !== '') {
                        selectedProducts = selectedProducts.split(',');

                        $.each(selectedProducts, function (i, v) {
                            documentProductsModal.find('input.pr[value=' + v + ']').prop('checked', true);
                        });
                    }

                    spinnerLoaderForButton(self, true);
                    self.tooltip('destroy');

                    documentProductsModal.modal();

                }
            }
        });

    });

    addProductsToDocBtn.click(function () {

        var productLists = [];
        var productValues = [];
        var activeTr = documentList.find('tr.active');

        $('.document-products-list .pr:checked').each(function () {
            productLists.push($(this).data('pr-number'));
            productValues.push($(this).val());
        });

        checkInputsIsChanged();

        activeTr.find('.product-ids').val(productValues);
        activeTr.find('.product-ids-list').html(productLists.join(', '));

        documentProductsModal.modal('hide');

        // Document product update part
        if (activeTr.find('.addProductsDocument').attr('data-update')) {

            $.ajax({
                type: 'POST',
                url: $cjs.admPath('single-application/update-document-products'),
                data: {products: productValues.join(), documentId: activeTr.find('input[name="document_id"]').val()},
                dataType: 'json',
                success: function (result) {
                }
            })
        }
    });
}

function addSubApplicationsToDocument() {

    var addSubApplicationToDocBtn = $('#addSubApplicationsToDocument');

    $(document).on("click", "#checkboxAllDocumentSubApplications", function () {
        $(this).closest('table').find("input[type='checkbox']").not('[readonly]').prop("checked", $(this).prop("checked"));
    });

    $(document).on('click', '.addSubApplicationsDocument', function (e) {

        e.preventDefault();
        var self = $(this);

        spinnerLoaderForButton(self);
        addSubApplicationToDocBtn.addClass('hide');

        documentList.find('tr').removeClass('active');
        $(this).closest('tr').addClass('active');

        var selectedSubApplications = $(this).closest('tr').find('.sub-application-ids').val();

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('single-application/render-document-sub-applications'),
            data: {documentId: $(this).data('document')},
            dataType: 'json',
            success: function (result) {

                if (result.status === 'OK') {

                    documentSubApplicationsModal.find('.document-sub-application-list').html(result.data.html);

                    if (result.data.subApplicationsCount) {
                        addSubApplicationToDocBtn.removeClass('hide');
                    }

                    var documentSubApplications = documentSubApplicationsModal.find(".sub-application-checkbox");
                    if (documentSubApplications.length > 0 && selectedSubApplications !== '') {

                        selectedSubApplications = selectedSubApplications.split(',');

                        $.each(selectedSubApplications, function (i, v) {
                            documentSubApplicationsModal.find('input.sub-application-checkbox[value=' + v + ']').prop('checked', true);
                        });

                    }

                    spinnerLoaderForButton(self, true);
                    self.tooltip('destroy');

                    documentSubApplicationsModal.modal();
                }
            }
        });

    });

    addSubApplicationToDocBtn.click(function () {

        var subApplicationLists = [];
        var subApplicationValues = [];
        var activeTr = documentList.find('tr.active');

        $('.document-sub-application-list .sub-application-checkbox:checked').each(function () {
            subApplicationLists.push($(this).data('sub-application-type'));
            subApplicationValues.push($(this).val());
        });

        activeTr.find('.sub-application-ids').val(subApplicationValues);
        activeTr.find('.sub-applications-codes-list').html(subApplicationLists.join(', '));

        documentSubApplicationsModal.modal('hide');

        // Document product update part
        if (activeTr.find('.addSubApplicationsDocument').attr('data-update')) {
            $.ajax({
                type: 'POST',
                url: $cjs.admPath('single-application/update-document-sub-applications'),
                data: {
                    subApplications: subApplicationValues.join(),
                    documentId: activeTr.find('input[name="document_id"]').val()
                },
                dataType: 'json',
                success: function (result) {
                }
            })
        }
    });

}

function storeDocument() {

    $(document).on('click', '.storeDocument', function (e) {

        e.preventDefault();

        var holderInfo = $('#importer :input');
        if (safRegime === safRegimeExport) {
            holderInfo = $('#exporter :input');
        }

        var documentInfo = $(this).closest('tr').find(':input');
        var data = $.merge(holderInfo, documentInfo);
        var self = $(this);
        var selfTr = self.closest('tr');

        spinnerLoaderForButton(self);

        $error.show('form-error', []);

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('single-application/store-document'),
            data: data,
            dataType: 'json',
            success: function (result) {
                downloadBtn = '';

                if (result.status === 'OK') {

                    selfTr.find('input').prop('readonly', true);
                    selfTr.find('.document').val(result.data.docId);
                    selfTr.find('.doc-checkbox').val(result.data.docId).removeClass('hidden');
                    selfTr.find('.access-password').html(result.data.accessPassword);
                    selfTr.find('.document-form').html(result.data.documentForm);
                    selfTr.find('.doc-status').html(result.data.docStatus);
                    selfTr.removeClass('not-saved');

                    if (result.data.documentFile) {

                        var downloadBtn = result.data.documentFileName;
                        var viewBtn = '<a target="_blank" href="' + result.data.documentFile + '" class="btn btn-success btn-xs btn--icon" data-toggle="tooltip"   title="' + $trans('swis.base.tooltip.view.button') + '"><i class="fas fa-eye"></i></a>';

                        selfTr.find('.actions-list').prepend(viewBtn);
                    }

                    selfTr.find('.g-upload-container').html(downloadBtn);
                    selfTr.find('.addProductsDocument').attr('data-update', true);
                    selfTr.find('.addSubApplicationsDocument').attr('data-update', true);

                    self.tooltip('destroy');
                    self.remove();

                    deleteCheckedDocumentsButton.removeClass('hide');

                    checkInputsIsChanged();

                } else if (result.status === 'INVALID_DATA') {

                    if (typeof result.errors != 'undefined' && typeof result.errors.taxService != 'undefined') {
                        showAlertsMessages.html('<div class="alert alert-danger">' + result.errors.message + '</div>');
                        animateScrollToElement(showAlertsMessages, 50, 1);
                    }
                }

                if (result.errors) {

                    var num = selfTr.data('num');
                    var errors = result.errors;

                    var showSideTabErrors = true;
                    $.each(documentInfo, function (k, v) {
                        var inputName = $(this).attr('name');

                        if (inputName !== undefined && errors[inputName] !== undefined) {
                            showSideTabErrors = false;
                            $('#form-error-' + inputName + '-' + num).addClass('form-error-text').html(errors[inputName]).css({'display': 'block'});
                        }
                    });

                    if(showSideTabErrors){
                        $error.show('form-error', errors);
                    }

                    spinnerLoaderForButton(self, true);
                    self.tooltip('destroy');
                }
            },
            error: function () {
                spinnerLoaderForButton(self, true);
                self.tooltip('destroy');
            }
        })
    })
}

function deleteDocument() {

    checkAllDocumentsCheckbox.on("click", function () {
        $(this).closest('table').find("input[type='checkbox']").not('[disabled]').prop("checked", $(this).prop("checked"));
    });

    $(document).on('click', 'button.dt-document', function (e) {

        e.preventDefault();

        var self = $(this);
        var selfTr = self.closest('tr');
        var documentId = selfTr.find('input[name="document_id"]').val();

        if (documentId !== '') {

            modalConfirmDelete(function (confirm) {

                if (confirm) {

                    loadContent();

                    documentsDeleteAjax([documentId])
                }
            });

        } else {
            selfTr.remove();

            if (!documentList.find('tbody tr').length) {
                deleteCheckedDocumentsButton.addClass('hide');
                documentList.closest('.table-responsive').addClass('hide');
            }
        }
    });

    deleteCheckedDocumentsButton.click(function () {

        var documentIds = documentList.find("tbody input:checkbox:checked").map(function () {
            if($(this).val()){
                return $(this).val();
            }
        }).get();

        if (documentIds.length) {

            var self = $(this);
            modalConfirmDelete(function (confirm) {

                if (confirm) {

                    loadContent();

                    spinnerLoaderForButton(self);

                    documentsDeleteAjax(documentIds);
                }
            });
        }

    });
}

function documentsDeleteAjax(documentIds) {

    $.ajax({
        type: 'POST',
        url: $cjs.admPath('single-application/delete-document'),
        data: {documentIds: documentIds},
        dataType: 'json',
        success: function (result) {

            if (result.status === 'OK') {

                $.each(documentIds,function (k,v) {
                    documentList.find('tbody .document[value="'+v+'"]').closest('tr').remove();
                })

                if (!documentList.find('tbody tr').length) {
                    deleteCheckedDocumentsButton.addClass('hide');
                    documentList.closest('.table-responsive').addClass('hide');
                }

                checkAllDocumentsCheckbox.prop('checked',false);
                spinnerLoaderForButton(deleteCheckedDocumentsButton,true);

                loadContent();
                checkInputsIsChanged();
            }
        }
    });

}

function documentOpenedNotSaved() {
    var storeDocumentButtons = $('.storeDocument');

    var notSaved = false;
    if (storeDocumentButtons.length) {
        notSaved = true;
        $.each(storeDocumentButtons, function (k, v) {
            $(this).closest('tr').addClass('not-saved');
        });

        $('ul.nav a[href="#tab-attached_documents"]').tab('show')
    }

    return notSaved;
}