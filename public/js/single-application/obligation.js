var payObligationModal = $('#PayObligationModal');
var payObligationBtn = $('#pay-obligation-modal');
var obligationTotalPrice = $('#totalPrice');

$(document).ready(function () {

    payObligations();

    obligationCheckCheckBoxOnTd();

    obligationAllCheckboxes();

    obligationFileReceiptUpload();

});

function obligationCheckCheckBoxOnTd() {
    $('.check-obligation').click(function (event) {
        if ($(this).find('input').length > 0) {

            if (!$(event.target).is('input')) {
                $('input:checkbox', this).prop('checked', function (i, value) {
                    return !value;
                });

                setTotalObligationPrice($('input:checkbox', this));
            }
        }
    });

    $('.obligation').on('click', function () {
        setTotalObligationPrice($(this));
    })
}

function payObligations() {

    payObligationBtn.on('click', function (event) {
        event.preventDefault();
        var data = [];
        $('button').prop('disabled', true);

        $('.obligation:checked').each(function (index, value) {
            data.push($(value).data('id'));
        });

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('single-application/pay-obligations-modal'),
            data: {data: JSON.stringify(data), regular_number: safNumber},
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    payObligationModal.html('');
                    payObligationModal.append(result.data.html);
                    payObligationModal.modal('show');
                }

                $('button').prop('disabled', false);
            }
        })
    });

    $(document).on('click', '#pay-obligation', function (event) {
        event.preventDefault();
        $('button').prop('disabled', true);

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('single-application/pay-obligations'),
            data: {sum: $(this).data('sum'), obligations: $(this).data('obligations'), regular_number: safNumber},
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    var backToUrl = result.data.backToUrl;
                    if (backToUrl) {
                        window.location.href = backToUrl;
                        if (backToUrl.indexOf('#') !== -1) {
                            window.location.reload();
                        }
                    }
                } else {
                    toastr.error($trans.get('core.loading.invalid_data'));
                }
                payObligationModal.modal('hide');
                $('button').prop('disabled', false);
            }
        })
    });

    $(document).on('click', '#refile', function (event) {
        $(this).prop('disabled', true)
        event.preventDefault();

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('/payments/getDynamicParams'),
            data: {order_id: $('#orderId').val(), amount: $('#amountField').val()},
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    $('#amountField').val(parseFloat($('#amountField').val()).toFixed(2))
                    $('#token').val(result.data.token);
                    $('#orderId').val(result.data.orderId);
                    $('#refile').prop('disabled', false);
                    $('#form-data-refile').submit();
                }
            }
        });

    })
}

function obligationAllCheckboxes() {
    $(document).on("click", "#check-all-obligations", function () {

        $(this).closest('table').find("input[type='checkbox']").not('[readonly]').prop("checked", $(this).prop("checked"));

        var obligationCheckbox = $('.obligation');

        var totalSum = 0;
        var payObligationBtnProp = true;

        if ($(this).is(':checked')) {

            if (obligationCheckbox.length) {
                $.each(obligationCheckbox, function (k, v) {
                    totalSum += parseFloat($(this).data('amount'))
                });

                payObligationBtnProp = false;
                payObligationBtn.prop('checked', false);
            }
        }

        totalSum = totalSum.toFixed(2);

        payObligationBtn.prop('disabled', payObligationBtnProp);
        obligationTotalPrice.data('value', totalSum).text(totalSum + ' ' + defaultCurrency);
    });
}

function obligationFileReceiptUpload() {

    if (typeof $gUploader != 'undefined') {
        var $XgUploader = Object.assign({}, $gUploader);

        $XgUploader.init('x-g-upload');

        $XgUploader.onSuccess = function (response, self) {

            var fileName = response.data[0].file_name;
            var obligationId = self.closest(".g-upload-container").data('id');

            $.ajax({
                type: 'POST',
                url: $cjs.admPath('single-application/store-obligation-receipt'),
                data: {file_name: fileName, obligation_id: obligationId},
                dataType: 'json',
                success: function (result) {
                    $(".g-uploaded-list", self.closest(".g-upload-container"))['html'](
                        '<div class="register-form__clear-box fs14 g-uploaded"><a target="_blank" href="' + result.data['fileUrl'] + '">' + response.data[0].label + '</a> ' +
                        '<button type="button" class="register-form__clear-btn sprite dib fs12 x-g-upload-x"> <i class="fa fa-times"></i> </button>' +
                        '</div>'
                    );

                    $XgUploader.onX(self)
                }
            })
        };

        //
        $XgUploader.onX = function (self) {

            $(".x-g-upload-x", self.closest(".g-upload-container")).click(function () {

                var self = $(this);
                var obligationId = self.closest(".g-upload-container").data('id');

                modalConfirmDelete(function (confirm) {

                    if (confirm) {

                        loadContent();

                        $.ajax({
                            type: 'POST',
                            url: $cjs.admPath('single-application/delete-obligation-receipt'),
                            data: {obligation_id: obligationId},
                            dataType: 'json',
                            success: function (result) {

                                if (result.status === 'OK') {
                                    loadContent();
                                    self.closest(".g-uploaded").remove();
                                }

                                if (result.status === 'INVALID_DATA') {
                                    loadContent();
                                    self.remove();

                                    alertMessageDiv.html('<div class="alert alert-danger">' + result.errors.message + '</div>');
                                    animateScrollToElement(alertMessageDiv, 100, 1);
                                }
                            }
                        });
                    }
                });
            });
        };

        //
        $('.x-g-upload-x').each(function () {
            $XgUploader.onX($(this));
        });
    }
}

function setTotalObligationPrice(element) {
    var amount = parseFloat(element.data('amount'));
    var total = parseFloat(obligationTotalPrice.data('value')) ? parseFloat(obligationTotalPrice.data('value')) : 0;

    total = (element.is(':checked')) ? total + amount : total - amount;
    total = parseFloat(total).toFixed(2);

    payObligationBtn.prop('disabled', !($('.obligation:checked').length > 0));
    obligationTotalPrice.data('value', total).text(total + ' ' + defaultCurrency);
}