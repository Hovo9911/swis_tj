var subApplicationTable = $('#subApplicationList');
var subApplicationProductsModal = $('#subApplicationProductsModal');
var subApplicationProductsNotValid = [];
var productsTable = $('table.productList');
var showErrorsDiv = $('#subApplicationErrorList');

$(document).ready(function () {

    sendSubApplication();

    renderSubApplicationInputs();

    deleteManualSubApplication();

    getSubApplicationInfo();

    addProductsToSubApplication();

    reGenerateAccessToken();

    downloadSubApplicationPdf();

    storeManualSubApplication();

});

function sendSubApplication() {
    $(document).on('click', '.send-sub-application', function () {

        var self = $(this);
        var selfTr = self.closest('tr');

        var expressControl = selfTr.find('.column_express_control_id');
        var agencySubDivision = selfTr.find('.column_agency_subdivision_id');
        var subApplicationId = self.attr('data-id');
        var needDigitalSignature = self.attr('data-signature');

        productsTable.find('tr.not-valid-product').removeClass('not-valid-product');
        showErrorsDiv.html('');

        selfTr.find('.form-group').removeClass('has-error');
        selfTr.find('.form-error-text').html('');

        $error.show('form-error', []);

        loadContent();
        alertMessageDiv.html('');

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('single-application/send-sub-application'),
            data: {
                id: subApplicationId,
                express_control_id: expressControl.val(),
                agency_subdivision_id: agencySubDivision.val(),
                need_digital_signature: needDigitalSignature
            },
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {

                    if (result.data.needConfirm) {
                        var subApplicationSendAndPay = $('#subApplicationSendAndPay');

                        subApplicationSendAndPay.html('');
                        subApplicationSendAndPay.append(result.data.html);
                        subApplicationSendAndPay.modal('show');

                        loadContent();
                    } else {
                        var bactToUrl = result.data.backToUrl;

                        if (bactToUrl) {
                            window.location.href = bactToUrl;
                            if (bactToUrl.indexOf('#') !== -1) {
                                window.location.reload();
                            }
                        }
                    }
                } else if (result.status === 'INVALID_DATA') {

                    if (typeof result.errors.valid_message != 'undefined' && result.errors.valid_message) {

                        alertMessageDiv.html('<div class="alert alert-danger">' + result.errors.message + '</div>');
                        animateScrollToElement(alertMessageDiv, 100, 1);

                        if (typeof result.errors.disable_send != 'undefined' && result.errors.disable_send) {
                            self.prop('disabled', true);
                        }
                    }

                    if (typeof result.errors.not_valid_saf_controlled_fields != 'undefined' && result.errors.not_valid_saf_controlled_fields) {

                        var hasNotValidProduct = hasNotValidSafField = false;
                        var errorHtml = '';
                        if (!$.isEmptyObject(result.errors.fields.inProducts)) {
                            subApplicationProductsNotValid = result.errors.fields.inProducts;

                            $.each(result.errors.fields.inProducts, function (k, v) {
                                productsTable.find('button[data-id="' + v + '"]').closest('tr').addClass('not-valid-product');
                            });

                            errorHtml = '<div class="alert alert-danger">' + result.errors.not_valid_products_message + '</div>';
                            hasNotValidProduct = true;
                        }

                        if (!$.isEmptyObject(result.errors.fields.inSaf)) {
                            $error.show('form-error', result.errors.fields.inSaf);

                            hasNotValidSafField = true
                        }

                        if (result.errors.sub_application_cant_send_message) {
                            errorHtml += '<div class="alert alert-danger">' + result.errors.sub_application_cant_send_message + '</div>'
                        }

                        // Show Error Alerts
                        alertMessageDiv.html(errorHtml);

                        // If only has not valid field in products show product tab
                        if (hasNotValidProduct && !hasNotValidSafField) {
                            $('.nav-tabs a[href="#tab-products"]').tab('show');
                        }

                        animateScrollToElement(alertMessageDiv, 100, 1);

                    } else {
                        $.each(result.errors, function (k, v) {
                            if (selfTr.find('.form-error-' + k).length) {
                                selfTr.find('.form-error-' + k).text(v)
                            } else if ($form.find('#form-error-' + k).length) {
                                $error.show('form-error', result.errors);
                            }
                        })
                    }

                    if (typeof result.errors.need_digital_signature != 'undefined' && typeof result.errors.need_digital_signature) {
                        $('#buttonMapping').val(result.errors.button);
                        $('#subapplicationId').val(subApplicationId);
                        openDigitalSignatureModal();
                    }

                    loadContent();
                }
            }
        });
    });

    $(document).on('click', '#payForSubApplication', function () {
        loadContent();
        $('button').prop('disabled', true);
        showErrorsDiv.html('');
        $.ajax({
            type: 'POST',
            url: $cjs.admPath('single-application/send-sub-application'),
            data: {
                id: $('#subApplicationId').val(),
                express_control_id: $('#expressControlId').val(),
                agency_subdivision_id: $('#agencySubdivisionId').val(),
                confirmed: true
            },
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    window.location.reload();
                } else if (result.status === 'INVALID_DATA') {
                    var errorMessage = result.errors.message;
                    showErrorsDiv.html('<div class="alert alert-danger">' + errorMessage + '</div>');
                    loadContent();
                }
            }
        });
    });
}

function addProductsToSubApplication() {

    $(document).on("click", '#checkboxSubApplicationProducts', function () {
        $(this).closest('table').find("input[type='checkbox']").prop("checked", $(this).prop("checked"));
    });

    $(document).on('click', '.addProductsToSubApplication', function (e) {
        e.preventDefault();

        var self = $(this);

        subApplicationTable.find('tr').removeClass('active');
        $(this).closest('tr').addClass('active');

        var selectedProducts = $(this).closest('tr').find('.product-ids').val();
        spinnerLoaderForButton(self);

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('single-application/render-sub-application-products'),
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {

                    subApplicationProductsModal.find('.sub-application-products-list').html(result.data.html);

                    var subApplicationProducts = subApplicationProductsModal.find(".pr");

                    if (subApplicationProducts.length > 0 && selectedProducts !== '') {
                        selectedProducts = selectedProducts.split(',');

                        subApplicationProducts.each(function (i, checkbox) {
                            if (selectedProducts.indexOf($(checkbox).val()) !== -1) {
                                $(this).prop('checked', true);
                            }
                        });
                    }

                    subApplicationProductsModal.modal();

                    spinnerLoaderForButton(self, true);

                    self.tooltip('destroy');
                }
            }
        });

    });

    $('#addProductToSubApplication').click(function () {

        var productLists = [];
        var productValues = [];
        var activeTr = subApplicationTable.find('tr.active');

        $('.sub-application-products-list .pr:checked').each(function () {
            productLists.push($(this).data('pr-number'));
            productValues.push($(this).val());
        });

        activeTr.find('.product-ids').val(productValues);
        activeTr.find('.product-ids-list').html(productLists.join());

        subApplicationProductsModal.modal('hide');
    });
}

function renderSubApplicationInputs() {
    $('.plusMultipleFieldSubApplication').click(function () {

        var self = $(this);

        spinnerLoaderForButton(self);

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('single-application/render-sub-application-inputs'),
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {

                    subApplicationTable.append(result.data.html);
                    subApplicationTable.closest('.table-responsive').removeClass('hide');

                    $('.sub-application-type').select2({
                        placeholder: $trans('core.base.label.select'),
                        allowClear: true
                    });

                    $('.sub-application-agency').select2({
                        placeholder: $trans('core.base.label.select'),
                        allowClear: true
                    });

                    animateScrollToElement(subApplicationTable.find('tr:last'), 100, 200);
                }

                spinnerLoaderForButton(self, true);
            }
        });
    });
}

function getSubApplicationConstructorDocumentsAllInfo(selfTr) {

    var selfSubApplicationType = selfTr.find('.sub-application-type');
    var selfAgency = selfTr.find('.sub-application-agency');

    $.ajax({
        type: 'POST',
        url: $cjs.admPath('single-application/get-sub-application-constructor-documents'),
        dataType: 'json',
        success: function (result) {
            if (result.status === 'OK') {

                var data = result.data;

                // Sub application types
                if (data.subApplicationTypes.length > 0) {
                    select2Reset(selfSubApplicationType, true, false);
                    $.each(data.subApplicationTypes, function (k, value) {
                        if (value.name) {
                            var newOption = new Option(value.name, value.id, false, false);

                            selfSubApplicationType.append(newOption);
                        }
                    });
                }

                // Agency
                if (data.agencies.length > 0) {
                    select2Reset(selfAgency, true, false);
                    $.each(data.agencies, function (k, value) {
                        if (value.name) {

                            var newOption = new Option(value.name, value.id, false, false);

                            selfAgency.append(newOption);
                        }
                    });
                }

                animateScrollToElement(selfTr, 100, 200);
            }
        }
    });
}

function getSubApplicationInfo() {
    $(document).on('change', '.sub-application-type', function () {

        var self = $(this);
        var selfTr = $(this).closest('tr');

        var selfAgency = selfTr.find('.sub-application-agency');
        var selfSubApplicationType = selfTr.find('.sub-application-type');
        var selfSubApplicationName = selfTr.find('.sub-application-name');

        var selfAgencyVal = selfAgency.val() || '';
        var selfSubApplicationTypeVal = selfSubApplicationType.val() || '';

        selfSubApplicationName.text('');

        if (selfSubApplicationTypeVal === '' && selfAgencyVal === '') {
            getSubApplicationConstructorDocumentsAllInfo(selfTr);
            return false;
        }

        if(selfSubApplicationTypeVal !== ''){
            $.ajax({
                type: 'POST',
                url: $cjs.admPath('single-application/get-sub-application-agency'),
                data: {sub_application_type: selfSubApplicationTypeVal},
                dataType: 'json',
                success: function (result) {
                    if (result.status === 'OK') {

                        var data = result.data;

                        selfAgencyVal = parseInt(selfAgencyVal);
                        selfSubApplicationName.text(data.subApplicationName);

                        // Agency
                        if (data.agencies.length > 0) {

                            if (data.agenciesIds.indexOf(selfAgencyVal) === -1) {
                                select2Reset(selfAgency, true , false);

                                var oneAgency = false;
                                $.each(data.agencies, function (k, value) {
                                    if (value.name) {

                                        if (data.agencies.length === 1) {
                                            oneAgency = true;
                                        }

                                        if (selfAgencyVal !== value.id) {
                                            var newOption = new Option(value.name, value.id, false, oneAgency);

                                            selfAgency.append(newOption);
                                            if (oneAgency) {
                                                selfAgency.trigger('change.select2');
                                            }
                                        }
                                    }
                                });
                            }

                        }else{
                            select2Reset(selfAgency)
                        }
                    }

                    animateScrollToElement(selfTr, 50, 100);
                    self.prop('disabled', false);
                }
            });
        }
    });

    $(document).on('change', '.sub-application-agency', function () {

        var self = $(this);
        var selfTr = $(this).closest('tr');
        var selfSubApplicationType = selfTr.find('.sub-application-type');
        var selfSubApplicationName = selfTr.find('.sub-application-name');

        select2Reset(selfSubApplicationType, true, false);
        selfSubApplicationName.text('');

        var selfSubApplicationTypeVal = selfSubApplicationType.val() || '';
        var selfAgencyVal = self.val() || '';

        if (selfSubApplicationTypeVal === '' && selfAgencyVal === '') {
            getSubApplicationConstructorDocumentsAllInfo(selfTr);
            return false;
        }

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('single-application/get-sub-application-type'),
            data: {agency_id: self.val(), sub_application_type: selfSubApplicationTypeVal},
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    var data = result.data;

                    // Sub application types
                    var oneSubApplication = false;
                    if (data.subApplicationTypes.length > 0) {

                        if (data.subApplicationTypes.length === 1) {
                            oneSubApplication = true;
                        }

                        $.each(data.subApplicationTypes, function (k, value) {
                            if (value.document_name) {
                                var newOption = new Option(value.document_name, value.id, false, oneSubApplication);

                                selfSubApplicationType.append(newOption);

                                if (oneSubApplication) {
                                    selfSubApplicationType.trigger('change.select2');
                                }
                            }
                        });

                    }else{
                        select2Reset(selfSubApplicationType);
                    }

                    animateScrollToElement(selfTr, 50, 100);
                }
            }
        });

    });
}

function storeManualSubApplication() {
    $(document).on('click', '.storeSupApplication', function () {

        var self = $(this);
        var selfTr = self.closest('tr');

        selfTr.find('.form-error').html('').css({'display': 'none'});

        var subApplicationInfo = selfTr.find(':input');

        loadContent();

        spinnerLoaderForButton(self);

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('single-application/store-manual-sub-application'),
            data: subApplicationInfo,
            dataType: 'json',
            success: function (result) {

                if (result.status === 'OK') {
                    selfTr.replaceWith(result.data.html);

                    if (typeof result.data.needRefresh != 'undefined' && result.data.needRefresh) {

                        window.location.href = result.data.refreshUrl;
                        window.location.reload();

                        return;
                    }

                    subApplicationTable.find('select.select2').not(".select2-hidden-accessible").select2({
                        placeholder: $selectLabel,
                        minimumResultsForSearch: 10,
                    });

                    if (result.data.needAttachDocMessage) {
                        showErrorsDiv.html('<div class="alert alert-danger">' + result.data.needAttachDocMessage + '</div>');
                        animateScrollToElement(showErrorsDiv, 100, 1)
                    } else {
                        animateScrollToElement(subApplicationTable.find('tr:last'), 0, 1);
                    }
                }

                if (result.errors) {

                    var errors = result.errors;
                    $.each(subApplicationInfo, function (k, v) {
                        var inputName = $(this).attr('name');

                        if (inputName !== undefined) {
                            if (errors[inputName] !== undefined) {
                                selfTr.find('.form-error-' + inputName).addClass('form-error-text').html(errors[inputName]).css({'display': 'block'});
                            }
                        }
                    });

                    if (typeof errors.agency_not_found_message != 'undefined') {
                        selfTr.find('.form-error-agency_id').addClass('form-error-text').html(errors.agency_not_found_message).css({'display': 'block'})
                    }

                }

                spinnerLoaderForButton(self, true);
                loadContent();
            }
        })
    });
}

function deleteManualSubApplication() {

    $(document).on('click', '.dt-sub-application', function () {

        var self = $(this);
        var selfTr = self.closest('tr');
        var subApplicationId = self.data('id');

        if (typeof subApplicationId !== "undefined" && subApplicationId !== '') {

            modalConfirmDelete(function (confirm) {

                if (confirm) {

                    loadContent();

                    $.ajax({
                        type: 'POST',
                        url: $cjs.admPath('single-application/delete-manual-sub-application'),
                        data: {id: subApplicationId},
                        dataType: 'json',
                        success: function (result) {

                            if (result.status === 'OK') {
                                selfTr.remove();

                                if (!subApplicationTable.find('tbody tr').length) {
                                    subApplicationTable.closest('.table-responsive').addClass('hide');
                                }

                                loadContent();
                            }

                            if (result.status === 'INVALID_DATA') {
                                loadContent();
                            }
                        }
                    })
                }
            });

        } else {
            selfTr.remove();

            if (!subApplicationTable.find('tbody tr').length) {
                subApplicationTable.closest('.table-responsive').addClass('hide');
            }
        }
    });
}

function downloadSubApplicationPdf() {
    $('.downloadSubApplicationPdf').click(function () {
        $.ajax({
            type: 'POST',
            url: $cjs.admPath('single-application/subapplication-pdf'),
            data: {id: self.data('id')},
            dataType: 'json',
            success: function (result) {
                if (result.data) {
                    var bactToUrl = result.data.backToUrl;

                    if (bactToUrl) {

                        window.location.href = bactToUrl;

                        if (bactToUrl.indexOf('#') !== -1) {
                            window.location.reload();
                        }
                    }
                }
            }
        });
    })
}

function reGenerateAccessToken() {
    $('.regenerate-access-token').on('click', function () {

        var self = $(this);
        var applicationId = $(this).data('id');

        spinnerLoaderForButton(self);
        self.prop('disabled',true);

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('user-documents/regenerate-access-token'),
            data: {applicationId: applicationId},
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {

                    toastr.success($trans.get('core.loading.success'));
                    $('.column_access_token_' + applicationId).text(result.data.token);

                    spinnerLoaderForButton(self,true);
                    self.prop('disabled',false);

                } else {
                    toastr.error($trans.get('core.loading.invalid_data'));

                    spinnerLoaderForButton(self, true);
                    self.prop('disabled',false);
                }
            }
        })
    });
}

function openDigitalSignatureModal() {

    var digitalSignatureModal = $('#rutoken-modal');

    digitalSignatureModal.modal({
        backdrop: 'static',
        keyboard: false
    });
}
