$(document).ready(function () {
    checkDigitalSignature();
});

function checkDigitalSignature() {

    var rutokenModalCheck = $('#rutoken-modal-check');
    $(".result").html();

    $('.checkRutokenModal').click(function (e) {
        e.preventDefault();
        var self = $(this);
        var digitalSignatureId = self.attr('data-id');
        var checkRutokenAlertMessage = $('#checkRutokenAlertMessage');
        var digitalSignatureInfo = $('#digitalSignatureInfo');

        checkRutokenAlertMessage.html('');
        digitalSignatureInfo.html('');

        rutokenModalCheck.modal();
        self.prop('disabled', true);
        $.ajax({
            url: $cjs.admPath('single-application/check-digital-signature'),
            type: 'post',
            dataType: 'json',
            data: {digitalSignatureId: digitalSignatureId},
            success: function (result) {
                if (result.status === 'OK') {
                    var str = '';

                    if (typeof result.data.command.subject != 'undefined') {
                        str += '<p>' + $trans('swis.user-documents.digital-signature.issuer') + " " + result.data.command.subject.CN + '</p>';
                    }

                    if (typeof result.data.command.issuer != 'undefined') {
                        str += '<p>' + result.data.command.issuer.CN + '</p>';
                    }

                    if (typeof result.data.signing_time != 'undefined') {
                        str += '<p>' + $trans('swis.user-documents.digital-signature.signing_time') + " " + result.data.signing_time + '</p>';
                    }

                    if (result.data.command == false) {
                        checkRutokenAlertMessage.html('<div class="alert alert-danger"><img src="/image/digital-signature/sign-error-icon.png" class="image_middle">' + ' ' + $trans('swis.user-documents.digital_signature.not_valid') + '</div>')
                    } else {
                        checkRutokenAlertMessage.html('<div class="alert alert-success"><img src="/image/digital-signature/sign-confirmed-icon.png" class="image_middle">' + ' ' + $trans('swis.user-documents.digital_signature.valid') + '</div>')
                        digitalSignatureInfo.html(str);
                    }
                }
            },
            error: function (xhr, status, error) {
            },
        });
    });
}