var alertDivForLabExamination = $('.alertDivForLabExamination');
var labExpertiseCrudModalBody = $('#labExpertiseCrudModalBody');

$(document).ready(function () {

    labExamination();

});

function labExamination() {

    var typeValue = null;
    var generateLaboratoryApplication = $('#generateLaboratoryApplication');

    $('.addLabExpertiseButton').on('click', function () {
        $('#laboratoryCrudModal').modal('show');
    });

    $('#selectLabExpertise').on('select2:select', function (e) {

        var self = $(this);
        self.prop('disabled', true);

        typeValue = self.val();

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('single-application/laboratory/create'),
            data: {
                type: typeValue,
                saf_number: safNumber,
                edit: false
            },
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    self.prop('disabled', false);

                    labExpertiseCrudModalBody.html(result.data.viewHtml);

                    resetSelect2Values();

                    $('#laboratoryCrudModal').modal('show');
                }
            },
            error: function (request, status, error) {
                // showError(self, request);
            }
        });
    });

    $(document).on('select2:select', '.selectLabExpertiseProduct', function () {

        if (typeValue == '1') {
            return false;
        }

        var self = $(this);
        var selfBlock = self.closest('.multipleProductBlockLabExamination');
        var selectLabExpertiseLaboratory = $('#selectLabExpertiseLaboratory');
        selfBlock.find('.LabExpertiseProductSampleQuantity').val('');
        selfBlock.find('.LabExpertiseProductSampleNumber').val('');

        self.prop('disabled', true);

        if (typeValue != '3') {
            selectLabExpertiseLaboratory.find('option').remove().end().val('').append("<option value=''></option>").trigger('change');
            selectLabExpertiseLaboratory.prop('disabled', true);
        } else {
            selectLabExpertiseLaboratory.val('').trigger('change');
            selectLabExpertiseLaboratory.prop('disabled', true);
        }

        var selectedProducts = [];
        $('.selectLabExpertiseProduct').each(function (key, value) {
            selectedProducts.push($(value).val());
        })

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('single-application/laboratory/get-laboratories'),
            data: {
                saf_number: safNumber,
                product_ids: selectedProducts,
                product_id: self.val()
            },
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    self.prop('disabled', false);
                    selectLabExpertiseLaboratory.prop('disabled', false);

                    if (result.data.labs.length && typeValue != '3') {
                        $.each(result.data.labs, function (key, value) {
                            selectLabExpertiseLaboratory.prop('disabled', false);
                            selectLabExpertiseLaboratory.append($("<option></option>")
                                .attr("value", value.id)
                                .text("(" + value.tax_id + ") " + value.name));
                        })
                    }

                    blockEnabledOrDisabled(self.closest('.multipleProductBlockLabExamination'), false, result.data.measurement_unit);
                } else if (result.status === 'INVALID_DATA') {
                    self.prop('disabled', false);
                    var block = self.closest('.multipleProductBlockLabExamination');
                    block.find('.selectMeasurementUnit').val('').trigger('change');
                }
            }
        });
    });

    $(document).on('select2:select', '#selectLabExpertiseLaboratory', function (e) {
        generateLaboratoryApplication.prop('disabled', false);
    });

    $(document).on('select2:unselecting', '#selectLabExpertiseLaboratory', function (e) {
        generateLaboratoryApplication.prop('disabled', true);
    });

    $(document).on('change', '.LabExpertiseProductSampleNumber,.LabExpertiseProductSampleQuantity,.selectMeasurementUnit', function () {
        generateLaboratoryApplication.prop('disabled', false);
    })

    generateLaboratoryApplication.on('click', function () {
        var subapplicationId = $('#labApplicationId');
        var self = $(this);
        var labExaminationTableContainer = $('.lab-examination-table-container');
        var checkedValues = $('.indicatorSelect').val();
        var selectedProducts = [];

        self.prop('disabled', true);

        $('.multipleProductBlockLabExamination').each(function (key, item) {
            var it = $(item);
            selectedProducts.push({
                'product_id': it.find('.selectLabExpertiseProduct').val(),
                'sample_quantity': it.find('.LabExpertiseProductSampleQuantity').val(),
                'sample_measurement_unit': it.find('.selectMeasurementUnit').val(),
                'sample_number': it.find('.LabExpertiseProductSampleNumber').val()
            })
        });

        var data = {
            laboratory_id: $('#selectLabExpertiseLaboratory').val(),
            products: selectedProducts,
            saf_number: safNumber,
            type: typeValue,
            lab_application_id: subapplicationId.length ? subapplicationId.val() : null,
        };

        if (typeValue == '1') {
            data['indicators'] = checkedValues;
        }

        alertDivForLabExamination.hide();
        alertDivForLabExamination.text('');

        var action = subapplicationId.length ? 'update-laboratory-applications' : 'generate-laboratory-applications';

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('single-application/laboratory/' + action),
            data: data,
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {

                    self.prop('disabled', false);

                    labExaminationTableContainer.html(result.data.laboratoriesView);

                    labExpertiseCrudModalBody.html('');
                    $('#laboratoryCrudModal').modal('hide');

                } else if (result.status === 'INVALID_DATA') {

                    if (typeof result.errors.laboratory_or_document_not_valid !== 'undefined') {
                        alertDivForLabExamination.show();
                        alertDivForLabExamination.text(result.errors.laboratory_or_document_not_valid);
                    }

                    if (result.errors) {
                        $error.show('form-error', result.errors);
                    }
                }
            },
            error: function (request, status, error) {
                // showError(self, request);
            }
        });
    });

    $(document).on('select2:unselecting', '.selectLabExpertiseProductByProducts, .selectLabExpertiseProduct', function () {
        var self = $(this);

        var selfBlock = self.closest('.multipleProductBlockLabExamination');
        var indicatorSelect = $('.indicatorSelect');
        var selectLabExpertiseLaboratory = $('#selectLabExpertiseLaboratory');

        indicatorSelect.find('option').remove().end().trigger('change');
        indicatorSelect.prop('disabled', true);
        selfBlock.find('.LabExpertiseProductSampleQuantity').val('');
        selfBlock.find('.LabExpertiseProductSampleNumber').val('');

        blockEnabledOrDisabled(selfBlock, true);

        selectLabExpertiseLaboratory.find('option').remove().end().val('').trigger('change');
        selectLabExpertiseLaboratory.prop('disabled', true);

        self.prop('disabled', false);
    });

    $(document).on('select2:select', '.selectLabExpertiseProductByProducts', function () {
        var self = $(this);
        var selfBlock = self.closest('.multipleProductBlockLabExamination');

        var indicatorSelect = $('.indicatorSelect');
        indicatorSelect.find('option').remove().end().trigger('change');
        indicatorSelect.prop('disabled', true);

        self.prop('disabled', true);
        selfBlock.find('.LabExpertiseProductSampleQuantity').val('');
        selfBlock.find('.LabExpertiseProductSampleNumber').val('');

        var selectLabExpertiseLaboratory = $('#selectLabExpertiseLaboratory');
        selectLabExpertiseLaboratory.find('option').remove().end().val('').trigger('change');
        selectLabExpertiseLaboratory.prop('disabled', true);

        if (!self.val()) {
            return false;
        }
        $.ajax({
            type: 'POST',
            url: $cjs.admPath('single-application/laboratory/get-indicators'),
            data: {
                product_id: self.val(),
            },
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    self.prop('disabled', false);

                    if (result.data.indicators.length) {
                        $.each(result.data.indicators, function (key, value) {
                            indicatorSelect.append($("<option></option>")
                                .attr("value", value.id)
                                .text("(" + value.code + ") " + value.name));
                        });
                    }
                    indicatorSelect.prop('disabled', false);
                    blockEnabledOrDisabled(selfBlock, false, result.data.measurement_unit);
                }
            },
            error: function (request, status, error) {
                self.prop('disabled', false);
                self.closest('.multipleProductBlockLabExamination').find('.selectMeasurementUnit').val('').trigger('change');
            }
        });
    });

    $(document).on('select2:select select2:unselect', '.indicatorSelect', function () {

        var self = $(this);
        var selectLabExpertiseLaboratory = $('#selectLabExpertiseLaboratory');
        var checkedValues = $(this).val();

        self.prop('disabled', true);
        select2Reset(selectLabExpertiseLaboratory);
        selectLabExpertiseLaboratory.prop('disabled', true);

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('single-application/laboratory/get-laboratories-by-indicators'),
            data: {
                indicators: checkedValues,
            },
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {

                    self.prop('disabled', false);

                    $.each(result.data.labs, function (key, value) {
                        selectLabExpertiseLaboratory.append($("<option></option>")
                            .attr("value", value.id)
                            .text("(" + value.tax_id + ") " + value.name));
                    });

                    resetSelect2Values(true);

                    selectLabExpertiseLaboratory.prop('disabled', false);

                }
            },
            error: function (request, status, error) {
                self.prop('disabled', false);
                self.closest('.multipleProductBlockLabExamination').find('.selectMeasurementUnit').val('').trigger('change');
            }
        });
    });

    $(document).on('click', '.delete-lab', function () {

        var self = $(this);
        var selfTr = self.closest('tr');
        var subApplicationId = self.data('id');

        if (typeof subApplicationId !== "undefined" && subApplicationId !== '') {

            modalConfirmDelete(function (confirm) {

                if (confirm) {

                    loadContent();

                    $.ajax({
                        type: 'POST',
                        url: $cjs.admPath('single-application/laboratory/delete-lab-examination'),
                        data: {id: subApplicationId},
                        dataType: 'json',
                        success: function (result) {

                            if (result.status === 'OK') {
                                selfTr.remove();

                                if (!subApplicationTable.find('tbody tr').length) {
                                    subApplicationTable.closest('.table-responsive').addClass('hide');
                                }

                                loadContent();
                            }

                            if (result.status === 'INVALID_DATA') {
                                loadContent();
                            }
                        }
                    })
                }
            });

        } else {
            selfTr.remove();

            if (!subApplicationTable.find('tbody tr').length) {
                subApplicationTable.closest('.table-responsive').addClass('hide');
            }
        }
    });

    $(document).on('click', '.edit-lab', function () {
        var self = $(this);
        typeValue = self.data('type');

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('single-application/laboratory/create'),
            data: {
                type: typeValue,
                saf_number: safNumber,
                application_id: self.data('id'),
                edit: true
            },
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    $('#selectLabExpertise').val(typeValue).trigger('change');
                    labExpertiseCrudModalBody.html(result.data.viewHtml);

                    resetSelect2Values();

                    $('#laboratoryCrudModal').modal('show');
                }
            },
            error: function (request, status, error) {
                // showError(self, request);
            }
        });
    });

    $(document).on('click', '.send-to-lab', function () {
        var self = $(this);
        typeValue = self.data('type');

        spinnerLoaderForButton(self);

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('single-application/laboratory/send-to-lab'),
            data: {
                sub_application_id: self.data('id'),
            },
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    var bactToUrl = result.data.backToUrl;
                    if (bactToUrl) {
                        window.location.href = bactToUrl;
                        if (bactToUrl.indexOf('#') !== -1) {
                            window.location.reload();
                        }
                    }
                }
            },
            error: function (request, status, error) {
                // showError(self, request);
                spinnerLoaderForButton(self, true);
            }
        });
    });

    $(document).on("click", ".addNewProductLabExamination", function () {
        var selectedProducts = [];

        $('.multipleProductBlockLabExamination').each(function (key, item) {
            var item = $(item);
            selectedProducts.push({
                'product_id': item.find('.selectLabExpertiseProduct').val(),
                'sample_quantity': item.find('.LabExpertiseProductSampleQuantity').val(),
                'sample_measurement_unit': item.find('.selectMeasurementUnit').val(),
                'sample_number': item.find('.LabExpertiseProductSampleNumber').val()
            })
        });

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('single-application/laboratory/get-products-view'),
            data: {
                saf_number: safNumber,
                selectedProducts: selectedProducts,
                type: 'add'
            },
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    $('.labExaminationCrudProductsContainer').html(result.data.productsView);

                    resetSelect2Values(true)
                }
            },
            error: function (request, status, error) {
                // showError(self, request);
            }
        });
    });

    $(document).on("click", ".deleteProductLabExamination", function () {
        if ($('.multipleProductBlockLabExamination').length == 1) {
            return false;
        } else {
            $(this).closest('.multipleProductBlockLabExamination').remove();
        }

        var selectedProducts = [];
        $('.multipleProductBlockLabExamination').each(function (key, item) {
            var item = $(item);
            selectedProducts.push({
                'product_id': item.find('.selectLabExpertiseProduct').val(),
                'sample_quantity': item.find('.LabExpertiseProductSampleQuantity').val(),
                'sample_measurement_unit': item.find('.selectMeasurementUnit').val(),
                'sample_number': item.find('.LabExpertiseProductSampleNumber').val()
            })
        });

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('single-application/laboratory/get-products-view'),
            data: {
                saf_number: safNumber,
                selectedProducts: selectedProducts,
                type: 'remove'
            },
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    $('.labExaminationCrudProductsContainer').html(result.data.productsView);

                    resetSelect2Values(true);
                }
            },
            error: function (request, status, error) {
                // showError(self, request);
            }
        });
    });

    $(document).on('click', '.getLabResult', function () {
        var self = $(this);
        $.ajax({
            type: 'POST',
            url: $cjs.admPath('single-application/laboratory/get-lab-result'),
            data: {
                sub_application_id: self.data('id'),
            },
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    $('.labExpertiseResultModalBody').html(result.data.result);
                    updateTotalPrice();
                    $('#laboratoryResultModal').modal('show')
                }
                updateTotalPrice();
            },
            error: function (request, status, error) {
                // showError(self, request);
            }
        });
    });

    $('#laboratoryCrudModal').on('hidden.bs.modal', function () {
        $('#selectLabExpertise').val('').trigger('change');
        labExpertiseCrudModalBody.html('');
        alertDivForLabExamination.hide();
        alertDivForLabExamination.text('');
    })
}

function blockEnabledOrDisabled(block, action, unitId = '') {
    block.find('.selectLabExpertiseProduct').prop('disabled', action);
    block.find('.LabExpertiseProductSampleQuantity').prop('disabled', action);
    block.find('.selectMeasurementUnit').val(unitId).trigger('change');
    block.find('.selectMeasurementUnit').prop('disabled', action);
    block.find('.LabExpertiseProductSampleNumber').prop('disabled', action);
}

function updateTotalPrice() {
    var value = 0;
    $('.price').each(function (i, v) {
        value += parseFloat($(v).text() || 0);
    });

    $('.labExaminationTotal').val(value);
}

function resetSelect2Values(onlyLabProducts = false) {
    $('.selectLabExpertiseProduct').select2({
        allowClear: true,
        placeholder: $trans("swis.saf.lab_examination.select_product_placeholder"),
        language: {
            noResults: function (params) {
                return $trans("swis.saf.lab_examination.no_results_found_products");
            }
        }
    });
    $('.selectMeasurementUnit').select2({
        allowClear: true,
        placeholder: $trans("swis.saf.lab_examination.select_measurement_unit_placeholder")
    });
    $('#selectLabExpertiseLaboratory').select2({
        allowClear: true,
        placeholder: $trans("swis.saf.lab_examination.select_laboratory_placeholder"),
        language: {
            noResults: function (params) {
                return $trans("swis.saf.lab_examination.no_results_found_laboratories");
            }
        }
    });

    if (!onlyLabProducts) {
        $('#selectLabExpertiseProductCustom').select2({allowClear: true, placeholder: ""});
        $('.indicatorSelect').select2({
            allowClear: true,
            placeholder: $trans("swis.saf.lab_examination.select_indicator_placeholder"),
            language: {
                noResults: function (params) {
                    return $trans("swis.saf.lab_examination.no_results_found_indicators");
                }
            }
        });
    }
}