/*
 * Product Create Form
 */
var optionsProductForm = {
    addPath: '/single-application/product',
    customSaveFunction: function () {
        $(".subFormModal.in").find('.btn').prop('disabled', true);
        return true;
    },
    customOptions: {
        onSaveSuccess: function (data) {
            $(".subFormModal.in").find('.btn').prop('disabled', false);
            productsTableList(data);
            checkInputsIsChanged();
        },
        onSaveError: function (error) {
            $(".subFormModal.in").find('.btn').prop('disabled', false);
        }
    }
};

/*
 * Product Update Form
 */
var optionsProductEditForm = {
    // addPath: '/single-application/sub-form',
    customSaveFunction: function () {
        $(".subFormModal.in").find('.btn').prop('disabled', true);
        return true;
    },
    customOptions: {
        onSaveSuccess: function (data) {
            $(".subFormModal.in").find('.btn').prop('disabled', false);
            productListReloadAfterEdit(data);
            checkInputsIsChanged();
        },
        onSaveError: function (error) {
            $(".subFormModal.in").find('.btn').prop('disabled', false);
        }
    }
};

/*
 * Init Form
 */
var $productFormApplication = new FormRequest('sub-form', optionsProductForm);

$productFormApplication.initEditPage = function () {
    $productFormApplication.initForm();
};

//
var productEditFormModal = $('#productListEdit');
var productCreateFormModal = $('#productList');
var productsTable = $('table.productList');
var productFormModalEditDiv = $('.sub-form-edit');
var productModalInit = false;
var deleteCheckedProductsButton = $("#deleteCheckedProducts");

$(document).ready(function () {

    productFormRender();

    productClone();

    productOrBatchRowEdit();

    productDelete();

    renderProductsBatchRow();

    totalAmountChangeSelectors();

});

function productClone() {
    $(document).on('click', '.s-table-clone', function () {

        var id = $(this).data('id');
        var self = $(this);

        modalConfirmClone(function (confirm) {

            if (confirm) {

                self.prop('disabled', true);
                self.tooltip('destroy');

                spinnerLoaderForButton(self, false);

                loadContent();

                if (id) {

                    $.ajax({
                        type: 'POST',
                        url: $cjs.admPath('single-application/product/clone'),
                        data: {id: id},
                        dataType: 'json',
                        success: function (result) {

                            if (result.status === 'OK') {

                                productsTableList(result.data, function () {
                                    $('.s-table-edit[data-id="' + result.data.productId + '"]').trigger('click');
                                });

                            }

                            self.prop('disabled', false);
                            spinnerLoaderForButton(self, true);
                            loadContent();
                        }
                    });
                }
            }
        });
    });
}

function productFormRender() {

    $(document).on('change', "[name='measurement_unit']", function () {
        $(this).closest('form').find('.batch_measurement_unit').html($("<option></option>").attr("value", $(this).val()).text($(this).find('option:selected').text()));
    });

    $('.subForm').click(function (e) {

        e.preventDefault();

        var self = $(this);
        var num = +productsTable.find('tbody tr').length + 1;
        num = isNaN(num) ? 1 : num;

        spinnerLoaderForButton(self);

        // Reset Form
        productCreateFormModal.find('form')[0].reset();

        $productFormApplication.init();

        productCreateFormModal.find('.select2').val(null).trigger('change');
        productCreateFormModal.find('.select2-ajax').val(null).trigger('change');
        productCreateFormModal.find('.form-group.has-error').removeClass("has-error");
        productCreateFormModal.find('div.form-error.form-error-text').text('').removeClass("form-error-text");

        // Reset Batch
        productCreateFormModal.find('.batches--block').addClass('hide');
        productCreateFormModal.find('table.products_batch').find('tbody tr').remove();

        productHsCodes(productCreateFormModal);
        productProducerCountry(productCreateFormModal, productModalInit);
        nettoFieldWithMeasurement(productCreateFormModal);

        productCreateFormModal.find('.autoincrement').val(num);
        productCreateFormModal.modal();

        productModalInit = true;

        setTimeout(function () {
            spinnerLoaderForButton(self, true);
        }, 300);

    });
}

function productOrBatchRowEdit() {

    $(document).on('click', '.s-table-edit', function () {

        var id = $(this).data('id');
        var self = $(this);

        self.prop('disabled', true);
        spinnerLoaderForButton(self);

        if (id) {
            $.ajax({
                type: 'POST',
                url: $cjs.admPath('single-application/product/edit'),
                data: {id: id, viewType: viewType},
                dataType: 'json',
                success: function (data) {

                    productFormModalEditDiv.html(data.html);

                    //
                    productEditFormModal = $('#productListEdit');
                    productEditFormModal.modal();

                    refAutocomplete();
                    select2Init(productEditFormModal);
                    productHsCodes(productEditFormModal);
                    productProducerCountry(productEditFormModal);
                    nettoFieldWithMeasurement(productEditFormModal);
                    datePickerInit(productEditFormModal);

                    // Update product total netto bruto total
                    updateProductTotals(data.productsTotals);

                    if (self.closest('tr').hasClass('disabled') || viewType === 'view') {

                        disableFormInputs(productEditFormModal.find('.modal-body'));

                        productEditFormModal.find('button.renderTr').prop('disabled', true);
                        productEditFormModal.find('.products_batch button').prop('disabled', true);
                        productEditFormModal.find('.productList-btn-edit').remove();
                        productEditFormModal.find('.s-table-delete').remove();
                    }

                    setTimeout(function () {

                        var $productFormEditApplication = new FormRequest('sub-form-edit', optionsProductEditForm);

                        $productFormEditApplication.initEditPage = function () {
                            $productFormEditApplication.initForm();
                        };

                        $productFormEditApplication.init();

                    }, 0);

                    self.tooltip('destroy');
                    self.prop('disabled', false);
                    spinnerLoaderForButton(self, true);
                }
            });
        }
    });
}

function productDelete() {

    $(document).on("click", "#checkAllProducts", function () {
        $(this).closest('table').find("input[type='checkbox']").not('[disabled]').prop("checked", $(this).prop("checked"));
    });

    $(document).on('click', '.s-table-delete', function () {

        var id = $(this).data('id');
        var module = $(this).closest('table').data('module');
        var self = $(this);

        self.prop('disabled', true);

        if (id) {

            if (module !== 'products_batch') {
                productModalConfirmDelete(function (confirm) {

                    if (confirm) {

                        loadContent();

                        productsDeleteAjax([id])

                    } else {
                        self.tooltip('destroy');
                        self.prop('disabled', false);
                    }
                });
            }
        }

        if (module === 'products_batch') {

            var listTable = self.closest('table');
            var batchBlock = self.closest('.batches--block');
            self.closest('tr').remove();

            var listTableTr = listTable.find('tbody tr');
            if (listTable.find('.incrementNumber').length > 0) {
                var incNumber = 1;
                $.each(listTableTr, function (k, v) {
                    $(this).find('.incrementNumber').text(incNumber);
                    $(this).attr('data-id', incNumber);
                    incNumber++;
                })
            }

            if (!listTable.find('tbody tr').length) {
                batchBlock.addClass('hide');
            }

            checkInputsIsChanged();
        }
    });

    deleteCheckedProductsButton.click(function () {

        var productIds = $(".productList tbody input:checkbox:checked").map(function () {
            if ($(this).val()) {
                return $(this).val();
            }
        }).get();

        if (productIds.length) {

            var self = $(this);
            productModalConfirmDelete(function (confirm) {

                if (confirm) {

                    loadContent();

                    productsDeleteAjax(productIds);

                    spinnerLoaderForButton(self);
                }
            });
        }

    });
}

function productsDeleteAjax(productIds) {

    $.ajax({
        type: 'POST',
        url: $cjs.admPath('single-application/product/delete'),
        data: {ids: productIds},
        dataType: 'json',
        success: function (result) {

            if (result.status === 'OK') {

                if (result.data) {

                    var backToUrl = result.data.backToUrl;

                    if (backToUrl) {
                        window.location.href = backToUrl;
                        window.location.reload();
                    }
                }

            }
        }
    });
}

var productModalConfirmDelete = function (callback, modalId) {

    var modalConfirmDiv = $('#deleteProductsModal');

    if (modalId !== undefined) {
        modalConfirmDiv = $('#' + modalId);
    }

    return confirmModalFunc(modalConfirmDiv, callback)
};

function productsTableList(data, callback) {

    productCreateFormModal.modal('hide');

    productsTable.closest('.table-responsive').removeClass('hide');
    deleteCheckedProductsButton.removeClass('hide');

    productsTable.find('tbody').append(data.html);

    updateProductTotals(data.productsTotals);

    if (typeof callback != 'undefined') {
        callback();
    }
}

function renderProductsBatchRow() {
    $(document).on('click', '.renderTr', function () {

        var parentForm = $(this).closest('form');
        var renderTable = parentForm.find('table.' + $(this).data('table'));
        var currentModal = $(this).closest('.modal.in');
        var self = $(this);

        var num = renderTable.find('tbody tr:last-child').data('id') + 1;
        num = isNaN(num) ? 1 : num;

        spinnerLoaderForButton(self);
        $.ajax({
            type: 'POST',
            url: $cjs.admPath('single-application/product/render-batch-inputs'),
            data: {
                num: num,
                incrementNumber: num,
            },
            dataType: 'json',
            success: function (result) {

                if (result.html) {
                    renderTable.append(result.html);

                    currentModal.animate({scrollTop: self.offset().top + 200}, 1);

                    var batchBlockDiv = self.closest('.modal-body').find('.batches--block');

                    if (batchBlockDiv.hasClass('hide')) {
                        batchBlockDiv.removeClass('hide');
                    }
                }

                var measurementSelectedOption = parentForm.find("[name='measurement_unit']");

                parentForm.find('.batch_measurement_unit').html($("<option></option>")
                    .attr("value", measurementSelectedOption.val())
                    .text(measurementSelectedOption.find('option:selected').text()));

                refAutocomplete();

                datePickerInit(renderTable.find('tr[data-id="' + num + '"]'));

                spinnerLoaderForButton(self, true);
                num++;
            }
        });
    });
}

function totalAmountChangeSelectors() {
    $(document).on('change', 'select[name="total_value_code"]', function () {
        totalAmountShowInNationalCurrency();
    });

    $(document).on('keyup', 'input[name="total_value"]', function () {
        totalAmountShowInNationalCurrency();
    });
}

function totalAmountShowInNationalCurrency() {

    var openedModal = $('.subFormModal.in');
    var totalAmountVal = openedModal.find('input[name="total_value"]').val();
    var totalAmountErrortDiv = openedModal.find('input[name="total_value"]').closest('div').find('.form-error');
    var totalAmountCodeVal = openedModal.find('select[name="total_value_code"]').val();
    var totalValueCodeErrorDiv = openedModal.find('#form-error-total_value_code');
    var totalAmountNational = openedModal.find('input[name="total_value_national"]');

    totalAmountNational.val('');
    if (totalAmountVal && totalAmountCodeVal) {

        spinnerLoadState(totalAmountNational);

        $(totalAmountErrortDiv)
            .removeClass('form-error-text')
            .text('')
            .closest('.form-group').removeClass('has-error');

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('single-application/product/total-value-national-currency'),
            data: {total_value: totalAmountVal, total_value_code: totalAmountCodeVal},
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {

                    $(totalValueCodeErrorDiv)
                        .removeClass('form-error-text')
                        .text('')
                        .closest('.form-group').removeClass('has-error');

                    totalAmountNational.val(result.data.totalNationalCurrency)
                }

                if (result.status === 'INVALID_DATA') {

                    $(totalValueCodeErrorDiv)
                        .addClass('form-error-text')
                        .text(result.errors.total_value_code)
                        .closest('.form-group').addClass('has-error');

                }

                spinnerLoadState(totalAmountNational, true);
            },
            error: function (error) {

                $(totalAmountErrortDiv)
                    .addClass('form-error-text')
                    .text($trans.get('core.loading.invalid_data'))
                    .closest('.form-group').addClass('has-error');

                spinnerLoadState(totalAmountNational, true);
            }
        })
    }
}

function nettoFieldWithMeasurement(divSelector) {

    if (typeof divSelector != 'undefined') {
        divSelector.find('.main_netto_weight').on('keyup', function () {
            var measurementTypeValue = divSelector.find('.quantity_measurement_1');

            if (measurementTypeValue.val() && divSelector.find(".quantity_measurement_1 option:selected").attr('data-code') === refKilogramCode) {
                divSelector.find('.main_quantity').val($(this).val())
            }
        });
    }
}

function updateProductTotals(productsTotals) {
    if (productsTotals !== 'undefined') {
        $.each(productsTotals, function (k, v) {
            $('input[name="saf[product][' + k + ']"').val(v)
        })
    }
}

function productHsCodes(openedModal) {

    var select2Ajax = openedModal.find('.hsCodes');
    var selfParentDiv = select2Ajax.closest('.modal-body');

    var fieldsData = select2Ajax.data('fields');
    var quantityForm_2 = selfParentDiv.find('.form-quantity-2');
    var measurementUnitSelect_1 = selfParentDiv.find('.quantity_measurement_1');
    var measurementUnitSelect_2 = selfParentDiv.find('.quantity_measurement_2');
    var quantityInput_1 = selfParentDiv.find('input[name="quantity"]');
    var quantityInput_2 = selfParentDiv.find('input[name="quantity_2"]');

    if (openedModal.attr('id') == 'productList') {
        select2Ajax.find('option').remove();
    }

    select2Ajax.select2({
        placeholder: $select2AutoCompleteLabel,
        ajax: {
            url: $cjs.admPath('single-application/product/hs-codes'),
            type: 'post',
            dataType: 'json',
            delay: 250,
            data: function (params) {

                var returnObject = {
                    q: params.term,   // search term
                    page: params.page
                };

                return returnObject;
            },

            processResults: function (data, page) {
                return {
                    results: data.items
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        minimumInputLength: 2,
        language: {
            inputTooShort: function (args) {
                return $trans('core.base.select2.enter_text_1') + " " + (args.minimum - args.input.length) + " " + $trans('core.base.select2.enter_text_2');
            }
        },
        templateResult: formatRepo,
        templateSelection: formatRepoSelection,
        allowClear: true
    });


    function formatRepoSelection(repo) {
        return repo.code || repo.text;
    }

    function formatRepo(repo) {
        if (repo.disabled) {
            return repo.text;
        }
        return "<span title='" + repo.long_name + "'>" + "(" + repo.code + ") " + repo.name + "</span>";
    }

    // Select
    select2Ajax.on('select2:select', function (e) {

        /*      measurementUnitSelect_1.html($("<option></option>")
                  .attr({"value": '', "selected": 'selected'}).attr({"data-code": ""}).text($select2AutoCompleteLabel));

                measurementUnitSelect_2.html($("<option></option>")
                  .attr({"value": '', "selected": 'selected'}).text($select2AutoCompleteLabel));*/

        measurementUnitSelect_1.val(null).trigger('change');
        measurementUnitSelect_2.val(null).trigger('change');

        quantityInput_2.val('');
        quantityInput_1.val('');

        // Products Batch
        selfParentDiv.find('.batch_measurement_unit').html($("<option></option>")
            .attr({"value": '', "selected": 'selected'}).text($select2AutoCompleteLabel));

        // quantityForm_2.addClass('hide');

        var info = e.params.data;

        if (typeof fieldsData !== "undefined" && fieldsData.length !== 0 && info !== undefined) {

            $.each(fieldsData, function (k, v) {
                $('input[name="' + v.to + '"]').val(info[v.from]);
                $('textarea[name="' + v.to + '"]').val(info[v.from]);
            });
        }

        if (info.quantity_value_1) {

            measurementUnitSelect_1.val(info.quantity_value_1);
            measurementUnitSelect_1.trigger('change')

            /*          measurementUnitSelect_1.html($("<option></option>")
                          .attr({"value": info.quantity_value_1, "selected": 'selected'})
                          .attr({"data-code": info.quantity_code_1})
                          .text(info.quantity_name_1));*/

            // if batch table is open add - Unit of Measurement
            selfParentDiv.find('.batch_measurement_unit').html($("<option></option>")
                .attr({"value": info.quantity_value_1, "selected": 'selected'}).text(info.quantity_name_1));
        }

        if (info.quantity_value_2) {

            /*      quantityForm_2.removeClass('hide');
                     measurementUnitSelect_2.html($("<option></option>")
                         .attr({"value": info.quantity_value_2, "selected": 'selected'}).text(info.quantity_name_2));*/

            measurementUnitSelect_2.val(info.quantity_value_2);
            measurementUnitSelect_2.trigger('change')
        }
    });

    // Delete selected clear all selected data
    select2Ajax.on('select2:unselect', function (e) {

        measurementUnitSelect_1.val(null).trigger('change');
        measurementUnitSelect_2.val(null).trigger('change');

        /*        measurementUnitSelect_1.html($("<option></option>")
                    .attr({"value": '', "selected": 'selected'}).text($select2AutoCompleteLabel));

                 measurementUnitSelect_2.html($("<option></option>")
                    .attr({"value": '', "selected": 'selected'}).text($select2AutoCompleteLabel));*/

        quantityInput_2.val('');
        quantityInput_1.val('');

        // Products Batch
        selfParentDiv.find('.batch_measurement_unit').html($("<option></option>")
            .attr({"value": '', "selected": 'selected'}).text($select2AutoCompleteLabel));

        // quantityForm_2.addClass('hide');

        if (typeof fieldsData !== "undefined" && fieldsData.length !== 0) {

            $.each(fieldsData, function (k, v) {
                $('input[name="' + v.to + '"]').val('');
                $('textarea[name="' + v.to + '"]').val('');
            });
        }

    });
}

function productProducerCountry(divSelector, modalInit = false) {

    var producerCountry = divSelector.find('.producer-country'),
        producerCode = divSelector.find('.producer-code'),
        producerName = divSelector.find('.producer-name');

    if (producerCountry.val() === '') {
        producerCode.prop('disabled', true);
        producerName.removeAttr('readonly');
    }

    if (!modalInit) {
        producerCountry.on('select2:select', function () {

            var self = $(this);
            var producerCountryVal = self.val() || '';

            if (producerCountryVal === '') {

                producerName.removeAttr('readonly');
                producerCode.prop('disabled', true);


            } else {

                producerCode.prop('disabled', false);
                producerName.attr('readonly', 'readonly');

                producerCode.find('option:not(:first-child)').remove();
                producerCode.val(null).trigger('change');
                producerName.val('');

                $.ajax({
                    type: 'POST',
                    url: $cjs.admPath('single-application/product/producer-code'),
                    data: {country_id: self.val()},
                    dataType: 'json',
                    success: function (result) {

                        if (result.status === 'OK') {
                            var items = result.data.items;

                            if (items.length) {
                                $.each(items, function (k, value) {
                                    var name = '(' + value.code + ') ' + value.name;

                                    producerCode.append($("<option></option>")
                                        .attr("value", value.id)
                                        .attr("data-code", value.code)
                                        .attr("data-name", value.name)
                                        .text(name));
                                });
                            }
                        }
                    }
                });

            }

        });

        producerCode.select2({
            placeholder: $trans('core.base.label.select'),
            allowClear: true,
            templateSelection: function (data, container) {
                // $(data.element).attr('data-custom-attribute', data.customValue);

                var selectedValue = producerCode.find(':selected').attr('data-code');
                var value = data.text;

                if (typeof selectedValue !== 'undefined') {
                    value = selectedValue;
                }

                return value;
            }
        });

        producerCountry.on('select2:unselect', function (e) {
            producerName.removeAttr('readonly').val('');
            producerCode.prop('disabled', true).val(null).trigger('change');
        });

        producerCode.on('select2:select', function (e) {
            producerName.val(producerCode.find(':selected').attr('data-name'));
        });

        producerCode.on('select2:unselect', function (e) {
            producerName.val('');
        });
    }

}

function productListReloadAfterEdit(data) {

    var productId = data['productId'];
    var updateProductTr = productsTable.find('.s-table-edit[data-id="' + productId + '"]').closest('tr');

    productEditFormModal.modal('hide');

    updateProductTotals(data.productsTotals);
    updateProductTr.replaceWith(data['html']);

    if (subApplicationProductsNotValid.length) {
        $.each(subApplicationProductsNotValid, function (k, v) {
            productsTable.find('button[data-id="' + v + '"]').closest('tr').addClass('not-valid-product');
        });
    }
}