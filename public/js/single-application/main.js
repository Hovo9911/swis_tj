$.ajaxSetup({
    headers: {
        'sNumber': $('#safNumber').val()
    }
});

// Select2 on modal content not working in firefox
$.fn.modal.Constructor.prototype.enforceFocus = function () {};

var safType = $('#safType').val();
var alertMessageDiv = $('#alertMessages');

/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        drawCallback: function () {
            $(".btn-remove").click(function () {
                $singleApplication.deleteRow([$(this).data('saf-number')])
            });
        },
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
        order: [],
        aaSorting: false,
        // responsive: true,
        lengthChange: true,
        iDisplayLength: 25
    },

    customOptions: {
        afterInitCallback: function () {
        },
        tableDataPath: 'single-application/table',
        searchPath: 'single-application',
        actions: {
            edit: {
                render: function (row) {
                    if (row.status_type !== safStatusTypeVoid) {
                        return '<a href="' + $cjs.admPath('single-application/edit/' + safType + '/' + row.regular_number + '') + '"  class="btn btn-edit" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.edit.button') + '"><i class="fa fa-pen"></i></a>'
                    }
                }
            },
            view: {
                render: function (row) {
                    return '<a href="' + $cjs.admPath('single-application/view/' + safType + "/" + row.regular_number) + '" class="btn btn-view" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.view.button') + '"><i class="fa fa-eye"></i></a>'
                }
            },
            custom: {
                render: function (row) {
                    return '<a href="' + $cjs.admPath('single-application/clone' + "/" + row.regular_number) + '" class="btn saf-clone btn-clone" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.clone.button') + '"><i class="fas fa-copy"></i></a>'
                }
            },
            delete: {
                render: function (row) {
                    if (row.status_type === safStatusTypeCreated) {
                        return '<button class="btn-remove" data-saf-number="' + row.regular_number + '" data-toggle="tooltip"  title="' + $trans('swis.base.tooltip.delete.button') + '"><i class="fa fa-times"></i></button>';
                    }
                }
            }
        },
        columnsRender: {
            export_country: {
                render: function (row) {
                    if (row) {
                        return '<span title="' + row.export_country + '">' + row.export_country + '</span>'
                    }
                }
            },
            import_country: {
                render: function (row) {
                    if (row) {
                        return '<span title="' + row.import_country + '">' + row.import_country + '</span>'
                    }
                }
            },
            status_type: {
                render: function (row) {
                    if (row) {
                        return $trans('swis.saf.status_type.' + row.status_type)
                    }
                }
            },
            regular_number: {
                render: function (row) {
                    if (row.status_type !== safStatusTypeCreated) {
                        return row.regular_number
                    } else {
                        return ''
                    }
                }
            }
        },

    }
};

/*
 * Options for Form Request
 */
var optionsForm = {
    deletePath: '/single-application/delete',
    searchPath: '/single-application/' + safRegime,
    addPath: '/single-application/create/' + safType,
    // rShowLoadingInvalidDataMessage:true
    customSaveFunction: function () {
        loadContent();
        alertMessageDiv.text('').removeClass('alert alert-success');
        return true;
    },
    customOptions: {
        onSaveSuccess: function (result) {

            if (result.backToUrl) {

                var bactToUrl = result.backToUrl;

                window.location.href = bactToUrl;

                if (bactToUrl.indexOf('#') !== -1) {
                    window.location.reload();
                }
            }

            if (result.message) {
                $error.show('form-error', []);
                alertMessageDiv.text(result.message).addClass('alert alert-success ');

                scrollToAlertMessage();
                loadContent();
            }
        },
        onSaveError: function (error) {

            if (typeof error.messages != 'undefined' && error.messages.length) {

                var messageStr = '';
                $.each(error.messages, function (k, v) {
                    messageStr += '<div class="alert alert-danger">' + v + '</div>'
                });

                alertMessageDiv.html(messageStr);
                scrollToAlertMessage();
            }

            loadContent();
        }
    },
};

/*
 * Init Datatable, Form
 */
var $singleApplicationTable = new DataTable('list-data', optionsTable);
var $singleApplication = new FormRequest('form-data', optionsForm);

//
var $form = $('#form-data');
var transitCountryMainDiv = $('.multipleFields.saf_transit_country');
var transitCountries;

$(document).ready(function () {

    // init
    $singleApplication.init();
    $singleApplicationTable.init();

    // -------

    safClone();

    sendSafData();

    deleteSaf();

    safExportXml();

    sendMessageSubApplication();

    viewRegimeAndByRegimeTypeCloseFields();

    authUserIsBroker();

    refAutocomplete();

    safMultipleFields();

    deleteMultipleRow();

    actionDelete();

    tabsInputsChangeKeyup();

    enableContent();
});

function enableContent() {
    $('.tabs-container').removeClass('loading-close');
}

// Disable inputs if saf has submitted subApplication
if (typeof hasSubmittedSubApplication != 'undefined' && hasSubmittedSubApplication) {
    $(function () {
        $('.saf-controlled-field ,#tab-transportation input,#tab-sides input,#tab-transportation select,#tab-sides select,#tab-products select').prop('disabled', true);

        setTimeout(function () {
            $('.plusMultipleFields').attr('disabled', 'disabled');
            $('.deleteMultipleRow').attr('disabled', 'disabled');
        }, 1000)
    });
}

function safClone() {
    $(document).on('click', '.btn.saf-clone', function () {
        $('.btn.saf-clone').addClass('disabled');
    })
}

function sendSafData() {
    $('.sendData').click(function (e) {
        var type = $(this).data('type');
        var hasNotSavedDocument = documentOpenedNotSaved();

        $('#statusType').val(type);

        if (!hasNotSavedDocument) {
            $('.mc-nav-button-save').trigger('click');
        }
    })
}

function deleteSaf() {

    $('.delete-saf-data').on('click', function () {

        modalConfirmDelete(function (confirm) {

            if (confirm) {
                loadContent()

                $.ajax({
                    type: 'POST',
                    url: $cjs.admPath('single-application/delete'),
                    dataType: 'json',
                    data: {type: safRegime},
                    success: function (result) {

                        if (result.status === 'OK') {
                            if (result.data) {
                                var backToUrl = result.data.backToUrl;
                                if (backToUrl) {
                                    window.location.href = backToUrl;
                                    if (backToUrl.indexOf('#') !== -1) {
                                        window.location.reload();
                                    }
                                }
                            }
                        }
                    }
                });
            }
        }, 'deleteSafModal');


    });
}

function safExportXml() {
    $('.exportSafXml').click(function () {
        $.ajax({
            type: 'POST',
            url: $cjs.admPath('single-application/export-saf'),
            dataType: 'json',
        });
    })
}

function sendMessageSubApplication() {

    var messageModal = $('#sendMessageModal');
    var notesBlock = $('.notes-block');
    var subApplicationId = 0;

    $('.sendMessage').click(function () {
        subApplicationId = $(this).data('id');
        messageModal.modal();
    });

    messageModal.find('.sendMessageSubApp').click(function () {

        if (subApplicationId) {

            var message = messageModal.find('textarea').val();
            var self = $(this);
            var formGroup = messageModal.find('.form-group');

            self.prop('disabled',true);

            $.ajax({
                type: 'POST',
                url: $cjs.admPath('single-application/send-message'),
                data: {message: message, subAppId: subApplicationId},
                dataType: 'json',
                success: function (result) {
                    if (result.status === 'OK') {

                        messageModal.modal('hide');
                        messageModal.find('textarea').val('');
                        self.prop('disabled',false);

                        if (result.data.html) {
                            formGroup.removeClass('has-error');
                            formGroup.find('.form-error').removeClass('form-error-text').text('');

                            notesBlock.html(result.data.html)
                        }

                    }else if (result.status === 'INVALID_DATA') {
                        self.prop('disabled',false);
                        $error.show('form-error',result.errors)
                    }
                },
                error: function (request, status, error) {
                    self.prop('disabled',false);
                }
            })
        }
    });

    messageModal.find('.closeMessageSubApp').click(function () {
        messageModal.find('textarea').val('');
        messageModal.find('.form-group').removeClass('has-error');
        messageModal.find('.form-error').removeClass('form-error-text').text('');
    })
}

function viewRegimeAndByRegimeTypeCloseFields() {
    if (typeof viewType != 'undefined') {

        if (viewType === 'view') {
            $form.find('input , textarea , select').removeAttr('name').prop('disabled', true);
            $form.removeAttr('method action id').find('.btn, button').not('a').not('.s-table-edit,.btn-pdf').remove();
            $('.input-group-addon').addClass('readonly');
        }

        setTimeout(function () {
            if (viewType === 'edit' && safCurrentStatus === safStatusTypeFormed) {
                if (safRegime === safRegimeImport || safRegime === safRegimeExport) {
                    var regimeBlock = $('#' + safRegime + 'er');
                    regimeBlock.find('input, select').attr('readonly', 'readonly').attr('tabindex', -1).addClass('readonly');
                    regimeBlock.find('.input-group-addon').addClass('readonly');
                }
            }
        }, 400);
    }
}

function authUserIsBroker() {

    if (typeof safCurrentStatus != 'undefined' && safCurrentStatus === safStatusTypeCreated && !$authUserIsBroker) {

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('broker/is-broker'),
            dataType: 'json',
            success: function (result) {

                if (result.status === 'OK') {

                    var safRegimePanelBlock = $('#' + safRegime + 'er');

                    disableFormInputs(safRegimePanelBlock, true);
                    loadContent(safRegimePanelBlock);

                    $.each(result.data.userData, function (k, v) {
                        $('input[name="saf[sides][' + safRegime + '][' + k + ']"]').val(v);

                        if (k === 'country') {
                            $('select[name="saf[sides][' + safRegime + '][' + k + ']"]').find("option[value=" + v + "]").attr('selected', true).trigger('change');
                        }

                        if (k === 'type') {
                            $('select[name="saf[sides][' + safRegime + '][' + k + ']"]').find("option[value=" + v + "]").attr('selected', true);
                        }

                        $('.input-group-addon').addClass('readonly');

                    });

                    loadContent(safRegimePanelBlock);
                    initTaxIdPassportType();
                }
            }
        });
    }
}

function safMultipleFields() {
    if (transitCountryMainDiv.data('fields') !== undefined) {

        // Transit Counties
        transitCountries = JSON.parse(JSON.stringify($('.saf_transit_country').data('fields')));

        $.each(transitCountries, function (index, value) {
            if (value) {
                var cloneSelect = transitCountryMainDiv.find('select.select2:first').closest('.clearfix').clone();
                cloneSelect.find('span').remove('span');
                cloneSelect.find('button').remove('button');
                cloneSelect.appendTo(transitCountryMainDiv).append("<button type='button' class='btn btn-danger deleteMultipleRow'><i class='fa fa-minus'></i></button>");

                //
                var newSelectRow = transitCountryMainDiv.find('select.select2:last');
                newSelectRow.select2();
                newSelectRow.val(value).trigger('change');
            }
        });

        transitCountriesIndexNumbersRefresh();
    }

    $(document).on('click', '.plusMultipleFields', function () {
        var formGroup = $(this).closest('.form-group');
        var cloneDiv, firstDiv, firstInput, lastInput ,lastDiv;

        // Transit Countries
        if (formGroup.find('select.select2:first').length > 0) {
            var currentSelect = transitCountryMainDiv.find('select:first');
            var currentSelectValue = currentSelect.val();
            var currentSelectParentDiv = currentSelect.closest('div');

            currentSelectParentDiv.find('span:first').removeClass('tr-exist')
            if(transitCountries.indexOf(currentSelectValue) === -1 && currentSelectValue){
                var clonedNewSelect = currentSelect.closest('.clearfix').clone();

                clonedNewSelect.find('span').remove('span');
                clonedNewSelect.find('button').remove('button');
                clonedNewSelect.appendTo(transitCountryMainDiv).append("<button type='button' class='btn btn-danger deleteMultipleRow'><i class='fa fa-minus'></i></button>");

                //
                var newSelectRow = transitCountryMainDiv.find('select.select2:last');
                select2Init(newSelectRow.closest('div'))
                newSelectRow.val(currentSelectValue).trigger('change');

                //
                currentSelect.val('').trigger('change');

                //
                transitCountries.push(currentSelectValue);

                //
                transitCountriesIndexNumbersRefresh();
            }else{
                currentSelectParentDiv.find('span:first').addClass('tr-exist');
            }

        // State Numbers
        }else{

            firstDiv = formGroup.find('.multipleFields > div:first');
            cloneDiv = firstDiv.clone();
            var newId = 'form-error-saf-transportation-state_number_customs_support-' + index + '-state_number';
            cloneDiv.find('.form-error').attr('id', newId);
            cloneDiv.find('.plusMultipleFields').after(' ' + '<button type="button" class="btn btn-danger deleteMultipleRow"><i class="fa fa-minus"></i></button>');
            cloneDiv.find('.plusMultipleFields').remove();
            firstInput = firstDiv.find('input:first');
            lastInput = firstDiv.find('input:last');

            if (!(firstInput.prop('required') || lastInput.prop('required')) && !(firstInput.val() !== '' || lastInput.val() !== '')) {
                firstDiv.addClass('has-error');
                return false;
            } else {
                firstDiv.removeClass('has-error');
                cloneDiv.removeClass('has-error');
            }

            cloneDiv.appendTo(formGroup.find('.multipleFields'));

            lastDiv = formGroup.find('.multipleFields > div:last');
            lastDiv.find('input:first').val(firstInput.val());
            lastDiv.find('input:last').val(lastInput.val());
            firstInput.val('');
            lastInput.val('');

            if (lastDiv.find('input').length > 0) {

                $.each(lastDiv.find('input'), function (k, v) {
                    if ($(this).data('name')) {
                        $(this).attr({
                            name: $(this).data('name').replace('_index_', index)
                        })
                    }
                });

                index++
            }
        }
    });

    // Disabled to select same country multiple times
    var previousSelectVal;
    $(document).on("select2:selecting", "[name='saf[transportation][transit_country][]']", function () {
        previousSelectVal = $(this).val();
    });

    $(document).on('select2:select', "[name='saf[transportation][transit_country][]']", function () {
        if (transitCountries.indexOf($(this).val()) !== -1) {
            $(this).val(previousSelectVal).trigger('change');
            return false;
        } else if(previousSelectVal) {

            transitCountries = jQuery.grep(transitCountries, function (value) {
                return value != previousSelectVal;
            });
            // transitCountries.push($(this).val());
        }
    });

    // State Numbers
    var index = 2;
    if ($('.state_number_customs_support').data('fields') !== undefined) {
        var fields = JSON.parse(JSON.stringify($('.state_number_customs_support').data('fields')));
        $.each(fields, function (key, value) {
            var stateNumberCustomsSupport = $('.state_number_customs_support').find('div:first');
            if (key > 1) {
                var cloneSelect = stateNumberCustomsSupport.clone();
                cloneSelect.find('input:first').val(value['state_number']);
                cloneSelect.find('input:last').val(value['customs_support']);
                cloneSelect.find('button').remove('button');
                cloneSelect.appendTo($('.state_number_customs_support')).append("<button type='button' class='btn btn-danger deleteMultipleRow'><i class='fa fa-minus'></i></button>");

                if (cloneSelect.find('input').length > 0) {
                    $.each(cloneSelect.find('input'), function (k, v) {
                        if ($(this).data('name')) {
                            $(this).attr({name: $(this).data('name').replace('_index_', index)})
                        }
                    });
                    var newId = 'form-error-saf-transportation-state_number_customs_support-' + index + '-state_number';
                    cloneSelect.find('.form-error').attr('id', newId);
                }
            } else {
                stateNumberCustomsSupport.find('input:first').val(value['state_number']);
                stateNumberCustomsSupport.find('input:last').val(value['customs_support']);

                if (stateNumberCustomsSupport.find('input').length > 0) {
                    $.each(stateNumberCustomsSupport.find('input'), function (k, v) {
                        if ($(this).data('name')) {
                            $(this).attr({name: $(this).data('name').replace('_index_', index)})
                        }
                    });
                    var newId = 'form-error-saf-transportation-state_number_customs_support-' + index + '-state_number';
                    stateNumberCustomsSupport.find('.form-error').attr('id', newId);
                }
            }
            index++
        });
    }
}

function transitCountriesIndexNumbersRefresh() {
    var transitSelects = transitCountryMainDiv.find('select:not(:first)');
    $.each(transitSelects,function (k,v) {
        var index = k + 1
        var selectParentDiv = $(this).closest('div');

        if(selectParentDiv.find('.transit-country-index-number').length){
            selectParentDiv.find('.transit-country-index-number').text(index)
        }else{
            selectParentDiv.append('<span class="transit-country-index-number">' + index + '</span>')
        }
    })
}

function deleteMultipleRow() {
    $(document).on('click', '.deleteMultipleRow', function () {
        var multipleFields = $(this).closest('.multipleFields');
        if (multipleFields.find('div.clearfix').length === 1) {
            return false;
        }

        var clearfixElement = $(this).closest('div.clearfix');
        clearfixElement.remove();

        multipleFields.find('div.clearfix:first').find('button').remove();
        multipleFields.find('div.clearfix:first').append('' + '<button type="button" class="btn btn-primary plusMultipleFields"><i class="fa fa-plus"></i></button> ')

        if (clearfixElement.find("[name='saf[transportation][transit_country][]']").val() !== undefined) {
            var itemPosition = transitCountries.indexOf(clearfixElement.find("[name='saf[transportation][transit_country][]']").val() )
            transitCountries.splice(itemPosition,1);
            transitCountriesIndexNumbersRefresh();
        }
    });
}

function actionDelete() {
    $(document).on('click', 'button.delete', function () {
        var listTable = $(this).closest('table');

        $(this).closest('tr').remove();

        var listTableTr = listTable.find('tbody tr');

        checkInputsIsChanged();

        if (listTable.find('.incrementNumber').length > 0) {
            var incNumber = 1;
            $.each(listTableTr, function (k, v) {
                $(this).find('.incrementNumber').text(incNumber);
                $(this).attr('data-id', incNumber);
                incNumber++;
            })
        }
    })
}

function checkInputsIsChanged() {

    $('#formIsChanged').val(1);

    if (safCurrentStatus !== safStatusTypeCreated && safCurrentStatus !== safStatusTypeDraft) {
        $('.send-sub-application').prop('disabled', true).removeAttr('data-id');

        $('.sendData[data-type="' + safStatusTypeFormed + '"]').text($trans('core.base.button.resend'))
    }
}

function tabsInputsChangeKeyup() {
    $('#tab-sides :input , #tab-transportation :input,.saf-controlled-field').keyup(function (e) {
        checkInputsIsChanged()
    });

    $('#tab-sides select, #tab-transportation select , select.saf-controlled-field').change(function (e) {
        checkInputsIsChanged()
    });
}

function scrollToAlertMessage() {
    if (alertMessageDiv.find('.alert').length || alertMessageDiv.hasClass('alert')) {
        animateScrollToElement(alertMessageDiv, 0, 1);
    }
}