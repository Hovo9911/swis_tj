/*
 * Options for Form Request
 */
var optionsForm = {
    searchPath: '/document',
    addPath: '/document/create',
    afterGetErrorResult: function (error) {
        if (typeof error.permission_denied != 'undefined' && error.permission_denied) {
            $('#permissionDeniedModal').modal('show');
        }
    },
};

/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
        "order": [[1, "desc"]],
        lengthChange: true,
        iDisplayLength: 25
    },
    customOptions: {
        afterInitCallback: function () {
        },
        tableDataPath: 'document/table',
        searchPath: 'document',
        columnsRender: {
            document_status: {
                render: function (row) {
                    if (row.document_status) {
                        return $trans('swis.document.' + row.document_status + '.title');
                    }
                }
            }
        },
        actions: {
            edit: {
                render: function (row) {
                    if (row.canEdit !== false && row.document_form !== 'electronic') {
                        return '<a href="' + $cjs.admPath('document/' + row.id + '/edit') + '" class="btn btn-edit" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.edit.button') + '"><i class="fa fa-pen"></i></a>'
                    }
                }
            },
            view: {
                render: function (row) {

                    var viewByAccessKey = '';
                    if (row.searched_by_access_password && row.canEdit === false) {
                        viewByAccessKey = '?accesskey=' + row.document_access_password;
                    }

                    return '<a href="' + $cjs.admPath('document/' + row.id + '/view' + viewByAccessKey) + '" class="btn btn-view" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.view.button') + '"><i class="fa fa-eye"></i></a>'
                }
            },
            custom: {
                render: function (row) {
                    if (row.document_file) {
                        return '<a target="_blank" href="' + row.document_file + '" class="btn btn-view" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.view.document') + '"><i class="far fa-file-pdf"></i></a>'
                    }
                }
            }
        }
    }
};

/*
 * Init Datatable, Form
 */
var $document = new FormRequest('form-data', optionsForm);
var $documentTable = new DataTable('list-data', optionsTable);

//
var $approveTypeAgency = 'agency';
var $approveTypeOther = 'other';

//
var documentStatus = $('#documentStatus');
var subjectApprovement = $('#subjectApp');
var approveType = $('#approveType');
var docTypeId = $('#docTypeId');
var approveAgency = $('#approveAgency');
var $formId = 'form-data';

$(document).ready(function () {

    // init
    $document.init();
    $documentTable.init();

    // --------------

    disableContentFields();

    getTypeOfDocument();

    authUserIsBroker();

    saveData();

    approveTypeAgencyChange();

    cancelAction();
});

function approveTypeAgencyChange() {

    approveType.change(function () {

        subjectApprovement.prop('disabled', false);
        approveAgency.val('');

        if ($(this).val() === $approveTypeOther) {
            subjectApprovement.prop('disabled', true);
            subjectApprovement.prop('checked', false);

        } else if ($(this).val() == '') {
            subjectApprovement.prop('disabled', true);
            subjectApprovement.prop('checked', false);
            approveAgency.val($(this).find(':selected').attr('data-tax-id'))
        } else {
            approveAgency.val($(this).find(':selected').attr('data-tax-id'))
        }

    });
}

function getTypeOfDocument() {

    var documentTypeInput = $('.documentTypeInput');
    var fieldsData = documentTypeInput.data('fields');

    if (documentTypeInput.length > 0) {
        documentTypeInput.typeahead({
            source: function (query, process) {
                return $.post($cjs.admPath("/document/type-of-document"), {
                    query: query,
                }, function (response) {

                    approveType.attr('readonly', 'readonly');
                    approveType.find('option').remove();

                    docTypeId.val('');

                    $.each(fieldsData, function (k, v) {
                        $('input[name="' + v.to + '"]').val('');
                    });

                    return process(response.items);
                });
            },
            displayText: function (data) {
                return '(' + data.code + ') ' + data.name;
            },
            afterSelect: function (data) {

                if (fieldsData !== undefined && fieldsData.length !== 0) {

                    var agencies = data.agencies;
                    approveAgency.val('');

                    // subjectApprovement.prop('disabled', false);

                    if (agencies.length) {
                        approveType.removeAttr('readonly');

                        approveType
                            .prepend($("<option></option>")
                                .attr("value", "")
                                .text($trans($selectLabel)));
                        subjectApprovement.prop('checked', false);

                        $.each(agencies, function (k, agency) {
                            approveType.append($("<option data-tax-id='" + agency.agency_code + "'></option>")
                                .attr("value", $approveTypeAgency)
                                .text(agency.agency_name))
                        });
                    }

                    approveType
                        .append($("<option></option>")
                            .attr("value", $approveTypeOther)
                            .text($trans('swis.document.label.other')));

                    $.each(fieldsData, function (k, v) {
                        $('input[name="document_type"]').val(data.code);
                        $('input[name="' + v.to + '"]').val(data[v.from]);
                    });
                }
            },

        });
    }
}

function authUserIsBroker() {

    if (typeof $saveMode != "undefined" && !$authUserIsBroker) {

        var holderInfoPanel = $("#holderInfo");

        if ($saveMode === $viewModeAdd) {

            $.ajax({
                type: 'POST',
                url: $cjs.admPath('broker/is-broker'),
                dataType: 'json',
                success: function (result) {

                    if (result.status === 'OK') {

                        disableFormInputs(holderInfoPanel, true);

                        $.each(result.data.userData, function (k, v) {

                            $('input[name="holder_' + k + '"]').val(v);

                            if (k === 'country') {
                                $('select[name="holder_' + k + '"]').find("option[value=" + v + "]").attr('selected', true).trigger('change');
                            }

                            if (k === 'type') {
                                $('select[name="holder_' + k + '"]').find("option[value=" + v + "]").attr('selected', true);
                            }

                        });

                        initTaxIdPassportType();
                    }
                }
            });
        }

        if ($saveMode === $viewModeEdit) {
            disableFormInputs(holderInfoPanel, true);
        }
    }
}

function saveData() {
    $(document).on('click', '.save-button', function () {
        documentStatus.val($(this).data('status'));

        $('#' + $formId).submit();
    });
}

function disableContentFields() {

    if (typeof $saveMode != 'undefined' && $saveMode === 'view') {
        $('.save-button').remove();
        $('.input-group-addon').addClass('readonly');
        disableFormInputs($('#' + $formId));
    }

    if (typeof $disabledFields != 'undefined' && $disabledFields) {
        disableFormInputs($('.tabs-container'));
        $('.input-group-addon').addClass('readonly');
    }
}

function cancelAction() {
    $('.button-cancel').click(function (e) {
        window.history.back();
    });
}