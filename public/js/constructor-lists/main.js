/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        order: [[1, "desc"]],
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
            $('td:eq(1)', nRow).addClass('text-center');
        },
        lengthChange: true,
        pageLength: 100
    },
    customOptions: {
        afterInitCallback: function () {
        },
        tableDataPath: 'constructor-lists/table',
        searchPath: 'constructor-lists',
        columnsRender: {
            list_type: {
                render: function (row) {
                    switch (row.list_type) {
                        case 1:
                            return 'Non Visible'
                        case 2:
                            return 'Editable';
                        case 3:
                            return 'mandatory';
                    }
                }
            }
        }
    }
};

/*
 * Options for Form Request
 */
var optionsForm = {
    searchPath: '/constructor-lists/' + constructorDocumentId,
    addPath: '/constructor-lists/create/' + constructorDocumentId,
};

/*
 * Init Datatable, Form
 */
var $constructorListsTable = new DataTable('list-data', optionsTable);
var $constructorLists = new FormRequest('form-data', optionsForm);

$(document).ready(function () {

    // init
    $constructorListsTable.init();
    $constructorLists.init();

    // ---------------

    constructorDocumentList()

    listType();

    safFields();

    checkAllFields();

});

function constructorDocumentList() {
    $('.document-list').on('change', function () {
        window.location.href = '/' + $cLng + '/constructor-lists/' + $(this).val();
    });
}

function checkAllCheckbox(checkbox, checkAllField) {
    $(checkbox).on('change', function () {
        var checkAll = true;

        $(checkbox).each(function (index, value) {
            if (!$(this).is(':checked')) {
                checkAll = false;
            }
        });

        $(checkAllField).prop('checked', checkAll)
    });
}

function listType() {
    $('#list_type').on('change', function () {

        var typeId = $(this).val();

        $('#form-data').find('input, select, textarea').prop('disabled', true);
        $('.tab-saf-fields-panel').html('');
        $('.loading-in-edit-mode').removeClass('hide');

        $.ajax({
            url: $(this).data('url'),
            type: 'post',
            dataType: 'json',
            delay: 250,
            data: {'typeID': typeId, 'constructorListId': listId},
            success: function (result, page) {

                $('.tab-saf-fields-panel').html(result['view']);
                $('.loading-in-edit-mode').addClass('hide');
                $('#form-data').find('input, select, textarea').prop('disabled', false);

                if (typeId === mandatoryType) {
                    $('.single-application-tabs').hide();
                } else {
                    $('.single-application-tabs').show();
                }
            }
        });
    });
}

function safFields() {
    $(document).on('change', '.saf_field_checkbox', function () {
        var checkAll = true;

        $('.saf_field_checkbox').each(function (index, value) {
            if (!$(this).is(':checked')) {
                checkAll = false;
            }
        });

        $('.check-all-saf-fields').prop('checked', checkAll)
    });

    checkAllCheckbox('.saf_field_checkbox', '.check-all-saf-fields')
    checkAllCheckbox('.mdm_field_checkbox', '.check-all-mdm-fields')
}

function checkAllFields() {
    $(document).on('click', '.check-all-fields', function () {
        var table = $(this).closest("table");
        var checked = $(this).is(':checked');

        table.find('tbody input[type=checkbox]').prop('checked', checked);
    });
}