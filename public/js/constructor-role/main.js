/*
 *  Options for Form Request
 */
var optionsForm = {
    addPath: '/constructor-roles/store',
};

/*
 * Init Form
 */
var $constructorRoles = new FormRequest('form-data', optionsForm);

//
var forbiddenIds = $('#forbiddenIDs');
var params = JSON.stringify({documentID: documentID, forbiddenIDs: forbiddenIds.val()});

$(document).ready(function () {

    // init
    $constructorRoles.init();

    // -------------

    constructorDocumentList();

    selectRole();

    removeBlock();

    formSubmit();

});

function formSubmit(){
    $("form").submit(function() {
        $(this).find(":input").filter( function(){ return !this.value; }).attr("disabled", "disabled");
        return true;
    });
}

function selectRole() {
    $('#select-role').on('select2:select', function (e) {
        $.post($cjs.admPath("constructor-roles/get-role-field"),
            {
                roleID: $("#select-role").val(),
                documentID: documentID
            },
            function (data) {
                $('.attributes').append(data['html']);
                $("#select-role").val(null).trigger('change');

                var roleID = JSON.parse(forbiddenIds.val());

                roleID.push(data['roleID']);
                forbiddenIds.val(JSON.stringify(roleID));

                var params = JSON.stringify({documentID: documentID, forbiddenIDs: forbiddenIds.val()});
                $('.select2-ajax').attr('data-params', params);
            })
    });
}

function removeBlock() {

    $(document).on('click', '.remove-block', function () {
        var roleID = $(this).data('id');

        $('#block-' + roleID).remove();

        var forbiddenIDs = JSON.parse(forbiddenIds.val());
        var index = forbiddenIDs.indexOf(roleID);

        if (index > -1) {
            forbiddenIDs.splice(index, 1);
        }

        forbiddenIds.val(JSON.stringify(forbiddenIDs));
        var params = JSON.stringify({documentID: documentID, forbiddenIDs: forbiddenIds.val()});
        $('.select2-ajax').attr('data-params', params);
    });

}

function constructorDocumentList() {
    $('.select2-ajax').attr('data-params', params);

    $('.document-list').on('change', function () {
        $('#search-filter').submit();
    });
}
