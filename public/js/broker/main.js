/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching:false,
        drawCallback:function(){},
        "order": [[1, "desc"]],
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
        lengthChange: true,
        iDisplayLength: 25

    },
    customOptions: {
        afterInitCallback: function () {},
        tableDataPath:'broker/table',
        searchPath:'broker',
    }
};

/*
 * Options for Form Request
 */
var optionsForm = {
    searchPath: '/broker',
    addPath: '/broker/create',
};

// Init Datatable, Form
var $brokersTable = new DataTable('list-data', optionsTable);
var $brokers = new FormRequest('form-data', optionsForm);

$(document).ready(function () {

    // init
    $brokersTable.init();
    $brokers.init();

    // --------

    brokerInfo();

});

function brokerInfo() {

    var legalEntityName = $('#legalEntityName');
    var address = $('#address');

    getCompanyInfoByTaxID($('#taxID'), function () {
        legalEntityName.val('');
        address.val('');
    }, function (result) {
        legalEntityName.val(result.data.company.name);
        address.val(result.data.company.address);
    });
}

