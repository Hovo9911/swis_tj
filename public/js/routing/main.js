/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        "order": [[1, "desc"]],
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
        lengthChange: true,
        iDisplayLength: 25
    },
    customOptions: {
        afterInitCallback: function () {
        },
        tableDataPath: '/routing/table',
        searchPath: '/routing',
        columnsRender: {
            regime: {
                render: function (row) {
                    if (row.regime) {
                        return $trans('swis.' + row.regime + '_title');
                    }
                    return ''
                }
            },
        }
    }
};

/*
 * Options for Form Request
 */
var optionsForm = {
    searchPath: '/routing',
    addPath: '/routing/create',
};

/*
 * Init Datatable, Form
 */
var $routingTable = new DataTable( 'list-data', optionsTable);
var $routing = new FormRequest('form-data', optionsForm);

//
var agencySelect = $('#agency');
var constructorDocumentSelect = $('#constructorDocuments');

$(document).ready(function () {

    // init
    $routingTable.init();
    $routing.init();

    // -----------

    getAgencyConstructorDocuments();

    multipleReferenceClassificator();

    isManualRouting();
});

function getAgencyConstructorDocuments() {

    agencySelect.change(function () {

        var self = $(this);
        var selectVal = self.val();

        select2Reset(constructorDocumentSelect)

        if (selectVal) {

            self.prop('disabled', true);
            constructorDocumentSelect.prop('disabled', true);

            $.ajax({
                type: 'POST',
                url: $cjs.admPath('routing/get-agency-documents'),
                data: {agency_id: self.val()},
                dataType: 'json',
                success: function (result) {

                    if (result.status === 'OK') {
                        if (typeof result.data.items != 'undefined' && result.data.items.length) {
                            $.each(result.data.items, function (k, v) {

                                constructorDocumentSelect
                                    .append($("<option></option>")
                                        .attr("value", v.id)
                                        .text(v.name));
                            })
                        }

                        self.prop('disabled', false);
                        constructorDocumentSelect.prop('disabled', false);
                    }
                }
            });
        }
    });
}

function multipleReferenceClassificator() {

    var refClassificatorMainDiv = $('.ref-classificators');
    var refSelectCount = refClassificatorMainDiv.find('.form-group').length + 1;

    $(document).on('click', '.multiple-ref-classificator', function (e) {

        var newSelectDiv = refClassificatorMainDiv.find('.form-group:first').clone();
        var selectName = refClassificatorMainDiv.data('name');

        // New Select row
        newSelectDiv.find('select').attr('name',selectName + '[' +refSelectCount+'][]');
        newSelectDiv.find('.select2-container').remove();
        newSelectDiv.find('option').removeAttr('selected');
        newSelectDiv.find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');
        newSelectDiv.find('.add-ref-classificator').toggleClass('add-ref-classificator btn-remove-classificator');
        newSelectDiv.find('.btn-remove-classificator').removeClass('hide');

        refClassificatorMainDiv.append(newSelectDiv);

        // last select2 init
        select2Init(refClassificatorMainDiv.find('.form-group:last'));

        refSelectCount++;
    }).on('click', '.btn-remove-classificator', function (e) {

        $(this).closest('.form-group').remove();

        e.preventDefault();
        return false;
    });

}

function isManualRouting() {
    $('.is-manual').change(function () {
        var ruleTextArea = $('.rule');
        if ($(this).is(':checked')) {
            ruleTextArea.prop('disabled', true).val('');
            ruleTextArea.closest('.form-group').removeClass('required');
        } else {
            ruleTextArea.prop('disabled', false);
            ruleTextArea.closest('.form-group').addClass('required');
        }
    })
}
