/*
 * options for Form request
 */
var optionsForm = {
    addPath: '/constructor-data-set',
    addCallback: function () {
        $('.remove-btn').remove();
    }
};

/*
 * Init for Form Request
 */
var $constructorDataSet = new FormRequest('saf-form', optionsForm);

//
var checkboxIsChanged = false;

$(document).ready(function () {

    // init
    $constructorDataSet.init();

    // ------------

    closeLoadBrowser();

    mdmFieldSearch();

    formSubmit();

    removeBtn();

});

// Close Load browser
$(window).bind('beforeunload', function () {
    if (checkboxIsChanged) {
        return $trans('core.want_to_leave');
    }
});

function formSubmit() {
    $("form").submit(function () {

        $(this).find(":input").filter(function () {
            return !this.value;
        }).attr("disabled", "disabled");

        return true;
    });
}

function mdmFieldSearch() {
    $('#mdmFiledSearch').on('select2:select', function (e) {

        $.post($cjs.admPath('constructor-data-set/get-mdm-field'),
            {
                mdmFieldID: $("#mdmFiledSearch").val(),
                documentID: documentID,
                lastIncrementedID: $('#lastIncrementID').val()
            },
            function (data) {
                $('#tbody').append(data['row']);
                $("#select2Box-" + data['field_id']).select2();
                $("#lastIncrementID").val(data['lastIncrementedID'])
            });

        $(this).val(null).trigger('change');
    });
}

function closeLoadBrowser() {

    $("#saf-form :input").change(function () {
        checkboxIsChanged = true;
    });

    $('.mc-nav-button-save').on('click', function () {
        checkboxIsChanged = false;
    });

    $('.document-list').on('change', function () {
        $('#search-filter').submit();
    });
}

function removeBtn() {
    $(document).on('click', '.remove-btn', function () {
        checkboxIsChanged = true;

        var trID = $(this).data('id');

        $('#tr-' + trID).remove();
    });
}