/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        order: [[1, "desc"]],
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
        lengthChange: true,
        pageLength: 100
    },
    customOptions: {
        afterInitCallback: function () {
        },
        tableDataPath: 'constructor-actions/table',
        searchPath: 'constructor-actions'
    }
};

/*
 * Options for Form Request
 */
var optionsForm = {
    searchPath: '/constructor-actions/' + constructorDocumentId,
    addPath: '/constructor-actions/create/' + constructorDocumentId,
};

/*
 * Init Datatable, Form
 */
var $constructorActionsTable = new DataTable('list-data', optionsTable);
var $constructorActions = new FormRequest('form-data', optionsForm);

$(document).ready(function () {

    // init
    $constructorActions.init();
    $constructorActionsTable.init();

    // ----

    constructorDocumentList();

    iCheck();

    specialTypes();

});

function constructorDocumentList() {
    $('.document-list').on('change', function () {
        window.location.href = '/' + $cLng + '/constructor-actions/' + $(this).val();
    });
}

function specialTypes() {
    $("#special-types-select").change(function () {

        if ($(this).val() === '2') {
            $("#templateSection").removeClass('hide');
            $(".template-checkbox").prop("disabled", false);
        } else {
            $("#templateSection").addClass('hide');
            $(".template-checkbox").prop("disabled", true);
        }
    });

}

function iCheck() {

    var iChecks = $('.i-checks');

    if (iChecks.length) {
        iChecks.iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    }
}