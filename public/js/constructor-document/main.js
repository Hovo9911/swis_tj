
/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        order: [[1, "desc"]],
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
        lengthChange: true,
        pageLength: 100
    },
    customOptions: {
        afterInitCallback: function () {
        },
        tableDataPath: 'constructor-documents/table',
        searchPath: 'constructor-documents',
        actions: {
            edit: {
                render: function (row) {
                    return '<a href="' + $cjs.admPath('constructor-documents/' + row.id + '/edit') + '" class="btn btn-edit" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.edit.button') + '"><i class="fa fa-pen"></i></a>'

                }
            },
            custom: {
                render: function (row) {
                    return '<a href="' + $cjs.admPath('constructor-documents/export/' + row.id) + '" class="btn btn-view" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.view.button') + '"><i class="fa fa-file-excel"></i></a>'
                }
            }
        }

    }
};

/*
 * Options for Form Request
 */
var optionsForm = {
    searchPath: '/constructor-documents',
    addPath: '/constructor-documents/create',
};

/*
 * Init Datatable, Form
 */
var $constructorDocumentsTable = new DataTable('list-data', optionsTable);
var $constructorDocuments = new FormRequest('form-data', optionsForm);

$(document).ready(function () {

    // init
    $constructorDocumentsTable.init();
    $constructorDocuments.init();

});
