//!!!!!!!!Don't forget to remove csrf verification for upload action!!!!!!!!!!

$gUploader = {};
$gUploader.className = 'g-upload';

$gUploader.beforeUpload = function(self){};
$gUploader.afterUpload = function(self){};

$gUploader.onX = function(self){
    $(".g-upload-x", self.closest(".g-upload-container")).click(function(){
        $(this).closest(".g-uploaded").remove();
    });
};

$gUploader.onSuccess = function(response, self) {
    var functionNameForAddElement  = (self.attr('multiple') == undefined ) ? 'html' : 'append';
    for (var i in response.data) {
        $(".g-uploaded-list", self.closest(".g-upload-container"))[functionNameForAddElement](
            '<div class="register-form__clear-box fs14 g-uploaded">' +
            response.data[i].label+
            '<input type="hidden" class="g-uploaded-file-name" name="' + self.attr('name') + '" value="' + response.data[i].file_name + '" />' +
            ' <button type="button" class="register-form__clear-btn sprite dib fs12 g-upload-x"> <i class="fa fa-times"></i> </button>' +
            '</div>'
        );
    }
};

$gUploader.onError = function(response, self) {
    $(".form-error-files", self.closest(".g-upload-container")).text(response.message);
};

$gUploader.onCustomResponse = function(response, self){
    if (typeof response.message != 'undefined') {
        $(".form-error-files", self.closest(".g-upload-container")).text(response.message);
    } else {
        $(".form-error-files", self.closest(".g-upload-container")).text("Something went wrong");
    }
};

$gUploader.init = function(selector){
    if(typeof selector != 'undefined'){
        this.className = selector;
    }
    var uploader = this;
    $("." + this.className).not('.has-uploader').on('change', function() {
        var self = $(this);

        uploader.beforeUpload(self);
        self.attr('disabled', 'disabled');
        $(".uploader-loading", self.closest(".g-upload-box")).show();

        var data = new FormData();
        $.each(self[0].files, function(i, file){
            data.append('files['+i+']',  file);
        });

        var progressBarFileUploader = $(this).closest('.g-upload-container').find('.progressBarFileUploader');

        if (progressBarFileUploader.length > 0) {
            progressBarFileUploader.show();
            progressBarFileUploader.val(0);
            $(".form-error-files", self.closest(".g-upload-container")).text('');
        }

        data.append('conf', self.data('conf'));
        $.ajax({
            data: data,
            url: self.data('action'),
            type: 'post',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            xhrFields: {
                withCredentials: true
            },
            xhr: function () {
                var jqXHR = null;
                if ( window.ActiveXObject ) {
                    jqXHR = new window.ActiveXObject( "Microsoft.XMLHTTP" );
                } else {
                    jqXHR = new window.XMLHttpRequest();
                }

                jqXHR.upload.addEventListener( "progress", function ( e ) {
                    if ( e.lengthComputable ) {
                        var percentComplete = Math.round( (e.loaded * 100) / e.total );
                        if (progressBarFileUploader.length > 0) {
                            progressBarFileUploader.val(percentComplete)
                        }
                    }
                }, false );
                return jqXHR;
            },
            success: function (response) {
                $(self).removeAttr('disabled');

                if (response.status == 'OK') {
                    uploader.onSuccess(response, self);
                } else if (response.status == 'INVALID_DATA') {
                    uploader.onError(response, $(self));
                } else {
                    uploader.onCustomResponse(response, self);
                }
                self.val('');
                uploader.onX(self);
                $(".uploader-loading", self.closest(".g-upload-box")).hide();
                uploader.afterUpload(self);
                progressBarFileUploader.hide();
            },
            error: function(response){
                $(self).removeAttr('disabled');
                $(".uploader-loading", self.closest(".g-upload-box")).hide();
                uploader.onCustomResponse(response, self);
                progressBarFileUploader.hide();
            }
        });

        $gUploader.onX(self);
    });

    $(".g-upload").not('.has-uploader').addClass('has-uploader');
};

$(document).ready(function(){
    $gUploader.init();
    $('.g-upload').each(function(){
        $gUploader.onX($(this));
    });

});