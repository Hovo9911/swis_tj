/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
        order: [[1, "desc"]],
        lengthChange: true,
        pageLength: 100
    },
    customOptions: {
        afterInitCallback: function () {
        },
        tableDataPath: 'constructor-states/table',
        searchPath: 'constructor-states'
    }
};

/*
 * Options for Form Request
 */
var optionsForm = {
    searchPath: '/constructor-states/' + constructorDocumentId,
    addPath: '/constructor-states/create/' + constructorDocumentId,
};

// Init Datatable, Form
var $constructorStateTable = new DataTable('list-data', optionsTable);
var $constructorState = new FormRequest('form-data', optionsForm);

$(document).ready(function () {

    // init
    $constructorState.init();
    $constructorStateTable.init();

    // ----------

    constructorDocumentList();

    statesChange();

});

function constructorDocumentList() {
    $('.document-list').on('change', function () {
        window.location.href = '/' + $cLng + '/constructor-states/' + $(this).val();
    });
}

function statesChange() {
    $('input[name="state_type"]').change(function () {

        var currentVal = $(this).val();
        var stateApprovedStatus = $('#stateApprovedStatus');

        if (currentVal === 'end') {
            stateApprovedStatus.closest('.form-group').removeClass('hide');
        } else {
            stateApprovedStatus.closest('.form-group').addClass('hide');
        }
    })
}