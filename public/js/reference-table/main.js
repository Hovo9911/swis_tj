/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        "order": [[1, "desc"]],
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
        lengthChange: true,
    },

    customOptions: {
        afterInitCallback: function () {

        },
        actions: {
            edit: {
                render: function (row) {
                    return '<a href="' + $cjs.admPath('reference-table/' + row.id + '/edit') + '" class="btn btn-edit"  data-toggle="tooltip" title="' + $trans('swis.base.tooltip.edit.button') + '"><i class="fa fa-pen"></i></a>'
                }
            },
            clone: {
                render: function (row) {
                    return '<a href="' + $cjs.admPath('reference-table/' + row.id + '/clone') + '" class="btn btn-success btn-xs btn--icon"  data-toggle="tooltip" title="' + $trans('swis.base.tooltip.clone.button') + '"><i class="fas fa-copy"></i></a>'
                }
            },
            view: {
                render: function (row) {
                    return '<a href="' + $cjs.admPath('reference-table/' + row.id + '/view') + '" class="btn btn-view"  data-toggle="tooltip" title="' + $trans('swis.base.tooltip.view.button') + '"><i class="fa fa-eye"></i></a>'
                }
            },
        },
        tableDataPath: 'reference-table/table',
        searchPath: 'reference-table',
    }
};

/*
 * Options for Form Request
 */
var optionsForm = {
    searchPath: '/reference-table',
    addPath: '/reference-table/create',
};

/*
 * Init Datatable, Form
 */
var $referenceTableForm = new FormRequest('form-data', optionsForm);
var $referenceTable = new DataTable('list-data', optionsTable);

//
var structureTableBody = $('#reference-structure');
var form = $('#form-data');
var formKey = $('.mc-form-key');

$(document).ready(function () {

    // init
    $referenceTableForm.init();
    $referenceTable.init();

    // ----------------

    moreStructure();

    selectedAgencies();

    selectAgencyPermission();

    checkAllAgencies();

    fieldTypeParameters();

    autoCompleteReferenceFields();

    sortOrderUnique();

    importFormat();

    disableFieldsIfReferenceInactive();

});

// NOT USED
function formIsChanged() {

    if (formKey.length && formKey.val() !== '0') {

        var currentFormData = form.serialize();
        $('.mc-nav-button-save').click(function (e) {

            var newFormData = form.serialize();

            if (currentFormData == newFormData) {

                alert('Nothing Changed');

                e.stopImmediatePropagation();
            }
        });
    }
}

function moreStructure() {

    $('.btn-more').click(function (e) {

        e.preventDefault();

        var self = $(this);
        var lastSortOrderNum = +structureTableBody.find('tr:last .sort_order').val() + 1;

        spinnerLoaderForButton(self);

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('reference-table/render-table'),
            data: {i: lastSortOrderNum},
            dataType: 'json',
            success: function (result) {

                if (result.status === 'OK') {
                    structureTableBody.append(result.data.html);
                }

                animateScrollToElement(structureTableBody.find('tr:last'), 120, 300);
                spinnerLoaderForButton(self, true);
            }
        });
    });

    $(document).on('click', '.btn-remove', function (e) {

        e.preventDefault();

        $(this).closest('tr').remove();

        return false;
    });
}

function selectedAgencies() {
    var checkedAgencies = $('.agency-table input[type="checkbox"]:checked');
    var allAgencyRadio = $('.agency-table input[type="radio"]');
    var allAgencyShowOnlySelf = $('.agency-table .show_only_self');

    allAgencyRadio.prop('disabled', true);
    allAgencyShowOnlySelf.prop('disabled', true);

    $.each(checkedAgencies, function (k, v) {
        $(this).closest('tr').find('input[type="radio"]').prop('disabled', false);
        $(this).closest('tr').find('.show_only_self').prop('disabled', false);
    });
}

function selectAgencyPermission() {
    $('.agency-table input[type="checkbox"]').not('.show_only_self').change(function () {

        $(this).closest('tr').find('input[type="radio"]').prop('checked', this.checked);

        if (!$(this).is(':checked')) {
            $(this).closest('tr').find('.show_only_self').prop('checked', false);
        }

        selectedAgencies();
    });
}

function checkAllAgencies() {
    $('#checkAllAgencies').click(function () {
        var agencyTable = $(this).closest('table');

        agencyTable.find('input[type="checkbox"]').not('.show_only_self').prop('checked', this.checked);

        if ($(this).is(':checked')) {
            agencyTable.find('input[type="radio"]').not(':checked').prop('checked', true);
        } else {
            agencyTable.find('input[type="radio"]').prop('checked', false);
            agencyTable.find('.show_only_self').prop('checked', false)
        }
    })
}

function fieldTypeParameters() {

    $(document).on('change', '.fieldType', function () {

        var self = $(this);
        var selfTr = self.closest('tr');
        var mlCheckbox = selfTr.find('input[type="checkbox"].mlInput');
        var fieldType = self.val();

        self.prop('disabled', true);
        mlCheckbox.prop('disabled', true).prop('checked', false);
        if (fieldType === 'text') {
            mlCheckbox.prop('disabled', false);
        }

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('reference-table/render-table-parameter'),
            data: {type: fieldType, i: $(this).data('i'), current: formKey.val()},
            dataType: 'json',
            success: function (result) {

                if (result.status === 'OK') {

                    self.prop('disabled', false);
                    selfTr.find('.parameter-content').html(result.data.html)

                    datePickerInit(structureTableBody.find(selfTr));

                    if (fieldType == 'select' || fieldType == 'autocomplete') {
                        select2Init(selfTr)
                    }
                }
            }
        });
    })
}

// NOT USED
function structureFieldsDescription() {

    $(document).on('change', '.structureField', function () {

        var self = $(this);
        var descTextarea = self.closest('tr').find('.fieldsDescription');

        descTextarea.removeClass('hide');

        descTextarea.val(function () {
            if (self.is(':checked')) {
                return descTextarea.val() + "{" + self.data('field-name') + "}";
            } else {
                descTextarea.val(descTextarea.val().replace("{" + self.data('field-name') + "}", ''));

                return descTextarea.val();
            }
        });
    });
}

function autoCompleteReferenceFields() {

    $(document).on('change', '.autoCompleteReference', function () {

        var self = $(this);
        var selfTr = self.closest('tr');
        var fieldType = self.closest('tr').find('.fieldType').val();
        var rowFieldName = self.closest('tr').find('.ref-row-field-name');
        var selfValName = self.find("option:selected").text();

        selfValName = replaceAll(selfValName, ' ', '_');
        selfValName = replaceAll(selfValName, '/', '_');
        selfValName = selfValName.toLowerCase();

        if (fieldType === 'autocomplete') {

            $.ajax({
                type: 'POST',
                url: $cjs.admPath('reference-table/render-table-fields'),
                data: {id: $(this).val(), i: $(this).data('i')},
                dataType: 'json',
                success: function (result) {

                    if (result.status === 'OK') {
                        selfTr.find('.structure-fields').html(result.data.html)
                        // rowFieldName.val(selfValName);
                    }
                }
            });
        }

        /*if (fieldType === 'select') {
            rowFieldName.val(selfValName);
        }*/
    })
}

function sortOrderUnique() {

    var saveBtn = $('.mc-nav-button-save');

    $(document).on('input', '.sort_order', function () {
        var allSortOrders = $('.sort_order');
        var sortOrdersValues = [];
        var self = $(this);

        saveBtn.prop('disabled', false);

        allSortOrders.not($(this)).each(function () {
            sortOrdersValues.push($(this).val());
        });

        self.closest('.form-group').removeClass('has-error');

        if ($.inArray(self.val(), sortOrdersValues) != '-1') {
            self.closest('.form-group').addClass('has-error');
            saveBtn.prop('disabled', true);
        }
    });
}

function importFormat() {
    $('input[name="manually_edit"]').change(function () {
        $('.impFormat').prop('disabled', !+$(this).val());
    })
}

function disableFieldsIfReferenceInactive() {
    if (typeof referenceTableStatus != 'undefined' && referenceTableStatus === '2' && !clone) {
        var refTabContent = $('.referenceFormData .tab-content');
        refTabContent.find(":input").prop("disabled", true);
    }
}

