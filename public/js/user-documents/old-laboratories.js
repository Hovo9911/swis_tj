
function viewActions() {
    $(document).on('click', '.collapseLabCreate', function () {

        if ($(this).find('i').hasClass('fa-chevron-left')) {
            $(this).find('i').removeClass('fa-chevron-left');
            $(this).find('i').addClass('fa-chevron-down');
        } else {
            $(this).find('i').removeClass('fa-chevron-down');
            $(this).find('i').addClass('fa-chevron-left');
        }

    });

    $('#lab_goods').on('select2:select select2:unselect', function () {

        var url = $(this).data('url');
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            delay: 250,
            data: {id: $(this).val()},
            success: function (data, page) {
                $('#lab_goods_batches').find('option').remove();
                $.each(data, function (index, value) {
                    var newOption = new Option($trans(value.batch_number), value.id, false, false);
                    $('#lab_goods_batches').append(newOption).trigger('change');
                });
            },
            error: function (xhr, status, error) {
                $('#lab_goods_batches').find('option').remove();
            },
            cache: true
        })

    });

    $(document).on('click', '#processBtn', function (event) {

        event.preventDefault();

        $('.loading_laboratories_actions').removeClass('hide');
        $('.laboratories_actions').addClass('hide');

        var data = {};
        $.each($('.process_param'), function (index, value) {
            data[$(value).attr('name')] = $(value).val();
        });

        $.ajax({
            url: $(this).data('url'),
            type: 'post',
            dataType: 'json',
            delay: 250,
            data: data,
            success: function (result, page) {
                $('.loading_laboratories_actions').addClass('hide');
                $('.laboratories_actions').removeClass('hide').show();
                $("#totalPrice").val(0);
                $('.laboratories_actions tbody').text('').append(result);
            },
            cache: true
        })

    });

    $(document).on('click', '.save_laboratory_indicators', function (event) {

        event.preventDefault();
        var data = {};
        var checkedLAbs = [];

        $.each($('.checkIndicator'), function (k, v) {
            if ($(this).is(':checked')) {
                checkedLAbs.push($(this).attr('data-lab'))
            }
        });

        $.each($('.saveParam'), function (index, value) {
            if ($(value).attr('type') == 'checkbox') {
                if ($(value).is(':checked')) {
                    data[$(value).attr('name')] = $(value).val();
                    data['lab_id'] = checkedLAbs;
                }
            } else {
                data[$(value).attr('name')] = $(value).val();
            }
        });

        loadContent(loadContentDiv);

        $.ajax({
            url: $(this).data('url'),
            type: 'post',
            dataType: 'json',
            delay: 250,
            data: data,
            success: function (result, page) {
                if (result.status === 'OK') {

                    location.reload();
                } else if (result.status === 'INVALID_DATA') {

                    loadContent(loadContentDiv);

                    if (result.errors) {
                        $error.show('form-error', result.errors);
                    }

                    toastr.error($trans.get('core.loading.invalid_data'));
                }
            },
            cache: true
        })

    });


    $(document).on('click', '.editLabIndicator', function (event) {

        event.preventDefault();
        $('.data-block').addClass('hide');
        $('.loading-in-edit-mode').removeClass('hide');
        $.ajax({
            url: $(this).data('url'),
            type: 'post',
            dataType: 'json',
            delay: 250,
            data: {},
            success: function (result, page) {
                if (result.status === 'OK') {
                    $('.data-block').removeClass('hide');
                    $('.loading-in-edit-mode').addClass('hide');
                    $('.createEditBlock').empty();
                    $('.createEditBlock').html(result.body);
                    $('.select2_edit_view').select2();
                    checkSaveButtonVisible();
                    $('.editing_title').removeClass('hide');
                    $('.create_title').addClass('hide');
                    $('#collapseLabCreate').addClass('in');
                } else if (result.status === 'INVALID_DATA') {
                    if (result.errors) {
                        $error.show('form-error', result.errors);
                    }
                    toastr.error($trans.get('core.loading.invalid_data'));
                    return false;
                }
            },
            cache: true
        })

    });

    $(document).on('click', '.checkIndicator', function () {

        $(this).val(1);
        var price = parseInt($(this).closest('tr').find('.price_td').text());
        var totalPrice = parseInt($("#totalPrice").val());
        totalPrice = ($(this).is(':checked')) ? totalPrice + price : totalPrice - price;

        $("#totalPrice").val(totalPrice);

        checkSaveButtonVisible()

    });

    $(document).on('click', ".reset_form_edit_laboratory", function (event) {

        event.preventDefault();

        $('.saveParam').not('.saf_number_field').not('.application_id_field').val('').trigger('change');
        $('.checkIndicator').prop('checked', false);
        $('.laboratories_actions').hide();
        $('.editing_title').addClass('hide');
        $('.create_title').removeClass('hide');
        $('.save_laboratory_indicators').attr('data-url', labStoreUrl).addClass('hide');
        $(this).hide();

    });
}

function checkSaveButtonVisible() {

    var hideSaveBtn = true;
    $('.checkIndicator').each(function (index, value) {

        if ($(value).is(':checked')) {
            hideSaveBtn = false;
        }

    });

    if (!hideSaveBtn) {
        $('.save_laboratory_indicators').removeClass('hide');
    } else {
        $('.save_laboratory_indicators').addClass('hide');
    }

}

function renderExaminationInputs() {

    $('.plusMultipleFieldsExamination').click(function () {

        var self = $(this);
        var num = examinationList.find('tbody tr:last-child').data('num') + 1;
        num = isNaN(num) ? 1 : num;

        spinnerLoaderForButton(self);

        $.ajax({
            type: 'POST',
            url: $cjs.admPath(userDocUrl + 'render-examination-inputs'),
            data: {num: num, applicationId: applicationId},
            dataType: 'json',
            success: function (result) {

                if (result.status === 'OK') {
                    examinationList.find('tbody').append(result.data.html);

                    // $('.delete-examination').hide();

                    select2Init();

                    refAutocomplete();

                    spinnerLoaderForButton(self, true);
                }
            }
        });
    });

}

function storeExamination() {

    $(document).on('click', '.storeExamination', function (e) {

        e.preventDefault();

        var productInfo = $(this).closest('tr').find(':input');
        var self = $(this);
        var selfTr = self.closest('tr');

        var typeOfStones = $(this).closest('tr').find('#typeOfStones').val();

        self.prop('disabled', true);

        $error.show('form-error', []);

        $.ajax({
            type: 'POST',
            url: $cjs.admPath(userDocUrl + 'store-examination'),
            data: productInfo,
            headers: {
                'sNumber': safNumber,
                'typeOfStones' :typeOfStones
            },
            dataType: 'json',
            success: function (result) {
                var num = selfTr.data('num');
                if (result.status === 'OK') {

                    selfTr.find('input').prop('readonly', true);
                    selfTr.find('.select2').prop('disabled', true);
                    var td = self.closest('td');
                    self.remove();
                    self.prop('disabled', false);

                    td.closest('tr').addClass('examination-'+result.data.examinationId);
                    td.empty();
                    var html = '<button class="btn-edit edit-examination" data-toggle="tooltip" data-num="'+num+'" data-examination-id="'+result.data.examinationId+'" title="'+$trans('swis.base.tooltip.edit_examination.button')+'"><i class="fas fa-pen"></i></button> ';
                    html += '<button class="btn-remove delete-examination" data-toggle="tooltip" data-num="'+num+'" data-examination-id="'+result.data.examinationId+'" title="'+$trans('swis.base.tooltip.delete_examination.button')+'"><i class="fas fa-times"></i></button>';

                    td.html(html);


                    $("input[name='saf[examination][platinium]']").val(result.data.platinumPrice);
                    $("input[name='saf[examination][gold]']").val(result.data.goldPrice);
                    $("input[name='saf[examination][palladium]']").val(result.data.palladiumPrice);
                    $("input[name='saf[examination][metal_with_silver]']").val(result.data.percentSilverPrice);
                    $("input[name='saf[examination][silver]']").val(result.data.silverPrice);
                    $("input[name='saf[examination][bijouterie]']").val(result.data.bijouteriePrice);
                    $("input[name='saf[examination][stones]']").val(result.data.stonePrice);
                    $("input[name='saf[examination][definition_of_carats_of_stones]']").val(result.data.caratPrice);
                    $("input[name='saf[examination][total]']").val(result.data.totalPrice);

                    $('.delete-examination').attr("data-examination-id",result.data.examinationId);
                    $('.edit-examination').attr("data-examination-id",result.data.examinationId);

                }

                if (result.errors) {

                    // $error.show('form-error', result.errors);

                    var errors = result.errors;

                    $.each(productInfo, function (k, v) {
                        var inputName = $(this).attr('name');

                        if (inputName !== undefined) {
                            if (errors[inputName] !== undefined) {
                                $('#form-error-' + inputName + '-' + num).addClass('form-error-text').html(errors[inputName]).css({'display': 'block'});
                            }
                        }
                    });
                    self.prop('disabled', false);
                }
            },
            error: function () {
                self.prop('disabled', false);
            },
        })
    })
}

function unlinkExamination() {
    $(document).on('click', 'button.delete.dt-examination', function (e) {
        e.preventDefault();
        var self = $(this);
        var selfTr = self.closest('tr');

        selfTr.remove();

    })
}

function examinationChange(){

    $(document).on('change', '#saf_product_id', function () {

        var self = $(this);
        var selfTr = self.closest('tr');

        var meauserementUnit =selfTr.find(':selected').data('unit');

        selfTr.find('.measurement_unit').val(meauserementUnit);

    });
}

function editExamination() {
    $(document).on('click', '.edit-examination', function (e) {
        e.preventDefault();
        var self = $(this);
        var num = self.data('num');
        var selfTr = self.closest('tr');
        var examinationId = self.attr('data-examination-id');

        if (examinationId) {
            $.ajax({
                type: 'POST',
                url: $cjs.admPath(userDocUrl + 'edit-examination'),
                data: {examinationId: examinationId, num: num},
                dataType: 'json',
                success: function (result) {
                    if (result.status === 'OK') {
                        var examination = $('.examination-'+examinationId);
                        examination.empty();
                        examination.append(result.data.html);

                        select2Init();
                    }
                }
            });
        } else {
            selfTr.remove();
        }

    })
}

function updateExamination() {
    $(document).on('click', '.updateExamination', function (e) {
        e.preventDefault();
        var self = $(this);
        var productInfo = self.closest('tr').find(':input');
        var typeOfStones = self.closest('tr').find('#typeOfStones').val();
        var num = self.data('num');
        var selfTr = self.closest('tr');
        var examinationId = self.attr('data-examination-id');

        if (examinationId) {
            $.ajax({
                type: 'POST',
                url: $cjs.admPath(userDocUrl + 'update-examination'),
                data: productInfo,
                dataType: 'json',
                headers: {
                    'sNumber': safNumber,
                    'typeOfStones': typeOfStones
                },
                success: function (result) {
                    if (result.status === 'OK') {
                        selfTr.find('input').prop('readonly', true);
                        selfTr.find('.select2').prop('disabled', true);

                        self.prop('disabled', false);
                        var td = self.closest('td');
                        td.empty();
                        var html = '<button class="btn-edit edit-examination" data-toggle="tooltip" data-num="'+num+'" data-examination-id="'+examinationId+'" title="'+$trans('swis.base.tooltip.edit_examination.button')+'"><i class="fas fa-pen"></i></button> ';
                        html += '<button class="btn-remove delete-examination" data-toggle="tooltip" data-num="'+num+'" data-examination-id="'+examinationId+'" title="'+$trans('swis.base.tooltip.delete_examination.button')+'"><i class="fas fa-times"></i></button>';

                        td.html(html);

                        $("input[name='saf[examination][platinium]']").val(result.data.platinumPrice);
                        $("input[name='saf[examination][gold]']").val(result.data.goldPrice);
                        $("input[name='saf[examination][palladium]']").val(result.data.palladiumPrice);
                        $("input[name='saf[examination][metal_with_silver]']").val(result.data.percentSilverPrice);
                        $("input[name='saf[examination][silver]']").val(result.data.silverPrice);
                        $("input[name='saf[examination][bijouterie]']").val(result.data.bijouteriePrice);
                        $("input[name='saf[examination][stones]']").val(result.data.stonePrice);
                        $("input[name='saf[examination][definition_of_carats_of_stones]']").val(result.data.caratPrice);
                        $("input[name='saf[examination][total]']").val(result.data.totalPrice);

                    }
                }
            });
        } else {
            // selfTr.remove();
        }

    })
}

function deleteExamination() {
    $(document).on('click', '.delete-examination', function (e) {
        e.preventDefault();
        var self = $(this);
        var selfTr = self.closest('tr');
        var examinationId = self.attr('data-examination-id');

        if (examinationId != '') {
            $.ajax({
                type: 'POST',
                url: $cjs.admPath(userDocUrl + 'delete-examination'),
                data: {examinationId: examinationId},
                dataType: 'json',
                success: function (result) {
                    if (result.status === 'OK') {
                        selfTr.remove();

                        $("input[name='saf[examination][platinium]']").val(result.data.platinumPrice);
                        $("input[name='saf[examination][gold]']").val(result.data.goldPrice);
                        $("input[name='saf[examination][palladium]']").val(result.data.palladiumPrice);
                        $("input[name='saf[examination][metal_with_silver]']").val(result.data.percentSilverPrice);
                        $("input[name='saf[examination][silver]']").val(result.data.silverPrice);
                        $("input[name='saf[examination][bijouterie]']").val(result.data.bijouteriePrice);
                        $("input[name='saf[examination][stones]']").val(result.data.stonePrice);
                        $("input[name='saf[examination][definition_of_carats_of_stones]']").val(result.data.caratPrice);
                        $("input[name='saf[examination][total]']").val(result.data.totalPrice);
                    }
                }
            })
        } else {
            selfTr.remove();
        }

    })
}