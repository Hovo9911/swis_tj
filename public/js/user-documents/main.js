var hash = window.location.hash;
hash && $('ul.nav a[href="' + hash + '"]').tab('show');

var currentStateId = $('#currentState').val();
var applicationId = $('#appId').val();
var safNumber = $('#safNumber').val();
var documentId = $('#documentID').val();
var examinationList = $('#examinationList');
var userDocUrl = 'user-documents/' + documentId + '/application/' + applicationId + '/';
var documentStatusChangedModalId = 'documentStatusChangedModal';
var alertMessageDiv = $('#alertMessage');
var loadContentDiv = $('.ibox-content');
var subApplicationProductsModal = $('#subApplicationProductsModal');
var pdfHashKey = $('#pdfHashKey').val();
var subApplicationTable = $('#subApplicationList');
var needDigitalSignature = $('#needDigitalSignature').val();
var transitCountryMainDiv = $('.multipleFields.saf_transit_country');
var printFormProductsErrorsDiv = $('#productsErrorList');

/*
 * Modal Options
 */
var optionsDocumentStatusChangedModal = {
    showHeader: false,
    closeBtn: false,
    bodyContent: function () {
        return $trans('swis.user_documents.document_status_is_changed')
    },
    modalSize: 'lg',
    footerButtons: {
        buttons: {
            success: {
                click: function (event) {
                    location.reload()
                },
                title: $trans('swis.user_documents.document_status_changed_button'),
                class: 'btn-authorize'
            },
        }
    }
};

/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            /*  var submittedDate = moment(aData['submittedDate']);
              var currentDate = moment(moment().format("YYYY-MM-DD"));

              console.log(aData['currentStatus'])

              if (typeof aData['currentStatus'] != "undefined" && aData['currentStatus'] == 'success') {
                  $('td', nRow).addClass('table-row-background-success');//.css('background-color', '#00cc00' );
              } else if (typeof aData['currentStatus'] != "undefined" && aData['currentStatus'] == 'rejected') {
                  $('td', nRow).addClass('table-row-background-rejected');//.css('background-color', '#ff5050' );
              } else if (currentDate.diff(submittedDate, "days") >= 5) {
                  $('td', nRow).addClass('table-row-background-5-days');//.css('background-color', '#F6C9C9' );
              } else if (currentDate.diff(submittedDate, "days") >= 2) {
                  $('td', nRow).addClass('table-row-background-2-days');//.css('background-color', '#D6D6D6');
              }*/
        },
        "order": [[1, "desc"]],
        fnRowCallback: function (nRow, aData) {
            $('td:eq(0)', nRow).addClass('text-center');

            if (!aData['is_edited']) {
                $(nRow).addClass('not-edited');
            }

        },
        lengthChange: true,
        iDisplayLength: 25
    },
    customOptions: {
        afterInitCallback: function () {
        },
        tableDataPath: $('#list-data').data('action'),
        searchPath: $('#list-data').data('search-action'),
        columnsRender: {
            state: {
                render: function (row) {
                    if (row.state) {

                        var submittedDate = moment(row['submittedDateOriginal']);
                        var currentDate = moment(moment().format("YYYY-MM-DD"));

                        var className = '';
                        if (typeof row.currentStatus != "undefined" && row.currentStatus == 'success') {
                            className = 'table-row-background-success';
                        } else if (typeof row.currentStatus != "undefined" && row.currentStatus == 'rejected') {
                            className = 'table-row-background-rejected';
                        } else if (currentDate.diff(submittedDate, "days") >= 5) {
                            className = 'table-row-background-5-days';
                        } else if (currentDate.diff(submittedDate, "days") >= 2) {
                            className = 'table-row-background-2-days';
                        }

                        return '<span class="' + className + '">' + row.state + '</span>';

                    }
                    return ''
                }
            },
            saf_general_sub_application_express_control_id: {
                render: function (row) {
                    if (row.expressControlCode == 1) {
                        return '<i class="fa fa-circle text-warning"></i> ' + row.saf_general_sub_application_express_control_id;
                    }
                    return row.saf_general_sub_application_express_control_id
                }
            }
        }
    }
};

var $documentStatusChangedModal = new Modal(documentStatusChangedModalId, optionsDocumentStatusChangedModal);
var customErrorFields = {};

/*
 * Options for Form Request
 */
var optionsForm = {
    deletePath: '/user-documents/delete',
    searchPath: $('#form-data').data('search-path'),
    addPath: '/user-documents/create',
    customSaveFunction: function () {
        loadContent(loadContentDiv);
        alertMessageDiv.html('');
        return true;
    },
    afterGetErrorResult: function (result) {

    },
    keepButtonsDisabledUntilReload: true,
    customOptions: {
        onSaveSuccess: function (result) {

            if (result.backToUrl) {

                var bactToUrl = result.backToUrl;

                window.location.href = bactToUrl;

                if (bactToUrl.indexOf('#') !== -1) {
                    location.reload();
                }
            } else {
                loadContent();
            }
        },
        onSaveError: function (result) {
            alertMessageDiv.html('');

            if (typeof result.state_changed != 'undefined' && result.state_changed) {
                documentStatusChanged();
            }

            if (typeof result.obligation_exist != 'undefined') {
                alertMessageDiv.html('<div class="alert alert-danger">' + result.obligation_exist + '</div>')
            }

            if (typeof result.laboratory_doesnt_exist != 'undefined' && result.laboratory_doesnt_exist) {
                alertMessageDiv.html('<div class="alert alert-danger">' + result.laboratory_doesnt_exist + '</div>')
            }

            if (typeof result.need_digital_signature != 'undefined' && result.need_digital_signature && needDigitalSignature) {
                //open rutoken modal
                $('#buttonMapping').val(result.button);
                openRutokenModal();
            }

            if ('rule_show_error' in result) {
                var errorText = $trans('swis.user-documents-validation-error');
                $.each(result.custom_errors, function (field, index) {
                    if ($("#form-error-" + field).length == 0) {
                        if (result.rule_show_error[field]) {
                            errorText += "<br />" + result.rule_show_error[field] + "<br />";
                        }
                        delete result.custom_errors[field];
                    }
                });
                alertMessageDiv.html('<div class="alert alert-danger">' + errorText + '</div>');
                $error.show('form-error', result.custom_errors);
            } else if ('rule_show_warning' in result) {
                var modalBody = "";
                customErrorFields = result.custom_errors;
                $.each(result.rule_show_warning, function (field, key) {
                    modalBody += key + "<br />";
                });
                $("#showWarningModalBody").html('').html(modalBody);
                $('.confirmWarning').data('button', result.confirmButton);
                $('#showWarning').modal('show');
            } else if ('rule_show_warning2' in result) {
                var modalBody = "";
                customErrorFields = result.custom_errors;
                $.each(result.rule_show_warning2, function (field, key) {
                    modalBody += key + "<br />";
                });
                $("#showWarning2ModalBody").html('').html(modalBody);
                $('.confirmWarning2').data('button', result.confirmButton);
                $('#showWarning2').modal('show');
            }

            loadContent(loadContentDiv);
        }
    },
};

/*
 * Init Datatable, Form
 */
var $userDocumentsTable = new DataTable('list-data', optionsTable);
var $userDocuments = new FormRequest('form-data', optionsForm);

$userDocuments.initEditPage = function () {

    $('#document-id').change(function () {
        window.location.href = window.location.pathname + '?document-id=' + $(this).val();
    });

    $userDocuments.initForm();
};

$userDocuments.initActionsChange = function () {
    $('#document-actions-block button').click(function () {
        $('#document-actions-block button').attr('disabled', 'disabled');
        $userDocuments.changeState($(this).data('state'), $(this).data('action'), $(this).data('mapping-id'));
    });
};

$userDocuments.changeState = function (stateId, actionId, mappingId) {
    $('#documentStateID').val(stateId);
    $('#documentActionID').val(actionId);
    $('#mappingID').val(mappingId);
};

$(document).ready(function () {

    // init
    $userDocumentsTable.init();
    $userDocuments.init();
    $userDocuments.initEditPage();
    $userDocuments.initActionsChange();
    $documentStatusChangedModal.init();

    // ---------------

    hideEmptyBlocks();

    renderNewObligationField();

    approvalAndRejectedFieldValidation();

    approvalAndRejectedNettoWeightFieldValidation();

    refAutocomplete();

    reCallSubApplicationModal();

    addProductsToPrintForm();

    confirmAndRejectWarningModal();

    // -----------

    removeNotUsedButtons();

    multipleFields();

    viewActions();

    defaultSave();

    // -------

    labExamination();

    disableDocumentTabFieldsIfAppInLaboratory();

    disableLabTabIfNotExistMappingToLab();

    renderExaminationInputs();

    storeExamination();

    unlinkExamination();

    editExamination();

    updateExamination();

    deleteExamination();

    examinationChange();

    // ---------

    deleteSuperscribe();

    superscribeUser();

    superscribeUserDelete();

    // ---------

    enableContent();

    // ---------
    checkRutokenModal();
});

$(document).on("keypress", ".onlyNumbers", function (evt) {
    if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
        evt.preventDefault();
    }
});

$(document).on("keypress", ".onlyWithFloat", function (evt) {
    if (evt.which != 8 && evt.which != 0 && $(this).val().indexOf('.') != -1 && evt.which < 48 || evt.which > 57) {
        evt.preventDefault();
    }
});

// -------------

function enableContent() {
    $('.tabs-container').removeClass('loading-close');
}

function hideEmptyBlocks() {
    var blockContents = $('.userDocumentList').find('.panel-body');

    $.each(blockContents, function (k, v) {
        if (!$.trim($(this).html()).length) {
            $(this).closest('.panel').addClass('hide');
        }
    });
}

function multipleFields() {
    //create multiple fields
    if ($('.saf_transit_country').data('fields') !== undefined) {
        var fields = JSON.parse(JSON.stringify($('.saf_transit_country').data('fields')));
        $.each(fields, function (index, value) {
            if (value) {
                if (index > 0) {
                    var $cloneSelect = $('.saf_transit_country').find('select.select2:first').closest('.clearfix').clone();
                    $cloneSelect.find('span').remove('span');
                    $cloneSelect.find('button').remove('button');
                    $cloneSelect.appendTo($('.saf_transit_country')).append("<button type='button' class='btn btn-danger deleteMultipleRow'><i class='fa fa-minus'></i></button>");
                    $('.saf_transit_country').find('select.select2:last').select2();
                    $('.saf_transit_country').find('select.select2:last').val(value).trigger('change');
                } else {
                    $('.saf_transit_country').find('select.select2:first').val(value).trigger('change');
                }
            }
        });

        transitCountriesIndexNumbersRefresh();
    }

    var index = 1;

    if ($('.state_number_customs_support').data('fields') !== undefined) {
        var fields = JSON.parse(JSON.stringify($('.state_number_customs_support').data('fields')));
        var stateNumberCustomsSupport = $('.state_number_customs_support').find('div:first');

        $.each(fields, function (key, value) {
            if (value) {
                var $cloneStateNumberCustomsSupport = stateNumberCustomsSupport.clone();

                $cloneStateNumberCustomsSupport.find('.state_number').val(value['state_number']);
                $cloneStateNumberCustomsSupport.find('.customs_support').val(value['customs_support']);
                $cloneStateNumberCustomsSupport.find('.track_container_temperature').val(value['track_container_temperature']);
                $cloneStateNumberCustomsSupport.find('button').remove('button');
                $cloneStateNumberCustomsSupport.appendTo($('.state_number_customs_support')).append("<button type='button' class='btn btn-danger deleteMultipleRow'><i class='fa fa-minus'></i></button>");
                if ($cloneStateNumberCustomsSupport.find('input').length > 0) {
                    var name = "saf[transportation][state_number_customs_support][" + index + "][state_number]";
                    $.each($cloneStateNumberCustomsSupport.find('input'), function (k, v) {
                        if ($(this).data('name')) {
                            $(this).attr({name: $(this).data('name').replace('_index_', index)})
                        }
                    });
                }
            }

            index++;
        });
        stateNumberCustomsSupport.remove();
    }
}

function transitCountriesIndexNumbersRefresh() {
    var transitSelects = transitCountryMainDiv.find('select');
    var index = 1;

    $.each(transitSelects, function (k, v) {

        if ($(this).val()) {
            var selectParentDiv = $(this).closest('div');

            if (selectParentDiv.find('.transit-country-index-number').length) {
                selectParentDiv.find('.transit-country-index-number').text(index)
            } else {
                selectParentDiv.append('<span class="transit-country-index-number">' + index + '</span>')
            }

            index++;
        }

    });
}

// -------------

function documentStatusChanged() {
    disableFormInputs(loadContentDiv, false, true);

    $documentStatusChangedModal.open(true);
}

function removeNotUsedButtons() {

    $(document).ready(function () {

        var tabContent = $('.tab-content');

        setTimeout(function () {
            tabContent.find('button').not('.btn-edit,.addLabExpertiseButton,.documentSearch,.plusMultipleFieldsDocument,.obligation-delete-ajax,.plusMultipleFieldsObligation,.dt-document,.add-products,.visible-buttons,.addProductsDocument,.close,.resetButton,.addDocument,.closeResultTab,.obligation-btn,.btn--icon,.plusMultipleFieldsExamination,.dt-examination,.delete-examination,#deleteCheckedDocuments').remove();
        }, 300);

        if (typeof subApplicationType != 'undefined' && subApplicationType === 'requested') {
            tabContent.find('button').remove()
        }
    });
}

function defaultSave() {
    $('.btn-save-change').on('click', function (e) {

        e.preventDefault();

        var formObj = $('#form-data');
        var sendData = formObj.serializeArray();
        var rPath = formObj.attr('action');

        loadContent(loadContentDiv);
        $.ajax({
            type: 'POST',
            url: rPath + '-default',
            data: sendData,
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {

                    window.location.href = result.data.backToUrl;

                } else if (result.status == 'INVALID_DATA') {

                    loadContent(loadContentDiv);
                    $('.btn-state-change').prop('disabled', false);
                    $('.btn-save-change').prop('disabled', false);
                    $error.show('form-error', result.errors);
                }
            }
        });
    });
}

// -----------

function approvalAndRejectedFieldValidation() {
    $(document).on('input', '.batch_approved_quantity', function () {

        var $tr = $(this).closest('tr');
        var quantity = parseFloat($(this).data('quantity'));
        var approved = parseFloat($(this).val());

        if (approved > quantity) {
            $(this).val(quantity);
            $tr.find('.batch_rejected_quantity').val(0)
        } else {
            $tr.find('.batch_rejected_quantity').val(parseFloat(quantity - approved))
        }
    })
}

function approvalAndRejectedNettoWeightFieldValidation() {
    $(document).on('input', '.batch_approved_netto_weight', function () {

        var $tr = $(this).closest('tr');
        var quantity = parseFloat($tr.find('.batch_netto_weight').val());
        var approved = parseFloat($(this).val());

        if (approved > quantity) {
            $(this).val(quantity);
            $tr.find('.batch_rejected_netto_weight').val(0)
        } else {
            $tr.find('.batch_rejected_netto_weight').val(parseFloat(quantity - approved))
        }
    })
}

function disableDocumentTabFieldsIfAppInLaboratory() {
    if (typeof appInLaboratory != 'undefined' && appInLaboratory) {
        var documentTab = $('#tab-attached_documents');

        documentTab.find('button').remove();
        documentTab.find('input').prop('disabled', true);
    }
}

function disableLabTabIfNotExistMappingToLab() {
    if (typeof hasLaboratoryState != 'undefined' && !hasLaboratoryState) {
        var laboratoryTab = $('#tab-laboratories');

        laboratoryTab.addClass('close-panel');
        laboratoryTab.find('button').remove();
        laboratoryTab.find(':input').prop('disabled', true);
    }
}

function reCallSubApplicationModal() {
    var reCallSubApplicationModal = $('#reCallModal');

    if (reCallSubApplicationModal.length > 0) {
        reCallSubApplicationModal.modal({backdrop: 'static'})
    }
}

function addProductsToPrintForm() {

    var templateCode = '';

    $(document).on("click", '#check-all-subApplication-add-products', function () {
        $(this).closest('table').find("input[type='checkbox']").prop("checked", $(this).prop("checked"));
    });


    $('.addProductsToPrintForm').click(function (e) {
        e.preventDefault();

        var self = $(this);

        subApplicationProductsModal.find('tr').removeClass('active');
        $(this).closest('tr').addClass('active');

        templateCode = self.data("id")

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('user-documents/render-sub-application-products'),
            headers: {
                'sNumber': safNumber,
                'applicationId': applicationId
            },
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {

                    printFormProductsErrorsDiv.html('');
                    subApplicationProductsModal.find('.sub-application-products-list').html(result.data.html);

                    subApplicationProductsModal.modal();

                    self.prop('disabled', false);
                }
            }
        });

    });

    $('.printFormButton').click(function () {

        var productValues = [];

        $('.sub-application-products-list .pr:checked').each(function () {
            productValues.push($(this).val() + '-' + $(this).attr('data-pr-number'));
        });

        if (productValues.length === 0) {
            printFormProductsErrorsDiv.html('<div class="alert alert-danger">' + $trans('swis.user_documents.choose_products.empty') +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
                '    <span aria-hidden="true">&times;</span>\n' +
                '  </button>' + '</div> ');
        } else {
            if (templateCode == "") {
                $('#subApplicationProductsModal').modal('hide');
                // window.open($cjs.admPath('/user-documents/' + applicationId + '/' + pdfHashKey + '/' + documentId + '/' + productValues), '_blank');
                window.open($cjs.admPath('/user-documents/' + documentId + '/application/' + applicationId + '/' + pdfHashKey + '/default/' + productValues), '_blank');
            } else {
                $('#subApplicationProductsModal').modal('hide');
                window.open($cjs.admPath('/user-documents/' + documentId + '/application/' + applicationId + '/' + pdfHashKey + '/' + templateCode + '/' + productValues), '_blank');
            }
        }

        // subApplicationProductsModal.modal('hide');
    });


    $('.printWithAllProductsFormButton').click(function () {

        templateCode = $(this).data("id")
        var productValues = $(this).closest('.printWithAllProductsFormButton').data('products');

        if (templateCode == "") {
            window.open($cjs.admPath('/user-documents/' + documentId + '/application/' + applicationId + '/' + pdfHashKey + '/default/' + productValues), '_blank');
        } else {
            window.open($cjs.admPath('/user-documents/' + documentId + '/application/' + applicationId + '/' + pdfHashKey + '/' + templateCode + '/' + productValues), '_blank');
        }

        // subApplicationProductsModal.modal('hide');
    });
}

// -----------

function confirmAndRejectWarningModal() {
    $('.rejectWarning').on('click', function (e) {

        $('#showWarningModalBody').html('');
        $('#showWarning').modal('hide');
        var errorText = '';

        $.each(customErrorFields, function (field, index) {
            if ($("#form-error-" + field).length == 0) {
                errorText += index + "<br />";
                delete customErrorFields[field];
            }
        });
        if (errorText) {
            alertMessageDiv.html('<div class="alert alert-danger">' + errorText + '</div>');
        }

        $error.show('form-error', customErrorFields);

    });

    $('.confirmWarning').on('click', function () {

        $('#isConfirmed').val(true);
        $($(this).data('button')).trigger('click');

    });
}

// -------------

function superscribeUser() {
    $('#superscribeUser').submit(function (e) {
        e.preventDefault();
        var self = $(this);

        self.find('button').prop('disabled', true);

        loadContent(loadContentDiv);

        $.ajax({
            type: 'POST',
            url: self.attr('action'),
            data: $('#form-data, #superscribeUser').serialize(),
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    location.href = result.data.backToUrl
                } else if (result.status === 'INVALID_DATA') {

                    loadContent(loadContentDiv);

                    if (typeof result.errors.state_changed != 'undefined' && result.errors.state_changed) {
                        documentStatusChanged();
                    } else {
                        if (result.errors) {
                            $error.show('form-error', result.errors);
                        }
                        toastr.error($trans.get('core.loading.invalid_data'))
                    }
                }

                self.find('button').prop('disabled', false);
            }
        });
    })
}

function deleteSuperscribe() {
    $('#deleteSuperscribe').click(function () {
        var self = $(this);

        modalConfirmDelete(function (confirm) {
            if (confirm) {
                loadContent(loadContentDiv);
                self.closest('form').submit();
            }
        });
    });
}

function superscribeUserDelete() {
    $('#superscribeUserDelete').submit(function (e) {
        e.preventDefault();
        var self = $(this);

        self.find('button').prop('disabled', true);

        $.ajax({
            type: 'POST',
            url: self.attr('action'),
            data: self.serialize(),
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    location.href = result.data.backToUrl
                }

                self.find('button').prop('disabled', false);

                if (typeof result.errors.state_changed != 'undefined' && result.errors.state_changed) {
                    documentStatusChanged();
                }
            }
        });
    })
}

// ---------- RuToken

function openRutokenModal() {

    var rutokenModal = $('#rutoken-modal');

    rutokenModal.modal({
        backdrop: 'static',
        keyboard: false
    });
}

function checkRutokenModal() {

    var rutokenModalCheck = $('#rutoken-modal-check');
    $(".result").html();

    $('.checkRutokenModal').click(function (e) {

        e.preventDefault();

        var self = $(this);
        var digitalSignatureId = self.attr('data-id');
        var checkRutokenAlertMessage = $('#checkRutokenAlertMessage');
        var digitalSignatureInfo = $('#digitalSignatureInfo');

        checkRutokenAlertMessage.html('');
        digitalSignatureInfo.html('');

        rutokenModalCheck.modal();
        self.prop('disabled', true);
        $.ajax({
            url: $cjs.admPath('user-documents/check-digital-signature'),
            type: 'post',
            dataType: 'json',
            data: {digitalSignatureId: digitalSignatureId},
            success: function (result) {
                if (result.status === 'OK') {
                    var str = '';

                    if (typeof result.data.command.subject != 'undefined') {
                        str += '<p>' + $trans('swis.user-documents.digital-signature.issuer') + " " + result.data.command.subject.CN + '</p>';
                    }

                    if (typeof result.data.command.issuer != 'undefined') {
                        str += '<p>' + result.data.command.issuer.CN + '</p>';
                    }

                    if (typeof result.data.signing_time != 'undefined') {
                        str += '<p>' + $trans('swis.user-documents.digital-signature.signing_time') + " " + result.data.signing_time + '</p>';
                    }

                    if (result.data.command == false) {
                        checkRutokenAlertMessage.html('<div class="alert alert-danger"><img src="/image/digital-signature/sign-error-icon.png" class="image_middle">' + ' ' + $trans('swis.user-documents.digital_signature.not_valid') + '</div>')
                    } else {
                        checkRutokenAlertMessage.html('<div class="alert alert-success"><img src="/image/digital-signature/sign-confirmed-icon.png" class="image_middle">' + ' ' + $trans('swis.user-documents.digital_signature.valid') + '</div>')
                        digitalSignatureInfo.html(str);
                    }
                }
            },
            error: function (xhr, status, error) {
            },
        });
    });
}