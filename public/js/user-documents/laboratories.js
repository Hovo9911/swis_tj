var alertDivForLabExamination =$('.alertDivForLabExamination');
var labExpertiseCrudModalBody  = $('#labExpertiseCrudModalBody');

function labExamination() {
    updateTotalPrice();

    var typeValue = null;
    var selectedIndicators = {};
    var generateLaboratoryApplication = $('#generateLaboratoryApplication');

    $('.addLabExpertiseButton').on('click', function () {
        $('#laboratoryCrudModal').modal('show');
    });

    $('#selectLabExpertise').on('select2:select', function (e) {

        var self = $(this);
        self.prop('disabled', true);

        typeValue = self.val();

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('user-documents/laboratory/create'),
            data: {
                type: typeValue,
                saf_number: safNumber,
                sub_application_id: applicationId,
                edit: false,
                superscribe: self.data('superscribe')
            },
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    self.prop('disabled', false);

                    $('#labExpertiseCrudModalBody').html(result.data.viewHtml);

                    resetSelect2Values()

                    $('#laboratoryCrudModal').modal('show');
                }
            },
            error: function (request, status, error) {
                // showError(self, request);
            }
        });
    });

    $(document).on('select2:select', '.selectLabExpertiseProduct', function () {

        if (typeValue == '1') {
            return false;
        }

        var self = $(this);
        var selfBlock = self.closest('.multipleProductBlockLabExamination');
        var selectLabExpertiseLaboratory = $('#selectLabExpertiseLaboratory');
        selfBlock.find('.LabExpertiseProductSampleQuantity').val('');
        selfBlock.find('.LabExpertiseProductSampleNumber').val('');

        self.prop('disabled', true);

        if (typeValue != '3') {
            selectLabExpertiseLaboratory.find('option').remove().end().val('').append("<option value=''></option>").trigger('change');
            selectLabExpertiseLaboratory.prop('disabled', true);
        } else {
            selectLabExpertiseLaboratory.val('').trigger('change');
            selectLabExpertiseLaboratory.prop('disabled', true);
        }

        var selectedProducts = [];
        $('.selectLabExpertiseProduct').each(function (key, value) {
            selectedProducts.push($(value).val());
        })

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('user-documents/laboratory/get-laboratories'),
            data: {
                saf_number: safNumber,
                sub_application_id: applicationId,
                product_ids: selectedProducts,
                product_id: self.val()
            },
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    self.prop('disabled', false);
                    selectLabExpertiseLaboratory.prop('disabled', false);

                    if (result.data.hasOwnProperty('labs') && typeValue != '3') {
                        $.each(result.data.labs, function (key, value) {
                            selectLabExpertiseLaboratory.prop('disabled', false);
                            selectLabExpertiseLaboratory.append($("<option></option>")
                                .attr("value", value.id)
                                .text("(" + value.tax_id + ") " + value.name));
                        });
                    }

                    blockEnabledOrDisabled(self.closest('.multipleProductBlockLabExamination'), false, result.data.measurement_unit);
                }
            },
            error: function (request, status, error) {
                self.prop('disabled', false);
                var block = self.closest('.multipleProductBlockLabExamination');
                block.find('.selectMeasurementUnit').val('').trigger('change');
            }
        });
    });

    $(document).on('select2:select', '#selectLabExpertiseLaboratory', function (e) {
        generateLaboratoryApplication.prop('disabled', false);
    });

    $(document).on('select2:unselecting', '#selectLabExpertiseLaboratory', function (e) {
        generateLaboratoryApplication.prop('disabled', true);
    });

    $(document).on('change', '.LabExpertiseProductSampleNumber,.LabExpertiseProductSampleQuantity,.selectMeasurementUnit', function () {
        generateLaboratoryApplication.prop('disabled', false);
    });

    generateLaboratoryApplication.on('click', function () {
        var subapplicationId = $('#labApplicationId');
        var self = $(this);
        var labExaminationTableContainer = $('.lab-examination-table-container');
        var checkedValues = $('.indicatorSelect').val();
        var selectedProducts = [];

        self.prop('disabled', true);

        $('.multipleProductBlockLabExamination').each(function (key, item) {
            var itemSelector = $(item);
            selectedProducts.push({
                'product_id': itemSelector.find('.selectLabExpertiseProduct').val(),
                'sample_quantity': itemSelector.find('.LabExpertiseProductSampleQuantity').val(),
                'sample_measurement_unit': itemSelector.find('.selectMeasurementUnit').val(),
                'sample_number': itemSelector.find('.LabExpertiseProductSampleNumber').val()
            })
        });

        var data = {
            laboratory_id: $('#selectLabExpertiseLaboratory').val(),
            products: selectedProducts,
            saf_number: safNumber,
            sub_application_id: applicationId,
            type: typeValue,
            lab_application_id: subapplicationId.length ? subapplicationId.val() : null,
        };

        if (typeValue == '1') {
            data['indicators'] = checkedValues;
        }

        alertDivForLabExamination.hide();
        alertDivForLabExamination.text('');

        var action = subapplicationId.length ? 'update-laboratory-applications' : 'generate-laboratory-applications';

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('user-documents/laboratory/'+action),
            data: data,
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {

                    self.prop('disabled', false);

                    labExaminationTableContainer.html(result.data.laboratoriesView);

                    labExpertiseCrudModalBody.html('');
                    $('#laboratoryCrudModal').modal('hide');

                } else if (result.status === 'INVALID_DATA') {

                    if (typeof result.errors.laboratory_or_document_not_valid !== 'undefined') {
                        alertDivForLabExamination.show();
                        alertDivForLabExamination.text(result.errors.laboratory_or_document_not_valid);
                    }

                    if (result.errors) {
                        $error.show('form-error', result.errors);
                    }

                }
            },
            error: function (request, status, error) {
                // showError(self, request);
            }
        });
    });

    $(document).on('select2:unselecting', '.selectLabExpertiseProductByProducts, .selectLabExpertiseProduct', function () {
        var self = $(this);

        var selfBlock = self.closest('.multipleProductBlockLabExamination');
        var indicatorSelect = $('.indicatorSelect');
        var selectLabExpertiseLaboratory = $('#selectLabExpertiseLaboratory');

        indicatorSelect.find('option').remove().end().trigger('change');
        indicatorSelect.prop('disabled', true);
        selfBlock.find('.LabExpertiseProductSampleQuantity').val('');
        selfBlock.find('.LabExpertiseProductSampleNumber').val('');

        blockEnabledOrDisabled(self.closest('.multipleProductBlockLabExamination'), true);

        selectLabExpertiseLaboratory.find('option').remove().end().val('').trigger('change');
        selectLabExpertiseLaboratory.prop('disabled', true);

        self.prop('disabled', false);
    });

    $(document).on('select2:select', '.selectLabExpertiseProductByProducts', function () {
        var self = $(this);
        var selfBlock = self.closest('.multipleProductBlockLabExamination');
        var indicatorSelect = $('.indicatorSelect');
        indicatorSelect.find('option').remove().end().trigger('change');
        indicatorSelect.prop('disabled', true);

        self.prop('disabled', true);
        selfBlock.find('.LabExpertiseProductSampleQuantity').val('');
        selfBlock.find('.LabExpertiseProductSampleNumber').val('');

        var selectLabExpertiseLaboratory = $('#selectLabExpertiseLaboratory');
        selectLabExpertiseLaboratory.find('option').remove().end().val('').trigger('change');
        selectLabExpertiseLaboratory.prop('disabled', true);

        if (!self.val()) {
            return false;
        }
        $.ajax({
            type: 'POST',
            url: $cjs.admPath('user-documents/laboratory/get-indicators'),
            data: {
                product_id: self.val(),
                sub_application_id: applicationId
            },
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    self.prop('disabled', false);

                    if (result.data.indicators.length) {
                        $.each(result.data.indicators, function (key, value) {
                            indicatorSelect.append($("<option></option>")
                                .attr("value", value.id)
                                .text("("+value.code+") "+value.name));
                        });
                    }
                    indicatorSelect.prop('disabled', false);
                    blockEnabledOrDisabled(selfBlock, false, result.data.measurement_unit);
                }
            },
            error: function (request, status, error) {
                self.prop('disabled', false);
                self.closest('.multipleProductBlockLabExamination').find('.selectMeasurementUnit').val('').trigger('change');
            }
        });
    });

    $(document).on('select2:select select2:unselect', '.indicatorSelect', function () {

        var self = $(this);
        var selectLabExpertiseLaboratory = $('#selectLabExpertiseLaboratory');
        var checkedValues = $(this).val();

        self.prop('disabled', true);
        select2Reset(selectLabExpertiseLaboratory);
        selectLabExpertiseLaboratory.prop('disabled', true);

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('user-documents/laboratory/get-laboratories-by-indicators'),
            data: {
                indicators: checkedValues,
                sub_application_id: applicationId,
            },
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {

                    self.prop('disabled', false);

                    $.each(result.data.labs, function (key, value) {
                        selectLabExpertiseLaboratory.append($("<option></option>")
                            .attr("value", value.id)
                            .text("("+value.tax_id+") "+value.name));
                    })

                    resetSelect2Values(true)

                    selectLabExpertiseLaboratory.prop('disabled', false);

                }
            },
            error: function (request, status, error) {
                self.prop('disabled', false);
                self.closest('.multipleProductBlockLabExamination').find('.selectMeasurementUnit').val('').trigger('change');
            }
        });
    });

    $(document).on('click', '.delete-lab', function () {

        var self = $(this);
        var selfTr = self.closest('tr');
        var subApplicationId = self.data('id');

        if (typeof subApplicationId !== "undefined" && subApplicationId !== '') {

            modalConfirmDelete(function (confirm) {

                if (confirm) {

                    loadContent();

                    $.ajax({
                        type: 'POST',
                        url: $cjs.admPath('user-documents/laboratory/delete-lab-examination'),
                        data: {id: subApplicationId},
                        dataType: 'json',
                        success: function (result) {

                            if (result.status === 'OK') {
                                selfTr.remove();

                                if (!subApplicationTable.find('tbody tr').length) {
                                    subApplicationTable.closest('.table-responsive').addClass('hide');
                                }
                                loadContent();
                            }

                            if (result.status === 'INVALID_DATA') {
                                loadContent();
                            }
                        }
                    })
                }
            });

        } else {
            selfTr.remove();

            if (!subApplicationTable.find('tbody tr').length) {
                subApplicationTable.closest('.table-responsive').addClass('hide');
            }
        }
    });

    $(document).on('click', '.edit-lab', function () {
        var self = $(this);
        typeValue = self.data('type');

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('user-documents/laboratory/create'),
            data: {
                type: typeValue,
                saf_number: safNumber,
                sub_application_id: applicationId,
                application_id: self.data('id'),
                edit: true,
                superscribe: self.data('superscribe')
            },
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {

                    $('#selectLabExpertise').val(typeValue).trigger('change');
                    $('#labExpertiseCrudModalBody').html(result.data.viewHtml);

                    resetSelect2Values();

                    $('#laboratoryCrudModal').modal('show');
                }
            },
            error: function (request, status, error) {
                showError(self, request);
            }
        });
    });

    $(document).on('click', '.send-to-lab', function () {
        var self = $(this);
        typeValue = self.data('type');

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('user-documents/laboratory/send-to-lab'),
            data: {
                lab_sub_application_id: self.data('id'),
                sub_application_id: applicationId,
            },
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    window.location.reload();
                }
            },
            error: function (request, status, error) {
                showError(self, request);
            }
        });
    });

    $('.addIndicatorButton').on('click', function (event) {
        var self = $(this);
        var productId = $(this).data('productId');
        $('#collapseOneLabProduct'+productId).collapse("show");
        var hasEmptyRow = false;

        $('.selectIndicator').each(function (key, item) {
            $(item).closest('.form-group').removeClass('has-error')
            if (!$(item).val()) {
                $(item).closest('.form-group').addClass('has-error')
                hasEmptyRow = true;
            }
        });

        if (hasEmptyRow) {
            return false;
        }

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('user-documents/laboratory/add-indicator-view'),
            data: {
                product_id: productId,
                sub_application_id: applicationId,
            },
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    $('.tableBody'+productId).append(result.data.indicatorView);
                    $('.selectIndicator').select2({ allowClear: true, placeholder: "" });

                    var correspondClass = $('.correspond'+productId).text();
                    $('.labExaminationCorrespond'+productId).val(3)
                    updateTotalPrice();

                    var table = self.closest('panel').find('tbody');
                    updateCorrespondField(table, productId)

                }
            },
            error: function (request, status, error) {
                showError(self, request);
            }
        });
    });

    $(document).on('change', '.correspond_or_not', function () {
        var productId = $(this).data('productId');
        var table = $(this).closest('tbody');
        updateCorrespondField(table, productId);
    });

    $(document).on('select2:unselecting', '.selectIndicator', function (e) {
        var self = $(this);
        var productId = self.data('productId');

        if (selectedIndicators[productId]) {
            var index = selectedIndicators[productId].indexOf(self.val());
            if (index > -1) {
                selectedIndicators[productId].splice(index, 1);
            }
        }
    });

    $(document).on('select2:selecting','.selectIndicator', function () {
        var self = $(this);
        var productId = self.data('productId');

        if (selectedIndicators[productId]) {
            var index = selectedIndicators[productId].indexOf(self.val());
            if (index > -1) {
                selectedIndicators[productId].splice(index, 1);
            }
        }
    });

    $(document).on('select2:select', '.selectIndicator', function () {
        var self = $(this);
        var productId = self.data('productId');
        if (selectedIndicators[productId]) {
            $('.hiddenIndicatorValue'+productId).each(function (i, v) {
                if ($(v).val() == self.val()) {
                    toastr.error($trans('swis.user_documents.choose_different_product'));

                    self.val(null).trigger('change');
                    return false;
                }
            })

            if(!selectedIndicators[productId].includes(self.val())){
                selectedIndicators[productId].push(self.val());
            } else {
                toastr.error($trans('swis.user_documents.choose_different_product'));

                self.val(null).trigger('change');
                return false;
            }
        } else {
            $('.hiddenIndicatorValue'+productId).each(function (i, v) {
                if ($(v).val() == self.val()) {
                    toastr.error($trans('swis.user_documents.choose_different_product'));

                    self.val(null).trigger('change');
                    return false;
                }
            })

            selectedIndicators[productId] = [];
            selectedIndicators[productId].push(self.val());
        }

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('user-documents/laboratory/get-indicator-value'),
            data: {
                indicator_id: self.val(),
                sub_application_id: applicationId
            },
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    var data = result.data.data;
                    var tr = self.closest('tr');
                    tr.find('.limit_3').text(data.limit_3);
                    tr.find('.measurement_units').text(data.measurement_units);
                    tr.find('.limit_quality_2').text(data.limit_quality_2);
                    tr.find('.price').text(data.price);
                    tr.find('.duration').text(data.duration);
                    tr.find('.labExaminationPrice').val(data.price);
                    tr.find('.labExaminationDuration').val(data.duration);

                    var productId = tr.find('.labProductId').val();
                    // update names
                    tr.find('.found_size').attr('name', 'lab_examination['+productId+']['+self.val()+'][found_size]');
                    tr.find('.found_or_not').attr('name', 'lab_examination['+productId+']['+self.val()+'][found_or_not]');
                    tr.find('.correspond_or_not').attr('name', 'lab_examination['+productId+']['+self.val()+'][correspond_or_not]');
                    tr.find('.selectIndicator').attr('name', 'lab_examination['+productId+']['+self.val()+'][indicator]');
                    tr.find('.labExaminationPrice').attr('name', 'lab_examination['+productId+']['+self.val()+'][price]');
                    tr.find('.labExaminationDuration').attr('name', 'lab_examination['+productId+']['+self.val()+'][duration]');
                    // update error ids
                    tr.find('.errorIndicator').attr('id', 'form-error-lab_examination-'+productId+'-'+self.val()+'-indicator');
                    tr.find('.errorFoundSize').attr('id', 'form-error-lab_examination-'+productId+'-'+self.val()+'-found_size');
                    tr.find('.errorFoundOrNot').attr('id', 'form-error-lab_examination-'+productId+'-'+self.val()+'-found_or_not');
                    tr.find('.errorCorrespondOrNot').attr('id', 'form-error-lab_examination-'+productId+'-'+self.val()+'-correspond_or_not');
                    updateTotalPrice();
                }
            },
            error: function (request, status, error) {
                showError(self, request);
            }
        });
    });

    $(document).on('click', '.removeLabIndicator', function () {
        var selectIndicator = $(this).closest('tr').find('.selectIndicator');
        var productId = selectIndicator.data('productId')
        if (selectIndicator.val() && productId) {
            if (selectedIndicators[productId]) {
                var index = selectedIndicators[productId].indexOf(selectIndicator.val());
                if (index > -1) {
                    selectedIndicators[productId].splice(index, 1);
                }
            }
        }

        var productId = $(this).data('productId');
        var table = $(this).closest('tbody');

        $(this).closest('tr').remove();
        updateCorrespondField(table, productId)

        updateTotalPrice();
    });

    $(document).on("click", ".addNewProductLabExamination", function () {
        var selectedProducts = [];
        $('.multipleProductBlockLabExamination').each(function (key, item) {
            var item = $(item);
            selectedProducts.push({
                'product_id': item.find('.selectLabExpertiseProduct').val(),
                'sample_quantity': item.find('.LabExpertiseProductSampleQuantity').val(),
                'sample_measurement_unit': item.find('.selectMeasurementUnit').val(),
                'sample_number': item.find('.LabExpertiseProductSampleNumber').val()
            })
        });

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('user-documents/laboratory/get-products-view'),
            data: {
                saf_number: safNumber,
                sub_application_id: applicationId,
                selectedProducts: selectedProducts,
                type: 'add'
            },
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    $('.labExaminationCrudProductsContainer').html(result.data.productsView);

                    resetSelect2Values(true);
                }
            },
            error: function (request, status, error) {
                showError(self, request);
            }
        });
    });

    $(document).on("click", ".deleteProductLabExamination", function () {
        if ($('.multipleProductBlockLabExamination').length == 1) {
            return false;
        } else {
            $(this).closest('.multipleProductBlockLabExamination').remove();
        }

        var selectedProducts = [];
        $('.multipleProductBlockLabExamination').each(function (key, item) {
            var item = $(item);
            selectedProducts.push({
                'product_id': item.find('.selectLabExpertiseProduct').val(),
                'sample_quantity': item.find('.LabExpertiseProductSampleQuantity').val(),
                'sample_measurement_unit': item.find('.selectMeasurementUnit').val(),
                'sample_number': item.find('.LabExpertiseProductSampleNumber').val()
            })
        });

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('user-documents/laboratory/get-products-view'),
            data: {
                saf_number: safNumber,
                selectedProducts: selectedProducts,
                type: 'remove'
            },
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    $('.labExaminationCrudProductsContainer').html(result.data.productsView);

                    resetSelect2Values(true);
                }
            },
            error: function (request, status, error) {
                showError(self, request);
            }
        });
    });

    $(document).on('click', '.getLabResult', function () {
        var self = $(this);
        $.ajax({
            type: 'POST',
            url: $cjs.admPath('user-documents/laboratory/get-lab-result'),
            data: {
                sub_application_id: self.data('id'),
            },
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    $('.labExpertiseResultModalBody').html(result.data.result);
                    updateTotalPrice();
                    $('#laboratoryResultModal').modal('show')
                }
            },
            error: function (request, status, error) {
                showError(self, request);
            }
        });
    });

    $('#laboratoryCrudModal').on('hidden.bs.modal', function () {
        $('#selectLabExpertise').val('').trigger('change');
        $('#labExpertiseCrudModalBody').html('');
        $('.alertDivForLabExamination').hide();
        $('.alertDivForLabExamination').text('');
    });
}

function blockEnabledOrDisabled (block, action, unitId = '') {
    block.find('.selectLabExpertiseProduct').prop('disabled', action);
    block.find('.LabExpertiseProductSampleQuantity').prop('disabled', action);
    block.find('.selectMeasurementUnit').val(unitId).trigger('change');
    block.find('.selectMeasurementUnit').prop('disabled', action);
    block.find('.LabExpertiseProductSampleNumber').prop('disabled', action);
}

function showError (self, request) {
    json = $.parseJSON(request.responseText);

    self.prop('disabled', false);
    if (json.errors.length) {
        toastr.error(value);
    }
}

function updateTotalPrice() {
    var value = 0;
    $('.price').each(function (i, v) {
        value += parseFloat($(v).text() || 0);
    });

    $('.labExaminationTotal').val(value);
}

function resetSelect2Values(onlyLabProducts = false) {

    $('.selectLabExpertiseProduct').select2({ allowClear: true,
        placeholder: $trans("swis.saf.lab_examination.select_product_placeholder"),
        language: {
            noResults: function (params) {
                return $trans("swis.saf.lab_examination.no_results_found_products");
            }
        }
    });
    $('#selectLabExpertiseLaboratory').select2({ allowClear: true,
        placeholder: $trans("swis.saf.lab_examination.select_laboratory_placeholder"),
        language: {
            noResults: function (params) {
                return $trans("swis.saf.lab_examination.no_results_found_laboratories");
            }
        }
    });
    $('.selectMeasurementUnit').select2({ allowClear: true, placeholder: $trans("swis.saf.lab_examination.select_measurement_unit_placeholder") });

    if (!onlyLabProducts) {
        $('#selectLabExpertiseProductCustom').select2({ allowClear: true, placeholder: "" });
        $('.indicatorSelect').select2({ allowClear: true,
            placeholder: $trans("swis.saf.lab_examination.select_indicator_placeholder"),
            language: {
                noResults: function (params) {
                    return $trans("swis.saf.lab_examination.no_results_found_indicators");
                }
            }
        });
    }
}

function updateCorrespondField (table, productId) {
    var correspond = 1;
    var correspondClass = $('.correspond'+productId);

    if (!table.find('tr').length) {
        correspond = 3
    } else {
        table.find('tr').each(function (i, v) {
            var checked = $(v).find('.correspond_or_not:checked').val();
            if (checked === undefined) {
                correspond = 3
                return false;
            } else if (checked == 0) {
                correspond = 2
                return false;
            }
        });
    }

    if (correspond === 1) {
        correspondClass.text($trans('swis.user-documents.lab_examinations.correspond.yes'))
    } else if (correspond === 2) {
        correspondClass.text($trans('swis.user-documents.lab_examinations.correspond.no'))
    } else {
        correspondClass.text('')
    }
    $('.labExaminationCorrespond'+productId).val(correspond)
}