function renderNewObligationField() {

    var incNumber = 1;
    var totalSum;
    var obligationTableBody = $('#obligationsTBody');

    $('.plusMultipleFieldsObligation').on('click', function () {

        var self = $(this);
        spinnerLoaderForButton(self)
        $.ajax({
            url: $cjs.admPath(userDocUrl + 'get-obligation-row'),
            type: 'post',
            dataType: 'json',
            data: {incrementNumber: incNumber},
            success: function (result) {

                obligationTableBody.append(result.data.html);

                $('.obligation-type').select2({placeholder: $selectLabel, allowClear: true});

                incNumber++;

                spinnerLoaderForButton(self,true)
                animateScrollToElement(obligationTableBody.find('tr:last'),100,200)
            },
            error: function (xhr, status, error) {
            },
        });

    });

    $(document).on('change', '.obligation-type', function () {

        var self = $(this);
        var selfTr = $(this).closest('tr');

        var accountNumber = self.find(':selected').attr('data-account_number');

        selfTr.find('.budget-line').val(accountNumber);

    });

    $(document).on('change', '.freeSumObligation', function () {

        totalSum = 0;

        var value = parseFloat($(this).val() ? $(this).val() : 0);

        $('.freeSumObligation').each(function (index, value) {
            totalSum += parseFloat($(value).val() ? $(value).val() : 0)
        });

        $('.totalObligationSum').text(parseFloat(totalSum).toFixed(2));
        $('.totalObligationBalanceSum').text(parseFloat(totalSum).toFixed(2));

        $(this).val(value.toFixed(2));
        $(this).closest('tr').find('.freeSumObligationBalance').text(value.toFixed(2));

    });

    $(document).on('click', '.obligation-delete', function () {

        var currentRowSum = $(this).closest('tr').find('.freeSumObligation').val();

        if (currentRowSum !== '') {
            totalSum = totalSum - currentRowSum;

            $('.totalObligationSum').text(parseFloat(totalSum).toFixed(2));
            $('.totalObligationBalanceSum').text(parseFloat(totalSum).toFixed(2));
        }

        $(this).closest('tr').remove();

    });

    $('.obligation-delete-ajax').on('click', function () {
        totalSum = 0;
        var self = $(this);

        modalConfirmDelete(function (confirm) {

            if (confirm) {

                $('.obligation-delete-ajax').prop('disabled', true);

                loadContent();

                $.ajax({
                    url: $cjs.admPath('/single-application/delete-free-sum-obligation'),
                    type: 'post',
                    dataType: 'json',
                    data: {obligationId: self.data('obligationId')},
                    success: function (data) {

                        $('.obligation-delete-ajax').prop('disabled', false);
                        self.closest('tr').remove();

                        $('.freeSumObligation').each(function (index, value) {
                            totalSum += parseFloat($(value).val() ? $(value).val() : 0)
                        });

                        $('.totalObligationSum').text(parseFloat(totalSum).toFixed(2));
                        $('.totalObligationBalanceSum').text(parseFloat(totalSum).toFixed(2));

                        loadContent();
                    },
                    cache: true
                });
            }
        });

    });
}