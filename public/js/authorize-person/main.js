var modalId = 'userInfo';

/*
 * Modal Options
 */
var optionsModal = {
    showHeader: false,
    closeBtn: false,
    footerButtons: {
        buttons: {
            success: {
                click: function (event) {
                    $('input[name="authorized_first_name"]').val($('.person_first_name').data('val'));
                    $('input[name="authorized_last_name"]').val($('.person_last_name').data('val'));

                    $('#' + modalId).modal('hide')
                },
                title: $trans('news.field.approved.yes'),
                class: 'btn-authorize'
            },
            close: {
                click: function (event) {
                    $('#' + modalId).modal('hide');
                    $('#form-data')[0].reset()
                },
                title: $trans('news.field.approved.no'),
                class: 'btn-reject btn-danger'
            }
        }
    }
};

/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
        "order": [],
        // "order": [[0, "desc"]],
        lengthChange: true,
        iDisplayLength: 25
    },

    customOptions: {
        afterInitCallback: function () {
        },
        tableDataPath: 'authorize-person/table',
        searchPath: 'authorize-person',
        columnsRender: {
            menu_name: {
                render: function (row) {
                    if (row.menu_name) {
                        return $trans(row.menu_name);
                    }
                    return ''
                }
            },
            role_status: {
                render: function (row) {
                    if (row.role_status) {
                        return '<span class="label label-primary">' + $trans('core.base.label.status.active') + '</span>';
                    }
                    return '<span class="label label-danger">' + $trans('core.base.label.status.inactive') + '</span>';
                }
            }
        }
    },

};

/*
 * Options for Form Request
 */
var optionsForm = {
    searchPath: '/authorize-person',
    addPath: '/authorize-person/create',
};

// Init Datatable, Form
var $authorizePerson = new FormRequest('form-data', optionsForm);
var $provideARole = new FormRequest('provide-role', {});
var $authorizeTable = new DataTable('list-data', optionsTable);
var $authorizePersonModal = new Modal(modalId, optionsModal);

//
var changedModules = $('#changedModules');
var roleTableBody = $('#roleTable tbody');
var currentChangedModulesArr = [];

$(document).ready(function () {

    // init
    $authorizeTable.init();
    $authorizePersonModal.init();
    $authorizePerson.init();
    $provideARole.init();

    // -----------

    getUserBySSN();

    getRolesOfMenuIndexPage();

    getRolesOfMenu();

    getAttributesOfRole();

    getValuesOfAttribute();

    renderRoleRow();

    getChangedModules();

});

function getUserBySSN() {

    var authorizedSSN = $('input[name="authorized_ssn"]');
    var authorizedPassport = $('input[name="authorized_passport"]');
    var authorizedFields = $('.get-authorized-info');

    if (authorizedFields.length) {
        $(document).on('input', authorizedFields, function (e) {

            e.preventDefault();

            // if (authorizedSSN.val().length >= $ssnMinLength && authorizedPassport.val().length >= parseInt($passportMinLength)) {
            if (authorizedSSN.val().length >= $ssnMinLength) {

                var authorizeModal = $('#' + modalId);

                authorizedFields.prop('disabled', true);
                authorizedSSN.addClass('loading-state');

                $('input[name="authorized_first_name"]').val('');
                $('input[name="authorized_last_name"]').val('');

                $error.show('form-error', []);

                $.ajax({
                    type: 'POST',
                    url: $cjs.admPath('authorize-person/get-person-info'),
                    // data: {authorized_ssn: authorizedSSN.val(), authorized_passport: authorizedPassport.val()},
                    data: {authorized_ssn: authorizedSSN.val()},
                    dataType: 'json',
                    success: function (result) {

                        if (result.status === 'OK') {

                            var fName = '';
                            var lName = '';

                            for (var i = 0; i < result.data.first_name.length; i++) {
                                if (i < 2) {
                                    fName += result.data.first_name[i]
                                } else {
                                    fName += '*';
                                }
                            }

                            for (var j = 0; j < result.data.last_name.length; j++) {
                                if (j < 2) {
                                    lName += result.data.last_name[j]
                                } else {
                                    lName += '*';
                                }
                            }

                            var str = '<h2 class="m-t-none">' + $trans('swis.authorized_user.modal.title') + '</h2><hr />' +
                                '<h2 data-val="' + result.data.first_name + '" class="person_first_name">' + $trans('swis.authorized_first_name') + '- ' + fName + '</h2>' +
                                '<h2 data-val="' + result.data.last_name + '" class="person_last_name" >' + $trans('swis.authorized_last_name') + '- ' + lName + '</h2>';

                            authorizeModal.find('.modal-body').html(str);
                            authorizeModal.modal({backdrop: 'static', keyboard: false})

                        } else if (result.status === 'INVALID_DATA') {
                            if (result.errors) {
                                $error.show('form-error', result.errors);
                            }
                        }

                        authorizedFields.prop('disabled', false);
                        authorizedSSN.removeClass('loading-state');
                    }
                });
            }
        })
    }
}

function getRolesOfMenuIndexPage() {

    var menuSelect = $('#menu');
    var rolesSelect = $('#roles');

    menuSelect.change(function () {

        select2Reset(rolesSelect);
        rolesSelect.prop('disabled', true);

        sendAjaxGetRolesByMenuID('authorize-person/roles-and-menus-search', $(this).val(), rolesSelect);

    });

}

function getRolesOfMenu() {

    $(document).on('change', '.menu', function () {

        var selfTr = $(this).closest('tr');
        var rolesSelect = selfTr.find('select.rolesList');
        var roleInput = selfTr.find('input.role');
        var roleGroupInput = selfTr.find('input.role-group');
        var attributeValuesSelect = selfTr.find('select.attribute-value');
        var attributeValuesDiv = selfTr.find('div.attribute-values');
        var attributesSelect = selfTr.find('select.attribute');
        var attributesSelectMultiple = selfTr.find('select.attribute-multiple');
        var showOnlySelfSelect = selfTr.find('select.show-only-self');

        rolesSelect.prop('disabled', true);

        select2Reset(rolesSelect);
        select2Reset(attributesSelect);
        select2Reset(attributeValuesSelect);
        select2Reset(attributesSelectMultiple);
        select2Reset(showOnlySelfSelect);
        attributesSelect.prop('disabled', true);
        attributesSelectMultiple.prop('disabled', true);
        showOnlySelfSelect.prop('disabled', true);
        roleInput.val('');
        roleGroupInput.val('');

        //
        attributeValuesDiv.addClass('hide');
        attributesSelect.closest('.form-group').removeClass('hide');
        attributesSelectMultiple.closest('.form-group').addClass('hide');

        sendAjaxGetRolesByMenuID('menu/get-roles', $(this).val(), rolesSelect, showOnlySelfSelect);
    })
}

function getAttributesOfRole() {

    $(document).on('change', '.rolesList', function () {

        var self = $(this);
        var selfTr = self.closest('tr');

        if (self.val()) {

            var currentMenu = selfTr.find('select.menu').val();
            var roleGroupInput = selfTr.find('input.role-group');
            var roleInput = selfTr.find('input.role');
            var isRoleGroup = self.find(':selected').attr('data-group');

            var attributesSelect = selfTr.find('select.attribute');
            var attributesSelectFormGroup = attributesSelect.closest('.form-group');

            var attributesSelectMultiple = selfTr.find('select.attribute-multiple');
            var attributesSelectMultipleFormGroup = attributesSelectMultiple.closest('.form-group');

            var attributesValuesSelect = selfTr.find('select.attribute-value');
            var attributesValuesSelectFormGroup = attributesValuesSelect.closest('.form-group');

            attributesValuesSelectFormGroup.addClass('hide');

            roleGroupInput.val('');
            roleInput.val('');

            if (isRoleGroup) {
                roleGroupInput.val(self.val())
            } else {
                roleInput.val(self.val())
            }

            // --------------
            if (currentMenu === reportMenu) {

                attributesSelectFormGroup.addClass('hide');
                attributesSelectMultipleFormGroup.removeClass('hide');

                attributesSelectMultiple.find('option').remove().val('').trigger('change');
                attributesSelectMultiple.prop('disabled', false);

                $.ajax({
                    type: 'POST',
                    url: $cjs.admPath('role/get-multiple-attributes'),
                    data: {menu_id: reportMenu},
                    dataType: 'json',
                    success: function (result) {

                        if (result.status === 'OK') {
                            if (typeof result.data.reports != 'undefined') {
                                $.each(result.data.reports, function (k, v) {

                                    var newOption = new Option(v.name, v.id, false, false);
                                    attributesSelectMultiple.append(newOption);
                                })
                            }
                        }
                    }
                });
            }

            // --------------
            if (currentMenu === myApplicationMenu) {

                attributesSelectFormGroup.removeClass('hide');
                attributesSelectMultipleFormGroup.addClass('hide');

                select2Reset(attributesSelect);
                attributesSelect.prop('disabled', true);

                select2Reset(attributesValuesSelect);
                attributesValuesSelect.prop('disabled', true);

                $.ajax({
                    type: 'POST',
                    url: $cjs.admPath('role/get-attributes'),
                    data: {role: self.val(), isRoleGroup: isRoleGroup},
                    dataType: 'json',
                    success: function (result) {

                        if (result.status === 'OK') {
                            attributesSelect.prop('disabled', false);
                            if (typeof result.data.attributes.saf != 'undefined') {
                                $.each(result.data.attributes.saf, function (k, v) {

                                    var newOption = new Option(v, k, false, false);

                                    newOption.setAttribute('data-type', 'saf');
                                    attributesSelect.append(newOption);
                                })
                            }

                            if (typeof result.data.attributes.mdm != 'undefined') {
                                $.each(result.data.attributes.mdm, function (k, v) {

                                    var newOption = new Option(v, k, false, false);

                                    newOption.setAttribute('data-type', 'mdm');
                                    attributesSelect.append(newOption);
                                })
                            }
                        }
                    }
                });
            }

            // --------------
            if (currentMenu === paymentAndObligationsMenu && isAgency) {

                attributesSelectFormGroup.removeClass('hide');
                attributesSelectMultipleFormGroup.addClass('hide');

                select2Reset(attributesSelect);
                attributesSelect.prop('disabled', true);

                select2Reset(attributesValuesSelect);
                attributesValuesSelect.prop('disabled', true);

                $.ajax({
                    type: 'POST',
                    url: $cjs.admPath('role/get-payment-and-obligations-attributes'),
                    data: {role: self.val()},
                    dataType: 'json',
                    success: function (result) {

                        if (result.status === 'OK') {
                            attributesSelect.prop('disabled', false);
                            if (typeof result.data.attributes != 'undefined') {
                                $.each(result.data.attributes, function (k, v) {

                                    var newOption = new Option(v, k, false, false);

                                    newOption.setAttribute('data-type', 'paymentAndObligation');
                                    attributesSelect.append(newOption);
                                })
                            }
                        }
                    }
                });
            }

            animateScrollToElement(selfTr, 0, 0);
        }
    });
}

function getValuesOfAttribute() {

    $(document).on('change', 'select.attribute', function () {

        var self = $(this);
        var selfTr = self.closest('tr');
        var attributeValuesSelect = selfTr.find('select.attribute-value');
        var attributeValuesFormGroup = selfTr.find('.attribute-values');
        var attributeType = self.find(':selected').attr('data-type');
        var attributeTypeInput = selfTr.find('.attribute_type');
        var rolesSelect = selfTr.find('select.rolesList');
        var isRoleGroup = rolesSelect.find(':selected').attr('data-group');

        select2Reset(attributeValuesSelect);
        attributeTypeInput.val(attributeType);

        if (self.val()) {

            $.ajax({
                type: 'POST',
                url: $cjs.admPath('role/get-attributes/values'),
                data: {attribute: self.val(), type: attributeType, role: rolesSelect.val(), isRoleGroup: isRoleGroup},
                dataType: 'json',
                success: function (result) {

                    if (result.status === 'OK') {
                        attributeValuesFormGroup.removeClass('hide');
                        attributeValuesSelect.prop('disabled', false);

                        if (typeof result.data.attributeValues[attributeType] != 'undefined') {
                            $.each(result.data.attributeValues[attributeType], function (k, v) {

                                var newOption = new Option(v, k, false, false);

                                newOption.setAttribute('data-type', attributeType);
                                newOption.setAttribute('title', v);
                                attributeValuesSelect.append(newOption);

                            });

                            animateScrollToElement(self, 0, 0);
                        }
                    }
                }
            });

        }
    })
}

function sendAjaxGetRolesByMenuID(ajaxUrl, menuId, rolesSelect, showOnlySelfSelect) {

    $.ajax({
        type: 'POST',
        url: $cjs.admPath(ajaxUrl),
        data: {menu_id: menuId},
        dataType: 'json',
        success: function (result) {
            if (result.status === 'OK') {

                var selectRole = false;
                if (result.data.roles.length === 1) {
                    selectRole = true;
                }

                $.each(result.data.roles, function (key, value) {

                    var rolegroupStr = '';
                    if (value.role_group != '0') {
                        rolegroupStr = "data-group='true'";
                    }

                    if (value.name) {

                        rolesSelect
                            .append($("<option " + rolegroupStr + "></option>")
                                .attr("value", value.id)
                                .attr("title", value.description ? value.description : value.name)
                                .text(value.name));

                        if (selectRole) {
                            rolesSelect.val(value.id);
                            rolesSelect.trigger('change')
                        }
                    }
                });

                rolesSelect.prop('disabled', false);
                animateScrollToElement(rolesSelect, 0, 0);

                // ------

                if (typeof showOnlySelfSelect != "undefined") {

                    if (result.data.showOnlySelf) {
                        $.each(result.data.showOnlySelf, function (key, value) {

                            var defaultSelected = false;
                            if (result.data.showOnlySelf.length === 1 || parseInt(value.id) === 1) {
                                defaultSelected = true;
                            }

                            var newOption = new Option(value.name, value.id, defaultSelected, defaultSelected);

                            showOnlySelfSelect.append(newOption);
                            showOnlySelfSelect.trigger('change.select2');

                        });
                    }

                    showOnlySelfSelect.prop('disabled', false);

                }
            }
        }
    });
}

function renderRoleRow() {

    $(document).on('click', '.btn-more', function (e) {
        e.preventDefault();

        var self = $(this);
        var num = roleTableBody.find('tr:last-child').data('id') + 1;
        num = isNaN(num) ? 1 : num;

        spinnerLoaderForButton(self);

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('authorize-person/render-table'),
            data: {id: num},
            dataType: 'json',
            success: function (result) {

                if (result.status === 'OK') {

                    roleTableBody.append(result.data.html);

                    var newRoleRow = roleTableBody.find('tr:last');

                    select2Init(newRoleRow);
                    datePickerInit(newRoleRow);
                    animateScrollToElement(newRoleRow, 0, 100);

                    spinnerLoaderForButton(self, true);
                }
            }
        });

    }).on('click', '.multiple .btn-remove', function (e) {

        e.preventDefault();

        $(this).closest('tr').remove();

        return false;
    });
}

function getChangedModules() {
    roleTableBody.find('tr :input').change(function () {
        setChangedModules($(this).closest('tr').find('.menu').val());
    })
}

function setChangedModules(module) {

    //
    if (currentChangedModulesArr.indexOf(module) === -1) {
        currentChangedModulesArr.push(module);
    }

    changedModules.val(currentChangedModulesArr.join(','))
}


