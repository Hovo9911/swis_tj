/*
 * Modal Options
 */
var optionsModal = {
    showHeader: false,
    footerButtons: {
        buttons: {
            success: {
                click: function (event) {
                    $('#info').modal('hide')
                },
                title: 'yes',
                class: 'btn-authorize'
            },
        }
    },
};

/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        "order": [[1, "desc"]],
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
        "processing": false,
        lengthChange: true,
        iDisplayLength: 25
    },
    customOptions: {
        afterInitCallback: function () {
        },
        tableDataPath: 'data-model/table',
        searchPath: 'data-model',
    }
};

/*
 * Options for Form Request
 */
var optionsForm = {
    searchPath: '/data-model',
    addPath: '/data-model/create',
    customSaveFunction: function () {
        interFaceFormatGenerate();

        return true;
    },
};

/*
 * Init Datatable, Form, Modal
 */
var $dataModelTable = new DataTable('list-data', optionsTable);
var $dataModel = new FormRequest('form-data', optionsForm);
var $dataModelModal = new Modal('info', optionsModal);

//
var dbType = $('#dbType');
var minMaxInputs = $('.min-max');
var decimalInputs = $('.decimal');
var wcoClassName = $('#wcoClassName');
var eaeuTypeSelect = $('#eaeuType');
var addValuesButton = $('#addValues');
var interFaceFormatInput = $('input[name="interface_format"]');
var clientMaxInput = $('input[name="client_max"]');
var clientMinInput = $('input[name="client_min"]');
var dbMaxInput = $('input[name="db_max"]');
var decimalFractionInput = $('input[name="decimal_fraction"]');
var infoSection = $(".info-section");

addValuesButton.prop('disabled', true);

$(document).ready(function () {

    // init
    $dataModelTable.init();
    $dataModel.init();
    $dataModelModal.init();

    // -------------------

    autocomplete();

    typesChange();

    eaeuType();

    wcoEaeuValuesAdd();

    clientMaxChanged();

    showInfoSection();
});

function autocomplete() {

    var autocomplete = $('input.autocomplete');

    if (autocomplete.length) {

        autocomplete.each(function (k, v) {

            var self = $(this);

            self.typeahead({
                items: 15,
                source: function (query, process) {

                    return $.post($cjs.admPath("/data-model/autocomplete"), {
                        query: query,
                        type: self.data('type'),
                        eaeuType: eaeuTypeSelect.val(),
                    }, function (response) {

                        if (self.data('type') === 'wco') {
                            select2Reset(wcoClassName);
                        }

                        return process(response.options);
                    });
                },

                afterSelect: function (data) {

                    self.attr('data-value', data.values);
                    addValuesButton.prop('disabled', false);

                    // EAEU
                    if (self.data('type') === 'eaeu') {

                        var eaeuData = jQuery.parseJSON(data.values);

                        $('#eaeuMin').val(eaeuData.min);
                        $('#eaeuMax').val(eaeuData.max);
                        $('#eaeuDbType').val(eaeuData.db_type);
                        $('#eaeuDbTotalDigits').val(eaeuData.total_digits);
                        $('#eaeuDbFractionDigits').val(eaeuData.fraction_digits);

                        $('input[name="eaec"]').val(data.id)
                    }

                    // WCO
                    if (self.data('type') === 'wco') {
                        $('input[name="wco"]').val(data.wco_id)
                    }

                    if (data.classNames !== undefined && data.classNames.length > 0) {

                        wcoClassName.prop('disabled', false);

                        if (data.classNames.length === 1) {

                            var newOption = new Option(data.classNamesAndCodes[0], data.classNames[0], true, true);

                            wcoClassName.append(newOption).trigger('change')

                        } else {
                            $.each(data.classNamesAndCodes, function (key, value) {
                                wcoClassName.append($("<option></option>").attr("value", data.classNames[key]).text(value));
                            });
                        }
                    }
                },
            });
        });
    }
}

function wcoEaeuValuesAdd() {

    addValuesButton.click(function () {

        var wcoInput = $("#wcoId");
        var eaeuInput = $("#eaeuId");

        if (wcoInput.attr('data-value')) {
            var wcoData = jQuery.parseJSON(wcoInput.attr('data-value'));
            var wcoMin = wcoData.min;
            var wcoMax = wcoData.max;
        }

        if (eaeuInput.attr('data-value')) {
            var eaeuData = jQuery.parseJSON(eaeuInput.attr('data-value'));
            var eaeuMin = parseInt(eaeuData.min);
            var eaeuMax = parseInt(eaeuData.max);
            var eaeuTotalDigits = parseInt(eaeuData.total_digits);
            var eaeuFractionDigits = parseInt(eaeuData.fraction_digits);

            eaeuMax = (eaeuTotalDigits > eaeuMax) ? eaeuTotalDigits : eaeuMax;
        }

        minMaxInputs.attr('type', 'number');

        var decimalFraction = '';
        var minValue = wcoMin;
        var maxValue = wcoMax;
        var maxValueClient = eaeuMax;

        if (wcoData && wcoData.decimal) {
            minValue = 1
        }

        if (wcoMin > eaeuMin) {
            minValue = eaeuMin;
        }

        if (wcoMax < eaeuMax) {
            maxValue = eaeuMax;

            maxValueClient = wcoMax;
        }

        if (maxValueClient == 0 || isNaN(maxValueClient)) {
            maxValueClient = wcoMax
        }

        if ((wcoData && wcoData.decimal) || eaeuFractionDigits) {
            wcoData ? wcoData.type = 'dc' : '';
            dbType.val('decimal');
            decimalFraction = (eaeuFractionDigits > wcoMin) ? eaeuFractionDigits : wcoMin;
            minMaxInputs.closest('.form-group').removeClass('hide');
        }

        if ((wcoData && (wcoData.type === 'an' || wcoData.type === 'a')) && (eaeuData && (eaeuData.type === 'an' || eaeuData.type === 'a'))) {
            dbType.val('varchar')
        }

        if (wcoData && wcoData.type === 'n') {
            dbType.val('integer')
        }

        if (minValue === maxValue) {
            dbType.val('char')
        }

        clientMinInput.val(minValue);
        clientMaxInput.val(maxValueClient);
        dbMaxInput.val(maxValue);
        decimalFractionInput.val(decimalFraction);

        if (decimalFraction != '') {
            decimalFraction = ',' + decimalFraction;
            decimalInputs.closest('.form-group').removeClass('hide')
        } else {
            decimalFraction = '';
            decimalInputs.closest('.form-group').addClass('hide')
        }

        $('input[name="type"]').prop('checked', false);
        if (wcoData) {
            $('input[name="type"][value="' + wcoData.type + '"]').prop('checked', true);
        } else {
            $('input[name="type"][value="an"]').prop('checked', true);
        }

        if (wcoData && wcoData.type == 'dc') {
            wcoData.type = 'an'
        }
        if (wcoData) {
            interFaceFormatInput.val(wcoData.type + '..' + maxValueClient + decimalFraction);
        }

        // $('input[name="db_min"]').val(wcoData.min);
    });
}

function eaeuType() {
    eaeuTypeSelect.change(function () {
        $('input[name="eaec_id"]').removeAttr('readonly')
    })
}

function typesChange() {
    $('input[name="type"]').change(function () {

        var currentVal = $(this).val();

        minMaxInputs.attr('type', 'number');
        minMaxInputs.closest('.form-group').removeClass('hide');
        decimalInputs.closest('.form-group').addClass('hide');

        if (currentVal === 'line' || currentVal === 'm') {
            infoSection.addClass('hide');
        } else {
            infoSection.removeClass('hide');
        }

        if (currentVal === 'an' || currentVal === 'a') {
            dbType.val('varchar');
            $('#isContentBlock').show();
        } else {
            $('#isContentBlock').hide();
        }

        if (currentVal === 'n') {
            dbType.val('integer');
        }

        if (currentVal === 'd') {
            minMaxInputs.val(null)
            minMaxInputs.closest('.form-group').addClass('hide');

            dbType.val('date')
        }

        if (currentVal === 'dc') {
            dbType.val('decimal');
            minMaxInputs.attr('type', 'text');
            decimalInputs.closest('.form-group').removeClass('hide');
        } else {
            decimalInputs.val('')
        }

        if (currentVal === 'dt') {
            minMaxInputs.val(null)
            minMaxInputs.closest('.form-group').addClass('hide');

            dbType.val('datetime')
        }

        // interFaceFormatInput.val(firstCharacter + '..' + clientMax);
    })
}

function clientMaxChanged() {
    clientMaxInput.change(function () {

        var currentVal = $(this).val();
        var interfaceFormatVal = interFaceFormatInput.val()

        interfaceFormatVal = interfaceFormatVal.split('..')[0] + '..' + currentVal
        interFaceFormatInput.val(interfaceFormatVal)
    })
}

function interFaceFormatGenerate() {

    var clientMax = clientMaxInput.val();
    var dbType = $('input[name="type"]:checked').val();

    var firstCharacter = '';
    var decimalFraction = decimalFractionInput.val();

    if (clientMax.length == 0) {
        clientMax = 35;
    }

    if (dbType === 'a') {
        firstCharacter = 'a'
    }

    if (dbType === 'n') {
        firstCharacter = 'n'
    }

    if (dbType === 'an' || dbType === 'd' || dbType === 'dt') {
        firstCharacter = 'an'
    }

    if (dbType === 'dc') {
        firstCharacter = 'an'
    }

    if (decimalFraction.length > 0) {
        decimalFraction = ',' + decimalFraction;
    }

    interFaceFormatInput.val(firstCharacter + '..' + clientMax + decimalFraction);
}

function showInfoSection() {

    var checkedRadioButton = $('input[name=type]:checked').val();

    if (checkedRadioButton === 'line' || checkedRadioButton === 'm') {
        infoSection.addClass('hide');
    } else {
        infoSection.removeClass('hide');
    }
}


