var customHideSearchParams = true;

// Options Form Users DataTable
var optionsUserTable = {
    datatableOptions: {
        searching: false,
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
        "order": [[0, "desc"]],
        lengthChange: true,
        iDisplayLength: 25

    },
    customOptions: {
        afterInitCallback: function () { },
        tableDataPath: 'monitoring/users/table',
        searchPath: 'monitoring',
    }
};

// Options Form Users Roles DataTable
var optionsUserRolesTable = {
    datatableOptions: {
        searching: false,
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
        "order": [[0, "desc"]],
        lengthChange: true,
        iDisplayLength: 25

    },
    customOptions: {
        afterInitCallback: function () { },
        tableDataPath: 'monitoring/user-roles/table',
        searchPath: 'monitoring',
        columnsRender: {
            role_status: {
                render: function (row) {
                    switch (row.role_status) {
                        case '1':
                            return '<span class="label label-primary">' + $trans('core.base.label.status.active') + '</span>';
                        case '2':
                            return '<span class="label label-danger">' + $trans('core.base.label.status.inactive') + '</span>';
                        case '0':
                            return '<span class="label label-danger">' + $trans('core.base.label.status.deleted') + '</span>';
                    }
                }
            }
        }
    }
};

// Options Form Applications DataTable
var optionsApplicationsTable = {
    datatableOptions: {
        searching: false,
        "order": [[0, "desc"]],
        lengthChange: true,
        iDisplayLength: 25
    },
    customOptions: {
        afterInitCallback: function () { },
        tableDataPath: 'monitoring/applications/table',
        searchPath: 'monitoring',
        columnsRender: {
            state: {
                render: function (row) {
                    if (row.state) {

                        var submittedDate = moment(row['submittedDate']);
                        var currentDate = moment(moment().format("YYYY-MM-DD"));

                        var className = '';
                        if (typeof row.currentStatus != "undefined" && row.currentStatus == 'success') {
                            className = 'table-row-background-success';
                        } else if (typeof row.currentStatus != "undefined" && row.currentStatus == 'rejected') {
                            className = 'table-row-background-rejected';
                        } else if (currentDate.diff(submittedDate, "days") >= 5) {
                            className = 'table-row-background-5-days';
                        } else if (currentDate.diff(submittedDate, "days") >= 2) {
                            className = 'table-row-background-2-days';
                        }

                        return '<span class="' + className + '">' + row.state + '</span>';

                    }
                    return ''
                }
            },
            saf_general_sub_application_express_control_id: {
                render: function (row) {
                    if (row.expressControlCode == 1) {
                        return '<i class="fa fa-circle text-warning"></i> ' + row.saf_general_sub_application_express_control_id;
                    }
                    return row.saf_general_sub_application_express_control_id
                }
            }
        }
    }
};

// Options Form Users DataTable
var optionsPaymentsTable = {
    datatableOptions: {
        searching: false,
        "order": [[0, "desc"]],
        lengthChange: true,
        iDisplayLength: 25

    },
    customOptions: {
        afterInitCallback: function () { },
        tableDataPath: 'monitoring/payments/table',
        searchPath: 'monitoring',
    }
};

var optionsPaysTable = {
    datatableOptions: {
        searching: false,
        "order": [[0, "desc"]],
        lengthChange: true,
        iDisplayLength: 25
    },
    customOptions: {
        afterInitCallback: function () { },
        tableDataPath: 'monitoring/pays/table',
        searchPath: 'monitoring',
    }
}

/*
 * Init Datatable
 */
var $usersTable = new DataTable('users-data', optionsUserTable);
var $userRolesTable = new DataTable('user-roles-data', optionsUserRolesTable);
var $applicationsTable = new DataTable('applications-data', optionsApplicationsTable);
var $paymentsTable = new DataTable('payments-data', optionsPaymentsTable);
var $paysTable = new DataTable('pays-data', optionsPaysTable);

//
$usersTable.mSearchFilterObject = $('#search-filter-users');
$applicationsTable.mSearchFilterObject = $('#search-filter-applications');
$userRolesTable.mSearchFilterObject = $('#search-filter-user-roles');
$paymentsTable.mSearchFilterObject = $('#search-filter-payments');
$paysTable.mSearchFilterObject = $('#search-filter-pays');

//
var modalLoadBody = $('.modal-load-body');

$(document).ready(function () {

    // init
    $usersTable.init();
    $userRolesTable.init();
    $applicationsTable.init();
    $paymentsTable.init();
    $paysTable.init();

    // ---------

    hideSearchParams();

    lastEventsCustomFunctions();

    getAgencySubdivisions();
});

function hideSearchParams() {

    $('.hide-or-show-params').on('click', function (e) {
        e.stopPropagation();

        modalLoadBody.show();

        var formId = $(this).data('form');
        var table = $('#'+$(this).data('tableId'));
        var hashCode = 'hiddenParams'+table.data('moduleForSearchParams');
        var hiddenFields = JSON.parse(localStorage.getItem(hashCode)) || {'params': [], 'columns': []};

        $('.hide-params-section-body').html("");
        $("#"+formId).find('.searchParam').each(function (index, item) {
            var checked = (hiddenFields.params.includes($(item).data('field'))) ? 'checked' : '';
            $('.hide-params-section-body').append('<label><input type="checkbox" class="hide-search-params" '+checked+' data-col="'+$(item).data('field')+'" /> '+$(item).find('label').text()+'</label><br />');
        });

        $('.hide-columns-section-body').html("");
        $.each(table.data('customColumns'), function (index, item) {
            var checked = (hiddenFields.columns.includes(index)) ? 'checked' : '';
            $('.hide-columns-section-body').append('<label><input type="checkbox" class="hide-search-column" '+checked+' data-col="'+index+'" /> '+item+'</label><br />')
        });
        $('.hideButton').data('module', hashCode);
        $('.restoreButton').data('module', hashCode);

        modalLoadBody.hide();
        $('.modal-main-body').show();
        $('#hideOrShowParams').modal('show');
    });

    $('.dataTables-example').each(function (index, item) {
        var hashCode = 'hiddenParams'+$(this).data('moduleForSearchParams');
        var hiddenFields = JSON.parse(localStorage.getItem(hashCode)) || {'params': [], 'columns': []};
        var form = $("#"+$(item).data('formId'));

        form.find('.searchParam').each(function (index, value) {
            if (hiddenFields.params.includes($(value).data('field'))) {
                form.find('.' + $(value).data('field')).hide();
            }
        });
    });

    $('.restoreButton').on('click', function () {
        localStorage.removeItem($(this).data('module'));
        window.location.reload();
    });

    $('.hideButton').on('click', function () {
        var hiddenFields = JSON.parse(localStorage.getItem($(this).data('module'))) || {'params': [], 'columns': []};

        hiddenFields.params = [];
        $('.hide-search-params:checked').each(function (index, item) {
            var field = $(item).data('col');
            hiddenFields.params.push(field)
        });

        hiddenFields.columns = [];
        $('.hide-search-column:checked').each(function (index, item) {
            var field = $(item).data('col');
            hiddenFields.columns.push(field)
        });

        localStorage.setItem($(this).data('module'), JSON.stringify(hiddenFields));
        window.location.reload();
    });
}

function lastEventsCustomFunctions() {

    var lastEventsApplicationStatus = $('#lastEventsApplicationStatus');

    $('#lastEventsDocuments').change(function () {

        var documentCode = $(this).val()

        select2Reset(lastEventsApplicationStatus);
        lastEventsApplicationStatus.prop('disabled', true);

        if(documentCode){
            $.ajax({
                type	 : 'post',
                url		 : $cjs.admPath('/monitoring/get-document-statues'),
                data	 : { documentCode: documentCode},
                dataType : 'json',
                success	 : function(result) {
                    $.each(result, function(key, value) {

                        lastEventsApplicationStatus
                            .append($("<option></option>")
                                .attr("value", value.id)
                                .text(value.state_name));

                    });

                    lastEventsApplicationStatus.prop('disabled', false);
                }
            });
        }
    });
}

function getAgencySubdivisions() {

    var agencySelect = $('#agencies');

    agencySelect.change(function () {

        var self = $(this);
        var selectVal = self.val();
        var agencySubDivisions = $('#subdivision');

        select2Reset(agencySubDivisions,true);

        if (selectVal) {

            self.prop('disabled', true);
            agencySubDivisions.prop('disabled', true);

            $.ajax({
                type: 'POST',
                url: $cjs.admPath('monitoring/get-agency-subdivisions'),
                data: {agency_tax_id: self.val()},
                dataType: 'json',
                success: function (result) {

                    if (result.status === 'OK') {

                        if (typeof result.data.subdivisions != 'undefined' && result.data.subdivisions.length) {
                            $.each(result.data.subdivisions, function (k, v) {

                                agencySubDivisions
                                    .append($("<option></option>")
                                        .attr("value", v.id)
                                        .text(v.name));
                            })
                        }

                        self.prop('disabled', false);
                        agencySubDivisions.prop('disabled', false);
                    }
                }
            });
        }
    });
}