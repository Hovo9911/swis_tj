var $expertiseId = $('#expertiseId').val();
var $labId = $('#labId').val();
var formId = 'form-data';
var formSelector =  $('#'+formId);
var documentList = $('#documentList');
var userLabUrl = 'user-laboratory/' + $labId + '/expertise/' + $expertiseId + '/';
var listData = $('#list-data');

/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        "order": [],
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
        lengthChange: true,
        iDisplayLength: 25
    },
    customOptions: {
        afterInitCallback: function () { },
        tableDataPath: listData.data('action'),
        searchPath: listData.data('search-path'),
        actions: {
            edit: {
                render: function (row) {
                    if (!row.send_indicators) {
                        return '<a href="' + $cjs.admPath("/user-laboratory/" + $labId + "/expertise/" + row.id + "/edit") + '"  class="btn btn-edit" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.edit.button') + '"><i class="fa fa-pen"></i></a>'
                    } else {
                        return '<a href="javascript:;"  class="btn btn-edit" disabled="" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.edit.button') + '"><i class="fa fa-pen"></i></a>'
                    }
                }
            },
            custom: {
                render: function (row) {

                    if (row.has_indicators) {
                        if (!row.send_indicators) {
                            return '<a href="javascript:;" data-id="' + row.id + '" class="btn btn--icon-send  btn-status update-status btn--icon" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.send.button') + '"><i class="fas fa-long-arrow-alt-right"></i></a>'
                        } else {
                            return '<a href="javascript:;" disabled class="btn btn--icon-send btn-status btn--icon"  data-toggle="tooltip" title="' + $trans('swis.base.tooltip.send.button') + '" ><i class="fas fa-long-arrow-alt-right"></i></a>'
                        }
                    }
                }
            },
            view: {
                render: function (row) {
                    return '<a href="' + $cjs.admPath("user-laboratory/" + $labId + "/expertise/" + row.id + "/view") + '" class="btn btn-view" data-toggle="tooltip"  title="' + $trans('swis.base.tooltip.view.button') +'"><i class="fa fa-eye"></i></a>'
                }
            },
        },
        columnsRender: {
            saf_product_id: {
                render: function (row) {
                    if (row.saf_product_id) {
                        return $.parseJSON(row.saf_product_id)
                    }
                    return ''
                }
            }
        }

    }
};

/*
 * Options Form DataTable(Document)
 */
var optionsDocumentSearchTable = {
    datatableOptions: {
        searching: false,
        drawCallback: function (data, json) {},
        initComplete: function (settings, json) {},
        "order": [[1, "desc"]],
    },
    customOptions: {
        afterInitCallback: function (data) {
        },
        tableDataPath: 'folder/document-search',
        columnsRender: {
            document_file: {
                render: function (row) {
                    if (row.document_file) {
                        return '<a target="_blank" href="' + row.document_file + '" class="btn btn-success btn-xs btn--icon"  title="' + $trans('swis.base.tooltip.view.button') + '" data-toggle="tooltip" ><i class="fas fa-eye"></i></a>'
                    }

                    return ''
                }
            },
            id: {
                render: function (row) {
                    if (row.incrementedId) {
                        return "<span class='document-incremented-real-id'>" + row.incrementedId + "</span>";
                    }
                }
            },
            document_form: {
                render: function (row) {
                    if (row.document_form) {
                        return $trans('swis.document.document_form.'+row.document_form+'.title');
                    }
                }
            },
        }
    }
};


/*
 * Options for Form Request
 */
var optionsForm = {
    searchPath: formSelector.data('search-path'),
    addPath: '/user-laboratory/create',
};

/*
 * Init Datatable, Form
 */
var $userLaboratoryTable = new DataTable('list-data', optionsTable);
var $userLaboratory = new FormRequest(formId, optionsForm);
var $documentSearch = new DataTable('search-document', optionsDocumentSearchTable);

$(document).ready(function () {

    // init
    $userLaboratoryTable.init();
    $userLaboratory.init();

    //Documents Part
    documentSearch();

    addDocument();

    renderDocumentInputs();

    storeDocument();

    deleteDocument();

    //Lab Part
    expertiseChangeStatus();

    viewRegime();
});

function expertiseChangeStatus() {
    $(document).on('click', '.update-status', function () {

        var self = $(this);

        modalConfirmSend(function (confirm) {

            if (confirm) {

                $.ajax({
                    type: 'POST',
                    url: $cjs.admPath('user-laboratory/' + $labId + '/expertise/' + self.data('id') +'/update-status'),
                    dataType: 'json',
                    success: function (result) {

                        if (result.status === 'OK') {
                            location.href = result.data.backToUrl;
                        }
                    }
                })
            }
        });
    })
}

function viewRegime() {
    if(typeof $viewRegime !== 'undefined' && $viewRegime){
        disableFormInputs($('.lab-all-content'),false,true);
        $('.mc-nav-button-save').remove()
    }
}

// DOCUMENT CLOUD IN USER DOCUMENTS
var initDataTable = true;

function documentSearch() {

    $('.documentSearch').click(function (e) {

        e.preventDefault();

        var self = $(this);
        var documentSearchFilter = $('#documentSearchFilter');
        var documentSearchDiv = $('.searchDoc');

        self.prop('disabled',true);

        if (initDataTable) {
            initDataTable = false;
            $documentSearch.init();
        }

        documentSearchDiv.removeClass('hide');
        $documentSearch.mSearchFilterForm = documentSearchFilter;
        $documentSearch.reload();

        $('#search-document').on('xhr.dt', function (e, settings, data, xhr) {
            self.prop('disabled',false);
        })
    })
}

function addDocument() {

    var num = 1;
    var documentErrorInfo = $('#documentErrorInfo');

    $('.addDocument').click(function (e) {

        e.preventDefault();

        var self = $(this);
        var tableTrLength = documentList.find('tbody tr').length + 1;
        var selectedDocs = {};
        var searchDocTable = $('#search-document');

        if (tableTrLength > 1 && num < tableTrLength) {
            num = tableTrLength;
        }

        $.each(searchDocTable.find('tbody input:checked'), function () {
            selectedDocs[$(this).closest('tr').find('.document-incremented-real-id').text()] = $(this).val();
        });

        searchDocTable.find('input:checkbox').prop('checked', false).closest('tr.doc-exist').removeClass('doc-exist');
        documentErrorInfo.html('');

        if (Object.keys(selectedDocs).length > 0) {

            self.prop('disabled',true);

            $.ajax({
                type: 'POST',
                url: $cjs.admPath(userLabUrl + 'add-document'),
                data: {documentItems: selectedDocs, num: num},
                dataType: 'json',
                success: function (result) {

                    if (result.status === 'OK') {
                        documentList.find('tbody').append(result.data.html);
                        documentList.closest('.table-responsive').removeClass('hide');

                        if (result.data.newDocumentCount) {
                            animateScrollToElement(documentList.find('tbody tr:last'),0,200);
                            num++;
                        }

                        if (typeof result.data.error !== 'undefined') {
                            var $dataError = result.data.error;

                            documentErrorInfo.html('<div class="alert alert-info">' + $dataError.message + '</div>');

                            $.each($dataError.docIds, function (k, v) {
                                searchDocTable.find('input:checkbox[value="' + v + '"]').closest('tr').addClass('doc-exist');
                            })
                        }
                    }

                    self.prop('disabled',false);
                }
            });
        }

    });

}

function storeDocument() {

    $(document).on('click', '.storeDocument', function (e) {

        e.preventDefault();

        var holderInfo = $('#holderInfo :input');
        var productInfo = $(this).closest('tr').find(':input');
        var data = $.merge(holderInfo, productInfo);
        var self = $(this);
        var selfTr = self.closest('tr');

        self.prop('disabled', true);
        $error.show('form-error', []);

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('user-laboratory/' + $labId + '/expertise/' + $expertiseId + '/store-document'),
            data: data,
            dataType: 'json',
            success: function (result) {
                downloadBtn = '';

                if (result.status === 'OK') {

                    selfTr.find('input').prop('readonly', true);
                    selfTr.find('.doc-id').val(result.data.docId);
                    selfTr.find('.access-password').html(result.data.accessPassword);
                    selfTr.find('.document-form').html(result.data.documentForm);

                    if (result.data.documentFile) {

                        var downloadBtn = result.data.documentFileName;
                        var viewBtn = '<a target="_blank" href="' + result.data.documentFile + '" class="btn btn-success btn-xs btn--icon" title="' + $trans('swis.base.tooltip.view.button') + '" data-toggle="tooltip" ><i class="fas fa-eye"></i></a>';

                        selfTr.find('.actions-list').prepend(viewBtn);
                    }

                    selfTr.find('.g-upload-container').html(downloadBtn);

                    self.tooltip('destroy');
                    self.remove();
                }

                if (result.errors) {

                    $error.show('form-error', result.errors);

                    var num = selfTr.data('num');

                    var errors = result.errors;

                    $.each(productInfo, function (k, v) {
                        var inputName = $(this).attr('name');

                        if (inputName !== undefined) {
                            if (errors[inputName] !== undefined) {

                                $('#form-error-' + inputName + '-' + num).addClass('form-error-text').html(errors[inputName]).css({'display': 'block'});

                            }
                        }
                    });

                    self.prop('disabled', false);

                }
            },
            error: function() {
                self.prop('disabled', false);
            },
        })
    })
}

function renderDocumentInputs() {

    $('.plusMultipleFieldsDocument').click(function () {

        var self = $(this);
        var num = documentList.find('tbody tr:last-child').data('num') + 1 ;
        num = isNaN(num) ? 1 : num;

        spinnerLoaderForButton(self);

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('user-laboratory/' + $labId + '/expertise/' + $expertiseId + '/render-document-inputs'),
            data: {num: num},
            dataType: 'json',
            success: function (result) {

                if (result.status === 'OK') {
                    documentList.find('tbody').append(result.data.html);
                    documentList.closest('.table-responsive').removeClass('hide');

                    refAutocomplete();

                    datePickerInit(documentList.find('tr[data-num="'+num+'"]'));

                    $gUploader.init();
                    $('.g-upload').not('.has-uploader').each(function () {
                        $gUploader.onX($(this));
                    });

                    spinnerLoaderForButton(self,true);
                }
            }
        });
    })
}

function deleteDocument() {
    $(document).on('click', 'button.dt-document', function (e) {
        e.preventDefault();
        var self = $(this);
        var selfTr = self.closest('tr');
        var docId = selfTr.find('input[name="document_id"]').val();

        if (docId != '') {
            $.ajax({
                type: 'POST',
                url: $cjs.admPath('user-laboratory/' + $labId + '/expertise/' + $expertiseId + '/delete-document'),
                data: {doc_id: docId},
                dataType: 'json',
                success: function (result) {

                    if (result.status === 'OK') {
                        selfTr.remove();

                        if(!documentList.find('tbody tr').length){
                            documentList.closest('.table-responsive').addClass('hide');
                        }
                    }
                }
            })
        } else {
            selfTr.remove();

            if(!documentList.find('tbody tr').length){
                documentList.closest('.table-responsive').addClass('hide');
            }
        }
    })
}
