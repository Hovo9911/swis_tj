/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        "order": [[0, "desc"]],
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
        lengthChange: true,
        iDisplayLength: 25
    },

    customOptions: {
        afterInitCallback: function () {
        },
        tableDataPath: 'notifications/table',
        columnsRender: {
            in_app_notification: {
                render: function (row) {
                    if (row.read == 0) {
                        return row.in_app_notification + '<div class="notification__unread-button--out notification__actions"><button class="notification__unread-button btn btn-xs unread-button read_action_' + row.id + '" data-notification_id=' + row.id + '><i  class="fa fa-bookmark"></i> ' + $trans('swis.dashboard.mark_as_read') + '</button></div></div>';
                    }
                    return row.in_app_notification;
                }
            }
        }
    }
};

/*
 * Init Datatable
 */
var $notificationsTable = new DataTable('list-data', optionsTable);

$(document).ready(function () {

    $notificationsTable.init();

    notificationUnreadButtonClick();

    notificationMarkAsReadByURL();

});

function notificationUnreadButtonClick() {
    $(document).on('click', '.notification__unread-button', function () {

        var notification_id = $(this).data('notification_id');

        if (notification_id !== undefined) {
            updateNotification(notification_id);
        }
    });
}

//if clicked saf or sub-application URL mark notification as read
function notificationMarkAsReadByURL() {
    $(document).on('click', '.mark-as-read', function () {

        var notification_id = $(this).closest('td').find('button').data('notification_id');

        if (notification_id !== undefined) {
            updateNotification(notification_id);
        }

        window.open($(this).attr('href'));
    });
}

