/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching:false,
        "order": [[1, "desc"]],
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
        lengthChange: true,
        pageLength: 100
    },
    customOptions: {
        afterInitCallback: function () {},
        tableDataPath:'constructor-templates/table',
        searchPath:'constructor-templates',
        columnsRender: {
            logo: {
                render: function (row) {
                    if (row.logo) {
                        return '<img width="50px" style="max-height: 30px;" src="/images/user/' + row.logo + '" />';
                    }
                    return ''
                }
            }
        }
    }
};

/*
 * Options for Form Request
 */
var optionsForm = {
    searchPath: '/constructor-templates',
    addPath: '/constructor-templates/create',
};

/*
 * Init Datatable, Form
 */
var $constructorTemplatesTable = new DataTable('list-data', optionsTable);
var $constructorTemplates = new FormRequest('form-data', optionsForm);

$(document).ready(function () {

    // init
    $constructorTemplatesTable.init();
    $constructorTemplates.init();

    // ----------

    qrCodeChange();
});

function qrCodeChange(){
    $('input[name="qr_code"]').change(function () {

        var authorizationWithoutLogin = $('#authorizationWithoutLogin');

        if(this.checked){
            authorizationWithoutLogin.closest('.form-group').removeClass('hide');
        }else{
            authorizationWithoutLogin.closest('.form-group').addClass('hide');
        }
    })
}