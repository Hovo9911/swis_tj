/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        "order": [[1, "desc"]],
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
        lengthChange: true,
        iDisplayLength: 25
    },
    customOptions: {
        afterInitCallback: function () {
        },
        tableDataPath: '/transport-application/table',
        searchPath: '/transport-application',
        columnsRender: {
            current_status: {
                render: function (row) {
                    if (row.current_status) {
                        return $trans('swis.transport_application.current_status.' + row.current_status + '.status');
                    }
                }
            }
        },
    }
};

/*
 * Options for Form Request
 */
var optionsForm = {
    searchPath: '/transport-application',
    addPath: '/transport-application/create',
};

/*
 * Init Datatable, Form
 */
var $transportApplicationTable = new DataTable('list-data', optionsTable);
var $transportApplication = new FormRequest('form-data', optionsForm);
var $rejectReason = new FormRequest('reject-reason', []);
var $paymentAmount = new FormRequest('payment-amount', []);

//
var mainForm = $('#form-data');
var transportPermitTypesSelect = $('#transportPermitTypes');
var transportPermitTypesInput = $('#transportPermitType');
var transportPermitFieldsDiv = $('.permit-type-fields');
var transportApplicationStatusInput = $('#transportApplicationStatus');
var transportApplicationCurrentStatus = $('#transportApplicationCurrentStatus').val();
var transportPermitValidityFromDate = $('#permitValidityPeriodFrom');
var transportPermitValidityToDate = $('#permitValidityPeriodTo');
var transportPermitValidityDiffInput = $('#permitValidityPeriodDiff');
var rejectReasonModal = $('#rejectReasonModal');
var invoiceModal = $('#invoiceModal');
var printInvoiceBtn = $('#printInvoice');

$(document).ready(function () {

    // init
    $transportApplicationTable.init();
    $transportApplication.init();
    $rejectReason.init();
    $paymentAmount.init();

    // ---------

    transportPermitTypes();

    autocomplete();

    periodValidityDaysDiff();

    periodValidityDayFrom();

    disableContent();

    saveData();

});

function transportPermitTypes() {

    if (!mainForm.data('view-mode')) {
        transportPermitTypesFields(transportPermitTypesSelect.find(':selected').data('type'))
    } else {
        transportPermitTypesFields(transportPermitTypesInput.data('type'))
    }

    //
    transportPermitTypesSelect.not(':disabled').change(function () {

        transportPermitFieldsDiv.addClass('hide');

        transportPermitTypesFields($(this).find(':selected').data('type'));
        animateScrollToElement(transportPermitTypesSelect, 50, 150);
    });
}

function transportPermitTypesFields(permitType) {
    $('.permit-type-fields[data-permit-type*="' + permitType + '"]').removeClass('hide');
}

function autocomplete() {

    var autoComplete = $('input.autocomplete');

    if ($(autoComplete).length > 0) {

        autoComplete.each(function (k, v) {
            var self = $(this);
            var refId = self.data('source');
            var autocompleteSelectedId = self.closest('div').find('input[name=' + self.data('name') + '][type="hidden"]');

            self.keyup(function () {
                if (self.val() === '') {
                    autocompleteSelectedId.val('');
                }
            });

            self.typeahead({
                minLength: 2,
                source: function (query, process) {
                    return $.post($cjs.admPath("/reference-table-form/" + refId + "/autocomplete"), {
                        query: query,
                        r: self.data('source'),
                    }, function (response) {
                        return process(response.items);
                    });
                },

                afterSelect: function (data) {
                    autocompleteSelectedId.val(data.id);
                },
            });
        })
    }
}

function periodValidityDayFrom() {
    transportPermitValidityFromDate.change(function () {
        // transportPermitValidityToDate.datepicker("option", "minDate", new Date())
    })

}

function periodValidityDaysDiff() {
    $('.permit-date').change(function () {
        if (transportPermitValidityFromDate.val() && transportPermitValidityToDate.val()) {

            var toDate = moment(transportPermitValidityToDate.val(), 'DD/MM/YYYY')
            var fromDate = moment(transportPermitValidityFromDate.val(), 'DD/MM/YYYY')
            var diffDays = toDate.diff(fromDate, 'days') + 1;

            transportPermitValidityDiffInput.val('');
            if (diffDays > 0) {
                transportPermitValidityDiffInput.val(diffDays);
            }
        }
    })
}

function disableContent() {
    if (mainForm.data('view-mode')) {
        disableFormInputs(mainForm.find('.panel-body'), false, true, 0, '#files');
    }
}

function saveData() {
    $(document).on('click', '.save-button', function () {
        var buttonStatus = $(this).data('status');

        switch (buttonStatus) {
            case rejectedStatus:
                rejectReasonModal.modal()
                break;

            case waitingPaymentStatus:
                invoiceModal.modal()
                break;

            default:
                transportApplicationStatusInput.val(buttonStatus);
                mainForm.submit();
        }
    });
}
