/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
        "order": [[1, "desc"]],
        lengthChange: true,
        iDisplayLength: 25
    },

    customOptions: {
        afterInitCallback: function () {},
        tableDataPath: 'regional-office/table',
        searchPath: 'regional-office',
        columnsRender: {
            address: {
                render: function (row) {
                    if (row.address) {
                        return '<span title="' + row.address + '">' + row.address + '</span>'
                    }
                    return ''
                }
            },
        },
    }
};

/*
 * Options for Form Request
 */
var optionsForm = {
    searchPath: '/regional-office',
    addPath: '/regional-office/create',
};

/*
 * Init Datatable, Form
 */
var $regionalOfficeTable = new DataTable('list-data', optionsTable);
var $regionalOffice = new FormRequest('form-data', optionsForm);

//
var agencySelect = $('#agency');
var laboratoriesList = $('.laboratoriesList');
var regionalOfficeId = $('#regionalOfficeId').val();
var constructorDocumentSelect = $('#constructorDocuments');

$(document).ready(function () {

    // init
    $regionalOfficeTable.init();
    $regionalOffice.init();

    // -----------------

    getAgencyConstructorDocuments();

    disableFieldsIfRegionalOfficeInactive();

});

function getAgencyConstructorDocuments() {

    agencySelect.change(function () {

        var self = $(this);
        var selectVal = self.val();

        laboratoriesList.html('');
        select2Reset(constructorDocumentSelect)

        if (selectVal) {

            self.prop('disabled', true);
            constructorDocumentSelect.prop('disabled', true);

            $.ajax({
                    type: 'POST',
                    url: $cjs.admPath('regional-office/get-agency-documents'),
                    data: {company_tax_id: selectVal, regional_office_id: regionalOfficeId},
                    dataType: 'json',
                    success: function (result) {

                        if (result.status === 'OK') {
                            if (typeof result.data.items != 'undefined' && result.data.items.length) {
                                $.each(result.data.items, function (k, v) {

                                    constructorDocumentSelect.append($("<option></option>").attr("value", v.code).text(v.name));
                                })
                            }

                            laboratoriesList.html(result.data.laboratories);
                            datePickerInit();
                            self.prop('disabled', false);
                            constructorDocumentSelect.prop('disabled', false);
                        }
                    }
                }
            );
        }
    });
}

function disableFieldsIfRegionalOfficeInactive() {

    if (typeof regionalOfficeStatus != 'undefined' && regionalOfficeStatus === '2') {
        disableFormInputs($('.tab-content'), true,true);
    }
}
