var serviceInfoTextArea = $('#serviceInfo');

/*
 * Options for Form Request
 */
var optionsForm = {
    searchPath: '/user',
    customSaveFunction: function () {
        serviceInfoTextArea.val('');
        $error.show('form-error', []);
        return true;
    },
    customOptions: {
        onSaveSuccess: function (data) {
            serviceInfoTextArea.val(data.info);
        },
    }
};

// Init Form
var $serviceInfo = new FormRequest('form-data', optionsForm);

$(document).ready(function () {

    $serviceInfo.init();

});