/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        "order": [[1, "desc"]],
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
        lengthChange: true,
        iDisplayLength: 25
    },

    customOptions: {
        afterInitCallback: function () {
        },
        tableDataPath: 'user/table',
        searchPath: 'user',
        actions: {
            edit: {
                render: function (row) {
                    var editUrl = 'user';
                    if (row.is_customs_operator) {
                        editUrl = 'user_customs_operator';
                    }

                    return '<a href="' + $cjs.admPath(editUrl + '/' + row.id + '/edit') + '" class="btn btn-edit" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.edit.button') + '"><i class="fa fa-pen"></i></a>'
                }
            },
            view: {
                render: function (row) {
                    var editUrl = 'user';
                    if (row.is_customs_operator) {
                        editUrl = 'user_customs_operator';
                    }

                    return '<a href="' + $cjs.admPath(editUrl + '/' + row.id + '/view') + '" class="btn btn-view" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.view.button') + '"><i class="fa fa-eye"></i></a>'
                }
            }
        }
    }
};

//
var $userInfoServiceMessage = $("#userInfoServiceMessage");

/*
 * Options for Form Request
 */
var optionsForm = {
    searchPath: '/user',
    addPath: '/user/create',
};

/*
 * Options for Form Request Customs operator
 */
var optionsFormCustomsOperator = {
    searchPath: '/user',
    addPath: '/user/create',
};

// Init Datatable, Form
var $userTable = new DataTable('list-data', optionsTable);
var $user = new FormRequest('form-data', optionsForm);
var $userCustomsOperator = new FormRequest('form-data-customs-operator', optionsFormCustomsOperator);

//
var getUserInfoBySSN = true;
var currentUserId = $('.mc-form-key').val();
var ssnInput = $('#userSSN');
var updateUserInfoFromServiceBtn = $('#updateUserInfoFromService');
var firstNameInput = $('#firstName');
var lastNameInput = $('#lastName');
var passportInput = $('#passport');
var passportDetailsDiv = $('#passportFields')
var addManuallyCheckbox = $('#addedManually');

$(document).ready(function () {

    // init
    $userTable.init();
    $user.init();
    $userCustomsOperator.init();

    // --------

    addManuallyUser();

    // Company Part
    getCompanyByTaxId();

    companyHeads();

    addMultipleCompanies();

    // Email part
    sendForConfirmation();

    emailSendAgain();

    // User part
    getUserBySSN();

    generateUsername();

    updateUserInfoFromService();

    // Search
    indexPageGetModuleRoles();

});

function addManuallyUser() {

    userInfoFieldsOpen(addManuallyCheckbox.is(':checked'));

    //
    addManuallyCheckbox.click(function () {
        userInfoFieldsOpen($(this).is(':checked'), true);
    })
}

function userInfoFieldsOpen(isChecked, resetVal = false) {
    firstNameInput.attr('readonly', !isChecked);
    lastNameInput.attr('readonly', !isChecked);
    passportInput.attr('readonly', !isChecked);
    passportDetailsDiv.find('input').attr('readonly', !isChecked);
    updateUserInfoFromServiceBtn.prop('disabled', isChecked);

    if (resetVal && $saveMode == 'add') {
        firstNameInput.val('');
        lastNameInput.val('');
        passportInput.val('');
        passportDetailsDiv.find('input').val('');

        passportDetailsDiv.addClass('hide');
        if (isChecked) {
            passportDetailsDiv.removeClass('hide');
        }
    }

    getUserInfoBySSN = !isChecked;
}

function sendForConfirmation() {

    $('#sendConfirmation').change(function () {

        var emailField = $('#emailForm');
        var userNamePassDiv = $('.username-pass');
        var disabledField = true;

        if ($(this).is(':checked')) {
            emailField.addClass('required');
            emailField.closest('.form-group').removeClass('hide');
        } else {

            disabledField = false;

            emailField.closest('.form-group').addClass('hide');
            emailField.removeClass('required');

            if (!emailField.data('edit')) {
                emailField.find('input').val('');
            }
        }

        userNamePassDiv.find('input').prop('disabled', disabledField);
        userNamePassDiv.toggleClass('hide');
    })
}

function addMultipleCompanies() {

    var controlForm = $('.multiple');
    var i = controlForm.find('.form-group').length + 1;

    $(document).on('click', '.btn-add', function (e) {
        e.preventDefault();
        var name = $(this).closest(controlForm).data('name');

        var currentEntry = $(this).parents('.form-group:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input[type="text"]').attr('name', name + '[' + i + '][tax_id]').val('');
        newEntry.find('input[type="checkbox"]').attr('name', name + '[' + i + '][is_head]').prop('checked', false);
        newEntry.find('.form-error').attr('id', 'form-error-' + name + '-' + i).removeClass('form-error-text').text('');
        newEntry.removeClass('has-error');
        newEntry.find('.company-name').text('');

        controlForm.find('.form-group:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="glyphicon glyphicon-minus"></span>');

        i++;

    }).on('click', '.multiple .btn-remove', function (e) {

        $(this).parents('.form-group:first').remove();

        e.preventDefault();
        return false;
    });

}

function emailSendAgain() {
    $('#emailSendAgain').click(function () {
        var self = $(this);
        var lngId = $('#userLangId').val();

        self.prop('disabled', true);
        $.ajax({
            type: 'POST',
            url: $cjs.admPath('user/email-send-again'),
            data: {user_id: currentUserId, 'lng_id': lngId},
            dataType: 'json',
            success: function (result) {

                if (result.status === 'OK') {

                    if (result.data) {
                        var bactToUrl = result.data.backToUrl;

                        window.location.href = bactToUrl;
                    }

                }
            }
        });
    })
}

function getUserBySSN() {
    ssnInput.not('[readonly]').keyup(function () {

        if (getUserInfoBySSN && $(this).val().length >= $ssnMinLength && !+currentUserId) {

            getUserInfoBySSN = false;

            var self = $(this),
                formGroup = self.closest('.form-group'),
                address = $('#address'),
                email = $('#email'),
                phoneNumber = $('#phoneNumber');

            spinnerLoadState(self);

            formGroup.removeClass('has-error');
            formGroup.find('.form-error').removeClass('form-error-text').text('');

            firstNameInput.val('');
            lastNameInput.val('');
            passportInput.val('');
            address.val('');
            email.val('');
            phoneNumber.val('');

            $.ajax({
                type: 'POST',
                url: $cjs.admPath('user/get-user-info'),
                data: {ssn: $(this).val(), onlySSN: true},
                dataType: 'json',
                success: function (result) {

                    if (result.status === 'OK') {

                        if (result.data && result.data) {

                            firstNameInput.val(result.data.first_name);
                            lastNameInput.val(result.data.last_name);
                            passportInput.val(result.data.passport);
                            address.val(result.data.address);
                            email.val(result.data.email);
                            phoneNumber.val(result.data.phone_number);

                        }
                    } else if (result.status === 'INVALID_DATA') {
                        formGroup.find('.form-error').addClass('form-error-text');
                        formGroup.addClass('has-error');
                        formGroup.find('.form-error').text(result.errors.message);

                        if (typeof result.errors.taxService != 'undefined') {
                            addManuallyCheckbox.prop('disabled', false);
                        }
                    }

                    getUserInfoBySSN = true;
                    spinnerLoadState(self, true);
                },
                error: function () {
                    spinnerLoadState(self, true);
                    formGroup.addClass('has-error');
                    formGroup.find('.form-error').addClass('form-error-text').text($trans('core.loading.invalid_data'));

                    getUserInfoBySSN = true;
                }
            });
        }
    })
}

function getCompanyByTaxId() {
    var ajaxSend = true;
    $(document).on('keyup', '.tax-id input', function () {

        var self = $(this);
        var selfFormGroup = self.closest('.form-group');
        var companyName = selfFormGroup.find('.company-name');
        var companyHeadCheckbox = selfFormGroup.find('.company-head');

        if (self.is('[readonly]')) {
            return false;
        }

        companyName.text('');
        companyHeadCheckbox.removeAttr('data-company-tax-id').prop('disabled', true).prop('checked', false);

        if (ajaxSend && self.val().length >= parseInt($taxIdMinLength) && self.val().length <= parseInt($taxIdMaxLength)) {
            ajaxSend = false;
            var currentCompanyTaxId = self.val();

            var taxIdInputName = $(this).attr('name'),
                formGroup = $('input[name="' + taxIdInputName + '"]').closest('.form-group'),
                saveBtn = $('.mc-nav-button-save');

            formGroup.removeClass('has-error');
            formGroup.find('.form-error').removeClass('form-error-text').text('');

            if ($('.tax-id .form-group.has-error').length === 0) {
                saveBtn.prop('disabled', false);
            }

            spinnerLoadState(self);

            $.ajax({
                type: 'POST',
                url: $cjs.admPath('user/get-company-info'),
                data: {tax_id: currentCompanyTaxId, name: taxIdInputName},
                dataType: 'json',
                success: function (result) {

                    if (result.status === 'OK') {

                        if (result.data) {
                            companyName.text(result.data.company_name);
                            companyHeadCheckbox.attr('data-company-tax-id', currentCompanyTaxId).prop('disabled', false)
                        }

                    } else if (result.status === 'INVALID_DATA') {
                        if (typeof result.errors != "undefined" && typeof result.errors.message != 'undefined') {
                            var message = result.errors.message;

                            formGroup.find('.form-error').addClass('form-error-text');
                            formGroup.addClass('has-error');

                            formGroup.find('.form-error').text(message);
                        }
                    }

                    ajaxSend = true;
                    spinnerLoadState(self, true);
                },
                error: function () {
                    ajaxSend = true;
                    spinnerLoadState(self, true);

                    formGroup.addClass('has-error');
                    formGroup.find('.form-error').addClass('form-error-text').text($trans('core.loading.invalid_data'));
                }
            });
        }
    })
}

function generateUsername() {

    var userNameField = $('#checkUsername');
    var suggestionList = $('.hidden-suggestion-list');
    var suggestionListBlock = $('.hidden-suggestion-list-block');

    var currentUserName = userNameField.val();

    userNameField.keyup(delay(function (e) {
        var self = $(this);
        var username = self.val();

        if (username !== '') {
            suggestionListBlock.hide();
            suggestionList.html('');

            if (username != currentUserName) {

                self.prop('disabled', true);

                $.ajax({
                    type: 'POST',
                    url: $cjs.admPath('user/check/username'),
                    data: {username: username},
                    dataType: 'json',
                    success: function (result) {

                        if (result.status === 'OK') {
                            if (result.data.suggestionList.length) {
                                suggestionListBlock.show();

                                $.each(result.data.suggestionList, function (index, value) {
                                    suggestionList.append('<a class="list-group-item list-group-item-action suggestion-item">' + value + '</a>');
                                });
                            }
                        }

                        self.prop('disabled', false);

                        if (!$(':input').not('#checkUsername').is(':focus')) {
                            self.focus();
                        }
                    }
                });
            }
        }

    }, 1200));

    $(document).on('click', '.suggestion-item', function () {
        userNameField.val($(this).text());
        suggestionList.html('');
        suggestionListBlock.hide();
    });
}

function companyHeads() {

    var companyAdminModal = $('#companyAdminModal');
    var companyAdminName = companyAdminModal.find('.admin-name');
    var cancelChangeAdminBtn = $('#cancelChangeAdmin');

    $(document).on('change', '.company-head', function () {
        var self = $(this);
        var companyTaxId = self.attr('data-company-tax-id');

        if (self.is(':checked')) {
            self.prop('disabled', true);

            $.ajax({
                type: 'POST',
                url: $cjs.admPath('user/get-company-head'),
                data: {companyTaxId: companyTaxId},
                dataType: 'json',
                success: function (result) {
                    if (result.status === 'OK') {

                        self.prop('disabled', false);

                        if (result.data.companyHasAdmin) {
                            companyAdminModal.modal({backdrop: 'static', keyboard: false});
                            companyAdminName.text(result.data.companyAdminName);
                            cancelChangeAdminBtn.attr('data-company-tax-id', companyTaxId);
                        }

                    } else if (result.status === 'INVALID_DATA') {
                        self.prop('disabled', false);
                    }
                }
            });
        }
    });

    companyAdminModal.on('hidden.bs.modal', function () {
        companyAdminName.text('')
    });

    cancelChangeAdminBtn.click(function () {
        $('input.company-head[data-company-tax-id="' + $(this).data('company-tax-id') + '"]').prop('checked', false);
        $(this).removeAttr('data-company-tax-id');
        companyAdminModal.modal('toggle');
    })
}

function indexPageGetModuleRoles() {

    var roleListSelect = $('#roleList');

    $('#menu').change(function () {

        var self = $(this);

        select2Reset(roleListSelect);
        roleListSelect.prop('disabled', true);

        if (self.val()) {
            $.ajax({
                type: 'POST',
                url: $cjs.admPath('menu/get-module-roles'),
                data: {menu_id: self.val()},
                dataType: 'json',
                success: function (result) {

                    if (result.status === 'OK') {
                        if (result.data.roles.length) {

                            var listOptions = [];
                            $.each(result.data.roles, function (key, role) {
                                listOptions.push(new Option(role.name, role.id, false, false));
                            });

                            roleListSelect.append(listOptions);
                        }

                        roleListSelect.prop('disabled', false);
                    }
                }
            });
        }
    })
}

function updateUserInfoFromService() {

    updateUserInfoFromServiceBtn.click(function () {

        var self = $(this);

        $userInfoServiceMessage.html('');
        spinnerLoaderForButton(self);

        $error.show('form-error', []);

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('user/update-user-info-from-service'),
            data: {user_id: currentUserId},
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    var alertType = 'success';

                    if (typeof result.data.changedInfo != "undefined") {
                        alertType = 'danger';

                        $.each(result.data.changedInfo, function (k, v) {
                            var currentInput = $('#' + k);
                            currentInput.val(v).closest('.form-group').addClass('has-error');

                            if(k === 'passport_issue_date'){
                                datePickerInit(currentInput.closest('.form-group'))
                            }
                        })

                    }

                    var alertMessage = '<div class="alert alert-' + alertType + '">' + result.data.message + '</div>';

                    $userInfoServiceMessage.html(alertMessage);
                    spinnerLoaderForButton(self, true);


                } else if (result.status === 'INVALID_DATA') {

                    $userInfoServiceMessage.html('<div class="alert alert-danger">' + result.errors.message + '</div>');

                    animateScrollToElement($userInfoServiceMessage, 50, 1);
                    spinnerLoaderForButton(self, true);
                }
            }
        });
    });
}

function delay(callback, ms) {
    var timer = 0;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            callback.apply(context, args);
        }, ms || 0);
    };
}


