/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        "order": [],
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
        lengthChange: true,
        iDisplayLength: 25
    },

    customOptions: {
        afterInitCallback: function () {
        },
        tableDataPath: 'customs-portal/table',
        searchPath: 'customs-portal',
        actions: {
            custom: {
                render: function (row) {
                    if (row.pdf_hash_key) {
                        return '<a target="_blank" href="' + row.pdf_hash_key + '" class="btn btn-success btn-xs btn--icon" data-toggle="tooltip"  title="' + $trans('swis.custom_portal.search.pdf_link.button') + '"><i class="fa fa-file-pdf-o"></i></a>';
                    }
                    return ''
                }
            },
            view: {
                render: function (row) {
                    if (row.saf_info_link) {
                        return '<a target="_blank" href="' + row.saf_info_link + '" class="btn btn-success btn-xs btn--icon" data-toggle="tooltip"  title="' + $trans('swis.custom_portal.search.saf_link.button') + '"><i class="fas fa-eye"></i></a>';
                    }
                    return ''
                }
            }
        },
        columnsRender: {
            regime: {
                render: function (row) {
                    if (row.regime) {
                        return $trans('swis.' + row.regime + '_title');
                    }
                    return ''
                }
            },
        }
    }
};

/*
 * Init Datatable
 */
var $customsPortalTable = new DataTable('list-data', optionsTable);

//
var agencySelect = $('#agency');
var agencySubDivisions = $('#subdivision');
var agencyConstructorDocumentSelect = $('#constructorDocuments');

$(document).ready(function () {

    // init
    $customsPortalTable.init();

    // -------

    getAgencyConstructorDocumentsAndSubdivisions();

});

function getAgencyConstructorDocumentsAndSubdivisions() {

    agencySelect.change(function () {

        var self = $(this);
        var selectVal = self.val();

        select2Reset(agencyConstructorDocumentSelect, true);
        select2Reset(agencySubDivisions, true);

        if (selectVal) {

            self.prop('disabled', true);
            agencyConstructorDocumentSelect.prop('disabled', true);
            agencySubDivisions.prop('disabled', true);

            $.ajax({
                type: 'POST',
                url: $cjs.admPath('customs-portal/get-agency-documents-subdivisions'),
                data: {agency_id: self.val()},
                dataType: 'json',
                success: function (result) {

                    if (result.status === 'OK') {
                        if (typeof result.data.constructorDocs != 'undefined' && result.data.constructorDocs.length) {
                            $.each(result.data.constructorDocs, function (k, v) {

                                agencyConstructorDocumentSelect
                                    .append($("<option></option>")
                                        .attr("value", v.code)
                                        .text(v.name));
                            })
                        }

                        if (typeof result.data.subdivisions != 'undefined' && result.data.subdivisions.length) {
                            $.each(result.data.subdivisions, function (k, v) {

                                agencySubDivisions
                                    .append($("<option></option>")
                                        .attr("value", v.id)
                                        .text(v.name));
                            })
                        }

                        self.prop('disabled', false);
                        agencyConstructorDocumentSelect.prop('disabled', false);
                        agencySubDivisions.prop('disabled', false);
                    }
                }
            });
        }
    });
}
