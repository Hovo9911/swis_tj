$(function () {

    userRoleSelect();

    noRoleModal();

    needUpdateUserRolesSession();

    resetButton();

    generateXls();

    closeResultDiv();

    select2Init();

    datePickerInit();

    renderStatusButtons();

    closeRefreshingPage();

    datatableAddScroll();

    viewModeContent();

    searchInMenu();

    inputGroupAddonsAddReadonly();

    $('body').tooltip({selector: '[data-toggle="tooltip"]', trigger: 'hover'});

    if (typeof customHideSearchParams == 'undefined') {
        hideSearchParams();
    }

});

function datatableAddScroll() {
    var dataTables = $('.dataTables-example.dataTable');

    if (dataTables.length) {
        dataTables.closest('div').addClass('scroll-with-fix-box');
    }
}

function renderStatusButtons() {
    $.each($('.form-show-status'), function (i, elem) {

        elem = $(elem);

        var elementName = elem.attr('name'),
            radiGroup = '<div class="btn-group" data-toggle="buttons">',
            checked, buttonClass;

        elementName = elementName ? 'name="' + elementName + '"' : '';

        $.each(elem.find('option'), function (j, option) {

            option = $(option);
            buttonClass = '';
            checked = '';
            if (option.attr('selected')) {
                buttonClass = ' active';
                checked = ' checked="" ';
            }
            idDDD++;
            radiGroup += '<label class="btn btn-default' + buttonClass + '">';
            radiGroup += '<input type="radio" ' + elementName + ' ' + checked + ' value="' + option.val() + '" id="ddddd-' + idDDD + '" />';
            radiGroup += option.text();
            radiGroup += '</label>';

        });
        radiGroup += '</div>';

        $(this).replaceWith(radiGroup);
    });

}

function resetButton() {
    $('.resetButton').click(function () {
        var form = $(this).closest('form');
        var select2 = $('.select2');
        var select2Ajax = $('.select2-ajax');

        var documentForm = $('#documentSearchFilter :input:not(".no-reset")');


        if (documentForm.length > 0) {
            documentForm.val('').trigger('change');
        } else {

            form[0].reset();

            if (form.find(select2).length > 0) {
                form.find(select2).val('').trigger('change');
            }

            if (form.find(select2Ajax).length > 0) {
                form.find(select2Ajax).val('').trigger('change');
            }

            if (form.find('.alt-field').length > 0) {
                form.find('.alt-field').val('');
            }

        }

        if (typeof $(this).data('submit') != "undefined" && $(this).data('submit')) {
            form.submit();
        }

        if (typeof $(this).data('search-button') != "undefined" && $(this).data('search-button')) {
            var searchBtnClass = $(this).data('search-button');

            if ('.' + searchBtnClass.length) {
                $('.' + searchBtnClass).click();
            }
        }

    })
}

function generateXls() {
    $('.generate-xls').on('click', function (e) {
        e.preventDefault();

        var data = $(this).closest('form').serializeArray().filter(function(item) { return item.value != ""; });
        var url = $(this).attr('href')+"?";

        $.each(data, function (index, item) {
            url += item.name+"="+item.value+"&";
        });

        url = url.substring(0, url.length - 1);
        window.location.href = url;
    });
}

function closeResultDiv() {
    $('.closeResultTab').click(function () {
        var type = $(this).data('type');
        var divName = $(this).data('div');

        var typeVal = '.';
        if (type === 'id') {
            typeVal = '#';
        }

        if ($(typeVal + divName.length > 0)) {
            $(typeVal + divName).addClass('hide')
        }
    })
}

function datePickerInit(divSelector) {

    var dateInputs = $('.date');
    var dateTimeInputs = $('.datetime');

    if (typeof divSelector != 'undefined') {
        dateInputs = divSelector.find('.date')
    }

    // Date
    if (dateInputs.length) {

        if (typeof $cLng == 'undefined') {
            $cLng = 'en';
        }

        dateInputs.each(function () {
            var self = $(this);
            var defaultVal = self.find('input').val();
            var defaultStartDate = self.find('input').data('start-date');

            var startDate;
            if (defaultStartDate && !self.find('input').prop('disabled')) {
                if (defaultVal) {
                    var inputStartDate = moment(defaultStartDate);
                    var inputDate;

                    if (defaultVal.indexOf('/') !== -1) {
                        inputDate = moment(defaultVal, 'DD/MM/YYYY')
                    } else {
                        inputDate = moment(defaultVal)
                    }

                    if (inputDate.diff(inputStartDate, 'days') >= 0) {
                        startDate = new Date(defaultStartDate);
                    }
                } else {
                    startDate = new Date(defaultStartDate);
                }
            }

            var endDate;
            if (self.find('input').data('end-date') && !self.find('input').prop('disabled')) {
                endDate = new Date(self.find('input').data('end-date'));
                endDate.setDate(endDate.getDate() + 1)
            }

            // format: 'dd/mm/yyyy',
            $(this).datepicker({
                language: $cLng,
                format: {

                    toDisplay: function (date, format, language) {

                        initCustomDate(self, date);

                        return moment(date).format('DD/MM/YYYY');
                    },
                    toValue: function (date, format, language) {

                        initCustomDate(self, date, moment.ISO_8601);

                        return moment(date, 'DD/MM/YYYY').format("YYYY-MM-DD");
                    }

                },
                todayHighlight: true,
                clearBtn: true,
                // todayBtn: true,
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                // calendarWeeks: true,
                startDate: startDate,
                endDate: endDate,
                autoclose: true,

            });

            $(this).find('input[type="text"]').inputmask("dd/mm/yyyy",
                {
                    "placeholder": $datePlaceHolder,
                    /* onincomplete: function(){
                         $(this).val(defaultVal);
                     }*/
                }
            );

            // Set Default value
            setValue(self, defaultVal);

            //
            $(this).datepicker().on('hide', function (e) {
                setValue($(this), $(this).find('input').val());
                $(this).find('input').blur();
            });

            $(this).datepicker().on('show', function (e) {
                if ($(this).find('input[type="text"]').is('[readonly]') || $(this).find('input[type="text"]').is(':disabled')) {
                    $(this).datepicker("remove");
                }
            });

            // if disabled/readonly close input group addon
            if ($(this).find('input').is('[readonly]') || $(this).find('input').is('[disabled]')) {
                $(this).find('.input-group-addon').addClass('readonly');
            }
        });

        function initCustomDate(self, date) {

            var inputName = self.attr('name');
            var inputElem = $(self).find('input');
            var altField = $(self).find('.alt-field');

            var inputValue = moment(date, moment.ISO_8601).format('YYYY-MM-DD');
            if (date.toString().indexOf('/') !== -1) {
                inputValue = moment(date, 'DD/MM/YYYY').format('YYYY-MM-DD')
            }

            if (moment(inputValue).isValid()) {
                if (!altField.length) {
                    var html = inputElem.prop('outerHTML');
                    var hiddenInputElem = $(html);

                    hiddenInputElem.attr({type: "hidden", name: inputName}).addClass('alt-field').val(inputValue);
                    inputElem.after(hiddenInputElem).removeAttr('name').removeAttr('id');
                } else {
                    altField.val(inputValue);
                }
            } else {
                altField.val('');
            }
        }

        function setValue(self, defaultVal) {

            if (defaultVal === '') {
                $(self).find('.alt-field').val('');
            } else {

                var inputNewtValue = moment(defaultVal, moment.ISO_8601).format('YYYY-MM-DD');

                if (defaultVal.toString().indexOf('/') !== -1) {
                    inputNewtValue = moment(defaultVal, 'DD/MM/YYYY').format('YYYY-MM-DD');
                }

                if (Date.parse(inputNewtValue)) {
                    self.datepicker("setDate", new Date(inputNewtValue));
                }
            }
        }

    }

    // DateTime
    if (dateTimeInputs.length) {

        /*dateTimeInputs.each(function(){
            var self = $(this);

            $(this).datetimepicker({
                format: {

                    toDisplay: function (date, format, language) {

                        initCustomDate(self,date);

                        return moment(date).format('DD/MM/YYYY HH:mm:ss').toString();

                    },
                    toValue: function (date, format, language) {

                        initCustomDate(self,date);
                        console.log(moment(date, 'DD/MM/YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss"));
                        return moment(date,'DD/MM/YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss").toString();
                    }

                },
            })
        });*/

        // $.fn.datetimepicker.dates['sv'] = {
        //     days: ["Söndag", "Måndag", "Tisdag", "Onsdag", "Torsdag", "Fredag", "Lördag"],
        //     daysShort: ["Sön", "Mån", "Tis", "Ons", "Tor", "Fre", "Lör"],
        //     daysMin: ["Sö", "Må", "Ti", "On", "To", "Fr", "Lö"],
        //     months: ["Januari", "Februari", "Mars", "April", "Maj", "Juni", "Juli", "Augusti", "September", "Oktober", "November", "December"],
        //     monthsShort: ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec"],
        //     meridiem: '',
        //     today: "Idag",
        //     clear: "Rensa"
        // };

        var $dateTimeLng = $cLng;

        if ($dateTimeLng == 'tj') {
            $dateTimeLng = 'tg';
        }

        dateTimeInputs.datetimepicker({
            locale: $dateTimeLng,
            format: 'DD/MM/YYYY HH:mm',
            showClear: true,
        });

        dateTimeInputs.find('input[type="text"]').inputmask("datetime", {
            mask: "1/2/y h:s",
            placeholder: $dateTimePlaceHolder,
            leapday: "/02/29",
            separator: "/",
            alias: "dd/mm/yyyy"
        });

        dateTimeInputs.each(function () {
            // if disabled/readonly close input group addon
            if ($(this).find('input').is('[readonly]') || $(this).find('input').is('[disabled]')) {
                $(this).find('.input-group-addon').addClass('readonly');
            }
        });
    }
}

// From Reference get data
function refAutocomplete() {

    var autoComplete = $('input.autocomplete');

    if ($(autoComplete).length > 0) {

        autoComplete.each(function (k, v) {

                var self = $(this);
                var timeout;

                self.typeahead({

                    source: function (query, process) {
                        if (timeout) {
                            clearTimeout(timeout);
                        }

                        timeout = setTimeout(function () {
                            return $.post($cjs.admPath("/reference-table/ref"), {
                                q: query,
                                ref: self.data('ref'),
                                refType: self.data('ref-type')
                            }, function (response) {
                                return process(response.items);
                            });
                        }, 500);
                    },
                    displayText: function (item) {

                        var tooltip = item.name;
                        if (typeof item.long_name != 'undefined' && item.long_name != '') {
                            tooltip = item.long_name;
                        }
                        return '<span title="' + tooltip + '">' + '(' + item.code + ') ' + item.name + '</span>';
                    },
                    highlighter: function (item) {
                        return item;
                    },
                    afterSelect: function (data) {

                        var fieldsData = self.data('fields');
                        var documentTypeMode = self.data('mode');

                        if (typeof fieldsData != 'undefined' && fieldsData.length !== 0) {
                            $.each(fieldsData, function (k, v) {

                                var currentInput = self.closest('tr').find('input[name="' + v.to + '"]');
                                currentInput.val(data[v.from]);

                                if (currentInput.is('[readonly]')) {
                                    var tooltip = data[v.from];
                                    if (typeof data.long_name != 'undefined' && data.long_name != '') {
                                        tooltip = data.long_name;
                                    }
                                    currentInput.attr('title', tooltip);
                                }

                                self.closest('tr').find('textarea[name="' + v.to + '"]').val(data[v.from]);
                            });

                        }
                        if (typeof documentTypeMode != 'undefined') {
                            $('#searchDocumentType').val(data.code);
                        }
                    },

                });

            }
        );

    }

}

function userRoleSelect() {

    var userRolesSelect = $('#userRoles');
    var currentRole = $('#currentRole');
    var currentRoleType = $('#currentRoleType');

    if (userRolesSelect.find('option').length === 1) {
        var closestForm = userRolesSelect.closest('form');

        currentRole.val(userRolesSelect.find('option').attr('data-role'));
        currentRoleType.val(userRolesSelect.find('option').attr('data-role-type'));

        if (closestForm.find('button[type="submit"]').length) {
            closestForm.submit();
        }
    }

    userRolesSelect.change(function () {
        var self = $(this);

        currentRole.val(self.find(':selected').attr('data-role'));
        currentRoleType.val(self.find(':selected').attr('data-role-type'));
    });

    userRolesSelect.change(function () {
        var closestForm = $(this).closest('form');

        if (!closestForm.find('button[type="submit"]').length) {
            $(this).closest('form').submit();
        }
    });

    //Write only number
    var intNumber = /^[0-9]+$/;
    $(".field--number").on('input keyup', function () {
        var self = $(this);
        var numberBox = self.val();

        if (!(numberBox.match(intNumber))) {
            self.val(numberBox.slice(0, -1));
            if (numberBox.length == 1) {
                self.closest('.input-box').removeClass('active');
            }
        }

    });
}

// Notification
var notificationCountDiv = $('.notifications-count');
var unreadInAppNotificationsCount = +notificationCountDiv.data('count') || 0;
var notificationCountText = $('.notifications-count-text');
var markingAsReadAllUnreadNotifications = $('.markAsReadAllUnreadNotifications');

function updateNotification(notification_id) {

    $('.read_action_' + notification_id).prop('disabled', true);

    $.ajax({
        type: 'POST',
        url: $cjs.admPath('/notifications/update/'),
        data: {notification_id: notification_id},

        success: function (response) {

            if (response.status === 'OK') {

                $('.read_action_' + notification_id).closest('.notification__actions').remove();
                notificationCountDiv.text(--unreadInAppNotificationsCount);

                if (unreadInAppNotificationsCount === 0) {
                    notificationCountDiv.addClass('hide');
                    notificationCountText.addClass('hide');
                    markingAsReadAllUnreadNotifications.addClass('hide');
                }
            }
        }
    });
}

// Select2
function select2Init(parentDiv) {

    var select2 = $('.select2');

    if (typeof parentDiv != 'undefined') {
        select2 = parentDiv.find('.select2');
    }

    if (select2.length) {

        function customTemplateSelection(data) {
            var originalOption = data.element;
            if ($(originalOption).data('option-text')) {
                return $(originalOption).data('option-text');
            }

            return data.text;
        }

        $.each(select2, function () {

            var placeholder = $selectLabel;

            if ($(this).data('select2-placeholder')) {
                placeholder = $(this).data('select2-placeholder');
            }

            var allowClear = true;
            if (typeof $(this).data('select2-allow-clear') !== 'undefined') {
                allowClear = $(this).data('select2-allow-clear');
            }

            $(this).select2({
                placeholder: placeholder,
                allowClear: allowClear,
                minimumResultsForSearch: 10,

                templateResult: customTemplateSelection,
                templateSelection: customTemplateSelection
            });
        });


        select2.on('select2:select', function (e) {

            var fieldsData = $(this).data('fields');
            var attr = $(this).find(':selected').attr('data-fields-values');

            if (typeof attr !== typeof undefined && attr !== false) {
                var info = JSON.parse(attr);

                if (fieldsData.length !== 0 && info !== undefined) {

                    $.each(fieldsData, function (k, v) {
                        $('input[name="' + v.to + '"]').val(info[v.from]);
                        $('textarea[name="' + v.to + '"]').val(info[v.from]);
                    });
                }
            }
        });
    }

    // -------

    var select2Ajax = $('.select2-ajax');

    if (typeof parentDiv != 'undefined') {
        select2Ajax = parentDiv.find('.select2-ajax');
    }

    if (select2Ajax.length) {

        $.each(select2Ajax, function () {

            var self = $(this);
            var url = $(this).data('url');
            var ref = $(this).data('ref');
            var idValue = $(this).data('id-value');
            var codeName = $(this).data('code-name');
            var fieldId = $(this).data('field-id');
            var minInputLength = $(this).data('min-input-length');
            var placeholder = $select2AutoCompleteLabel;

            if ($(this).data('select2-placeholder')) {
                placeholder = $(this).data('select2-placeholder');
            }

            ref = $.trim(ref);

            self.select2({
                // multiple: true,
                placeholder: placeholder,
                ajax: {
                    url: url,
                    type: 'post',
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        var returnObject = {
                            q: params.term,   // search term
                            ref: ref,         // search term
                            idValue: idValue,
                            page: params.page,
                            codeName: codeName
                        };

                        if (self.attr('data-params') !== undefined) {
                            var dataParamsParse = JSON.parse(self.attr('data-params'));
                            for (var prop in dataParamsParse) {
                                returnObject[prop] = dataParamsParse[prop]
                            }
                        }
                        return returnObject;
                    },
                    processResults: function (data, page) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data.items
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                },
                minimumInputLength: (minInputLength === undefined) ? 2 : minInputLength,
                language: {
                    inputTooShort: function (args) {
                        return $trans('core.base.select2.enter_text_1') + " " + (args.minimum - args.input.length) + " " + $trans('core.base.select2.enter_text_2');
                    }
                },
                templateResult: formatRepo,
                templateSelection: formatRepoSelection,
                allowClear: true
            });


            function formatRepoSelection(repo) {
                if (repo.showOnlyCode) {
                    return repo.code;
                } else {
                    return repo.name || repo.text;
                }
            }

            function formatRepo(repo) {
                if (repo.disabled) {
                    return repo.text;
                }
                return "<span title='" + repo.long_name + "'>" + repo.name + "</span>";
            }

        });

        select2Ajax.on('select2:select', function (e) {

            var fieldsData = $(this).data('fields');
            var info = e.params.data;

            if (typeof fieldsData !== "undefined" && fieldsData.length !== 0 && info !== undefined) {

                $.each(fieldsData, function (k, v) {
                    $('input[name="' + v.to + '"]').val(info[v.from]);
                    $('textarea[name="' + v.to + '"]').val(info[v.from]);
                });
            }

        });
    }
}

function select2Reset(select, allowClear, triggerChange) {

    if (allowClear === undefined) {
        allowClear = false;
    }

    if (triggerChange === undefined) {
        triggerChange = true;
    }

    select.find('option').remove().end().append('<option value="">' + $trans('core.base.label.select') + '</option>');

    if (triggerChange) {
        select.trigger('change.select2')
    }

    select.select2({
        placeholder: $selectLabel,
        allowClear: allowClear,
        minimumResultsForSearch: 10,
    });

}

// Function to closed select box values for disables and readonly fields
function disableFormInputs(element, readonly, buttons, time, notElement) {
    if (readonly === undefined) {
        readonly = false;
    }

    if (buttons === undefined) {
        buttons = false;
    }

    if (time === undefined) {
        time = 0;
    }

    if (notElement === undefined) {
        notElement = '';
    }

    if (typeof element !== "undefined" && element.length > 0) {
        setTimeout(function () {
            if (readonly) {
                element.find(":input").attr("readonly", 'readonly').addClass('readonly').attr('tabindex', -1);

                element.find('select').on('select2:open', function (event) {
                    $(this).select2('close');
                });

                if (element.find('.input-group-addon')) {
                    element.find(".input-group-addon").addClass('readonly');
                }
            } else {
                element.find(":input").not('.s-table-edit,.btn-pdf').not(notElement).attr("disabled", 'disabled');

                element.find('select:disabled').on('select2:open', function (event) {
                    $(this).select2('close');
                });

                if (buttons) {
                    element.find("button").not('.s-table-edit,.btn-pdf').attr("disabled", 'disabled');
                }

                if (element.find('.general-status')) {
                    setTimeout(function () {
                        element.find(".general-status").addClass('disabled');
                    }, 300)
                }

                if (element.find('.input-group-addon')) {
                    element.find(".input-group-addon").addClass('readonly');
                }

                if (element.find('.dropzone')) {
                    element.find('.dropzone').remove()
                }
            }
        }, time)
    }
}

// If user has not access to current modal show this modal
function noRoleModal() {
    var noRoleModal = $('#noRoleModal');

    if (noRoleModal.length > 0) {
        noRoleModal.modal();
    }
}

function needUpdateUserRolesSession() {
    var needUpdateUserRolesSession = $('#needUpdateUserRolesSessionModal');

    if (needUpdateUserRolesSession.length > 0) {
        needUpdateUserRolesSession.modal({
            backdrop: 'static',
            keyboard: false
        });
    }
}

// Function to set content all disabled
function viewModeContent(element) {

    if (element === undefined) {
        element = $(".tab-content");
    }

    if (typeof $saveMode != 'undefined' && $saveMode === 'view') {
        disableFormInputs(element, false, true);
    }
}

function inputGroupAddonsAddReadonly() {

    var inputGroups = $('.input-group');
    $.each(inputGroups, function (k, v) {
        if ($(this).find('.input-group-addon').length && ($(this).find('input').is('[readonly]') || $(this).find('input').is('[disabled]'))) {
            $(this).find('.input-group-addon').addClass('readonly');
        }
    });
}

// Get company info by tax id
function getCompanyInfoByTaxID(taxIdInput, afterSend, getResultCallback) {
    if (taxIdInput.length > 0) {
        $(taxIdInput).on('input', function (e) {
            if (taxIdInput.val().length >= $taxIdMinLength && taxIdInput.val().length <= $taxIdMaxLength) {
                var selfFormGroup = taxIdInput.closest('.form-group');

                selfFormGroup.removeClass('has-error');
                selfFormGroup.find('.form-error').text('').removeClass('form-error-text');

                spinnerLoadState(taxIdInput);
                afterSend();

                $.ajax({
                    type: 'POST',
                    url: $cjs.admPath('entity/company-info'),
                    data: {tax_id: taxIdInput.val()},
                    dataType: 'json',
                    success: function (result) {

                        if (result.status === 'OK') {
                            getResultCallback(result)
                        }

                        if (result.status === 'INVALID_DATA') {
                            selfFormGroup.addClass('has-error');
                            selfFormGroup.find('.form-error').text(result.errors.message).addClass('form-error-text').css('display', 'block');
                        }

                        spinnerLoadState(taxIdInput, true);
                    },
                    error: function () {
                        spinnerLoadState(taxIdInput, true);
                        selfFormGroup.addClass('has-error');
                        selfFormGroup.find('.form-error').addClass('form-error-text').text($trans('core.loading.invalid_data')).css('display', 'block');
                    }
                });
            }
        });
    }
}

//
function animateScrollToElement(element, timeout, animateTime) {

    if (timeout === undefined) {
        timeout = 200;
    }
    if (animateTime === undefined) {
        animateTime = 600;
    }

    setTimeout(function () {
        $('html, body').animate({
            scrollTop: element.offset().top - 300
        }, animateTime);
    }, timeout);
}

// Function to show confirm for delete modal
function spinnerLoaderForButton(button, remove) {
    if (remove === undefined) {
        remove = false;
    }
    if (!remove) {
        button.prop('disabled', true);
        button.find('i').addClass('fa-spin fa-spinner')
    }

    if (remove) {
        button.prop('disabled', false);
        button.find('i').removeClass('fa-spin fa-spinner')
    }
}

// Add item Loading state
function spinnerLoadState(item, remove) {

    if (remove === undefined) {
        remove = false;
    }

    if (!remove) {
        item.addClass('loading-state').prop('disabled', true);
    }

    if (remove) {
        item.removeClass('loading-state').prop('disabled', false);
    }
}

// Load Content
function loadContent(selector) {

    var hideContent = $('.tab-content');

    if (selector !== undefined) {
        hideContent = selector;
    }

    hideContent.toggleClass('loading-close')
}

// Close refresh if user want to refresh the page
function closeRefreshingPage() {

    if (window.location.pathname.endsWith('/login')) {
        return;
    }

    if (localStorage.getItem("closeRefresh") !== null) {
        localStorage.removeItem('closeRefresh');
    }

    var refreshModal = $("#d-refresh-page-message");
    var logoutForm = document.getElementById('logout-form');

    if (localStorage.getItem('closeRefresh') !== 'true') {
        $(document).on('change', 'form:not("#search-filter") :input:not([readonly]):not([disabled])', function (e) {
            localStorage.setItem('closeRefresh', 'true');
        });
    }

    $('.closeRefreshGroup a:not([href="#"])').click(function () {

        if (localStorage.getItem('closeRefresh') === 'true') {

            var self = $(this);

            refreshModal.modal({backdrop: 'static', keyboard: false});
            refreshModal.find('.btn-refresh').attr('href', self.attr('href'));

            return false;
        }

    });

    $('#logoutBtn').click(function (e) {
        e.preventDefault();

        if (localStorage.getItem('closeRefresh') === 'true') {
            refreshModal.modal({backdrop: 'static', keyboard: false});
            refreshModal.find('.btn-refresh').attr('data-logout', true);
        } else {
            logoutForm.submit();
        }

    });

    refreshModal.find('.btn-refresh').click(function () {
        var self = $(this);

        if (typeof self.data('logout') !== undefined && self.data('logout')) {
            logoutForm.submit();
        }
    });

    refreshModal.on('hidden.bs.modal', function () {
        refreshModal.find('.btn-refresh').removeAttr('href');
    })

}

// ---- Modals Part

var modalConfirmDelete = function (callback, modalId) {

    var modalConfirmDiv = $('#d-delete-confirm');

    if (modalId !== undefined) {
        modalConfirmDiv = $('#' + modalId);
    }

    return confirmModalFunc(modalConfirmDiv, callback)
};

var modalConfirmClone = function (callback) {

    var modalConfirmDiv = $('#d-clone-confirm');

    return confirmModalFunc(modalConfirmDiv, callback)
};

var modalConfirmSend = function (callback) {

    var modalConfirmDiv = $('#d-send-confirm');

    return confirmModalFunc(modalConfirmDiv, callback)
};

function confirmModalFunc(modalConfirmDiv, callback) {
    modalConfirmDiv.modal('show');

    modalConfirmDiv.find('.btn-delete').on("click", function () {
        callback(true);
        modalConfirmDiv.modal('hide');
    });

    modalConfirmDiv.find('.btn-cancel').on("click", function () {
        callback(false);
        modalConfirmDiv.modal('hide');
    });

    modalConfirmDiv.on('hidden.bs.modal', function () {
        callback(false);
        modalConfirmDiv.find('.btn-delete').unbind('click');
        modalConfirmDiv.find('.btn-cancel').unbind('click');
    });
}

// Language list open
$(document).click(function (event) {

    var clicked = $(event.target);

    if (!clicked.parents().is('.languages')) {
        $('.languages__title').removeClass('active').next().stop().slideUp(300);
    }
});

// Global Main js functionality
if (typeof Dropzone !== 'undefined') {
    Dropzone.autoDiscover = false;
}

// Helpers
$(function () {

    var onlyNumbers = $(".onlyNumbers");
    if (onlyNumbers.length > 0) {
        onlyNumbers.on("keypress", function (evt) {
            if (evt.which !== 8 && evt.which !== 13 && evt.which !== 0 && evt.which < 48 || evt.which > 57) {
                evt.preventDefault();
            }
        });
    }

    var onlyLatinAlpha = $(".onlyLatinAlpha");
    if (onlyLatinAlpha.length > 0) {
        var englishAlphabetAndWhiteSpace = /[A-Za-z0-9\_]/;

        onlyLatinAlpha.on("keypress", function (evt) {

            var key = String.fromCharCode(evt.which);

            if (evt.which !== 8 && evt.which !== 13 && evt.which !== 0 && !englishAlphabetAndWhiteSpace.test(key)) {
                evt.preventDefault();
            }
        });
    }

    var onlyWithFloat = $(".onlyWithFloat");
    if (onlyWithFloat.length > 0) {
        onlyWithFloat.on("keypress", function (evt) {
            if (evt.which !== 8 && evt.which !== 13 && evt.which !== 0 && $(this).val().indexOf('.') != -1 && evt.which < 48 || evt.which > 57) {
                evt.preventDefault();
            }
        });
    }

    // ------------------------------------------

    $('.languages__title').click(function () {

        $(".menu__item.active").removeClass("active");
        $('.login.active').removeClass('active').stop().slideUp(300);
        $(".menu__sub").stop().slideUp();

        $(this).toggleClass('active').next().stop().slideToggle(300);

        return false;

    });

    $('.header-notification__sub').click(function () {
        return false;
    });

    $('.header-notification__sub a').click(function (event) {
        event.stopPropagation();
    });

    // ------------------------------------------

    if ($('.custom-alert-success').length > 0) {
        setTimeout(function () {
            $('.custom-alert-success').slideUp()
        }, 10000)
    }

    // ------------------------------------------

    // Collapse
    $(document).on("click", ".collapseWithIcon", function () {
        if ($(this).find('i').hasClass('fa-chevron-left')) {
            $(this).find('i').removeClass('fa-chevron-left').addClass('fa-chevron-down')
        } else {
            $(this).find('i').removeClass('fa-chevron-down').addClass('fa-chevron-left')
        }
    });

    $(document).on("click", ".search-submit-btn", function () {
        var formId = $(this).data('formId');
        if (formId && $("#" + formId).length > 0) {
            $("#" + formId).submit();
        }
    });

    // ------------------------------------------

    // Focus Fields
    $(".field--focus").focus(function () {
        $('.eid-mid__block').addClass('passive');
        $(this).closest('.eid-mid__block').addClass('focused');
    }).focusout(function () {
        $('.eid-mid__block').removeClass('passive').removeClass('focused');
    });

    // ------------------------------------------

    //Set equal height
    function setEqualHeightEachRowElems() {

        if ((window.innerWidth || screen.availWidth) > 767) {

            var elems = $('.eid-mid__block'),
                max = 0;

            if (!elems.length) {
                return;
            }

            elems.removeAttr('style');

            elems.each(function (i, e) {
                var $e = $(e);
                if ($e.height() > max) {
                    max = $e.height();
                }

            });

            elems.height(max);
        }

    }

    setEqualHeightEachRowElems();
    setTimeout(setEqualHeightEachRowElems, 100);
    $(window).resize(function () {
        setTimeout(setEqualHeightEachRowElems, 100);
    });

});

/* START ------------------------------- Hide Search Params And Columns Start ----------------------------- */
if (typeof customHideSearchParams == 'undefined') {
    function hideSearchParams() {

        var hashCode = 'hiddenParams' + $('#list-data').data('moduleForSearchParams');
        var hiddenFields = JSON.parse(localStorage.getItem(hashCode)) || {'params': [], 'columns': []};

        $('.hide-or-show-params').on('click', function (e) {
            e.stopPropagation();
            $('#hideOrShowParams').modal('show');
        });

        $('#hideOrShowParams').on('shown.bs.modal', function () {
            $('.hide-params-section-body').html("");
            $('.searchParam').each(function (index, item) {
                $('.hide-params-section-body').append('<label><input type="checkbox" class="hide-search-params" data-col="' + $(item).data('field') + '" /> ' + $(item).find('label').text() + '</label><br />');
            });
            $('.hide-search-column').each(function (index, value) {
                if (hiddenFields.columns.includes($(value).data('col'))) {
                    $(value).prop('checked', true);
                }
            });

            $('.hide-search-params').each(function (index, value) {
                if (hiddenFields.params.includes($(value).data('col'))) {
                    $(value).prop('checked', true);
                }
            });
            $('.modal-load-body').hide();
            $('.modal-main-body').show();
        });

        $('.restoreButton').on('click', function () {
            localStorage.removeItem(hashCode);
            window.location.reload();
        });

        $('.hideButton').on('click', function () {
            hiddenFields.params = [];
            $('.hide-search-params:checked').each(function (index, item) {
                var field = $(item).data('col');
                hiddenFields.params.push(field)
            });

            hiddenFields.columns = [];
            $('.hide-search-column:checked').each(function (index, item) {
                var field = $(item).data('col');
                hiddenFields.columns.push(field)
            });

            localStorage.setItem(hashCode, JSON.stringify(hiddenFields));
            window.location.reload();
        });

        $('.searchParam').each(function (index, value) {
            if (hiddenFields.params.includes($(value).data('field'))) {
                $('.' + $(value).data('field')).hide();
            }
        });
    }
}

function replaceAll(str, find, replace) {
    var escapedFind = find.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
    return str.replace(new RegExp(escapedFind, 'g'), replace);
}

/* END ------------------------------- Hide Search Params And Columns END ----------------------------- */


// Fix tab position

var tabsContainer = $('.tabs-container').not("[data-not-fix]");
tabsContainer.closest('body').addClass('heading-page-fixed');

function fixTab() {
    var winWidth = window.innerWidth || screen.availWidth,
        wrapperContent = $('.wrapper-content'),
        customWrapperContent = $('.custom-wrapper__main'),
        navTabs = $('.nav-tabs'),
        constructorButtons = $('.constructor-buttons'),
        alertMessages = $("#alertMessages");
    if (winWidth > 768) {
        wrapperContent.css('padding-top', customWrapperContent.outerHeight());
        navTabs.css('margin-top', customWrapperContent.outerHeight());
        //constructorButtons.css( 'margin-top', '-' + ( constructorButtons.height() + 20 ) + 'px' );
        tabsContainer.closest('.ibox-content').css('padding-top', navTabs.outerHeight() + 5 + 'px');
    } else {
        wrapperContent.removeAttr('style');
        navTabs.removeAttr('style');
        tabsContainer.closest('.ibox-content').removeAttr('style');
        constructorButtons.removeAttr('style');
    }
}

if (tabsContainer.length) {
    fixTab();
    $(window).on('scroll resize load', fixTab);
}

// Page heading fix
function fixActionButtons() {
    var actionBtn = $(".action-btn");

    actionBtn.click(function () {

        $("body").toggleClass("action--active");

        actionBtn.toggleClass("action-btn--active");

        return false;
    });

    $(document).click(function (e) {
        if (!$(e.target).closest('.action-block').length) {

            $("body").removeClass("action--active")

            actionBtn.removeClass("action-btn--active");
        }
    });
}


function countActionInnerHeight() {

    var actionBlockTop = $(".page-heading.custom-wrapper__main").outerHeight();

    $(".action-block").css('top', actionBlockTop);

}

function wrapperWithMenu() {

    var winWidth = window.innerWidth || screen.availWidth;
    if (winWidth > 768) {
        $("body").addClass('wrapper--withMenu');
    } else {
        $("body").removeClass('wrapper--withMenu');
    }

}

if ($("#side-menu").length) {
    wrapperWithMenu();
    $(window).on('scroll resize load', wrapperWithMenu);
}

$(document).on('click', '.wrapper--withMenu.mini-navbar #side-menu a', function () {

    if ($(this).next('ul').length) {
        $(".header__burger").trigger('click');
    }

});

(function ($) {
    $.fn.focusTextToEnd = function () {
        this.focus();
        var $thisVal = this.val();
        this.val('').val($thisVal);
        return this;
    }
}(jQuery));

function searchInMenu() {

    var searchMenuInput = $('.searchMenu');
    $('#side-menu li').click(function () {
        if ($(this).find('.searchMenu').length) {
            searchMenuInput.focus();
        }
    });

    searchMenuInput.keyup(function (e) {

        var li = $(this).closest("li").find('li').not('.inactive-ref-title');
        var filter = $(this).val().toLowerCase();

        $.each(li, function (k, v) {

            var txtValue = $(this).find("a").text();

            if (txtValue.toLowerCase().indexOf(filter) > -1) {
                $(this).removeClass('hide')
            } else {
                $(this).addClass('hide')
            }
        })
    });

    // Set position of scroll in active menu
    var secondLevelMenu = $('#side-menu .nav-second-level');
    if (secondLevelMenu.find('li').hasClass('active')) {
        secondLevelMenu.scrollTop(secondLevelMenu.find('li.active').offset().top - 850);
    }

    if ($('#side-menu > li').hasClass('active')) {
        $('.navbar-default').scrollTop($('#side-menu > li.active').offset().top - 300);
    }
}
