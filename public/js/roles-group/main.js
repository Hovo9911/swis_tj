/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
        "order": [[1, "desc"]],
        lengthChange: true,
        iDisplayLength: 25
    },

    customOptions: {
        afterInitCallback: function () {
        },
        tableDataPath: 'roles-group/table',
        searchPath: 'roles-group',
    }
};

/*
 * Options for Form Request
 */
var optionsForm = {
    searchPath: '/roles-group',
    addPath: '/roles-group/create',
};

/*
 * Init Datatable, Form
 */
var $rolesGroupTable = new DataTable('list-data', optionsTable);
var $rolesGroup = new FormRequest('form-data', optionsForm);

//
var modulesSelect = $('#modules');
var rolesSelect = $('#roles');
var rolesAttributesSelect = $('#rolesAttributes');
var rolesAttributesFormGroup = rolesAttributesSelect.closest('.form-group');
var selectedAttributes = [];

$(document).ready(function () {

    // init
    $rolesGroupTable.init();
    $rolesGroup.init();

    // ------------

    getRolesOfMenu();

    getRolesAttributes();

    selectAttributes();
});

function getRolesOfMenu() {

    modulesSelect.on('change', function () {

        var menu = $(this).val();

        rolesSelect.prop('disabled', true);
        rolesAttributesSelect.prop('disabled', true);
        select2Reset(rolesSelect);
        select2Reset(rolesAttributesSelect);
        selectedAttributes = [];

        //
        rolesAttributesFormGroup.addClass('hide');
        if (menu === myApplicationMenu) {
            rolesAttributesFormGroup.removeClass('hide');
        }

        if (menu) {
            $.ajax({
                type: 'POST',
                url: $cjs.admPath('roles-group/get-roles'),
                data: {menu_id: menu},
                dataType: 'json',
                success: function (result) {

                    if (result.status === 'OK') {
                        rolesSelect.prop('disabled', false);

                        $.each(result.data.roles, function (key, value) {
                            if (value.name) {
                                rolesSelect.append($("<option></option>").attr("title", value.description ? value.description : value.name).attr("value", value.id).text(value.name));
                            }
                        });
                    }
                }
            });
        }
    })
}

function getRolesAttributes() {
    rolesSelect.on('change', function () {

        select2Reset(rolesAttributesSelect);
        rolesAttributesSelect.prop('disabled', true);

        if (rolesSelect.val()) {
            $.ajax({
                type: 'POST',
                url: $cjs.admPath('roles-group/get-roles-attributes'),
                data: {role_ids: rolesSelect.val()},
                dataType: 'json',

                success: function (result) {

                    if (result.status === 'OK') {

                        rolesAttributesSelect.prop('disabled', false);

                        if (typeof result.data.attributes.saf != 'undefined') {
                            $.each(result.data.attributes.saf, function (k, v) {
                                var selectSafAttribute = false;
                                var attributeVal = 'saf_' + k;

                                if (selectedAttributes.indexOf(attributeVal) !== -1) {
                                    selectSafAttribute = true
                                }

                                var newOption = new Option(v, attributeVal, false, selectSafAttribute);

                                rolesAttributesSelect.append(newOption);

                                if (selectSafAttribute) {
                                    rolesAttributesSelect.trigger('change.select2')
                                }
                            })
                        }

                        if (typeof result.data.attributes.mdm != 'undefined') {
                            $.each(result.data.attributes.mdm, function (k, v) {
                                var selectMdmAttribute = false;
                                var attributeVal = 'mdm_' + k;

                                if (selectedAttributes.indexOf(attributeVal) !== -1) {
                                    selectMdmAttribute = true
                                }

                                var newOption = new Option(v, attributeVal, false, selectMdmAttribute);

                                rolesAttributesSelect.append(newOption);

                                if (selectMdmAttribute) {
                                    rolesAttributesSelect.trigger('change.select2')
                                }
                            })
                        }
                    }
                }
            });
        }
    });
}

function selectAttributes() {

    if (rolesAttributesSelect.val()) {
        selectedAttributes = rolesAttributesSelect.val()
    }

    rolesAttributesSelect.on('change', function () {
        selectedAttributes = $(this).val()
    });
}