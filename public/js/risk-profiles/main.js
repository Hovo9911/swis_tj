/*
 * Options for Form Request
 */
var optionsForm = {
    searchPath: '/risk-profiles',
    addPath: '/risk-profiles/create',
    afterGetErrorResult: function (result) { },
};

/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        drawCallback: function () {},
        "order": [[1, "desc"]],
        lengthChange: true,
        iDisplayLength: 25,
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
    },
    customOptions: {
        afterInitCallback: function () { },
        tableDataPath: 'risk-profiles/table',
        searchPath: 'risk-profiles',
    }
};

/*
 * Init Datatable, Form
 */
var $riskProfile = new FormRequest('form-data', optionsForm);
var $riskProfileTable = new DataTable('list-data', optionsTable);

$(document).ready(function () {

    // init
    $riskProfile.init();
    $riskProfileTable.init();

});