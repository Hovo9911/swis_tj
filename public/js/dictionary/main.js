/*
 * Options DataTable
 */
var optionsTable = {
    customOptions: {
        afterInitCallback: function (data) {
            $dictionary.languagesList();
        },
        actions: {
            edit: {
                render: function (row) {
                    return '<span data-toggle="modal"  data-target="#dictionaryModal"><a  data-toggle="tooltip" data-id="' + row.id + '" class="btn btn-edit"  ' +
                        ' title="' + $trans('swis.base.tooltip.edit.button') + '"><i class="fa fa-pen"></i></a></span>'
                }
            },

        },
        tableDataPath: 'dictionary/table',
    },
    datatableOptions: {
        searching: true,
        lengthChange: true,
        lengthMenu: [[25, 100, -1], [25, 100, $trans('swis.base.dictionary.all')]],
        ordering: false,
        drawCallback: function () {
            var api = this.api();
            var data = api.rows({page: 'current'}).data();
            $dictionary.actions();
            $dictionary.storeData(data);
        }
    }

};

/*
 * Options Form Request
 */
var optionsForm = {
    deletePath: '/dictionary/delete?appType=' + $('input[name="appType"]').val(),
    customOptions: {
        onSaveSuccess: function () {
            reloadTable()
        },
        onDeleteSuccess: function () {
            reloadTable()
        }
    }
};

function reloadTable() {
    $dictionaryTable.reload();

    $dictionary.resetForm();

    $dictModal.modal('hide');

    $dictionary.actions();
}

/*
 * INIT
 */
var $dictionaryTable = new DataTable('list-data', optionsTable);

var $dictionary = new FormRequest('form-data', optionsForm);

var $dictModal = $('#dictionaryModal');

/*
 * Storing all data for edit
 */
$dictionary.storeData = function (data) {
    var dictItem;
    this.dictData = {};

    for (var i in data) {
        dictItem = data[i];
        this.dictData['item-' + dictItem.id] = dictItem;
    }
};

/*
 * Edit && Remove Buttons
 */
$dictionary.actions = function () {

    var $langs = this.languagesList();
    var self = this;

    /*
     * Edit Part
     */
    $('.btn-edit').click(function () {
        var id = $(this).data('id');
        var data = self.dictData['item-' + id];

        $.each($langs, function (k, v) {


            $dictModal.find('input[name="ml[' + v + ']"]').val(data[v])
        });

        $dictModal.find('input[name="key"]').val(data['key'])
    });

    /*
     * Delete Part
     */
    $('.btn-remove').click(function () {

        $dictionary.deleteRow([$(this).data('id')]);

    });
};


/*
 * Get all languages list (codes)
 */
$dictionary.languagesList = function () {
    this.languages = [];

    for (var i in languagesList) {
        this.languages.push(languagesList[i].code)
    }

    return this.languages;
};


$(document).ready(function () {

    $dictionaryTable.init();

    $dictionary.init();

});

/*
 * Application Type (add GET param)
 */
$("#applications-list").change(function () {
    document.location.href = $cjs.admPath('/dictionary?appType=' + $(this).val());
});

/*
 * ADD/EDIT modal show
 */
$('.mc-nav-button-add').click(function () {
    $dictionary.resetForm();

    $dictModal.modal();
});
