function timedProxy(pluginObject, name) {
    return function () {
        console.time(name);
        arguments[arguments.length - 2] = timedCallbackProxy(arguments[arguments.length - 2], name);
        arguments[arguments.length - 1] = timedCallbackProxy(arguments[arguments.length - 1], name);
        pluginObject[name].apply(pluginObject, arguments);
    };
}

function cryptoPlugin(pluginObject, noAutoRefresh) {
    this.autoRefresh = noAutoRefresh ? false : true;

    this.pluginObject = pluginObject;
    if (!this.pluginObject.valid) this.delayedReport("Error: couldn't get CryptopluginObject");

    for (var key in this.pluginObject) {
        if (this[key]) continue;

        if (typeof (this.pluginObject[key]) == "function") this[key] = timedProxy(this.pluginObject, key);
        else this[key] = this.pluginObject[key];
    }

    this.errorCodes = this.pluginObject.errorCodes;
    this.errorDescription[this.errorCodes.UNKNOWN_ERROR] = "Неизвестная ошибка";
    this.errorDescription[this.errorCodes.BAD_PARAMS] = "Неправильные параметры";
    this.errorDescription[this.errorCodes.NOT_ENOUGH_MEMORY] = "Недостаточно памяти";

    this.errorDescription[this.errorCodes.DEVICE_NOT_FOUND] = "Устройство не найдено";
    this.errorDescription[this.errorCodes.DEVICE_ERROR] = "Ошибка устройства";
    this.errorDescription[this.errorCodes.TOKEN_INVALID] = "Ошибка чтения/записи устройства. Возможно, устройство было извлечено. Попробуйте выполнить enumerate";

    this.errorDescription[this.errorCodes.CERTIFICATE_CATEGORY_BAD] = "Недопустимый тип сертификата";
    this.errorDescription[this.errorCodes.CERTIFICATE_EXISTS] = "Сертификат уже существует на устройстве";
    this.errorDescription[this.errorCodes.CERTIFICATE_NOT_FOUND] = "Сертификат не найден";
    this.errorDescription[this.errorCodes.CERTIFICATE_HASH_NOT_UNIQUE] = "Хэш сертификата не уникален";
    this.errorDescription[this.errorCodes.CA_CERTIFICATES_NOT_FOUND] = "Корневые сертификаты не найдены";
    this.errorDescription[this.errorCodes.CERTIFICATE_VERIFICATION_ERROR] = "Ошибка проверки сертификата";

    this.errorDescription[this.errorCodes.PKCS11_LOAD_FAILED] = "Не удалось загрузить PKCS#11 библиотеку";

    this.errorDescription[this.errorCodes.PIN_LENGTH_INVALID] = "Некорректная длина PIN-кода";
    this.errorDescription[this.errorCodes.PIN_INCORRECT] = "Некорректный PIN-код";
    this.errorDescription[this.errorCodes.PIN_LOCKED] = "PIN-код заблокирован";
    this.errorDescription[this.errorCodes.PIN_CHANGED] = "PIN-код был изменен";
    this.errorDescription[this.errorCodes.PIN_INVALID] = "PIN-код содержит недопустимые символы";
    this.errorDescription[this.errorCodes.USER_PIN_NOT_INITIALIZED] = "PIN-код пользователя не инициализирован";

    this.errorDescription[this.errorCodes.SESSION_INVALID] = "Состояние токена изменилось";
    this.errorDescription[this.errorCodes.USER_NOT_LOGGED_IN] = "Выполните вход на устройство";
    this.errorDescription[this.errorCodes.ALREADY_LOGGED_IN] = "Вход на устройство уже был выполнен";

    this.errorDescription[this.errorCodes.ATTRIBUTE_READ_ONLY] = "Свойство не может быть изменено";
    this.errorDescription[this.errorCodes.KEY_NOT_FOUND] = "Соответствующая сертификату ключевая пара не найдена";
    this.errorDescription[this.errorCodes.KEY_ID_NOT_UNIQUE] = "Идентификатор ключевой пары не уникален";
    this.errorDescription[this.errorCodes.CEK_NOT_AUTHENTIC] = "Выбран неправильный ключ";
    this.errorDescription[this.errorCodes.KEY_LABEL_NOT_UNIQUE] = "Метка ключевой пары не уникальна";
    this.errorDescription[this.errorCodes.WRONG_KEY_TYPE] = "Неправильный тип ключа";
    this.errorDescription[this.errorCodes.LICENCE_READ_ONLY] = "Лицензия доступна только для чтения";

    this.errorDescription[this.errorCodes.DATA_INVALID] = "Неверные данные";
    this.errorDescription[this.errorCodes.DATA_LEN_RANGE] = "Некорректный размер данных";
    this.errorDescription[this.errorCodes.UNSUPPORTED_BY_TOKEN] = "Операция не поддерживается токеном";
    this.errorDescription[this.errorCodes.KEY_FUNCTION_NOT_PERMITTED] = "Операция запрещена для данного типа ключа";

    this.errorDescription[this.errorCodes.BASE64_DECODE_FAILED] = "Ошибка декодирования даных из BASE64";
    this.errorDescription[this.errorCodes.PEM_ERROR] = "Ошибка разбора PEM";
    this.errorDescription[this.errorCodes.ASN1_ERROR] = "Ошибка декодирования ASN1 структуры";

    this.errorDescription[this.errorCodes.FUNCTION_REJECTED] = "Операция отклонена пользователем";
    this.errorDescription[this.errorCodes.FUNCTION_FAILED] = "Невозможно выполнить операцию";
    this.errorDescription[this.errorCodes.MECHANISM_INVALID] = "Указан неправильный механизм";
    this.errorDescription[this.errorCodes.ATTRIBUTE_VALUE_INVALID] = "Передан неверный атрибут";

    this.errorDescription[this.errorCodes.X509_UNABLE_TO_GET_ISSUER_CERT] = "Невозможно получить сертификат подписанта";
    this.errorDescription[this.errorCodes.X509_UNABLE_TO_GET_CRL] = "Невозможно получить CRL";
    this.errorDescription[this.errorCodes.X509_UNABLE_TO_DECRYPT_CERT_SIGNATURE] = "Невозможно расшифровать подпись сертификата";
    this.errorDescription[this.errorCodes.X509_UNABLE_TO_DECRYPT_CRL_SIGNATURE] = "Невозможно расшифровать подпись CRL";
    this.errorDescription[this.errorCodes.X509_UNABLE_TO_DECODE_ISSUER_PUBLIC_KEY] = "Невозможно раскодировать открытый ключ эмитента";
    this.errorDescription[this.errorCodes.X509_CERT_SIGNATURE_FAILURE] = "Неверная подпись сертификата";
    this.errorDescription[this.errorCodes.X509_CRL_SIGNATURE_FAILURE] = "Неверная подпись CRL";
    this.errorDescription[this.errorCodes.X509_CERT_NOT_YET_VALID] = "Срок действия сертификата еще не начался";
    this.errorDescription[this.errorCodes.X509_CRL_NOT_YET_VALID] = "Срок действия CRL еще не начался";
    this.errorDescription[this.errorCodes.X509_CERT_HAS_EXPIRED] = "Срок действия сертификата истек";
    this.errorDescription[this.errorCodes.X509_CRL_HAS_EXPIRED] = "Срок действия CRL истек";
    this.errorDescription[this.errorCodes.X509_ERROR_IN_CERT_NOT_BEFORE_FIELD] = "Некорректные данные в поле \"notBefore\" у сертификата";
    this.errorDescription[this.errorCodes.X509_ERROR_IN_CERT_NOT_AFTER_FIELD] = "Некорректные данные в поле \"notAfter\" у сертификата";
    this.errorDescription[this.errorCodes.X509_ERROR_IN_CRL_LAST_UPDATE_FIELD] = "Некорректные данные в поле \"lastUpdate\" у CRL";
    this.errorDescription[this.errorCodes.X509_ERROR_IN_CRL_NEXT_UPDATE_FIELD] = "Некорректные данные в поле \"nextUpdate\" у CRL";
    this.errorDescription[this.errorCodes.X509_OUT_OF_MEM] = "Нехватает памяти";
    this.errorDescription[this.errorCodes.X509_DEPTH_ZERO_SELF_SIGNED_CERT] = "Недоверенный самоподписанный сертификат";
    this.errorDescription[this.errorCodes.X509_SELF_SIGNED_CERT_IN_CHAIN] = "В цепочке обнаружен недоверенный самоподписанный сертификат";
    this.errorDescription[this.errorCodes.X509_UNABLE_TO_GET_ISSUER_CERT_LOCALLY] = "Невозможно получить локальный сертификат подписанта";
    this.errorDescription[this.errorCodes.X509_UNABLE_TO_VERIFY_LEAF_SIGNATURE] = "Невозможно проверить первый сертификат";
    this.errorDescription[this.errorCodes.X509_CERT_CHAIN_TOO_LONG] = "Слишком длинная цепочка сертификатов";
    this.errorDescription[this.errorCodes.X509_CERT_REVOKED] = "Сертификат отозван";
    this.errorDescription[this.errorCodes.X509_INVALID_CA] = "Неверный корневой сертификат";
    this.errorDescription[this.errorCodes.X509_INVALID_NON_CA] = "Неверный некорневой сертфикат, помеченный как корневой";
    this.errorDescription[this.errorCodes.X509_PATH_LENGTH_EXCEEDED] = "Превышена длина пути";
    this.errorDescription[this.errorCodes.X509_PROXY_PATH_LENGTH_EXCEEDED] = "Превышина длина пути прокси";
    this.errorDescription[this.errorCodes.X509_PROXY_CERTIFICATES_NOT_ALLOWED] = "Проксирующие сертификаты недопустимы";
    this.errorDescription[this.errorCodes.X509_INVALID_PURPOSE] = "Неподдерживаемое назначение сертификата";
    this.errorDescription[this.errorCodes.X509_CERT_UNTRUSTED] = "Недоверенный сертификат";
    this.errorDescription[this.errorCodes.X509_CERT_REJECTED] = "Сертифкат отклонен";
    this.errorDescription[this.errorCodes.X509_APPLICATION_VERIFICATION] = "Ошибка проверки приложения";
    this.errorDescription[this.errorCodes.X509_SUBJECT_ISSUER_MISMATCH] = "Несовпадения субьекта и эмитента";
    this.errorDescription[this.errorCodes.X509_AKID_SKID_MISMATCH] = "Несовпадение идентификатора ключа у субьекта и доверенного центра";
    this.errorDescription[this.errorCodes.X509_AKID_ISSUER_SERIAL_MISMATCH] = "Несовпадение серийного номера субьекта и доверенного центра";
    this.errorDescription[this.errorCodes.X509_KEYUSAGE_NO_CERTSIGN] = "Ключ не может быть использован для подписи сертификатов";
    this.errorDescription[this.errorCodes.X509_UNABLE_TO_GET_CRL_ISSUER] = "Невозможно получить CRL подписанта";
    this.errorDescription[this.errorCodes.X509_UNHANDLED_CRITICAL_EXTENSION] = "Неподдерживаемое расширение";
    this.errorDescription[this.errorCodes.X509_KEYUSAGE_NO_CRL_SIGN] = "Ключ не может быть использован для подписи CRL";
    this.errorDescription[this.errorCodes.X509_KEYUSAGE_NO_DIGITAL_SIGNATURE] = "Ключ не может быть использован для цифровой подписи";
    this.errorDescription[this.errorCodes.X509_UNHANDLED_CRITICAL_CRL_EXTENSION] = "Неподдерживаемое расширение CRL";
    this.errorDescription[this.errorCodes.X509_INVALID_EXTENSION] = "Неверное или некорректное расширение сертификата";
    this.errorDescription[this.errorCodes.X509_INVALID_POLICY_EXTENSION] = "Неверное или некорректное расширение политик сертификата";
    this.errorDescription[this.errorCodes.X509_NO_EXPLICIT_POLICY] = "Явные политики отсутствуют";
    this.errorDescription[this.errorCodes.X509_DIFFERENT_CRL_SCOPE] = "Другая область CRL";
    this.errorDescription[this.errorCodes.X509_UNSUPPORTED_EXTENSION_FEATURE] = "Неподдерживаемое расширение возможностей";
    this.errorDescription[this.errorCodes.X509_UNNESTED_RESOURCE] = "RFC 3779 неправильное наследование ресурсов";
    this.errorDescription[this.errorCodes.X509_PERMITTED_VIOLATION] = "Неправильная структура сертифката";
    this.errorDescription[this.errorCodes.X509_EXCLUDED_VIOLATION] = "Неправильная структура сертфиката";
    this.errorDescription[this.errorCodes.X509_SUBTREE_MINMAX] = "Неправильная структура сертифката";
    this.errorDescription[this.errorCodes.X509_UNSUPPORTED_CONSTRAINT_TYPE] = "Неправильная структура сертфиката";
    this.errorDescription[this.errorCodes.X509_UNSUPPORTED_CONSTRAINT_SYNTAX] = "Неправильная структура сертифката";
    this.errorDescription[this.errorCodes.X509_UNSUPPORTED_NAME_SYNTAX] = "Неправильная структура сертфиката";
    this.errorDescription[this.errorCodes.X509_CRL_PATH_VALIDATION_ERROR] = "Неправильный путь CRL";
    this.errorDescription[this.errorCodes.CMS_CERTIFICATE_ALREADY_PRESENT] = "Сертификат уже используется";
    this.errorDescription[this.errorCodes.CANT_HARDWARE_VERIFY_CMS] = "Проверка множественной подписи с вычислением хеша на устройстве не поддерживается";
    this.errorDescription[this.errorCodes.DECRYPT_UNSUCCESSFUL] = "Расшифрование не удалось"

    if (this.autoRefresh) this.enumerateDevices();
}

cryptoPlugin.prototype = {
    pluginObject: null,
    errorCodes: null,
    errorDescription: [],
    methods: null,
    constants: null,
    autoRefresh: null,

    delayedReport: function (message) {
        setTimeout(function () {
            // ui.writeln(message + "\n");
            console.log(message + "\n");
        }, 0);
    },

    enumerateDevices: function (update) {
        if (update) {
            var options = {"mode": this.ENUMERATE_DEVICES_EVENTS};

            this.pluginObject.enumerateDevices(options).then($.proxy(function (devices) {
                for (key in devices) {
                    switch (key) {
                        case "connected":
                            for (var d in devices[key]) {
                                var dev = devices[key][d];
                                // To handle fast device reconnect first try to remove it.
                                ui.removeDevice(dev);

                                this.pluginObject.getDeviceInfo(dev, plugin.TOKEN_INFO_LABEL).then($.proxy(function (device) {
                                    return function (label) {
                                        if (label == "Rutoken ECP <no label>") label = "Rutoken ECP #" + device.toString();
                                        ui.removeInfoInDeviceList();
                                        ui.addDevice(device, label, false);
                                        $('#rutokenAlertMessage').addClass('hide');

                                        if (ui.device() == device) {
                                            if (this.autoRefresh) this.enumerateKeys(device);
                                            if (this.autoRefresh) this.enumerateCertificates(device);
                                            else ui.clearCertificateList("Обновите список сертификатов");
                                        }
                                    };
                                }(dev), this), $.proxy(ui.printError, ui));
                            }
                            break;
                        case "disconnected":
                            for (var d in devices[key]) {
                                var selectedDevice = ui.device(),
                                    device = devices[key][d];

                                ui.removeDevice(device);

                                if (device == selectedDevice) {
                                    try {
                                        var dev = ui.device();

                                        if (this.autoRefresh) this.enumerateKeys(ui.device());
                                        if (this.autoRefresh) this.enumerateCertificates(ui.device());
                                        else ui.clearCertificateList("Обновите список сертификатов");
                                    } catch (e) {
                                        ui.clearDeviceList($trans('swis.user_documents.digital_signature_sign.no_device_available'));
                                        ui.clearCertificateList($trans('swis.user_documents.digital_signature_sign.no_device_available'));
                                        ui.clearKeyList($trans('swis.user_documents.digital_signature_sign.no_device_available'));

                                        alertMessage($trans('swis.user_documents.digital_signature_sign.no_device_available'), 'danger');
                                    }
                                }
                            }
                            break;
                    }
                }
                ui.enumerateDevicesCallback();
            }, this), $.proxy(ui.printError, ui));
        } else {
            ui.clearDeviceList("Список устройств обновляется...");

            var options = {"mode": this.ENUMERATE_DEVICES_LIST};

            this.pluginObject.enumerateDevices(options).then($.proxy(function (devices) {
                if (Object.keys(devices).length == 0) {
                    ui.clearDeviceList($trans('swis.user_documents.digital_signature_sign.no_device_available'));
                    ui.clearCertificateList($trans('swis.user_documents.digital_signature_sign.no_device_available'));
                    ui.clearKeyList($trans('swis.user_documents.digital_signature_sign.no_device_available'));

                    alertMessage($trans('swis.user_documents.digital_signature_sign.no_device_available'), 'danger');
                    return;
                }
                //            ui.clearKeyList("Выполните вход на устройство");
                ui.clearDeviceList();
                if (this.autoRefresh) this.enumerateKeys(devices[0]);
                if (this.autoRefresh) this.enumerateCertificates(devices[0]);
                else ui.clearCertificateList("Обновите список сертификатов");

                for (var d in devices) {
                    this.pluginObject.getDeviceInfo(devices[d], plugin.TOKEN_INFO_LABEL).then($.proxy(function (device) {
                        return function (label) {
                            if (label == "Rutoken ECP <no label>") label = "Rutoken ECP #" + device.toString();
                            ui.addDevice(device, label, false);
                        };
                    }(devices[d]), this), $.proxy(ui.printError, ui));
                }

                ui.enumerateDevicesCallback(devices);
            }, this), $.proxy(ui.printError, ui));
        }
    },

    enumerateKeys: function (deviceId, marker) {
        ui.clearKeyList("Список ключевых пар обновляется...");
        marker = (marker === undefined) ? "" : marker;
        deviceId = (deviceId === undefined) ? ui.device() : deviceId;
        this.pluginObject.enumerateKeys(deviceId, marker).then($.proxy(function (keys) {
            if (keys.length == 0) {
                ui.clearKeyList("На устройстве отсутствуют ключевые пары");
                return;
            }

            ui.clearKeyList();
            for (var k in keys) {
                this.pluginObject.getKeyLabel(deviceId, keys[k]).then(function (key) {
                    return function (label) {
                        if (label == "") label = "key: " + key.toString();
                        ui.addKey(key, label);
                    };
                }(keys[k]), $.proxy(ui.printError, ui));
            }
            ui.enumerateKeysCallback();
        }, this), function (error) {
            let errorCode = getErrorCode(error);
            if (errorCode == plugin.errorCodes.USER_NOT_LOGGED_IN) ui.clearKeyList(plugin.errorDescription[errorCode]);
            else ui.printError(error);
        });
    },

    enumerateCertificates: function (deviceId) {
        ui.clearCertificateList("Список сертификатов обновляется...");
        var device = (deviceId === undefined) ? ui.device() : deviceId;
        try {
            var certs = [];
            this.pluginObject.enumerateCertificates(device, this.CERT_CATEGORY_USER).then($.proxy(function (certificates) {
                ui.clearCertificateList();
                for (var c in certificates)
                    certs.push({certificate: certificates[c], category: this.CERT_CATEGORY_USER});

                return this.pluginObject.enumerateCertificates(device, this.CERT_CATEGORY_CA);
            }, this)).then($.proxy(function (certificates) {
                for (var c in certificates)
                    certs.push({certificate: certificates[c], category: this.CERT_CATEGORY_CA});

                return this.pluginObject.enumerateCertificates(device, this.CERT_CATEGORY_OTHER);
            }, this)).then($.proxy(function (certificates) {
                for (var c in certificates)
                    certs.push({certificate: certificates[c], category: this.CERT_CATEGORY_OTHER});

                return this.pluginObject.enumerateCertificates(device, this.CERT_CATEGORY_UNSPEC);
            }, this)).then($.proxy(function (certificates) {
                for (var c in certificates)
                    certs.push({certificate: certificates[c], category: this.CERT_CATEGORY_UNSPEC});

                var parsedCerts = [];
                for (var c in certs) {
                    parsedCerts.push(this.pluginObject.parseCertificate(device, certs[c].certificate).then(function (handle, category) {
                        return function (parsedCert) {
                            ui.addCertificate(handle, parsedCert, category);
                        };
                    }(certs[c].certificate, certs[c].category), $.proxy(ui.printError, ui)));
                }

                Promise.all(parsedCerts).then(function () {
                    try {
                        ui.certificate();
                        ui.enumerateCertificatesCallback();
                    } catch (e) {
                        ui.clearCertificateList("На устройстве отсутствуют сертификаты");
                    }
                });
            }, this), function (error) {
                ui.printError(error);
                ui.clearCertificateList("Произошла ошибка");
            });
        } catch (e) {
            // ui now throws an exception if there is no devices avalable
            console.log(e);
        }
    },

    enumerateStoreCertificates: function () {
        function addSystemStoreCertificates(certificates) {
            for (var c in certificates) {
                ui.addSystemStoreCertificate(certificates[c]);
            }
        }

        ui.clearSystemStoreCertificateList("Список сертификатов обновляется...");
        try {
            var options = {};
            this.pluginObject.enumerateStoreCertificates(options).then($.proxy(function (certificates) {
                ui.clearSystemStoreCertificateList();
                $.proxy(addSystemStoreCertificates, this)(certificates);

                try {
                    var systemStoreCertificate = ui.systemStoreCertificate();
                } catch (e) {
                    ui.clearSystemStoreCertificateList("В хранилище отсутствуют сертификаты");
                }
            }, this), function (error) {
                ui.printError(error);
                ui.clearSystemStoreCertificateList("Произошла ошибка");
            });
        } catch (e) {
            console.log(e);
        }
    },

    login: function () {
        this.pluginObject.login(ui.device(), ui.pin()).then($.proxy(function () {
            ui.writeln("Вход выполнен\n");

            var module = $('#module').val();
            var subApplicationId = $('#subapplicationId').val();
            var message = $('#message');
            var digitalSignatureId = $('#digitalSignatureId');

            disableFormInputs($('#rutoken-modal'), false, true);

            if (typeof module != 'undefined' && module === 'SAF') {
                var URL = '/single-application/sign-digital-signature';
            } else {
                var URL = '/user-documents/sign-digital-signature';
            }

            $.ajax({
                type: 'POST',
                url: $cjs.admPath(URL),
                data: {subApplicationId: subApplicationId},
                success: function (result) {
                    if (result.status === 'OK') {
                        message.val(result.data.fileDigest);
                        digitalSignatureId.val(result.data.digitalSignatureId);

                        setTimeout(function () {
                            $('#sign-button').trigger('click');
                        }, 500);

                    }
                }
            });
            if (this.autoRefresh) this.enumerateKeys();
            else ui.clearKeyList("Обновите список ключевых пар");
        }, this), $.proxy(ui.printError, ui));
    },

    logout: function () {
        this.pluginObject.logout(ui.device()).then($.proxy(function () {
            ui.writeln("Выход выполнен\n");
            plugin.pluginObject.getDeviceInfo(ui.device(), plugin.TOKEN_INFO_IS_LOGGED_IN).then(function (result) {
                if (!result) ui.clearKeyList("Выполните вход на устройство");
            }, $.proxy(ui.printError, ui));
        }, this), $.proxy(ui.printError, ui));
    },

    savePin: function () {
        this.pluginObject.savePin(ui.device()).then($.proxy(function () {
            ui.writeln("PIN-код сохранен в кэше\n");
        }, this), $.proxy(ui.printError, ui));
    },

    removePin: function () {
        this.pluginObject.removePin(ui.device()).then($.proxy(function () {
            ui.writeln("PIN-код удален из кэша\n");
            ui.clearKeyList("Выполните вход на устройство");
        }, this), $.proxy(ui.printError, ui));
    }
};

function onPluginLoaded(pluginObject) {
    try {
        var noAutoRefresh = (document.location.search.indexOf("noauto") !== -1);

        plugin = new cryptoPlugin(pluginObject, noAutoRefresh);
        ui.registerEvents();

        window.setInterval(function () {
            if (document.visibilityState == "visible") {
                plugin.enumerateDevices(true);
            }
        }, 500);
    } catch (error) {
        ui.writeln(error);
    }
}

function getErrorCode(error) {
    let errorCode = 0;
    if (isNmPlugin)
        errorCode = parseInt(error.message);
    else
        errorCode = error;
    return errorCode;
}

function showRutokenError(reason) {
    console.log(reason);
}

function alertMessage(message, status) {
    var alertMessageDiv = $('#rutokenAlertMessage');
    alertMessageDiv.removeClass('hide');
    alertMessageDiv.html('<div class="alert alert-' + status + '">' + message + '</div>');
}

function disableModalInputs() {
    let digitalSignatureModal = $('#rutoken-modal');

    if (typeof digitalSignatureModal !== "undefined" && digitalSignatureModal.length > 0) {
        digitalSignatureModal.find(":input").not('.modal__close',).attr("disabled", true);
        alertMessage($trans('swis.user_documents.digital_signature_sign.rutoken_plugin_missed'), 'danger');
    }
}

function uiControls() {
    this.deviceList = $("#device-list");
    this.keyList = $("#key-list");
    this.certificateList = $("#cert-list");
    this.systemStoreCertificateList = $("#system-store-cert-list");

    this.refreshDeviceListButton = $("#refresh-dev");
    this.refreshKeyListButton = $("#refresh-keys");
    this.refreshCertificateListButton = $("#refresh-certs");
    this.refreshSystemStoreCertificateListButton = $("#refresh-system-store-certs");

    this.loginButton = $("#login");
    this.logoutButton = $("#logout");

    this.savePinButton = $("#save-pin");
    this.removePinButton = $("#remove-pin");

    this.pinInput = $("#device-pin");
}

uiControls.prototype = {
    deviceList: null,
    keyList: null,
    certificateList: null,
    systemStoreCertificateList: null,

    refreshDeviceListButton: null,
    refreshKeyListButton: null,
    refreshCertificateListButton: null,
    refreshSystemStoreCertificateListButton: null,
    loginButton: null,
    logoutButton: null,
    savePinButton: null,
    removePinButton: null,

    pinInput: null
};

function rutokenUi() {
    this.controls = new uiControls();
};

rutokenUi.prototype = {
    controls: null,

    writeln: function (text) {
        console.log(text);
    },

    printError: function (error) {
        // if (this.useConsole) {
        //     console.trace();
        //     console.debug(arguments);
        // }
        alertMessage($trans('swis.user_documents.digital_signature_sign.wrong_credentials'), 'danger');

        let errorCode = getErrorCode(error);
        if (plugin.errorDescription[errorCode] === undefined) {
            this.writeln("Внутренняя ошибка (Код: " + errorCode + ") \n");
        } else {
            this.writeln("Ошибка: " + plugin.errorDescription[errorCode] + "\n");
        }
    },

    registerEvents: function () {
        this.controls.refreshDeviceListButton.click($.proxy(function () {
            try {
                plugin.enumerateDevices();
            } catch (error) {
                this.writeln(error.toString());
                this.clearDeviceList(error.toString());
            }
        }, this));
        /*
        this.controls.refreshKeyListButton.click($.proxy(function () {
            try {
                plugin.enumerateKeys();
            } catch (error) {
                this.writeln(error.toString());
                this.clearKeyList(error.toString());
            }
        }, this));
        */

        this.controls.refreshCertificateListButton.click($.proxy(function () {
            try {
                plugin.enumerateCertificates();
            } catch (error) {
                this.writeln(error.toString());
                this.clearCertificateList(error.toString());
            }
        }, this));

        /*
        this.controls.refreshSystemStoreCertificateListButton.click($.proxy(function () {
            try {
                plugin.enumerateStoreCertificates();
            } catch (error) {
                this.writeln(error.toString());
                this.clearSystemStoreCertificateList(error.toString());
            }
        }, this));
        */

        this.controls.loginButton.click($.proxy(function () {
            try {
                plugin.login();
            } catch (error) {
                this.writeln(error.toString());
            }
        }, this));

        this.controls.logoutButton.click($.proxy(function () {
            try {
                plugin.logout();
            } catch (error) {
                this.writeln(error.toString());
            }
        }, this));

        /*
        this.controls.savePinButton.click($.proxy(function () {
            try {
                plugin.savePin();
            } catch (error) {
                this.writeln(error.toString());
            }
        }, this));

        this.controls.removePinButton.click($.proxy(function () {
            try {
                plugin.removePin();
            } catch (error) {
                this.writeln(error.toString());
            }
        }, this));*/

        this.controls.deviceList.change($.proxy(function () {
            if (plugin.autoRefresh) {
                plugin.enumerateKeys();
                plugin.enumerateCertificates();
            } else {
                this.clearKeyList("Обновите список ключевых пар");
                this.clearCertificateList("Обновите список сертификатов");
            }
        }, this));
    },

    pin: function () {
        return this.controls.pinInput.val();
    },

    device: function () {
        var deviceId = Number(this.controls.deviceList.val());
        if (isNaN(deviceId)) {
            throw "Нет доступных устройств";
        }
        return deviceId;
    },

    addDevice: function (deviceId, label, selected) {
        selected = (selected === undefined) ? false : selected;
        ui.controls.deviceList.append($("<option>", {
            'value': deviceId,
            'selected': selected,
        }).text(label));
    },

    removeDevice: function (deviceId) {
        this.controls.deviceList.find("option[value='" + deviceId + "']").remove();
        if (!this.controls.deviceList.has('option').length) this.controls.deviceList.append($("<option>").text("Нет доступных устройств"));
    },

    removeInfoInDeviceList: function () {
        this.controls.deviceList.find('option:not([value])').remove();
    },

    clearDeviceList: function (message) {
        this.controls.deviceList.empty();
        if (message) this.controls.deviceList.append($("<option>").text(message));
    },

    enumerateDevicesCallback: function () {
        // device list received
        var devices = $(ui.controls.deviceList).find('option');
        if (devices.length > 0) {
            if (devices.length > 1) {
                $(ui.controls.deviceList).closest('.block').show();
            } else {
                $(ui.controls.deviceList).closest('.block').hide();
            }
        }
    },

    certificate: function () {
        if (this.controls.certificateList.val() == null) throw "Сертификат не выбран";
        return this.controls.certificateList.val();
    },

    addCertificate: function (handle, certificate, category) {

        var description = "";
        switch (category) {
            case plugin.CERT_CATEGORY_USER:
                description = "Пользовательский| ";
                break;
            case plugin.CERT_CATEGORY_CA:
                description = "Корневой| ";
                break;
            case plugin.CERT_CATEGORY_OTHER:
                description = "Другой| ";
                break;
            case plugin.CERT_CATEGORY_UNSPEC:
                description = "Не задана| ";
                break;
        }

        var subjectDNs = certificate.subject;
        var noSubject = true;
        for (c in subjectDNs) {
            if (subjectDNs[c]["rdn"] == "commonName" || subjectDNs[c]["rdn"] == "emailAddress") {
                noSubject = false;
                description += subjectDNs[c]["rdn"] + "=" + subjectDNs[c]["value"] + "|";
            }
        }
        if (noSubject) description += certificate.serialNumber;

        var title = "Serial number: " + certificate.serialNumber + "\n\nIssuer:\n\t";
        var issuerDNs = certificate.issuer;
        for (c in issuerDNs) {
            title += issuerDNs[c]["rdn"] + "=" + issuerDNs[c]["value"] + "\n\t";
        }

        this.controls.certificateList.append($("<option>", {
            'value': handle,
            'title': $.trim(title).replace(/&/g, "&amp;").replace(/>/g, "&gt;").replace(/</g, "&lt;").replace(/"/g, "&quot;")
        }).text(noSubject ? certificate.serialNumber : description));
    },

    clearCertificateList: function (message) {
        // disable sign button
        $('#sign-button').attr('disabled', 'disabled');
        this.controls.certificateList.empty();
        if (message) this.controls.certificateList.append($("<option>").text(message));
    },

    enumerateCertificatesCallback: function () {
        // certificates list received
        // for example show dropdown if multiple certificates received
        var certsList = $(ui.controls.certificateList).find('option');
        if (certsList.length > 0) {
            if (certsList.length > 1) {
                $(ui.controls.certificateList).closest('.block').show();
            } else {
                $(ui.controls.certificateList).closest('.block').hide();
            }
        }

        // or enable sign button
        $('#sign-button').removeAttr('disabled');
    },

    key: function () {
        return this.controls.keyList.val();
    },

    addKey: function (keyId, label) {
        this.controls.keyList.append($("<option>", {
            'value': keyId
        }).text(label));
    },

    refreshKeyList: function (keys) {
        this.clearKeyList();
        if (keys.length != 0)
            for (var d in keys) this.addKey(keys[d]);
        else this.controls.keyList.append($("<option>").text("Ключи на устройстве отсутствуют"));
    },

    clearKeyList: function (message) {
        this.controls.keyList.empty();
        if (message) this.controls.keyList.append($("<option>").text(message));
    },

    enumerateKeysCallback: function () {
        var keysList = $(ui.controls.keyList).find('option');
        if (keysList.length > 0) {
            if (keysList.length > 1) {
                $(ui.controls.certificateList).closest('.block').show();
            } else {
                $(ui.controls.certificateList).closest('.block').hide();
            }
        }
    },

    signMessage: function (message, options, callback) {
        let dataFormat = 0; //DATA_FORMAT_PLAIN;
        if (typeof options == 'undefined' || options == null) {
            var options = {};
            options.addSignTime = true;
            options.useHardwareHash = true;
            options.detached = false; // for login action set true
            options.addUserCertificate = true;
            options.addSystemInfo = false;
        }

        try {
            console.log(ui.certificate());
            plugin.pluginObject.sign(ui.device(), ui.certificate(), message, dataFormat, options).then($.proxy(function (res) {
                callback({
                    'status': 'OK',
                    'message': res
                });

            }, this), function (e) {
                callback({
                    'status': 'ERROR',
                    'message': e
                })
            });
        } catch (error) {
            console.log(error);
        }
    }
};

window.onload = function () {
    rutoken.ready.then(function () {
        ui = new rutokenUi();

        $('#sign-button').click(function () {
            ui.signMessage($('#message').val(), null, function (result) {
                if (result.status === 'OK') {
                    var fileDigestHash = result.message;
                    var subApplicationId = $('#subapplicationId').val();
                    var digitalSignatureId = $('#digitalSignatureId').val();
                    var buttonMapping = $('#buttonMapping').val();
                    var module = $('#module').val();

                    if (typeof module != 'undefined' && module === 'SAF') {
                        var URL = '/single-application/sign-digital-signature';
                    } else {
                        var URL = '/user-documents/sign-digital-signature';
                    }

                    $.ajax({
                        type: 'POST',
                        url: $cjs.admPath(URL),
                        data: {
                            fileDigestHash: fileDigestHash,
                            digitalSignatureId: digitalSignatureId,
                            subApplicationId: subApplicationId
                        },
                        success: function (result) {
                            if (result.status === 'INVALID_DATA') {
                                alertMessage($trans('swis.user_documents.digital_signature_sign.success'), 'success')
                                setTimeout(function () {
                                    $('#rutoken-modal').modal('hide')
                                    $('#needDigitalSignature').val(false);
                                    $(buttonMapping).attr('data-signature', false);
                                    $(buttonMapping).trigger('click');
                                }, 500);
                            }
                        }
                    });
                } else {
                    let errorCode = getErrorCode(result.message);
                    if (errorCode == plugin.errorCodes.USER_NOT_LOGGED_IN) {
                        $('#device-pin').closest('.block').show();
                    }
                    ui.printError(result.message);
                }
            });
        });

        var isChrome = !!window.chrome;
        var isFirefox = typeof InstallTrigger !== 'undefined';
        var verOffset, fullVersion, majorVersion;
        var performCheck = true;
        if ((verOffset = navigator.userAgent.indexOf('Firefox')) != -1) {
            fullVersion = navigator.userAgent.substring(verOffset + 8);
            majorVersion = parseInt('' + fullVersion, 10);
            if (majorVersion < 53) { // Don't check on ESR and older ones
                performCheck = false;
            }
        }

        isNmPlugin = true;
        if (performCheck && (isChrome || isFirefox)) {
            return rutoken.isExtensionInstalled();
        } else {
            isNmPlugin = false;
            return Promise.resolve(true);
        }
    }).then(function (result) {
        if (result) {
            return rutoken.isPluginInstalled();
        } else {
            disableModalInputs();
            throw "Расширение \"Адаптер Рутокен Плагин\" не установлено";
        }
    }).then(function (result) {
        if (result) {
            return rutoken.loadPlugin();
        } else {
            disableModalInputs();
            throw "Рутокен Плагин не установлен";
        }
    }).then(function (plugin) {
        onPluginLoaded(plugin);
    }).then(undefined, function (reason) {
        showRutokenError(reason);
    });
}