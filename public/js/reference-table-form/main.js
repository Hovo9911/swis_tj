var $refId = $('.mc-form-key').val();
var importErrorMessages = $('#importErrorMessages');

/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        "order": [[1, "desc"]],
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
        lengthChange: true,
    },

    customOptions: {
        afterInitCallback: function () {},
        tableDataPath: '/reference-table-form/' + $refId + '/table',
        searchPath: 'reference-table-form/' + $refId,
        actions: {
            edit: {
                render: function (row) {
                    if ($userHasAccess) {
                        return '<a href="' + $cjs.admPath("/reference-table-form/" + $refId + "/" + row.id + "/edit") + '"  class="btn btn-edit" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.edit.button') + '"><i class="fa fa-pen"></i></a>'
                    }
                }
            },
            view: {
                render: function (row) {
                    return '<a href="' + $cjs.admPath("/reference-table-form/" + $refId + "/" + row.id + "/view") + '"  class="btn btn-view" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.view.button') + '"><i class="fa fa-eye"></i></a>'
                }
            }
        },
    }
};

/*
 * Options for Form Request
 */
var optionsForm = {
    deletePath: '/reference-table-form/' + $refId + '/delete',
    searchPath: '/reference-table-form/' + $refId,
    addPath: '/reference-table-form/' + $refId + '/create',
};

/*
 * Options for Form Request
 */
var optionsFormImport = {
    customSaveFunction: function () {
        loadContent();
        importErrorMessages.html('');
        return true;
    },
    customOptions: {
        onSaveError: function (errors) {

            var messageStr = '';
            $.each(errors, function (k, value) {
                messageStr += '<div class="alert alert-danger">';
                messageStr += '<h4>' + value.rowInfo + '</h4>';

                messageStr += '<ul>';
                $.each(value.errorMessages, function (k, message) {
                    messageStr += '<li>' + message + '</li>'
                })
                messageStr += '</ul>';

                messageStr += '</div>';
            });

            importErrorMessages.removeClass('hide');
            importErrorMessages.html(messageStr);

            loadContent();
        }
    }
};

/*
 * Init Datatable, Form
 */
var $referenceTableForm = new FormRequest('form-data', optionsForm);
var $referenceTableFormImport = new FormRequest('form-data-import', optionsFormImport);
var $referenceTable = new DataTable('list-data', optionsTable);

$(document).ready(function () {

    // init
    $referenceTable.init();
    $referenceTableForm.init();
    $referenceTableFormImport.init();

    // ------------

    disableFieldsIfReferenceInactive();

    autocomplete();

    duplicateMlInfo();

});

function autocomplete() {

    var autoComplete = $('input.autocomplete');

    if ($(autoComplete).length > 0) {

        autoComplete.each(function (k, v) {

            var self = $(this);
            var autocompleteSelectedId = self.closest('div').find('input[name=' + self.data('name') + '][type="hidden"]');
            var descriptionTextArea = self.closest('div').find('textarea');

            self.keyup(function () {
                if (self.val() === '') {
                    autocompleteSelectedId.val('');
                    descriptionTextArea.val('')
                }
            });

            self.typeahead({
                minLength: 1,
                source: function (query, process) {
                    return $.post($cjs.admPath("/reference-table-form/" + $refId + "/autocomplete"), {
                        query: query,
                        r: self.data('source'),
                    }, function (response) {
                        return process(response.items);
                    });
                },

                afterSelect: function (data) {
                    autocompleteSelectedId.val(data.id);
                },
            });
        })
    }
}

function disableFieldsIfReferenceInactive() {

    if (typeof referenceTableStatus != 'undefined' || typeof referenceParentStatus != 'undefined') {
        if (referenceTableStatus === '2' || referenceParentStatus === '2') {
            disableFormInputs($('.tab-content'), true);
        }
    }
}

function duplicateMlInfo() {
    $(".duplicate-info-btn").click(function () {

        var self = $(this);
        var toLngCode = self.data('to-lang-code');
        var currentTabData = self.closest('.tab-pane').find(':input');
        var toTabData = $('#tab-' + toLngCode);

        self.prop('disabled', true)

        $.each(currentTabData, function (k, v) {
            var inputName = $(this).attr('name');
            var inputVal = $(this).val();

            if (inputName) {
                var toInputName = inputName.replace(self.data('current-lang-id'), self.data('to-lang-id'));

                toTabData.find('input[name="' + toInputName + '"]').val(inputVal);
                self.prop('disabled', false)
            }
        });
    });
}
