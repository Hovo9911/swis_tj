
/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        drawCallback: function () {
            $(".btn-remove").click(function () {
                $paymentProviders.deleteRow([$(this).data('id')])
            });
        },
        "order": [[2, "desc"]],
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
            $('td:eq(1)', nRow).addClass('text-center');
        },
    },
    customOptions: {
        afterInitCallback: function () {
        },
        tableDataPath: 'payment-providers/table',
        searchPath: 'payment-providers',
        actions: {
            edit: {
                render: function (row) {
                    if (row.canEdit !== false) {
                        return '<a href="' + $cjs.admPath('payment-providers/' + row.id + '/edit') + '" class="btn btn-edit" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.edit.button') + '"><i class="fa fa-pen"></i></a>'
                    }
                }
            },
            view: {
                render: function (row) {
                    return '<a href="' + $cjs.admPath('payment-providers/' + row.id + '/view') + '" class="btn btn-view" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.view.button') + '"><i class="fa fa-eye"></i></a>'
                }
            },
            custom: {
                render: function (row) {
                    return '<button class="btn-remove" data-id="' + row.id + '" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.delete.button') + '"><i class="fa fa-times"></i></button>';
                }
            }
        }
    }
};

/*
 * Options for Form Request
 */
var optionsForm = {
    deletePath: '/payment-providers/delete',
    searchPath: '/payment-providers',
    addPath: '/payment-providers/create',
};

/*
 * Init Datatable, Form
 */
var $paymentProvidersTable = new DataTable('list-data', optionsTable);
var $paymentProviders = new FormRequest('form-data', optionsForm);

$(document).ready(function () {

    // init
    $paymentProvidersTable.init();
    $paymentProviders.init();

});
