/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        drawCallback: function () {
            $('.data-table').show();
        },
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
        "order": [[1, "desc"]],
        lengthChange: true,
        iDisplayLength: 25
    },

    customOptions: {
        afterInitCallback: function () {
        },
        tableDataPath: 'laboratory-indicator/table',
        searchPath: 'laboratory-indicator',
        columnsRender: {
            date_time: {
                render: function (row) {
                    if (row.date_time) {
                        return $trans('swis.laboratory_indicator.' + row.date_time + '.label')
                    }
                    return ''
                }
            }
        }
    }
};

/*
 * Options for Form Request
 */
var addPath = '/laboratory-indicator/create';
var laboratoryList = $('.laboratory-list');

var optionsForm = {
    searchPath: '/laboratory-indicator',
    addPath: addPath,
};

/*
 * Init Datatable, Form
 */
var $laboratoryIndicatorTable = new DataTable('list-data', optionsTable);
var $laboratoryIndicatorForm = new FormRequest('form-data', optionsForm);

$(document).ready(function () {

    // init
    $laboratoryIndicatorForm.init();

    // -------------

    indexPageSelectLaboratoryList();

});

function indexPageSelectLaboratoryList() {

    laboratoryList.val('').trigger('change');

    laboratoryList.on('change', function () {

        if ($(this).val()) {
            optionsForm.addPath = addPath + '/' + $(this).val();
            $('input[name="laboratory_id"]').val($(this).val())
        }

        if (!$.fn.DataTable.isDataTable('#list-data')) {
            $laboratoryIndicatorTable.init();
        } else {
            $laboratoryIndicatorTable.reload();
        }

    });

    if (sessionStorage.getItem('labId')) {

        if ($('#list-data').length > 0) {
            if (!$.fn.DataTable.isDataTable('#list-data')) {
                $laboratoryIndicatorTable.init();
            } else {
                $laboratoryIndicatorTable.reload();
            }

            sessionStorage.clear('labId')
        }
    }
}



