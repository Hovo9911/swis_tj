$(document).ready(function () {

    markAsReadAllUnreadNotifications();

    notificationUnreadButtonClick();

    notificationMarkAsReadByURL()

});

function markAsReadAllUnreadNotifications() {

    var markAsReadAllUnreadNotificationsBtn = $('.markAsReadAllUnreadNotifications');

    markAsReadAllUnreadNotificationsBtn.click(function () {

        var self = $(this);
        self.prop('disabled', true);
        var unreadButton = $('.unread-button');
        var notificationCountDiv = $('.notifications-count');
        var notificationCountText = $('.notifications-count-text');

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('/notifications/mark-as-read'),
            success: function (response) {

                if (response.status === 'OK') {

                    notificationCountDiv.addClass('hide');
                    notificationCountText.addClass('hide');
                    markAsReadAllUnreadNotificationsBtn.addClass('hide');
                    unreadButton.addClass('hide');

                } else {
                    self.prop('disabled', false);
                }
            }
        });

    });
}

function notificationUnreadButtonClick() {
    $(".notification__unread-button").on("click", function (e) {
        e.preventDefault();

        var notification_id = $(this).data('notification_id');

        if (notification_id !== undefined) {
            updateNotification(notification_id);
        }

    });
}

//if clicked saf or sub-application URL mark notification as read
function notificationMarkAsReadByURL() {
    $(".mark-as-read").on("click", function (e) {

        e.preventDefault();

        var notification_id = $(this).closest('div').find('button').data('notification_id');

        if (notification_id !== undefined) {
            updateNotification(notification_id);
        }

        window.open($(this).attr('href'));
    });
}


