/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        fnRowCallback: function (nRow) {
            $('td:eq(0)', nRow).addClass('text-center');
        },
        "order": [[1, "desc"]],
        lengthChange: true,
        iDisplayLength: 25
    },

    customOptions: {
        afterInitCallback: function () {
        },
        tableDataPath: 'laboratory/table',
        searchPath: 'laboratory',
        columnsRender: {
            logo: {
                render: function (row) {
                    if (row.logo) {
                        return '<img width="50px" style="max-height: 30px;" src="/images/laboratory/' + row.logo + '" />';
                    }
                    return ''
                }
            }
        }
    }
};

/*
 * Options for Form Request
 */
var optionsForm = {
    searchPath: '/laboratory',
    addPath: '/laboratory/create',
};

/*
 * Init Datatable, Form
 */
var $laboratoryTable = new DataTable('list-data', optionsTable);
var $laboratory = new FormRequest('form-data', optionsForm);

//
var mainIndicatorsTable = $('.mainIndicatorsTable');
var indicatorsTableBlock = $('.indicatorsTableBlock');
var chooseLabAlert = $('#chooseLabAlert');
var taxIdSelect = $('#taxID');

$(document).ready(function () {

    // init
    $laboratoryTable.init();
    $laboratory.init();

    // -----------------

    laboratory();

});

// NOT USED FOR NOW
function getAgencyUsersList() {

    var userListSelect = $('.users-list');

    $('.agency-list').change(function () {

        var self = $(this);

        userListSelect.find('option')
            .remove()
            .end();

        userListSelect.select2("val", "");

        if (self.val()) {
            $.ajax({
                type: 'POST',
                url: $cjs.admPath('laboratory/get-agency-users'),
                data: {tax_id: self.val()},
                dataType: 'json',
                success: function (result) {

                    if (result.status === 'OK') {
                        if (result.data.users.length) {

                            var listOptions = [];
                            $.each(result.data.users, function (key, user) {
                                listOptions.push(new Option(user.name, user.id, false, false));
                            });

                            userListSelect.append(listOptions);
                        }
                    }
                }
            });
        }
    })
}

function laboratory() {

    var modal = $('#addIndicatorsModal');

    modal.on('hidden.bs.modal', function () {
        indicatorsTableBlock.html('');

        $('#searchSphereForm').trigger("reset");
    });

    taxIdSelect.on('change', function (e) {
        e.preventDefault();
        getLabInfo();
    });


    $('#agencyId').on('change', function (e) {
        e.preventDefault();

        var agency = $(this).val();

        taxIdSelect.html('').addClass('readonly');

        getLabInfo();

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('laboratory/get-agency-available-labs'),
            data: {agency: agency},
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    taxIdSelect.removeClass('readonly');

                    $.each(result.data, function (index, value) {

                        taxIdSelect.append($("<option></option>")
                            .attr("value", value.value)
                            .text(value.name));
                    });
                }
            }
        });
    });

    $('.addNewIndicators').on('click', function (e) {
        e.preventDefault();
        modal.modal('show');
    });

    $('.searchExpertise').on('click', function (e) {

        e.preventDefault();

        var self = $(this);
        var agencyId = $('#agencyId').val();
        var labId = taxIdSelect.val();

        chooseLabAlert.hide();
        indicatorsTableBlock.html('');

        self.prop('disabled', true);

        var data = $('#searchSphereForm').serializeArray().reduce(function(obj, item) {
            obj[item.name] = item.value;
            return obj;
        }, {});

        data['lab'] = labId;
        data['agency'] = agencyId;

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('laboratory/get-indicators'),
            data: data,
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {

                    self.prop('disabled', false);

                    indicatorsTableBlock.append(result.data);

                }
            },
            error: function(error){
                self.prop('disabled', false);

                chooseLabAlert.show();
            }

        });
    });

    $('.addIndicators').on('click', function (e) {

        e.preventDefault();

        var data = [];

        $(".lab_indicator_checkbox:checked").each(function (index, value) {
            data.push($(value).data('id'));
        });

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('laboratory/save-indicators'),
            data: {indicators: data, agency: $('#agencyId').val(), lab: taxIdSelect.val()},
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {

                    mainIndicatorsTable.html('').append(result.data);
                    indicatorsTableBlock.html('');

                    modal.modal('hide');
                }
            }
        });
    });

    $(document).on('click', '.lab_indicator_checkbox', function () {
        var is_checked = true;

        $(".lab_indicator_checkbox:checked").each(function (index, value) {
            is_checked = false;
        });

        $('.addIndicators').prop('disabled', is_checked);
    });

    $(document).on('click', '.remove-indicators-btn', function (e) {
        e.preventDefault();

        var id = $(this).data('id');
        var self = $(this);

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('laboratory/delete-indicators'),
            data: {indicator: id},
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {
                    self.closest('tr').remove();
                }
            }

        });
    });
}

function getLabInfo() {
    var taxId = taxIdSelect.val();

    if (taxId) {
        $.ajax({
            type: 'POST',
            url: $cjs.admPath('laboratory/get-lab-data'),
            data: {tax_id: taxId, agency: $('#agencyId').val()},
            dataType: 'json',
            success: function (result) {
                if (result.status === 'OK') {

                    $.each(result.data.info, function (key, value) {
                        $('input[name ='+key+']').val(value);
                    })

                    mainIndicatorsTable.html('').append(result.data.table);
                    indicatorsTableBlock.html('');
                }
            }
        });
    }
}
