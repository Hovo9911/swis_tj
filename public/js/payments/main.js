/*
 * Options Form DataTable
 */
var optionsTable = {
    datatableOptions: {
        searching: false,
        drawCallback: function () {
        },
        order: [[0, "desc"]],
        lengthChange: true,
        iDisplayLength: 25
    },
    customOptions: {
        afterInitCallback: function () {
        },
        tableDataPath: 'payments/table',
        searchPath: 'payments',
        columnsRender: {
            amount: {
                render: function (row) {
                    var symbol = (row.type == 'input') ? '+' : '';
                    var value = symbol + parseFloat(row.amount).toFixed(2);
                    var className = (row.type == 'input') ? 'text-navy' : 'text-danger';

                    if (row.type == 'output') {
                        return "<a href='" + $cjs.admPath('single-application/edit/' + row.safType + '/' + row.saf_number) + '#tab-responsibilities' + "'><p class='" + className + "'>" + value + " " + currency + "</p></a>";
                    } else {
                        return "<p class='" + className + "'>" + value + " " + currency + "</p>";
                    }
                }
            },
            service_provider_name: {
                render: function (row) {
                    return (row.service_provider_name === " - ") ? $trans('swis.core.single_window.title') : row.service_provider_name;
                }
            },
            obligation_type: {
                render: function (row) {
                    return (row.obligation_type.trim() === "()") ? '' : row.obligation_type;
                }
            },
            total_balance: {
                render: function (row) {
                    return parseFloat(row.total_balance).toFixed(2) + " " + currency
                }
            }
        }
    }
};

/*
 * Options for Form Request
 */
var optionsForm = {
    searchPath: '/payments',
};

/*
 * Init Datatable, Form
 */
var $paymentsTable = new DataTable('list-data', optionsTable);
var $payments = new FormRequest('form-data', optionsForm);

$(document).ready(function () {

    // init
    $paymentsTable.init();
    $payments.init();

    // -----------

    changeAmountFormat();
});

function changeAmountFormat() {
    $('#refile').on('click', function (event) {
        $(this).prop('disabled', true)
        event.preventDefault();

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('/payments/getDynamicParams'),
            data: {order_id: $('#orderId').val(), amount: $('#amountField').val()},
            dataType: 'json',
            success: function (result) {
                if (result.status == 'OK') {

                    $('#amountField').val(parseFloat($('#amountField').val()).toFixed(2))
                    $('#token').val(result.data.token);
                    $('#orderId').val(result.data.orderId);
                    $('#refile').prop('disabled', false);

                    $('#form-data-refile').submit();

                } else {
                    if (result.errors) {
                        $error.show('form-error', result.errors);
                    }
                }
            }
        });

    });

    $('#amountField').on('input', function () {
        if ($(this).val() > 0) {
            $('#refile').prop('disabled', false)
        } else {
            $('#refile').prop('disabled', true)
        }
    });
}