/*
 * Options Form DataTable Balance
 */
var optionsBalanceTable = {
    datatableOptions: {
        searching: false,
        "order": [[0, "asc"]],
        lengthChange: true,
        iDisplayLength: 25
    },
    customOptions: {
        afterInitCallback: function () {
        },
        tableDataPath: 'balance-operations/table',
        searchPath: 'balance-operations',
        columnsRender: {
            sum: {
                render: function (row) {
                    var renderSum = true;
                    if (typeof $onlyViewMode != 'undefined' && !parseInt($onlyViewMode)) {
                        renderSum = false;
                    }

                    if (renderSum) {
                        return '<div class="form-group">' +
                            '<input id="refund-' + row.id + '" data-balance="' + row.balance + '" type="number" min="0" oninput="validity.valid||(value=\'0\');" class="form-control refundInput">' +
                            '<div class="form-error" id="form-error-refund-' + row.id + '"></div>' +
                            '</div>';
                    } else {
                        return '';
                    }
                }
            },
            action: {
                render: function (row) {
                    var renderRefund = true;
                    if (typeof $onlyViewMode != 'undefined' && !parseInt($onlyViewMode)) {
                        renderRefund = false;
                    }

                    if (renderRefund) {
                        return '<button class="btn btn-danger refund btn-rounded" disabled data-id="' + row.id + '">' + $trans('swis.balance_operations.refund.button') + '</button>'
                    } else {
                        return '';
                    }
                }
            }
        }
    }
};

/*
 * Options Form DataTable Pays
 */
var optionsPaysTable = {
    datatableOptions: {
        searching: false,
        "order": [[0, "desc"]],
        lengthChange: true,
        iDisplayLength: 25
    },
    customOptions: {
        afterInitCallback: function () {
        },
        tableDataPath: 'balance-operations/pays-table',
        searchPath: 'balance-operations',
    }
};

/*
 * Options Form DataTable Transaction
 */
var optionsTransactionsTable = {
    datatableOptions: {
        searching: false,
        "order": [[0, "desc"]],
        lengthChange: true,
        iDisplayLength: 25
    },
    customOptions: {
        afterInitCallback: function () {
        },
        tableDataPath: 'balance-operations/transactions-table',
        searchPath: 'balance-operations',
    }
};

// Init Datatable, Form
var $balanceTable = new DataTable('balance-data', optionsBalanceTable);
var $paysTable = new DataTable('pays-data', optionsPaysTable);
var $transactionsTable = new DataTable('transactions-data', optionsTransactionsTable);

//
var refundModal = $('#refundModal');

//
$paysTable.mSearchFilterObject = $('#search-filter-pays');
$transactionsTable.mSearchFilterObject = $('#search-transactions-filter');

$(document).ready(function () {

    // init
    $balanceTable.init();
    $paysTable.init();
    $transactionsTable.init();

    // -------

    refund();

});

function refund() {

    //
    $(document).on('input', '.refundInput', function () {
        var buttonRefund = $(this).closest('tr').find('.refund');

        if ($(this).val() > 0 && $(this).val() <= parseInt($(this).data('balance'))) {
            buttonRefund.prop('disabled', false);
        } else {
            buttonRefund.prop('disabled', true);
        }
    });

    //
    $(document).on('click', '.refund', function () {

        var self = $(this);
        var selfFormGroup = self.closest('tr').find('.form-group')

        self.prop('disabled', true);

        refundModal.html('');

        $.ajax({
            type: 'POST',
            url: $cjs.admPath('/balance-operations/get-company-balance'),
            data: {id: self.data('id'), val: selfFormGroup.find('.refundInput').val()},
            success: function (response) {

                if (response.status === 'OK') {
                    refundModal.html(response.data.modal);
                    refundModal.modal('show');
                } else {
                    selfFormGroup.addClass('has-error');
                }

                self.prop('disabled', false);
            }
        });
    });

    //
    $(document).on('click', '.refundConfirm', function (event) {
        $.ajax({
            type: 'POST',
            url: $cjs.admPath('balance-operations/confirm-refund'),
            data: {refund: $('#refundHiddenData').val(), id: $('#idHiddenData').val()},
            success: function (result) {
                if (result.status === 'OK') {

                    var backToUrl = result.data.backToUrl;
                    if (backToUrl) {
                        window.location.href = backToUrl;
                        if (backToUrl.indexOf('#') !== -1) {
                            location.reload();
                        }
                    }

                } else {

                    refundModal.modal('hide');
                    toastr.error($trans.get('core.loading.invalid_data'));
                }
            }
        });
    });
}