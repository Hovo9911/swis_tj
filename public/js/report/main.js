/*
 * Options for Form Request
 */
optionsForm = {
    customOptions: {
        onSaveSuccess: function (data) {
            $('#reportResult').html(data.html)

            var downloadFile = $('#downloadFile');

            if (downloadFile.length) {
                downloadFile.get(0).click();
            }
        },
    }
};

/*
 * Init Form
 */
var $report = new FormRequest('form-data', optionsForm);

//
var $webPrefix = 'report';
var agencySelect = $('.agency-list');
var documentSelect = $('.document-list');
var documentStatusSelect = $('.document-status');
var documentSubdivisionsSelect = $('.document-subdivisions');

$(document).ready(function () {

    // init
    $report.init();

    // -----------------

    documentSelectChange();

    getConstructorDocumentsForAgency();

    datePickerOnlyYear();

});

function documentSelectChange() {

    documentSelect.change(function () {

        // States
        getDocumentStatuses($(this));

        // SubDivisions
        getDocumentSubdivisions($(this));

    });
}

function getDocumentStatuses(self) {

    var allStatuses = self.attr('data-statuses');

    select2Reset(documentStatusSelect, true);
    documentStatusSelect.prop('disabled', true);

    if (self.val()) {

        // Document is more one close getting states
        if ($.isArray(self.val()) && self.val().length != 1) {
            return;
        }

        $.ajax({
            url: $cjs.admPath($webPrefix + '/document-statuses/' + self.val()),
            type: 'post',
            dataType: 'json',
            data: {allStatuses: allStatuses},
            success: function (result) {
                if (result.status === 'OK') {
                    if (result.data) {
                        $.each(result.data.statuses, function (k, v) {
                            documentStatusSelect.append($("<option></option>")
                                .attr("value", k)
                                .text(v));
                        })
                    }

                    documentStatusSelect.prop('disabled', false);
                }

            },
        })
    } else if (typeof documentStatusSelect.val() != 'undefined') {
        select2Reset(documentStatusSelect, true);
        documentStatusSelect.prop('disabled', true);
    }
}

function getDocumentSubdivisions(self) {

    if (typeof getSubdivisions != 'undefined') {

        select2Reset(documentSubdivisionsSelect);
        documentSubdivisionsSelect.prop('disabled', true);

        if (self.val().length) {
            $.ajax({
                url: $cjs.admPath($webPrefix + '/document-subdivisions'),
                type: 'post',
                dataType: 'json',
                data: {document_id: self.val()},
                success: function (result) {

                    if (result.status === 'OK') {
                        if (result.data) {
                            $.each(result.data.subdivisions, function (k, v) {
                                documentSubdivisionsSelect.append($("<option></option>")
                                    .attr("value", k)
                                    .text(v.name));
                            })
                        }
                    }

                    documentSubdivisionsSelect.prop('disabled', false);
                },
            })
        }

    }
}

function getConstructorDocumentsForAgency() {

    agencySelect.change(function (e) {

        var companyTaxId = $(this).val();

        select2Reset(documentSelect);
        select2Reset(documentStatusSelect);
        documentSelect.prop('disabled', true);
        documentStatusSelect.prop('disabled', true);

        if (companyTaxId) {
            $.ajax({
                url: $cjs.admPath($webPrefix + '/constructor-documents'),
                type: 'post',
                dataType: 'json',
                data: {company_tax_id: companyTaxId},
                success: function (result) {
                    if (result.status === 'OK') {
                        if (result.data) {

                            $.each(result.data.documents, function (k, v) {
                                documentSelect.append($("<option></option>")
                                    .attr("value", v.id)
                                    .text(v.document_name));
                            });

                        }
                    }

                    documentSelect.prop('disabled', false);
                },
            });
        }
    });
}

function datePickerOnlyYear() {
    $('.onlyYear').datepicker({
        minViewMode: 2,
        format: 'yyyy'
    });

    $('.onlyMonth').datepicker({
        format: 'mm',
        minViewMode: "months"
    });
}