if (typeof $legalEntityValues != 'undefined') {

    var legalEntitySelects = $('.legalEntityType');

    var typeTaxId = 'tax_id';
    var typeIdCard = 'id_card';
    var foreignCitizen = 'f_citizen';
    var typeTaxIDValue, typeIdCardValue, foreignCitizenValue;

    typeTaxIDValue = $legalEntityValues[typeTaxId];
    typeIdCardValue = $legalEntityValues[typeIdCard];
    foreignCitizenValue = $legalEntityValues[foreignCitizen];

    // Init Part
    $(function () {

        initTaxIdPassportType();
        byTinGetInfo();

    });

    //
    function initTaxIdPassportType() {

        $.each(legalEntitySelects, function (k, v) {
            byTypeModifyColumns($(this));
        });

        //
        legalEntitySelects.change(function () {

            var panelBody = $(this).closest('.panel-body');

            panelBody.find(':input').not($(this)).val('');
            panelBody.find('select').not($(this)).val(null).trigger("change");

            byTypeModifyColumns($(this));

        });
    }

    //
    function byTypeModifyColumns(currentLegalEntityTypeSelect) {

        //
        var currentValue = currentLegalEntityTypeSelect.val();
        var parentDiv = currentLegalEntityTypeSelect.closest('.panel-body');
        var selfNaturalPersonFieldsFormGroup = parentDiv.find('.natural-person-field ,.passportField').closest('.form-group');
        var selfLegalPersonFieldsFormGroup = parentDiv.find('.legal-person-field').closest('.form-group');

        //
        var foreignCitizenInputFields = parentDiv.find('.f-citizen').not('select');
        var foreignCitizenSelectFields = parentDiv.find('select.f-citizen');

        //
        if (currentLegalEntityTypeSelect.is(":not(:disabled)")) {

            if (currentValue == foreignCitizenValue && !currentLegalEntityTypeSelect.is('[readonly]')) {
                foreignCitizenInputFields.removeAttr('readonly');
                foreignCitizenSelectFields.removeClass('readonly');
            } else if(typeof currentValue != 'undefined') {
                foreignCitizenInputFields.attr('readonly', 'readonly');
                foreignCitizenSelectFields.addClass('readonly');
            }
        }

        // by type open or close fields
        switch (parseInt(currentValue)) {
            case typeTaxIDValue:
                selfNaturalPersonFieldsFormGroup.addClass('hide');
                selfLegalPersonFieldsFormGroup.removeClass('hide');
                break;
            case typeIdCardValue:
                selfNaturalPersonFieldsFormGroup.removeClass('hide');
                selfLegalPersonFieldsFormGroup.addClass('hide');
                break;
            case  foreignCitizenValue:
                selfNaturalPersonFieldsFormGroup.removeClass('hide');
                selfLegalPersonFieldsFormGroup.removeClass('hide');
                break;
            default:
                selfNaturalPersonFieldsFormGroup.addClass('hide');
                selfLegalPersonFieldsFormGroup.addClass('hide');
        }
    }

    //
    function byTinGetInfo() {

        $('.getPartiesInfo').not('.passportField').on('input', function (e) {

            var self = $(this);
            var sendAjax = false;
            var parentDiv = $(this).closest('.panel-body');
            var legalEntityType = parentDiv.find('.legalEntityType');
            var passportField = parentDiv.find('.passportField');
            var taxIdSSNField = parentDiv.find('.taxIdOrSSN');
            var taxIdSSNFormGroup = taxIdSSNField.closest('.form-group');

            var legalEntityTypeVal = +$('select[data-type="' + taxIdSSNField.data('type') + '"]').val();
            var fieldsData = taxIdSSNField.data('fields');

            if (legalEntityType.length > 0 && legalEntityType.children("option:selected").val() == foreignCitizenValue) {
                return false;
            } else if ($(this).hasClass('readonly')) {
                return false;
            }

            // if need to get Company Info
            if (legalEntityTypeVal === typeTaxIDValue) {
                if (taxIdSSNField.val().length >= $taxIdMinLength) {
                    sendAjax = true;
                }
            }

            // if need to get User Info
            if (legalEntityTypeVal === typeIdCardValue) {
            // && passportField.val().length >= parseInt($passportMinLength)
                if (taxIdSSNField.val().length >= $ssnMinLength) {
                    sendAjax = true;
                }
            }

            if (fieldsData.length !== 0) {
                $.each(fieldsData, function (k, v) {
                    parentDiv.find('select[name="' + v.to + '"]').val(null).trigger("change");
                    parentDiv.find('input[name="' + v.to + '"]').val('');
                    parentDiv.find('textarea[name="' + v.to + '"]').val('');
                });
            }

            //
            if (sendAjax) {

                sendAjax = false;
                legalEntityType.prop('disabled', true);

                taxIdSSNFormGroup.removeClass('has-error');
                taxIdSSNFormGroup.find('.form-error').text('').removeClass('form-error-text');

                passportField.prop('disabled', true);
                spinnerLoadState(taxIdSSNField);

                $.ajax({
                    type: 'POST',
                    url: $cjs.admPath('entity/get-info'),
                    // , passport: passportField.val()
                    data: {type: legalEntityTypeVal, taxIdSSN: taxIdSSNField.val()},
                    dataType: 'json',
                    success: function (result) {

                        if (result.status === 'OK') {
                            var info = result.data.info;
                            if (fieldsData.length !== 0 && info !== undefined) {
                                $.each(fieldsData, function (k, v) {
                                    parentDiv.find('select[name="' + v.to + '"]').val(info[v.from]).trigger('change');
                                    parentDiv.find('input[name="' + v.to + '"]').val(info[v.from]);
                                    parentDiv.find('textarea[name="' + v.to + '"]').val(info[v.from]);
                                });
                            }
                        }

                        if (result.status === 'INVALID_DATA') {
                            taxIdSSNFormGroup.addClass('has-error');
                            taxIdSSNFormGroup.find('.form-error').text(result.errors.message).addClass('form-error-text').css('display', 'block');
                        }

                        sendAjax = true;
                        passportField.prop('disabled', false);
                        legalEntityType.prop('disabled', false);

                        spinnerLoadState(taxIdSSNField, true);

                        setTimeout(function () {
                            self.focus();
                        }, 200)
                    }
                });
            }
        });
    }
}