<?php
$data = parse_ini_file('../.env');

$link = pg_connect("host=".$data['DB_WRITE_HOST']." dbname=".$data['DB_DATABASE']." user=".$data['DB_USERNAME']." password=".$data['DB_PASSWORD']."");
if (!$link) {
    echo 'DOWN';
    die();
}

$result = pg_query($link, "SELECT 1");
if (!$result) {
    echo 'DOWN';
    die();
}

echo 'OK';