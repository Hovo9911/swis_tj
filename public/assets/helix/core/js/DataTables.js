function DataTable(formObjectId, options) {

    this.mTable = null;
    this.mFormObject = null;
    this.mForm = null;
    this.mFormObjectId = formObjectId;

    this.mSearchFilterObject = null;
    this.mSearchFilterForm = null;
    this.mMainSearchFilterId = 'search-filter';
    this.mSearchFilterId = 'search-filter-main';

    this.mColumns = [];
    this.mOptions = options;
}

DataTable.prototype = {

    /**
     * Initialize
     */
    init: function () {

        if(this.mSearchFilterObject){
            this.mMainSearchFilterId = this.mSearchFilterObject.attr('id');
        }

        var hiddenFormHtml = '<form class="form-horizontal fill-up dn" id="'+this.mMainSearchFilterId+'-main"></form>';
        var mainSearchFilter = $('#' + this.mMainSearchFilterId);
        var defaultSearchParams = mainSearchFilter.find('.default-search');

        mainSearchFilter.parent().append(hiddenFormHtml);

        if (defaultSearchParams.length) {
            var searchDefaultFieldsHtml = '';

            $.each(defaultSearchParams, function (k, v) {
                searchDefaultFieldsHtml += '<input type="hidden" name="' + $(this).attr('name') + '" value="' + $(this).val() + '" />'
            });

            $('#' + this.mSearchFilterId).html(searchDefaultFieldsHtml);
        }

        var self = this;
        mainSearchFilter.submit(function () {
            var data = $('#' + self.mMainSearchFilterId).serializeArray();

            var searchFieldsHtml = '';
            for (var i in data) {
                var dataVal = data[i].value;
                // dataVal = dataVal.replace('"', "");

                searchFieldsHtml += '<input type="hidden" name="' + data[i].name + '" value="' + dataVal + '" />';
            }

            $('#' + self.mSearchFilterId).html(searchFieldsHtml);
            $('#' + self.mSearchFilterId).submit();

            /* if($('#list-data_wrapper').length){
                 $('html, body').animate({
                     scrollTop: $('#list-data_wrapper').offset().top
                 }, 800);
             }*/
            return false;
        });

        toastr.options.preventDuplicates = true;

        this.mForm = this._getForm();

        if (this.mForm) {

            var dataTableOptions = this._getDefaultOptions();
            dataTableOptions = Object.assign(dataTableOptions, this.mOptions.datatableOptions);

            if(this.mForm.find('thead th').length){
                this.mTable = this.mForm.DataTable(dataTableOptions);
            }

            if (this.mSearchFilterForm) {

                this.mSearchFilterForm.submit(function (e) {

                    e.preventDefault();

                    self._getMainSearchForm().find('button').prop('disabled', true);
                    self.mTable.ajax.reload();

                });

                this.mSearchFilterForm.find("select[data-type='real-time']").change(function (e) {

                    e.preventDefault();

                    self.mTable.ajax.reload();

                    toastr.success($trans.get('core.loading.success'));

                });
            }


            this._actions()
        }

        if (this.mTable) {
            this._startListenToWindowResizeAndRecalculateColumns();
            this._startListenToWrapperSizeChangedEvent();
        }

        return null;
    },

    reload: function () {
        this.mTable.ajax.reload();
    },

    _startListenToWindowResizeAndRecalculateColumns: function () {
        var self = this;
        $(window).resize(function () {
            self.mTable.columns.adjust();
        })
    },

    _startListenToWrapperSizeChangedEvent: function () {
        var self = this;
        window.addEventListener('wrapper-size-changed', function (e) {
            setTimeout(function () {
                self.mTable.columns.adjust();
            }, 0)
        }, false);

    },

    _buildSearchData: function (d) {


        if (this.mSearchFilterForm) {

            var values = [];
            var formInputs = this.mSearchFilterForm.find("input").not('select2');

            if (formInputs) {

                formInputs.each(function () {
                    if ($(this).val() && $(this).attr("name")) {
                        // values.push({
                        //     name: $(this).attr("name"),
                        //     val: $(this).val()
                        // })
                        var name = $(this).attr("name");

                        if (values.hasOwnProperty(name)) {
                            if (Array.isArray(values[name])) {
                                values[$(this).attr("name")].push($(this).val());
                            } else {
                                var tmpArray = [values[$(this).attr("name")]];
                                tmpArray.push($(this).val());
                                values[$(this).attr("name")] = tmpArray;
                            }
                        } else {
                            values[$(this).attr("name")] = $(this).val();
                        }
                    }

                });
            }

            var formSelects = this.mSearchFilterForm.find("select");

            if (formSelects.length) {

                formSelects.each(function () {

                    if ($(this).val() && $(this).attr("name")) {
                        // values.push({
                        //     name: $(this).attr("name"),
                        //     val: $(this).val()
                        // })

                        values[$(this).attr("name")] = $(this).val();
                    }

                });

            }

            d.f = Object.assign({}, values);

            // to make new search parameter
            if (this.mTable) {
                if (this.mTable.order().length) {
                    d.orderColumnName = this.mColumns[this.mTable.order()[0][0]].orderFieldName;
                }

            }

        }

    },

    _actions: function () {

        var self = this;

        self.mForm.closest('.dataTables_scroll').find('.dataTables_scrollHead thead').on('click', '.th-checkbox  input', function () {
            var checkboxes = self.mForm.find(':checkbox');
            checkboxes.prop('checked', $(this).is(':checked'));
        });

    },

    _getColumns: function () {
        var editBtn = null;
        var deleteBTn = null;
        var viewBTn = null;
        var customBTn = null;
        var cloneBTn = null;
        var self = this;
        var hiddenFields = JSON.parse(localStorage.getItem('hiddenParams' + self.mForm.data('moduleForSearchParams'))) || {
            'params': [],
            'columns': []
        };

        var closeActions = false;
        if (typeof $onlyViewMode != 'undefined' && !+$onlyViewMode) {
            closeActions = true;
        }

        $.each(self.mForm.find('th[data-col]'), function () {

            var column = $(this);
            column.attr('title', column.data('col-title') ? column.data('col-title') : column.text());
            if (hiddenFields.columns.includes($(this).data('col'))) {
                $(this).remove();
                return true;
            }
            switch ($(this).data('col')) {
                case 'check':

                    self.mColumns.push({
                        "targets": 0,
                        "data": null,
                        "orderable": false,
                        "sortable": false,
                        "render": function (data, type, row, meta) {
                            return '<input type="checkbox" value="' + row.id + '">'
                        }
                    });

                    break;

                case 'obligation-check':
                    var customValueKey = column.data('keyForValue');
                    self.mColumns.push({
                        "targets": 0,
                        "data": null,
                        "orderable": false,
                        "sortable": false,
                        "render": function (data, type, row, meta) {
                            if (row.payment == row.obligation) {
                                return '<i class="fas fa-check"></i>';
                            } else {
                                var addDisabled = '';
                                if (closeActions) {
                                    addDisabled = 'disabled';
                                }
                                return '<input type="checkbox" ' + addDisabled + ' class="obligation"  data-id="' + row.id + '" data-amount="' + row.obligation + '"/>';
                            }

                        }
                    });

                    break;

                case 'actions':

                    self.mColumns.push({
                        "targets": -1,
                        "data": null,
                        "orderable": false,
                        "render": function (data, type, row, meta) {
                            if (self.mOptions.customOptions.actions) {

                                if (!closeActions) {
                                    if (self.mOptions.customOptions.actions.edit) {
                                        editBtn = self.mOptions.customOptions.actions.edit.render(row)
                                    }

                                    if (self.mOptions.customOptions.actions.delete) {
                                        deleteBTn = self.mOptions.customOptions.actions.delete.render(row)
                                    }
                                }

                                if (self.mOptions.customOptions.actions.view) {
                                    viewBTn = self.mOptions.customOptions.actions.view.render(row)
                                }

                                if (!closeActions) {
                                    if (self.mOptions.customOptions.actions.custom) {
                                        customBTn = self.mOptions.customOptions.actions.custom.render(row)
                                    }

                                    if (self.mOptions.customOptions.actions.clone) {
                                        cloneBTn = self.mOptions.customOptions.actions.clone.render(row)
                                    }
                                }

                            } else {


                                var showEdtBtn = true;
                                if (column.data('edit') != undefined) {
                                    if (!column.data('edit')) {
                                        showEdtBtn = false;
                                    }

                                    if (column.data('edit')) {
                                        closeActions = false;
                                    }

                                }

                                if (showEdtBtn && !closeActions) {
                                    editBtn = '<a href="' + $cjs.admPath(self.mOptions.customOptions.searchPath + "/" + row.id + "/edit") + '" data-id="' + row.id + '" class="btn  btn-edit" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.edit.button') + '"><i class="fa fa-pen"></i></a>';
                                }

                                //
                                var showDltBtn = true;
                                if (column.data('delete') != undefined && !column.data('delete')) {
                                    showDltBtn = false;
                                } else if (self.mOptions.customOptions.hiddens !== undefined && self.mOptions.customOptions.hiddens.indexOf(row.id) >= 0) {
                                    showDltBtn = false;
                                }

                                if (showDltBtn && !closeActions) {
                                    deleteBTn = '<button class="btn-remove" data-id="' + row.id + '"><i class="fa fa-times"></i></button>';
                                }

                                //
                                var showViewButton = false;
                                if ((column.data('view') != undefined && column.data('view')) || typeof $onlyViewMode != 'undefined') {
                                    showViewButton = true;
                                }

                                if (showViewButton) {
                                    viewBTn = '<a href="' + $cjs.admPath(self.mOptions.customOptions.searchPath + "/" + row.id + "/view") + '" class="btn btn-view" data-id="' + row.id + '" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.view.button') + '"><i class="fa fa-eye"></i></a>';
                                }

                                if (customBTn && !closeActions) {
                                    viewBTn = '<a href="' + $cjs.admPath(self.mOptions.customOptions.searchPath + "/" + row.id + "/view") + '" class="btn btn-view" data-id="' + row.id + '" data-toggle="tooltip" title="' + $trans('swis.base.tooltip.view.button') + '"><i class="fa fa-eye"></i></a>';
                                }

                            }

                            deleteBTn = (deleteBTn != null) ? deleteBTn : '';
                            editBtn = (editBtn != null) ? editBtn : '';
                            viewBTn = (viewBTn != null) ? viewBTn : '';
                            cloneBTn = (cloneBTn != null) ? cloneBTn : '';
                            customBTn = (customBTn != null) ? customBTn : '';
                            return editBtn + '  ' + viewBTn + ' ' + cloneBTn + '  ' + customBTn + ' ' + deleteBTn;
                        }
                    });

                    break;

                default:

                    var $colName = $(this).data('col');

                    var orderingType = $(this).data('ordering');

                    var orderable = true;
                    if (orderingType !== undefined) {
                        orderable = orderingType
                    }

                    var orderFieldName = $(this).data('order-name');

                    self.mColumns.push({
                        data: $(this).data('col'),
                        orderable: orderable,
                        orderFieldName: orderFieldName,
                        "render": function (data, type, row, meta) {

                            if (self.mOptions.customOptions.columnsRender) {

                                if (typeof self.mOptions.customOptions.columnsRender[$colName] != 'undefined' && self.mOptions.customOptions.columnsRender[$colName]) {
                                    return self.mOptions.customOptions.columnsRender[$colName].render(row)
                                }
                            }

                            if ($colName == 'show_status') {

                                switch (row.show_status) {
                                    case '1':
                                        return '<span data-toggle="tooltip" title="' + $trans('core.base.label.status.active') + '" class="label label-primary">' + $trans('core.base.label.status.active') + '</span>';
                                    case '2':
                                        return '<span data-toggle="tooltip" title="' + $trans('core.base.label.status.inactive') + '" class="label label-danger">' + $trans('core.base.label.status.inactive') + '</span>';
                                    case '0':
                                        return '<span data-toggle="tooltip" title="' + $trans('core.base.label.status.deleted') + '" class="label label-danger">' + $trans('core.base.label.status.deleted') + '</span>';
                                }
                            }

                            return data;
                        }

                    });

                    break;
            }


        });

    },

    _getForm: function () {

        if (this.mFormObject === null) {

            if (this.mFormObjectId && $("#" + this.mFormObjectId).length > 0) {

                this.mFormObject = $("#" + this.mFormObjectId);
            } else {
                this.mFormObject = false;
            }
        }

        return this.mFormObject;
    },

    _getSearchForm: function () {

        if (this.mSearchFilterObject === null) {

            if (this.mSearchFilterId && $("#" + this.mSearchFilterId).length > 0) {

                this.mSearchFilterObject = $("#" + this.mSearchFilterId);
            } else {
                // this.mSearchFilterObject = false;
            }
        }

        return this.mSearchFilterObject;
    },

    _getMainSearchForm: function () {

        if (this.mMainSearchFilterId && $("#" + this.mMainSearchFilterId).length > 0) {
            return $("#" + this.mMainSearchFilterId);
        }

        return this.mSearchFilterObject;
    },

    _getDefaultOptions: function () {

        this._getColumns();

        var self = this;

        var tableDataPath = this.mOptions.customOptions.tableDataPath;
        var columns = this.mColumns;
        var initFirstTime = true;
        this.mSearchFilterForm = this._getSearchForm();

        var datatableOptions = {
            scrollX: true,
            scrollY: true,
            "ordering": true,
            "order": [[1, "desc"]],
            responsive: true,
            pageLength: 25,

            ajax: function (data, callback, settings) {
                self._buildSearchData(data);

                $.ajax({
                    "type": 'POST',
                    "url": $cjs.admPath(tableDataPath),
                    "data": data,
                    "success": function (response) {

                        if (typeof response.status != 'undefined' && response.status === 'INVALID_DATA') {
                            $error.show('form-error', response.errors, true);
                        } else {
                            if (!initFirstTime) {
                                // toastr.success($trans.get('core.loading.success'));
                            }

                            $error.show('form-error', [], true);
                            callback(response);


                            var animateToTable = true;
                            if (typeof self.mOptions.customOptions.animateToTable != 'undefined') {
                                animateToTable = self.mOptions.customOptions.animateToTable;
                            }

                            if (response.data.length && !initFirstTime && animateToTable) {
                                animateScrollToElement(self.mForm, 0, 200)
                            }

                            var mainWrapper = $('#' + self.mFormObjectId + '_wrapper');
                            if (!initFirstTime && mainWrapper.length) {
                                mainWrapper.find('.dataTables_scrollBody').animate({
                                    scrollTop: 0
                                }, 100);
                            }

                            initFirstTime = false;
                        }

                        if (self._getMainSearchForm()) {
                            self._getMainSearchForm().find('button').prop('disabled', false);
                        }
                    },
                });
            },

            /*ajax: {
                "url": $cjs.admPath(tableDataPath),
                "data": function (d) {

                    self._buildSearchData(d);

                    if (typeof self.mOptions.customOptions != 'undefined' && typeof self.mOptions.customOptions.buildCustomSearchData != 'undefined') {
                        self.mOptions.customOptions.buildCustomSearchData(d);
                    }
                },
                "success": function (response) {

                   var  myDataTable = $('#list-data').DataTable()

                },
            },*/

            searching: false,
            columns: columns,
            processing: true,
            serverSide: true,
            lengthChange: false,
            // scrollX: true,
            autoWidth: false,
            // deferRender: true,
            initComplete: function (settings, json) {
                // toastr.success($trans.get('core.loading.success'));
                if (typeof self.mOptions.customOptions != 'undefined' && typeof self.mOptions.customOptions.afterInitCallback != 'undefined') {
                    self.mOptions.customOptions.afterInitCallback(json);
                }

                self.mTable.columns.adjust();
            },

            oLanguage: $dataTableLanguages

        };

        return datatableOptions;
    }
};

$.fn.DataTable.ext.pager.numbers_length = 5;

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    $.fn.dataTable
        .tables({visible: true, api: true})
        .columns.adjust();
});


// Extend DataTable set Global configs
// $.extend(true, $.fn.dataTable.defaults, {
//     "ordering": true,
//     "order": [[1, "asc"]],
//     responsive: true,
//     pageLength: 25,
//     "initComplete": function (settings, json) {
//         toastr.success($trans.get('core.loading.success'));
//
//     }
//
// });