function FormRequest(formObjectId, options) {

    this.rFormObject = null;
    this.rFormErrorNamespace = 'form-error';
    this.rFormObjectId = formObjectId;
    this.rShowLoadingInvalidDataMessage = true;

    this.rSaveResponse = null;
    this.rTable = null;

    this.rMainButtons = null;
    this.rNavButtonsInited = false;
    this.rSaveBtn = null;

    // Delete Data variables
    this.rDeleteConfirmModalObj = null;
    this.rDeleteConfirmModalSelector = "#d-delete-confirm";
    this.rSelectedForDeleteItems = [];

    this.rDeleteEmptyConfirmModalObj = null;
    this.rDeleteEmptyConfirmModalSelector = "#d-delete-empty-message";
    this.rRedirectOnDelete = true;

    // options
    this.rOptions = options;
}


FormRequest.prototype = {

    /**
     * Initialize Run
     */
    initEditPage: function () {

        this.initForm();
        this.onInitEditPage && this.onInitEditPage();
    },

    initForm: function () {

        var formObj;
        var self = this;

        if (formObj = this._getForm()) {

            formObj.submit(
                function () {

                    self._save();
                    return false;
                }
            );

            formObj.append('<input type="submit" class="hidden-form-elem" value="." />');

            $('.nav-tabs a').on('click', function () {
                if ($(this).data('auto-focus') != 'off') {
                    setTimeout(function () {

                        var firstElement = $('.tab-pane.active .form-group:first input');

                        if(!firstElement.is('[disabled]') && !firstElement.is('[readonly]')){
                            firstElement.focusTextToEnd();
                        }

                    }, 10);
                }
            });
        }
    },

    init: function () {

        toastr.options.preventDuplicates = true;
        toastr.options.positionClass = 'toast-top-center';

        var self = this;
        $(document).ready(function () {
            self._initNavButtons();

            if ($("#" + self.rFormObjectId).length > 0) {
                self.initEditPage();
            }
            else {
                self.initSearchPage();
            }
        });

    },

    resetForm : function (){
        if(this._getForm()){
            this._getForm()[0].reset();
            this._getForm().find('.form-error ').text('');
            this._getForm().find('.form-group').removeClass('has-error')
        }
    },

    initSearchPage: function () {
    },

    /**
     * Save Data Part
     */
    _save: function () {

        $('.main-content').toggleClass('sk-loading');

        if (typeof tinymce != "undefined") {
            tinymce.triggerSave();
        }

        var self = this,
            formObj = this._getForm(),
            sendData,
            rPath,
            saved = false;

        // ClassicEditor
        if (typeof ClassicEditor != 'undefined') {
            $('textarea.ckeditor').each(function() {
                var self = $(this);
                self.val(CK_INSTANCES[self.attr('name')].getData());
            });
        }

        if (typeof self.rOptions != 'undefined' && typeof self.rOptions.customSaveFunction != 'undefined') {
            if (!self.rOptions.customSaveFunction()) {
                return false;
            }
        }

        if (!formObj) {
            throw new Error(" MC - Form Not Found  ");
        }

        if (this._isDisabled()) {
            return false;
        }

        this._disableButton();
        $cjs.processErrorClear();

        sendData = formObj.serializeArray();
        rPath = formObj.attr('action');

        $.each(sendData,function (k,v) {
            var currentInput = $('input[name="'+v.name+'"]');
            if(currentInput.length && currentInput.val() != ""){
               /* if(currentInput.closest('.input-group').hasClass('date')){
                    sendData[k]['value'] = moment(currentInput.val(),'DD/MM/YYYY').format("YYYY-MM-DD")
                }*/

                if(currentInput.closest('.input-group').hasClass('datetime')){
                    sendData[k]['value'] = moment(currentInput.val(),'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss")
                }
            }
        });

        $.ajax({
            type: 'POST',
            url: rPath,
            data: sendData,
            dataType: 'json',
            success: function (result) {

                if (self.rSaveResponse && saveResponse(result) === false) {
                    return false;
                }

                if (result.status == 'OK') {

                    self._showErrors(result);
                    if (typeof self.rOptions.addCallback != 'undefined') {
                        self.rOptions.addCallback();
                    }

                    $('.tj-login__error').css('display','none');

                    if (typeof self.rOptions != 'undefined' && typeof self.rOptions.customOptions != 'undefined' && typeof self.rOptions.customOptions.onSaveSuccess != 'undefined') {
                        self.rOptions.customOptions.onSaveSuccess(result.data);


                        if( typeof result.data != 'undefined'){
                            var backUrl = result.data.backToUrl;

                            if (!backUrl) {
                                self._enableButton();
                            }

                            return;
                        }else{
                            self._enableButton();
                        }
                    }

                    saved = true;
                    toastr.success($trans.get('core.loading.saved'));

                    if (result.data && result.data.backToUrl) {
                        var bactToUrl = result.data.backToUrl;

                        if (bactToUrl) {

                            window.location.href = bactToUrl;

                            if( bactToUrl.indexOf('#') !== -1 ){
                                location.reload();
                            }

                        }
                    } else {
                        self._backToSearch();
                    }

                } else if (result.status == 'INVALID_DATA') {
                    self._showErrors(result);

                    $('.tj-login__error').css('display','table');

                    if (self.rOptions.afterGetErrorResult !== undefined) {
                        self.rOptions.afterGetErrorResult(result.errors);
                    }

                    if(self.rOptions.rShowLoadingInvalidDataMessage){
                        toastr.error($trans.get('core.loading.invalid_data'));
                    }

                    if (typeof self.rOptions != 'undefined' && typeof self.rOptions.customOptions != 'undefined' && typeof self.rOptions.customOptions.onSaveError != 'undefined') {
                        self.rOptions.customOptions.onSaveError(result.errors);
                    }

                    if (typeof result.errors != 'undefined' && typeof result.errors.taxService != 'undefined') {
                        showAlertsMessages.html('<div class="alert alert-danger">' + result.errors.message + '</div>');
                        animateScrollToElement(showAlertsMessages, 50, 1);
                    }

                } else {
                    self._processError(result);
                }

                if (!saved && (self.rOptions.keepButtonsDisabledUntilReload == undefined || self.rOptions.keepButtonsDisabledUntilReload)) {
                    self._enableButton();
                }

                // if (self.onSaveFinish) {
                //     self.onSaveFinish();
                // }

            },
            error: function(error){
                self._processError(error);
            }

        });

    },

    /**
     * Errors Show Part
     */
    _showErrors: function (result) {
        if (result.errors) {
            $error.show(this._getFormErrorNamespace(), result.errors);
        }
    },

    _processError: function (result) {
        $cjs.processError(result);
        this.__showErros(result);
        // $.winStatus();
    },

    /**
     * Links
     */
    _backToSearch: function () {
        if (this.rOptions.searchPath) {
            document.location.href = $cjs.admPath(this.rOptions.searchPath);
        }
    },

    /**
     * Form Part
     */
    _getFormErrorNamespace: function () {
        return this.rFormErrorNamespace;
    },

    _getFormKey: function () {
        return $('.mc-form-key', this._getForm()).val();
    },

    _getForm: function () {

        if (this.rFormObject === null) {
            if (this.rFormObjectId && $("#" + this.rFormObjectId).length > 0) {
                this.rFormObject = $("#" + this.rFormObjectId);
            }
            else {
                this.rFormObject = false;
            }
        }
        return this.rFormObject;

    },

    _isFormOpen: function () {

        return this._getForm().length > 0;
    },

    /**
     *  Content Buttons (select,enable,disable)
     */
    _isDisabled: function () {
        if (this._getMainButtons().hasClass('disabled')) {
            return true;
        }
        return false;
    },

    _disableButton: function () {
        this._getMainButtons().each(function () {
            $(this).addClass('disabled');
        });
    },

    _enableButton: function () {
        this._getMainButtons().each(function () {
            $(this).removeClass('disabled').removeAttr('disabled');
        });
    },

    _getMainButtons: function () {
        if (this.rMainButtons === null) {
            this.rMainButtons = $(".main-content .btn");
        }
        return this.rMainButtons;
    },

    _initNavButtons: function () {

        var self = this;

        if (this.rNavButtonsInited === true) {
            return;
        }

        $('.mc-nav-button-delete').click(function () {
            var data;
            // && !table

            if (self._isFormOpen() && (!self._table().length)) {
                data = [self._getFormKey()];
            }

            self._deleteData(data);
        });

        $('.mc-nav-button-add').click(function () {

            if (self._isFormOpen() &&  (self._getForm().data('modal') === 'undefined')) {
                self._save();
            }
            else {
                self._showForm();
            }
            return false;
        });

        if (this._savBtn()) {
            this._savBtn().click(function () {

                self._save();
            });
        }


        $('.mc-nav-button-cancel').click(function () {
            self._backToSearch();
        });



    },

    _showForm: function () {

        if(this.rOptions.addPath){
            document.location.href = $cjs.admPath(this.rOptions.addPath);
        }
    },

    _savBtn: function () {
        if (this._getForm()) {

            var saveBtn = this._getForm().data('save-btn');

            if(saveBtn !== undefined){
                this.rSaveBtn = $('.' + saveBtn)
            }else{

                this.rSaveBtn = $('.mc-nav-button-save');
            }
        }


        return this.rSaveBtn;
    },

///////////////////////////////////////////////////// DELETE PART START ////////////////////////////////////////////////////////


    deleteRow: function (id) {
        this._deleteData(id)
    },

    _getSelectedValues: function () {
        var selected = [];
        var self = this;

        $.each(self._table().find('tbody input:checked'), function () {
            selected.push($(this).val())
        });

        return selected;
    },

    _deleteData: function (itemsList) {

        if (this._isDisabled()) {
            return false;
        }
        if (typeof itemsList === "undefined" && this._table().length > 0) {

            itemsList = this._getSelectedValues()
        }

        itemsList = itemsList || [];

        if (itemsList.length == 0) {
            this._showDeleteEmptyListModal();
            return false;
        }

        this.rSelectedForDeleteItems = itemsList;

        this._askToConfirmDelete();

    },

    _deleteConfirm: function (itemsList) {

        var self = this;
        itemsList = itemsList || this.rSelectedForDeleteItems;

        if (this._isDisabled()) {
            return;
        }
        this._disableButton();
        this.rSelectedForDeleteItems = null;


        // toastr.info($trans.get('core.loading.deleting'));

        $.ajax({
            type: 'post',
            url: self._getDeleteActionPath(),
            data: {
                deleteItems: itemsList
            },
            dataType: 'json',
            success: function (result) {

                self._deleteConfirmModal().modal('hide');

                if (result.status == 'OK') {
                    // if (onDeleteSuccess) {
                    //     onDeleteSuccess(result);
                    //     return;
                    // }
                    toastr.remove();

                    toastr.success($trans.get('core.loading.removed'));


                    if (typeof self.rOptions != 'undefined' && typeof self.rOptions.customOptions != 'undefined' && typeof self.rOptions.customOptions.onDeleteSuccess != 'undefined') {
                        self.rOptions.customOptions.onDeleteSuccess();

                        self._enableButton();
                        return;
                    }

                    if (self._table() && (!self._isFormOpen() || this.rRedirectOnDelete === false)) {
                        if (self._table()) {
                            self._table().DataTable().ajax.reload(function () {
                                $('.th-checkbox input').prop('checked', false)
                            });
                        } else {

                            // self._treeDeleteCallback(itemsList);
                        }
                    } else {

                        document.location.href = $cjs.admPath(self.rOptions.searchPath);
                    }
                } else {
                    toastr.remove();
                    toastr.clear();
                    self._processError(result);
                }


            }
        });
    },

    _table: function () {

        if (this.rTable === null) {

            if(this.rOptions.tableId !== undefined){
                if ($("#" + this.rOptions.tableId).length > 0) {
                    this.rTable = $("#" + this.rOptions.tableId);
                }
            }else{
                this.rTable = $("#list-data")
            }

        }


        return this.rTable;
    },

    _deleteConfirmModal: function () {
        var self = this;

        if (this.rDeleteConfirmModalObj === null) {

            this.rDeleteConfirmModalObj = $(this.rDeleteConfirmModalSelector);
            this.rDeleteConfirmModalObj.modal({
                show: false
            });

            this.rDeleteConfirmModalObj.on('hidden.bs.modal', function () {
                self._enableButton();
            });

            $('.btn-delete', this.rDeleteConfirmModalObj).click(function () {
                self._deleteConfirm();
            });

        }
        return this.rDeleteConfirmModalObj;

    },

    _deleteEmptyListModal: function () {
        if (this.rDeleteEmptyConfirmModalObj === null) {
            this.rDeleteEmptyConfirmModalObj = $(this.rDeleteEmptyConfirmModalSelector);
            this.rDeleteEmptyConfirmModalObj.modal({show: false});
        }
        return this.rDeleteEmptyConfirmModalObj;
    },

    _treeDeleteCallback: function (ids) {
        var tree = $('#tree-data');
        for (var i in ids) {
            var item = $('.dd-item-' + ids[i], tree);
            var parents = item.parents('.dd-item');
            item.remove();
            parents.each(function () {
                if ($('.dd-item', $(this)).length <= 0) {
                    $('button', $(this)).remove();
                }
            });
        }
        if ($('.dd-item', tree).length <= 0) {
            tree.append($trans.get('core.base.no_available_data'));
        }
    },

    _showDeleteEmptyListModal: function () {
        this._deleteEmptyListModal().modal('show');
    },

    _askToConfirmDelete: function () {
        this._deleteConfirmModal().modal('show');
    },


    _getDeleteActionPath: function () {

        var deletePath = null;

        if (this.rOptions.deletePath !== null) {
            deletePath = $cjs.admPath(this.rOptions.deletePath)
        }

        return deletePath;
    },


    __showErros: function (response) {
        var msg = 'An error occurred.';

        if (typeof response.message != 'undefined') {
            msg = response.message;
        } else if(typeof response.data!= 'undefined' && typeof response.data.message != 'undefined') {
            msg = response.data.message;
        }
    }


};


/*********************************************************************************/


var $error = {

    errors: {},
    errorKey: '',

    restructureIfObjectExists: function (errors, parentNode) {

        parentNode = parentNode || '';
        for (var i in errors) {
            var error = errors[i];
            if (error instanceof Object) {
                this.errorKey += i + '_';
                this.restructureIfObjectExists(error, i);
            } else {
                // remove last underscore
                var key = this.errorKey + i;
                this.errors[key] = error;
            }
        }

        var regExp = new RegExp(parentNode + '.{1}$');
        this.errorKey = this.errorKey.replace(regExp, '');
    },

    show: function (name, errors, isDataTableValidation) {

        String.prototype.replaceAll = function(search, replacement) {
            var target = this;
            return target.split(search).join(replacement);
        };

        if(typeof isDataTableValidation == "undefined"){
            isDataTableValidation = false;
        }

        this.restructureIfObjectExists(errors);
        errors = this.errors;
        this.errors = {};


        var allErrorBoxes,
            allErrors = {};

        allErrorBoxes = $('.' + name);

        // Reset all errors
        allErrorBoxes.each(
            function (i, elem) {
                $(elem)
                    .removeClass('form-error-text')
                    .html('')
                    .hide()
                    .closest('.form-group').removeClass('has-error');

                if (elem.closest('.formAccordion')) {
                    $(elem.closest('.panel')).find('.panel-heading').removeClass('has-error');
                }
            }
        );

        //  Highlight errors
        var tabPane, firstErrorElem = null, firstErrorTabs = null,accordionTitle,
            parentTabs = [];

        allErrorBoxes.each(
            function (i, elem) {
                var errName;
                elem = $(elem);
                tabPane = elem.parents('.tab-pane');

                if (elem.parents('.formAccordion').length) {
                    accordionTitle = elem.closest('.panel').find('.panel-heading')
                }

                if(elem.attr('id')){
                errName = elem.attr('id').replace(name + "-", "");

                    // if (your_string.indexOf('hello') > -1)

                    if(isDataTableValidation){
                        errName = 'f-'+errName;
                    }

                    if (typeof  errors[errName] !== "undefined") {
                        allErrors[errName] = errName;
                        elem.html(errors[errName]);
                        elem.addClass('form-error-text')
                            .closest('.form-group').addClass('has-error');
                        elem.show();
                        if(accordionTitle){
                            if(elem.closest('.formAccordion').length){
                                accordionTitle.addClass('has-error');
                            }
                        }
                        tabPane.data('hasErrors', true);
                        if (!firstErrorElem) {
                            firstErrorElem = elem;
                            tabPane.data('firstError', true);
                        }
                    } else if (!tabPane.data('hasErrors')) {
                        tabPane.data('hasErrors', false);
                    }
                    parentTabs = parentTabs.concat(tabPane.get());
                }
            }
        );

        // Highlight tab errors, and remove valid tabs
        var tab;
        parentTabs = $.unique(parentTabs);
        for (var i in parentTabs) {
            tabPane = $(parentTabs[i]);
            if (tabPane.data('hasErrors')) {
                tab = $('li a[href="#' + tabPane.attr('id') + '"]', $('.nav-tabs')).parent();
                tab.addClass('tab-has-error');
                if (tabPane.data('firstError')) {
                    if (!$(tab.attr('href')).data('hasErrors')) {
                        $('li a[href="#' + tabPane.attr('id') + '"]', $('.nav-tabs')).tab('show');
                    }
                }
            } else {
                $('li a[href="#' + tabPane.attr('id') + '"]', $('.nav-tabs')).parent().removeClass('tab-has-error');
            }
        }

        for (var i in parentTabs) {
            tabPane = $(parentTabs[i]);
            tabPane.removeData('hasErrors');
            tabPane.removeData('firstError');
        }

        if (firstErrorElem) {
            var errorFieldOffset = 10;
            if(isDataTableValidation){
                 errorFieldOffset = 80;
            }

            var firstErrorBox = firstErrorElem.closest('.form-group');
            // var firstErrorBox = firstErrorElem.parents('.form-group:first-child');

            firstErrorBox = firstErrorBox || firstErrorElem;
            if (firstErrorBox && firstErrorBox.length) {

                var navHeight = 0;

                if ( $('body').hasClass('heading-page-fixed') ) {
                    navHeight = $('.custom-wrapper__main').outerHeight();
                }

                var mainNavHeight = $('header').outerHeight();

                $("html, body").animate({
                    scrollTop: (firstErrorBox.offset().top - navHeight - 30) - errorFieldOffset - mainNavHeight
                }, 400, function(){
                    if (!firstErrorBox.find('.date').length && !firstErrorBox.find('.multipleFields').length) {
                        $('input[type=text]', firstErrorBox).focusTextToEnd();
                    }
                });
            }
        }
    }
};