var $translations = {};

var $app = {
    paths: {
        UIS_ADM_PATH: "",
        UIS_ADM_MEDIA_PATH: "",
        API_PATH: ":yourApplicationApiURL"
    }
};

if (typeof $jsTrans !== 'undefined') {
    Object.assign($translations,$jsTrans.trans)
}


var $trans = function (key) {
    return $trans.get(key);
};

$trans.get = function (key) {
    if(typeof $translations[key] !== 'undefined'){
        return $translations[key]
    }

    return key;
};


$trans.cLng = function (key) {
    if (key == 'code') {
        return $("#json-trans").attr('lngCode');
    }
    else if (key == 'id') {
        return $("#json-trans").attr('lngId');
    }
};


