function Modal(modalId, options) {
    this.modalId = modalId;
    this.mOptions = options;
    this.mModalOptions = null;
}

Modal.prototype = {

    // Initialize
    init: function () {
        var modalOptions = this._getDefaultOptions();

        this.mModalOptions = Object.assign(modalOptions, this.mOptions);

        this._render();
        this._events();
    },

    // Rendering
    _render: function () {
        var modal = '';

        modal += '<div class="modal fade" id="' + this.modalId + '" tabindex="-1" role="dialog" aria-hidden="true">';
        modal += '<div class="modal-dialog modal-'+ this.mModalOptions.modalSize+'">';
        modal += this._modalContent();
        modal += '</div>';
        modal += '</div>';

        $('#hModals').append(modal)
    },

    // Buttons events
    _events: function () {
        var self = this;
        var modal = $('#' + self.modalId);

        if (typeof this.mModalOptions.footerButtons != 'undefined') {


            $.each(self.mModalOptions.footerButtons.buttons, function (k, v) {

                var $a = v.class.replace(' ','.');

                modal.find('.' + $a + ":not([data-dismiss='modal'])").click(function () {

                    self.mModalOptions.footerButtons.buttons[k].click($(this))
                });
            });


        }
    },

    // Modal Content
    _modalContent: function () {
        var row = '';
        row += '<div class="modal-content">';
        if (typeof this.mModalOptions.showHeader == 'undefined' || typeof this.mModalOptions.showHeader == true) {
            row += this._modalHeader();
        }

        row += this._modalBody();
        row += this._modalFooter();
        row += '</div>';

        return row;
    },

    // Modal Header
    _modalHeader: function () {
        var row = '';
        row += '<div class="modal-header" >';
        row += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
        row += '<h4 class="modal-title">' + this.mModalOptions.headerText + '</h4>';
        row += '</div>';

        return row;
    },

    // Modal Body
    _modalBody: function () {
        var row = '';
        row += '<div class="modal-body" >';
        row += this.mModalOptions.bodyContent();
        row += '</div>';

        return row
    },

    // Modal Footer
    _modalFooter: function () {
        var row = '';

        row += '<div class="modal-footer">';

        if (typeof this.mModalOptions.footerButtons !== 'undefined' && typeof this.mModalOptions.footerButtons.buttons !== 'undefined') {

            $.each(this.mModalOptions.footerButtons.buttons, function (k, v) {
                row += '<button class="btn a btn-primary ' + v.class + '">' + v.title + '</button>'
            });

        }

        if (this.mModalOptions.closeBtn) {

            var cText = $trans('core.base.button.close');

                if(typeof this.mModalOptions.closeBtnText != 'undefined'){
                    cText = this.mModalOptions.closeBtnText
                }
            row += '<button class="btn btn-default" data-dismiss="modal">' + cText + '</button>';
        }

        row += '</div>';

        return row
    },

    // Default Options
    _getDefaultOptions: function () {
        return {
            modalSize:'md',
            closeBtn: true,
            headerText: 'Modal Header Text',
            bodyContent: function () {
                return 'Modal Body Text';
            }
        }
    },

    open: function (staticModal = false) {
       var self = this;
       var modal = $('#' + self.modalId);

       if(modal.length > 0){
           if(staticModal){
               modal.modal({backdrop:'static'});
           }else{
               modal.modal();
           }
       }
    }
};