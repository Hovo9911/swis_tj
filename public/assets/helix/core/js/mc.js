var $mc = {};

function FormObject(name) {
    this.name = name
}



$mc.STATUS_ACTIVE = '1';
$mc.STATUS_INACTIVE = '2';

$mc.isDisabled = function() {
    if( this.getMainButtons().hasClass( 'disabled' ) ){
        return true;
    }
    return false;
};

$mc.disableButton = function() {
    this.getMainButtons().each(function () {
        $(this).addClass( 'disabled' );
    });
};

$mc.enableButton = function(){
    this.getMainButtons().each(function () {
        $(this).removeClass( 'disabled' ).removeAttr( 'disabled' );
    });
};

$mc.mainButtons = null;




$mc.getMainButtons = function(){
    if( this.mainButtons === null ) {
        this.mainButtons = $( ".main-content .btn" );
    }
    return this.mainButtons;
};

$mc.initEditor = function() {

    if (typeof tinymce == "undefined") {
        return;
    }

    // init simple editor
    this.initSimpleEditor('textarea.simple-editor');

    // init basic editor
    this.initBasicEditor('textarea.basic-editor');
};

$mc.initSimpleEditor = function(selector){
    tinymce.init({
        selector: selector,
        theme: "modern",
        plugins: "link autolink pagebreak wordcount image paste media hr code fullscreen table visualblocks",
        height: '250px',
        code_dialog_width: 800,
        code_dialog_height: 450,
        image_advtab: true,
        relative_urls: false,
        custom_elements : 'style,script',
        content_css : "/assets/helix/core/css/tiny_mce.css",
        image_class_list: [
            {title: 'None', value: ''},
            {title: 'Align left', value: 'static-img-left'},
            {title: 'Align right', value: 'static-img-right'},
            {title: 'Align center', value: 'static-img-center'}
        ],
        file_browser_callback: function(field_name, url, type, win) {

            $fileManager.containerSelector = '.file-manager-container';
            $fileManager.onFileClick = function(path){
                win.document.getElementById(field_name).value = path;
                $('.file-manager-modal').modal('hide');
            };
            $fileManager.render();

            $('.file-manager-modal').modal();
        }
    });
};

$mc.initBasicEditor = function(selector){
    tinymce.init({
        selector: selector,
        theme: "modern",
        plugins: "",
        toolbar: "bold italic underline | alignleft aligncenter alignright | bullist numlist",
        menubar: false,
        image_advtab: true,
        relative_urls: false,
        content_css : "/assets/helix/core/css/tiny_mce.css",
        image_class_list: [
            {title: 'None', value: ''},
            {title: 'Align left', value: 'static-img-left'},
            {title: 'Align right', value: 'static-img-right'},
            {title: 'Align center', value: 'static-img-center'}
        ],
        file_browser_callback: function(field_name, url, type, win) {

            $fileManager.containerSelector = '.file-manager-container';
            $fileManager.onFileClick = function(path){
                win.document.getElementById(field_name).value = path;
                $('.file-manager-modal').modal('hide');
            };
            $fileManager.render();

            $('.file-manager-modal').modal();
        }
    });
};

/*********************************************************************************/

$mc.formObjectId =  'form-data';


$mc.formObject   =  null;
$mc.formErrorNamespace    = 'form-error';
$mc.saveResponse = null;

$mc.getFormErrorNamespace = function(){
    return this.formErrorNamespace;
};

$mc.getFormKey = function(){
    return $( '.mc-form-key', this.getForm()).val();
};

$mc.initForm = function(){

    var self = this,
        formObj;

    if( formObj = this.getForm() ){

        formObj.submit(
            function () {
                self.save();
                return false;
            }
        );

        // $('.form-show-status',formObj).radioButton();
        // $(".form-checkbox",formObj).iCheck({
        //     checkboxClass: 'icheckbox_flat-aero',
        //     radioClass: 'iradio_flat-aero'
        // });

        // if($.ui.imgUploader){
        //     $(".img-uploader-box",formObj).imgUploader();
        // }

        // if ($.fn.datetimepicker) {
        //     //$(".datetime-box",formObj).datetimepicker();
        //     $(".datetime-box",formObj).each(function(){
        //         var input = $(this).find("input[type=text]");
        //         $(this).datetimepicker().on('changeDate', function(ev){
        //             var altField = $(input).data('alt-field');
        //             $("#" + altField).val($(input).datetimepickerVal());
        //         });
        //     });
        // }

        // if ($.fn.datepicker) {
        //     $(".date-box",formObj).each(function(){
        //         var input = $(this).find("input[type=text]");
        //         $(this).datepicker().on('changeDate', function(ev){
        //             $(this).datepicker('hide');
        //             var altField = $(input).data('alt-field');
        //             $("#" + altField).val($(input).datetimepickerVal());
        //         });
        //     });
        // }

        formObj.append( '<input type="submit" class="hidden-form-elem" value="." />' );
        self.initEditor();

        $('.nav-tabs a').on('click', function() {
            setTimeout(function() {
                $('.tab-pane.active .form-group:first input').focus();
            }, 10);
        });
    }

};

$mc.navButtonsInited = false;
$mc.initNavButtons = function(){

    var self = this;
    if( self.navButtonsInited === true ){
        return;
    }

    $('.mc-nav-button-delete').click(function(){
        var data;
        if( self.isFormOpen() && !self.table ){
            data = [ self.getFormKey() ];
        }
        self.deleteData(data);
    });

    $('.mc-nav-button-add').click(function(){
        if( self.isFormOpen() ){
            self.save();
        }
        else{
            self.showFrom();
        }
        return false;
    });

    $('.mc-nav-button-save').click(function(){
        self.save();
    });

    $('.mc-nav-button-cancel').click(function(){
        self.backToSearch();
    });

};

$mc.showFrom = function(id){
    if(typeof this.addPath == 'undefined'){
        this.addPath = this.formPath;
    }
    document.location.href = $cjs.admPath( this.addPath );
};

$mc.backToSearch = function(){
    if (this.searchPath) {
        document.location.href = $cjs.admPath(this.searchPath);
    }
};

$mc.init = function(){

    var self = this;
    $(document).ready(function(){
        self.initNavButtons();
        if ( $("#form-data").length > 0 ) {
            self.initEditPage();
        }
        else {
            self.initSearchPage();
        }
    });

};

$mc.initEditPage = function(){

    this.initForm();
    this.onInitEditPage && this.onInitEditPage();
};

$mc.getForm = function(){

    if( this.formObject === null ){
        if( this.formObjectId && $("#"+this.formObjectId).length > 0 ){
            this.formObject =  $("#"+this.formObjectId);
        }
        else{
            this.formObject = false;
        }
    }
    return this.formObject;

};

$mc.isFormOpen = function(){

    return this.getForm().length > 0;
};
toastr.options.positionClass = 'toast-top-center'
$mc.save = function () {
    $('.main-content').toggleClass('sk-loading');

    if (typeof tinymce !="undefined" ){
        tinymce.triggerSave();
    }

    var self = this,
        formObj = this.getForm(),
        sendData,
        rPath;

    if( !formObj ){
        throw new Error( " MC - Form Not Found  " );
    }

    if( this.isDisabled() ){
        return false;
    }
    this.disableButton();

    sendData =  formObj.serializeArray();
    rPath    =  formObj.attr( 'action' );

    $.ajax ( {
        type		:	'POST',
        url			: 	rPath,
        data		: 	sendData,
        dataType	: 	'json',
        success		: 	function ( result ) {

            toastr.options.preventDuplicates = true;
            if($mc.ajaxForceCallback !== undefined){
                $mc.ajaxForceCallback(result);
            }

            if( self.saveResponse && self.saveResponse(result) === false ){
                return false;
            }


            if ( result.status == 'OK' ) {

                self.showErrors(result);
                // if($.ui.imgUploader){
                //     $(".img-uploader-box",formObj).imgUploader('saveState');
                // }
                if( self.onSaveSuccess && self.onSaveSuccess(result) === false ){
                    self.enableButton();
                    return;
                }


                var bactToUrl = result.data.backToUrl;

                if (bactToUrl) {

                    toastr.options.onShown = function() {
                        window.location.href = bactToUrl;
                    };

                    toastr.success($trans.get( 'core.loading.saved' ));


                } else {
                    self.backToSearch();
                }

            }
            else if ( result.status == 'INVALID_DATA' ) {
                self.showErrors(result);
                // $.winStatus( $trans.get( 'core.loading.invalid_data' ) , true , null, true  );
                toastr.error( $trans.get( 'core.loading.invalid_data' ));
            }
            else {
                self.processError( result );
            }
            self.enableButton();
            if(self.onSaveFinish) {
                self.onSaveFinish();
            }

        }

    } );

};

$mc.showErrors = function(result){
    if( result.errors ){
        $error.show( this.getFormErrorNamespace() , result.errors );
    }
};

$mc.processError = function(result){
    $cjs.processError( result );
    $.winStatus();
};

//	-------------------------------------------------------------------
//	Delete Data Functionality
//	-------------------------------------------------------------------

$mc.deleteConfirmModalObj = null;
$mc.deleteConfirmModalSelector = "#d-delete-confirm";

$mc.deleteEmptyConfirmModalObj = null;
$mc.deleteEmptyConfirmModalSelector = "#d-delete-empty-message";

$mc.deletePath = null;
$mc.redirectOnDelete = true;

$mc.selectedForDeleteItems = null;

$mc.deleteData = function ( itemsList ) {

    var self = this;

    if( self.isDisabled() ){
        return false;
    }

    if (typeof itemsList === "undefined") {
        itemsList = self.table.getSelectedValues();
    }

    itemsList = itemsList || [];

    if ( itemsList.length == 0  ) {
        self.showDeleteEmptyListModal();
        return false;
    }
    self.selectedForDeleteItems = itemsList;

    self.askToConfirmDelete();

};

$mc.deleteConfirm = function(itemsList) {
    var self = this;
    itemsList = itemsList || self.selectedForDeleteItems;

    if (self.isDisabled()) {
        return;
    }
    self.disableButton();
    self.selectedForDeleteItems = null;

    $.winStatus($trans.get('core.loading.deleting'));

    $.ajax({
        type	 : 'post',
        url		 : self.getDeleteActionPath(),
        data	 : {
            deleteItems : itemsList
        },
        dataType : 'json',
        success	 : function(result) {

            self.deleteConfirmModal().modal('hide');
            if (result.status == 'OK') {
                if (self.onDeleteSuccess) {
                    self.onDeleteSuccess(result);
                    return;
                }
                $.winStatus($trans.get('core.loading.removed'), true);
                if ((self.table || self.tree) && (!self.isFormOpen() || self.redirectOnDelete === false)) {
                    if (self.table) {
                        self.table.updateData();
                    } else {
                        //self.tree.uisTree("updateData");
                        $mc.treeDeleteCallback(itemsList);
                    }
                } else {
                    $.winStatus($trans.get('core.loading.removed'), true, function() {
                        document.location.href =  $cjs.admPath(self.searchPath);
                    });
                }
            } else {
                self.processError(result);
            }
        }
    });
};

$mc.treeDeleteCallback = function(ids) {
    var tree = $('#tree-data');
    for (var i in ids) {
        var item = $('.dd-item-'+ids[i], tree);
        var parents = item.parents('.dd-item');
        item.remove();
        parents.each(function() {
            if ($('.dd-item', $(this)).length <= 0) {
                $('button', $(this)).remove();
            }
        });
    }
    if ($('.dd-item', tree).length <= 0) {
        tree.append($trans.get('core.base.no_available_data'));
    }
};

$mc.getDeleteActionPath = function(){
    if( this.deletePath === null ){
        return null;
    }
    return $cjs.admPath(this.deletePath,true);
};

$mc.askToConfirmDelete = function(){
    this.deleteConfirmModal().modal('show');
};

$mc.deleteConfirmModal = function(){

    var self = this;
    if( this.deleteConfirmModalObj === null ){

        this.deleteConfirmModalObj = $(this.deleteConfirmModalSelector);
        this.deleteConfirmModalObj.modal({
            show : false
        });

        this.deleteConfirmModalObj.on('hidden.bs.modal', function(){
            self.enableButton();
        });

        $('.btn-delete', this.deleteConfirmModalObj).click(function(){
            self.deleteConfirm();
        });

    }
    return this.deleteConfirmModalObj;

};

$mc.deleteEmptyListModal = function(){
    if( this.deleteEmptyConfirmModalObj === null ){
        this.deleteEmptyConfirmModalObj = $( this.deleteEmptyConfirmModalSelector );
        this.deleteEmptyConfirmModalObj.modal({show:false});
    }
    return this.deleteEmptyConfirmModalObj;
};

$mc.showDeleteEmptyListModal = function(){
    this.deleteEmptyListModal().modal('show');
};

$mc.getBackTo = function(){
    var queryString = window.location.search;
    if (queryString.indexOf('backTo') > -1) {

        var query = {};
        var a = queryString.split('&');
        for (var i = 0; i < a.length; i++) {
            var b = a[i].split('=');
            query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
        }
        return query.backTo;
    }
    return null;
};

$mc.initShowStatusReq = function() {
    var generalStatusBox = $('.general-status'),
        generalActiveStatus = generalStatusBox.find('input[value="1"]'),
        generalInactiveStatus = generalStatusBox.find('input[value="2"]'),
        mlStatusBox = $('.ml-status'),
        mlActiveStatus = mlStatusBox.find('input[value="1"]'),
        mlInactiveStatus = mlStatusBox.find('input[value="2"]');

    generalInactiveStatus.change(function() {
        if ($(this).prop('checked')) {
            $('label.data-req').addClass('required').removeClass('data-req');
        }
    }).change();

    mlInactiveStatus.change(function() {
        if ($(this).prop('checked')) {
            $(this).closest('.tab-pane').find('label.data-req').addClass('required').removeClass('data-req');
        }
    }).change();

    generalActiveStatus.change(function() {
        if ($(this).prop('checked')) {
            if (mlActiveStatus.length > 0) {
                $('#tab-general').find('.required').addClass('data-req');
                mlActiveStatus.change();
            } else {
                $('.required').addClass('data-req');
            }
        }
    }).change();

    mlActiveStatus.change(function() {
        if ($(this).prop('checked') && generalActiveStatus.prop('checked')) {
            $(this).closest('.tab-pane').find('label.required').addClass('data-req');
        }
    }).change();
};


var $error = {

    errors : {},
    errorKey: '',

    restructureIfObjectExists: function(errors, parentNode) {

        parentNode = parentNode || '';
        for (var i in errors) {
            var error = errors[i];
            if (error instanceof Object) {
                this.errorKey += i + '_';
                this.restructureIfObjectExists(error, i);
            } else {
                // remove last underscore
                var key = this.errorKey + i;
                this.errors[key] = error;
            }
        }

        var regExp = new RegExp(parentNode+'.{1}$');
        this.errorKey = this.errorKey.replace(regExp, '');
    },

    show : function( name, errors ){

        this.restructureIfObjectExists(errors);
        errors = this.errors;
        this.errors = {};

        var allErrorBoxes,
            allErrors = {};

        allErrorBoxes = $('.'+name);

        // Reset all errors
        allErrorBoxes.each (
            function ( i ,elem ) {
                $(elem)
                    .removeClass('form-error-text')
                    .html('')
                    .hide()
                    .closest('.form-group').removeClass('has-error');
            }
        );

        //  Highlight errors
        var tabPane, firstErrorElem = null, firstErrorTabs = null,
            parentTabs = [];
        allErrorBoxes.each (
            function( i ,elem )	{
                var errName
                elem = $(elem);
                tabPane = elem.parents( '.tab-pane');
                errName = elem.attr( 'id' ).replace(  name+"-", ""  );
                if (typeof  errors[errName]!=="undefined") {
                    allErrors[errName] = errName;
                    elem.html( errors[errName] );
                    elem.addClass('form-error-text')
                        .closest('.form-group').addClass('has-error');
                    elem.show( );
                    tabPane.data('hasErrors', true);
                    if (!firstErrorElem) {
                        firstErrorElem = elem;
                        tabPane.data('firstError', true);
                    }
                } else if (!tabPane.data('hasErrors')) {
                    tabPane.data('hasErrors', false);
                }
                parentTabs = parentTabs.concat(tabPane.get());
            }
        );

        // Highlight tab errors, and remove valid tabs
        var tab;
        parentTabs = $.unique(parentTabs);
        for (var i in parentTabs) {
            tabPane = $(parentTabs[i]);
            if (tabPane.data('hasErrors')) {
                tab = $('li a[href="#'+tabPane.attr('id')+'"]' , $('.nav-tabs') ).parent();
                tab.addClass('tab-has-error');
                if (tabPane.data('firstError')) {
                    if (!$(tab.attr('href')).data('hasErrors')) {
                        $('li a[href="#'+tabPane.attr('id')+'"]' , $('.nav-tabs') ).tab('show');
                    }
                }
            } else {
                $('li a[href="#'+tabPane.attr('id')+'"]' , $('.nav-tabs') ).parent().removeClass('tab-has-error');
            }
        }

        for (var i in parentTabs) {
            tabPane = $(parentTabs[i]);
            tabPane.removeData('hasErrors');
            tabPane.removeData('firstError');
        }

        if (firstErrorElem) {
            var firstErrorBox = firstErrorElem.parents('.form-group:first-child');
            firstErrorBox = firstErrorBox || firstErrorElem;
            if (firstErrorBox && firstErrorBox.length) {
                $('input[type=text]',firstErrorBox).focus();
                $("html, body").animate({
                    scrollTop: firstErrorBox.offset().top-10
                }, 400 );
            }
        }
    }
};