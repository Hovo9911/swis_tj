function Calendar(className, options) {
    this.calendarObject = className;

    // options
    this.options = options;
    this.show = false;
}

Calendar.prototype = {

    initCalendar: function () {
        this._drawMonths();
    },

    _getCalendar: function () {
        if ($('.' + this.calendarObject).length) {
            return $('.' + this.calendarObject);
        }
    },

    _drawCalendar: function () {

        let year = this.options.viewDateOptions.year;
        let currentYear = this.options.viewDateOptions.currentYear;
        let language = this.options.viewDateOptions.language;
        let nonWorkingDays = this.options.viewDateOptions.nonWorkingDays;

        for (let i = 0; i < 12; i++) {

            let startDate = new Date(year, i, 1);
            let endDate = new Date(year, i, new Date(year, i + 1, 0).getDate());

            //Disable current month previous days
            if (i === today.getMonth() && year == currentYear) {
                //start date tomorrow
                startDate =  new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
            }

            this._getCalendar().find('.datepicker-' + i).datepicker({
                format: dateFormat,
                multidate: true,
                todayHighlight: true,
                defaultViewDate: {year: year, month: i, day: 1},
                weekStart: 1,
                startDate: startDate,
                endDate: endDate,
                language: language,
                beforeShowDay: function (date) {
                    let allDates = year + '-' + (date.getMonth() + 1).toString().padStart(2, '0') + '-' + date.getDate().toString().padStart(2, '0');
                    if (nonWorkingDays.indexOf(allDates) != -1) {
                        return {tooltip: $trans.get('swis.calendar.non_working_days.tooltip')};
                    }
                }
            });
        }

        //Disable previous months
        this._disableMonths();

        //User can't change month,year
        $("th").removeClass("prev next datepicker-switch");

        //Show,hide week ends in calendar
        this._checkWeekEnds();

        //Showing active dates in calendar
        this._setActiveDates();
    },

    _setActiveDates: function () {

        let nonWorkingDays = this.options.viewDateOptions.nonWorkingDays;
        let nonWorkingDaysDateFormat = [];

        if (nonWorkingDays.length) {
            for (let j = 0; j < nonWorkingDays.length; j++) {
                let date = new Date(nonWorkingDays[j]);
                nonWorkingDaysDateFormat.push(new Date(date.getFullYear(), date.getMonth(), date.getDate()));
            }
        }

        for (let i = 0; i < 12; i++) {
            this._getCalendar().find('.datepicker-' + i).datepicker('setDates', nonWorkingDaysDateFormat);
        }
    },

    _drawMonths: function () {
        let str = '';
        for (let i = 0; i < 12; i++) {
            str += '<div class="col-lg-3 col-md-4 col-sm-6 large-calendar">' +
                '<div class="datepicker-' + i + '"></div>' +
                '</div>';
        }

        this._getCalendar().html(str);
        this._drawCalendar();
    },

    _checkWeekEnds: function () {
        let self = this;

        $('.weekend-days').on('click', function () {
            let currentTabYear = self._getActiveTab().find('a').data('year');

            if (currentTabYear == self.options.viewDateOptions.year) {

                self.show = !self.show;
                if (self.show) {
                    self._isWeekEnd();
                    for (let i = 0; i < 12; i++) {
                        self._getCalendar().find('.datepicker-' + i).datepicker('setDates', self.options.viewDateOptions.allWeekends);
                    }

                } else {
                    for (let i = 0; i < 12; i++) {
                        self._getCalendar().find('.datepicker-' + i).datepicker('clearDates', self.options.viewDateOptions.allWeekends);
                    }
                }
            }
        });
    },

    _isWeekEnd: function () {
        {
            let self = this;
            let startDate = new Date(self.options.viewDateOptions.year, 0, 1);
            let endDate = new Date(self.options.viewDateOptions.year, 11, 31);
            let dayMilliseconds = 1000 * 60 * 60 * 24;

            self.options.viewDateOptions.allWeekends = [];
            while (startDate <= endDate) {
                if (startDate > today && (startDate.getDay() == 0 || startDate.getDay() == 6)) {
                    self.options.viewDateOptions.allWeekends.push(startDate)
                }
                startDate = new Date(+startDate + dayMilliseconds);
            }

        }
    },

    _getActiveTab: function () {
        return $('.nav-tabs').find('li.active');
    },

    _disableMonths: function () {
        for (let i = 0; i < 12; i++) {
            if (i < this.options.viewDateOptions.today.getMonth() && this.options.viewDateOptions.year == this.options.viewDateOptions.currentYear) {
                this._getCalendar().find('.datepicker-' + i).find('.table-condensed').addClass('disable');
            }
        }
    }

};

//
let saveButton = $('.mc-nav-button-save');

saveButton.on('click', function () {

    let self = $(this);
    let date = [];
    let year = $('.nav-tabs').find('li.active').find('a').data('year');
    let selectCalendar = $('.tab-content').find('div.active div div').hasClass('calendar-1');
    let alert = $('.tab-content').find('div.active .alert');

    self.prop('disabled', true);

    for (let i = 0; i < 12; i++) {
        let dates = selectCalendar ? $('.datepicker-' + i + ':first').datepicker('getDates') : $('.datepicker-' + i + ':last').datepicker('getDates');

        if (dates.length == 1) {
            date.push(formatDate(dates));

        } else if (dates.length > 1) {
            for (let j = 0; j < dates.length; j++) {
                date.push(formatDate(dates[j]));
            }
        }
    }

    $.ajax({
        type: 'POST',
        url: $cjs.admPath('calendar'),
        data: {date: date, year: year},
        dataType: 'json',
        success: function (result) {
            if (result.status === 'OK') {
                location.reload();
            } else if (result.status === 'INVALID_DATA') {
                alert.removeClass('hide');
                self.prop('disabled', false);
            }
        }
    });
});

function formatDate(date) {
    let d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}


