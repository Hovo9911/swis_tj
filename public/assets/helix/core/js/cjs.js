$cjs = {};

$cjs.enableButtos = function () {
    $(".main-content .btn").each(function () {
        $(this).removeClass('disabled').removeAttr('disabled');
    });
};

$cjs.admPath = function (path, includeToken) {

    var tokenAppendStr = '',
        token;
    path += '';

    if (includeToken && typeof $csrfToken !== "undefined") {
        if (path.indexOf('?') === -1 && $csrfToken) {
            path += '?_token=' + encodeURIComponent($csrfToken);
        }
        else {
            path += '&_token=' + encodeURIComponent($csrfToken);
        }
    }

    var locationPath = $app.paths.UIS_ADM_PATH + path;

    if (locationPath[0] !== '/') {
        locationPath = '/' + locationPath
    }

    return '/' + $cLng + locationPath;

};

$cjs.mediaPath = function (path) {
    return $app.paths.UIS_ADM_MEDIA_PATH + path;
};

$cjs.basePath = function (path) {
    return $app.paths.UIS_ADM_PATH + path;
};

$cjs.admPathWithoutLng = function (path) {
    var lPath = $app.paths.UIS_ADM_PATH + path;

    return  '/' +lPath;
};

$cjs.apiPath = function (path) {
    return $app.paths.API_PATH + path;
};


var showAlertsMessages = $("#showAlertsMessages");
var cjsMessages = $(".cjs-messages");

$cjs.processError = function(data) {

    if (showAlertsMessages.length) {
        var messageStr = '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>' + $trans('swis.db.exception.message') + '</div>'

        showAlertsMessages.html(messageStr);
        $cjs.enableButtos();
        $('.tab-content').removeClass('loading-close');
        animateScrollToElement(showAlertsMessages,1,1);
    }
};

$cjs.processErrorClear = function(){
    showAlertsMessages.html('');
    cjsMessages.html('');
};

$cjs.onOk = null;
$cjs.processErrorModal = false;
$cjs.processError_old = function (data) {

    var status = data.status ? data.status : data;
    if (!$cjs.processErrorModal) {

        var modalHtml = '<div  class="modal ui2-error-dialog" tabindex="-1" role="dialog" aria-hidden="true" >';
        modalHtml += '<div class="modal-header" >';
        modalHtml += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true" >x</button>';
        modalHtml += '<h3></h3>';
        modalHtml += '</div>';

        modalHtml += '<div class="modal-body modal-confirm-text" >';
        modalHtml += '<div class="ui2-error-dialog-message"></div>';
        modalHtml += '</div>';

        modalHtml += '<div class="modal-footer" >';
        modalHtml += '<a class="btn btn-primary" data-dismiss="modal" aria-hidden="true">' + $trans.get('base.ok') + '</a>';
        modalHtml += '</div>';
        modalHtml += '</div>';

        $cjs.processErrorModal = $(modalHtml);
        $cjs.processErrorModal.modal().on('hide', function () {

            if ($cjs.onOk != null) {
                $cjs.onOk();
            }

        });

        $cjs.processErrorModal.on('hidden.bs.modal', function () {
            $cjs.enableButtos()
        });

    }

    $cjs.onOk = null;
    var title = '';
    var text = '';

    if (!data.message) {
        text = $trans.get('core.error.500.message');
        switch (status) {
            case 'BAD_REQUEST':
                text = $trans.get('core.error.400.message');
                break;
            case 'FORBIDDEN':
                text = $trans.get('core.error.403.message');
                break;
            case 'NOT_FOUND':
                text = $trans.get('core.error.404.message');
                break;
            case 'UNAVAILABLE_OPERATION':
                text = $trans.get('core.error.405.message');
                break;
            case 'NOT_AUTH':
                text = '<a href="' + $cjs.admPath('/login') + '" >' + $trans.get('core.error.401.message') + '</a>';
                $cjs.onOk = function () {
                    document.location.href = $cjs.admPath('/login');
                };
                break;
            case 'VERIFY_NETWORK':
                text = $trans.get('core.error.verify_network.message');
                break;
        }
    } else {
        text = '<div>' + data.message + '</div>';
        if (data.exceptionText) {
            text += '<pre>' + data.exceptionText + '</pre>';
        }
    }

    if (!data.head_title) {
        title = $trans.get('core.error.500.head_title');
        switch (status) {
            case 'BAD_REQUEST':
                title = $trans.get('core.error.400.head_title');
                break;
            case 'FORBIDDEN':
                title = $trans.get('core.error.403.head_title');
                break;
            case 'NOT_FOUND':
                title = $trans.get('core.error.404.head_title');
                break;
            case 'UNAVAILABLE_OPERATION':
                title = $trans.get('core.error.405.head_title');
                break;
            case 'NOT_AUTH':
                title = $trans.get('core.error.401.head_title');
                break;
            case 'VERIFY_NETWORK':
                title = $trans.get('core.error.verify_network.head_title');
                break;
        }
    } else {
        title = data.head_title;
    }

    $('.ui2-error-dialog-message', $cjs.processErrorModal).html(text);
    $('h3', $cjs.processErrorModal).html(title);
    $cjs.processErrorModal.modal('show');

};


var idDDD = 0;
$.fn.radioButton = function () {

    $(this).each(function (i, elem) {

        elem = $(elem);

        var elementName = elem.attr('name'),
            radiGroup = '<div class="btn-group" data-toggle="buttons">',
            checked, buttonClass;

        elementName = elementName ? 'name="' + elementName + '"' : '';

        $('option', elem).each(function (j, option) {

            option = $(option);
            buttonClass = '';
            checked = '';
            if (option.attr('selected')) {
                buttonClass = ' active';
                checked = ' checked="" ';
            }
            idDDD++;
            radiGroup += '<label class="btn btn-default' + buttonClass + '">';
            radiGroup += '<input type="radio" ' + elementName + ' ' + checked + ' value="' + option.val() + '" id="ddddd-' + idDDD + '" />';
            radiGroup += option.text();
            radiGroup += '</label>';

        });
        radiGroup += '</div>';
        radiGroup = $(radiGroup);

        radiGroup.button();

        elem.replaceWith(radiGroup);


    });

    //	id="option2"
    //  $(this).button();
    //

};