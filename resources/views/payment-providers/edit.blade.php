@extends('layouts.app')

@section('title'){{trans('swis.payment_providers.title')}} @endsection

@section('contentTitle') {{trans('swis.payment_providers.forms_title')}} @endsection

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@endsection

@section('content')
    <?php
    switch ($saveMode) {
        case 'add':
            $url = 'payment-providers';
            break;
        default:
            $url = $saveMode != 'view' ? 'payment-providers/update/' . $paymentProviders->id : '';

            break;
    }

    $languages = activeLanguages();
    ?>

    <form class="form-horizontal fill-up " id="form-data" action="{{urlWithLng($url)}}" autocomplete="off">
        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-general"> {{trans('core.base.tab.general')}}</a></li>
            </ul>
            <div class="tab-content">
                <div id="tab-general" class="tab-pane active">
                    <div class="panel-body">

                        <div class="form-group required">
                            <label class="col-lg-2 control-label">{{trans('swis.payment_providers.name.label')}}</label>
                            <div class="col-lg-8">
                                <input value="{{ !empty($paymentProviders) ? $paymentProviders->name : ''}}"
                                       type="text" name="name" placeholder="" class="form-control">
                                <div class="form-error" id="form-error-name"></div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-lg-2 control-label">{{trans('swis.payment_providers.description.label')}}</label>
                            <div class="col-lg-8">
                                <input value="{{ !empty($paymentProviders) ? $paymentProviders->description : ''}}"
                                       type="text" name="description" placeholder="" class="form-control">
                                <div class="form-error" id="form-error-description"></div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-lg-2 control-label">{{trans('swis.payment_providers.ip_address.label')}}</label>
                            <div class="col-lg-8">
                                <input value="{{ !empty($paymentProviders) ? $paymentProviders->ip_address : ''}}"
                                       type="text" name="ip_address" placeholder="" class="form-control">
                                <div class="form-error" id="form-error-ip_address"></div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-lg-2 control-label">{{trans('swis.payment_token.token.label')}}</label>
                            <div class="col-lg-8">
                                <input value="{{ !empty($paymentProviders) ? $paymentProviders->token : ''}}"
                                       type="text" name="token" placeholder="" class="form-control">
                                <div class="form-error" id="form-error-token"></div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-lg-2 control-label label__title">{{trans('swis.payment_providers.address.label')}}</label>
                            <div class="col-lg-8">
                                <textarea name="address"
                                                  placeholder="" class="form-control"
                                                  rows="5">{{ !empty($paymentProviders) ? $paymentProviders->address : ''}}</textarea>
                                <div class="form-error" id="form-error-address"></div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-lg-2 control-label label__title">{{trans('swis.payment_providers.contacts.label')}}</label>
                            <div class="col-lg-8">
                                <textarea name="contacts"
                                                  placeholder="" class="form-control"
                                                  rows="5">{{ !empty($paymentProviders) ? $paymentProviders->contacts : ''}}</textarea>
                                <div class="form-error" id="form-error-contacts"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">{{trans('swis.constructor_templates.show_status.label')}}</label>
                            <div class="col-lg-10 general-status">
                                @php
                                    $activeStatus   = \App\Models\PaymentProviders\PaymentProviders::STATUS_ACTIVE;
                                    $inactiveStatus = \App\Models\PaymentProviders\PaymentProviders::STATUS_INACTIVE;
                                @endphp
                                <select name="show_status" class="form-show-status">
                                    <option value="{{$activeStatus}}"{{(!empty($paymentProviders) && $paymentProviders->show_status == $activeStatus) || $saveMode == 'add' ?  ' selected' : ''}}>{{trans('core.base.label.status.active')}}</option>
                                    <option value="{{$inactiveStatus}}"{{!empty($paymentProviders) &&  $paymentProviders->show_status == $inactiveStatus ? 'selected': ''}}>{{trans('core.base.label.status.inactive')}}</option>
                                </select>
                                <div class="form-error" id="form-error-show_status"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script src="{{asset('js/payment-providers/main.js')}}"></script>
@endsection