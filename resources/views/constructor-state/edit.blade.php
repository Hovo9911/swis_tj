@extends('layouts.app')

@section('title'){{trans('swis.constructor_state.states_title')}}@stop

@section('contentTitle') {{trans('swis.constructor_state.states_title')}} @stop

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('topScripts')
    <script>
        var constructorDocumentId  = "{{$selectedConstructorDocumentId or $constructorState->constructor_document_id}}";
    </script>
@endsection

@section('content')
    <?php

    switch ($saveMode) {
        case 'add':
            $url = 'constructor-states';
            break;
        default:
            $url = 'constructor-states/update/' . $constructorState->id;
            $mls = $constructorState->ml()->notDeleted()->base(['lng_id', 'state_name', 'description', 'show_status'])->keyBy('lng_id');
            break;
    }

    $languages = activeLanguages();
    ?>

    <form class="form-horizontal fill-up " id="form-data" action="{{urlWithLng($url)}}">

        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-general"> {{trans('core.base.tab.general')}}</a></li>
                @foreach($languages as $language)
                    <li>
                        <a data-toggle="tab" href="#tab-{{$language->code}}">{{ trans("core.base.language.".strtolower($language->name)) }}</a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content">
                <div id="tab-general" class="tab-pane active">
                    <div class="panel-body">

                        <div class="form-group required"><label
                                    class="col-lg-2 control-label">{{ trans('swis.document.create.title') }}</label>
                            <div class="col-lg-10">
                                <select class="form-control select2" name="constructor_document_id">
                                    <option></option>
                                    @foreach($constructorDocuments as $constructorDocument)
                                        <option {{(isset($constructorState) && $constructorState->constructor_document_id == $constructorDocument->id) || (isset($selectedConstructorDocumentId) == $constructorDocument->id) ? 'selected': '' }} value="{{$constructorDocument->id}}">{{$constructorDocument->current()->document_name}}</option>
                                    @endforeach
                                </select>
                                <div class="form-error" id="form-error-constructor_document_id"></div>
                            </div>
                        </div>

                        @if($saveMode == 'edit')
                            <div class="form-group "><label
                                        class="col-lg-2 control-label">{{ trans('swis.constructor_state.state_id') }}</label>
                                <div class="col-lg-10">
                                    <input value="{{$constructorState->state_id or ''}}" disabled="" type="text"
                                           placeholder="" class="form-control">
                                </div>
                            </div>
                        @endif

                        <div class="form-group">
                            <label class="control-label col-lg-2">{{ trans('swis.constructor_state.state_type') }}</label>
                            <div class="col-lg-10">
                                @foreach(\App\Models\ConstructorStates\ConstructorStates::STATE_TYPES as $stateType)
                                    <label class="radio-inline">
                                        <input type="radio" value="{{$stateType}}"
                                               {{((isset($constructorState) && $constructorState->state_type == $stateType) ? 'checked' : '' )}} name="state_type">{{ucfirst($stateType)}}
                                    </label>
                                @endforeach

                                <div class="form-error" id="form-error-state_type"></div>
                            </div>
                        </div>
                        <div class="form-group  {{(isset($constructorState) && $constructorState->state_type == \App\Models\ConstructorStates\ConstructorStates::END_STATE_TYPE) ? '' :'hide'}}">
                            <label class="control-label col-lg-2">{{ trans('swis.constructor_state.state_end_approved') }}</label>
                            <div class="col-lg-8">
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="state_approved_status" value="1" id="stateApprovedStatus"
                                           @if(isset($constructorState) && $constructorState->state_approved_status) checked @endif>{{trans('swis.constructor_state.state_end_approved_yes')}}
                                </label>
                                <div class="form-error" id="form-error-state_approved_status"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">{{ trans('swis.constructor_state.is_countable') }}</label>
                            <div class="col-lg-10">
                                <input type="checkbox" value="1"
                                       {{((isset($constructorState) && $constructorState->is_countable) ? 'checked' : '' )}} {{(empty($constructorState) ? 'checked' : '' )}} name="is_countable">
                                <div class="form-error" id="form-error-is_countable"></div>
                            </div>
                        </div>


                        {{--<div class="form-group"><label class="col-lg-3 control-label">Receive Notifications via:</label>--}}
                        {{--<div class="col-lg-9">--}}
                        {{--<label class="checkbox-inline">--}}
                        {{--<input type="checkbox" name="email_notification" value="1">E-mail--}}
                        {{--</label>--}}
                        {{--<label class="checkbox-inline">--}}
                        {{--<input type="checkbox" name="sms_notification" value="1">SMS--}}
                        {{--</label>--}}
                        {{--</div>--}}
                        {{--</div>--}}


                        <div class="form-group">
                            <label class="control-label col-lg-2">{{trans('core.base.label.status')}}</label>
                            <div class="col-lg-10 general-status">
                                <select name="show_status" class="form-show-status">
                                    <option value="{{\App\Models\ConstructorStates\ConstructorStates::STATUS_ACTIVE}}"{{(isset($constructorState) && $constructorState->show_status == \App\Models\ConstructorStates\ConstructorStates::STATUS_ACTIVE) || $saveMode == 'add' ?  ' selected="selected"' : ''}}>{{trans('core.base.label.status.active')}}</option>
                                    <option value="{{\App\Models\ConstructorStates\ConstructorStates::STATUS_INACTIVE}}"{{isset($constructorState) &&  $constructorState->show_status == \App\Models\ConstructorStates\ConstructorStates::STATUS_INACTIVE ? ' selected="selected"': ''}}>{{trans('core.base.label.status.inactive')}}</option>
                                </select>
                                <div class="form-error" id="form-error-show_status"></div>
                            </div>
                        </div>

                    </div>
                </div>
                @foreach($languages as $language)
                    <?php $lngId = $language->id ?>
                    <div id="tab-{{$language->code}}" class="tab-pane">
                        <div class="panel-body">
                            <div class="form-group required"><label
                                        class="col-lg-2 control-label">{{ trans('swis.constructor_state.state_name') }}</label>
                                <div class="col-lg-10">
                                    <input value="{{isset($mls[$lngId]) ? $mls[$lngId]->state_name : ''}}"
                                           name="ml[{{$language->id}}][state_name]" type="text"
                                           placeholder=""
                                           class="form-control">

                                    <div class="form-error"
                                         id="form-error-ml-{{$language->id}}-state_name"></div>
                                </div>
                            </div>

                            <div class="form-group"><label
                                        class="col-lg-2 control-label">{{trans('core.base.description')}}</label>
                                <div class="col-lg-10">
                                    <textarea rows="6" name="ml[{{$language->id}}][description]"
                                              class="form-control">{{isset($mls[$lngId]) ? $mls[$lngId]->description : ''}}</textarea>
                                    <div class="form-error"
                                         id="form-error-ml-{{$language->id}}-description"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                @endforeach
            </div>

            <input type="hidden" value="{{$constructorState->id or 0}}" name="id" class="mc-form-key"/>

            {!! csrf_field() !!}
        </div>
    </form>

@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script src="{{asset('js/constructor-state/main.js')}}"></script>
@endsection