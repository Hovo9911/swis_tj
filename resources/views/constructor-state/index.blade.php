@extends('layouts.app')

@section('title'){{trans('swis.constructor_state.title')}}@stop

@section('contentTitle') {{trans('swis.constructor_state.title')}} @stop

@section('topScripts')
    <script>
        var constructorDocumentId  = "{{$selectedConstructorDocumentId}}";
    </script>
@endsection

@if($selectedConstructorDocumentId)
    @section('form-buttons-top')
        {!! renderAddButton() !!}
    @stop
@endif

@section('content')

    <form class="form-horizontal fill-up" id="search-filter">
        <div class="form-group">
            <label class="col-md-4 col-lg-3 control-label">{{trans('swis.document.select_document')}}</label>
            <div class="col-md-6 col-lg-6">
                <select class="form-control select2 document-list default-search auto-search-filter" name="constructor_document_id">
                    <option></option>
                    @if ($constructorDocuments->count())
                        @foreach ($constructorDocuments as $constructorDocument)
                            <option {{$selectedConstructorDocumentId == $constructorDocument->id ? 'selected' : ''}} value="{{ $constructorDocument->id }}">{{ $constructorDocument->document_name }}</option>
                        @endforeach
                    @endif
                </select>
                <div class="form-error" id="form-error-email"></div>
            </div>
        </div>

        <div class="collapse" id="collapseSearch">
            <div class="card card-body">
                <div class="form-group"><label class="col-md-4 col-lg-3 control-label">ID</label>
                    <div class="col-md-6 col-lg-6">
                        <input type="number" min="0"  class="form-control onlyNumbers" name="id" id="" value="">
                        <div class="form-error" id="form-error-id"></div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-offset-4 col-md-6 col-lg-offset-3 col-lg-6 text-right">
                        <button type="submit" class="btn btn-primary">{{trans('core.base.label.search')}}</button>
                        <button type="button" class="btn btn-danger resetButton" data-submit="true">{{trans('core.base.label.reset')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    @if($selectedConstructorDocumentId)
    <div class="data-table">
        <div class="clearfix">
            <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse" data-target="#collapseSearch" aria-expanded="false" aria-controls="collapseSearch">
                <i class="fa fa-search"> </i><span class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
            </button>
            <br />
            <table class="table table-striped table-bordered table-hover dataTables-example" id="list-data">
                <thead>
                <tr>
                    <th data-col="actions" class="th-actions" data-delete="0">{{trans('core.base.label.actions')}}</th>
                    <th data-col="id">{{ trans('swis.document_cloud.id') }}</th>
                    <th data-col="state_name">{{ trans('swis.constructor_state.state_name') }}</th>
                    <th data-col="state_id">{{ trans('swis.constructor_state.state_id') }}</th>
                    <th data-col="state_type">{{ trans('swis.constructor_state.state_type') }}</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
    @endif

@endsection

@section('scripts')
    <script src="{{asset('js/constructor-state/main.js')}}"></script>
@endsection