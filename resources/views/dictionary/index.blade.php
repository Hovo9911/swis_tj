@extends('layouts.app')

@section('title'){{trans('core.admin_menu.system.dictionary')}}@stop

@section('contentTitle') {{trans('core.admin_menu.system.dictionary')}} @stop

@section('form-buttons-top')
    {!! renderAddButton() !!}
@stop

<?php
$jsTrans->addTrans([
    "swis.base.dictionary.all",
]);
?>

@section('styles')
    <style>
        #list-data_wrapper .dataTables_paginate, #list-data_wrapper .dataTables_info {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="clearfix dictionary-content">
        <div class="row hide">
            <div class="col-md-2 col-md-offset-10">
                <form class="fill-up pull-right" id="search-filter">

                    <div class="form-group">
                        <label for="applications-list">
                            {{trans('core.dictionary.search.filter.application_name')}}
                        </label>
                        <select id="applications-list" class="form-control  default-search" name="appType" data-type="real-time">
                            <?php
                            $languages = null;
                            $selectedApp = null;

                            foreach ($apps as $app) {
                            $selected = $appType == $app->name ? 'selected' : '';
                            if ($appType == $app->name) {
                                $languages = $app->notDeletedLanguages;
                                $selectedApp = $app;
                            }
                            ?>
                            <option {{$selected}} value="{{$app->name}}">{{$app->name}}</option>
                            <?php } ?>
                        </select>
                    </div>

                </form>
            </div>
        </div>

        <div class="data-table">
            <table class="table table-striped table-bordered table-hover dataTables-example dictionaryTable"
                   id="list-data"
                   data-search-path="{{url('dictionary/table')}}">
                <thead>
                <tr>
                    <th data-col="key">{{trans('core.dictionary.key')}}</th>

                    @foreach ($languages as $key => $lng)
                        <th data-col="{{$lng->code}}">{{$lng->name}}</th>
                    @endforeach

                    {{--<th data-col="id">ID</th>--}}
                    <th data-col="actions" class="th-actions" data-delete="0">{{trans('core.base.label.actions')}}</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="modal fade" id="dictionaryModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <form class="form-horizontal fill-up" id="form-data" data-modal="true"
                  action="{{urlWithLng('dictionary/edit')}}">
                <input type="hidden" name="appType" value="{{$appType}}">
                {{csrf_field()}}
                <div class="modal-content animated">
                    <div class="modal-header">
                        <h4 class="modal__title fb">{{trans('core.dictionary.form.head_title')}}</h4>
                        <button type="button" class="close modal__close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group required">
                            <label class="control-label">Key</label>
                            <input type="text" value="" name="key" placeholder="" class="form-control">
                            <div class="form-error" id="form-error-key"></div>
                        </div>

                        @foreach ($languages as $key => $lng)
                            <div class="form-group"><label>{{$lng->name}}</label> <input type="text"
                                                                                         name="ml[{{$lng->code}}]"
                                                                                         placeholder=""
                                                                                         class="form-control"></div>
                            <div class="form-error" id="form-error-{{$lng->code}}"></div>
                        @endforeach

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">{{trans('core.base.button.close')}}</button>
                        <button type="button"
                                class="btn btn-primary mc-nav-button-save">{{trans('core.base.button.save')}}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')

    <script type="text/javascript">
        var languagesList = <?php echo json_encode($languages)?>;
    </script>

    <script src="{{asset('js/dictionary/main.js')}}"></script>
@endsection