@extends('layouts.app')

@section('title'){{trans('swis.monitoring.title')}}@stop

@section('contentTitle') {{trans('swis.monitoring.title')}} @stop

@section('content')

    @php($companyTaxId = Auth::user()->companyTaxId())
    <div class="tabs-container" id="monitoringPage">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tab-users"> {{trans('swis.monitoring.tab.users')}}</a></li>
            <li><a data-toggle="tab" href="#tab-user-roles"> {{trans('swis.monitoring.tab.user_roles')}}</a></li>
            <li><a data-toggle="tab" href="#tab-latest_events"> {{trans('swis.monitoring.tab.latest_events')}}</a></li>
            <li><a data-toggle="tab" href="#tab-payment_events"> {{trans('swis.monitoring.tab.payment_events')}}</a></li>
            <li><a data-toggle="tab" href="#tab-pays"> {{trans('swis.monitoring.tab.pays')}}</a></li>
        </ul>

        <div class="tab-content">
            <div id="tab-users" class="tab-pane active">

                <div class="clearfix search__out">
                    <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse" data-target="#collapseUsersSearch" aria-expanded="false" aria-controls="collapseSearch">
                        <i class="fa fa-search"> </i><span class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
                    </button>
                </div>

                <div class="collapse" id="collapseUsersSearch">
                    <div class="card card-body">
                        <form class="form-horizontal fill-up" id="search-filter-users">

                            <div class="form-group searchParam id_block_params" data-field="id_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">ID</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input type="text" class="form-control" name="id" id="" value="">
                                            <div class="form-error" id="form-error-id"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam passport_block_params" data-field="passport_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.passport')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input type="text" class="form-control" name="passport" id="" value="">
                                            <div class="form-error" id="form-error-passport"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam ssn_block_params" data-field="ssn_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.ssn')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input type="text" class="form-control" name="ssn" id="" value="">
                                            <div class="form-error" id="form-error-ssn"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam first_name_block_params" data-field="first_name_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.first_name')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input type="text" class="form-control" name="first_name" id="" value="">
                                            <div class="form-error" id="form-error-first_name"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group searchParam last_name_block_params" data-field="last_name_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.last_name')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input type="text" class="form-control" name="last_name" id="" value="">
                                            <div class="form-error" id="form-error-last_name"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group searchParam registered_at_block_params" data-field="registered_at_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.user.registered_at.label')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-4 col-lg-5 field__two-col">
                                            <div class='input-group date'>
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                <input value="" name="registered_at_date_start" autocomplete="off"
                                                       type="text"
                                                       placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-5 field__two-col">
                                            <div class='input-group date'>
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                <input value="" name="registered_at_date_end" autocomplete="off"
                                                       type="text"
                                                       placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam authorized_user_block_params" data-field="authorized_user_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.user.authorized_user')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input type="text" class="form-control" name="authorized_user" id="" value="">
                                            <div class="form-error" id="form-error-authorized_user"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam status_changed_by_user_block_params" data-field="status_changed_by_user_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.user.status_changed_by_user')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input type="text" class="form-control" name="status_changed_by_user" id="" value="">
                                            <div class="form-error" id="form-error-status_changed_by_user"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam show_status_block_params" data-field="show_status_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.user.show_status.label')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <select name="show_status" class="form-control">
                                                <option selected value="all">{{trans('core.base.label.status.all')}}</option>
                                                <option value="{{App\Models\ReferenceTable\ReferenceTable::STATUS_ACTIVE}}">{{trans('core.base.label.status.active')}}</option>
                                                <option value="{{App\Models\ReferenceTable\ReferenceTable::STATUS_INACTIVE}}">{{trans('core.base.label.status.inactive')}}</option>
                                            </select>
                                            <div class="form-error" id="form-error-show_status"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-8 col-lg-offset-3 text-right">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8">
                                            <button type="submit" class="btn btn-primary mr-10">{{trans('core.base.label.search')}}</button>
                                            <button type="button" class="btn btn-danger resetButton" data-submit="true">{{trans('core.base.label.reset')}}</button>
                                            <button type="button" class="btn btn-default hide-or-show-params" data-table-id="users-data" data-form="search-filter-users">{{trans('core.base.label.apply_hidden_columns')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <?php
                $customColumnsForUserData = json_encode([
                    "id" => "ID",
                    "ssn" => trans('core.base.label.ssn'),
                    "passport" => trans('core.base.label.passport'),
                    "first_name" => trans('core.base.label.first_name'),
                    "last_name" => trans('core.base.label.last_name'),
                    "email" => trans('core.base.label.email'),
                    "phone_number" => trans('core.base.label.phone'),
                    "show_status" => trans('core.base.label.status'),
                    "registered_at" => trans('swis.user.label.registered_at'),
                    "authorized_user" => trans('swis.user.label.authorized_user'),
                    "status_changed_by_user" => trans('swis.user.label.status_changed_by_user')
                ]);
                ?>
                <div class="data-table">
                    <div class="clearfix">
                        <table class="table table-striped table-bordered table-hover dataTables-example"
                               id="users-data"
                               data-form-id="search-filter-users"
                               data-custom-columns="{{ $customColumnsForUserData }}"
                               data-module-for-search-params="Monitoring{{ hash('sha256', $companyTaxId.'users-data') }}">
                            <thead>
                            <tr>
                                <th data-col="id">ID</th>
                                <th data-col="ssn">{{trans('core.base.label.ssn')}}</th>
                                <th data-col="passport">{{trans('core.base.label.passport')}}</th>
                                <th data-col="first_name">{{trans('core.base.label.first_name')}}</th>
                                <th data-col="last_name">{{trans('core.base.label.last_name')}}</th>
                                <th data-col="email">{{trans('core.base.label.email')}}</th>
                                <th data-col="phone_number">{{trans('core.base.label.phone')}}</th>
                                <th data-col="show_status">{{trans('core.base.label.status')}}</th>
                                <th data-col="registered_at">{{trans('swis.user.label.registered_at')}}</th>
                                <th data-col="authorized_user">{{trans('swis.user.label.authorized_user')}}</th>
                                <th data-col="status_changed_by_user">{{trans('swis.user.label.status_changed_by_user')}}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>

            <div id="tab-user-roles" class="tab-pane">

                <div class="clearfix search__out">
                    <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse" data-target="#collapseUserRolesSearch" aria-expanded="false" aria-controls="collapseSearch">
                        <i class="fa fa-search"> </i><span class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
                    </button>
                </div>

                <div class="collapse" id="collapseUserRolesSearch">
                    <div class="card card-body">
                        <form class="form-horizontal fill-up" id="search-filter-user-roles">

                            <div class="form-group searchParam id_block_params" data-field="id_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">ID</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input type="text" class="form-control" name="id" id="" value="">
                                            <div class="form-error" id="form-error-id"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam ssn_block_params" data-field="ssn_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.ssn')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input type="text" class="form-control" name="ssn" id="" value="">
                                            <div class="form-error" id="form-error-ssn"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam first_name_block_params" data-field="first_name_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.first_name')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input type="text" class="form-control" name="first_name" id="" value="">
                                            <div class="form-error" id="form-error-first_name"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group searchParam last_name_block_params" data-field="last_name_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.last_name')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input type="text" class="form-control" name="last_name" id="" value="">
                                            <div class="form-error" id="form-error-last_name"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam authorized_user_block_params" data-field="authorized_user_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.user.authorized_user')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input type="text" class="form-control" name="authorized_user" id="" value="">
                                            <div class="form-error" id="form-error-authorized_user"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-8 col-lg-offset-3 text-right">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8">
                                            <button type="submit" class="btn btn-primary mr-10">{{trans('core.base.label.search')}}</button>
                                            <button type="button" class="btn btn-danger resetButton" data-submit="true">{{trans('core.base.label.reset')}}</button>
                                            <button type="button" class="btn btn-default hide-or-show-params" data-table-id="user-roles-data" data-form="search-filter-user-roles">{{trans('core.base.label.apply_hidden_columns')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <?php
                    $customColumnsForUserRoles = json_encode([
                        "id" => "ID",
                        "username" => trans('swis.monitoring.user_roles.username_and_ssn.label'),
                        "module" => trans('swis.monitoring.user_roles.module.label'),
                        "role_name" => trans('swis.monitoring.user_roles.role_name.label'),
                        "company_name" => trans('swis.monitoring.user_roles.company_name.label'),
                        "subdivision" => trans('swis.monitoring.user_roles.subdivision.label'),
                        "show_only_self" => trans('swis.monitoring.user_roles.show_only_self.label'),
                        "role_status" => trans('swis.monitoring.user_roles.role_status.label'),
                        "available_at" => trans('swis.monitoring.user_roles.available_at.label'),
                        "available_to" => trans('swis.monitoring.user_roles.available_to.label'),
                        "authorized_user" => trans('swis.monitoring.user_roles.authorized_user.label')])
                ?>
                <div class="data-table">
                    <div class="clearfix">
                        <table class="table table-striped table-bordered table-hover dataTables-example"
                                id="user-roles-data"
                                data-form-id="search-filter-user-roles"
                                data-module-for-search-params="Monitoring{{ hash('sha256', $companyTaxId.'user-roles-data') }}"
                                data-custom-columns="{{ $customColumnsForUserRoles }}">
                            <thead>
                            <tr>
                                <th data-col="id">ID</th>
                                <th data-col="username">{{trans('swis.monitoring.user_roles.username_and_ssn.label')}}</th>
                                <th data-col="module" >{{trans('swis.monitoring.user_roles.module.label')}}</th>
                                <th data-col="role_name" data-ordering="0">{{trans('swis.monitoring.user_roles.role_name.label')}}</th>
                                <th data-col="company_name" >{{trans('swis.monitoring.user_roles.company_name.label')}}</th>
                                <th data-col="subdivision"  data-ordering="0">{{trans('swis.monitoring.user_roles.subdivision.label')}}</th>
                                <th data-col="show_only_self" >{{trans('swis.monitoring.user_roles.show_only_self.label')}}</th>
                                <th data-col="role_status" >{{trans('swis.monitoring.user_roles.role_status.label')}}</th>
                                <th data-col="available_at" >{{trans('swis.monitoring.user_roles.available_at.label')}}</th>
                                <th data-col="available_to" >{{trans('swis.monitoring.user_roles.available_to.label')}}</th>
                                <th data-col="authorized_user" >{{trans('swis.monitoring.user_roles.authorized_user.label')}}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>

            <div id="tab-latest_events" class="tab-pane ">

                <div class="clearfix search__out">
                    <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse" data-target="#collapseLastEventsSearch" aria-expanded="false" aria-controls="collapseSearch">
                        <i class="fa fa-search"> </i><span class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
                    </button>
                </div>

                <div class="collapse" id="collapseLastEventsSearch">
                    <div class="card card-body">
                        <form class="form-horizontal fill-up" id="search-filter-applications">

                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group searchParam id_block_params" data-field="id_block_params">
                                        <label class="col-md-3 col-lg-3 control-label label__title">ID</label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-lg-10 col-md-8 field__one-col">
                                                    <input type="text" class="form-control" name="id" id="" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group searchParam document_name_block_params" data-field="document_name_block_params">
                                        <label class="col-md-3 col-lg-3 control-label label__title">{{ trans('swis.monitoring.last_events.document') }}</label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-lg-10 col-md-8 field__one-col">
                                                    <select class="select2" id="lastEventsDocuments" name="documentCode">
                                                        <option value="">{{ trans('swis.monitoring.last_events.select_document') }}</option>
                                                        @foreach($constructorDocuments as $constructorDocument)
                                                            <option value="{{ $constructorDocument->document_code }}">{{ $constructorDocument->document_name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group searchParam state_id_block_params" data-field="state_id_block_params">
                                        <label class="col-md-3 col-lg-3 control-label label__title">{{ trans('swis.user_documents.state.label') }}</label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-lg-10 col-md-8 field__one-col">
                                                    <select class="select2" disabled id="lastEventsApplicationStatus" name="state_id"></select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group searchParam saf_regime_block_params" data-field="saf_regime_block_params">
                                        <label class="col-md-3 col-lg-3 control-label label__title">{{ trans('swis.monitoring.last_events.status') }}</label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-lg-10 col-md-8 field__one-col">
                                                    <select class="select2" name="saf_regime">
                                                        <option></option>
                                                        @foreach(App\Models\SingleApplication\SingleApplication::REGIMES as $regime)
                                                            <option value="{{ $regime }}">{{ trans('swis.'.$regime.'_title') }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group searchParam saf_number_block_params" data-field="saf_number_block_params">
                                        <label class="col-md-3 col-lg-3 control-label label__title">{{ trans('swis.single_app.regular_number') }}</label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-lg-10 col-md-8 field__one-col">
                                                    <input type="text" class="form-control" value="" name="saf_number">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group searchParam permission_number_block_params" data-field="permission_number_block_params">
                                        <label class="col-md-3 col-lg-3 control-label label__title">{{ trans('swis.sub_application.permission_number.title') }}</label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-lg-10 col-md-8 field__one-col">
                                                    <input type="text" class="form-control" value="" name="permission_number">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group searchParam registration_number_block_params" data-field="registration_number_block_params">
                                        <label class="col-md-3 col-lg-3 control-label label__title">{{ trans('swis.sub_application.registration_number.title') }}</label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-lg-10 col-md-8 field__one-col">
                                                    <input type="text" class="form-control" value="" name="registration_number">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group searchParam saf_applicant_user_block_params" data-field="saf_applicant_user_block_params">
                                        <label class="col-md-3 col-lg-3 control-label label__title">{{ trans('swis.sub_application.saf_applicant_user_tin.title') }}</label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-lg-10 col-md-8 field__one-col">
                                                    <input type="text" class="form-control" value="" name="saf_applicant_user">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-6">

                                    <div class="form-group searchParam sub_app_number_block_params" data-field="sub_app_number_block_params">
                                        <label class="col-md-3 col-lg-3 control-label label__title">{{ trans('swis.user_document.sub_application_number') }}</label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-lg-10 col-md-8 field__one-col">
                                                    <input type="text" class="form-control" value="" name="sub_app_number">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group searchParam sub_application_express_control_block_params" data-field="sub_application_express_control_block_params">
                                        <label class="col-md-3 col-lg-3 control-label label__title">{{ trans('swis.user_document.express_control_id') }}</label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-lg-10 col-md-8 field__one-col">
                                                    <select class="select2" name="sub_application_express_control">
                                                        <option></option>
                                                        @foreach($expressControls as $item)
                                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group searchParam sub_application_agency_block_params" data-field="sub_application_agency_block_params">
                                        <label class="col-md-3 col-lg-3 control-label label__title">{{ trans('swis.sub_application.agency.title') }}</label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-lg-10 col-md-8 field__one-col">
                                                    <select class="select2" name="sub_application_agency" id="agencies">
                                                        <option></option>
                                                        @foreach($agencies as $agency)
                                                            <option value="{{ $agency->tax_id }}">{{ $agency->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group searchParam subdivision_block_params" data-field="subdivision_block_params">
                                        <label class="col-md-3 col-lg-3 control-label label__title">{{ trans('swis.regionalOffice.title') }}</label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-lg-10 col-md-8 field__one-col">
                                                    <select class="select2" name="subdivision" id="subdivision">
                                                        <option value=""></option>
                                                    </select>
                                                    <div class="form-error" id="form-error-subdivision"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group searchParam saf_applicant_tin_block_params" data-field="saf_applicant_tin_block_params">
                                        <label class="col-md-3 col-lg-3 control-label label__title">{{ trans('swis.sub_application.saf_applicant_tin.title') }}</label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-lg-10 col-md-8 field__one-col">
                                                    <input type="text" class="form-control" value="" name="saf_applicant_tin_or_name">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group searchParam saf_importer_tin_block_params" data-field="saf_importer_tin_block_params">
                                        <label class="col-md-3 col-lg-3 control-label label__title">{{ trans('swis.sub_application.saf_importer_tin_or_name.title') }}</label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-lg-10 col-md-8 field__one-col">
                                                    <input type="text" class="form-control" value="" name="saf_importer_tin_or_name">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group searchParam saf_exporter_tin_block_params" data-field="saf_exporter_tin_or_name_block_params">
                                        <label class="col-md-3 col-lg-3 control-label label__title">{{ trans('swis.sub_application.saf_exporter_tin_or_name.title') }}</label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-lg-10 col-md-8 field__one-col">
                                                    <input type="text" class="form-control" value="" name="saf_exporter_tin_or_name">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group searchParam payment_date_row row submitting_date_block_params" data-field="submitting_date_block_params">
                                        <label class="col-md-3 control-label label__title">{{ trans('swis.user_document.subapplication_submitting_date') }}</label>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-6 field__two-col">
                                                    <div class='input-group date'>
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                        <input value="" name="submitting_date_start_date_filter" type="text" placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                                    </div>
                                                </div>
                                                <span style=" position: absolute; top: 6px;margin-left: -5px;">-</span>
                                                <div class="col-md-6 field__two-col">
                                                    <div class='input-group date'>
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                        <input value="" name="submitting_date_end_date_filter" type="text" placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group searchParam payment_date_row row registration_date_block_params" data-field="registration_date_block_params">
                                        <label class="col-md-3 control-label label__title">{{ trans('swis.sub_application.registration_date.title') }}</label>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-6 field__two-col">
                                                    <div class='input-group date'>
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                        <input value="" name="registration_date_start_date_filter" type="text" placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                                    </div>
                                                </div>
                                                <span style=" position: absolute; top: 6px;margin-left: -5px;">-</span>
                                                <div class="col-md-6 field__two-col">
                                                    <div class='input-group date'>
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                        <input value="" name="registration_date_end_date_filter" type="text" placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-6">

                                    <div class="form-group searchParam payment_date_row row permission_date_block_params" data-field="permission_date_block_params">
                                        <label class="col-md-3 control-label label__title">{{ trans('swis.sub_application.permission_date.title') }}</label>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-6 field__two-col">
                                                    <div class='input-group date'>
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                        <input value="" name="permission_date_start_date_filter" type="text" placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                                    </div>
                                                </div>
                                                <span style=" position: absolute; top: 6px;margin-left: -5px;">-</span>
                                                <div class="col-md-6 field__two-col">
                                                    <div class='input-group date'>
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                        <input value="" name="permission_date_end_date_filter" type="text" placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group searchParam payment_date_row row expiration_date_block_params" data-field="expiration_date_block_params">
                                        <label class="col-md-3 control-label label__title">{{ trans('swis.sub_application.expiration_date.title') }}</label>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-6 field__two-col">
                                                    <div class='input-group date'>
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                        <input value="" name="expiration_date_start_date_filter" type="text" placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                                    </div>
                                                </div>
                                                <span style=" position: absolute; top: 6px;margin-left: -5px;">-</span>
                                                <div class="col-md-6 field__two-col">
                                                    <div class='input-group date'>
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                        <input value="" name="expiration_date_end_date_filter" type="text" placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-8 col-lg-offset-3 text-right">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8">
                                            <button type="submit" class="btn btn-primary mr-10">{{trans('core.base.label.search')}}</button>
                                            <button type="button" class="btn btn-danger resetButton" data-submit="true">{{trans('core.base.label.reset')}}</button>
                                            <button type="button" class="btn btn-default hide-or-show-params" data-table-id="applications-data" data-form="search-filter-applications">{{trans('core.base.label.apply_hidden_columns')}}</button>
                                            <a href="{{ urlWithLng("monitoring/applications/export") }}" class="btn btn-default generate-xls">{{trans('swis.monitoring.generate.xls.title')}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <?php
                $customColumnsForApplications = json_encode([
                    "saf_sub_application_id" => "ID",
                    "state" => trans('swis.user_documents.state.label'),
                    "saf_status_type" => trans('swis.saf.status_type'),
                    "saf_number" => trans('swis.single_app.regular_number'),
                    "sub_application_send_date" => trans('swis.user_document.subapplication_submitting_date'),
                    "sub_application_number" => trans('swis.user_document.sub_application_number'),
                    "document_type" => trans('swis.user_document.sub_application_type'),
                    "sub_application_express_control" => trans('swis.user_document.express_control_id'),
                    "sub_application_agency" => trans('swis.sub_application.agency.title'),
                    "sub_application_agency_subdivision" => trans('swis.monitoring.user_roles.subdivision.label'),
                    "sub_application_registration_number" => trans('swis.sub_application.registration_number.title'),
                    "sub_application_registration_date" => trans('swis.sub_application.registration_date.title'),
                    "sub_application_permission_number" => trans('swis.sub_application.permission_number.title'),
                    "sub_application_permission_date" => trans('swis.sub_application.permission_date.title'),
                    "sub_application_expiration_date" => trans('swis.sub_application.expiration_date.title'),
                    "saf_importer" => trans('swis.sub_application.saf_importer_tin_or_name.title'),
                    "saf_exporter" => trans('swis.sub_application.saf_exporter_tin_or_name.title'),
                    "saf_applicant_tin" => trans('swis.sub_application.saf_applicant_tin.title'),
                    "saf_applicant_user" => trans('swis.sub_application.saf_applicant_user_tin.title'),
                ]);
                ?>

                <div class="data-table">
                    <div class="clearfix">
                        <table class="table table-striped table-bordered table-hover dataTables-example userDocTable"
                               id="applications-data"
                               data-module-for-search-params="Monitoring{{ hash('sha256', $companyTaxId.'applications-data') }}"
                               data-custom-columns="{{ $customColumnsForApplications }}"
                               data-form-id="search-filter-applications">
                            <thead>
                                <tr>
                                    <th data-col="saf_sub_application_id">ID</th>
                                    <th data-col="state" data-order-name="state">{{trans('swis.user_documents.state.label')}}</th>
                                    <th data-col="saf_status_type">{{trans('swis.saf.status_type')}}</th>
                                    <th data-col="saf_number">{{trans('swis.single_app.regular_number')}}</th>
                                    <th data-col="sub_application_send_date">{{trans('swis.user_document.subapplication_submitting_date')}}</th>
                                    <th data-col="sub_application_number">{{trans('swis.user_document.sub_application_number')}}</th>
                                    <th data-col="document_type">{{trans('swis.user_document.sub_application_type')}}</th>
                                    <th data-col="sub_application_express_control">{{trans('swis.user_document.express_control_id')}}</th>
                                    <th data-col="sub_application_agency">{{trans('swis.sub_application.agency.title')}}</th>
                                    <th data-col="sub_application_agency_subdivision">{{trans('swis.monitoring.user_roles.subdivision.label')}}</th>
                                    <th data-col="sub_application_registration_number">{{trans('swis.sub_application.registration_number.title')}}</th>
                                    <th data-col="sub_application_registration_date">{{trans('swis.sub_application.registration_date.title')}}</th>
                                    <th data-col="sub_application_permission_number">{{trans('swis.sub_application.permission_number.title')}}</th>
                                    <th data-col="sub_application_permission_date">{{trans('swis.sub_application.permission_date.title')}}</th>
                                    <th data-col="sub_application_expiration_date">{{trans('swis.sub_application.expiration_date.title')}}</th>
                                    <th data-col="saf_importer" data-ordering="0">{{trans('swis.sub_application.saf_importer_tin_or_name.title')}}</th>
                                    <th data-col="saf_exporter" data-ordering="0">{{trans('swis.sub_application.saf_exporter_tin_or_name.title')}}</th>
                                    <th data-col="saf_applicant_tin" data-ordering="0">{{trans('swis.sub_application.saf_applicant_tin.title')}}</th>
                                    <th data-col="saf_applicant_user" data-ordering="0">{{trans('swis.sub_application.saf_applicant_user_tin.title')}}</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>

            <div id="tab-payment_events" class="tab-pane">

                <div class="clearfix search__out">
                    <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse" data-target="#collapsePaymentsSearch" aria-expanded="false" aria-controls="collapseSearch">
                        <i class="fa fa-search"> </i><span class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
                    </button>
                </div>

                <div class="collapse" id="collapsePaymentsSearch">
                    <div class="card card-body">
                        <form class="form-horizontal fill-up" id="search-filter-payments">

                            <div class="form-group searchParam id_block_params" data-field="id_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">ID</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input type="text" class="form-control" name="id" id="" value="">
                                            <div class="form-error" id="form-error-id"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam id_block_params" data-field="id_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.payments.username.title')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input type="text" class="form-control" name="username" id="" value="">
                                            <div class="form-error" id="form-error-username"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam user_ssn_block_params" data-field="user_ssn_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.payments.user_ssn.title')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input type="text" class="form-control" name="user_ssn" id="" value="">
                                            <div class="form-error" id="form-error-user_ssn"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam company_name_block_params" data-field="company_name_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.payments.company_name.title')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input type="text" class="form-control" name="company_name" id="" value="">
                                            <div class="form-error" id="form-error-company_name"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{--<div class="form-group searchParam transaction_id_block_params" data-field="transaction_id_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.payments.transaction_id.title')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input type="text" class="form-control" name="transaction_id" id="" value="">
                                            <div class="form-error" id="form-error-transaction_id"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
--}}
                            <div class="form-group searchParam updated_at_block_params" data-field="updated_at_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.payments.transaction_date.label')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-4 col-lg-5 field__two-col">
                                            <div class='input-group date'>
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                <input value="" name="updated_at_date_start" autocomplete="off"
                                                       type="text"
                                                       placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-5 field__two-col">
                                            <div class='input-group date'>
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                <input value="" name="updated_at_date_end" autocomplete="off"
                                                       type="text"
                                                       placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam amount_block_params" data-field="amount_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.payments.amount.title')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input type="text" class="form-control" name="amount" id="" value="">
                                            <div class="form-error" id="form-error-amount"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-8 col-lg-offset-3 text-right">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8">
                                            <button type="submit" class="btn btn-primary mr-10">{{trans('core.base.label.search')}}</button>
                                            <button type="button" class="btn btn-danger resetButton" data-submit="true">{{trans('core.base.label.reset')}}</button>
                                            <button type="button" class="btn btn-default hide-or-show-params" data-table-id="payments-data" data-form="search-filter-payments">{{trans('core.base.label.apply_hidden_columns')}}</button>
                                            <a href="{{ urlWithLng("monitoring/payments/export") }}" class="btn btn-default generate-xls">{{trans('swis.monitoring.generate.xls.title')}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <?php
                    $customColumnsForPaymentsData = json_encode([
                        "id" => "ID",
                        "username" => trans('swis.payments.username.title'),
                        "user_ssn" => trans('swis.payments.user_ssn.title'),
                        "company_name" => trans('swis.payments.company_name.title'),
                        "transaction_id" => trans('swis.payments.transaction_id.title'),
                        "updated_at" => trans('swis.payments.transaction_date.title'),
                        "saf_number" => trans('swis.payments.saf_number.title'),
                        "document_number" => trans('swis.payments.sub_application_number.title'),
                        "agency" => trans('swis.payments.agency.title'),
                        "subdivision" => trans('swis.payments.subdivision.title'),
                        "document" => trans('swis.payments.document.title'),
                        "obligation_type" => trans('swis.payments.obligation_type.title'),
                        "amount" => trans('swis.payments.amount.title'),
                    ]);
                ?>
                <div class="data-table">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example"
                                   id="payments-data"
                                   data-form-id="search-filter-payments"
                                   data-custom-columns="{{ $customColumnsForPaymentsData }}"
                                   data-module-for-search-params="Monitoring{{ hash('sha256', $companyTaxId.'payments-data') }}">
                                <thead>
                                    <tr>
                                        <th data-col="id">ID</th>
                                        <th data-col="username">{{ trans('swis.payments.username.title') }}</th>
                                        <th data-col="user_ssn">{{ trans('swis.payments.user_ssn.title') }}</th>
                                        <th data-col="company_name">{{ trans('swis.payments.company_name.title') }}</th>
                                        <th data-col="payment_id">{{ trans('swis.payments.transaction_id.title') }}</th>
                                        <th data-col="transaction_date">{{ trans('swis.payments.transaction_date.title') }}</th>
                                        <th data-col="saf_number">{{ trans('swis.payments.saf_number.title') }}</th>
                                        <th data-col="document_number">{{ trans('swis.payments.sub_application_number.title') }}</th>
                                        <th data-col="agency">{{ trans('swis.payments.agency.title') }}</th>
                                        <th data-col="subdivision">{{ trans('swis.payments.subdivision.title') }}</th>
                                        <th data-col="document">{{ trans('swis.payments.document.title') }}</th>
                                        <th data-col="obligation_type">{{ trans('swis.payments.obligation_type.title') }}</th>
                                        <th data-col="amount">{{ trans('swis.payments.amount.title') }}</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div id="tab-pays" class="tab-pane">

                <div class="clearfix search__out">
                    <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse" data-target="#collapsePaysSearch" aria-expanded="false" aria-controls="collapseSearch">
                        <i class="fa fa-search"> </i><span class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
                    </button>
                </div>

                <div class="collapse" id="collapsePaysSearch">
                    <div class="card card-body">
                        <form class="form-horizontal fill-up" id="search-filter-pays">

                            <div class="form-group searchParam id_block_params" data-field="id_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">ID</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input type="text" class="form-control" name="id" id="" value="">
                                            <div class="form-error" id="form-error-id"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam user_name_block_params" data-field="user_name_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.pays.user_name.title')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input type="text" class="form-control" name="user_name" id="" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam user_ssn_block_params" data-field="user_ssn_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.pays.user_ssn.title')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input type="text" class="form-control" name="user_ssn" id="" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam transaction_id_block_params" data-field="transaction_id_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.pays.transaction_id.title')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input type="text" class="form-control" name="transaction_id" id="" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam updated_at_block_params" data-field="updated_at_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.pays.transaction_date.label')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-4 col-lg-5 field__two-col">
                                            <div class='input-group date'>
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                <input value="" name="payment_date_date_start" autocomplete="off"
                                                       type="text"
                                                       placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-5 field__two-col">
                                            <div class='input-group date'>
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                <input value="" name="payment_date_date_end" autocomplete="off"
                                                       type="text"
                                                       placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam company_name_block_params" data-field="company_name_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.pays.company_name.title')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input type="text" class="form-control" name="company_name" id="" value="">
                                            <div class="form-error" id="form-error-company_name"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam company_name_block_params" data-field="company_name_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.pays.provider.title')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <select class="select2" name="payment_provider_id">
                                                <option></option>
                                                @foreach($paymentProviders as $paymentProvider)
                                                    <option value="{{ $paymentProvider->id }}">{{ $paymentProvider->name }}</option>
                                                @endforeach
                                            </select>
                                            <div class="form-error" id="form-error-payment_provider_id"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-8 col-lg-offset-3 text-right">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8">
                                            <button type="submit" class="btn btn-primary mr-10">{{trans('core.base.label.search')}}</button>
                                            <button type="button" class="btn btn-danger resetButton" data-submit="true">{{trans('core.base.label.reset')}}</button>
                                            <button type="button" class="btn btn-default hide-or-show-params" data-table-id="pays-data" data-form="search-filter-pays">{{trans('core.base.label.apply_hidden_columns')}}</button>
                                            <a href="{{ urlWithLng("monitoring/pays/export") }}" class="btn btn-default generate-xls">{{trans('swis.monitoring.generate.xls.title')}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <?php
                $customColumnsForPays = json_encode([
                    "id" => "ID",
                    "user_name" => trans('swis.pays.username.title'),
                    "user_ssn" => trans('swis.pays.user_ssn.title'),
                    "transaction_id" => trans('swis.pays.transaction_id.title'),
                    "payment_date" => trans('swis.pays.transaction_date.title'),
                    "amount" => trans('swis.pays.amount.title'),
                    "provider" => trans('swis.pays.provider.title'),
                    "company_tax_id" => trans('swis.pays.company_tax_id.title'),
                    "company_name" => trans('swis.pays.company_name.title')
                ]);
                ?>
                <div class="data-table">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example"
                                   id="pays-data"
                                   data-form-id="search-filter-pays"
                                   data-custom-columns="{{ $customColumnsForPays }}"
                                   data-module-for-search-params="Monitoring{{ hash('sha256', $companyTaxId.'pays') }}">
                                <thead>
                                <tr>
                                    <th data-col="id">ID</th>
                                    <th data-col="user_name">{{ trans('swis.pays.username.title') }}</th>
                                    <th data-col="user_ssn">{{ trans('swis.pays.user_ssn.title') }}</th>
                                    <th data-col="transaction_id">{{ trans('swis.pays.transaction_id.title') }}</th>
                                    <th data-col="payment_date">{{ trans('swis.pays.transaction_date.title') }}</th>
                                    <th data-col="amount">{{ trans('swis.pays.amount.title') }}</th>
                                    <th data-col="provider">{{ trans('swis.pays.provider.title') }}</th>
                                    <th data-col="company_tax_id">{{ trans('swis.pays.company_tax_id.title') }}</th>
                                    <th data-col="company_name">{{ trans('swis.pays.company_name.title') }}</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="hideOrShowParams" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{ trans('swis.user_documents.hide_params_columns_modal.title') }}</h4>
                </div>
                <div class="modal-body" id="hideOrShowParamsModalContent">
                    <div class="row text-center modal-load-body"><i class="fa fa-spinner fa-spin" style="font-size: 18px;"></i></div>
                    <div class="row modal-main-body" style="display: none;">
                        <div class="col-md-6 hide-params-section">
                            <label>{{ trans('swis.user_documents.search.hide_params.label') }}</label><br /><br />
                            <div class="hide-params-section-body"></div>
                        </div>
                        <div class="col-md-6 hide-column-section">
                            <label>{{ trans('swis.user_documents.search.hide_columns.label') }}</label><br /><br />

                            <div class="hide-columns-section-body">

                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary restoreButton" data-module="">{{trans('core.base.label.restore')}}</button>
                    <button type="button" class="btn btn-danger hideButton" data-module="">{{trans('core.base.label.hide')}}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('core.base.button.close') }}</button>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('js/monitoring/main.js')}}"></script>
@endsection