@extends('layouts.app')

@section('title'){{trans('swis.constructor_role.title')}}@stop

@section('contentTitle') {{trans('swis.constructor_role.title')}} @stop

@section('form-buttons-top')
    @if ($selectedConstructorDocumentId)
        {!! renderEditButton() !!}
    @endif
@stop

@section('content')

    <form class="form-horizontal fill-up" id="search-filter" action="{{ urlWithLng('constructor-roles') }}">
        <div class="form-group">
            <label class="col-md-4 col-lg-3 control-label">{{trans('swis.document.select_document')}}</label>
            <div class="col-md-6 col-lg-6">
                <select class="form-control select2 document-list auto-search-filter default-search" name="documentID">
                    <option value=""></option>
                    @if ($constructorDocuments->count())
                        @foreach ($constructorDocuments as $constructorDocument)
                            <option value="{{ $constructorDocument->id }}" {{ $selectedConstructorDocumentId == $constructorDocument->id ? 'selected' : '' }}>{{ $constructorDocument->document_name }}</option>
                        @endforeach
                    @endif
                </select>
                <div class="form-error" id="form-error-documentID"></div>
            </div>
        </div>
    </form>

    @if ($selectedConstructorDocumentId)
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-md-4 col-lg-3 control-label">{{trans('swis.constructor.select_roles')}}</label>
                <div class="col-md-6 col-lg-6">
                    <select class="form-control select2-ajax" id="select-role" data-url="{{urlWithLng('constructor-roles/roles')}}" data-min-input-length="1" name="roles"></select>
                    <div class="form-error" id="form-error-roles"></div>
                </div>
                <input type="hidden" value="{{ $forbiddenIDs }}" id="forbiddenIDs" />
            </div>
        </div>
    @endif

    <form action="" id="form-data">
        <div class="col-md-12 attributes">
            @if (isset($roles) && is_array($roles))
                @foreach ($roles as $role)
                    <div class='col-md-3 text-left' id="block-{{ $role['roleID'] }}">
                        <h3 class='text-center' style="height: 34px;"> <a target="_blank" href="{{urlWithLng('role/'.$role['roleID'].'/edit')}}">{{ $role['name'] }} {{ trans('swis.attributes_title') }} </a> <i class="far fa-times-circle remove-block" style="cursor: pointer;" data-id="{{$role['roleID']}}"></i></h3>
                        @if (isset($role['attributes']))
                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>{{ trans('swis.constructor_data_set.saf_name') }}</th>
                                        <th>{{ trans('swis.constructor_data_set.saf_tab') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($role['attributes'] as $attrID => $attribute)
                                        <tr>
                                            <td><input type='checkbox' id='checkbox-{{$role['roleID']}}-{{$attrID}}' name='attributes[{{ $role['roleID'] }}][{{ $attribute['type'] }}][{{ $attrID }}]' {{ $attribute['check'] ? 'checked' : '' }}></td>
                                            <td style="max-width: 100px; overflow-wrap: break-word;"><label for="checkbox-{{$role['roleID']}}-{{$attrID}}"> {{ $attribute['name'] }} </label></td>
                                            <td><label for="checkbox-{{$role['roleID']}}-{{$attrID}}"> {{ getSafAttrPath($attribute['path']) }} </label></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @endif
                        <input type="hidden" name="roles[]" value="{{ $role['roleID'] }}"/>
                    </div>
                @endforeach
            @endif
        </div>
    </form>
@endsection

@section('form-buttons-bottom')
    @if ($selectedConstructorDocumentId)
        {!! renderEditButton() !!}
    @endif
@stop

@section('scripts')
    <script>
        var documentID = "{{ $selectedConstructorDocumentId }}";
    </script>
    <script src="{{asset('js/constructor-role/main.js')}}"></script>
@endsection