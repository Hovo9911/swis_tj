@extends('layouts.app')

@section('title'){{trans('swis.constructor_role.title')}}@stop

@section('contentTitle')

    @switch($saveMode)
        @case('add')
        {{trans('swis.constructor_role.create.title')}}
        @break
        @case('edit')
        {{trans('swis.constructor_role.title')}}
        @break

    @endswitch

@stop

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! $saveMode == 'edit' ? renderDeleteButton() : '' !!}
    {!! renderCancelButton() !!}
@stop

@section('content')
    <?php

    switch ($saveMode) {
        case 'add':
            $url = 'constructor-roles';
            break;
        case 'edit':
            $url = 'constructor-roles/update/' . $constructorRole->id;

            $mls = $constructorRole->ml()->notDeleted()->base(['lng_id', 'role_name', 'description', 'show_status'])->keyBy('lng_id');
            break;
    }

    $languages = activeLanguages();
    ?>

    <form class="form-horizontal fill-up " id="form-data" action="{{urlWithLng($url)}}">

        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-general"> {{trans('core.base.tab.general')}}</a></li>
                @foreach($languages as $language)
                    <li class=""><a data-toggle="tab" href="#tab-{{$language->code}}">{{ trans("core.base.language.".strtolower($language->name)) }}</a></li>
                @endforeach
            </ul>
            <div class="tab-content">
                <div id="tab-general" class="tab-pane active">
                    <div class="panel-body">

                        <div class="form-group required"><label
                                    class="col-lg-2 control-label">Document</label>
                            <div class="col-lg-10">
                                <select class="form-control select2" name="constructor_document_id">
                                    <option></option>
                                    @foreach($constructors as $constructor)
                                        <option {{(isset($constructorRole) && $constructorRole->constructor_document_id == $constructor->id) ? 'selected': '' }} value="{{$constructor->id}}">{{$constructor->current()->document_name}}</option>
                                    @endforeach
                                </select>
                                <div class="form-error" id="form-error-constructor_id"></div>
                            </div>
                        </div>

                        @if($saveMode == 'edit')
                            <div class="form-group "><label class="col-lg-2 control-label">Role id</label>
                                <div class="col-lg-10">
                                    <input value="{{$constructorRole->role_id or ''}}" disabled="" type="text" placeholder="" class="form-control">
                                </div>
                            </div>
                        @endif

                        <div class="form-group "><label
                                    class="col-lg-2 control-label">Attributes list</label>
                            <div class="col-lg-10">
                                <select class="form-control select2" name="attributes">
                                    <option></option>
                                    <option value="">Attr1</option>
                                    <option value="">Attr2</option>
                                    <option value="">Attr94</option>
                                </select>
                                <div class="form-error" id="form-error-attributes"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">{{trans('core.base.label.status')}}</label>
                            <div class="col-lg-10 general-status">
                                <select name="show_status" class="form-show-status">
                                    <option value="{{\App\Models\ConstructorRoles\ConstructorRoles::STATUS_ACTIVE}}"{{(isset($constructorRole) && $constructorRole->show_status == \App\Models\ConstructorRoles\ConstructorRoles::STATUS_ACTIVE) || $saveMode == 'add' ?  ' selected="selected"' : ''}}>{{trans('core.base.label.status.active')}}</option>
                                    <option value="{{\App\Models\ConstructorRoles\ConstructorRoles::STATUS_INACTIVE}}"{{isset($constructorRole) &&  $constructorRole->show_status == \App\Models\ConstructorRoles\ConstructorRoles::STATUS_INACTIVE ? ' selected="selected"': ''}}>{{trans('core.base.label.status.inactive')}}</option>
                                </select>
                                <div class="form-error" id="form-error-show_status"></div>
                            </div>
                        </div>
                    </div>
                </div>

                @foreach($languages as $language)
                    <?php $lngId = $language->id ?>
                    <div id="tab-{{$language->code}}" class="tab-pane">
                        <div class="panel-body">


                            <div class="form-group required"><label
                                        class="col-lg-2 control-label">Role name</label>
                                <div class="col-lg-10">
                                    <input value="{{isset($mls[$lngId]) ? $mls[$lngId]->role_name : ''}}"
                                           name="ml[{{$language->id}}][role_name]" type="text"
                                           placeholder=""
                                           class="form-control">


                                    <div class="form-error"
                                         id="form-error-ml-{{$language->id}}-role_name"></div>
                                </div>
                            </div>

                            <div class="form-group required"><label
                                        class="col-lg-2 control-label">{{trans('core.base.description')}}</label>
                                <div class="col-lg-10">
                                    <textarea rows="6" name="ml[{{$language->id}}][description]"
                                              class="form-control">{{isset($mls[$lngId]) ? $mls[$lngId]->description : ''}}</textarea>
                                    <div class="form-error"
                                         id="form-error-ml-{{$language->id}}-description"></div>
                                </div>
                            </div>
                        </div>
                    </div>


                @endforeach
            </div>

            <input type="hidden" value="{{$constructorRole->id or 0}}" name="id" class="mc-form-key"/>

            {!! csrf_field() !!}
        </div>
    </form>

@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! $saveMode == 'edit' ? renderDeleteButton() : '' !!}
    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script src="{{asset('assets/helix/core/plugins/bootstrap-typeahead/bootstrap-typeahead.js')}}"></script>
    <script src="{{asset('js/constructor-role/main.js')}}"></script>
@endsection