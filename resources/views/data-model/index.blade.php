@extends('layouts.app')

@section('title'){{trans('swis.data_model_title')}}@stop

@section('contentTitle') {{trans('swis.data_model_title')}} @stop

@section('form-buttons-top')
    {!! renderAddButton() !!}
@stop

@section('content')

    <div class="clearfix search__out">
        <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse" data-target="#collapseSearch" aria-expanded="false" aria-controls="collapseSearch">
            <i class="fa fa-search"> </i><span class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
        </button>
    </div>
    <div class="collapse" id="collapseSearch">
        <div class="card card-body">
            <form class="form-horizontal fill-up" id="search-filter" autocomplete="off">
                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">ID</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="number" min="0" class="form-control onlyNumbers" name="id" id="" value="">
                                <div class="form-error" id="form-error-id"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.name')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="name" id="" value="">
                                <div class="form-error" id="form-error-name"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{ trans('swis.data-model.wco_id') }}</label>
                    <div class="col-md-8">
                        <div class="row">

                            <div class="col-lg-10 col-md-8 field__one-col">
                                <select class="select2 form-control" name="wco">
                                    @if($dataModels->count() > 0)
                                        <option value=""></option>
                                        @foreach($dataModels as $dataModel)
                                            <option value="{{$dataModel->wco}}">{{$dataModel->wco_id.' - '.$dataModel->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{ trans('swis.data-model.eaeu_id') }}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="eaec" id="" value="">
                                <div class="form-error" id="form-error-eaec"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{ trans('swis.data-model.reference_table') }}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <select class="form-control select2-ajax"
                                        data-url="{{urlWithLng('data-model/reference')}}" data-ref="all"
                                        name="reference_table">
                                </select>
                                <div class="form-error" id="form-error-reference_table"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{ trans('swis.data-model.definition') }}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="definition" value="">
                                <div class="form-error" id="form-error-definition"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{ trans('swis.data-model.type') }}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <select class="form-control select2" name="type">
                                    <option value=""></option>
                                    @foreach(\App\Models\DataModel\DataModel::MDM_ELEMENT_TYPES_ALL as $type)
                                        <option value="{{$type}}">{{ trans("swis.data-model.type.{$type}.label") }}</option>
                                    @endforeach
                                </select>
                                <div class="form-error" id="form-error-type"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8 col-lg-offset-3 text-right">
                        <div class="row">
                            <div class="col-lg-10 col-md-8">
                                <button type="submit" class="btn btn-primary mr-10">{{trans('core.base.label.search')}}</button>
                                <button type="button" class="btn btn-danger resetButton" data-submit="true">{{trans('core.base.label.reset')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="data-table">
        <div class="clearfix">
            <table class="table table-striped table-bordered table-hover dataTables-example data-model-table"
                   id="list-data">
                <thead>
                <tr>
                    <th data-col="actions" data-delete="0" class="th-actions">{{trans('core.base.label.actions')}}</th>
                    <th data-col="id">{{ trans('swis.data-model.label.id') }}</th>
                    <th data-col="name">{{trans('core.base.label.name')}}</th>
                    <th data-col="client_min">{{ trans('swis.data-model.label.min') }}</th>
                    <th data-col="client_max">{{ trans('swis.data-model.label.max') }}</th>
                    <th data-col="wco">{{ trans('swis.data-model.label.wco') }}</th>
                    <th data-col="eaec">{{ trans('swis.data-model.label.eaeu') }}</th>
                    <th data-col="type">{{ trans('swis.data-model.label.type') }}</th>
                    <th data-col="show_status">{{trans('core.base.label.status')}}</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('js/data-model/main.js')}}"></script>
@endsection