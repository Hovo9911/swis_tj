@extends('layouts.app')

@section('title'){{trans('swis.data_model.title')}}@stop

@section('contentTitle')

    @switch($saveMode)
        @case('add')
        {{trans('swis.data_model.create.title')}}
        @break
        @case('edit')
        {{trans('swis.data_model.title')}}
        @break

    @endswitch

@stop

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('content')
    <?php

    switch ($saveMode) {
        case 'add':
            $url = 'data-model';

            break;
        default:
            $url = $saveMode != 'view' ? 'data-model/update/' . $dataModel->id: '';

            $mls = $dataModel->ml()->notDeleted()->base(['lng_id', 'description', 'show_status'])->keyBy('lng_id');

            $type = 'number';
            if ($dataModel->type == 'd') {
                $type = 'date';
            }

            if ($dataModel->type == 'dt') {
                $type = 'datetime-local';
            }

            break;
    }

    $languages = activeLanguages();

    $hideClientsRange = false;
    $showDecimalFraction = false;
    if ($saveMode != 'add' && ($dataModel->type == 'd' || $dataModel->type == 'dt')) {
        $hideClientsRange = true;
    }

    if ($saveMode != 'add' && $dataModel->type == 'dc') {
        $showDecimalFraction = true;
    }

    ?>

    @include('partials.translations',['transType'=>'modal'])

    <form class="form-horizontal fill-up " id="form-data" action="{{urlWithLng($url)}}" autocomplete="off">

        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-general"> {{trans('core.base.tab.general')}}</a></li>
                @foreach($languages as $language)
                    <li>
                        <a data-toggle="tab" href="#tab-{{$language->code}}">{{ trans("core.base.language.".strtolower($language->name)) }}</a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content">
                <div id="tab-general" class="tab-pane active">
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-lg-3 control-label">{{ trans('swis.data-model.reference') }}</label>
                            <div class="col-lg-8">
                                @if(empty($dataModel->reference))
                                    <select class="form-control select2-ajax" data-url="{{urlWithLng('data-model/reference')}}" name="reference"></select>
                                @else

                                    @php ($reference = \App\Models\ReferenceTable\ReferenceTable::where('table_name',$dataModel->reference)->first())

                                    @if(!is_null($reference))
                                        <input class="form-control" type="text" disabled value="{{$reference->current()->name}}">
                                    @endif

                                @endif

                                <div class="form-error" id="form-error-reference"></div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-lg-3 control-label">{{trans('swis.base.technical_name')}}</label>
                            <div class="{{$saveMode == 'edit' ? 'col-lg-6' : 'col-lg-8' }}">
                                <input value="{{$dataModel->name or ''}}" name="name" type="text" placeholder="" class="form-control">
                                <div class="form-error" id="form-error-name"></div>
                            </div>

                            @if($saveMode == 'edit')
                                <div class="col-lg-2">
                                    <label class="col-lg-3 text-right">ID</label>
                                    <div class="col-lg-9 no-padding">
                                        <input class="form-control" value="{{$dataModel->id}}" disabled>
                                    </div>
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">{{ trans('swis.data-model.definition') }}</label>
                            <div class="col-lg-8">
                                <input value="{{$dataModel->definition or ''}}" name="definition" type="text" placeholder="" class="form-control">
                                <div class="form-error" id="form-error-definition"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">{{ trans('swis.data-model.wco_id') }}</label>
                            <div class="col-lg-8">
                                <input value="{{$dataModel->wco_id or ''}}" id="wcoId" name="wco_id" type="text"
                                       placeholder="{{trans('swis.document.label.autocomplete')}}" class="form-control autocomplete" data-type="wco"
                                       data-value="">
                                <div class="form-error" id="form-error-wco_id"></div>
                                <input type="hidden" name="wco" value="{{$dataModel->wco or ''}}">
                            </div>
                        </div>

                        <div class="form-group"><label class="col-lg-3 control-label">{{ trans('swis.data-model.wco_class_name') }}</label>
                            <div class="col-lg-8">
                                <select class="form-control select2" {{($saveMode == 'edit' && !empty($dataModel->wco_id )) ? '' : 'disabled'}} id="wcoClassName" name="wco_class_name">
                                    <option></option>
                                    @if(isset($wcoClassNames))
                                        @foreach($wcoClassNames as $key=>$className)
                                            <option {{($dataModel->wco_class_name == $className) ? 'selected' : ''}} value="{{$className}}">{{$wcoClassNamesAndCodes[$key]}}</option>
                                        @endforeach
                                    @endif
                                </select>

                                <div class="form-error" id="form-error-wco_class_name"></div>
                            </div>
                        </div>

                        <div class="form-group"><label class="col-lg-3 control-label">{{ trans('swis.data-model.eaeu_type') }}</label>
                            <div class="col-lg-8">
                                <select class="form-control select2" name="eaeu_type" id="eaeuType">
                                    <option></option>
                                    <option {{(isset($dataModel) && $dataModel->eaeu_type == 'SimpleData' ? 'selected' : '')}} value="SimpleData">
                                        SimpleData
                                    </option>
                                    <option {{(isset($dataModel) && $dataModel->eaeu_type == 'ComplexData' ? 'selected' : '')}} value="ComplexData">
                                        ComplexData
                                    </option>
                                    <option {{(isset($dataModel) && $dataModel->eaeu_type == 'ComplexDataSequences' ? 'selected' : '')}} value="ComplexDataSequences">
                                        ComplexDataSequences
                                    </option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">{{ trans('swis.data-model.eaeu_id') }}</label>
                            <div class="col-lg-8">
                                <input value="{{$dataModel->eaec_id or ''}}" {{($saveMode == 'edit') ? '' : 'readonly'}} data-values="" name="eaec_id"
                                       id="eaeuId" type="text"
                                       placeholder="{{trans('swis.document.label.autocomplete')}}" class="form-control autocomplete" data-type="eaeu">
                                <div class="form-error" id="form-error-eaec_id"></div>
                                <input type="hidden" name="eaec" value="{{$dataModel->eaec or ''}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-3 col-lg-offset-3">
                                <button type="button" class="btn btn-success btn--add" id="addValues">{{trans('swis.data_model.suggest.button')}}</button>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-3 ">
                                <div class="col-lg-2">
                                    <label>{{ trans('swis.data-model.eaeu_min') }}</label>
                                    <input name="eaeu_min" class="form-control" id="eaeuMin" readonly value="{{$dataModel->eaeu_min or ''}}">
                                </div>
                                <div class="col-lg-2">
                                    <label>{{ trans('swis.data-model.eaeu_max') }}</label>
                                    <input name="eaeu_max" class="form-control" id="eaeuMax" readonly value="{{$dataModel->eaeu_max or ''}}">
                                </div>
                                <div class="col-lg-2">
                                    <label>{{ trans('swis.data-model.eaeu_type') }}</label>
                                    <input name="found_type" class="form-control" id="eaeuDbType" readonly value="{{$dataModel->found_type or ''}}">
                                </div>
                                <div class="col-lg-2">
                                    <label>{{ trans('swis.data-model.eaeu_total_digits') }}</label>
                                    <input name="eaeu_total_digits" class="form-control" id="eaeuDbTotalDigits" readonly value="{{$dataModel->eaeu_total_digits or ''}}">
                                </div>
                                <div class="col-lg-2">
                                    <label>{{ trans('swis.data-model.eaeu_fraction_digits') }}</label>
                                    <input name="eaeu_fraction_digits" class="form-control" id="eaeuDbFractionDigits" readonly value="{{$dataModel->eaeu_fraction_digits or ''}}">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">

                        <div class="form-group">
                            <label class="col-lg-3 control-label">{{trans('core.base.label.type')}}</label>
                            <div class="col-lg-8">

                                @foreach(\App\Models\DataModel\DataModel::MDM_ELEMENT_TYPES_ALL as $mdmElement)
                                    <label class="radio-inline">
                                        <input type="radio" {{isset($dataModel) ? ($dataModel->type == $mdmElement) ? 'checked' : '' : $mdmElement == 'a' ? 'checked' : ''}} name="type" value="{{$mdmElement}}">{{ trans('swis.data-model.type.'.$mdmElement.'.label') }}
                                    </label>
                                @endforeach

                                <div class="form-error" id="form-error-type"></div>
                            </div>
                        </div>

                        <div class="row" id="isContentBlock" style="display: {{ isset($dataModel) && ($dataModel->type == 'a' || $dataModel->type == 'an') ? 'display' : 'none' }}">
                            <label class="col-lg-3 control-label">{{trans('swis.data-model.is_content.label')}}</label>
                            <div class="col-lg-8">
                                <input type="checkbox" style="margin: 12px 0px;" name="is_content" {{ isset($dataModel) && ($dataModel->is_content) ? 'checked' : '' }}>
                            </div>
                        </div>

                        <div class="row info-section">
                            <div class="col-md-8 col-md-offset-3">
                                <div class="panel-default">
                                    <div class="panel-heading">
                                        Info
                                    </div>
                                    <div class="panel-body">

                                        <div class="form-group required {{($hideClientsRange) ? 'hide' :''}}"><label class="col-lg-2 control-label">{{ trans('swis.data-model.client_min') }}</label>
                                            <div class="col-lg-8">
                                                <input value="{{$dataModel->client_min or ''}}"
                                                       name="client_min" type="{{$type or 'number'}}" placeholder=""
                                                       class="form-control min-max">
                                                <div class="form-error" id="form-error-client_min"></div>
                                            </div>
                                        </div>

                                        <div class="form-group required {{($hideClientsRange) ? 'hide' :''}}"><label class="col-lg-2 control-label">{{ trans('swis.data-model.client_max') }}</label>
                                            <div class="col-lg-8">
                                                <input value="{{$dataModel->client_max or ''}}"
                                                       name="client_max" type="{{$type or 'text'}}" placeholder=""
                                                       class="form-control min-max">
                                                <div class="form-error" id="form-error-client_max"></div>
                                            </div>
                                        </div>

                                        {{--<div class="form-group"><label
                                                    class="col-lg-3 control-label">DB Min</label>
                                            <div class="col-lg-8">
                                                <input value="{{$dataModel->db_min or ''}}"
                                                       name="db_min" type="{{$type or 'number'}}" placeholder=""
                                                       class="form-control min-max">
                                                <div class="form-error" id="form-error-db_min"></div>
                                            </div>
                                        </div>--}}

                                        <div class="form-group required {{($hideClientsRange) ? 'hide' :''}}"><label class="col-lg-2 control-label">{{ trans('swis.data-model.db_max') }}</label>
                                            <div class="col-lg-8">
                                                <input value="{{$dataModel->db_max or ''}}"
                                                       name="db_max" type="{{$type or 'text'}}" placeholder=""
                                                       class="form-control min-max">
                                                <div class="form-error" id="form-error-db_max"></div>
                                            </div>
                                        </div>

                                        <div class="form-group"><label class="col-lg-2 control-label">{{ trans('swis.data-model.db_type') }}</label>
                                            <div class="col-lg-8">
                                                <input type="text" readonly
                                                       value="{{($saveMode == 'add') ? 'varchar' : (($saveMode == 'edit') ? $dataModel->db_type  : '')}}"
                                                       class="form-control" name="db_type" id="dbType">
                                                <div class="form-error" id="form-error-db_type"></div>
                                            </div>
                                        </div>

                                        <div class="form-group required {{($showDecimalFraction) ? '' :'hide'}}"><label class="col-lg-2 control-label">{{ trans('swis.data-model.decimal.fraction') }}</label>
                                            <div class="col-lg-8">
                                                <input value="{{$dataModel->decimal_fraction or ''}}"
                                                       name="decimal_fraction" type="text" placeholder=""
                                                       class="form-control decimal required">
                                                <div class="form-error" id="form-error-decimal_fraction"></div>
                                            </div>
                                        </div>

                                        <div class="form-group"><label class="col-lg-2 control-label">{{ trans('swis.data-model.pattern') }}</label>
                                            <div class="col-lg-8">
                                                <input value="{{$dataModel->pattern or ''}}"
                                                       name="pattern" type="text" placeholder="/^[1-9]\d*$/"
                                                       class="form-control">
                                                <div class="form-error" id="form-error-pattern"></div>
                                            </div>
                                        </div>

                                        <div class="form-group {{($saveMode == 'edit') ? '' :'hide'}}"><label class="col-lg-2 control-label">{{ trans('swis.data-model.iterface_format') }}</label>
                                            <div class="col-lg-8">
                                                <input value="{{$dataModel->interface_format or ''}}"
                                                       name="interface_format" type="text" placeholder=""
                                                       class="form-control readonly">
                                                <div class="form-error" id="form-error-interface_format"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <br/>

                        <div class="form-group required"><label class="col-lg-3 control-label">{{trans('core.base.label.start')}}</label>
                            <div class="col-lg-8">
                                <div class='input-group date'>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                    <input value="{{ ($saveMode == 'edit') ? (isset($dataModel) && !empty($dataModel->available_at) ?  formattedDate($dataModel->available_at) : ''  ) :  currentDate()}}" {{($saveMode == 'edit' && !empty($dataModel->available_at) && $dataModel->available_at < currentDate()) ? 'readonly' : '' }} name="available_at" data-start-date="{{currentDate()}}" autocomplete="off" type="text"  placeholder="{{setDatePlaceholder()}}" class="form-control "/>
                                </div>
                                <div class="form-error" id="form-error-available_at"></div>
                            </div>
                        </div>

                        <div class="form-group"><label class="col-lg-3 control-label">{{trans('core.base.label.end')}}</label>
                            <div class="col-lg-8">

                                <div class='input-group date'>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                    <input value="{{isset($dataModel) ? formattedDate($dataModel->available_to) : ''}}" {{($saveMode == 'edit' && (!empty($dataModel->available_to && $dataModel->available_to < currentDate())) ) ? 'readonly' : '' }} name="available_to" autocomplete="off" type="text"  placeholder="{{setDatePlaceholder()}}" class="form-control "/>
                                </div>

                                <div class="form-error" id="form-error-available_to"></div>
                            </div>
                        </div>

                    </div>
                </div>
                </div>
                @foreach($languages as $language)
                    <?php $lngId = $language->id ?>
                    <div id="tab-{{$language->code}}" class="tab-pane">
                        <div class="panel-body">

                            <div class="form-group"><label class="col-lg-3 control-label">{{trans('core.base.description')}}</label>
                                <div class="col-lg-8">
                                    <textarea rows="6" name="ml[{{$language->id}}][description]" class="form-control">{{isset($mls[$lngId]) ? $mls[$lngId]->description : ''}}</textarea>
                                    <div class="form-error" id="form-error-ml-{{$language->id}}-description"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <input type="hidden" value="{{$dataModel->id or 0}}" name="id" class="mc-form-key"/>

        {!! csrf_field() !!}
    </form>


@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script src="{{asset('assets/helix/core/plugins/bootstrap-typeahead/bootstrap-typeahead.js')}}"></script>
    <script src="{{asset('js/data-model/main.js')}}"></script>
@endsection