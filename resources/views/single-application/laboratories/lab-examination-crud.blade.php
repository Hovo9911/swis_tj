<?php
    $measurementUnits = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, false, false, ['id', 'code', 'name']);

    $selectedProducts = (!empty($data)) ? $data['products'] : [
        [
            'product_id' => '',
            'sample_quantity' => '',
            'sample_measurement_unit' => '',
            'sample_number' => ''
        ]
    ];
?>
<form id="labExaminationForm">
    <div class="row" style="margin-top: 50px;">
    @if ($type == App\Models\Laboratory\Laboratory::SELECT_INDICATOR_BY_PRODUCT_INDICATOR)

        <div class="labExaminationCrudProductsContainer">
            @include("single-application.laboratories.lab-examination-crud-products", ['single' => true, 'disabled' => (isset($disabled) && $disabled)])
        </div>

        <div class="col-md-4 text-center border-right">
            <p>{{ trans("swis.saf.lab_examination.select_indicator") }}</p>
            <div class="indicators-container text-left">
                <select class="form-control select2 indicatorSelect" {{ (isset($disabled) && $disabled) ? 'disabled' : '' }} multiple>
                    @if (isset($data['available_indicators']))
                        @foreach ($data['available_indicators'] as $availableIndicator)
                            <option value="{{ $availableIndicator->id }}" {{ !empty($data) && in_array($availableIndicator->id, $data['indicators']) ? 'selected' : '' }}>{{ "({$availableIndicator->code}) {$availableIndicator->name}" }}</option>
                        @endforeach
                    @endif
                </select>

                <div class="form-error" id="form-error-indicators"></div>
            </div>
        </div>
        <div class="col-md-3 text-center">
            <p>{{ trans("swis.saf.lab_examination.select_laboratory") }}</p>
            <select class="form-control select2" id="selectLabExpertiseLaboratory" {{ (isset($disabled) && $disabled) ? 'disabled' : '' }}>
                @if (isset($data['available_labs']))
                    @foreach ($data['available_labs'] as $lab)
                        <option value="{{$lab->id}}" {{ (isset($data,$data['agency_id']) &&  $data['agency_id'] == $lab->id) ? 'selected' : '' }}>{{ "({$lab->tax_id}) {$lab->name}" }}</option>
                    @endforeach
                @endif
            </select>

            <div class="form-error" id="form-error-laboratory_id"></div>
        </div>

    @elseif($type == App\Models\Laboratory\Laboratory::SELECT_PRODUCT_BY_SPHERE)

        <div class="labExaminationCrudProductsContainer">
            @include("single-application.laboratories.lab-examination-crud-products", ['single' => false])
        </div>

        <div class="col-md-6 text-center border-right">
            <p>{{ trans("swis.saf.lab_examination.select_laboratory") }}</p>
            <select class="form-control select2" id="selectLabExpertiseLaboratory" {{ (isset($disabled) && $disabled) ? 'disabled' : '' }}>
                @if (isset($data['available_labs']))
                    @foreach ($data['available_labs'] as $lab)
                        <option value="{{$lab->id}}" {{ (isset($data,$data['agency_id']) && $data['agency_id'] == $lab->id) ? 'selected' : '' }}>{{ "({$lab->tax_id}) {$lab->name}" }}</option>
                    @endforeach
                @endif
            </select>
            <div class="form-error" id="form-error-laboratory_id"></div>
        </div>

    @elseif($type == App\Models\Laboratory\Laboratory::SELECT_CUSTOM_SEND_TO_LAB)

        <div class="labExaminationCrudProductsContainer">
            @include("single-application.laboratories.lab-examination-crud-products", ['single' => false])
        </div>

        <div class="col-md-6 text-center border-right">
            <p>{{ trans("swis.saf.lab_examination.select_laboratory") }}</p>
            <select class="form-control select2" id="selectLabExpertiseLaboratory" {{ (isset($disabled) && $disabled) ? 'disabled' : '' }}>
                <option value=""></option>
                @foreach ($labs as $lab)
                    <option value="{{ $lab->id }}" {{ (isset($data,$data['agency_id']) && $data['agency_id'] == $lab->id) ? 'selected' : '' }}>{{ "({$lab->tax_id}) {$lab->name}" }}</option>
                @endforeach
            </select>
            <div class="form-error" id="form-error-laboratory_id"></div>
        </div>
    @endif
    @if (isset($labApplicationId))
        <input type="hidden" id="labApplicationId" value="{{ $labApplicationId }}" />
    @endif
</div>
</form>