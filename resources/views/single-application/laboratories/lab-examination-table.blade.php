<?php

use App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure;

$lastXML = SingleApplicationDataStructure::lastXml();

$labExaminationListTable = $lastXML->xpath("//*[@tableClass='labExaminationList']");

$columns = (array)$labExaminationListTable[0]->columns;
$columnsActions = $labExaminationListTable[0]->columnsActions;

$labExaminations = \App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications::select(array_merge($columns, ['id', 'status_date', 'access_token', 'pdf_hash_key', 'lab_type', 'send_lab_company_tax_id']))
    ->where('saf_number', $saf->regular_number)
    ->where('is_lab', true);

    if (isset($subApplication)) {
        $labExaminations->where('from_subapplication_id_for_labs', $subApplication->id);
        if (!isset($editable)) {
            $editable = false;
        }
    } else {
        $editable = true;
        $labExaminations->whereNull('from_subapplication_id_for_labs');
    }

    $labExaminations = $labExaminations->orderBy('id')->get();

    $totalIndicatorValue = \App\Models\SingleApplicationSubApplicationsLaboratory\SingleApplicationSubApplicationsLaboratory::totalIndicatorPrice($labExaminations);
?>
<div class="{{!$labExaminations->count() ? 'hide' : ''}}">
<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
            @foreach($columns as $column)
                <th>{{trans("swis.saf.lab_examination.{$column}.column")}}</th>
            @endforeach
            @if(!empty($columnsActions))
                <th style="max-width: 130px"></th>
            @endif
        </thead>

        <tbody>
            @foreach($labExaminations as $lab)
                <tr>
                    @foreach ($columns as $column)
                        <?php
                            $value = '';
                            switch ($column) {

                                case 'agency_id':
                                    $agency = \App\Models\Agency\Agency::select(DB::raw('COALESCE(short_name, name) as name'), 'tax_id')->where('id', $lab->{$column})->joinMl()->first();
                                    if (!is_null($agency)) {
                                        $value = "({$agency->tax_id}) {$agency->name}";
                                    }
                                    break;

                                case 'current_status':

                                    $subAppState = \App\Models\SingleApplicationSubApplicationStates\SingleApplicationSubApplicationStates::where(['saf_number' => $saf->regular_number, 'saf_subapplication_id' => $lab->id, 'is_current' => '1'])->first();

                                    if (!is_null($subAppState)) {
                                        if ($currentState = \App\Models\ConstructorStates\ConstructorStates::where('id', $subAppState->state_id)->first()) {
                                            $value = $currentState->current()->state_name;
                                        }
                                    }

                                    break;

                                case 'products':
                                    if (isset($subApplication)) {
                                        $value = $lab->productNumbers($data['applicationProductsKeyValue']);
                                    } else {
                                        $value = $lab->productNumbers($saf->productsOrderedList());
                                    }
                                    break;

                                case 'status_date':
                                    $value = formattedDate($lab->status_date) ?? '';
                                    $subAppState = \App\Models\SingleApplicationSubApplicationStates\SingleApplicationSubApplicationStates::where(['saf_number' => $saf->regular_number, 'saf_subapplication_id' => $lab->id, 'is_current' => '1'])->first();

                                    if (!is_null($subAppState)) {
                                        $value = $subAppState->created_at;
                                    }

                                    break;

                                case 'access_token':
                                    $value = $lab->access_token ?? '';
                                    break;

                                case 'document_number':
                                    $value = $lab->document_number ?? '';
                                    break;
                            }
                        ?>

                        <td>{{ $value ?? '' }}</td>
                    @endforeach
                    @if(!empty($columnsActions) && $columnsActions->actions)
                        <td class="text-nowrap">
                            @foreach($columnsActions->actions->action as $action)

                                @php($actionType = $action->attributes()->type)

                                @if($editable && is_null($lab->send_lab_company_tax_id) && ($actionType == 'delete' || $actionType == 'edit'))
                                        <button class="btn--icon {{$actionType}}-lab"
                                                data-status=""
                                                title="{{trans("swis.saf.lab_examination.{$actionType}.button")}}"
                                                data-toggle="tooltip"
                                                data-id="{{$lab->id}}"
                                                data-type="{{$lab->lab_type}}"
                                                type="button"
                                                data-superscribe="{{isset($superscribe) ? 'true' : 'false'}}"
                                                {{$actionType == 'delete' ? $disabled ?? '' : ''}}>
                                            <i class="{{$action->attributes()->icon}}"></i>
                                        </button>
                                    @continue
                                @endif

                                @if($editable && !empty($action->attributes()->ajax) && is_null($lab->send_lab_company_tax_id))
                                    <button class="btn--icon send-to-lab"
                                            data-status=""
                                            title="{{trans('swis.saf.lab_examination.send.button')}}"
                                            data-toggle="tooltip"
                                            data-id="{{$lab->id}}"
                                            type="button"
                                            {{$disabled ?? ''}}>
                                        <i class="{{$action->attributes()->icon}}"></i></button>
                                @endif

                                @if($action->attributes()->type == 'result')
                                    <?php
                                        $subAppState = \App\Models\SingleApplicationSubApplicationStates\SingleApplicationSubApplicationStates::where(['saf_subapplication_id' => $lab->id, 'is_current' => '1'])->first();
                                    ?>
                                    <button class="btn--icon getLabResult"
                                        data-status=""
                                        title="{{trans('swis.saf.lab_examination.result.button')}}"
                                        data-toggle="tooltip"
                                        data-id="{{$lab->id}}"
                                        data-type="{{$lab->lab_type}}"
                                        type="button"
                                        {{$disabled ?? ''}}>
                                        <i class="{{$action->attributes()->icon}}"></i>
                                    </button>

                                    @continue
                                @endif
                            @endforeach
                        </td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="form-group pull-right">
    <label class="col-md-7 control-label label__title">{{trans('swis.lab-examination-for-laboratory.total')}}</label>
    <div class="col-md-5">
        <div class="row">
            <div class="col-md-12 field__one-col">
                <input type="text" class="form-control readonly" name="" id="" value="{{ $totalIndicatorValue }}" disabled readonly>
            </div>
        </div>
    </div>
</div>
</div>