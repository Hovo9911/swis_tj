<?php
$editable = true;
if (isset($subApplication)) {
    $editable = $tab['editable'];
}
?>

<div class="lab-examination-form-container"></div>

<div class="lab-examination-table-container clearfix">
    @include('single-application.laboratories.lab-examination-table')
</div>

<div class="clearfix">
    <div class="row">
        <div class="col-md-12">
            @if($editable)
                @php($disabled = (isset($subApplication) && !isset($superscribe)) ? 'disabled' : '')
                <button type="button" class="btn btn-primary pull-left addLabExpertiseButton" {{ $disabled }}><i class="fa fa-plus"></i> {{ trans("swis.saf.lab_examination.add.button") }}</button>
            @endif
        </div>
    </div>
</div>