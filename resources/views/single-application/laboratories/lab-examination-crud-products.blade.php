<?php
    $selectedProductsIds = array_map(function ($value) {
        return (int)$value['product_id'];
    }, $selectedProducts);
?>
<div class="col-md-{{ (isset($single) && $single) ? '5' : '6' }} border-right">
    <p class="text-center">{{ trans("swis.saf.lab_examination.select_product") }}</p>
    @foreach ($selectedProducts as $key => $selectedProduct)
        <div class="multipleProductBlockLabExamination">
            <div class="col-md-{{ (isset($single) && $single) ? '12' : '10' }}">
                <select class="form-control select2 selectLabExpertiseProduct {{ (isset($single) && $single) ? 'selectLabExpertiseProductByProducts' : '' }}" >
                    <option value=""></option>
                    @foreach($products ?? [] as $product)
                        <option value="{{ $product->id }}" {{ ($product->id == $selectedProduct['product_id']) ? 'selected' : '' }} {{ (($product->id != $selectedProduct['product_id']) && in_array($product->id, $selectedProductsIds)) ? 'disabled="true"' : '' }}>
                            {{ "({$product->hs_code}) {$product->hs_code_path_name}" }}
                        </option>
                    @endforeach
                </select>

                <div class="form-error" id="form-error-products-{{$key}}-product_id"></div>
            </div>
            @if (isset($single) && !$single)
                <div class="col-md-2">
                    <div class="row">
                        <button type="button" class="btn btn-primary addNewProductLabExamination">+</button>
                        <button type="button" class="btn btn-danger deleteProductLabExamination">-</button>
                    </div>
                </div>
            @endif

            <div class="col-md-12">
                <div class="row" style="margin-top: 10px;">
                    <div class="col-md-6">
                        <p class="text-center">{{ trans("swis.saf.lab_examination.sample_quantity") }}</p>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <input type="text" class="form-control LabExpertiseProductSampleQuantity" value="{{$selectedProduct['sample_quantity']}}" {{ (isset($disabled) && $disabled) ? 'disabled' : '' }}>
                                <div class="form-error" id="form-error-products-{{$key}}-sample_quantity"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <p class="text-center">{{ trans("swis.saf.lab_examination.sample_measurement_unit") }}</p>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <select class="select2 selectMeasurementUnit" {{ (isset($disabled) && $disabled) ? 'disabled' : '' }}>
                                    <option value=""></option>
                                    @foreach ($measurementUnits as $measurementUnit)
                                        <option value="{{ $measurementUnit->id }}" {{ ($measurementUnit->id == $selectedProduct['sample_measurement_unit']) ? 'selected' : '' }}>{{ $measurementUnit->name }}</option>
                                    @endforeach
                                </select>
                                <div class="form-error" id="form-error-products-{{$key}}-sample_measurement_unit"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <p class="text-center">{{ trans("swis.saf.lab_examination.sample_number") }}</p>
                <div class="form-group row">
                    <div class="col-lg-12">
                        <input type="text" class="form-control LabExpertiseProductSampleNumber" value="{{$selectedProduct['sample_number']}}" {{ (isset($disabled) && $disabled) ? 'disabled' : '' }}>
                        <div class="form-error" id="form-error-products-{{$key}}-sample_number"></div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    @if (isset($action) && $action == 'add')
        <div class="multipleProductBlockLabExamination">
            <div class="col-md-10">
                <select class="form-control select2 selectLabExpertiseProduct">
                    <option value=""></option>
                    @foreach($products ?? [] as $product)
                        <option value="{{ $product->id }}" {{ in_array($product->id, $selectedProductsIds) ? 'disabled="true"' : '' }}>{{ "({$product->hs_code}) {$product->hs_code_path_name}" }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <div class="row">
                    <button type="button" class="btn btn-primary addNewProductLabExamination">+</button>
                    <button type="button" class="btn btn-danger deleteProductLabExamination">-</button>
                </div>
            </div>

            <div class="col-md-12">
                <div class="row" style="margin-top: 10px;">
                    <div class="col-md-6">
                        <p>{{ trans("swis.saf.lab_examination.sample_quantity") }}</p>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <input type="text" class="form-control LabExpertiseProductSampleQuantity" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <p>{{ trans("swis.saf.lab_examination.sample_measurement_unit") }}</p>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <select class="select2 selectMeasurementUnit" disabled>
                                    <option value=""></option>
                                    @foreach ($measurementUnits as $measurementUnit)
                                        <option value="{{ $measurementUnit->id }}">{{ $measurementUnit->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <p>{{ trans("swis.saf.lab_examination.sample_number") }}</p>
                <div class="form-group row">
                    <div class="col-lg-12">
                        <input type="text" class="form-control LabExpertiseProductSampleNumber" disabled>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>