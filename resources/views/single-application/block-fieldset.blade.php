<?php
$safData = null;
$safControlledData = null;

if (!empty($saf)) {
    $safData = $saf->data;
}

$createdStateNoneVisibleFields = [
    'saf[general][regular_number]',
    'saf[general][date]',
];

if (!is_null($safData)) {
    $safData = prefixKey('', $safData);

    // Saf Controlled data merge with saf data
    if (!is_null($saf->saf_controlled_fields)) {
        $safControlledData = prefixKey('', $saf->saf_controlled_fields);
        $safData = array_merge($safData, $safControlledData);
    }
}

$fieldNamesForReplaceCommaToDot = [
    'saf.product.total_value_all',
    'saf.product.total_value',
    'saf.product.netto_weight',
    'saf.product.brutto_weight'
];

// Datamodel All Data
$dataModelAll = \App\Models\DataModel\DataModel::allData();
?>

{{--TAB SAF NOTES--}}
@if(!empty($simpleAppTab) && (string)$simpleAppTab->attributes()->key == 'notes')
    @include('single-application.notes.saf-notes-actions')
@endif

@foreach($block->fieldset as $fieldSet)
    <?php

    // Hide fields in SAF
    if (!empty($fieldSet->field->attributes()->hideOnSaf) && !in_array((string)$fieldSet->field->attributes()->id, $safControlledFields['visible_fields'])) {
        continue;
    }

    // Multiple Fields add data with JS
    $className = '';
    if ($fieldSet->attributes()->multiple) {
        if ((string)$fieldSet->field->attributes()->name == "saf[transportation][transit_country][]") {

            $transitCountries = array_filter($saf->data['saf']['transportation']['transit_country'] ?? [], function($value) { return !is_null($value); });
            $fieldData = json_encode(array_values($transitCountries));
            $className = "saf_transit_country";
        } elseif ((string)$fieldSet->field->attributes()->name == "saf[transportation][state_number_customs_support][1][state_number]" && !empty($saf->data['saf']['transportation']['state_number_customs_support'])) {
            $stateNumberCustomsSupports = [];
            $i = 1;
            foreach ($saf->data['saf']['transportation']['state_number_customs_support'] as $stateNumberCustomsSupport) {
                $stateNumberCustomsSupports[$i++] = $stateNumberCustomsSupport;
            }
            $fieldData = json_encode($stateNumberCustomsSupports);
            $className = "state_number_customs_support";
        }
    };

    // add class hide to form group
    $showFormGroup = true;
    if (!empty($saf) && $saf->status_type == \App\Models\SingleApplication\SingleApplication::CREATED_TYPE && in_array($fieldSet->field->attributes()->name, $createdStateNoneVisibleFields)) {
        $showFormGroup = false;
    }
    $fieldSetAttributes = $fieldSet->attributes();
    $fieldSetAttributesDefaultClass = (string)$fieldSetAttributes->defaultClass;

    // in edit part ,if products has second quantity type need to open form-group , remove hide class
    if (isset($product) && !empty($product->measurement_unit_2)) {
        $fieldSetAttributesDefaultClass = str_replace('hide', '', $fieldSetAttributesDefaultClass);
    }
    ?>

    <div class="form-group {{$fieldSetAttributesDefaultClass}} {{($fieldSet->field->attributes()->required) ? 'required' : '' }} {{!$showFormGroup ? 'hide' : ''}}
    {{in_array((string)$fieldSet->field->attributes()->id,$safControlledFields['mandatory_fields']) ? 'required' : '' }}">
        <label class="col-md-4 col-lg-3 control-label label__title"><span
                    @if($fieldSet->field->attributes()->tooltipText)
                    data-toggle="tooltip"
                    data-placement="{{(empty($fieldSet->field->attributes()->tooltipPlacement)) ? 'top' : $fieldSet->field->attributes()->tooltipPlacement}}"
                    title="{{trans($fieldSet->field->attributes()->tooltipText)}}"
                @endif
            >{!!  trans((string)$fieldSet->field->attributes()->title)!!}</span></label>

        <div class="col-md-8">
            <div class="{{($fieldSet->attributes()->multiple) ? 'multipleFields '.$className : ''}}"
                 @if(!empty($fieldData)) data-fields="{{ $fieldData }}" @endif>
                <div class="row clearfix">
                    @foreach($fieldSet->field as $field)
                        <?php
                        $fieldAttributes = $field->attributes();
                        ?>

                        {{-- Hide fields in SAF --}}
                        @if(!empty($fieldAttributes->hideOnSaf) && !in_array((string)$fieldAttributes->id,$safControlledFields['visible_fields'])) @continue @endif

                        <?php

                        // Field attributes
                        $fieldSafId = (string)$fieldAttributes->id;
                        $fieldName = (string)$fieldAttributes->name;
                        $fieldPlaceholder = (string)$fieldAttributes->placeholder;
                        $fieldDataName = (string)$fieldAttributes->dataName;
                        $fieldType = (string)$fieldAttributes->type;
                        $fieldDefaultClass = (string)$fieldAttributes->defaultClass;
                        $fieldDefaultId = (string)$fieldAttributes->defaultID;
                        $fieldDefaultValue = (string)$fieldAttributes->defaultValue;
                        $fieldDataType = (string)$fieldAttributes->dataType;
                        $fieldMdmId = (string)$fieldAttributes->mdm;
                        $fieldNameDot = (string)$fieldAttributes->name;
                        $fieldReadOnly = (string)$fieldAttributes->readOnly;
                        $fieldReferenceSelectColumns = (string)$fieldAttributes->refSelectOptionColumns;
                        $fieldReferenceTooltipColumns = (string)$fieldAttributes->refTooltipColumns;
                        $fieldSendAjax = ($fieldAttributes->ajax) ? true : false;
                        $fieldIsPhoneNumber = ($fieldAttributes->isPhoneNumber) ? true : false;
                        $fieldShowColumns = (string)$fieldAttributes->showColumns;
                        $fieldStoredValue = '';
                        $fieldValueMultiple = [];

                        // Field name change with field name dot ( saf[general][regime] -> saf.general.regime )
                        if (strpos($fieldNameDot, ']') !== false) {
                            $fieldNameDot = str_replace(['][', '['], '.', $fieldNameDot);
                            $fieldNameDot = str_replace(']', '', $fieldNameDot);
                        }

                        // Field mapping data
                        $mapData = [];
                        if ($field->mapping) {
                            foreach ($field->mapping->map as $map) {
                                $mapData[] = [
                                    'from' => (string)$map->attributes()->from,
                                    'to' => (string)$map->attributes()->to,
                                ];
                            }
                        }

                        // Get Field Value
                        if (!empty($safData) && !empty($safData[$fieldNameDot])) {
                            $fieldStoredValue = $safData[$fieldNameDot];
                        }

                        // If global DEFAULT VALUE (::)
                        if (strpos($fieldDefaultValue, '::') !== false && !empty($safGlobalDefaultData)) {
                            $defaultValueParts = explode('::', $fieldDefaultValue);

                            if (isset($safGlobalDefaultData[$defaultValueParts[0]], $safGlobalDefaultData[$defaultValueParts[0]][$defaultValueParts[1]])) {
                                $fieldDefaultValue = $safGlobalDefaultData[$defaultValueParts[0]][$defaultValueParts[1]];
                            }
                        }

                        // For "import || export" type (f_citizen) for import open export fields ,for export open import fields
                        if ($fieldDefaultValue === 'f_citizen') {
                            $fieldDefaultValue = !empty($referenceLegalEntityValues[$fieldDefaultValue]) ? $referenceLegalEntityValues[$fieldDefaultValue] : $fieldDefaultValue;
                        }

                        // If need to show option name not (code value) , custom columns
                        $refSelectOptionColumns = null;
                        if (!empty($fieldReferenceSelectColumns)) {
                            $refSelectOptionColumns = explode(',', $fieldReferenceSelectColumns);
                            if (!empty($refSelectOptionColumns[0])) {
                                $refSelectedOptionCode = $refSelectOptionColumns[0];
                            }

                            if (!empty($refSelectOptionColumns[1])) {
                                $refSelectedOptionName = $refSelectOptionColumns[1];
                            }

                            if (!empty($refSelectOptionColumns[2])) {
                                $refSelectedCustomName = $refSelectOptionColumns[2];
                            }
                        }

                        // If edit a ROW (product,batch)
                        if (isset($product)) {
                            if (!empty($product->{$fieldNameDot}) || (isset($product->{$fieldNameDot})) && $product->{$fieldNameDot} === '0') {
                                $fieldStoredValue = $product->{$fieldNameDot};
                            }
                        }

                        // Field current value , if opened first time will default data then from saf
                        $fieldValue = $fieldDefaultValue;
                        if ($fieldStoredValue || $fieldStoredValue === '0') {
                            $fieldValue = $fieldStoredValue;
                        }

                        // In Transportation Tab if regime is import disable import country else disable export country
                        if (!empty($regime)) {
                            if (($regime == 'import' && $fieldName == 'saf[transportation][import_country]') || ($regime == 'export' && $fieldName == 'saf[transportation][export_country]')) {
                                $fieldValue = getReferenceRowIdByCode(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_COUNTRIES, config('swis.country_mode'));
                                $fieldReadOnly = 'disabled';
                            }
                        }

                        // If field have reference get reference data
                        $referenceData = [];
                        $refSubmittedRow = null;
                        $refIsMultiple = false;
                        $refMultipleIds = [];

                        // Country
                        $isCountryRef = false;
                        if (!empty($fieldMdmId)) {

                            $mdm = null;
                            if (isset($dataModelAll[$fieldMdmId])) {
                                $mdm = (object)$dataModelAll[$fieldMdmId];
                            }

//                            $mdm = \App\Models\DataModel\DataModel::where(DB::raw('id::varchar'), $fieldMdmId)->first();
                            if (!empty($mdm->reference)) {

                                if ($mdm->type == \App\Models\DataModel\DataModel::MDM_ELEMENT_TYPE_MULTIPLE) {
                                    if (in_array($fieldSafId, $safControlledFields['visible_fields'])) {
                                        $refIsMultiple = true;
                                    }
                                }

                                if ($mdm->reference == \App\Models\ReferenceTable\ReferenceTable::REFERENCE_COUNTRIES_TABLE_NAME) {
                                    $isCountryRef = true;
                                }

                                if ($fieldSendAjax) {
                                    if (!empty($fieldValue)) {
                                        $referenceData = getReferenceRows($mdm->reference, $fieldValue, false, '*');
                                    }
                                } else {

                                    $referenceData = getReferenceRows($mdm->reference, false, false, '*');

                                    // Multiple Fields Values
                                    if ($refIsMultiple) {

                                        // Check Produt or global field
                                        if (empty($product)) {

                                            for ($i = 0; ; $i++) {
                                                if (!isset($safData[$fieldNameDot . '.' . $i])) {
                                                    break;
                                                } else {
                                                    $refMultipleIds[] = $safData[$fieldNameDot . '.' . $i];
                                                }
                                            }
                                        } else {
                                            $refMultipleIds = $fieldValue ?: [];
                                        }
                                    }

                                    // if not submitted saf we get reference last rows by code if saf submitted we get by id
                                    if (isset($hasSubmittedSubApplication) && !$hasSubmittedSubApplication || (isset($isDisabledProduct) && !$isDisabledProduct)) {
                                        $savedRefData = getReferenceRows($mdm->reference, $fieldValue, false, 'code');

                                        if (!is_null($savedRefData)) {
                                            $fieldValue = optional($savedRefData)->code;
                                        }

                                        if (count($refMultipleIds)) {
                                            $refMultipleCodes = getReferenceRowsWhereIn($mdm->reference, $refMultipleIds, [], 'code');

                                            if ($refMultipleCodes->count()) {
                                                $fieldValueMultiple = $refMultipleCodes->pluck('code')->toArray();
                                            }
                                        }
                                    } else {
                                        if ($refIsMultiple) {
                                            if(count($refMultipleIds)){
                                                $refSubmittedRow = getReferenceRowsWhereIn($mdm->reference, $refMultipleIds);
                                            }else{
                                                $refSubmittedRow = collect();
                                            }
                                        } else {
                                            $refSubmittedRow = getReferenceRows($mdm->reference, $fieldValue, false, '*');
                                        }
                                    }

                                }
                            }
                        }

                        // Product producer - country code name
                        if ($fieldName == 'producer_name') {
                            if (!empty($product->producer_country)) {
                                $fieldReadOnly = 'readonly';
                            }
                        }

                        if ($fieldName == 'producer_code') {
                            $referenceData = [];
                            if (!empty($product->producer_code)) {
                                $referenceData = getReferenceRows($mdm->reference, false, false, false, 'get', false, false, ['producer_registry_operating_country' => $product->producer_registry_country]);
                            }
                        }

                        // Producer Registry Country
                        if ($fieldName == 'producer_registry_country') {
                            $producerRegistryCountries = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_REGISTRY_OF_PRODUCERS, false, false, ['producer_registry_operating_country'])->unique()->pluck('producer_registry_operating_country')->all();
                            $referenceData = [];
                            if (count($producerRegistryCountries)) {
                                $referenceData = getReferenceRowsWhereIn(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_COUNTRIES, $producerRegistryCountries);
                            }
                        }

                        // Field error name for form-group
                        $fieldErrorName = str_replace('.', '-', $fieldNameDot);

                        if (in_array($fieldNameDot,$fieldNamesForReplaceCommaToDot)) {
                            $fieldValue = str_replace(',', '.', $fieldValue);
                        }

                        ?>

                        <div class="{{(count($fieldSet->field) > 1) ? 'col-lg-5 col-md-4 field__two-col' : 'col-lg-10 col-md-8 field__one-col'}}">

                            @switch($fieldType)
                                {{-- FIELD TYPE IS -> SELECT --}}
                                @case('select')

                                <select class="form-control  {{$fieldDefaultClass}}" name="{{$fieldName}}"
                                        id="{{$fieldDefaultId}}" {{$fieldReadOnly}}

                                        @if($fieldDataType)data-type="{{$fieldDataType}}" @endif tabindex="-1"
                                        @if(!empty($mapData)) data-fields="{{json_encode($mapData)}}" @endif>

                                    @if(!$fieldSendAjax)
                                        <option value="" selected>{{trans('core.base.label.select')}}</option>

                                        @if(count($referenceData) > 0)
                                            @foreach($referenceData as $refRow)
                                                <?php
                                                $checkByValue = 'id';

                                                if (is_null($refSubmittedRow)) {
                                                    $checkByValue = 'code';
                                                }
                                                ?>

                                                <option {{ ($fieldValue == $refRow->$checkByValue) ? 'selected' : ''}}  value="{{$refRow->id}}">{{$refRow->name}}</option>
                                            @endforeach
                                        @endif
                                    @else
                                        @if(!empty($referenceData))
                                            <?php
                                            $currentRefValue = '(' . $referenceData->code . ')' . ' ' . $referenceData->name;
                                            if (!empty($fieldShowColumns)) {
                                                $currentRefValue = $referenceData->{$fieldShowColumns};
                                            }
                                            ?>
                                            <option selected
                                                    @if($fieldName == 'measurement_unit') data-code="{{$referenceData->code}}"
                                                    @endif value="{{$referenceData->id}}">{{$currentRefValue}}</option>
                                        @else
                                            <option value=""
                                                    selected>{{trans('swis.document.label.autocomplete')}}</option>
                                        @endif

                                    @endif
                                </select>

                                @break

                                {{-- FIELD TYPE IS -> INPUT --}}
                                @case('input')

                                {{-- Input type (text,number,date) --}}
                                @php($inputType = (string)$fieldAttributes->inputType)
                                @if($fieldIsPhoneNumber)
                                    <div class="input-group">
                                        <span class="input-group-addon">+</span>
                                        @endif

                                        @if($fieldSafId == 'SAF_ID_1' && !empty($safGlobalDefaultData))
                                            <input type="text" class="form-control"
                                                   value="{{$safGlobalDefaultData['global']['typeName']}}" disabled>
                                        @endif

                                        <input
                                                {{$fieldReadOnly}}
                                                value="{{$fieldValue}}"
{{--                                                data-toggle="tooltip"--}}
{{--                                                title="{{$fieldValue}}"--}}
                                                type="{{($inputType) ? $inputType : 'text'}}"
                                                name="{{$fieldName}}"
                                                id="{{$fieldDefaultId}}"
                                                @if($fieldDataType) data-type="{{$fieldDataType}}" @endif
                                                data-name="{{$fieldDataName}}"
                                                @if($mapData) data-fields="{{json_encode($mapData)}}" @endif
                                                class="form-control {{$fieldDefaultClass}}"
                                                autocomplete="nope">

                                        @if($fieldIsPhoneNumber)
                                    </div>
                                @endif

                                @break

                                {{-- FIELD TYPE IS -> TEXTAREA --}}
                                @case('textarea')

                                <textarea rows="5" cols="5" class="form-control {{$fieldDefaultClass}}"
                                          {{$fieldReadOnly}} name="{{$fieldName}}">{{$fieldValue}}</textarea>

                                @break

                                {{-- FIELD TYPE IS -> AUTOCOMPLETE - (select2) --}}
                                @case('autocomplete')

                                <?php $fieldRefType = $fieldAttributes->refType; ?>

                                {{-- Send ajax request for get data with autocomplete --}}
                                {{-- NOT USED (need to check) --}}
                                @if($fieldSendAjax)

                                    <select   {{$fieldReadOnly}}
                                            class="form-control select2-ajax select2-ajax-saf {{$fieldDefaultClass}}"
                                            name="{{$fieldName}}"
                                            data-id-value="1"
                                            data-url="{{urlWithLng('/reference-table/ref')}}"
                                            data-ref="{{\App\Models\ReferenceTable\ReferenceTable::getReferenceIdByTableName(env($fieldAttributes->ref))}}"
                                            @if(!empty($mapData)) data-fields="{{json_encode($mapData)}}" @endif>

                                        @if(!empty($referenceData))
                                            <option selected value="{{$referenceData->id}}">({{$referenceData->code}})
                                            </option>
                                        @endif
                                    </select>

                                @else

                                    <?php $refSelectName = $fieldName  ?>
                                    @if($refIsMultiple)
                                        <?php $refSelectName = $fieldName . '[]'  ?>
                                    @endif

                                    <select @if($fieldPlaceholder) data-select2-placeholder="{{trans($fieldPlaceholder)}}" @endif {{$fieldReadOnly}} class="form-control select2 {{$fieldDefaultClass}}"
                                            name="{{$refSelectName}}" {{$fieldReadOnly}}
                                            {{($refIsMultiple) ? 'data-select2-allow-clear=false multiple' : ''}}
                                            @if(!empty($mapData)) data-fields="{{json_encode($mapData)}}" @endif>
                                        <option value=""></option>

                                        {{-- if not submitted saf we get reference last rows by code if saf submitted we get by id --}}
                                        @if(!is_null($refSubmittedRow))
                                            @if(!$refIsMultiple)

                                                <?php
                                                $optionValue = '(' . $refSubmittedRow->code . ')' . ' ' . $refSubmittedRow->name;

                                                // if need to show custom columns
                                                if (!empty($refSelectOptionColumns) && !empty($refSubmittedRow->{$refSelectedOptionCode}) && !empty($refSubmittedRow->{$refSelectedOptionName})) {
                                                    $optionValue = '(' . $refSubmittedRow->{$refSelectedOptionCode} . ')' . ' ' . $refSubmittedRow->{$refSelectedOptionName};
                                                }

                                                if (!empty($refSelectedCustomName) && !empty($refSubmittedRow->{$refSelectedCustomName})) {
                                                    $optionValue .= ', ' . $refSubmittedRow->{$refSelectedCustomName};
                                                }

                                                if ($fieldName == 'producer_code') {
                                                    $optionValue = $refSubmittedRow->code;
                                                }
                                                ?>

                                                <option selected
                                                        value="{{$refSubmittedRow->id}}">{{$optionValue}}</option>
                                            @else
                                                @if(!is_null($refSubmittedRow))
                                                    @foreach($refSubmittedRow as $field)
                                                        <option selected
                                                                value="{{$field->id}}">{{$field->name}}</option>
                                                    @endforeach
                                                @endif
                                            @endif
                                        @else
                                            <?php

                                            $addOPtionText = false;
                                            if ($fieldName == 'total_value_code' || $isCountryRef) {
                                                $addOPtionText = true;
                                            }
                                            ?>

                                            @if(count($referenceData) > 0)
                                                @foreach($referenceData as $refRow)

                                                    <?php
                                                    $optionValue = '(' . $refRow->code . ')' . ' ' . $refRow->name;

                                                    // if need to show custom columns
                                                    if (!empty($refSelectOptionColumns) && !empty($refRow->{$refSelectedOptionCode}) && !empty($refRow->{$refSelectedOptionName})) {
                                                        $optionValue = '(' . $refRow->{$refSelectedOptionCode} . ')' . ' ' . $refRow->{$refSelectedOptionName};

                                                        if (!empty($refSelectedCustomName) && !empty($refRow->{$refSelectedCustomName})) {
                                                            $optionValue .= ', ' . $refRow->{$refSelectedCustomName};
                                                        }
                                                    }

                                                    if ($fieldName == 'producer_code') {
                                                        $optionValue = $refRow->code;
                                                    }

                                                    ?>

                                                        <option @if($fieldName == 'producer_code') data-name="{{$refRow->name}}" @endif @if($fieldName == 'measurement_unit') data-code="{{$refRow->code}}" @endif
                                                        @if(!empty($mapData)) data-fields-values="{{json_encode($refRow)}}" @endif
                                                                @if(!$refIsMultiple)
                                                                {{ ($fieldValue == $refRow->code) ? 'selected' : ''}}
                                                                @else
                                                                {{count($fieldValueMultiple) && count($fieldValueMultiple) && in_array($refRow->code,$fieldValueMultiple) ? 'selected' : ''}}
                                                                @endif
                                                                value="{{$refRow->id}}"
                                                                @if($addOPtionText) data-option-text="{{$optionValue}}" @endif>@if($addOPtionText){{$isCountryRef ? $refRow->country_id ?? 0 : $refRow->code }} @endif {{$optionValue}}</option>

                                                @endforeach
                                            @endif

                                        @endif

                                    </select>

                                @endif

                                @break

                                {{-- FIELD TYPE IS -> HIDDEN --}}
                                @case('hidden')

                                <input value="{{$fieldValue}} " type="hidden" name="{{$fieldName}}">

                                @break

                                {{-- FIELD TYPE IS -> AUTOINCREMENT --}}
                                @case('autoincrement')

                                <?php
                                $incrementValue = 1;
                                if (!empty($productsIdNumbers) && isset($product)) {
                                    if (isset($productsIdNumbers[$product->id])) {
                                        $incrementValue = $productsIdNumbers[$product->id];
                                    }
                                }
                                ?>

                                <input value="{{ $incrementValue }}" type="text" readonly
                                       class="form-control autoincrement" name="{{$fieldName}}">

                                @break

                            @endswitch

                            <div class="form-error" id="form-error-{{$fieldErrorName}}"></div>

                            @if(empty($fieldMdmId) && $fieldType != 'hidden')
                                <b style="color: red">Need add MDM field
                                    id {{ $fieldSet->field->attributes()->inputType}}</b>
                            @endif
                        </div>

                    @endforeach

                    @if($fieldSet->attributes()->multiple)
                        <button type="button" class="btn btn-primary plusMultipleFields"> <i class="fa fa-plus"></i>
                        </button>
                        {{--<button type='button' class='btn btn-danger deleteMultipleRow'><i class='fa fa-minus'></i></button>--}}
                    @endif
                </div>
            </div>
        </div>
    </div>

@endforeach

{{--Products Batch Table Part--}}
@include('single-application.products.products-batch-table')