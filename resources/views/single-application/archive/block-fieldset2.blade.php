<?php
use Illuminate\Support\Facades\DB;$safData = null;

if (!empty($saf)) {
    $safData = $saf->data;

    if(!empty($updatedSafData)){
        $safData = $updatedSafData;
    }

}

$createdStateNonVisibleFields = [
    'saf[general][date]',
    'saf[general][regular_number]'
];
?>

{{--TAB SAF NOTES--}}
@if(!empty($simpleAppTab) && (string)$simpleAppTab->attributes()->key == 'notes')
    @include('single-application.notes.saf-notes-actions')
@endif

@foreach($block->fieldset as $fieldSet)

    <?php
        if ($fieldSet->attributes()->showInApplication == true) { continue; }
        if (count($fieldSet->field) == 1 && !empty($fieldSet->field->attributes()->hideOnSaf) ) {
            continue;
        }

    ?>

    <div class="form-group {{($fieldSet->field->attributes()->required) ? 'required' : '' }}">
        <label
            @if($fieldSet->field->attributes()->tooltipText)
                data-toggle="tooltip"
                data-placement="{{(empty($fieldSet->field->attributes()->tooltipPlacement)) ? 'top' : $fieldSet->field->attributes()->tooltipPlacement}}"
                title="{{trans($fieldSet->field->attributes()->tooltipText)}}"
            @endif
            class="col-lg-3 control-label {{ (!empty($saf) && $saf->status_type == 'created' && in_array($fieldSet->field->attributes()->name, $createdStateNonVisibleFields)) ? 'hide' : ''}}">{!!  trans((string)$fieldSet->field->attributes()->title)!!}
        </label>

        <div class="col-md-8">
            <?php
                $fieldData = [];
                $className = '';
                if ($fieldSet->attributes()->multiple) {
                    if ( (string)$fieldSet->field->attributes()->name == "saf[transportation][transit_country][]" && !empty($saf->data['saf']['transportation']['transit_country']) ) {
                        $fieldData = json_encode($saf->data['saf']['transportation']['transit_country']);
                        $className = "saf_transit_country";
                    } elseif ( (string)$fieldSet->field->attributes()->name == "saf[transportation][state_number_customs_support][1][state_number]" && !empty($saf->data['saf']['transportation']['state_number_customs_support']) ) {
                        $fieldData = json_encode($saf->data['saf']['transportation']['state_number_customs_support']);
                        $className = "state_number_customs_support";
                    }
                };
            ?>
            <div class="{{($fieldSet->attributes()->multiple) ? 'multipleFields '.$className : ''}}" data-fields="{{ (!empty($fieldData)) ? $fieldData : '' }}">
                <div class="clearfix">
                    @foreach($fieldSet->field as $field)

                        <?php
                            if ( !empty($field->attributes()->hideOnSaf) ) {
                                if ( in_array((string)$field->attributes()->dataName, $safHiddenColumns) || in_array((string)$field->attributes()->name, $safHiddenColumns)) {
                                    continue;
                                }
                            }

                            $refData = [];

                            if ($field->attributes()->mdm) {

                                if (!empty($field->attributes()->mdm)) {

                                    $mdm = \App\Models\DataModel\DataModel::where(DB::raw('id::varchar'), $field->attributes()->mdm)->first();

                                    if (!empty($mdm->reference) && empty($field->attributes()->ajax)) {

                                        $refData = getReferenceRows($mdm->reference,false,false,'*');
                                    }
                                }
                            }

                        ?>

                        <div class="{{(count($fieldSet->field) > 1) ? 'col-md-3' : 'col-md-10'}}">
                            <?php
                            $fieldName = $field->attributes()->name . '';

                            if (strpos($fieldName, ']') !== false) {
                                $fieldName = str_replace(['][', '['], '.', $fieldName);
                                $fieldName = str_replace(']', '', $fieldName);
                            }

                            ?>

                            <?php
                            $storeValue = '';
                            if (!empty($safData)) {
                                $safData = prefixKey('', $safData);
                                if (!empty($safData[$fieldName])) {
                                    $storeValue = $safData[$fieldName];
                                }

                            }
                            ?>

                            <?php $mapData = [] ?>
                            @if($field->mapping)
                                @foreach($field->mapping->map as $map)
                                    <?php
                                    $mapData[] = [
                                        'from' => (string)$map->attributes()->from,
                                        'to' => (string)$map->attributes()->to,
                                    ];
                                    ?>
                                @endforeach
                            @endif


                            @switch($field->attributes()->type)

                                @case('select')

                                    <select class="form-control select2 {{$field->attributes()->defaultClass}}"
                                            name="{{$field->attributes()->name}}"
                                            @if($field->attributes()->dataType)
                                                data-type="{{$field->attributes()->dataType}}"
                                            @endif
                                            {{ (!empty($onlyView) && $onlyView === true) ? 'disabled' : '' }}>
                                        <option value="" selected>select</option>

                                        @if(count($refData) > 0)
                                            @foreach($refData as $rVal)
                                                <option {{(!empty(empty($storeValue) && $field->attributes()->defaultValue) && ($field->attributes()->defaultValue ==  $rVal->code))? 'selected' : ''}}  {{(!empty($safData) && ($storeValue == $rVal->code) ? 'selected' : '')}} value="{{$rVal->code}}">{{$rVal->name}}</option>
                                            @endforeach
                                        @endif

                                    </select>

                                    @break

                                @case('input')

                                    <?php
                                        $defaultValue = '';
                                        if ($field->attributes()->defaultValue) {
                                            $defaultValueParts = explode('::', $field->attributes()->defaultValue);

                                            switch ($defaultValueParts[0]) {
                                                case 'global':
                                                    $defaultValue = $defaultValueParts[1];

                                                    if ($defaultValueParts[1] == 'regularNumber') {
                                                        $defaultValue = $saf->regular_number;
                                                    }

                                                    if ($defaultValueParts[1] == 'safDate') {
                                                        $defaultValue = $saf->created_at->format('Y-m-d');
                                                    }

                                                    if ($defaultValueParts[1] == 'type') {
                                                        $defaultValue = $regime;
                                                    }

                                                    break;

                                                case 'user':

                                                    switch ($defaultValueParts[1]){
                                                        case 'passport':
                                                                $defaultValue = @\Auth::user()->getCreator()->ssn ?? '';
                                                            break;
                                                        case 'name':
                                                                $defaultValue = @\Auth::user()->getCreator()->name ?? '';
                                                            break;
                                                        case 'address':
                                                                $defaultValue = @\Auth::user()->getCreator()->address ?? '';
                                                            break;
                                                        case 'phone_number':
                                                                $defaultValue = @\Auth::user()->getCreator()->phone_number ?? '';
                                                            break;
                                                        case 'email':
                                                                $defaultValue = @\Auth::user()->getCreator()->email ?? '';
                                                            break;
                                                    }

                                            }

                                        }

                                        $phoneNumberFields = [
                                            'saf[sides][import][phone_number]',
                                            'saf[sides][export][phone_number]',
                                            'saf[sides][applicant][phone_number]',
                                        ];
                                    ?>

                                    @if (!empty($saf) && $saf->status_type == 'created' && in_array($fieldSet->field->attributes()->name, $createdStateNonVisibleFields))
                                        <input type="hidden"
                                               name="{{$fieldSet->field->attributes()->name}}"
                                               value="{{ (empty($safData)) ? (!empty($raw->{$fieldName})) ? $raw->{$fieldName} : $defaultValue : $storeValue }}">
                                        @continue;
                                    @endif

                                        @if (in_array($field->attributes()->name, $phoneNumberFields))
                                            <div class="input-group">
                                                <span class="input-group-addon">+</span>
                                                <input type="text"
                                                       class="form-control {{$field->attributes()->defaultClass}}"
                                                       name="{{$field->attributes()->name}}"
                                                       value="{{(empty($safData)) ? (!empty($raw->{$fieldName})) ? $raw->{$fieldName} : $defaultValue : $storeValue}}"
                                                       autocomplete="nope"
                                                       id="{{$field->attributes()->defaultID or ''}}"
                                                       data-fields="{{json_encode($mapData)}}"
                                                       data-name="{{$field->attributes()->dataName or ''}}"
                                                       {{$field->attributes()->readOnly or ''}}>
                                            </div>
                                        @else
                                            <input {{$field->attributes()->readOnly or ''}}
                                                   value="{{(empty($safData)) ? (!empty($raw->{$fieldName})) ? $raw->{$fieldName} : $defaultValue : $storeValue}}"
                                                   type="{{($field->attributes()->inputType) ? $field->attributes()->inputType : 'text'}}"
                                                   name="{{$field->attributes()->name}}"
                                                   id="{{$field->attributes()->defaultID or ''}}"
                                                   @if($field->attributes()->dataType) data-type="{{$field->attributes()->dataType}}" @endif
                                                   data-name="{{$field->attributes()->dataName or ''}}"
                                                   data-fields="{{json_encode($mapData)}}"
                                                   class="form-control {{$field->attributes()->defaultClass}}"
                                                   autocomplete="nope"
                                                   {{ (!empty($onlyView) && $onlyView === true) ? 'disabled' : '' }}>
                                        @endif
                                @if($field->attributes()->rightLabel)
                                    <span class="right-label">{{$field->attributes()->rightLabel}}</span>
                                @endif

                                @break

                                @case('textarea')

                                    <textarea rows="5" cols="5"
                                              class="form-control"
                                              {{$field->attributes()->readOnly or ''}}
                                              name="{{$field->attributes()->name}}"
                                              {{ (!empty($onlyView) && $onlyView === true) ? 'disabled' : '' }}>{{!empty($raw->{$fieldName}) ? $raw->{$fieldName} : $storeValue }}</textarea>

                                @break

                                @case('autocomplete')

                                    @if(empty($field->attributes()->refType) || ($field->attributes()->refType != 'input'))

                                        <?php
                                            $rawRef = null;
                                            if (!empty($raw)) {

                                                if (!empty($field->attributes()->ref)) {

                                                    $refName = 'reference_' . env($field->attributes()->ref);
                                                    $refNameMl = 'reference_' . env($field->attributes()->ref) . '_ml';
                                                }

                                                if (!empty($field->attributes()->mdm)) {
                                                    $dataModel = \App\Models\DataModel\DataModel::where(DB::raw('id::varchar'), $field->attributes()->mdm)->first();

                                                    if(!empty($dataModel->reference)){
                                                        $refName = 'reference_' . $dataModel->reference;
                                                        $refNameMl = 'reference_' . $dataModel->reference . '_ml';
                                                    }
                                                }


                                                $rawRef = \Illuminate\Support\Facades\DB::table($refName)
                                                    ->join($refNameMl, $refName . '.id', '=', $refNameMl . '.' . $refName . '_id')->where($refNameMl . '.lng_id', '=', cLng('id'))
                                                    ->where('id', $raw->{$fieldName})
                                                    ->first();

                                            }

                                            $refSelectOptionColumns = null;
                                            if(!empty($field->attributes()->refSelectOptionColumns)){
                                                $refSelectOptionColumns = explode(',',$field->attributes()->refSelectOptionColumns);
                                            }

                                            $addDisabled = '';
                                            if (!empty($regime) && $regime == 'import' && (string)$field->attributes()->name == 'saf[transportation][import_country]') {
                                                $addDisabled = "disabled";
                                            } elseif (!empty($regime) && $regime == 'export' && (string)$field->attributes()->name == 'saf[transportation][export_country]') {
                                                $addDisabled = "disabled";
                                            }


                                        ?>

                                        <select
                                                {{ $addDisabled }}
                                                {{ (!empty($onlyView) && $onlyView === true) ? 'disabled' : '' }}
                                                {{$field->attributes()->readOnly or ''}}
                                                class="form-control  autocomplete {{(!empty($field->attributes()->ajax)) ? 'select2-ajax select2-ajax-saf' : 'select2'}} {{$field->attributes()->defaultClass}}"
                                                name="{{$field->attributes()->name}}"
                                                @if(!empty($field->attributes()->ajax) && (string)$field->attributes()->ajax == 'true')
                                                data-url="{{urlWithLng('/reference-table/ref')}}"
                                                data-ref="{{\App\Models\ReferenceTable\ReferenceTable::getReferenceIdByTableName(env($field->attributes()->ref))}}"
                                                @endif
                                                @if(!empty($mapData)) data-fields="{{json_encode($mapData)}}" @endif>

                                            <option value=""></option>

                                            @if(!empty($raw) && !is_null($rawRef))

                                                <?php

                                                $rawOptionVal = '('.$rawRef->code.')'.' '.$rawRef->name;

                                                if(!empty($refSelectOptionColumns)){

                                                    if(!empty($refSelectOptionColumns[0])){
                                                        $rCode = $refSelectOptionColumns[0];
                                                    }

                                                    if(!empty($refSelectOptionColumns[1])){
                                                        $rName = $refSelectOptionColumns[1];
                                                    }


                                                    if(!empty($rawRef->$rCode) && !empty($rawRef->$rName)){
                                                        $rawOptionVal = '('.$rawRef->$rCode.')'.' '.$rawRef->$rName;
                                                    }
                                                }

                                                ?>
                                                <option selected value="{{@$rawRef->id}}"> {{$rawOptionVal}} </option>
                                            @endif

                                            @if(count($refData) > 0)
                                                @foreach($refData as $rVal)

                                                    @if(!empty($raw) && !empty($rawRef) && $rawRef->code == $rVal->code)
                                                        @continue
                                                    @endif

                                                    @if (!empty($regime) && $regime == 'import' && (string)$field->attributes()->name == 'saf[transportation][export_country]')
                                                        @if (env('COUNTRY_MODE') == $rVal->code)
                                                            @continue
                                                        @endif
                                                    @endif

                                                    @if(!empty($regime) && $regime == 'export' && (string)$field->attributes()->name == 'saf[transportation][import_country]')
                                                        @if (env('COUNTRY_MODE') == $rVal->code)
                                                            @continue
                                                        @endif
                                                    @endif

                                                    @php($selected = '')

                                                    @if (!empty($regime) && $regime == 'import' && env('COUNTRY_MODE') == $rVal->code && (string)$field->attributes()->name == 'saf[transportation][import_country]')
                                                        @php($selected = 'selected')
                                                    @endif
                                                    @if (!empty($regime) && $regime == 'export' && env('COUNTRY_MODE') == $rVal->code && (string)$field->attributes()->name == 'saf[transportation][export_country]')
                                                        @php($selected = 'selected')
                                                    @endif
                                                    <?php
                                                        $refOptionVal = '('.$rVal->code.') '.$rVal->name;

                                                        if(!empty($refSelectOptionColumns)){

                                                            if(!empty($refSelectOptionColumns[0])){
                                                                $rCode = $refSelectOptionColumns[0];
                                                            }

                                                            if(!empty($refSelectOptionColumns[1])){
                                                                $rName = $refSelectOptionColumns[1];
                                                            }


                                                            if(!empty($rVal->$rCode) && !empty($rVal->$rName)){
                                                                $refOptionVal = '('.$rVal->$rCode.')'.' '.$rVal->$rName;
                                                            }
                                                        }

                                                    ?>

                                                    <option
                                                        {{(!empty($safData) && ($storeValue == $rVal->code)) ? 'selected' : $selected}}
                                                        {{(!empty($raw) && ($raw->{$fieldName} == $rVal->code)) ? 'selected' : ''}}
                                                        @if(!empty($mapData))
                                                            data-fields-values="{{json_encode($rVal)}}"
                                                        @endif
                                                        value="{{$rVal->id}}">

                                                        {{$refOptionVal}}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>

                                    @elseif(($field->attributes()->refType != 'input'))

                                        <input value="{{$raw->{$fieldName} or ''}}"
                                               {{$field->attributes()->readOnly or ''}}
                                               type="{{($field->attributes()->inputType) ? $field->attributes()->inputType : 'text'}}"
                                               name="{{$field->attributes()->name}}"
                                               placeholder="autocomplete"
                                               class="form-control autocomplete {{$field->attributes()->defaultClass}}"
                                               @if($field->attributes()->dataType) data-type="{{$field->attributes()->dataType}}"
                                               @endif
                                               autocomplete="nope"
                                               data-ref="{{\App\Models\ReferenceTable\ReferenceTable::getReferenceIdByTableName(env($field->attributes()->ref))}}"
                                               data-fields="{{json_encode($mapData)}}"
                                               {{ (!empty($onlyView) && $onlyView === true) ? 'disabled' : '' }}>

                                    @endif

                                @break

                                @case('hidden')

                                    <input value="{{!empty($raw->{$fieldName}) ? $raw->{$fieldName} : $field->attributes()->defaultValue }} "
                                           type="hidden"
                                           name="{{$field->attributes()->name}}"
                                           {{ (!empty($onlyView) && $onlyView === true) ? 'disabled' : '' }}>

                                    @break

                                @case('autoincrement')

                                    <?php
                                    $incVal = @$raw->{$fieldName};
                                    if(!empty($productsIdNumbers))    {
                                        if(isset($productsIdNumbers[$raw->id])){
                                            $incVal = $productsIdNumbers[$raw->id];
                                        }
                                    }

                                    ?>

                                    <input value="{{ $incVal}}"
                                           type="{{($field->attributes()->inputType) ? $field->attributes()->inputType : 'text'}}"
                                           readonly
                                           class="form-control autoincrement"
                                           name="{{$field->attributes()->name}}"
                                           {{ (!empty($onlyView) && $onlyView === true) ? 'disabled' : '' }}>
                                    @break

                            @endswitch

                            <?php
                                $fieldName = str_replace(']', '', $fieldName);
                                $fieldName = str_replace('[', '-', $fieldName);
                                $errorName = str_replace('.', '-', $fieldName)
                            ?>

                            <div class="form-error" id="form-error-{{$errorName}}"></div>

                            @if(empty($fieldSet->field->attributes()->mdm) && $field->attributes()->type != 'hidden')
                                <b style="color: red">Need add MDM field id {{ $fieldSet->field->attributes()->inputType}}</b>
                            @endif
                        </div>

                    @endforeach

                    @if($fieldSet->attributes()->multiple)
                        <button type="button" class="btn btn-primary plusMultipleFields"><i class="fa fa-plus"></i></button>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endforeach

@include('single-application.products.products-batch-table')