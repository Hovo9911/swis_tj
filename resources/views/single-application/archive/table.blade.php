<div class="list-data_wrapper table-responsive">
    <table class="table table-bordered {{$block->attributes()->tableClass}}"
        @if(!empty($block->attributes()->DbTableName))

            <?php
            dd('aa');
            $columns = (array)$block->columns;
            $hideColumns = (array)$block->columnsNotShowInView ?? [];
            $refDataColumns = $block->columnsRefData ?? [];
            $columnsActions = $block->columnsActions;
            $tooltipColumns = $block->columnsTooltip;

            $dataValues = \Illuminate\Support\Facades\DB::table((string)$block->attributes()->DbTableName)->select($columns)->where('saf_number', $saf->regular_number)->get()->toArray();

            $refDataColumnsArr = [];
            if (!empty($refDataColumns)) {
                foreach ($refDataColumns as $refDataColumn) {
                    $refAttr = $refDataColumn->attributes();
                    $fromRef = (string)$refAttr->from;
                    $toColumn = (string)$refAttr->to;
                    $refDataColumnsArr[$toColumn] = $fromRef;
                }
            }

            $tooltipColumnsArr = [];
            if (!empty($tooltipColumns)) {
                foreach ($tooltipColumns as $tooltipColumn) {
                    $tooltipAttr = $tooltipColumn->attributes();
                    $fromColumn = (string)$tooltipAttr->from;
                    $toColumnTooltip = (string)$tooltipAttr->to;
                    $tooltipColumnsArr[$toColumnTooltip] = $fromColumn;
                }
            }

            ?>

            <thead>
            <tr>
                @if(!empty($columnsActions))
                    <th></th>
                @endif

                @foreach($columns as $column)
                    @if(in_array($column,$hideColumns))
                        @continue
                    @endif
                    <th>{{trans('swis.single_app.'.$column)}}</th>
                @endforeach

                @if(!empty($block->attributes()->actions) && ((string)$block->attributes()->actions == 'true'))
                    <th></th>
                @endif
            </tr>
            </thead>

            <tbody>

            @if(!empty($dataValues))
                @include('single-application.table-tr-render',[
                    'data'=>$dataValues,
                    'hideColumns'=>$hideColumns,
                    'actions'=>@(string)$block->attributes()->actions,
                    'columnsActions'=>$columnsActions,
                    'blockModule'=>$block->attributes()->module,
                    'subApplicationProducts'=>$subApplicationProducts,
                    'refDataColumnsArr'=>$refDataColumnsArr,
                    'tooltipColumnsArr'=>$tooltipColumnsArr
                ])
            @endif

            </tbody>

        @endif
    </table>
</div>