{{-- BATCHES TABLE RENDER --}}
<?php
use Illuminate\Support\Facades\DB;if (empty($num)) {
    $num = 1;
}




?>
<tr data-id="{{$num}}" data-auto-generated="{{ isset($data->auto_generated) ? $data->auto_generated : 0 }}">
    <?php $batchId = !empty($data) ? $data->id : "" ?>
    <input type="hidden" name="batch[{{$num}}][id]" class="batchesId" value="{{ $batchId }}">
    @foreach($block->fieldset as $key=>$column)
        <?php
        if (!empty($column->attributes()->showInView) && !(string)$column->attributes()->showInView) {
            continue;
        }
        if (
            !empty($column->field->attributes()->dataName) &&
            in_array((string)$column->field->attributes()->dataName, $safHiddenColumns) &&
            empty($isApplication)) {
            continue;
        }

        $name = $column->field->attributes()->name . '';

        if (strpos($name, ']') !== false) {
            $name = str_replace(['][', '['], '.', $name);
            $name = str_replace(']', '', $name);
        }

        if (!empty($column->field->attributes()->dataName)) {
            $name = (string)$column->field->attributes()->dataName;
        }

        ?>

        <td style="{{ ($column->field->attributes()->type == 'select') ? 'width: 200px;' : '' }}">
            @switch($column->field->attributes()->type)

                @case('select')
                <?php
                $refData = [];
                $mdm = \App\Models\DataModel\DataModel::select('id', 'reference')->where(DB::raw('id::varchar'), $column->field->attributes()->mdm)->first();
                $showName = false;

                if ($column->field->attributes()->ref) {
                    $refData = \Illuminate\Support\Facades\DB::table('reference_' . (string)$column->field->attributes()->ref)->get();
                } elseif (!empty($mdm->reference)) {
                    $showName = true;
                    $rTable = 'reference_' . $mdm->reference;
                    $rTableMl = 'reference_' . $mdm->reference . '_ml';

                    $refData = \Illuminate\Support\Facades\DB::table($rTable)
                        ->select("{$rTable}.code", "{$rTableMl}.name")
                        ->join($rTableMl, "{$rTable}.id", '=', "{$rTableMl}.{$rTable}_id")->where("{$rTableMl}.lng_id", '=', cLng('id'))
                        ->where("{$rTable}.show_status", '=', \App\Models\ReferenceTable\ReferenceTable::STATUS_ACTIVE)
                        ->get();
                }
                ?>

                <select class="form-control select2 {{$column->field->attributes()->defaultClass}}"
                        {{ (!empty($onlyView) && $onlyView === true) ? 'disabled' : '' }} {{ $readOnly }} name="{{(!empty($isApplication)) ? "product_batches[{$data->id}][{$column->field->attributes()->dataName}]" : str_replace('_index_',$num,$column->field->attributes()->name)}}">
                    <option value="" disabled selected></option>
                    @if(count($refData) > 0)
                        @foreach($refData as $ref)
                            <option {{ (!empty($data->{$name}) && $ref->code == $data->{$name}) ? 'selected' : '' }} value="{{$ref->code}}">{{ ($showName) ? "({$ref->code}) {$ref->name}": $ref->code}}</option>
                        @endforeach
                    @endif
                </select>

                @break

                @case('input')
                <?php
                if (!empty($data->{$name}) || (!empty($data->{$name}) && $data->{$name} == 0.0)) {
                    $dataNameView = ((string)$column->field->attributes()->dataName == 'batch_number') ? trans($data->{$name}) : $data->{$name};
                } else {
                    $dataNameView = ($column->field->attributes()->dataName == 'rejected_quantity') ? (int)$column->field->attributes()->defaultValue : $column->field->attributes()->defaultValue;
                }

                // if value of column need to get from reference
                $isRefData = false;
                $refDataValue = '';
                if (!empty($refDataColumnsArr)) {

                    if (isset($refDataColumnsArr[$name])) {

                        $refTableData = getReferenceRows($refDataColumnsArr[$name], false, $dataNameView, [], 'first');
                        if (!is_null($refTableData)) {
                            $isRefData = true;
                            $refDataValue = '(' . $refTableData->code . ') ' . $refTableData->name;
                        }
                    }
                }

                ?>

                <input {{ $column->field->attributes()->readOnly  }}
                       value="{{ $dataNameView }}"
                       type="{{($column->field->attributes()->inputType) ? $column->field->attributes()->inputType : 'text'}}"
                       class="form-control {{($isRefData) ? 'hide' : ''}} {{$column->field->attributes()->defaultClass}}"
                       name="{{ (!empty($isApplication)) ? "product_batches[{$data->id}][{$column->field->attributes()->dataName}]" : str_replace('_index_',$num,$column->field->attributes()->name)}}"
                        {{ (!empty($onlyView) && $onlyView === true) ? 'disabled' : '' }}>

                @if($isRefData)

                    <input type="text" value="{{$refDataValue or ''}}" {{ $column->field->attributes()->readOnly  }} class="form-control">
                @endif

                @break

                @case('textarea')
                <textarea rows="5" cols="5"
                          name="{{(!empty($isApplication)) ? "product_batches[{$data->id}][{$column->field->attributes()->dataName}]" : str_replace('_index_',$num,$column->field->attributes()->name)}}"
                          class="form-control "
                              {{ ($column->field->attributes()->readOnly ) ? $column->field->attributes()->readOnly : ''}}
                        {{ (!empty($onlyView) && $onlyView === true) ? 'disabled' : '' }}></textarea>

                @break

                @case('autocomplete')

                <?php $mapData = [] ?>
                @foreach($column->field->mapping->map as $map)
                    <?php $mapData[] = [
                        'from' => (string)$map->attributes()->from,
                        'to' => (string)$map->attributes()->to,
                    ] ?>
                @endforeach

                <input value=""
                       type="{{($column->field->attributes()->inputType) ? $column->field->attributes()->inputType : 'text'}}"
                       name="{{ (!empty($isApplication)) ? "product_batches[{$data->id}][{$column->field->attributes()->dataName}]" : "batch[{$num}][{$column->field->attributes()->name}]" }}"
                       placeholder="autocomplete"
                       class="form-control autocomplete "
                       data-ref="{{$column->field->attributes()->ref}}"
                       data-fields="{{json_encode($mapData)}}"
                        {{ (!empty($onlyView) && $onlyView === true) ? 'disabled' : '' }}>
                @break

                @case('actions')
                @foreach($column->field->actions->action as $action)
                    @if(!empty($isNew))
                        @if($action->attributes()->type == 'print')
                            @continue
                        @endif
                    @else

                    @endif
                    @if (!empty($onlyView) && $onlyView === true)
                        @continue
                    @endif
                    <button type="button" data-id="{{!empty($data) ? $data->id : ''}}"
                            class="{{(empty($raw)) ? $action->attributes()->type : ' s-table-delete'}}"><i
                                class="{{$action->attributes()->icon}}"></i></button>
                @endforeach

                @break

                @case('autoincrement')
                <span class="incrementNumber">{{empty($incrementNumber) ? $num : $incrementNumber}}</span>
                @break

                @case('date')
                <input value="{{!empty($data->{$name}) ? $data->{$name} : @$column->field->attributes()->defaultValue }}"
                       name="{{(!empty($isApplication)) ? "product_batches[{$data->id}][{$column->field->attributes()->dataName}]" : str_replace('_index_',$num,$column->field->attributes()->name)}}"
                       type="date"
                       class="form-control {{$column->field->attributes()->defaultClass}}"
                        {{ (!empty($onlyView) && $onlyView === true) ? 'disabled' : '' }}>
                @break

            @endswitch
            @if (!empty($isApplication))
                <div class="form-error"
                     id="form-error-product_batches-{{ $data->id }}-{{$column->field->attributes()->dataName}}"></div>
            @else
                <div class="form-error" id="form-error-customData-{{ (string)$column->field->attributes()->id }}"></div>
            @endif
        </td>

    @endforeach
</tr>