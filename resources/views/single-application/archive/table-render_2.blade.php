<div class="list-data_wrapper">
    <table class="table table-bordered {{$columns->attributes()->renderTable}}" data-module="{{$columns->attributes()->module}}">
        <thead>
        <tr>
            @foreach($columns->fieldset as $column)
                <td>{{trans($column->field->attributes()->title)}}</td>
            @endforeach
        </tr>
        </thead>
        @if(count($data) > 0)
            <tbody>
            @foreach($data as $key=>$value)
                <tr>

                    @foreach($columns->fieldset as $column)

                        @if($column->field->actions)
                            <td width="115px">
                                @foreach($column->field->actions->action as $action)
                                    @if(!empty($isNew) && $isNew)
                                        @if($action->attributes()->type == 'uploadFile')
                                            @continue
                                        @endif
                                    @else
                                        @if($action->attributes()->type == 'delete')
                                            @continue
                                        @endif
                                    @endif
                                    <button type="button"><i class="{{$action->attributes()->icon}}"></i></button>
                                @endforeach
                            </td>

                        @endif

                    @endforeach

                    <td>{{$key+1}}</td>
                    @foreach($value as $v)
                        <td>
                            {{$v}}
                        </td>
                    @endforeach
                </tr>
            @endforeach
            </tbody>
        @endif
    </table>
</div>