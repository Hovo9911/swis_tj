<thead>
<tr>
    @foreach($columns as $key=>$column)
        @if(!empty($hideColumns) && in_array($column,$hideColumns))
            @continue
        @endif
        <th>{{trans('swis.single_app.'.$column)}}</th>
    @endforeach
    <th></th>
</tr>
</thead>
<tbody>
@foreach($data as $value)
    <tr class="{{(isset($module) && $module == 'products' && isset($subAppProducts) && in_array($value->product_number,$subAppProducts)) ? 'disabled' : ''}}">
        @foreach($value as $key=>$val)

            <?php $tooltipText = ''; ?>
            <?php
                if (!empty($tooltipColumnsArr)) {
                    if (isset($tooltipColumnsArr[$key])) {
                        $tCode = $tooltipColumnsArr[$key];
                        if (!empty($value->$tCode)) {
                            $tooltipText = $value->$tCode;
                        }
                    }
                }
            ?>
            @if(!empty($hideColumns) && in_array($key,$hideColumns))
                @continue
            @endif
            <?php
            if (!empty($refDataColumnsArr)) {
                if (isset($refDataColumnsArr[$key])) {
                    $refTableData = getReferenceRows($refDataColumnsArr[$key], false, $val);

                    if (!is_null($refTableData)) {
                        $val = $refTableData->name ?? '';
                    }
                }
            }
            ?>

            <td><span {!! ($key == 'product_number') ? 'class="incrementNumber" ' : '' !!} {!!  (!empty($tooltipText)) ? 'data-toggle=tooltip title="'.$tooltipText.'"'  : ''!!}>{{$val}}</span></td>
        @endforeach
        <td>
            <button type="button" class="btn s-table-edit btn-edit" data-id="{{$value->id or 0}}" title="{{ trans('swis.base.tooltip.edit.button') }}" data-toggle="tooltip"> <i class="fa fa-pen"></i></button>
            <button type="button" class="btn-remove s-table-delete" data-id="{{$value->id or 0}}"><i class="fa fa-times"></i></button>
        </td>
    </tr>
@endforeach
</tbody>
