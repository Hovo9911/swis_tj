{{dd('a')}}
@if(count($data) > 0)
    @foreach($data as $key=>$value)
        <tr class="{{(!empty($blockModule) && $blockModule == 'products' && !empty($subAppDisabledProducts) && in_array($value->product_number,$subAppDisabledProducts)) ? 'disabled' : ''}}">

            @if(!empty($columnsActions))
                @if($columnsActions->actions)
                    <td width="115px">
                        @foreach($columnsActions->actions->action as $action)
                            @if(!empty($isNew) && $isNew)
                                @if($action->attributes()->type == 'uploadFile')
                                    @continue
                                @endif
                            @else
                                @if($action->attributes()->type == 'delete')
                                    @continue
                                @endif
                            @endif
                        @endforeach
                    </td>
                @endif
            @endif

            @foreach($value as $k=>$v)

                <?php $tooltipText = ''; ?>

                <?php
                if (!empty($tooltipColumnsArr)) {
                    if (isset($tooltipColumnsArr[$k])) {
                        $tCode = $tooltipColumnsArr[$k];
                        if (!empty($value->$tCode)) {
                            $tooltipText = $value->$tCode;
                        }

                    }
                }
                ?>
                @if(!empty($hideColumns) && in_array($k,$hideColumns)) @continue @endif
                <td>
                    <?php
                    if (!empty($refDataColumnsArr)) {
                        if (isset($refDataColumnsArr[$k])) {
                            $refTableData = getReferenceRows($refDataColumnsArr[$k], false, $v);

                            if (!is_null($refTableData)) {
                                $v = $refTableData->name ?? '';
                            }
                        }
                    }
                    ?>

                    <span {!! ($k == 'product_number') ? 'class="incrementNumber" ' : '' !!} {!!  (!empty($tooltipText)) ? 'data-toggle=tooltip title="'.$tooltipText.'"'  : ''!!}>{{$v}}</span>
                </td>
            @endforeach

            @if(!empty($actions) && $actions == 'true')

                <td>
                    <button type="button" class="btn s-table-edit btn-edit" data-id="{{@$value->id}}" title="{{ trans('swis.base.tooltip.edit.button') }}" data-toggle="tooltip" ><i class="fa fa-pen"></i></button>
                    <button type="button" class="btn-remove s-table-delete" data-id="{{@$value->id}}"><i class="fa fa-times"></i></button>
                </td>
            @endif
        </tr>
    @endforeach
@endif