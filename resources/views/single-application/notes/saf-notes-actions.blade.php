<p class="notes__title ver-top-box"><b>{{ trans('swis.saf.notes.current_state') }}-</b> {{ trans('swis.saf.notes.state.'.$saf->status_type) }}</p>
@include('single-application.notes.saf-notes-list')

@if(config('global.digital_signature'))
    @include('single-application.digital-signatures.digital-signature-check-modal')
@endif

