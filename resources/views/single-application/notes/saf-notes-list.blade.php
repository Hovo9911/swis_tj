@if(count($notes) > 0)
    <div class="ibox-content no-padding  notes__out ">
        <div class="notes-block saf-notes">
            <ul class="list-group notes__list">
                @foreach ($notes as $note)
                    <li class="list-group-item notes__item">
                        <div class="notes__item--inner">
                            <div class="table-cell notes__item--left">
                                <p>{!! $note->note !!}</p>
                            </div>
                            <div class="table-cell text-right notes__item--right">
                                <small class="block text-muted"><i class="fa fa-clock-o"></i>
                                    {{ $note->created_at }}
                                </small>

                                @if ($note->to_saf_holder)
                                    <small class="text-muted">
                                        <i class="fas fa-envelope"></i> {!! $note->note_from_sub_application !!}
                                    </small>
                                @endif

                                @if ($note->type == \App\Models\SingleApplicationNotes\SingleApplicationNotes::NOTE_TYPE_MESSAGE && $note->send_from_module == \App\Models\SingleApplicationNotes\SingleApplicationNotes::NOTE_SEND_MODULE_SAF)
                                    <small class="text-muted">
                                        <i class="fas fa-envelope"></i> {!! $note->message_from_saf !!}
                                    </small>
                                @endif
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endif