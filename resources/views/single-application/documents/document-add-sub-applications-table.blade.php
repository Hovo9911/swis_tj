@if(count($subApplications))

<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th style="width: 30px"><input type="checkbox" id="checkboxAllDocumentSubApplications"></th>
                <th>{{trans('swis.saf.document.add_sub_application.inc_id')}}</th>
                <th>{{trans('swis.saf.document.add_sub_application.agency_name')}}</th>
                <th>{{trans('swis.saf.document.add_sub_application.sub_division_name')}}</th>
                <th>{{trans('swis.saf.document.add_sub_application.sub_application_type')}}</th>
                <th>{{trans('swis.saf.document.add_sub_application.sub_application_name')}}</th>
                <th>{{trans('swis.saf.document.add_sub_application.sub_application_products')}}</th>
            </tr>
        </thead>
        <tbody>
        @foreach($subApplications as $subApplication)
            <tr>
                <td><input class="sub-application-checkbox" data-sub-application-type="{{$subApplication['sub_application_type']}}" value="{{$subApplication['id']}}" {{in_array($subApplication['id'] ,$documentSubApplications) && in_array($subApplication['id'],$allFormedSubApplications) ? 'checked readonly' : ''}} type="checkbox"></td>
                <td>{{$subApplication['increment_number']}}</td>
                <td>{{$subApplication['agency_name']}}</td>
                <td>{{$subApplication['sub_division_name']}}</td>
                <td>{{$subApplication['sub_application_type']}}</td>
                <td>{{$subApplication['sub_application_name']}}</td>
                <td>{{$subApplication['sub_application_products']}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@else
    <h3>{{trans('swis.saf.document.add_sub_application.no_sub_applications.message')}}</h3>
@endif
