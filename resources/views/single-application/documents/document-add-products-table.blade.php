@if($products->count())
<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th style="width: 30px"><input type="checkbox" id="checkboxDocumentProducts"></th>
                <th>{{trans('swis.single_app.product_number')}}</th>
                <th>{{trans('swis.single_app.product_code')}}</th>
                <th>{{trans('swis.single_app.commercial_description')}}</th>
            </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
            <?php
            $hsCode = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_HS_CODE_6,$product->product_code);
            ?>
            <tr>
                <td><input data-pr-number="{{$productsIdNumbers[$product->id]}}" class="pr" {{isset($selectedProducts) &&  in_array($product->id, $selectedProducts) && $product->isDisabled() ? 'readonly checked' : ''}} value="{{$product->id}}" type="checkbox"></td>
                <td>{{$productsIdNumbers[$product->id]}}</td>
                <td><span data-toggle="tooltip" data-placement="top" title="{{$product->product_description}}">{{$hsCode->code ?? ''}}</span></td>
                <td>{{$product->commercial_description}}</td>
            </tr>
        @endforeach
        </tbody>
</table>
</div>

@else
    <h3>{{trans('swis.saf.document.add_products.no_products.message')}}</h3>
@endif
