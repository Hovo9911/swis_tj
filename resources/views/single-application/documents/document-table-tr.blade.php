@if(empty($renderInput))

    @if(count($attachedDocumentProducts) > 0)

        <?php $num = $num ?? 1 ?>
        @foreach($attachedDocumentProducts as $attachedDocumentProduct)
            <?php
            $document = is_null($attachedDocumentProduct->document) ? $attachedDocumentProduct : $attachedDocumentProduct->document;
            $documentSubApplications = $attachedDocumentProduct->documentSubApplications;

            $canDeleteDocument = isset($addFromDocList) || ($attachedDocumentProduct->canDeleteDocument() && $attachedDocumentProduct->added_from_module == \App\Models\Document\Document::DOCUMENT_CREATE_MODULE_SAF);

            ?>

            <tr data-num="{{$num}}">
                <td class="text-center">@if($canDeleteDocument)<input type="checkbox" value="{{$document->id}}">@endif</td>
                <td class="text-center">{{$num}}</td>
                <td>{{trans('swis.document.document_form.'.$document->document_form.'.title')}}</td>
                <td class="text-break-word">{{$document->document_access_password}}</td>
                <td>{{$document->document_type}}</td>
                <td class="text-break-word">{{$document->document_name}}</td>
                <td>{{$document->document_description}}</td>
                <td>{{$document->document_number}}</td>
                <td>{{$document->document_release_date}}</td>
                <td class="text-break-word">{{$document->fileBaseName()}}</td>
                <td>
                    <span class="product-ids-list text-break-word">{{isset($saf) ? $attachedDocumentProduct->productNumbers($saf->productsOrderedList()) : ''}}</span>

                    @if($attachedDocumentProduct->added_from_module == \App\Models\Document\Document::DOCUMENT_CREATE_MODULE_SAF || (isset($addFromDocList) && $addFromDocList))
                        <input type="hidden" class="document" name="document_id" value="{{$document->id}}">
                        <input class="product-ids" value="{{!isset($addFromDocList) ? $attachedDocumentProduct->productIdList() : ''}}" name="document_products" type="hidden">
                        @if(isset($saveMode) && $saveMode != 'view')
                            <button class="btn btn-success pull-right btn-xs addProductsDocument" data-update="true" data-document="{{$document->id}}" title="{{ trans('swis.base.tooltip.add_product.button') }}" data-toggle="tooltip"><i class="fa fa-plus"></i></button>
                        @endif
                    @endif
                </td>
                <td>
                    <span class="sub-applications-codes-list">{{!isset($addFromDocList) ? $attachedDocumentProduct->subApplicationsList($documentSubApplications,'code') : ''}}</span>

                    @if(isset($saveMode) && $saveMode != 'view')
                        <input type="hidden" class="document" name="document_id" value="{{$document->id}}">
                        <input class="sub-application-ids" value="{{!isset($addFromDocList) ? $attachedDocumentProduct->subApplicationsList($documentSubApplications) : ''}}" name="document_sub_applications" type="hidden">

                        <button class="btn btn-success pull-right btn-xs addSubApplicationsDocument" data-update="true" data-document="{{$document->id}}" title="{{ trans('swis.base.tooltip.add_sub_application.button') }}" data-toggle="tooltip"><i class="fa fa-plus"></i></button>
                    @endif
                </td>
                <td class="text-nowrap text-center">

                    <a target="_blank" href="{{$document->filePath()}}" class="btn btn-success btn-xs btn--icon" title="{{ trans('swis.base.tooltip.view.button') }}" data-toggle="tooltip"><i class="fas fa-eye"></i></a>

                    @if(isset($saveMode) && $saveMode != 'view' && $canDeleteDocument)
                         <button class="dt-document" title="{{ trans('swis.saf.documents.delete_document.button') }}" data-toggle="tooltip"><i class="fas fa-times"></i></button>
                    @endif
                </td>
            </tr>
            <?php $num++ ?>
        @endforeach

    @endif

@else
    <?php
    $gUploader = new \App\GUploader('document_report');
    ?>
    <tr data-num="{{$num}}">
        <td class="text-center"><input type="checkbox" value="" class="hidden doc-checkbox"></td>
        <td><input type="hidden" name="document_id" class="document" value="">{{$num}}</td>
        <td class="document-form"></td>
        <td class="access-password"></td>
        <td>
            <input type="text" autocomplete="off" class="autocomplete form-control"
                   data-ref="{{\App\Models\ReferenceTable\ReferenceTable::getReferenceIdByTableName(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS)}}"
                   data-ref-type="autocomplete"
                   data-fields="{{json_encode([['from'=>'name','to'=>'document_name'],['from'=>'code','to'=>'document_type']])}}"
                   placeholder="autocomplete"
                   name="document_type">

            <input type="hidden" name="document_type_value" value="{{$document->document_type_value or ''}}">
            <div class="form-error" id="form-error-document_type-{{$num}}"></div>
        </td>
        <td>
            <input type="text" class="form-control" name="document_name" readonly="readonly">
            <div class="form-error" id="form-error-document_name-{{$num}}"></div>
        </td>
        <td>
            <input type="text" class="form-control" name="document_description">
            <div class="form-error" id="form-error-document_description-{{$num}}"></div>
        </td>
        <td>
            <input type="text" class="form-control" name="document_number">
            <div class="form-error" id="form-error-document_number-{{$num}}"></div>
        </td>
        <td>
            <div class='input-group date'>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                <input placeholder="{{setDatePlaceholder()}}" autocomplete="off" type="text" class="form-control" name="document_release_date">
            </div>
            <div class="form-error" id="form-error-document_release_date-{{$num}}"></div>
        </td>
        <td class="text-break-word">
            <div class="g-upload-container">
                <div class="g-upload-box">
                    <label style="width:35px"><i class="fas fa-upload btn btn-success btn-sm"></i> <input style="visibility: hidden" class="dn g-upload" type="file" name="document_file" {{$gUploader->isMultiple() ? 'multiple' : ''}} data-conf="document_report" data-action="{{urlWithLng('/g-uploader/upload')}}" accept="{{$gUploader->getAcceptTypes()}}"/></label>
                </div>
                <div class="license-form__uploaded-file-list g-uploaded-list "></div>
                {{-- Progress bar for file uploding process, if you want to show process--}}
                <progress class="progressBarFileUploader" value="0" max="100"></progress>
                {{-- Progress bar for file uploding process --}}
                <div class="form-error-text form-error-files fb fs12"></div>

                <div class="form-error" id="form-error-document_file-{{$num}}"></div>
            </div>
        </td>
        <td>
            <span class="product-ids-list"></span>

            <input class="product-ids" name="document_products" type="hidden">
            <button class="btn btn-success pull-right btn-xs addProductsDocument" title="{{ trans('swis.base.tooltip.add_product.button') }}" data-toggle="tooltip"><i class="fa fa-plus"></i></button>
        </td>
        <td>
            <span class="sub-applications-codes-list"></span>

            <input class="sub-application-ids" name="document_sub_applications" type="hidden">
            <button class="btn btn-success pull-right btn-xs addSubApplicationsDocument" title="{{ trans('swis.base.tooltip.add_sub_application.button') }}" data-toggle="tooltip"><i class="fa fa-plus"></i></button>
        </td>
        <td class="actions-list text-nowrap text-center">
            <button type="button" class="btn btn-primary btn-xs storeDocument btn--icon" title="{{ trans('swis.base.tooltip.store_document.button') }}" data-toggle="tooltip"><i class="fa fa-check"></i></button>
            <button class="dt-document" title="{{ trans('swis.saf.documents.delete_document.button') }}" data-toggle="tooltip"><i class="fas fa-times"></i></button>
        </td>
    </tr>

@endif