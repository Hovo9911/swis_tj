@extends('layouts.app')
<?php
//$t1 = microtime(1);
?>
@section('title'){{trans('swis.single_application.'.$regime.'.create.title')}}@stop

@section('contentTitle')
    {{trans('swis.single_application.'.$regime.'.create.title')}}
@stop

@section('topScripts')
    <script>
        var safRegime = "{!! $saf->regime !!}";
        var safRegimeImport = "{{$safRegimeImport}}";
        var safRegimeExport = "{{$safRegimeExport}}";

        var safNumber = "{!! $saf->regular_number !!}";
        var safCurrentStatus = "{{ $saf->status_type or "" }}";
        var safStatusTypeCreated = "{{$safCreatedType}}";
        var safStatusTypeDraft = "{{$safDraftType}}";
        var safStatusTypeFormed = "{{$safSendType}}";

        var hasSubmittedSubApplication = "{{ $hasSubmittedSubApplication}}";
        var viewType = "{{ $saveMode }}";
        var defaultCurrency = "{{ config('swis.default_currency') }}";

        var refKilogramCode = "{{\App\Models\ReferenceTable\ReferenceTable::REF_KILOGRAM_CODE}}"
    </script>
    <?php
        $jsTrans->addTrans([
            "swis.saf.lab_examination.select_product_placeholder",
            "swis.saf.lab_examination.no_results_found_products",
            "swis.saf.lab_examination.select_laboratory_placeholder",
            "swis.saf.lab_examination.no_results_found_laboratories",
            "swis.saf.lab_examination.select_measurement_unit_placeholder",
            "swis.saf.lab_examination.select_indicator_placeholder",
            "swis.saf.lab_examination.no_results_found_indicators"
        ]);
    ?>
@endsection

@section('form-buttons-top')
    @if($saveMode !== 'view')
        <div class="buttons-container">

            <button type="submit" class="mr-10 btn btn-success sendData  {{$saf->status_type == $safSendType ? 'hide' : ''}}" data-type="draft">{{trans('core.base.button.draft')}}</button>
            <button type="button" class="mr-10 btn btn-warning sendData" data-type="verify">{{trans('swis.single_app.verify_operation_button')}}</button>
            <button type="button" class="mr-10 btn btn-primary sendData" data-type="send">{{trans('core.base.button.send')}}</button>

            @if(!$hasSubmittedOrRequestedSubApplication && $saf->status_type != $safCreatedType)
                <button type="button" class="mr-10 btn btn-danger sendData"
                        data-type="void">{{trans('swis.single_app.void_operation_button')}}</button>
            @endif

            @if ($saf->status_type == $safCreatedType)
                <button type="button" class="btn btn-danger delete-saf-data"><i class="fa fa-times"></i> {{trans('swis.saf.delete_saf')}}</button>
            @endif

            @if ($saf->status_type != $safCreatedType)
                {!! renderCancelButton() !!}
            @endif

        </div>

    @else
        {!! renderCancelButton() !!}
    @endif
@stop

@section('content')
    {{-- Modal Text Translations --}}
    @include('partials.translations',['transType'=>'modal'])

    <?php

    // Global Data
    $safGlobalDefaultData = $saf->getSafGlobalDefaultValue();
    $subAppNeedAttachDocs = Session::get($saf->regular_number . '_need_attach_docs');

    $xmlTabSidesKey = \App\Models\SingleApplication\SingleApplication::XML_MODULE_SIDES;
    $xmlTabGeneralKey = \App\Models\SingleApplication\SingleApplication::XML_MODULE_GENERAL;
    $xmlTabAttachedDocKey = \App\Models\SingleApplication\SingleApplication::XML_MODULE_ATTACHED_DOCUMENTS;
    $xmlTabSubApplicationsKey = \App\Models\SingleApplication\SingleApplication::XML_MODULE_SUB_APPLICATIONS;

    $tabActiveKey = $xmlTabSidesKey;
    if ($saf->status_type != $safCreatedType) {
        $tabActiveKey = $xmlTabGeneralKey;
    }

    if($saf->status_type == $safSendType){
        $tabActiveKey = $xmlTabSubApplicationsKey;
    }
    ?>

    @if($saf->status_type == $safSendType && !$hasSubmittedSubApplication && $saf->subApplications->count())
        <div class="alert alert-primary" role="alert">
            {{trans('swis.saf.create_manual_sub_application.attach_product.message')}}
        </div>

        <div class="alert alert-primary" role="alert">
            {{trans('swis.saf.products_under_quarantine_control.message')}}
        </div>
    @endif

    <div id="alertMessages">
        @if(!is_null($subAppNeedAttachDocs))
            @foreach($subAppNeedAttachDocs as $docAlert)
                <div class="alert alert-danger">
                    {!! $docAlert !!}
                </div>
            @endforeach
        @endif
    </div>

    <div id="subApplicationAlertMessages"></div>

    @if($saf->status_type == $safSendType && count($safProductsNotFormedSubApplication))
        <div class="alert alert-info" role="alert">
            {{trans('swis.saf.products.not_formed_sub_application.message',['product_codes'=>implode(', ',$safProductsNotFormedSubApplication)])}}
        </div>
    @endif

    <form class="form-horizontal form-horizontal--fixed fill-up" id="form-data" action="{{urlWithLng('single-application')}}" autocomplete="off">
        <div class="tabs-container loading-close">
            <ul class="nav nav-tabs">
                @foreach($simpleAppTabs as $key=>$simpleAppTab)
                    @if((bool)$simpleAppTab->attributes()->hideOnSaf)
                        @continue
                    @endif
                    <li class="{{($simpleAppTab->attributes()->key == $tabActiveKey) ? 'active' : ''}}{{   (!empty($simpleAppTab->attributes()->show) && $simpleAppTab->attributes()->show == 'false') ?  ($saf->status_type == \App\Models\SingleApplication\SingleApplication::SEND_TYPE) ? '' : 'hide' : '' }}">
                        <a data-toggle="tab" data-auto-focus="off" href="#tab-{{$simpleAppTab->attributes()->key}}">{{trans($simpleAppTab->attributes()->name)}}</a>
                    </li>
                @endforeach
            </ul>

            <div class="tab-content">
                @foreach($simpleAppTabs as $key=>$simpleAppTab)
                    
                    @if((bool)$simpleAppTab->attributes()->hideOnSaf)
                        @continue
                    @endif

                    <?php
                    $currentTabKey = (string)$simpleAppTab->attributes()->key;
                    ?>

                    <div id="tab-{{$currentTabKey}}" class="tab-pane {{($currentTabKey == $tabActiveKey) ? 'active' : ''}}">

                        <?php $isSideTab = false ?>
                        @if((string)$currentTabKey == $xmlTabSidesKey)
                            <?php $isSideTab = true ?>
                            @foreach($simpleAppTab->typeBlock as $key=>$typeBlock)

                                @if((string)$typeBlock->attributes()->regime == $regime)
                                    <?php $simpleAppTab = $typeBlock; ?>
                                @endif

                            @endforeach
                        @endif

                        @foreach($simpleAppTab->block as $block)
                            @if($block->attributes()->type != 'subForm' && (empty($block->attributes()->draw) || $block->attributes()->draw != 'false'))
                                <div class="panel panel-default"

                                    @if(!empty($block->attributes()->idName)) id="{{$block->attributes()->idName}}" @endif>
                                    @if(!empty($block->attributes()->name))<div class="panel-heading">{{ trans($block->attributes()->name)}}</div>@endif

                                    <div class="panel-body">

                                    @if($block->attributes()->type != 'dataTable')
                                            @include('single-application.block-fieldset',['block'=>$block,'safGlobalDefaultData'=>$safGlobalDefaultData])
                                        @else

                                            <?php $currentModule = (string)$block->attributes()->module ?>

                                            @switch($currentModule)

                                                @case('subtitles')

                                                    @include('single-application.sub-applications.sub-application-table')

                                                @break

                                                @case('responsibilities')

                                                    @include('single-application.obligations.obligations-table',['block'=>$block])

                                                @break

                                                @case('products')

                                                     @include('single-application.products.products-table',['block'=>$block])

                                                @break

                                                @case('lab_examination')

                                                    @include('single-application.laboratories.lab-examination')

                                                @break

                                                @default

                                                {{--          archive/table.blade           --}}

                                                @break

                                            @endswitch
                                        @endif

                                        @if($block->attributes()->multiple)
                                            <button type="button" class="btn btn-primary {{(!empty($block->attributes()->subForm)) ? 'subForm' : ''}} {{(!empty($block->attributes()->renderTr)) ? 'renderTr' : ''}}">
                                                <i class="fa fa-plus"></i> <span class="documents-plusBtn__text"> {{trans('swis.saf.products.add_product_button')}}</span>
                                            </button>
                                        @endif

                                    </div>
                                </div>
                            @endif

                        @endforeach

                        @if($currentTabKey == $xmlTabAttachedDocKey)
                            @include('single-application.documents.document-module')
                        @endif
                    </div>
                @endforeach
            </div>
        </div>

        <input type="hidden" name="status_type" id="statusType" value="draft">
        <input type="hidden" id="formIsChanged" value="0">

    </form>

    <input type="hidden" id="safNumber" value="{{$saf->regular_number}}">

@endsection


@section('footer-part')
    <?php
    $simpleAppTabs = \App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure::lastXml();

    $blocks = $simpleAppTabs->xpath('/tabs/tab/block[@type="subForm"]');
    ?>

    @foreach ($blocks as $block)

        <div class="sub-form-modals">
            <div class="sub-form-add">
                @include('single-application.products.products-edit-form',['block' => $block])
            </div>
            <div class="sub-form-edit"> </div>
        </div>

    @endforeach

    <div class="modal fade" id="sendMessageModal" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <h2>{{trans('swis.single_app.notes')}}</h2>
                    <div class="form-group m-b-none">
                        <textarea class="form-control" rows="10"></textarea>
                        <div class="form-error" id="form-error-message"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary sendMessageSubApp">{{trans('swis.saf.sub_application.send_message.button')}}</button>
                    <button type="button" class="btn btn-default closeMessageSubApp" data-dismiss="modal">{{trans('core.base.button.close')}}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="PayObligationModal" role="dialog"></div>

    <div class="modal fade" id="laboratoryCrudModal" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 1150px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="text-center">{{trans('swis.saf.lab_examinations.form.title')}}</h2>
                </div>

                <div class="modal-body">

                    <div class="col-md-6 col-md-offset-3">
                        <select class="form-control select2" id="selectLabExpertise">
                            <option value="">{{ trans("swis.saf.lab_examination.select_type") }}</option>
                            @foreach(\App\Models\Laboratory\Laboratory::LAB_TYPES as $key => $value)
                                <option value="{{ $value }}"> {{ trans("swis.saf.lab_examination.{$key}.option") }} </option>
                            @endforeach
                        </select>
                    </div><br /><br /><hr />
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger alertDivForLabExamination" role="alert" style="display: none;"></div>
                        </div>
                    </div>
                    <div id="labExpertiseCrudModalBody">

                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="generateLaboratoryApplication" disabled>{{trans('swis.saf.lab_examination.generate.button')}}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('core.base.button.close')}}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="laboratoryResultModal" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 1150px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="text-center">{{trans('swis.saf.lab_examinations.result.title')}}</h2>
                </div>

                <div class="modal-body">
                    <div class="labExpertiseResultModalBody">

                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('core.base.button.close')}}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteSafModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-body">
                    <h4 class="m-b-none m-t-none">{{trans('swis.single_app.delete.modal.message')}}</h4>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-default btn-cancel" data-dismiss="modal">{{trans('swis.single_app.delete.modal.cencel.button')}}</button>
                    <button class="btn btn-primary btn-delete">{{trans('swis.single_app.delete.modal.delete.button')}}</button>
                </div>

            </div>
        </div>
    </div>

    <!-- Delete Products modal -->
    <div class="modal fade" id="deleteProductsModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="fb modal__title">{{trans('core.base.delete_confirm.title')}}</h4>
                    <button type="button" class="close modal__close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" >
                    <div class="modal-delete-confirm-message"><strong class="text-danger">{{trans('swis.saf.products.modal.delete.message')}}</strong></div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default btn-cancel" data-dismiss="modal">{{trans('core.base.button.close')}}</button>
                    <button class="btn btn-primary btn-delete">{{trans('core.base.button.delete')}}</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('form-buttons-bottom')
    @if($saf->status_type == $safSendType)
        <a href="{{urlWithLng('single-application/export-saf/'.$saf->regular_number)}}" type="button" class="pull-right btn btn-info">{{trans('swis.saf.export_xml.button')}}</a>
    @endif
    @if($saveMode !== 'view')

        <button class="mc-nav-button-save hide"></button>
        <button type="submit" class="mr-10 btn btn-success sendData  {{$saf->status_type == $safSendType ? 'hide' : ''}}" data-type="draft">{{trans('core.base.button.draft')}}</button>
        <button type="button" class="mr-10 btn btn-warning sendData" data-type="verify">{{trans('swis.single_app.verify_operation_button')}}</button>
        <button type="button" class="mr-10 btn btn-primary sendData" data-type="send">{{trans('core.base.button.send')}}</button>

        @if(!$hasSubmittedOrRequestedSubApplication && $saf->status_type != $safCreatedType)
            <button type="button" class="mr-10 btn btn-danger sendData" data-type="void">{{trans('swis.single_app.void_operation_button')}}</button>
        @endif

        @if ($saf->status_type == $safCreatedType)
            <button type="button" class="btn btn-danger delete-saf-data"><i class="fa fa-times"></i>{{trans('swis.saf.delete_saf')}}</button>
        @endif

        @if ($saf->status_type != $safCreatedType)
            {!! renderCancelButton() !!}
        @endif
    @else
        {!! renderCancelButton() !!}
    @endif
@stop

@section('scripts')
    <script src="{{asset('assets/helix/core/plugins/bootstrap-typeahead/bootstrap-typeahead.js')}}"></script>
    <script src="{{asset('js/single-application/main.js')}}"></script>
    <script src="{{asset('js/single-application/sub-application.js')}}"></script>
    <script src="{{asset('js/single-application/document.js')}}"></script>
    <script src="{{asset('js/single-application/product.js')}}"></script>
    <script src="{{asset('js/single-application/obligation.js')}}"></script>
    <script src="{{asset('js/single-application/laboratories.js')}}"></script>
    <script src="{{asset('js/single-application/digital-signature.js')}}"></script>
    <script src="{{asset('js/userType.js')}}"></script>
    <script src="{{asset('js/guploader.js')}}"></script>
@endsection

<?php

//dd($_SESSION)
/*dd($_SESSION);

$t2 = microtime(1);
die($t2 - $t1);
 */?>