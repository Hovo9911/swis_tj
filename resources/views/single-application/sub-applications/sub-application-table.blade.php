<?php
use App\Models\BaseModel;
use App\Models\RegionalOffice\RegionalOffice;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use App\Models\ReferenceTable\ReferenceTable;
use Illuminate\Support\Facades\DB;
use \App\Models\Agency\Agency;

$subApplications = SingleApplicationSubApplications::select(['id','agency_id','agency_subdivision_id','subtitle_type','name','current_status','document_number','status_date','access_token','express_control_id','manual_created', 'pdf_hash_key', 'reference_classificator_id', 'document_code'])
    ->with(['productList','applicationCurrentState'])
    ->where('saf_number', $saf->regular_number)
    ->notLabType()
    ->orderByRaw('document_number ASC NULLS last')
    ->orderByAsc()
    ->get();

//
$allReferenceClassificatorOfDocuments = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS);
$allReferenceExpressControl = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_EXPRESS_CONTROL);
$allReferenceSubdivisions = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS);

//
$agencies = Agency::select(DB::raw('COALESCE(short_name, name) as name'),'id','subdivision_mark_type')->joinMl()->get()->keyBy('id')->all();
$safProductsOrderedList = $saf->productsOrderedList();

//
$incrementNumber = 0;
$statusRequested = SingleApplicationSubApplications::STATUS_REQUESTED;
$actionSend = SingleApplicationSubApplications::ACTION_SEND;
?>
<div class="list-data_wrapper">
    <div id="subApplicationErrorList"></div>
    <div class="table-responsive {{!$subApplications->count() ? 'hide' : ''}}">
        <table class="table table-bordered table-hover" id="subApplicationList">
            <thead>
                <tr>
                    <th style="width:40px" class="text-center">{{trans('swis.single_app.id')}}</th>
                    <th style="width: 135px">{{trans('swis.single_app.agency_id')}}</th>
                    <th style="width: 130px">{{trans('swis.single_app.agency_subdivision_id')}}</th>
                    <th style="width: 135px">{{trans('swis.single_app.subtitle_type')}}</th>
                    <th style="width: 110px">{{trans('swis.single_app.name')}}</th>
                    <th style="width: 60px">{{trans('swis.single_app.products')}}</th>
                    <th style="width: 120px">{{trans('swis.single_app.current_status')}}</th>
                    <th style="width: 80px">{{trans('swis.single_app.document_number')}}</th>
                    <th style="width: 70px">{{trans('swis.single_app.status_date')}}</th>
                    <th style="width: 80px">{{trans('swis.single_app.access_token')}}</th>
                    <th style="width: 100px">{{trans('swis.single_app.express_control_id')}}</th>
                    <th style="width: 112px"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($subApplications as $subApplication)
                    <?php

                    $currentStatus = $subApplication->current_status;

                    //
                    if (!is_null($currentStatus)) {
                        $incrementNumber++;
                    }

                    //
                    $canModifyInfo = false;
                    if (is_null($currentStatus) || $currentStatus == $statusRequested) {
                        $canModifyInfo = true;
                    }

                    //
                    $currentStateName = $currentStateDate = '';
                    if(!is_null($subApplication->applicationCurrentState)){

                        $currentStateDate = $subApplication->applicationCurrentState->created_at;

                        if(!is_null($subApplication->applicationCurrentState->state)){
                            $currentStateName = $subApplication->applicationCurrentState->state->current()->state_name;
                        }
                    }

                    //
                    $orderedProducts = [];
                    foreach ($subApplication->productList as $product){
                        if(isset($safProductsOrderedList[$product->id])){
                            $orderedProducts[] = $safProductsOrderedList[$product->id];
                        }
                    }

                    sort($orderedProducts);

                    $firstTenProducts = array_slice($orderedProducts, 0, 10);

                    //
                    if($canModifyInfo){
                        $queryExpressWhere = function ($query) use ($subApplication) {
                            return $query->where("document", $subApplication->reference_classificator_id ?? 0)->orWhereNull('document');
                        };

                        $refExpressControl = getReferenceRows(ReferenceTable::REFERENCE_EXPRESS_CONTROL, false, false, ['id', 'code', 'name'], 'get', false, false, $queryExpressWhere);
                        $referenceSubDivisions = RegionalOffice::getRefSubdivisionsByConstructorDocument($subApplication->document_code);
                    }

                    ?>
                    <tr>
                        <td class="text-center">
                            @if($currentStatus) {{$incrementNumber}}@endif
                        </td>
                        <td><span data-toggle="tooltip" title="{{$agencies[$subApplication->agency_id]['name'] ?? '' }}" class="agency_id">{{$agencies[$subApplication->agency_id]['name'] ?? '' }}</span></td>
                        <td>
                            @if($canModifyInfo)
                                @if($referenceSubDivisions->count())
                                <select class="select2 column_agency_subdivision_id" data-select2-allow-clear="false" name="agency_subdivision_id">
                                    @if($referenceSubDivisions->count() != 1)
                                    <option></option>
                                    @endif
                                    @foreach($referenceSubDivisions as $refSubdivision)
                                        <option {{$subApplication->agency_subdivision_id == $refSubdivision->id ? 'selected' : ''}} value="{{$refSubdivision->id}}">{{ $refSubdivision->short_name ?: $refSubdivision->name }}</option>
                                    @endforeach
                                </select>
                                @endif
                                <div class="form-error-agency_subdivision_id form-error-text"></div>
                            @else
                                <?php
                                $referenceSubDivision = $allReferenceSubdivisions[$subApplication->agency_subdivision_id] ?? null;
                                ?>

                                @if($referenceSubDivision)
                                    <span data-toggle="tooltip" title="{{$referenceSubDivision['short_name'] ?: $referenceSubDivision['name']}}">{{$referenceSubDivision['short_name'] ?: $referenceSubDivision['name']}}</span>
                                @endif
                            @endif
                        </td>
                        <td><span data-toggle="tooltip" title="{{$allReferenceClassificatorOfDocuments[$subApplication->reference_classificator_id]['code'] ?? ''}}">{{$allReferenceClassificatorOfDocuments[$subApplication->reference_classificator_id]['code'] ?? ''}}</span></td>
                        <td><span data-toggle="tooltip" title="{{$allReferenceClassificatorOfDocuments[$subApplication->reference_classificator_id]['name'] ?? ''}}">{{$allReferenceClassificatorOfDocuments[$subApplication->reference_classificator_id]['name'] ?? ''}}</span></td>
                        <td><span data-toggle="tooltip" title="{{implode(', ',$orderedProducts)}}" class="products">{{implode(', ',$firstTenProducts)}} {{count($orderedProducts) > 10 ? '...' : ''}}</span></td>
                        <td><span data-toggle="tooltip" title="{{$currentStateName}}">{{$currentStateName}}</span></td>
                        <td><span data-toggle="tooltip" title="{{$subApplication->document_number}}">{{$subApplication->document_number}}</span></td>
                        <td><span data-toggle="tooltip" title="{{$currentStateDate}}">{{$currentStateDate}}</span></td>
                        <td><span data-toggle="tooltip" title="{{$subApplication->access_token}}" class="column_access_token_{{$subApplication->id}} access_token">{{$subApplication->access_token}}</span></td>
                        <td>
                            @if($canModifyInfo)
                                @if($refExpressControl->count())
                                <select class="select2 column_express_control_id" data-select2-allow-clear="false" name="express_control_id">
                                    <option></option>
                                    @foreach($refExpressControl as $expressControl)
                                        <option {{ $subApplication->express_control_id && $expressControl->id == $subApplication->express_control_id ? 'selected' : $expressControl->code == 2 ? 'selected' : '' }} value="{{$expressControl->id}}">{{$expressControl->name}}</option>
                                    @endforeach
                                </select>
                                @endif
                                <div class="form-error-express_control_id form-error-text"></div>
                            @else
                                <?php
                                $expressControl = $allReferenceExpressControl[$subApplication->express_control_id] ?? null;
                                ?>
                                @if($expressControl)
                                    <span data-toggle="tooltip" title="{{$expressControl['name']}}">{{$expressControl['name']}}</span>
                                @endif
                            @endif
                        </td>
                        <td>
                            @if($subApplication->manual_created == SingleApplicationSubApplications::TRUE && is_null($currentStatus))
                                <button data-id="{{$subApplication->id}}" title="{{trans('swis.saf.sub_application_manual.delete.button')}}" class="btn--icon dt-sub-application" type="button" data-toggle="tooltip"><i class="fas fa-times"></i></button>
                            @endif

                            @if($currentStatus != SingleApplicationSubApplications::STATUS_CANCELED && $canModifyInfo)
                                <button class="btn--icon send-sub-application" data-status="{{$subApplication->current_status}}" data-id="{{$subApplication->id}}" id="{{$subApplication->id}}" title="{{trans('swis.saf.sub_application.send.button')}}" data-toggle="tooltip" type="button"><i class="fas fa-long-arrow-alt-right"></i></button>
                            @endif

                            <a href="{{urlWithLng('/application/'.$subApplication->id.'/'.$subApplication->pdf_hash_key)}}" target="_blank" download>
                                <button class="btn--icon" type="button" title="{{trans('swis.saf.sub_application.print.button')}}" data-toggle="tooltip"><i class="fas fa-print"></i></button>
                            </a>

                            <a href="{{urlWithLng('/application/'.$subApplication->id.'/'.$subApplication->pdf_hash_key)}}" target="_blank">
                                <button class="btn--icon btn-pdf" type="button" title="{{trans('swis.saf.sub_application.view.button')}}" data-toggle="tooltip"><i class="fas fa-eye"></i></button>
                            </a>

                            @if(!is_null($currentStatus))
                                <button class="btn--icon sendMessage" data-status="{{$currentStatus}}" data-id="{{$subApplication->id}}" type="button" title="{{trans('swis.saf.sub_application.message.button')}}" data-toggle="tooltip"><i class="fas fa-envelope"></i></button>
                            @endif

                            {{--@if($subApplication->access_token)
                                <button class="btn--icon regenerate-access-token" data-id="{{$subApplication->id}}" data-toggle="tooltip" title="{{ trans('swis.saf.sub_application.regenerate.button') }}" type="button"><i class="fas fa-sync-alt"></i></button>
                            @endif--}}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <button type="button" class="btn btn-primary plusMultipleFieldSubApplication"><i class="fa fa-plus"></i> <span class="documents-plusBtn__text">{{trans('swis.saf.sub_application.add_document_button')}}</span></button>
</div>

<div id="subApplicationProductsModal" class="modal fade subFormModal" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal__title fb">{{trans('swis.saf.sub_application.add_product.modal.title')}}</h3>
                <button type="button" class="close modal__close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="sub-application-products-list"></div>
                <button type="button" class="btn btn-primary m-t" id="addProductToSubApplication">{{trans('swis.saf.sub_application.add_product')}}</button>
            </div>
        </div>

    </div>
</div>

<div id="subApplicationSendAndPay" class="modal fade" role="dialog" data-backdrop="static"></div>

@if(config('global.digital_signature'))
    @include('single-application.digital-signatures.digital-signature-modal')

    <script src="{{asset('js/rutoken/dependencies.js')}}"></script>
    <script src="{{asset('js/rutoken/crypto.js')}}"></script>
@endif