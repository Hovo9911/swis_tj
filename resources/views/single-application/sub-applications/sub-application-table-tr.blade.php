@if($renderInputs)
    <tr>
        <td></td>
        <td class="agency-name">
            @if($agencies->count())
                <select class="form-control select2 sub-application-agency" name="agency_id">
                    <option></option>
                    @foreach($agencies as $agency)
                        <option value="{{$agency->id}}">{{$agency->name}}</option>
                    @endforeach
                </select>
                <div class="form-error form-error-agency_id"></div>
            @endif
        </td>
        <td>
            <select class="form-control select2 agency-subdivision hide" name="agency_subdivision_id">
                <option value="">{{trans('core.base.label.select')}}</option>
            </select>
            <div class="form-error form-error-agency_subdivision_id"></div>
        </td>
        <td>
            @if($constructorDocuments->count())
                <select class="form-control select2 sub-application-type" name="sub_application_type" >
                    <option value="">{{ trans('core.base.label.select') }}</option>
                    @foreach($constructorDocuments as $constructorDocument)
                        <option title="{{$constructorDocument->current()->document_name}}" value="{{$constructorDocument->id}}">{{$constructorDocument->current()->document_name}}</option>
                    @endforeach
                </select>
            @endif
            <div class="form-error form-error-sub_application_type"></div>
        </td>
        <td class="sub-application-name"></td>
        <td>
            <span class="product-ids-list"></span>
            <input class="product-ids" name="sub_application_products" type="hidden">
            <div class="text-right">
                <button type="button" class="btn btn-primary btn-xs addProductsToSubApplication" title="{{ trans('swis.base.tooltip.add_product_sub_app.button') }}" data-toggle="tooltip"><i class="fa fa-plus"></i></button>
            </div>
            <div class="form-error form-error-sub_application_products"></div>
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td class="text-nowrap ">
            <button type="button" class="btn btn-primary btn-xs storeSupApplication" title="{{ trans('swis.base.tooltip.sub_application_store.button') }}" data-toggle="tooltip"><i class="fa fa-check"></i></button>
            <button title="{{trans('swis.saf.sub_application_manual.delete.button')}}" class="dt-sub-application"  data-toggle="tooltip" type="button"><i class="fas fa-times"></i></button>
        </td>
    </tr>
@else
    <tr>
        <td></td>
        <td><span class="agency_id" title="{{$subApplicationData['agencyName']}}" data-toggle="tooltip">{{$subApplicationData['agencyName']}}</span></td>
        <td>
            <?php
            $agencySubDivisions = $subApplicationData['agencySubDivisions'];
            $refExpressControl = $subApplicationData['refExpressControl'];
            ?>
            @if($agencySubDivisions->count())
                <select class="form-control select2 column_agency_subdivision_id" name="agency_subdivision_id"  @if(count($agencySubDivisions) == 1)  data-select2-allow-clear="false" @endif>
                    @if(count($agencySubDivisions) != 1)
                        <option value="">{{trans('core.base.label.select')}}</option>
                    @endif
                    @foreach($agencySubDivisions as $ref)
                        <option title="{{$ref->name}}" value="{{$ref->id}}">{{!is_null($ref->short_name) ? $ref->short_name : $ref->name}}</option>
                    @endforeach
                </select>
            @endif
            <div class="form-error-agency_subdivision_id form-error-text"></div>
        </td>
        <td>{{$subApplicationData['subApplicationType']}}</td>
        <td><span title="{{$subApplicationData['subApplicationName']}}" data-toggle="tooltip">{{$subApplicationData['subApplicationName']}}</span></td>
        <td>{{$subApplicationData['productNumbers']}}</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td>
            @if($refExpressControl->count())
                <select class="form-control select2 column_express_control_id" name="express_control_id">
                    <option value="">{{trans('core.base.label.select')}}</option>
                    @foreach($refExpressControl as $ref)
                        <option {{$ref->code == '2' ? 'selected' : ''}} value="{{$ref->id}}">{{$ref->name}}</option>
                    @endforeach
                </select>
            @endif
            <div class="form-error-express_control_id form-error-text"></div>
        </td>
        <td class="text-nowrap" >
            <button title="{{trans('swis.saf.sub_application_manual.delete.button')}}" data-id="{{$subApplicationData['id']}}" id="{{$subApplicationData['id']}}" class="btn--icon dt-sub-application"  data-toggle="tooltip" type="button"><i class="fas fa-times"></i></button>
            <button class="btn--icon send-sub-application" data-status="" title="{{trans('swis.saf.sub_application.send.button')}}" data-id="{{$subApplicationData['id']}}" type="button" data-toggle="tooltip"><i class="fas fa-long-arrow-alt-right"></i></button>
            <a href="{{urlWithLng('/application/'.$subApplicationData['id'].'/'.$subApplicationData['pdf_hash_key'])}}" target="_blank" download><button class="btn--icon" type="button"  data-toggle="tooltip"  title="{{ trans('swis.base.tooltip.print.button') }}"><i class="fas fa-print"></i></button></a>
            <a href="{{urlWithLng('/application/'.$subApplicationData['id'].'/'.$subApplicationData['pdf_hash_key'])}}" target="_blank"><button class="btn--icon" type="button" title="{{ trans('swis.base.tooltip.view.button') }}" data-toggle="tooltip"><i class="fas fa-eye"></i></button></a>
        </td>
    </tr>

@endif