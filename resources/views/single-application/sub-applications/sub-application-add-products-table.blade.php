<div class="table-responsive">
<table class="table table-bordered">
    <thead>
        <tr>
            <th style="width: 30px"><input type="checkbox" id="checkboxSubApplicationProducts"></th>
            <th>{{trans('swis.single_app.product_number')}}</th>
            <th>{{trans('swis.single_app.product_code')}}</th>
            <th>{{trans('swis.single_app.commercial_description')}}</th>
        </tr>
    </thead>
    <tbody>
    @foreach($products as $product)
        <?php
            $hsCode = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_HS_CODE_6,$product->product_code);
        ?>
        <tr>
            <td><input data-pr-number="{{$productsIdNumbers[$product->id]}}" class="pr"  value="{{$product->id}}" type="checkbox"></td>
            <td>{{$productsIdNumbers[$product->id]}}</td>
            <td><span data-toggle="tooltip" data-placement="top" title="{{$product->product_description}}">{{$hsCode->code ?? ''}}</span></td>
            <td>{{$product->commercial_description}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>
