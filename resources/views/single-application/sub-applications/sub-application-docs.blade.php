@extends('layouts.app')

@section('title'){{trans('swis.single_application.title')}}@stop

@section('contentTitle')
    {{trans('swis.sub_application_documents.title',['safNumber'=>$safNumber])}}
@stop

@section('content')

    @if(count($safProductsNotFormedSubApplication))
        <div class="alert alert-info" role="alert">
            {{trans('swis.saf.products.not_formed_sub_application.message',['product_codes'=>implode(', ',$safProductsNotFormedSubApplication)])}}
        </div>
        <hr />
    @endif

    <div class="sub-application-docs">
        @if($subApplications->count())
            @foreach($subApplications as $subApplication)

                <h2>&#8470;  {{$subApplication->type}} - {{$subApplication->name}}</h2>
                <table class="table table-bordered table-hover m-b-none">
                    <thead>
                    <tr>
                        <th></th>
                        <th>{{trans('swis.electronic_document.name.title')}}</th>
                        <th>{{trans('swis.electronic_document.type.title')}}</th>
                        <th>{{trans('swis.electronic_document.number.title')}}</th>
                        <th>{{trans('swis.electronic_document.status_date.title')}}</th>
                        <th>{{trans('swis.electronic_document.current_state.title')}}</th>
                        <th>{{trans('swis.electronic_document.current_state_date.title')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="width: 45px">
                            <a href="{{urlWithLng('/application/'.$subApplication->id.'/'.$subApplication->pdf_hash_key)}}" target="_blank">
                                <button class="btn--icon" type="button" title="{{ trans('swis.base.tooltip.view.button') }}" data-toggle="tooltip"><i class="fas fa-eye"></i></button>
                            </a>
                            {{--<a href="{{urlWithLng('/application/'.$subApplication['id'].'/'.$subApplication['pdf_hash_key'])}}"
                               target="_blank" download>
                                <button class="btn--icon" type="button" data-toggle="tooltip"
                                        title="{{ trans('swis.base.tooltip.print.button') }}"><i
                                            class="fas fa-print"></i></button>
                            </a>--}}
                        </td>
                        <td style="width: 550px">{{$subApplication->name}}</td>
                        <td style="width: 75px">{{$subApplication->type}}</td>
                        <td style="width: 198px">{{$subApplication->document_number}}</td>
                        <td style="width: 254px">{{formattedDate($subApplication->status_date)}}</td>
                        <td style="width: 190px">
                            @if($subApplication->auto_released)
                                <strong class="text-danger">{{trans('swis.sub_application.is.auto_released.message')}}</strong>
                            @else
                                <span @if($subApplication->isApproved) style="color: green;font-weight: bold" @endif>{{$subApplication->state}}</span>
                            @endif
                        </td>
                        <td style="width: 220px">{{formattedDate($subApplication->current_state_date)}}</td>
                    </tr>
                    </tbody>
                </table>
                @if($subApplication->subApplicationDocuments()->count())
                    @php $i=1 @endphp
                    <br/>
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th></th>
                                <th>{{trans('swis.document.document_form')}}</th>
                                <th>{{trans('swis.document.type')}}</th>
                                <th>{{trans('core.base.label.name')}}</th>
                                <th>{{trans('core.base.description')}}</th>
                                <th>{{trans('swis.document.number')}}</th>
                                <th>{{trans('swis.document.release_date')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($subApplication->subApplicationDocuments() as $document)
                            <tr>
                                <td style="width: 45px" class="text-center"> {{ $i++ }}</td>
                                <td style="width: 45px">
                                    @if($document->filePath())
                                        <a target="_blank" href="{{$document->filePath()}}" class="btn btn-success btn-xs btn--icon" title="{{ trans('swis.base.tooltip.view.button') }}" data-toggle="tooltip"><i class="fas fa-eye"></i></a>
                                    @endif
                                </td>
                                <td style="width: 270px">{{trans('swis.document.document_form.'.$document->document_form.'.title')}}</td>
                                <td style="width: 235px">{{$document->document_type}}</td>
                                <td style="width: 273px">{{$document->document_name}}</td>
                                <td style="width: 254px">{{$document->document_description}}</td>
                                <td style="width: 190px">{{$document->document_number}}</td>
                                <td style="width: 220px">{{$document->document_release_date}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif

                @if(!$loop->last)
                    <hr/>
                @endif
            @endforeach

        @else
            <h2 class="m-b-none">{{trans('swis.sub_application.submitted.not_exist.message')}}</h2>
        @endif
    </div>

@endsection

@section('scripts')
    <script>
        $('.gray-bg').css("min-height", $(window).height() + "px");
    </script>
@endsection