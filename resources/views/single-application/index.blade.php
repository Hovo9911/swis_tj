@extends('layouts.app')

@section('title') {{trans('swis.single_application.'.$regime.'.title')}} @stop

@section('contentTitle') {{trans('swis.single_application.'.$regime.'.title')}} @stop

@section('form-buttons-top')
    {!! renderAddButton('swis.single_application.add_button') !!}
@stop

@section('topScripts')
    <script>
        var safRegime = '';
        var safStatusTypeCreated = "{{$safCreatedType}}";
        var safStatusTypeVoid = "{{$safVoidType}}";
    </script>
@endsection

@section('content')

    <div class="clearfix search__out">
        <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse" data-target="#collapseSearch" aria-expanded="false" aria-controls="collapseSearch">
            <i class="fa fa-search"> </i><span class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
        </button>
    </div>

    <div class="collapse" id="collapseSearch">
        <div class="card card-body">
            <form class="form-horizontal fill-up" id="search-filter" autocomplete="off">
                <input type="hidden" class="form-control default-search" name="regime" id="safType" value="{{$regime}}">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group searchParam regular_number_block_params" data-field="regular_number_block_params">
                            <label class="col-md-4 control-label label__title">{{trans('swis.single_app.regular_number')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <input type="text" class="form-control" name="regular_number" id="" value="">
                                        <div class="form-error" id="form-error-regular_number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam importer_value_block_params" data-field="importer_value_block_params">
                            <label class="col-md-4 control-label label__title">{{trans('swis.saf.importer.tax_id_passport')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <input type="text" class="form-control" name="importer_value" id="" value="">
                                        <div class="form-error" id="form-error-importer_value"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam importer_name_block_params" data-field="importer_name_block_params">
                            <label class="col-md-4 control-label label__title">{{trans('swis.saf.importer.name')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <input type="text" class="form-control" name="importer_name" id="" value="">
                                        <div class="form-error" id="form-error-importer_name"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam exporter_value_block_params" data-field="exporter_value_block_params">
                            <label class="col-md-4 control-label label__title">{{trans('swis.saf.exporter.tax_id_passport')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <input type="text" class="form-control" name="exporter_value" id="" value="">
                                        <div class="form-error" id="form-error-exporter_value"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam exporter_name_block_params" data-field="exporter_name_block_params">
                            <label class="col-md-4 control-label label__title">{{trans('swis.saf.exporter.name')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <input type="text" class="form-control" name="exporter_name" id="" value="">
                                        <div class="form-error" id="form-error-exporter_name"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam applicant_value_block_params" data-field="applicant_value_block_params">
                            <label class="col-md-4 control-label label__title">{{trans('swis.saf.applicant.tax_id_passport')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <input type="text" class="form-control" name="applicant_value" id="" value="">
                                        <div class="form-error" id="form-error-applicant_value"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam applicant_name_block_params" data-field="applicant_name_block_params">
                            <label class="col-md-4 control-label label__title">{{trans('swis.saf.applicant.name')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <input type="text" class="form-control" name="applicant_name" id="" value="">
                                        <div class="form-error" id="form-error-applicant_name"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if(\Auth::user()->isLocalAdmin())
                            <div class="form-group searchParam sub_application_sender_user_block_params" data-field="sub_application_sender_user_block_params">
                                <label class="col-md-4 control-label label__title pd-t-0">{{trans('swis.saf.sub_application.sender_users.label')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-12 field__one-col">
                                            <input type="text"  class="form-control" name="sub_application_sender_user">
                                            <div class="form-error" id="form-error-sub_application_sender_user"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="form-group searchParam importer_exporter_phone_number_block_params" data-field="importer_exporter_phone_number_block_params">
                            <label class="col-md-4 control-label label__title">{{trans('swis.saf.importer_exporter_phone_number.label')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <input type="text" class="form-control onlyNumbers" name="importer_exporter_phone_number" id="" value="">
                                        <div class="form-error" id="form-error-importer_exporter_phone_number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam importer_exporter_email_block_params" data-field="importer_exporter_email_block_params">
                            <label class="col-md-4 control-label label__title">{{trans('swis.saf.importer_exporter_email.label')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <input type="text" class="form-control" name="importer_exporter_email" id="" value="">
                                        <div class="form-error" id="form-error-importer_exporter_email"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-6">

                        <div class="form-group searchParam status_type_block_params" data-field="status_type_block_params">
                            <label class="col-md-4 control-label label__title">{{trans('swis.saf.status_type')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <select class="form-control select2" name="status_type" id="">
                                            <option value=""></option>
                                            <option value="{{\App\Models\SingleApplication\SingleApplication::CREATED_TYPE}}">{{trans('swis.saf.status_type.created')}}</option>
                                            <option value="{{\App\Models\SingleApplication\SingleApplication::SEND_TYPE}}">{{trans('swis.saf.status_type.send')}}</option>
                                            <option value="{{\App\Models\SingleApplication\SingleApplication::VOID_TYPE}}">{{trans('swis.saf.status_type.void')}}</option>
                                        </select>
                                        <div class="form-error" id="form-error-status_type"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam departure_block_params" data-field="departure_block_params">
                            <label class="col-md-4 control-label label__title">{{trans('swis.single_app.type.departure')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <select name="departure" class="form-control select2">
                                            <option></option>
                                            @foreach($customBodyReference as $value)
                                                <option value="{{$value->id}}">{{'('.$value->code.') '.$value->name . $value->address }}</option>
                                            @endforeach
                                        </select>
                                        <div class="form-error" id="form-error-departure"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam appointment_block_params" data-field="appointment_block_params">
                            <label class="col-md-4 control-label label__title">{{trans('swis.single_app.type.appointment')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <select name="appointment" class="form-control select2">
                                            <option></option>
                                            @foreach($customBodyReference as $value)
                                                <option value="{{$value->id}}">{{'('.$value->code.') '.$value->name . $value->address }}</option>
                                            @endforeach
                                        </select>
                                        <div class="form-error" id="form-error-appointment"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam export_country_block_params" data-field="export_country_block_params">
                            <label class="col-md-4 control-label label__title">{{trans('swis.single_app.type.export_country')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <select name="export_country" class="form-control select2"
                                                data-select2-allow-clear="true">
                                            <option></option>
                                            @foreach($countries as $value)
                                                <option value="{{$value->code}}">{{'('.$value->code.') '.$value->name}}</option>
                                            @endforeach
                                        </select>
                                        <div class="form-error" id="form-error-export_country"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam import_country_block_params" data-field="import_country_block_params">
                            <label class="col-md-4 control-label label__title">{{trans('swis.single_app.type.import_country')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <select name="import_country" class="form-control select2"
                                                data-select2-allow-clear="true">
                                            <option></option>
                                            @foreach($countries as $value)
                                                <option value="{{$value->code}}">{{'('.$value->code.') '.$value->name}}</option>
                                            @endforeach
                                        </select>
                                        <div class="form-error" id="form-error-import_country"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam created_at_block_params" data-field="created_at_block_params">
                            <label class="col-md-4 control-label label__title">{{trans('swis.saf.created_at')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-6 field__two-col">
                                        <div class='input-group date'>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                            <input value="" name="created_at_date_start" autocomplete="off"
                                                   type="text"
                                                   placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                            <div class="form-error" id="form-error-created_at_date_start"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 field__two-col">
                                        <div class='input-group date'>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                            <input value="" name="created_at_date_end" autocomplete="off"
                                                   type="text"
                                                   placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                            <div class="form-error" id="form-error-created_at_date_end"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam constructor_document_block_params" data-field="constructor_document_block_params">
                            <label class="col-md-4 control-label label__title">{{trans('swis.saf.constructor_documents.label')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <select name="constructor_document" class="form-control select2">
                                            <option></option>
                                            @foreach($constructorDocuments as $document)
                                                <option value="{{$document->document_code}}">{{$document->document_name}}</option>
                                            @endforeach
                                        </select>
                                        <div class="form-error" id="form-error-constructor_document"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam submitted_application_block_params" data-field="submitted_application_block_params">
                            <label class="col-md-4 control-label label__title pd-t-0">{{trans('swis.saf.submitted_application.label')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <input type="checkbox" value="1" name="submitted_application">
                                        <div class="form-error" id="form-error-submitted_application"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam approved_sub_applications_block_params" data-field="approved_sub_applications_block_params">
                            <label class="col-md-4 control-label label__title pd-t-0">{{trans('swis.saf.has_request_approved_application.label')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <input type="checkbox" value="1" name="approved_sub_applications">
                                        <div class="form-error" id="form-error-approved_sub_applications"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam requested_sub_applications_block_params" data-field="requested_sub_applications_block_params">
                            <label class="col-md-4 control-label label__title pd-t-0">{{trans('swis.saf.has_request_sub_application.label')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <input type="checkbox" value="1" name="requested_sub_applications">
                                        <div class="form-error" id="form-error-requested_sub_applications"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8 col-lg-offset-3 text-right">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary mr-10">{{trans('core.base.label.search')}}</button>
                                <button type="button" class="btn btn-danger resetButton" data-submit="true">{{trans('core.base.label.reset')}}</button>
                                <button type="button" class="btn btn-default hide-or-show-params" style="margin-top: 10px;">{{trans('core.base.label.apply_hidden_columns')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    @php($companyTaxId = Auth::user()->companyTaxId())
    <div class="data-table">
        <div class="clearfix">
            <table class="table table-striped table-bordered table-hover dataTables-example nowrap" id="list-data" style="width:100%"  data-module-for-search-params="SingleApplication{{ hash('sha256', $companyTaxId) }}">
                <thead>
                <tr>
                    <th data-col="actions" class="th-actions text-nowrap">{{trans('core.base.label.actions')}}</th>
                    <th data-col="regular_number">{{trans('swis.single_app.regular_number')}}</th>
                    <th data-col="status_type">{{trans('swis.saf.status_type')}}</th>
                    <th data-col="created_at">{{trans('swis.saf.created_at')}}</th>
                    <th data-ordering="0" data-col="export_country">{{trans('swis.saf.export_country')}}</th>
                    <th data-ordering="0" data-col="exporter">{{trans('swis.saf.exporter')}}</th>
                    <th data-ordering="0" data-col="import_country">{{trans('swis.saf.import_country')}}</th>
                    <th data-ordering="0" data-col="importer">{{trans('swis.saf.importer')}}</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
    <?php
    $searchColumns = [
        'regular_number' => trans('swis.single_app.regular_number'),
        'status_type' => trans('swis.saf.status_type'),
        'created_at' => trans('swis.saf.created_at'),
        'export_country' => trans('swis.saf.export_country'),
        'exporter' => trans('swis.saf.exporter'),
        'import_country' => trans('swis.saf.import_country'),
        'importer' => trans('swis.saf.importer')
    ];
    ?>
@endsection

@section('scripts')
    <script src="{{asset('js/single-application/main.js')}}"></script>
@endsection