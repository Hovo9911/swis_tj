<div class="modal-dialog modal-lg">

    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal__title fb">{{trans('swis.saf.sub_application.pay_for_sub_application.modal.header')}}</h3>
            <button type="button" class="close modal__close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <span>{{trans('swis.saf.sub_application.pay_for_sub_application.modal.info',['pay_sum'=>$sum])}}</span>
            <input type="hidden" id="subApplicationId" value="{{$subApplicationId}}">
            <input type="hidden" id="expressControlId" value="{{$expressControlId}}">
            <input type="hidden" id="agencySubdivisionId" value="{{$agencySubdivisionId}}">
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="payForSubApplication">{{trans('swis.saf.sub_application.pay_for_sub_application.pay')}}</button>
            <button type="button" class="btn btn-default" class="close modal__close" data-dismiss="modal">{{trans('swis.saf.sub_application.pay_for_sub_application.cancel')}}</button>
        </div>
    </div>

</div>