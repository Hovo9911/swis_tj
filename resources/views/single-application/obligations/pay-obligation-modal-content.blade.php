<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            @if ($pay === true)
                <h3 class="modal__title fb">{{ trans('swis.saf.modal.obligation.pay.title') }}</h3>
            @elseif ($pay == 'refill')
                <h3 class="modal__title fb">{{ trans('swis.saf.modal.obligation.refill.title') }}</h3>
            @endif
            <button type="button" class="close modal__close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            @if ($pay === true)
                @if (!Auth::user()->isAgency() && !Auth::user()->isSwAdmin())
                    <p>{{ trans('swis.saf.modal.obligation.pay.description') }} </p>
                    <p>{{ trans('swis.saf.modal.current_balance') }} {{ Auth::user()->balance() .' '. config('swis.default_currency') }} </p>
                    <p>{{ trans('swis.saf.modal.balance_after_pay') }} {{ number_format(Auth::user()->balance() - $sum, 2, '.', '')  .' '. config('swis.default_currency') }} </p>
                @else
                    <p>{{ trans('swis.saf.modal.obligation.modal_for_agencies', ['sum' => $sum]) }} </p>
                @endif
            @elseif ($pay == 'refill')

                <form action="{{ config('payment.alif.url') }}" method="post" id="form-data-refile">
                    <input type="hidden" name="token" id="token" value="">
                    <input type="hidden" name="key" id="key" value="{{ $params->key }}">
                    <input type="hidden" name="callbackUrl" id="callbackUrl" value="{{ $params->callbackUrl }}">

                    @if (isset($saf))
                        <input type="hidden" name="returnUrl" id="returnUrl" value="{{ urlWithLng("/single-application/edit/{$saf->regime}/{$saf->regular_number}#tab-responsibilities") }}">
                    @else
                        <input type="hidden" name="returnUrl" id="returnUrl" value="{{ urlWithLng("/payment-and-obligations") }}">
                    @endif

                    <input type="hidden" name="orderId" id="orderId" value="">
                    <input type="hidden" name="info" id="info" value="{{ $params->info }}">

                    <p>{{ trans('swis.saf.modal.obligation.pay.terminal.description') }} </p>
                    <p>{{ trans('swis.saf.modal.current_balance') }} {{ Auth::user()->balance() .' '. config('swis.default_currency') }} </p>
                    <p>{{ trans('swis.saf.modal.need.description') }} {{ number_format($sum - Auth::user()->balance(), '2', '.', '') .' '. config('swis.default_currency') }} </p>

                    <hr/>

                    <div class="form-group row">
                        <div class="col-lg-5">
                            <label>{{ trans('swis.saf.modal.obligation.refill.label') }}</label>
                            <input type="number" value="{{ number_format($sum - Auth::user()->balance(), '2', '.', '') }}" class="form-control" name="amount" id="amountField">
                            <div class="form-error" id="form-error-amount"></div>
                        </div>
                    </div>

                </form>
            @endif
        </div>
        <div class="modal-footer">
            @if ($pay === true)
                <button type="button" id="pay-obligation" class="btn btn-primary" data-sum="{{ $sum }}" data-obligations="{{ json_encode($obligationIds) }}">{{ trans('swis.saf.modal.obligation.pay.yes') }}</button>
            @elseif ($pay == 'refill')
                <button class="btnbtn-primary" id="refile">{{ trans('swis.payments.refill.title') }}</button>
            @endif
            <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('swis.saf.modal.obligation.pay.no') }}</button>
        </div>
    </div>
</div>