<?php
$balanceSum = number_format(0, 2, '.', '');
$columns = (array)$block->columns;
$hideColumns = (array)$block->columnsNotShowInView ?? [];
$safObligations = new \App\Models\SingleApplicationObligations\SingleApplicationObligations();
$obligations = $safObligations->getObligationsForPDF($saf);

$gUploader = new \App\GUploader('obligation_receipt');

$obligationReceiptMode = config('swis.obligation_receipt_mode');
?>
@if (count($obligations['obligations']) > 0 )
    <div class="table-responsive">
        <table class="table table-bordered table-hover {{$block->attributes()->tableClass}}" id="safObligations">
            <thead>
            <tr>
                @foreach($columns as $column)
                    @if(in_array($column,$hideColumns) or $column == 'id' || $column == 'subtitle_type')
                        @continue
                    @endif
                    @if(!$obligationReceiptMode && $column == 'receipt')  @continue  @endif
                    <th>{{trans('swis.single_app.'.$column)}}</th>
                @endforeach
            </tr>
            </thead>

            <tbody>
            @foreach($obligations['obligations'] as $obligation)
                <tr>
                    <td align="center"><a href="{{urlWithLng("/payment-and-obligations/generatePdf/{$obligation['id']}/{$obligation['pdf_hash_key']}")}}" target="_blank" ><button class="btn--icon" type="button" data-toggle="tooltip"  title="<?php echo trans('swis.base.tooltip.print.button') ?>"><i class="fas fa-print"></i></button></a></td>
                    @if($obligationReceiptMode)
                        <td>
                            <div class="g-upload-container" data-id="{{$obligation['id']}}">
                                <div class="g-upload-box">
                                    <label style="width: 10px"><i class="fas fa-upload btn btn-success btn-sm"></i>
                                        <input style="display: none" class="dn x-g-upload" type="file" name="document_file" {{$gUploader->isMultiple() ? 'multiple' : ''}} data-conf="obligation_receipt" data-action="{{urlWithLng('/g-uploader/upload')}}"
                                                accept="{{$gUploader->getAcceptTypes()}}"/>
                                    </label>
                                </div>
                                <div class="license-form__uploaded-file-list g-uploaded-list ">
                                    @if($obligation['obligation_receipt_file_name'])
                                    <div class="register-form__clear-box fs14 g-uploaded {{$obligation['obligation_is_payed'] ? 'is-payed-obligation' : ''}}">
                                        <a target="_blank" href="{{$obligation['obligation_receipt_file_path']}}">{{$obligation['obligation_receipt_file_name']}}</a>
                                        @if(!$obligation['obligation_is_payed'])
                                            <button type="button" class="register-form__clear-btn sprite dib fs12 x-g-upload-x"> <i class="fa fa-times"></i> </button>
                                        @endif
                                    </div>
                                    @endif
                                </div>
                                {{-- Progress bar for file uploding process, if you want to show process--}}
                                <progress class="progressBarFileUploader" value="0" max="100"></progress>
                                {{-- Progress bar for file uploding process --}}
                                <div class="form-error-text form-error-files fb fs12"></div>
                            </div>
                        </td>
                    @endif
                    <td>{{ $obligation['subApplication'] }}</td>
                    <td>{{ $obligation['documentId'] }}</td>
                    <td style="width: 200px">{{ $obligation['obligationType'] }}</td>
                    <td style="width: 220px">{{ $obligation['subApplicationName'] }}</td>
                    <td style="width: 100px" class="text-break-word">{{ $obligation['budgetLine'] }}</td>
                    <td>{{ $obligation['paymentDate'] }}</td>
                    <td style="width: 220px;">{{ $obligation['payer'] }}</td>
                    <td style="width: 220px;">{{ $obligation['payer_person'] }}</td>
                    <td>{{ $obligation['obligation_sum'] }}</td>
                    <td>{{ $obligation['payments'] }}</td>
                    <td>{{ $obligation['balance'] }}</td>
                </tr>
            @endforeach

            <tr>
                <td colspan="{{$obligationReceiptMode ? 10 : 9}}" class="text-right">{{ trans('swis.saf.obligations.pay.total') }} </td>
                <td id="totalSum">{{ $obligations['totalSum'] }}</td>
                <td>{{ $obligations['paymentSum'] }}</td>
                <td>{{ number_format($obligations['balanceSum'], 2, '.', '') }}</td>
            </tr>
            </tbody>
        </table>
    </div>

@else
    <h3 class="m-b-xs">{{trans('swis.saf.obligations.not_exist.message')}}</h3>
@endif

