<div id="rutoken-modal" class="modal fade"  role="dialog" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal__title fb"><img src="/image/digital-signature/check-popup-icon.png" class="image_middle"> {{trans('core.base.rutoken.title')}}</h4>
                <button type="button" class="close modal__close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div id="rutokenAlertMessage"></div>
                <div class="form-group row block">
                    <label class="col-lg-3 control-label">{{trans('swis.user_documents.digital_signature.devices_list')}}</label>
                    <div class="col-lg-6">
                        <select name="device-list" id="device-list" class="form-control"></select>
                    </div>
                    <div class="col-lg-3">
                        <button type="button" class="btn btn-primary" id="refresh-dev">{{trans('core.base.button.refresh')}}</button>
                    </div>
                </div>
                <div class="form-group row block">
                    <label class="col-lg-3 control-label">{{trans('swis.user_documents.digital_signature.certificates_list')}}</label>
                    <div class="col-lg-9">
                        <select name="cert-list" id="cert-list" class="form-control"></select>
                    </div>
                </div>

                <div class="form-group row block">
                    <label class="col-lg-3 control-label">{{trans('swis.user_documents.digital_signature.pin')}}</label>
                    <div class="col-lg-9">
                        <input type="text" id="device-pin" class="form-control"/>
                        <div class="form-error"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="block">
                    <input type="hidden" name="moduleName" id="module" value="SAF">
                    <input type="hidden" name="subapplicationId" id="subapplicationId" value="">
                    <button id="buttonMapping" name="buttonMapping" value="" style="display:none"></button>
                    <button type="button" id="login" class="btn btn-primary">{{trans('login.modal.title')}}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('swis.single_app.modal.close')}}</button>
                </div>
                <textarea name="" id="message" cols="30" rows="10" style="display:none"></textarea>
                <input type="hidden" name="digitalSignatureId" id="digitalSignatureId" val="" style="display:none">
                <button id="sign-button" style="display:none">sign</button>
            </div>
        </div>
    </div>
</div>