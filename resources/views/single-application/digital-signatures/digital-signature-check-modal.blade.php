<div class="modal fade" id="rutoken-modal-check" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal__title fb">{{trans('core.base.rutoken-check.title')}}</h4>
                <button type="button" class="close modal__close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <div id="checkRutokenAlertMessage"></div>
                <div id="digitalSignatureInfo"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>