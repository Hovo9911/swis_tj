<?php

$products = \App\Models\SingleApplicationProducts\SingleApplicationProducts::select(['id','product_code','commercial_description','producer_country','total_value','total_value_code','quantity','measurement_unit','brutto_weight','netto_weight'])->where('saf_number', $saf->regular_number)->orderBy('id')->get();
$productsCount = $products->count();
$productNumbers = $saf->productsOrderedList();

$refHsCodeAllData = getReferenceRowsKeyValue(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_HS_CODE_6);
$refCountriesAllData = getReferenceRowsKeyValue(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_COUNTRIES);
$refCurrenciesAllData = getReferenceRowsKeyValue(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_CURRENCIES);
$refUnitOfMeasurementAllData = getReferenceRowsKeyValue(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT);

?>
<div class="table-responsive  {{!$productsCount ? 'hide' : ''}}">
    <table class="table table-bordered table-hover {{$block->attributes()->tableClass}}" data-module="{{$block->attributes()->module}}">
        <thead>
            <tr>
                <th><input type="checkbox" id="checkAllProducts"></th>
                <th style="width: 70px">{{trans('swis.single_app.product_number')}}</th>
                <th style="width: 110px">{{trans('swis.single_app.product_code')}}</th>
                <th>{{trans('swis.single_app.commercial_description')}}</th>
                <th style="width: 400px">{{trans('swis.single_app.producer_country')}}</th>
                <th>{{trans('swis.single_app.total_value')}}</th>
                <th>{{trans('swis.single_app.total_value_code')}}</th>
                <th>{{trans('swis.single_app.quantity')}}</th>
                <th>{{trans('swis.single_app.brutto_weight')}}</th>
                <th>{{trans('swis.single_app.netto_weight')}}</th>
                <th>{{trans('swis.single_app.measurement_unit')}}</th>
                <th></th>
            </tr>
        </thead>
        <tbody>

        @if(count($products) > 0)
            @foreach($products as $key => $product)
                @include('single-application.products.products-table-tr')
            @endforeach
        @endif

        </tbody>
    </table>
</div>

@if(empty($isRender))
<button type="button" class="btn btn-danger m-r {{!$productsCount ?  'hide' : ''}}" id="deleteCheckedProducts"><i class="fa fa-times"></i> {{trans('swis.saf.products.delete.multiple.button')}}</button>
@endif
