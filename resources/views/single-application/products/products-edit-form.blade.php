<div class="modal fade subFormModal" id="{{$block->attributes()->renderTable}}{{(isset($product) ? 'Edit': '')}}" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h3 class="modal__title fb">{{trans('swis.single_app.modal.title')}}</h3>
                <button type="button" class="close modal__close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

                <?php
                $fUrl = urlWithLng('single-application/product/store');
                if (isset($product)) {
                    $fUrl = urlWithLng('single-application/product/update');
                }
                ?>

                @if(empty($tabKey))
                  <form action="{{$fUrl}}" method="post" class="form-horizontal fill-up" data-save-btn="{{$block->attributes()->renderTable.'-btn'}}{{(isset($product) ? '-edit': '')}}" id="sub-form{{isset($product) ? '-edit':""}}">
                @else
                    <div class="form-horizontal">
                @endif

                    @include('single-application.block-fieldset')

                    @if(isset($product))
                        <input type="hidden" name="id" value="{{$product->id or ''}}">
                    @endif

                @if(empty($tabKey))
                    </form>
                @else
                    </div>
                @endif

            </div>

            <div class="modal-footer">
                @if (!isset($onlyView) || $onlyView !== true)
                    <button class="btn btn-primary {{$block->attributes()->renderTable.'-btn'}}{{(isset($product) ? '-edit': '')}}" type="button" data-sub-form="{{$block->attributes()->renderTable}}">{{(isset($product) ? trans('swis.single_app.modal.update') : trans('swis.single_app.modal.save'))}}</button>
                @endif

                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('swis.single_app.modal.close')}}</button>
            </div>

        </div>
    </div>
</div>
