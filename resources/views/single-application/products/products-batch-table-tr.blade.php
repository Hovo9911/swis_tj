<?php
$num = empty($num) ? 1 : $num;
?>

<tr data-id="{{$num}}" data-auto-generated="{{ isset($batch) ? $batch->auto_generated : 0 }}">

    <input type="hidden" name="batch[{{$num}}][auto_generated]"  value="{{  $batch->auto_generated or 0 }}"/>
    <input type="hidden" name="batch[{{$num}}][id]" class="batchesId" value="{{  $batch->id or '' }}">

    @foreach($block->fieldset as $fieldSet)

        <?php

        // Hide fields in SAF
        if (!empty($fieldSet->field->attributes()->hideOnSaf)) {
            continue;
        }

        // Field
        $field = $fieldSet->field;

        // Field attributes
        $fieldName = (string)$field->attributes()->name . '';
        $fieldDataName = (string)$field->attributes()->dataName . '';
        $fieldType = (string)$field->attributes()->type;
        $fieldDefaultClass = (string)$field->attributes()->defaultClass;
        $fieldDefaultId = (string)$field->attributes()->defaultID;
        $fieldStoredValue = '';
        $fieldDefaultValue = (string)$field->attributes()->defaultValue;
        $fieldReadOnly = (string)$field->attributes()->readOnly;

        // Field Name
        $fieldName = str_replace('_index_', $num, $fieldName);

        // If edit a ROW (batch)
        $fieldOptionName = '';
        if (isset($batch)) {
            if (!empty($batch->{$fieldDataName})) {
                $fieldStoredValue = $batch->{$fieldDataName};

                if ($fieldDataName == 'measurement_unit') {
                    $referenceMeasurement = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $fieldStoredValue);
                    if (!is_null($referenceMeasurement)) {
                        $fieldOptionName = '(' . $referenceMeasurement->code . ')' . ' ' . $referenceMeasurement->name;
                    }
                }
            }
        }

        // Field current value , if opened first time will default data then from saf
        $fieldValue = $fieldDefaultValue;
        if ($fieldStoredValue) {
            $fieldValue = $fieldStoredValue;
        }

        if($fieldDataName == 'quantity'){
            $fieldValue = str_replace(',','.',$fieldValue);
        }

        if(isset($batch) && $batch->auto_generated){
            $fieldValue = trans($fieldValue);
        }

        ?>

        <td>
            <div class="form-group">
            @switch($fieldType)

                {{-- FIELD TYPE IS -> SELECT --}}
                @case('select')

                <select {{$fieldReadOnly}} class="form-control {{$fieldDefaultClass}}" name="{{$fieldName}}">
                    <option value="{{$fieldStoredValue}}" selected>{{$fieldOptionName}}</option>
                </select>

                @break

                {{-- FIELD TYPE IS -> INPUT --}}
                @case('input')

                {{-- Input type (text,number,date) --}}
                <?php $inputType = (string)$field->attributes()->inputType;?>

                    @if($inputType == 'date')

                        <div class='input-group date'>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            <input {{$fieldReadOnly}} value="{{formattedDate($fieldValue)}}" name="{{$fieldName}}" autocomplete="off" type="text"   id="{{$fieldDefaultId}}" placeholder="{{setDatePlaceholder()}}" class="form-control {{$fieldDefaultClass}}"/>
                        </div>

                    @else

                    <input {{$fieldReadOnly}}value="{{$fieldValue}}" type="{{($inputType) ? $inputType : 'text'}}" name="{{$fieldName}}" id="{{$fieldDefaultId}}" class="form-control {{$fieldDefaultClass}}" autocomplete="nope">

                    @endif
                @break

                {{-- FIELD TYPE IS -> AUTOINCREMENT --}}
                @case('autoincrement')
                <span class="incrementNumber">{{empty($incrementNumber) ? $num : $incrementNumber}}</span>
                @break

                {{-- FIELD TYPE IS -> ACTIONS --}}
                @case('actions')
                <button type="button" data-id="{{isset($batch) ? $batch->id : ''}}" class="s-table-delete"><i class="fas fa-times"></i></button>
                @break

            @endswitch

            <?php
                $fieldName = str_replace(']', '', $fieldName);
                $fieldName = str_replace('[', '-', $fieldName);
            ?>
            <div class="form-error" id="form-error-{{$fieldName}}"></div></div>
        </td>

    @endforeach
</tr>
