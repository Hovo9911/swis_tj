<?php
$hsCode = $refHsCodeAllData[$product->product_code] ?? [];
$producerCountry = $refCountriesAllData[$product->producer_country] ?? [];
$currency = $refCurrenciesAllData[$product->total_value_code] ?? [];
$measurementUnit = $refUnitOfMeasurementAllData[$product->measurement_unit] ?? [];

$isDisabled = !empty($subAppDisabledProducts) && in_array($product->id,$subAppDisabledProducts) ? true : false;
?>


<tr @if($isDisabled) class="disabled" @endif>

    <td>@if(!$isDisabled)<input type="checkbox" value="{{$product->id}}"> @endif</td>
    <td class="text-center">{{isset($productNumbers[$product->id]) ? $productNumbers[$product->id] : ''}}</td>
    <td><span data-toggle="tooltip" title="{{$hsCode['path_name'] ?? ''}}">{{$hsCode['code'] ?? ''}}</span></td>
    <td><span data-toggle="tooltip" title="{{$product->commercial_description}}">{{cutString($product->commercial_description,30)}}</span></td>
    <td><span data-toggle="tooltip" title="{{$producerCountry['code'] ?? ''}}">{{$producerCountry['name'] ?? ''}}</span></td>
    <td>{{$product->total_value}}</td>
    <td><span data-toggle="tooltip" title="{{$currency['code'] ?? ''}}">{{$currency['name'] ?? ''}}</span></td>
    <td><span data-toggle="tooltip" title="{{$product->quantity}}">{{$product->quantity}}</span></td>
    <td><span data-toggle="tooltip" title="{{$product->brutto_weight}}">{{$product->brutto_weight}}</span></td>
    <td><span data-toggle="tooltip" title="{{$product->netto_weight}}">{{$product->netto_weight}}</span></td>
    <td><span data-toggle="tooltip" title="({{$measurementUnit['code'] ?? ''}}) {{$measurementUnit['name'] ?? ''}}">{{$measurementUnit['name'] ?? ''}}</span></td>

    <td style="min-width: 130px;text-align: center">
        @if($saveMode != 'view')

            @if(!$isDisabled)
                <button type="button" title="{{trans('swis.base.tooltip.edit.button')}}" class="btn-edit btn s-table-edit" data-id="{{$product->id or 0}}" data-toggle="tooltip"><i class="fa fa-pen"></i></button>
            @else
                <button type="button" title="{{trans('swis.base.tooltip.view.button')}}" class="btn-edit btn s-table-edit" data-id="{{$product->id or 0}}" data-toggle="tooltip"><i class="fa fa-eye"></i></button>
            @endif

            <button type="button" title="{{trans('swis.base.tooltip.clone.button')}}" class="btn-clone btn s-table-clone" data-id="{{$product->id or 0}}" data-toggle="tooltip"><i class="fas fa-copy"></i></button>

            @if(!$isDisabled)
                <button type="button" title="{{trans('core.base.button.delete')}}" class="btn-remove s-table-delete" data-id="{{$product->id or 0}}" data-toggle="tooltip"><i class="fa fa-times"></i></button>
            @endif
        @else
            <button type="button" title="{{trans('swis.base.tooltip.view.button')}}" class="btn-edit btn s-table-edit" data-id="{{$product->id or 0}}" data-toggle="tooltip"><i class="fa fa-eye"></i></button>
        @endif
    </td>
</tr>