@if(!empty($block->subBlock))
    @php
        $batches = collect();
        if (!empty($block->subBlock->attributes()->DbTableName) && isset($edit)) {
            $batches = \App\Models\SingleApplicationProductsBatch\SingleApplicationProductsBatch::select((array)$block->subBlock->columns)->where('saf_product_id',$product->id)->orderBy('id')->get();

            $simpleAppTabs = \App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure::lastXml();
            $subBlock = $simpleAppTabs->xpath('/tabs/tab[@key="' . $block->subBlock->attributes()->parentModule . '"]/block/subBlock[@renderTr]');
        }
    @endphp

    <?php

    $refDataColumns = $block->subBlock->columnsRefData ?? [];

    $refDataColumnsArr = [];
    if (!empty($refDataColumns)) {
        foreach ($refDataColumns as $refDataColumn) {
            $refAttr = $refDataColumn->attributes();
            $fromRef = (string)$refAttr->from;
            $toColumn = (string)$refAttr->to;
            $refDataColumnsArr[$toColumn] = $fromRef;
        }
    }

    ?>

    <h3 class="fb batches--main-title ">{{ trans('swis.products.more_info.title') }}</h3>

    <hr/>
    <div class="batches--block {{!$batches->count() ? 'hide' : ''}}">
        <h4 class="fb text--batches">{{ trans('swis.single_app.batches') }}</h4>
        <div>
            <div class="collapse in" id="goodsProductBatch{{(isset($product) ? 'Edit': '')}}">
                @foreach($block->subBlock as $block)
                    <div class="table-responsive">
                        <table class="table table-bordered m-b-none {{$block->attributes()->tableClass}}" data-module="{{(empty($block->attributes()->tableClass)) ? $block->attributes()->module : $block->attributes()->tableClass}}">
                            <thead>
                            <tr>
                                @foreach($block->fieldset as $fieldset)
                                    <?php
                                    // Hide fields in SAF
                                    if (!empty($fieldset->field->attributes()->hideOnSaf)) {
                                        continue;
                                    }
                                    ?>

                                    <td>{{trans($fieldset->field->attributes()->title)}}</td>
                                @endforeach
                            </tr>
                            </thead>

                            <tbody>
                            @if(empty($block->attributes()->draw) || $block->attributes()->draw != 'false')
                                @include('single-application.products.products-batch-table-tr.',['block'=>$block])
                            @endif

                            @if($batches->count())
                                @foreach($batches as $key=>$batch)
                                    @include('single-application.products.products-batch-table-tr',['batch'=>$batch,'block'=>$subBlock[0],'num'=>$key+1])
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    @if (empty($onlyView) || $onlyView !== true)
        <button type="button" class="btn btn-primary {{(!empty($block->attributes()->subForm)) ? 'subForm' : ''}} {{(!empty($block->attributes()->renderTr)) ? 'renderTr' : ''}}"
                data-table="{{$block->attributes()->tableClass}}"> <i class="fa fa-plus"></i><span class="documents-plusBtn__text"> {{trans('swis.saf.products.modal.add_product_batch_button')}}</span>
        </button>
    @endif

@endif