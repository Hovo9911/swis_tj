@extends('layouts.app')

@section('title'){{trans('swis.constructor_routing.title')}}@stop

@section('contentTitle') {{trans('swis.constructor_routing.title')}} @stop

@section('form-buttons-top')
    {!! renderAddButton() !!}
@stop

<?php

$jsTrans->addTrans([
    'swis.import_title',
    'swis.export_title',
    'swis.transit_title',
]);

?>

@section('content')

    <div class="clearfix search__out">
        <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse"
                data-target="#collapseSearch" aria-expanded="false" aria-controls="collapseSearch">
            <i class="fa fa-search"> </i><span
                    class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
        </button>
    </div>

    <div class="collapse" id="collapseSearch">
        <div class="card card-body">
            <form class="form-horizontal fill-up" id="search-filter" autocomplete="off">
                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">ID</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="number" class="form-control onlyNumbers" min="0" name="id" id="" value="">
                                <div class="form-error" id="form-error-id"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.name')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="name" id="" value="">
                                <div class="form-error" id="form-error-name"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.routing.rule.label')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <textarea rows="3" name="rule" class="form-control"></textarea>
                                <div class="form-error" id="form-error-rule"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group" id="agency-select-list">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.routing.agency.label')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            @if(count($agencies) > 0)
                                <div class="col-lg-10 col-md-8  field__one-col">
                                    <select class="form-control select2" id="agency"
                                            name="agency_id">
                                        <option value=""></option>
                                        @foreach($agencies as $agency)
                                            <option value="{{$agency->id}}">{{$agency->name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="form-error" id="form-error-agency_id"></div>
                                </div>
                            @else
                                <i class="no-more">{{trans('swis.agency.no_more_agencies.message')}}</i>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.routing.constructor_document.label')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8  field__one-col">
                                <select class="form-control select2" id="constructorDocuments"
                                        name="constructor_document_id">
                                    <option></option>
                                </select>
                                <div class="form-error" id="form-error-constructor_document_id"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.routing.reference_classificator.label')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8  field__one-col">
                                <select name="ref_classificator_codes" multiple
                                        data-select2-allow-clear="false" class="select2">
                                    <option></option>
                                    @foreach($classificatorDocuments as $document)
                                        <option value="{{$document->code}}">({{$document->code}}) {{$document->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group regime-list">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.routing.regime.label')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8  field__one-col">
                                @foreach (App\Models\SingleApplication\SingleApplication::REGIMES as $key=>$regime)
                                    <label>
                                        <input type="radio" name="regime"
                                               value="{{ $regime }}" {{ (isset($routing) && $routing->regime == $regime) ? 'checked' : '' }}/> {{ trans('swis.'.$regime.'_title') }}
                                    </label>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.show_status')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <select class="form-control select2 default-search" name="show_status">
                                    <option value="{{\App\Models\BaseModel::STATUS_ACTIVE}}">{{trans('core.base.label.status.active')}}</option>
                                    <option value="{{\App\Models\BaseModel::STATUS_INACTIVE}}">{{trans('core.base.label.status.inactive')}}</option>
                                </select>
                                <div class="form-error" id="form-error-show_status"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8 col-lg-offset-3 text-right">
                        <div class="row">
                            <div class="col-lg-10 col-md-8">
                                <button type="submit"
                                        class="btn btn-primary  mr-10">{{trans('core.base.label.search')}}</button>
                                <button type="button" class="btn btn-danger resetButton"
                                        data-submit="true">{{trans('core.base.label.reset')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="data-table">
        <div class="clearfix">
            <table class="table table-striped table-bordered table-hover dataTables-example" id="list-data">
                <thead>
                <tr>
                    <th data-col="actions" data-delete="0" class="th-actions">{{trans('core.base.label.actions')}}</th>
                    {{--<th data-col="check" class="th-checkbox"><input type="checkbox"></th>--}}
                    <th data-col="id">ID</th>
                    <th data-col="name">{{trans('swis.ref_table_form.alamezon.sname.label')}}</th>
                    <th data-col="regime">{{trans('swis.saf.regime')}}</th>
                    <th data-col="show_status">{{trans('core.base.label.status')}}</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/routing/main.js')}}"></script>
@endsection