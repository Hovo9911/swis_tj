@extends('layouts.app')

@section('title'){{trans('swis.constructor_routing.title')}}@stop

@section('contentTitle')

    @switch($saveMode)
        @case('add')
        {{trans('swis.constructor_routing.create.title')}}
        @break
        @case('edit')
        {{trans('swis.constructor_routing.title')}}
        @break

    @endswitch

@stop

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('content')
    <?php

    switch ($saveMode) {
        case 'add':
            $url = 'routing';
            break;
        default:
            $url = $saveMode != 'view' ? 'routing/update/' . $routing->id: '';
            break;
    }

    ?>

    <div class="tabs-container">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tab-rule"> {{trans('swis.routing.tab.rules')}}</a></li>
            <li><a data-toggle="tab" href="#tab-saf-fields"> {{trans('swis.routing.tab.saf_fields')}}</a></li>
        </ul>
        <form class="form-horizontal fill-up " id="form-data" action="{{urlWithLng($url)}}" autocomplete="off">
            <div class="tab-content">
                <div id="tab-rule" class="tab-pane active">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-10">

                                <div class="form-group"><label
                                            class="col-md-4 col-lg-3 control-label pd-t-0 label__title">{{trans('swis.constructor_routing.is_manual.label')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <input value="1"
                                                       {{isset($routing) && $routing->is_manual ? 'checked' : ''}}
                                                       name="is_manual" class="is-manual"
                                                       type="checkbox" placeholder="">
                                                <div class="form-error" id="form-error-is_manual"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group required"><label
                                            class="col-lg-3 control-label">{{trans('core.base.label.name')}}</label>
                                    <div class="col-lg-9">
                                        <input value="{{$routing->name or ''}}" name="name" type="text" placeholder=""
                                               class="form-control">
                                        <div class="form-error" id="form-error-name"></div>
                                    </div>
                                </div>

                                <div class="form-group {{isset($routing) ? $routing->is_manual ? '' : 'required' : 'required'}}">
                                    <label
                                            class="col-lg-3 control-label">{{trans('swis.routing.rule')}}</label>
                                    <div class="col-lg-9">
                                        <textarea name="rule" rows="10" placeholder=""
                                                  {{isset($routing) && $routing->is_manual ? 'disabled' : ''}}
                                                  class="form-control rule">{{$routing->rule or ''}}</textarea>
                                        <div class="form-error" id="form-error-rule"></div>
                                    </div>
                                </div>

                                <div class="form-group required" id="agency-select-list">
                                    <label class="col-lg-3 control-label">{{trans('swis.agency')}}</label>
                                    @if(count($agencies) > 0)
                                        <div class="col-lg-9">
                                            @if(count($agencies) == 1 && !\Auth::user()->isSwAdmin())
                                                <input value="{{$agencies[0]->name}}" disabled class="form-control">
                                            @else
                                                <select class="form-control select2" id="agency" name="agency_id">
                                                    <option value=""></option>
                                                    @foreach($agencies as $agency)
                                                        <option {{(!empty($routing) && $agency->id == $routing->agency_id) ? 'selected' : ''}} value="{{$agency->id}}">{{$agency->name}}</option>
                                                    @endforeach
                                                </select>
                                            @endif
                                            <div class="form-error" id="form-error-agency_id"></div>
                                        </div>
                                    @else
                                        <i class="no-more">{{trans('swis.agency.no_more_agencies.message')}}</i>
                                    @endif
                                </div>

                                <div class="form-group required">
                                    <label class="col-lg-3 control-label">{{trans('swis.constructor_document.title')}}</label>
                                    <div class="col-lg-9">
                                        <select class="form-control select2" id="constructorDocuments"
                                                name="constructor_document_id">
                                            <option></option>
                                            @if (isset($routing))
                                                <option value="{{ $routing->constructor_document_id }}"
                                                        selected>({{$routing->document_code}}) {{ $routing->document_name }}</option>
                                            @endif
                                        </select>
                                        <div class="form-error" id="form-error-constructor_document_id"></div>
                                    </div>
                                </div>

                                <div class="clearfix">
                                    <label class="control-label col-lg-3">{{trans('swis.constructor_routing.status.reference_classificator.label')}}</label>
                                    <div class="col-lg-9">

                                        <div class="ref-classificators" data-name="ref_classificator_codes">
                                            @if(isset($routing) && !is_null($routing->ref_classificator_codes))
                                                @foreach($routing->ref_classificator_codes as $key=>$refCodes)
                                                    <div class="form-group">
                                                        <select name="ref_classificator_codes[{{$key}}][]" multiple
                                                                data-select2-allow-clear="false" class="select2 m-b">
                                                            <option></option>
                                                            @foreach($classificatorDocuments as $document)
                                                                <option {{is_array($refCodes) && in_array($document->code,$refCodes,true) ? 'selected' : ''}} value="{{$document->code}}">({{$document->code}}) {{$document->name}}</option>
                                                            @endforeach
                                                        </select>

                                                            <button type="button" class="btn btn-danger {{!$loop->index ? 'hide' : ''}} btn-remove-classificator"><i class="fa fa-minus"></i></button>
                                                    </div>

                                                @endforeach

                                            @else
                                                <div class="form-group">
                                                    <select name="ref_classificator_codes[1][]" multiple
                                                            data-select2-allow-clear="false" class="select2 m-b">
                                                        <option></option>
                                                        @foreach($classificatorDocuments as $document)
                                                            <option {{!empty($routing->ref_classificator_codes) && in_array($document->code,$routing->ref_classificator_codes,true) ? 'selected' : ''}} value="{{$document->code}}">({{$document->code}}) {{$document->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    <button type="button" class="btn btn-danger hide btn-remove-classificator"><i class="fa fa-minus"></i></button>

                                                </div>
                                            @endif

                                        </div>

                                        <div class="text-right m-t">
                                            <div class="form-group">
                                                <button type="button" class="btn btn-primary multiple-ref-classificator"><i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>
                                        <hr />
                                    </div>
                                </div>

                                <div class="form-group required regime-list">
                                    <label class="col-lg-3 control-label">{{trans('swis.constructor_routing.regime')}}</label>
                                    <div class="col-lg-9">
                                        @foreach (\App\Models\SingleApplication\SingleApplication::REGIMES as $key=>$regime)
                                            <label>
                                                <input type="radio" name="regime"
                                                       value="{{ $regime }}" {{ (isset($routing) && $routing->regime == $regime) ? 'checked' : '' }}/> {{ trans('swis.'.$regime.'_title') }}
                                            </label>
                                        @endforeach
                                        <div class="form-error" id="form-error-regime"></div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-lg-3">{{trans('core.base.label.status')}}</label>
                                    <div class="col-lg-9 general-status">
                                        <select name="show_status" class="form-show-status">
                                            <option value="{{App\Models\Routing\Routing::STATUS_ACTIVE}}"
                                                    {{(!empty($routing) && $routing->show_status ==
                                                App\Models\Routing\Routing::STATUS_ACTIVE) || $saveMode == 'add' ? '
                                                selected="selected"' :
                                                ''}}>{{trans('core.base.label.status.active')}}
                                            </option>
                                            <option value="{{App\Models\Routing\Routing::STATUS_INACTIVE}}"
                                                    {{!empty($routing) && $routing->show_status ==
                                                App\Models\Routing\Routing::STATUS_INACTIVE ? ' selected="selected"':
                                                ''}}>{{trans('core.base.label.status.inactive')}}
                                            </option>
                                        </select>
                                        <div class="form-error" id="form-error-show_status"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="tab-saf-fields" class="tab-pane ">
                    <div class="panel-body p-lr-n">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered table-hover dataTables-example"
                                       id="list-saf-data">
                                    <thead>
                                    <tr>
                                        <th>{{ trans('swis.routing.saf_field.id') }} </th>
                                        <th>{{ trans('swis.routing.saf_field.mdm_id') }} </th>
                                        <th>{{ trans('swis.routing.saf_field.name') }} </th>
                                        <th>{{ trans('swis.routing.saf_field.path') }} </th>
                                        <th>{{ trans('swis.routing.saf_field.make_visible') }} </th>
                                        <th>{{ trans('swis.routing.saf_field.make_mandatory') }} </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $safControlledFields = $routing->saf_controlled_fields ?? null;
                                    ?>
                                    @foreach($safFields as $key => $field)
                                        <?php
                                        $isHidden = $field->hideOnSaf ? true  : false;
                                        $safFieldId = $field->id;
                                        ?>
                                        <tr>
                                            <td>{{$safFieldId}}</td>
                                            <td>{{$field->mdm}}</td>
                                            <td>{{trans($field->title)}}</td>
                                            <td>{{\App\Models\Routing\Routing::getSafXmlPathByFieldId($safFieldId)}}</td>
                                            <td>
                                                <div class="form-group m-n">
                                                    <input value="1" {{(isset($safControlledFields[$safFieldId]) && isset($safControlledFields[$safFieldId]['make_visible']) && $safControlledFields[$safFieldId]['make_visible']) ? 'checked' : ''}} {{!$isHidden ? 'checked disabled' : ''}} type="checkbox" name="saf_controlled_fields[{{$safFieldId}}][make_visible]">
                                                    <div class="form-error" id="form-error-saf_controlled_fields-{{$safFieldId}}-make_visible"></div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group m-n">
                                                    <input value="1" type="checkbox" {{(isset($safControlledFields[$safFieldId]) && isset($safControlledFields[$safFieldId]['make_mandatory']) && $safControlledFields[$safFieldId]['make_mandatory']) ? 'checked' : ''}} name="saf_controlled_fields[{{$safFieldId}}][make_mandatory]">
                                                    <div class="form-error" id="form-error-saf_controlled_fields-{{$safFieldId}}-make_mandatory"></div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </form>
    </div>

@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script src="{{asset('js/routing/main.js')}}"></script>
@endsection