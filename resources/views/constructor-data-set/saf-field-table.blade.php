<?php
$searchParameterDefaultFields = \App\Models\ConstructorDataSet\ConstructorDataSet::DEFAULT_SEARCH_PARAMETER_FIELDS;
?>
<div class="table-responsive">
<table class="table table-striped table-bordered table-hover dataTables-example" id="list-saf-data">
    <thead>
    <tr>
        <th data-col="id">{{ trans('swis.constructor_data_set.saf_field_id') }} </th>
        <th data-col="mdm_id">{{ trans('swis.constructor_data_set.mdm_id') }} </th>
        <th data-col="saf_name">{{ trans('swis.constructor_data_set.saf_name') }}</th>
        <th data-col="saf_description"
            class="data_set_description">{{ trans('swis.constructor_data_set.saf_description') }}</th>
        <th data-col="saf_tab">{{ trans('swis.constructor_data_set.saf_tab') }}</th>
        <th>{{ trans('swis.constructor_data_set_saf.field_label') }}</th>
        <th>{{ trans('swis.constructor_data_set_saf.tool_tip') }}</th>
        <th data-col="saf_is_attribute">{{ trans('swis.constructor_data_set.is_attribute') }}</th>
        <th data-col="saf_hide_on_system">{{ trans('swis.constructor_data_set.hide_on_system') }}</th>
        <th data-col="saf_search_param">{{ trans('swis.constructor_data_set.search_param') }}</th>
        <th data-col="saf_include_search_result">{{ trans('swis.constructor_data_set.include_search_result') }}</th>
        {{--<th data-col="saf_is_unique">{{ trans('swis.constructor_data_set.is_unique') }}</th>--}}
    </tr>
    </thead>
    <tbody>
    @forelse($safFields as $fieldIdD => $field)

        <tr>
            <td>{{ $fieldIdD }}</td>
            <td>{{ $field['mdm_id'] }}</td>
            <td>
                {{ $field['name'] }}
                <input type="hidden" name="params[{{$fieldIdD}}][xml_field_name]" value="{{$field['xml_field_name']}}">
                <input type="hidden" name="params[{{$fieldIdD}}][title_trans_key]"
                       value="{{$field['title_trans_key']}}">
            </td>
            <td>{{ $field['description'] }}</td>
            <td>
                {{ $field['tab'] }}
                <input type="hidden" name="params[{{$fieldIdD}}][group]" value="{{$field['path']}}">
            </td>
            <td class="w-200">
                <div class="form-group">
                    @foreach (activeLanguages() as $lang)
                        {{ $lang->name }}<br><input type='text'
                                                    class="form-control"
                                                    name='params[{{$fieldIdD}}][field_label][{{$lang->id}}]'
                                                    value="{{(isset($previousDataForDocuments['saf'][$fieldIdD]) && isset($previousDataForDocuments['saf'][$fieldIdD]['field_label'])) ? $previousDataForDocuments['saf'][$fieldIdD]['field_label'][$lang->id] : '' }}"/>
                        <div class='form-error'></div><br/>
                    @endforeach
                </div>
            </td>
            <td class="w-200">
                <div class="form-group">
                    @foreach (activeLanguages() as $lang)
                        {{ $lang->name }}<br><input type='text' name='params[{{$fieldIdD}}][tooltip][{{$lang->id}}]' class="form-control"
                                                    value="{{(isset($previousDataForDocuments['saf'][$fieldIdD]) && isset($previousDataForDocuments['saf'][$fieldIdD]['tooltip'])) ? $previousDataForDocuments['saf'][$fieldIdD]['tooltip'][$lang->id] : '' }}"/>
                        <div class='form-error'></div><br/>
                    @endforeach
                </div>
            </td>
            <td align="center">
                <input title="{{ trans('swis.constructor_data_set.is_attribute') }}" type="checkbox"
                       {{ (isset($previousDataForDocuments['saf'][$fieldIdD]) && $previousDataForDocuments['saf'][$fieldIdD]['is_attribute']) ? 'checked' : '' }} name="params[{{$fieldIdD}}][is_attribute]" {{ ($field['show_attr']) ? '' : 'disabled' }}>
            </td>
            <td align="center">
                <input title="{{ trans('swis.constructor_data_set.hide_on_system') }}" type="checkbox"
                       {{ (isset($previousDataForDocuments['saf'][$fieldIdD]) && $previousDataForDocuments['saf'][$fieldIdD]['is_hidden']) ? 'checked' : '' }} name="params[{{$fieldIdD}}][hide_on_system]">
            </td>
            <td align="center">
                <input title="{{ trans('swis.constructor_data_set.search_param') }}" type="checkbox"
                       @if(in_array($fieldIdD,$searchParameterDefaultFields)) disabled checked @endif
                       {{ (isset($previousDataForDocuments['saf'][$fieldIdD]) && $previousDataForDocuments['saf'][$fieldIdD]['is_search_param']) ? 'checked' : '' }} name="params[{{$fieldIdD}}][search_param]">
            </td>
            <td align="center">
                <input title="{{ trans('swis.constructor_data_set.include_search_result') }}"
                       type="checkbox"
                       {{ (isset($previousDataForDocuments['saf'][$fieldIdD]) && $previousDataForDocuments['saf'][$fieldIdD]['show_in_search_results']) ? 'checked' : '' }} name="params[{{$fieldIdD}}][include_search_result]">
            </td>
        </tr>
    @empty
        <tr>
            <td colspan="5" align="center">No data available yet</td>
        </tr>
    @endforelse
    </tbody>
</table>
</div>