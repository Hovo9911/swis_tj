<tr id='tr-{{$incrementedCode}}'>
    <td>{{$incrementedCode}}</td>
    <td>
        <div class="form-group">
            <input class="form-control" type="number" name="paramsMDM[{{$incrementedCode}}][order]" value="0">
        </div>
    </td>
    <td>{{$mdmField->id}}<input type='hidden' value='{{$mdmField->id}}'
                                name='paramsMDM[{{$incrementedCode}}][mdm_field_id]'></td>
    <td>{{$mdmField->name}}}</td>
    <td>{{$mdmField->description}}</td>
    <td>
        <div class='form-group'>
            <select class='select2' id='select2Box-{{$incrementedCode}}' name='paramsMDM[{{$incrementedCode}}][group]'>
                <option value=''>Select Group</option>
                @foreach ($groupPath['groupPath'] as $savePath => $path)
                    <option value='{{$savePath}}'>{{$path}}</option>
                @endforeach
            </select>
        </div>
        <div class='form-error' id='form-error-paramsMDM-{{$incrementedCode}}-group'></div>
    </td>
    <td>
        <div class='form-group'>
            @foreach (activeLanguages() as $lang)
                {{$lang->name}} <input class="form-control" type='text' name='paramsMDM[{{$incrementedCode}}][field_label][{{$lang->id}}]'/>
                <div class='form-error' id='form-error-paramsMDM-{{$incrementedCode}}-field_label-{{$lang->id}}'></div>
                <br/>
            @endforeach
        </div>
    </td>
    <td>
        <div class='form-group'>
            @foreach (activeLanguages() as $lang)
                {{$lang->name}} <input class="form-control" type='text' name='paramsMDM[{{$incrementedCode}}][tooltip][{{$lang->id}}]'/>
                <div class='form-error' id='form-error-paramsMDM-{{$incrementedCode}}-tooltip-{{$lang->id}}'></div>
                <br/>
            @endforeach
        </div>
    </td>
    <td align='center'>
        <input title='{{$viewData['titleAttr']}}' type='checkbox'
               {{ (!is_null($mdmField->reference)) ? '' : 'disabled' }} name='paramsMDM[{{$incrementedCode}}][is_attribute]' {{$viewData['disabled']}}>
    </td>
    <td align='center'><input title='{{$viewData['titleHide']}}' type='checkbox'
                              name='paramsMDM[{{$incrementedCode}}][is_hidden]'></td>
    <td align='center'><input title='{{$viewData['titleParam']}}' type='checkbox'
                              name='paramsMDM[{{$incrementedCode}}][is_search_param]'></td>
    <td align='center'><input title='{{$viewData['titleIncludeSearch']}}' type='checkbox'
                              name='paramsMDM[{{$incrementedCode}}][show_in_search_results]'></td>
    <td align='center'><input title='{{$viewData['titleIsUnique']}}' type='checkbox'
                              name='paramsMDM[{{$incrementedCode}}][is_unique]'></td>
    <td align='center'><input title='{{$viewData['titleActive']}}' type='checkbox'
                              name='paramsMDM[{{$incrementedCode}}][is_active]'></td>
    <td align='center'>
        <button class='btn btn-danger btn-circle remove-btn' data-id='{{$incrementedCode}}' type='button'
                title='{{ $viewData['titleDelete'] }}'><i class='fa fa-trash'></i></button>
    </td>
</tr>