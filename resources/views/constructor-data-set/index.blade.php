@extends('layouts.app')

@section('title'){{trans('swis.constructor_data-set.title')}}@stop

@section('contentTitle') {{trans('swis.constructor_data-set.title')}} @stop

<?php
$jsTrans->addTrans([
    'core.want_to_leave',
]);
?>

@section('form-buttons-top')
    @if ($selectedConstructorDocumentId)
        {!! renderEditButton() !!}
    @endif
@stop

@section('content')
    <form class="form-horizontal fill-up" id="search-filter" action="{{ urlWithLng('constructor-data-set') }}">
        <div class="form-group">
            <label class="col-md-4 col-lg-3 control-label">{{trans('swis.document.select_document')}}</label>
            <div class="col-md-6 col-lg-6">
                <select class="form-control select2 document-list auto-search-filter default-search" name="documentID">
                    <option value="">{{ trans('swis.document.select_document') }}</option>
                    @if ($constructorDocuments->count())
                        @foreach ($constructorDocuments as $constructorDocument)
                            <option value="{{ $constructorDocument->id }}" {{ ( $selectedConstructorDocumentId == $constructorDocument->id) ? 'selected' : '' }}>{{ $constructorDocument->document_name }}</option>
                        @endforeach
                    @endif
                </select>
                <div class="form-error" id="form-error-email"></div>
            </div>
        </div>
    </form>

    @if ($selectedConstructorDocumentId)
        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab"
                                      href="#tab-from-saf"> {{trans('swis.constructor_data-set.data_from_saf')}}</a>
                </li>
                <li class=""><a data-toggle="tab"
                                href="#tab-from-mdm"> {{trans('swis.constructor_data-set.data_from_mdm')}}</a></li>
            </ul>
            <form action="{{ urlWithLng('/constructor-data-set') }}" method="post" id="saf-form">
                {{ csrf_field() }}
                <input type="hidden" name="documentID" value="{{ $selectedConstructorDocumentId }}">
                <input type="hidden" name="lastIncrementID" id="lastIncrementID">
                <div class="tab-content">

                    {{------------------- SAF -------------------}}
                    <div id="tab-from-saf" class="tab-pane active">
                        <div class="panel-body">
                            @include('constructor-data-set.saf-field-table')
                        </div>
                    </div>

                    {{------------------- MDM -------------------}}
                    <div id="tab-from-mdm" class="tab-pane">
                        <div class="panel-body">
                            <div class="form-group"><label
                                        class="col-md-4 col-lg-3 control-label text-right">{{trans('swis.data_model.title')}}</label>
                                <div class="col-md-6 col-lg-6">
                                    <select class="select2-ajax" id="mdmFiledSearch"
                                            data-url="{{urlWithLng('constructor-data-set/search')}}"
                                            data-min-input-length="1" name="field"></select>
                                    <div class="form-error" id="form-error-field"></div>
                                </div>
                            </div>
                            <br/><br/><br/>

                            @include('constructor-data-set.mdm-field-table')
                        </div>
                    </div>
                </div>
                <br/>

            </form>
        </div>
    @endif
@endsection

@section('form-buttons-bottom')
    @if ($selectedConstructorDocumentId)
        {!! renderEditButton() !!}
    @endif
@stop

@section('scripts')
    <script>
        var documentID = "{{ $selectedConstructorDocumentId }}";
    </script>
    <script src="{{asset('js/constructor-data-set/main.js')}}"></script>
@endsection