<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover dataTables-example">
        <thead>
        <tr>
            <th>{{ trans('swis.constructor_data_set.field_id') }} </th>
            <th>{{ trans('swis.constructor_data_set.order') }} </th>
            <th>{{ trans('swis.constructor_data_set.element_id') }} </th>
            <th>{{ trans('swis.constructor_data_set.saf_name') }}</th>
            <th class="data_set_description">{{ trans('swis.constructor_data_set.saf_description') }}</th>
            <th>{{ trans('core.base.label.group') }}</th>
            <th>{{ trans('swis.constructor_data_set.field_label') }}</th>
            <th>{{ trans('swis.constructor_data_set.tool_tip') }}</th>
            <th>{{ trans('swis.constructor_data_set.is_attribute') }}</th>
            <th>{{ trans('swis.constructor_data_set.hide_on_system') }}</th>
            <th>{{ trans('swis.constructor_data_set.search_param') }}</th>
            <th>{{ trans('swis.constructor_data_set.include_search_result') }}</th>
            <th>{{ trans('swis.constructor_data_set.is_unique') }}</th>
            <th>{{ trans('swis.constructor_data_set.is_active') }}</th>
            <th>{{ trans('core.base.action.delete') }}</th>
        </tr>
        </thead>
        <tbody id="tbody">
        @if ($getPreviousDataForDocumentsMdm && count($getPreviousDataForDocumentsMdm) > 0)
            @foreach ($getPreviousDataForDocumentsMdm as $item)
                <tr id="tr-{{ $item['field_id'] }}">
                    <td>{{$item['field_id']}}</td>
                    <td>
                        <div class="form-group" data-toggle="tooltip" title="{{$item['mdm_field_id']}}">
                            <input style="width: 80px" class="form-control" type='number' name='paramsMDM[{{$item['field_id']}}][order]' value="{{ $item['order']}}">
                        </div>
                    </td>
                    <td>{{$item['mdm_field_id']}}<input type="hidden" value="{{$item['mdm_field_id']}}" name="paramsMDM[{{$item['field_id']}}][mdm_field_id]"></td>
                    <td>{{$item['name']}}</td>
                    <td>{{$item['description']}}</td>
                    <td>
                        <div class="form-group">
                            <select class='select2' name='paramsMDM[{{$item['field_id']}}][group]'>
                                <option value=''>Select Group</option>

                                @foreach ($groupPath as $savePath => $path)
                                    <option value="{{$savePath}}" {{ ($savePath == $item['group'] || $path == $item['group']) ? 'selected' : '' }}>{{$path}}</option>
                                @endforeach
                            </select>
                            <div class='form-error' id='form-error-paramsMDM-{{$item['field_id']}}-group'></div>
                        </div>
                    </td>
                    <td class="w-200">
                        <div class="form-group">
                            @foreach (activeLanguages() as $lang)
                                {{ $lang->name }}
                                <input type='text' class="form-control" name='paramsMDM[{{$item['field_id']}}][field_label][{{$lang->id}}]' value="{{ $item['field_label'][$lang->id] }}"/>
                                <div class='form-error' id='form-error-paramsMDM-{{$item['field_id']}}-field_label-{{$lang->id}}'></div>
                                <br/>
                            @endforeach
                        </div>
                    </td>
                    <td class="w-200">
                        <div class="form-group">
                            @foreach (activeLanguages() as $lang)
                                {{ $lang->name }}
                                <input type='text' class="form-control" name='paramsMDM[{{$item['field_id']}}][tooltip][{{$lang->id}}]' value="{{ $item['tooltip'][$lang->id] ?? '' }}"/>
                                <div class='form-error' id='form-error-paramsMDM-{{$item['field_id']}}-tooltip-{{$lang->id}}'></div>
                                <br/>
                            @endforeach
                        </div>
                    </td>
                    <td align='center'>
                        <input title='{{ trans('swis.constructor_data_set.is_attribute') }}'
                               {{ ($item['is_attribute']) ? 'checked' : '' }} {{ $item['reference'] }} type='checkbox'
                               name='paramsMDM[{{$item['field_id']}}][is_attribute]'>
                    </td>
                    <td align='center'>
                        <input title='{{ trans('swis.constructor_data_set.hide_on_system') }}'
                                              {{ ($item['is_hidden']) ? 'checked' : '' }} type='checkbox'
                                              name='paramsMDM[{{$item['field_id']}}][is_hidden]'>
                    </td>
                    <td align='center'>
                        <input title='{{ trans('swis.constructor_data_set.search_param') }}'
                                              {{ ($item['is_search_param']) ? 'checked' : '' }} type='checkbox'
                                              name='paramsMDM[{{$item['field_id']}}][is_search_param]'>
                    </td>
                    <td align='center'>
                        <input title='{{ trans('swis.constructor_data_set.include_search_result') }}'
                                              {{ ($item['show_in_search_results']) ? 'checked' : '' }} type='checkbox'
                                              name='paramsMDM[{{$item['field_id']}}][show_in_search_results]'>
                    </td>
                    <td align='center'>
                        <input title='{{ trans('swis.constructor_data_set.is_unique') }}'
                                              {{ ($item['is_unique']) ? 'checked' : '' }} type='checkbox'
                                              name='paramsMDM[{{$item['field_id']}}][is_unique]'>
                    </td>
                    <td align='center'>
                        <input title='{{ trans('swis.constructor_data_set.is_active') }}'
                                              {{ ($item['is_active']) ? 'checked' : '' }} type='checkbox'
                                              name='paramsMDM[{{$item['field_id']}}][is_active]'>
                    </td>
                    <td align='center'></td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>