@extends('layouts.app')

@section('title'){{trans('swis.risk_instruction.title')}}@endsection

@section('contentTitle') {{trans('swis.risk_instruction.title')}} @endsection

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@endsection

@section('content')

    <?php
    switch ($saveMode) {
        case 'add':
            $url = 'risk-instructions';
            break;
        default :
            $url = $saveMode != 'view' ? 'risk-instructions/update/' . $riskInstruction->id : '';

            $mls = $riskInstruction->ml()->notDeleted()->base(['lng_id', 'description', 'name', 'show_status'])->keyBy('lng_id');

            break;
    }

    $languages = activeLanguages();
    ?>

    <form class="form-horizontal fill-up " id="form-data" action="{{urlWithLng($url)}}">

        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-general"> {{trans('core.base.tab.general')}}</a></li>
                @foreach($languages as $language)
                    <li><a data-toggle="tab" href="#tab-{{$language->code}}">{{ trans("core.base.language.".strtolower($language->name)) }}</a></li>
                @endforeach
            </ul>
            <div class="tab-content">
                <div id="tab-general" class="tab-pane active">
                    <div class="panel-body">

                        <div class="form-group required">
                            <label class="col-lg-2 control-label">{{trans('swis.risk_instructions.document.label')}}</label>
                            <div class="col-lg-8">
                                <select class="form-control select2" name="document_id">
                                    <option value="">{{trans('swis.risk_instructions.document.select.label')}}</option>
                                    @foreach($documents as $document)
                                        <option {{ (isset($riskInstruction) && $riskInstruction->document_id == $document->id) ? 'selected' : '' }} value="{{$document->id}}">{{$document->document_name}}</option>
                                    @endforeach
                                </select>
                                <div class="form-error" id="form-error-document_id"></div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-lg-2 control-label">{{trans('swis.risk_instructions.code.label')}}</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" name="code" value="{{ isset($riskInstruction) ? $riskInstruction->code : '' }}" {{ isset($riskInstruction) ? "readonly" : '' }}>
                                <div class="form-error" id="form-error-code"></div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-lg-2 control-label">{{trans('swis.risk_instructions.type.label')}}</label>
                            <div class="col-lg-8">
                                <select class="form-control select2" name="type">
                                    <option value="">{{trans('swis.risk_instructions.type.select.label')}}</option>
                                    @foreach($types as $type)
                                        <option {{ (isset($riskInstruction) && $riskInstruction->type_code == $type->code) ? 'selected' : '' }} value="{{$type->id}}">{{$type->name}}</option>
                                    @endforeach
                                </select>
                                <div class="form-error" id="form-error-type"></div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-lg-2 control-label">{{trans('swis.risk_instructions.feedback.label')}}</label>
                            <div class="col-lg-8">
                                <select class="form-control select2 " name="feedback[]" multiple="multiple">
                                    <option value="">{{trans('swis.risk_instructions.feedback.select.label')}}</option>
                                    @foreach($feedbacks as $feedback)
                                        <option {{ (isset($riskInstruction) && in_array($feedback->code, $attachedFeedback)) ? 'selected' : '' }} value="{{$feedback->id}}">{{$feedback->name}}</option>
                                    @endforeach
                                </select>
                                <div class="form-error" id="form-error-feedback"></div>
                            </div>
                        </div>

{{--                        <div class="form-group required">--}}
{{--                            <label class="col-lg-2 control-label">{{trans('swis.risk_instructions.name.label')}}</label>--}}
{{--                            <div class="col-lg-8">--}}
{{--                                <input type="text" class="form-control" name="name" value="{{ isset($riskInstruction) ? $riskInstruction->name : '' }}">--}}


{{--                                <div class="form-error" id="form-error-name"></div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

                        <div class="form-group required">
                            <label class="col-lg-2 control-label">{{trans('swis.risk_instructions.active_date_from.label')}}</label>
                            <div class="col-lg-8">
                                <div class='input-group date'>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                    <input data-start-date="{{currentDate()}}" value="{{ isset($riskInstruction) ? $riskInstruction->active_date_from : '' }}" name="active_date_from"
                                           type="text" autocomplete="off"
                                           placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                </div>
                                <div class="form-error" id="form-error-active_date_from"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('swis.risk_instructions.active_date_to.label')}}</label>
                            <div class="col-lg-8">
                                <div class='input-group date'>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                    <input  data-start-date="{{currentDate()}}" value="{{ isset($riskInstruction) ? $riskInstruction->active_date_to : '' }}" name="active_date_to"
                                           type="text" autocomplete="off"
                                           placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                </div>
                                <div class="form-error" id="form-error-active_date_to"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">{{trans('core.base.label.status')}}</label>
                            <div class="col-lg-8 general-status ">
                                <select name="show_status" class="form-show-status">
                                    <option value="{{\App\Models\Role\Role::STATUS_ACTIVE}}" {{ empty($riskInstruction) ? 'selected' : '' }} {{(isset($riskInstruction) && $riskInstruction->show_status == App\Models\RolesGroup\RolesGroup::STATUS_ACTIVE) ?  'selected' : ''}}>{{trans('core.base.label.status.active')}}</option>
                                    <option value="{{\App\Models\Role\Role::STATUS_INACTIVE}}" {{(isset($riskInstruction) && $riskInstruction->show_status == App\Models\RolesGroup\RolesGroup::STATUS_INACTIVE) ?  'selected' : ''}}>{{trans('core.base.label.status.inactive')}}</option>
                                </select>
                                <div class="form-error" id="form-error-show_status"></div>
                            </div>
                        </div>

                    </div>

                </div>

                @foreach($languages as $language)
                    <div id="tab-{{$language->code}}" class="tab-pane">
                        <div class="panel-body">
                            <div class="form-group required">
                                <label class="col-lg-2 control-label">{{trans('swis.risk_instructions.name.label')}}</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="ml[{{$language->id}}][name]"
                                           value="{{ isset($mls[$language->id]) ? $mls[$language->id]->name : '' }}">

                                    <div class="form-error" id="form-error-ml-{{$language->id}}-name"></div>
                                </div>
                            </div>

                            <div class="form-group required"><label class="col-lg-2 control-label">{{trans('core.base.description')}}</label>
                                <div class="col-lg-8">
                                    <textarea rows="6" name="ml[{{$language->id}}][description]"
                                              class="form-control">{{isset($mls[$language->id]) ? $mls[$language->id]->description : ''}}</textarea>
                                    <div class="form-error"
                                         id="form-error-ml-{{$language->id}}-description"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <input type="hidden" value="{{$riskInstruction->id or 0}}" name="id" class="mc-form-key"/>
            {!! csrf_field() !!}
        </div>
    </form>
@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@endsection


@section('scripts')
    <script src="{{asset('js/risk-instructions/main.js')}}"></script>
@endsection