@extends('layouts.app')

@section('title'){{trans('swis.risk_instructions.title')}}@endsection

@section('contentTitle') {{trans('swis.risk_instruction.title')}} @endsection

@section('form-buttons-top')
    {!! renderAddButton() !!}
@endsection

@section('content')
    <div class="clearfix search__out">
        <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse" data-target="#collapseSearch" aria-expanded="false" aria-controls="collapseSearch">
            <i class="fa fa-search"> </i><span class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
        </button>
    </div>
    <div class="collapse" id="collapseSearch">
        <div class="card card-body">
            <div class="row">
                <div>
                    <form class="form-horizontal fill-up" id="search-filter" autocomplete="off">

                        <div class="form-group"><label class="col-md-4 col-lg-3 control-label">{{ trans('swis.risk-instructions.document_id.label') }}</label>
                            <div class="col-lg-6">
                                <select class="form-control select2" name="document_id">
                                    <option value=""></option>
                                    @foreach ($constructorDocuments as $constructorDocument)
                                        <option value="{{$constructorDocument->id}}">{{ $constructorDocument->document_name }}</option>
                                    @endforeach
                                </select>
                                <div class="form-error" id="form-error-document_id"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.risk_instructions.table.name')}}</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="name" id="" value="">
                                <div class="form-error" id="form-error-name"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.risk_instructions.table.code')}}</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="code" id="" value="">
                                <div class="form-error" id="form-error-name"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.risk_instructions.table.description')}}</label>
                            <div class="col-lg-6 field__one-col">
                                <input type="text" class="form-control" name="description" id="" value="">
                                <div class="form-error" id="form-error-description"></div>
                            </div>
                        </div>

                        <div class="form-group searchParam created_at_block_params" data-field="created_at_block_params">
                            <label class="col-md-4 col-lg-3  control-label label__title">{{trans('swis.risk_instructions.table.active_date_from')}}</label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-4 field__two-col">
                                        <div class='input-group date'>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                            <input value="" name="active_date_from_start" autocomplete="off"
                                                   type="text"
                                                   placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                            <div class="form-error" id="form-error-active_date_from_start"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 field__two-col">
                                        <div class='input-group date'>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                            <input value="" name="active_date_from_end" autocomplete="off"
                                                   type="text"
                                                   placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                            <div class="form-error" id="form-error-active_date_from_end"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam created_at_block_params" data-field="created_at_block_params">
                            <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.risk_instructions.table.active_date_to')}}</label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-4 field__two-col">
                                        <div class='input-group date'>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                            <input value="" name="active_date_to_start" autocomplete="off"
                                                   type="text"
                                                   placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                            <div class="form-error" id="form-error-active_date_to_start"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 field__two-col">
                                        <div class='input-group date'>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                            <input value="" name="active_date_to_end" autocomplete="off"
                                                   type="text"
                                                   placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                            <div class="form-error" id="form-error-active_date_to_end"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-8 col-lg-offset-1 text-right">
                                <button type="submit" class="btn btn-primary">{{trans('core.base.label.search')}}</button>
                                <button type="button" class="btn btn-danger resetButton" data-submit="true">{{trans('core.base.label.reset')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="data-table">
        <div class="clearfix">
            <table class="table table-striped table-bordered table-hover dataTables-example" id="list-data">
                <thead>
                <tr>
                    <th data-col="actions" data-delete="0" class="th-actions">{{trans('swis.risk_instructions.table.actions')}}</th>
                    <th data-col="id">{{trans('swis.risk_instructions.table.id')}}</th>
                    <th data-col="name">{{trans('swis.risk_instructions.table.name')}}</th>
                    <th data-col="code">{{trans('swis.risk_instructions.table.code')}}</th>
                    <th data-col="description">{{trans('swis.risk_instructions.table.description')}}</th>
                    <th data-col="document_name">{{trans('swis.risk_instructions.table.document_name')}}</th>
                    <th data-col="show_status">{{trans('swis.risk_instructions.table.show_status')}}</th>
                    <th data-col="active_date_from">{{trans('swis.risk_instructions.table.active_date_from')}}</th>
                    <th data-col="active_date_to">{{trans('swis.risk_instructions.table.active_date_to')}}</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('js/risk-instructions/main.js')}}"></script>
@endsection