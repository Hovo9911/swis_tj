@extends('layouts.app')

@section('title'){{trans('swis.reports_access.title')}}@endsection

@section('contentTitle') {{trans('swis.reports_access.forms_title')}} @endsection

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@endsection

@section('content')
    <?php
    use App\Models\ReportsAccess\ReportsAccess;
    
    switch ($saveMode) {
        case 'add':
            $url = 'reports-access';
            break;
        default:
            $url = $saveMode != 'view' ? 'reports-access/update/' . $reportsAccess->id : '';

            $currentValue = '<option value="{"uri":"' . $reportsAccess->jasper_report_uri . '","label":"' . $reportsAccess->jasper_report_label . '"}" selected>' . $reportsAccess->jasper_report_label . ' </option>';
            break;
    }
    ?>

    <form class="form-horizontal fill-up " id="form-data" action="{{urlWithLng($url)}}" autocomplete="off">
        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-general"> {{trans('core.base.tab.general')}}</a></li>
            </ul>
            <div class="tab-content">
                <div id="tab-general" class="tab-pane active">
                    <div class="panel-body">
                        <div class="form-group required"><label
                                    class="col-lg-2 control-label">{{trans('swis.reports_access.report_name.label')}}</label>
                            <div class="col-lg-10">
                                <select class="form-control select2" name="jasper_report_info">
                                    <option></option>
                                    {!! $currentValue ?? '' !!}
                                    @if(count($jasperReports))
                                        @foreach($jasperReports as $reportUri => $reportLabel)
                                            <option value='{"uri":"{{$reportUri}}","label":"{{$reportLabel}}"}'>{{$reportLabel}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <div class="form-error" id="form-error-jasper_report_info"></div>
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-lg-2 control-label">{{trans('swis.reports_access.report_type.label')}}</label>
                            <div class="col-lg-10 radio-list-report">
                                <label>
                                    <input type="radio" name="role_type"
                                           value="{{ReportsAccess::ROLE_TYPE_SW_ADMIN}}"
                                           @if(isset($reportsAccess) && $reportsAccess->role_type  == ReportsAccess::ROLE_TYPE_SW_ADMIN) checked @endif /> {{ trans('swis.reports_access.report_type.sw_admin') }}
                                </label>
                                <label>
                                    <input type="radio" name="role_type"
                                           value="{{ReportsAccess::ROLE_TYPE_AGENCY}}"
                                           @if(isset($reportsAccess) && $reportsAccess->role_type == ReportsAccess::ROLE_TYPE_AGENCY) checked @endif /> {{ trans('swis.reports_access.report_type.agency') }}
                                </label>
                                <div class="form-error" id="form-error-role_type"></div>
                            </div>
                        </div>
                        <div class="form-group agencies required @if(!(isset($reportsAccess) && $reportsAccess->role_type == ReportsAccess::ROLE_TYPE_AGENCY)) hide @endif">
                            <label class="col-lg-2 control-label">{{trans('swis.reports_access.label.agency')}}</label>
                            <div class="col-lg-10">
                                @if(count($agencies) > 0)
                                    <select class="form-control select2" id="agency" name="agencies[]" multiple>
                                        @foreach($agencies as $agency)
                                            <option {{(isset($selectedAgencies) && in_array($agency->tax_id,$selectedAgencies)) ? 'selected' : ''}}  value="{{$agency->tax_id}}">{{$agency->name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="form-error" id="form-error-agencies"></div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">{{trans('swis.constructor_templates.show_status.label')}}</label>
                            <div class="col-lg-10 general-status">
                                <select name="show_status" class="form-show-status">
                                    <option value="{{\App\Models\BaseModel::STATUS_ACTIVE}}"{{(!empty($reportsAccess) && $reportsAccess->show_status == \App\Models\BaseModel::STATUS_ACTIVE) || $saveMode == 'add' ?  ' selected' : ''}}>{{trans('core.base.label.status.active')}}</option>
                                    <option value="{{\App\Models\BaseModel::STATUS_INACTIVE}}"{{!empty($reportsAccess) &&  $reportsAccess->show_status == \App\Models\BaseModel::STATUS_INACTIVE ? 'selected': ''}}>{{trans('core.base.label.status.inactive')}}</option>
                                </select>
                                <div class="form-error" id="form-error-show_status"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script src="{{asset('js/reports-access/main.js')}}"></script>
@endsection