@extends('layouts.app')

@section('title'){{trans('swis.reports_access.title')}}@endsection

@section('contentTitle') {{trans('swis.reports_access.title')}} @endsection

@section('form-buttons-top')
    {!! renderAddButton() !!}
    {!! renderDeleteButton() !!}
@endsection

@section('content')
    <div class="clearfix search__out">
        <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse" data-target="#collapseSearch" aria-expanded="false" aria-controls="collapseSearch">
            <i class="fa fa-search"> </i><span class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
        </button>
    </div>
    <form class="form-horizontal fill-up" id="search-filter" autocomplete="off">
        <div class="collapse" id="collapseSearch">
            <div class="card card-body">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="form-group"><label class="col-lg-2 control-label">ID</label>
                            <div class="col-lg-8">
                                <input type="number" min="0" class="form-control onlyNumbers" name="id" id="" value="">
                                <div class="form-error" id="form-error-id"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-8">
                                <button type="submit" class="btn btn-primary">{{trans('core.base.label.search')}}</button>
                                <button type="button" class="btn btn-danger resetButton" data-submit="true">{{trans('core.base.label.reset')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class="data-table table-block">
        <div class="clearfix">
            <table class="table table-striped table-bordered table-hover dataTables-example" id="list-data">
                <thead>
                <tr>
                    <th data-col="actions" class="th-actions" data-delete="0">{{trans('core.base.label.actions')}}</th>
                    <th data-col="check" class="th-checkbox"><input type="checkbox" name=""></th>
                    <th data-col="id">ID</th>
                    <th data-col="jasper_report_label">{{trans('swis.reports_access.jasper_report.label')}}</th>
                    <th data-col="show_status">{{trans('swis.reports_access.show_status.label')}}</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('js/reports-access/main.js')}}"></script>
@endsection