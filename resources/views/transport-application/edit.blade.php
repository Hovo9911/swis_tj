@extends('layouts.app')

@section('title') {{trans('swis.transport_application.title')}}@stop

@section('contentTitle') {{trans('swis.transport_application.title')}}@stop

@section('form-buttons-top')

    @if($saveMode != 'view')
    {!! \App\Models\TransportApplication\TransportApplication::generateStatusButtons($currentStatus) !!}
    @endif

    @if($currentStatus == $waitingPaymentStatus || $currentStatus == $issuedStatus)

        @if($currentStatus == $issuedStatus)
            <a target="_blank" href="{{urlWithLng('transport-application/print-permit-doc/'. $transportApplication->id)}}"
               class="btn btn-primary m-r-sm">{{trans('swis.transport_application.print_permit_doc.button')}}</a>
        @endif

        <a target="_blank" href="{{urlWithLng('transport-application/print-invoice/'. $transportApplication->id)}}"
           class="btn btn-default m-r-sm">{{trans('swis.transport_application.print_invoice.button')}}</a>
    @endif

    {!! renderCancelButton() !!}
@stop

@section('topScripts')
    <script>
        var registeredStatus = '{{$registeredStatus}}'
        var rejectedStatus = '{{$rejectedStatus}}'
        var waitingPaymentStatus = '{{$waitingPaymentStatus}}'
    </script>
@endsection

@section('content')
    <?php

    switch ($saveMode) {
        case 'add':
            $url = 'transport-application';
            break;
        default:
            $url = $saveMode != 'view' ? 'transport-application/update/' . $transportApplication->id : '';
            break;
    }

    $gUploader = new \App\GUploader('transport_application_receipt_file');
    ?>

    <form class="form-horizontal fill-up" id="form-data" {{!is_null($currentStatus) && $currentStatus != $registeredStatus ? 'data-view-mode="true"' : '' }} action="{{urlWithLng($url)}}" autocomplete="off">
        <div class="tabs-container">

            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-general"> {{trans('core.base.tab.general')}}</a></li>
                @if(!is_null($currentStatus))
                <li><a data-toggle="tab" href="#tab-states"> {{trans('swis.transport_application.tab.states')}}</a></li>
                @endif
            </ul>

            <div class="tab-content">

                <div id="tab-general" class="tab-pane active">
                    <div class="panel-body">

                    @if($saveMode != 'add')

                        @if($currentStatus == $rejectedStatus)
                            <div class="form-group">
                                <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.reject_reason.label')}}</label>
                                <div class="col-lg-6">
                                    <textarea rows="10" class="form-control">{{$transportApplication->reject_reason}}</textarea>
                                </div>
                            </div>
                            <hr />
                        @endif

                        @if($currentStatus == $waitingPaymentStatus)
                            <div class="form-group">
                                <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.receipt_document.label')}}</label>
                                <div class="col-lg-6">
                                    <div class="g-upload-container">
                                        <div class="register-form__list">
                                            <div class="register-form__item">

                                                <div class="g-upload-box">
                                                    <label class="btn btn-primary btn-upload">
                                                        <i class="fa fa-upload"></i>
                                                        {{trans('swis.transport_application.upload.receipt.button')}}
                                                        <input id="files"
                                                               class="dn g-upload" type="file"
                                                               name="transport_application_receipt_file"
                                                               value="{{$transportApplication->transport_application_receipt_file or ''}}"
                                                               {{$gUploader->isMultiple() ? 'multiple' : ''}} data-conf="transport_application_receipt_file"
                                                               data-action="{{urlWithLng('/g-uploader/upload')}}"
                                                               accept="{{$gUploader->getAcceptTypes()}}"/>
                                                    </label>
                                                </div>

                                                <div class="license-form__uploaded-file-list g-uploaded-list"></div>
                                                <progress class="progressBarFileUploader" value="0"
                                                          max="100"></progress>
                                                <div class="form-error-text form-error-files fb fs12"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-error" id="form-error-transport_application_receipt_file"></div>
                                </div>
                            </div>
                            <hr />
                        @endif

                        <div class="form-group">
                            <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.current_status.label')}}</label>
                            <div class="col-lg-6">
                                <input value="{{trans('swis.transport_application.current_status.'.$transportApplication->current_status.'.status')}}" disabled type="text" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.application_number.label')}}</label>
                            <div class="col-lg-6">
                                <input value="{{$transportApplication->application_number or ''}}" disabled type="text" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.created_at.label')}}</label>
                            <div class="col-lg-6">
                                <input value="{{$transportApplication->created_at or ''}}" disabled type="text" class="form-control">
                            </div>
                        </div>

                        @if(!is_null($transportApplication->invoice))
                            <div class="form-group">
                                <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.invoice.payment_amount.label')}}</label>
                                <div class="col-lg-6">
                                    <input value="{{$transportApplication->invoice->payment_amount}}" disabled type="text" class="form-control">
                                </div>
                            </div>
                        @endif

                        @if($currentStatus == $issuedStatus)
                            <div class="form-group">
                                <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.receipt_file.label')}}</label>
                                <div class="col-lg-6">
                                    <div class="m-t-xs">
                                        <a target="_blank"
                                           href="{{$transportApplication->filePath()}}">{{$transportApplication->invoiceFileBaseName()}}</a>
                                        <span class="documents-separate-line">|</span>
                                        <a class="btn btn-primary btn-xs" download=""
                                           href="{{$transportApplication->filePath()}}"><i
                                                    class="fas fa-download"></i> {{trans('swis.document.label.download')}}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <hr />
                    @endif

                    @if($saveMode != 'add' && $currentStatus != $rejectedStatus)

                        <div class="form-group required">
                            <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.permission_number.label')}}</label>
                            <div class="col-lg-6">
                                <input value="{{$transportApplication->permission_number or ''}}" data-toggle="tooltip" title="{{$transportApplication->permission_number or ''}}" name="permission_number" type="text" class="form-control">
                                <div class="form-error" id="form-error-permission_number"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.issue_country.label')}}</label>
                            <div class="col-lg-6">
                                <select class="select2" name="ref_issue_country">
                                    <option></option>
                                    @foreach($refCountries as $country)
                                        <option {{isset($transportApplication) && $transportApplication->ref_issue_country == $country->id ? 'selected' : ''}} value="{{$country->id}}">{{'('.$country->code.')'}} {{$country->name}}</option>
                                    @endforeach
                                </select>
                                <div class="form-error" id="form-error-ref_issue_country"></div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.permit_validity_period_from.label')}}</label>
                            <div class="col-lg-6">
                                <div class='input-group date'>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                    <input autocomplete="off" id="permitValidityPeriodFrom" placeholder="{{setDatePlaceholder()}}" value="{{$transportApplication->permit_validity_period_from or ''}}" data-start-date="{{currentDate()}}" name="permit_validity_period_from" type='text' class="form-control permit-date"/>
                                </div>
                                <div class="form-error" id="form-error-permit_validity_period_from"></div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.permit_validity_period_to.label')}}</label>
                            <div class="col-lg-6">
                                <div class='input-group date'>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                    <input autocomplete="off" id="permitValidityPeriodTo" placeholder="{{setDatePlaceholder()}}"  value="{{$transportApplication->permit_validity_period_to or ''}}" data-start-date="{{currentDate()}}" name="permit_validity_period_to" type='text' class="form-control permit-date"/>
                                </div>
                                <div class="form-error" id="form-error-permit_validity_period_to"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.period_validity.label')}}</label>
                            <div class="col-lg-6">
                                <input id="permitValidityPeriodDiff" value="{{$transportApplication->period_validity or ''}}" name="period_validity" readonly type="text" class="form-control">
                                <div class="form-error" id="form-error-period_validity"></div>
                            </div>
                        </div>

                        <div class="permit-type-fields hide" data-permit-type="{{$transportPermitTypeP1.','.$transportPermitTypeP_BP}}">

                            @if($currentStatus == $registeredStatus)

                                <div class="form-group required">
                                    <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.ref_border_crossing.label')}}</label>
                                    <div class="col-lg-6">
                                        <select class="select2" name="ref_border_crossing">
                                            <option></option>
                                            @foreach($refTransportBorderCheckpoints as $borderCheckpoint)
                                                <option {{isset($transportApplication) && $transportApplication->ref_border_crossing == $borderCheckpoint->id ? 'selected' : ''}} value="{{$borderCheckpoint->id}}">{{'('.$borderCheckpoint->short_name.')'}} {{$borderCheckpoint->name}}</option>
                                            @endforeach
                                        </select>
                                        <div class="form-error" id="form-error-ref_border_crossing"></div>
                                    </div>
                                </div>

                            @elseif($transportApplication->ref_border_crossing)

                                <?php
                                $refBorderCrossing = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_TRANSPORT_BORDER_CHECKPOINTS,$transportApplication->ref_border_crossing,false,['id','code','name','short_name']);
                                ?>

                                <div class="form-group required">
                                    <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.ref_border_crossing.label')}}</label>
                                    <div class="col-lg-6">
                                        <input class="form-control" value="{{'('.$refBorderCrossing->short_name.')'}} {{$refBorderCrossing->name}}" data-toggle="tooltip" title="{{'('.$refBorderCrossing->short_name.')'}} {{$refBorderCrossing->name}}">
                                    </div>
                                </div>

                            @endif

                            <div class="form-group required">
                                <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.arrival_date.label')}}</label>
                                <div class="col-lg-6">
                                    <div class='input-group date'>
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        <input autocomplete="off" placeholder="{{setDatePlaceholder()}}"  value="{{$transportApplication->arrival_date or ''}}" name="arrival_date" data-start-date="{{currentDate()}}" type='text' class="form-control"/>
                                    </div>
                                    <div class="form-error" id="form-error-arrival_date"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.departure_date.label')}}</label>
                                <div class="col-lg-6">
                                    <div class='input-group date'>
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        <input autocomplete="off" placeholder="{{setDatePlaceholder()}}"  value="{{$transportApplication->departure_date or ''}}" name="departure_date" data-start-date="{{currentDate()}}" type='text' class="form-control"/>
                                    </div>
                                    <div class="form-error" id="form-error-departure_date"></div>
                                </div>
                            </div>

                        </div>
                        <hr />

                    @endif

                    <div class="form-group required">
                        <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.driver_name.label')}}</label>
                        <div class="col-lg-6">
                            <input value="{{$transportApplication->driver_name or ''}}" data-toggle="tooltip" title="{{$transportApplication->driver_name or ''}}" name="driver_name" type="text" class="form-control">
                            <div class="form-error" id="form-error-driver_name"></div>
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.ref_transport_carriers_id.label')}}</label>
                        <div class="col-lg-6">

                            <input type="text" placeholder="{{trans('swis.core.autocomplete.placeholder')}}" value="{{$transportApplication->refTransportCarriers or ''}}" data-toggle="tooltip" title="{{$transportApplication->refTransportCarriers or ''}}"
                                   data-source="{{\App\Models\ReferenceTable\ReferenceTable::getReferenceIdByTableName(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_TRANSPORT_PERMIT_CARRIERS)}}"
                                   data-name="ref_transport_carriers_id"
                                   class="form-control autocomplete">
                            <input type="hidden" name="ref_transport_carriers_id" value="{{$transportApplication->ref_transport_carriers_id or ''}}">

                            @if(is_null($currentStatus) || $currentStatus == $registeredStatus)
                                <a target="_blank" class="fs13" href="{{urlWithLng('reference-table-form/'.\App\Models\ReferenceTable\ReferenceTable::getReferenceIdByTableName(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_TRANSPORT_PERMIT_CARRIERS).'/create')}}">{{trans('swis.transport_application.ref_transport_carriers_id.ref_link')}}</a>
                            @endif
                            <div class="form-error" id="form-error-ref_transport_carriers_id"></div>
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.vehicle_brand.label')}}</label>
                        <div class="col-lg-6">
                            <input value="{{$transportApplication->vehicle_brand or ''}}" name="vehicle_brand" data-toggle="tooltip" title="{{$transportApplication->vehicle_brand or ''}}" type="text" class="form-control">
                            <div class="form-error" id="form-error-vehicle_brand"></div>
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-lg-3 control-label label__title pd-t-0">{{trans('swis.transport_application.vehicle_reg_number.label')}}</label>
                        <div class="col-lg-6">
                            <input value="{{$transportApplication->vehicle_reg_number or ''}}" data-toggle="tooltip" title="{{$transportApplication->vehicle_reg_number or ''}}" name="vehicle_reg_number" type="text" class="form-control">
                            <div class="form-error" id="form-error-vehicle_reg_number"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.trailer_reg_number.label')}}</label>
                        <div class="col-lg-6">
                            <input value="{{$transportApplication->trailer_reg_number or ''}}" data-toggle="tooltip" title="{{$transportApplication->trailer_reg_number or ''}}" name="trailer_reg_number" type="text" class="form-control">
                            <div class="form-error" id="form-error-trailer_reg_number"></div>
                        </div>
                    </div>

                    @if(is_null($currentStatus) || $currentStatus == $registeredStatus)

                        <div class="form-group required">
                            <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.origin_country.label')}}</label>
                            <div class="col-lg-6">
                                <select class="select2" name="ref_origin_country">
                                    <option></option>
                                    @foreach($refCountries as $country)
                                        <option {{isset($transportApplication) && $transportApplication->ref_origin_country == $country->id ? 'selected' : ''}} value="{{$country->id}}">{{'('.$country->code.')'}} {{$country->name}}</option>
                                    @endforeach
                                </select>
                                <div class="form-error" id="form-error-ref_origin_country"></div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.destination_country.label')}}</label>
                            <div class="col-lg-6">
                                <select class="select2" name="ref_destination_country">
                                    <option></option>
                                    @foreach($refCountries as $country)
                                        <option {{isset($transportApplication) && $transportApplication->ref_destination_country == $country->id ? 'selected' : ''}} value="{{$country->id}}">{{'('.$country->code.')'}} {{$country->name}}</option>
                                    @endforeach
                                </select>
                                <div class="form-error" id="form-error-ref_destination_country"></div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.ref_transport_permit_type.label')}}</label>
                            <div class="col-lg-6">
                                <select class="select2" name="ref_transport_permit_type" id="transportPermitTypes">
                                    <option></option>
                                    @foreach($refTransportPermitTypes as $permitType)
                                        <option data-type="{{$permitType->code}}" {{isset($transportApplication) && $transportApplication->ref_transport_permit_type == $permitType->id ? 'selected' : ''}} value="{{$permitType->id}}">{{'('.$permitType->short_name.')'}} {{$permitType->name}}</option>
                                    @endforeach
                                </select>
                                <div class="form-error" id="form-error-ref_transport_permit_type"></div>
                            </div>
                        </div>

                    @else

                        <?php
                            $refOriginCountry = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_COUNTRIES,$transportApplication->ref_origin_country);
                            $refDestinationCountry = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_COUNTRIES,$transportApplication->ref_destination_country);
                            $refTransportPermitType =  getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_TRANSPORT_PERMIT_TYPES,$transportApplication->ref_transport_permit_type,false,['id','code','name','short_name']);
                        ?>

                        <div class="form-group required">
                            <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.origin_country.label')}}</label>
                            <div class="col-lg-6">
                                <input class="form-control" value="{{'('.$refOriginCountry->code.')'}} {{$refOriginCountry->name}}" data-toggle="tooltip" title="{{'('.$refOriginCountry->code.')'}} {{$refOriginCountry->name}}">
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.destination_country.label')}}</label>
                            <div class="col-lg-6">
                                <input class="form-control" value="{{'('.$refDestinationCountry->code.')'}} {{$refDestinationCountry->name}}" data-toggle="tooltip" title="{{'('.$refDestinationCountry->code.')'}} {{$refDestinationCountry->name}}">
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.ref_transport_permit_type.label')}}</label>
                            <div class="col-lg-6">
                                <input class="form-control" id="transportPermitType" data-type="{{$refTransportPermitType->code}}" value="{{'('.$refTransportPermitType->short_name.')'}} {{$refTransportPermitType->name}}" data-toggle="tooltip" title="{{'('.$refTransportPermitType->short_name.')'}} {{$refTransportPermitType->name}}">
                            </div>
                        </div>

                    @endif

                    {{--  Permit Fields by type open close  --}}
                    <div class="permit-type-fields hide" data-permit-type="{{$transportPermitTypeP1.','.$transportPermitTypeP_BP}}">

                        <div class="form-group required">
                            <label class="col-lg-3 control-label label__title pd-t-0">{{trans('swis.transport_application.vehicle_carrying_capacity.label')}}</label>
                            <div class="col-lg-6">
                                <input value="{{$transportApplication->vehicle_carrying_capacity or ''}}" name="vehicle_carrying_capacity" type="text" class="form-control">
                                <div class="form-error" id="form-error-vehicle_carrying_capacity"></div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.vehicle_empty_capacity.label')}}</label>
                            <div class="col-lg-6">
                                <input value="{{$transportApplication->vehicle_empty_capacity or ''}}" name="vehicle_empty_capacity" type="text" class="form-control">
                                <div class="form-error" id="form-error-vehicle_empty_capacity"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.trailer_carrying_capacity.label')}}</label>
                            <div class="col-lg-6">
                                <input value="{{$transportApplication->trailer_carrying_capacity or ''}}" name="trailer_carrying_capacity" type="text" class="form-control">
                                <div class="form-error" id="form-error-trailer_carrying_capacity"></div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.trailer_empty_capacity.label')}}</label>
                            <div class="col-lg-6">
                                <input value="{{$transportApplication->trailer_empty_capacity or ''}}" name="trailer_empty_capacity" type="text" class="form-control">
                                <div class="form-error" id="form-error-trailer_empty_capacity"></div>
                            </div>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.place_of_unloading.label')}}</label>
                        <div class="col-lg-6">
                            <input value="{{$transportApplication->place_of_unloading or ''}}" name="place_of_unloading" data-toggle="tooltip" title="{{$transportApplication->place_of_unloading or ''}}" type="text" class="form-control">
                            <div class="form-error" id="form-error-place_of_unloading"></div>
                        </div>
                    </div>

                    {{--  Permit Fields by type open close  --}}
                    <div class="permit-type-fields hide" data-permit-type="{{$transportPermitTypeP3}}">

                        <div class="form-group">
                            <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.nature_of_goods.label')}}</label>
                            <div class="col-lg-6">
                                <input value="{{$transportApplication->nature_of_goods or ''}}" name="nature_of_goods" data-toggle="tooltip" title="{{$transportApplication->nature_of_goods or ''}}" type="text" class="form-control">
                                <div class="form-error" id="form-error-nature_of_goods"></div>
                            </div>
                        </div>

                    </div>

                    <div class="form-group required">
                        <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.entry_goods_weight.label')}}</label>
                        <div class="col-lg-6">
                            <input value="{{$transportApplication->entry_goods_weight or ''}}" name="entry_goods_weight" type="text" class="form-control">
                            <div class="form-error" id="form-error-entry_goods_weight"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label label__title">{{trans('swis.transport_application.departure_goods_weight.label')}}</label>
                        <div class="col-lg-6">
                            <input value="{{$transportApplication->departure_goods_weight or ''}}" name="departure_goods_weight" type="text" class="form-control">
                            <div class="form-error" id="form-error-departure_goods_weight"></div>
                        </div>
                    </div>


                    </div>
                </div>

                @if(!is_null($currentStatus))
                <div id="tab-states" class="tab-pane">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>{{trans('swis.transport_application.state.user_changes')}}</th>
                                <th>{{trans('swis.transport_application.state.from_status')}}</th>
                                <th>{{trans('swis.transport_application.state.to_status')}}</th>
                                <th>{{trans('swis.transport_application.state.created_at')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($transportApplication->states as $state)
                                <tr>
                                    <td>{{$state->user->name}}</td>
                                    <td>{{$state->from_status ?  trans('swis.transport_application.status.'.$state->from_status.'.status') : '-'}}</td>
                                    <td>{{trans('swis.transport_application.status.'.$state->to_status.'.status')}}</td>
                                    <td>{{$state->created_at}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @endif
            </div>

            {{--  --}}
            <input type="hidden" id="transportApplicationCurrentStatus" value="{{$currentStatus}}">
            <input type="hidden" id="transportApplicationStatus" name="current_status">
            <input type="hidden" id="transportId" name="id" value="{{$transportApplication->id or 0}}">
        </div>
    </form>

    @include('transport-application.partials.modals')
@endsection

@section('form-buttons-bottom')

    @if($saveMode != 'view')
    {!! \App\Models\TransportApplication\TransportApplication::generateStatusButtons($currentStatus) !!}
    @endif

    @if($currentStatus == $waitingPaymentStatus || $currentStatus == $issuedStatus)

        @if($currentStatus == $issuedStatus)
            <a target="_blank" href="{{urlWithLng('transport-application/print-permit-doc/'. $transportApplication->id)}}"
               class="btn btn-primary m-r-sm">{{trans('swis.transport_application.print_permit_doc.button')}}</a>
        @endif

        <a target="_blank" href="{{urlWithLng('transport-application/print-invoice/'. $transportApplication->id)}}"
           class="btn btn-default m-r-sm">{{trans('swis.transport_application.print_invoice.button')}}</a>
    @endif

    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script src="{{asset('assets/helix/core/plugins/bootstrap-typeahead/bootstrap-typeahead.js')}}"></script>
    <script src="{{asset('js/transport-application/main.js')}}"></script>
    <script src="{{asset('js/guploader.js')}}"></script>
@endsection