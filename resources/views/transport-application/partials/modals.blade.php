@if(isset($transportApplication))

    @if($transportApplication->current_status == $registeredStatus)

        <div class="modal fade" id="rejectReasonModal" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <form class="form-horizontal fill-up" id="reject-reason" action="{{urlWithLng('transport-application/reject/'.$transportApplication->id)}}" autocomplete="off">
                        <div class="modal-body">
                            <h2>{{trans('swis.saf.transport_application.reject_reason.title')}}</h2>
                            <div class="form-group ml-mr-none">
                                <textarea class="form-control" rows="10" name="reject_reason"></textarea>
                                <div class="form-error" id="form-error-reject_reason"></div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">{{trans('swis.saf.transport_application.reject_reason.modal.button')}}</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('core.base.button.close')}}</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>

    @endif

    @if($transportApplication->current_status == $approvedStatus)

        <div class="modal fade" id="invoiceModal" role="dialog">
            <div class="modal-dialog modal-md">

                <!-- Modal content-->
                <div class="modal-content">
                    <form class="form-horizontal fill-up" id="reject-reason" action="{{urlWithLng('transport-application/invoice/'.$transportApplication->id)}}" autocomplete="off">
                        <div class="modal-body">
                            <h3>{{trans('swis.saf.transport_application.payment_amount.title')}}</h3>
                            <div class="form-group ml-mr-none">
                                <input type="text" class="form-control" name="payment_amount">
                                <div class="form-error" id="form-error-payment_amount"></div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">{{trans('swis.saf.transport_application.invoice.modal.button')}}</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('core.base.button.close')}}</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>

    @endif

@endif

