@extends('layouts.app')

@section('title'){{trans('swis.transport_applications.title')}}@stop

@section('contentTitle') {{trans('swis.transport_applications.title')}} @stop

@section('form-buttons-top')
    {!! renderAddButton() !!}
@stop

<?php
$transportStatusTrans = [];
foreach (\App\Models\TransportApplication\TransportApplication::TRANSPORT_APPLICATION_STATUSES as $status) {
    $transportStatusTrans[] = 'swis.transport_application.current_status.'.$status.'.status';
}

$jsTrans->addTrans($transportStatusTrans);
?>

@section('content')

    <div class="clearfix search__out">
        <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse"
                data-target="#collapseSearch" aria-expanded="false" aria-controls="collapseSearch">
            <i class="fa fa-search"> </i><span
                    class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
        </button>
    </div>

    <div class="collapse" id="collapseSearch">
        <div class="card card-body">
            <form class="form-horizontal fill-up" id="search-filter" autocomplete="off">
                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">ID</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="number" class="form-control onlyNumbers" min="0" name="id" id="" value="">
                                <div class="form-error" id="form-error-id"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.transport_application.application_number.label')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control"  name="application_number" id="" value="">
                                <div class="form-error" id="form-error-application_number"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.transport_application.vehicle_reg_number.label')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control"  name="vehicle_reg_number" id="" value="">
                                <div class="form-error" id="form-error-vehicle_reg_number"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.transport_application.permit_type.label')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <select class="select2" name="ref_transport_permit_type">
                                    <option></option>
                                    @foreach($refTransportPermitTypes as $permitTypes)
                                        <option value="{{$permitTypes->id}}">{{'('.$permitTypes->short_name.')'}} {{$permitTypes->name}}</option>
                                    @endforeach
                                </select>
                                <div class="form-error" id="form-error-permit_type"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.transport_application.current_status.label')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <select class="select2" name="current_status">
                                    <option></option>
                                    @foreach(\App\Models\TransportApplication\TransportApplication::TRANSPORT_APPLICATION_STATUSES as $applicationStatus)
                                        <option value="{{$applicationStatus}}">{{trans('swis.transport_application.current_status.'.$applicationStatus.'.status')}}</option>
                                    @endforeach
                                </select>
                                <div class="form-error" id="form-error-current_status"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8 col-lg-offset-3 text-right">
                        <div class="row">
                            <div class="col-lg-10 col-md-8">
                                <button type="submit" class="btn btn-primary mr-10">{{trans('core.base.label.search')}}</button>
                                <button type="button" class="btn btn-danger resetButton" data-submit="true">{{trans('core.base.label.reset')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="data-table">
        <div class="clearfix">
            <table class="table table-striped table-bordered table-hover dataTables-example" id="list-data">
                <thead>
                <tr>
                    <th data-col="actions" data-delete="0" class="th-actions">{{trans('core.base.label.actions')}}</th>
                    <th data-col="id">ID</th>
                    <th data-col="permit_type">{{trans('swis.transport_application.permit_type.label')}}</th>
                    <th data-col="created_user_name">{{trans('swis.transport_application.inspector_name.label')}}</th>
                    <th data-col="application_number">{{trans('swis.transport_application.application_number.label')}}</th>
                    <th data-col="vehicle_reg_number">{{trans('swis.transport_application.vehicle_reg_number.label')}}</th>
                    <th data-col="trailer_reg_number">{{trans('swis.transport_application.trailer_reg_number.label')}}</th>
                    <th data-col="current_status">{{trans('swis.transport_application.current_status.label')}}</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/transport-application/main.js')}}"></script>
@endsection