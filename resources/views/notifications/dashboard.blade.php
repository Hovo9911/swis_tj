<h2>
    <span class="notification__title ver-top-box">{{trans('swis.dashboard.notifications')}}</span>
    @if($unreadNotificationsCount > 0)
        <span class="notification__count label label-warning-light float-right notifications-count-text ver-top-box"> {{trans('swis.dashboard.notifications.unread_messages')}} <span
                    class="notifications-count">{{$unreadNotificationsCount}}</span></span>
        <button type="button" class="btn btn-primary btn-xs markAsReadAllUnreadNotifications" title="{{trans('swis.notifications.mark_as_read')}}"><i class="fa fa-check"></i></button>
    @endif
</h2>

<div class="ibox-content dashboard-ibox-content">
    <div>
        <div class="feed-activity-list">
            @if(count($notifications) > 0)
                @foreach($notifications as $notification)
                    @if(!is_null($notification->in_app_notification))
                    <div class="feed-element">
                        <div class="media-body">
                            <div class="@if($notification->read == \App\Models\Notifications\Notifications::NOTIFICATION_STATUS_UNREAD) disable @endif">
                                {!! $notification->in_app_notification !!}
                                <br>
                                <small class="text-muted">{{$notification->created_at}}</small>
                                @if($notification->read == \App\Models\Notifications\Notifications::NOTIFICATION_STATUS_UNREAD)
                                <div class="actions notification__actions">
                                    <button class="notification__unread-button btn btn-xs unread-button read_action_{{$notification->id}}"
                                            data-notification_id="{{$notification->id}}"
                                            {{--onClick="notificationUnreadButtonClick(this.id)"--}}
                                    >
                                        <i class="fa fa-bookmark"></i> {{trans('swis.dashboard.mark_as_read')}}
                                    </button>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
            @else
                <div class="media-body text-center">
                    {{trans('swis.dashboard.notifications.empty_notifications')}}
                </div>
            @endif
        </div>

        @if(count($notifications) > 10)
            <a href="{{urlWithLng('notifications')}}" class="btn btn-primary btn-block m-t">{{trans('swis.dashboard.notifications.show_more')}}</a>
        @endif
    </div>
</div>
