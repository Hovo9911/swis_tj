@extends('layouts.app')
<?php
$jsTrans->addTrans([
    'swis.dashboard.mark_as_read'
]);
?>
@section('title'){{trans('swis.notifications.title')}}@stop

@section('contentTitle')
    {{trans('swis.notifications.title')}}
@stop


@section('content')
    <div class="data-table">
        <div class="clearfix">
            <table class="table table-striped table-bordered table-hover dataTables-example" id="list-data">
                <thead>
                    <tr>
                        <th data-col="id">ID</th>
                        <th data-ordering="0" data-col="in_app_notification">{{trans('swis.notifications.label.notification')}}</th>
                        <th data-col="created_at">{{trans('core.base.created_at')}}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('js/notifications/main.js')}}"></script>
@endsection





















