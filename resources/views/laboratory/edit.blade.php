@extends('layouts.app')

@section('title'){{trans('swis.laboratory.create.title')}}@stop

@section('contentTitle')
    {{trans('swis.laboratory.create.title')}}
@stop

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('content')

    <?php
    switch ($saveMode) {
        case 'add':
            $url = 'laboratory';
            break;
        default:
            $url = $saveMode != 'view' ? 'laboratory/update/' . $laboratory->id : '';

            $mls = $laboratory->ml()->notDeleted()->base(['lng_id', 'name', 'head_name', 'description', 'show_status'])->keyBy('lng_id');
            break;
    }

    $languages = activeLanguages();
    ?>

    <form class="form-horizontal fill-up " id="form-data" action="{{urlWithLng($url)}}" autocomplete="off">

        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-general"> {{trans('core.base.tab.general')}}</a></li>
                @foreach($languages as $language)
                    <li><a data-toggle="tab" href="#tab-{{$language->code}}">{{ trans("core.base.language.".strtolower($language->name)) }}</a> </li>
                @endforeach
                <li><a data-toggle="tab" href="#tab-sphere"> {{trans('swis.laboratory.sphere.tab')}}</a></li>
            </ul>
            <div class="tab-content">
                <div id="tab-general" class="tab-pane active">
                    <div class="panel-body">
                        <div class="form-group required"><label class="col-lg-2 control-label">{{trans('swis.agency')}}</label>
                            @if(count($agencies) > 0)
                                <div class="col-lg-10">
                                    @if(count($agencies) == 1)
                                        <input value="{{$agencies[0]->name}}" disabled class="form-control">

                                        <input type="hidden" name="" id="agencyId" value="{{$agencies[0]->tax_id}}" />
                                    @else
                                        <select class="form-control select2 agency-list"
                                                name="company_tax_id" id="agencyId">
                                            @if(empty($laboratory) && count($agencies) > 1)
                                                <option selected disabled
                                                        value="">{{trans('swis.select_agency')}}</option>
                                            @endif
                                            @foreach($agencies as $agency)
                                                <option {{(!empty($laboratory) && $agency->tax_id == $laboratory->company_tax_id) ? 'selected' : ''}} value="{{$agency->tax_id}}">{{$agency->name}}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                    <div class="form-error" id="form-error-company_tax_id"></div>
                                </div>
                            @else
                                <i class="no-more">{{trans('swis.agency.no_more_agencies.message')}}</i>
                            @endif
                        </div>

                        <div class="form-group required">
                            <label class="col-lg-2 control-label">{{trans('swis.tax_id')}}</label>
                            <div class="col-lg-10">
                                <select class="select2 form-control laboratories-list {{ Auth::user()->isSwAdmin() ? 'readonly': '' }}" name="tax_id" id="taxID">
                                    <option value="">{{trans('core.base.label.select')}}</option>
                                    @foreach($laboratories as $item)
                                        <option {{ (!empty($laboratory) &&  $item->tax_id == $laboratory->tax_id) ? 'selected' : '' }} value="{{$item->tax_id}}">{{ "({$item->tax_id}) {$item->name}"}}</option>
                                    @endforeach
                                </select>
                                <div class="form-error" id="form-error-tax_id"></div>
                            </div>
                        </div>

                        <div class="form-group required"><label
                                    class="col-lg-2 control-label">{{trans('swis.laboratory_code')}}</label>
                            <div class="col-lg-10">
                                <input readonly value="{{$laboratory->laboratory_code or ''}}" name="laboratory_code" type="text"
                                       placeholder="" class="form-control">
                                <div class="form-error" id="form-error-laboratory_code"></div>
                            </div>
                        </div>

                        <div class="form-group required"><label
                                    class="col-lg-2 control-label">{{trans('swis.laboratory_certification_number')}}</label>
                            <div class="col-lg-10">
                                <input readonly value="{{$laboratory->certificate_number or ''}}" name="certificate_number"
                                       type="text"
                                       placeholder="" class="form-control">
                                <div class="form-error" id="form-error-certificate_number"></div>
                            </div>
                        </div>
                        <br />
                        <div class="form-group required"><label
                                    class="col-lg-2 control-label">{{trans('swis.laboratory_certification_end_date')}}</label>
                            <div class="col-lg-10">
                                <div class='input-group datetime'>
                                             <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                             </span>
                                    <input readonly type="text" placeholder="{{setDatePlaceholder(true)}}"
                                           value="{{$laboratory->certification_period_validity ?? ''}}"
                                           name="certification_period_validity" class="form-control">
                                </div>
                                <div class="form-error" id="form-error-certification_period_validity"></div>
                            </div>
                        </div>

                        <div class="form-group required"><label
                                    class="col-lg-2 control-label">{{trans('swis.laboratory.legal_entity_name')}}</label>
                            <div class="col-lg-10">
                                <input name="legal_entity_name"
                                       value="{{$laboratory->legal_entity_name or ''}}"
                                       type="text"
                                       readonly
                                       id="legalEntityName"
                                       placeholder="" class="form-control">
                                <div class="form-error" id="form-error-legal_entity_name"></div>
                            </div>
                        </div>

                        <div class="form-group"><label
                                    class="col-lg-2 control-label">{{trans('core.base.label.address')}}</label>
                            <div class="col-lg-10">
                                <input readonly value="{{$laboratory->address or ''}}" id="address" name="address"
                                       type="text" placeholder=""
                                       class="form-control">
                                <div class="form-error" id="form-error-address"></div>
                            </div>
                        </div>
                        <div class="form-group row required">
                            <label class="col-lg-2 control-label">{{trans('core.base.label.phone')}}</label>
                            <div class="col-lg-10">
                                <div class="input-group">
                                    <span class="input-group-addon">+</span>
                                    <input readonly type="text"
                                           class="form-control onlyNumbers"
                                           name="phone_number"
                                           value="{{$laboratory->phone_number or ''}}"
                                           autocomplete="off"
                                           required>
                                </div>
                                <div class="form-error" id="form-error-phone_number"></div>
                            </div>
                        </div>
                        <div class="form-group required"><label
                                    class="col-lg-2 control-label">{{trans('core.base.label.email')}}</label>
                            <div class="col-lg-10">
                                <input readonly value="{{$laboratory->email or ''}}" name="email" type="text" placeholder=""
                                       class="form-control">
                                <div class="form-error" id="form-error-email"></div>
                            </div>
                        </div>
                        <div class="form-group"><label
                                    class="col-lg-2 control-label">{{trans('core.base.label.url')}}</label>
                            <div class="col-lg-10">
                                <input readonly value="{{$laboratory->website_url or ''}}" name="website_url" type="text"
                                       placeholder="" class="form-control">
                                <div class="form-error" id="form-error-website_url"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-2">{{trans('core.base.label.status')}}</label>
                            <div class="col-lg-10 general-status">
                                <select name="show_status" class="form-show-status">
                                    <option value="{{App\Models\Laboratory\Laboratory::STATUS_ACTIVE}}"
                                            {{(!empty($laboratory) && $laboratory->show_status ==
                                        App\Models\Laboratory\Laboratory::STATUS_ACTIVE) || $saveMode == 'add' ? '
                                        selected="selected"' : ''}}>{{trans('core.base.label.status.active')}}</option>
                                    <option value="{{App\Models\Laboratory\Laboratory::STATUS_INACTIVE}}"
                                            {{!empty($laboratory) && $laboratory->show_status ==
                                        App\Models\Laboratory\Laboratory::STATUS_INACTIVE ? ' selected="selected"':
                                        ''}}>{{trans('core.base.label.status.inactive')}}</option>
                                </select>
                                <div class="form-error" id="form-error-show_status"></div>
                            </div>
                        </div>
                    </div>
                </div>
                @foreach($languages as $language)
                    <?php $lngId = $language->id ?>
                    <div id="tab-{{$language->code}}" class="tab-pane">
                        <div class="panel-body">
                            <div class="form-group required"><label
                                        class="col-lg-2 control-label">{{trans('core.base.label.name')}}</label>
                                <div class="col-lg-10">
                                    <input value="{{isset($mls[$lngId]) ? $mls[$lngId]->name : ''}}"
                                           name="ml[{{$language->id}}][name]" type="text" placeholder=""
                                           class="form-control">
                                    <div class="form-error" id="form-error-ml-{{$language->id}}-name"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label">{{trans('swis.laboratory_head_name')}}</label>
                                <div class="col-lg-10">
                                    <input name="ml[{{$language->id}}][head_name]" type="text"
                                           value="{{isset($mls[$lngId]) ? $mls[$lngId]->head_name : ''}}"
                                           placeholder="" class="form-control">
                                    <div class="form-error"
                                         id="form-error-ml-{{$language->id}}-laboratory_head_name"></div>
                                </div>
                            </div>

                            <div class="form-group"><label
                                        class="col-lg-2 control-label">{{trans('core.base.description')}}</label>
                                <div class="col-lg-10">
                                    <textarea rows="6" name="ml[{{$language->id}}][description]"
                                              class="form-control">{{isset($mls[$lngId]) ? $mls[$lngId]->description : ''}}</textarea>
                                    <div class="form-error" id="form-error-ml-{{$language->id}}-description"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                @endforeach
                <div id="tab-sphere" class="tab-pane">
                    <div class="panel-body">
                        <div class="collapse in" id="laboratoryTable">

                            <div class="row">
                                <button type="button" class="btn btn-primary visible-buttons pull-right addNewIndicators">{{ trans('swis.laboratory.new_data_expertise') }}</button>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="table-responsive mainIndicatorsTable">
                                    @include('laboratory.indicators-table')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" value="{{$laboratory->id or 0}}" name="id" class="mc-form-key"/>

        {!! csrf_field() !!}
    </form>

    <div class="modal fade" id="addIndicatorsModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal__title fb">{{trans('swis.laboratory.new_indicator.modal.title')}}</h4>
                    <button type="button" class="close modal__close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <form action="" id="searchSphereForm">

                        <div class="row">
                            <div class="col-md-4">
                                <label>{{ trans('swis.laboratory.new_indicator.modal.sphere.label') }}</label>

                                <select class="select2" name="sphere">
                                    <option value="">{{trans('core.base.label.select')}}</option>
                                    @foreach($spheres as $sphere)
                                        <option value="{{$sphere->code}}">{{ "({$sphere->code}) {$sphere->name}"}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-4">
                                <label>{{ trans('swis.laboratory.new_indicator.modal.hs_start.label') }}</label>
                                <input type="number" max="10" class="form-control" name="hs_code">
                            </div>

                            <div class="col-md-4">
                                <label>{{ trans('swis.laboratory.new_indicator.modal.indicator.label') }}</label>
                                <input type="text" class="form-control" name="indicator">
                            </div>
                        </div>

                        <br/>

                        <button class="btn btn-primary visible-buttons pull-right searchExpertise">{{ trans('swis.laboratory.new_indicator.modal.search.button') }}</button>

                        <br /><hr />


                        <div class="row">
                            <div class="alert alert-danger" role="alert" id="chooseLabAlert" style="display: none;"> {{ trans('swis.laboratory.new_indicator.modal.validation.lab') }} </div>
                            <div class="table-responsive">
                                <div class="indicatorsTableBlock">

                                </div>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default btn-refresh-cancel" data-dismiss="modal">{{trans('core.base.button.refresh.close')}}</button>
                    <button class="btn btn-success addIndicators" disabled>{{trans('swis.laboratory.new_indicator.modal.save.button')}}</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script src="{{asset('js/laboratory/main.js')}}"></script>
@endsection