<table class="table table-bordered">

    <thead>
        <td>{{trans("swis.laboratory.new_indicator.modal.table.sphere")}}</td>
        <td>{{trans("swis.laboratory.new_indicator.modal.table.hs_code")}}</td>
        <td>{{trans("swis.laboratory.new_indicator.modal.table.indicator")}}</td>
        <td>{{trans("swis.laboratory.new_indicator.modal.table.price")}}</td>
        <td>{{trans("swis.laboratory.new_indicator.modal.table.duration")}}</td>
        <td>{{trans("swis.laboratory.new_indicator.modal.table.actions")}}</td>
    </thead>

    <tbody>
    @forelse($indicators as $indicator)
        <tr>
            <td>{{ $indicator->sphere }}</td>
            <td>{{ $indicator->hs_code }}</td>
            <td>{{ "({$indicator->indicator_code}) {$indicator->indicator}" }}</td>
            <td>
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="number" name="indicators[{{$indicator->id}}][price]" value="{{ $indicator->price ?? '' }}" class="form-control">
                        <div class="form-error" id="form-error-indicators-{{$indicator->id}}-price"></div>
                    </div>
                </div>
            </td>
            <td>
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="number" name="indicators[{{$indicator->id}}][duration]" value="{{ $indicator->duration ?? '' }}" class="form-control">
                        <div class="form-error" id="form-error-indicators-{{$indicator->id}}-duration"></div>
                    </div>
                </div>
            </td>
            <td align='center'>
                <button class='btn btn-danger btn-circle remove-indicators-btn' data-id='{{$indicator->id}}' type='button' title='{{ trans("swis.laboratory.new_indicator.modal.table.delete-title") }}'>
                    <i class='fa fa-trash'></i>
                </button>
            </td>
        </tr>
    @empty
        <tr>
            <td colspan="6" align="center">{{ trans("swis.laboratory.new_indicator.modal.table.not_found") }}</td>
        </tr>
    @endforelse

    </tbody>

</table>
