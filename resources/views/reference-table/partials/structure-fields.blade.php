@foreach($fields as $field)
    <div>
        <label data-toggle="tooltip" title="{{$field->field_name}}"><input {{(isset($structure) && !empty($structure->parameters['fields'])) && in_array($field->id,$structure->parameters['fields'])? 'checked' : ''}} type="checkbox" class="structureField" data-field-name="{{$field->field_name}}"  name="structure[{{$i}}][fields][]" value="{{$field->id}}">{{$field->field_name}}</label>
    </div>
@endforeach
<div class="form-error" id="form-error-structure-{{$i}}-fields"></div>
