@extends('layouts.app')

@section('title'){{trans('swis.reference_table')}}@stop

@section('contentTitle') {{trans('swis.reference_table')}} @stop

@section('form-buttons-top')
    {!! renderAddButton() !!}
@stop

@section('content')
    <div class="clearfix search__out">
        <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse" data-target="#collapseSearch" aria-expanded="false" aria-controls="collapseSearch">
            <i class="fa fa-search"> </i><span class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
        </button>
    </div>
    <div class="collapse" id="collapseSearch">
        <div class="card card-body">
            <form class="form-horizontal fill-up" id="search-filter" autocomplete="off">
                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">ID</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="number" class="form-control onlyNumbers" min="0" name="id" id="" value="">
                                <div class="form-error" id="form-error-id"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.reference_table.label.code')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="name" id="" value="">
                                <div class="form-error" id="form-error-name"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.technical_name')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="table_name" id="" value="">
                                <div class="form-error" id="form-error-table_name"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $dates = ['start_date']?>

                @foreach($dates as $date)
                    <div class="form-group">
                        <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.reference_table.'.$date.'.label_date')}}</label>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-4 col-lg-5 field__two-col">
                                    <div class='input-group date'>
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        <input  name="{{$date}}_start" autocomplete="off" type="text"  placeholder="{{setDatePlaceholder()}}" class="form-control "/>
                                    </div>
                                </div>
                                {{--<div class="guion">-</div>--}}
                                <div class="col-md-4 col-lg-5 field__two-col">
                                    <div class='input-group date'>
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        <input  name="{{$date}}_end" autocomplete="off" type="text"  placeholder="{{setDatePlaceholder()}}" class="form-control "/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.status')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <select class="form-control select2 default-search" name="show_status">
                                    <option selected value="{{App\Models\ReferenceTable\ReferenceTable::STATUS_ACTIVE}}">{{trans('core.base.label.status.active')}}</option>
                                    <option value="{{App\Models\ReferenceTable\ReferenceTable::STATUS_INACTIVE}}">{{trans('core.base.label.status.inactive')}}</option>
                                </select>
                                <div class="form-error" id="form-error-show_status"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8 col-lg-offset-3 text-right">
                        <div class="row">
                            <div class="col-lg-10 col-md-8">
                                <button type="submit" class="btn btn-primary mr-10">{{trans('core.base.label.search')}}</button>
                                <button type="button" class="btn btn-danger resetButton" data-submit="true">{{trans('core.base.label.reset')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="data-table">
        <div class="clearfix">
            <table class="table table-striped table-bordered table-hover dataTables-example" id="list-data">
                <thead>
                    <tr>
                        <th data-col="actions" style="min-width: 70px;" data-delete="0" class="th-actions">{{trans('core.base.label.actions')}}</th>
                        <th data-col="id">ID</th>
                        <th data-col="name">{{trans('core.base.label.name')}}</th>
                        <th data-col="available_at">{{trans('core.base.label.start')}}</th>
                        <th data-col="show_status">{{trans('core.base.label.status')}}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/reference-table/main.js')}}"></script>
@endsection