<?php
$title = isset($referenceTable) ?  $referenceTable->current()->name : trans('swis.reference_table_create');
?>

@extends('layouts.app')

@section('title'){{$title}}@stop

@section('contentTitle'){{$title}}@stop

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('topScripts')
    <script>
        var referenceTableStatus = '{{$referenceTable->show_status or ''}}';
        var clone = '{{$clone or '0'}}';
    </script>
@endsection

@section('content')
    <?php

    switch ($saveMode) {
        case 'add':
            $url = 'reference-table';
            break;
        default:
            $url = $saveMode != 'view' ? 'reference-table/update/' . $referenceTable->id: '';

            $mls = $referenceTable->ml()->notDeleted()->base(['lng_id', 'name', 'show_status'])->keyBy('lng_id');
            break;
    }

    if ($clone) {
        $mls = $referenceTable->ml()->notDeleted()->base(['lng_id', 'name', 'show_status'])->keyBy('lng_id');
    }

    $languages = activeLanguages();

    ?>
    <form class="form-horizontal fill-up referenceFormData" id="form-data" action="{{urlWithLng($url)}}" autocomplete="off">

        <div class="tabs-container">

            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-general"> {{trans('core.base.tab.general')}}</a></li>
                <li><a data-toggle="tab" href="#tab-structure">{{trans('swis.reference_table.structure')}}</a></li>
                @foreach($languages as $language)
                    <li><a data-toggle="tab" href="#tab-{{$language->code}}">{{ trans("core.base.language.".strtolower($language->name)) }}</a></li>
                @endforeach
                @if ($saveMode != 'add')
                    <li><a data-toggle="tab" href="#tab-audit">{{trans('swis.reference_table.audit.title')}}</a></li>
                @endif
            </ul>

            <div class="tab-content">
                <div id="tab-general" class="tab-pane active">
                    <div class="panel-body">
                        <div class="form-group required">
                            <label class="col-lg-3 control-label">{{trans('swis.reference_table.table_name')}}</label>
                            <div class="col-lg-8">
                                <input value="{{$referenceTable->table_name or ''}}"
                                       {{(!$clone && $saveMode == 'edit') ? 'readonly' : ''}} name="table_name"
                                       type="text"
                                       placeholder=""
                                       class="form-control">
                                <div class="form-error" id="form-error-table_name"></div>
                            </div>
                        </div>

                        <div class="form-group required"><label class="col-lg-3 control-label">{{trans('swis.reference_table.manually_edit.label')}}</label>
                            <div class="col-lg-8">
                                <label class="radio-inline">
                                    <input type="radio" {{(isset($referenceTable) && $referenceTable->manually_edit) ? 'checked' : ''}} name="manually_edit" value="1">{{trans('swis.reference_table.manually_edit.yes')}}
                                </label>

                                <label class="radio-inline">
                                    <input type="radio" {{(isset($referenceTable) && !$referenceTable->manually_edit) ? 'checked' : ''}} {{($saveMode == 'add') ? 'checked' : ''}} name="manually_edit" value="0">{{trans('swis.reference_table.manually_edit.no')}}
                                </label>
                                <div class="form-error" id="form-error-manually_edit"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">{{trans('swis.reference_table.import_format.label')}}</label>
                            <div class="col-lg-8">
                                <select class="form-control select2 impFormat" multiple {{(isset($referenceTable) && $referenceTable->manually_edit) ? '' : 'disabled'}} name="import_format[]">
                                    @foreach(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_IMPORT_FORMATS as $importFormat)
                                        <option {{(isset($referenceTable) && !empty($referenceTable->import_format) && in_array($importFormat,$referenceTable->import_format)) ? 'selected' : ''}} value="{{$importFormat}}">{{trans('swis.reference_table.import_format.'.$importFormat.'.label')}}</option>
                                    @endforeach
                                </select>
                                <div class="form-error" id="form-error-import_format"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">{{trans('swis.reference_table.api_url.label')}}</label>
                            <div class="col-lg-8">
                                <input value="{{$referenceTable->api_url or ''}}" disabled name="api_url" type="text" placeholder="" class="form-control">
                                <div class="form-error" id="form-error-api_url"></div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-lg-3 control-label">{{trans('core.base.label.start')}}</label>
                            <div class="col-lg-8">
                                <?php
                                $availableAt = currentDate();
                                if (!$clone && isset($referenceTable) && $referenceTable->available_at) {
                                    $availableAt = $referenceTable->available_at;
                                }
                                ?>

                                <div class='input-group date'>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                    <input name="available_at" value="{{formattedDate($availableAt)}}" data-start-date="{{currentDate()}}" autocomplete="off" type="text" placeholder="{{setDatePlaceholder()}}" class="form-control "/>
                                </div>

                                <div class="form-error" id="form-error-available_at"></div>
                            </div>
                        </div>

                        <div class="form-group row" id="agencyFormGroup">

                            <label class="control-label col-md-3 required">{{trans('swis.reference_table.agencies.label')}}</label>
                            <div class="col-md-8">
                                <div class="agencies-list">
                                    <table class="table table-bordered agency-table" style="margin-bottom: 0">
                                        <thead>
                                            <tr>
                                                <th style="width: 10px"><input type="checkbox" id="checkAllAgencies"></th>
                                                <th>{{trans('swis.reference_table.agency_name.label')}}</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($agencies as $agency)
                                            <?php
                                            $agencySelected = false;
                                            $agencySelectedAccess = '';
                                            $agencyShowOnlySelf = '0';
                                            if (isset($referenceTable)) {
                                                $refAgencies = $referenceTable->agencies->keyBy('agency_id')->toArray();

                                                if(isset($refAgencies[$agency->id])){
                                                    $agencySelected = true;
                                                    $agencySelectedAccess = $refAgencies[$agency->id]['access'];
                                                    $agencyShowOnlySelf = $refAgencies[$agency->id]['show_only_self'];
                                                }
                                            }
                                            ?>
                                            <tr>
                                                <td>
                                                    <input {{(isset($referenceTable) && $agencySelected) ? 'checked' : ''}} type="checkbox" name="agencies[{{$agency->id}}][id]" value="{{$agency->id}}"/>
                                                </td>

                                                <td>
                                                    {{'(' .$agency->tax_id .')'}} {{$agency->name}}
                                                </td>

                                                <td>
                                                    <div class="form-group">
                                                        <label class="radio-inline">
                                                            <input {{(isset($referenceTable) && ($agencySelected && $agencySelectedAccess == 'edit')) ? 'checked' : ''}} type="radio" name="agencies[{{$agency->id}}][access]" disabled value="edit">{{trans('swis.reference_table.edit.label')}}
                                                        </label>

                                                        <label class="radio-inline">
                                                            <input {{(isset($referenceTable) && ($agencySelected && $agencySelectedAccess == 'read')) ? 'checked' : ''}} type="radio" name="agencies[{{$agency->id}}][access]" disabled value="read">{{trans('swis.reference_table.read.label')}}
                                                        </label>

                                                        <div class="form-error" id="form-error-agencies-{{$agency->id}}-access"></div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label>
                                                            <input class="show_only_self" {{isset($referenceTable) && $agencyShowOnlySelf ? 'checked' : ''}} name="agencies[{{$agency->id}}][show_only_self]" disabled type="checkbox" value="1">{{trans('swis.role.show_only_self.only_self')}}
                                                        </label>

                                                        <div class="form-error" id="form-error-agencies-{{$agency->id}}-show_only_self"></div>
                                                    </div>
                                                </td>

                                            </tr>
                                        @endforeach
                                        </tbody>

                                    </table>
                                </div>
                            </div>

                            <div class="col-md-8 col-md-offset-3">
                                <div class="form-error" id="form-error-agencies"></div>
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3">{{trans('core.base.label.status')}}</label>
                            <div class="col-lg-9  general-status">
                                <select name="show_status" class="form-show-status">
                                    <option value="{{App\Models\ReferenceTable\ReferenceTable::STATUS_ACTIVE}}"{{(isset($referenceTable) && $referenceTable->show_status == App\Models\ReferenceTable\ReferenceTable::STATUS_ACTIVE) || $saveMode == 'add' ?  ' selected="selected"' : ''}}>{{trans('core.base.label.status.active')}}</option>
                                    <option value="{{App\Models\ReferenceTable\ReferenceTable::STATUS_INACTIVE}}"{{isset($referenceTable) &&  $referenceTable->show_status == App\Models\ReferenceTable\ReferenceTable::STATUS_INACTIVE ? ' selected="selected"': ''}}>{{trans('core.base.label.status.inactive')}}</option>
                                </select>
                                <div class="form-error" id="form-error-show_status"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="tab-structure" class="tab-pane ">
                    <div class="panel-body">

                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>{{trans('swis.reference_table.field_name')}}</th>
                                    <th>{{trans('swis.reference_table.type.label')}}</th>
                                    <th>{{trans('swis.reference_table.parameter.label')}}</th>
                                    <th>{{trans('swis.reference_table.required.label')}}</th>
                                    <th>{{trans('swis.reference_table.search_filter.label')}}</th>
                                    <th>{{trans('swis.reference_table.search_result.label')}}</th>
                                    <th>{{trans('swis.reference_table.sortable.label')}}</th>
                                    <th>{{trans('swis.reference_table.sort_order.label')}}</th>
                                    <th>{{trans('swis.reference_table.ml.label')}}</th>
                                    <th>{{trans('core.base.label.status')}}</th>
                                    <th></th>
                                </tr>
                            </thead>

                            <tbody id="reference-structure">

                            {{--  Default Structure  --}}
                            @foreach($defaultDataStructure as $structure)
                                <tr>
                                    <td>
                                        <input type="text" class="form-control" title="{{$structure['field_name']}}" data-toggle="tooltip" disabled value="{{$structure['field_name']}}">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" disabled value="{{$structure['type']}}">
                                    </td>
                                    <td>
                                        @if($structure['type'] == \App\Models\ReferenceTable\ReferenceTableStructure::COLUMN_TYPE_TEXT)
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <div class="input-group-addon">{{trans('swis.reference_table.min.label')}}</div>
                                                    <input type="number" disabled value="{{$structure['min']}}" style="max-width:70px" class="form-control" placeholder="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="input-group ">
                                                    <div class="input-group-addon">{{trans('swis.reference_table.max.label')}}</div>
                                                    <input type="number" disabled value="{{$structure['max']}}" style="max-width:70px" class="form-control" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                        @endif

                                        @if($structure['type'] == \App\Models\ReferenceTable\ReferenceTableStructure::COLUMN_TYPE_DATE)
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">{{trans('swis.reference_table.start.label')}}</div>
                                                <input disabled type="date" class="form-control" placeholder="">
                                            </div>

                                        </div>
                                        <br/>

                                        <div class="form-group">
                                            <div class="input-group ">
                                                <div class="input-group-addon">{{trans('swis.reference_table.end.label')}}</div>
                                                <input disabled type="date" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        @endif
                                    </td>
                                    <td>
                                        <input type="checkbox" disabled  {{$structure['required'] ? 'checked' : ''}}>
                                    </td>
                                    <td>
                                        <input type="checkbox" disabled {{$structure['search_filter'] ? 'checked' : ''}}>
                                    </td>
                                    <td>
                                        <input type="checkbox" disabled {{$structure['search_result'] ? 'checked' : ''}}>
                                    </td>
                                    <td>
                                        <input type="checkbox" disabled {{$structure['sortable'] ? 'checked' : ''}}>
                                    </td>
                                    <td style="width: 90px;">
                                        <input type="number" class="form-control sort_order" disabled value="{{$structure['sort_order']}}">
                                    </td>
                                    <td>
                                        <input type="checkbox" disabled {{$structure['has_ml'] ? 'checked' : ''}}>
                                    </td>
                                    <td>
                                        <select disabled class="form-control">
                                            <option selected>{{trans('core.base.label.status.active')}}</option>
                                        </select>
                                    </td>
                                    <td></td>
                                </tr>
                            @endforeach

                            {{--  Custom Structure  --}}
                            @if(isset($referenceTable) && $referenceTable->customStructure->count())
                                <?php $i = 5 ?>
                                @foreach($referenceTable->customStructure as $structure)
                                    @include('reference-table.structure-table',['i'=>$i,'structure'=>$structure,'field_type'=>$structure->type])
                                    <?php $i++ ?>
                                @endforeach
                            @endif

                            </tbody>
                        </table>

                        <hr/>
                        <button class="btn-more btn-primary btn" type="button"><i class="fa fa-plus"></i></button>
                        @if($saveMode != 'add')
                            <a href='{{ urlWithLng("/reference-table/{$referenceTable->id}/export") }}' class="btn btn-warning">{{trans('core.base.button.export')}}</a>
                        @endif
                    </div>
                </div>

                @foreach($languages as $language)
                    <?php $lngId = $language->id ?>
                    <div id="tab-{{$language->code}}" class="tab-pane">
                        <div class="panel-body">
                            <div class="form-group required"><label class="col-lg-3 control-label">{{trans('core.base.label.name')}}</label>
                                <div class="col-lg-8">
                                    <input value="{{isset($mls[$lngId]) ? $mls[$lngId]->name : ''}}" name="ml[{{$language->id}}][name]" type="text" placeholder="" class="form-control">
                                    <div class="form-error" id="form-error-ml-{{$language->id}}-name"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

                <div id="tab-audit" class="tab-pane">
                    <div class="panel-body">
                        @include('reference-table.audit')
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" value="{{$referenceTable->id or 0}}" @if(!$clone) name="id" @endif class="mc-form-key"/>

        {!! csrf_field() !!}
    </form>
@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script src="{{asset('js/reference-table/main.js')}}"></script>
@endsection