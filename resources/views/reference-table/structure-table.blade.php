<tr>
    <td>
        <div class="form-group">
            <input type="hidden" name="structure[{{$i}}][id]" value="{{$structure->id or ''}}">
            <input type="hidden" name="structure[{{$i}}][field_name]" value="{{$structure->field_name or ''}}">
            <input type="text" title="{{$structure->field_name or ''}}" data-toggle="tooltip"  title="{{$structure->field_name or ''}}" data-toggle="tooltip" class="form-control ref-row-field-name" value="{{$structure->field_name or ''}}" name="structure[{{$i}}][field_name]" {{isset($referenceTable) ? 'disabled' : ''}}>
            <div class="form-error" id="form-error-structure-{{$i}}-field_name"></div>
        </div>
    </td>
    <td>
        <input type="hidden" name="structure[{{$i}}][type]" value="{{$structure->type or ''}}">
        <select name="structure[{{$i}}][type]" class="form-control fieldType" data-i="{{$i}}" {{isset($referenceTable) ? 'disabled' : ''}}>
            @foreach(\App\Models\ReferenceTable\ReferenceTableStructure::COLUMN_TYPES as $columnType)
                <option {{(isset($structure) && $structure->type == $columnType) ? 'selected' : ''}} value="{{$columnType}}">{{$columnType}}</option>
            @endforeach
        </select>
    </td>
    <td>
        <div class="parameter-content">
            @include('reference-table.structure-parameter')
        </div>
    </td>
    <td>
        <div class="form-group">
            <input type="checkbox" {{(isset($structure) && $structure->required) ? 'checked' : ''}} name="structure[{{$i}}][required]" value="1">
            <div class="form-error" id="form-error-structure-{{$i}}-required"></div>
        </div>
    </td>
    <td>
        <div class="form-group">
            <input type="checkbox" {{(isset($structure) && $structure->search_filter) ? 'checked' : ''}} name="structure[{{$i}}][search_filter]" value="1">
            <div class="form-error" id="form-error-structure-{{$i}}-search_filter"></div>
        </div>
    </td>
    <td>
        <div class="form-group">
            <input type="checkbox" class="searchRes" {{(isset($structure) && $structure->search_result) ? 'checked' : ''}} name="structure[{{$i}}][search_result]" value="1">
            <div class="form-error" id="form-error-structure-{{$i}}-search_result"></div>
        </div>
    </td>
    <td>
        <div class="form-group">
            <input type="checkbox" class="sortable" {{(isset($structure) && $structure->sortable) ? 'checked' : ''}}   name="structure[{{$i}}][sortable]" value="1">
            <div class="form-error" id="form-error-structure-{{$i}}-sortable"></div>
        </div>
    </td>
    <td>
        <div class="form-group">
            <input class="form-control sort_order" {{(isset($structure) && $structure->sort_order) ? 'checked' : ''}} value="{{$structure->sort_order or $i}}" type="number" name="structure[{{$i}}][sort_order]">
            <div class="form-error" id="form-error-structure-{{$i}}-sort_order"></div>
        </div>
    </td>
    <td>
        @if(isset($mode) && $mode == 'edit')
            <div class="form-group">
                <input type="checkbox" class="mlInput" {{(isset($structure) && $structure->has_ml) ? 'checked' : ''}} name="structure[{{$i}}][has_ml]" value="1">
                <div class="form-error" id="form-error-structure-{{$i}}-has_ml"></div>
            </div>
        @else
           {{-- @if(isset($structure) && $structure->has_ml)
                <input type="checkbox" checked class="readonly" name="structure[{{$i}}][has_ml]" value="1">
            @else
                <input type="checkbox" disabled="">
            @endif--}}
            <input type="checkbox" disabled="" {{isset($structure) && $structure->has_ml ? 'checked' : ''}}>
        @endif
    </td>
    <td>
        <select class="form-control" name="structure[{{$i}}][show_status]" data-name="role_status">
            <option {{(isset($structure) && $structure->show_status == \App\Models\ReferenceTable\ReferenceTable::STATUS_ACTIVE) ? 'selected' : ''}} value="{{\App\Models\ReferenceTable\ReferenceTable::STATUS_ACTIVE}}">{{trans('core.base.label.status.active')}}</option>
            <option {{(isset($structure) && $structure->show_status == \App\Models\ReferenceTable\ReferenceTable::STATUS_INACTIVE) ? 'selected' : ''}} value="{{\App\Models\ReferenceTable\ReferenceTable::STATUS_INACTIVE}}">{{trans('core.base.label.status.inactive')}}</option>
        </select>
    </td>
    <td>
        @if((isset($mode) && $mode == 'edit') || (isset($clone) && $clone))
            <button class="btn-remove btn-danger" title="{{trans('swis.reference_table.minus.label')}}"><i class="fa fa-minus"></i></button>
        @endif
    </td>
</tr>