<?php
use App\Models\ReferenceTable\ReferenceTableStructure;
?>

@switch($field_type)

    @case(ReferenceTableStructure::COLUMN_TYPE_DATE)

        <div class="form-group">
            <div class='input-group date'>
                <span class="input-group-addon">{{trans('swis.reference_table.start.label')}}</span>
                <input value="{{isset($structure) ? formattedDate($structure->parameters['start']) : currentDate()}}"
                        name="structure[{{$i}}][start]"  autocomplete="off" type="text"  placeholder="{{setDatePlaceholder()}}" class="form-control "/>
            </div>
            <div class="form-error" id="form-error-structure-{{$i}}-start"></div>
        </div>

        <br/>

        <div class="form-group">
            <div class='input-group date'>
                <span class="input-group-addon">{{trans('swis.reference_table.end.label')}}</span>
                <input value="{{isset($structure) ? formattedDate($structure->parameters['end']) : currentDate()}}"
                        name="structure[{{$i}}][end]"  autocomplete="off" type="text"  placeholder="{{setDatePlaceholder()}}" class="form-control "/>
            </div>
            <div class="form-error" id="form-error-structure-{{$i}}-end"></div>
        </div>

    @break

    @case(ReferenceTableStructure::COLUMN_TYPE_DATETIME)

        <div class="form-group">
            <div class='input-group datetime'><span class="input-group-addon">{{trans('swis.reference_table.start.label')}}</span>
                <input type="text" placeholder="{{setDatePlaceholder(true)}}"
                       value="{{isset($structure) ? formattedDate($structure->parameters['start'],true) : currentDateTimeFront(true)}}"
                       name="structure[{{$i}}][start]"  min="0" class="form-control">
            </div>
            <div class="form-error" id="form-error-structure-{{$i}}-start"></div>
        </div>

        <br/>

        <div class="form-group">
            <div class='input-group datetime'><span class="input-group-addon">{{trans('swis.reference_table.end.label')}}</span>
                <input type="text" placeholder="{{setDatePlaceholder(true)}}"
                       value="{{isset($structure) ? formattedDate($structure->parameters['end'],true) : currentDateTimeFront(true)}}"
                       name="structure[{{$i}}][end]"  min="0" class="form-control">
            </div>
            <div class="form-error" id="form-error-structure-{{$i}}-end"></div>
        </div>

    @break

    @case(ReferenceTableStructure::COLUMN_TYPE_AUTOCOMPLETE)

        @if(isset($referenceTables) && $referenceTables->count())
            <div class="form-group">
                <select class="form-control autoCompleteReference select2" name="structure[{{$i}}][source]" data-i="{{$i}}">
                    <option></option>
                    <?php $refId = null ?>
                    @foreach($referenceTables as $referenceTable)
                        @if(isset($structure) && $structure->parameters['source'] == $referenceTable->id)
                            <?php $refId = $referenceTable->id?>
                        @endif
                        <option {{isset($structure) && $structure->parameters['source'] == $referenceTable->id ? 'selected' : ''}} value="{{$referenceTable->id}}">{{$referenceTable->name}}</option>
                    @endforeach
                </select>
                <div class="form-error" id="form-error-structure-{{$i}}-source"></div>
            </div>
            <div class="structure-fields">
                @if($refId)
                    <?php $refStructureFields = \App\Models\ReferenceTable\ReferenceTable::structureField($refId)  ?>
                    @if($refStructureFields->count())
                        @include('reference-table.partials.structure-fields',['fields'=>$refStructureFields,'i'=>$i])
                    @endif
                @endif
                <div class="form-error" id="form-error-structure-{{$i}}-fields"></div>
            </div>
        @endif

    @break

    @case(ReferenceTableStructure::COLUMN_TYPE_SELECT)

        @if(isset($referenceTables) && $referenceTables->count())
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <select class="form-control autoCompleteReference select2" name="structure[{{$i}}][source]" data-i="{{$i}}">
                            <option></option>
                            @foreach($referenceTables as $referenceTable)
                                <option {{isset($structure) && $structure->type == $referenceTable->id ? 'selected' : ''}} {{isset($structure) && $structure->parameters['source'] == $referenceTable->id ? 'selected' : ''}} value="{{$referenceTable->id}}">{{$referenceTable->current()->name}}</option>
                            @endforeach
                        </select>
                        <div class="form-error" id="form-error-structure-{{$i}}-source"></div>
                    </div>
                </div>
            </div>
        @endif

    @break

    @case(ReferenceTableStructure::COLUMN_TYPE_DECIMAL)

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>{{trans('swis.reference_table.left.label')}}</label>
                    <input type="number" value="{{isset($structure) ? $structure->parameters['left'] : ''}}"
                           class="form-control" name="structure[{{$i}}][left]" min="1"
                           max="20"
                           placeholder="">
                    <div class="form-error" id="form-error-structure-{{$i}}-left"></div>
                </div>
            </div>
            <div class="col-md-12">
                <br />
                <div class="form-group">
                    <label>{{trans('swis.reference_table.right.label')}}</label>
                    <input type="number"  class="form-control"
                           value="{{isset($structure) ? $structure->parameters['right'] : ''}}"
                           name="structure[{{$i}}][right]" min="0" max="8" placeholder="">
                    <div class="form-error" id="form-error-structure-{{$i}}-right"></div>
                </div>
            </div>
        </div>

    @break

    @default

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">{{trans('swis.reference_table.min.label')}}</div>
                        <input type="number" value="{{isset($structure) ? $structure->parameters['min'] : ''}}"
                               style="max-width:80px" class="form-control" name="structure[{{$i}}][min]" min="0"
                               placeholder="">
                    </div>
                    <div class="form-error" id="form-error-structure-{{$i}}-min"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="input-group ">
                        <div class="input-group-addon">{{trans('swis.reference_table.max.label')}}</div>
                        <input type="number" style="max-width:80px" class="form-control"
                               value="{{isset($structure) ? $structure->parameters['max'] : ''}}"
                               name="structure[{{$i}}][max]" min="0" placeholder="">
                    </div>
                    <div class="form-error" id="form-error-structure-{{$i}}-max"></div>
                </div>
            </div>
        </div>

@endswitch

