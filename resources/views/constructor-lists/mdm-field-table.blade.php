<table class="table table-striped table-bordered table-hover dataTables-example">
    <thead>
    <tr>
        <th>{{ trans('swis.constructor_data_set.field_id') }} </th>
        <th>{{ trans('swis.constructor_data_set.element_id') }} </th>
        <th>{{ trans('swis.constructor_data_set.saf_name') }}</th>
        <th>{{ trans('swis.constructor_data_set.saf_description') }}</th>
        <th>{{ trans('core.base.label.group') }}</th>
        <th>{{ trans('swis.constructor_data_set.field_label') }}</th>
        <th>{{ trans('swis.constructor_data_set.tool_tip') }}</th>
        <th class="text-center"><input type="checkbox"
                                       class="check-all-fields check-all-mdm-fields" {{ ($checked['isCheckedAllMdm']) ? 'checked' : '' }}>
        </th>
    </tr>
    </thead>
    <tbody id="tbody">
    @if ($mdmData && count($mdmData) > 0)
        @foreach ($mdmData as $item)
            <?php
            $group = explode('/', $item['group']);
            $groupString = '';
            if (count($group) > 0) {
                foreach ($group as $key => $value) {
                    $groupString .= ($key == 0) ? $value : '/' . trans(trim($value));
                }
            }
            ?>
            <tr>
                <td>{{$item['field_id']}}</td>
                <td>{{$item['mdm_field_id']}}</td>
                <td>{{$item['name']}}</td>
                <td>{{$item['description']}}</td>
                <td>{{$groupString}}</td>
                <td>{{$item['label']}}</td>
                <td>{{$item['tooltip']}}</td>
                <td align='center'><input type='checkbox' class="mdm_field_checkbox"
                                          name='paramsMDM[{{$item['field_id']}}]' {{ (!empty($constructorList) && in_array($item['field_id'], $checked['mdm'])) ? 'checked' : '' }}>
                </td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="8" align="center">No data available yet</td>
        </tr>
    @endif
    </tbody>
</table>