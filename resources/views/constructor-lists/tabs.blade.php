
<table class="table table-striped table-bordered table-hover dataTables-example">
    <thead>
    <tr>
        <th>{{ trans('swis.constructor_data_set.tab_name') }} </th>
        <th class="text-center"><input type="checkbox" class="check-all-fields check-all-tabs-fields" {{ ($checked['isCheckedAllTabs']) ? 'checked' : '' }}></th>
    </tr>
    </thead>
    <tbody id="tbody">

        @if ($tabs && count($tabs) > 0)
            @foreach($tabs as $tab)
                <tr>
                    <td>{{$tab['name']}}</td>
                    <td align='center'>
                        <input type='checkbox' class="tab_checkbox" name='paramsTabs[{{$tab['key']}}]' {{ (!empty($constructorList) && in_array($tab['key'], $checked['tabs'])) ? 'checked' : '' }}>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="8" align="center">No data available yet</td>
            </tr>
        @endif
    </tbody>
</table>
