@extends('layouts.app')

@section('title'){{trans('swis.constructor_list.list_title')}}@endsection

@section('contentTitle') {{trans('swis.constructor_list.list_title')}} @endsection

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@endsection

@section('content')
    <?php
        switch ($saveMode) {
            case 'add':
                $url = 'constructor-lists';
                break;
            default:
                $url = 'constructor-lists/update/' . $constructorList->id;
                break;
        }

        $nonVisible = \App\Models\ConstructorLists\ConstructorLists::NON_VISIBLE;
        $editable   = \App\Models\ConstructorLists\ConstructorLists::EDITABLE;
        $mandatory  = \App\Models\ConstructorLists\ConstructorLists::MANDATORY;

    ?>
    <form class="form-horizontal fill-up " id="form-data" action="{{urlWithLng($url)}}">
        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-general"> {{trans('swis.constructor_list.tabs.general')}}</a></li>
                <li class="saf-field-tab"><a data-toggle="tab" href="#tab-saf-fields">{{trans('swis.constructor_list.tabs.saf')}}</a></li>
                <li class=""><a data-toggle="tab" href="#tab-mdm-fields">{{trans('swis.constructor_list.tabs.mdm')}}</a></li>
                <li class="single-application-tabs" style="display: {{ (isset($constructorList) && $constructorList->list_type != $mandatory) ? 'block' : 'nano' }}"><a data-toggle="tab" href="#tab-tabs">{{trans('swis.constructor_list.tabs.tabs')}}</a></li>
            </ul>
        </div>

        <div class="tab-content">
            <div id="tab-general" class="tab-pane active">
                <div class="panel-body">

                    <div class="form-group required">
                        <label class="col-lg-2 control-label">{{trans('swis.document.select_document')}}</label>
                        <div class="col-lg-8">
                            <select class="form-control select2" name="document_id" id="document-id">
                                <option></option>
                                @if ($constructorDocuments->count())
                                    @foreach ($constructorDocuments as $constructorDocument)
                                        <option {{(isset($constructorList) && $constructorList->document_id == $constructorDocument->id) || (isset($selectedConstructorDocumentId) && $selectedConstructorDocumentId == $constructorDocument->id) ? 'selected' : ''}} value="{{ $constructorDocument->id }}">{{ $constructorDocument->document_name }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div class="form-error" id="form-error-document_id"></div>
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-lg-2 control-label">{{trans('swis.constructor_list.table.list_type')}}</label>
                        <div class="col-lg-8">
                            <select class="form-control select2" name="list_type" id="list_type" data-url="{{ urlWithLng('constructor-lists/get-saf-fields') }}">
                                <option></option>
                                <option value="{{ $nonVisible }}" {{ (isset($constructorList) && $constructorList->list_type == $nonVisible) ? 'selected' : '' }}>Non Visible</option>
                                <option value="{{ $editable }}" {{ (isset($constructorList) && $constructorList->list_type == $editable) ? 'selected' : '' }}>Editable</option>
                                <option value="{{ $mandatory }}" {{ (isset($constructorList) && $constructorList->list_type == $mandatory) ? 'selected' : '' }}>Mandatory</option>
                            </select>
                            <div class="form-error" id="form-error-list_type"></div>
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-lg-2 control-label">{{trans('core.base.description')}}</label>
                        <div class="col-lg-8">
                            <textarea style="resize: none;" rows="6" name="description" class="form-control">{{ isset($constructorList) ? $constructorList->description : '' }}</textarea>
                            <div class="form-error" id="form-error-description"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">{{trans('core.base.label.status')}}</label>
                        <div class="col-lg-10 general-status">
                            @php
                                $statusActive   = \App\Models\ConstructorDocument\ConstructorDocument::STATUS_ACTIVE;
                                $statusInactive = \App\Models\ConstructorDocument\ConstructorDocument::STATUS_INACTIVE;
                            @endphp
                            <select name="show_status" class="form-show-status">
                                <option value="{{$statusActive}}" {{ (isset($constructorList) && $constructorList->show_status == $statusActive) ? 'selected' : '' }} {{ empty($constructorList) ? 'selected' : '' }}>{{trans('core.base.label.status.active')}}</option>
                                <option value="{{$statusInactive}}" {{ (isset($constructorList) && $constructorList->show_status == $statusInactive) ? 'selected' : '' }}>{{trans('core.base.label.status.inactive')}}</option>
                            </select>
                            <div class="form-error" id="form-error-show_status"></div>
                        </div>
                    </div>

                </div>
            </div>

            {{---------------------  SAF Tab  ----------------------}}
            <div id="tab-saf-fields" class="tab-pane">
                <div class="row loading-in-edit-mode hide text-center m-t">
                    <i class="fas fa-sync fa-spin" style="font-size: 35px;"></i>
                </div>

                <div class="tab-saf-fields-panel panel-body">
                    @include('constructor-lists.saf-field-table')
                </div>
            </div>

            {{---------------------  MDM Tab  ----------------------}}
            <div id="tab-mdm-fields" class="tab-pane">
                <div class="panel-body">
                    @include('constructor-lists.mdm-field-table')
                </div>
            </div>


            {{--------------------- Tabs Tab  ----------------------}}
            <div id="tab-tabs" class="tab-pane">
                <div class="panel-body">
                    @include('constructor-lists.tabs')
                </div>
            </div>
        </div>
    </form>

@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('topScripts')
    <script>
        var listId          = "{{ isset($constructorList->id) ? $constructorList->id : null }}";
        var mandatoryType   = "{{ $mandatory }}";
        var constructorDocumentId  = "{{$selectedConstructorDocumentId or $constructorList->document_id}}";
    </script>
@endsection

@section('scripts')
    <script src="{{asset('js/constructor-lists/main.js')}}"></script>
@endsection