<table class="table table-striped table-bordered table-hover dataTables-example">
    <thead>
    <tr>
        <th data-col="id">{{ trans('swis.constructor_data_set.saf_field_id') }} </th>
        <th data-col="mdm_id">{{ trans('swis.constructor_data_set.mdm_id') }} </th>
        <th data-col="saf_name">{{ trans('swis.constructor_data_set.saf_name') }}</th>
        <th data-col="saf_description">{{ trans('swis.constructor_data_set.saf_description') }}</th>
        <th data-col="saf_tab">{{ trans('swis.constructor_data_set.saf_tab') }}</th>
        <th data-col="saf_is_attribute" class="text-center">
            <input type="checkbox" class="check-all-fields check-all-saf-fields" {{ (!empty($checked) && $checked['isCheckedAllSaf']) ? 'checked' : '' }}>
        </th>
    </tr>
    </thead>
    <tbody id="tbody">
        @if ($safData && count($safData) > 0)
            @foreach ($safData as $safID => $item)
                <tr>
                    <td>{{ $safID }}</td>
                    <td>{{ $item['mdm_id'] }}</td>
                    <td>{{ $item['name'] }}</td>
                    <td>{{ trans($item['description']) }}</td>
                    <td>{{ $item['tab'] }}</td>
                    <td align="center">
                        <input type="checkbox"
                               class="saf_field_checkbox"
                               name="paramsSaf[{{$safID}}]"
                               {{ (!empty($constructorList) && in_array($safID, $checked['saf'])) ? 'checked' : '' }}
                        >
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="8" align="center">No data available yet</td>
            </tr>
        @endif
    </tbody>
</table>