@extends('layouts.app')

@section('title'){{trans('swis.customs_portal.title')}}@endsection

@section('contentTitle') {{trans('swis.customs_portal.title')}}  @endsection

<?php

$jsTrans->addTrans([
    'swis.custom_portal.search.saf_link.button',
    'swis.custom_portal.search.pdf_link.button',
    'swis.import_title',
    'swis.export_title',
    'swis.transit_title',
]);

?>

@section('content')
    <div class="clearfix search__out">
        <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse" data-target="#collapseSearch" aria-expanded="true" aria-controls="collapseSearch">
            <i class="fa fa-search"> </i><span class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
        </button>
    </div>

    <div class="collapse in" id="collapseSearch" aria-expanded="true">
        <div class="card card-body">
            <form class="form-horizontal fill-up" id="search-filter" autocomplete="off">

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.custom_portal.saf.agency.label')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <select class="select2" name="agency" id="agency">
                                    <option value=""></option>
                                    @if($agencies->count())
                                        @foreach($agencies as $agency)
                                            <option value="{{$agency->id}}">{{$agency->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <div class="form-error" id="form-error-agency"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.custom_portal.saf.agency_subdivisions.label')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <select class="select2" name="subdivision" id="subdivision">
                                    <option value=""></option>
                                </select>
                                <div class="form-error" id="form-error-subdivision"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.custom_portal.saf.constructor_documents.label')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <select name="constructor_document" class="form-control select2" id="constructorDocuments">
                                    <option value=""></option>
                                </select>
                                <div class="form-error" id="form-error-constructor_document"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.custom_portal.saf.regular_number.label')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="regular_number" id="" value="">
                                <div class="form-error" id="form-error-regular_number"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.custom_portal.saf.importer.tax_id_passport.label')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="importer_value" id="" value="">
                                <div class="form-error" id="form-error-importer_value"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.custom_portal.saf.exporter.tax_id_passport.label')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="exporter_value" id="" value="">
                                <div class="form-error" id="form-error-exporter_value"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.custom_portal.saf.applicant.tax_id_passport.label')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="applicant_value" id="" value="">
                                <div class="form-error" id="form-error-applicant_value"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.custom_portal.sub_application.approve_reject_number')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="approve_reject_number" id="" value="">
                                <div class="form-error" id="form-error-approve_reject_number"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.custom_portal.saf.sub_application_received_date.label')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-4 col-lg-5 field__two-col">
                                <div class='input-group date'>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                    <input value="" name="sub_application_received_at_date_start" autocomplete="off"
                                           type="text"
                                           placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                    <div class="form-error" id="form-error-sub_application_received_at_date_start"></div>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-5 field__two-col">
                                <div class='input-group date'>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                    <input value="" name="sub_application_received_at_date_end" autocomplete="off"
                                           type="text"
                                           placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                    <div class="form-error" id="form-error-sub_application_received_at_date_end"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8 col-lg-offset-3 text-right">
                        <div class="row">
                            <div class="col-lg-10 col-md-8">
                                <button type="submit" class="btn btn-primary mr-10">{{trans('core.base.label.search')}}</button>
                                <button type="button" class="btn btn-danger resetButton" data-submit="true">{{trans('core.base.label.reset')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="data-table">
        <div class="clearfix">
            <table class="table table-striped table-bordered table-hover dataTables-example" id="list-data">
                <thead>
                    <tr>
                        <th data-col="actions">{{trans('core.base.label.actions')}}</th>
                        <th data-col="applicant_value">{{trans('swis.custom_portal.saf.applicant.tax_id')}}</th>
                        <th data-ordering="0" data-col="importer_exporter">{{trans('swis.custom_portal.importer_exporter')}}</th>
                        <th data-col="regime">{{trans('swis.saf.regime')}}</th>
                        <th data-col="sub_application_number">{{trans('swis.custom_portal.sub_application.document_number')}}</th>
                        <th data-col="sub_application_type">{{trans('swis.custom_portal.sub_application.document_type')}}</th>
                        <th data-col="approve_reject_number">{{trans('swis.custom_portal.sub_application.approve_reject_number')}}</th>
                        <th data-col="sub_application_name">{{trans('swis.custom_portal.sub_application.document_name')}}</th>
                        <th data-col="status">{{trans('swis.custom_portal.sub_application.status')}}</th>
                        <th data-col="sub_application_status_date">{{trans('swis.custom_portal.sub_application.status_date')}}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/customs-portal/main.js')}}"></script>
@endsection