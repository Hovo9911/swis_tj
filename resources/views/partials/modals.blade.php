<!-- Delete modal -->
<div class="modal fade" id="d-delete-confirm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="fb modal__title">{{trans('core.base.delete_confirm.title')}}</h4>
                <button type="button" class="close modal__close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body" >
                <div class="modal-delete-confirm-message">{{trans('core.base.delete.confirm_text')}}</div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default btn-cancel" data-dismiss="modal">{{trans('core.base.button.close')}}</button>
                <button class="btn btn-primary btn-delete">{{trans('core.base.button.delete')}}</button>
            </div>
        </div>
    </div>
</div>

<!-- Clone modal -->
<div class="modal fade" id="d-clone-confirm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal__title fb">{{trans('core.base.clone_confirm.title')}}</h4>
                <button type="button" class="close modal__close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body" >
                <div class="modal-delete-confirm-message">{{trans('core.base.clone.confirm_text')}}</div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default btn-cancel" data-dismiss="modal">{{trans('core.base.button.close')}}</button>
                <button class="btn btn-primary btn-delete">{{trans('core.base.button.clone')}}</button>
            </div>
        </div>
    </div>
</div>

<!-- Send Confirm modal -->
<div class="modal fade" id="d-send-confirm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="fb modal__title">{{trans('core.base.modal.send_confirm.title')}}</h4>
                <button type="button" class="close modal__close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body" >
                <div class="modal-delete-confirm-message">{{trans('core.base.modal.send_confirm_text')}}</div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default btn-cancel" data-dismiss="modal">{{trans('core.base.button.close')}}</button>
                <button class="btn btn-primary btn-delete">{{trans('core.base.button.modal.button.send_confirm')}}</button>
            </div>
        </div>
    </div>
</div>


<!-- Delete empty modal -->
<div class="modal fade" id="d-delete-empty-message" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal__title fb">{{trans('core.base.delete.empty_message.title')}}</h4>
                <button type="button" class="close modal__close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body" >
                <div class="modal-delete-confirm-message">{{trans('core.base.delete.empty_message.text')}}</div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal">{{trans('core.base.button.close')}}</button>
            </div>
        </div>
    </div>
</div>

<!-- Input changed close refresh -->
<div class="modal fade" id="d-refresh-page-message" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal__title fb">{{trans('core.base.refresh_message.title')}}</h4>
                <button type="button" class="close modal__close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <div class="modal-refresh-confirm-message">{{trans('core.base.refresh.confirm_text')}}</div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default btn-refresh-cancel" data-dismiss="modal">{{trans('core.base.button.refresh.close')}}</button>
                <a class="btn btn-primary btn-refresh">{{trans('core.base.button.refresh')}}</a>
            </div>
        </div>
    </div>
</div>

<!-- File manager  -->
<div class="modal fade file-manager-modal" tabindex="-1" role="dialog" aria-labelledby="">

    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>

        <div class="file-manager-container">
        </div>
    </div>
</div>

@if(!is_null(session('hasNoRole')) && session('hasNoRole'))
    <?php session()->forget('hasNoRole') ?>
    <div id="noRoleModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <p>{{trans('swis.has_no_role_message')}}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('core.base.button.close')}}</button>
                </div>
            </div>

        </div>
    </div>
@endif

@if(!is_null(session('needUpdateUserRolesSession')) && session('needUpdateUserRolesSession'))
    <?php session()->forget('needUpdateUserRolesSession');session()->save() ?>

    <div id="needUpdateUserRolesSessionModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <p>{{trans('swis.base.user_session.need_update.message')}}</p>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-success" onclick="event.preventDefault(); document.getElementById('clear-session-form').submit();">{{trans('core.base.button.update.session')}}</a>
                </div>
            </div>

        </div>
    </div>
    <form id="clear-session-form" action="{{urlWithLng('clear-user-session')}}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
@endif

{{-- Will Insert with Modal.js --}}
<div id="hModals"></div>

@if(!empty($searchColumns))
    <div id="hideOrShowParams" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{ trans('swis.user_documents.hide_params_columns_modal.title') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="row text-center modal-load-body"><i class="fa fa-spinner fa-spin" style="font-size: 18px;"></i></div>
                    <div class="row modal-main-body" style="display: none;">
                        <div class="col-md-6 hide-params-section">
                            <label>{{ trans('swis.user_documents.search.hide_params.label') }}</label><br /><br />
                            <div class="hide-params-section-body"></div>
                        </div>
                        <div class="col-md-6 hide-column-section">
                            <label>{{ trans('swis.user_documents.search.hide_columns.label') }}</label><br /><br />

                            <div class="hide-column-section-body">
                                @foreach ($searchColumns as $column => $trans)
                                    <label><input type="checkbox" class="hide-search-column" data-col="{{ $column }}" /> {{ $trans }}</label><br />
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary restoreButton">{{trans('core.base.label.restore')}}</button>
                    <button type="button" class="btn btn-danger hideButton">{{trans('core.base.label.hide')}}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('core.base.button.close') }}</button>
                </div>
            </div>

        </div>
    </div>
@endif