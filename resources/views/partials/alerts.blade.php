@if (session('success'))
    <div class="alert alert-success custom-alert-success">
        {{ session('success') }}
    </div>
    <?php session()->forget('success') ?>
@endif

@if (session('error'))
    <div class="alert alert-danger custom-alert-danger">
        {{ session('error') }}
    </div>
@endif