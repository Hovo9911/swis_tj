<?php

// Global translations
if (!empty($transType) && $transType == 'global'):

    $jsTrans->addTrans([
        "core.error.500.message",
        "core.error.400.message",
        "core.error.403.message",
        "core.error.404.message",
        "core.error.405.message",
        "core.error.401.message",
        "core.error.500.head_title",
        "base.ok",
        "core.loading.invalid_data",
        "core.loading.success",
        "core.loading.saved",
        "core.loading",
        "core.loading.saving",
        "core.loading.processing",
        "core.loading.deleting",
        "core.loading.removed",
        'core.error.500.head_title',
        "core.error.500.message",
        "core.base.no_available_data",
        "core.base.field.required",
        "core.multi_box.check_all",
        "core.multi_box.uncheck_all",
        "core.img.uploader.upload_btn",
        "core.base.button.search",
        "core.base.label.select",
        "core.file.uploader.select_file",
        "swis.base.tooltip.edit.button",
        "swis.base.tooltip.delete.button",
        "swis.base.tooltip.clone.button",
        "swis.base.tooltip.send.button",
        "swis.base.tooltip.view.button",
        "swis.base.tooltip.view.document",
        "swis.base.tooltip.print.button",
        "core.base.label.status.active",
        "core.base.label.status.inactive",
        "core.base.label.status.deleted",
        "core.base.field.unique",
        "swis.single_app.batches.auto-generated.batch_number",
        "swis.user_documents.document_status_is_changed",
        "swis.user_documents.document_status_changed_button",
        'core.base.button.resend',
        'swis.saf.status_type.created',
        'swis.saf.status_type.send',
        'swis.saf.status_type.draft',
        'swis.saf.status_type.void',
        "swis.authorized_first_name",
        "swis.authorized_last_name",
        "swis.authorized_user.modal.title",
        "swis.documents.already_exist.info",
        "swis.single_app.batch.invalid_quantity",
        "swis.single_app.batch.invalid_netto_weight",
        "swis.single_app.batch.invalid_expiration_date",
        "swis.single_app.batch.invalid_production_date",
        "swis.document.document_form.electronic.title",
        "swis.document.document_form.paper.title",
        "core.base.select2.enter_text_1",
        "core.base.select2.enter_text_2",
        "swis.user_documents.choose_products.empty",
        "swis.db.exception.message",
        "core.base.delete.empty_message.title",
        "core.base.delete.confirm_text",
        "core.base.button.delete",
        "core.base.button.close",
        "core.base.label.total_value_select",
        "swis.base.tooltip.send_notification.button",
        "swis.user_manual_type.user_manuals",
        "swis.user_manual_type.videos",
        "swis.user_manual_type.technical_specifications",
        "swis.user_manual_type.etc",
        "swis.global_notifications.send_status.success",
        "swis.core.single_window.title",
        "swis.authorized_ssn",
        "swis.user_documents.choose_different_product",
        "swis.calendar.non_working_days.tooltip",
        "swis.user_documents.digital_signature_sign.wrong_credentials",
        "swis.user_documents.digital_signature_sign.success",
        "swis.user-documents.digital-signature.issuer",
        "swis.user-documents.digital-signature.issuer_address",
        "swis.user-documents.digital-signature.valid_from",
        "swis.user-documents.digital-signature.valid_to",
        "swis.user-documents.digital-signature.signing_time",
        "swis.user-documents.digital_signature.valid",
        "swis.user-documents.digital_signature.not_valid",
        "swis.user_documents.digital_signature_sign.no_device_available",
        "swis.user_documents.digital_signature_sign.rutoken_plugin_missed",
    ]);

endif;

// Modal translations
if (!empty($transType) && $transType == 'modal'):

    $jsTrans->addTrans([
        'core.base.delete.empty_message.title',
        'core.base.button.close',
        'core.base.delete.confirm_text',
        'core.base.button.delete',
        'core.base.field.unique',
        "core.base.select2.enter_text_1",
        "core.base.select2.enter_text_2"
    ]);

endif;