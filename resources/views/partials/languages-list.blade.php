<ul class="nav navbar-top-links navbar-right closeRefreshGroup">
    @if(Auth::check() && Route::currentRouteName() != 'roleSet' && !\Auth::user()->isNaturalPerson())
        @if(Auth::user()->hasPaymentModuleAccess())
            <li><a class="dropdown-toggle count-info" href="{{urlWithLng('payments')}}"> <i
                            class="fa fa-money text-navy"></i> <span class="text-navy"
                                                                     id="userBalance">{{ Auth::user()->balance() }}</span><span
                            class="text-navy">{{ config('swis.default_currency') }}</span></a>
            </li>
        @endif
        <li>
            <a class="dropdown-toggle count-info" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false"> <i class="fa fa-bell"></i>
                @if($unreadNotificationsCount > 0)
                    <span class="label label-warning-light notifications-count">{{$unreadNotificationsCount}}</span>
                @endif
            </a>
            <ul class="dropdown-menu dropdown-alerts" aria-labelledby="navbarDropdownMenuLink">
                @foreach(App\Models\Notifications\Notifications::newestNotifications() as $newestNotification)
                    @if(!is_null($newestNotification->in_app_notification))
                    <li class="header-notification__item @if($newestNotification->read == \App\Models\Notifications\Notifications::NOTIFICATION_STATUS_UNREAD)header-notification__item--unread @endif">
                        <div>
                            <div class="media-body">
                                <span>{!! $newestNotification->in_app_notification!!}</span><br>
                                <small class="text-muted">{{$newestNotification->created_at}}</small>
                                <button class="hidden" id="{{$newestNotification->id}}"> </button>
                            </div>
                        </div>
                    </li>
                    <li class="dropdown-divider"></li>
                    @endif
                @endforeach
                <li>
                    <div class="text-center link-block">
                        <a href="{{urlWithLng('notifications')}}" class="dropdown-item">
                            {{trans('swis.header.button.show_all_notifications')}}
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </li>
            </ul>
        </li>
    @endif
    @auth
        {{--<li><a class="dropdown-toggle count-info" href="#">   <i class="fa fa-money text-navy"></i>  <span class="text-navy">1,200.000 {{config('swis.default_currency')}}</span></a> </li>--}}
        {{--<li><a class="dropdown-toggle count-info" href="{{urlWithLng('dashboard')}}">   <i class="fa fa-bell"></i>  <span class="label label-warning-light">8</span></a> </li>--}}

        <li>
            <a href="#" id="logoutBtn">
                <i class="fa fa-sign-out-alt"></i> {{trans('swis.core.logout')}}
            </a>
            <form id="logout-form" action="{{ urlWithLng('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    @endauth
    @if(!Auth::check() && Request::route()->getName() == \App\Models\UserManuals\UserManuals::PAGE_TYPE_HELP)
        <li>
            <a href="{{ urlWithLng('/login') }}">
                <i class="fa fa-sign-in-alt"></i> {{trans('swis.core.signin')}}
            </a>
        </li>
    @endif
    <li><a class="dropdown-toggle count-info" href="{{urlWithLng('help')}}"> <i class="fa fa-question-circle"></i></a></li>
    <li>
        <div class="languages-list">

            <?php
            $languages = activeLanguages();
            $lngManager = \App\Http\Controllers\Core\Language\LanguageManager::getInstance();
            ?>

            @foreach($languages as $language)
                <a href="{{$lngManager->getUrl($language)}}"
                   class="btn {{(cLng('id') == $language->id) ? 'btn-white active' : '' }}"><img
                            src="{{asset('image/'.$language->icon)}}"><span>{{trans('core.base.language.'.$language->code,[],$language->code)}}</span></a>
            @endforeach
        </div>
    </li>
</ul>
