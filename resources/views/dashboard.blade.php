@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-6">
            @include('notifications.dashboard')
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('js/dashboard/main.js')}}"></script>
@endsection
