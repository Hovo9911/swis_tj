@extends('layouts.app')

@section('title'){{trans('swis.roles_group.create.title')}}@stop

@section('contentTitle')
    {{trans('swis.roles_group.create.title')}}
@stop

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('content')

    <?php
    switch ($saveMode) {
        case 'add':
            $url = 'roles-group';
            break;
        default :
            $url =  $saveMode != 'view' ? 'roles-group/update/' . $roleGroup->id : '';

            $mls = $roleGroup->ml()->base(['lng_id', 'name' , 'description'])->keyBy('lng_id');
            break;
    }

    $languages = activeLanguages();
    ?>

    <form class="form-horizontal fill-up " id="form-data" action="{{urlWithLng($url)}}">

        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-general"> {{trans('core.base.tab.general')}}</a></li>
                @foreach($languages as $language)
                    <li><a data-toggle="tab" href="#tab-{{$language->code}}">{{ trans("core.base.language.".strtolower($language->name)) }}</a></li>
                @endforeach
            </ul>
            <div class="tab-content">
                <div id="tab-general" class="tab-pane active">
                    <div class="panel-body">
                        <div class="form-group required"><label class="col-lg-3 control-label">{{trans('swis.module_title')}}</label>
                            <div class="col-md-9">
                                @if(!isset($roleGroup))
                                    <select class="form-control  select2" id="modules" name="menu_id" {{($saveMode == 'edit') ? 'readonly':''}}>
                                        <option></option>
                                        @foreach($menuModules as $menu)
                                            @if($menu->id == \App\Models\Menu\Menu::AUTHORIZE_PERSON_MENU_ID))
                                                @continue
                                            @endif
                                            <option value="{{$menu->id}}">{{trans($menu->title)}}</option>
                                        @endforeach
                                    </select>
                                    <div class="form-error" id="form-error-menu_id"></div>
                                @else
                                    <input value="{{$menuName or ''}}" disabled type="text" class="form-control">
                                    <input value="{{$roleGroup->menu_id or ''}}"  type="hidden" name="menu_id" class="form-control">
                                @endif
                            </div>
                        </div>

                        <div class="form-group required"><label class="col-lg-3 control-label">{{trans('swis.roles_title')}}</label>
                            <div class="col-lg-9">
                                <select class="form-control select2" name="roles[]" id="roles" multiple data-allow-clear="false">
                                    @if($saveMode != 'add')
                                        @foreach($roles as $role)
                                            <option {{in_array($role->id,$checkedRoles) ? 'selected' : ''}} title="{{$role->current->description}}" value="{{$role->id}}">{{$role->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <div class="form-error" id="form-error-roles"></div>
                            </div>
                        </div>

                        @if($saveMode == 'add')

                            <div class="form-group hide"><label class="col-lg-3 control-label">{{trans('swis.roles_group.roles_attributes.label')}}</label>
                                <div class="col-lg-9">
                                    <select class="form-control select2" disabled name="roles_attributes[]" id="rolesAttributes" multiple data-allow-clear="false"></select>
                                    <div class="form-error" id="form-error-roles_attributes"></div>
                                </div>
                            </div>

                        @elseif(isset($roleGroup) && $roleGroup->menu_id == $myApplicationMenuId)
                            <?php
                                $attributeTypeSaf = \App\Models\Role\Role::ROLE_ATTRIBUTE_TYPE_SAF;
                                $attributeTypeMDM = \App\Models\Role\Role::ROLE_ATTRIBUTE_TYPE_MDM;
                            ?>

                            <div class="form-group"><label class="col-lg-3 control-label">{{trans('swis.roles_group.roles_attributes.label')}}</label>
                                <div class="col-lg-9">
                                    <select class="form-control select2" name="roles_attributes[]" id="rolesAttributes" multiple data-allow-clear="false">
                                        @if(isset($rolesAttributes[$attributeTypeSaf]))
                                            @foreach($rolesAttributes[$attributeTypeSaf] as $safAttributeKey => $safAttributeValue)
                                                <option {{isset($checkedAttributes[$attributeTypeSaf]) && in_array($safAttributeKey,$checkedAttributes[$attributeTypeSaf]) ? 'selected' : ''}} value="{{$attributeTypeSaf.'_'.$safAttributeKey}}">{{$safAttributeValue}}</option>
                                            @endforeach
                                        @endif

                                        @if(isset($rolesAttributes[$attributeTypeMDM]))
                                            @foreach($rolesAttributes[$attributeTypeMDM] as $mdmAttributeKey => $mdmAttributeValue)
                                                <option {{isset($checkedAttributes[$attributeTypeMDM]) && in_array($mdmAttributeKey,$checkedAttributes[$attributeTypeMDM]) ? 'selected' : ''}} value="{{$attributeTypeMDM.'_'.$mdmAttributeKey}}">{{$mdmAttributeValue}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <div class="form-error" id="form-error-roles_attributes"></div>
                                </div>
                            </div>
                        @endif


                        @if($saveMode != 'add')

                            <div class="form-group ">
                                <label class="col-lg-3 control-label">{{trans('core.base.created_at')}}</label>
                                <div class="col-lg-9">
                                    <input value="{{$roleGroup->created_at}}" disabled type="text" placeholder="" class="form-control">
                                </div>
                            </div>
                        @endif

                        <div class="form-group">
                            <label class="control-label col-lg-3">{{trans('core.base.label.status')}}</label>
                            <div class="col-lg-9 general-status ">
                                <select name="show_status" class="form-show-status">
                                    <option value="{{\App\Models\RolesGroup\RolesGroup::STATUS_ACTIVE}}"{{(isset($roleGroup) && $roleGroup->show_status == App\Models\RolesGroup\RolesGroup::STATUS_ACTIVE) || $saveMode == 'add' ?  ' selected="selected"' : ''}}>{{trans('core.base.label.status.active')}}</option>
                                    <option value="{{\App\Models\RolesGroup\RolesGroup::STATUS_INACTIVE}}"{{isset($roleGroup) &&  $roleGroup->show_status ==  App\Models\RolesGroup\RolesGroup::STATUS_INACTIVE ? ' selected="selected"': ''}}>{{trans('core.base.label.status.inactive')}}</option>
                                </select>
                                <div class="form-error" id="form-error-show_status"></div>
                            </div>
                        </div>
                    </div>
                </div>
                @foreach($languages as $language)
                    <?php $lngId = $language->id ?>
                    <div id="tab-{{$language->code}}" class="tab-pane">
                        <div class="panel-body">
                            <div class="form-group required"><label
                                        class="col-lg-2 control-label">{{trans('core.base.label.name')}}</label>
                                <div class="col-lg-10">
                                    <input value="{{isset($mls[$lngId]) ? $mls[$lngId]->name : ''}}"
                                           name="ml[{{$language->id}}][name]" type="text" placeholder=""
                                           class="form-control">
                                    <div class="form-error" id="form-error-ml-{{$language->id}}-name"></div>
                                </div>
                            </div>

                            <div class="form-group"><label class="col-lg-2 control-label">{{trans('core.base.description')}}</label>
                                <div class="col-lg-10">
                                    <textarea rows="6" name="ml[{{$language->id}}][description]"
                                              class="form-control">{{isset($mls[$lngId]) ? $mls[$lngId]->description : ''}}</textarea>
                                    <div class="form-error" id="form-error-ml-{{$language->id}}-description"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <input type="hidden" value="{{$roleGroup->id or 0}}" name="id" class="mc-form-key"/>

        {!! csrf_field() !!}
    </form>


@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script>
        var myApplicationMenu = '{{$myApplicationMenuId or 0}}';
    </script>
    <script src="{{asset('js/roles-group/main.js')}}"></script>
@endsection