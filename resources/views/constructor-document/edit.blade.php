@extends('layouts.app')

@section('title'){{trans('swis.constructor_document.document_title')}}@stop

@section('contentTitle') {{trans('swis.constructor_document.document_title')}} @stop

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('content')
    <?php

    switch ($saveMode) {
        case 'add':
            $url = 'constructor-documents';
            $disabled = '';
            break;
        case 'edit':
            $url = 'constructor-documents/update/' . $constructorDocument->id;
            $disabled = 'disabled';
            $mls = $constructorDocument->ml()->notDeleted()->base(['lng_id', 'document_name', 'description', 'show_status'])->keyBy('lng_id');
            break;
    }

    $languages = activeLanguages();

    ?>

    <form class="form-horizontal fill-up " id="form-data" action="{{urlWithLng($url)}}">

        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-general"> {{trans('core.base.tab.general')}}</a></li>
                @foreach($languages as $language)
                    <li class=""><a data-toggle="tab"
                                    href="#tab-{{$language->code}}">{{ trans("core.base.language.".strtolower($language->name)) }}</a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content">
                <div id="tab-general" class="tab-pane active">
                    <div class="panel-body">

                        <div class="form-group required"><label
                                    class="col-lg-2 control-label">{{trans('swis.document_code')}}</label>
                            <div class="col-lg-10">
                                <input value="{{$constructorDocument->document_code or ''}}" name="document_code" type="text"
                                       placeholder=""
                                       class="form-control"
                                        {{$disabled}}>
                                <div class="form-error" id="form-error-document_code"></div>
                            </div>
                        </div>

                        @if($saveMode == 'edit')
                            <div class="form-group required"><label
                                        class="col-lg-2 control-label">{{trans('swis.agency.title')}}</label>
                                <div class="col-lg-10">
                                    <input value="{{ (isset($constructorDocument->agency) && $constructorDocument->agency->current()->name) ? $constructorDocument->agency->current()->name : ''}}"
                                           disabled="" type="text" placeholder="" class="form-control">
                                </div>
                            </div>
                        @endif

                        <div class="form-group required"><label
                                    class="col-lg-2 control-label">{{trans('swis.constructor.start_date')}}</label>
                            <div class="col-lg-10">
                                <div class='input-group date'>
                                    <span class="input-group-addon"><span
                                                class="glyphicon glyphicon-calendar"></span></span>
                                    <input value="{{isset($constructorDocument) ? formattedDate($constructorDocument->start_date) : '' }}"
                                           name="start_date" autocomplete="off" type="text"
                                           placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                </div>

                                <div class="form-error" id="form-error-start_date"></div>
                            </div>
                        </div>

                        <div class="form-group "><label
                                    class="col-lg-2 control-label">{{trans('swis.constructor.end_date')}}</label>
                            <div class="col-lg-10">

                                <div class='input-group date'>
                                    <span class="input-group-addon"><span
                                                class="glyphicon glyphicon-calendar"></span></span>
                                    <input value="{{isset($constructorDocument) ? formattedDate($constructorDocument->end_date) : '' }}"
                                           name="end_date" autocomplete="off" type="text"
                                           placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                </div>

                                <div class="form-error" id="form-error-end_date"></div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="control-label col-lg-2">{{trans('swis.constructor.version')}}</label>
                            <div class="col-lg-10">
                                <input value="{{$constructorDocument->version or ''}}" name="version" type="text" placeholder=""
                                       class="form-control">
                                <div class="form-error" id="form-error-version"></div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="control-label col-lg-2">{{trans('swis.reference.document_type')}}</label>
                            <div class="col-lg-10">
                                <select class="form-control select2" name="document_type_id">
                                <option value="">{{trans('core.base.label.select')}}</option>
                                    @foreach($typeOfDocuments as $item)
                                        <option {{ (isset($constructorDocument) && $constructorDocument->document_type_id == $item->id) ? 'selected' : '' }} value="{{$item->id}}">{{"($item->id)"}} {{$item->name}}</option>
                                    @endforeach
                                </select>

                                @if(isset($typeOfDocuments,$constructorDocument) && !in_array($constructorDocument->document_type_id,$typeOfDocuments->pluck('id')->toArray()))
                                    <small class="text-danger">{{trans('swis.constructor_document.document_type_id.ref_row_inactive',['id'=>$constructorDocument->document_type_id])}}</small>
                                @endif
                                <div class="form-error" id="form-error-document_type_id"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">{{trans('swis.constructor_document.expertise_type')}}</label>
                            <div class="col-lg-10">
                                <select class="form-control select2" name="expertise_type[]" multiple>
                                    @foreach(\App\Models\Laboratory\Laboratory::LAB_TYPES as $key => $value)
                                        <option {{ (isset($constructorDocument) && in_array($value, $constructorDocument->expertise_type ?? [])) ? 'selected' : '' }} value="{{ $value }}"> {{ trans("swis.saf.lab_examination.{$key}.option") }} </option>
                                    @endforeach
                                </select>
                                <div class="form-error" id="form-error-expertise_type"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">{{trans('swis.send_environment')}}</label>
                            <div class="col-lg-10">
                                <input value="{{$constructorDocument->environment or '1'}}" name="send_environment"
                                       type="checkbox" placeholder="">
                                <div class="form-error" id="form-error-send_environment"></div>
                            </div>
                        </div>

                        @if(config('global.digital_signature'))
                            <div class="form-group">
                                <label class="control-label col-lg-2">{{trans('swis.constructor.documents.digital_signature')}}</label>
                                <div class="col-lg-10">
                                    <input value="1" name="digital_signature" type="checkbox" placeholder=""
                                           @if(isset($constructorDocument) && $constructorDocument->digital_signature) checked @endif >
                                    <div class="form-error" id="form-error-digital_signature"></div>
                                </div>
                            </div>
                        @endif

                        @if (Auth::user()->isLaboratory())
                            <div class="form-group">
                                <label class="control-label col-lg-2">{{trans('swis.constructor.documents.is_lab_flow')}}</label>
                                <div class="col-lg-10">
                                    <input value="1" name="is_lab_flow" type="checkbox" {{ (isset($constructorDocument) && $constructorDocument->is_lab_flow) ? 'checked' : '' }}>
                                    <div class="form-error" id="form-error-is_lab_flow"></div>
                                </div>
                            </div>
                        @endif

                        <div class="form-group">
                            <label class="control-label col-lg-2">{{trans('core.base.label.status')}}</label>
                            <div class="col-lg-10 general-status">
                                <select name="show_status" class="form-show-status ">
                                    <option value="{{\App\Models\ConstructorDocument\ConstructorDocument::STATUS_ACTIVE}}"{{(!empty($constructorDocument) && $constructorDocument->show_status == \App\Models\ConstructorDocument\ConstructorDocument::STATUS_ACTIVE) || $saveMode == 'add' ?  ' selected="selected"' : ''}}>{{trans('core.base.label.status.active')}}</option>
                                    <option value="{{\App\Models\ConstructorDocument\ConstructorDocument::STATUS_INACTIVE}}"{{!empty($constructorDocument) &&  $constructorDocument->show_status == \App\Models\ConstructorDocument\ConstructorDocument::STATUS_INACTIVE ? ' selected="selected"': ''}}>{{trans('core.base.label.status.inactive')}}</option>
                                </select>
                                <div class="form-error" id="form-error-show_status"></div>
                            </div>
                        </div>
                    </div>

                </div>
                @foreach($languages as $language)
                    <?php $lngId = $language->id ?>
                    <div id="tab-{{$language->code}}" class="tab-pane">
                        <div class="panel-body">

                            <div class="form-group required"><label
                                        class="col-lg-2 control-label">{{trans('swis.document_name')}}</label>
                                <div class="col-lg-10">
                                    <input value="{{isset($mls[$lngId]) ? $mls[$lngId]->document_name : ''}}"
                                           name="ml[{{$language->id}}][document_name]" type="text"
                                           placeholder=""
                                           class="form-control">

                                    <div class="form-error"
                                         id="form-error-ml-{{$language->id}}-document_name"></div>
                                </div>
                            </div>

                            <div class="form-group "><label
                                        class="col-lg-2 control-label">{{trans('core.base.description')}}</label>
                                <div class="col-lg-10">
                                    <textarea rows="6" name="ml[{{$language->id}}][description]"
                                              class="form-control">{{isset($mls[$lngId]) ? $mls[$lngId]->description : ''}}</textarea>
                                    <div class="form-error"
                                         id="form-error-ml-{{$language->id}}-description"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <input type="hidden" value="{{$constructorDocument->id or 0}}" name="id" class="mc-form-key"/>

            {!! csrf_field() !!}
        </div>
    </form>

@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script src="{{asset('js/constructor-document/main.js')}}"></script>
@endsection