@extends('layouts.app')

@section('title'){{trans('swis.constructor_document.title')}}@stop

@section('contentTitle') {{trans('swis.constructor_document.title')}} @stop

@section('form-buttons-top')
    {!! renderAddButton() !!}
@stop

@section('content')

    <div class="clearfix search__out">
        <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse" data-target="#collapseSearch" aria-expanded="false" aria-controls="collapseSearch">
            <i class="fa fa-search"> </i><span class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
        </button>
    </div>

    <div class="collapse" id="collapseSearch">
        <div class="card card-body">
            <div class="row">
                <form class="form-horizontal fill-up" id="search-filter" autocomplete="off">
                    <div class="form-group">
                        <label class="col-md-4 col-lg-3 control-label">ID</label>
                        <div class="col-lg-6">
                            <input type="number" min="0" class="form-control onlyNumbers" name="id" id="" value="">
                            <div class="form-error" id="form-error-id"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-5 col-sm-4 text-right">
                            <button type="submit"
                                    class="btn btn-primary">{{trans('core.base.label.search')}}</button>
                            <button type="button" class="btn btn-danger resetButton"
                                    data-submit="true">{{trans('core.base.label.reset')}}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="data-table">
        <div class="clearfix">
            <table class="table table-striped table-bordered table-hover dataTables-example" id="list-data">
                <thead>
                <tr>
                    <th data-col="actions" class="th-actions" data-delete="0">{{trans('core.base.label.actions')}}</th>
                    <th data-col="id">{{ trans('swis.document_cloud.id') }}</th>
                    <th data-col="document_name">{{ trans('swis.document_name') }}</th>
                    <th data-col="document_code">{{ trans('swis.document_code') }}</th>
                    <th data-col="start_date">{{ trans('swis.constructor.start_date') }}</th>
                    <th data-col="version">{{ trans('swis.constructor.version') }}</th>
                    <th data-col="document_type">{{ trans('swis.document.type') }}</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/constructor-document/main.js')}}"></script>
@endsection