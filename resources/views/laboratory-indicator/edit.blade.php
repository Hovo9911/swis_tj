@extends('layouts.app')

@section('title'){{trans('swis.laboratory_indicator.title')}}@stop

@section('contentTitle')
    {{trans('swis.laboratory_indicator.create.title')}}
@stop

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('content')

    <?php
    switch ($saveMode) {
        case 'add':
            $url = 'laboratory-indicator';
            break;
        case 'edit':
            $url = 'laboratory-indicator/update/' . $laboratoryIndicator->id;

            break;
    }
    ?>

    <form class="form-horizontal fill-up " id="form-data" action="{{urlWithLng($url)}}">

        <div class="col-md-6 col-md-offset-3">
            <div class="tabs-container">

                <div class="form-group required">
                    <label class="col-lg-2 control-label">{{trans('swis.laboratory_indicator.select_laboratory')}}</label>
                    <div class="col-lg-10">
                        <select class="form-control select2" name="laboratory_id">
                            @if($saveMode == 'add')
                            <option value="" disabled selected>{{trans('core.base.label.select')}}</option>
                            @endif
                            @if (!empty($laboratories))
                                @foreach ($laboratories as $laboratory)
                                    <option {{(!empty($labId) && empty($laboratoryIndicator) && $labId == $laboratory->id) ? 'selected' : ''}}  {{(!empty($laboratoryIndicator) && $laboratoryIndicator->laboratory_id == $laboratory->id) ? 'selected' : ''}} value="{{ $laboratory->id }}">{{ @$laboratory->current()->name }}</option>
                                @endforeach
                            @endif
                        </select>
                        <div class="form-error" id="form-error-laboratory_id"></div>
                    </div>
                </div>

                <div class="form-group required"><label
                            class="col-lg-2 control-label">{{trans('swis.laboratory_indicator.indicator_select.label')}}</label>
                    <div class="col-lg-10">
                        <select data-url="{{urlWithLng('laboratory-indicator/autocomplete')}}" class="form-control select2-ajax" name="indicator_id">
                            @if(!empty($indicator))
                                <option selected value="{{$indicator->id}}">{{$indicator->name}}</option>
                            @endif
                        </select>
                        <div class="form-error" id="form-error-indicator_id"></div>
                    </div>
                </div>

                <div class="form-group required"><label
                            class="col-lg-2 control-label">{{trans('swis.laboratory_indicator.price.label')}}</label>
                    <div class="col-lg-10">
                        <input type="number" value="{{$laboratoryIndicator->price or ''}}" name="price" class="form-control onlyNumbers">
                        <div class="form-error" id="form-error-price"></div>
                    </div>
                </div>

                <div class="form-group required"><label
                            class="col-lg-2 control-label">{{trans('swis.laboratory_indicator.duration.label')}}</label>
                    <div class="col-lg-10">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="number" name="duration" value="{{$laboratoryIndicator->duration or ''}}" class="form-control onlyNumbers">
                            </div>
                            <div class="col-md-6">
                                <select class="form-control select2" data-allow-clear="false" name="date_time">
                                    <option {{(!empty($laboratoryIndicator) && $laboratoryIndicator->date_time == 'hours') ? 'selected' : ''}} value="hours">{{trans('swis.laboratory_indicator.hours.label')}}</option>
                                    <option {{(!empty($laboratoryIndicator) && $laboratoryIndicator->date_time == 'days') ? 'selected' : ''}} value="days">{{trans('swis.laboratory_indicator.days.label')}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-error" id="form-error-duration"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2">{{trans('core.base.label.status')}}</label>
                    <div class="col-lg-10 general-status">
                        <select name="show_status" class="form-show-status">
                            <option value="{{App\Models\LaboratoryIndicator\LaboratoryIndicator::STATUS_ACTIVE}}"{{(!empty($laboratoryIndicator) && $laboratoryIndicator->show_status == App\Models\LaboratoryIndicator\LaboratoryIndicator::STATUS_ACTIVE) || $saveMode == 'add' ?  ' selected="selected"' : ''}}>{{trans('core.base.label.status.active')}}</option>
                            <option value="{{App\Models\LaboratoryIndicator\LaboratoryIndicator::STATUS_INACTIVE}}"{{!empty($laboratoryIndicator) &&  $laboratoryIndicator->show_status == App\Models\LaboratoryIndicator\LaboratoryIndicator::STATUS_INACTIVE ? ' selected="selected"': ''}}>{{trans('core.base.label.status.inactive')}}</option>
                        </select>
                        <div class="form-error" id="form-error-show_status"></div>
                    </div>
                </div>

            </div>
            <input type="hidden" value="{{$laboratoryIndicator->id or 0}}" name="id" class="mc-form-key"/>

            {!! csrf_field() !!}
        </div>
    </form>

@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script src="{{asset('js/laboratory-indicator/main.js')}}"></script>
@endsection