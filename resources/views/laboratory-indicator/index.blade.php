@extends('layouts.app')

@section('title'){{trans('swis.laboratory_indicator.title')}}@stop

@section('contentTitle') {{trans('swis.laboratory_indicator.title')}} @stop

<?php

$jsTrans->addTrans([
    "swis.laboratory_indicator.hours.label",
    "swis.laboratory_indicator.days.label",
]);

?>

@section('form-buttons-top')
    {!! renderAddButton() !!}
@stop

@section('content')
    <form class="form-horizontal fill-up" id="search-filter" autocomplete="off">
        <div class="form-group">
            <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.laboratory_indicator.select_laboratory')}}</label>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-lg-10 col-md-8 field__one-col">
                        <select class="form-control select2 laboratory-list  default-search" name="laboratory_id">
                            <option></option>
                            @if (!empty($laboratories))
                                @foreach ($laboratories as $laboratory)
                                    <option value="{{ $laboratory->id }}">{{ !is_null($laboratory->current()) ? $laboratory->current()->name : '' }}</option>
                                @endforeach
                            @endif
                        </select>
                        <div class="form-error" id="form-error-laboratory_id"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="collapse" id="collapseSearch">
            <div class="card card-body">

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">ID</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="number" min="0" class="form-control onlyNumbers" name="id" id="" value="">
                                <div class="form-error" id="form-error-id"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8 col-lg-offset-3 text-right">
                        <div class="row">
                            <div class="col-lg-10 col-md-8">
                                <button type="submit"
                                        class="btn btn-primary mr-10">{{trans('core.base.label.search')}}</button>
                                <button type="button" class="btn btn-danger resetButton"
                                        data-submit="true">{{trans('core.base.label.reset')}}</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </form>

    <div class="data-table" style="display: none">
        <div class="clearfix">
            <button class="btn btn-success pull-right" type="button" data-toggle="collapse"
                    data-target="#collapseSearch" aria-expanded="false" aria-controls="collapseSearch">
                <i class="fa fa-search"> </i> {{trans('core.base.button.search')}}
            </button>

            <table class="table table-striped table-bordered table-hover dataTables-example" id="list-data">
                <thead>
                <tr>
                    <th data-col="actions" data-delete="0" class="th-actions">{{trans('core.base.label.actions')}}</th>
                    <th data-col="id">ID</th>
                    <th data-col="indicator">{{trans('swis.laboratory_indicator.indicator.label')}}</th>
                    <th data-col="price">{{trans('swis.laboratory_indicator.price.label')}}</th>
                    <th data-col="duration">{{trans('swis.laboratory_indicator.duration.label')}}</th>
                    <th data-col="date_time">{{trans('swis.laboratory_indicator.date_time.label')}}</th>
                    <th data-col="show_status">{{trans('core.base.label.status')}}</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('js/laboratory-indicator/main.js')}}"></script>
@endsection