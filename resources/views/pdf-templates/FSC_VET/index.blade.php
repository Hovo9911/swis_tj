<?php

use App\Models\ReferenceTable\ReferenceTable;

//----- Get fields values by MDM_ID
$mdmFields = getFieldsValueByMDMID($documentId, $customData);
list($mainDataBySafId, $productsDataBySafId, $productBatchesDataBySafId) = getSafDataFieldIdValue($saf, $subApplication, $subAppProducts, $availableProductsIds);

function productFontSize($productDescription)
{
    $countTextSymbols = mb_strlen($productDescription, 'UTF-8');
    switch (true) {
        case $countTextSymbols < 80:
            $fontSize = 10;
            break;
        case $countTextSymbols < 120:
            $fontSize = 7;
            break;
        case $countTextSymbols < 180:
            $fontSize = 6;
            break;
        default :
            $fontSize = 4;
            break;
    }

    return $fontSize;
}

//----- 1.
$subdivisionId = optional($subApplication)->agency_subdivision_id;
if (!is_null($subdivisionId)) {
    $subDivision = optional(getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME, $subdivisionId))->name;
}
$point1 = getMappingValueInMultipleRows($subDivision ?? '', 55, 100);

//----- 8.
$safID14 = (isset($mainDataBySafId['SAF_ID_14']) && !empty($mainDataBySafId['SAF_ID_14'])) ? $mainDataBySafId['SAF_ID_14'] . ', ' : '';
$safID15 = (isset($mainDataBySafId['SAF_ID_15']) && !empty($mainDataBySafId['SAF_ID_15'])) ? $mainDataBySafId['SAF_ID_15'] . ', ' : '';
$safID16 = (isset($mainDataBySafId['SAF_ID_16']) && !empty($mainDataBySafId['SAF_ID_16'])) ? $mainDataBySafId['SAF_ID_16'] . ', ' : '';
$safID17 = (isset($mainDataBySafId['SAF_ID_17']) && !empty($mainDataBySafId['SAF_ID_17'])) ? $mainDataBySafId['SAF_ID_17'] : '';

$point8 = $safID14 . $safID15 . $safID16 . $safID17;
$point8 = getMappingValueInMultipleRows($point8, 65, 90);

//----- 9.
$productCountries = $productsTable = [];
$productsCount = count($subAppProducts);

foreach ($subAppProducts as $subAppProduct) {

    $productCountries[] = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $subAppProduct['producer_country']))->name;

    //----- 3. ,4. ,5. ,6. ,7.
    $point3 = notEmptyForPdfFields($productsDataBySafId[$subAppProduct->id], 'SAF_ID_91');
    $point4 = notEmptyForPdfFields($customData, 'Veterinary_50_pr_' . $subAppProduct->id);
    $point5 = notEmptyForPdfFields($customData, 'Veterinary_51_pr_' . $subAppProduct->id);
    $point6 = notEmptyForPdfFields($customData, 'Veterinary_52_pr_' . $subAppProduct->id);
    $point7 = (!empty(notEmptyForPdfFields($customData, 'Veterinary_53_pr_' . $subAppProduct->id))) ? notEmptyForPdfFields($customData, 'Veterinary_53_pr_' . $subAppProduct->id) . ', ' : '';
    $point7 .= notEmptyForPdfFields($productsDataBySafId[$subAppProduct->id], 'SAF_ID_99');

    $productsTable[] = '<tr>
                    <td style="font-size: 11px;width:200px;font-size:' . productFontSize($point3) . 'px">' . $point3 . '</td>
                    <td style="font-size: 11px;width:50px;">' . $point4 . '</td>
                    <td style="font-size: 11px;width:70px;">' . $point5 . '</td>
                    <td style="font-size: 11px;width:70px;">' . $point6 . '</td>
                    <td style="font-size: 11px;width:220px;">' . $point7 . '</td>
                </tr>';
}


$emptyRow = '<tr>
                    <td style="font-size: 11px;width:200px;"></td>
                    <td style="font-size: 11px;width:50px;"></td>
                    <td style="font-size: 11px;width:70px;"></td>
                    <td style="font-size: 11px;width:70px;"></td>
                    <td style="font-size: 11px;width:220px;"></td>
                </tr>';
$productCountries = array_unique($productCountries);
$productCountriesList = implode(', ', $productCountries);
$point9 = getMappingValueInMultipleRows($productCountriesList, 90, 90);

//----- 10.
$point10 = getMappingValueInMultipleRows($mdmFields[431] ?? '', 40, 90);

//----- 11.
$point11 = getMappingValueInMultipleRows($mdmFields[25] ?? '', 90, 90);

//----- 13.
$point13 = getMappingValueInMultipleRows($mainDataBySafId['SAF_ID_73'] ?? '', 60, 90);

//----- 14.
$transitCountries = $saf->data['saf']['transportation']['transit_country'] ?? '';

foreach ($transitCountries as $transitCountry) {
    if (!is_null($transitCountry)) {
        $transCountries[] = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $transitCountry))->name;
    }
}
if (!empty($transCountries)) {
    $transCountries = array_unique($transCountries);
    $point14 = implode(', ', $transCountries);
}

//----- 16.
$safID6 = (isset($mainDataBySafId['SAF_ID_6']) && !empty($mainDataBySafId['SAF_ID_6'])) ? $mainDataBySafId['SAF_ID_6'] . ', ' : '';
$safID7 = (isset($mainDataBySafId['SAF_ID_7']) && !empty($mainDataBySafId['SAF_ID_7'])) ? $mainDataBySafId['SAF_ID_7'] . ', ' : '';
$safID8 = (isset($mainDataBySafId['SAF_ID_8']) && !empty($mainDataBySafId['SAF_ID_8'])) ? $mainDataBySafId['SAF_ID_8'] . ', ' : '';
$safID9 = (isset($mainDataBySafId['SAF_ID_9']) && !empty($mainDataBySafId['SAF_ID_9'])) ? $mainDataBySafId['SAF_ID_9'] : '';

$point16 = $safID6 . $safID7 . $safID8 . $safID9;
$point16 = getMappingValueInMultipleRows($point16, 60, 90);

//----- 17.
$safID75 = (isset($mainDataBySafId['SAF_ID_75']) && !empty($mainDataBySafId['SAF_ID_75'])) ? $mainDataBySafId['SAF_ID_75'] . ' - ' : '';

$subApplicationSafData = $subApplication->data;
$safTransportationStateNumberCustomSupports = $subApplicationSafData['saf']['transportation']['state_number_customs_support'] ?? '';

foreach ($safTransportationStateNumberCustomSupports as $safTransportationStateNumberCustomSupport) {
    if (!is_null($safTransportationStateNumberCustomSupport)) {

        $customsSupport[] = ($safTransportationStateNumberCustomSupport['state_number'] ?? '') . ' ' . ($safTransportationStateNumberCustomSupport['customs_support'] ?? '');
    }
}

$customsSupportList = (count($customsSupport)) ? implode(', ', $customsSupport) : '';

$point17 = $safID75 . $customsSupportList;
$point17 = getMappingValueInMultipleRows($point17, 70, 90);

//----- 33. ,34. ,35.
if (isset($mdmFields['355']) && !empty($mdmFields['355'])) {
    $dateFormat = strtr($mdmFields['355'], '/', '-');
    $time = strtotime($dateFormat);
    $point33 = date('d', $time);
    $point34 = month(date('m', $time));
    $point35 = date('y', $time);
}

//----- 36.
$fieldId_420 = (isset($mdmFields['420']) && !empty($mdmFields['420'])) ? $mdmFields['420'] . ', ' : '';
$fieldId_382 = $mdmFields['382'] ?? '';
$point36 = $fieldId_420 . $fieldId_382;
$point36 = getMappingValueInMultipleRows($point36, 50, 90);

?>
<table cellspacing="0" cellpadding="1" border="0">
    <tr>
        <td style="height:300px"></td>
    </tr>
</table>
<table cellspacing="0" cellpadding="0">
    <tr>
        <td style="font-size: 11px;font-weight: bold;width:70%">Управление Комитета на госгранице и транспорте</td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight: bold;width:42%">Board of Committee at the state border
            and transport
        </td>
        <td style="border-bottom: 1px solid #000000;font-size: 11px;width:58%;line-height: 1.6;">{{$point1[0] ?? ''}}
        </td>
    </tr>
    @if(count($point1)>1)
        @for($i=1;$i<count($point1);$i++)
            <tr>
                <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:100%;line-height:1.4">
                    {{$point1[$i]}}
                </td>
            </tr>
        @endfor
    @else
        <tr>
            <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:100%;line-height:2.0">
            </td>
        </tr>
    @endif
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight: bold;width:100%">Количество животных</td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight: bold;width:16%">Number of animals</td>
        <td style="border-bottom: 1px solid #000000;font-size: 11px;width:84%;">{{$mdmFields[429] ?? ''}}</td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="width:100%">
            <table cellpadding="2" border="1">
                <tr>
                    <td style="font-size: 11px;font-weight: bold;width:200px;text-align: center;">Вид животного «*» <br>Species.
                        Kind «*»
                    </td>
                    <td style="font-size: 11px;font-weight: bold;width:50px;text-align: center;">Пол<br>sex</td>
                    <td style="font-size: 11px;font-weight: bold;width:70px;text-align: center;">Порода<br>breed</td>
                    <td style="font-size: 11px;font-weight: bold;width:70px;text-align: center;">Возраст<br>age</td>
                    <td style="font-size: 11px;font-weight: bold;width:220px;text-align: center;">Ушная метка, клеймо,
                        кличка, вес<br>ear mark, brand, name, weight
                    </td>
                </tr>
                @for($i=0;$i<5;$i++)
                    {!! $productsTable[$i] ?? $emptyRow !!}
                @endfor
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight:bold;">При перевозке более 5 животных составляется опись животных,
            которая подписывается ветеринарным врачом Управление Комитета на госгранице и транспорте и является
            неотъемлемой частью данного сертификата.<br>The inventory is made, if more than 5 animals are shipped, it is
            signed by the veterinarian from Board of Committee at the state border and transport and constitute an
            integral part of this certificate.
        </td>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight:bold;">«*» Улей с пчелами(пчелиная семья), прелопакеты (сотовые,
            бессотовые), пчелиные матки и пр.
            <br>A have with bees (bee family), bee parcels (honeycombs, unhoneycombs), bee queens etc.
        </td>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight:bold;text-align:center">1. Происхождения животных/ Origin of the
            animals
        </td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight: bold;width:70%">Наименование и адрес экспортера</td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight: bold;width:24%">Name and address of exporter
        </td>
        <td style="border-bottom: 1px solid #000000;font-size: 11px;width:76%;">{{$point8[0] ?? ''}}</td>
    </tr>
    @if(count($point8) >1)
        @for($i=1;$i<count($point8);$i++)
            <tr>
                <td style="border-bottom: 1px solid #000000;font-size: 11px;width:100%">{{$point8[$i]}}</td>
            </tr>
        @endfor
    @else
        <tr>
            <td style="border-bottom: 1px solid #000000;width:100%"></td>
        </tr>
    @endif
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight:bold;width:100%">Место происхождения животных (место рождения или
            приобретение животных страна, область,район)<br>Place of origin of the animals (place of birth or
            acquirement of the animals country, region, district)
        </td>
        <td></td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;font-size: 11px;width:100%">{{$point9[0] ?? ''}}</td>
    </tr>
    @if(count($point9) >1)
        @for($i=1;$i<count($point9);$i++)
            <tr>
                <td style="border-bottom: 1px solid #000000;font-size: 11px;width:100%">{{$point9[$i]}}</td>
            </tr>
        @endfor
    @endif
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight: bold;width:70%">Животные находились в Республике Таджикистан</td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight: bold;width:42%">Animals have been in the Republic of the Tajikistan
        </td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:58%;">{{$point10[0]??''}}
        </td>
    </tr>
    @if(count($point10) >1)
        @for($i=1;$i<count($point10);$i++)
            <tr>
                <td style="border-bottom: 1px solid #000000;font-size: 11px;width:100%">{{$point10[$i]}}</td>
            </tr>
        @endfor
    @endif
    <tr>
        <td style="font-size: 13px;line-height: 1.4;text-align:left;width:45%;line-height: 1.4;"></td>
        <td style="font-size: 8px;text-align:right;line-height: 1.4;width:55%">
            <span>(с рождения или не менее 6 мес./from birth or not less than 6 months)</span>
        </td>
    </tr>
</table>


<----   2-nd page    ----->

<table pagebreak="true">
    <tr>
        <td style="font-size: 11px;font-weight: bold;width:100%">(Для диких животных указать место отлова/For wild
            animals indicate the place of capture)
        </td>
        <td></td>
    </tr>
    @for($i=0;$i<count($point11);$i++)
        <tr>
            <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:100%;">
                {{$point11[$i]}}
            </td>
        </tr>
    @endfor
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight: bold;width:100%">Место карантинирования</td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight: bold;width:16%">Place of quarantine</td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:84%;">{{$mdmFields[26] ?? ''}}</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight:bold;text-align:center;width:100%;">2. Направление животных/Information
            about destination
        </td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight: bold;width:100%;">Страна Назначения</td>
        <td style="line-height:0;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight: bold;width:18%;line-height:1.4;">Country of destination</td>
        <td style="border-bottom: 1px solid #000000;font-size: 11px;width:82%;">{{$mainDataBySafId['SAF_ID_79'] ?? ''}}</td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight: bold;width:100%;">Страна транзита</td>
        <td style="line-height:0;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight: bold;width:15%;line-height:1.4;">Country of transit</td>
        <td style="border-bottom: 1px solid #000000;font-size: 11px;width:85%;">{{$point14 ?? ''}}</td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight: bold;width:100%;">Пункт пересечения границы</td>
        <td style="line-height:0;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight: bold;width:22%;line-height:1.4;">Point of crossing the border</td>
        <td style="border-bottom: 1px solid #000000;font-size: 11px;width:78%;">{{$point13[0] ?? ''}}</td>
    </tr>
    @if(count($point13)>1)
        @for($i=1;$i<count($point13);$i++)
            <tr>
                <td style="border-bottom: 1px solid #000000;font-size: 11px;width:100%;line-height:1.4;">{{$point13[$i] ?? ''}}</td>
            </tr>
        @endfor
    @endif
    <tr>
        <td style="font-size: 11px;font-weight: bold;width:100%;">Наименование и адрес получателя</td>
        <td style="line-height:0;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight: bold;width:24%;line-height:1.4;">Name and address of consignee</td>
        <td style="border-bottom: 1px solid #000000;font-size: 11px;width:76%;">{{$point16[0] ??''}}</td>
    </tr>
    @if(count($point16)>1)
        @for($i=1;$i<count($point16);$i++)
            <tr>
                <td style="border-bottom: 1px solid #000000;font-size: 11px;width:100%;">{{$point16[$i] ?? ''}}</td>
            </tr>
        @endfor
    @endif
    <tr>
        <td style="font-size: 11px;font-weight: bold;width:100%;">Транспорт</td>
        <td style="line-height:0;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight: bold;width:15%;line-height:1.4;">Means of transport</td>
        <td style="border-bottom: 1px solid #000000;font-size: 11px;width:85%;">{{$point17[0] ?? ''}}</td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:30%;"></td>
        <td style="font-size: 8px;text-align:center;width:70%">
            <span>(указать N вагона автомашины, рейс самолета., судна/</span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:100%;border-bottom: 1px solid #000000;">{{$point17[1] ?? ''}}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;width:100%;line-height:1.4;">
            <span>specify the number of the wagon, truck, fight-number, name of the shih)</span>
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight:bold;width:100%;line-height:1.4">
            3. Я, нижеподписавшийся Государственный ветеринарный врач Республики Таджикистан, удостоверяю, что
            вышеуказанные животные прошли дневный карантин с ежедневным клиническим осмотром, не имели контакта с
            другими животными, обследованы в день выдачи сертификата и не имеют клинических признаков инфекционных
            болезней.<br>
            I, the undersigned veterinarian of the Government of the Republic of Tajikistan certify, that abovementioned
            animals, were placed in <u>{{$mdmFields[44] ?? ''}}</u>-days quarantine with daily clinical examinations,
            have no contacts with the other
            animals, were examined at the day of issue of the certificate and showed no clinical symptoms of the
            infectious diseases.
        </td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight:bold;width:100%">Транспорные средства очищены и продезинфицированы
            принятыми в Республике Таджикистан методами и средствами.<br>Means of transport have been cleaned and
            disinfected by the methods and means, approved in the Republic of Tajikistan.
        </td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight:bold;width:100%">Животные выходят из местности, где не
            регистрировались:<br>The animals came from the locate free from:
        </td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;width: 50%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;">в течении последних</td>
        <td style="font-size: 8px;text-align:center;width:25%;border-bottom: 1px solid #000000;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:center;width:50%;font-weight:bold;"></td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;">during the last</td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;"></td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;width: 50%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;">в течении последних</td>
        <td style="font-size: 8px;text-align:center;width:25%;border-bottom: 1px solid #000000;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:center;width:50%;font-weight:bold;"></td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;">during the last</td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;"></td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;width: 50%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;">в течении последних</td>
        <td style="font-size: 8px;text-align:center;width:25%;border-bottom: 1px solid #000000;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:center;width:50%;font-weight:bold;"></td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;">during the last</td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;"></td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;width: 50%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;">в течении последних</td>
        <td style="font-size: 8px;text-align:center;width:25%;border-bottom: 1px solid #000000;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:center;width:50%;font-weight:bold;"></td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;">during the last</td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;"></td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;width: 50%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;">в течении последних</td>
        <td style="font-size: 8px;text-align:center;width:25%;border-bottom: 1px solid #000000;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:center;width:50%;font-weight:bold;"></td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;">during the last</td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;"></td>
    </tr>
</table>
<table pagebreak="true">
    <tr>
        <td style="font-size: 11px;font-weight:bold;width:100%">Животные выходят из хозяйств, где не
            регистрировались:<br>The animals came from the premises free from:
        </td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;width: 50%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;">в течении последних</td>
        <td style="font-size: 8px;text-align:center;width:25%;border-bottom: 1px solid #000000;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:center;width:50%;font-weight:bold;"></td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;">during the last</td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;"></td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;width: 50%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;">в течении последних</td>
        <td style="font-size: 8px;text-align:center;width:25%;border-bottom: 1px solid #000000;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:center;width:50%;font-weight:bold;"></td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;">during the last</td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;"></td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;width: 50%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;">в течении последних</td>
        <td style="font-size: 8px;text-align:center;width:25%;border-bottom: 1px solid #000000;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:center;width:50%;font-weight:bold;"></td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;">during the last</td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;"></td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;width: 50%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;">в течении последних</td>
        <td style="font-size: 8px;text-align:center;width:25%;border-bottom: 1px solid #000000;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:center;width:50%;font-weight:bold;"></td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;">during the last</td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;"></td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;width: 50%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;">в течении последних</td>
        <td style="font-size: 8px;text-align:center;width:25%;border-bottom: 1px solid #000000;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:center;width:50%;font-weight:bold;"></td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;">during the last</td>
        <td style="font-size: 11px;text-align:center;width:25%;font-weight:bold;"></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight:bold;width:100%">Животные в период дневного карантина исследовались в
            государственной ветеринарной лаборатории, имеющей разрешение на такие исследования, с отрицательным
            результатом на:<br>Animals during__days quarantine were examined in the State Veterinary Laboratory,
            licensed for conducting such examination, with negative results for:
        </td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;width: 60%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:10%;font-weight:bold;">«____»</td>
        <td style="font-size: 8px;text-align:center;width:20%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:10%;font-weight:bold;">20____г.</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;width: 60%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:10%;font-weight:bold;">«____»</td>
        <td style="font-size: 8px;text-align:center;width:20%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:10%;font-weight:bold;">20____г.</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;width: 60%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:10%;font-weight:bold;">«____»</td>
        <td style="font-size: 8px;text-align:center;width:20%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:10%;font-weight:bold;">20____г.</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;width: 60%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:10%;font-weight:bold;">«____»</td>
        <td style="font-size: 8px;text-align:center;width:20%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:10%;font-weight:bold;">20____г.</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;width: 60%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:10%;font-weight:bold;">«____»</td>
        <td style="font-size: 8px;text-align:center;width:20%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:10%;font-weight:bold;">20____г.</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;width: 60%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:10%;font-weight:bold;">«____»</td>
        <td style="font-size: 8px;text-align:center;width:20%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:10%;font-weight:bold;">20____г.</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;width: 60%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:10%;font-weight:bold;">«____»</td>
        <td style="font-size: 8px;text-align:center;width:20%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:10%;font-weight:bold;">20____г.</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight:bold;width:100%">Проведена вакцанация против:<br>Animals were vaccinated
            against:
        </td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;width: 60%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:10%;font-weight:bold;">«____»</td>
        <td style="font-size: 8px;text-align:center;width:20%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:10%;font-weight:bold;">20____г.</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;width: 60%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:10%;font-weight:bold;">«____»</td>
        <td style="font-size: 8px;text-align:center;width:20%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:10%;font-weight:bold;">20____г.</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;width: 60%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:10%;font-weight:bold;">«____»</td>
        <td style="font-size: 8px;text-align:center;width:20%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:10%;font-weight:bold;">20____г.</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;width: 60%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:10%;font-weight:bold;">«____»</td>
        <td style="font-size: 8px;text-align:center;width:20%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:10%;font-weight:bold;">20____г.</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;width: 60%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:10%;font-weight:bold;">«____»</td>
        <td style="font-size: 8px;text-align:center;width:20%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:10%;font-weight:bold;">20____г.</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight:bold;width:100%">Животные обработаны против паразитов:<br>Animals were
            treated parasites:
        </td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;width: 60%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:10%;font-weight:bold;">«____»</td>
        <td style="font-size: 8px;text-align:center;width:20%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:10%;font-weight:bold;">20____г.</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;width: 60%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:10%;font-weight:bold;">«____»</td>
        <td style="font-size: 8px;text-align:center;width:20%;border-bottom: 1px solid #000000;"></td>
        <td style="font-size: 11px;text-align:center;width:10%;font-weight:bold;">20____г.</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight:bold;width:100%">Корма и другие сопровождающие грузы происходят
            непосредственно из хозайства-экспортера и не контаминированы возбудителями инфекционных болезней
            животных.<br>The Feed and other accompanying things are originated from export premises and not contaminated
            with pathogenetic organisms.
        </td>
        <td></td>
    </tr>
</table>
<table pagebreak="true">
    <tr>
        <td style="font-size: 11px;width:110px">Составлено/Made on</td>
        <td style="font-size: 11px;text-align:center;width:40px;border-bottom: 1px solid #000000;">«{{$point33 ?? ''}}
            »
        </td>
        <td style="font-size: 11px;width:2px"></td>
        <td style="font-size: 11px;text-align:center;width:240px;border-bottom: 1px solid #000000;">{{$point34 ?? ''}}</td>
        <td style="font-size: 11px;text-align:center;width:18px;">20</td>
        <td style="font-size: 11px;width:14px;border-bottom: 1px solid #000000;">{{$point35 ?? ''}}</td>
        <td style="font-size: 11px;width:10px;">г.</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;;width:26%;">Ветеринарный врач / Veterinarian</td>
        <td style="border-bottom: 1px solid #000000;font-size: 11px;width:74%;">{{$point36[0]}}</td>
    </tr>
    @if(count($point36) >1)
        @for($i=1;$i<count($point36);$i++)
            <tr>
                <td style="border-bottom: 1px solid #000000;font-size: 11px;width:100%;">{{$point36[$i]}}</td>
            </tr>
        @endfor
    @endif
    <tr>
        <td style="font-size: 13px;line-height: 1.4;text-align:left;width:35%;line-height: 1.4;"></td>
        <td style="font-size: 8px;text-align:center;line-height: 1.4;width:65%">
            <span>(должность,фамилия / title,name)</span>
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;;width:16%;">Подпись / Signature:</td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:84%;"></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;;width:35%;">Печать / Stamp</td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:100%">Сертификат действителен при возвращении в Республику Таджикистан из
            зарубежных стран животных в течении 90 дней момента выдачи без проведения дополнительных исследований и
            обработок при условии, что животные не находились в местах, где имелись вспышки инфекционных болезней, что
            должно быть подтверждено государственной ветеринарной службой этих стран.
        </td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:100%">The certificate is valid for returning into the Republic of Tajikistan
            from the foreign countries of animals within 90 days after issuing without additional testing and treatment,
            if the animals have not been in the localities, where infectious diseases were registered, what must be
            confirmed by the State Veterinary Service of these countries.
        </td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:100%">Сертификат считается недействителным, если животные в течение 24 часов
            после его подписания не были погружены на транспортное средство.
        </td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:100%">The certificate is not valid, if the animals were not loaded on the
            vehicle during 24 hours after the signing.
        </td>
        <td></td>
    </tr>
</table>
@if($productsCount>5)
    <table pagebreak="true" cellpadding="2" border="1">
        <tr>
            <td style="font-size: 11px;font-weight: bold;width:200px;text-align: center;">Вид животного «*» <br>Species.
                Kind «*»
            </td>
            <td style="font-size: 11px;font-weight: bold;width:50px;text-align: center;">Пол<br>sex</td>
            <td style="font-size: 11px;font-weight: bold;width:70px;text-align: center;">Порода<br>breed</td>
            <td style="font-size: 11px;font-weight: bold;width:70px;text-align: center;">Возраст<br>age</td>
            <td style="font-size: 11px;font-weight: bold;width:220px;text-align: center;">Ушная метка, клеймо,
                кличка, вес<br>ear mark, brand, name, weight
            </td>
        </tr>
        @for($i=5;$i<$productsCount;$i++)
            {!! $productsTable[$i] ?? '' !!}
        @endfor
    </table>
@endif



