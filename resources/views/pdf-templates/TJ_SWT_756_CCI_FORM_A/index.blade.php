<?php

use App\Models\BaseModel;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\RegionalOffice\RegionalOffice;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use App\Models\Document\Document;
use App\Models\ConstructorTemplates\ConstructorTemplates;
use App\Http\Controllers\Core\Dictionary\DictionaryManager;

function getColumnsMaxRowsCountForPrintForms($array)
{

    if (!empty($array)) {
        $maxCount = 1;
        foreach ($array as $value) {
            $countTextSymbol = count(str_split($value['text'], $value['rowStringLength']));
            if ($countTextSymbol > $maxCount) {
                $maxCount = $countTextSymbol;
            }
        }
        return $maxCount;
    }
    return 0;
}

//tableRows.png lines left
//53
//136
//411
//502
//608
// image size 692 395

$dictionaryManager = new DictionaryManager();

$languageCode = cLng();

if ($languageCode != BaseModel::LANGUAGE_CODE_EN) {
    $languageCode = ConstructorTemplates::LANGUAGE_CODE_RU;
}

function getTransCustom($dictionaryManager, $key, $lngCode, $concatType = '<br>')
{
    if ($lngCode == BaseModel::LANGUAGE_CODE_EN) {
        return $dictionaryManager->getTransByLngCode($key, $lngCode);
    } else {
        return $dictionaryManager->getTransByLngCode($key, BaseModel::LANGUAGE_CODE_EN) . $concatType . $dictionaryManager->getTransByLngCode($key, ConstructorTemplates::LANGUAGE_CODE_RU);
    }
}

$gerbImagePath = '/image/gerb_' . env('COUNTRY_MODE') . '_sm_2.png';
$swisImagePath = '/image/tj_swis_logo.png';

$mdmFields = getFieldsValueByMDMID($documentId, $customData, cLng());

// ----- 1.
$safSideNameExportName = $saf->data['saf']['sides']['export']['name'] ?? '';

$safSidesExportCommunity = $saf->data['saf']['sides']['export']['community'] ?? '';
$safSidesExportCommunity = ($safSidesExportCommunity == '-') ? '' : $safSidesExportCommunity;

$safSidesExportAddress = $saf->data['saf']['sides']['export']['address'] ?? '';
$safSidesExportAddress = ($safSidesExportAddress == '-') ? '' : $safSidesExportAddress;

$safSidesExportCountry = $saf->data['saf']['sides']['export']['country'] ?? '';
$safSidesExportCountryName = !empty($safSidesExportCountry) ? optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safSidesExportCountry, false, [], 'get', false, false, [], false, $languageCode))->name : '';

$safSidesExportInfo = $safSideNameExportName .
    ', ' . $safSidesExportCommunity .
    ', ' . $safSidesExportAddress .
    ', ' . $safSidesExportCountryName;

if ($languageCode == BaseModel::LANGUAGE_CODE_EN) {
    $safSidesExportInfo = mb_strlen($safSidesExportInfo, 'UTF-8') > 630 ? mb_substr($safSidesExportInfo, 0, 630, 'UTF-8') . "..." : $safSidesExportInfo;
} else {
    $safSidesExportInfo = mb_strlen($safSidesExportInfo, 'UTF-8') > 445 ? mb_substr($safSidesExportInfo, 0, 445, 'UTF-8') . "..." : $safSidesExportInfo;
}

// ----- 2.
$safSideNameImportName = $saf->data['saf']['sides']['import']['name'] ?? '';

$safSidesImportCommunity = $saf->data['saf']['sides']['import']['community'] ?? '';
$safSidesImportCommunity = ($safSidesImportCommunity == '-') ? '' : $safSidesImportCommunity;

$safSidesImportAddress = $saf->data['saf']['sides']['import']['address'] ?? '';
$safSidesImportAddress = ($safSidesImportAddress == '-') ? '' : $safSidesImportAddress;

$safSidesImportCountry = $saf->data['saf']['sides']['import']['country'] ?? '';
$safSidesImportCountryName = !empty($safSidesImportCountry) ? optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safSidesImportCountry, false, [], 'get', false, false, [], false, $languageCode))->name : '';

$safSidesImportInfo = $safSideNameImportName .
    ', ' . $safSidesImportCommunity .
    ', ' . $safSidesImportAddress .
    ', ' . $safSidesImportCountryName;

if ($languageCode == BaseModel::LANGUAGE_CODE_EN) {
    $safSidesImportInfo = mb_strlen($safSidesImportInfo, 'UTF-8') > 478 ? mb_substr($safSidesImportInfo, 0, 478, 'UTF-8') . "..." : $safSidesImportInfo;
} else {
    $safSidesImportInfo = mb_strlen($safSidesImportInfo, 'UTF-8') > 312 ? mb_substr($safSidesImportInfo, 0, 312, 'UTF-8') . "..." : $safSidesImportInfo;
}

// ----- 3.
$transportType = $saf->data['saf']['transportation']['type_of_transport'] ?? '';

if ($transportType) {
    $transportType = optional(getReferenceRows(ReferenceTable::REFERENCE_TRANSPORT_TYPE, $transportType, false, [], 'get', false, false, [], false, $languageCode))->name;
}

$transitCountries = $saf->data['saf']['transportation']['transit_country'] ?? array();
$transCountriesList = '';

foreach ($transitCountries as $transitCountry) {
    if (!is_null($transitCountry)) {
        $transCountries[] = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $transitCountry, false, [], 'get', false, false, [], false, $languageCode))->name;
    }
}

if (!empty($transCountries)) {
    $transCountriesList = ', ' . implode(', ', $transCountries);
}

$stateNumberCustomsSupport = $saf->data['saf']['transportation']['state_number_customs_support'] ?? [];
$stateNumberCustomsSupportStateNumber = [];

foreach ($stateNumberCustomsSupport as $value) {
    $stateNumberCustomsSupportStateNumber[] = $value['state_number'];
}
$stateNumberCustomsSupportStateNumber = implode(', ', $stateNumberCustomsSupportStateNumber);
$transportCountriesListTransitCountry = $stateNumberCustomsSupportStateNumber . $transCountriesList . ', ' . $transportType;

if ($languageCode == BaseModel::LANGUAGE_CODE_EN) {
    $transportCountriesListTransitCountry = mb_strlen($transportCountriesListTransitCountry, 'UTF-8') > 793 ? mb_substr($transportCountriesListTransitCountry, 0, 793, 'UTF-8') . "..." : $transportCountriesListTransitCountry;
} else {
    $transportCountriesListTransitCountry = mb_strlen($transportCountriesListTransitCountry, 'UTF-8') > 571 ? mb_substr($transportCountriesListTransitCountry, 0, 571, 'UTF-8') . "..." : $transportCountriesListTransitCountry;
}

// -----
$mdmId506 = $mdmFields['506'] ?? '';

if ($languageCode == BaseModel::LANGUAGE_CODE_EN) {
    $mdmId506 = mb_strlen($mdmId506, 'UTF-8') > 793 ? mb_substr($mdmId506, 0, 793, 'UTF-8') . "..." : $mdmId506;
} else {
    $mdmId506 = mb_strlen($mdmId506, 'UTF-8') > 571 ? mb_substr($mdmId506, 0, 571, 'UTF-8') . "..." : $mdmId506;
}

// ----- 4.->2.
$fieldIDCCI_001_5 = !is_null($customData) ? !empty($customData['CCI_001_5']) && gettype($customData['CCI_001_5']) == 'array' ? $customData['CCI_001_5']['value'] : $customData['CCI_001_5'] ?? '' : '';

// ----- 4.->3.
$keyFixText1_1 = $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.fix_text_1.1", BaseModel::LANGUAGE_CODE_RU);
$keyFixText1_2 = $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.fix_text_1.2", BaseModel::LANGUAGE_CODE_RU);
if ($languageCode == BaseModel::LANGUAGE_CODE_EN) {
    $keyFixText1_1 = $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.fix_text_1.1", BaseModel::LANGUAGE_CODE_EN);
    $keyFixText1_2 = $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.fix_text_1.2", BaseModel::LANGUAGE_CODE_EN);
}
$keyFixText1 = $keyFixText1_1 . '<br>' . $keyFixText1_2;

// ----- 4.->4., 12.->1.
$safTransportationExportCountry = $saf->data['saf']['transportation']['export_country'] ?? '';
$safTransportationExportCountryName = '';

if (!empty($safTransportationExportCountry)) {
    $safTransportationExportCountry = getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safTransportationExportCountry, false, [], 'get', false, false, [], false, $languageCode);
    $safTransportationExportCountryName = optional($safTransportationExportCountry)->name;
}

if ($languageCode == BaseModel::LANGUAGE_CODE_EN) {
    $safTransportationExportCountryName = mb_strlen($safTransportationExportCountryName, 'UTF-8') > 60 ? mb_substr($safTransportationExportCountryName, 0, 60, 'UTF-8') . "..." : $safTransportationExportCountryName;
} else {
    $safTransportationExportCountryName = mb_strlen($safTransportationExportCountryName, 'UTF-8') > 47 ? mb_substr($safTransportationExportCountryName, 0, 47, 'UTF-8') . "..." : $safTransportationExportCountryName;
}

// ----- 4.->5.
$safTransportationImportCountry = $saf->data['saf']['transportation']['import_country'] ?? '';
$safTransportationImportCountryName = '';
if (!empty($safTransportationImportCountry)) {
    $safTransportationImportCountry = getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safTransportationImportCountry, false, [], 'get', false, false, [], false, $languageCode);
    $safTransportationImportCountryName = optional($safTransportationImportCountry)->name;
}

if ($languageCode == BaseModel::LANGUAGE_CODE_EN) {
    $safTransportationImportCountryName = mb_strlen($safTransportationImportCountryName, 'UTF-8') > 62 ? mb_substr($safTransportationImportCountryName, 0, 62, 'UTF-8') . "..." : $safTransportationImportCountryName;
} else {
    $safTransportationImportCountryName = mb_strlen($safTransportationImportCountryName, 'UTF-8') > 49 ? mb_substr($safTransportationImportCountryName, 0, 49, 'UTF-8') . "..." : $safTransportationImportCountryName;
}

// ----- 5.
$fieldIDCCI_001_17 = !is_null($customData) ? !empty($customData['CCI_001_17']) && gettype($customData['CCI_001_17']) == 'array' ? $customData['CCI_001_17']['value'] : $customData['CCI_001_17'] ?? '' : '';

// ----- 11.->1.
$keyFixText2 = $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.fix_text_2", BaseModel::LANGUAGE_CODE_EN);

// ----- 11.->2.
$agencyName = $agency->current()->name;

// ----- 11.->3.
$subdivisionId = optional($subApplication)->agency_subdivision_id;
$subDivisionCode = optional(getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME, $subdivisionId, false))->code;

$regionalOffice = RegionalOffice::select('regional_office_ml.address', 'phone_number', 'regional_office_ml.name')
    ->leftJoin('regional_office_ml', 'regional_office.id', '=', 'regional_office_ml.regional_office_id')
    ->where(['lng_id' => cLng('id'), 'office_code' => $subDivisionCode])
    ->active()
    ->first();

$regionalOfficeAddress = optional($regionalOffice)->address;

// ----- 11.->4.
$fieldIDCCI_001_8 = $customData['CCI_001_8'] ?? '';
$fieldIDCCI_001_8 = !empty($fieldIDCCI_001_8) ? date('d/m/Y', strtotime($customData['CCI_001_8'])) : '';

// ----- 11.->5.
$fieldIDCCI_001_6 = $customData['CCI_001_6'] ?? '';
$fieldIDCCI_001_6 = !empty($fieldIDCCI_001_6) ? date('d/m/Y', strtotime($customData['CCI_001_6'])) : '';

// ----- other pages
$keyFixText3_1 = $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.fix_text_3.1", BaseModel::LANGUAGE_CODE_EN);
$keyFixText3_2 = $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.fix_text_3.2", BaseModel::LANGUAGE_CODE_EN);

// ----- 12.->2.
$subApplicationSubmittingDate = date('d/m/Y', strtotime($subApplication->status_date)) ?? '';


// ----- 6.
$number = 1;

// ----- 7., 8., 9., 10. only $partData logic. if you want to use $subAppProducts create other foreach
//        @fixme delete
//$subAppProducts = \App\Models\SingleApplicationProducts\SingleApplicationProducts::limit(6)->orderBy('id', 'desc')->get();

$partData = '';
$partDataMaxRowsCount = 22;
$partDataRowsCount = 0;
$createOtherPage = false;
$keySubAppProduct = 0;
$countSubAppProducts = $subAppProducts->count();
$i = 1;

$xxx = '
        <tr>
            <td valign="top" width="1.4cm" height="1.2cm" class="tc fs9 pr5">xxxxxxxxxx</td>
            <td valign="top" width="2.2cm" height="1.2cm" class="tc fs9 pr5">xxxxxxxxxxxxxxxxx</td>
            <td valign="top" width="7.3cm" height="1.2cm" class="tc fs9 pr5">xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx</td>
            <td valign="top" width="2.4cm" height="1.2cm" class="tc fs9 pr5">xxxxxxxxxxxxxxxx</td>
            <td valign="top" width="2.8cm" height="1.2cm" class="tc fs9 pr5">xxxxxxxxxxxxxxxxxxxx</td>
            <td valign="top" width="2.2cm" height="1.2cm" class="tc fs9 pr5">xxxxxxxxxxxxxxxxxx</td>
        </tr>';

// ----- column-8 in table

$fieldIDCCI_001_20 = !is_null($customData) ? !empty($customData['CCI_001_20']) && gettype($customData['CCI_001_20']) == 'array' ? $customData['CCI_001_20']['value'] : $customData['CCI_001_20'] ?? '' : '';

foreach ($subAppProducts as $key => $subAppProduct) {

    // ----- 7.
    $productNumberOfPlaces = (int)$subAppProduct['number_of_places'];

    $productPlaceType = getReferenceRows(ReferenceTable::REFERENCE_PACKAGING_REFERENCE, $subAppProduct['places_type'], false, [], 'get', false, false, [], false, $languageCode);
    $productNumberTypeOfPlaces = ' ' . $productNumberOfPlaces . ' ' . optional($productPlaceType)->name;

    // ----- column-8 in table
    $fieldIDCCI_001_20 = notEmptyForPdfFields($customData, 'CCI_001_20_pr_' . $subAppProduct->id);

    // ----- 8.
    $productComercialDescription = ' ' . $subAppProduct->commercial_description;

    // ----- 9.
    $productBruttoWeight = $subAppProduct->brutto_weight ?? '';

    $productNettoWeight = $subAppProduct->netto_weight ?? '';

    $totalQuantity = optional($subAppProduct)->quantity; // SAF_ID_100

    $refMeasurementUintInfo = '<br>&nbsp;'; // SAF_ID_101
    $refMeasurementUint = getReferenceRows(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $subAppProduct['measurement_unit'], false, [], 'get', false, false, [], false, $languageCode);
    if (!empty(optional($refMeasurementUint)) && optional($refMeasurementUint)->code != ReferenceTable::REF_KILOGRAM_CODE) {
        $refMeasurementUintInfo .= '' . $totalQuantity . ' ' . optional($refMeasurementUint)->name;
    }

    $totalQuantity2 = optional($subAppProduct)->quantity_2; // SAF_ID_102

    $refMeasurementUint2Info = '<br>&nbsp;'; // SAF_ID_103
    $refMeasurementUint2 = getReferenceRows(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $subAppProduct['measurement_unit_2'], false, [], 'get', false, false, [], false, $languageCode);
    if (!empty($totalQuantity2)) {
        $refMeasurementUint2Info .= '' . $totalQuantity2 . ' ' . optional($refMeasurementUint2)->name;
    }

    $productBruttoNetto = '<span>' . $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.brutto_weight", BaseModel::LANGUAGE_CODE_EN) . ' ' . $productBruttoWeight
        . '<br>' . $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.netto_weight", BaseModel::LANGUAGE_CODE_EN) . ' ' . $productNettoWeight
        . $refMeasurementUintInfo
        . $refMeasurementUint2Info . '</span>';

    // ----- 10.
    $attachedDocumentProducts = SingleApplicationSubApplications::getAttachedDocumentProductsForPDF($subApplication, [$subAppProduct->id]);
    $documentInfo = [];
    foreach ($attachedDocumentProducts as $value) {
        if ($value->document->document_type == Document::DOCUMENT_CODE_FOR_TJ_SWT_756_CCI_FORM_A_PDF_TEMPLATE) {
            $documentInfo[] = $value->document->document_number . ' ' . $value->document->document_release_date;
        }
    }
    $documentInfo = implode(', ', $documentInfo);

    $partDataInfo = [
        'productNumberTypeOfPlaces' => [
            'text' => $productNumberTypeOfPlaces,
            'rowStringLength' => ($languageCode == BaseModel::LANGUAGE_CODE_EN) ? 18 : 14
        ],
        'productComercialDescription' => [
            'text' => $productComercialDescription,
            'rowStringLength' => ($languageCode == BaseModel::LANGUAGE_CODE_EN) ? 65 : 52
        ],
        'fieldIDCCI_001_20' => [
            'text' => $fieldIDCCI_001_20,
            'rowStringLength' => ($languageCode == BaseModel::LANGUAGE_CODE_EN) ? 19 : 15
        ],
        'productBruttoNetto' => [
            'text' => $productBruttoNetto,
            'rowStringLength' => ($languageCode == BaseModel::LANGUAGE_CODE_EN) ? 23 : 18
        ],
        'documentInfo' => [
            'text' => $documentInfo,
            'rowStringLength' => ($languageCode == BaseModel::LANGUAGE_CODE_EN) ? 21 : 17
        ]
    ];
    $partDataRowsCount += getColumnsMaxRowsCountForPrintForms($partDataInfo);

    if ($partDataRowsCount > $partDataMaxRowsCount) {
        $createOtherPage = true;
        $keySubAppProduct = $key;
        $partData .= $xxx;
        break;
    }

    // ----- 7., 8., 9., 10.
    $partData .= '
    <tr>
        <td valign="top" width="1.4cm" class="pr5 fs9 pl5">' . $number . '.</td>
        <td valign="top" width="2.2cm" class="pr5 fs9 pl5">' . $productNumberTypeOfPlaces . '</td>
        <td valign="top" width="7.3cm" class="pr5 fs9 pl5">' . $productComercialDescription . '</td>
        <td valign="top" width="2.4cm" class="pr5 fs9 pl5">' . $fieldIDCCI_001_20 . '</td>
        <td valign="top" width="2.8cm" class="pr5 fs9 pl5">' . $productBruttoNetto . '</td>
        <td valign="top" width="2.2cm" class="pr5 fs9 pl5">' . $documentInfo . '</td>
    </tr>';

    if (($partDataRowsCount <= $partDataMaxRowsCount) && ($countSubAppProducts == $i)) {

        $partDataEmptyRowsCount = $partDataMaxRowsCount - $partDataRowsCount;

        $partData .= $xxx;

    }

    $number++;
    $i++;
}

$partDataOtherPages = [];
$partDataOtherPagesRowsCount = $partDataOtherPagesEmptyRowsCount = 0;
$partDataMaxRowsCountOtherPages = 46;
$i = 0;

$mdmId365 = notEmptyForPdfFields($mdmFields, '365');

if ($createOtherPage) {

    $partDataOtherPagesFirstPart = '
    <pagebreak></pagebreak>

    <table>

        <tr>
            <td width="18.6cm" height="2.5cm" class="br2 bt2 bl2 tc" colspan="2">
                <span class="fs16">' . trans("swis.pdf_templates.{$template}.other_page.title1") . ' ' . $mdmId365 . '</span>
                <br>
                <span class="fs14">' . trans("swis.pdf_templates.{$template}.other_page.title2") . '</span>
            </td>
        </tr>

    </table>

    <table>

        <tr>
            <td valign="top" width="1.4cm" height="2.2cm" class="pl5 bt br fs11 bl2">5.' . getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.list.number", cLng()) . '</td>
            <td valign="top" width="2.2cm" height="2.2cm" class="pl5 bt br fs11">6.' . getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.list.count.type_of_package", cLng()) . '</td>
            <td valign="top" width="7.3cm" height="2.2cm" class="pl5 bt br fs11">7.' . getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.list.product_description", cLng()) . '</td>
            <td valign="top" width="2.4cm" height="2.2cm" class="pl5 bt br fs11">8.' . getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.list.origin_criterion", cLng()) . '</td>
            <td valign="top" width="2.8cm" height="2.2cm" class="pl5 bt br fs11">9.' . getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.list.weight.brutto_netto", cLng()) . '</td>
            <td valign="top" width="2.5cm" height="2.2cm" class="pl5 bt fs11 br2">10.' . getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.list.number_and_date", cLng()) . '</td>
        </tr>

    </table>

    <table style="background-image: url(\'/image/tableRows2.png\');">

        <tr>
            <td valign="top" width="18.6cm" height="15.8cm" class="bl2 br2">
                <table>
    ';

    $partDataOtherPagesLastPart = '

                </table>
            </td>
        </tr>
    </table>

    <table>

        <tr>
            <td valign="top" width="9.2cm" height="4.5cm" class="bt br bl2">
                <table>
                    <tr>
                        <td valign="top" width="0.8cm" class="fb fs12">&nbsp;&nbsp;&nbsp;11. </td>
                        <td valign="top" class="fb fs12">' . getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.certificate", cLng()) . '</td>
                    </tr>
                    <tr>
                        <td valign="top" width="0.8cm" class="fb fs12"></td>
                        <td valign="top" class="fb fs12">' . $keyFixText2 . '</td>
                    </tr>
                    <tr>
                        <td valign="top" width="0.8cm" class="fb fs12"></td>
                        <td valign="top" class="fs9">' . $agencyName . '</td>
                    </tr>
                    <tr>
                        <td valign="top" width="0.8cm" class="fb fs12"></td>
                        <td valign="top" class="fs9">' . $regionalOfficeAddress . '</td>
                    </tr>
                    <tr>
                        <td valign="top" width="0.8cm" class="fb fs12"></td>
                        <td valign="top" class="fs9">' . $fieldIDCCI_001_8 . '</td>
                    </tr>
                </table>
            </td>
            <td valign="top" width="9.4cm" height="4.5cm" class="br2 bt ">
                <table>
                    <tr>
                        <td valign="top" width="0.8cm" class="fb fs12">&nbsp;&nbsp;&nbsp;12. </td>
                        <td valign="top" class="fb fs12" colspan="2">' . getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.applicant_declaration", cLng()) . '</td>
                    </tr>
                    <tr>
                        <td valign="top" width="0.8cm" class="fb fs12"></td>
                        <td valign="top" class="fb fs11" colspan="2">' . $keyFixText3_1 . '</td>
                    </tr>
                    <tr>
                        <td valign="top" width="0.8cm" class="fb fs12"></td>
                        <td valign="top" width="7.8cm" class="fs9 bb tc">' . $safTransportationExportCountryName . '</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td valign="top" width="0.8cm" class="fb fs12"></td>
                        <td valign="top" width="7.8cm" class="fs7 tc">' . getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.list.country_name", cLng(), ', ') . '</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td valign="top" width="0.8cm" class="fb fs12"></td>
                        <td valign="top" class="fb fs11" colspan="2">' . $keyFixText3_2 . '</td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td valign="top" width="9.2cm" height="1.5cm" class="br bl2 bb2" style="padding-top: 0.3cm">
                <table>
                    <tr>
                        <td valign="top" width="0.8cm"></td>
                        <td width="2.5cm" class="tc fs9 bb"></td>
                        <td width="2.5cm" class="tc fs9 bb">' . $fieldIDCCI_001_6 . '</td>
                        <td width="2.5cm" class="tc fs9 bb"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td valign="top" width="0.8cm"></td>
                        <td width="2.5cm" class="tc fs9">' . getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.signature", cLng()) . '</td>
                        <td width="2.5cm" class="tc fs9">' . getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.date", cLng()) . '</td>
                        <td width="2.5cm" class="tc fs9">' . getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.seal", cLng()) . '</td>
                        <td></td>
                    </tr>
                </table>
            </td>
            <td valign="top" width="9.4cm" height="1.5cm" class="br2 bb2" style="padding-top: 0.3cm">
                <table>
                    <tr>
                        <td valign="top" width="0.8cm"></td>
                        <td width="2.5cm" class="tc fs9 bb"></td>
                        <td width="2.5cm" class="tc fs9 bb">' . $subApplicationSubmittingDate . '</td>
                        <td width="2.5cm" class="tc fs9 bb"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td valign="top" width="0.8cm"></td>
                        <td width="2.6cm" class="tc fs9">' . getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.signature", cLng()) . '</td>
                        <td width="2.6cm" class="tc fs9">' . getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.date", cLng()) . '</td>
                        <td width="2.6cm" class="tc fs9">' . getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.seal", cLng()) . '</td>
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    ';

    $partDataOtherPages[$i] = $partDataOtherPagesFirstPart;

    foreach ($subAppProducts as $key => $subAppProduct) {

        if ($keySubAppProduct > $key) {
            continue;
        }

        // ----- 7.
        $productNumberOfPlaces = (int)$subAppProduct['number_of_places'];

        $productPlaceType = getReferenceRows(ReferenceTable::REFERENCE_PACKAGING_REFERENCE, $subAppProduct['places_type'], false, [], 'get', false, false, [], false, $languageCode);
        $productNumberTypeOfPlaces = ' ' . $productNumberOfPlaces . ' ' . optional($productPlaceType)->name;

        // ----- 8.
        $productComercialDescription = ' ' . $subAppProduct->commercial_description;

        // ----- 9.
        $productBruttoWeight = $subAppProduct->brutto_weight ?? '';

        $productNettoWeight = $subAppProduct->netto_weight ?? '';
        $totalQuantity = optional($subAppProduct)->quantity; // SAF_ID_100

        $refMeasurementUintInfo = '<br>&nbsp;'; // SAF_ID_101
        $refMeasurementUint = getReferenceRows(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $subAppProduct['measurement_unit'], false, [], 'get', false, false, [], false, $languageCode);
        if (!empty(optional($refMeasurementUint)) && optional($refMeasurementUint)->code != ReferenceTable::REF_KILOGRAM_CODE) {
            $refMeasurementUintInfo .= '' . $totalQuantity . ' ' . optional($refMeasurementUint)->name;
        }

        $totalQuantity2 = optional($subAppProduct)->quantity_2; // SAF_ID_102

        $refMeasurementUint2Info = '<br>&nbsp;'; // SAF_ID_103
        $refMeasurementUint2 = getReferenceRows(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $subAppProduct['measurement_unit_2'], false, [], 'get', false, false, [], false, $languageCode);
        if (!empty(optional($refMeasurementUint2))) {
            $refMeasurementUint2Info .= '' . $totalQuantity2 . ' ' . optional($refMeasurementUint2)->name;
        }

        $productBruttoNetto = $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.brutto_weight", BaseModel::LANGUAGE_CODE_EN) . ' ' . $productBruttoWeight
            . '<br>' . $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.netto_weight", BaseModel::LANGUAGE_CODE_EN) . ' ' . $productNettoWeight
            . $refMeasurementUintInfo
            . $refMeasurementUint2Info;

        // ----- 10.
        $attachedDocumentProducts = SingleApplicationSubApplications::getAttachedDocumentProductsForPDF($subApplication, [$subAppProduct->id]);
        $documentInfo = [];
        foreach ($attachedDocumentProducts as $value) {
            if ($value->document->document_type == Document::DOCUMENT_CODE_FOR_TJ_SWT_756_CCI_FORM_A_PDF_TEMPLATE) {
                $documentInfo[] = $value->document->document_number . ' ' . $value->document->document_release_date;
            }
        }
        $documentInfo = implode(', ', $documentInfo);

        $partDataOtherPagesInfo = [
            'productNumberTypeOfPlaces' => [
                'text' => $productNumberTypeOfPlaces,
                'rowStringLength' => ($languageCode == BaseModel::LANGUAGE_CODE_EN) ? 18 : 14
            ],
            'productComercialDescription' => [
                'text' => $productComercialDescription,
                'rowStringLength' => ($languageCode == BaseModel::LANGUAGE_CODE_EN) ? 65 : 52
            ],
            'fieldIDCCI_001_20' => [
                'text' => $fieldIDCCI_001_20,
                'rowStringLength' => ($languageCode == BaseModel::LANGUAGE_CODE_EN) ? 19 : 15
            ],
            'productBruttoNetto' => [
                'text' => $productBruttoNetto,
                'rowStringLength' => ($languageCode == BaseModel::LANGUAGE_CODE_EN) ? 23 : 18
            ],
            'documentInfo' => [
                'text' => $documentInfo,
                'rowStringLength' => ($languageCode == BaseModel::LANGUAGE_CODE_EN) ? 21 : 17
            ]
        ];

        $partDataOtherPagesRowsCount += getColumnsMaxRowsCountForPrintForms($partDataOtherPagesInfo);

        if ($partDataOtherPagesRowsCount > $partDataMaxRowsCountOtherPages) {
            $partDataOtherPages[$i] .= $xxx;
            $partDataOtherPages[$i] .= $partDataOtherPagesLastPart;

            $i++;
            $partDataOtherPagesRowsCount = 0;

            $partDataOtherPages[$i] = $partDataOtherPagesFirstPart;

        }

        // ----- 7., 8., 9., 10.
        $partDataOtherPages[$i] .= '<tr>
        <td valign="top" width="1.4cm" class="fs9 pl5 pr5">' . $number . '.</td>
        <td valign="top" width="2.2cm" class="fs9 pl5 pr5">' . $productNumberTypeOfPlaces . '</td>
        <td valign="top" width="7.3cm" class="fs9 pl5 pr5">' . $productComercialDescription . '</td>
        <td valign="top" width="2.4cm" class="fs9 pl5 pr5">' . $fieldIDCCI_001_20 . '</td>
        <td valign="top" width="2.8cm" class="fs9 pl5 pr5">' . $productBruttoNetto . '</td>
        <td valign="top" width="2.2cm" class="fs9 pl5 pr5">' . $documentInfo . '</td>
    </tr>';

        if (($partDataOtherPagesRowsCount <= $partDataMaxRowsCountOtherPages) && ($countSubAppProducts - 1 == $key)) {

            $partDataOtherPagesEmptyRowsCount = $partDataMaxRowsCountOtherPages - $partDataOtherPagesRowsCount;

            $partDataOtherPages[$i] .= $xxx;

            $partDataOtherPages[$i] .= $partDataOtherPagesLastPart;

        }

        $number++;
    }

}

// ----- column-12 data

$keyCountryFixText1 = "<span class='fb'>" . $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.applicant_declaration", BaseModel::LANGUAGE_CODE_EN)
    . "</span><br>"
    . $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.country.fix_text_1", BaseModel::LANGUAGE_CODE_EN);
if (cLng() != BaseModel::LANGUAGE_CODE_EN) {
    $keyCountryFixText1 .= "<br><span class='fb'>" . $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.applicant_declaration", ConstructorTemplates::LANGUAGE_CODE_RU)
        . "</span><br>"
        . $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.country.fix_text_1", ConstructorTemplates::LANGUAGE_CODE_RU);
}

$keyCountryFixText2en = $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.country.fix_text_2", BaseModel::LANGUAGE_CODE_EN);
$keyCountryFixText2ru = $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.country.fix_text_2", BaseModel::LANGUAGE_CODE_RU);

$keyCountryFixText3 = $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.country.fix_text_3", BaseModel::LANGUAGE_CODE_EN);
if (cLng() != BaseModel::LANGUAGE_CODE_EN) {
    $keyCountryFixText3 .= ", " . $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.country.fix_text_3", ConstructorTemplates::LANGUAGE_CODE_RU);
}

$keyImportCountryFixText1 = $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.import_country.fix_text_1", BaseModel::LANGUAGE_CODE_EN);
if (cLng() != BaseModel::LANGUAGE_CODE_EN) {
    $keyImportCountryFixText1 .= "<br>" . $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.import_country.fix_text_1", ConstructorTemplates::LANGUAGE_CODE_RU);
}

$keyImportCountryFixText2 = $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.import_country.fix_text_2", BaseModel::LANGUAGE_CODE_EN);
if (cLng() != BaseModel::LANGUAGE_CODE_EN) {
    $keyImportCountryFixText2 .= ", " . $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.import_country.fix_text_2", ConstructorTemplates::LANGUAGE_CODE_RU);
}

?>
<style>

    @page {
        margin-left: 1.2cm;
        margin-top: 1.6cm;
        margin-right: 0cm;
        margin-bottom: 0cm;
    }

    table {
        overflow: wrap;
        width: 18.6cm;
        border-collapse: collapse;
    }

    .fb {
        font-weight: bold;
    }

    .tc {
        text-align: center;
    }


    .main-table {
        padding-top: 100px;
    }

    .bordered-table td {
        border: 1px solid #000000;
    }

    .tr {
        text-align: right;
    }

    .pt5 {
        padding-top: 5px;
    }

    .tj {
        text-align: justify;
    }

    .fs7 {
        font-size: 7px;
    }

    .fs8 {
        font-size: 8px;
    }

    .fs9 {
        font-size: 9px;
    }

    .fs10 {
        font-size: 10px;
    }

    .fs10-9 {
        font-size: 10.9px;
    }

    .fs11 {
        font-size: 11px;
    }

    .fs11-5 {
        font-size: 11.5px;
    }

    .fs12 {
        font-size: 12px;
    }

    .fs13 {
        font-size: 13px;
    }

    .fs14 {
        font-size: 14px;
    }

    .fs15 {
        font-size: 15px;
    }

    .fs16 {
        font-size: 16px;
    }

    .fs17 {
        font-size: 17px;
    }

    .fs18 {
        font-size: 18px;
    }

    .fs19 {
        font-size: 19px;
    }

    .fs20 {
        font-size: 20px;
    }

    .fs21 {
        font-size: 21px;
    }

    .fs22 {
        font-size: 22px;
    }

    .fs23 {
        font-size: 23px;
    }

    .fs24 {
        font-size: 24px;
    }

    .fs25 {
        font-size: 25px;
    }

    .fs26 {
        font-size: 26px;
    }

    .fs27 {
        font-size: 27px;
    }

    .fs28 {
        font-size: 28px;
    }

    .fs29 {
        font-size: 29px;
    }

    .fs30 {
        font-size: 30px;
    }

    .bt {
        border-top: 1px solid #000000;
    }

    .bb {
        border-bottom: 1px solid #000000;
    }

    .bbd {
        border-bottom: 1px dashed #000000;
    }

    .bl {
        border-left: 1px solid #000000;
    }

    .br {
        border-right: 1px solid #000000;
    }

    .border {
        border: 1px solid #000000;
    }


    .bt2 {
        border-top: 2px solid #000000;
    }

    .bb2 {
        border-bottom: 2px solid #000000;
    }

    .bl2 {
        border-left: 2px solid #000000;
    }

    .br2 {
        border-right: 2px solid #000000;
    }

    .pl5 {
        padding-left: 5px;
    }

    .pr5 {
        padding-right: 5px;
    }
</style>

<table>

    {{-----------------------------------------------------block 1----------------------------------------------------}}

    <tr>
        <td valign="top" width="9.3cm" height="2.9cm" class="br bt2 bl2" rowspan="2">
            <span class="fs11">&nbsp;1.{!! getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.exporter.text1", cLng()) !!}</span>
            <br>
            <span class="fs9">{{$safSidesExportInfo}}</span>
        </td>
        <td width="5.3cm" class="fs11 bt2"><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.number", BaseModel::LANGUAGE_CODE_EN)}}</td>
        <td width="4cm" class="fb fs9 bt2 br2"><br>{{$fieldIDCCI_001_5}}</td>
    </tr>
    <tr>
        <td valign="top" width="9.3cm" height="2cm" class="tc br2 fs10 fb" colspan="2">{!! $keyFixText1 !!}</td>
    </tr>

</table>

<table>

    {{-----------------------------------------------------block 2----------------------------------------------------}}

    <tr>
        <td valign="top" width="9.3cm" height="2.4cm" class="bt br bl2" rowspan="2">
            <span class="fs11">&nbsp;2.{!! getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.importer.text1", cLng()) !!}</span>
            <br><span class="fs9">{{$safSidesImportInfo}}</span>
        </td>
        <td valign="bottom" width="1.5cm" class="fs11">
            &nbsp;{!! $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.issued.text1", BaseModel::LANGUAGE_CODE_EN) !!}</td>
        <td valign="bottom" width="7.3cm" class="tc <?php echo ($languageCode == BaseModel::LANGUAGE_CODE_EN) ? 'bbd' : 'bb'; ?> fs9">{{$safTransportationExportCountryName}}</td>
        <td width="0.5cm" height="0.4cm" class="br2"></td>
    </tr>
    <tr>
        <td valign="top" width="1.5cm"></td>
        <td valign="top" width="7.3cm" class="tc fs7">{!! $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.issued.text2", BaseModel::LANGUAGE_CODE_EN) !!}</td>
        <td class="br2" height="2cm"></td>
    </tr>

</table>

<table>

    {{-----------------------------------------------------block 3----------------------------------------------------}}

    <tr>
        <td valign="top" width="9.3cm" height="3.5cm" class="bt br bl2">
            <span class="fs11 ">&nbsp;3.{!! getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.transport.type.transit_country.text1", cLng()) !!}</span>
            <br><span class="fs9">{{$transportCountriesListTransitCountry}}</span>
        </td>
        <td valign="top" width="9.3cm" height="3.5cm" class="bt br2">
            <span class="fs11 ">&nbsp;4.{!! getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.service_marks", cLng()) !!}</span>
            <br><span class="fs9">{!! $mdmId506 !!}</span>
        </td>
    </tr>

</table>

<table>

    {{-----------------------------------------------------block 4----------------------------------------------------}}

    <tr>
        <td valign="top" width="1.4cm" height="2.2cm" class="pl5 bt br fs11 bl2">5.{!! getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.list.number", cLng()) !!}</td>
        <td valign="top" width="2.2cm" height="2.2cm" class="pl5 bt br fs11">6.{!! getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.list.count.type_of_package", cLng()) !!}</td>
        <td valign="top" width="7.3cm" height="2.2cm" class="pl5 bt br fs11">7.{!! getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.list.product_description", cLng()) !!}</td>
        <td valign="top" width="2.4cm" height="2.2cm" class="pl5 bt br fs11">8.{!! getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.list.origin_criterion", cLng()) !!}</td>
        <td valign="top" width="2.8cm" height="2.2cm" class="pl5 bt br fs11">9.{!! getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.list.weight.brutto_netto", cLng()) !!}</td>
        <td valign="top" width="2.5cm" height="2.2cm" class="pl5 bt fs11 br2">10.{!! getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.list.number_and_date", cLng()) !!}</td>
    </tr>

</table>

<table style="background-image: url('/image/tableRows2.png'); background-repeat: no-repeat;">

    {{-----------------------------------------------------block 5----------------------------------------------------}}

    <tr>
        <td valign="top" width="18.6cm" height="7.8cm" class="bl2 br2" >
            <table>
                @if (!empty($partData))
                    {!! $partData !!}
                @endif
            </table>
        </td>
    </tr>

</table>

<table>

    {{-----------------------------------------------------block 6----------------------------------------------------}}

    <tr>
        <td valign="top" width="9.3cm" height="6.3cm" class="bt br bl2">
            <table>
                <tr>
                    <td valign="top" width="0.8cm" class="fb fs11">&nbsp;&nbsp;&nbsp;11. </td>
                    <td valign="top" class="fs11"><?php
                        if (cLng() == BaseModel::LANGUAGE_CODE_EN) {
                            echo '<span class="fb">' . $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.certificate.text_1", BaseModel::LANGUAGE_CODE_EN) . '</span>'
                                . '<br>'
                                . $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.certificate.text_2", BaseModel::LANGUAGE_CODE_EN);
                        } else {
                            echo '<span class="fb">' . $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.certificate.text_1", BaseModel::LANGUAGE_CODE_EN) . '</span>'
                                . '<br>'
                                . $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.certificate.text_2", BaseModel::LANGUAGE_CODE_EN)
                                . '<br>'
                                . '<span class="fb">' . $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.certificate.text_1", ConstructorTemplates::LANGUAGE_CODE_RU) . '</span>'
                                . '<br>'
                                . $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.certificate.text_2", ConstructorTemplates::LANGUAGE_CODE_RU);
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td valign="top" width="0.8cm" class="fb fs11"></td>
                    <td valign="top" class="fb fs11">{{$keyFixText2}}</td>
                </tr>
                <tr>
                    <td valign="top" width="0.8cm" class="fb fs11"></td>
                    <td valign="top" class="fs9">{{$agencyName}}</td>
                </tr>
                <tr>
                    <td valign="top" width="0.8cm" class="fb fs11"></td>
                    <td valign="top" class="fs9">{{$regionalOfficeAddress}}</td>
                </tr>
                <tr>
                    <td valign="top" width="0.8cm" class="fb fs11"></td>
                    <td valign="top" class="fs9">{{$fieldIDCCI_001_8}}</td>
                </tr>
            </table>
        </td>
        <td valign="top" width="9.3cm" height="6.3cm" class="br2 bt">
            <table>
                <tr>
                    <td valign="top" width="0.8cm" class="fb fs11">&nbsp;&nbsp;&nbsp;12. </td>
                    <td valign="top" class="fs10-9" colspan="2">{!! $keyCountryFixText1 !!}</td>
                </tr>
                <tr>
                    <td valign="top" colspan="3">
                        <table>
                            <tr>
                                <td width="2.1cm" class="fs11">{!! $keyCountryFixText2en !!}</td>
                                <td width="7.1cm" class="<?php echo ($languageCode == BaseModel::LANGUAGE_CODE_EN) ? 'bbd' : 'bb'; ?> fs9 tc">{{$safTransportationExportCountryName}}</td>
                            </tr>
                            <tr>
                                <td valign="top" width="2.1cm" class="fs11">
                                    {!! (cLng() != BaseModel::LANGUAGE_CODE_EN) ? $keyCountryFixText2ru : '' !!}</td>
                                <td width="7.1cm" class="tc fs9">{!! $keyCountryFixText3 !!}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" width="0.8cm" class="fb fs11"></td>
                    <td valign="top" width="7.8cm" class="fs11" >{!! $keyImportCountryFixText1 !!}</td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table>
                            <tr>
                                <td width="0.2cm" class="fs11"></td>
                                <td valign="top" width="8.9cm" class="fs11 <?php echo ($languageCode == BaseModel::LANGUAGE_CODE_EN) ? 'bbd' : 'bb'; ?> tc">{{$safTransportationImportCountryName}}</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td width="0.2cm" class="fs11"></td>
                                <td valign="top" width="8.9cm" class="tc fs9">{!! $keyImportCountryFixText2 !!}</td>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>
        </td>
    </tr>

    <tr>
        <td valign="top" width="9.3cm" height="1.4cm" class="bb2 bl2 br">
            <table>
                <tr>
                    <td width="0.2cm" class="fs11"></td>
                    <td width="2.1cm" class="fs11 <?php echo ($languageCode == BaseModel::LANGUAGE_CODE_EN) ? 'bbd' : 'bb'; ?>">{{$fieldIDCCI_001_6}}</td>
                    <td width="1.4cm" class="fs11 <?php echo ($languageCode == BaseModel::LANGUAGE_CODE_EN) ? 'bbd' : 'bb'; ?>">&nbsp;</td>
                    <td width="5.4cm" class="fs11 <?php echo ($languageCode == BaseModel::LANGUAGE_CODE_EN) ? 'bbd' : 'bb'; ?>"></td>
                    <td></td>
                </tr>
                <tr>
                    <td width="0.2cm" class="fs11"></td>
                    <td width="2.1cm" class="fs11">{!! getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.home.date", cLng()) !!}</td>
                    <td width="1.4cm" class="fs11">{!! getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.home.signature", cLng()) !!}</td>
                    <td width="5.4cm" class="fs11">{!! getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.home.seal", cLng()) !!}</td>
                    <td></td>
                </tr>
            </table>
        </td>
        <td valign="top" width="9.3cm" height="1.2cm" class="bb2 br2">
            <table>
                <tr>
                    <td width="0.2cm" class="fs11"></td>
                    <td width="2.1cm" class="fs11 <?php echo ($languageCode == BaseModel::LANGUAGE_CODE_EN) ? 'bbd' : 'bb'; ?>">{{$subApplicationSubmittingDate}}</td>
                    <td width="1.4cm" class="fs11 <?php echo ($languageCode == BaseModel::LANGUAGE_CODE_EN) ? 'bbd' : 'bb'; ?>">&nbsp;</td>
                    <td width="5.4cm" class="fs11 <?php echo ($languageCode == BaseModel::LANGUAGE_CODE_EN) ? 'bbd' : 'bb'; ?>"></td>
                    <td></td>
                </tr>
                <tr>
                    <td width="0.2cm" class="fs11"></td>
                    <td width="2.1cm" class="fs11">{!! getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.home.date", cLng()) !!}</td>
                    <td width="1.4cm" class="fs11">{!! getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.home.signature", cLng()) !!}</td>
                    <td width="5.4cm" class="fs11">{!! getTransCustom($dictionaryManager, "swis.pdf_templates.{$template}.home.seal", cLng()) !!}</td>
                    <td></td>
                </tr>
            </table>
        </td>
    </tr>

</table>

@if (!empty($partDataOtherPages))

    @foreach($partDataOtherPages as $valueOP)

        {!! $valueOP !!}

    @endforeach

@endif