<?php
//------------- Get custom_data field values by mdm_id ----------//
use App\Http\Controllers\Core\Dictionary\DictionaryManager;
use App\Models\BaseModel;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\RegionalOffice\RegionalOffice;
use App\Models\SingleApplication\SingleApplication;

$dictionaryManager = new DictionaryManager();

$fieldValuesTJ = getFieldsValueByMDMID($documentId, $customData, BaseModel::LANGUAGE_CODE_TJ);

//------------- Get data field values by saf_id ----------//\
list($mainDataBySafId, $productsDataBySafId, $productBatchesDataBySafId) = getSafDataFieldIdValue($saf, $subApplication, $subAppProducts, $availableProductsIds, BaseModel::LANGUAGE_CODE_TJ);

//----- 2.
if (isset($fieldValuesTJ['355']) && !empty($fieldValuesTJ['355'])) {

    $approvedDateParse = date_parse_from_format(config('swis.date_time_format_front'), $fieldValuesTJ['355']) ?? '';

    $mapping_355_day = !empty($approvedDateParse) ? $approvedDateParse['day'] : '';
    $mapping_355_month = !empty($approvedDateParse) ? $approvedDateParse['month'] : '';
    $mapping_355_year = !empty($approvedDateParse) ? $approvedDateParse['year'] : '';

    $point1 = "<table>
                   <tr>
                       <td style='width:7mm'><span class='fb'>" . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_1", BaseModel::LANGUAGE_CODE_TJ) . "</span> " . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_1", BaseModel::LANGUAGE_CODE_RU) . "</td>
                       <td style='width:5mm' class='border-bottom fb tc'>«" . $mapping_355_day . "» </td>
                       <td style='width:2mm'>&nbsp;</td>
                       <td style='width:46mm' class='border-bottom fb tc'>" . $mapping_355_month . "</td>
                       <td style='width:2mm'>&nbsp;</td>
                       <td style='width:5mm' class='border-bottom fb tc'>" . $mapping_355_year . "</td>
                       <td style='width:10mm'>
                       <span class='fb'>" . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_2", BaseModel::LANGUAGE_CODE_TJ) . "</span> " . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_2", BaseModel::LANGUAGE_CODE_RU) . "
                       </td>
                   </tr>
                </table>";
} else {
    $point1 = "<span class='fb'>" . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_1", BaseModel::LANGUAGE_CODE_TJ) . "</span> " . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_1", BaseModel::LANGUAGE_CODE_RU) . "«_____» ____________________ 20_____<span class='fb'>" . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_2", BaseModel::LANGUAGE_CODE_TJ) . "</span> " . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_2", BaseModel::LANGUAGE_CODE_RU);
}

//----- 3.
$subdivisionId = optional($subApplication)->agency_subdivision_id;

if (!is_null($subdivisionId)) {
    $subDivisionCode = optional(getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME, $subdivisionId, false, [], false, false, false, [], false, BaseModel::LANGUAGE_CODE_TJ))->code;

    if (!is_null($subDivisionCode)) {
        $regionalOffice = RegionalOffice::leftJoin('regional_office_ml', 'regional_office.id', '=', 'regional_office_ml.regional_office_id')
            ->where(['lng_id' => BaseModel::LANGUAGE_CODE_TJ, 'office_code' => $subDivisionCode])
            ->active()
            ->pluck('regional_office_ml.name')
            ->first();

        $regionalOfficeInfo = $regionalOffice . (isset($fieldValuesTJ['473']) ? ', ' . $fieldValuesTJ['473'] : '');
    }
}

$point3 = getMappingValueInMultipleRows($regionalOfficeInfo ?? '', 85, 85);

//----- 4.
$safTransportationStateNumberCustomSupports = $saf->data['saf']['transportation']['state_number_customs_support'] ?? '';
$stateNumbers = [];
foreach ($safTransportationStateNumberCustomSupports as $safTransportationStateNumberCustomSupport) {
    if (!is_null($safTransportationStateNumberCustomSupport)) {
        $stateNumbers[] = $safTransportationStateNumberCustomSupport['state_number'];
    }
}

if (count($stateNumbers) > 0) {
    $stateNumbersList = implode(', ', $stateNumbers);
}

foreach ($subAppProducts as $subAppProduct) {
    $productInfo[] = notEmptyForPdfFields($productsDataBySafId[$subAppProduct->id], 'SAF_ID_91') . " " . notEmptyForPdfFields($productsDataBySafId[$subAppProduct->id], 'SAF_ID_99') . " " . trans("swis.pdf.{$template}.key_7");
}
$productList = implode(', ', $productInfo);
$point4 = count($stateNumbers) . ' ' . ($mainDataBySafId['SAF_ID_75'] ?? '') . ' ' . $stateNumbersList . '. ' . $productList;
$point4 = getMappingValueInMultipleRows($point4 ?? '', 60, 85);

//----- 5.
if (isset($mainDataBySafId['SAF_ID_1']) && !empty($mainDataBySafId['SAF_ID_1'])) {
    switch ($mainDataBySafId['SAF_ID_1']) {
        case SingleApplication::REGIME_IMPORT:
            $country = $mainDataBySafId['SAF_ID_78'] ?? '';
            break;
        case SingleApplication::REGIME_EXPORT:
            $country = $mainDataBySafId['SAF_ID_79'] ?? '';
            break;
        case SingleApplication::REGIME_TRANSIT:
            $country = '';
            break;
    }
}
$point5 = $country ?? '';

//----- 6.
$point6 = $fieldValuesTJ['474'] ?? '';

//----- 7.
if (isset($fieldValuesTJ['475']) && !empty($fieldValuesTJ['475'])) {
    $dateParse = date_parse_from_format(config('swis.date_time_format_front'), $fieldValuesTJ['475']) ?? '';

    $mapping_475_day = !empty($dateParse) ? $dateParse['day'] : '';
    $mapping_475_month = !empty($dateParse) ? $dateParse['month'] : '';
    $mapping_475_year = !empty($dateParse) ? $dateParse['year'] : '';

    $point7 = "<table>
                   <tr>
                       <td>" . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_10", BaseModel::LANGUAGE_CODE_RU) . "
                                                <u class='fb'>" . $point6 . "&nbsp;</u></td>
                       <td><span class='fb'>" . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_1", BaseModel::LANGUAGE_CODE_TJ) . "</span> " . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_1", BaseModel::LANGUAGE_CODE_RU) . "</td>
                       <td class='border-bottom fb tc'>«" . $mapping_475_day . "» </td>
                       <td>&nbsp;</td>
                       <td style='width:20%' class='border-bottom fb tc'>" . $mapping_475_month . "</td>
                       <td>&nbsp;</td>
                       <td class='border-bottom fb tc'>" . $mapping_475_year . "</td>
                       <td>
                       <span class='fb'>" . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_2", BaseModel::LANGUAGE_CODE_TJ) . "</span> " . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_2", BaseModel::LANGUAGE_CODE_RU) . "
                       </td>
                       <td style='width:20%'>&nbsp;</td>
                   </tr>
                </table>";
} else {
    $point7 = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_10", BaseModel::LANGUAGE_CODE_RU) . "
    <u class='fb'>'.$point6.'&nbsp;</u><span class='fb'>" . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_1", BaseModel::LANGUAGE_CODE_TJ) . "</span> " . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_1", BaseModel::LANGUAGE_CODE_RU) . " «_____» _______________ 20_____<span class='fb'>" . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_2", BaseModel::LANGUAGE_CODE_TJ) . "</span> " . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_2", BaseModel::LANGUAGE_CODE_RU);
}

//----- 8.
if (isset($mainDataBySafId['SAF_ID_1']) && !empty($mainDataBySafId['SAF_ID_1'])) {
    switch ($mainDataBySafId['SAF_ID_1']) {
        case SingleApplication::REGIME_IMPORT:
            $sideName = $mainDataBySafId['SAF_ID_6'] ?? '';
            break;
        case SingleApplication::REGIME_EXPORT:
            $sideName = $mainDataBySafId['SAF_ID_14'] ?? '';
            break;
        case SingleApplication::REGIME_TRANSIT:
            $sideName = '';
            break;
    }
}
$point8 = $sideName ?? '';

//----- 9.
$point9 = getMappingValueInMultipleRows($fieldValuesTJ['482'] ?? '', 25, 80);

//----- 10.
$point10 = getMappingValueInMultipleRows($fieldValuesTJ['471'] ?? '', 60, 80);

//----- 11.
$point11 = getMappingValueInMultipleRows($fieldValuesTJ['472'] ?? '', 40, 80);

//----- 12.
$point12 = $fieldValuesTJ['478'];

//----- 13.
$fieldId_476 = isset($fieldValuesTJ['476']) && !empty($fieldValuesTJ['476']) ? $fieldValuesTJ['476'] . ", " : '';
$point13 = $fieldId_476 . ($fieldValuesTJ['477'] ?? '');

//----- 14.
if (isset($fieldValuesTJ['479']) && !empty($fieldValuesTJ['479']) ) {

    $dateParse = date_parse_from_format(config('swis.date_time_format_front'), $fieldValuesTJ['479']) ?? '';

    $mapping_479_day = !empty($dateParse) ? $dateParse['day'] : '';
    $mapping_479_month = !empty($dateParse) ? $dateParse['month'] : '';
    $mapping_479_year = !empty($dateParse) ? $dateParse['year'] : '';

    $point14 = "<table>
                   <tr>
                       <td class='border-bottom fb tc'>«" . $mapping_479_day . "» </td>
                       <td >&nbsp;</td>
                       <td class='border-bottom fb tc' style='width:50%'>" . $mapping_479_month . "</td>
                       <td >&nbsp;</td>
                       <td class='border-bottom fb tc'>" . $mapping_479_year . "</td>
                       <td class='fb'>
                       <span class='fb'>" . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_2", BaseModel::LANGUAGE_CODE_TJ) . "</span> " . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_2", BaseModel::LANGUAGE_CODE_RU) . "
                       </td>
                   </tr>
                </table>";
} else {
    $point14 = "«_____» ______________ 20____<span class='fb'>" . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_2", BaseModel::LANGUAGE_CODE_TJ) . "</span> " . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_2", BaseModel::LANGUAGE_CODE_RU);
}

//----- 15.
if (isset($fieldValuesTJ['480']) && !empty($fieldValuesTJ['480']) ) {

    $dateParse = date_parse_from_format(config('swis.date_time_format_front'), $fieldValuesTJ['480']) ?? '';

    $mapping_480_day = !empty($dateParse) ? $dateParse['day'] : '';
    $mapping_480_month = !empty($dateParse) ? $dateParse['month'] : '';
    $mapping_480_year = !empty($dateParse) ? $dateParse['year'] : '';

    $point15 = "<table>
                   <tr>
                       <td class='border-bottom fb tc'>«" . $mapping_480_day . "» </td>
                       <td >&nbsp;</td>
                       <td class='border-bottom fb tc' style='width:50%'>" . $mapping_480_month . "</td>
                       <td >&nbsp;</td>
                       <td class='border-bottom fb tc'>" . $mapping_480_year . "</td>
                       <td class='fb'>
                       <span class='fb'>" . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_2", BaseModel::LANGUAGE_CODE_TJ) . "</span> " . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_2", BaseModel::LANGUAGE_CODE_RU) . "
                       </td>
                   </tr>
                </table>";
} else {
    $point15 = "«_____» ______________ 20____<span class='fb'>" . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_2", BaseModel::LANGUAGE_CODE_TJ) . "</span> " . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_2", BaseModel::LANGUAGE_CODE_RU);
}
?>
<table class="fs12">
    <tr>
        <td>
            <table>
                <tr>
                    <td style="padding-left:69mm; padding-right:63mm;width:80mm">
                        {!! $point1 !!}
                    </td>
                </tr>
                <tr>
                    <td style="padding-left:50mm; padding-right:19mm">
                        <span class="fb">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_3", BaseModel::LANGUAGE_CODE_TJ)}}</span>
                    </td>
                </tr>
                <tr>
                    <td style="padding-left:50mm; padding-right:19mm">
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_3", BaseModel::LANGUAGE_CODE_RU)}}
                    </td>
                </tr>
                <tr>
                    <td style="padding-left:17mm; padding-right:15mm">
                        <table>
                            <tr>
                                <td class="border-bottom fb">{{$point3[0] ?? ''}}</td>
                            </tr>
                            <tr>
                                <td class="fs10 tc"><em><span
                                                class="fb">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_4", BaseModel::LANGUAGE_CODE_TJ)}}</span>{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_4", BaseModel::LANGUAGE_CODE_RU)}}
                                    </em></td>
                            </tr>
                            @if(count($point3)>1)
                                @for($i=1;$i<count($point3);$i++)
                                    <tr>
                                        <td class="border-bottom fb">{{$point3[$i] ?? ''}}</td>
                                    </tr>
                                @endfor
                            @endif
                            <tr>
                                <td class="fb">
                                    {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_5", BaseModel::LANGUAGE_CODE_TJ)}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_5", BaseModel::LANGUAGE_CODE_RU)}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td style="width:40%">
                                                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_6", BaseModel::LANGUAGE_CODE_RU)}}
                                            </td>
                                            <td class="border-bottom fb" style="width:60%">
                                                {{$point4[0]}}
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="border-bottom fb">{{$point4[1] ??''}}&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="border-bottom fb">{{$point4[2] ?? ''}}&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="fs10 tc"><em><span class="fb">
                                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_8", BaseModel::LANGUAGE_CODE_TJ)}}</span>{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_8", BaseModel::LANGUAGE_CODE_RU)}}
                                    </em>
                                </td>
                            </tr>
                            <tr>
                                <td class="fb">
                                    {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_9", BaseModel::LANGUAGE_CODE_TJ)}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td style="width:25%">
                                                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_9", BaseModel::LANGUAGE_CODE_RU)}}
                                            </td>
                                            <td style="width:75%" class="border-bottom fb">{{$point5}}&nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="fb">
                                    {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_10", BaseModel::LANGUAGE_CODE_TJ)}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {!! $point7 !!}
                                </td>
                            </tr>
                            <tr>
                                <td class="border-bottom fb">{{$point8}}&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="fs10 tc"><em><span class="fb">
                                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_11", BaseModel::LANGUAGE_CODE_TJ)}}</span>{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_11", BaseModel::LANGUAGE_CODE_RU)}}
                                    </em></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding-left:30mm; padding-right:15mm">
                        <table>
                            <tr>
                                <td class="fb">
                                    {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_12", BaseModel::LANGUAGE_CODE_TJ)}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td style="width:50%">
                                                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_12", BaseModel::LANGUAGE_CODE_RU)}}
                                            </td>
                                            <td style="width:50%" class="border-bottom fb">
                                                {{$point9[0]}}
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="border-bottom fb">{{$point9[1] ?? ''}}&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="fb">
                                    {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_13", BaseModel::LANGUAGE_CODE_TJ)}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_13", BaseModel::LANGUAGE_CODE_RU)}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td style="width:10%">
                                                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_14", BaseModel::LANGUAGE_CODE_RU)}}
                                            </td>
                                            <td style="width:90%" class="border-bottom fb">
                                                {{$point10[0]}}
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="border-bottom fb"> {{$point10[1] ?? ''}}&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="fb">
                                    {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_15", BaseModel::LANGUAGE_CODE_TJ)}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td style="width:41%">
                                                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_15", BaseModel::LANGUAGE_CODE_RU)}}
                                            </td>
                                            <td style="width:59%" class="border-bottom fb">
                                                {{$point11[0]}}
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="border-bottom fb">{{$point11[1] ?? ''}}&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="fb">
                                    {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_16", BaseModel::LANGUAGE_CODE_TJ)}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td style="width:27%">
                                                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_16", BaseModel::LANGUAGE_CODE_RU)}}
                                            </td>
                                            <td style="width:73%" class="border-bottom fb">
                                                {{$point12}}
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="fb">
                                    {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_17", BaseModel::LANGUAGE_CODE_TJ)}}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td style="width:47%">
                                                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_17", BaseModel::LANGUAGE_CODE_RU)}}
                                            </td>
                                            <td class="border-bottom fb" style="width:53%">{{$point13 ?? ''}}</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td class="fs10 tc"><em><span class="fb">
                                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_18", BaseModel::LANGUAGE_CODE_TJ)}}</span>
                                                    {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_18", BaseModel::LANGUAGE_CODE_RU)}}
                                                </em>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="fb" colspan="2">
                                                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_19", BaseModel::LANGUAGE_CODE_TJ)}}
                                            </td>
                                            <td class="fb" colspan="2">
                                                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_20", BaseModel::LANGUAGE_CODE_TJ)}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:20%">
                                                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_19", BaseModel::LANGUAGE_CODE_RU)}}
                                            </td>
                                            <td style="width:35%">
                                                {!! $point14 !!}
                                            </td>
                                            </td>
                                            <td style="width:20%">
                                                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_20", BaseModel::LANGUAGE_CODE_RU)}}
                                            </td>
                                            <td style="width:35%">
                                                {!! $point15 !!}
                                            </td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td width="18%">
                                                <span class="fb">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_21", BaseModel::LANGUAGE_CODE_TJ)}}</span>{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_21", BaseModel::LANGUAGE_CODE_RU)}}
                                            </td>
                                            <td width="20%" class="border-bottom">
                                            </td>
                                            <td width="62%">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td class="fs10 tc">
                                                <em><span class="fb">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_22", BaseModel::LANGUAGE_CODE_TJ)}})</span>{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_22", BaseModel::LANGUAGE_CODE_RU)}}
                                                </em>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<style>
    @page {
        margin-top: 83mm;
        margin-left: 0mm;
        margin-right: 0mm;
        line-height: 1.8;

    }

    .fs10 {
        font-size: 10px;
    }

    .fs12 {
        font-size: 12px;
    }

    .fb {
        font-weight: bold;
    }

    .tc {
        text-align: center;
    }

    .tr {
        text-align: right;
    }

    .tl {
        text-align: left;
    }

    .border-bottom {
        border-bottom: 1px solid #000000;
    }

    table {
        border-collapse: collapse;
        overflow: wrap;
        width: 100%;
    }

</style>
