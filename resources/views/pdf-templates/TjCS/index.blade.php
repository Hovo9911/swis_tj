<?php

use App\Models\BaseModel;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplication\SingleApplication;

$fieldID1645_1 = !is_null($customData) ? $customData['CSCC_001_3'] ?? '' : '';
$fieldID1645_5 = !is_null($customData) ? $customData['CSCC_001_7'] ?? '' : '';
$fieldID1645_6 = !is_null($customData) ? $customData['CSCC_001_8'] ?? '' : '';
$fieldID1645_16 = !is_null($customData) ? $customData['CSCC_001_17'] ?? '' : '';
$fieldID1645_17 = !is_null($customData) ? $customData['CSCC_001_18'] ?? '' : '';
$fieldID1645_18 = !is_null($customData) ? $customData['CSCC_001_19'] ?? '' : '';
$fieldID1645_19 = !is_null($customData) ? $customData['CSCC_001_20'] ?? '' : '';
$fieldID1645_20 = !is_null($customData) ? $customData['CSCC_001_21'] ?? '' : '';
$fieldID1645_21 = !is_null($customData) ? $customData['CSCC_001_22'] ?? '' : '';
$fieldID1645_22 = !is_null($customData) ? $customData['CSCC_001_23'] ?? '' : '';
$fieldID1645_23 = !is_null($customData) ? $customData['CSCC_001_24'] ?? '' : '';
$fieldID1645_24 = !is_null($customData) ? $customData['CSCC_001_25'] ?? '' : '';
$fieldID1645_25 = !is_null($customData) ? $customData['CSCC_001_26'] ?? '' : '';
$fieldID1645_26 = !is_null($customData) ? $customData['CSCC_001_27'] ?? '' : '';
$fieldID1645_27 = !is_null($customData) ? $customData['CSCC_001_28'] ?? '' : '';

if (!empty($fieldID1645_5)) {
    $fromDay = date('d', strtotime($fieldID1645_5));
    $fromMonth = month(date('m', strtotime($fieldID1645_5)));
    $fromYear = date('y', strtotime($fieldID1645_5));
}
if (!empty($fieldID1645_6)) {
    $toDay = date('d', strtotime($fieldID1645_6));
    $toMonth = month(date('m', strtotime($fieldID1645_6)));
    $toYear = date('y', strtotime($fieldID1645_6));
}

$subAppProductsCountries = $subAppProducts->pluck('producer_country')->unique();

foreach ($subAppProductsCountries as $subAppProductsCountry) {
    $productProducerCountries[] = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $subAppProductsCountry, false, [], 'get', false, false, [], false, BaseModel::LANGUAGE_CODE_TJ))->name;
}

$productProducerCountries = implode(', ', ($productProducerCountries ?? []));

$safGeneralRegime = $saf->data['saf']['general']['regime'] ?? '';

switch ($safGeneralRegime) {
    case SingleApplication::REGIME_IMPORT:
        $safSideName = $saf->data['saf']['sides']['import']['name'] ?? '';
        $safSideAddress = $saf->data['saf']['sides']['import']['address'] ?? '';
        $safSideCommunity = $saf->data['saf']['sides']['import']['community'] ?? '';
        break;
    case SingleApplication::REGIME_EXPORT:
        $safSideName = $saf->data['saf']['sides']['export']['name'] ?? '';
        $safSideAddress = $saf->data['saf']['sides']['export']['address'] ?? '';
        $safSideCommunity = $saf->data['saf']['sides']['export']['community'] ?? '';
        break;
    default:
        $safSideName = '';
        $safSideAddress = '';
        $safSideCommunity = '';
        break;
}
$safSideCommunity = ($safSideCommunity == '-') ? '' : $safSideCommunity;
$safSideAddress = ($safSideAddress == '-') ? '' : $safSideAddress;
$safSideInfo = '';

if (!empty($safSideCommunity) AND !empty($safSideAddress)) {
    $safSideInfo = $safSideCommunity . ' , ' . $safSideAddress;
} elseif (!empty($safSideCommunity)) {
    $safSideInfo = $safSideCommunity;
} elseif (!empty($safSideAddress)) {
    $safSideInfo = $safSideAddress;
}

$fieldID1645_1 = mb_strlen($fieldID1645_1, 'UTF-8') > 40 ? mb_substr($fieldID1645_1, 0, 40, 'UTF-8') . "..." : $fieldID1645_1;
$fieldID1645_16 = mb_strlen($fieldID1645_16, 'UTF-8') > 50 ? mb_substr($fieldID1645_16, 0, 50, 'UTF-8') . "..." : $fieldID1645_16;
$fieldID1645_17 = mb_strlen($fieldID1645_17, 'UTF-8') > 50 ? mb_substr($fieldID1645_17, 0, 50, 'UTF-8') . "..." : $fieldID1645_17;
$fieldID1645_18 = mb_strlen($fieldID1645_18, 'UTF-8') > 40 ? mb_substr($fieldID1645_18, 0, 40, 'UTF-8') . "..." : $fieldID1645_18;
$fieldID1645_19 = mb_strlen($fieldID1645_19, 'UTF-8') > 20 ? mb_substr($fieldID1645_19, 0, 20, 'UTF-8') . "..." : $fieldID1645_19;
$fieldID1645_20 = mb_strlen($fieldID1645_20, 'UTF-8') > 20 ? mb_substr($fieldID1645_20, 0, 20, 'UTF-8') . "..." : $fieldID1645_20;
if ($fieldID1645_21) {
    $fieldID1645_21 = mb_strlen($fieldID1645_21, 'UTF-8') > 20 ? mb_substr($fieldID1645_21, 0, 20, 'UTF-8') . "..." : $fieldID1645_21 . " Мест";
}
if ($fieldID1645_22) {
    $fieldID1645_22 = mb_strlen($fieldID1645_22, 'UTF-8') > 20 ? mb_substr($fieldID1645_22, 0, 20, 'UTF-8') . "..." : "(" . $fieldID1645_22 . " шт)";
}
$fieldID1645_23 = mb_strlen($fieldID1645_23, 'UTF-8') > 150 ? mb_substr($fieldID1645_23, 0, 150, 'UTF-8') . "..." : $fieldID1645_23;
$fieldID1645_24 = mb_strlen($fieldID1645_24, 'UTF-8') > 100 ? mb_substr($fieldID1645_24, 0, 100, 'UTF-8') . "..." : $fieldID1645_24;
$fieldID1645_25 = mb_strlen($fieldID1645_25, 'UTF-8') > 200 ? mb_substr($fieldID1645_25, 0, 200, 'UTF-8') . "..." : $fieldID1645_25;
$fieldID1645_26 = mb_strlen($fieldID1645_26, 'UTF-8') > 25 ? mb_substr($fieldID1645_26, 0, 25, 'UTF-8') . "..." : $fieldID1645_26;
$fieldID1645_27 = mb_strlen($fieldID1645_27, 'UTF-8') > 25 ? mb_substr($fieldID1645_27, 0, 25, 'UTF-8') . "..." : $fieldID1645_27;
$safSideInfo = mb_strlen($safSideInfo, 'UTF-8') > 60 ? mb_substr($safSideInfo, 0, 60, 'UTF-8') . "..." : $safSideInfo;
$safSideName = mb_strlen($safSideName, 'UTF-8') > 45 ? mb_substr($safSideName, 0, 45, 'UTF-8') . "..." : $safSideName;
$productProducerCountries = mb_strlen($productProducerCountries, 'UTF-8') > 100 ? mb_substr($productProducerCountries, 0, 100, 'UTF-8') . "..." : $productProducerCountries;

$someText = '';
if (count($subAppProducts) > 1) {
    $someText = 'Таҷҳизодҳои радиотехники  мувофики замимаи';
} else {
    $someText = $subAppProducts[0]->commercial_description;
    $someText = mb_strlen($someText, 'UTF-8') > 40 ? mb_substr($someText, 0, 40, 'UTF-8') . "..." : $someText;
}
?>
<table>
    <tr>
        <td style="height:130px"></td>
    </tr>
</table>

<table>
    <tr>
        <td style="
			font-size: 16px;
			font-style: italic;
			text-align: center;
			width: 75%;
		"></td>
        <td style="
			text-align: right;
			font-size: 12px;
			width: 25%;
		">
            <div style="
				font-size: 10px;
				float: right;
				width: 100px;
			">
                <span style="font-weight:bold;font-size: 12px;">№ {{$fieldID1645_1}}</span>
            </div>
        </td>
    </tr>
</table>
<br/>
<br/>
<table align="left">
    <tr>
        <td style="
			font-size: 13px;;
			vertical-align: top;
			width: 140px;
			font-weight:bold;
		">Эътибор дорад аз<br/>Срок действия с
        </td>
        <td>
            <table>
                <tr>
                    <td style="width: 230px;">
                        <table>
                            <tr style="font-size: 12px;">
                                <td style="
									width: 10px;
									vertical-align: top;
								">“
                                </td>
                                <td style="
									border-bottom: 1px solid #000000;
									vertical-align: top;
									text-align:center;
									width: 40px;
								">{{$fromDay ?? ''}}</td>
                                <td style="
									vertical-align: top;
									width: 10px;
								">“
                                </td>
                                <td style="
									border-bottom: 1px solid #000000;
									vertical-align: top;
									text-align: center;
									width: 100px;
								">{{$fromMonth ?? ''}}</td>
                                <td style="
									font-size: 13px;;
									vertical-align: top;
									width: 20px;
								">20
                                </td>
                                <td style="
									border-bottom: 1px solid #000000;
									vertical-align: top;
									width: 20px;
								">{{$fromYear ?? ''}}</td>
                                <td style="
									font-size: 12px;;
									vertical-align: top;
									width: 15px;
									font-weight:bold;
								">с.
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="
						font-size: 12px;
						vertical-align: middle;
						text-align: center;
						width: 30px;
						font-weight:bold;
					" valign="middle">ТО<br/>ДО
                    </td>
                    <td style="width: 230px;">
                        <table>
                            <tr style="font-size: 12px;">
                                <td style="
									vertical-align: top;
									width: 10px;
								">“
                                </td>
                                <td style="
									border-bottom: 1px solid #000000;
									vertical-align: top;
									text-align:center;
									width: 40px;
								">{{$toDay ?? ''}}
                                </td>
                                <td style="
									vertical-align: top;
									width: 10px;
								">“
                                </td>
                                <td style="
									border-bottom: 1px solid #000000;
									vertical-align: top;
									text-align:center;
									width: 100px;
								">{{$toMonth ?? ''}}
                                </td>
                                <td style="
									font-size: 13px;;
									vertical-align: top;
									width: 20px;
								">20
                                </td>
                                <td style="
									border-bottom: 1px solid #000000;
									vertical-align: top;
									width: 20px;
								">{{$toYear ?? ''}}
                                </td>
                                <td style="
									font-size: 12px;;
									vertical-align: top;
									width: 15px;
									font-weight:bold;
								">с.
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>

        </td>
    </tr>
</table>
<br/>
<br/>
<table>
    <tr>
        <td style="
			font-size: 12px;;
			vertical-align: top;
			text-align: left;
			width: 35%;
			font-weight:bold;
		">Мақомот оид ба<br/>сертификатсияи маҳсулот<br/>Орган по сертификации
        </td>
        <td style="
			text-align: left;
			font-size: 12px;
			width: 65%;
		">{{$fieldID1645_16}}
            <br/>
            {{$fieldID1645_17}}
            <br/>
            {{$fieldID1645_18}}
        </td>
    </tr>
    <tr>
        <td style="
			font-size: 13px;;
			vertical-align: top;
			text-align: left;
			width: 75%;
		">
        </td>
        <td style="
			text-align: right;
			font-size: 12px;
			width: 25%;
		">
            <div style="
				font-size: 10px;
				float: right;
				width: 100px;
			">
                ---------------------------------------<br/>
                <span style="font-weight:bold;font-size: 9px;">рамзи НУМ / код ОКП</span>
            </div>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td style="
			font-size: 12px;;
			vertical-align: top;
			text-align: left;
			width: 20%;
			font-weight:bold;
		">Маҳсулот<br/>Продукция
        </td>
        <td style="
			text-align: left;
			font-size: 12px;
			width: 55%;
		">{{$someText}}<br>{{$fieldID1645_20}}, {{$fieldID1645_21}}, {{$fieldID1645_22}}
        </td>
        <td style="
			text-align: right;
			width: 25%;
		">
            <br/>
            <div style="
				font-size: 12px;
				width: 100px;
				float:right;
			"><span style="font-size: 12px;text-align:center;">{{$fieldID1645_19}}</span><br>
                <span style="font-size:10px">---------------------------------------</span><br/>
                <span style="font-size: 9px;font-weight:bold;">рамзи НМ ФИХ / код ТВ ВЭД</span>
            </div>
        </td>
    </tr>
</table>
<br>
<table>
    <tr>
        <td style="
			font-size: 12px;;
			vertical-align: top;
			text-align: left;
			font-weight:bold;
			width:40%;
		">Ба талаботи ҳуҷҷатҳои меъёрии<br/>зерин мутобиқат мекунад<br>Соответствует требованиям<br/>нормативных
            документов
        </td>
        <td style="font-size: 12px;width:60%;">{{$fieldID1645_23}}
        </td>
    </tr>
    <tr>
        <td style="
			font-size: 11px;
			vertical-align: top;
			text-align: left;
			font-weight:bold;
		">
        </td>
        <td></td>
    </tr>
</table>
<table>
    <tr>
        <td style="
			font-size: 12px;;
			vertical-align: top;
			text-align: left;
			font-weight:bold;
			width:40%;
		">Истеҳсол шудааст<br>Изготовлено
        </td>
        <td style="font-size: 12px;width:60%;">{!!  $productProducerCountries!!}</td>
    </tr>
    <tr>
        <td style="
			font-size: 12px;
			vertical-align: top;
			text-align: left;
			font-weight:bold;
		">
        </td>
    </tr>
</table>
<br/>
<table>
    <tr>
        <td style="
			font-size: 12px;;
			vertical-align: top;
			text-align: left;
			font-weight:bold;
			width:40%;
		">Сертификат дода шуд<br><span style="
			font-size: 11px;
			font-weight:bold;
		">Сертификат выдан</span>
        </td>
        <td style="font-size: 12px;width:60%">{{$safSideName}}<br>{{$safSideInfo}}
        </td>
    </tr>
    <tr>
        <td style="
			font-size: 12px;
			vertical-align: top;
			text-align: left;
			width:60%;
		">
        </td>
    </tr>
</table>
<br/>
<table>
    <tr>
        <td style="
			font-size: 12px;;
			vertical-align: top;
			text-align: left;
			font-weight:bold;
			width:40%;
		">Дар асосӣ<br><span style="
			font-size: 11px;
			font-weight:bold;
		">На основании
        </span>
        </td>
        <td style="font-size: 12px;width:60%;">{{$fieldID1645_24}}
        </td>
    </tr>
    <tr>
        <td style="
			font-size: 12px;
			vertical-align: top;
			text-align: left;
		">
        </td>
        <td></td>
    </tr>
</table>
<br/>
<table>
    <tr>
        <td style="
			font-size: 12px;;
			vertical-align: top;
			text-align: left;
			font-weight:bold;
			width:40%;
		">Маълумоти иловагӣ<br><span style="font-size: 11px;font-weight:bold;">Допалнительная информация</span>
        </td>
        <td style="
			font-size: 12px;
			vertical-align: top;
			text-align: left;
			width:60%;
			height:85px;
		">{{$fieldID1645_25}}
        </td>
    </tr>
</table>
<br/>
<br/>
<br/>
<table>
    <tr>
        <td style="
			font-size: 20px;
			font-weight: bold;
			vertical-align: top;
			text-align: center;
			width: 15%;
		"><br/><br/>Ҷ.М.<br/>М.П.
        </td>
        <td style="
			vertical-align: top;
			text-align: left;
			width: 85%;
		">
            <table>
                <tr>
                    <td style="
						font-size: 13px;;
						vertical-align: top;
						text-align: left;
						width: 190px;
					">
                        <table>
                            <tr>
                                <td style="
									font-size: 12px;
									vertical-align: top;
									text-align: left;
									font-weight:bold;
								">Роҳбари мақомот
                                </td>
                            </tr>
                            <tr>
                                <td style="
									font-size: 11px;
									vertical-align: top;
									text-align: left;
									font-weight:bold;
								">Руководитель органа
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="
						font-size: 10px;
						text-align: center;
						width: 118px;
					"><span style="color:white;font-size: 12px;text-align: center;">emptyText</span>
                        -----------------------------<br/>
                        <span style="font-size: 9px;font-weight:bold;">имзо / подпись</span>
                    </td>
                    <td style="
						font-size: 10px;
						text-align: right;
						width: 200px;
					"><span style="font-size: 12px;text-align: center;">{{$fieldID1645_26}}</span>
                        -------------------------------------------------<br/>
                        <span style="font-size: 9px;font-weight:bold;">ному насаб / инициалы, фамилия</span>
                    </td>
                </tr>
            </table>
            <br/>
            <br/>
            <table>
                <tr>
                    <td style="
						font-size: 13px;;
						vertical-align: top;
						text-align: left;
						width: 200px;
					">
                        <table>
                            <tr>
                                <td style="
									font-size: 12px;
									vertical-align: top;
									text-align: left;
									font-weight:bold;
								">Сардори Раёсат (шӯъба)
                                </td>
                            </tr>
                            <tr>
                                <td style="
									font-size: 11px;
									vertical-align: top;
									text-align: left;
									font-weight:bold;
								">Начальник управления (отдела)
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="
						font-size: 10px;
						text-align: center;
						width: 118px;
					"><span style="color:white;font-size: 12px;text-align: center;">emptyText</span>
                        -----------------------------<br/>
                        <span style="font-size: 9px;font-weight:bold;">имзо / подпись</span>
                    </td>
                    <td style="
						font-size: 10px;
						text-align: right;
						width: 200px;
					"><span style="font-size: 12px;text-align: center">{{$fieldID1645_27}}</span>
                        -------------------------------------------------<br/>
                        <span style="font-size: 9px;font-weight:bold;">ному насаб / инициалы, фамилия</span>
                    </td>
                </tr>
            </table>

        </td>
    </tr>
</table>
