<?php

use App\Models\BaseModel;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\RegionalOffice\RegionalOffice;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;

$safSideInfo = '';

$fieldID6041_1 = $customData['6041_1'] ?? '';
$fieldID6041_2 = $customData['6041_2'] ?? '';
$fieldID6041_6 = $customData['6041_6'] ?? '';
$fieldID6041_18 = $customData['6041_18'] ?? '';
$fieldID6041_27 = $customData['6041_27'] ?? '';
$fieldID6041_28 = $customData['6041_28'] ?? '';
$fieldID6041_29 = $customData['6041_29'] ?? '';
$fieldID6041_30 = $customData['6041_30'] ?? '';
$fieldID6041_31 = $customData['6041_31'] ?? '';
$fieldID6041_32 = $customData['6041_32'] ?? '';
$fieldID6041_34 = $customData['6041_34'] ?? '';

if (!empty($fieldID6041_2)) {
    $fromDay = date('d', strtotime($fieldID6041_2));
    $fromMonth = month(date('m', strtotime($fieldID6041_2)), BaseModel::LANGUAGE_CODE_TJ);
    $fromYear = date('y', strtotime($fieldID6041_2));
}
if (!empty($fieldID6041_6)) {
    $fromDay1 = date('d', strtotime($fieldID6041_6));
    $fromMonth1 = month(date('m', strtotime($fieldID6041_6)), BaseModel::LANGUAGE_CODE_TJ);
    $fromYear1 = date('y', strtotime($fieldID6041_6));
}
if (!empty($fieldID6041_30)) {
    $fromDay2 = date('d', strtotime($fieldID6041_30));
    $fromMonth2 = month(date('m', strtotime($fieldID6041_30)), BaseModel::LANGUAGE_CODE_TJ);
    $fromYear2 = date('y', strtotime($fieldID6041_30));
}

$approvedQuantity = $subDivision = $regionalOfficeAddress = $regionalOfficePhoneNumber = $regionalOfficeInfo = '';

$subdivisionId = $subApplication->agency_subdivision_id;
if (!is_null($subdivisionId)) {
    $subDivision = optional(getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME, $subdivisionId));
}
$regionalOffice = RegionalOffice::select('address', 'phone_number')->where('office_code', optional($subDivision)->code)->active()->first();

if (!is_null($regionalOffice)) {
    $regionalOfficeAddress = $regionalOffice->address;
    $regionalOfficePhoneNumber = $regionalOffice->phone_number;
    $regionalOfficeEmail = $regionalOffice->emaile;

    $regionalOfficeInfo = $regionalOfficeAddress . ' ' . $regionalOfficePhoneNumber . ' ' . $regionalOfficeEmail;
}

$productPlaceType = $productPlaceType = $productCountries = [];

foreach ($subAppProducts as $key => $subAppProduct) {

    $numberOfPlaces = $subAppProduct->number_of_places;
    $measurementUnit = optional(getReferenceRows(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $subAppProduct->measurement_unit))->name;
    $measurementUnit2 = optional(getReferenceRows(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $subAppProduct->measurement_unit_2))->name;

    $productsInfo[] = $subAppProduct['commercial_description'] . ' ' . $numberOfPlaces . ' мест ' . optional($subAppProduct)->quantity . ' ' . $measurementUnit . ' ' . optional($subAppProduct)->quantity_2 . ' ' . $measurementUnit2;
    $productCountries[] = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $subAppProduct['producer_country']))->name;
}

$productCountries = array_unique($productCountries);
$productCountriesList = implode(', ', $productCountries);

$productsInfoList = implode(', ', $productsInfo);

$safSideName = $saf->data['saf']['sides']['import']['name'] ?? '';
$safSideAddress = $saf->data['saf']['sides']['import']['address'] ?? '';
$safSideTypeValue = $saf->data['saf']['sides']['import']['type_value'] ?? '';
$safSideCommunity = $saf->data['saf']['sides']['import']['community'] ?? '';

$safSideInfo .= $safSideTypeValue . ' ';

$referenceLegalEntity = ReferenceTable::legalEntityValues();
$legalEntityType = array_search($saf->data['saf']['sides']['import']['type'], $referenceLegalEntity);

if ($legalEntityType == BaseModel::ENTITY_TYPE_USER_ID) {
    $safSideImporterPassport = $saf->data['saf']['sides']['import']['passport'] ?? '';
    $safSideInfo .= $safSideImporterPassport . ' ';
}

$safSideName = (!empty($safSideName) && $safSideName != '-') ? $safSideName : '';
$safSideAddress = (!empty($safSideAddress) && $safSideAddress != '-') ? $safSideAddress : '';
$safSideCommunity = (!empty($safSideCommunity) && $safSideCommunity != '-') ? $safSideCommunity : '';


$safSideInfo .= (!empty($safSideName)) ? $safSideName : ' ';
$safSideInfo .= (!empty($safSideCommunity)) ? ' ' . $safSideCommunity : ' ';
$safSideInfo .= (!empty($safSideAddress)) ? ' ' . $safSideAddress : ' ';

$availableProductsIds = $subApplication->productList->pluck('id')->all();
$attachedDocumentProducts = SingleApplicationSubApplications::getAttachedDocumentProductsForPDF($subApplication, $availableProductsIds);

$documentInfo = '';

foreach ($attachedDocumentProducts as $attachedDocumentProduct) {
    $document = (is_null($attachedDocumentProduct->document)) ? $attachedDocumentProduct : $attachedDocumentProduct->document;

    if ($document->document_type == App\Models\Document\Document::DOCUMENT_CODE_FOR_PDF_TEMPLATE) {
        $documentNumber = $document->document_number;
        $documentReleaseDate = $document->document_release_date;
        $documentInfo .= 'Протокол ' . $documentNumber . ' ' . $documentReleaseDate . ', ';
    }
}

$labInfo = !empty($fieldID6041_18) ? optional(getReferenceRows(ReferenceTable::REFERENCE_LABORATORIES, $fieldID6041_18))->name : '';
?>
<table>
    <tr>
        <td>
            <table>
                <tr>
                    <td style="width: 680px;"></td>
                    <td style="width: 300px;">
                        <table>
                            <tr>
                                <td style="font-size: 12px;font-weight: bold;line-height: 1.4;text-align:center;">
                                    Утверждаю
                                </td>

                            </tr>
                            <tr>
                                <td style="font-size: 12px;line-height: 1.4;text-align:right;">Главный Государственный
                                    санитарный врач
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 12px;line-height: 1.4;text-align:right;">Республики Таджиктстан
                                    заместитель Министра
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 750px;"></td>
                    <td style="width: 225px;">
                        <table>
                            <tr>
                                <td style="border-bottom: 1px solid #000000;vertical-align: top;width:160px;">&nbsp;</td>
                                <td style="font-size: 12px;text-align:left;">{{$fieldID6041_34}}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width:750px;"></td>
                    <td style="width:225px;">
                        <table>
                            <tr>
                                <td style="font-size: 12px;text-align:right;width:50px">от «{{$fromDay ?? ''}}»</td>
                                <td style="font-size: 12px;text-align:right;width:10px"></td>
                                <td style="font-size: 12px;text-align:center;border-bottom: 1px solid #000000;vertical-align: top;width:110px">{{$fromMonth ?? ''}}</td>
                                <td style="font-size: 12px;text-align:right;width:60px">20{{($fromYear ?? '').' года'}}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="font-size: 13px;text-align:center;line-height: 1.6;font-weight:bold;">
            Регистрационный номер {!!$fieldID6041_1!!} от «{!!$fromDay ?? '' !!}» {!!$fromMonth ?? ''!!}
            20{!!$fromYear ?? ''!!} г.
        </td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.0;text-align:center;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:left;width:110px;line-height: 1.4;font-weight:bold">Заключение выдано</td>
        <td style="border-bottom: 1px solid #000000;vertical-align: top;text-align:center;font-size: 11px;">{!!$productsInfoList!!}</td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.4;text-align:left;width:140px;"></td>
        <td style="font-size: 10px;text-align:center;line-height: 1.4;"><span>(на соответствие (несоответствие) санитарно-гигиеническим правилам факторов среды обитания, хозяйственную и иной вид деятельности, продукции, </span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:left;width:1px;line-height: 1.4;font-weight:bold"></td>
        <td style="border-bottom: 1px solid #000000;vertical-align: top;text-align:center;font-size: 11px;">{!!'для ввоза в РТ, производство '.$productCountriesList!!}</td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.4;text-align:left;width:1px;line-height: 1.4;"></td>
        <td style="font-size: 10px;text-align:center;line-height: 1.4;"><span>Работ и услуг, проектов нормативных актов, проектов строительства объектов,  эксплуатационной документации и др.)</span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:left;width:30px;line-height: 1.4;font-weight:bold">Кому</td>
        <td style="border-bottom: 1px solid #000000;vertical-align: top;text-align:center;font-size: 11px;">{!!$safSideInfo!!}</td>
    </tr>
    <tr>
        <td style="font-size: 13px;text-align:left;width:40px;line-height: 1.4;"></td>
        <td style="font-size: 10px;text-align:center;line-height: 1.4;"><span>(<span style="font-weight: bold;text-align:center;line-height: 1.4;">для юридических лиц</span> наименование и юридический адрес предприятия, организации, учреждения, получившего заключение;
                <span style="font-weight: bold;text-align:center;line-height: 1.4;">Для физических лиц</span> фамилия, имя, отчество, паспортные данные): серия, номер, кем и когда выдан, место жительства)
            </span>
        </td>
    </tr>
    <tr>
        <td style="line-height: 0.5;text-align:center;"></td>
    </tr>
    <tr>
        <td style="width:1px;line-height: 1.4;"></td>
        <td style="font-size: 11px;text-align:left;line-height: 1.4;">На основании резлультатов санитарно-эпидемиологических экспертиз, расследованний, обследований,
            исследований,
            испытаний, токсикологических, гигиенических исследований иных видов оценок (<span
                    style="font-weight: bold;text-align:center;line-height: 1.4;">нужное подчеркнуть</span>).
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:left;width:175px;line-height: 1.4;font-weight:bold">Настоящим удостоверяется, что</td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;">{!!$documentInfo!!}</td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.4;text-align:left;width:230px;line-height: 1.4;"></td>
        <td style="font-size: 10px;text-align:center;line-height: 1.4;"><span>(наименование)</span></td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.4;text-align:left;width:1px;line-height: 1.4;"></td>
        <td style="font-size: 11px;text-align:left;line-height: 1.4;">Соответствует (не соответствует) санитарным нормам и правилам, пригодно(ы), условно пригодно(ы),
            непригдно(ы), <span style="font-weight: bold;text-align:center;line-height: 1.4;">разрешается</span> к
            производству, применению (использованию строительству, ввозу и вывозу,
            реализации на территории Республики Таджикистан <span style="font-weight:bold">(нужное подчеркнуть)</span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 0.5;text-align:left;width:1px;"></td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.4;text-align:left;width:1px;line-height: 1.4;"></td>
        <td style="font-size: 13px;text-align:center;line-height: 1.6">СРОК ДЕЙСТВИЯ ЗАКЛЮЧЕНИЯ ДО «{!!$fromDay1 ?? ''!!}» {!!$fromMonth1 ?? ''!!} 20{!!$fromYear1 ?? ''!!} г.
        </td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 0.5;text-align:left;width:1px;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:left;width:430px;line-height: 1.4;font-weight:bold">ГЛАВНЫЙ ГОСУДАРСТВЕННЫЙ САНИТАРНЫЙ ВРАЧ (ИЛИ ЗАМЕСТИТЕЛЬ)
        </td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;">{!!$fieldID6041_29!!}</td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.4;text-align:left;width:500px;"></td>
        <td style="font-size: 10px;text-align:center;line-height: 1.4;"><span>(Республика Таджикистан, область, город, район,)</span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:430px;line-height: 1.4;">
            <table>
                <tr>
                    <td style="font-size: 11px;line-height: 1.4;text-align:left;font-weight:bold;">М.П.</td>
                    <td style="font-size: 11px;line-height: 1.4;text-align:right;font-weight:bold;">Подпись</td>
                </tr>
            </table>
        </td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;">{!!$fieldID6041_28!!}</td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.4;text-align:left;width:500px;line-height: 1.4;"></td>
        <td style="font-size: 10px;text-align:center;line-height: 1.4;"><span>(Фамилия) (Имя)</span></td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 0.5;text-align:left;width:1px;"></td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.4;text-align:left;width:1px;line-height: 1.4;"></td>
        <td style="font-size: 13px;text-align:center;line-height: 1.6;">ЗАКЛЮЧЕНИЕ ПРОДЛЕНО ДО «{!!($fromDay2 ?? '')!!}» {!!($fromMonth2 ?? '')!!} 20{!!($fromYear2 ?? '')!!} г.</td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 0.5;text-align:left;width:1px;line-height: 1.4;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:left;width:430px;line-height: 1.4;font-weight:bold">ГЛАВНЫЙ ГОСУДАРСТВЕННЫЙ САНИТАРНЫЙ ВРАЧ (ИЛИ ЗАМЕСТИТЕЛЬ)</td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;">{!!$fieldID6041_32!!}</td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.4;text-align:left;width:500px;line-height: 1.4;"></td>
        <td style="font-size: 10px;text-align:center;line-height: 1.4;"><span>(Республика Таджикистан, область, город, район,)</span></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:430px;line-height: 1.4;">
            <table>
                <tr>
                    <td style="font-size: 11px;line-height: 1.4;text-align:left;font-weight:bold;">
                        М.П.
                    </td>
                    <td style="font-size: 11px;line-height: 1.4;text-align:right;font-weight:bold;">
                        Подпись
                    </td>
                </tr>
            </table>
        </td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;">{!!$fieldID6041_31!!}</td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.4;text-align:left;width:500px;line-height: 1.4;"></td>
        <td style="font-size: 10px;text-align:center;line-height: 1.4;"><span>(Фамилия) (Имя)</span></td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:left;width:1px;font-weight:bold"></td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;line-height: 1.4"></td>
    </tr>
    <tr>
        <td style="font-size: 13px;text-align:left;width:1px;"></td>
        <td style="font-size: 11px;text-align:center;font-weight:bold;line-height: 2.0"><span>АДРЕС УПОЛНОМОЧЕННОГО ОРГАНА ВЫДАВШЕГО ЗАКЛЮЧЕНИЕ:</span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:left;width:1px;"></td>
        <td style="font-size: 11px;line-height: 1.4;">
            <table>
                <tr>
                    <td style="font-size: 11px;text-align:left;width:1px;line-height: 1.4;font-weight:bold"></td>
                    <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:100%">{{ $regionalOfficeInfo}}</td>
                </tr>
                <tr>
                    <td style="font-size: 13px;line-height: 1.4;text-align:left;width:1px;"></td>
                    <td style="font-size: 10px;text-align:center;line-height: 1.4;"><span>(Область, город, район, улица, номер, тел, эл.почта)</span></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
            <table>
                <tr>
                    <td style="width: 650px;"></td>
                    <td style="width: 330px;">
                        <table>
                            <tr>
                                <td style="font-size: 12px;font-weight: bold;line-height: 1.4;text-align:center;">
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 12px;line-height: 1.4;text-align:right;">Сардухтури Давлатии
                                    Санитарии
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 12px;line-height: 1.4;text-align:right;"> Ҷумҳурии Тоҷикистон
                                    муовини Вазир
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 750px;"></td>
                    <td style="width: 225px;">
                        <table>
                            <tr>
                                <td style="border-bottom: 1px solid #000000;vertical-align: top;width:160px;"></td>
                                <td style="font-size: 12px;line-height: 1.4;text-align:left;line-height: 1.6">{{$fieldID6041_34}}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width:750px;"></td>
                    <td style="width:230px;">
                        <table>
                            <tr>
                                <td style="font-size: 12px;text-align:right;width:50px">аз «{{$fromDay ?? ''}}»</td>
                                <td style="font-size: 12px;text-align:right;width:10px"></td>
                                <td style="font-size: 12px;text-align:center;border-bottom: 1px solid #000000;vertical-align: top;width:110px">{{$fromMonth ?? ''}}</td>
                                <td style="font-size: 12px;text-align:right;width:60px">соли 20{{($fromYear ?? '')}}</td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>
        </td>
    </tr>

    <tr>
        <td style="font-size: 13px;text-align:center;line-height: 1.6;font-weight:bold;">
            Рақами бақайдгирӣ {!!$fieldID6041_1!!} аз «{!!$fromDay ?? ''!!}» {!!$fromMonth ?? ''!!}
            20{!!$fromYear ?? ''!!} г.
        </td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.0;text-align:center;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:left;width:115px;line-height: 1.4;font-weight:bold">Хулоса дода шудааст</td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;">{!!$productsInfoList!!}</td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.4;text-align:left;width:140px;line-height: 1.4;"></td>
        <td style="font-size: 10px;text-align:center;line-height: 1.4;"><span> (ба мутобиқати (номутобиқати) ба қоидаҳои санитарию гигиенӣ, омилҳои муҳити зист, фаъолияти ҳоҷагӣ ва ғаӣра, маҳсулот, қорҳо, ҳизматрасонӣ,
</span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:left;width:1px;line-height: 1.4;font-weight:bold"></td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;">{!!'для ввоза в РТ, производство '.$productCountriesList!!}</td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.4;text-align:left;width:1px;line-height: 1.4;"></td>
        <td style="font-size: 10px;text-align:center;line-height: 1.4;"><span>инчунин лоиҳаҳои санадҳои меъёрӣ, лоиҳаҳои сохтмони объектҳо ҳуҷҷатҳои истифодабарӣ ва ғ.)</span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:left;width:18px;line-height: 1.4;font-weight:bold">Ба</td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;">{!!$safSideInfo!!}</td>
    </tr>
    <tr>
        <td style="font-size: 13px;text-align:left;width:18px;line-height: 1.4;"></td>
        <td style="font-size: 10px;text-align:center;line-height: 1.4;"><span>(барои шахсони ҳуқуқӣ номгӯй ва суроғаи ҳуқуқии корхона, ташқилоте, ки хулосаро мегирад;
барои шахсони фардӣ-насаб ном, номи падар, нишондодҳои шиноснома: силсила, рақам, аз тарафи кӣ ва кай дода шудааст, макони зист)
            </span>
        </td>
    </tr>
    <tr>
        <td style="line-height: 0.5;text-align:center;"></td>
    </tr>
    <tr>
        <td style="width:1px;line-height: 1.4;"></td>
        <td style="font-size: 11px;text-align:left;line-height: 1.4;">дар асоси натиҷаҳои экспертизаи (санҷиши) санитарию эпидемиологӣ, тафтишот,
            муоинаҳо тадкиқот, озмоишҳо, тадқиқотҳои токсикологӣ гигиенӣ ва дигар навъ <br>баҳодиҳӣ (<span
                    style="font-size: 11px;font-weight:bold;">ба зери заруриаш хат кашед</span>).
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:left;width:170px;line-height: 1.4;font-weight:bold">Бо ин тасдиқ карда мешавад, ки
        </td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;">{!!$documentInfo!!}</td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.4;text-align:left;width:230px;line-height: 1.4;"></td>
        <td style="font-size: 10px;text-align:center;line-height: 1.4;"><span>(номгӯй)</span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.4;text-align:left;width:1px;line-height: 1.4;"></td>
        <td style="font-size: 11px;text-align:left;line-height: 1.4;">Ба меъёрҳо ва қоидаҳои санитарӣ мутобиқ (номутобиқ) аст, қобили қабул (анд), шартан қобили қабул (анд),
            ғайри қобили қабул (анд) аст. ба истеҳсол, татбиқ (истифодабарӣ),
            сохтмон, воридоту содирот ва фурӯш дар миқёси Ҷумҳурии Тоҷикистон иҷозат дода мешавад .
            <span style="font-weight:bold">(ба зери заруриаш хат кашед)</span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.0;text-align:left;width:1px;"></td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.4;text-align:left;width:1px;line-height: 1.4;"></td>
        <td style="font-size: 13px;text-align:center;line-height: 1.6">МӮҲЛАТИ АМАЛКУНИИ ХУЛОСА ТО «{!!$fromDay1 ?? ''!!}» {!!$fromMonth1 ?? ''!!} 20{!!$fromYear1 ?? ''!!}
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:290px;line-height: 1.4;">
            <table>
                <tr>
                    <td style="font-size: 11px;line-height: 1.4;text-align:left;font-weight:bold;">
                        Ҷ.М.
                    </td>
                    <td style="font-size: 11px;line-height: 1.4;text-align:right;font-weight:bold;">
                        Имзо
                    </td>
                </tr>
            </table>
        </td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;">{!!$fieldID6041_28!!}</td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.4;text-align:left;width:300px;line-height: 1.2;"></td>
        <td style="font-size: 10px;text-align:center;line-height: 1.2;"><span>(Насаб) (Ном)</span></td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.4;text-align:left;width:1px;line-height: 0.5;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:left;width:290px;line-height: 1.2;font-weight:bold">САРДУХТУРИ ДАВЛАТИИ САНИТАРӢ (Ё МУОВИН)</td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;">{!!$fieldID6041_29!!}</td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.2;text-align:left;width:300px;"></td>
        <td style="font-size: 10px;text-align:center;line-height: 1.2;"><span>(Ҷумҳурии Тоҷикистон, вилоят, шаҳр ноҳия)</span></td>
    </tr>

    <tr>
        <td style="font-size: 13px;line-height: 0.8;text-align:left;width:1px;"></td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.4;text-align:left;width:1px;line-height: 1.2;"></td>
        <td style="font-size: 13px;text-align:center;line-height: 1.4;">МӮҲЛАТИ ХУЛОСА ТО «{!!$fromDay2 ?? ''!!}» {!!$fromMonth2 ?? ''!!} 20{!!$fromYear2 ?? ''!!} ДАРОЗ КАРДА ШУД
        </td>
    </tr>

    <tr>
        <td style="font-size: 11px;width:290px;line-height: 1.2;">
            <table>
                <tr>
                    <td style="font-size: 11px;line-height: 1.4;text-align:left;font-weight:bold;">
                        Ҷ.М.
                    </td>
                    <td style="font-size: 11px;line-height: 1.4;text-align:right;font-weight:bold;">
                        Имзо
                    </td>
                </tr>
            </table>
        </td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;">{!!$fieldID6041_31!!}</td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.4;text-align:left;width:300px;line-height: 0.8;"></td>
        <td style="font-size: 10px;text-align:center;line-height: 1.4;"><span>(Насаб) (Ном)</span></td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.4;text-align:left;width:1px;line-height: 1.4;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:left;width:290px;line-height: 1.4;font-weight:bold">САРДУХТУРИ ДАВЛАТИИ САНИТАРӢ (Ё МУОВИН)</td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;">{!!$fieldID6041_32!!}</td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.4;text-align:left;width:300px;line-height: 1.4;"></td>
        <td style="font-size: 10px;text-align:center;line-height: 1.4;"><span>(Ҷумҳурии Тоҷикистон, вилоят, шаҳр ноҳия)</span></td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:left;width:1px;font-weight:bold"></td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;line-height: 1.2"></td>
    </tr>
    <tr>
        <td style="font-size: 13px;text-align:left;width:1px;"></td>
        <td style="font-size: 11px;text-align:center;font-weight:bold;line-height: 2.0"><span>СУРОҒАИ МАҚОМОТИ ВАКОЛАТДОРЕ КИ ХУЛОСАРО ДОДААСТ:</span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:left;width:1px;"></td>
        <td style="font-size: 11px;line-height: 1.4;">
            <table>
                <tr>
                    <td style="font-size: 11px;text-align:left;width:1px;line-height: 1.2;font-weight:bold"></td>
                    <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:100%">{{ $regionalOfficeInfo}}</td>
                </tr>
                <tr>
                    <td style="font-size: 13px;line-height: 1.2;text-align:left;width:1px;"></td>
                    <td style="font-size: 10px;text-align:center;line-height: 1.4;"><span> (Вилоят, шаҳр, ноҳия, кӯча, рақам, тел, почтаи электронӣ)</span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>