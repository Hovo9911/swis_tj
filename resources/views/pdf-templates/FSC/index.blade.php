<?php

use App\Models\ConstructorTemplates\ConstructorTemplates;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationProductsBatch\SingleApplicationProductsBatch;

$fieldID6012_5 = $customData['6012_5'] ?? '';
$fieldID6012_6 = $customData['6012_6'] ?? '';

if (!empty($fieldID6012_5) && !empty($fieldID6012_6)) {

    $interval = date_diff(date_create($fieldID6012_5), date_create($fieldID6012_6));
    $daysDiff = $interval->format('%a');
}

$transportType = $saf->data['saf']['transportation']['type_of_transport'] ?? '';
$safGeneralRegime = $saf->data['saf']['general']['regime'] ?? '';

if ($transportType) {
    $transportType = optional(getReferenceRows(ReferenceTable::REFERENCE_TRANSPORT_TYPE, $transportType))->name;
}

$products = $productCountries = $productCountriesList = $productProducersName = [];
$productsInfo = $transCountriesList = '';


foreach ($subAppProducts as $key => $subAppProduct) {

    $productBatches = SingleApplicationProductsBatch::select(['approved_quantity'])->where('saf_product_id', $subAppProduct->id)->get();
    $totalQuantity = 0;

    foreach ($productBatches as $productBatch) {
        $totalQuantity += $productBatch->approved_quantity;
    }

    $refMeasurementUnit = getReferenceRows(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $subAppProduct['measurement_unit']);
    $measurementUnit = optional($refMeasurementUnit)->name;
    $measurementUnitText = (!is_null($measurementUnit)) ? $measurementUnit : '';
    $productsInfo .= $subAppProduct['commercial_description'] . ' в количестве ' . $totalQuantity . ' ' . $measurementUnitText . ', ';
    $productCountries[] = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $subAppProduct['producer_country']))->name;
    $productProducersName[] = $subAppProduct['producer_name'];
}


$transitCountries = $saf->data['saf']['transportation']['transit_country'] ?? '';
$safTransportationAppointment = $saf->data['saf']['transportation']['appointment'] ?? '';

if (!empty($safTransportationAppointment)) {
    $safTransportationAppointment = getReferenceRows(ReferenceTable::REFERENCE_CUSTOM_BODY_REFERENCE, $safTransportationAppointment, false, ['name', 'address']);
    $safTransportationAppointmentName = optional($safTransportationAppointment)->name;
    $safTransportationAppointmentAddress = optional($safTransportationAppointment)->address;
}

foreach ($transitCountries as $transitCountry) {
    if (!is_null($transitCountry)) {
        $transCountries[] = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $transitCountry))->name;
    }
}

if (!empty($transCountries)) {
    $transCountries = array_unique($transCountries);
    $transCountries = array_reverse($transCountries);
    $transCountriesList = "по маршруту " . implode(', ', $transCountries);
}

if (!empty($safTransportationExportCountry)) {
    $safTransportationExportCountry = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safTransportationExportCountry))->name;
}

$productCountries = array_unique($productCountries);
$productProducersName = array_unique($productProducersName);

$productCountriesList = implode(',', $productCountries);
$productProducersList = implode(',', $productProducersName);

switch ($safGeneralRegime) {
    case SingleApplication::REGIME_IMPORT:
        $safTransportationExportCountry = $saf->data['saf']['transportation']['export_country'] ?? '';

        if (!empty($safTransportationExportCountry)) {
            $safTransportationExportCountry = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safTransportationExportCountry))->name;
        }

        $safTransportationImportCountryText = '';
        $safSideName = $saf->data['saf']['sides']['import']['name'] ?? '';
        $fixText1 = 'Главгосветинспекторам<br>' . $safTransportationExportCountry;
        $fixText2 = "Управление Комитета<br>на государственной границе и<br>транспорте<br>" . $safSideName;
        $fixText3 = "на ввоз в Республику";
        break;
    case SingleApplication::REGIME_EXPORT:
        $safTransportationImportCountry = $saf->data['saf']['transportation']['import_country'] ?? '';

        if (!empty($safTransportationImportCountry)) {
            $safTransportationImportCountry = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safTransportationImportCountry, false, [], 'get', false, false, [], false, ConstructorTemplates::LANGUAGE_CODE_RU))->name;
        }

        $safTransportationImportCountryText = $safTransportationImportCountry . " " . trans("swis.pdf.{$template}.fix_text");
        $safSideName = $saf->data['saf']['sides']['export']['name'] ?? '';
        $fixText2 = "Управление Комитета<br> на государственной границе и<br> транспорте<br>" . $safSideName;
        $fixText3 = "на вывоз из Республики";
        break;
    default:
        $safTransportationImportCountryText = '';
        $safSideName = $saf->data['saf']['sides']['import']['name'] ?? '';
        $fixText2 = "Управление Комитета<br>на государственной границе и<br>транспорте<br>" . $safSideName;
        $fixText3 = "на ввоз в Республику";
        break;
}

?>
<table>
    <tr>
        <td style="height:250px"></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
</table>
<table>
    <tr>
        <td style="width:20%"></td>
        <td style="width:40%;text-align:right;font-size: 13px"><span style="color:white">{!! $fixText1 ?? '' !!}</span><br>Копия։&nbsp;&nbsp;
        </td>
        <td style="width:40%;text-align:left;font-size: 13px">{!! $fixText1 ?? '' !!}<br>{!! $fixText2 ?? '' !!}</td>
    </tr>
</table>
<br>
<br>
<br>
<br>
<table>
    <tr>
        <td style="font-size: 13px;text-align: left;width:3%;line-height: 1.6;"></td>
        <td style="font-size: 13px;text-align: justify;width:94%;line-height: 1.6;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Комитет продовольственной безопасности при Правительстве
            Республики Таджикистан разрешает ООО «{{$safSideName}}» {{$fixText3}}
            Таджикистан средством {{$transportType}} (а), {{$productsInfo}} страна производства {{$productCountriesList}} компания
            «{{$productProducersList}}»
            {{$transCountriesList}} назначением {{$safTransportationImportCountryText}}
            {{$safTransportationAppointmentName." ".$safTransportationAppointmentAddress}} Республика Таджикистан с
            соблюдением ветеринарно-санитарных правил при
            перевозке.<br><br><br>Срок действия данного разрешения {{$daysDiff ?? ''}} дней.
        </td>
        <td style="font-size: 13px;text-align: left;width:3%;line-height: 1.6;"></td>
    </tr>
</table>
<br><br><br><br>
<table width="100%" border="0">
    <tr>
        <td style="width: 10%;"></td>
        <td style="font-size: 13px;text-align: left;width: 40%;">
            Председатель
        </td>
        <td style="font-size: 13px;line-height: 20px;text-align: right;width: 40%;">
            {{$customData['6012_11'] ?? ''}}
        </td>
        <td style="width: 10%;"></td>
    </tr>
</table>
