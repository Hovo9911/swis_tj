<?php

use App\Http\Controllers\Core\Dictionary\DictionaryManager;
use App\Models\BaseModel;
use App\Models\ReferenceTable\ReferenceTable;

$dictionaryManager = new DictionaryManager();

// ----- 2.
$fieldId01207_25 = notEmptyForPdfFields($customData, '01207_25');

$keyFromDate_ru = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.from_date", BaseModel::LANGUAGE_CODE_RU);
$keyFromDate_en = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.from_date", BaseModel::LANGUAGE_CODE_EN);
$keyFromDate_tj = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.from_date", BaseModel::LANGUAGE_CODE_TJ);
$keyFromDate = $keyFromDate_tj . ' / ' . $keyFromDate_en . ' / ' . $keyFromDate_ru . ' ';

$keyC_ru = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.c", BaseModel::LANGUAGE_CODE_RU);

if (!empty($fieldId01207_25)) {
    $time = strtotime($fieldId01207_25);
    $fieldId01207_25_day = date('d', $time);
    $fieldId01207_25_month = month(date('m', $time), BaseModel::LANGUAGE_CODE_TJ);
    $fieldId01207_25_year = date('Y', $time);
    $fieldId01207_25 = '«' . $fieldId01207_25_day . '» ' . $fieldId01207_25_month . ' ' . $fieldId01207_25_year . ' ' . $keyC_ru;
} else {
    $fieldId01207_25 = '«____» ________________ 20____' . $keyC_ru;
}

// ----- 3.
$fieldId01207_42 = notEmptyForPdfFields($customData, '01207_42');
$keyDocumentGroup_tj = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.document_group", BaseModel::LANGUAGE_CODE_TJ);
$keyDocumentGroup_en = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.document_group", BaseModel::LANGUAGE_CODE_EN);
$keyDocumentGroup_ru = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.document_group", BaseModel::LANGUAGE_CODE_RU);

$keyNameSurname = '(' . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.name_surname", BaseModel::LANGUAGE_CODE_TJ)
. ' / ' . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.name_surname", BaseModel::LANGUAGE_CODE_EN)
. ' / ' . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.name_surname", BaseModel::LANGUAGE_CODE_RU) . ')';

$keyForParticipation_tj = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.for_participation", BaseModel::LANGUAGE_CODE_TJ);
$keyForParticipation_en = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.for_participation", BaseModel::LANGUAGE_CODE_EN);
$keyForParticipation_ru = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.for_participation", BaseModel::LANGUAGE_CODE_RU);


// ----- 4.
$fieldId01207_44 = notEmptyForPdfFields($customData, '01207_44');
$keyNameSurnamePositionOrganization = '(' . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.name_surname_position_organization", BaseModel::LANGUAGE_CODE_TJ)
    . ' / ' . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.name_surname_position_organization", BaseModel::LANGUAGE_CODE_EN)
    . ' / ' . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.name_surname_position_organization", BaseModel::LANGUAGE_CODE_RU) . ')';

// ----- 5.
$fieldId01207_45 = notEmptyForPdfFields($customData, '01207_45');

// ----- 6.
$keyTerritory_tj = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.territory", BaseModel::LANGUAGE_CODE_TJ);
$keyTerritory_en = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.territory", BaseModel::LANGUAGE_CODE_EN);
$keyTerritory_ru = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.territory", BaseModel::LANGUAGE_CODE_RU);

$fieldId01207_46 = notEmptyForPdfFields($customData, '01207_46');

$keyPlaceOfWork = '(' . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.place_of_work", BaseModel::LANGUAGE_CODE_TJ)
    . ' / ' . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.place_of_work", BaseModel::LANGUAGE_CODE_EN)
    . ' / ' . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.place_of_work", BaseModel::LANGUAGE_CODE_RU) . ')';

// ----- 7.
$keyDecontaminated_tj = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.decontaminated", BaseModel::LANGUAGE_CODE_TJ);
$keyDecontaminated_en = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.decontaminated", BaseModel::LANGUAGE_CODE_EN);
$keyDecontaminated_ru = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.decontaminated", BaseModel::LANGUAGE_CODE_RU);

$fieldId01207_21 = notEmptyForPdfFields($customData, '01207_21');
$fieldId01207_22 = notEmptyForPdfFields($customData, '01207_22');
$fieldId01207_21_and_22 = $fieldId01207_21 . ', ' . $fieldId01207_22;
$fieldId01207_21_and_22 = trim($fieldId01207_21_and_22, ', ');

$keyMethodPoison = '(' . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.method_poison", BaseModel::LANGUAGE_CODE_TJ)
    . ' / ' . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.method_poison", BaseModel::LANGUAGE_CODE_EN)
    . ' / ' . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.method_poison", BaseModel::LANGUAGE_CODE_RU) . ')';

// ----- 8.
$keyAgainst_tj = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.against", BaseModel::LANGUAGE_CODE_TJ);
$keyAgainst_en = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.against", BaseModel::LANGUAGE_CODE_EN);
$keyAgainst_ru = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.against", BaseModel::LANGUAGE_CODE_RU);

$fieldId01207_47 = notEmptyForPdfFields($customData, '01207_47');

$keyNamesOfHarmful = '(' . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.names_of_harmful", BaseModel::LANGUAGE_CODE_TJ)
    . ' / ' . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.names_of_harmful", BaseModel::LANGUAGE_CODE_EN)
    . ' / ' . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.names_of_harmful", BaseModel::LANGUAGE_CODE_RU) . ')';

// ----- 9.
$keySterilizationConditions_tj = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_conditions", BaseModel::LANGUAGE_CODE_TJ);
$keySterilizationConditions_en = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_conditions", BaseModel::LANGUAGE_CODE_EN);
$keySterilizationConditions_ru = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_conditions", BaseModel::LANGUAGE_CODE_RU);
$keySterilizationConditions = $keySterilizationConditions_tj .
    ' / ' . $keySterilizationConditions_en .
    ' / ' . $keySterilizationConditions_ru;

// ----- 11.
$commercialDescription = !empty(optional($subAppProducts)->first()->commercial_description) ? $subAppProducts->first()->commercial_description : ''; // SAF_ID_91

// ----- 12., 21.
$nettoWeight = !empty(optional($subAppProducts)->first()->netto_weight) ? $subAppProducts->first()->netto_weight : ''; // SAF_ID_99

// ----- 13., 22.
$bruttoWeight = !empty(optional($subAppProducts)->first()->brutto_weight) ? $subAppProducts->first()->brutto_weight : ''; // SAF_ID_98

// ----- 14., 23.
$quantity = !empty(optional($subAppProducts)->first()->quantity) ? $subAppProducts->first()->quantity : ''; // SAF_ID_100

$measurementUnit = '';
if (!empty(optional($subAppProducts)->first()->measurement_unit)) {
    $measurementUnit = $subAppProducts->first()->measurement_unit; // SAF_ID_101
    $measurementUnit = optional(getReferenceRows(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $measurementUnit, false, ['short_name'], 'get', false, false, [], false, BaseModel::LANGUAGE_CODE_TJ))->short_name;
}

$quantityMeasurementUnit = $quantity . ' ' . $measurementUnit;

// ----- 15., 24.
$fieldId01207_49 = notEmptyForPdfFields($customData, '01207_49');

// ----- 16.
$fieldId01207_50 = notEmptyForPdfFields($customData, '01207_50');

// ----- 17., 25.
$fieldId01207_23 = notEmptyForPdfFields($customData, '01207_23');

// ----- 18., 26.
$fieldId01207_51 = notEmptyForPdfFields($customData, '01207_51');

// ----- 19., 27.
$fieldId01207_52 = notEmptyForPdfFields($customData, '01207_52');

// ----- 20., 28.
$fieldId01207_53 = notEmptyForPdfFields($customData, '01207_53');

// ----- 29.
$keyReason_tj = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.reason", BaseModel::LANGUAGE_CODE_TJ);
$keyReason_en = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.reason", BaseModel::LANGUAGE_CODE_EN);
$keyReason_ru = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.reason", BaseModel::LANGUAGE_CODE_RU);

$fieldId01207_48 = notEmptyForPdfFields($customData, '01207_48');

// ----- 30.
$keyAdditionalInfo_tj = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.additional_info", BaseModel::LANGUAGE_CODE_TJ);
$keyAdditionalInfo_en = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.additional_info", BaseModel::LANGUAGE_CODE_EN);
$keyAdditionalInfo_ru = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.additional_info", BaseModel::LANGUAGE_CODE_RU);

$fieldId01207_35 = notEmptyForPdfFields($customData, '01207_35');

?>

<style>

    @page {
        margin-left: 25mm;
        margin-right: 25mm;
        margin-top: 60mm;
        margin-bottom: 20mm;
    }

    .fs5  { font-size: 5px; }
    .fs6  { font-size: 6px; }
    .fs7  { font-size: 7px; }
    .fs8  { font-size: 8px; }
    .fs9  { font-size: 9px; }
    .fs10 { font-size: 10px; }
    .fs11 { font-size: 11px; }
    .fs12 { font-size: 12px; }
    .fs13 { font-size: 13px; }
    .fs14 { font-size: 14px; }
    .fs15 { font-size: 15px; }
    .fs16 { font-size: 16px; }
    .fs17 { font-size: 17px; }
    .fs18 { font-size: 18px; }
    .fs19 { font-size: 19px; }
    .fs20 { font-size: 20px; }
    .fs21 { font-size: 21px; }
    .fs22 { font-size: 22px; }
    .fs23 { font-size: 23px; }
    .fs24 { font-size: 24px; }
    .fs25 { font-size: 25px; }
    .fs26 { font-size: 26px; }
    .fs27 { font-size: 27px; }
    .fs28 { font-size: 28px; }
    .fs29 { font-size: 29px; }
    .fs30 { font-size: 30px; }

    .fb { font-weight: bold; }
    .fi { font-style: italic;}
    .fl { font-weight: lighter; }
    .tl { text-align: left; }
    .tc { text-align: center; }
    .tr { text-align: right; }
    .cb { clear: both; }
    .db { display: block; }

    .fs9-5 { font-size: 9.5px; }
    .fs13-5 { font-size: 13.5px; }

    .cell  {
        border: 0.2px solid #000000;
        padding-left: 8x;
        padding-right: 6px;
        vertical-align: middle;
        text-align: center;
    }

    .fix {
        padding-top: -4px;
    }

    .title {
        border-top: 1.5px solid #000000;
    }

    .pl {
        padding-left: 30mm;
    }

    .stamp {
        padding-left: 10mm;
        vertical-align: bottom;
        padding-right: -17mm;
    }

    table {
        border-collapse: collapse;
        overflow: wrap;
        /*width: 100%;*/
    }

    table td {
        line-height: 1;
    }

    .bb {
        border-bottom: 1px solid #000000;
    }
</style>

<table style="width: 935px;">

    <tr>
        <td>
            <table>
                <tr>
                    <td width="280px"></td>
                    <td width="655px" class="fs12"><span class="fb">{{$keyFromDate}}</span>{{$fieldId01207_25}}</td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td>
            <br>
            <table>
                <tr>
                    <td width="415px" class="fb fs12">{{$keyDocumentGroup_tj}}</td>
                    <td width="400px" class="bb fs12">{{$fieldId01207_42}}</td>
                    <td width="120px" class="fb fs12" style="text-align: right" >{{$keyForParticipation_tj}}</td>
                </tr>
                <tr>
                    <td width="415px" class="fb fs8">{{$keyDocumentGroup_en}} / {{$keyDocumentGroup_ru}}</td>
                    <td width="400px" class="tc fs8">{{$keyNameSurname}}</td>
                    <td width="120px" class="fb fs8">{{$keyForParticipation_en}} / {{$keyForParticipation_ru}}</td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td width="935px" class="fs12 bb">
            <br>
            <span class="fb">1. </span><span>{{$fieldId01207_44}}</span>
        </td>
    </tr>
    <tr>
        <td width="935px" class="fs8 tc">
            {{$keyNameSurnamePositionOrganization}}
        </td>
    </tr>

    <tr>
        <td width="935px" class="fs12 bb">
            <br>
            <span class="fb">2. </span><span>{{$fieldId01207_45}}</span>
        </td>
    </tr>
    <tr>
        <td width="935px" class="fs8 tc">
            {{$keyNameSurnamePositionOrganization}}
        </td>
    </tr>

    <tr>
        <td>
            <br>
            <table>
                <tr>
                    <td width="220px" class="fs12">{{$keyTerritory_tj}}</td>
                    <td width="715px" class="bb fs12">{{$fieldId01207_46}}</td>
                </tr>
                <tr>
                    <td colspan="2" width="935px" class="fs8">
                        <span class="fb">{{$keyTerritory_en}} / {{$keyTerritory_ru}}</span>&emsp;&emsp;{{$keyPlaceOfWork}}
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td>
            <br>
            <table>
                <tr>
                    <td width="165px" class="fs12">{{$keyDecontaminated_tj}}</td>
                    <td width="770px" class="bb fs12">{{$fieldId01207_21_and_22}}</td>
                </tr>
                <tr>
                    <td width="165px" class="fs8 fb">{{$keyDecontaminated_en}} / {{$keyDecontaminated_ru}}</td>
                    <td width="770px" class="fs8 tc">{{$keyMethodPoison}}</td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td>
            <br>
            <table>
                <tr>
                    <td width="60px" class="fs12">{{$keyAgainst_tj}}</td>
                    <td width="875px" class="bb fs12">{{$fieldId01207_47}}</td>
                </tr>
                <tr>
                    <td width="60px" class="fs8 fb">{{$keyAgainst_en}} / {{$keyAgainst_ru}}</td>
                    <td width="875px" class="fs8 tc">{{$keyNamesOfHarmful}}</td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td class="tc" width="935px">
            <span class="fs12 fb">{{$keySterilizationConditions}}</span>
        </td>
    </tr>

    <tr>
        <td>
            <table border="1">
                <tr>
                    <td rowspan="2" class="tc fs8">
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.number", BaseModel::LANGUAGE_CODE_TJ)}}
                    </td>
                    <td rowspan="2" class="tc fs8">
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.disinfection", BaseModel::LANGUAGE_CODE_TJ)}} /
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.disinfection", BaseModel::LANGUAGE_CODE_EN)}} /
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.disinfection", BaseModel::LANGUAGE_CODE_RU)}}
                    </td>
                    <td rowspan="2" class="tc fs8">
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.weight", BaseModel::LANGUAGE_CODE_TJ)}} /
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.weight", BaseModel::LANGUAGE_CODE_EN)}} /
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.weight", BaseModel::LANGUAGE_CODE_RU)}}
                    </td>
                    <td rowspan="2" class="tc fs8">
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.packaging_weight", BaseModel::LANGUAGE_CODE_TJ)}} /
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.packaging_weight", BaseModel::LANGUAGE_CODE_EN)}} /
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.packaging_weight", BaseModel::LANGUAGE_CODE_RU)}}
                    </td>
                    <td rowspan="2" class="tc fs8">
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.number_pieces", BaseModel::LANGUAGE_CODE_TJ)}} /
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.number_pieces", BaseModel::LANGUAGE_CODE_EN)}} /
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.number_pieces", BaseModel::LANGUAGE_CODE_RU)}}
                    </td>
                    <td rowspan="2" class="tc fs8">
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.building", BaseModel::LANGUAGE_CODE_TJ)}} /
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.building", BaseModel::LANGUAGE_CODE_EN)}} /
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.building", BaseModel::LANGUAGE_CODE_RU)}}
                    </td>
                    <td rowspan="2" class="tc fs8">
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.xkm", BaseModel::LANGUAGE_CODE_TJ)}}
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.xkm", BaseModel::LANGUAGE_CODE_EN)}}
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.xkm", BaseModel::LANGUAGE_CODE_RU)}}
                    </td>
                    <td rowspan="2" class="tc fs8">
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.temperature", BaseModel::LANGUAGE_CODE_TJ)}} /
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.temperature", BaseModel::LANGUAGE_CODE_EN)}} /
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.temperature", BaseModel::LANGUAGE_CODE_RU)}}
                    </td>
                    <td rowspan="2" class="tc fs8">
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.processing_time", BaseModel::LANGUAGE_CODE_TJ)}} /
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.processing_time", BaseModel::LANGUAGE_CODE_EN)}} /
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.processing_time", BaseModel::LANGUAGE_CODE_RU)}}
                    </td>
                    <td colspan="2" class="tc fs8">
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.consumption", BaseModel::LANGUAGE_CODE_TJ)}} /
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.consumption", BaseModel::LANGUAGE_CODE_EN)}} /
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.consumption", BaseModel::LANGUAGE_CODE_RU)}}
                    </td>
                </tr>
                <tr>
                    <td class="tc fs8">
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.in", BaseModel::LANGUAGE_CODE_TJ)}} /
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.in", BaseModel::LANGUAGE_CODE_EN)}} /
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.in", BaseModel::LANGUAGE_CODE_RU)}}
                        <br>
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.in.description", BaseModel::LANGUAGE_CODE_RU)}}
                    </td>
                    <td class="tc fs8">
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.total", BaseModel::LANGUAGE_CODE_TJ)}} /
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.total", BaseModel::LANGUAGE_CODE_EN)}} /
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.total", BaseModel::LANGUAGE_CODE_RU)}}
                        <br>
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.total.description", BaseModel::LANGUAGE_CODE_RU)}}
                    </td>
                </tr>
                <tr>
                    <td class="fs12">1.</td>
                    <td class="fs12">{{$commercialDescription}}</td>
                    <td class="fs12">{{$nettoWeight}}</td>
                    <td class="fs12">{{$bruttoWeight}}</td>
                    <td class="fs12">{{$quantityMeasurementUnit}}</td>
                    <td class="fs12">{{$fieldId01207_49}}</td>
                    <td class="fs12">{{$fieldId01207_50}}</td>
                    <td class="fs12">{{$fieldId01207_23}}</td>
                    <td class="fs12">{{$fieldId01207_51}}</td>
                    <td class="fs12">{{$fieldId01207_52}}</td>
                    <td class="fs12">{{$fieldId01207_53}}</td>
                </tr>
                <tr>
                    <td class="fs12">&nbsp;</td>
                    <td class="fs12"></td>
                    <td class="fs12"></td>
                    <td class="fs12"></td>
                    <td class="fs12"></td>
                    <td class="fs12"></td>
                    <td class="fs12"></td>
                    <td class="fs12"></td>
                    <td class="fs12"></td>
                    <td class="fs12"></td>
                    <td class="fs12"></td>
                </tr>
                <tr>
                    <td class="fs12 fb" colspan="2">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.total", BaseModel::LANGUAGE_CODE_TJ)}} / {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.total", BaseModel::LANGUAGE_CODE_EN)}} / {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.sterilization_condition.total", BaseModel::LANGUAGE_CODE_RU)}}</td>
                    <td class="fs12">{{$nettoWeight}}</td>
                    <td class="fs12">{{$bruttoWeight}}</td>
                    <td class="fs12">{{$quantityMeasurementUnit}}</td>
                    <td class="fs12">{{$fieldId01207_49}}</td>
                    <td class="fs12">{{$fieldId01207_50}}</td>
                    <td class="fs12">{{$fieldId01207_23}}</td>
                    <td class="fs12">{{$fieldId01207_51}}</td>
                    <td class="fs12">{{$fieldId01207_52}}</td>
                    <td class="fs12">{{$fieldId01207_53}}</td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td>
            <table>
                <tr>
                    <td width="115px" class="fs12"></td>
                    <td width="160px" class="fs12">{{$keyReason_tj}}</td>
                    <td width="660px" class="fs12 bb">{{$fieldId01207_48}}</td>
                </tr>
                <tr>
                    <td width="115px" class="fs12"></td>
                    <td width="820px" class="fs8 fb" colspan="2">{{$keyReason_en}} / {{$keyReason_ru}}</td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td>
            <table>
                <tr>
                    <td width="115px" class="fs12"></td>
                    <td width="110px" class="fs12">{{$keyAdditionalInfo_tj}}</td>
                    <td width="710px" class="fs12 bb">{{$fieldId01207_35}}</td>
                </tr>
                <tr>
                    <td width="115px" class="fs12"></td>
                    <td width="820px" class="fs8 fb" colspan="2">{{$keyAdditionalInfo_en}} / {{$keyAdditionalInfo_ru}}</td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td>
            <table>
                <tr>
                    <td width="38px" class="fs8"></td>
                    <td width="39px" class="fs8">
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.stamp", BaseModel::LANGUAGE_CODE_TJ)}} /
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.stamp", BaseModel::LANGUAGE_CODE_EN)}} /
                    </td>
                    <td width="38px" class="fs8"></td>

                    <td width="70px" class="fs12">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.specialist", BaseModel::LANGUAGE_CODE_TJ)}}</td>
                    <td width="200px" class="fs12 bb"></td>

                    <td width="100px" class="fs12">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.accepted_job", BaseModel::LANGUAGE_CODE_TJ)}}</td>
                    <td width="180px" class="fs12 bb"></td>

                    <td width="120px" class="fs12">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.participation_person", BaseModel::LANGUAGE_CODE_TJ)}}</td>
                    <td width="160px" class="fs12 bb"></td>

                </tr>
                <tr>
                    <td width="38px" class="fs8"></td>

                    <td width="39px" class="fs8">
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.stamp", BaseModel::LANGUAGE_CODE_RU)}}
                    </td>

                    <td width="38px" class="fs8"></td>

                    <td width="270px" class="fs8 fb" colspan="2">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.specialist", BaseModel::LANGUAGE_CODE_EN)}} / {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.specialist", BaseModel::LANGUAGE_CODE_RU)}}</td>

                    <td width="270px" class="fs8 fb" colspan="2">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.accepted_job", BaseModel::LANGUAGE_CODE_EN)}} / {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.accepted_job", BaseModel::LANGUAGE_CODE_RU)}}</td>

                    <td width="280px" class="fs8 fb" colspan="2">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.participation_person", BaseModel::LANGUAGE_CODE_EN)}} / {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.participation_person", BaseModel::LANGUAGE_CODE_RU)}}</td>
                </tr>
            </table>
        </td>
    </tr>

</table>