<?php

use App\Models\BaseModel;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationExaminations\SingleApplicationExaminations;

$fieldID_1 = $customData['SAS_001_1'] ?? '';
$fieldID_2 = $customData['SAS_001_2'] ?? '';
$fieldID_5 = $customData['SAS_001_5'] ?? '';
$fieldID_6 = $customData['SAS_001_6'] ?? '';
$fieldID_15 = isset($customData['SAS_001_15']) ? mb_strtoupper($customData['SAS_001_15'], 'UTF-8') : '';
$fieldID_16 = $customData['SAS_001_16'] ?? '';
$fieldID_17 = $customData['SAS_001_17'] ?? '';
$fieldID_18 = $customData['SAS_001_18'] ?? '';
$fieldID_19 = $customData['SAS_001_19'] ?? '';
$fieldID_20 = $customData['SAS_001_20'] ?? '';
$fieldID_21 = $customData['SAS_001_21'] ?? '';
$fieldID_22 = $customData['SAS_001_22'] ?? '';
$fieldID_23 = $customData['SAS_001_23'] ?? '';
$fieldID_24 = $customData['SAS_001_24'] ?? '';
$fieldID_25 = $customData['SAS_001_25'] ?? '';
$fieldID_26 = $customData['SAS_001_26'] ?? '';
$fieldID_27 = $customData['SAS_001_27'] ?? '';
$fieldID_52 = $customData['SAS_001_52'] ?? '';

if (!empty($fieldID_5)) {
    $fromDay = date('d', strtotime($fieldID_5));
    $fromMonth = month(date('m', strtotime($fieldID_5)));
    $fromYear = date('y', strtotime($fieldID_5));
}
if (!empty($fieldID_6)) {
    $toDay = date('d', strtotime($fieldID_6));
    $toMonth = month(date('m', strtotime($fieldID_6)));
    $toYear = date('y', strtotime($fieldID_6));
}

$subAppProductsCountries = $subAppProducts->pluck('producer_country')->unique();

foreach ($subAppProductsCountries as $subAppProductsCountry) {
    $productProducerCountries[] = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $subAppProductsCountry))->name;
}

$productProducerCountries = implode(', ', ($productProducerCountries ?? []));

$safGeneralRegime = $saf->data['saf']['general']['regime'] ?? '';

switch ($safGeneralRegime) {
    case SingleApplication::REGIME_IMPORT:
        $safSideName = $saf->data['saf']['sides']['import']['name'] ?? '';
        $safSideAddress = $saf->data['saf']['sides']['import']['address'] ?? '';
        $safSideCommunity = $saf->data['saf']['sides']['import']['community'] ?? '';
        break;
    case SingleApplication::REGIME_EXPORT:
        $safSideName = $saf->data['saf']['sides']['export']['name'] ?? '';
        $safSideAddress = $saf->data['saf']['sides']['export']['address'] ?? '';
        $safSideCommunity = $saf->data['saf']['sides']['export']['community'] ?? '';
        break;
    default:
        $safSideName = '';
        $safSideAddress = '';
        $safSideCommunity = '';
        break;
}
$safSideCommunity = ($safSideCommunity == '-') ? '' : $safSideCommunity;
$safSideAddress = ($safSideAddress == '-') ? '' : $safSideAddress;
$safSideInfo = '';

if (!empty($safSideCommunity) AND !empty($safSideAddress)) {
    $safSideInfo = $safSideCommunity . ', ' . $safSideAddress;
} elseif (!empty($safSideCommunity)) {
    $safSideInfo = $safSideCommunity;
} elseif (!empty($safSideAddress)) {
    $safSideInfo = $safSideAddress;
}

$fieldID_1 = mb_strlen($fieldID_1, 'UTF-8') > 40 ? mb_substr($fieldID_1, 0, 40, 'UTF-8') . "..." : $fieldID_1;
$fieldID_16 = mb_strlen($fieldID_16, 'UTF-8') > 50 ? mb_substr($fieldID_16, 0, 50, 'UTF-8') . "..." : $fieldID_16;
$fieldID_17 = mb_strlen($fieldID_17, 'UTF-8') > 50 ? mb_substr($fieldID_17, 0, 50, 'UTF-8') . "..." : $fieldID_17;
$fieldID_18 = mb_strlen($fieldID_18, 'UTF-8') > 40 ? mb_substr($fieldID_18, 0, 40, 'UTF-8') . "..." : $fieldID_18;
$fieldID_19 = mb_strlen($fieldID_19, 'UTF-8') > 20 ? mb_substr($fieldID_19, 0, 20, 'UTF-8') . "... ," : $fieldID_19 . ", ";
$fieldID_20 = mb_strlen($fieldID_20, 'UTF-8') > 20 ? mb_substr($fieldID_20, 0, 20, 'UTF-8') . "... ," : $fieldID_20 . ", ";
if ($fieldID_21) {
    $fieldID_21 = mb_strlen($fieldID_21, 'UTF-8') > 20 ? mb_substr($fieldID_21, 0, 20, 'UTF-8') . "..." : $fieldID_21 . " Мест";
}
if ($fieldID_22) {
    $fieldID_22 = mb_strlen($fieldID_22, 'UTF-8') > 20 ? mb_substr($fieldID_22, 0, 20, 'UTF-8') . "..." : "(" . $fieldID_22 . " кг)";
}
$fieldID_23 = mb_strlen($fieldID_23, 'UTF-8') > 150 ? mb_substr($fieldID_23, 0, 150, 'UTF-8') . "..." : $fieldID_23;
$fieldID_24 = mb_strlen($fieldID_24, 'UTF-8') > 100 ? mb_substr($fieldID_24, 0, 100, 'UTF-8') . "..." : $fieldID_24;
$fieldID_25 = mb_strlen($fieldID_25, 'UTF-8') > 200 ? mb_substr($fieldID_25, 0, 200, 'UTF-8') . "..." : $fieldID_25;
$fieldID_26 = mb_strlen($fieldID_26, 'UTF-8') > 25 ? mb_substr($fieldID_26, 0, 25, 'UTF-8') . "..." : $fieldID_26;
$fieldID_27 = mb_strlen($fieldID_27, 'UTF-8') > 25 ? mb_substr($fieldID_27, 0, 25, 'UTF-8') . "..." : $fieldID_27;
$safSideInfo = mb_strlen($safSideInfo, 'UTF-8') > 200 ? mb_substr($safSideInfo, 0, 200) . "..." : $safSideInfo;
$safSideName = mb_strlen($safSideName, 'UTF-8') > 200 ? mb_substr($safSideName, 0, 200) . "..." : $safSideName;
$productProducerCountries = mb_strlen($productProducerCountries, 'UTF-8') > 100 ? mb_substr($productProducerCountries, 0, 100) . "..." : $productProducerCountries;

$someText = '';
if (count($subAppProducts) > 1) {
    $someText = 'Молҳои ниёзи мардум мувофики замимаи';
} else {
    $someText = $subAppProducts[0]->commercial_description;
    $someText = mb_strlen($someText, 'UTF-8') > 40 ? mb_substr($someText, 0, 40, 'UTF-8') . "..." : $someText;
}
$examinations = SingleApplicationExaminations::select('saf_examinations.*', 'saf_products.commercial_description')
    ->leftJoin('saf_products', 'saf_products.id', '=', 'saf_examinations.saf_product_id')
    ->where('saf_subapplication_id', $applicationId)
    ->whereIn('saf_examinations.saf_product_id', $availableProductsIds)->active()->get();

$productsInfo = [];
$productsSection = null;
$totalQuantity = $totalWeight = 0;

foreach ($examinations as $examination) {

    $typeOfStoneInfo = [];
    $typeOfStones = (!empty($examination->type_of_stone)) ? explode(',', $examination->type_of_stone) : '';

    if (!empty($typeOfStones)) {
        foreach ($typeOfStones as $typeOfStone) {
            $typeOfStoneRef = getReferenceRows(ReferenceTable::REFERENCE_TYPE_OF_STONES, $typeOfStone, false, ['code', 'name'], false, false, false, [], false, BaseModel::LANGUAGE_CODE_TJ) ?? '';
            $typeOfStoneInfo[] = optional($typeOfStoneRef)->name;
        }
        $typeOfStoneInfo = implode(', ', $typeOfStoneInfo);
    }

    $typeOfStoneInfo = (!is_array($typeOfStoneInfo)) ? $typeOfStoneInfo : '';
    $typeOfMetalRef = getReferenceRows(ReferenceTable::REFERENCE_TYPE_OF_METAL, $examination['type_of_metal'], false, ['code', 'sample', 'name'], false, false, false, [], false, BaseModel::LANGUAGE_CODE_TJ);

    $typeOfMetalCode = optional($typeOfMetalRef)->code;
    $typeOfMetalName = optional($typeOfMetalRef)->name;
    $typeOfMetalSample = optional($typeOfMetalRef)->sample;

    if (!isset($productsInfo[$examination['saf_product_id']][$typeOfMetalCode][$typeOfStoneInfo]['product_commercial_description'])) {
        $productsInfo[$examination['saf_product_id']][$typeOfMetalCode][$typeOfStoneInfo]['product_commercial_description'] = $examination['commercial_description'];
    }

    if (!empty($typeOfStoneInfo)) {
        $examinationCarat = (!is_null($examination['carat'])) ? $examination['carat'] : '';
        if (isset($productsInfo[$examination['saf_product_id']][$typeOfMetalCode][$typeOfStoneInfo]['carat'])) {
            $productsInfo[$examination['saf_product_id']][$typeOfMetalCode][$typeOfStoneInfo]['carat'] += $examinationCarat;
        } else {
            $productsInfo[$examination['saf_product_id']][$typeOfMetalCode][$typeOfStoneInfo]['carat'] = $examinationCarat;
        }
    }

    if (isset($productsInfo[$examination['saf_product_id']][$typeOfMetalCode][$typeOfStoneInfo]['quantity'])) {
        $productsInfo[$examination['saf_product_id']][$typeOfMetalCode][$typeOfStoneInfo]['quantity'] += $examination['quantity'];
    } else {
        if (!is_null($examination['quantity'])) {
            $productsInfo[$examination['saf_product_id']][$typeOfMetalCode][$typeOfStoneInfo]['quantity'] = $examination['quantity'];
        }
    }

    if (isset($productsInfo[$examination['saf_product_id']][$typeOfMetalCode][$typeOfStoneInfo]['weight'])) {
        $productsInfo[$examination['saf_product_id']][$typeOfMetalCode][$typeOfStoneInfo]['weight'] += $examination['weight'];
    } else {
        if (!is_null($examination['weight'])) {
            $productsInfo[$examination['saf_product_id']][$typeOfMetalCode][$typeOfStoneInfo]['weight'] = $examination['weight'];
        }
    }

    $productsInfo[$examination['saf_product_id']][$typeOfMetalCode][$typeOfStoneInfo]['sample'] = $typeOfMetalSample;
    $productsInfo[$examination['saf_product_id']][$typeOfMetalCode][$typeOfStoneInfo]['name'] = $typeOfMetalName;

}
if (count($productsInfo) > 0) {
    $silverNum = $goldNum = $platinumNum = $palladiumNum = $silver30percentNum = $bijouterieNum = 1;
    foreach ($productsInfo as $productInfo) {
        foreach ($productInfo as $typeOfMetal => $products) {
            switch (substr($typeOfMetal, 0, 2)) {
                case ReferenceTable::REFERENCE_METAL_TYPE_SILVER:
                    foreach ($products as $stoneTypes => $product) {

                        $carat = '';
                        if (isset($product['carat'])) {
                            $carat = !empty($product['carat']) ? $product['carat'] : '';
                        }

                        $totalQuantity += $product['quantity'];
                        $totalWeight += $product['weight'];

                        $productsSection .= '<tr>
                            <td class="text">' . $product['product_commercial_description'] . ' ' .
                            $stoneTypes . ' ' .
                            $carat . ' - ' .
                            $product['quantity'] . ' ' .
                            trans("swis.pdf.{$template}.key_1") . ' (' . $product['weight'] . ' ' .
                            trans("swis.pdf.{$template}.key_2") . ' )-' .
                            trans("swis.pdf.{$template}.key_3") . ' ' . $product['sample'] . ' ' . $product['name'] . '
                            </td>
                            </tr>';
                    };
                    break;
                case ReferenceTable::REFERENCE_METAL_TYPE_GOLD:
                case ReferenceTable::REFERENCE_METAL_TYPE_PLATINUM:
                case ReferenceTable::REFERENCE_METAL_TYPE_PALLADIUM:
                    foreach ($products as $stoneTypes => $product) {
                        $carat = '';
                        if (isset($product['carat'])) {
                            $carat = !empty($product['carat']) ? $product['carat'] : '';
                        }

                        $totalQuantity += $product['quantity'];
                        $totalWeight += $product['weight'];

                        $productsSection .= '<tr>
                        <td class="text">' . $product['product_commercial_description'] . ' ' .
                            $stoneTypes . ' ' .
                            $carat . ' - ' .
                            $product['quantity'] . ' ' .
                            trans("swis.pdf.{$template}.key_1") . ' (' . $product['weight'] . ' ' .
                            trans("swis.pdf.{$template}.key_2") . ' )-' .
                            trans("swis.pdf.{$template}.key_3") . ' ' . $product['sample'] . ' ' . $product['name'] . '
                        </td>
                        </tr>';
                    };
                    break;
            }
        }
    }
}

?>
<table>
    <tr>
        <td style="height:180px"></td>
    </tr>
</table>
<table align="left">
    <tr>
        <td class="fs13 fb" style="width: 140px;">Эътибор дорад аз<br/>Срок действия с</td>
        <td>
            <table>
                <tr>
                    <td style="width: 230px;">
                        <table>
                            <tr style="font-size: 12px;">
                                <td style="width: 10px;">“</td>
                                <td style="border-bottom: 1px solid #000000;text-align:center;width: 40px;">{{$fromDay ?? ''}}</td>
                                <td style="width: 10px;">“</td>
                                <td style="border-bottom: 1px solid #000000;text-align: center;width: 100px;">{{$fromMonth ?? ''}}</td>
                                <td style="font-size: 13px;width: 20px;">20</td>
                                <td style="border-bottom: 1px solid #000000;width: 20px;">{{$fromYear ?? ''}}</td>
                                <td style="font-size: 12px;width: 15px;font-weight:bold;">с.</td>
                            </tr>
                        </table>
                    </td>
                    <td style="font-size: 12px;text-align: center;width: 30px;font-weight:bold;">ТО<br/>ДО</td>
                    <td style="width: 230px;">
                        <table>
                            <tr style="font-size: 12px;">
                                <td style="width: 10px;">“</td>
                                <td style="border-bottom: 1px solid #000000;text-align:center;width: 40px;">{{$toDay ?? ''}}</td>
                                <td style="width: 10px;">“</td>
                                <td style="border-bottom: 1px solid #000000;text-align:center;width: 100px;">{{$toMonth ?? ''}} </td>
                                <td style="font-size: 13px;width: 20px;">20</td>
                                <td style="border-bottom: 1px solid #000000;width: 20px;">{{$toYear ?? ''}}</td>
                                <td style="font-size: 12px;width: 15px;font-weight:bold;">с.</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br/>
<br/>
<table>
    <tr>
        <td style="font-size: 12px;text-align: left;width: 35%;font-weight:bold;">Мақомот оид ба<br/>сертификатсияи
            маҳсулот<br/>Орган по сертификации
        </td>
        <td style="text-align: left;font-size: 12px;width: 65%;">{{$fieldID_15}}
            <br/>
            {{$fieldID_16}}
            <br/>
            {{$fieldID_17 .' '. $fieldID_1}}
        </td>
    </tr>
    <tr>
        <td style="font-size: 13px;text-align: left;width: 75%;"></td>
        <td style="text-align: right;font-size: 12px;width: 25%;">
            <div style="font-size: 10px;float: right;width: 100px;">
                ---------------------------------------<br/>
                <span style="font-weight:bold;font-size: 9px;">рамзи НУМ / код ОКП</span>
            </div>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td style="font-size: 12px;text-align: left;width: 30%;font-weight:bold;">Маҳсулот<br/>Продукция
        </td>
        <td style="text-align: left;font-size: 12px;width: 45%;">
            <table>
                @if (count($productsInfo) > 0)
                    <tr>
                        <td class="title">{{trans("swis.pdf.{$template}.key_4")}}</td>
                    </tr>
                    <tr>
                        <td>{!!(isset($customData['SAS_001_63']) ? nl2br($customData['SAS_001_63']) :'')!!}</td>
                    </tr>
                    <tr>
                        <td class="bold">
                            Ҷамъ:{{$totalQuantity." "}}{{trans("swis.pdf.{$template}.key_1")." "}}{{trans("swis.pdf.{$template}.key_5")." "}}{{$totalWeight}}{{" ".trans("swis.pdf.{$template}.key_2")}}</td>
                    </tr>
                @endif
            </table>
        </td>
        <td style="text-align: right;width: 25%;">
            <br/>
            <div style="font-size: 12px;width: 100px;float:right;"><span
                        style="font-size: 12px;text-align:center;">{{$fieldID_18}}</span><br>
                <span style="font-size:10px">---------------------------------------</span><br/>
                <span style="font-size: 9px;font-weight:bold;">рамзи НМ ФИХ / код ТВ ВЭД</span>
            </div>
        </td>
    </tr>
</table>
<br>
<table>
    <tr>
        <td style="font-size: 12px;text-align: left;font-weight:bold;width:40%;">Ба талаботи ҳуҷҷатҳои меъёрии<br/>зерин
            мутобиқат мекунад<br>Соответствует требованиям<br/>нормативных
            документов
        </td>
        <td style="font-size: 12px;width:60%;"><br><br><br>{{$fieldID_23}}
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align: left;font-weight:bold;">
        </td>
        <td></td>
    </tr>
</table>
<table>
    <tr>
        <td style="font-size: 12px;text-align: left;font-weight:bold;width:40%;">Истеҳсол шудааст<br>Изготовлено
        </td>
        <td style="font-size: 12px;width:60%;">{!!  $productProducerCountries!!}</td>
    </tr>
    <tr>
        <td style="font-size: 12px;text-align: left;font-weight:bold;">
        </td>
    </tr>
</table>
<br/>
<table>
    <tr>
        <td style="font-size: 12px;text-align: left;font-weight:bold;width:40%;">Сертификат дода шуд<br><span
                    style="font-size: 11px;font-weight:bold;">Сертификат выдан</span></td>
        <td style="font-size: 12px;width:60%;">{{$safSideName}}<br>{{$safSideInfo}}
        </td>
    </tr>
    <tr>
        <td style="font-size: 12px;text-align: left;width:60%;">
        </td>
    </tr>
</table>
<br/>
<table>
    <tr>
        <td style="font-size: 12px;text-align: left;font-weight:bold;width:40%;">Дар асосӣ<br><span
                    style="font-size: 11px;font-weight:bold;">На основании</span></td>
        <td style="font-size: 12px;width:60%;">{{$fieldID_24}}</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
</table>
<br/>
<table>
    <tr>
        <td style="font-size: 12px;text-align: left;font-weight:bold;width:40%;">Маълумоти иловагӣ<br><span
                    style="font-size: 11px;font-weight:bold;">Допалнительная информация</span>
        </td>
        <td style="font-size: 12px;text-align: left;width:60%;height:85px;">{{$fieldID_25}}
        </td>
    </tr>
</table>
<br/>
<br/>
<br/>
<table>
    <tr>
        <td style="font-size: 20px;font-weight: bold;text-align: center;width: 15%;"><br/><br/>Ҷ.М.<br/>М.П.
        </td>
        <td style="text-align: left;width: 85%;">
            <table>
                <tr>
                    <td style="font-size: 13px;text-align: left;width: 190px;">
                        <table>
                            <tr>
                                <td style="font-size: 12px;text-align: left;font-weight:bold;">Роҳбари мақомот</td>
                            </tr>
                            <tr>
                                <td style="font-size: 11px;text-align: left;font-weight:bold;">Руководитель органа</td>
                            </tr>
                        </table>
                    </td>
                    <td style="font-size: 10px;text-align: center;width: 118px;"><span
                                style="color:white;font-size: 12px;text-align: center;">emptyText</span>
                        -----------------------------<br/>
                        <span style="font-size: 9px;font-weight:bold;">имзо / подпись</span>
                    </td>
                    <td style="font-size: 10px;text-align: right;width: 200px;
					"><span style="font-size: 12px;text-align: center;">{{$fieldID_26}}</span>
                        -------------------------------------------------<br/>
                        <span style="font-size: 9px;font-weight:bold;">ному насаб / инициалы, фамилия</span>
                    </td>
                </tr>
            </table>
            <br/>
            <br/>
            <table>
                <tr>
                    <td style="font-size: 13px;text-align: left;width: 200px;">
                        <table>
                            <tr>
                                <td style="font-size: 12px;text-align: left;font-weight:bold;">Сардори Раёсат (шӯъба)
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 11px;text-align: left;font-weight:bold;">Начальник управления
                                    (отдела)
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="
						font-size: 10px;
						text-align: center;
						width: 118px;
					"><span style="color:white;font-size: 12px;text-align: center;">emptyText</span>
                        -----------------------------<br/>
                        <span style="font-size: 9px;font-weight:bold;">имзо / подпись</span>
                    </td>
                    <td style="
						font-size: 10px;
						text-align: right;
						width: 200px;
					"><span style="font-size: 12px;text-align: center">{{$fieldID_52}}</span>
                        -------------------------------------------------<br/>
                        <span style="font-size: 9px;font-weight:bold;">ному насаб / инициалы, фамилия</span>
                    </td>
                </tr>
            </table>

        </td>
    </tr>
</table>
<style>
    .title {
        font-weight: bold;
        text-align: center;
    }

    .bold {
        font-weight: bold;
        text-align: left;
        font-size: 10px;
    }

    .text {
        font-size: 10px;
    }

    .fs13 {
        font-size: 13px;
    }


    .fb {
        font-weight: bold;
    }

</style>