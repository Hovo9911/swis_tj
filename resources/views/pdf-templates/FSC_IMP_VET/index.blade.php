<?php

use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationProductsBatch\SingleApplicationProductsBatch;
use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm;

$gerbImagePath = '/image/gerb_' . env('COUNTRY_MODE') . '_sm_2.png';

$safGeneralRegime = $saf->data['saf']['general']['regime'] ?? '';
$safTransportationExportCountry = $saf->data['saf']['transportation']['export_country'] ?? '';
$transitCountries = $saf->data['saf']['transportation']['transit_country'] ?? '';
$transportType = $saf->data['saf']['transportation']['type_of_transport'] ?? '';
$transportAppointment = $saf->data['saf']['transportation']['appointment'] ?? '';

$transCountriesList = $productsSection = '';

$transCountries = [];

if (!empty($safTransportationExportCountry)) {
    $safTransportationExportCountry = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safTransportationExportCountry))->name;
}

if ($transportType) {
    $transportType = optional(getReferenceRows(ReferenceTable::REFERENCE_TRANSPORT_TYPE, $transportType))->name;
}

if ($transportAppointment) {
    $transportAppointment = optional(getReferenceRows(ReferenceTable::REFERENCE_CUSTOM_BODY_REFERENCE, $transportAppointment))->name;
}

foreach ($transitCountries as $transitCountry) {
    if (!is_null($transitCountry)) {
        $transCountries[] = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $transitCountry))->name;
    }
}
if (!empty($transCountries)) {
    $transCountries = array_unique($transCountries);
    $transCountries = array_reverse($transCountries);
    $transCountriesList = implode(', ', $transCountries);
    $transCountriesList1 = trans("swis.pdf.{$template}.new_key_1") . " " . $transCountriesList;
}

switch ($safGeneralRegime) {
    case SingleApplication::REGIME_IMPORT:
        $safSideName = $saf->data['saf']['sides']['import']['name'] ?? '';
        $safTransportationCountry = $saf->data['saf']['transportation']['export_country'] ?? '';
        $fixText1 = 'Главгосветинспекторам<br>' . $safTransportationExportCountry . " " . ($transCountriesList1 ?? '');
        $fixText2 = "Управление Комитета<br>на государственной границе и<br>транспорте<br>" . $safSideName;
        $fixText4 = trans('swis.pdf_templates.FSC_IMP_VET.import_fix_text_1');
        $fixText5 = trans('swis.pdf_templates.FSC_IMP_VET.import_fix_text_2');
        break;
    case SingleApplication::REGIME_EXPORT:
        $safSideName = $saf->data['saf']['sides']['export']['name'] ?? '';
        $safTransportationCountry = $saf->data['saf']['transportation']['import_country'] ?? '';
        $fixText2 = "Управление Комитета<br>на государственной границе и<br>транспорте<br>" . $safSideName;
        $fixText4 = trans('swis.pdf_templates.FSC_IMP_VET.export_fix_text_1');
        $fixText5 = trans('swis.pdf_templates.FSC_IMP_VET.export_fix_text_2');
        break;
    default:
        $safSideName = $saf->data['saf']['sides']['import']['name'] ?? '';
        $safTransportationExportCountry = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $saf->data['saf']['transportation']['export_country']))->name;
        $safTransportationImportCountry = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $saf->data['saf']['transportation']['import_country']))->name;
        $safTransportationCountry = $safTransportationExportCountry . '/' . $safTransportationImportCountry;
        $fixText2 = "Управление Комитета<br>на государственной границе и<br>транспорте<br>" . $safSideName;
        $fixText4 = trans('swis.pdf_templates.FSC_IMP_VET.transit_fix_text_1');
        $fixText5 = trans('swis.pdf_templates.FSC_IMP_VET.transit_fix_text_2');
        break;
}

$safTransportationCountry = !empty($safTransportationCountry) ? optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safTransportationCountry))->name : '';

$number = 1;

foreach ($subAppProducts as $key => $subAppProduct) {

    $totalBatchesQuantity = 0;
    $productBatchsQuantity = SingleApplicationProductsBatch::where('saf_product_id', $subAppProduct['id'])->pluck('approved_quantity')->all();

    foreach ($productBatchsQuantity as $productBatchQuantity) {
        $totalBatchesQuantity += $productBatchQuantity;
    }

    $measurementUnit = optional( getReferenceRows(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $subAppProduct['measurement_unit'],false,['short_name']))->short_name;
    $measurementUnitText = (!is_null($measurementUnit)) ? $measurementUnit : '';

    $productCountry = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $subAppProduct['producer_country']))->name;

    $productsSection .= '<tr nobr="true" style="padding-bottom:20px">
        <td style="line-height: 5px;text-align: center;width: 5%;">' . $number++ . '</td>
        <td style="text-align: left;width: 20%;">' . $subAppProduct['commercial_description'] . '</td>
        <td style="text-align: center;width: 15%;">' . $totalBatchesQuantity . '</td>
        <td style="text-align: center;width: 15%;">' . $measurementUnitText . '</td>
        <td style="text-align: center;width: 20%;">' . $productCountry . '</td>
        <td style="text-align: center;width: 25%;">' . $subAppProduct['producer_name'] . '</td>
    </tr>';

}

if (!is_null($customData)) {

    $mdmPermissionDateFieldId = DocumentsDatasetFromMdm::where(['document_id' => $documentId, 'mdm_field_id' => 355])->pluck('field_id')->first();
    $mdmExpirationDateFieldId = DocumentsDatasetFromMdm::where(['document_id' => $documentId, 'mdm_field_id' => 357])->pluck('field_id')->first();

    $subApplicationPermissionDate = (!is_null($mdmPermissionDateFieldId) && array_key_exists($mdmPermissionDateFieldId, $customData)) ? $customData[$mdmPermissionDateFieldId] : '';
    $subApplicationExpirationDate = (!is_null($mdmExpirationDateFieldId) && array_key_exists($mdmExpirationDateFieldId, $customData)) ? $customData[$mdmExpirationDateFieldId] : '';

    if (!empty($subApplicationPermissionDate) && !empty($subApplicationExpirationDate)) {

        $interval = date_diff(date_create($subApplicationPermissionDate), date_create($subApplicationExpirationDate));
        $daysDiff = $interval->format('%a');
    }
}
?>
<table cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td style="width:40%">
            <table>
                <tr>
                    <td style=""></td>
                </tr>
            </table>
        </td>
        <td style="width:20%;">
            <img src="<?php echo e($gerbImagePath); ?>"/>
        </td>
        <td style="width:40%"></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="width:5%">
        </td>
        <td style="width:90%;font-weight:bold;text-align:center;">
            {{ trans("swis.pdf.{$template}.header_text_1")}}<br> {{ trans("swis.pdf.{$template}.header_text_2")}}
        </td>
        <td style="width:5%"></td>
    </tr>
    <tr>
        <td style="border-bottom: 2px solid #000000;text-align:center;width:100%;"></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="width:20%"></td>
        <td style="width:40%;text-align:right;"><span style="color:white">{!! $fixText1 ?? '' !!}</span><br>Копия։&nbsp;&nbsp;
        </td>
        <td style="width:40%;text-align:left;">{!! $fixText1 ?? '' !!}<br>{!! $fixText2 ?? '' !!}</td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="width:100%;">
            {{$fixText4 ?? ''}}
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="width:5%"></td>
        <td style="width:100%;">
            {{$fixText5 ?? ''}}
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="width:5%"></td>
        <td style="width:100%;">
            Срок действия данного разрешения {{$daysDiff ?? ''}} дней.
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="width:100%">
            <table cellpadding="1" border="1">
                <tr>
                    <td style="font-weight: bold;text-align: left;"> Компания</td>
                    <td style="text-align: center;">{{$safSideName}}</td>
                </tr>
                <tr>
                    <td style="font-weight:bold; text-align: left;"> Тип транспортного
                        средства
                    </td>
                    <td style=" text-align: center;">{{$transportType}}</td>
                </tr>
                <tr>
                    <td style="font-weight: bold;text-align: left;"> Маршрут</td>
                    <td style="text-align: center;">{{$transCountriesList}}</td>
                </tr>
                <tr>
                    <td style="font-weight: bold;text-align: left;"> Страна отправления
                    </td>
                    <td style="text-align: center;">{{$safTransportationCountry}}</td>
                </tr>
                <tr>
                    <td style="font-weight: bold;text-align: left;"> ТО назначения
                    </td>
                    <td style="text-align: center;">{{$transportAppointment}}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="width:100%">
            <table cellpadding="5" border="1">
                <tr nobr="true">
                    <td style="text-align: center;width: 5%;font-weight:bold;">№</td>
                    <td style="text-align: left;width: 20%;font-weight:bold;">Товар(ы)</td>
                    <td style="text-align: center;width: 15%;font-weight:bold;">Количество</td>
                    <td style="text-align: center;width: 15%;font-weight:bold;">Ед. Изм.</td>
                    <td style="text-align: center;width: 20%;font-weight:bold;">Страна происхождения
                    </td>
                    <td style="text-align: center;width: 25%;font-weight:bold;">Производитель</td>
                </tr>
                {!! $productsSection !!}
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="Width:5%"></td>
        <td style="width:45%;text-align:left;">Председатель</td>
        <td style="width:45%;text-align:right;">
            {{$customData['6012_11'] ?? ''}}
        </td>
    </tr>
</table>
<style>
    table > tr > td {
        font-size:14px;
    }
</style>