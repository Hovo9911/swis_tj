<?php

use App\Models\ReferenceTable\ReferenceTable;use App\Models\SingleApplication\SingleApplication;use App\Models\SingleApplicationObligations\SingleApplicationObligations;use App\Models\User\User;

$gerbImagePath = '/image/gerb_' . env('COUNTRY_MODE') . '_sm.png';
$gerbImagePath1 = '/image/gerb_' . env('COUNTRY_MODE') . '_sm1.png';

$obligationsSection = '';
$saf = SingleApplication::select('data')->where('regular_number', $obligation->saf_number)->first();

switch ($obligation->regime) {
    case SingleApplication::REGIME_IMPORT:
        $obligationImporterExporterValue = $obligation->importer_value;
        $safSideName = $saf->data['saf']['sides']['import']['name'] ?? '';
        $safSideAddress = $saf->data['saf']['sides']['import']['address'] ?? '';
        $safSideCommunity = $saf->data['saf']['sides']['import']['community'] ?? '';
        $safSideCountry = $saf->data['saf']['sides']['import']['country'] ?? '';
        break;
    case SingleApplication::REGIME_EXPORT:
        $obligationImporterExporterValue = $obligation->exporter_value;
        $safSideName = $saf->data['saf']['sides']['export']['name'] ?? '';
        $safSideAddress = $saf->data['saf']['sides']['export']['address'] ?? '';
        $safSideCommunity = $saf->data['saf']['sides']['export']['community'] ?? '';
        $safSideCountry = $saf->data['saf']['sides']['export']['country'] ?? '';
        break;
    default:
        $obligationImporterExporterValue = $obligation->importer_value;
        $safSideName = $saf->data['saf']['sides']['import']['name'] ?? '';
        $safSideAddress = $saf->data['saf']['sides']['import']['address'] ?? '';
        $safSideCommunity = $saf->data['saf']['sides']['import']['community'] ?? '';
        $safSideCountry = $saf->data['saf']['sides']['import']['country'] ?? '';
        break;
}

$safSideCountry = !empty($safSideCountry) ? optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safSideCountry))->name : '';

$applicantName = $saf->data['saf']['sides']['applicant']['name'] ?? '';
$applicantEmail = $saf->data['saf']['sides']['applicant']['email'] ?? '';
$applicantPhoneNumber = $saf->data['saf']['sides']['applicant']['phone_number'] ?? '';
$applicantAddress = $saf->data['saf']['sides']['applicant']['address'] ?? '';

if (!is_null($obligation->payer_user_type)) {
    if ($obligation->payer_user_type == SingleApplicationObligations::SAF_OBLIGATIONS_TYPE_NATURAL_PERSON) {
        $obligation->obligation_payer = "(" . $obligation->payer_company_tax_id . ') ' . User::find($obligation->payer_user_id)->name();
    } elseif ($obligation->payer_user_type == SingleApplicationObligations::SAF_OBLIGATIONS_TYPE_AGENCY || $obligation->payer_user_type == SingleApplicationObligations::SAF_OBLIGATIONS_TYPE_BROKER) {
        $obligation->obligation_payer = "(" . $obligation->payer_company_tax_id . ') ' . optional(\App\Models\Company\Company::getCompanyByTaxID($obligation->payer_company_tax_id))->name;
    } else {
        $obligation->obligation_payer = "(" . $obligation->payer_company_tax_id . ') ' . trans("swis.saf_obligations.payer.{$obligation->payer_user_type}");
    }
}

$user = User::find($obligation->payer_user_id);

?>
<table width="100%">
    <tr nobr="true">
        <td style="width: 20%;">
            <img src="{{$gerbImagePath}}"/>
        </td>
        <td style="text-align: center;width: 60%;">
            <table width="100%">
                <tr>
                    <td style="font-size: 10px;line-height: 5;">
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px;">
                        {{trans('swis.obligation.print_form.obligation_info')}}
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>
            </table>
        </td>
        <td style="text-align: right;width: 20%;">
        </td>
    </tr>
</table>
<br>
<table width="100%" border="1" cellpadding="1">
    <tr nobr="true">
        <td rowspan="4" style="background-color: #D9D9D9;width: 40%;">
            <table>
                <tr>
                    <td class="grey_background fb" style="width:40%;">
                        1. {{trans('swis.obligation.print_form.obligation_payer')}}
                    </td>
                    <td class="grey_background fb center" style="width:40%;">
                        {{trans('swis.obligation.print_form.ssn_pasport')}}
                    </td>
                    <td class="grey_background center" style="width:20%;">{{$obligationImporterExporterValue}}</td>
                </tr>
            </table>
            <br><br>
            <table width="100%">
                <tr>
                    <td style="font-size: 11px;line-height: 20px;">{{$safSideName}}</td>
                </tr>
                <tr>
                    <td style="font-size: 10px;line-height: 20px;">{{$safSideCountry}}, {{$safSideCommunity}}
                        , {{$safSideAddress}}</td>
                </tr>
            </table>
        </td>
        <td style="width:40%;background-color: #D9D9D9;text-align:center;">
            <span style="font-size: 10px;font-weight: bold;">3. {{trans('swis.obligation.print_form.agency')}}</span>
        </td>
        <td style="background-color: #D9D9D9;font-size: 10px;font-weight: bold;line-height: 20px;width: 20%;">
            5. {{ trans('swis.obligation.print_form.saf_number')}}
        </td>
    </tr>
    <tr>
        <td rowspan="3" style="font-size: 11px;line-height: 20px;text-align:center">
            <span style="line-height: 20px"><br>{{$obligation->agency}}</span>
        </td>
        <td style="background-color: #FFFFFF;font-size: 10px;line-height: 20px;width: 20%;text-align:center;">{{$obligation->saf_number}}
        </td>
    </tr>
    <tr>
        <td style="background-color: #D9D9D9;font-size: 10px;font-weight: bold;line-height: 20px;width: 20%;">
            6. {{ trans('swis.obligation.print_form.sub_application_number')}}
        </td>
    </tr>
    <tr>
        <td style="background-color: #FFFFFF;font-size: 10px;line-height: 20px;width: 20%;text-align:center">{{$obligation->sub_application_number}}
        </td>
    </tr>
    <tr nobr="true">
        <td rowspan="4" style="background-color: #D9D9D9;width: 40%;">
            <table>
                <tr>
                    <td class="grey_background fb" style="line-height: 20px;width:40%">
                        2. {{trans('swis.obligation.print_form.applicant')}}
                    </td>
                    <td class="grey_background fb center" style="line-height: 20px;width:40%">
                        {{trans('swis.obligation.print_form.applicant_ssn_pasport')}}
                    </td>
                    <td class="grey_background center" style="line-height: 20px;width:20%">{{$obligation->applicant_tin}}</td>
                </tr>
            </table>
            <br><br>
            <table width="100%">
                <tr>
                    <td style="font-size: 11px;line-height: 1.5;">{{$applicantName}}</td>
                </tr>
                <tr>
                    <td style="font-size: 10px;line-height: 1.5;">{{$applicantAddress}}</td>
                </tr>
                <tr>
                    <td style="font-size: 10px;line-height: 1.5;">{{$applicantPhoneNumber}}</td>
                </tr>
                <tr>
                    <td style="font-size: 10px;
						line-height: 1.5;
					">{{$applicantEmail}}</td>
                </tr>
            </table>
        </td>
        <td style="width:40%;background-color: #D9D9D9;text-align:center;">
            <span style="font-size: 10px;font-weight: bold;"> 4. <?php echo trans('swis.obligation.print_form.permitted_document')?></span>
        </td>
        <td style="background-color: #D9D9D9;font-size: 10px;font-weight: bold;line-height: 20px;width: 20%;">
            7. <?php echo trans('swis.obligation.print_form.obligation_number')?>
        </td>
    </tr>
    <tr>
        <td rowspan="3" style="background-color: #FFFFFF;font-size: 10px;line-height: 20px;text-align:center;">
            <br><br>{{$obligation->document}}
        </td>
        <td style="background-color: #FFFFFF;font-size: 10px;line-height: 20px;width: 20%;text-align:center;">{{$obligation->id}}
        </td>
    </tr>
    <tr>
        <td style="background-color: #D9D9D9;font-size: 10px;font-weight: bold;line-height: 20px;width: 20%;">
            8.<?php echo trans('swis.obligation.print_form.obligation_creation_date')?>
        </td>
    </tr>
    <tr>
        <td style="background-color: #FFFFFF;font-size: 10px;line-height: 20px;width: 20%;text-align:center;">{{formattedDate($obligation->obligation_created_date,true)}}
        </td>
    </tr>
</table>
<table width="100%" border="1" cellpadding="5">
    <tr>
        <td colspan="9" style="background-color: #D9D9D9;font-size: 10px;font-weight: bold;">
            9. <?php echo trans('swis.pdf_templates.saf.obligation_title')?>
        </td>
    </tr>
    <tr nobr="true">
        <td style="
			background-color: #D9D9D9;
			font-size: 10px;
			font-weight: bold;
			text-align: center;
			width: 14.28%;
		"><?php echo trans('swis.pdf_templates.saf.obligation_type')?>
        </td>
        <td style="
			background-color: #D9D9D9;
			font-size: 10px;
			font-weight: bold;
			text-align: center;
			width: 14.28%;
		"><?php echo trans('swis.obligation.print_form.payer_bank_account')?>
        </td>
        <td style="
			background-color: #D9D9D9;
			font-size: 10px;
			font-weight: bold;
			text-align: center;
			width: 14.28%;
		"><?php echo trans('swis.pdf_templates.saf.obligation_in_somoni')?>
        </td>
        <td style="
			background-color: #D9D9D9;
			font-size: 10px;
			font-weight: bold;
			text-align: center;
			width: 14.28%;
		"><?php echo trans('swis.pdf_templates.saf.obligation_payment_in_somoni')?>
        </td>
        <td style="
			background-color: #D9D9D9;
			font-size: 10px;
			font-weight: bold;
			text-align: center;
			width: 14.28%;
		"><?php echo trans('swis.pdf_templates.saf.obligation_balance_in_somoni')?>
        </td>
        <td style="
			background-color: #D9D9D9;
			font-size: 10px;
			font-weight: bold;
			text-align: center;
			width: 14.28%;
		"><?php echo trans('swis.pdf_templates.saf.obligation_payment_date')?>
        </td>
        <td style="
			background-color: #D9D9D9;
			font-size: 10px;
			font-weight: bold;
			text-align: center;
			width: 14.28%;
		"><?php echo trans('swis.obligation.print_form.payment_confirming_organization')?>
        </td>
    </tr>
    <tr nobr="true">
        <td style="
			background-color: #ffffff;
			font-size: 10px;
			text-align: center;
			width: 14.28%;
		">{{$obligation->obligation_type}}
        </td>
        <td style="
			background-color: #ffffff;
			font-size: 10px;
			text-align: center;
			width: 14.28%;
		">{{$obligation->budget_line}}
        </td>
        <td style="
			background-color: #ffffff;
			font-size: 10px;
			text-align: center;
			width: 14.28%;
		">{{$obligation->obligation}}
        </td>
        <td style="
			background-color: #ffffff;
			font-size: 10px;
			text-align: center;
			width: 14.28%;
		">{{$obligation->payment}}
        </td>
        <td style="
			background-color: #ffffff;
			font-size: 10px;
			text-align: center;
			width: 14.28%;
		">{{$obligation->balance}}
        </td>
        <td style="
			background-color: #ffffff;
			font-size: 10px;
			text-align: center;
			width: 14.28%;
		">{{$obligation->obligation_pay_date}}
        </td>
        <td style="
			background-color: #ffffff;
			font-size: 10px;
			text-align: center;
			width: 14.28%;
		">{{$obligation->obligation_payer}}
        </td>
    </tr>
</table>
<br/>
<br/>

<table width="100%" nobr="true">
    <tr nobr="true">
        <td style="width: 33.3333%;">
            <table width="100%" border="1">
                <tr>
                    <td style="background-color: #D9D9D9;height:100px">
                        <table width="100%" cellpadding="5">
                            <tr>
                                <td style="font-size: 10px;text-align: left;width: 40%;
								">
                                    <b>10. <?php echo trans('swis.obligation.print_form.executor')?></b>
                                </td>
                                <td style="
									font-size: 10px;
									font-weight: bold;
									text-align: center;
									width: 35%;
								"><?php echo trans('swis.pdf_templates.saf.applicant_passport')?>
                                </td>
                                <td style="
									font-size: 10px;
									text-align: center;
									width: 25%;
								">{{$user->ssn or ''}}
                                </td>
                            </tr>
                            <tr>
                                <td style="
                                        font-size: 10px;
                                        text-align: left;
                                        width: 100%;">{{$user->first_name or ''}} {{$user->last_name or ''}}<br/>
                                    {{$user->registration_address or ''}}<br/>
                                    {{$user->phone_number or ''}}<br/>
                                    {{$user->email or ''}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #FFFFFF;font-size: 10px;text-align: left;height:48px">
                        <br/>
                        <br/>
                        <b><?php echo "  " . trans('swis.pdf_templates.saf.agency_signature')?></b>&nbsp;&nbsp;&nbsp;_ _
                        _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ <br/>
                    </td>
                </tr>
            </table>
        </td>
        <td style="width: 33.3333%;">
            <table width="100%" border="1">
                <tr>
                    <td>
                        <table width="100%" cellpadding="5">
                            <tr>
                                <td style="width: 25%;">
                                    <img src="<?php echo $gerbImagePath1;?>" width="60"/>
                                </td>
                                <td style="font-size: 10px;width: 75%;">
                                    <br/>
                                    <br/>
                                    <?php echo trans('swis.pdf_templates.saf.electronic_document_description')?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table width="100%">
                                        <tr>
                                            <td style="background-color: #000000;font-size: 10px;height: 15px;">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table>
                                        <tr>
                                            <td style="font-size: 10px;text-align: right;width: 110px;">
                                                <?php echo trans('swis.pdf_templates.saf.electronic_document_printed_by')?>
                                            </td>
                                            <td style="font-size: 10px;">&nbsp;&nbsp;
                                                {{optional(Auth::user())->name()}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px;text-align: right;width: 110px;">
                                                <?php echo trans('swis.pdf_templates.saf.electronic_document_when_printed')?>
                                            </td>
                                            <td style="font-size: 10px;">&nbsp;&nbsp;
                                                <?php echo currentDateFront(); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="
															font-size: 10px;
															
															text-align: right;
															width: 110px;
														"><?php echo trans('swis.pdf_templates.saf.electronic_document_ep_number')?>
                                            </td>
                                            <td style="
															font-size: 10px;
															
														">&nbsp;&nbsp;000000000000000
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
        <td style="width: 33.3333%;">
            <table width="100%" border="1">
                <tr>
                    <td style="background-color: #D9D9D9;">
                        <table width="100%" cellpadding="5">
                            <tr>
                                <td style="font-size: 10px;text-align: left;width: 100%;height:100px">
                                    <b>11.</b>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #FFFFFF;font-size: 10px;text-align: left;height:48px;">
                        <br/>
                        <br/>
                        <b><?php echo "  " . trans('swis.pdf_templates.saf.agency_signature')?></b>&nbsp;&nbsp;&nbsp;_ _
                        _ _ _
                        _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ <br/>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<style>
    .grey_background {
        background-color: #D9D9D9;
        font-size: 10px;
    }

    .tc {
        text-align: center
    }

    .fb {
        font-weight: bold;
    }
</style>