<?php

use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplicationProductsBatch\SingleApplicationProductsBatch;

//----- Get fields values by MDM_ID
$mdmFields = getFieldsValueByMDMID($documentId, $customData);
list($mainDataBySafId, $productsDataBySafId, $productBatchesDataBySafId) = getSafDataFieldIdValue($saf, $subApplication, $subAppProducts, $availableProductsIds);

//----- 1.
$subdivisionId = optional($subApplication)->agency_subdivision_id;
if (!is_null($subdivisionId)) {
    $subDivision = optional(getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME, $subdivisionId))->name;
}
$point1 = getMappingValueInMultipleRows($subDivision ?? '', 55, 100);

//----- 2. ,3. ,6.
$productCountries = $productCommercialDescription = [];
$point3 = $point6 = 0;
$productsTable = '';
foreach ($subAppProducts as $subAppProduct) {

    $productCountries[] = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $subAppProduct['producer_country']))->name;
    $productBatches = SingleApplicationProductsBatch::select('netto_weight')->where('saf_product_id', $subAppProduct->id)->get();

    foreach ($productBatches as $productBatch) {

        $point6 += (int)optional($productBatch)->netto_weight;
    }
    $productCommercialDescription[] = notEmptyForPdfFields($productsDataBySafId[$subAppProduct->id], 'SAF_ID_91');
    $point3 += notEmptyForPdfFields($productsDataBySafId[$subAppProduct->id], 'SAF_ID_96');
}

//----- 4.
$point4 = isset($subAppProducts[0]) ? optional(getReferenceRows(ReferenceTable::REFERENCE_PACKAGING_REFERENCE, $subAppProducts[0]['places_type']))->name : '';

$productCommercialDescription = array_unique($productCommercialDescription);
$productCommercialDescriptionList = implode(', ', $productCommercialDescription);

$point2 = getMappingValueInMultipleRows($productCommercialDescriptionList, 55, 90);

//----- 5.
$point5 = $mdmFields[95] ?? '';

//----- 7.
$safID14 = (isset($mainDataBySafId['SAF_ID_14']) && !empty($mainDataBySafId['SAF_ID_14'])) ? $mainDataBySafId['SAF_ID_14'] . '-' : '';
$safID16 = (isset($mainDataBySafId['SAF_ID_16']) && !empty($mainDataBySafId['SAF_ID_16'])) ? $mainDataBySafId['SAF_ID_16'] . ', ' : '';
$safID17 = (isset($mainDataBySafId['SAF_ID_17']) && !empty($mainDataBySafId['SAF_ID_17'])) ? $mainDataBySafId['SAF_ID_17'] : '';

$point7 = $safID14 . $safID16 . $safID17;
$point7 = getMappingValueInMultipleRows($point7, 70, 100);

//----- 8.
$point8 = isset($subAppProducts[0]) ? $subAppProducts[0]['producer_name'] : '';

//----- 10.
$transitCountries = $saf->data['saf']['transportation']['transit_country'] ?? '';

foreach ($transitCountries as $transitCountry) {
    if (!is_null($transitCountry)) {
        $transCountries[] = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $transitCountry))->name;
    }
}
if (!empty($transCountries)) {
    $transCountries = array_unique($transCountries);
    $transCountries = array_reverse($transCountries);
    $transCountriesList = implode(', ', $transCountries);
}

$point10 = getMappingValueInMultipleRows($transCountriesList ?? '', 40, 85);

//----- 11.
$transportationDeparture = $saf->data['saf']['transportation']['departure'];
$transportInfo = getReferenceRows(ReferenceTable::REFERENCE_CUSTOM_BODY_REFERENCE, $transportationDeparture, false, ['code', 'name', 'address']);//SAF_ID_72
if (!is_null($transportInfo)) {
    $point11 = '(' . optional($transportInfo)->code . ') ' . optional($transportInfo)->name . ', ' . optional($transportInfo)->address;
}
$point11 = getMappingValueInMultipleRows($point11 ?? '', 40, 85);

//----- 12.
$safID6 = (isset($mainDataBySafId['SAF_ID_6']) && !empty($mainDataBySafId['SAF_ID_6'])) ? $mainDataBySafId['SAF_ID_6'] . ', ' : '';
$safID8 = (isset($mainDataBySafId['SAF_ID_8']) && !empty($mainDataBySafId['SAF_ID_8'])) ? $mainDataBySafId['SAF_ID_8'] . ', ' : '';
$safID9 = (isset($mainDataBySafId['SAF_ID_9']) && !empty($mainDataBySafId['SAF_ID_9'])) ? $mainDataBySafId['SAF_ID_9'] : '';

$point12 = $safID6 . $safID8 . $safID9;
$point12 = getMappingValueInMultipleRows($point12, 60, 90);

//----- 13.
$safID75 = (isset($mainDataBySafId['SAF_ID_75']) && !empty($mainDataBySafId['SAF_ID_75'])) ? $mainDataBySafId['SAF_ID_75'] . ', ' : '';

$stateNumberCustomsSupport = $subApplication->data['saf']['transportation']['state_number_customs_support'] ?? [];
$stateNumber = $customsSupport = $trackContainerTemperature = [];

foreach ($stateNumberCustomsSupport as $value) {
    if (isset($value['state_number'])) {
        $stateNumber[] = $value['state_number'];
    }
    if (isset($value['customs_support'])) {
        $customsSupport[] = $value['customs_support'];
    }
    if (isset($value['track_container_temperature'])) {
        $trackContainerTemperature[] = $value['track_container_temperature'];
    }
}

$safID76 = count($stateNumber) ? implode(', ', $stateNumber) . '; ' : '';
$safID77 = count($customsSupport) ? implode(', ', $customsSupport) . '; ' : '';
$safID135 = count($trackContainerTemperature) ? implode(', ', $trackContainerTemperature) : '';

$point13 = $safID75 . $safID76 . $safID77 . $safID135;
$point13 = getMappingValueInMultipleRows($point13, 70, 85);


//---- 15. ,16. ,17.
if (isset($mdmFields['469']) && !empty($mdmFields['469'])) {
    $dateFormat = strtr($mdmFields['469'], '/', '-');
    $time = strtotime($dateFormat);
    $point15 = date('d', $time);
    $point16 = month(date('m', $time));
    $point17 = date('y', $time);
}

$safTransportationCountry = $saf->data['saf']['transportation']['import_country'] ?? '';

if (!empty($safTransportationCountry)) {
    $safTransportationCountry = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safTransportationCountry))->name;
}

if (!is_null($customData)) {

    $permissionDateDatasetMdm = DocumentsDatasetFromMdm::select('field_id')->where(['document_id' => $documentId, 'mdm_field_id' => 355])->first();
    $mdmPermissionDateFieldId = !is_null($permissionDateDatasetMdm) ? $permissionDateDatasetMdm->field_id : null;
    $subApplicationPermissionDate = (!is_null($mdmPermissionDateFieldId) && array_key_exists($mdmPermissionDateFieldId, $customData)) ? $customData[$mdmPermissionDateFieldId] : '';
    
    if (!empty($subApplicationPermissionDate)) {
        $permissionDateDay = date('d', strtotime($subApplicationPermissionDate));
        $permissionDateMonth = month(date('m', strtotime($subApplicationPermissionDate)));
        $permissionDateYear = date('y', strtotime($subApplicationPermissionDate));
    }
}
?>
<table cellspacing="0" cellpadding="1" border="0">
    <tr>
        <td style="height:300px"></td>
    </tr>
</table>
<table cellspacing="0" cellpadding="0">
    <tr>
        <td style="font-size: 11px;font-weight: bold;width:70%">Управление Комитета на госгранице и транспорте</td>
        <td style="line-height:0;"></td>
    </tr>
    <tr style="line-height:1.6;">
        <td style="font-size: 11px;font-weight: bold;width:42%;">Board of Committee at the state border
            and transport
        </td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:58%;">{{$point1[0]}}</td>
    </tr>
    @if(count($point1)>1)
        @for($i=1;$i<count($point1);$i++)
            <tr>
                <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:100%;line-height:1.4">
                    {{$point1[$i]}}
                </td>
            </tr>
        @endfor
    @endif
    <tr style="line-height:1.6;">
        <td style="font-size: 11px;font-weight: bold;width:36%;">Наименование сырье / Name of raw material</td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:64%;">{{$point2[0]}}</td>
    </tr>
    @if(count($point2)>1)
        @for($i=1;$i<count($point2);$i++)
            <tr>
                <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:100%;line-height:1.4">
                    {{$point2[$i]}}
                </td>
            </tr>
        @endfor
    @endif
    <tr style="line-height:1.6;">
        <td style="font-size: 11px;font-weight: bold;width:28%;">Число мест / Number of packages</td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:72%;">{{$point3}}</td>
    </tr>

    <tr style="line-height:1.6;">
        <td style="font-size: 11px;font-weight: bold;width:23%;">Упаковка / Type of package</td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:77%;">{{$point4 ?? ''}}</td>
    </tr>

    <tr style="line-height:1.6;">
        <td style="font-size: 11px;font-weight: bold;width:28%;">Маркировка / Identification marks</td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:72%;">{{$point5 ??''}}</td>
    </tr>
    <tr style="line-height:1.6;">
        <td style="font-size: 11px;font-weight: bold;width:18%;">Все нетто / Net weight</td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:82%;">{{$point6}}</td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight:bold;text-align:center;width:100%;">1. Происхождение сырья / Origin of
            the raw material
        </td>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr style="line-height:0.5;">
        <td style="font-size: 11px;font-weight: bold;width:70%;">Наименование и адрес экспортера</td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight: bold;width:23%">Name and address of exporter
        </td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:77%;">{{$point7[0]}}
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:100%;line-height:1.4">
            {{$point7[1] ?? ''}}
        </td>
    </tr>
    @if(count($point7)>2)
        @for($i=2;$i<count($point7);$i++)
            <tr>
                <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:100%;line-height:1.4">
                    {{$point7[$i]}}
                </td>
            </tr>
        @endfor
    @endif
    <tr>
        <td></td>
    </tr>
    <tr style="line-height: 0.5;">
        <td style="font-size: 11px;font-weight: bold;width:30%">Происхождение сырья</td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight: bold;width:20%">Origin of the raw material
        </td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:80%;">{{$point8 ??''}}</td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.4;text-align:left;width:30%;line-height: 1.4;"></td>
        <td style="font-size: 10px;text-align:center;line-height: 1.4;width:70%">
            <span>(сборное, боенское, другие источники / from slaughter, collected, other sources)</span>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight:bold;text-align:center;width:100%;">2. Направление сырья / Destination of
            the raw material
        </td>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>

    <tr style="line-height:1.6;">
        <td style="font-size: 11px;font-weight: bold;width:35%;">Страна назначения / Country of destination</td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:65%;">{{$safTransportationCountry}}</td>
    </tr>

    <tr style="line-height:1.6;">
        <td style="font-size: 11px;font-weight: bold;width:30%;">Страна транзита / Country of transit</td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:70%;">{{$point10[0]}}</td>
    </tr>
    @if(count($point10)>1)
        @for($i=1;$i<count($point10);$i++)
            <tr>
                <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:100%;line-height:1.4">
                    {{$point10[$i]}}
                </td>
            </tr>
        @endfor
    @endif
    <tr style="line-height:1.6;">
        <td style="font-size: 11px;font-weight: bold;width:47%;">Пункт пересечения границы / Point of crossing the
            border
        </td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:53%;">{{$point11[0]}}</td>
    </tr>
    @if(count($point11)>1)
        @for($i=1;$i<count($point11);$i++)
            <tr>
                <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:100%;line-height:1.4">
                    {{$point11[$i]}}
                </td>
            </tr>
        @endfor
    @endif
    <tr>
        <td style="font-size: 11px;font-weight: bold;width:70%">Наименование и адрес получателя</td>
        <td style="line-height:0;"></td>
    </tr>
    <tr style="line-height:1.6;">
        <td style="font-size: 11px;font-weight: bold;width:25%;">Name and address of consignee
        </td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:75%;">{{$point12[0]}}</td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:100%;line-height:1.4">
            {{$point12[1] ?? ''}}
        </td>
    </tr>
    @if(count($point12)>2)
        @for($i=2;$i<count($point12);$i++)
            <tr>
                <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:100%;line-height:1.4">
                    {{$point12[$i]}}
                </td>
            </tr>
        @endfor
    @endif
    <tr style="line-height:1.6;">
        <td style="font-size: 11px;font-weight: bold;width:70%">Транспорт</td>
        <td style="line-height:0;"></td>
    </tr>
    <tr style="line-height:1.6;">
        <td style="font-size: 11px;font-weight: bold;width:15%;">Means of transport</td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:85%;">{{$point13[0]}}</td>
    </tr>
    <tr style="line-height:1.6;">
        <td style="font-size: 13px;text-align:left;width:25%;"></td>
        <td style="font-size: 10px;text-align:center;width:75%">
            <span>(указать № вагона, автомашины, рейс самолета, судна)</span>
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:100%;line-height:1.4">
            {{$point13[1] ?? ''}}
        </td>
    </tr>
    <tr style="line-height:1.6;">
        <td style="font-size: 10px;text-align:center;width:100%">
            <span>specify the number of the wagon, truck, flight-number, name of the ship)</span>
        </td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight:bold;width:100%;line-height: 1.6;">
            3. Я, нижеподписавшийся Государственный ветеринарный врач Республики Таджикистан, удостоверяю, что
            предъявленное к осмотру указанное сырье:<br>
            I, the undersigned veterinarian of the Government of the Republic Tajikistan certify, that subjected to
            examination raw product:
        </td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight:bold;width:100%;line-height: 1.6;">
            -получено от убоя здоровых животных на предприяриях, имеющих разрешение на экспорт сырья;<br>
            derived from the healthy animals at the premises having permission for export of raw product;
        </td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight:bold;width:100%;line-height: 1.6;">
            -происходит из хозяйств и местности, благополучных по инфекционным болезням списка «A» МЭБ в течении
            последних
            3 месяцев, а также сибирской язве (для пушно-мехового и кожевенного сырья ) и сальмонеллезу (для пуха и
            пера);<br>
            derived from the premises and locaeity, free from infectious diseases, included in the O.I.E. list «A»
            during
            the last 3 months, as well as anthrax (for fur, hides and skins) and salmonellosis (for down and feather);
        </td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight:bold;width:100%;line-height: 1.6;">
            -сборное кожевенное сырье полностью исследовано на сибирскую язву с отрицательным результатом в
            государственной ветеринарной лаборатории, имеющей разрешение на такие исследования.<br>
            Collected hides and skins, raw products were completely tested with negative results for anthrax in the
            State
            Veterinary Labdoratory, licenced for conductiny such tests.
        </td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight:bold;width:100%;line-height: 1.6;">
            По трбованию страны-импортера сырье перед отправкой исследовано на радиоактивное загрязнение в
            государственной ветеринарной лаборатории; уровень радиоактивного загрязнения при исследовании не превышает:
            <u>{{$mdmFields['468'] ??''}}</u> беккерель/кг.<br>
            On the requirement of the importing country the products before shipment were tested for radioactive
            contamination in the State Veterinary Laboratory, the level of contamination did not exceed
            <u>{{$mdmFields['468'] ??''}}</u> bk/kg.
        </td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:340px;font-weight:bold;"></td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:40px;">«{{$point15 ?? ''}}
            »
        </td>
        <td style="font-size: 11px;width:5px;font-weight:bold;"></td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:150px;">{{$point16 ?? ''}}</td>
        <td style="font-size: 11px;width:25px;font-weight:bold;">20{{$point17 ?? ''}}</td>
        <td style="font-size: 11px;width:25px;font-weight:bold;">г.</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;font-weight:bold;width:100%;line-height: 1.6;">
            Транспортные средства очищены и продезинфицированы принятыми в Республике Таджикистан методами и средствами.<br>
            Means of transport have been cleaned and disinfected by the methods and means, approved in the Republic of
            Tajikistan.
        </td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:140px;font-weight:bold;">Составлено / Made on</td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:40px;">
            «{{$permissionDateDay ?? ''}}»
        </td>
        <td style="font-size: 11px;width:5px;font-weight:bold;"></td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:150px;">{{$permissionDateMonth ??''}}</td>
        <td style="font-size: 11px;width:45px;font-weight:bold;">20{{$permissionDateYear ?? ''}}г.</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;;width:30%;font-weight:bold;">Ветеринарный врач / Veterinarian</td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:70%;">{{($customData['Veterinary_30']  ?? '').' '.($customData['Veterinary_9']  ?? '')}}</td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.4;text-align:left;width:35%;line-height: 1.4;"></td>
        <td style="font-size: 10px;text-align:center;line-height: 1.4;width:65%;font-weight:bold;">
            <span>(должность, фамилия / title, name)</span>
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;;width:18%;font-weight:bold;">Подпись / Signature:</td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:82%;"></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;;width:35%;font-weight:bold;">Печать / Stamp</td>
        <td></td>
    </tr>

</table>