<?php

use App\Models\BaseModel;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\RegionalOffice\RegionalOffice;
use App\Models\SingleApplication\SingleApplication;

$fieldIDCCVP_1 = $customData['CCVP_1'] ?? '';
$fieldIDCCVP_6 = $customData['CCVP_6'] ?? '';
$fieldIDCCVP_7 = $customData['CCVP_7'] ?? '';
$fieldIDCCVP_17 = $customData['CCVP_17'] ?? '';
$fieldIDCCVP_16 = $customData['CCVP_16'] ?? '';
$fieldIDCCVP_18 = $customData['CCVP_18'] ?? '';
$fieldIDCCVP_19 = $customData['CCVP_19'] ?? '';
$fieldIDCCVP_20 = $customData['CCVP_20'] ?? '';
$fieldIDCCVP_21 = $customData['CCVP_21'] ?? '';
$fieldIDCCVP_22 = $customData['CCVP_22'] ?? '';
$fieldIDCCVP_23 = $customData['CCVP_23'] ?? '';
$fieldIDCCVP_24 = $customData['CCVP_24'] ?? '';
$fieldIDCCVP_25 = $customData['CCVP_25'] ?? '';
$fieldIDCCVP_26 = $customData['CCVP_26'] ?? '';
$fieldIDCCVP_27 = $customData['CCVP_27'] ?? '';
$fieldIDCCVP_28 = $customData['CCVP_28'] ?? '';

if (!empty($fieldIDCCVP_6)) {
    $fromDay = date('d', strtotime($fieldIDCCVP_6));
    $fromMonth = month(date('m', strtotime($fieldIDCCVP_6)));
    $fromYear = date('y', strtotime($fieldIDCCVP_6));
}
if (!empty($fieldIDCCVP_7)) {
    $toDay = date('d', strtotime($fieldIDCCVP_7));
    $toMonth = month(date('m', strtotime($fieldIDCCVP_7)));
    $toYear = date('y', strtotime($fieldIDCCVP_7));
}

$productProducerInfo = [];

foreach ($subAppProducts as $key => $value) {
    $referenceCountry = getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, optional($value)->producer_country);
    if (!is_null($referenceCountry)) {
        $productProducerCountryId = getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, optional($value)->producer_country)->id;
        $countriesReferenceMlTableName = ReferenceTable::REFERENCE_COUNTRIES . '_ml';
        $productProducerInfo[$key] = \DB::table($countriesReferenceMlTableName)->select('name')->where(['lng_id' => BaseModel::LANGUAGE_CODE_TJ, 'reference_countries_id' => $productProducerCountryId])->first()->name;
    }
    $productProducerInfo[$key] .= ' ' . optional($value)->producer_name;
}
if (!empty($productProducerInfo)) {
    $productProducerInfo = implode(', ', $productProducerInfo);
} else {
    $productProducerInfo = '';
}

$safGeneralRegime = $saf->data['saf']['general']['regime'] ?? '';

switch ($safGeneralRegime) {
    case SingleApplication::REGIME_IMPORT:
        $safSideName = $saf->data['saf']['sides']['import']['name'] ?? '';
        $safSideAddress = $saf->data['saf']['sides']['import']['address'] ?? '';
        $safSideCommunity = $saf->data['saf']['sides']['import']['community'] ?? '';
        break;
    case SingleApplication::REGIME_EXPORT:
        $safSideName = $saf->data['saf']['sides']['export']['name'] ?? '';
        $safSideAddress = $saf->data['saf']['sides']['export']['address'] ?? '';
        $safSideCommunity = $saf->data['saf']['sides']['export']['community'] ?? '';
        break;
    default:
        $safSideName = '';
        $safSideAddress = '';
        $safSideCommunity = '';
        break;
}
$safSideCommunity = ($safSideCommunity == '-') ? '' : $safSideCommunity;
$safSideAddress = ($safSideAddress == '-') ? '' : $safSideAddress;
$safSideInfo = '';

if (!empty($safSideCommunity) AND !empty($safSideAddress)) {
    $safSideInfo = $safSideCommunity . ' , ' . $safSideAddress;
} elseif (!empty($safSideCommunity)) {
    $safSideInfo = $safSideCommunity;
} elseif (!empty($safSideAddress)) {
    $safSideInfo = $safSideAddress;
}

$fieldIDCCVP_1 = mb_strlen($fieldIDCCVP_1, 'UTF-8') > 40 ? mb_substr($fieldIDCCVP_1, 0, 40, 'UTF-8') . "..." : $fieldIDCCVP_1;

$regionalOfficePhoneNumber = '';
$subdivisionId = optional($subApplication)->agency_subdivision_id;
if (!is_null($subdivisionId)) {
    $subDivisionCode = optional(getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME, $subdivisionId, false))->code;

    $regionalOffice = RegionalOffice::select('regional_office_ml.address', 'phone_number', 'regional_office_ml.name')
        ->leftJoin('regional_office_ml', 'regional_office.id', '=', 'regional_office_ml.regional_office_id')
        ->where(['lng_id' => cLng('id'), 'office_code' => $subDivisionCode])
        ->active()
        ->first();

    if (!is_null($regionalOffice)) {
        $regionalOfficePhoneNumber = 'тел. ' . $regionalOffice->phone_number;
    }
}

//$fieldIDCCVP_17 = mb_strlen($fieldIDCCVP_17, 'UTF-8') > 50 ? mb_substr($fieldIDCCVP_17, 0, 50, 'UTF-8') . "..." : $fieldIDCCVP_17;
//$fieldIDCCVP_16 = mb_strlen($fieldIDCCVP_16, 'UTF-8') > 50 ? mb_substr($fieldIDCCVP_16, 0, 50, 'UTF-8') . "..." : $fieldIDCCVP_16;
$fieldIDCCVP_18 = mb_strlen($fieldIDCCVP_18, 'UTF-8') > 40 ? mb_substr($fieldIDCCVP_18, 0, 40, 'UTF-8') . "..." : $fieldIDCCVP_18;
//$fieldIDCCVP_19 = mb_strlen($fieldIDCCVP_19, 'UTF-8') > 20 ? mb_substr($fieldIDCCVP_19, 0, 20, 'UTF-8') . "..." : $fieldIDCCVP_19;
$fieldIDCCVP_20 = mb_strlen($fieldIDCCVP_20, 'UTF-8') > 20 ? mb_substr($fieldIDCCVP_20, 0, 20, 'UTF-8') . "..." : $fieldIDCCVP_20;
if ($fieldIDCCVP_21) {
    $fieldIDCCVP_21 = mb_strlen($fieldIDCCVP_21, 'UTF-8') > 20 ? mb_substr($fieldIDCCVP_21, 0, 20, 'UTF-8') . "..." : $fieldIDCCVP_21 . " Мест";
}
if ($fieldIDCCVP_22) {
    $fieldIDCCVP_22 = mb_strlen($fieldIDCCVP_22, 'UTF-8') > 20 ? mb_substr($fieldIDCCVP_22, 0, 20, 'UTF-8') . "..." : "(" . $fieldIDCCVP_22 . " кг)";
}
$fieldIDCCVP_23 = mb_strlen($fieldIDCCVP_23, 'UTF-8') > 150 ? mb_substr($fieldIDCCVP_23, 0, 150, 'UTF-8') . "..." : $fieldIDCCVP_23;
//$fieldIDCCVP_24 = mb_strlen($fieldIDCCVP_24, 'UTF-8') > 100 ? mb_substr($fieldIDCCVP_24, 0, 100, 'UTF-8') . "..." : $fieldIDCCVP_24;
$fieldIDCCVP_25 = mb_strlen($fieldIDCCVP_25, 'UTF-8') > 200 ? mb_substr($fieldIDCCVP_25, 0, 200, 'UTF-8') . "..." : $fieldIDCCVP_25;
//$fieldIDCCVP_26 = mb_strlen($fieldIDCCVP_26, 'UTF-8') > 25 ? mb_substr($fieldIDCCVP_26, 0, 25, 'UTF-8') . "..." : $fieldIDCCVP_26;
$fieldIDCCVP_27 = mb_strlen($fieldIDCCVP_27, 'UTF-8') > 25 ? mb_substr($fieldIDCCVP_27, 0, 25, 'UTF-8') . "..." : $fieldIDCCVP_27;
$safSideInfo = mb_strlen($safSideInfo, 'UTF-8') > 200 ? mb_substr($safSideInfo, 0, 200) . "..." : $safSideInfo;
$safSideName = mb_strlen($safSideName, 'UTF-8') > 200 ? mb_substr($safSideName, 0, 200) . "..." : $safSideName;
//$productProducerCountries = mb_strlen($productProducerCountries, 'UTF-8') > 100 ? mb_substr($productProducerCountries, 0, 100) . "..." : $productProducerCountries;


$someText = '';
if (count($subAppProducts) > 1) {
    $someText = 'Молҳои ниёзи мардум мувофики замимаи';
} else {
    $someText = $subAppProducts[0]->commercial_description;
    $someText = mb_strlen($someText, 'UTF-8') > 40 ? mb_substr($someText, 0, 40, 'UTF-8') . "..." : $someText;
}

?>

<table>
    <tr>
        <td style="
			font-size: 16px;
			padding: 0;
			text-transform: uppercase;
			text-align: center;
		"></td>
    </tr>
    <tr>
        <td style="
			font-size: 12px;
			text-transform: uppercase;
			text-align: center;
		"></td>
    </tr>
    <tr>
        <td style="
			font-size: 26px;
			text-align: center;
		"></td>
    </tr>
</table>
<br/>
<br/>
<table>
    <tr>
        <td style="
			font-size: 16px;
			font-style: italic;
			text-align: center;
		"></td>
        <td style="
			font-size: 16px;
			font-style: italic;
			text-align: center;
		"></td>
    </tr>
</table>
<br/>
<br/>
<table>
    <tr>
        <td style="
			font-size: 38px;
			padding: 0;
			text-align: center;
		"></td>
    </tr>
</table>

<table align="left">
    <tr>
        <td style="
			font-size: 13px;;
			vertical-align: top;
			width: 140px;
			font-weight:bold;
			text-align: left;
		" valign="top">Эътибор дорад аз<br/>Срок действия с
        </td>
        <td>
            <table>
                <tr>
                    <td style="width: 230px;">
                        <table>
                            <tr style="font-size: 12px;">
                                <td style="
									width: 10px;
									vertical-align: top;
								" valign="top">“
                                </td>
                                <td style="
									border-bottom: 1px solid #000000;
									vertical-align: top;
									text-align:center;
									width: 40px;
								" valign="top">{{$fromDay ?? ''}}</td>
                                <td style="
									vertical-align: top;
									width: 10px;
								" valign="top">“
                                </td>
                                <td style="
									border-bottom: 1px solid #000000;
									vertical-align: top;
									text-align: center;
									width: 100px;
								" valign="top">{{$fromMonth ?? ''}}</td>
                                <td style="
									font-size: 13px;;
									vertical-align: top;
									width: 20px;
								" valign="top">20
                                </td>
                                <td style="
									border-bottom: 1px solid #000000;
									vertical-align: top;
									width: 20px;
								" valign="top">{{$fromYear ?? ''}}</td>
                                <td style="
									font-size: 12px;;
									vertical-align: top;
									width: 15px;
									font-weight:bold;
								" valign="top">с.
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="
						font-size: 12px;
						vertical-align: middle;
						text-align: center;
						width: 30px;
						font-weight:bold;
					" valign="middle">ТО<br/>ДО
                    </td>
                    <td style="width: 230px;">
                        <table>
                            <tr style="font-size: 12px;">
                                <td style="
									vertical-align: top;
									width: 10px;
								" valign="top">“
                                </td>
                                <td style="
									border-bottom: 1px solid #000000;
									vertical-align: top;
									text-align:center;
									width: 40px;
								" valign="top">{{$toDay ?? ''}}
                                </td>
                                <td style="
									vertical-align: top;
									width: 10px;
								" valign="top">“
                                </td>
                                <td style="
									border-bottom: 1px solid #000000;
									vertical-align: top;
									text-align:center;
									width: 100px;
								" valign="top">{{$toMonth ?? ''}}
                                </td>
                                <td style="
									font-size: 13px;;
									vertical-align: top;
									width: 20px;
								" valign="top">20
                                </td>
                                <td style="
									border-bottom: 1px solid #000000;
									vertical-align: top;
									width: 20px;
								" valign="top">{{$toYear ?? ''}}
                                </td>
                                <td style="
									font-size: 12px;;
									vertical-align: top;
									width: 15px;
									font-weight:bold;
								" valign="top">с.
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>

        </td>
    </tr>
</table>
<br/>
<br/>
<table>
    <tr>
        <td style="font-size: 12px;vertical-align: top;text-align: left;width: 26%;font-weight:bold;">Мақомот оид
            ба<br/>сертификатсияи маҳсулот<br/>Орган по сертификации
        </td>
        <td style="text-align: center;font-size: 12px;width: 74%;">{{$fieldIDCCVP_16}}
            <br/>
            {{$fieldIDCCVP_17 . ', ' . $fieldIDCCVP_19 . ' '. $fieldIDCCVP_1 . ' ' . $regionalOfficePhoneNumber}}
        </td>
    </tr>
    <tr>
        <td style="
			font-size: 13px;;
			vertical-align: top;
			text-align: left;
			width: 75%;
		">
        </td>
        <td style="width: 4%"></td>
        <td style="
			text-align: right;
			font-size: 12px;
			width: 21%;
		">
            <div style="
				font-size: 10px;
				float: right;
				width: 100px;
				text-align: center;
				line-height: 0.6;
			">
                ---------------------------------------<br/>
                <span style="font-weight:bold;font-size: 9px;">рамзи НУМ / код ОКП</span>
            </div>
        </td>
    </tr>
</table>
<br>
<br>
<table>
    <tr>
        <td style="
			font-size: 12px;;
			vertical-align: top;
			text-align: left;
			width: 15%;
			font-weight:bold;
		">Маҳсулот<br/>Продукция
        </td>
        <td style="
			text-align: center;
			font-size: 12px;
			width: 60%;
		">{{$someText}}<br>{{$fieldIDCCVP_21}}, {{$fieldIDCCVP_22}}, {{$fieldIDCCVP_23}}
        </td>
        <td width="3%"></td>
        <td style="text-align: center;width: 22%;font-size: 12px;line-height: 0.6;">
            @if (!empty($fieldIDCCVP_20))
                <span style="font-size: 12px;text-align:center;">{{$fieldIDCCVP_20}}</span>
            @endif
            <br>
            <span style="font-size:10px">---------------------------------------</span><br>
            <span style="font-size: 9px;font-weight:bold;">рамзи НМ ФИХ / код ТВ ВЭД</span>
        </td>
    </tr>
</table>
<br>
<br>
<table>
    <tr>
        <td style="
			font-size: 12px;;
			vertical-align: top;
			text-align: left;
			font-weight:bold;
			width:33%;
		">Ба талаботи ҳуҷҷатҳои меъёрии<br/>зерин мутобиқат мекунад<br>Соответствует требованиям<br/>нормативных
            документов
        </td>
        <td style="text-align: center; font-size: 12px;width:67%;">{{$fieldIDCCVP_24}}
        </td>
    </tr>
    <tr>
        <td style="
			font-size: 11px;
			vertical-align: top;
			text-align: left;
			font-weight:bold;
		">
        </td>
        <td></td>
    </tr>
</table>
<table>
    <tr>
        <td style="
			font-size: 12px;;
			vertical-align: top;
			text-align: left;
			font-weight:bold;
			width:20%;
		">Истеҳсол шудааст<br>Изготовлено
        </td>
        <td style="font-size: 12px;width:80%; text-align: center;">{!! $productProducerInfo !!}</td>
    </tr>
    <tr>
        <td style="
			font-size: 12px;
			vertical-align: top;
			text-align: left;
			font-weight:bold;
		">
        </td>
    </tr>
</table>
<br/>
<table>
    <tr>
        <td style="
			font-size: 12px;;
			vertical-align: top;
			text-align: left;
			font-weight:bold;
			width:25%;
		">Сертификат дода шуд<br><span style="
			font-size: 11px;
			font-weight:bold;
		">Сертификат выдан</span>
        </td>
        <td style="font-size: 12px;width:75%;text-align: center;">{{$safSideName}}<br>{{$safSideInfo}}
        </td>
    </tr>
    <tr>
        <td style="
			font-size: 12px;
			vertical-align: top;
			text-align: left;
			width:60%;
		">
        </td>
    </tr>
</table>
<br/>
<table>
    <tr>
        <td style="
			font-size: 12px;;
			vertical-align: top;
			text-align: left;
			font-weight:bold;
			width:15%;
		">Дар асосӣ<br><span style="
			font-size: 11px;
			font-weight:bold;
		">На основании
        </span>
        </td>
        <td style="font-size: 12px;width:85%;text-align: center;">{{$fieldIDCCVP_25}}
        </td>
    </tr>
    <tr>
        <td style="
			font-size: 12px;
			vertical-align: top;
			text-align: left;
		">
        </td>
        <td></td>
    </tr>
</table>
<br/>
<table>
    <tr>
        <td style="
			font-size: 12px;;
			vertical-align: top;
			text-align: left;
			font-weight:bold;
			width:30%;
		">Маълумоти иловагӣ<br><span style="font-size: 11px;font-weight:bold;">Допалнительная информация</span>
        </td>
        <td style="
			font-size: 12px;
			vertical-align: top;
			text-align: center;
			width:70%;
			height:85px;
		">{{$fieldIDCCVP_26}}
        </td>
    </tr>
</table>
<br/>
<br/>
<br/>
<table>
    <tr>
        <td style="
			font-size: 20px;
			font-weight: bold;
			vertical-align: top;
			text-align: center;
			width: 15%;
		"><br/><br/>Ҷ.М.<br/>М.П.
        </td>
        <td style="
			vertical-align: top;
			text-align: left;
			width: 85%;
		">
            <table>
                <tr>
                    <td style="
						font-size: 13px;;
						vertical-align: top;
						text-align: left;
						width: 190px;
					">
                        <table>
                            <tr>
                                <td style="
									font-size: 12px;
									vertical-align: top;
									text-align: left;
									font-weight:bold;
								">Роҳбари мақомот
                                </td>
                            </tr>
                            <tr>
                                <td style="
									font-size: 11px;
									vertical-align: top;
									text-align: left;
									font-weight:bold;
								">Руководитель органа
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="
						font-size: 10px;
						text-align: center;
						width: 118px;
					"><span style="color:white;font-size: 12px;text-align: center;">emptyText</span>
                        -----------------------------<br/>
                        <span style="font-size: 9px;font-weight:bold;">имзо / подпись</span>
                    </td>
                    <td style="
						font-size: 10px;
						text-align: right;
						width: 200px;
					"><span style="font-size: 12px;text-align: center;">{{$fieldIDCCVP_27}}</span>
                        -------------------------------------------------<br/>
                        <span style="font-size: 9px;font-weight:bold;">ному насаб / инициалы, фамилия</span>
                    </td>
                </tr>
            </table>
            <br/>
            <br/>
            <table>
                <tr>
                    <td style="
						font-size: 13px;;
						vertical-align: top;
						text-align: left;
						width: 200px;
					">
                        <table>
                            <tr>
                                <td style="
									font-size: 12px;
									vertical-align: top;
									text-align: left;
									font-weight:bold;
								">Сардори Раёсат (шӯъба)
                                </td>
                            </tr>
                            <tr>
                                <td style="
									font-size: 11px;
									vertical-align: top;
									text-align: left;
									font-weight:bold;
								">Начальник управления (отдела)
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="
						font-size: 10px;
						text-align: center;
						width: 118px;
					"><span style="color:white;font-size: 12px;text-align: center;">emptyText</span>
                        -----------------------------<br/>
                        <span style="font-size: 9px;font-weight:bold;">имзо / подпись</span>
                    </td>
                    <td style="
						font-size: 10px;
						text-align: right;
						width: 200px;
					"><span style="font-size: 12px;text-align: center">{{$fieldIDCCVP_28}}</span>
                        -------------------------------------------------<br/>
                        <span style="font-size: 9px;font-weight:bold;">ному насаб / инициалы, фамилия</span>
                    </td>
                </tr>
            </table>

        </td>
    </tr>
</table>
