<?php

use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplication\SingleApplication;

$safGeneralRegime = $saf->data['saf']['general']['regime'] ?? '';
$safRegularNumber = $saf->data['saf']['general']['regular_number'] ?? '';

switch ($safGeneralRegime) {
    case SingleApplication::REGIME_EXPORT:
        $safSideName = $saf->data['saf']['sides']['export']['name'] ?? '';
        break;
    default:
        $safSideName = $saf->data['saf']['sides']['import']['name'] ?? '';
        break;
}
$transportType = $saf->data['saf']['transportation']['type_of_transport'] ?? '';
$safTransportationExportCountry = $saf->data['saf']['transportation']['export_country'] ?? '';

if ($safTransportationExportCountry) {
    $safTransportationExportCountry = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safTransportationExportCountry))->name;
}

if ($transportType) {
    $transportType = optional(getReferenceRows(ReferenceTable::REFERENCE_TRANSPORT_TYPE, $transportType))->name;
}

$transitCountries = $saf->data['saf']['transportation']['transit_country'];
$transCountriesList = '';
$transCountries = [];

foreach ($transitCountries as $transitCountry) {
    if (!is_null($transitCountry)) {
        $transCountries[] = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $transitCountry))->name;
    }
}
if (!empty($transCountries)) {
    $transCountries = array_unique($transCountries);
    $transCountriesList = implode(',', $transCountries);
}

$totalApprovedQuantity = 0;
$number = 1;
$productsInfo = '';

foreach ($subAppProducts as $key => $subAppProduct) {
    $productsInfo .= $number++ . '. ' . $subAppProduct['commercial_description'] .'<br>';
    $productCountries[] = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $subAppProduct['producer_country']))->name;
}

$productCountries = array_unique($productCountries);
$number1 = $number2 = 1;
$productCountriesList = $productProducersNameList = '';

foreach ($productCountries as $productCountry) {
    if (!is_null($productCountry)) {
        $productCountriesList .= $number1++ . '. ' . $productCountry."<br>";
    }
}

if (!is_null($customData)) {

    $permissionDateDatasetMdm = DocumentsDatasetFromMdm::where(['document_id' => $documentId, 'mdm_field_id' => 355])->pluck('field_id')->first();
    $expirationDateDatasetMdm = DocumentsDatasetFromMdm::where(['document_id' => $documentId, 'mdm_field_id' => 357])->pluck('field_id')->first();

    $subApplicationPermissionDate = (!is_null($permissionDateDatasetMdm) && array_key_exists($permissionDateDatasetMdm, $customData)) ? $customData[$permissionDateDatasetMdm] : '';
    $subApplicationExpirationDate = (!is_null($expirationDateDatasetMdm) && array_key_exists($expirationDateDatasetMdm, $customData)) ? $customData[$expirationDateDatasetMdm] : '';

    if (!empty($subApplicationPermissionDate) && !empty($subApplicationExpirationDate)) {

        $interval = date_diff(date_create($subApplicationPermissionDate), date_create($subApplicationExpirationDate));
        $daysDiff = $interval->format('%a');
    }
}

//----- Get fields values by MDM_ID
$mdmFields = getFieldsValueByMDMID($documentId, $customData);

?>
<table cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td style="height:45px;"></td>
    </tr>
    <tr>
        <td style="width:635px;text-align:right;font-size: 14px">{{$safRegularNumber}}</td>
        <td style="width:25px;"></td>
    </tr>
    <tr>
        <td style="height:260px"></td>
    </tr>
    <tr>
        <td style="width:90%;font-size: 14px;font-weight:bold;text-align:center;line-height:1.4">
            <span color="#3793FF">
            ИМПОРТНОЕ КАРАНТИННОЕ РАЗРЕШЕНИЯ
            </span>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="width:90%;font-size: 14px;line-height:1.4">
            Комитет продовольственной безопасности при Правительстве РТ разрешает ввоз товаров в Республику Таджикистан.
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="width:5%"></td>
        <td style="width:100%;font-size: 14px;line-height:1.4">
            Детальная информация о ввозе представлена в таблице.
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="width:5%"></td>
        <td style="width:100%;font-size: 14px;line-height:1.4">
            Срок действия данного разрешения {{$daysDiff ?? ''}} дней.
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="width:90%">
            <table cellpadding="1" border="1">
                <tr>
                    <td style="font-size: 14px;font-weight: bold;text-align: left;line-height:1.4"> Предприниматель
                    </td>
                    <td style="font-size: 14px;text-align: center;">{{$safSideName}}</td>
                </tr>
                <tr>
                    <td style="font-size: 14px;font-weight:bold; text-align: left;line-height:1.4"> Транспортное<br>
                        средство
                    </td>
                    <td style="font-size: 14px; text-align: center;">{{$transportType}}</td>
                </tr>
                <tr>
                    <td style="font-size: 14px;font-weight: bold;text-align: left;line-height:1.4"> Маршрут
                    </td>
                    <td style="font-size: 14px;text-align: center;">{{$transCountriesList}}</td>
                </tr>
                <tr>
                    <td style="font-size: 14px;font-weight: bold;text-align: left;line-height:1.4"> Отправление
                    </td>
                    <td style="font-size: 14px;text-align: center;">{{$safTransportationExportCountry}}</td>
                </tr>
                <tr>
                    <td style="font-size: 14px;font-weight: bold;text-align: left;line-height:1.4"> Товар(ы)</td>
                    <td style="font-size: 14px;text-align: center;">{!! $productsInfo !!}</td>
                </tr>
                <tr>
                    <td style="font-size: 14px;font-weight: bold;text-align: left;line-height:1.4"> Производства<br>
                        страны
                    </td>
                    <td style="font-size: 14px;text-align: center;">{!! $productCountriesList !!}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="height:100px"></td>
    </tr>
    <tr>
        <td style="width:45%;"></td>
        <td style="width:45%;font-size: 14px;text-align:right;line-height:1.4">
            {{$mdmFields['365'] ?? ''}}
        </td>
    </tr>
</table>