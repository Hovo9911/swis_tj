<?php

use App\Http\Controllers\Core\Dictionary\DictionaryManager;
use App\Models\BaseModel;
use App\Models\ConstructorTemplates\ConstructorTemplates;
use App\Models\ReferenceTable\ReferenceTable;

//----- Get fields values by MDM_ID
$mdmFields = getFieldsValueByMDMID($documentId, $customData, ConstructorTemplates::LANGUAGE_CODE_RU);

//------------- Get data field values by saf_id ----------//\
list($mainDataBySafId, $productsDataBySafId, $productBatchesDataBySafId) = getSafDataFieldIdValue($saf, $subApplication, $subAppProducts, $availableProductsIds, BaseModel::LANGUAGE_CODE_RU);

$dictionaryManager = new DictionaryManager();

//----- 2.
$field6141_4 = $customData['6141_4'] ?? '';
$field6141_27 = $customData['6141_27'] ?? '';
$typeOfNarcoticDrugs = optional(getReferenceRows(ReferenceTable::REFERENCE_TYPE_OF_NARCOTIC_DRUGS, $field6141_27, false, ['code_type_of_narcotic_drugs'], false, false, false, [], false, BaseModel::LANGUAGE_CODE_RU))->code_type_of_narcotic_drugs;

$point2 = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.key_10", BaseModel::LANGUAGE_CODE_RU) . ' ' . $field6141_4 . '-' . $typeOfNarcoticDrugs;

//----- 3.
$field6141_5 = $customData['6141_5'] ?? '';
if (!is_null($field6141_5) && !empty($field6141_5)) {

    $time = strtotime($field6141_5);

    $field6141_5_day = date('d', $time);
    $field6141_5_month = '<u class="tc">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . month(date('m', $time), BaseModel::LANGUAGE_CODE_RU) . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>';
    $field6141_5_year = date('Y', $time);
}

//----- 4.
$safID14 = (isset($mainDataBySafId['SAF_ID_14']) && !empty($mainDataBySafId['SAF_ID_14'])) ? $mainDataBySafId['SAF_ID_14'] . ',<br>' : '';
$safID15 = (isset($mainDataBySafId['SAF_ID_15']) && !empty($mainDataBySafId['SAF_ID_15'])) ? $mainDataBySafId['SAF_ID_15'] . ', ' : '';
$safID16 = (isset($mainDataBySafId['SAF_ID_16']) && !empty($mainDataBySafId['SAF_ID_16'])) ? $mainDataBySafId['SAF_ID_16'] . ', ' : '';
$safID17 = (isset($mainDataBySafId['SAF_ID_17']) && !empty($mainDataBySafId['SAF_ID_17'])) ? $mainDataBySafId['SAF_ID_17'] : '';

$point4 = '<u>' . $safID14 . $safID15 . $safID16 . $safID17 . '</u>';

//----- 5
$safID6 = (isset($mainDataBySafId['SAF_ID_6']) && !empty($mainDataBySafId['SAF_ID_6'])) ? $mainDataBySafId['SAF_ID_6'] . ',<br>' : '';
$safID7 = (isset($mainDataBySafId['SAF_ID_7']) && !empty($mainDataBySafId['SAF_ID_7'])) ? $mainDataBySafId['SAF_ID_7'] . ', ' : '';
$safID8 = (isset($mainDataBySafId['SAF_ID_8']) && !empty($mainDataBySafId['SAF_ID_8'])) ? $mainDataBySafId['SAF_ID_8'] . ', ' : '';
$safID9 = (isset($mainDataBySafId['SAF_ID_9']) && !empty($mainDataBySafId['SAF_ID_9'])) ? $mainDataBySafId['SAF_ID_9'] : '';

$point5 = '<u>' . $safID6 . $safID7 . $safID8 . $safID9 . '</u>';

//----- 6.
$point6 = $customData['6141_28'] ?? '';

//----- 8.
$field6141_29 = $customData['6141_29'] ?? '';
$field6141_6 = isset($customData['6141_6']) && !empty($customData['6141_6']) ? formattedDate($customData['6141_6']) : $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.key_18", BaseModel::LANGUAGE_CODE_RU) . '<br>';
$field6141_14 = $customData['6141_14'] ?? '';
$targetOfImport = optional(getReferenceRows(ReferenceTable::REFERENCE_TARGET_OF_IMPORT, $field6141_14))->name;

?>
<table>
    <tr>
        <td class="main-table vat">
            <table>
                <tr>
                    <td class="tc">
                        <div class="fs20 fb">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.key_1", BaseModel::LANGUAGE_CODE_RU)}}</div>
                        <div class="fs20 fb">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.key_2", BaseModel::LANGUAGE_CODE_RU)}}</div>
                    </td>
                </tr>
                <br>
                <br>
                <tr>
                    <td class="tc">
                        <div class="fs24 fb uppercase">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.key_3", BaseModel::LANGUAGE_CODE_RU)}}</div>
                    </td>
                </tr>
                <br>
                <tr>
                    <td class="tc">
                        <div class="fs16 fb">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.key_4", BaseModel::LANGUAGE_CODE_RU)}}
                        </div>
                    </td>
                </tr>
                <br>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="fs13 tc"
                                    width="35%">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.key_5", BaseModel::LANGUAGE_CODE_RU)}}
                                    <b>{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.key_6", BaseModel::LANGUAGE_CODE_RU)}}</b>
                                </td>
                                <td class="fs13" width="65%"></td>
                            </tr>
                            <tr>
                                <td class="fs13">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.key_7", BaseModel::LANGUAGE_CODE_RU)}}
                                    <b><u>{{$point2}}</u></b></td>
                                <td class="fs13 tc fb">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.key_8", BaseModel::LANGUAGE_CODE_RU)}}
                                    «{{$field6141_5_day ?? ''}}
                                    » {!! $field6141_5_month??'__________________'!!}  {{($field6141_5_year ?? "")." ".$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.key_9", BaseModel::LANGUAGE_CODE_RU)}}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <br>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="fs13 vat"
                                    width="35%">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.key_11", BaseModel::LANGUAGE_CODE_RU)}}</td>
                                <td class="fs13 vat fb" width="65%">{!! $point4 !!}</td>
                            </tr>
                            <br><br>
                            <tr>
                                <td class="fs13"></td>
                                <td class="fb"></td>
                            </tr>
                            <tr>
                                <td class="fs13 vat"
                                    width="35%">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.key_12", BaseModel::LANGUAGE_CODE_RU)}}</td>
                                <td class="fs13 vat fb" width="65%">{!! $point5 !!}</td>
                            </tr>
                            <br><br>
                            <tr>
                                <td class="fs13 vat"
                                    width="35%">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.key_13", BaseModel::LANGUAGE_CODE_RU)}}</td>
                                <td class="fs13 vat fb" width="65%">
                                    <u>{!! nl2br($point6) !!}</u></td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;
                                <td>
                            </tr>
                            <br><br>
                            <tr>
                                <td class="fs13 vat" width="35%">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.key_14", BaseModel::LANGUAGE_CODE_RU)}}</td>
                                <td class="fs13 vat fb" width="65%">{!! nl2br($customData['6141_22'] ?? '') !!}</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;
                                <td>
                            </tr>
                            <br><br>
                            <tr>
                                <td class="fs13 vat">{!! $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.key_15", BaseModel::LANGUAGE_CODE_RU)!!}</td>
                                <td class="fs13 vat fb"><u>{!! $field6141_29 !!}</u></td>
                            </tr>
                            <br><br>
                            <tr>
                                <td class="fs13 vat">{!! $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.key_19", BaseModel::LANGUAGE_CODE_RU)!!}
                                </td>
                                <td class="fs13 vat fb"><u>{!! $field6141_6 !!}</u></td>
                            </tr>
                            <tr>
                                <td class="fs13">{!! $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.key_20", BaseModel::LANGUAGE_CODE_RU)!!}
                                </td>
                                <td class="fs13 fb"><u>{!! $targetOfImport !!}</u></td>
                            </tr>
                            <tr>
                                <td class="fs13">{!! $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.key_21", BaseModel::LANGUAGE_CODE_RU)!!}
                                </td>
                                <td class="fs13 fb"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="height:450px;"></td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="fs13 fb"
                                    width="40%">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.key_16", BaseModel::LANGUAGE_CODE_RU)}}</td>
                                <td class="border-bottom" width="30%"></td>
                                <td width="30%"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td class="fs11 tc">
                                    <em>{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.key_17", BaseModel::LANGUAGE_CODE_RU)}}</em>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="fs13 fb" width="50%">«_____»_________________________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.key_9", BaseModel::LANGUAGE_CODE_RU)}}</td>
                                <td colspan="2"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<style>
    @page {
        margin: 5mm 10mm;
    }

    table {
        width: 100%;
        overflow: wrap;
    }

    .main-table {
        width: 100%;
        border: 4px double black;
        height: 1220px;
    }

    .tc {
        text-align: center;
    }

    .border-bottom {
        border-bottom: 1px solid #000000;
    }

    .uppercase {
        text-transform: uppercase;
    }

    .fs11 {
        font-size: 11px;
    }

    .fs13 {
        font-size: 13px;
    }

    .fs16 {
        font-size: 16px;
    }

    .fs20 {
        font-size: 20px;
    }

    .fs24 {
        font-size: 24px;
    }

    .fb {
        font-weight: bold;
    }

    .vat {
        vertical-align: top;
    }
</style>
