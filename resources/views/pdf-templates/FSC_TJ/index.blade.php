<?php

use App\Http\Controllers\Core\Dictionary\DictionaryManager;
use App\Models\BaseModel;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplicationProductsBatch\SingleApplicationProductsBatch;

$translationsTJ = (new DictionaryManager())->getTransKeysByLng($template, BaseModel::LANGUAGE_CODE_TJ);

$fieldID6012_5 = $customData['6012_5'] ?? '';
$fieldID6012_6 = $customData['6012_6'] ?? '';

if (!empty($fieldID6012_5) && !empty($fieldID6012_6)) {

    $interval = date_diff(date_create($fieldID6012_5), date_create($fieldID6012_6));
    $daysDiff = $interval->format('%a');
}

$transportType = $saf->data['saf']['transportation']['type_of_transport'] ?? '';
$safGeneralRegime = $saf->data['saf']['general']['regime'] ?? '';

if ($transportType) {
    $transportType = optional(getReferenceRows(ReferenceTable::REFERENCE_TRANSPORT_TYPE, $transportType, false, [], 'get', false, false, [], false, BaseModel::LANGUAGE_CODE_TJ))->name;
}

$products = $productCountries = $productCountriesList = $productProducersName = [];
$productsInfo = [];
$transCountriesList = '';


foreach ($subAppProducts as $key => $subAppProduct) {

    $productBatches = SingleApplicationProductsBatch::select(['approved_quantity'])->where('saf_product_id', $subAppProduct->id)->get();
    $totalQuantity = 0;

    foreach ($productBatches as $productBatch) {
        $totalQuantity += $productBatch->approved_quantity;
    }

    $refMeasurementUnit = getReferenceRows(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $subAppProduct['measurement_unit'], false, [], 'get', false, false, [], false, BaseModel::LANGUAGE_CODE_TJ);
    $measurementUnit = optional($refMeasurementUnit)->name;
    $measurementUnitText = (!is_null($measurementUnit)) ? $measurementUnit : '';
    $productsInfo[] = $totalQuantity . ' ' . $measurementUnitText . ' ' . $subAppProduct['commercial_description'];
    $productCountries[] = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $subAppProduct['producer_country'], false, [], 'get', false, false, [], false, BaseModel::LANGUAGE_CODE_TJ))->name;
    $productProducersName[] = $subAppProduct['producer_name'];
}

$productsInfo = implode(', ', $productsInfo);

$transitCountries = $saf->data['saf']['transportation']['transit_country'] ?? '';
$safTransportationAppointment = $saf->data['saf']['transportation']['appointment'] ?? '';

if (!empty($safTransportationAppointment)) {
    $safTransportationAppointment = getReferenceRows(ReferenceTable::REFERENCE_CUSTOM_BODY_REFERENCE, $safTransportationAppointment, false, ['name', 'address'], 'get', false, false, [], false, BaseModel::LANGUAGE_CODE_TJ);
    $safTransportationAppointmentName = optional($safTransportationAppointment)->name;
    $safTransportationAppointmentAddress = optional($safTransportationAppointment)->address;
}

if (!empty($safTransportationExportCountry)) {
    $safTransportationExportCountry = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safTransportationExportCountry, false, [], 'get', false, false, [], false, BaseModel::LANGUAGE_CODE_TJ))->name;
}

$exporterName = $saf->data['saf']['sides']['export']['name'] ?? '';
$importerName = $saf->data['saf']['sides']['import']['name'] ?? '';
$exportCountry = $saf->data['saf']['transportation']['export_country'] ?? '';
$importCountry = $saf->data['saf']['transportation']['import_country'] ?? '';
if (!empty($exportCountry)) {
    $safTransportationExportCountry = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $exportCountry, false, [], 'get', false, false, [], false, BaseModel::LANGUAGE_CODE_TJ))->name;
}
if (!empty($safTransportationImportCountry)) {
    $safTransportationImportCountry = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safTransportationImportCountry, false, [], 'get', false, false, [], false, BaseModel::LANGUAGE_CODE_TJ))->name;
}

?>
<table>
    <tr>
        <td style="height:250px"></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
</table>
<table>
    <tr>
        <td style="width:20%"></td>
        <td style="width:40%;text-align:right;font-size: 13px"><span
                    style="color:white">{!! ($translationsTJ["swis.pdf.{$template}.fix_text_1"] ?? '') !!}</span><br>{{($translationsTJ["swis.pdf.{$template}.copy"] ?? '')}}
            &nbsp;&nbsp;
        </td>
        <td style="width:40%;text-align:left;font-size: 13px">{!! ($translationsTJ["swis.pdf.{$template}.fix_text_1"] ?? '') !!}
            <br>{!! ($translationsTJ["swis.pdf.{$template}.fix_text_2"] ?? '').'<br>'.$importerName !!}</td>
    </tr>
</table>
<br>
<br>
<br>
<br>
<table>
    <tr>
        <td style="font-size: 13px;text-align: left;width:3%;line-height: 1.6;"></td>
        <td style="font-size: 13px;text-align: justify;width:94%;line-height: 1.6;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            {{($translationsTJ["swis.pdf.{$template}.description.fix_text_1"] ?? '')}} {{$importerName}} {{($translationsTJ["swis.pdf.{$template}.description.fix_text_2"] ?? '')}} {{$exporterName}}  {{$safTransportationExportCountry ?? ''}}
            {{($translationsTJ["swis.pdf.{$template}.description.fix_text_3"] ?? '')}} {{$productsInfo}} {{($translationsTJ["swis.pdf.{$template}.description.fix_text_4"] ?? '')}} {{$transportType}}  {{($translationsTJ["swis.pdf.{$template}.description.fix_text_5"] ?? '')}} {{$safTransportationImportCountry ?? ''}}
            {{$safTransportationAppointmentName." ".$safTransportationAppointmentAddress}} {!! nl2br($customData['6012_15'] ?? '') !!} {!! ($translationsTJ["swis.pdf.{$template}.description.fix_text_7"] ?? '') !!} {{$daysDiff ?? ''}} {{($translationsTJ["swis.pdf.{$template}.description.fix_text_8"] ?? '')}}
            .
        </td>
        <td style="font-size: 13px;text-align: left;width:3%;line-height: 1.6;"></td>
    </tr>
</table>
<br><br><br><br>
<table width="100%" border="0">
    <tr>
        <td style="width: 10%;"></td>
        <td style="font-size: 13px;text-align: left;width: 40%;">
            {{($translationsTJ["swis.pdf.{$template}.chairman"] ?? '')}}
        </td>
        <td style="font-size: 13px;line-height: 20px;text-align: right;width: 40%;">
            {{$customData['6012_11'] ?? ''}}
        </td>
        <td style="width: 10%;"></td>
    </tr>
</table>
