<?php

use App\Models\ReferenceTable\ReferenceTable;
use App\Models\RegionalOffice\RegionalOffice;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationProductsBatch\SingleApplicationProductsBatch;

$fieldID01207_8 = $customData['01207_8'] ?? '';
$fieldID01207_25 = $customData['01207_25'] ?? '';

list($mainDataBySafId, $productsDataBySafId) = getSafDataFieldIdValue($saf, $subApplication, $subAppProducts, $availableProductsIds);
//----- 1.
$safID14 = (isset($mainDataBySafId['SAF_ID_14']) && !empty($mainDataBySafId['SAF_ID_14'])) ? $mainDataBySafId['SAF_ID_14'] . ', ' : '';
$safID15 = (isset($mainDataBySafId['SAF_ID_15']) && !empty($mainDataBySafId['SAF_ID_15'])) ? $mainDataBySafId['SAF_ID_15'] : '';
$safID16 = (isset($mainDataBySafId['SAF_ID_16']) && !empty($mainDataBySafId['SAF_ID_16'])) ? $mainDataBySafId['SAF_ID_16'] . ', ' : '';
$safID17 = (isset($mainDataBySafId['SAF_ID_17']) && !empty($mainDataBySafId['SAF_ID_17'])) ? $mainDataBySafId['SAF_ID_17'] . ', ' : '';

$point1 = $safID14 . $safID16 . $safID17 . $safID15;

//----- 3.
$safID6 = (isset($mainDataBySafId['SAF_ID_6']) && !empty($mainDataBySafId['SAF_ID_6'])) ? $mainDataBySafId['SAF_ID_6'] . ', ' : '';
$safID7 = (isset($mainDataBySafId['SAF_ID_7']) && !empty($mainDataBySafId['SAF_ID_7'])) ? $mainDataBySafId['SAF_ID_7'] : '';
$safID8 = (isset($mainDataBySafId['SAF_ID_8']) && !empty($mainDataBySafId['SAF_ID_8'])) ? $mainDataBySafId['SAF_ID_8'] . ', ' : '';
$safID9 = (isset($mainDataBySafId['SAF_ID_9']) && !empty($mainDataBySafId['SAF_ID_9'])) ? $mainDataBySafId['SAF_ID_9'] . ', ' : '';

$point3 = $safID6 . $safID8 . $safID9 . $safID7;

//----- 7. ,9.
$productCountries = [];
$point7 = $point9 = '';
$productsCount = count($subAppProducts);
if ($productsCount > 1) {
    $number = 1;
    foreach ($subAppProducts as $subAppProduct) {
        $safID91 = notEmptyForPdfFields($productsDataBySafId[$subAppProduct->id], 'SAF_ID_91');
        $safID96 = notEmptyForPdfFields($productsDataBySafId[$subAppProduct->id], 'SAF_ID_96');
        $safID97 = notEmptyForPdfFields($productsDataBySafId[$subAppProduct->id], 'SAF_ID_97');
        $safID98 = trans("swis.pdf.{$template}.brutto_weight").' '.notEmptyForPdfFields($productsDataBySafId[$subAppProduct->id], 'SAF_ID_98').' '.trans("swis.pdf.{$template}.kg");
        $safID99 = '&nbsp;&nbsp;&nbsp;'.trans("swis.pdf.{$template}.netto_weight").' '.notEmptyForPdfFields($productsDataBySafId[$subAppProduct->id], 'SAF_ID_99').' '.trans("swis.pdf.{$template}.kg");

        $point7 .= $number . '. ' . $safID91 . '; ' . ($customData['01207_37'] ?? '') . ' - ' . $safID96 . ' ' . $safID97 . '<br>';
        $point9 .= $number++ . '. ' . $safID98 . '<br> ' . $safID99.'<br>';
    }
} else {

    $safID98 = trans("swis.pdf.{$template}.brutto_weight").' '.notEmptyForPdfFields($productsDataBySafId[$subAppProducts[0]->id], 'SAF_ID_98').' '.trans("swis.pdf.{$template}.kg");
    $safID99 = trans("swis.pdf.{$template}.netto_weight").' '.notEmptyForPdfFields($productsDataBySafId[$subAppProducts[0]->id], 'SAF_ID_99').' '.trans("swis.pdf.{$template}.kg");

    $point7 = notEmptyForPdfFields($productsDataBySafId[$subAppProducts[0]->id], 'SAF_ID_91') .' '. ($customData['01207_37'] ?? '') . ' - ' . notEmptyForPdfFields($productsDataBySafId[$subAppProducts[0]->id], 'SAF_ID_96') . ' ' . notEmptyForPdfFields($productsDataBySafId[$subAppProducts[0]->id], 'SAF_ID_97');
    $point9 = $safID98 . '<br> ' . $safID99;
}


$safTransportationCountry = $saf->data['saf']['transportation']['import_country'] ?? '';
$safTransportationCountry = !empty($safTransportationCountry) ? optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safTransportationCountry))->name : '';

foreach ($subAppProducts as $key => $subAppProduct) {

    $productCountries[] = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $subAppProduct['producer_country']))->name;
    $productProducersName[] = $subAppProduct['producer_name'];
}

$productCountries = array_unique($productCountries);
$productCountriesList = implode(',', $productCountries);
$stateNumbers = [];

$transportType = $saf->data['saf']['transportation']['type_of_transport'] ?? '';

if ($transportType) {
    $transportType = optional(getReferenceRows(ReferenceTable::REFERENCE_TRANSPORT_TYPE, $transportType))->name;
}
$safTransportationStateNumberCustomSupports = $saf->data['saf']['transportation']['state_number_customs_support'];

foreach ($safTransportationStateNumberCustomSupports as $safTransportationStateNumberCustomSupport) {
    if (!is_null($safTransportationStateNumberCustomSupport)) {
        $stateNumbers[] = $safTransportationStateNumberCustomSupport['state_number'];
    }
}
if (count($stateNumbers) > 0) {
    $stateNumbersList = implode(',', $stateNumbers);
}

$subdivisionId = optional($subApplication)->agency_subdivision_id;
if (!is_null($subdivisionId)) {
    $subDivision = optional(getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME, $subdivisionId, false));
}

//----- Get fields values by MDM_ID
$mdmFields = getFieldsValueByMDMID($documentId, $customData);

?>
<table cellspacing="0" cellpadding="1" border="1">
    <tr>
        <td class="fs11" style="width:40%">
            <table>
                <tr>
                    <td class="fb line_height">1.Ном ва суроғаи содиротгар</td>
                </tr>
                <tr>
                    <td>Название и адрес экспортера</td>
                </tr>
                <tr>
                    <td style="line-height: 1"></td>
                </tr>
                <tr>
                    <td class="fb">{{$point1}}</td>
                </tr>
            </table>
        </td>
        <td style="width:20%;text-align: center;height:90px"></td>
        <td style="width:40%"></td>
    </tr>
    <tr>
        <td class="fs11" style="width:45%;">
            <table>
                <tr>
                    <td class="fb line_height">3.Ном ва суроғаи воридотгар</td>
                </tr>
                <tr>
                    <td>Получатель и его адрес</td>
                </tr>
                <tr>
                    <td style="line-height: 1"></td>
                </tr>
                <tr>
                    <td class="fb">{{$point3}}</td>
                </tr>
            </table>
        </td>
        <td style="width:55%;">
            <table>
                <tr>
                    <td style="font-size: 11px;font-weight: bold;line-height: 1.4">4.Кумитаи бехатарии озуқавории</td>
                </tr>
                <tr>
                    <td style="font-size: 11px;font-weight: bold;line-height: 1.4">назди Ҳукумати Ҷумҳурии Тоҷикистон
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 11px;line-height: 1.4">Комитет продовольственной безопасности</td>
                </tr>
                <tr>
                    <td style="font-size: 11px;line-height: 1.4">при Правительстве Республики Таджикистанн</td>
                </tr>
                <tr>
                    <td style="font-size: 11px;font-weight: bold;line-height: 1.4">Ба ташкилоти (ҳои) карантин ва
                        муҳофизати растании
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 11px;line-height: 1.4">Организации (ям) по карантину и защите растений</td>
                </tr>
                <tr>
                    <td style="font-size: 11px;font-weight: bold;line-height: 1.4"></td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid #000000;font-weight: bold;text-align:center;width:300px;font-size: 11px;">{{$safTransportationCountry}}</td>
                </tr>
                <tr>
                    <td style="font-size: 10px;text-align:center;line-height: 1.4"><span style="font-weight: bold">(номи давлат)  </span><span>(название страни)</span>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 11px;font-weight: bold;line-height: 1.4"></td>
                </tr>
                <table>
                    <tr>
                        <td style="font-size: 11px;font-weight: bold;line-height: 1.4">5.Макони пайдоиш</td>
                        <td class="fs11 fb line_height" style="text-align:left;">
                            {{$productCountriesList}}
                        </td>
                    </tr>
                </table>
                <tr>
                    <td style="font-size: 11px;line-height: 1.4">Место происхождения
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table>
                <tr>
                    <td style="font-size: 11px;font-weight: bold;line-height: 1.4">6.Тарзи боркашонӣ</td>
                </tr>
                <table>
                    <tr>
                        <td style="font-size: 11px;line-height: 1.4;width:25%">Способ транспортировки
                        </td>
                        <td class="fs11 fb" style="text-align:left;width:75%;line-height: 0">
                            {{$transportType.' '.$stateNumbersList}}
                        </td>
                    </tr>
                </table>

                <tr>
                    <td class="fs11 fb line_height">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table>
                <tr>
                    <td style="font-size: 11px;font-weight: bold;line-height: 1.4">7.Нуқтаи воридсозӣ</td>
                </tr>
                <table>
                    <tr>
                        <td style="font-size: 11px;line-height: 1.4;width:25%">Пункт ввоза
                        </td>
                        <td class="fs11 fb" style="text-align:left;width:75%;line-height: 0">
                            {{($customData['01207_18'] ?? '')}}
                        </td>
                    </tr>
                </table>

                <tr>
                    <td class="fs11 fb line_height">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="width:80%">
            <table>
                <tr>
                    <td style="font-size: 11px;font-weight: bold;line-height: 1.4">8.Нишонаҳои фарқкунанда (маркировка);
                        миқдори ҷои ва намуди бастабандӣ; номгӯи маҳсулот; номи ботаникии растанӣ
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 11px;line-height: 1.4">Отличительные знаки (маркировка); количество мест и
                        описание упаковки; наименование продукции; ботаническое название растений
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 11px;line-height: 1.4"></td>
                </tr>
                <tr>
                    <td class="fs11 fb line_height">{!! $point7 !!}</td>
                </tr>
            </table>
        </td>
        <td style="width:20%;height:120px;">
            <table>
                <tr>
                    <td style="font-size: 11px;font-weight: bold;line-height: 1.4">9.Миқдори маҳсулот</td>
                </tr>
                <tr>
                    <td style="font-size: 11px;line-height: 1.4">Количество продукции
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 11px;line-height: 1.4"></td>
                </tr>
                <tr class="fs11 fb line_height">
                    <td>{!! $point9 !!}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table>
                <tr>
                    <td style="font-size: 11px;font-weight: bold;line-height: 1.4">10.Тасдиқ карда мешавад, ки растаниҳо
                        ё маҳсулотҳои растанигии зикршуда мутобиқи методика
                        ва қоидаҳои амалкунанда таҳқиқ шудаанд,аз организмҳои зараррасони карантинӣ ва дигар организмҳои
                        осебрасон озоданд ва онҳо ҷавобгуӣ талаботҳои қоидаҳои
                        фитосанитарии мамлакати воридгар мебошанд․
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 11px;line-height: 1.4">Настоящим удостоверяется,что растения или растительные
                        продукции описанные выше были
                        исследованы в соответствии с существующими методиками и правилами,признаны свободными от
                        карантинных и других причиняющих
                        ущерб вредных организмов и что они отвечают требованиям фитосанитарных правил страны импортера․
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <table nobr="true" cellspacing="0" cellpadding="1" border="1">
        <tr>
            <td colspan="2">
                <table>
                    <tr>
                        <td style="width:25%;font-size: 11px;font-weight: bold;line-height: 1.4;">11.Эъломияи иловагӣ
                        </td>
                        <td style="width:75%;font-size: 11px;font-weight: bold;line-height: 1.4;">{{$mdmFields['489'] ?? ''}}</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-size: 11px;line-height: 1.4">Дополнительная декларация</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width:50%;">
                <table>
                    <tr>
                        <td style="font-size: 11px;font-weight: bold;line-height: 1.4;text-align:center;">
                            БЕЗАРАРГАРДОНӢ
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 11px;line-height: 1.4;text-align:center;">ОБЕЗЗАРАЖИВАНИЕ</td>
                    </tr>
                </table>
            </td>
            <td rowspan="7" style="width:50%;">
                <table>
                    <tr>
                        <td style="font-size: 11px;font-weight: bold;line-height: 1.4;text-align:left;">18.Ҷои додани
                            ҳуҷҷат
                        </td>
                    </tr>
                    <table>
                        <tr>
                            <td style="font-size: 11px;line-height: 1.4;text-align:left;">Место выдачи</td>
                            <td class="fs11 fb line_height" style="text-align:left;">{{$subDivision->name ?? ''}}</td>
                        </tr>
                    </table>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="font-size: 11px;font-weight: bold;line-height: 1.4">Сана</td>
                    </tr>
                    <table>
                        <tr>

                            <td style="font-size: 11px;line-height: 1.4">Дата</td>
                            <td class="fb fs11" style="text-align:left;">
                                {{formattedDate($fieldID01207_8)}}
                            </td>
                        </tr>
                    </table>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="font-size: 11px;font-weight: bold;line-height: 1.4;width:400px;">Ному насаб ва имзои
                            нозир:
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 11px;line-height: 1.4;width:400px;">Фамилия и подпись инспектора:
                        </td>
                    </tr>
                    <tr>
                        <td class="fs12 fb" style="width:70%;">{{($customData['01207_10'] ?? '')}}</td>
                        <td style="font-size: 12px;font-weight: bold;vertical-align: top;width:30%;text-align:center">
                            <br/>Ҷ.М.
                        </td>
                    </tr>
                    <tr>
                        <td style="width:70%"></td>
                        <td style="font-size: 12px;vertical-align: top;width:30%;text-align:center">М.П.</td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td style="font-size: 10px;font-weight: bold;line-height: 1.4;text-align:left;">Ягон ӯхдадории
                            молиявӣ нисбати ҳамин сертификат,ба Кумитаи бехатарии озуқавории назди
                            Ҳукумати Ҷумҳурии Тоҷикистон ё ба ягон нафар нозирон ё намояндагони вай,гузошта намешавад.
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height:0.5;"></td>
                    </tr>
                    <tr>
                        <td style="font-size: 10px;line-height: 1.4;text-align:left;">Никаких финансовых обязательств в
                            отношении настоящего сертификата не налагается на Комитет продовольственной безопасности при
                            Правительстве Республики Таджикистан или на кого-либо из ее инспекторов или представителей․
                        </td>
                    </tr>
                </table>
            </td>

        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="fs12 fb line_height">12.Усули коркард</td>
                    </tr>
                    <tr>
                        <td class="fs12">Способ обработки</td>
                    </tr>
                    <tr>
                        <td class="fs12 fb line_height">{{($customData['01207_21'] ?? '')}}</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="fs12 fb line_height">13. Заҳркимиё (маводи таъсиркунанда)</td>
                    </tr>
                    <tr>
                        <td class="fs12">Химикат (действующее вещество)</td>
                    </tr>
                    <tr>
                        <td class="fs12 fb line_height">{{($customData['01207_22'] ?? '')}}</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="fs12 fb line_height">14. Давомнокии коркард ва ҳарорат</td>
                    </tr>
                    <tr>
                        <td class="fs12 line_height">Экспозиция и температура °C</td>
                    </tr>
                    <tr>
                        <td class="fs12 fb line_height">{{(isset($customData['01207_23']) ? $customData['01207_23'].'°C':'')}}</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="fs12 fb line_height">15. Консентратсия</td>
                    </tr>
                    <tr>
                        <td class="fs12 line_height">Концентрация <span class="fb">{{($customData['01207_24'] ?? '')}}</span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="fs12 fb line_height">16. Сана</td>
                    </tr>
                    <tr>
                        <td class="fs12 line_height">Дата <span class="fb">{{formattedDate($fieldID01207_25)}}</span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="fs12 fb line_height">17. Маълумоти иловагӣ</td>
                    </tr>
                    <tr>
                        <td class="fs12 line_height">Дополнительная информация<br><span
                                    class="fb">{{$mdmFields['397'] ?? ''}}</span></td>
                        <td class="fs12 fb line_height">
                            {{($customData['01207_35'] ?? '')}}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</table>
<style>
    .fb {
        font-weight: bold;
    }

    .fs11 {
        font-size: 11px;
    }

    .fs12 {
        font-size: 12px;
    }

    .line_height {
        line-height: 1.4
    }
</style>