<?php

use App\Models\Document\Document;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\RegionalOffice\RegionalOffice;
use App\Models\SingleApplicationProductsBatch\SingleApplicationProductsBatch;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use Illuminate\Support\Facades\DB;

//----- Get fields values by MDM_ID
$mdmFields = getFieldsValueByMDMID($documentId, $customData);

list($mainDataBySafId, $productsDataBySafId, $productBatchesDataBySafId) = getSafDataFieldIdValue($saf, $subApplication, $subAppProducts, $availableProductsIds);

$subdivisionId = $subApplication->agency_subdivision_id;
if (!is_null($subdivisionId)) {
    $subDivisionCode = optional(getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME, $subdivisionId, false))->code;

    $regionalOffice = RegionalOffice::select('regional_office_ml.address', 'phone_number', 'regional_office_ml.name')
        ->leftJoin('regional_office_ml', 'regional_office.id', '=', 'regional_office_ml.regional_office_id')
        ->where(['lng_id' => cLng('id'), 'office_code' => $subDivisionCode])
        ->active()
        ->first();

    if (!is_null($regionalOffice)) {

        $regionalOfficeAddress = $regionalOffice->address;
        $regionalOfficePhoneNumber = $regionalOffice->phone_number;

        $regionalOfficeInfo = $regionalOfficeAddress . ' ' . $regionalOfficePhoneNumber;
        $regionalOfficeName = $regionalOffice->name;
    }
}
$subApplicationSafData = $subApplication->data;

//----- 1. ,2. ,3.
$approvedDate = $mdmFields[355] ?? '';

$approvedDateParse = date_parse_from_format(config('swis.date_time_format_front'), $approvedDate) ?? '';

$point1 = !empty($approvedDateParse) ? $approvedDateParse['day'] : '';
$point2 = !empty($approvedDateParse) ? month($approvedDateParse['month']) : '';
$point3 = !empty($approvedDateParse) ? substr($approvedDateParse['year'], 2, 2) : '';

//----- 4. ,13.1
switch ($mainDataBySafId['SAF_ID_1']) {
    case SingleApplication::REGIME_IMPORT:
        $point4 = $mainDataBySafId['SAF_ID_6'] ?? '';

        //----- 16.
        $safTransportationAppointment = $subApplicationSafData['saf']['transportation']['appointment'] ?? '';
        if (!empty($safTransportationAppointment)) {
            $transportInfo = getReferenceRows(ReferenceTable::REFERENCE_CUSTOM_BODY_REFERENCE, $safTransportationAppointment, false, ['code', 'name', 'address']);//SAF_ID_73
            if (!is_null($transportInfo)) {
                $point16 = '(' . optional($transportInfo)->code . ') ' . optional($transportInfo)->name . ', ' . optional($transportInfo)->address . ' ' . trans("swis.pdf.{$template}.new_key_3");
            }
        }
        break;
    case SingleApplication::REGIME_EXPORT:
        $point4 = $mainDataBySafId['SAF_ID_14'] ?? '';

        //----- 16.
        if (!empty($safTransportationDeparture)) {
            $safTransportationDeparture = $subApplicationSafData['saf']['transportation']['departure'] ?? '';
            $transportInfo = getReferenceRows(ReferenceTable::REFERENCE_CUSTOM_BODY_REFERENCE, $safTransportationDeparture, false, ['code', 'name', 'address']);//SAF_ID_73
            if (!is_null($transportInfo)) {
                $point16 = '(' . optional($transportInfo)->code . ') ' . optional($transportInfo)->name . ', ' . optional($transportInfo)->address . ' ' . trans("swis.pdf.{$template}.new_key_4");
            }
        }
        break;
    case SingleApplication::REGIME_TRANSIT:
        //----- 16.
        $safTransportationAppointment = $subApplicationSafData['saf']['transportation']['appointment'] ?? '';
        if (!empty($safTransportationAppointment)) {
            $transportInfo = getReferenceRows(ReferenceTable::REFERENCE_CUSTOM_BODY_REFERENCE, $safTransportationAppointment, false, ['code', 'name', 'address']);//SAF_ID_73
            if (!is_null($transportInfo)) {
                $point16 = '(' . optional($transportInfo)->code . ') ' . optional($transportInfo)->name . ', ' . optional($transportInfo)->address . ' ' . trans("swis.pdf.{$template}.new_key_5");
            }
        }
        break;
}

//----- 5. , 6.1 ,17.
$safID96 = $safID99 = 0;
foreach ($subAppProducts as $key => $subAppProduct) {

    $totalQuantity = $totalApprovedQuantity = $totalRejectedQuantity = 0;
    $productBatches = SingleApplicationProductsBatch::select(['quantity', 'measurement_unit', 'approved_quantity', 'rejected_quantity'])->where('saf_product_id', $subAppProduct->id)->get();
    $refMeasurementUnit = getReferenceRows(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $subAppProduct['measurement_unit'], false, ['short_name']);

    foreach ($productBatches as $productBatch) {
        $totalQuantity += optional($productBatch)->quantity;
        $totalApprovedQuantity += optional($productBatch)->approved_quantity;
        $totalRejectedQuantity += optional($productBatch)->rejected_quantity;
    }

    if (isset($productsInfo[$subAppProduct['commercial_description']][optional($refMeasurementUnit)->short_name])) {
        $productsInfo[$subAppProduct['commercial_description']][optional($refMeasurementUnit)->short_name]['quantity'] += $totalQuantity;
        $productsInfo[$subAppProduct['commercial_description']][optional($refMeasurementUnit)->short_name]['approved_quantity'] += $totalApprovedQuantity;
        $productsInfo[$subAppProduct['commercial_description']][optional($refMeasurementUnit)->short_name]['rejected_quantity'] += $totalRejectedQuantity;
    } else {
        $productsInfo[$subAppProduct['commercial_description']][optional($refMeasurementUnit)->short_name]['quantity'] = $totalQuantity;
        $productsInfo[$subAppProduct['commercial_description']][optional($refMeasurementUnit)->short_name]['approved_quantity'] = $totalApprovedQuantity;
        $productsInfo[$subAppProduct['commercial_description']][optional($refMeasurementUnit)->short_name]['rejected_quantity'] = $totalRejectedQuantity;
    }
    //----- 6.1
    $safID96 += (int)notEmptyForPdfFields($productsDataBySafId[$subAppProduct->id], 'SAF_ID_96');

    //----- 18.
    $safID99 += (int)notEmptyForPdfFields($productsDataBySafId[$subAppProduct->id], 'SAF_ID_99');
}

$point5Array = $point6Array = $point6TmpArray = $point19Array = $point19TmpArray = $point21TmpArray = $point21Array = $productQuantityAndMesUnit = [];

$countProductsInfo = count($productsInfo);

foreach ($productsInfo as $productCommercialDescription => $productMesUnitArray) {

    if ($countProductsInfo == 1) {
        $point5Array[] = $productCommercialDescription;
    }

    foreach ($productMesUnitArray as $productMesUnit => $productQuantity) {
        if ($countProductsInfo > 1) {
            $point5Array[] = $productCommercialDescription . ' - ' . $productQuantity['quantity'] . ' ' . $productMesUnit;
        }
        if (isset($point6TmpArray[$productMesUnit])) {
            $point6TmpArray[$productMesUnit] += $productQuantity['quantity'];
        } else {
            $point6TmpArray[$productMesUnit] = $productQuantity['quantity'];
        }

        if (isset($point19TmpArray[$productMesUnit])) {
            $point19TmpArray[$productMesUnit] += $productQuantity['rejected_quantity'];
        } else {
            $point19TmpArray[$productMesUnit] = $productQuantity['rejected_quantity'];
        }

        if (isset($point21TmpArray[$productMesUnit])) {
            $point21TmpArray[$productMesUnit] += $productQuantity['approved_quantity'];
        } else {
            $point21TmpArray[$productMesUnit] = $productQuantity['approved_quantity'];
        }
    }
}

foreach ($point6TmpArray as $mesUnit => $quantity) {
    $point6Array[] = $quantity . ' ' . $mesUnit;
}

foreach ($point19TmpArray as $mesUnit => $quantity) {
    $point19Array[] = $quantity . ' ' . $mesUnit;
}

foreach ($point21TmpArray as $mesUnit => $quantity) {
    $point21Array[] = $quantity . ' ' . $mesUnit;
}

$point5 = implode(', ', $point5Array);
$point5 = getMappingValueInMultipleRows($point5, 75, 90);

$point6 = implode(', ', $point6Array);
$point17 = $point6;

//----- 19.
$point19 = implode(', ', $point19Array);

//----- 21.
$point21 = implode(', ', $point21Array);

//----- 6.2
$point6_2 = notEmptyForPdfFields($productsDataBySafId[$subAppProducts[0]->id], 'SAF_ID_97');

//----- 7. ,8.
$safID92 = notEmptyForPdfFields($productsDataBySafId[$subAppProducts[0]->id], 'SAF_ID_92');
$safID95 = $point8 = isset($subAppProducts[0]) ? $subAppProducts[0]['producer_name'] : '';
$productBatches = array_values($productBatchesDataBySafId)[0];
$safID109 = $safID151 = [];

foreach ($productBatches as $productBatch) {
    if (!is_null($productBatch['SAF_ID_109'])) {
        $safID109[] = formattedDate($productBatch['SAF_ID_109']);
    }
}
if (count($safID109)) {
    $safID109List = implode(' ', $safID109);
}

$point7 = $safID92 . ' ' . $safID95;
$point7 = getMappingValueInMultipleRows($point7, 70, 90);

$safID146 = $safID145 = 0;
foreach ($productBatchesDataBySafId as $productBatchDataBySafId) {
    foreach ($productBatchDataBySafId as $productBatch) {

        if (!is_null($productBatch['SAF_ID_151'])) {
            $safID151[] = $productBatch['SAF_ID_151'];
            $safID146 += $productBatch['SAF_ID_146'];
            $safID145 += $productBatch['SAF_ID_145'];
        }
    }
}

if (count($safID151)) {
    $safID151 = array_unique($safID151);
    $point8 = implode(', ', $safID151);
} else {
    $point8 = trans("swis.pdf.{$template}.new_key_1");
}
$point8 = getMappingValueInMultipleRows($point8, 65, 90);

//----- 9.
$transitCountries = $subApplicationSafData['saf']['transportation']['transit_country'] ?? '';
foreach ($transitCountries as $transitCountry) {
    if (!is_null($transitCountry)) {
        $transCountries[] = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $transitCountry))->name;
    }
}
if (!empty($transCountries)) {
    $transCountries = array_unique($transCountries);
    $transCountriesList = implode(', ', $transCountries);
}

$safTransportationExportCountryID = $saf->data['saf']['transportation']['export_country'] ?? '';//SAF_ID_78
$safTransportationExportCountry = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safTransportationExportCountryID, false))->name;

$safTransportationImportCountryID = $saf->data['saf']['transportation']['import_country'] ?? '';//SAF_ID_79
$safTransportationImportCountry = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safTransportationImportCountryID, false))->name;

$countriesList = $safTransportationExportCountry . ' - ' . ($transCountriesList ?? '') . ', ' . $safTransportationImportCountry;

$customsSupport = $trackContainerTemperature = [];

$safTransportationStateNumberCustomSupports = $subApplicationSafData['saf']['transportation']['state_number_customs_support'] ?? '';
foreach ($safTransportationStateNumberCustomSupports as $safTransportationStateNumberCustomSupport) {
    if (isset($safTransportationStateNumberCustomSupport['track_container_temperature']) && !is_null($safTransportationStateNumberCustomSupport['track_container_temperature'])) {
        $trackContainerTemperature[] = $safTransportationStateNumberCustomSupport['track_container_temperature'];
    }
    if (isset($safTransportationStateNumberCustomSupport['customs_support']) && !is_null(($safTransportationStateNumberCustomSupport['customs_support']))) {
        $customsSupport[] = $safTransportationStateNumberCustomSupport['customs_support'];
    }

}

$trackContainerTemperatureList = count($trackContainerTemperature) ? ',  ' . implode(', ', $trackContainerTemperature) : '';
$point9 = ($mainDataBySafId['SAF_ID_75'] ?? '') . ' ' . ($countriesList ?? '') . ' ' . $trackContainerTemperatureList;

//----- 10.
$safID6 = (isset($mainDataBySafId['SAF_ID_6']) && !empty($mainDataBySafId['SAF_ID_6'])) ? $mainDataBySafId['SAF_ID_6'] . ' ' : '';
$safID7 = (isset($mainDataBySafId['SAF_ID_7']) && !empty($mainDataBySafId['SAF_ID_7'])) ? $mainDataBySafId['SAF_ID_7'] . ' ' : '';
$safID8 = (isset($mainDataBySafId['SAF_ID_8']) && !empty($mainDataBySafId['SAF_ID_8'])) ? $mainDataBySafId['SAF_ID_8'] . ' ' : '';
$safID9 = (isset($mainDataBySafId['SAF_ID_9']) && !empty($mainDataBySafId['SAF_ID_9'])) ? $mainDataBySafId['SAF_ID_9'] : '';

$point10 = $safID6 . $safID7 . $safID8 . $safID9;

//----- 11.
$transPortDocumentCodes = DB::table(ReferenceTable::REFERENCE_DOCUMENT_DEPENDING_TRANSPORT)->where('show_status', ReferenceTable::STATUS_ACTIVE)->pluck('document_code')->all();

$attachedDocumentProducts = SingleApplicationSubApplications::getAttachedDocumentProductsForPDF($subApplication, $availableProductsIds);
foreach ($attachedDocumentProducts as $value) {

    if (in_array($value->document->document_type, $transPortDocumentCodes)) {
        $documents[] = $value->document->document_number . ' ' . $value->document->document_release_date;
    }

    if ($value->document->document_type == Document::DOCUMENT_CODE_6012) {
        $documents6012[] = $value->document->document_number . ' ' . $value->document->document_release_date;
    }

}
$point11 = isset($documents) ? implode(', ', ($documents ?? '')) : '';

//----- 12.
$point12 = ($mdmFields[412] ?? '') . ' ' . ($mdmFields[414] ?? '') . ' ' . ($mdmFields[417] ?? '') .' '. ($mdmFields[511] ?? '');
$point12 = getMappingValueInMultipleRows($point12, 15, 120);

//----- 13.2
if ($mainDataBySafId['SAF_ID_1'] == SingleApplication::REGIME_IMPORT) {
    $point13_2 = isset($documents6012) ? implode(', ', ($documents6012 ?? '')) : '';
} else {
    $point13_2 = '';
}

//----- 13.3
$point13_3 = $mdmFields[199] ?? '';

?>
<table cellspacing="0" cellpadding="1" border="0">
    <tr>
        <td height="30px"></td>
    </tr>
</table>
<table>
    <tr style="line-height:1.0">
        <td></td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:50%;text-align:left;font-size: 11px;">{{$regionalOfficeInfo ?? ''}}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;width:48%;text-align:center;">
            <span>(ноҳия, шаҳр / район, город) </span>
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:50%;text-align:left;font-size: 11px;">{{$regionalOfficeName ?? ''}}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;width:48%;text-align:center;">
            <span>(номи муассиса / наименование учреждения) </span>
        </td>
    </tr>
</table>
<table cellspacing="0" cellpadding="1" border="0">
    <tr>
        <td style="height:100px"></td>
    </tr>
</table>
<table>
    <tr>
        <td style="font-size: 11px;width:420px;font-weight:bold;"></td>
        <td style="border-bottom: 1px solid #000000;text-align:justify;font-size: 11px;width:30px;">
            «{{$point1 ?? ''}}»
        </td>
        <td style="font-size: 11px;width:5px;font-weight:bold;"></td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:140px;">{{$point2 ?? ''}}</td>
        <td style="text-align:center;font-size: 11px;width:15px;">20</td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:20px;">{{$point3 ?? ''}} </td>
        <td style="font-size: 11px;width:50px;">сол/год</td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:105%;line-height: 1.2;text-align:center;">Ман, духтури байтории дар поён
            имзокарда, шаҳодатномаи байтории мазкурро ба/<br> Я нижеподписавшийся ветеринарный врач, выдал настоящее
            ветеринарное свидетельство
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:665px;text-align:left;font-size: 11px;">{{$point4 ?? ''}}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;width:100%;text-align:center;line-height: 1.2;">
            <span>(ба кӣ-номи шахси юридикӣ ё ному насаби шахси воқеӣ / кому, наименования юридических лиц или Ф,И,О физических лиц) </span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;;width:145px;">дар бобати он, ки / в том, что</td>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:520px">{{$point5[0] ?? ''}}</td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 1.2;text-align:left;width:30%;"></td>
        <td style="font-size: 8px;text-align:center;line-height: 1.2;width:70%;">
            <span>(номгӯи маҳсулот / наименование продукции)</span>
        </td>
    </tr>
    @if(count($point5)>1)
        @for($i=1;$i<count($point5);$i++)
            <tr>
                <td style="border-bottom: 1px solid #000000;text-align:left;width:665px"
                    class="fs11">{{$point5[$i]}}</td>
            </tr>
        @endfor
    @endif
    <tr>
        <td style="font-size: 11px;width:130px;">ба микдори / в количестве</td>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:535px">
            <table>
                <tr>
                    <td style="width:28%;text-align:center">{!! $point6!!}</td>
                    <td style="width:44%;text-align:center">{{$safID96." ".$point6_2}}</td>
                    <td style="width:23%;text-align:center">{{$mdmFields[95] ?? ''}}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 1.2;text-align:left;width:130px;"></td>
        <td style="font-size: 8px;text-align:center;line-height: 1.2;width:535px">
            <table>
                <tr>
                    <td style="width:28%;text-align:center">(ҷой, дона, кг / мест, штук, кг)</td>
                    <td style="width:44%;text-align:center">(борбанд / упаковка)</td>
                    <td style="width:23%;text-align:center">(тамғазанӣ / маркировка)</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;;width:170px">тайёр карда шудааст / выработанная</td>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:495px;">{{$point7[0]}}</td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 1.2;text-align:left;width:33%;"></td>
        <td style="font-size: 7px;text-align:center;line-height: 1.2;width:67%;">
            <span>(номи корҳона, ному насаб, соҳиби маҳсулот, суроға / наименование предприятия, ф.и.о. владельца, адрес)</span>
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:665px;text-align:left;font-size: 11px;">{{$safID109List ?? ''}}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;width:100%;text-align:center;line-height: 1.2;">
            <span>(таърихи коркард/дата выработки)</span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:105%;line-height: 1.2;">пурра аз санҷиши байторию беҳдоштӣ гузаронида
            шудааст / аз ашёи хом тайёр шудааст, ки аз санҷиши байторию беҳдоштӣ гузаштааст (ҷои нодаркорро хат занед)/
            подвергнута ветеринарно-санитарной экспертизе в полном объёме/ изготовлено из сырья, прошедшего
            ветеринарно-санитарную экспертизу (ненужное зачеркнуть).
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;;width:205px;">Ва муносиб барои / и признано годным для</td>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:460px;">{{$point8[0]}}</td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 1.2;text-align:left;width:20%;"></td>
        <td style="font-size: 7px;text-align:center;line-height: 1.2;width:80%;">
            <span>(фурӯш бемаҳдудият, бо маҳдудият-сабабҳо нишон дода шавад/реализация без ограничений с ограничением указать причины)</span>
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:665px;text-align:left;font-size: 11px;">{{($mdmFields[501] ?? '')}}</td>
    </tr>
    <tr>
        <td style="font-size: 7px;width:100%;text-align:center;line-height: 1.2;">
            <span>(ё мувофиқи қоидаҳои байторию беҳдоштӣ аз нав кор карда баромадан тасдиқ шудааст/или переработки согласно правилам ветсанэкспертизы)</span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;;width:130px;">ва тариқи / и направляется</td>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:535px;">{{$point9}}</td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 1.2;text-align:left;width:20%;"></td>
        <td style="font-size: 8px;text-align:center;line-height: 1.2;width:80%;">
            <span>(намуди нақлиёт, самти хатсайр, шарти кашонидан/ вид транспорта, маршрут следования, условия перевозки)</span>
        </td>
    </tr>
    <tr>
        <td style="text-align:left;font-size: 11px;width:35px;line-height: 1.2;">ба / в</td>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:320px;line-height: 1.2;">{{$point10}}</td>
        <td style="font-size: 11px;width:30px;text-align:right;line-height: 1.2;"></td>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:280;line-height: 1.2;">мувофиқи / по {{$point11}}</td>
    </tr>
    <tr>
        <td style="text-align:center;font-size: 11px;width:35px;line-height: 1.2;"></td>
        <td style="text-align:center;font-size: 7px;width:280px;line-height: 1.2;">(ном ва суроғаи шахси
            қабулқунанда/наименование и адрес получателя)
        </td>
        <td style="font-size: 11px;width:90px;text-align:right;line-height: 1.2;"></td>
        <td style="text-align:center;font-size: 7px;width:260px;line-height: 1.2;">(№ ҳуҷҷатҳои молию нақлиётӣ / № и
            дата выдачи товаротранспортного документа)
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:left;line-height: 1.2;width:550px">Маҳсулот аз ташхиси иловагии лабораторӣ
            / Продукция подвергнута дополнительным лабораторным исследованиям
        </td>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:115px;">{{$point12[0]}}</td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:665px;">{{$point12[1] ?? ''}}</td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 1.2;text-align:left;width:20%;"></td>
        <td style="font-size: 8px;text-align:center;line-height: 1.2;width:80%;">
            <span>(номи озмоишгоҳ/наименование лаборатории)</span>
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:100%;">{{$point12[2] ?? ''}}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;line-height: 1.2;width:100%;">
            <span>(№ санҷиш ва натиҷаи ташхис / № экспертизы и результат исследования)</span>
        </td>
    </tr>
    <tr>
        <td style="width:100%;text-align:center;font-size: 11px;line-height:1.4"></td>
    </tr>
    <tr>
        <td style="font-size: 13px;;width:270px">ИШОРАҲОИ МАХСУС / ОСОБЫЕ ОТМЕТКИ</td>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:395px">{{$mdmFields[437] ?? ''}}</td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 1.2;text-align:left;width:45%;"></td>
        <td style="font-size: 7px;text-align:center;line-height: 1.2;width:55%;">
            <span>(ҳолати эпизоотии маҳал/указываются эпизоотическое благополучия местности</span>
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:665px;text-align:left;font-size: 11px;">{{$point13_2 ??''}}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;width:100%;text-align:center;line-height: 1.2;">
            <span>таърих ва рақами ичозатномаи нозири давлатии роҳбариқунанда / дата и номера разрешений вышестоящего госуветинспектора</span>
        </td>
    </tr>

    <tr>
        <td style="border-bottom: 1px solid #000000;width:665px;text-align:left;font-size: 11px;">{{$point13_3 ??''}}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;width:100%;text-align:center;line-height: 1.2;">
            <span>барои баровардани маҳсулот берун аз ҳудуди кишвар / на вывоз продукции за пределы территории республики</span>
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:665px;text-align:left;font-size: 11px;">{{$mdmFields[464] ??''}}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;width:100%;text-align:center;line-height: 1.2;">
            <span>№ тамга ва г / № перечисляются клейм и др.) </span>
        </td>
    </tr>
    <tr>
        <td style="width:100%;line-height:0.5"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:105%;line-height: 1.2;text-align:center;">Намуди нақлиёт тоза ва безарар карда
            шудааст / Транспортное средство очищено и продезинфицировано.
        </td>
    </tr>
    <tr>
        <td style="width:100%;line-height:0.5;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:100%;line-height: 1.2;">Шаҳодатнома дар вақти назорати бое дар давоми роҳ
            пешниҳод карда шуда, ба гирандаи бор дода мешавад. Нусхаҳои шаҳодатнома эътибор надоранд. Дар ҳолати муайян
            кардани вайронкунии тартиби пуркунии варақа шаҳодатнома ба сарнозири давлатии байтории субъекти Ҷумҳурии
            Тоҷикистон аз рӯи маҳали содирот ва нишондоди қойдавайронкунӣ супорида мешавад/ Свидетельство предъявляется
            для контроля при погрузке, в пути следования и передаётся грузополучателю. Копии свидетельства
            недействительны. При установлении нарушений порядка заполнения бланка свидетельство передаётся главному
            госветинспектору субъекта Республики Таджикистан по месту выдачи с указанием выявленных нарушений.
        </td>
    </tr>
    <tr>
        <td style="width:50%"></td>
        <td style="font-size: 12px;font-weight:bold;width:50%;line-height: 1.2;">
            Духтури байторӣ<br>
            Ветеринарный врач
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:200px;line-height: 1.2;">
            <table>
                <tr>
                    <td style="font-size: 11px;line-height: 1.2;text-align:left;font-weight:bold;">
                        Ҷ.М.
                    </td>
                </tr>
            </table>
        </td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:420px">{{$mdmFields['420'] ??''}}</td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.2;text-align:left;"></td>
        <td style="font-size: 9px;text-align:center;line-height: 1.2;width:400px">
            <span>(имзо, номи пурраи вазифа/подпись, полное наименование должности)</span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:200px;line-height: 1.2;">
            <table>
                <tr>
                    <td style="font-size: 11px;line-height: 1.2;text-align:left;font-weight:bold;">
                        М.П.
                    </td>
                </tr>
            </table>
        </td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:420px">{{$mdmFields['382'] ??''}}</td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.2;text-align:left;"></td>
        <td style="font-size: 9px;text-align:center;line-height: 1.2;width:400px">
            <span>(ному насаб/ ф.и.о.)</span>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td></td>
    </tr>
</table>
<table pagebreak="true">
    <tr>
        <td style="font-size: 11px;width:100%;line-height: 1.2;text-align:center;">Қайд дар бораи муоинаи байторию
            бехдоштӣ ҳангоми интиқол додани бор, дар хатсайр ва нуқтаи таъиншуда./<br>
            Отметки о ветеринарно-санитарном осмотре при погрузке, в пути следования и на месте назначения
        </td>
    </tr>
</table>
<table>
    <tr>
        <td></td>
    </tr>
</table>
<table cellspacing="0" cellpadding="1" border="1">
    <tr>
        <td rowspan="2" style="font-size: 11px;line-height: 1.2;text-align:center;width:135px;">
            Сана ва номгӯи нуқта,<br> ки муоинаи байторӣ<br> гузаронида мешавад.<br> (гирифтан)<br> БК-бор кардан,<br>
            Т-транзит,<br> БФ-борфарорӣ/<br> Дата<br> и наименование пункта,<br> где проводился<br> ветеринарный
            осмотр<br> (изьятие)<br> (П-погрузка,<br> Т-транзит,<br> В-выгрузка)
        </td>
        <td colspan="2"
            style="font-size: 11px;line-height: 1.2;text-align:center;width:138px;height:10px">
            <br><br><br><br>Муоинани маҳсулот, ашё/<br> Осмотрено<br> продуктов,<br> сырья
        </td>
        <td colspan="2" style="font-size: 11px;line-height: 1.2;text-align:center;width:140px">
            Миқдори маҳсулот, ашёи<br> мусодира кардашуда аз<br> ҷумла: бесифат,<br>
            ва ғайра/ Кол-во изъятых<br> продуктов, сырья,<br> в т.ч. из-за<br> недоброкачественности,<br> порчи и др.
        </td>
        <td colspan="2" style="font-size: 11px;line-height: 1.2;text-align:center;width:140px">
            Миқдори маҳсулот, ашёи<br> иҷозатдодашуда<br> барои интиқоли<br> минбаъда/<br> Кол-во продуктов, сырья<br>
            разрешенных к<br> дальнейшему<br> следованию
        </td>
        <td rowspan="2" style="font-size: 11px;line-height: 1.2;text-align:center;width:135px">
            <br> <br> <br> <br> Имзои шахси<br> вазифадор, ки муоина<br> мегузаронад ва муҳр/<br> Подпись<br>
            должностного лица,<br> производившего осмотр<br> и печать.
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 1.2;text-align:center;">
            <br><br>ҷой(дона)/ мест<br>(штук)
        </td>
        <td style="font-size: 11px;line-height: 1.2;text-align:center;">
            <br> <br>вазн /<br> вес
        </td>
        <td style="font-size: 11px;line-height: 1.2;text-align:center;">
            <br> <br>ҷой(дона)/<br> мест<br> (штук)
        </td>
        <td style="font-size: 11px;line-height: 1.2;text-align:center;">
            <br><br>вазн /<br> вес
        </td>
        <td style="font-size: 11px;line-height: 1.2;text-align:center;">
            <br><br> ҷой(дона)/<br> мест<br> (штук)
        </td>
        <td style="font-size: 11px;line-height: 1.2;text-align:center;">
            <br><br>вазн /<br> вес
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 1.2;text-align:center;">
            1
        </td>
        <td style="font-size: 11px;line-height: 1.2;text-align:center;">
            2
        </td>
        <td style="font-size: 11px;line-height: 1.2;text-align:center;">
            3
        </td>
        <td style="font-size: 11px;line-height: 1.2;text-align:center;">
            4
        </td>
        <td style="font-size: 11px;line-height: 1.2;text-align:center;">
            5
        </td>
        <td style="font-size: 11px;line-height: 1.2;text-align:center;">
            6
        </td>
        <td style="font-size: 11px;line-height: 1.2;text-align:center;">
            7
        </td>
        <td style="font-size: 11px;line-height: 1.2;text-align:center;">
            8
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">{{$point16 ?? ''}}
        </td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">{{$point17 ?? ''}}
        </td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">{{$safID99." ".trans("swis.pdf.{$template}.new_key_6")}}
        </td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">{{$point19 ?? ''}}
        </td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">{{$safID146." ".trans("swis.pdf.{$template}.new_key_6")}}
        </td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">{{$point21}}
        </td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">{{$safID145." ".trans("swis.pdf.{$template}.new_key_6")}}
        </td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;"></td>
        <td style="font-size: 11px;line-height: 2.0;text-align:center;">
        </td>
    </tr>
</table>
<style>
    .fs11 {
        font-size: 11px;
    }
</style>