<?php

$day = $invoice_day;
$month = '&nbsp;'.month($invoice_month);
$year = $invoice_year;

$paymentsTable = "<tr>
    <td width='7%' class='fb tc'>".trans("swis.pdf.{$template}.number")."</td>
    <td width='73%' class='fb tc'>".trans("swis.pdf.{$template}.payment_purpose")."</td>
    <td width='20%' class='fb tc'>".trans("swis.pdf.{$template}.amount")." </td>
</tr>";

$paymentsTable .= "
<tr>
    <td width='7%' class='tc' style=' border-top: 0;  border-right: 0; padding-left: 4px;  vertical-align: top'>1.</td>
    <td width='73%' class='tc' style=' border-top: 0;  border-right: 0; padding-left: 4px;  vertical-align: top'>$payment_purpose</td>
    <td width='20%' class='tc' style=' border-top: 0;  padding-left: 4px; vertical-align: top'>$payment_amount</td>
</tr>
";

$inspector = $inspector_name;
?>

<table>
    <tr>
        <td class="title tl bold">
            {{trans("swis.pdf.{$template}.top.main.title")}}
        </td>
    </tr>
    <tr>
        <td style="height:20px"></td>
    </tr>
    <tr>
        <td>{{trans("swis.pdf.{$template}.address")}}: {{trans("swis.pdf.{$template}.address.info")}}</td>
    </tr>
    <tr>
        <td>{{trans("swis.pdf.{$template}.rc")}}: {{trans("swis.pdf.{$template}.rc.info")}}</td>
    </tr>
    <tr>
        <td>{{trans("swis.pdf.{$template}.kc")}}: {{trans("swis.pdf.{$template}.kc.info")}}</td>
    </tr>
    <tr>
        <td>{{trans("swis.pdf.{$template}.bik")}}: {{trans("swis.pdf.{$template}.bik.info")}}</td>
    </tr>
    <tr>
        <td>{{trans("swis.pdf.{$template}.ssn")}}: {{trans("swis.pdf.{$template}.ssn.info")}}</td>
    </tr>
    <tr>
        <td>{{trans("swis.pdf.{$template}.bank")}}: {{trans("swis.pdf.{$template}.bank.info")}}</td>
    </tr>
    <tr>
        <td style="height:20px"></td>
    </tr>
    <tr>
        <td class="tc">
            <table>
                <tr>
                    <td class="fs13 tc fb">{{trans("swis.pdf.{$template}.invoice")}}
                        {!! $tracking_number ?? '___'!!}  {{trans("swis.pdf.{$template}.from")}}
                        {!! $day ?? '___'!!}{!! $month ??'__________________'!!}
                        20{{($year).trans("swis.pdf.{$template}.year")}}
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="height:80px"></td>
    </tr>
    <tr>
        <td>
            <table class="bordered-table table-padding">
                {!! $paymentsTable !!}
            </table>
        </td>
    </tr>
    <tr>
        <td style="height:20px"></td>
    </tr>
    <tr>
        <td>&nbsp;{{trans("swis.pdf.{$template}.inspector")}}: {!! $inspector ?? '__________________'!!}</td>
    </tr>
</table>
<style>
    @page {
        margin-left: 15mm;
        margin-right: 15mm;
        line-height: 1.4;
    }

    .fb {
        font-weight: bold;
    }

    .tc {
        text-align: center;
    }

    .tl {
        text-align: left;
    }

    .tc {
        text-align: center;
    }

    .bold {
        font-weight: bold;
    }

    .title {
        font-size: 18px;
    }

    table {
        border-collapse: collapse;
        overflow: wrap;
        width: 100%;
    }

    .bordered-table td {
        border: 1px solid #000000;
    }

    .table-padding td {
        padding: 2mm;
    }

</style>