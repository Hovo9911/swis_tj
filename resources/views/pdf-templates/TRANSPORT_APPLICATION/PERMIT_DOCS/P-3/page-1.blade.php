<style>
    @page {
        margin-left: 24mm;
        margin-right: 23mm;
        margin-top: 6mm;
        margin-bottom: 6mm;
    }
    .fs12 { font-size: 12px; }
    .br1 {
        /*border-right: 1px solid #000000;*/
        /*     border-right: 1px solid #ffffff;*/
    }
    table {
        border-collapse: collapse;
        overflow: wrap;
        font-family: Helvetica, Arial, sans-serif;
        font-weight: bold;
    }
    .table-header td {
        padding: 18px;
        vertical-align: top;
        text-align: left;
    }
    .first-table,
    .second-table {
        border: none;
        width: 100%;
    }
    .second-table{
        margin-top: 50px;
    }
    .first-table td,
    .first-table th {
        box-sizing: border-box;
        vertical-align: top;
        text-align: left;
    }
    .table-header tr td,
    .first-table tr td {
        /*       border-top: 1px solid #000000;*/
    }
    .second-table td,
    .second-table th {
        /*        border: 1px solid #000000; */
        box-sizing: border-box;
        vertical-align: middle;
        text-align: left;
    }
    .first-table .vm_c,
    .second-table .vm_c{
        vertical-align: middle;
        text-align: center;
    }
    table tr td.bw {
        /*       border: 1px solid #000000 !important;*/
    }
    .pr0 {
        padding: 0;
    }
</style>
<div style="height: 92mm"></div>

<table class="first-table fs12" width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td height="14mm" width="58%"></td>
        <td height="14mm" width="42%" class="vm_c" style="padding-right: 2mm; text-align: right;">{{$refTransportCarrierCountry->name}}</td>
    </tr>
    <tr>
        <td height="15mm" width="58%"></td>
        <td height="15mm" width="42%" class="vm_c" style="padding-right: 2mm; text-align: right;">{{$refOriginCountry->name}}</td>
    </tr>
    <tr>
        <td height="16mm" width="58%"></td>
        <td height="16mm" width="42%" class="vm_c" style="padding-right: 2mm; text-align: right;">{{$refDestinationCountry->name}}</td>
    </tr>
    <tr>
        <td colspan="2" width="100%">
            <table>
                <tr>
                    <td height="11mm" width="71mm" style=""></td>
                    <td height="11mm" width="95mm" class="vm_c" style="padding-top: 5mm;  padding-right: 0; text-align: right;">{{$refTransportCarrierName}}<br/>{{$refTransportCarrierAddress}}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table width="100%">
                <tr>
                    <td width="67mm">
                        <table width="100%">
                            <tr>
                                <td  height="19mm" width="48mm"> </td>
                                <td  style="text-align: right;" height="19mm" width="37mm" class="bw vm_c">{{$periodValidity}}</td>
                            </tr>
                        </table>
                    </td>
                    <td width="53mm">
                        <table width="100%">
                            <tr>
                                <td  height="19mm" width="15mm"></td>
                                <td  style="text-align: left; padding-left: 0; padding-right: 0" height="19mm" width="38mm" class="bw vm_c">{{$periodValidityFrom}}</td>
                            </tr>
                        </table>
                    </td>
                    <td width="43mm" style="padding-right: 0;">
                        <table width="100%">
                            <tr>
                                <td  style="border-top: none"  height="19mm" width="16mm"> </td>
                                <td  style="text-align: left;" height="19mm" width="27mm" class="bw vm_c">{{$periodValidityTo}}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table width="100%">
                <tr>
                    <td height="13mm" width="67mm"></td>
                    <td height="13mm" width="53mm"></td>
                    <td height="13mm" width="43mm"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table>
                <tr>
                    <td height="13mm" width="67mm">
                        <table style="border: none !important">
                            <tr>
                                <td  style="border-top: none" height="13mm" width="47mm"></td>
                                <td  style="border-top: none; text-align: right" height="13mm" width="20mm" class="vm_c bw">{{$vehicleBrand}}</td>
                            </tr>
                        </table>
                    </td>
                    <td height="13mm" class="vm_c" width="58mm">{{$vehicleRegNumber}}</td>
                    <td height="13mm" class="vm_c" width="40mm">{{$trailerRegNumber}}</td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td height="15mm" width="58%"></td>
        <td height="15mm" width="42%" class="vm_c" style="padding-right: 2mm; text-align: right;">{{$refOriginCountry->name}}</td>
    </tr>
    <tr>
        <td colspan="2" width="100%">
            <table width="100%">
                <tr>
                    <td height="1mm" width="56mm"></td>
                    <td height="16mm" width="106mm" class="vm_c" style="padding-right: 2mm; text-align: right;">
                        {{$placeOfUnloading}}</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
