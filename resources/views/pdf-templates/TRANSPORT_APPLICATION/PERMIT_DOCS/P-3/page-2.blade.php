<style>
    table {
        border-collapse: collapse;
        overflow: wrap;
        font-family: Helvetica, Arial, sans-serif;
    }
    ​
</style>
<div style="height: 164mm"></div>
<table class="second-table fs12" >
    <tr>
        <td height="19mm" width="54%"> </td>
        <td height="19mm" class="vm_c" width="23%"> </td>
        <td height="19mm" class="vm_c" width="23%"> </td>
    </tr>
    <tr>
        <td height="39mm" width="54%"></td>
        <td height="39mm" class="vm_c" width="23%"></td>
        <td height="39mm" class="vm_c" width="23%"></td>
    </tr>
    <tr>
        <td height="23mm" width="54%"> </td>
        <td height="23mm" class="vm_c" width="23%">{{$natureGoods}}</td>
        <td height="23mm" class="vm_c" width="23%"></td>
    </tr>
    <tr>
        <td height="22mm" width="54%"></td>
        <td height="22mm" class="vm_c" width="23%">{{$entryGoodsWeight}}</td>
        <td height="22mm" class="vm_c" width="23%">{{$departureGoodsWeight}}</td>
    </tr>
</table>