<style>
    ​
    .vm_c {
        vertical-align: middle;
        text-align: center;
    }
    ​
</style>

<div style="height: 142mm"></div>
<table class="second-table fs12" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td height="18mm" width="54%"> </td>
        <td height="18mm" class="vm_c" width="23%"> </td>
        <td height="18mm" class="vm_c" width="23%"> </td>
    </tr>
    <tr>
        <td height="36mm" width="54%"></td>
        <td height="36mm" class="vm_c" width="23%"></td>
        <td height="36mm" class="vm_c" width="23%"></td>
    </tr>
    <tr>
        <td height="22mm" width="54%"> </td>
        <td height="22mm" class="vm_c" width="23%">{{$entryGoodsWeight}}</td>
        <td height="22mm" class="vm_c" width="23%">{{$departureGoodsWeight}}</td>
    </tr>
    <tr>
        <td height="23mm" width="54%"></td>
        <td height="23mm" class="vm_c" width="23%">{{$placeOfUnloading}}</td>
        <td height="23mm" class="vm_c" width="23%"></td>
    </tr>
</table>