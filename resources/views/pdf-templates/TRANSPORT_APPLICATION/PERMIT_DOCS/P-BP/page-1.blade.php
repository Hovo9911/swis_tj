<style>
    .fs12 { font-size: 12px; }
    table {
        border-collapse: collapse;
        overflow: wrap;
        font-family: Helvetica, Arial, sans-serif;
        font-weight: bold;
    }
    .table-header td {
        vertical-align: top;
        text-align: left;
    }
    .first-table,
    .second-table {
        border: none;
        width: 100%;
    }
    .first-table td,
    .first-table th {
        vertical-align: top;
        text-align: left;
    }
    .second-table td,
    .second-table th {
        box-sizing: border-box;
        vertical-align: middle;
        text-align: left;
    }
    .vm_c,
    .first-table .vm_c,
    .second-table .vm_c {
        vertical-align: middle;
        text-align: center;
    }
</style>
<div style="height: 94mm"></div>
<table class="first-table fs12" width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td width="41%"></td>
        <td colspan="2" width="59%">
            <table class="fs12" width="100%">
                <tr>
                    <td style="width: 60mm;" height="12mm"></td>
                    <td style="width: 33mm; text-align: right; padding-right: 3mm;" height="12mm" class="vm_c">{{$refOriginCountry->name}}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="3" height="16.5mm"></td>
    </tr>
    <tr>
        <td width="41%">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td  style="border-top: none" height="14mm" width="37mm"> </td>
                    <td  style="text-align: right;" height="14mm" width="30mm" class="vm_c">{{$periodValidity}}</td>
                </tr>
            </table>
        </td>
        <td width="34%">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td  style="border-top: none" height="14mm" width="20mm"></td>
                    <td  style="text-align: left;" height="14mm" width="41mm" class="vm_c">{{$periodValidityFrom}}</td>
                </tr>
            </table>
        </td>
        <td width="25%">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td height="14mm" width="11mm"> </td>
                    <td style="text-align: left;" height="14mm" width="29mm" class="vm_c">{{$periodValidityTo}}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td width="41%">
            <table width="100%">
                <tr>
                    <td height="15mm" width="37mm"></td>
                    <td style="text-align: right;" height="15mm" width="30mm" class="vm_c">{{$refBorderCrossing}}</td>
                </tr>
            </table>
        </td>
        <td width="34%">
            <table width="100%">
                <tr>
                    <td height="15mm" width="27mm"> </td>
                    <td style="text-align: left;" height="15mm" width="28mm" class="vm_c">{{$arrivalDate}}</td>
                </tr>
            </table>
        </td>
        <td width="25%">
            <table width="100%">
                <tr>
                    <td height="15mm" width="21mm"></td>
                    <td style="text-align: left;" height="15mm" width="19mm" class="vm_c">{{$departureDate}}</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table class="fs12" width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td height="13mm" width="45%">&nbsp;</td>
        <td height="13mm" width="30%" class="vm_c">{{$refTransportCarrierName}}</td>
        <td height="13mm" width="25%" class="vm_c">{{$refTransportCarrierAddress}}</td>
    </tr>
    <tr>
        <td height="13mm" colspan="3">&nbsp;</td>
    </tr>
    <tr>
        <td height="16mm" width="45%">
            <table width="100%" border="0">
                <tr>
                    <td height="16mm" width="50mm"> </td>
                    <td style="text-align: right; padding-right: 2mm" height="16mm" width="22mm" class="vm_c">{{$vehicleBrand}}</td>
                </tr>
            </table>
        </td>
        <td height="15mm" class="vm_c" width="30%">{{$vehicleRegNumber}}</td>
        <td height="15mm" class="vm_c" width="25%">{{$trailerRegNumber}}</td>
    </tr>
    <tr>
        <td height="24mm" width="45%"></td>
        <td height="24mm" class="vm_c" width="30%">{{$vehicleCarryingCapacity}}</td>
        <td height="24mm" class="vm_c" width="25%">{{$trailerCarryingCapacity}}</td>
    </tr>
    <tr>
        <td height="20mm" width="45%"></td>
        <td height="20mm" class="vm_c" width="30%">{{$vehicleEmptyCapacity}}</td>
        <td height="20mm" class="vm_c" width="25%">{{$trailerEmptyCapacity}}</td>
    </tr>
</table>