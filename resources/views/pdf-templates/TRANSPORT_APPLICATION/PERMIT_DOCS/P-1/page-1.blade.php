<style>
    @page {
        margin-left: 22mm;
        margin-right: 20mm;
        margin-top: 6mm;
        margin-bottom: 6mm;
    }

    .fs12 { font-size: 12px; }

    table {
        border-collapse: collapse;
        overflow: wrap;
        font-family: Helvetica, Arial, sans-serif;
        font-weight: bold;
    }

    .first-table,
    .second-table {
        border: none;
        width: 100%;
    }

    .second-table{
        margin-top: 50px;
    }

    .first-table td,
    .first-table th {
        box-sizing: border-box;
        padding: 2mm 0;
        vertical-align: top;
        text-align: left;
    }

    .second-table td,
    .second-table th {
        box-sizing: border-box;
        padding: 18px;
        vertical-align: middle;
        text-align: left;
    }

    .first-table .vm_c,
    .second-table .vm_c{
        vertical-align: middle;
        text-align: center;
    }
</style>

<div style="height: 77mm"></div>
<table class="first-table fs12" width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td class="br1" height="16mm" width="41%"></td>
        <td colspan="2" height="16mm" width="59%">
            <table class="fs12" width="100%">
                <tr>
                    <td  style="width: 65mm; border-top: none;" height="15mm"></td>
                    <td  style="width: 29mm; text-align: right; padding-right: 2mm;" height="15mm" class="bw vm_c">{{$refOriginCountry->name}}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="15mm" width="41%">
            <table width="100%">
                <tr>
                    <td  style="border-top: none" height="10mm" width="37mm"> </td>
                    <td  style="text-align: right; " height="10mm" width="30mm" class="bw vm_c">{{$periodValidity}}</td>
                </tr>
            </table>
        </td>
        <td height="15mm" width="34%">
            <table width="100%">
                <tr>
                    <td  style="border-top: none" height="10mm" width="25mm"></td>
                    <td  style="text-align: left; padding-left: 0; padding-right: 0" height="10mm" width="36mm" class="bw vm_c">{{$periodValidityFrom}}</td>
                </tr>
            </table>
        </td>
        <td height="15mm" width="25%" style="padding-right: 0">
            <table width="100%">
                <tr>
                    <td  style="border-top: none"  height="10mm" width="16mm"> </td>
                    <td  style="text-align: left;" height="10mm" width="24mm" class="bw vm_c">{{$periodValidityTo}}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="15mm" width="41%">
            <table width="100%">
                <tr>
                    <td  style="border-top: none" height="14mm" width="37mm"> </td>
                    <td  style="text-align: right; padding-right: 0; padding-left: 0" height="14mm" width="30mm" class="bw vm_c">{{$refBorderCrossing}}</td>
                </tr>
            </table>
        </td>
        <td height="15mm" width="34%">
            <table width="100%">
                <tr>
                    <td style="border-top: none" height="14mm" width="32mm"> </td>
                    <td style="text-align: left;" height="14mm" width="23mm" class="bw vm_c">{{$arrivalDate}}</td>
                </tr>
            </table>
        </td>
        <td height="15mm" width="25%" style="padding-right: 0">
            <table width="100%">
                <tr>
                    <td  style="border-top: none" height="14mm" width="22mm"></td>
                    <td  style="text-align: left; padding-right: 2mm;" height="14mm" width="16mm" class="bw vm_c">{{$departureDate}}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td  height="16mm" width="41%"></td>
        <td colspan="2" height="16mm" width="59%" class="vm_c" style="text-align: right; padding-right: 2mm;">{{$refTransportCarrierName}}<br/>{{$refTransportCarrierAddress}}</td>
    </tr>
    <tr>
        <td height="18mm" width="41%" class="br1"></td>
        <td height="18mm" width="31%" class="br1"></td>
        <td height="18mm" width="28%"></td>
    </tr>
    <tr>
        <td class="br1" width="42%">
            <table style="border: none !important">
                <tr>
                    <td  style="border-top: none" height="12mm" width="47mm"> </td>
                    <td  style="border-top: none; text-align: right" height="12mm" width="20mm" class="vm_c bw">{{$vehicleBrand}}</td>
                </tr>
            </table>
        </td>
        <td height="14mm" class="br1 vm_c" width="30%">{{$vehicleRegNumber}}</td>
        <td height="14mm" class="vm_c" width="28%">{{$trailerRegNumber}}</td>
    </tr>
    <tr>
        <td height="28mm" class="br1" width="42%"></td>
        <td height="28mm" class="br1 vm_c" width="30%">{{$vehicleCarryingCapacity}}</td>
        <td height="28mm" class="vm_c" width="28%">{{$trailerCarryingCapacity}}</td>
    </tr>
    <tr>
        <td height="25mm" class="br1" width="42%"></td>
        <td height="25mm" class="br1 vm_c" width="30%">{{$vehicleEmptyCapacity}}</td>
        <td height="25mm" class="vm_c" width="28%">{{$trailerEmptyCapacity}}</td>
    </tr>
</table>