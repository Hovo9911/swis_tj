
<style>

    @page {
        margin-left: 14mm;
        margin-right: 14mm;
        margin-top: 0mm;
        margin-bottom: 0mm;
    }

    .second-table tr td {
        /*border: 1px solid #000000 !important;*/
    }

    table {
        border-collapse: collapse;
        overflow: wrap;
        font-family: Helvetica, Arial, sans-serif;
        font-weight: bold;
    }

</style>

<div style="height: 108mm"></div>
<table class="second-table fs12" >
    <tr>
        <td height="19mm" width="54%"> </td>
        <td height="19mm" class="vm_c" width="23%"> </td>
        <td height="19mm" class="vm_c" width="23%"> </td>
    </tr>
    <tr>
        <td height="39mm" width="54%"></td>
        <td height="39mm" class="vm_c" width="23%"></td>
        <td height="39mm" class="vm_c" width="23%"></td>
    </tr>
    <tr>
        <td height="23mm" width="54%"> </td>
        <td height="23mm" class="vm_c" width="23%">{{$entryGoodsWeight}}</td>
        <td height="23mm" class="vm_c" width="23%">{{$departureGoodsWeight}}</td>
    </tr>
    <tr>
        <td height="22mm" width="54%"></td>
        <td height="22mm" class="vm_c" width="23%">{{$placeOfUnloading}}</td>
        <td height="22mm" class="vm_c" width="23%"></td>
    </tr>
</table>