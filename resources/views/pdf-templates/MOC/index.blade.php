<?php

use App\Models\ConstructorTemplates\ConstructorTemplates;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplication\SingleApplication;

//----- Get fields values by MDM_ID
$mdmFields = getFieldsValueByMDMID($documentId, $customData, ConstructorTemplates::LANGUAGE_CODE_TJ);

$approvedDateParse = date_parse_from_format(config('swis.date_time_format_front'), ($mdmFields[355] ?? '')) ?? '';

if (!empty($approvedDateParse)) {
    $fromDay = $approvedDateParse['day'] ?? '';
    $fromMonth = month($approvedDateParse['month']) ?? '';
    $fromYear = $approvedDateParse['year'] ?? '';
}

$safExporterName = $saf->data['saf']['sides']['export']['name'] ?? '';
switch ($saf->regime) {
    case SingleApplication::REGIME_IMPORT:
        $safSideName = $saf->data['saf']['sides']['import']['name'] ?? '';
        break;
    case SingleApplication::REGIME_EXPORT:
        $safSideName = $saf->data['saf']['sides']['export']['name'] ?? '';
        break;
    default:
        $safSideName = '';
        break;
}

$safSideApplicantName = $saf->data['saf']['sides']['applicant']['name'] ?? '';
$safTransportationImportCountry = $saf->data['saf']['transportation']['import_country'] ?? '';
$safTransportationImportCountry = !empty($safTransportationImportCountry) ? optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safTransportationImportCountry, false, [], 'get', false, false, [], false, ConstructorTemplates::LANGUAGE_CODE_RU))->name : '';

$productsInfo = '';
$number = 1;

foreach ($subAppProducts as $key => $subAppProduct) {
    $productsInfo .= ($number++) . '- ' . $subAppProduct['commercial_description'] . ', ';
}

if (mb_strlen($productsInfo, 'UTF-8') > 70) {
    $productsInfoThirdRow = mb_substr($productsInfo, 140, 70, 'UTF-8');
    $productsInfoSecondRow = mb_substr($productsInfo, 70, 70, 'UTF-8');
    $productsInfo = mb_substr($productsInfo, 0, 70, 'UTF-8');
}

//----- 5.

$subApplicationSubmittingDate = formattedDate(optional($subApplication)->status_date); //SAF_ID_139
$subApplicationSubmittingDate = $subApplicationSubmittingDate ? $subApplicationSubmittingDate . ', ' : '';

?>

<table>
    <tr>
        <td class="left bold fs13" style="width:300px">
            {{trans("swis.pdf.{$template}.key_1")}}<br>
            {{trans("swis.pdf.{$template}.key_2")}}<br>
            {{trans("swis.pdf.{$template}.key_3")}}<br>
            {{trans("swis.pdf.{$template}.key_4")}}

        </td>
        <td class="center bold fs13" style="width:300px">
            {{trans("swis.pdf.{$template}.key_5")}}<br>
            {{trans("swis.pdf.{$template}.key_6")}}<br>
            {{trans("swis.pdf.{$template}.key_7")}}
        </td>
    </tr>
</table>
<table width="100%" border="0">
    <tr>
        <td style="width: 100%" class="header_text">
            М А Ъ Л У М О Т Н О М А
        </td>
    </tr>
    <tr>
        <td style="width: 100%" class="header_text_bold ">
            С П Р А В К А № {{$mdmFields[365] ?? ''}}
        </td>
    </tr>
</table>
<br>
<br>
<table>
    <tr>
        <td style="width: 20px;"><span class="bold">От</span><br/>Аз
        </td>
        <td>
            <table>
                <tr>
                    <td style="width: 230px;">
                        <table>
                            <tr>
                                <td style="width: 10px;">“</td>
                                <td style="width: 30px;" class="underline center">{{$fromDay ?? ''}}</td>
                                <td style="width: 10px;">“</td>
                                <td style="width: 100px;" class="underline center">{{$fromMonth ?? ''}}</td>
                                <td style="width: 10px;"></td>
                                <td style="width: 40px;" class="underline">{{$fromYear ?? ''}}</td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 180px"></td>
                    <td style="width: 230px;">
                        <table>
                            <tr>
                                <td class="bold">г․ Душанбе</td>
                            </tr>
                            <tr>
                                <td>ш․ Душанбе</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="width:80px;"><span class="bold">Выдана</span></td>
        <td class="left underline">{{$safSideName}}</td>
    </tr>
    <tr>
        <td style="width:100px;">Дода шуд</td>
        <td class="left"></td>
    </tr>
    <tr>
        <td style="width:400px;" class="fs10 bold">(ном ва номи падар ё ин ки ташкилот)
        </td>
        <td style="width:250px;" class="fs10 left bold">
            (Ф.И.О. или название организации)
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="width:50px;"></td>
        <td style="width: 100px" class="left bold">В</td>
        <td style="width: 80px" class="left bold">том,</td>
        <td style="width: 80px" class="left bold">что</td>
        <td style="width: 150px" class="left bold">принадлежащие</td>
        <td style="width: 150px;" class="bold right">ему,</td>
    </tr>
    <tr>
        <td style="width:30px;"><span class="bold">(ей)</span></td>
        <td style="width:580px;" class="underline">{{$productsInfo ?? ''}}</td>
    </tr>
    <tr>
        <td style="width:610px;" class="underline">{{$productsInfoSecondRow ?? ''}}</td>
    </tr>
    <tr>
        <td style="width:610px;" class="underline">{{$productsInfoThirdRow ?? ''}}</td>
    </tr>
    <tr>
        <td style="width:110px;"><span class="bold">вывозимые  в</span></td>
        <td style="width:500px;" class="underline">{{$safTransportationImportCountry}}</td>
    </tr>
</table>
<table width="100%" border="0">
    <tr>
        <td class="bold">являются предметами культурного
            назначения ,на государственном учёте
            не состоят , не подпадают под действие Закона Республики Таджикистан
            «О вывозе и ввозе культурных ценностей»․
        </td>
    </tr>
    <tr>
        <td style="width:10%;"></td>
        <td style="width:90%;">Дар он хусус , ки ашёи муталиқи ӯ ашёи таъиноти фархангӣ буда, дар қайди
        </td>
    </tr>
    <tr>
        <td colspan="2">давлатӣ ворид нестанд ва ба доираи фаъолияти
            Қонуни Ҷумҳурии Тоҷикистан
            «Дар бораи дохил кардан ва берун баровардани сарватҳои фарҳангӣ» дохил намешаванд).
        </td>
    </tr>
</table>
<br>
<br>
<table width="100%" border="0">
    <tr>
        <td style="width:190px;">Асос։ дархост аз</td>
        <td style="width:350px" class="underline"></td>
    </tr>
    <tr>
        <td style="width:190px;" class="bold">Основание։ Заявление от</td>
        <td style="width:350px;" class="underline">{{$subApplicationSubmittingDate.$safSideName}}</td>
    </tr>
    <tr>
        <td style="width:190px;">Хулосаи экспертиза аз</td>
        <td style="width:350px" class="underline"></td>
    </tr>
    <tr>
        <td style="width:200px;" class="bold">Заключение экспертизы от</td>
        <td style="width:340px"
            class="underline">{{($mdmFields[365] ?? '').' '.trans("swis.pdf.{$template}.key_8").' '.($mdmFields[355] ?? '').', '.($mdmFields[398] ?? '')}}</td>
    </tr>
    <tr>
        <td style="width:120px">Номгӯи ашё дар</td>
        <td style="width:50px" class="underline"></td>
        <td style="width:350px"> варақ пешниҳод карда мешавад (намешавад)</td>
    </tr>
    <tr>
        <td style="width:150px" class="bold">Список Предмет на -</td>
        <td style="width:50px" class="underline">{{$mdmFields[449] ?? ''}}</td>
        <td style="width:10px"></td>
        <td style="width:150px" class="bold">листах прилагается</td>
    </tr>
    <tr>
        <td style="width:160px">Акси ашё дар шумораи</td>
        <td class="underline" style="width:80px"></td>
        <td style="width:300px"> адад замима мешавад (намешавад)</td>
    </tr>
    <tr>
        <td class="bold" style="width:160px">Фотографии Предмет</td>
        <td class="bold underline" style="width:80px">{{$mdmFields[465] ?? ''}}</td>
        <td class="bold" style="width:120px"> прилагаются</td>
    </tr>
</table>
<br>
<br>
<br>
<table>
    <tr>
        <td style="width:250px;" class="fs13 left">
            <table>
                <tr>
                    <td class="left bold">Министр культуры<br>Республики Таджикистан
                    </td>
                </tr>
                <tr>
                    <td class="fs10 left">(Вазифа, ном ва насаб , Должность․ Ф․И․О․ )
                    </td>
                </tr>
            </table>
        </td>
        <td style="width:150px;">
            {{$mdmFields[398] ?? ''}}
        </td>
        <td class="fs10 center" style="width: 40%;"><span style="color:white;">emptyText</span><br>
            -------------------------------------------------------<br>
            <span class="fs10">(Имзо , подпись)</span>
        </td>

    </tr>
</table>
<style>
    table > tr > td {
        font-size: 12px;
        line-height: 1.6;
    }

    .bold {

        font-weight: bold;
    }

    .right {
        text-align: right;
    }

    .header_text {
        font-size: 16px;
        line-height: 1.6;
        text-align: center;
    }

    .header_text_bold {
        font-size: 16px;
        line-height: 1.8;
        text-align: center;
        font-weight: bold;
    }

    .underline {
        border-bottom: 1px solid #000000
    }

    .center {
        text-align: center;
    }

    .left {
        text-align: left;
    }

    .fs10 {
        font-size: 10px;
    }

    .fs13 {
        font-size: 13px
    }
</style>
