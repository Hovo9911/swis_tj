<?php

use App\Models\Document\Document;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\RegionalOffice\RegionalOffice;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use Illuminate\Support\Facades\DB;

//----- Get fields values by MDM_ID
$mdmFields = getFieldsValueByMDMID($documentId, $customData);
list($mainDataBySafId, $productsDataBySafId, $productBatchesDataBySafId) = getSafDataFieldIdValue($saf, $subApplication, $subAppProducts, $availableProductsIds);

$subApplicationSafData = $subApplication->data;

$subdivisionId = $subApplication->agency_subdivision_id;
if (!is_null($subdivisionId)) {
    $subDivisionCode = optional(getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME, $subdivisionId, false))->code;

    $regionalOffice = RegionalOffice::select('regional_office_ml.address', 'phone_number', 'regional_office_ml.name')
        ->leftJoin('regional_office_ml', 'regional_office.id', '=', 'regional_office_ml.regional_office_id')
        ->where(['lng_id' => cLng('id'), 'office_code' => $subDivisionCode])
        ->active()
        ->first();

    if (!is_null($regionalOffice)) {

        $regionalOfficeAddress = $regionalOffice->address;
        $regionalOfficePhoneNumber = $regionalOffice->phone_number;

        $regionalOfficeInfo = $regionalOfficeAddress . ' ' . $regionalOfficePhoneNumber;
        $regionalOfficeName = $regionalOffice->name;
    }
}

//----- 1. ,2. ,3.
if (isset($mdmFields['355']) && !empty($mdmFields['355'])) {
    $dateFormat = strtr($mdmFields['355'], '/', '-');
    $time = strtotime($dateFormat);
    $point1 = date('d', $time);
    $point2 = month(date('m', $time));
    $point3 = date('y', $time);
}

//----- 4.
$point4 = getMappingValueInMultipleRows($mainDataBySafId['SAF_ID_14'] ?? '', 80, 80);

//----- 5. ,6.
$point5 = getMappingValueInMultipleRows($mdmFields[4] ?? '', 40, 40);

//----- 7.
$point7 = 0;
foreach ($subAppProducts as $subAppProduct) {
    $point7 += notEmptyForPdfFields($productsDataBySafId[$subAppProduct->id], 'SAF_ID_96');
}

//----- 8.
$safID14 = (isset($mainDataBySafId['SAF_ID_14']) && !empty($mainDataBySafId['SAF_ID_14'])) ? $mainDataBySafId['SAF_ID_14'] . ', ' : '';
$safID15 = (isset($mainDataBySafId['SAF_ID_15']) && !empty($mainDataBySafId['SAF_ID_15'])) ? $mainDataBySafId['SAF_ID_15'] . ', ' : '';
$safID16 = (isset($mainDataBySafId['SAF_ID_16']) && !empty($mainDataBySafId['SAF_ID_16'])) ? $mainDataBySafId['SAF_ID_16'] . ', ' : '';
$safID17 = (isset($mainDataBySafId['SAF_ID_17']) && !empty($mainDataBySafId['SAF_ID_17'])) ? $mainDataBySafId['SAF_ID_17'] : '';

$point8 = $safID14 . $safID15 . $safID16 . $safID17;
$point8 = getMappingValueInMultipleRows($point8, 80, 80);

//----- 9. ,10. ,11.
$point9 = getMappingValueInMultipleRows($mdmFields[437] ?? '', 85, 85);

//----- 12.
$point12 = getMappingValueInMultipleRows($mdmFields[431] ?? '', 45, 85);

//----- 13.
$point13 = getMappingValueInMultipleRows(($mdmFields[26] ?? '') . ' ' . ($mdmFields[44] ?? ''), 85, 40);

//----- 14.
$point14 = getMappingValueInMultipleRows($mdmFields[412] ?? '', 55, 85);

//----- 15.
$point15 = $mdmFields[503] ?? '';

if (!empty($point15)) {
    $point15 = str_replace("\r\n", "\n", $point15);
    $point15 = explode("\n", $point15);
}

//----- 16.
$point16 = $mdmFields[504] ?? '';

if (!empty($point16)) {
    $point16 = str_replace("\r\n", "\n", $point16);
    $point16 = explode("\n", $point16);
}

//----- 17.
$point17 = $mdmFields[505] ?? '';

if (!empty($point17)) {
    $point17 = str_replace("\r\n", "\n", $point17);
    $point17 = explode("\n", $point17);
}

//----- 25.
$safTransportationAppointment = $saf->data['saf']['transportation']['appointment'] ?? '';
$transportInfo = getReferenceRows(ReferenceTable::REFERENCE_CUSTOM_BODY_REFERENCE, $safTransportationAppointment, false, ['code', 'name', 'address']);//SAF_ID_73
if (!is_null($transportInfo)) {
    $point25 = '(' . optional($transportInfo)->code . ') ' . optional($transportInfo)->name . ', ' . optional($transportInfo)->address;
}

$point25 = getMappingValueInMultipleRows(($point25 ?? '') . ', ' . $mainDataBySafId['SAF_ID_6'], 50, 90);

//----- 15.
$transPortDocumentCodes = DB::table(ReferenceTable::REFERENCE_DOCUMENT_DEPENDING_TRANSPORT)->where('show_status', ReferenceTable::STATUS_ACTIVE)->pluck('document_code')->all();
$attachedDocumentProducts = SingleApplicationSubApplications::getAttachedDocumentProductsForPDF($subApplication, $availableProductsIds);
foreach ($attachedDocumentProducts as $value) {
    if (in_array($value->document->document_type, $transPortDocumentCodes)) {
        $point26 = $value->document->document_number;
        $releaseDate = date_parse_from_format(config('swis.date_time_format_front'), $value->document->document_release_date) ?? '';

        $point27 = !empty($releaseDate) ? $releaseDate['day'] : '';
        $point28 = !empty($releaseDate) ? month($releaseDate['month']) : '';
        $point29 = !empty($releaseDate) ? substr($releaseDate['year'], 2, 2) : '';
        break;
    }
}

//-----31.
$transportType = $subApplicationSafData['saf']['transportation']['type_of_transport'] ?? '';//SAF_ID_75
$transportType = $transportType ? optional(getReferenceRows(ReferenceTable::REFERENCE_TRANSPORT_TYPE, $transportType))->name : '';

$safTransportationStateNumberCustomSupports = $subApplicationSafData['saf']['transportation']['state_number_customs_support'] ?? '';
$stateNumbersCustomsSupport = [];
foreach ($safTransportationStateNumberCustomSupports as $safTransportationStateNumberCustomSupport) {
    if (!is_null($safTransportationStateNumberCustomSupport)) {
        $stateNumbersCustomsSupport[] = $safTransportationStateNumberCustomSupport['state_number'] . ' ' . $safTransportationStateNumberCustomSupport['customs_support'];
    }
}

$stateNumbersCustomsSupportList = count($stateNumbersCustomsSupport) ? implode(', ', $stateNumbersCustomsSupport) : '';

$point31 = getMappingValueInMultipleRows((($transportType . ' - ' . $stateNumbersCustomsSupportList) ?? ''), 80, 120);

//----- 32.
$safTransportationExportCountryID = $subApplicationSafData['saf']['transportation']['export_country'] ?? '';
$safID78 = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safTransportationExportCountryID, false))->name;

$transitCountries = $subApplicationSafData['saf']['transportation']['transit_country'] ?? '';

foreach ($transitCountries as $transitCountry) {
    if (!is_null($transitCountry)) {
        $transCountries[] = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $transitCountry))->name;
    }
}

if (!empty($transCountries)) {
    $transCountries = array_unique($transCountries);
    $transCountriesList = implode(', ', $transCountries);
}

$safTransportationImportCountryID = $subApplicationSafData['saf']['transportation']['import_country'] ?? '';
$safID80 = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safTransportationImportCountryID, false))->name;

$point32 = getMappingValueInMultipleRows(($safID78 . ' - ' . ($transCountriesList ?? '') . ' - ' . $safID80), 65, 85);

//----- 33.
$point33 = getMappingValueInMultipleRows($mdmFields[199] ?? '', 85, 85);

switch ($saf->regime) {
    case SingleApplication::REGIME_IMPORT:
        $attachedDocumentProducts = SingleApplicationSubApplications::getAttachedDocumentProductsForPDF($subApplication, $availableProductsIds);

        foreach ($attachedDocumentProducts as $value) {
            if ($value->document->document_type == Document::DOCUMENT_CODE_6012) {
                $documents6012[] = $value->document->document_number . ' ' . $value->document->document_release_date;
            }
        }

        $documentInfo = isset($documents6012) ? implode(', ', ($documents6012 ?? '')) : '';
        break;
    default:
        $documentInfo = '';
        break;
}

?>
<table cellspacing="0" cellpadding="1" border="0">
    <tr>
        <td style="height:50px"></td>
    </tr>
</table>
<table>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:66%;text-align:left;font-size: 11px;">{{optional($agency)->name}}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;width:66%;text-align:center;line-height: 1.6;">
            <span>Субъект, Ҷумҳурии Тоҷикистон / субъект, Республика Таджикистан </span>
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:66%;text-align:left;font-size: 11px;">{{$regionalOfficeInfo}}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;width:66%;text-align:center;line-height: 1.6;">
            <span>(ноҳия, шаҳр / район, город) </span>
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:66%;text-align:left;font-size: 11px;">{{$regionalOfficeName}}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;width:66%;text-align:center;line-height: 1.6;">
            <span>(номи муассиса / наименование учреждения) </span>
        </td>
    </tr>
</table>
<table cellspacing="0" cellpadding="1" border="0">
    <tr>
        <td style="height:140px"></td>
    </tr>
</table>
<table style="text-align: justify">
    <tr>
        <td style="font-size: 11px;width:360px;font-weight:bold;"></td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:40px;">
            «{{$point1 ?? ''}}»
        </td>
        <td style="font-size: 11px;width:5px;font-weight:bold;"></td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:130px;">{{$point2 ?? ''}}</td>
        <td style="font-size: 11px;width:35px;">20<u>{{$point3 ??''}}</u></td>
        <td style="font-size: 11px;width:75px;"> сол/год</td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:620px;line-height: 1.4;">Ман, духтури байтории дар поён имзокарда, шаҳодатномаи
            байтории мазкурро ба/Я нижеподписавшийся ветеринарный врач, выдал настоящее ветеринарное свидетельство
        </td>
    </tr>
    @for($i=0;$i<count($point4);$i++)
        <tr>
            <td style="border-bottom: 1px solid #000000;width:620px;font-size: 11px;">{{$point4[$i]}}</td>
        </tr>
    @endfor
    <tr>
        <td style="font-size: 8px;width:620px;text-align:center;line-height: 1.4;">
            <span>(ба кӣ, номи шахси юридикӣ ё ному насаби шахси воқеӣ/кому, наименования юридических лиц или Ф.И.О. физических лиц)</span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:620px;line-height: 1.4;">дар бобати он, ки дар вақти аз назар гузаронидани
            хайвоноти фиристодашаванда/ в том, что при
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:270px;">ветеринарном осмотре подлежащих отправке животных</td>
        <td style="border-bottom: 1px solid #000000;font-size: 11px;width:350px;">{{$point5[0] ?? ''}}</td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 1.4;text-align:left;width:50%;line-height: 1.4;"></td>
        <td style="font-size: 8px;text-align:right;line-height: 1.4;width:50%;">
            <span>(намуди хайвонот нишон дода мешавад/указать вид животного)</span>
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;font-size: 11px;width:290px;line-height: 1.4;">{{$point5[1] ?? ''}}</td>
        <td style="font-size: 11px;width:130px;line-height: 1.4;">ба микдори/в количестве</td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:40px;line-height: 1.4;">{{$point7}}</td>
        <td style="font-size: 11px;width:160px;text-align:right;line-height: 1.4;">сар (ҷой, дона) ҳайвони касал ва</td>
    </tr>
    <tr>
        <td style="width:620px;line-height:0.5;"></td>
    </tr>
    <tr>
        <td style="text-align:justify;font-size: 11px;width:620px;line-height: 1.4;">нисбати касалиҳои сироятӣ шӯбҳанок
            пайдо нашуд ва онҳо аз/голов (мест, штук) больных и подозрительных по заболеванию
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:620px;line-height: 1.4;">заразными болезнями не
            обнаружено и они выходят (вывозятся) из
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:620px;font-size: 11px;">{{$point8[0]}}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;width:620px;text-align:center;line-height: 1.4;">
            <span>(номи ташкилоти ирсолкунанда, суроғаи пурра, аз он чумла номи/указать наименование организации отправителя, полный адрес, в т.ч. название </span>
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:620px;font-size: 11px;">{{$point8[1] ??''}}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;width:620px;text-align:center;line-height: 1.4;">
            <span>маҳалли аҳолинишин, кӯча ва рақами бино/населённого пункта, улицы и номера дома)</span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:620px;line-height: 1.4;">минтақаи наисбати касалиҳои хавфнок ва карантинии
            ҳайвонот мусоид бароварда мешаванд, додам/благополучного по особо опасным и карантинным болезням жовотных.
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:620px;line-height: 1.4;">
            Дар вақти содироти ҳайвонот мусоидии хоҷагӣ ва маҳал мувофиқи талаботи мамлакати воридкунанда ва мӯҳлати
            мусоидии онҳо (моҳ, сол) нишон дода мешавад./При отпрвке на экспорт указывают благополучие хозяйства и
            местности согласно требованиям страны-импортёра и срок их благопoлучия (месяц, лет)
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:620px;font-size: 11px;line-height:1.6;">{{$point9[0] ?? ''}}</td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:620px;font-size: 11px;line-height:1.6;">{{$point9[1] ?? ''}} </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:620px;font-size: 11px;line-height:1.6;">{{$point9[2] ?? ''}}</td>
    </tr>
    <tr>
        <td style="width:620px;line-height:1.0;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:620px;line-height: 1.4;">Ҳайвонҳо дар Ҷумҳурии Тоҷикистон ҷойгир буданд: аз
            таърихи таваллуд, на камтар аз 6 моҳ (бо зерхат нишон дода шавад) ё/Животные находились в Республике
            Таджикистан: с рождения, не менее 6
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:180px">месяцев (нужное подчеркнуть) или</td>
        <td style="border-bottom: 1px solid #000000;font-size: 11px;width:440px">{{$point12[0]}}</td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:620px;line-height: 1.4;">Ҳайвонот пеш аз ирсолшавӣ дар нуқтаи
            карантинӣ/Животные
            перед отправкой карантировались
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:620px;font-size: 11px;">{{$point13[0]}}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;width:620px;text-align:center;line-height: 1.4;">
            <span>(маҳалли карантин ва микдори шабонарӯз/место карантирования и количество суток</span>
        </td>
    </tr>
    <tr>
        <td style="width:620px;line-height:0.5;"></td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;font-size: 11px;width:35%;">{{$point13[1] ?? ''}}</td>
        <td style="font-size: 11px;text-align:left;width:65%;"> нигоҳ дошта шудаанд.</td>
    </tr>
    <tr>
        <td style="width:620px;line-height:0.5;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:620px;line-height: 1.4;">
            Дар давоми гузаронидани карантин ҳайвонот бо дигар ҳайвонот алоқа надоштанд; аз санҷиши ҳаррӯзаи клиникӣ
            гузаштанд, ҳарорати баданашон чен карда шуд, дар рӯзи додани сертификат ташхис карда шудаанд ва аломатҳои
            клиникии касалиҳои сироятӣ надоштанд/В период карантирования животных не имели контакта с другими животными;
            ежедневно клинически осматривались и у них измерялась температура тела; в день выдачи свидетельство
            обследованы, больных и подозрительных в заболевании не выявлено.
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:620px;line-height: 1.4;">
            Дар давоми карантин маводи ташхисӣ дар озмоишгоҳи/ В период карантирования диагностические
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:220px">материалы были исследованы в лаборатории</td>
        <td style="border-bottom: 1px solid #000000;font-size: 11px;width:400px">{{$point14[0]}}</td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 1.4;width:45%;line-height: 1.4;"></td>
        <td style="font-size: 8px;text-align:center;line-height: 1.4;width:55%;">
            <span>(номи озмоишгоҳ нишон дода шавад/указать наименование лаборатории)</span>
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;font-size: 11px;width:620px;line-height: 1.4;">{{$point14[1] ??''}}</td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:620px;line-height: 1.4;">аз санҷиш гузаронида шуд ва натиҷаҳои зерин ба даст
            оварда шуданд/ и получены следующие результаты:
        </td>
    </tr>
</table>
<table pagebreak="true">
    <tr>
        <td style="font-size: 11px;width:620px;line-height: 1.0;">{{trans("swis.pdf.{$template}.new_key_1")}}
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:620px;text-align:left;font-size: 11px;line-height: 1.2">{{$point15[0] ??''}}
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:620px;text-align:left;font-size: 11px;line-height: 1.2">{{$point15[1] ??''}}
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:620px;text-align:left;font-size: 11px;line-height: 1.2">{{$point15[2] ??''}}
        </td>
    </tr>
    <tr>
        <td style="width:620px;line-height:0.5;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:620px;line-height: 1.2;">{{trans("swis.pdf.{$template}.new_key_2")}}
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:left;width: 100%;border-bottom: 1px solid #000000;line-height: 1.2;">{{$point16[0] ?? ''}}</td>
    </tr>
    <tr style="line-height: 0.5">
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:left;width: 100%;border-bottom: 1px solid #000000;line-height: 1.2;">{{$point16[1] ?? ''}}</td>
    </tr>
    <tr style="line-height: 0.5">
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:left;width: 100%;border-bottom: 1px solid #000000;line-height: 1.2;">{{$point16[2] ?? ''}}</td>
    </tr>
    <tr style="line-height: 0.5">
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:left;width: 100%;border-bottom: 1px solid #000000;line-height: 1.2;">{{$point16[3] ?? ''}}</td>
    </tr>
    <tr style="line-height: 0.5">
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;width: 80%;border-bottom: 1px solid #000000;line-height: 1.2;"></td>
        <td style="font-size: 11px;text-align:center;width:20%;line-height: 1.2;">.{{trans("swis.pdf.{$template}.new_key_4")}}</td>
    </tr>
    <tr>
        <td style="width:620px;line-height:0.5;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:620px;line-height: 1.2;">{{trans("swis.pdf.{$template}.new_key_3")}}
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:left;width: 100%;border-bottom: 1px solid #000000;line-height: 1.2;">{{$point17[0] ?? ''}}</td>
    </tr>
    <tr style="line-height: 0.5">
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:left;width: 100%;border-bottom: 1px solid #000000;line-height: 1.2;">{{$point17[1] ?? ''}}</td>
    </tr>
    <tr style="line-height: 0.5">
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:left;width: 60%;border-bottom: 1px solid #000000;line-height: 1.2;"></td>
        <td style="font-size: 11px;text-align:center;width:20%;">{{trans("swis.pdf.{$template}.new_key_5")}}</td>
    </tr>
    <tr>
        <td style="width:620px;line-height:0.5;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:620px;line-height: 1.2;">
            Маводи бастабандӣ ва масолеҳи ҳамроҳӣ бевосита аз хоҷагии содиркунанда бароварда шуда, бо чорвои касал ё
            масолеҳ ва маводи сирояткунанда алоқа надоштанд/Упаковочный материал и сопровождающие грузы проиходят
            непосредственно из хозяйства-поставщика и не контаминированы возбудителями инфекционных болезней.
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;;width:280px;">Ҳайвонот фиристода мешавад ба/Животные направляются</td>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:340px;">{{$point25[0]}}</td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 1.2;text-align:left;width:50%;"></td>
        <td style="font-size: 8px;text-align:center;line-height: 1.2;width:50%;">
            <span>(ҷои таъиншуда ва кабулкунанда/пункт назначения и получатель)</span>
        </td>
    </tr>
    @if(count($point25)>1)
        @for($i=1;$i<count($point25);$i++)
            <tr>
                <td style="border-bottom: 1px solid #000000;width:620px;text-align:left;font-size: 11px;line-height: 1.4">{{$point25[$i]}}</td>
            </tr>
        @endfor
    @endif
    <tr style="line-height: 0.5">
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:620px;line-height: 1.2;">Бо сертификати (ҳуҷҷати яклухт, борхат)/при
            серификации
            (гуртовой ведомости, накладной)
        </td>
    </tr>
    <tr>
        <td style="text-align:center;font-size: 11px;width:15px;line-height: 1.2;">№</td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:85px;line-height: 1.2;">{{$point26 ?? ''}}</td>
        <td style="font-size: 11px;width:30px;text-align:right;line-height: 1.2;">aз/от</td>
        <td style="border-bottom: 1px solid #000000;font-size: 11px;text-align:center;width:40px;">«{{$point27 ?? ''}}
            »
        </td>
        <td style="font-size: 11px;text-align:center;width:5px;">&nbsp;</td>
        <td style="font-size: 11px;text-align:center;width:80px;border-bottom: 1px solid #000000;">{{$point28 ?? ''}}</td>
        <td style="font-size: 11px;text-align:right;width:50px;">соли 20</td>
        <td style="font-size: 11px;text-align:left;width:15px;border-bottom: 1px solid #000000;">{{$point29 ?? ''}}</td>
        <td style="font-size: 11px;text-align:left;width:20px;">г.</td>
        <td style="font-size: 11px;width:50px;text-align:right;line-height: 1.2;"> барои/для</td>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:233px;line-height: 1.2;">
            &nbsp;{{($mdmFields[198] ?? '')}}</td>
    </tr>
    <tr>
        <td style="width:225px"></td>
        <td style="font-size: 8px;text-align:right;line-height: 1.2;width:400px;">
            <span>(фарбеҳкунонӣ, афзоиш, фурӯш, кушгор ва ғ./откорма, разведения, продажа, убоя и т.д. )</span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;;width:100px;">бо воситаи/и следует</td>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:520px;">{{$point31[0]}}</td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 1.2;text-align:left;width:10%;"></td>
        <td style="font-size: 8px;text-align:center;line-height: 1.2;width:90%;">
            <span>(наклиёти рохи охан, обӣ, автомобил__, хавой, автомобил/ж/д. водным, автомобильным, воздушным транспортом, № автомобиля</span>
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:620px;">{{$point31[1] ?? ''}}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;line-height: 1.2;width:620px;">
            <span>№ катора,номи киштӣ. № сафар ва ғ./ № вагона, название судна, № рейса и т.д.)</span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;;width:125px;">аз рӯи самти/по маршуту</td>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:495px;">{{$point32[0]}}</td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 1.2;text-align:left;width:10%;"></td>
        <td style="font-size: 8px;text-align:center;line-height: 1.2;width:90%;">
            <span>(нуқтаи асосии таъиншуда ё стансия ва роҳи боркунӣ ва борфурорӣ/указать основные пункты следования) </span>
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:620px;">{{$point32[1] ?? ''}}</td>
    </tr>
    <tr>
        <td style="width:620px;line-height:0.5;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;text-align:left;line-height: 1.2;width:620px;">Намудҳои нақлиёт тоза ва безарар карда
            шудаанд/Транспорные средства очищены и продензифицированы.
        </td>
    </tr>
    <tr>
        <td style="width:620px;line-height:0.5;"></td>
    </tr>
    <tr>
        <td style="font-size: 13px;text-align:left;line-height: 1.2;width:620px;">ИШОРАҲОИ МАХСУС/ОСОБЫЕ ОТМЕТКИ:
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:620px;">{{$point33[0]}}</td>
    </tr>
    <tr>
        <td style="font-size: 7.8px;text-align:left;line-height: 1.2;width:620px;">Дар вақти фиристодани ҳайвоноти ба
            касалиҳои хавфнок гитифторшуда/Заполняется при отправке животных, переболевших особо опасными заболева-
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:620px;">{{$point33[1] ?? ''}}</td>
    </tr>
    <tr>
        <td style="font-size: 7.6px;text-align:left;line-height: 1.2;width:620px;">мавҷуд будани шароитҳои махсус ва аз
            рӯи иҷозати махсус пур карда мешавад/ниями, перевозке на особых условиях и по спец-му разрешению (указанию)
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:620px;">{{$documentInfo ?? ''}}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;line-height: 1.2;width:620px;">(аз тарафи кӣ дода шудааст ва таърихи
            он/кем выдано, номер и дата)
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:620px;">{{($mdmFields[464] ?? '')}}</td>
    </tr>
    <tr>
        <td style="font-size: 7.1px;text-align:left;line-height: 1.2;width:620px;">(кайди мақоми ваколатдори Кумита оиди
            муоина ҳангоми боркунӣ ва борфурорӣ/отметка уполномоченного органа Комитета об осмотре при погрузке,
            выгрузке)
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:620px;">{{($mdmFields[502] ?? '')}}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;line-height: 1.2;width:620px;">(вақти ҳаракат дар роҳ/время в пути
            следования)
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:620px;"></td>
    </tr>
    <tr>
        <td style="width:620px;line-height:0.5;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:620px;line-height: 1.2;">
            Шаҳодатнома дар вақти назорати боркунӣ дар давоми роҳ пешниҳод карда ба гирандаи бор дода мешавад. Нусхаҳои
            шаҳодатнома эътибор надоранд. Дар ҳолати муайян шудани вайронкунии шароити пур кардани варақа, шаҳодатнома
            эътибор надоранд. Дар ҳолати муайян шудани вайронкунии шарити пут кардани варақа, шаҳодатнома ба сарнозири
            давлатии байтории субъекти Ҷумҳурии Тоҷикистон аз рӯи маҳали содироти бор бо нишондоди коидавайронкунӣ
            супорида мешавад/Свидетельство предъявляется для контроля при погрузке, в пути следования и передаётся
            грузополучателю. Копии свидетельства недействительны. При установлении нарушений порядка заполнения бланка
            свидетельство передаётся главному госветинспектору субъекта Республики Таджикистан по месту выдачи с
            указанием выявленных нарушений.
        </td>
    </tr>
    <tr>
        <td style="width:50%"></td>
        <td style="font-size: 12px;font-weight:bold;width:50%;line-height: 1.2;">
            Духтури байторӣ<br>
            Ветеринарный врач
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:200px;line-height: 1.2;">
            <table>
                <tr>
                    <td style="font-size: 11px;line-height: 1.2;text-align:left;font-weight:bold;">
                        Ҷ.М.
                    </td>
                </tr>
            </table>
        </td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:420px">{{($mdmFields[420] ?? '')}}</td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.2;text-align:left;"></td>
        <td style="font-size: 9px;text-align:center;line-height: 1.2;width:400px">
            <span>(имзо, номи пурраи вазифа/подпись, полное наименование должности)</span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:200px;line-height: 1.2;">
            <table>
                <tr>
                    <td style="font-size: 11px;line-height: 1.2;text-align:left;font-weight:bold;">
                        М.П.
                    </td>
                </tr>
            </table>
        </td>
        <td style="border-bottom: 1px solid #000000;text-align:center;font-size: 11px;width:420px">{{($mdmFields[382] ?? '')}}</td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.2;text-align:left;"></td>
        <td style="font-size: 9px;text-align:center;line-height: 1.2;width:400px">
            <span>(ному насаб/ ф.и.о.)</span>
        </td>
    </tr>

</table>