<?php
use App\Http\Controllers\Core\Dictionary\DictionaryManager;
use App\Models\Agency\Agency;
use App\Models\ConstructorTemplates\ConstructorTemplates;
use App\Models\SingleApplication\SingleApplication;


$dictionaryManager = new DictionaryManager();

$languageCode = ConstructorTemplates::LANGUAGE_CODE_TJ;
$mdmIdNames = getFieldsValueByMDMID($documentId, $customData, $languageCode, 'name');

$safGeneralRegime = $saf->data['saf']['general']['regime'] ?? ''; // SAF_ID_1

// ----- 9.

switch ($safGeneralRegime) {
    case SingleApplication::REGIME_IMPORT:

        // ----- 9.
        $safSideName = $saf->data['saf']['sides']['import']['name']; //SAF_ID_6


        break;
    case SingleApplication::REGIME_EXPORT:

        // ----- 9.
        $safSideName = $saf->data['saf']['sides']['export']['name']; //SAF_ID_14

        break;
    default:

        // ----- 9.
        $safSideName = '';

        break;
}


// ----- 1.
$key1_1 = $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.key_1_1", $languageCode);

$mdmId355 = $mdmIdNames['355'] ?? ''; // MDM_ID_355
$mdmId355_tj = '';
if (!empty($mdmId355)) {
    $dateArray = explode('/', $mdmId355);

    $fromMonth = month($dateArray[1], ConstructorTemplates::LANGUAGE_CODE_TJ);

    $mdmId355_tj = '&laquo;' . $dateArray[0] . '&raquo;' . ' ' . $fromMonth . ' ' . $dateArray[2];
}

$key1_3 = $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.key_1_3", $languageCode);
$infoPage = $mdmId355_tj . '. ' . $key1_3;

// ----- 2.
$keyPhone = $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.phone", $languageCode);
$safGeneralSubApplicationAgencySubdivisionId = $saf->data['saf']['general']['sub_application_agency_subdivision_id'] ?? ''; // SAF_ID_142
$infoPhone = $keyPhone . ' ' . $safGeneralSubApplicationAgencySubdivisionId;

// ----- 3.
$mdmId399 = $mdmIdNames['399'] ?? ''; // MDM_ID_399

// ----- 4.
$key4_1 = $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.key_4_1", $languageCode);
$mdmId365 = $mdmIdNames['365'] ?? ''; // MDM_ID_365
$key4_3 = $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.key_4_3", $languageCode);
$title = $key4_1 . ' ' . $mdmId365 . ' ' . $key4_3 . ' ' . ( !empty($mdmId355) ? str_replace("/", ".", $mdmId355) : '' );

// ----- 5.
$key_5 = $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.key_5", $languageCode);
$safGeneralSubApplicationStatusDate = $subApplication['status_date'] ?? ''; // SAF_ID_139
$safGeneralSubApplicationStatusDateInfo = $key_5 . ' ' . (!empty($safGeneralSubApplicationStatusDate) ? \Carbon\Carbon::parse($safGeneralSubApplicationStatusDate)->format('d.m.Y') : '');

// ----- 6.
$key6 = $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.key_6", $languageCode);
$key6AndMdmId355 = $key6 . ' ' . ( !empty($mdmId355) ? str_replace("/", ".", $mdmId355) : '' );

// ----- 7.
$agencyName = Agency::select('agency_ml.name as name')
    ->join('agency_ml', function ($query) {
        $query->on('agency.id', '=', 'agency_ml.agency_id')->where('lng_id', ConstructorTemplates::LANGUAGE_CODE_TJ);
    })
    ->where('agency.id', $agency->id)
    ->active()
    ->pluck('name')
    ->first();

// ----- 8.
$mdmId454 = $mdmIdNames['454'] ?? ''; // ----- 14., MDM_ID_454
$mdmId382 = $mdmIdNames['382'] ?? ''; // ----- 15., MDM_ID_382
$mdmId454_382 = $mdmId454 . ' ' . $mdmId382;

// ----- 10.
$mdmId455 = $mdmIdNames['455'] ?? ''; // MDM_ID_455

// ----- 11.
$key11 = $dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.key_11", $languageCode);

// ----- 12.
$mdmId456 = $mdmIdNames['456'] ?? ''; // MDM_ID_456

// ----- 13.
$mdmId457 = $mdmIdNames['457'] ?? ''; // MDM_ID_457



?>


<style type="text/css">

    table > tr > td {
        font-size: 12px;
        text-align: left;
        line-height: 1;
    }

    .bold {
        font-weight: bold;
    }

    .tc {
        text-align: center;
    }

    .tl {
        text-align: left;
    }

    .tr {
        text-align: right;
    }

</style>


<table cellspacing="0" cellpadding="1" border="0">
    <tr>
        <td colspan="6">{{$key1_1}}</td>
        <td colspan="6" class="tr">{{$infoPhone}}</td>
    </tr>
    <tr>
        <td colspan="6">{{$infoPage}}</td>
        <td colspan="6" class="tr">{{$mdmId399}}</td>
    </tr>


    <tr>
        <td colspan="12"></td>
    </tr>
    <tr>
        <td colspan="12" class="bold tc">{{$title}}</td>
    </tr>


    <tr>
        <td colspan="12"></td>
    </tr>
    <tr>
        <td colspan="12">{{$safGeneralSubApplicationStatusDateInfo}}</td>
    </tr>


    <tr>
        <td colspan="12"></td>
    </tr>
    <tr>
        <td colspan="12">{{$key6AndMdmId355}}</td>
    </tr>


    <tr>
        <td colspan="12"></td>
    </tr>
    <tr>
        <td colspan="12"><span>{{$dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.key_7", $languageCode)}}</span> <u>{{$agencyName}}</u></td>
    </tr>


    <tr>
        <td colspan="12"></td>
    </tr>
    <tr>
        <td colspan="12"><span>{{$dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.key_8", $languageCode)}}</span> <u>{{$mdmId454_382}}</u></td>
    </tr>


    <tr>
        <td colspan="12"></td>
    </tr>
    <tr>
        <td colspan="12"><span>{{$dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.key_9", $languageCode)}}</span> <u>{{$safSideName}}</u></td>
    </tr>


    <tr>
        <td colspan="12"></td>
    </tr>
    <tr>
        <td colspan="12"><span>{{$dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.key_10", $languageCode)}}</span> <u>{{$mdmId455}}</u></td>
    </tr>


    <tr>
        <td colspan="12"></td>
    </tr>
    <tr>
        <td colspan="12"><span>{{$dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.key_11.label", $languageCode)}}</span> <u>{{$key11}}</u></td>
    </tr>


    <tr>
        <td colspan="12"></td>
    </tr>
    <tr>
        <td colspan="12" class="bold tc">{{$dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.title_2", $languageCode)}}</td>
    </tr>


    <tr>
        <td colspan="12"></td>
    </tr>
    <tr>
        <td colspan="12">{{$mdmId456}}</td>
    </tr>


    <tr>
        <td colspan="12"></td>
    </tr>
    <tr>
        <td colspan="12" class="bold tc">{{$dictionaryManager->getTransByLngCode("swis.pdf_templates.{$template}.title_3", $languageCode)}}</td>
    </tr>


    <tr>
        <td colspan="12"></td>
    </tr>
    <tr>
        <td colspan="12">{{$mdmId457}}</td>
    </tr>

    <tr>
        <td colspan="12"></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="5" class="tl">{{$mdmId454}}</td>
        <td colspan="5" class="tr">{{$mdmId382}}</td>
        <td></td>
    </tr>

</table>