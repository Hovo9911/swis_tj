<?php

use App\Models\ReferenceTable\ReferenceTable;

$newProductRow = $newTransitCountriesRow = false;
$safSideImporterName = $saf->data['saf']['sides']['import']['name'] ?? '';
$safSideImporterAddress = $saf->data['saf']['sides']['import']['address'] ?? '';

$safSideImporterName = getMappingValueInMultipleRows($safSideImporterName . ' ' . $safSideImporterAddress, 60, 90);

$safTransportationExportCountry = $saf->data['saf']['transportation']['export_country'] ?? '';
$safTransportationImportCountry = $saf->data['saf']['transportation']['import_country'] ?? '';

if (!empty($safTransportationExportCountry)) {
    $safTransportationExportCountry = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safTransportationExportCountry))->name;
}
if (!empty($safTransportationImportCountry)) {
    $safTransportationImportCountry = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safTransportationImportCountry))->name;
}

if (!empty($safTransportationExportCountry) && !empty($safTransportationImportCountry)) {
    $safTransportationCountries = $safTransportationExportCountry . ' -  ' . $safTransportationImportCountry;
} elseif (!empty($safTransportatioExportCountry)) {
    $safTransportationCountries = $safTransportatioExportCountry;
} elseif (!empty($safTransportationImportCountry)) {
    $safTransportationCountries = $safTransportationImportCountry;
}

switch (cLng()) {
    case \App\Models\BaseModel::LANGUAGE_CODE_RU:
        $productCommercialDescription = $customData['6033_14'] ?? '';
        break;
    case \App\Models\BaseModel::LANGUAGE_CODE_TJ:
        $productCommercialDescription = $customData['6033_15'] ?? '';
        break;
    case \App\Models\BaseModel::LANGUAGE_CODE_EN:
        $productCommercialDescription = $customData['6033_16'] ?? '';
        break;
}

$transitCountries = $saf->data['saf']['transportation']['transit_country'] ?? '';
$transCountriesList = '';
$transCountries = [];

foreach ($transitCountries as $transitCountry) {
    if (!is_null($transitCountry)) {
        $transCountries[] = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $transitCountry))->name;
    }
}
if (!empty($transCountries)) {
    $transCountries = array_unique($transCountries);
    $transCountriesList = implode(', ', $transCountries);

    $transCountriesList = getMappingValueInMultipleRows(($transCountriesList ?? ''), 40, 90);
}


if (mb_strlen($productCommercialDescription, 'UTF-8') > 50) {
    $productsInfoList1 = mb_substr($productCommercialDescription, 0, 70, 'UTF-8');
    $productsInfoList2 = mb_substr($productCommercialDescription, 70, 90, 'UTF-8');
    $productsInfoList3 = mb_substr($productCommercialDescription, 160, 90, 'UTF-8');
    $newProductRow = true;
}

$fieldID6033_5 = $customData['6033_5'] ?? '';
$fieldID6033_6 = $customData['6033_6'] ?? '';

if (!empty($fieldID6033_5)) {
    $fromDay = date('d', strtotime($fieldID6033_5));
    $fromMonth = month(date('m', strtotime($fieldID6033_5)));
    $fromYear = date('y', strtotime($fieldID6033_5));
}
if (!empty($fieldID6033_6)) {
    $toDay = date('d', strtotime($fieldID6033_6));
    $toMonth = month(date('m', strtotime($fieldID6033_6)));
    $toYear = date('y', strtotime($fieldID6033_6));
}
?>
<table cellspacing="0" cellpadding="1" border="0">
    <tr>
        <td style="height:250px"></td>
    </tr>
</table>
<table cellspacing="0" cellpadding="0">
    <tr style="line-height: 0.5;">
        <td style="font-size: 11px;font-weight: bold;width:30%">Изоҷат дода мешавад,ки</td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;;width:19%">Настоящим разрешается</td>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:81%;">{{$safSideImporterName[0]}}</td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.5;text-align:left;width:30%;line-height: 1.5;"></td>
        <td style="font-size: 9px;text-align:center;line-height: 1.5;width:70%">
            <span>(номи шахси воқеӣ ё ҳуқуқӣ)(наименование физического или юридического лица)</span>
        </td>
    </tr>
    @if(count($safSideImporterName) >1)
        @for($i=1;$i<count($safSideImporterName);$i++)
            <tr>
                <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:100%;">{{$safSideImporterName[$i]}}</td>
            </tr>
        @endfor
    @endif
    <tr>
        <td></td>
    </tr>
    <tr style="line-height: 0.5;">
        <td style="font-size: 11px;font-weight: bold;width:50%">ворид намояд ба Ҷумҳурии Тоҷикистон аз</td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;;width:27%">ввоз в Республику Таджикистан из</td>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:73%;">{{$safTransportationExportCountry}}</td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 0;text-align:left;width:40%;"></td>
        <td style="font-size: 9px;text-align:center;line-height: 1.5;width:60%">
            <span>(номи давлат)(название страны)</span>
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:100%;text-align:center;font-size: 11px;line-height:1.4;">
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:100%;text-align:center;font-size: 11px;line-height:1.4;">
        </td>
    </tr>
    <tr style="line-height: 1.0">
        <td></td>
    </tr>
    <tr style="line-height: 0.5;">
        <td style="font-size: 11px;font-weight: bold;width:50%">маҳсулоти растанигии зеринро:</td>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;;width:29%">следующих растительных материалов</td>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:71%;">{!! ($newProductRow == true) ? $productsInfoList1 : $productCommercialDescription !!}</td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:100%;text-align:left;font-size: 11px;line-height:1.4;">{!! ($newProductRow == true) ? $productsInfoList2 : ''!!}</td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:100%;text-align:left;font-size: 11px;line-height:1.4;">{!! ($newProductRow == true) ? $productsInfoList3 : ''!!}
        </td>
    </tr>
    <tr>
        <td style="font-size: 9px;text-align:center;line-height: 1.5;width:100%">
            <span>(номгӯи,микдор)(название,количество)</span>
        </td>
    </tr>
    <tr style="line-height: 1.0">
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:100%;line-height: 1.5;">
            <span style="font-weight: bold">1.	Маҳсулоти растанигии воридшаванда бояд аз чунин зараррасонҳо, касалиҳо ва алафҳои бегонаи карантинй озод бошад:</span><br>
            Ввозимые растительные материалы должны быть свободны от следующих карантинных вредителей,
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;;width:17%;">болезней и сорняков:</td>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:83%;">Груз должен быть
            свободным от всех видов вредителей, болезней растений и семян сорняков отсутствующих
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:100%;text-align:left;font-size: 11px;line-height:1.4;">на
            территории Республики Таджикистан
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:100%;text-align:center;font-size: 11px;line-height:1.4;"></td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:100%;text-align:center;font-size: 11px;line-height:1.4;">
        </td>
    </tr>
    <tr style="line-height: 1.0">
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:100%;line-height: 1.5;">
            <span style="font-weight: bold">2.	Ба талаботҳои иловагии зерии ҷавобогӯ бошад:</span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;;width:40%;">Отвечать следующим дополнительным требованиям:</td>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:60%;">Отгрузка в герметичных
            транспортах в сопровождении фитосанитарного
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:100%;text-align:left;font-size: 11px;line-height:1.4;">
            сертификата {{$safTransportationExportCountry}}
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:100%;text-align:center;font-size: 11px;line-height:1.4;">
        </td>
    </tr>
    <tr style="line-height: 1.0">
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:100%;line-height: 1.5;"><span style="font-weight: bold">3. Ҳар маҷмӯи маҳсулоти растанигӣ сертификати фитосанитарии мақомоти карантинӣ ё муҳофизати растании давлати содиркунандаро бояд дошта бошад, ки ҷавобгӯ будани маҳсулоти воридотиро ба Ҷумҳурии Тоҷикистон ба талаботҳои бандҳои 1 ва 2 иҷозати мазкур кафолат диҳад.</span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:100%;line-height: 1.5;">Каждая партия ввозимого растительного материала должна
            сопровождаться фитосанитарным сертификатом, выданным официальными организациями по карантину или защите
            растений экспортирующей страны, удостоверяющим , что отправляемые в Республику Таджикистан растительные
            материалы отвечают всем требованиям указанным в пунктах 1 и 2 настоящего разрешения.
        </td>
    </tr>
    <tr style="line-height: 1.0">
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:100%;line-height: 1.4;">
            <span style="font-weight: bold">4.	Воридоти маводи таҳти карантинӣ пас аз назорат ва экспертизая ҳатмии карантинӣ тавассути гузаргоҳҳои сарҳадии карантини растании зерин иҷозат дода мешавад:</span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:100%;">Ввоз подкарантинной продукции разрешается после обязательного
            карантинного контроля и
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;;width:56%;">экспертизы, через следующие пограничные пункты по карантину растений
        </td>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:44%;">{{$transCountriesList[0] ?? ''}}</td>
    </tr>

    <tr>
        <td style="border-bottom: 1px solid #000000;width:100%;text-align:left;font-size: 11px;line-height:1.4;">{{$transCountriesList[1] ?? ''}}
        </td>
    </tr>
    <tr style="line-height: 1.0">
        <td></td>
    </tr>
</table>
<table pagebreak="true" cellspacing="0" cellpadding="0">
    <tr>
        <td style="font-size: 11px;width:100%;line-height: 1.4;"><span style="font-weight: bold">5.	Маводи таҳти карантинии бо иҷозати мазкур воридшаванда бояд бо хатсайри зерин кашонида шавад:</span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:100%;line-height: 1.4;"> Завозимый по настоящему разрешению подкарантинный
            материал должен перевозиться по маршруту:
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:100%;text-align:left;font-size: 11px;line-height:1.4;">{{$safTransportationCountries}}
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:100%;text-align:center;font-size: 11px;line-height:1.4;">
        </td>
    </tr>
    <tr style="line-height: 0.5">
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:100%;line-height: 1.4;"><span style="font-weight: bold">6. Ҳангоми ворид шудании маҳсулоти растанигӣ ва воситаҳои нақлиёт ба нуқтаи муайяншуда, бояд чунин чорабиниҳои карантинӣ гузаронида шаванд:</span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:100%;line-height: 1.4;">По прибытии к месту назначения растительных материалов
            и транспортных средств должны быть
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;;width:38%;">проведены следующие карантинные мероприятия:</td>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:62%;"> По прибытии груза
            поставить в известность Комитет продовольственной
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:100%;text-align:left;font-size: 11px;line-height:1.4;">
            безопасности при Правительстве Республики Таджикистан.Реализовать груз только после досмотра специалистов
            Комитета
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:100%;text-align:left;font-size: 11px;line-height:1.4;">
        </td>
    </tr>
    <tr style="line-height: 0.5">
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:100%;line-height: 1.4;"><span style="font-weight: bold">7. Маҷмӯи маводи таҳти карантинӣ, ки ҷавобгӯи талаботҳои бандҳои 1,2,3 иҷозати мазкур нестанд бо амрномаи нозири карантини растанӣ бояд бoздошта, баргардонида, безарар, коркард ё нобуд карда шаванд.</span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:100%;line-height: 1.4;"> Партии подкарантинных материалов, не отвечающие
            требованиям указанным в пунктах 1,2,3 настоящего разрешения, предписанию инспектора по карантину тастений
            подлежат задержанию, возврату, обеззараживанию, переработке или уничтожению.
        </td>
    </tr>

    <tr>
        <td style="font-size: 11px;width:100%;line-height: 1.5;"><span style="font-weight: bold">8. Иҷозат дар асоси стандартҳои байналмилалӣ оид ба фитосанитарӣ ва Қонуни Ҷумҳурии Тоҷикистон «Дар бораи карантин ва муҳофизати растаниҳо» дода шудааст.</span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:100%;line-height: 1.5;"> Разрешение выдано на основании международных
            стандартов по фитосанитарии и Закона Республики Таджикистан «О карантине и защите ростений».
        </td>
    </tr>
    <tr style="line-height: 0.5">
        <td></td>
    </tr>
    <tr>
        <td style="width:8%"></td>
        <td style="font-size: 11px;width:42%;line-height: 1.5;font-weight:bold;">Дода шуд</td>
        <td style="width:7%"></td>
        <td style="font-size: 11px;width:42%;line-height: 1.5;font-weight:bold;">ба мӯҳлати то</td>
    </tr>
    <tr>
        <td style="width:7%"></td>
        <td style="font-size: 11px;width:42%;line-height: 1.5;">
            <table>
                <tr>
                    <td style="font-size: 11px;width:45px;">Выдано</td>
                    <td style="font-size: 11px;text-align:center;width:40px;">«{{$fromDay ?? ''}}»</td>
                    <td style="font-size: 11px;text-align:center;width:100px;border-bottom: 1px solid #000000;">{{$fromMonth ?? ''}}</td>
                    <td style="font-size: 11px;text-align:center;width:70px;"> 20{{$fromYear ?? ''}}(c.)(г.)</td>
                </tr>
            </table>
        </td>
        <td style="width:7%"></td>
        <td style="font-size: 11px;width:42%;line-height: 1.5;">
            <table>
                <tr>
                    <td style="font-size: 11px;width:60px;">сроком до</td>
                    <td style="font-size: 11px;text-align:center;width:40px;">«{{$toDay ?? ''}}»</td>
                    <td style="font-size: 11px;text-align:center;width:100px;border-bottom: 1px solid #000000;">{{$toMonth ?? ''}}</td>
                    <td style="font-size: 11px;text-align:center;width:70px;"> 20{{$toYear ?? ''}}(c.)(г.)</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr style="line-height: 1.0">
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:50%;line-height: 1.5;font-weight:bold;">Раиси Кумитаи бехатарии<br>озуқавории
            назди Ҳукумати<br>Ҷумҳурии Тоҷикистон
        </td>
        <td style="font-size: 11px;width:50%;line-height: 1.5;font-weight:bold;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:50%;line-height: 1.5;">Председатель Комитета продовольственной<br>безопасности
            при Правительстве
        </td>
        <td style="font-size: 11px;width:50%;line-height: 1.5;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:57%;line-height: 1.5;">Республики Таджикистан</td>
        <td style="font-size: 11px;width:150px;text-align:center;border-bottom: 1px solid #000000;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:50%;line-height: 1.5;font-weight:bold;">Мутахассис</td>
        <td style="font-size: 11px;width:50%;line-height: 1.5;font-weight:bold;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:57%;line-height: 1.5;">Специалист</td>
        <td style="font-size: 11px;width:150px;text-align:center;border-bottom: 1px solid #000000;"></td>
    </tr>
    <tr style="line-height: 1.0">
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:40px;line-height: 1.0;"></td>
        <td style="font-size: 11px;width:200px;line-height: 1.0;">
            <table>
                <tr>
                    <td style="font-size: 14px;line-height: 1.0;text-align:left;font-weight:bold;">Ҷ.М.
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:40px;line-height: 1.0;"></td>
        <td style="font-size: 11px;width:200px;line-height: 1.0;">
            <table>
                <tr>
                    <td style="font-size: 14px;line-height: 1.0;text-align:left;">М.П.
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr style="line-height: 1.5">
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:30%;line-height: 1.5;"><span style="font-weight:bold;">Эзоҳҳо:</span><br>Примечания:
        </td>
        <td style="font-size: 11px;width:70%;line-height: 1.5;"><span style="font-weight: bold;">1.Иҷозати воридоти карантинӣ хамчун дастурамал ва барои иҷро ба шахсони дахлдори воридкунандаи маҳсулот, гузаргоҳи сарҳадии карантини растанӣ ва мақомотҳoи давлатии карантин ва муҳофизати растанӣ фиристода мешавад.</span><br>Импортное
            карантинное разрешение направляется для руководства и исполнения заказчику груза, пограничному пункту по
            карантину раcтений и соответствующим государсвенным органам по карантину и защите растений.<br><span
                    style="font-weight: bold;">2.Тамоми хароҷоти гузаронидани чорабиниҳои фитосанитарӣ ва карантинӣ, аз он ҷумла безараргардонии, тозакунӣ, баргардонӣ, нобуд намудан ва ғайра, аз ҳисоби шахсони вокеӣ ё ҳуқуқии дахлдори воридкунандаи маводи таҳти карантинӣ пардохт карда мешавад.</span><br>Все
            расходы по осуществлению фитосанитарных и карантинных мероприятий, в т.ч. обеззараживание, очистка, возврат,
            уничтожение и т.д., оплачиваются соответствующими физическими или юридическими лицами, импортирующими
            подкарантинный материал.
        </td>
    </tr>

</table>
