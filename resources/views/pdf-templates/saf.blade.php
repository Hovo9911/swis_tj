<?php

use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\SingleApplicationProducts\SingleApplicationProducts;
use App\Models\SingleApplicationProductsBatch\SingleApplicationProductsBatch;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplicationSubApplicationStates\SingleApplicationSubApplicationStates;
use App\Models\UserRoles\UserRoles;
use App\Models\User\User;
use App\Models\Role\Role;
use App\Models\SingleApplication\SingleApplication;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use App\Models\SingleApplicationSubApplicationProducts\SingleApplicationSubApplicationProducts;
use App\Models\DataModel\DataModel;use Illuminate\Support\Carbon;

$agencyName = $agency->current()->name;

$documentCurrentState = SingleApplicationSubApplicationStates::where('saf_subapplication_id', $applicationId)->orderByDesc()->first();

$subAppState = SingleApplicationSubApplicationStates::where(['saf_number' => $saf->regular_number, 'saf_subapplication_id' => $applicationId, 'is_current' => '1'])->first();
if (!is_null($subAppState)) {
    $currentState = ConstructorStates::where('id', $subAppState->state_id)->first();

    if (!is_null($currentState)) {
        $stateName = $currentState->current()->state_name;
    }
}

$headOfCompany = UserRoles::where([
    'company_tax_id' => $agency->tax_id,
    'role_id' => Role::HEAD_OF_COMPANY,
    'role_status' => Role::STATUS_ACTIVE
])->first();//->whereNull('authorized_by_id')

$headUser = User::select('first_name', 'last_name')->find($headOfCompany->user_id);

mb_internal_encoding("UTF-8");
$headOfCompanyFullName = mb_substr(optional($headUser)->first_name, 0, 1) . '.' . optional($headUser)->last_name;
$gerbImagePath = '/image/gerb_' . env('COUNTRY_MODE') . '_sm.png';
$gerbImagePath1 = '/image/gerb_' . env('COUNTRY_MODE') . '_sm1.png';
$safGeneralRegime = $saf->data['saf']['general']['regime'] ?? '';

$importerTypeValue = $saf->data['saf']['sides']['import']['type_value'] ?? '';
$importerName = $saf->data['saf']['sides']['import']['name'] ?? '';
$importerAddress = $saf->data['saf']['sides']['import']['address'] ?? '';
$importerCommunity = $saf->data['saf']['sides']['import']['community'] ?? '';
$importerCountry = $saf->data['saf']['sides']['import']['country'] ?? '';

$exporterTypeValue = $saf->data['saf']['sides']['export']['type_value'] ?? '';
$exporterName = $saf->data['saf']['sides']['export']['name'] ?? '';
$exporterAddress = $saf->data['saf']['sides']['export']['address'] ?? '';
$exporterCommunity = $saf->data['saf']['sides']['export']['community'] ?? '';
$exporterCountry = $saf->data['saf']['sides']['export']['country'] ?? '';

$safTransportationExportCountry = $saf->data['saf']['transportation']['export_country'] ?? '';
$safTransportationImportCountry = $saf->data['saf']['transportation']['import_country'] ?? '';


$safExporterSideCommunity = ($exporterCommunity == '-') ? '' : $exporterCommunity;
$safExporterSideAddress = ($exporterAddress == '-') ? '' : $exporterAddress;
$safExporterSideInfo = '';

if (!empty($safExporterSideCommunity) && !empty($safExporterSideAddress)) {
    $safExporterSideInfo = $safExporterSideCommunity . ' , ' . $safExporterSideAddress . ' , ';
} elseif (!empty($safExporterSideCommunity)) {
    $safExporterSideInfo = $safExporterSideCommunity . ' , ';
} elseif (!empty($safExporterSideCommunity)) {
    $safExporterSideInfo = $safExporterSideAddress . ' , ';
}

$safImporterSideCommunity = ($importerCommunity == '-') ? '' : $importerCommunity;
$safImporterSideAddress = ($importerAddress == '-') ? '' : $importerAddress;
$safImporterSideInfo = '';

if (!empty($safImporterSideCommunity) && !empty($safImporterSideAddress)) {
    $safImporterSideInfo = $safImporterSideCommunity . ' , ' . $safImporterSideAddress . ' , ';
} elseif (!empty($safImporterSideCommunity)) {
    $safImporterSideInfo = $safImporterSideCommunity . ' , ';
} elseif (!empty($safImporterSideCommunity)) {
    $safImporterSideInfo = $safImporterSideAddress . ' , ';
}


if ($saf->regime == SingleApplication::REGIME_IMPORT) {
    $safTransportationImportCountry = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, false, config('swis.country_mode')))->name;
} elseif (!empty($safTransportationImportCountry)) {
    $safTransportationImportCountry = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safTransportationImportCountry))->name;
}

if ($saf->regime == SingleApplication::REGIME_EXPORT) {
    $safTransportationExportCountry = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, false, config('swis.country_mode')))->name;
} elseif (!empty($safTransportationExportCountry)) {

    $exportCountryRef = getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safTransportationExportCountry);
    $safTransportationExportCountry = optional($exportCountryRef)->name;
}

$exporterCountry = !empty($exporterCountry) ? optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $exporterCountry))->name : '';
$importerCountry = !empty($importerCountry) ? optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $importerCountry))->name : '';

$documentId = ConstructorDocument::where(['document_code' => $subApplication->document_code, 'show_status' => 1])->pluck('id')->first();

$applicantTypeValue = $saf->data['saf']['sides']['applicant']['type_value'] ?? '';
$applicantName = $saf->data['saf']['sides']['applicant']['name'] ?? '';
$applicantEmail = $saf->data['saf']['sides']['applicant']['email'] ?? '';
$applicantPhoneNumber = $saf->data['saf']['sides']['applicant']['phone_number'] ?? '';
$applicantAddress = $saf->data['saf']['sides']['applicant']['address'] ?? '';

$approvedNumberText = trans('swis.pdf_templates.saf.permission_number');
$approvedDateText = trans('swis.pdf_templates.saf.permission_date');
$validDateText = trans('swis.pdf_templates.saf.expiration_date');
$subApplicationNumber = $subApplication->document_number;

$registrationDateMdmId = DataModel::getMdmFieldIDByConstant(DataModel::REGISTRATION_DATE_MDM_ID);
$permissionDateMdmId = DataModel::getMdmFieldIDByConstant(DataModel::PERMISSION_DATE_MDM_ID);
$expirationDateMdmId = DataModel::getMdmFieldIDByConstant(DataModel::EXPIRATION_DATE_MDM_ID);
$rejectionNumberMdmId = DataModel::getMdmFieldIDByConstant(DataModel::REJECTION_NUMBER_MDM_ID);
$registrationNumberMdmId = DataModel::getMdmFieldIDByConstant(DataModel::REGISTRATION_NUMBER_MDM_ID);
$rejectionDateMdmId = DataModel::getMdmFieldIDByConstant(DataModel::REJECTION_DATE_MDM_ID);
$permissionNumberMdmId = DataModel::getMdmFieldIDByConstant(DataModel::PERMISSION_NUMBER_MDM_ID);

if (!is_null($documentId)) {

    //----- Get fields values by MDM_ID
    $mdmFields = getFieldsValueByMDMID($documentId, $customData);

    $registrationDate = isset($mdmFields[$registrationDateMdmId]) ? formattedDate($mdmFields[$registrationDateMdmId]) ?? '' : '';
    $approvedDate = isset($mdmFields[$permissionDateMdmId]) ? formattedDate($mdmFields[$permissionDateMdmId]) ?? '' : '';
    $validUntilDate = isset($mdmFields[$expirationDateMdmId]) ? formattedDate($mdmFields[$expirationDateMdmId]) ?? '' : '';
    $rejectedNumber = isset($mdmFields[$rejectionNumberMdmId]) ? $mdmFields[$rejectionNumberMdmId] ?? '' : '';
    $rejectedDate = isset($mdmFields[$rejectionDateMdmId]) ? formattedDate($mdmFields[$rejectionDateMdmId]) ?? '' : '';
    $registrationNumber = isset($mdmFields[$registrationNumberMdmId]) ? $mdmFields[$registrationNumberMdmId] ?? '' : '';
    $approveNumber = isset($mdmFields[$permissionNumberMdmId]) ? $mdmFields[$permissionNumberMdmId] ?? '' : '';
}

$constructorStateInfo = ConstructorStates::where('id', optional($documentCurrentState)->state_id)->first() ?? '';

if ($constructorStateInfo) {
    if (($constructorStateInfo->state_type == ConstructorStates::END_STATE_TYPE && $constructorStateInfo->state_approved_status == ConstructorStates::END_STATE_NOT_APPROVED) || $constructorStateInfo->state_type == ConstructorStates::CANCELED_STATE_TYPE) {

        $approvedNumberText = trans('swis.pdf_templates.saf.rejected_number');
        $approvedDateText = trans('swis.pdf_templates.saf.rejected_date');
        $validDateText = "";
        $approveNumber = $rejectedNumber ?? '';
        $approvedDate = $rejectedDate ?? '';
    }
}

$subAppProductsIds = SingleApplicationSubApplicationProducts::where(['subapplication_id' => $applicationId])->pluck('product_id')->all();

if ($products) {

    foreach ($products as $key => $product) {
        $productsInfo[] = explode('-', $product);
    }

    if (count($productsInfo)) {
        foreach ($productsInfo as $productInfo) {
            $productsNumbers[] = $productInfo[0];
            $productModalIds[] = $productInfo[1];
        }
    }

    $availableProductsIds = array_intersect($subAppProductsIds, $productsNumbers);
    $attachedDocumentProducts = SingleApplicationSubApplications::getAttachedDocumentProductsForPDF($subApplication, $availableProductsIds);

} else {

    $attachedDocumentProducts = SingleApplicationSubApplications::getAttachedDocumentProductsForPDF($subApplication, $subAppProductsIds);
}

$documentSection = $subAppProducts = '';

$availableProductNumbers = [];
$availableProductNumbers = $subApplication->productsOrderedList();

if (count($attachedDocumentProducts) > 0) {

    $num = (empty($num)) ? 1 : $num;

    foreach ($attachedDocumentProducts as $attachedDocumentProduct) {

        $productNumbers = !empty($availableProductNumbers) ? $attachedDocumentProduct->productNumbers($availableProductNumbers) : '';
        $document = (is_null($attachedDocumentProduct->document)) ? $attachedDocumentProduct : $attachedDocumentProduct->document;

        $documentSection = $documentSection . '<tr nobr="true">
                    <td class="bg_color_white fs10 fb lh20" style="width: 7%;">' . $num . '.</td>
                    <td class="bg_color_white fs10 tc lh20" style="width: 10%;">' . $productNumbers . '</td>
                    <td class="bg_color_white fs10 tc lh20" style="width: 53%;">' . $document->document_name . '</td>
                    <td class="bg_color_white fs10 tc lh20" style="width: 10%;">' . $document->document_type . '</td>
                    <td class="bg_color_white fs10 tc lh20" style="width: 10%;">' . $document->document_number . '</td>
                    <td class="bg_color_white fs10 tc lh20" style="width: 10%;">' . $document->document_release_date . '</td>
	            </tr>';
        $num++;
    }
}
$availableProductNumbers = array_keys(array_unique($availableProductNumbers));
$availableProductNumbers = $products ? $availableProductsIds : $availableProductNumbers;
$subAppProducts = SingleApplicationProducts::whereIn('id', $availableProductNumbers)->get();

$obligationsSection = '';
if (count($obligations['obligations']) > 0) {

    if (empty($numObligation)) {
        $numObligation = 1;
    }

    foreach ($obligations['obligations'] as $obligation) {

        $obligationsSection = $obligationsSection . '<tr nobr="true">
		<td class="bg_color_white fs10 fb" style="width: 7%;">' . $numObligation . '.</td>
		<td class="bg_color_white fs10 tc" style="width: 15%;">' . $agencyName . '</td>
		<td class="bg_color_white fs10 tc" style="width: 15%;">' . $obligation['obligationType'] . '</td>
		<td class="bg_color_white fs10 tc" style="width: 13%;">' . $obligation['subApplicationName'] . '</td>
		<td class="bg_color_white fs10 tc" style="width: 10%;">' . $obligation['budgetLine'] . '</td>
		<td class="bg_color_white fs10 tc" style="width: 10%;">' .$obligation['paymentDate']  . '</td>
		<td class="bg_color_white fs10 tc" style="width: 10%;">' . $obligation['obligation_sum'] . '</td>
		<td class="bg_color_white fs10 tc" style="width: 10%;">' . $obligation['payments'] . '</td>
		<td class="bg_color_white fs10 tc" style="width: 10%;">' . $obligation['balance'] . '</td>
	</tr>
	';
        $numObligation++;
    }
    $obligationsSection = $obligationsSection . '<tr nobr="true">
		<td colspan="6" class="bg_color_grey fs10 fb">' . trans('swis.pdf_templates.saf.obligations_total_sum') . '</td>
		<td class="bg_color_white fs10 tc">' . $obligations['totalSum'] . '</td>
		<td  class="bg_color_white fs10 tc">' . $obligations['paymentSum'] . '</td>
		<td  class="bg_color_white fs10 tc">' . number_format($obligations['balanceSum'], 2, '.', '') . '</td>
	</tr>';
}
$totalProductsNettoWeight = $totalProductsBruttoWeight = $totalValueNational = 0;
$productsSection = '';
if ($subAppProducts) {
    foreach ($subAppProducts as $subAppProduct) {

        $productsNum = (empty($productsNum)) ? 1 : $productsNum;

        $productCode = $subAppProduct->product_code ?? '';
        $productCommercialDescription = $subAppProduct->commercial_description ?? '';
        $productNettoWeight = $subAppProduct->netto_weight ?? '';
        $productBruttoWeight = $subAppProduct->brutto_weight ?? '';
        $productQuantity = $subAppProduct->quantity ?? '';
        $productMeasurementUnit = $subAppProduct->measurement_unit ?? '';
        $productTotalValue = $subAppProduct->total_value ?? '';
        $productTotalValueCode = $subAppProduct->total_value_code ?? '';
        $productTotalValueNational = $subAppProduct->total_value_national ?? '';
        $productProducerCountry = $subAppProduct->producer_country ?? '';

        !empty($productNettoWeight) ? $totalProductsNettoWeight += $productNettoWeight : '';
        !empty($productBruttoWeight) ? $totalProductsBruttoWeight += $productBruttoWeight : '';
        !empty($productTotalValueNational) ? $totalValueNational += $productTotalValueNational : '';

        $productMeasurementUnit = optional(getReferenceRows(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $productMeasurementUnit))->name;
        $referenceCurrency = getReferenceRows(ReferenceTable::REFERENCE_CURRENCIES, $productTotalValueCode);
        $productTotalValueCode = optional(getReferenceRows(ReferenceTable::REFERENCE_CURRENCIES, $productTotalValueCode))->name;
        $productProducerCountry = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $productProducerCountry))->name;
        $productCode = optional(getReferenceRows(ReferenceTable::REFERENCE_HS_CODE_6, $productCode))->code;
        $productBatchs = SingleApplicationProductsBatch::where('saf_product_id', $subAppProduct->id)->get();
        $productsBatchNum = 1;

        $productsSection = $productsSection . '<table width="100%" border="1" cellpadding="5">
    <tr nobr="true">
            <td class="bg_color_grey fs10 fb tc" style="width: 7%;">' . trans('swis.pdf_templates.saf.product_number') . '</td>
            <td class="bg_color_grey fs10 fb tc" style="width: 10%;">' . trans('swis.pdf_templates.saf.product_hs_code') . '</td>
            <td class="bg_color_grey fs10 fb tc" style="width: 43%;">' . trans('swis.pdf_templates.saf.product_description') . '</td>
            <td class="bg_color_grey fs10 fb tc" style="width: 10%;">' . trans('swis.pdf_templates.saf.product_netto_weight') . '</td>
            <td class="bg_color_grey fs10 fb tc" style="width: 10%;">' . trans('swis.pdf_templates.saf.product_quantity') . '<br>(' . $productMeasurementUnit . ')</td>
            <td class="bg_color_grey fs9 fb tc" style="width: 10%;">' . trans('swis.pdf_templates.saf.product_producer_country') . '</td>
            <td class="bg_color_grey fs10 fb tc" style="width: 10%;">' . trans('swis.pdf_templates.saf.product_sum') . '<br>(' . $productTotalValueCode . ')</td>
        </tr>
    <tr nobr="true">
            <td class="bg_color_white fs10 fb" style="width: 7%;">' . $productsNum . '.</td>
            <td class="bg_color_white fs10 tc" style="width: 10%;">' . $productCode . '</td>
            <td class="bg_color_white fs10 tc" style="width: 43%;">' . strip_tags($productCommercialDescription) . '</td>
            <td class="bg_color_white fs10 tc" style="width: 10%;">' . $productNettoWeight . '</td>
            <td class="bg_color_white fs10 tc" style="width: 10%;">' . $productQuantity . '</td>
            <td class="bg_color_white fs10 tc" style="width: 10%;">' . $productProducerCountry . '</td>
            <td class="bg_color_white fs10 tc" style="width: 10%;">' . $productTotalValue . '</td>
        </tr>
        </table>
';
        if (count($productBatchs) > 0) {
            $productsSection = $productsSection . '<table width="100%">
<tr nobr="true">
<td width="1%"></td>
<td width="99%">
<table width="100%" border="1" cellpadding="5"  >
        <tr nobr="true">
            <td class="bg_color_grey fs9 fb tc" style="width:6.1%">' . trans('swis.pdf_templates.saf.product_batch_number') . '</td>
            <td class="bg_color_grey fs9 fb tc" style="width:10.05%">' . trans('swis.pdf_templates.saf.product_batch_consignment_number') . '</td>
            <td class="bg_color_grey fs9 fb tc" style="width:8.69%;">' . trans('swis.pdf_templates.saf.product_batch_production_date') . '</td>
            <td class="bg_color_grey fs9 fb tc" style="width:8.69%;">' . trans('swis.pdf_templates.saf.product_batch_expiration_date') . '</td>
            <td class="bg_color_grey fs9 fb tc" style="width:8.69%;">' . trans('swis.pdf_templates.saf.product_batch_netto_weight') . '<br>(кг)</td>
            <td class="bg_color_grey fs9 fb tc" style="width:8.69%;">' . trans('swis.pdf_templates.saf.product_batch_approved_quantity') . ' (кг)</td>
            <td class="bg_color_grey fs9 fb tc" style="width:8.69%;">' . trans('swis.pdf_templates.saf.product_batch_rejected_quantity') . ' (кг)</td>
            <td class="bg_color_grey fs9 fb tc" style="width:10.1%;">' . trans('swis.pdf_templates.saf.product_batch_quantity') . ' (' . $productMeasurementUnit . ')</td>
            <td class="bg_color_grey fs9 fb tc" style="width:10.1%;">' . trans('swis.pdf_templates.saf.product_batch_approved_quantity_1') . ' <br>(' . $productMeasurementUnit . ')</td>
            <td class="bg_color_grey fs9 fb tc" style="width:10.1%;">' . trans('swis.pdf_templates.saf.product_batch_rejected_quantity_1') . '<br>(' . $productMeasurementUnit . ')</td>
            <td class="bg_color_grey fs9 fb tc" style="width:10.1%;">' . trans('swis.pdf_templates.saf.product_batch_rejection_reason') . '</td>
        </tr>';
        }
        if (count($productBatchs) > 0) {
            foreach ($productBatchs as $productBatch) {

                $productBatchRejectedReason = optional(getReferenceRows(ReferenceTable::REFERENCE_REJECTION_REASON, $productBatch->rejection_reason))->name ?? '';

                $productsSection = $productsSection . '<tr nobr="true">
            <td class="bg_color_white fs10 fb tl" >' . $productsNum . '.' . $productsBatchNum . '</td>
            <td class="bg_color_white fs10 tc">' . ((!is_null($productBatch->batch_number)) ? trans($productBatch->batch_number) : '') . '</td>
            <td class="bg_color_white fs10 tc">' . (formattedDate($productBatch->production_date) ?? '') . '</td>
            <td class="bg_color_white fs10 tc">' . (formattedDate($productBatch->expiration_date) ?? '') . '</td>
            <td class="bg_color_white fs10 tc">' . ($productBatch->netto_weight ?? '') . '</td>
            <td class="bg_color_white fs10 tc">' . ($productBatch->approved_quantity ?? '') . '</td>
            <td class="bg_color_white fs10 tc">' . ($productBatch->rejected_quantity ?? '') . '</td>
            <td class="bg_color_white fs10 tc">' . ($productBatch->quantity ?? '') . '</td>
            <td class="bg_color_white fs10 tc">' . ($productBatch->approved_quantity ?? '') . '</td>
            <td class="bg_color_white fs10 tc">' . ($productBatch->rejected_quantity ?? '') . '</td>
            <td class="bg_color_white fs10 tc">' . $productBatchRejectedReason . '</td>
        </tr>';
                $productsBatchNum++;
            }
        }
        if (count($productBatchs) != 0) {
            $productsSection = $productsSection . '</table></td></tr></table>';
        }
        $productsNum++;
    }
}
?>
<table width="100%">
    <tr nobr="true">
        <td style="width: 20%;">
            <img src="{{$gerbImagePath}}"/>
        </td>
        <td class="tc" style="width: 60%;">
            <table width="100%">
                <tr>
                    <td style="font-size: 14px;line-height: 30px;padding: 0 0 100px;">{{$agencyName}}</td>
                </tr>
                <tr>
                    <td style="font-size: 10px;line-height: 30px;">{{$agency->address}}
                        ,{{$agency->phone_number}}
                        ,{{$agency->email}}</td>
                </tr>
                <tr>
                    <td style="font-size: 12px;line-height: 30px;">{{$documentName}}</td>
                </tr>
            </table>
        </td>
        <td class="tr" style="width: 20%;">
        </td>
    </tr>
</table>
<br/>
<br/>
<table width="100%" border="1" cellpadding="5">
    <tr nobr="true">
        <td rowspan="5" class="bg_color_grey" style="width: 40%;">
            <table width="100%">
                <tr>
                    <td class="bg_color_grey fs10 fb lh20">1. {{trans('swis.pdf_templates.saf.importer')}}
                    </td>
                    <td class="bg_color_grey fs10 fb lh20 tc"
                        style="width:130px;"> {{trans('swis.pdf_templates.saf.importer_pasport')}}
                    </td>
                    <td class="bg_color_grey fs10 lh20 tr">{{$importerTypeValue}}</td>
                </tr>
            </table>
            <br/>
            <br/>
            <br/>
            <table width="100%">
                <tr>
                    <td class="fs12 lh20">{{$importerName}}</td>
                </tr>
                <tr>
                    <td class="fs12 lh20">{{$safImporterSideInfo . $importerCountry}}</td>
                </tr>
            </table>
        </td>
        <td class="bg_color_grey fs10 fb lh20" style="width: 20%;">
            3. {{trans('swis.pdf_templates.saf.subaplication_number')}}</td>
        <td class="bg_color_grey fs10 fb lh20" style="width: 20%;">
            4. {{trans('swis.pdf_templates.saf.register_number')}}</td>
        <td class="bg_color_grey fs10 fb lh20" style="width: 20%;">5. {{$approvedNumberText}}</td>
    </tr>
    <tr>
        <td class="bg_color_white fs10 lh20" style="width: 20%;">{{$subApplicationNumber}}</td>
        <td class="bg_color_white fs10 lh20" style="width: 20%;">{{$registrationNumber ?? ''}}</td>
        <td class="bg_color_white fs10 lh20" style="width: 20%;">{{$approveNumber ?? ''}}</td>
    </tr>
    <tr>
        <td class="bg_color_grey fs10 fb lh20" style="width: 20%;">
            6. {{trans('swis.pdf_templates.saf.subapplication_request_date')}}</td>
        <td class="bg_color_grey fs10 fb lh20" style="width: 20%;">
            7. {{trans('swis.pdf_templates.saf.subapplication_registration_date')}}</td>
        <td class="bg_color_grey fs10 fb lh20" style="width: 20%;">8. {{$approvedDateText}}</td>
    </tr>
    <tr>
        <td class="bg_color_white fs10 lh20" style="width: 20%;">{{$subApplicationSubmittingDate}}</td>
        <td class="bg_color_white fs10 lh20" style="width: 20%;">{{$registrationDate ?? ''}}</td>
        <td class="bg_color_white fs10 lh20" style="width: 20%;">{{$approvedDate ?? ''}}</td>
    </tr>
    <tr>
        <td class="bg_color_grey fs10 fb lh20" style="width: 20%;">
            9. {{trans('swis.pdf_templates.saf.current_state')}}</td>
        <td class="bg_color_grey fs10 fb lh20" style="width: 20%;">10. {{trans('swis.pdf_templates.saf.type')}}
        </td>
        <td class="bg_color_grey fs10 fb lh20" style="width: 20%;">11. {{$validDateText}}</td>
    </tr>
    <tr>
        <td rowspan="5" style="background-color: #D9D9D9;width: 40%;">
            <table width="100%">
                <tr>
                    <td class="bg_color_grey fs10 fb lh20">2. {{ trans('swis.pdf_templates.saf.exporter')}}
                    </td>
                    <td class="bg_color_grey fs10 fb lh20 tc"
                        style="width:130px;">{{trans('swis.pdf_templates.saf.exporter_passport')}}
                    </td>
                    <td class="bg_color_grey fs10 lh20 tr">{{$exporterTypeValue}} </td>
                </tr>
            </table>
            <br/>
            <br/>
            <br/>
            <table width="100%">
                <tr>
                    <td class="fs12 lh20">{{$exporterName}}</td>
                </tr>
                <tr>
                    <td class="fs12 lh20">{{$safExporterSideInfo . $exporterCountry}}</td>
                </tr>
            </table>
        </td>
        <td class="bg_color_white fs10 lh20" style="width: 20%;">{{$stateName ?? ''}}</td>
        <td class="bg_color_white fs10 lh20" style="width: 20%;">{{$safGeneralRegime ?? ''}}</td>
        <td class="bg_color_white fs10 lh20" style="width: 20%;">{{$validUntilDate ?? ''}} </td>
    </tr>
    <tr>
        <td class="bg_color_grey fs10 fb lh20" style="width: 20%;">12. {{trans('swis.pdf_templates.saf.total_sum')}}
        </td>
        <td class="bg_color_grey fs10 fb lh20" style="width: 20%;">
            13. {{trans('swis.pdf_templates.saf.export_country')}}
        </td>
        <td class="bg_color_grey fs10 fb lh20" style="width: 20%;">
            14. {{trans('swis.pdf_templates.saf.import_country')}}
        </td>
    </tr>
    <tr>
        <td class="bg_color_white fs10 lh20" style="width: 20%;">{{$totalValueNational}}</td>
        <td class="bg_color_white fs10 lh20" style="width: 20%;">{{$safTransportationExportCountry}}</td>
        <td class="bg_color_white fs10 lh20" style="width: 20%;">{{$safTransportationImportCountry}} </td>
    </tr>
    <tr>
        <td class="bg_color_grey fs10 fb lh20" style="width: 20%;">
            15. {{trans('swis.pdf_templates.saf.total_brutto_weight')}}
        </td>
        <td class="bg_color_grey fs10 fb lh20" style="width: 20%;">
            16. {{trans('swis.pdf_templates.saf.total_netto_weight')}}
        </td>
        <td class="bg_color_grey fs10 fb lh20" style="width: 20%;">
            17. {{trans('swis.pdf_templates.saf.total_products_count')}}
        </td>
    </tr>
    <tr>
        <td class="bg_color_white fs10 lh20" style="width: 20%;">{{$totalProductsBruttoWeight}}
        </td>
        <td class="bg_color_white fs10 lh20" style="width: 20%;">{{$totalProductsNettoWeight}}
        </td>
        <td class="bg_color_white fs10 lh20" style="width: 20%;"><?php if ($subAppProducts) {
                echo e(count($subAppProducts));
            } ?>
        </td>
    </tr>
</table>
<table width="100%" border="1" cellpadding="5">
    <tr>
        <td colspan="6" class="bg_color_grey fs10 fb lh20">
            18. {{ trans('swis.pdf_templates.saf.documents_title')}}</td>
    </tr>
    <tr>
        <td class="bg_color_grey fs10 fb tc lh20" style="width: 7%;">№</td>
        <td class="bg_color_grey fs10 fb tc lh20"
            style="width: 10%;">{{trans('swis.pdf_templates.saf.products_number')}}</td>
        <td class="bg_color_grey fs10 fb tc lh20"
            style="width: 53%;">{{trans('swis.pdf_templates.saf.products_name')}}</td>
        <td class="bg_color_grey fs10 fb tc lh20"
            style="width: 10%;">{{trans('swis.pdf_templates.saf.document_type')}}</td>
        <td class="bg_color_grey fs10 fb tc lh20"
            style="width: 10%;">{{trans('swis.pdf_templates.saf.document_number')}}</td>
        <td class="bg_color_grey fs10 fb tc lh20"
            style="width: 10%;">{{trans('swis.pdf_templates.saf.document_date')}}</td>
    </tr>
    {!! $documentSection !!}
</table>
<table width="100%" border="1" cellpadding="5">
    <tr>
        <td colspan="9" class="bg_color_grey fs10 fb">19. {{trans('swis.pdf_templates.saf.obligation_title')}}</td>
    </tr>
    <tr nobr="true">
        <td class="bg_color_grey fs10 fb tc" style="width: 7%;">№</td>
        <td class="bg_color_grey fs10 fb tc"
            style="width: 15%;">{{trans('swis.pdf_templates.saf.obligation_agency')}}</td>
        <td class="bg_color_grey fs10 fb tc"
            style="width: 15%;">{{trans('swis.pdf_templates.saf.obligation_type')}}</td>
        <td class="bg_color_grey fs10 fb tc"
            style="width: 13%;">{{trans('swis.pdf_templates.saf.obligation_permission_document')}}</td>
        <td class="bg_color_grey fs10 fb tc"
            style="width: 10%;">{{trans('swis.pdf_templates.saf.obligation_budget_line')}}</td>
        <td class="bg_color_grey fs10 fb tc"
            style="width: 10%;">{{trans('swis.pdf_templates.saf.obligation_payment_date')}}</td>
        <td class="bg_color_grey fs10 fb tc"
            style="width: 10%;">{{trans('swis.pdf_templates.saf.obligation_in_somoni')}}</td>
        <td class="bg_color_grey fs10 fb tc"
            style="width: 10%;">{{trans('swis.pdf_templates.saf.obligation_payment_in_somoni')}}</td>
        <td class="bg_color_grey fs10 fb tc"
            style="width: 10%;">{{trans('swis.pdf_templates.saf.obligation_balance_in_somoni')}}</td>
    </tr>
    {!! $obligationsSection !!}
</table>
<table width="100%" border="1" cellpadding="5" nobr="true">
    <tr nobr="true">
        <td colspan="7" class="bg_color_grey fs10 fb">20. {{ trans('swis.pdf_templates.saf.goods_title')}}
        </td>
    </tr>
</table>
{!! $productsSection !!}
<br/>
<br/>
<table width="100%" nobr="true">
    <tr nobr="true">
        <td style="width: 33.3333%;">
            <table width="100%" border="1">
                <tr>
                    <td style="background-color: #D9D9D9;height:100px;">
                        <table width="100%" cellpadding="5">
                            <tr>
                                <td class="fs10 fb tl" style="width: 35%;">
                                    21. {{trans('swis.pdf_templates.saf.applicant_title')}}</td>
                                <td class="fs10 fb tc"
                                    style="width: 35%;">{{trans('swis.pdf_templates.saf.applicant_passport')}}
                                </td>
                                <td class="fs10 tc" style="width: 33%;">{{$applicantTypeValue}}</td>
                            </tr>
                            <tr>
                                <td class="fs10 tl" style="width: 100%;">{{$applicantName}}
                                    <br/>
                                    {{$applicantAddress}}<br/>
                                    {{$applicantPhoneNumber}}<br/>
                                    {{$applicantEmail}}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #FFFFFF;font-size: 10px;text-align: left;height:48px">
                        <br/>
                        <br/>
                        <b>{{ " " . trans('swis.pdf_templates.saf.applicant_signature')}}</b>&nbsp;&nbsp;&nbsp;_
                        _
                        _ _ _ _ _ _ _ _ _ _ _ _ _
                        <br/>
                    </td>
                </tr>
            </table>

        </td>
        <td style="width: 33.3333%;">
            <table width="100%" border="1" cellpadding="5">
                <tr>
                    <td>
                        <table width="100%" cellpadding="5">
                            <tr>
                                <td style="width: 25%;">
                                    <img src="{{$gerbImagePath1}}" width="60"/>
                                </td>
                                <td style="font-size: 10px;width: 75%;">
                                    <br/>
                                    <br/>
                                    {{ trans('swis.pdf_templates.saf.electronic_document_description')}}
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table width="100%">
                                        <tr>
                                            <td style="background-color: #000000;font-size: 8px;">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table>
                                        <tr>
                                            <td style="font-size: 10px;text-align: right;width: 95px;">{{ trans('swis.pdf_templates.saf.electronic_document_printed_by')}}
                                            </td>
                                            <td style="font-size: 10px;">&nbsp;&nbsp;{{$userFullname}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px;text-align: right;width: 95px;">{{ trans('swis.pdf_templates.saf.electronic_document_when_printed')}}</td>
                                            <td style="font-size: 10px;">
                                                &nbsp;&nbsp;{{formattedDate(now()->toDateString())}}</td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px;text-align: right;width: 95px;">{{trans('swis.pdf_templates.saf.electronic_document_ep_number')}}</td>
                                            <td style="font-size: 10px;">&nbsp;&nbsp;000000000000000</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
        <td style="width: 33.3333%;">
            <table width="100%" border="1">
                <tr>
                    <td style="background-color: #D9D9D9;">
                        <table width="100%" cellpadding="5">
                            <tr>
                                <td style="font-size: 10px;text-align: left;width: 100%;height:100px;">
                                    <b>22. {{ trans('swis.pdf_templates.saf.fix_text')}}</b>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #FFFFFF;font-size: 10px;text-align: left;height:48px;">
                        <br/>
                        <br/>
                        <b>{{ " " . trans('swis.pdf_templates.saf.agency_signature')}}</b>&nbsp;&nbsp;&nbsp;_ _
                        _ _ _
                        _ _ _ _ _ _ _ _ _ _ _{{$headOfCompanyFullName ?? ''}}
                        <br/>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<style>
    .bg_color_grey {
        background-color: #D9D9D9;
    }

    .bg_color_white {
        background-color: #FFFFFF;
    }

    .fb {
        font-weight: bold;
    }

    .fs9 {
        font-size: 9px;
    }

    .fs10 {
        font-size: 10px;
    }

    .fs12 {
        font-size: 12px;
    }

    .tc {
        text-align: center;
    }

    .tl {
        text-align: left;
    }

    .tr {
        text-align: right;
    }

    .lh20 {
        line-height: 20px;
    }
</style>
