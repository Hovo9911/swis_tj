<?php

use App\Http\Controllers\Core\Dictionary\DictionaryManager;
use App\Models\BaseModel;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplication\SingleApplication;

$dictionaryManager = new DictionaryManager();

$fieldValuesTJ = getFieldsValues($documentId, $customData, BaseModel::LANGUAGE_CODE_TJ);
$fieldValuesRU = getFieldsValues($documentId, $customData, BaseModel::LANGUAGE_CODE_RU);

$mdmFields = getFieldsValueByMDMID($documentId, $customData);

//------------- Get data field values by saf_id ----------//\
list($mainDataBySafId, $productsDataBySafId, $productBatchesDataBySafId) = getSafDataFieldIdValue($saf, $subApplication, $subAppProducts, $availableProductsIds, BaseModel::LANGUAGE_CODE_TJ);

//----- 1. ,9. ,17.
if (isset($mainDataBySafId['SAF_ID_1'])) {
    switch ($mainDataBySafId['SAF_ID_1']) {
        case SingleApplication::REGIME_IMPORT:
            $point1 = ($mainDataBySafId['SAF_ID_6'] ?? '') . '<br>' . ($mainDataBySafId['SAF_ID_8'] ?? '') . ', ' . ($mainDataBySafId['SAF_ID_9'] ?? '');
            $point9 = $mainDataBySafId['SAF_ID_7'];
            break;
        case SingleApplication::REGIME_EXPORT:
            $point1 = ($mainDataBySafId['SAF_ID_14'] ?? '') . '<br>' . ($mainDataBySafId['SAF_ID_16'] ?? '') . ', ' . ($mainDataBySafId['SAF_ID_17'] ?? '');
            $point9 = $mainDataBySafId['SAF_ID_15'];
            break;
    }
}

//----- 2.
$point2 = $mdmFields['355'] ?? '';

if (!empty($point2)) {
    $point2 = date_parse_from_format(config('swis.date_time_format_front'), $point2);
    if (!empty($point2)) {
        $point2 = $point2['day'] . ' ' . month($point2['month'],BaseModel::LANGUAGE_CODE_TJ) . 'и ' . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_57", BaseModel::LANGUAGE_CODE_TJ) . ' ' . $point2['year'];
    }
}

//----- 6.

$point6 = formattedDate($subApplication->status_date) ?? '';

if (!empty($point6)) {
    $point6 = date_parse_from_format(config('swis.date_time_format_front'), $point6);
    if(!empty($point6)){
        $point6 = $point6['day'] . ' ' . month($point6['month'],BaseModel::LANGUAGE_CODE_TJ) . 'и ' . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_57", BaseModel::LANGUAGE_CODE_TJ) . ' ' . $point6['year'];
    }
}

//----- 15.
$point15 = '<span class="fs10 fb">' . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_28", BaseModel::LANGUAGE_CODE_TJ) . "</span><br><span class='fs10'>" . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_28", BaseModel::LANGUAGE_CODE_RU) . "</span>";

//----- 16.
$point16 = isset($customData['6141_6']) && !empty($customData['6141_6']) ? '<span class="fb">' . formattedDate($customData['6141_6']) . '</span>' : $point15;

//----- 17.
$point17 = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, false, 'TJ', [], 'get', false, false, [], false, BaseModel::LANGUAGE_CODE_TJ))->name;

//----- 18.
$point18 = '<span class="fs10 fb">' . ($fieldValuesTJ['6141_14'] ?? '') . '</span><br><span class="fs10">' . ($fieldValuesRU['6141_14'] ?? '') . '</span>';

$point18Code = optional(getReferenceRows(ReferenceTable::REFERENCE_TARGET_OF_IMPORT, $customData['6141_14'] ?? '', false))->code;
if ($point18Code == ReferenceTable::REFERENCE_TARGET_OF_IMPORT_CODE_HG) {

    $mainDataBySafId['SAF_ID_6'] = $mainDataBySafId['SAF_ID_14'] = $mainDataBySafId['SAF_ID_7'] = $mainDataBySafId['SAF_ID_15'] = '';
}

?>
<table class="border">
    <tr>
        <td colspan="6">
            <table>
                <tr>
                    <td width="70%"></td>
                    <td>
                        <span class="fs12">
                        <span>{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_1", BaseModel::LANGUAGE_CODE_TJ)}}</span>
                        <br/>
                        <span>{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_2", BaseModel::LANGUAGE_CODE_TJ)}}</span>
                        <br/>
                        <span>{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_3", BaseModel::LANGUAGE_CODE_TJ)}}</span>
                        <br/>
                        <span>{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_4", BaseModel::LANGUAGE_CODE_TJ)}}</span>
                    </span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="fs14 fb tc" colspan="6">
            <span>
                <span>{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_5", BaseModel::LANGUAGE_CODE_TJ)}}</span>
                <br/>
                <span>{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_6", BaseModel::LANGUAGE_CODE_TJ)}}</span>
            </span>
        </td>
    </tr>
    <tr>
        <td class="fs12 tc" colspan="6">
            <span>
                <span>{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_5", BaseModel::LANGUAGE_CODE_RU)}}</span>
                <br/>
                <span>{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_6", BaseModel::LANGUAGE_CODE_RU)}}</span>
            </span>
        </td>
    </tr>
    <tr>
        <td colspan="6" height="6px"></td>
    </tr>
    <tr>
        <td class="border-right border-left border-top tt mt20" colspan="2">
            <span class="fb fs10">
                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_7", BaseModel::LANGUAGE_CODE_TJ)}}
            </span>
            <br/>
            <span class="fs9">&nbsp;
                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_7", BaseModel::LANGUAGE_CODE_RU)}}
            </span>
        </td>
        <td class="border-right border-left border-top" colspan="4">
            <table>
                <tr>
                    <td class="tt" width="4%">
                        <span class="fb fs10">
                            7.
                        </span>
                    </td>
                    <td width="25%">
                        <span class="fs10 fb">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_8", BaseModel::LANGUAGE_CODE_TJ)}}&nbsp;{{trans("swis.pdf.{$template}.new_key_9")}}
                        </span>
                        <br/>
                        <span class="fs9">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_8", BaseModel::LANGUAGE_CODE_RU)}}
                        </span>
                    </td>
                    <td width="18%" class="fs10 fb vat">&nbsp;{{$customData['6141_4'] ?? ''}}</td>
                    <td width="5%">
                        <span class="fs10 fb">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_10", BaseModel::LANGUAGE_CODE_TJ)}}
                        </span>
                        <br/>
                        <span class="fs10">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_10", BaseModel::LANGUAGE_CODE_RU)}}
                        </span>
                    </td>
                    <td class="fs10 fb vat">
                        &nbsp;{{$point2 ?? ''}}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="border-right border-left fs10 fb" colspan="2">{!! $point1 ?? '' !!}
        </td>
        <td class="border-right" colspan="4">
            <table>
                <tr>
                    <td class="tt" width="6%">&nbsp;</td>
                    <td class="square white padding5"></td>
                    <td class="tt" width="4%">&nbsp;</td>
                    <td width="34%">
                        <span class="fs10">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_11", BaseModel::LANGUAGE_CODE_TJ)}}
                        </span>
                        <br/>
                        <span class="fs9">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_11", BaseModel::LANGUAGE_CODE_RU)}}
                        </span>
                    </td>
                    <td class="square black"></td>
                    <td class="tt" width="4%">&nbsp;</td>
                    <td>
                        <span class="fs10">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_12", BaseModel::LANGUAGE_CODE_TJ)}}
                        </span>
                        <br/>
                        <span class="fs9">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_12", BaseModel::LANGUAGE_CODE_RU)}}
                        </span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="border-right border-left" colspan="2" height="5px"></td>
        <td class="border-right border-left" colspan="4" height="5px"></td>
    </tr>
    <tr>
        <td class="border-right border-left tt" colspan="2">
            &nbsp;
        </td>
        <td class="border-right" colspan="4">
            <table>
                <tr>
                    <td class="tt" width="4%">
                        <span class="fb fs10">
                            8.
                        </span>
                    </td>
                    <td width="25%" class="vat">
                        <span class="fs10 fb">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_13", BaseModel::LANGUAGE_CODE_TJ)}}&nbsp;{{trans("swis.pdf.{$template}.new_key_9")}}
                        </span>
                        <br/>
                        <span class="fs9">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_13", BaseModel::LANGUAGE_CODE_RU)}}
                        </span>
                    </td>
                    <td width="18%" class="fs10 fb vat">&nbsp;{{$customData['6141_4'] ?? ''}}</td>
                    <td width="5%">
                        <span class="fs10 fb">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_10", BaseModel::LANGUAGE_CODE_TJ)}}
                        </span>
                        <br/>
                        <span class="fs10">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_10", BaseModel::LANGUAGE_CODE_RU)}}
                        </span>
                    </td>
                    <td class="fs10 fb vat">&nbsp;{{$point6 ?? ''}}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="border-right border-left" colspan="2">
            <span class="fb fs10">
                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_14", BaseModel::LANGUAGE_CODE_TJ)}}
            </span>
            <br/>
            <span class="fs9">
                &nbsp;&nbsp;&nbsp;
                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_14", BaseModel::LANGUAGE_CODE_RU)}}
            </span>
        </td>
        <td class="border-left border-right" colspan="4">
            <table>
                <tr>
                    <td class="tt" width="6%">&nbsp;</td>
                    <td class="square white padding5"></td>
                    <td class=" tt" width="4%">&nbsp;</td>
                    <td width="34%">
                        <span class="fs10">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_11", BaseModel::LANGUAGE_CODE_TJ)}}
                        </span>
                        <br/>
                        <span class="fs9">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_11", BaseModel::LANGUAGE_CODE_RU)}}
                        </span>
                    </td>
                    <td class="square black"></td>
                    <td class=" tt" width="4%">&nbsp;</td>
                    <td>
                        <span class="fs10">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_12", BaseModel::LANGUAGE_CODE_TJ)}}
                        </span>
                        <br/>
                        <span class="fs9">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_12", BaseModel::LANGUAGE_CODE_RU)}}
                        </span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="border-right border-left border-bottom" colspan="2" height="8px"></td>
        <td class="border-right border-left border-bottom" colspan="4" height="8px"></td>
    </tr>
    <tr>
        <td class="border-top border-right border-left" colspan="2" height="5px"></td>
        <td class="border-top border-right border-left" colspan="4" height="5px"></td>
    </tr>
    <tr>
        <td class="border-left border-right" colspan="2">
            <table>
                <tr>
                    <td class="fb fs10 tt" width="5%">3.</td>
                    <td class="square black"></td>
                    <td width="1%">&nbsp;</td>
                    <td>
                        <span class="fb fs10">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_15", BaseModel::LANGUAGE_CODE_TJ)}}
                        </span>
                        <br/>
                        <span class="fs9">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_15", BaseModel::LANGUAGE_CODE_RU)}}
                        </span>
                    </td>
                    <td class="square black"></td>
                    <td width="1%">&nbsp;</td>
                    <td>
                        <span class="fb fs10">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_16", BaseModel::LANGUAGE_CODE_TJ)}}
                        </span>
                        <br/>
                        <span class="fs9">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_15", BaseModel::LANGUAGE_CODE_RU)}}
                        </span>
                    </td>
                </tr>
            </table>
        </td>
        <td class="border-left border-right" colspan="4">
            <table>
                <tr>
                    <td class="fb fs10 tt" width="6%">9.</td>
                    <td class="square black"></td>
                    <td width="1%">&nbsp;</td>
                    <td>
                        <span class="fb fs10">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_17", BaseModel::LANGUAGE_CODE_TJ)}}
                        </span>
                        <br/>
                        <span class="fs9">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_17", BaseModel::LANGUAGE_CODE_RU)}}
                        </span>
                    </td>
                    <td width="2%">&nbsp;</td>
                    <td class="square black"></td>
                    <td width="1%">&nbsp;</td>
                    <td>
                        <span class="fb fs10">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_18", BaseModel::LANGUAGE_CODE_TJ)}}
                        </span>
                        <br/>
                        <span class="fs9">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_18", BaseModel::LANGUAGE_CODE_RU)}}
                        </span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="border-left border-right vat" colspan="2">
            <table>
                <tr>
                    <td class="fb fs10" width="1%">&nbsp;</td>
                    <td class="fb fs10" width="52%">&nbsp;{!! nl2br($customData['6141_20'] ?? '') !!}</td>
                    <td class="fb fs10">{{$point9 ?? ''}}</td>
                </tr>
            </table>
        </td>
        <td class="border-left border-right" colspan="4">
            <table>
                <tr>
                    <td class="fb fs10 vat" width="1%">&nbsp;</td>
                    <td class="fb fs10 vat" width="45%">{{$mainDataBySafId['SAF_ID_6'] ?? ''}}</td>
                    <td class="fb fs10 vat" width="6%">&nbsp;</td>
                    <td class="fb fs10 vat">&nbsp;{{$mainDataBySafId['SAF_ID_14'] ?? ''}}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="border-right border-left" colspan="2" height="15px"></td>
        <td class="border-right border-left" colspan="4" height="15px"></td>
    </tr>
    <tr>
        <td class="border-left border-right" colspan="2">&nbsp;</td>
        <td class="border-left border-right" colspan="4">
            <table>
                <tr>
                    <td width="5%" class="vat">
                        <span class="fb tt fs10">10.</span>
                    </td>
                    <td>
                        <span class="fb fs10">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_19", BaseModel::LANGUAGE_CODE_TJ)}}
                        </span>
                        <br/>
                        <span class="fs9">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_19", BaseModel::LANGUAGE_CODE_RU)}}
                        </span>
                    </td>
                    <td>
                        <span class="fb fs10">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_20", BaseModel::LANGUAGE_CODE_TJ)}}
                        </span>
                        <br/>
                        <span class="fs9">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_20", BaseModel::LANGUAGE_CODE_RU)}}
                        </span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="border-bottom border-right border-left" colspan="2" height="15px"></td>
        <td class="border-bottom border-right border-left" colspan="4" height="15px">
            <table>
                <tr>
                    <td width="7%" class="vat"></td>
                    <td class="fb fs10" width="48%">{{$mainDataBySafId['SAF_ID_7'] ?? ''}}</td>
                    <td class="fb fs10">{{$mainDataBySafId['SAF_ID_15'] ?? ''}}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="border-left tt" width="30%">
            <span class="fs10 fb">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_21", BaseModel::LANGUAGE_CODE_TJ)}}</span>
            <br/>
            <span class="fs9">
                    &nbsp;&nbsp;&nbsp;
                    {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_21", BaseModel::LANGUAGE_CODE_RU)}}
            </span>
        </td>
        <td class="fb fs10 vat" width="20%">
            {{$mainDataBySafId['SAF_ID_79'] ?? ''}}
        </td>
        <td class="border" rowspan="2">
            <span class="fs10 fb">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_22", BaseModel::LANGUAGE_CODE_TJ)}}</span>
            <br/>
            <span class="fs10 fb">
                &nbsp;&nbsp;&nbsp;
                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_23", BaseModel::LANGUAGE_CODE_TJ)}}
            </span>
            <br/>
            <span class="fs10 fb">
                &nbsp;&nbsp;&nbsp;
                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_24", BaseModel::LANGUAGE_CODE_TJ)}}
            </span>
            <br/>
            <span class="fs9">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_22", BaseModel::LANGUAGE_CODE_RU)}}
            </span>
            <br/>
            <span class="fs9">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_23", BaseModel::LANGUAGE_CODE_RU)}}
            </span>
            <br/>
            <br/>
            <br/>
        </td>
        <td class="border tt">
            <span class="fb fs10">
                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_25", BaseModel::LANGUAGE_CODE_TJ)}}
            </span>
            <br/>
            <span class="fs8">
                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_25", BaseModel::LANGUAGE_CODE_RU)}}
            </span>
        </td>
        <td class="border tt" colspan="2">
            <span class="fb fs10">
                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_26", BaseModel::LANGUAGE_CODE_TJ)}}
            </span>
            <br/>
            <span class="fs8">
                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_26", BaseModel::LANGUAGE_CODE_RU)}}
            </span>
        </td>
    </tr>
    <tr>
        <td class="border-left border-bottom vat">
            <span class="fs10 fb">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_27", BaseModel::LANGUAGE_CODE_TJ)}}</span>
            <br/>
            <span class="fs9">
                    &nbsp;&nbsp;&nbsp;
                    {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_27", BaseModel::LANGUAGE_CODE_RU)}}
            </span>
        </td>
        <td class="border-bottom fs10 fb vat">
            {!! nl2br($customData['6141_20'] ?? '') !!}
        </td>
        <td class="border">
            &nbsp;{!! $point15 !!}
        </td>
        <td class="border fs10" colspan="2">
            &nbsp;{!! $point16 ?? ''!!}
        </td>
    </tr>
    <tr>
        <td class="border-left tt" width="30%">
            <span class="fs10 fb">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_29", BaseModel::LANGUAGE_CODE_TJ)}}</span>
            <br/>
            <span class="fs9">
                    &nbsp;&nbsp;&nbsp;
                    {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_29", BaseModel::LANGUAGE_CODE_RU)}}
            </span>
        </td>
        <td class="fb fs10 vat" width="20%">
            {{$point17 ?? ''}}
        </td>
        <td class="border-left border-bottom">
            <span class="fs10 fb">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_30", BaseModel::LANGUAGE_CODE_TJ)}}</span>
            <br/>
            <span class="fs9">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_30", BaseModel::LANGUAGE_CODE_RU)}}
            </span>
            <br/>
            <span class="fs9">&nbsp;</span>
        </td>
        <td class="border-bottom vat">
            {!! $point18 !!}
        </td>
        <td colspan="3" class="border-bottom border-right"></td>
    </tr>
    <tr>
        <td class="border-left border-right" colspan="2">&nbsp;</td>
        <td class="border-top border-left border-right" colspan="4">
            <table>
                <tr>
                    <td width="28%">
                        <span class="fs10 fb">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_31", BaseModel::LANGUAGE_CODE_TJ)}}</span>
                        <br/>
                        <span class="fs9">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_31", BaseModel::LANGUAGE_CODE_RU)}}</span>
                        <br/>
                        <span class="fs9">&nbsp;</span>
                    </td>
                    <td class="fs10 fb vat" width="32%">
                        {!! nl2br($customData['6141_21'] ??'') !!}
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="border-top border-left tt">
            <span class="fs10 fb">
                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_32", BaseModel::LANGUAGE_CODE_TJ)}}
            </span>
            <br/>
            <span class="fs9">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_32", BaseModel::LANGUAGE_CODE_RU)}}
            </span>
        </td>
        <td class="border tt">
            <span class="fs10 fb">
                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_33", BaseModel::LANGUAGE_CODE_TJ)}}
            </span>
            <br/>
            <span class="fs10 fb">
                &nbsp;&nbsp;&nbsp;&nbsp;
                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_33", BaseModel::LANGUAGE_CODE_RU)}}
            </span>
            <br/>
            <span class="fs9">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_34", BaseModel::LANGUAGE_CODE_RU)}}
            </span>

        </td>
        <td class="border tt" width="15%">
             <span class="fs10 fb">
                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_35", BaseModel::LANGUAGE_CODE_TJ)}}
            </span>
            <br/>
            <span class="fs10 fb">
                &nbsp;&nbsp;&nbsp;&nbsp;
                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_36", BaseModel::LANGUAGE_CODE_TJ)}}
            </span>
            <br/>
            <span class="fs9">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_35", BaseModel::LANGUAGE_CODE_RU)}}
            </span>
            <br/>
            <span class="fs9">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_36", BaseModel::LANGUAGE_CODE_RU)}}
            </span>
        </td>
        <td class="border tt" width="15%">
            <span class="fs10 fb">
                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_37", BaseModel::LANGUAGE_CODE_TJ)}}
            </span>
            <br/>
            <span class="fs9">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_37", BaseModel::LANGUAGE_CODE_RU)}}
            </span>
            <br/>
            <br/>
            <br/>
        </td>
        <td class="border tt">
            <span class="fs10 fb">
                18.
            </span>
            <br/>
            <span class="fs9">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </span>
            <br/>
            <br/>
            <br/>
        </td>
        <td class="border tt">
            <span class="fs10 fb">
                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_38", BaseModel::LANGUAGE_CODE_TJ)}}
            </span>
            <br/>
            <span class="fs9">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_38", BaseModel::LANGUAGE_CODE_RU)}}
            </span>
            <br/>
            <br/>
            <br/>
        </td>
    </tr>
    <tr>
        <td class="border-left">
            <span class="fs10 fb">
                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_39", BaseModel::LANGUAGE_CODE_TJ)}} &nbsp;{{trans("swis.pdf.{$template}.new_key_9")}}&nbsp;{{$customData['6141_4'] ?? ''}}
            </span>
            <br/>
            <span class="fs9">
                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_39", BaseModel::LANGUAGE_CODE_RU)}}
            </span>
            <br/>
            <span class="fs9">&nbsp;</span>
        </td>
        <td class="border-left fs10 fb">&nbsp;{!! nl2br($customData['6141_23'] ??'') !!}</td>
        <td class="border-left border-right fs10 fb">&nbsp;{!! nl2br($customData['6141_25'] ?? '') !!}</td>
        <td class="border-left border-right fs10 fb tright">{{$customData['6141_24']?? '' }}</td>
        <td class="border-left border-right"></td>
        <td class="border-left border-right fs10 fb tright">{{$customData['6141_26'] ?? '' }}</td>
    </tr>
    <tr>
        <td class="border-left">
            <span class="fs10 fb">{{($customData['6141_22'] ??'')}}</span>
        </td>
        <td class="border-left border-right"></td>
        <td class="border-left border-right"></td>
        <td class="border-left border-right"></td>
        <td class="border-left border-right"></td>
        <td class="border-left border-right"></td>
    </tr>
    <tr>
        <td class="border-right border-left border-top tt" colspan="2">
            <span class="fs10 fb">
                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_41", BaseModel::LANGUAGE_CODE_TJ)}}
            </span>
            <br/>
            <span class="fs9">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_41", BaseModel::LANGUAGE_CODE_RU)}}
            </span>
            <br/><br/>
            <span class="fs10 fb">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_42", BaseModel::LANGUAGE_CODE_TJ)}} &nbsp;{{trans("swis.pdf.{$template}.new_key_9")}} &nbsp;{{$mainDataBySafId['SAF_ID_138'] ?? ''}}</span>
            <br/>
            <span class="fs9">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_42", BaseModel::LANGUAGE_CODE_RU)}}</span>
            <br/>
            <span class="fs10 fb">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_43", BaseModel::LANGUAGE_CODE_TJ)}} &nbsp;{{$point6 ?? ''}}</span>
        </td>
        <td class="border-top border-left border-right vat" colspan="4"><br/>

        </td>
    </tr>
    <tr>
        <td class="border-left border-right" colspan="2"></td>
        <td colspan="2"></td>
        <td class="border-right" colspan="2"></td>
    </tr>
    <tr>
        <td class="border-left border-right" colspan="2"></td>
        <td colspan="2"></td>
        <td class="border-right fs10 fb" colspan="2">&nbsp;</td>
    </tr>
    <tr>
        <td class="border-right border-left" colspan="2"></td>
        <td class="border-right border-left" colspan="4"></td>
    </tr>
    <tr>
        <td class="border-right border-left" colspan="2" height="10px"></td>
        <td class="border-right border-left" colspan="4" height="10px"></td>
    </tr>
    <tr>
        <td class="border-right border-left border-top tt" colspan="2">
            <span class="fb fs10">
                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_47", BaseModel::LANGUAGE_CODE_TJ)}}
            </span>
            <br/>
            <span class="fs9">
                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_47", BaseModel::LANGUAGE_CODE_RU)}}
            </span>
        </td>
        <td class="border-right border-left border-top" colspan="4">
            <span class="fb fs10">
                {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_44", BaseModel::LANGUAGE_CODE_TJ)}}
            </span>
            <br/>
            <span class="fs9">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_44", BaseModel::LANGUAGE_CODE_RU)}}
            </span>
        </td>
    </tr>
    <tr>
        <td class="border-right border-left" colspan="2" height="6px"></td>
        <td class="border-right border-left" colspan="4" height="6px"></td>
    </tr>
    <tr>
        <td class="border-right border-left" colspan="2"></td>
        <td colspan="2"></td>
        <td class="border-right vab" colspan="2"></td>
    </tr>
    <tr>
        <td class="border-left border-right" colspan="2">
            <table>
                <tr>
                    <td class="fs10 fb"
                        width="15%">{{$mdmFields['508'] ?? ''}}</td>
                </tr>
                <tr>
                    <td class="fs10" width="15%">{{$mainDataBySafId['SAF_ID_170'] ?? ''}}</td>
                </tr>
            </table>
        </td>
        <td colspan="2"></td>
        <td class="border-right" colspan="2">
        </td>
    </tr>
    <tr>
        <td class="border-left border-right" colspan="2"></td>
        <td colspan="2"></td>
        <td colspan="2" class="border-right"></td>
    </tr>
    <tr>
        <td class="border-right border-left" colspan="2">
            <table>
                <tr>
                    <td width="15%">&nbsp;</td>
                </tr>
                <td width="10%"></td>
                <td width="8%">
                    <span class="fb fs10">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_52", BaseModel::LANGUAGE_CODE_TJ)}}</span>
                    <br/>
                    <span class="fs9">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_52", BaseModel::LANGUAGE_CODE_RU)}}</span>
                </td>
                <td width="5%"></td>
                <td class="border-top tc">
                    <span class="fb fs9">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_46", BaseModel::LANGUAGE_CODE_TJ)}}</span>
                    <br/>
                    <span class="fs7">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_46", BaseModel::LANGUAGE_CODE_RU)}}</span>
                </td>
                <td width="5%"></td>
            </table>
        </td>
        <td class="border-left" colspan="2"></td>
        <td class="border-right" colspan="2">
            <span class="fs10 fb">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_45", BaseModel::LANGUAGE_CODE_TJ)}}</span>
            <br/>
            <span class="fs9">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_45", BaseModel::LANGUAGE_CODE_RU)}} </span>
            <br/><span class="fs10 fb">{{$customData['6141_18'] ?? '' }}</span>
        </td>
    </tr>
    <tr>
        <td class="border-left border-right" colspan="2">
            <table>
                <tr>
                    <td width="30%"></td>
                    <td width="10%">
                        <span class="fb fs10">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_53", BaseModel::LANGUAGE_CODE_TJ)}}
                        </span>
                        <br/>
                        <span class="fs9">
                            {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_53", BaseModel::LANGUAGE_CODE_RU)}}
                        </span>
                    </td>
                    <td class="border-bottom"></td>
                    <td width="5%"></td>
                </tr>
            </table>
        </td>
        <td class="border-left border-right" colspan="4">
            <table>
                <tr>
                    <td width="10%"></td>
                    <td class="border-top tc" width="50%">
                        <span class="fb fs9">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_46", BaseModel::LANGUAGE_CODE_TJ)}}</span>
                        <br/>
                        <span class="fs7">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_46", BaseModel::LANGUAGE_CODE_RU)}}</span>
                    </td>
                    <td></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="border-right border-left" colspan="2" height="8px"></td>
        <td class="border-right border-left" colspan="4" height="8px"></td>
    </tr>
    <tr>
        <td class="border" colspan="6">
            <table>
                <tr>
                    <td class="fb fs10"
                        width="20%">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_54", BaseModel::LANGUAGE_CODE_TJ)}}
                    </td>
                    <td class="fb fs10">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_55", BaseModel::LANGUAGE_CODE_TJ)}}</td>
                </tr>
                <tr>
                    <td class="fs9" width="20%">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        {{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_54", BaseModel::LANGUAGE_CODE_RU)}}
                    </td>
                    <td class="fs9">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.new_key_55", BaseModel::LANGUAGE_CODE_RU)}}</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<style>

    @page {
        margin: 5mm 7mm;
    }

    .fs7 {
        font-size: 7px;
    }

    .fs8 {
        font-size: 8px;
    }

    .fs9 {
        font-size: 9px;
    }

    .fs10 {
        font-size: 10px;
    }

    .fs12 {
        font-size: 12px;
    }

    .fs14 {
        font-size: 14px;
    }

    .fb {
        font-weight: bold;
    }

    .tc {
        text-align: center;
    }

    .tright {
        text-align: right;
    }

    .tt {
        vertical-align: baseline;
    }

    .border {
        border: 1px solid black;
    }

    .border-left {
        border-left: 1px solid black;
    }

    .border-right {
        border-right: 1px solid black;
    }

    .border-top {
        border-top: 1px solid black;
    }

    .border-bottom {
        border-bottom: 1px solid black;
    }

    .square {
        width: 29px;
        height: 20px;
    }

    .black {
        background-color: #000000;
    }

    .white {
        border: 1px solid #000000;
    }

    .mt20 {
        margin-top: 20px;
    }

    .vat {
        vertical-align: top;
    }

    .vab {
        vertical-align: bottom;
    }

    table {
        border-collapse: collapse;
        overflow: wrap;
        width: 100%;
    }

</style>