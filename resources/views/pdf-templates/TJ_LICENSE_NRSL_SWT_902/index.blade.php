<?php
use App\Models\ReferenceTable\ReferenceTable;
use App\Http\Controllers\Core\Dictionary\DictionaryManager;
use App\Models\SingleApplication\SingleApplication;
use App\Models\ConstructorTemplates\ConstructorTemplates;

$dictionaryManager = new DictionaryManager();
$mdmIdNames = getFieldsValueByMDMID($documentId, $customData, cLng());
list($mainDataBySafId, $productsDataBySafId, $productBatchesDataBySafId) = getSafDataFieldIdValue($saf, $subApplication, $subAppProducts, $availableProductsIds, ConstructorTemplates::LANGUAGE_CODE_RU);

// ------------------------------------------ General Page -------------------------------------------------------------
// ----- 2.
$fieldIDNaRSL_4 = !is_null($customData) ? $customData['NaRSL_4'] ?? '' : ''; // MDM_ID_365

// ----- 3., 3.[other page]
$fieldIDNaRSL_5 = !is_null($customData) ? $customData['NaRSL_5'] ?? '' : ''; // MDM_ID_355
if (!empty($fieldIDNaRSL_5)) {
    $fromDay = date('d', strtotime($fieldIDNaRSL_5));

    $fromMonth_ru = month(date('m', strtotime($fieldIDNaRSL_5)), ConstructorTemplates::LANGUAGE_CODE_RU);
    $fromMonth_tj = month(date('m', strtotime($fieldIDNaRSL_5)), ConstructorTemplates::LANGUAGE_CODE_TJ);

    $fromYear = date('y', strtotime($fieldIDNaRSL_5));

    $fieldIDNaRSL_5_ru = '&laquo;' . $fromDay . '&raquo;' . ' ' . $fromMonth_ru . ' 20' . $fromYear;
    $fieldIDNaRSL_5_tj = '&laquo;' . $fromDay . '&raquo;' . ' ' . $fromMonth_tj . ' 20' . $fromYear;
    // ----- 18.[other page]
    $fieldIDNaRSL_5_other_page = date('d.m.Y', strtotime($fieldIDNaRSL_5));
} else {

    $fieldIDNaRSL_5_ru = '&laquo;___&raquo;_____________________20___';
    $fieldIDNaRSL_5_tj = '&laquo;___&raquo;_____________________20___';

    // ----- 18.[other page]
    $fieldIDNaRSL_5_other_page = '';

}

// ----- 6.
$safGeneralRegime = $saf->data['saf']['general']['regime'] ?? ''; // SAF_ID_1

// ----- 4., 5., 8.
switch ($safGeneralRegime) {
    case SingleApplication::REGIME_IMPORT:

        // ----- 4., 5.[other page]
        $safSideName = $saf->data['saf']['sides']['import']['name'] ?? ''; //SAF_ID_6

        // ----- 6.
        $safGeneralRegimeInfo_ru = $dictionaryManager->getTransByLngCode("swis.saf.general.regime.import.title", ConstructorTemplates::LANGUAGE_CODE_RU) . ' ' . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.general_regime_text", ConstructorTemplates::LANGUAGE_CODE_RU);
        $safGeneralRegimeInfo_tj = $dictionaryManager->getTransByLngCode("swis.saf.general.regime.import.title", ConstructorTemplates::LANGUAGE_CODE_TJ) . ' ' . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.general_regime_text", ConstructorTemplates::LANGUAGE_CODE_TJ);

        // ----- 5., 7.[other page]
        $safSideCommunity = $saf->data['saf']['sides']['import']['community'] ?? ''; // SAF_ID_8
        $safSideAddress = $saf->data['saf']['sides']['import']['address'] ?? ''; //SAF_ID_9

        // ----- 8., 8.[other page], 9.->2.[other page]
        $safSidePhoneNumber = $saf->data['saf']['sides']['import']['phone_number'] ?? ''; // SAF_ID_10

        // ----- 11.[other page]
        $keyExportImportList =  $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_page.label.list_import", ConstructorTemplates::LANGUAGE_CODE_RU);

        break;
    case SingleApplication::REGIME_EXPORT:

        // ----- 4., 5.[other page]
        $safSideName = $saf->data['saf']['sides']['export']['name'] ?? '';//SAF_ID_14

        // ----- 5., 7.[other page]
        $safSideCommunity = $saf->data['saf']['sides']['export']['community'] ?? ''; // SAF_ID_16
        $safSideAddress = $saf->data['saf']['sides']['export']['address'] ?? '';//SAF_ID_17

        // ----- 6.
        $safGeneralRegimeInfo_ru = $dictionaryManager->getTransByLngCode("swis.saf.general.regime.export.title", ConstructorTemplates::LANGUAGE_CODE_RU) . ' ' . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.general_regime_text", ConstructorTemplates::LANGUAGE_CODE_RU);
        $safGeneralRegimeInfo_tj = $dictionaryManager->getTransByLngCode("swis.saf.general.regime.export.title", ConstructorTemplates::LANGUAGE_CODE_TJ) . ' ' . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.general_regime_text", ConstructorTemplates::LANGUAGE_CODE_TJ);

        // ----- 8., 8.[other page], 9.->2.[other page]
        $safSidePhoneNumber = $saf->data['saf']['sides']['export']['phone_number'] ?? ''; // SAF_ID_18

        // ----- 11.[other page]
        $keyExportImportList =  $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_page.label.list_export", ConstructorTemplates::LANGUAGE_CODE_RU);


        break;
    default:

        // ----- 4., 5.[other page]
        $safSideName = '';

        // ----- 5., 7.[other page]
        $safSideCommunity = '';
        $safSideAddress = '';

        // ----- 8., 8.[other page], 9.->2.[other page]
        $safSidePhoneNumber = '';

        // ----- 11.[other page]
        $keyExportImportList =  '';

        break;
}

// ----- 5., 7.[other page]
$safSideCommunityAddress = $safSideCommunity . ', ' . $safSideAddress;
$safSideCommunityAddress = trim($safSideCommunityAddress, ', ');

// ----- 7., 6.[other page]
$safControlledFieldsGeneralNuclearSafetySupervisor = notEmptyForPdfFields($mainDataBySafId, 'SAF_ID_170');

// ----- 9.
$keyResponsiblePerson_ru = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.responsible_person", ConstructorTemplates::LANGUAGE_CODE_RU);
$keyResponsiblePerson_tj= $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.responsible_person", ConstructorTemplates::LANGUAGE_CODE_TJ);

// ----- 10.
$keyApplicationLabel_ru = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.application", ConstructorTemplates::LANGUAGE_CODE_RU);
$keyApplicationLabel_tj = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.application", ConstructorTemplates::LANGUAGE_CODE_TJ);

$keyApplicationDescription_ru = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.application.description", ConstructorTemplates::LANGUAGE_CODE_RU);
$keyApplicationDescription_tj = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.application.description", ConstructorTemplates::LANGUAGE_CODE_TJ);

// ----- 11.
$keyGeneral_ru = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.general", ConstructorTemplates::LANGUAGE_CODE_RU);
$keyGeneral_tj = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.general", ConstructorTemplates::LANGUAGE_CODE_TJ);

// ----- 12.
$fieldIDNaRSL_6 = !is_null($customData) ? $customData['NaRSL_6'] ?? '' : ''; // MDM_ID_357
if (!empty($fieldIDNaRSL_6)) {
    $fromDay = date('d', strtotime($fieldIDNaRSL_6));

    $fromMonth_ru = month(date('m', strtotime($fieldIDNaRSL_6)), ConstructorTemplates::LANGUAGE_CODE_RU);
    $fromMonth_tj = month(date('m', strtotime($fieldIDNaRSL_6)), ConstructorTemplates::LANGUAGE_CODE_TJ);

    $fromYear = date('y', strtotime($fieldIDNaRSL_6));

    $fieldIDNaRSL_6_ru = '&laquo;' . $fromDay . '&raquo;' . ' ' . $fromMonth_ru . ' 20' . $fromYear;
    $fieldIDNaRSL_6_tj = '&laquo;' . $fromDay . '&raquo;' . ' ' . $fromMonth_tj . ' 20' . $fromYear;
} else {
    $fieldIDNaRSL_6_ru = '&laquo;___&raquo;_____________________20___';
    $fieldIDNaRSL_6_tj = '&laquo;___&raquo;_____________________20___';
}

// ----- 13.

$keyDepartmentNamePart1_ru = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.department_name.part_1", ConstructorTemplates::LANGUAGE_CODE_RU);
$keyDepartmentNamePart1_tj = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.department_name.part_1", ConstructorTemplates::LANGUAGE_CODE_TJ);

$keyDepartmentNamePart2_ru = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.department_name.part_2", ConstructorTemplates::LANGUAGE_CODE_RU);
$keyDepartmentNamePart2_tj = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.department_name.part_2", ConstructorTemplates::LANGUAGE_CODE_TJ);

$keyDepartmentNamePart3_ru = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.department_name.part_3", ConstructorTemplates::LANGUAGE_CODE_RU);
$keyDepartmentNamePart3_tj = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.department_name.part_3", ConstructorTemplates::LANGUAGE_CODE_TJ);

$keyDepartmentName_ru = $keyDepartmentNamePart1_ru .
    '<br>' . $keyDepartmentNamePart2_ru .
    '<br>' . $keyDepartmentNamePart3_ru;

$keyDepartmentName_tj = $keyDepartmentNamePart1_tj .
    '<br>' . $keyDepartmentNamePart2_tj .
    '<br>' . $keyDepartmentNamePart3_tj;

// ----- 14., 19.[other page]
$fieldIDNaRSL_7 = !is_null($customData) ? $customData['NaRSL_7'] ?? '' : ''; // MDM_ID_382

// ------------------------------------------- Other Page --------------------------------------------------------------

$productsTableList = [];

$productsNumber = 1;

// ----- 12., 13., 14., 15.

foreach ($subAppProducts as $key => $value) {

    if (!empty($customData['NaRSL_21_pr_' . $value['id']]) || !empty($customData['NaRSL_22_pr_' . $value['id']]) || !empty($customData['NaRSL_23_pr_' . $value['id']])) {

        // ----- 12.
        $productsTableList[$value['id']]['number'] = $productsNumber . '.';

        // ----- 13.
        $productsTableList[$value['id']]['NaRSL_21'] = $customData['NaRSL_21_pr_' . $value['id']] ?? ''; // MDM_ID_451

        // ----- 14.
        $productsTableList[$value['id']]['NaRSL_22'] = $customData['NaRSL_22_pr_' . $value['id']] ?? ''; // MDM_ID_452

        // ----- 15.
        $productsTableList[$value['id']]['NaRSL_23'] = $customData['NaRSL_23_pr_' . $value['id']] ?? ''; // MDM_ID_453

        $productsNumber++;

    }

}

$productsTableList = array_chunk($productsTableList, 4);
$productsTableListForPages = [];
foreach ($productsTableList as $key => $value) {
    $productsTableListForPages[$key] = array_chunk($value, 2);
}

// ----- 1.
// ----- 1.->1.
$keyOtherPageNumberText1 = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_page.number.text_1", ConstructorTemplates::LANGUAGE_CODE_RU);

// ----- 1.->2.
$keyOtherPageNumberText2Part1 = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_page.number.text_2.part_1", ConstructorTemplates::LANGUAGE_CODE_RU);
$fieldIDNaRSL_16 = !is_null($customData) ? $customData['NaRSL_16'] ?? '' : ''; //MDM_ID_442
$keyOtherPageNumberText2Part2 = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_page.number.text_2.part_2", ConstructorTemplates::LANGUAGE_CODE_RU);
$keyOtherPageNumberText2 = $keyOtherPageNumberText2Part1 . ' ' .$fieldIDNaRSL_16 . ' ' .$keyOtherPageNumberText2Part2;

// ----- 2.
$keyOtherPageTitle = '';
$keyOtherPageTitleText1Part1 = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_page.title.text_1.part_1", ConstructorTemplates::LANGUAGE_CODE_RU);
$keyOtherPageTitleText1Part2 = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_page.title.text_1.part_2", ConstructorTemplates::LANGUAGE_CODE_RU);
$keyOtherPageTitleText2 = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_page.title.text_2", ConstructorTemplates::LANGUAGE_CODE_RU);
$keyOtherPageTitle = $keyOtherPageTitleText1Part1 . '<br>' . $keyOtherPageTitleText1Part2 . '<br><br>' . $keyOtherPageTitleText2;

// ----- 4.
$fieldIDNaRSL_19 = !is_null($customData) ? $customData['NaRSL_19'] ?? '' : ''; //MDM_ID_396

// ----- 9.->1.
$safControlledFieldsGeneralNuclearSafetyResponsibleOfficer = notEmptyForPdfFields($mainDataBySafId, 'SAF_ID_171');

// ----- 10.
$fieldIDNaRSL_17 = !is_null($customData) ? $customData['NaRSL_17'] ?? '' : ''; // MDM_ID_446

// ----- 12.
$keyTableListNumber = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_page.table_list.number", ConstructorTemplates::LANGUAGE_CODE_RU);

// ----- 13.
$keyTableListNuclide = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_page.table_list.nuclide", ConstructorTemplates::LANGUAGE_CODE_RU);

// ----- 14.
$keyTableListWeight = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_page.table_list.weight", ConstructorTemplates::LANGUAGE_CODE_RU);

// ----- 15.
$keyTableListForm = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_page.table_list.form", ConstructorTemplates::LANGUAGE_CODE_RU);

// ----- 16.
$keyOtherPageStandardDocumentTitle = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_page.standard_document.title", ConstructorTemplates::LANGUAGE_CODE_RU);
$keyOtherPageStandardDocumentText1 = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_page.standard_document.text_1", ConstructorTemplates::LANGUAGE_CODE_RU);
$keyOtherPageStandardDocumentText2 = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_page.standard_document.text_2", ConstructorTemplates::LANGUAGE_CODE_RU);
$keyOtherPageStandardDocumentText3 = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_page.standard_document.text_3", ConstructorTemplates::LANGUAGE_CODE_RU);
$keyOtherPageStandardDocumentText4 = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_page.standard_document.text_4", ConstructorTemplates::LANGUAGE_CODE_RU);
$keyOtherPageStandardDocumentText5 = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_page.standard_document.text_5", ConstructorTemplates::LANGUAGE_CODE_RU);

// ----- 17.
$keyOtherPageAgency = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_page.label.agency", ConstructorTemplates::LANGUAGE_CODE_RU)
?>
<style type="text/css">

    table > tr > td {
        font-size: 12px;
        text-align: left;
        line-height: 1;
    }

    .fs14 {
        font-size: 14px;
    }

    .fs7 {
        font-size: 7px;
    }

    .fs11 {
        font-size: 11px;
    }

    .bold {
        font-weight:bold;
    }

    .tc {
        text-align: center;
    }

    .left_width {
        width: 95px;
    }

    .right_width {
        width: 75px;
    }

    .middle_width {
        width: 560px;
    }

    .text_2 {
        border-bottom: 1px solid #000000;
    }

    .text_1 {
        font-size: 7px;
        text-align: center;
        border-top: 1px solid #000000;
    }

    .bordered {
        border: 1px solid #000000;
    }

    /*
    other page
     */

    .op_left_width {
        width: 120px;
    }

    .op_middle_width {
        width: 540px;
    }

    .op_right_width {
        width: 75px;
    }

    .productTables > tr > td {
        line-height: 2;
        text-align: center;
    }

</style>

<?php
//   page width 740px 560px
?>

<table cellspacing="0" cellpadding="1" border="0">


    <tr>
        <td style="height: 300px; width: 740px;"></td>
    </tr>


    <tr>
        <td class="left_width"></td>
        <td style="width: 30px;"></td>
        <td style="width: 150px">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.registration_number", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
        <td style="width: 150px" class="text_2 bold">{{$fieldIDNaRSL_4}}</td>
        <td style="width: 30px;"></td>
        <td style="width: 200px; text-align: right;">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.from_date", ConstructorTemplates::LANGUAGE_CODE_RU) . ' ' . $fieldIDNaRSL_5_ru . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.year", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
    </tr>


    <tr>
        <td class="middle_width"></td>
    </tr>
    <tr>
        <td class="left_width"></td>
        <td style="width: 110px;">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.issued_by", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
        <td style="width: 450px;" class="bold">{{$safSideName}}</td>
    </tr>
    <tr>
        <td class="left_width"></td>
        <td style="width: 110px;"></td>
        <td style="width: 450px;" class="text_1">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.issued_by.description", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
    </tr>


    <tr>
        <td class="middle_width"></td>
    </tr>
    <tr>
        <td class="left_width"></td>
        <td style="width: 220px;">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.legal_address", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
        <td style="width: 340px" class="text_2 bold">{{$safSideCommunityAddress}}</td>
    </tr>


    <tr>
        <td class="middle_width"></td>
    </tr>
    <tr>
        <td class="left_width"></td>
        <td style="width: 110px;">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.general_info", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
        <td style="width: 450px;" class="bold">{{$safGeneralRegimeInfo_ru}}</td>
    </tr>
    <tr>
        <td class="left_width"></td>
        <td style="width: 110px;"></td>
        <td style="width: 450px;" class="text_1">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.general_info.description", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
    </tr>


    <tr>
        <td class="middle_width"></td>
    </tr>
    <tr>
        <td class="left_width"></td>
        <td style="width: 280px;">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.legal_entity", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
        <td style="width: 135px;" class="bold">{{$safControlledFieldsGeneralNuclearSafetySupervisor}}</td>
        <td style="width: 10px;"></td>
        <td style="width: 135px;" class="bold">{{$safSidePhoneNumber}}</td>
    </tr>
    <tr>
        <td class="left_width"></td>
        <td style="width: 280px;"></td>
        <td style="width: 135px;" class="text_1">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.fio", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
        <td style="width: 10px;"></td>
        <td style="width: 135px;" class="text_1">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.phone", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
    </tr>


    <tr>
        <td class="middle_width"></td>
    </tr>
    <tr>
        <td class="left_width"></td>
        <td style="width: 220px;">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.responsible_person", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
        <td style="width: 135px;" class="bold">{{$keyResponsiblePerson_ru}}</td>
        <td style="width: 70px;"></td>
        <td style="width: 135px;" class="bold"></td>
    </tr>
    <tr>
        <td class="left_width"></td>
        <td style="width: 220px;"></td>
        <td style="width: 135px;" class="text_1">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.fio", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
        <td style="width: 70px;"></td>
        <td style="width: 135px;" class="text_1">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.phone", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
    </tr>

    <tr>
        <td class="left_width"></td>
        <td class="middle_width"><b>{{$keyApplicationLabel_ru}} </b>{{$keyApplicationDescription_ru}}</td>
    </tr>
    <tr>
        <td class="left_width"></td>
        <td style="width: 100px;"></td>
        <td style="width: 460px;" class="bold">{{$keyGeneral_ru}}</td>
    </tr>


    <tr>
        <td class="middle_width"></td>
    </tr>
    <tr>
        <td class="left_width"></td>
        <td style="width: 100px;"></td>
        <td class="bold" style="width: 460px;">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.license_term", ConstructorTemplates::LANGUAGE_CODE_RU) . $fieldIDNaRSL_6_ru . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.year", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
    </tr>


    <tr>
        <td class="middle_width"></td>
    </tr>
    <tr>
        <td class="left_width"></td>
        <td style="width: 110px;"></td>
        <td style="width: 450px;">{!! $keyDepartmentName_ru !!}</td>
    </tr>


    <tr>
        <td class="left_width"></td>
        <td style="width: 110px;"></td>
        <td style="width: 200px;">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.general_inspector", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
        <td style="width: 100px;"></td>
        <td style="width: 150px;" class="bold">{{$fieldIDNaRSL_7}}</td>
    </tr>
</table>

<table cellspacing="0" cellpadding="1" border="0" pagebreak="true">


    <tr>
        <td style="height: 300px; width: 740px;"></td>
    </tr>


    <tr>
        <td class="left_width"></td>
        <td style="width: 30px;"></td>
        <td style="width: 150px;">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.registration_number", ConstructorTemplates::LANGUAGE_CODE_TJ)}}</td>
        <td style="width: 150px;" class="text_2 bold">{{$fieldIDNaRSL_4}}</td>
        <td style="width: 30px;"></td>
        <td style="width: 200px;">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.from_date", ConstructorTemplates::LANGUAGE_CODE_TJ) . $fieldIDNaRSL_5_tj . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.year", ConstructorTemplates::LANGUAGE_CODE_TJ)}}</td>
    </tr>


    <tr>
        <td class="middle_width"></td>
    </tr>
    <tr>
        <td class="left_width"></td>
        <td style="width: 110px;">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.issued_by", ConstructorTemplates::LANGUAGE_CODE_TJ)}}</td>
        <td style="width: 450px;" class="bold">{{$safSideName}}</td>
    </tr>
    <tr>
        <td class="left_width"></td>
        <td style="width: 110px;"></td>
        <td style="width: 450px;" class="text_1">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.issued_by.description", ConstructorTemplates::LANGUAGE_CODE_TJ)}}</td>
    </tr>


    <tr>
        <td class="middle_width"></td>
    </tr>
    <tr>
        <td class="left_width"></td>
        <td style="width: 220px;">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.legal_address", ConstructorTemplates::LANGUAGE_CODE_TJ)}}</td>
        <td style="width: 340px" class="text_2 bold">{{$safSideCommunityAddress}}</td>
    </tr>


    <tr>
        <td class="middle_width"></td>
    </tr>
    <tr>
        <td class="left_width"></td>
        <td style="width: 110px;">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.general_info", ConstructorTemplates::LANGUAGE_CODE_TJ)}}</td>
        <td style="width: 450px;" class="bold">{{$safGeneralRegimeInfo_tj}}</td>
    </tr>
    <tr>
        <td class="left_width"></td>
        <td style="width: 110px;"></td>
        <td style="width: 450px;" class="text_1">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.general_info.description", ConstructorTemplates::LANGUAGE_CODE_TJ)}}</td>
    </tr>


    <tr>
        <td class="middle_width"></td>
    </tr>
    <tr>
        <td class="left_width"></td>
        <td style="width: 280px;">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.legal_entity", ConstructorTemplates::LANGUAGE_CODE_TJ)}}</td>
        <td style="width: 135px;" class="bold">{{$safControlledFieldsGeneralNuclearSafetySupervisor}}</td>
        <td style="width: 10px;"></td>
        <td style="width: 135px;" class="bold">{{$safSidePhoneNumber}}</td>
    </tr>
    <tr>
        <td class="left_width"></td>
        <td style="width: 280px;"></td>
        <td style="width: 135px;" class="text_1">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.fio", ConstructorTemplates::LANGUAGE_CODE_TJ)}}</td>
        <td style="width: 10px;"></td>
        <td style="width: 135px;" class="text_1">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.phone", ConstructorTemplates::LANGUAGE_CODE_TJ)}}</td>
    </tr>


    <tr>
        <td class="middle_width"></td>
    </tr>
    <tr>
        <td class="left_width"></td>
        <td style="width: 220px;">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.responsible_person", ConstructorTemplates::LANGUAGE_CODE_TJ)}}</td>
        <td style="width: 135px;" class="bold">{{$keyResponsiblePerson_tj}}</td>
        <td style="width: 70px;"></td>
        <td style="width: 135px;" class="bold"></td>
    </tr>
    <tr>
        <td class="left_width"></td>
        <td style="width: 220px;"></td>
        <td style="width: 135px;" class="text_1">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.fio", ConstructorTemplates::LANGUAGE_CODE_TJ)}}</td>
        <td style="width: 70px;"></td>
        <td style="width: 135px;" class="text_1">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.phone", ConstructorTemplates::LANGUAGE_CODE_TJ)}}</td>
    </tr>

    <tr>
        <td class="left_width"></td>
        <td class="middle_width"><b>{{$keyApplicationLabel_tj}} </b>{{$keyApplicationDescription_tj}}</td>
    </tr>
    <tr>
        <td class="left_width"></td>
        <td style="width: 100px;"></td>
        <td style="width: 460px;" class="bold">{{$keyGeneral_tj}}</td>
    </tr>


    <tr>
        <td class="middle_width"></td>
    </tr>
    <tr>
        <td class="left_width"></td>
        <td style="width: 100px;"></td>
        <td class="bold" style="width: 460px;">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.license_term", ConstructorTemplates::LANGUAGE_CODE_TJ) . $fieldIDNaRSL_6_tj . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.year", ConstructorTemplates::LANGUAGE_CODE_TJ)}}</td>
    </tr>


    <tr>
        <td class="middle_width"></td>
    </tr>
    <tr>
        <td class="left_width"></td>
        <td style="width: 110px;"></td>
        <td style="width: 450px;">{!! $keyDepartmentName_tj !!}</td>
    </tr>


    <tr>
        <td class="left_width"></td>
        <td style="width: 110px;"></td>
        <td style="width: 200px;">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.general_inspector", ConstructorTemplates::LANGUAGE_CODE_TJ)}}</td>
        <td style="width: 100px;"></td>
        <td style="width: 150px;" class="bold">{{$fieldIDNaRSL_7}}</td>
    </tr>
</table>

<?php
// 540
?>

@if (!empty($productsTableListForPages))

    @foreach($productsTableListForPages as $valuePages)

        <table cellspacing="0" cellpadding="1" border="0" pagebreak="true">


            <tr>
                <td style="height: 80px;"></td>
            </tr>
            <tr>
                <td style="width: 510px;"></td>
                <td style="width: 150px;">{{$keyOtherPageNumberText1}}</td>
            </tr>
            <tr>
                <td style="width: 510px;"></td>
                <td style="width: 150px;">{{$keyOtherPageNumberText2}}</td>
            </tr>


            <tr>
                <td class="op_middle_width"></td>
            </tr>
            <tr>
                <td class="op_left_width"></td>
                <td style="font-size: 14px;" class="tc bold op_middle_width">{!! $keyOtherPageTitle !!}</td>
            </tr>


            <tr>
                <td class="op_middle_width"></td>
            </tr>
            <tr>
                <td class="op_left_width"></td>
                <td class="op_middle_width bold text_2">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_label.license_issued", ConstructorTemplates::LANGUAGE_CODE_RU) . ' ' . $fieldIDNaRSL_5_ru . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.year", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
            </tr>


            <tr>
                <td class="op_middle_width"></td>
            </tr>
            <tr>
                <td class="op_left_width"></td>
                <td class="op_middle_width bold">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_label.addition_to_license", ConstructorTemplates::LANGUAGE_CODE_RU) . ' ' . $fieldIDNaRSL_19}}</td>
            </tr>
            <tr>
                <td class="op_left_width"></td>
                <td class="op_middle_width text_1">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_label.addition_to_license.description", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
            </tr>


            <tr>
                <td class="op_middle_width"></td>
            </tr>
            <tr>
                <td class="op_left_width"></td>
                <td class="op_middle_width bold text_2">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_label.license_holder", ConstructorTemplates::LANGUAGE_CODE_RU) . ' ' . $safSideName}}</td>
            </tr>


            <tr>
                <td class="op_middle_width"></td>
            </tr>
            <tr>
                <td class="op_left_width"></td>
                <td style="width: 120px;" class="bold">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_label.supervisor", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
                <td style="width: 420px;" class="bold">{{$safControlledFieldsGeneralNuclearSafetySupervisor}}</td>
            </tr>
            <tr>
                <td class="op_left_width"></td>
                <td style="width: 120px;"></td>
                <td style="width: 420px;" class="text_1">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_label.supervisor.description", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
            </tr>


            <tr>
                <td class="op_middle_width"></td>
            </tr>
            <tr>
                <td class="op_left_width"></td>
                <td style="width: 250px" class="bold">{{$safSideCommunityAddress}}</td>
                <td style="width: 40px"></td>
                <td style="width: 250px" class="bold">{{$safSidePhoneNumber}}</td>
            </tr>
            <tr>
                <td class="op_left_width"></td>
                <td style="width: 250px" class="text_1">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_label.address", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
                <td style="width: 40px"></td>
                <td style="width: 250px" class="text_1">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_label.phone", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
            </tr>


            <tr>
                <td class="op_middle_width"></td>
            </tr>
            <tr>
                <td class="op_left_width"></td>
                <td class="bold op_middle_width"><u>{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_label.NRSL.part_1", ConstructorTemplates::LANGUAGE_CODE_RU)}}</u></td>
            </tr>


            <tr>
                <td class="op_middle_width"></td>
            </tr>
            <tr>
                <td class="op_left_width"></td>
                <td style="width: 350px;" class="bold text_2">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_label.NRSL.part_2", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
                <td style="width: 95px;">{{$safControlledFieldsGeneralNuclearSafetyResponsibleOfficer}}</td>
                <td style="width: 95px;">{{$safSidePhoneNumber}}</td>
            </tr>
            <tr>
                <td class="op_left_width"></td>
                <td style="width: 350px;"></td>
                <td style="width: 95px;" class="text_1">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_label.name_surName", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
                <td style="width: 95px;" class="text_1">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_label.phone", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
            </tr>


            <tr>
                <td class="op_middle_width"></td>
            </tr>
            <tr>
                <td class="op_left_width"></td>
                <td class="bold op_middle_width">{{$fieldIDNaRSL_17}}</td>
            </tr>


            <tr>
                <td class="op_middle_width"></td>
            </tr>
            <tr>
                <td class="op_left_width"></td>
                <td class="bold op_middle_width">{{$keyExportImportList}}</td>
            </tr>

            <tr>
                <td class="op_middle_width"></td>
            </tr>

            <?php $indexProducts = 1; ?>
            @foreach ($valuePages as $keyProducts => $valueRows)
                    <tr>
                        <td class="op_left_width"></td>
                        <td class="op_middle_width"><table class="productTables">
                                @if ($indexProducts === 1)
                                    <tr>
                                        <td style="width: 10%;"></td>
                                        <td style="width: 10%;" class="bordered bold">{{$keyTableListNumber}}</td>
                                        <td style="width: 10%;" class="bordered bold">{{$keyTableListNuclide}}</td>
                                        <td style="width: 10%;" class="bordered bold">{{$keyTableListWeight}}</td>
                                        <td style="width: 10%;" class="bordered bold">{{$keyTableListForm}}</td>
                                        @if (count($valueRows) == 2)
                                            <td style="width: 10%;" class="bordered bold">{{$keyTableListNumber}}</td>
                                            <td style="width: 10%;" class="bordered bold">{{$keyTableListNuclide}}</td>
                                            <td style="width: 10%;" class="bordered bold">{{$keyTableListWeight}}</td>
                                            <td style="width: 10%;" class="bordered bold">{{$keyTableListForm}}</td>
                                            <td style="width: 10%;"></td>
                                        @endif
                                    </tr>
                                @endif
                                <tr>
                                    <td style="width: 10%;"></td>
                                    @foreach ($valueRows as $valueColumn)
                                        <td style="width: 10%;" class="bordered">{{$valueColumn['number']}}</td>
                                        <td style="width: 10%;" class="bordered">{{$valueColumn['NaRSL_21']}}</td>
                                        <td style="width: 10%;" class="bordered">{{$valueColumn['NaRSL_22']}}</td>
                                        <td style="width: 10%;" class="bordered">{{$valueColumn['NaRSL_23']}}</td>
                                    @endforeach
                                    <td style="width: 10%;"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                <?php $indexProducts++; ?>
            @endforeach


            <tr>
                <td class="op_middle_width"></td>
            </tr>
            <tr>
                <td class="op_left_width"></td>
                <td class="op_middle_width bold">{{$keyOtherPageStandardDocumentTitle}}</td>
            </tr>
            <tr>
                <td class="op_middle_width"></td>
            </tr>
            <tr>
                <td class="op_left_width"></td>
                <td class="op_middle_width bold">{{$keyOtherPageStandardDocumentText1}}</td>
            </tr>
            <tr>
                <td class="op_left_width"></td>
                <td class="op_middle_width bold">{{$keyOtherPageStandardDocumentText2}}</td>
            </tr>
            <tr>
                <td class="op_left_width"></td>
                <td class="op_middle_width bold">{{$keyOtherPageStandardDocumentText3}}</td>
            </tr>
            <tr>
                <td class="op_left_width"></td>
                <td class="op_middle_width bold">{{$keyOtherPageStandardDocumentText4}}</td>
            </tr>
            <tr>
                <td class="op_left_width"></td>
                <td class="op_middle_width bold">{{$keyOtherPageStandardDocumentText5}}</td>
            </tr>


            <tr>
                <td class="op_middle_width"></td>
            </tr>
            <tr>
                <td class="op_left_width"></td>
                <td style="width: 220px;">{{$keyOtherPageAgency}}</td>
                <td style="width: 170px;"></td>
                <td style="width: 150px;"></td>
            </tr>
            <tr>
                <td class="op_left_width"></td>
                <td class="tc bold" style="width: 220px;">{{$fieldIDNaRSL_5_other_page}}</td>
                <td style="width: 170px;"></td>
                <td style="width: 150px;">{{$fieldIDNaRSL_7}}</td>
            </tr>
        </table>

    @endforeach
@else

    <table cellspacing="0" cellpadding="1" border="0" pagebreak="true">


        <tr>
            <td style="height: 80px;"></td>
        </tr>
        <tr>
            <td style="width: 510px;"></td>
            <td style="width: 150px;">{{$keyOtherPageNumberText1}}</td>
        </tr>
        <tr>
            <td style="width: 510px;"></td>
            <td style="width: 150px;">{{$keyOtherPageNumberText2}}</td>
        </tr>


        <tr>
            <td class="op_middle_width"></td>
        </tr>
        <tr>
            <td class="op_left_width"></td>
            <td style="font-size: 14px;" class="tc bold op_middle_width">{!! $keyOtherPageTitle !!}</td>
        </tr>


        <tr>
            <td class="op_middle_width"></td>
        </tr>
        <tr>
            <td class="op_left_width"></td>
            <td class="op_middle_width bold text_2">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_label.license_issued", ConstructorTemplates::LANGUAGE_CODE_RU) . ' ' . $fieldIDNaRSL_5_ru . $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.label.year", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
        </tr>


        <tr>
            <td class="op_middle_width"></td>
        </tr>
        <tr>
            <td class="op_left_width"></td>
            <td class="op_middle_width bold">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_label.addition_to_license", ConstructorTemplates::LANGUAGE_CODE_RU) . ' ' . $fieldIDNaRSL_19}}</td>
        </tr>
        <tr>
            <td class="op_left_width"></td>
            <td class="op_middle_width text_1">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_label.addition_to_license.description", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
        </tr>


        <tr>
            <td class="op_middle_width"></td>
        </tr>
        <tr>
            <td class="op_left_width"></td>
            <td class="op_middle_width bold text_2">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_label.license_holder", ConstructorTemplates::LANGUAGE_CODE_RU) . ' ' . $safSideName}}</td>
        </tr>


        <tr>
            <td class="op_middle_width"></td>
        </tr>
        <tr>
            <td class="op_left_width"></td>
            <td style="width: 120px;" class="bold">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_label.supervisor", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
            <td style="width: 420px;" class="bold">{{$safControlledFieldsGeneralNuclearSafetySupervisor}}</td>
        </tr>
        <tr>
            <td class="op_left_width"></td>
            <td style="width: 120px;"></td>
            <td style="width: 420px;" class="text_1">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_label.supervisor.description", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
        </tr>


        <tr>
            <td class="op_middle_width"></td>
        </tr>
        <tr>
            <td class="op_left_width"></td>
            <td style="width: 250px" class="bold">{{$safSideCommunityAddress}}</td>
            <td style="width: 40px"></td>
            <td style="width: 250px" class="bold">{{$safSidePhoneNumber}}</td>
        </tr>
        <tr>
            <td class="op_left_width"></td>
            <td style="width: 250px" class="text_1">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_label.address", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
            <td style="width: 40px"></td>
            <td style="width: 250px" class="text_1">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_label.phone", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
        </tr>


        <tr>
            <td class="op_middle_width"></td>
        </tr>
        <tr>
            <td class="op_left_width"></td>
            <td class="bold op_middle_width text_2">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_label.NRSL.part_1", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
        </tr>


        <tr>
            <td class="op_middle_width"></td>
        </tr>
        <tr>
            <td class="op_left_width"></td>
            <td style="width: 350px;" class="bold text_2">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_label.NRSL.part_2", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
            <td style="width: 95px;">{{$safControlledFieldsGeneralNuclearSafetyResponsibleOfficer}}</td>
            <td style="width: 95px;">{{$safSidePhoneNumber}}</td>
        </tr>
        <tr>
            <td class="op_left_width"></td>
            <td style="width: 350px;"></td>
            <td style="width: 95px;" class="text_1">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_label.name_surName", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
            <td style="width: 95px;" class="text_1">{{$dictionaryManager->getTransByLngCode("swis.pdf.{$template}.other_label.phone", ConstructorTemplates::LANGUAGE_CODE_RU)}}</td>
        </tr>


        <tr>
            <td class="op_middle_width"></td>
        </tr>
        <tr>
            <td class="op_left_width"></td>
            <td class="bold op_middle_width">{{$fieldIDNaRSL_17}}</td>
        </tr>


        <tr>
            <td class="op_middle_width"></td>
        </tr>
        <tr>
            <td class="op_left_width"></td>
            <td class="bold op_middle_width">{{$keyExportImportList}}</td>
        </tr>


        <tr>
            <td class="op_middle_width"></td>
        </tr>
        <tr>
            <td class="op_left_width"></td>
            <td class="op_middle_width bold">{{$keyOtherPageStandardDocumentTitle}}</td>
        </tr>
        <tr>
            <td class="op_middle_width"></td>
        </tr>
        <tr>
            <td class="op_left_width"></td>
            <td class="op_middle_width bold">{{$keyOtherPageStandardDocumentText1}}</td>
        </tr>
        <tr>
            <td class="op_left_width"></td>
            <td class="op_middle_width bold">{{$keyOtherPageStandardDocumentText2}}</td>
        </tr>
        <tr>
            <td class="op_left_width"></td>
            <td class="op_middle_width bold">{{$keyOtherPageStandardDocumentText3}}</td>
        </tr>
        <tr>
            <td class="op_left_width"></td>
            <td class="op_middle_width bold">{{$keyOtherPageStandardDocumentText4}}</td>
        </tr>
        <tr>
            <td class="op_left_width"></td>
            <td class="op_middle_width bold">{{$keyOtherPageStandardDocumentText5}}</td>
        </tr>


        <tr>
            <td class="op_middle_width"></td>
        </tr>
        <tr>
            <td class="op_left_width"></td>
            <td style="width: 220px;">{{$keyOtherPageAgency}}</td>
            <td style="width: 170px;"></td>
            <td style="width: 150px;"></td>
        </tr>
        <tr>
            <td class="op_left_width"></td>
            <td class="tc bold" style="width: 220px;">{{$fieldIDNaRSL_5_other_page}}</td>
            <td style="width: 170px;"></td>
            <td style="width: 150px;">{{$fieldIDNaRSL_7}}</td>
        </tr>
    </table>

@endif