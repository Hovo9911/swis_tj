<?php

use App\Models\Document\Document;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\RegionalOffice\RegionalOffice;
use App\Models\Agency\Agency;
use App\Models\SingleApplicationProductsBatch\SingleApplicationProductsBatch;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use Illuminate\Support\Facades\DB;

$fieldValues = getFieldsValueByMDMID($documentId, $customData);

list($mainDataBySafId, $productsDataBySafId, $productBatchesDataBySafId) = getSafDataFieldIdValue($saf, $subApplication, $subAppProducts, $availableProductsIds);

// ----- 1.
$agencyName = $agency->current()->name;

$regionalOfficeInfo = $subDivision = $regionalOfficeName = $transCountriesList = $transportType = '';

$subdivisionId = $subApplication->agency_subdivision_id;
if (!is_null($subdivisionId)) {
    $subDivision = optional(getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME, $subdivisionId, false, ['code']));
}
$regionalOffice = RegionalOffice::select('regional_office_ml.name', 'regional_office.address', 'regional_office.phone_number')
    ->join('agency', 'regional_office.company_tax_id', '=', 'agency.tax_id')
    ->join('agency_ml', 'agency.id', '=', 'agency_ml.agency_id')
    ->join('regional_office_ml', 'regional_office.id', '=', 'regional_office_ml.regional_office_id')
    ->where('regional_office_ml.lng_id', '=', cLng('id'))
    ->where(['agency_ml.lng_id' => cLng('id'), 'agency.show_status' => Agency::STATUS_ACTIVE, 'office_code' => $subDivision->code])
    ->notDeleted()->first();

if (!is_null($regionalOffice)) {

    $regionalOfficeAddress = $regionalOffice->address;
    $regionalOfficePhoneNumber = $regionalOffice->phone_number;

    // ----- 2.
    $regionalOfficeInfo = $regionalOfficeAddress . ' ' . $regionalOfficePhoneNumber;

    // ----- 3.
    $regionalOfficeName = $regionalOffice->name;
}

$subApplicationSafData = $subApplication->data;

$safGeneralRegime = $saf->data['saf']['general']['regime'] ?? '';
$safSideName = $saf->data['saf']['sides']['import']['name'] ?? '';//SAF_ID_6
$transportType = $saf->data['saf']['transportation']['type_of_transport'] ?? '';//SAF_ID_75
$transitCountries = $saf->data['saf']['transportation']['transit_country'] ?? '';//SAF_ID_80
$safTransportationStateNumberCustomSupports = $saf->data['saf']['transportation']['state_number_customs_support'] ?? '';
$customsSupportTrackContainerTemperature = $subApplicationSafData['saf']['transportation']['state_number_customs_support'] ?? '';//SAF_ID_135
$customsSupportTrackContainerTemperatureInfo = [];

if (!empty($customsSupportTrackContainerTemperature)) {
    foreach ($customsSupportTrackContainerTemperature as $value) {
        if (!is_null($value['track_container_temperature'])) {
            $customsSupportTrackContainerTemperatureInfo[] = $value['track_container_temperature'];
        }
    }
}

$customsSupportTrackContainerTemperatureInfo = implode(', ', $customsSupportTrackContainerTemperatureInfo);

foreach ($safTransportationStateNumberCustomSupports as $safTransportationStateNumberCustomSupport) {
    if (!is_null($safTransportationStateNumberCustomSupport)) {
        $stateNumbers[] = $safTransportationStateNumberCustomSupport['state_number'];
        $customsSupport[] = $safTransportationStateNumberCustomSupport['customs_support'];
    }
}

$transportType = $transportType ? optional(getReferenceRows(ReferenceTable::REFERENCE_TRANSPORT_TYPE, $transportType))->name : '';

foreach ($transitCountries as $transitCountry) {
    if (!is_null($transitCountry)) {
        $transCountries[] = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $transitCountry))->name;
    }
}
if (!empty($transCountries)) {
    $transCountries = array_unique($transCountries);
    $transCountriesList = implode(', ', $transCountries);
}

$safTransportationExportCountryID = $saf->data['saf']['transportation']['export_country'] ?? '';//SAF_ID_78
$safTransportationExportCountry = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safTransportationExportCountryID, false))->name;

$safTransportationImportCountryID = $saf->data['saf']['transportation']['import_country'] ?? '';//SAF_ID_79
$safTransportationImportCountry = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safTransportationImportCountryID, false))->name;

$countriesList = $safTransportationExportCountry . ' - ' . ($transCountriesList ?? '') . ', ' . $safTransportationImportCountry;

// ----- 15.
$transportInfo = $transportType . ' ' . $countriesList . ' ' . $customsSupportTrackContainerTemperatureInfo;

$fieldIDVet_6 = $fieldIDVet_7 = $fromDay = $fromMonth = $fromYear = '';

if ($customData) {

    $fieldIDVet_6 = $customData['Veterinary_6'] ?? '';
    $fieldIDVet_7 = $customData['Veterinary_7'] ?? '';

    if (!empty($fieldIDVet_7)) {
        $fromDay = date('d', strtotime($fieldIDVet_7));
        $fromMonth = month(date('m', strtotime($fieldIDVet_7)));
        $fromYear = date('y', strtotime($fieldIDVet_7));
    }
}

$productInfo1 = $productsQuantity = $productBatchApprovedType = $comDesc = $measUnit = [];

$safID96 = $safID99 = 0;

foreach ($subAppProducts as $key => $subAppProduct) {

    $numberOfPlaces = $subAppProduct->number_of_places;
    $productPlaceType = optional(getReferenceRows(ReferenceTable::REFERENCE_PACKAGING_REFERENCE, $subAppProduct['places_type']))->name;
    $productBatches = SingleApplicationProductsBatch::select('approved_type')->where('saf_product_id', $subAppProduct->id)->get();

    foreach ($productBatches as $productBatch) {

        $productBatchApprovedType[] = optional(getReferenceRows(ReferenceTable::REFERENCE_TYPE_OF_PERMISSION, $productBatch->approved_type))->name;
    }

    $measurementUnit = optional(getReferenceRows(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $subAppProduct->measurement_unit, false, ['short_name']))->short_name;

    // ----- 8.
    if (in_array($subAppProduct['commercial_description'], $comDesc)) {

        foreach ($productInfo1 as $keyPI1 => &$valuePI1) {
            if ($valuePI1['commercial_description'] == $subAppProduct['commercial_description']) {
                $valuePI1['quantity'] = (int)optional($subAppProduct)->quantity + (int)$valuePI1['quantity'];
            }
        }

    } else {

        $comDesc[$key] = $subAppProduct['commercial_description'];
        $productInfo1[$key]['commercial_description'] = $subAppProduct['commercial_description'];
        $productInfo1[$key]['quantity'] = optional($subAppProduct)->quantity;
        $productInfo1[$key]['measurementUnit'] = $measurementUnit;

    }

    // ----- 9.
    if (in_array($measurementUnit, $measUnit)) {

        foreach ($productsQuantity as $keyPQ => &$valuePQ) {
            if ($valuePQ['measurementUnit'] == $measurementUnit) {
                $valuePQ['quantity'] = (int)optional($subAppProduct)->quantity + (int)$valuePQ['quantity'];
                $valuePQ['numberOfPlaces'] = (int)$numberOfPlaces + (int)$valuePQ['numberOfPlaces'];
            }
        }

    } else {
        $measUnit[$key] = $measurementUnit;
        $productsQuantity[$key]['quantity'] = optional($subAppProduct)->quantity;
        $productsQuantity[$key]['measurementUnit'] = $measurementUnit;
        $productsQuantity[$key]['numberOfPlaces'] = $numberOfPlaces;

    }

    $safID96 += (int)notEmptyForPdfFields($productsDataBySafId[$subAppProduct->id], 'SAF_ID_96');
}

//----- 7.
$safID92 = notEmptyForPdfFields($productsDataBySafId[$subAppProducts[0]->id], 'SAF_ID_92');
$safID95 = isset($subAppProducts[0]) ? $subAppProducts[0]['producer_name'] : '';
$productBatches = array_values($productBatchesDataBySafId)[0];
$safID109 = [];

foreach ($productBatches as $productBatch) {
    if (!is_null($productBatch['SAF_ID_109'])) {
        $safID109[] = formattedDate($productBatch['SAF_ID_109']);
    }
}
if (count($safID109)) {
    $safID109List = implode(', ', $safID109);
}

$point7 = $safID92 . ', ' . $safID95 . ' ' . (isset($safID109List) ? ', ' . $safID109List : '');
$point7 = getMappingValueInMultipleRows($point7, 70, 90);

// ----- 8.
$productInfo1List = [];

if (count($productInfo1) == 1) {
    $productInfo1List[] = $subAppProducts[0]['commercial_description'];
} else {

    foreach ($productInfo1 as $value) {
        $productInfo1List[] = $value['commercial_description'] . ' ' . $value['quantity'] . ' ' . $value['measurementUnit'];
    }
}

$productInfo1List = implode(', ', $productInfo1List);

// ----- 9.
$productsQuantityList = [];

foreach ($productsQuantity as $value) {
    $productsQuantityList[] = $value['quantity'] . ' ' . $value['measurementUnit'];
}
$productsQuantityList = implode('<br>', $productsQuantityList);

// ----- 10.
$productPlaceType = '';
if (!empty($subAppProducts[0]['places_type'])) {
    $productPlaceType = optional(getReferenceRows(App\Models\ReferenceTable\ReferenceTable::REFERENCE_PACKAGING_REFERENCE, $subAppProduct['places_type']))->name;
}

// ----- 11.
$mdmId95 = notEmptyForPdfFields($fieldValues, '95');

// ----- 12.
$mdmId500 = notEmptyForPdfFields($fieldValues, '500');

// ----- 14.
$productBatchApprovedTypePart1 = '';
$productBatchApprovedType = array_unique($productBatchApprovedType);
$productBatchApprovedType = implode(', ', $productBatchApprovedType);
if (!empty($productBatchApprovedType)) {
    $productBatchApprovedType = str_split_unicode($productBatchApprovedType, 80, 140);
    $productBatchApprovedTypePart1 = $productBatchApprovedType[0];
}

// ----- 16.
$safSidesImportCountry = $saf->data['saf']['sides']['import']['country'] ?? '';//SAF_ID_7
$safSidesImportCountryName = !empty($safSidesImportCountry) ? optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safSidesImportCountry))->name : '';
$safSideCommunity = $saf->data['saf']['sides']['import']['community'] ?? '';//SAF_ID_8
$safSideAddress = $saf->data['saf']['sides']['import']['address'] ?? '';//SAF_ID_9
$safSideInfo = $safSideName . ' - ' . $safSidesImportCountryName . ' ' . $safSideCommunity . ' ' . $safSideAddress;

// ----- 17.
$transPortDocumentCodes = DB::table(ReferenceTable::REFERENCE_DOCUMENT_DEPENDING_TRANSPORT)->where('show_status', ReferenceTable::STATUS_ACTIVE)->pluck('document_code')->all();
$attachedDocumentProducts = SingleApplicationSubApplications::getAttachedDocumentProductsForPDF($subApplication, $availableProductsIds);
foreach ($attachedDocumentProducts as $value) {
    if (in_array($value->document->document_type, $transPortDocumentCodes)) {
        $documents[] = $value->document->document_number . ' ' . $value->document->document_release_date;
    }
}
$point11 = isset($documents) ? implode(', ', ($documents ?? '')) : '';

// ----- 18.
$mdmId467 = notEmptyForPdfFields($fieldValues, '467');

// ----- 20.
$attachedDocumentProducts = SingleApplicationSubApplications::getAttachedDocumentProductsForPDF($subApplication, $availableProductsIds);
foreach ($attachedDocumentProducts as $value) {

    if ($value->document->document_type == Document::DOCUMENT_CODE_6012) {
        $documents6012[] = $value->document->document_number . ' ' . $value->document->document_release_date;
    }

}
if ($mainDataBySafId['SAF_ID_1'] == SingleApplication::REGIME_IMPORT) {
    $point20 = isset($documents6012) ? implode(', ', ($documents6012 ?? '')) : '';
} else {
    $point20 = '';
}

// ----- 21.
$mdmId199 = notEmptyForPdfFields($fieldValues, '199');
$mdmId199 = str_split_unicode($mdmId199, 140);

// ----- 24.
$mdmId382 = notEmptyForPdfFields($fieldValues, '382');

?>
<table cellspacing="0" cellpadding="1" border="0">
    <tr>
        <td style="height:30px"></td>
    </tr>
</table>
<table>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:50%;vertical-align: top;text-align:left;font-size: 11px;">{{$agencyName}}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;width:48%;text-align:left;line-height: 1.6;">
            <span>Субъект, Ҷумҳурии Тоҷикистон / субъект, Республика Таджикистан </span>
        </td>
    </tr>
    <tr style="line-height:1.0">
        <td></td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:50%;vertical-align: top;text-align:left;font-size: 11px;">{{$regionalOfficeInfo}}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;width:48%;text-align:center;line-height: 1.6;">
            <span>(ноҳия, шаҳр / район, город) </span>
        </td>
    </tr>
    <tr style="line-height:1.0">
        <td></td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:50%;vertical-align: top;text-align:left;font-size: 11px;">{{$regionalOfficeName}}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;width:48%;text-align:center;line-height: 1.6;">
            <span>(номи муассиса / наименование учреждения) </span>
        </td>
    </tr>
</table>
<table cellspacing="0" cellpadding="1" border="0">
    <tr>
        <td style="height:100px"></td>
    </tr>
</table>
<table>
    <tr>
        <td style="font-size: 11px;width:380px;font-weight:bold;"></td>
        <td style="border-bottom: 1px solid #000000;vertical-align: top;text-align:center;font-size: 11px;width:40px;">
            «{{$fromDay}}»
        </td>
        <td style="font-size: 11px;width:5px;font-weight:bold;"></td>
        <td style="border-bottom: 1px solid #000000;vertical-align: top;text-align:center;font-size: 11px;width:150px;">{{$fromMonth}}</td>
        <td style="font-size: 11px;width:35px;font-weight:bold;">20{{$fromYear}}</td>
        <td style="font-size: 11px;width:65px;"> сол/год</td>
    </tr>
    <tr style="line-height:0.8">
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:105%;line-height: 1.6;text-align:center;">Ман, духтури байтории дар поён
            имзокарда, шаҳодатномаи байтории мазкурро ба/<br> Я нижеподписавшийся ветеринарный врач, выдал настоящее
            ветеринарное свидетельство
        </td>
    </tr>
    <tr style="line-height:0.5">
        <td></td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:100%;vertical-align: top;text-align:left;font-size: 11px;">{{$safSideName}}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;width:100%;text-align:center;line-height: 1.6;">
            <span>(ба кӣ-номи шахси юридикӣ ё ному насаби шахси воқеӣ / кому, наименования юридических лиц или Ф,И,О физических лиц) </span>
        </td>
    </tr>
    <tr style="line-height:0.5">
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;;width:22%;">дар бобати он, ки / в том, что</td>
        <td style="border-bottom: 1px solid #000000;vertical-align: top;text-align:left;font-size: 11px;width:78%;">{!! $productInfo1List !!}</td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 1.6;text-align:left;width:30%;"></td>
        <td style="font-size: 8px;text-align:center;line-height: 1.6;width:70%;">
            <span>(номгӯи ашёи хоми техникӣ ва хӯрока / наименование технического сырья и корма)</span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:130px;">ба микдори / в количестве</td>
        <td style="border-bottom: 1px solid #000000;vertical-align: top;text-align:left;font-size: 11px;width:535px">
            <table>
                <tr>
                    <td style="width:28%;text-align:center">{!! $productsQuantityList !!}</td>
                    <td style="width:44%;text-align:center">{!! $safID96." ".$productPlaceType  !!}</td>
                    <td style="width:23%;text-align:center">{{$mdmId95}}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 1.6;text-align:left;width:130px"></td>
        <td style="font-size: 8px;text-align:center;line-height: 1.6;width:535px">
            <table>
                <tr>
                    <td style="width:28%;text-align:center">(ҷой, дона, кг / мест, штук, кг)</td>
                    <td style="width:44%;text-align:center">(бастабанд / упаковка)</td>
                    <td style="width:23%;text-align:center">(тамгазанӣ / маркировка)</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;;width:20%;">пайдоиш / происхождение</td>
        <td style="border-bottom: 1px solid #000000;vertical-align: top;text-align:left;font-size: 11px;width:80%;">{{$mdmId500}}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:left;line-height: 1.6;width:100%;"><span>(дар салоҳхона куштагӣ, ҳаром мурдагӣ, омехта, аз ҳайвони касал ё сиҳат / боенское, палое, сборное полученное от здоровых или больных животных)</span>
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:105%;line-height: 1.6;">дар таҳти назорати мақомоти ваколатдори байторӣ коркард
            (ҷамъоварӣ) шудааст / выработано (заготовлено)
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;;width:40%;">под контролем уполномоченного ветеринарного органа</td>
        <td style="border-bottom: 1px solid #000000;text-align:left;font-size: 11px;width:60%;">{{$point7[0]}}</td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 1.6;text-align:left;width:55%;"></td>
        <td style="font-size: 8px;text-align:center;line-height: 1.6;width:45%;">
            <span></span>
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:100%;text-align:left;font-size: 11px;">{{$point7[1] ?? '' }}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;width:100%;text-align:center;line-height: 1.6;">
            <span>(номи корхона, ному насаб, соҳиби маҳсулот, суроға / наименование предприятия, ф.и.о. владельца, адрес)  </span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;;width:32%;">ва муносиб барои / и признано годным для</td>
        <td style="border-bottom: 1px solid #000000;vertical-align: top;text-align:left;font-size: 11px;width:68%;">{{$productBatchApprovedTypePart1}}</td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 1.6;text-align:left;width:15%;"></td>
        <td style="font-size: 8px;text-align:center;line-height: 1.6;width:85%;">
            <span>(фурӯш, аз нав коркард, истифодабарӣ бемаҳдудият / реализация, переработка, использования без ограничений</span>
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:100%;vertical-align: top;text-align:left;font-size: 11px;">{{($fieldValues[501] ?? '')}}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;width:100%;text-align:center;line-height: 1.6;">
            <span>(бо маҳдудият – бо нишондоди сабабхо ва реча / если с ограничениями указать причины и режим) </span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:150px;text-align:left;line-height: 1.6;">ва тариқи / и направляется</td>
        <td style="border-bottom: 1px solid #000000;vertical-align: top;text-align:left;font-size: 11px;width:516px;line-height: 1.6;">{{$transportInfo}}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;line-height: 1.6;width:100%;">
            <span>(намуди нақлиёт, самти хатсайр, шарти кашонидан / вид транспорта, маршрут следования, условия перевозки) </span>
        </td>
    </tr>
    <tr>
        <td style="text-align:left;font-size: 11px;width:35px;line-height: 1.6;">ба / в</td>
        <td style="border-bottom: 1px solid #000000;vertical-align: top;text-align:left;font-size: 11px;width:632px;line-height: 1.6;">{{$safSideInfo}}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;line-height: 1.6;width:100%;">
            <span>(ном ва суроғаи шахси қабулкунанда / наименование и адрес получателя)</span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:90px;text-align:left;line-height: 1.6;">мувофики / по</td>
        <td style="border-bottom: 1px solid #000000;vertical-align: top;text-align:center;font-size: 11px;width:577px;line-height: 1.6;">{{$point11}}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;text-align:center;line-height: 1.6;width:100%;">
            <span>(№ ва мӯҳлати додани ҳуҷҷатҳои молию нақлиёти / № и дата выдачи товаротранспортного документа)</span>
        </td>
    </tr>
    <tr>
        <td style="width:100%;vertical-align: top;text-align:center;font-size: 11px;line-height:1.4"></td>
    </tr>

    <tr>
        <td style="font-size: 11px;;width:55%;">Ашёи хом (хӯрока) аз коркарди / Сырьё (корма) подвергнуты переработке
        </td>
        <td style="border-bottom: 1px solid #000000;vertical-align: top;text-align:center;font-size: 11px;width:45%;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 1.6;text-align:left;width:15%;"></td>
        <td style="font-size: 8px;text-align:right;line-height: 1.6;width:85%;">
            <span>(безараргардонӣ, шустушӯй аз вайроншавӣ нигоҳ доштан / дезинфекция, мойка, консервация – </span>
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:100%;vertical-align: top;text-align:center;font-size: 11px;">{{$mdmId467}}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;width:100%;text-align:center;line-height: 1.6;">
            <span>бо нишондоди усул ва номи мавод / указать метод и наименование препаратов)  </span>
        </td>
    </tr>
    <tr style="line-height:0.5">
        <td></td>
    </tr>

    <tr>
        <td style="font-size: 13px;;width:43%;">ИШОРАҲОИ МАХСУС / ОСОБЫЕ ОТМЕТКИ</td>
        <td style="border-bottom: 1px solid #000000;vertical-align: top;text-align:left;font-size: 11px;width:57%;">{{$fieldValues[437] ?? ''}}</td>
    </tr>
    <tr>
        <td style="font-size: 11px;line-height: 1.6;text-align:left;width:45%;"></td>
        <td style="font-size: 8px;text-align:center;line-height: 1.6;width:55%;">
            <span>(ҳолати эпизоотии маҳал/указываются эпизоотическое благополучия местности</span>
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #000000;width:100%;vertical-align: top;text-align:left;font-size: 11px;">{{$point20 ?? '' }}</td>
    </tr>
    <tr>
        <td style="font-size: 8px;width:100%;text-align:center;line-height: 1.6;">
            <span>таърих ва рақами ичозатномаи нозири давлатии роҳбариқунанда / дата и номера разрешений вышестоящего госуветинспектора</span>
        </td>
    </tr>
    @if (empty($mdmId199))
        <tr>
            <td style="border-bottom: 1px solid #000000;width:100%;vertical-align: top;text-align:center;font-size: 11px;"></td>
        </tr>
        <tr>
            <td style="font-size: 8px;width:100%;text-align:center;line-height: 1.6;">
                <span>барои баровардани маҳсулот берун аз ҳудуди кишвар / на вывоз продукции за пределы территории республики</span>
            </td>
        </tr>
    @else
        @foreach($mdmId199 as $key => $value)
            <tr>
                <td style="border-bottom: 1px solid #000000;width:100%;vertical-align: top;text-align:left;font-size: 11px;">{{$value}}</td>
            </tr>
            @if ($key == 0)
                <tr>
                    <td style="font-size: 8px;width:100%;text-align:center;line-height: 1.6;">
                        <span>барои баровардани маҳсулот берун аз ҳудуди кишвар / на вывоз продукции за пределы территории республики</span>
                    </td>
                </tr>
            @else
                <tr>
                    <td style="font-size: 8px;width:100%;text-align:center;line-height: 1.6;">
                        <span></span>
                    </td>
                </tr>
            @endif
        @endforeach
    @endif
    <tr>
        <td style="width:100%;vertical-align: bottom;text-align:left;font-size: 11px;">{{($fieldValues[464] ?? '')}}</td>
    </tr>
    <tr>
        <td style="border-top: 1px solid #000000;font-size: 8px;width:100%;text-align:center;line-height: 1.6;">
            <span>№ тамга ва г / перечисляются № клейм и др.) </span>
        </td>
    </tr>
    <tr>
        <td style="width:100%;line-height:0.5"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:100%;line-height: 1.6;text-align:left;">Намуди наклиёт тоза ва безарар карда
            шудааст / Транспортное средство очищено и продезинфицировано.
        </td>
    </tr>
    <tr>
        <td style="width:100%;line-height:0.5;"></td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:100%;line-height: 1.6;">Шаҳодатнома дар вақти назорати бор дар давоми роҳ
            пешниҳод карда шуда, ба гирандаи бор дода мешавад. Нусхаҳои шаҳодатнома эътибор надоранд. Дар ҳолати муайян
            кардани вайронкунии тартиби пуркунии варақа шаҳодатнома ба сарнозири давлатии байтории субъекти Ҷумҳурии
            Тоҷикистон аз рӯи маҳали содирот ва нишондоди қойдавайронкунӣ супорида мешавад/ Свидетельство предъявляется
            для контроля при погрузке, в пути следования и передаётся грузополучателю. Копии свидетельства
            недействительны. При установлении нарушений порядка заполнения бланка свидетельство передаётся главному
            госветинспектору субъекта Республики Таджикистан по месту выдачи с указанием выявленных нарушений.
        </td>
    </tr>
    <tr style="line-height:2.0">
        <td></td>
    </tr>
    <tr>
        <td style="width:50%"></td>
        <td style="font-size: 12px;font-weight:bold;width:50%;line-height: 1.6;">
            Духтури байторӣ<br>
            Ветеринарный врач
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:200px;line-height: 1.6;">
            <table>
                <tr>
                    <td style="font-size: 11px;line-height: 1.6;text-align:left;font-weight:bold;">
                        Ҷ.М.
                    </td>
                </tr>
            </table>
        </td>
        <td style="border-bottom: 1px solid #000000;vertical-align: top;text-align:center;font-size: 11px;width:420px">{{($fieldValues[420] ?? '')}}</td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.6;text-align:left;"></td>
        <td style="font-size: 9px;text-align:center;line-height: 1.6;width:400px">
            <span>(имзо, номи пурраи вазифа/подпись, полное наименование должности)</span>
        </td>
    </tr>
    <tr>
        <td style="font-size: 11px;width:200px;line-height: 1.6;">
            <table>
                <tr>
                    <td style="font-size: 11px;line-height: 1.6;text-align:left;font-weight:bold;">
                        М.П.
                    </td>
                </tr>
            </table>
        </td>
        <td style="border-bottom: 1px solid #000000;vertical-align: top;text-align:center;font-size: 11px;width:420px">{{$mdmId382}}</td>
    </tr>
    <tr>
        <td style="font-size: 13px;line-height: 1.6;text-align:left;"></td>
        <td style="font-size: 9px;text-align:center;line-height: 1.6;width:400px">
            <span>(ному насаб/ ф.и.о.)</span>
        </td>
    </tr>
</table>