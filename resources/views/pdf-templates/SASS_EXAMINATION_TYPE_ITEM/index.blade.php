<?php

use App\Http\Controllers\Core\Dictionary\DictionaryManager;
use App\Models\BaseModel;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\RegionalOffice\RegionalOffice;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationExaminations\SingleApplicationExaminations;

$gerbImagePath = '/image/gerb_' . env('COUNTRY_MODE') . '_sm_2.png';

$fieldIDSAS_001_26 = $customData['SAS_001_26'] ?? '';
$fieldIDSAS_001_52 = $customData['SAS_001_52'] ?? '';
$fieldIDSAS_001_53 = $customData['SAS_001_53'] ?? '';
$fieldIDSAS_001_54 = $customData['SAS_001_54'] ?? '';
$fieldIDSAS_001_57 = $customData['SAS_001_57'] ?? '';
$fieldIDSAS_001_58 = $customData['SAS_001_58'] ?? '';
$fieldIDSAS_001_59 = $customData['SAS_001_59'] ?? '';
$fieldIDSAS_001_60 = $customData['SAS_001_60'] ?? '';

$subdivisionId = optional($subApplication)->agency_subdivision_id;
$subDivision = optional(getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME, $subdivisionId))->code;

$regionalOffice = RegionalOffice::select('regional_office.*', 'regional_office_ml.name')
    ->leftJoin('regional_office_ml', 'regional_office.id', '=', 'regional_office_ml.regional_office_id')
    ->where(['office_code' => $subDivision, 'regional_office_ml.lng_id' => BaseModel::LANGUAGE_CODE_TJ])->active()->first();

$agencyInfo = optional($regionalOffice)->region
    . ', ' . optional($regionalOffice)->city
    . ', ' . optional($regionalOffice)->area
    . ', ' . optional($regionalOffice)->street
    . ', тел:' . optional($regionalOffice)->phone_number
    . ', факс:' . optional($regionalOffice)->fax
    . ', E-mail:' . optional($regionalOffice)->email
    . ', ' . optional($agency)->website_url;

$regionalOfficeName = mb_strtoupper(optional($regionalOffice)->name, 'UTF-8');
$dictionaryManager = new DictionaryManager();

if (!empty($fieldIDSAS_001_54)) {
    $fromDay = date('d', strtotime($fieldIDSAS_001_54));
    $fromMonth = month(date('m', strtotime($fieldIDSAS_001_54)), BaseModel::LANGUAGE_CODE_TJ);
    $fromYear = date('y', strtotime($fieldIDSAS_001_54));
}

$safGeneralRegime = $saf->data['saf']['general']['regime'] ?? '';
$departure = getReferenceRows(ReferenceTable::REFERENCE_CUSTOM_BODY_REFERENCE, $saf->data['saf']['transportation']['departure'], false, [], false, false, false, [], false, BaseModel::LANGUAGE_CODE_TJ)->name;

switch ($safGeneralRegime) {
    case SingleApplication::REGIME_IMPORT:
        $safSideName = $saf->data['saf']['sides']['import']['name'] ?? '';
        $safSideAddress = $saf->data['saf']['sides']['import']['address'] ?? '';
        $safSideCommunity = $saf->data['saf']['sides']['import']['community'] ?? '';
        break;
    case SingleApplication::REGIME_EXPORT:
        $safSideName = $saf->data['saf']['sides']['export']['name'] ?? '';
        $safSideAddress = $saf->data['saf']['sides']['export']['address'] ?? '';
        $safSideCommunity = $saf->data['saf']['sides']['export']['community'] ?? '';
        break;
    default:
        $safSideName = '';
        $safSideAddress = '';
        $safSideCommunity = '';
        break;
}

$productsPart = '';
$productNumbers = $subApplication->productsOrderedList();
$totalQuantity = $totalNettoWeight = 0;

foreach ($subAppProducts as $subAppProduct) {

    $totalQuantity += is_numeric($subAppProduct['quantity']) ? $subAppProduct['quantity'] : 0;
    $totalNettoWeight += is_numeric($subAppProduct['netto_weight']) ? ($subAppProduct['netto_weight'] * 1000) : 0;

    $productsPart .= ' <tr>
            <td class="center">' . $productNumbers[$subAppProduct['id']] . '</td>
            <td>' . $subAppProduct['commercial_description'] . '</td>
            <td class="center">' . $subAppProduct['quantity'] . '</td>
            <td class="center">' . $subAppProduct['netto_weight'] * 1000 . '</td>
        </tr>';
}

$examinations = SingleApplicationExaminations::select('saf_examinations.*', 'saf_products.commercial_description')
    ->leftJoin('saf_products', 'saf_products.id', '=', 'saf_examinations.saf_product_id')
    ->where('saf_subapplication_id', $applicationId)
    ->whereIn('saf_examinations.saf_product_id', $availableProductsIds)->active()->get();

$productsInfo = [];

$agExaminationSection = $goldExaminationSection = $platinumExaminationSection = $palladiumExaminationSection = $bijouterieExaminationSection = $metalType30PercentSilverExaminationSection = null;

$totalExaminationSilverQuantity = $totalExaminationSilverWeight = $totalExaminationGoldQuantity = $totalExaminationGoldWeight =
$totalExaminationPlatinumQuantity = $totalExaminationPlatinumWeight = $totalExaminationPalladiumQuantity = $totalExaminationPalladiumWeight =
$totalExaminationSilver30PercentQuantity = $totalExaminationSilver30PercentWeight = $totalExaminationBijouterieQuantity = $totalExaminationBijouterieWeight = 0;

foreach ($examinations as $examination) {

    $typeOfStoneInfo = [];
    $typeOfStones = (!empty($examination->type_of_stone)) ? explode(',', $examination->type_of_stone) : '';

    if (!empty($typeOfStones)) {
        foreach ($typeOfStones as $typeOfStone) {
            $typeOfStoneRef = getReferenceRows(ReferenceTable::REFERENCE_TYPE_OF_STONES, $typeOfStone, false, ['code', 'name'], false, false, false, [], false, BaseModel::LANGUAGE_CODE_TJ) ?? '';
            $typeOfStoneInfo[] = optional($typeOfStoneRef)->name;
        }
        $typeOfStoneInfo = implode(', ', $typeOfStoneInfo);
    }

    $typeOfStoneInfo = (!is_array($typeOfStoneInfo)) ? $typeOfStoneInfo : '';
    $typeOfMetalRef = getReferenceRows(ReferenceTable::REFERENCE_TYPE_OF_METAL, $examination['type_of_metal'], false, ['code','name', 'sample']);

    $typeOfMetalCode = optional($typeOfMetalRef)->code;
    $typeOfMetalName = optional($typeOfMetalRef)->name;
    $typeOfMetalSample = optional($typeOfMetalRef)->sample;

    $typeOfGood = getReferenceRows(App\Models\ReferenceTable\ReferenceTable::REFERENCE_TYPES_OF_PRODUCTS, $examination['type_of_good']) ?? '';

    if (!isset($productsInfo[$examination['saf_product_id']][$typeOfMetalCode][$typeOfStoneInfo]['type_of_good'])) {
        $productsInfo[$examination['saf_product_id']][$typeOfMetalCode][$typeOfStoneInfo]['type_of_good'] = optional($typeOfGood)->code.', '.optional($typeOfGood)->name;
    }

    if (!empty($typeOfStoneInfo)) {
        $examinationCarat = (!is_null($examination['carat'])) ? $examination['carat'] : 0;
        if (isset($productsInfo[$examination['saf_product_id']][$typeOfMetalCode][$typeOfStoneInfo]['carat'])) {
            $productsInfo[$examination['saf_product_id']][$typeOfMetalCode][$typeOfStoneInfo]['carat'] += $examinationCarat;
        } else {
            $productsInfo[$examination['saf_product_id']][$typeOfMetalCode][$typeOfStoneInfo]['carat'] = $examinationCarat;
        }
    }

    if (isset($productsInfo[$examination['saf_product_id']][$typeOfMetalCode][$typeOfStoneInfo]['quantity'])) {
        $productsInfo[$examination['saf_product_id']][$typeOfMetalCode][$typeOfStoneInfo]['quantity'] += $examination['quantity'];
    } else {
        if (!is_null($examination['quantity'])) {
            $productsInfo[$examination['saf_product_id']][$typeOfMetalCode][$typeOfStoneInfo]['quantity'] = $examination['quantity'];
        }
    }

    if (isset($productsInfo[$examination['saf_product_id']][$typeOfMetalCode][$typeOfStoneInfo]['weight'])) {
        $productsInfo[$examination['saf_product_id']][$typeOfMetalCode][$typeOfStoneInfo]['weight'] += $examination['weight'];
    } else {
        if (!is_null($examination['weight'])) {
            $productsInfo[$examination['saf_product_id']][$typeOfMetalCode][$typeOfStoneInfo]['weight'] = $examination['weight'];
        }
    }
    $productsInfo[$examination['saf_product_id']][$typeOfMetalCode][$typeOfStoneInfo]['sample'] = $typeOfMetalSample;
    $productsInfo[$examination['saf_product_id']][$typeOfMetalCode][$typeOfStoneInfo]['metal_code_and_name'] = $typeOfMetalCode.', '.$typeOfMetalName;
}

if (count($productsInfo) > 0) {
    $silverNum = $goldNum = $platinumNum = $palladiumNum = $silver30percentNum = $bijouterieNum = 1;
    foreach ($productsInfo as $productInfo) {
        foreach ($productInfo as $typeOfMetal => $products) {

            switch (substr($typeOfMetal, 0, 2)) {
                case ReferenceTable::REFERENCE_METAL_TYPE_SILVER:
                    foreach ($products as $stoneTypes => $product) {

                        $totalExaminationSilverQuantity += $product['quantity'];
                        $totalExaminationSilverWeight += $product['weight'];

                        $agExaminationSection .= '<tr>
<td class="center">' . $silverNum++ . '</td>
<td>' . $product['type_of_good'] . '</td>
<td>' . $product['metal_code_and_name'] . '</td>
<td class="center">' . $product['quantity'] . '</td>
<td class="center">' . $product['weight'] . '</td>
<td class="center">' . $product['sample'] . '</td>
</tr>';
                    };
                    break;
                case ReferenceTable::REFERENCE_METAL_TYPE_GOLD:
                    foreach ($products as $stoneTypes => $product) {

                        $totalExaminationGoldQuantity += $product['quantity'];
                        $totalExaminationGoldWeight += $product['weight'];

                        $goldExaminationSection .= '<tr>
<td class="center">' . $goldNum++ . '</td>
<td>' . $product['type_of_good'] . '</td>
<td>' . $product['metal_code_and_name'] . '</td>
<td class="center">' . $product['quantity'] . '</td>
<td class="center">' . $product['weight'] . '</td>
<td class="center">' . $product['sample'] . '</td>
</tr>';
                    };
                    break;
                case ReferenceTable::REFERENCE_METAL_TYPE_PLATINUM:
                    foreach ($products as $stoneTypes => $product) {

                        $totalExaminationPlatinumQuantity += $product['quantity'];
                        $totalExaminationPlatinumWeight += $product['weight'];

                        $platinumExaminationSection .= '<tr>
<td class="center">' . $platinumNum++ . '</td>
<td>' . $product['type_of_good'] . '</td>
<td>' . $product['metal_code_and_name'] . '</td>
<td class="center">' . $product['quantity'] . '</td>
<td class="center">' . $product['weight'] . '</td>
<td class="center">' . $product['sample'] . '</td>
</tr>';
                    };
                    break;
                case ReferenceTable::REFERENCE_METAL_TYPE_PALLADIUM:
                    foreach ($products as $stoneTypes => $product) {

                        $totalExaminationPalladiumQuantity += $product['quantity'];
                        $totalExaminationPalladiumWeight += $product['weight'];

                        $palladiumExaminationSection .= '<tr>
<td class="center">' . $palladiumNum++ . '</td>
<td>' . $product['type_of_good'] . '</td>
<td>' . $product['metal_code_and_name'] . '</td>
<td class="center">' . $product['quantity'] . '</td>
<td class="center">' . $product['weight'] . '</td>
<td class="center">' . $product['sample'] . '</td>
</tr>';
                    };
                    break;
                case ReferenceTable::REFERENCE_METAL_TYPE_30_PERCENT_SILVER:
                    foreach ($products as $stoneTypes => $product) {

                        $totalExaminationSilver30PercentQuantity += $product['quantity'];
                        $totalExaminationSilver30PercentWeight += $product['weight'];

                        $metalType30PercentSilverExaminationSection .= '<tr>
<td class="center">' . $silver30percentNum++ . '</td>
<td>' . $product['type_of_good'] . '</td>
<td>' . $product['metal_code_and_name'] . '</td>
<td class="center">' . $product['quantity'] . '</td>
<td class="center">' . $product['weight'] . '</td>
</tr>';
                    };
                    break;
                case ReferenceTable::REFERENCE_METAL_TYPE_BIJOUTERIE:
                    foreach ($products as $stoneTypes => $product) {

                        $totalExaminationBijouterieQuantity += $product['quantity'];
                        $totalExaminationBijouterieWeight += $product['weight'];

                        $bijouterieExaminationSection .= '<tr>
<td class="center">' . $bijouterieNum++ . '</td>
<td>' . $product['type_of_good'] . '</td>
<td>' . $product['metal_code_and_name'] . '</td>
<td class="center">' . $product['quantity'] . '</td>
<td class="center">' . $product['weight'] . '</td>
<td class="center">' . $product['sample'] . '</td>
</tr>';
                    };
                    break;
            }
        }
    }
}

?>
<table>
    <tr>
        <td style="width:40%"></td>
        <td style="width:20%">
            <img class="gerb" src="<?php echo e($gerbImagePath); ?>"/>
        </td>
        <td style="width:40%"></td>
    </tr>
</table>
<table>
    <tr>
        <td class="title">{{$regionalOfficeName}}</td>
    </tr>
    <tr>
        <td></td>
    </tr>
</table>
<table class="bigHr">
</table>
<table>
    <tr>
        <td></td>
    </tr>
</table>
<table class="agency_info" cellpadding="5">
    <tr>
        <td class="mapping">{{$agencyInfo}}</td>
    </tr>
</table>
<table>
    <tr>
        <td class="right_side">{{trans("swis.pdf_templates.{$template}.header_first_row")}}</td>
    </tr>
    <tr>
        <td class="right_side">{{trans("swis.pdf_templates.{$template}.header_second_row")}}</td>
    </tr>
    <tr>
        <td class="right_side">{{trans("swis.pdf_templates.{$template}.header_third_row")}}</td>
    </tr>
    <tr>
        <td class="right_side">{{trans("swis.pdf_templates.{$template}.header_fourth_row")}}</td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td class="right_side"><u>{{trans("swis.pdf_templates.{$template}.header_six_row")}}</u></td>
    </tr>
    <tr>
        <td class="right_side">{{trans("swis.pdf_templates.{$template}.header_seven_row")}}</td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td class="center"><span
                    class="fix_text">{{trans("swis.pdf_templates.{$template}.key_1")}}</span>{{" ".$fieldIDSAS_001_53}}
        </td>
    </tr>
    <tr>
        <td>
            {{trans("swis.pdf_templates.{$template}.key_2")}}
            {{" ".$safSideName." "}}
            {{trans("swis.pdf_templates.{$template}.key_3")}}
            {{" ".$departure." "}}
            {{trans("swis.pdf_templates.{$template}.key_4")}}
            {{" ".$fieldIDSAS_001_60." "}}
            {{trans("swis.pdf_templates.{$template}.key_5")}}
            {{" ".$fieldIDSAS_001_59." "}}
            {{trans("swis.pdf_templates.{$template}.key_6")}}
        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
</table>
<table>
    <tr>
        <td width="5%"></td>
        <td width="95%">
            <table border="1" cellpadding="3">
                <tr>
                    <td class="table-header" width="5%">№</td>
                    <td class="table-header" width="50%">{{trans("swis.pdf_templates.{$template}.key_7")}}</td>
                    <td class="table-header" width="25%">{{trans("swis.pdf_templates.{$template}.key_8")}}</td>
                    <td class="table-header" width="20%">{{trans("swis.pdf_templates.{$template}.key_9")}}</td>
                </tr>
                {!! $productsPart !!}
                <tr>
                    <td colspan="3" class="table-header"
                        width="55%">{{trans("swis.pdf_templates.{$template}.key_10")}}</td>
                    <td class="table-header" width="25%">{{$totalQuantity}}</td>
                    <td class="table-header" width="20%">{{$totalNettoWeight}}</td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td class="table-header">{{trans("swis.pdf_templates.{$template}.key_11")}}</td>
    </tr>
    <tr>
        <td>{{trans("swis.pdf_templates.{$template}.key_12")}}</td>
    </tr>
</table>

@if(!is_null($agExaminationSection))
    <table>
        <tr>
            <td width="5%"></td>
            <td width="95%">
                <table border="1" cellpadding="3">
                    <tr>
                        <td class="table-header"
                            colspan="6">{{trans("swis.pdf_templates.{$template}.silver_table_title")}}</td>
                    </tr>
                    <tr>
                        <td class="table-header" width="5%">№</td>
                        <td class="table-header" width="25%">{{trans("swis.pdf_templates.{$template}.key_7")}}</td>
                        <td class="table-header" width="25%">{{trans("swis.pdf_templates.{$template}.key_7.1")}}</td>
                        <td class="table-header" width="15%">{{trans("swis.pdf_templates.{$template}.key_8")}}</td>
                        <td class="table-header" width="15%">{{trans("swis.pdf_templates.{$template}.key_9")}}</td>
                        <td class="table-header" width="15%">{{trans("swis.pdf_templates.{$template}.key_13")}}</td>
                    </tr>
                    {!! $agExaminationSection !!}
                    <tr>
                        <td colspan="3" class="table-header"
                            width="55%">{{trans("swis.pdf_templates.{$template}.key_10") }}</td>
                        <td class="table-header" width="15%">{{ $totalExaminationSilverQuantity }}</td>
                        <td class="table-header" width="15%">{{ $totalExaminationSilverWeight }}</td>
                        <td class="table-header" width="15%"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td></td>
        </tr>
    </table>
@endif
@if(!is_null($goldExaminationSection))
    <table>
        <tr>
            <td></td>
        </tr>
    </table>
    <table>
        <tr>
            <td width="5%"></td>
            <td width="95%">
                <table border="1" cellpadding="3" nobr="true">
                    <tr>
                        <td class="table-header"
                            colspan="6">{{trans("swis.pdf_templates.{$template}.gold_table_title")}}</td>
                    </tr>
                    <tr>
                        <td class="table-header" width="5%">№</td>
                        <td class="table-header" width="25%">{{trans("swis.pdf_templates.{$template}.key_7")}}</td>
                        <td class="table-header" width="25%">{{trans("swis.pdf_templates.{$template}.key_7.1")}}</td>
                        <td class="table-header" width="15%">{{trans("swis.pdf_templates.{$template}.key_8")}}</td>
                        <td class="table-header" width="15%">{{trans("swis.pdf_templates.{$template}.key_9")}}</td>
                        <td class="table-header" width="15%">{{trans("swis.pdf_templates.{$template}.key_13")}}</td>
                    </tr>
                    {!! $goldExaminationSection !!}
                    <tr>
                        <td colspan="3" class="table-header"
                            width="55%">{{trans("swis.pdf_templates.{$template}.key_10") }}</td>
                        <td class="table-header" width="15%">{{ $totalExaminationGoldQuantity }}</td>
                        <td class="table-header" width="15%">{{ $totalExaminationGoldWeight }}</td>
                        <td class="table-header" width="15%"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td></td>
        </tr>
    </table>
@endif
@if(!is_null($platinumExaminationSection))
    <table>
        <tr>
            <td></td>
        </tr>
    </table>
    <table>
        <tr>
            <td width="5%"></td>
            <td width="95%">
                <table border="1" cellpadding="3" nobr="true">
                    <tr>
                        <td class="table-header"
                            colspan="6">{{trans("swis.pdf_templates.{$template}.platinium_table_title")}}</td>
                    </tr>
                    <tr>
                        <td class="table-header" width="5%">№</td>
                        <td class="table-header" width="25%">{{trans("swis.pdf_templates.{$template}.key_7")}}</td>
                        <td class="table-header" width="25%">{{trans("swis.pdf_templates.{$template}.key_7.1")}}</td>
                        <td class="table-header" width="15%">{{trans("swis.pdf_templates.{$template}.key_8")}}</td>
                        <td class="table-header" width="15%">{{trans("swis.pdf_templates.{$template}.key_9")}}</td>
                        <td class="table-header" width="15%">{{trans("swis.pdf_templates.{$template}.key_13")}}</td>
                    </tr>
                    {!! $platinumExaminationSection !!}
                    <tr>
                        <td colspan="3" class="table-header"
                            width="55%">{{trans("swis.pdf_templates.{$template}.key_10") }}</td>
                        <td class="table-header" width="15%">{{ $totalExaminationPlatinumQuantity }}</td>
                        <td class="table-header" width="15%">{{ $totalExaminationPlatinumWeight }}</td>
                        <td class="table-header" width="15%"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td></td>
        </tr>
    </table>
@endif
@if(!is_null($palladiumExaminationSection))
    <table>
        <tr>
            <td></td>
        </tr>
    </table>
    <table>
        <tr>
            <td width="5%"></td>
            <td width="95%">
                <table border="1" cellpadding="3" nobr="true">
                    <tr>
                        <td class="table-header"
                            colspan="6">{{trans("swis.pdf_templates.{$template}.palladium_table_title")}}</td>
                    </tr>
                    <tr>
                        <td class="table-header" width="5%">№</td>
                        <td class="table-header" width="25%">{{trans("swis.pdf_templates.{$template}.key_7")}}</td>
                        <td class="table-header" width="25%">{{trans("swis.pdf_templates.{$template}.key_7.1")}}</td>
                        <td class="table-header" width="15%">{{trans("swis.pdf_templates.{$template}.key_8")}}</td>
                        <td class="table-header" width="15%">{{trans("swis.pdf_templates.{$template}.key_9")}}</td>
                        <td class="table-header" width="15%">{{trans("swis.pdf_templates.{$template}.key_13")}}</td>
                    </tr>
                    {!! $palladiumExaminationSection !!}
                    <tr>
                        <td colspan="2" class="table-header"
                            width="55%">{{trans("swis.pdf_templates.{$template}.key_10") }}</td>
                        <td class="table-header" width="15%">{{ $totalExaminationPalladiumQuantity }}</td>
                        <td class="table-header" width="15%">{{ $totalExaminationPalladiumWeight }}</td>
                        <td class="table-header" width="15%"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td></td>
        </tr>
    </table>
@endif
@if(!is_null($metalType30PercentSilverExaminationSection))
    <table>
        <tr>
            <td></td>
        </tr>
    </table>
    <table>
        <tr>
            <td width="5%"></td>
            <td width="95%">
                <table border="1" cellpadding="3" nobr="true">
                    <tr>
                        <td class="table-header"
                            colspan="4">{{trans("swis.pdf_templates.{$template}.silver_30_percent_table_title")}}</td>
                    </tr>
                    <tr>
                        <td class="table-header" width="5%">№</td>
                        <td class="table-header" width="35%">{{trans("swis.pdf_templates.{$template}.key_7")}}</td>
                        <td class="table-header" width="20%">{{trans("swis.pdf_templates.{$template}.key_7.1")}}</td>
                        <td class="table-header" width="20%">{{trans("swis.pdf_templates.{$template}.key_8")}}</td>
                        <td class="table-header" width="20%">{{trans("swis.pdf_templates.{$template}.key_9")}}</td>
                    </tr>
                    {!! $metalType30PercentSilverExaminationSection !!}
                    @if($silver30percentNum > 2)
                        <tr>
                            <td colspan="3" class="table-header"
                                width="60%">{{trans("swis.pdf_templates.{$template}.key_10") }}</td>
                            <td class="table-header" width="20%">{{ $totalExaminationSilver30PercentQuantity }}</td>
                            <td class="table-header" width="20%">{{ $totalExaminationSilver30PercentWeight }}</td>
                        </tr>
                    @endif
                    <tr>
                        <td colspan="4">
                            <span class="bold">{{$totalExaminationSilver30PercentQuantity}}</span>{{" ".trans("swis.pdf_templates.{$template}.key_14")." "}}
                            <span class="bold">{{$totalExaminationSilver30PercentWeight}}</span>{!! " ".trans("swis.pdf_templates.{$template}.key_15")." " !!}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td></td>
        </tr>
    </table>
@endif
@if(!is_null($bijouterieExaminationSection))
    <table>
        <tr>
            <td></td>
        </tr>
    </table>
    <table>
        <tr>
            <td width="5%"></td>
            <td width="95%">
                <table border="1" cellpadding="3" nobr="true">
                    <tr>
                        <td class="table-header"
                            colspan="6">{{trans("swis.pdf_templates.{$template}.bijouterie_table_title")}}</td>
                    </tr>
                    <tr>
                        <td class="table-header" width="5%">№</td>
                        <td class="table-header" width="25%">{{trans("swis.pdf_templates.{$template}.key_7")}}</td>
                        <td class="table-header" width="25%">{{trans("swis.pdf_templates.{$template}.key_7.1")}}</td>
                        <td class="table-header" width="15%">{{trans("swis.pdf_templates.{$template}.key_8")}}</td>
                        <td class="table-header" width="15%">{{trans("swis.pdf_templates.{$template}.key_9")}}</td>
                        <td class="table-header" width="15%">{{trans("swis.pdf_templates.{$template}.key_13")}}</td>
                    </tr>
                    {!! $bijouterieExaminationSection !!}
                    <tr>
                        <td colspan="3" class="table-header"
                            width="55%">{{trans("swis.pdf_templates.{$template}.key_10") }}</td>
                        <td class="table-header" width="15%">{{ $totalExaminationBijouterieQuantity }}</td>
                        <td class="table-header" width="15%">{{ $totalExaminationBijouterieWeight }}</td>
                        <td class="table-header" width="15%"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
@endif
<table>
    <tr>
        <td><i class="left">{{$fieldIDSAS_001_57}}</i></td>
    </tr>
</table>
<table>
    <tr>
        <td></td>
    </tr>
</table>
<table>
    <tr>
        <td class="left" style="width:50%">{!! trans("swis.pdf_templates.{$template}.key_20") !!}
            <br>{!! trans("swis.pdf_templates.{$template}.key_21") !!}</td>
        <td class="right" style="width:50%">{{$fieldIDSAS_001_52 }}</td>
    </tr>
</table>
<table>
    <tr>
        <td></td>
    </tr>
</table>
<table>
    <tr>
        <td class="left" style="width:50%"></td>
        <td class="right" style="width:50%">{{$fieldIDSAS_001_58 }}</td>
    </tr>
</table>
<table>
    <tr>
        <td></td>
    </tr>
</table>
<table>
    <tr>
        <td style="width:5%"></td>
        <td class="center" style="width:10%">{!! trans("swis.pdf_templates.{$template}.key_22") !!}</td>
        <td class="center" style="width:10%">{!! trans("swis.pdf_templates.{$template}.key_23") !!}</td>
        <td class="right" style="width:75%">{{$fieldIDSAS_001_26 }}</td>
    </tr>
</table>
<table>
    <tr>
        <td class="center" style="width:5%"></td>
        <td class="left" style="width:30%"><u>{{"“".($fromDay?? '')."”"}}</u>
            <u>{{"   ".($fromMonth ?? '')."   "}}</u>соли 20<u>{{($fromYear ?? '')}}</u></td>
        <td class="right" style="width:65%"></td>
    </tr>
</table>
<style>
    table > tr > td {
        font-size: 11px;
    }

    .table-header {
        text-align: center;
        font-size: 11px;
        font-weight: bold;
    }

    .center {
        text-align: center;
        font-size: 11px;
    }

    .left {
        text-align: left;
        font-size: 11px;
    }

    .right {
        text-align: right;
        font-size: 11px;
    }

    .gerb {
        height: 100px;
    }

    .title {
        font-weight: bold;
        font-size: 24px;
        text-align: center;
    }

    .fix_text {
        font-size: 11px;
    }

    .mapping {
        font-size: 9px;
    }

    .bold {
        font-weight: bold;
    }

    .right_side {
        text-align: right;
        font-size: 11px;
    }

    p {
        font-size: 11px;
    }

    .agency_info {
        border-bottom: 4px solid #3b699b;
        border-top: 4px solid #3b699b;
    }

    .bigHr {
        border: 8px solid #3b699b;
    }
</style>