<?php

use App\Http\Controllers\Core\Dictionary\DictionaryManager;
use App\Models\Agency\Agency;
use App\Models\BaseModel;
use App\Models\ConstructorTemplates\ConstructorTemplates;
use App\Models\Document\Document;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\RegionalOffice\RegionalOffice;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;

//----- Get fields values by MDM_ID
$mdmFields = getFieldsValueByMDMID($documentId, $customData,BaseModel::LANGUAGE_CODE_EN);

//----- 2.
$dictionaryManager = new DictionaryManager();

$key2_1 = trans("swis.pdf.{$template}.key2.1");
$key2_4 = trans("swis.pdf.{$template}.key2.4");

$safGeneralRegime = $saf->data['saf']['general']['regime'] ?? '';

switch ($safGeneralRegime) {
    case SingleApplication::REGIME_IMPORT:
        $key2_2 = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.import_key2.2", ConstructorTemplates::LANGUAGE_CODE_TJ);
        $key2_3 = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.import_key2.2", ConstructorTemplates::LANGUAGE_CODE_EN);

        //----- 4
        $key4 = trans("swis.pdf.{$template}.import_key4");

        $safTransportationCountryID = $saf->data['saf']['transportation']['export_country'] ?? '';//SAF_ID_78
        $safTransportationCountry = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safTransportationCountryID, false, [], 'get', false, false, [], false, ConstructorTemplates::LANGUAGE_CODE_EN))->name;

        //----- 14.
        $key14 = trans("swis.pdf.{$template}.import_info");

        //----- 14.1
        $transportationDeparture = $saf->data['saf']['transportation']['departure'];
        $transportInfo = !empty($transportationDeparture) ? getReferenceRows(ReferenceTable::REFERENCE_CUSTOM_BODY_REFERENCE, $transportationDeparture, false, [], 'get', false, false, [], false, ConstructorTemplates::LANGUAGE_CODE_EN)->name : '';//SAF_ID_72
        break;
    case SingleApplication::REGIME_EXPORT:
        $key2_2 = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.export_key2.2", ConstructorTemplates::LANGUAGE_CODE_TJ);
        $key2_3 = $dictionaryManager->getTransByLngCode("swis.pdf.{$template}.export_key2.2", ConstructorTemplates::LANGUAGE_CODE_EN);

        //----- 4
        $key4 = trans("swis.pdf.{$template}.export_key4");

        $safTransportationCountryID = $saf->data['saf']['transportation']['import_country'] ?? '';//SAF_ID_79
        $safTransportationCountry = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safTransportationCountryID, false, [], 'get', false, false, [], false, ConstructorTemplates::LANGUAGE_CODE_EN))->name;

        //----- 14.
        $key14 = trans("swis.pdf.{$template}.export_info");

        //----- 14.1
        $transportAppointment = $saf->data['saf']['transportation']['appointment'] ?? '';
        $transportInfo = !empty($transportAppointment) ? optional(getReferenceRows(ReferenceTable::REFERENCE_CUSTOM_BODY_REFERENCE, $transportAppointment, false, [], 'get', false, false, [], false, ConstructorTemplates::LANGUAGE_CODE_EN))->name : '';
        break;
    default:
        $key2_2 = $key2_3 = $key4 = $key14 = $safTransportationCountry = $transportInfo = '';
        break;
}

//----- 3.1 , 3.2 , 3.3
$safSideImporterName = $saf->data['saf']['sides']['import']['name'];//SAF_ID_6

$safSidesImporterPassport = $saf->data['saf']['sides']['import']['passport'] ? '<br><span class="bold">' . trans("swis.pdf.{$template}.key3.2") . ' ' . $saf->data['saf']['sides']['import']['passport'] . '</span>' : '';//SAF_ID_143

$safSidesImporterCommunity = $saf->data['saf']['sides']['import']['community'] ? '<br><span class="bold">' . trans("swis.pdf.{$template}.key3.3") . ' ' . $saf->data['saf']['sides']['import']['community'] . ', </span>' : '<br>' . trans("swis.pdf.{$template}.key3.3");//SAF_ID_8
$safSidesImporterAddress = $saf->data['saf']['sides']['import']['address'] ? '<span class="bold">' . $saf->data['saf']['sides']['import']['address'] . ', </span>' : '';//SAF_ID_9
$safSidesImporterCountry = $saf->data['saf']['sides']['import']['country'] ?? '';//SAF_ID_7

$importerCountry = !empty($safSidesImporterCountry) ? '<span class="bold">' . optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safSidesImporterCountry, false, [], 'get', false, false, [], false, ConstructorTemplates::LANGUAGE_CODE_EN))->name . '</span>' : '';

$importerAddress = $safSidesImporterCommunity . $safSidesImporterAddress . $importerCountry;

//----- 5.1
$safSideExporterName = $saf->data['saf']['sides']['export']['name'];//SAF_ID_14

$safSidesExporterPassport = $saf->data['saf']['sides']['export']['passport'] ? '<br><span class="bold">' . trans("swis.pdf.{$template}.key3.2") . ' ' . $saf->data['saf']['sides']['export']['passport'] . '</span>' : '';//SAF_ID_144

$safSidesExporterCommunity = $saf->data['saf']['sides']['export']['community'] ? '<br><span class="bold">' . trans("swis.pdf.{$template}.key5.3") . ' ' . $saf->data['saf']['sides']['export']['community'] . ', </span>' : '<br>' . trans("swis.pdf.{$template}.key5.3");//SAF_ID_16
$safSidesExporterAddress = $saf->data['saf']['sides']['export']['address'] ? '<span class="bold">' . $saf->data['saf']['sides']['export']['address'] . ', </span>' : '';//SAF_ID_17
$safSidesExporterCountry = $saf->data['saf']['sides']['export']['country'] ?? '';//SAF_ID_7

$exporterCountry = !empty($safSidesExporterCountry) ? '<span class="bold">' . optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safSidesExporterCountry, false, [], 'get', false, false, [], false, ConstructorTemplates::LANGUAGE_CODE_EN))->name . '</span>' : '';

$exporterAddress = $safSidesExporterCommunity . $safSidesExporterAddress . $exporterCountry;

$productsInfo = [];
$quantity = 0;
$measurementUnit = '';
$productNumber = 1;

foreach ($subAppProducts as $key => $subAppProduct) {

    //----- 6.
    $attachedDocumentProducts = SingleApplicationSubApplications::getAttachedDocumentProductsForPDF($subApplication, [$subAppProduct->id]);

    foreach ($attachedDocumentProducts as $value) {
        if ($value->document->document_type == Document::DOCUMENT_CODE_FOR_KOOC) {
            $documentNumber = trans("swis.pdf.{$template}.key6.1") . ' ' . $value->document->document_number;
            $documentReleaseDate = trans("swis.pdf.{$template}.key6.2") . ' ' . $value->document->document_release_date;
        }
    }

    //----- 10.
    if (count($subAppProducts) == 1) {
        $productsInfo = $subAppProduct['commercial_description'];
    } else {
        $productsInfo[] = $productNumber++ . '. ' . $subAppProduct['commercial_description'];
    }

    //----- 12.
    $measurementUnit = optional(getReferenceRows(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $subAppProducts[0]->measurement_unit, false, [], 'get', false, false, [], false, ConstructorTemplates::LANGUAGE_CODE_EN))->name;

    $quantity += optional($subAppProduct)->quantity ?? 0;
}

$measurementUnitAndQuantity = $quantity . " " . $measurementUnit;
$productsInfo = is_array($productsInfo) ? implode(', ', $productsInfo) : $productsInfo;
$productsInfo = cutString($productsInfo, $length = 100, $addDote = true);

//----- 7.
$agencyName = Agency::select('agency_ml.name as name')
    ->join('agency_ml', function ($query) {
        $query->on('agency.id', '=', 'agency_ml.agency_id')->where('lng_id', ConstructorTemplates::LANGUAGE_CODE_EN);
    })
    ->where('agency.id', $agency->id)
    ->active()
    ->pluck('name')
    ->first();

//----- 9
$source = optional(getReferenceRows(ReferenceTable::REFERENCE_CITES_SOURCE, ($customData['KOOC_001_17'] ?? ''), false, ['designation'], 'get', false, false, [], false, BaseModel::LANGUAGE_CODE_EN))->designation;

//----- 17.
$subdivisionId = optional($subApplication)->agency_subdivision_id;

$subDivision = !is_null($subdivisionId) ? optional(getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME, $subdivisionId)) : '';

if (!empty($subDivision)) {
    $regionalOffice = RegionalOffice::where('office_code', $subDivision->code)->notDeleted()->pluck('city')->first();
}

?>
<table border="0">
    <tr>
        <td style="height:130px"></td>
    </tr>
</table>
<table border="1" cellpadding="5">
    <tr>
        <td><span class="bold center fs12">{{$key2_1." ".$key2_2." / ".$key2_3." ".$key2_4." "}}</span><u
                    class="bold center fs12"> {{$mdmFields['365'] ?? ''}}</u></td>
    </tr>
    <tr>
        <td style="width:50%">{{trans("swis.pdf.{$template}.key3")}}</td>
        <td style="width:50%">{{$key4}}</td>
    </tr>
    <tr>
        <td style="width:50%"><span class="bold">{{$safSideImporterName}}</span>
            {!! $safSidesImporterPassport !!}
            {!! $importerAddress !!}
        </td>
        <td style="width:50%">
            <span class="bold center"><br>{{$safTransportationCountry}}</span>
        </td>
    </tr>
    <tr>
        <td style="width:50%">{{trans("swis.pdf.{$template}.key5")}}</td>
        <td style="width:50%">{{trans("swis.pdf.{$template}.key6")}}</td>
    </tr>
    <tr>
        <td style="width:50%"><span class="bold">{{$safSideExporterName}}</span>
            {!! $safSidesExporterPassport !!}
            {!! $exporterAddress !!}
        </td>
        <td style="width:50%">
            <span class="bold">{{$documentNumber ?? ''}}</span><br>
            <span class="bold">{{$documentReleaseDate ?? ''}}</span><br>
            <span class="bold">{{trans("swis.pdf.{$template}.key6.3").' '.($mdmFields['459'] ?? '')}}</span>
        </td>
    </tr>
    <tr>
        <td style="width:50%">{{trans("swis.pdf.{$template}.issuing_authority")}}</td>
        <td style="width:50%">{{trans("swis.pdf.{$template}.purpose_of_transportation")}}</td>
    </tr>
    <tr>
        <td style="width:50%" rowspan="3">
            <span class="bold center"><br>{{$agencyName}}</span>
        </td>
        <td><span class="bold center">{{($mdmFields['460'] ?? '').' '.trans("swis.pdf.{$template}.key8").' '.$safGeneralRegime}}</span>
        </td>
    </tr>
    <tr>
        <td><span class="bold ">{{trans("swis.pdf.{$template}.appendix")}}</span></td>
    </tr>
    <tr>
        <td><span class="bold center">{{($mdmFields['462'] ?? '').' '.trans("swis.pdf.{$template}.key9").' - '.$source}}</span>
        </td>
    </tr>
    <tr>
        <td style="width:40%">{{trans("swis.pdf.{$template}.scientific_name")}}</td>
        <td style="width:30%">{{trans("swis.pdf.{$template}.description_of_specimen")}}</td>
        <td style="width:30%"><span class="center">{{trans("swis.pdf.{$template}.quantity")}}</span></td>
    </tr>
    <tr>
        <td style="width:40%"><span class="center bold">{{$productsInfo}}</span></td>
        <td style="width:30%"><span class="center bold">{{$mdmFields['463'] ?? ''}}</span></td>
        <td style="width:30%"><span class="center bold">{{$measurementUnitAndQuantity}}</span></td>
    </tr>
    <tr>
        <td style="width:50%">{{trans("swis.pdf.{$template}.permit_valid_until")}}</td>
        <td style="width:50%">{{trans("swis.pdf.{$template}.special_information")}}</td>
    </tr>
    <tr>
        <td style="width:50%" class="center bold"> {{$mdmFields['357'] ?? ''}}</td>
        <td style="width:50%" class="bold" rowspan="3">{{$mdmFields['464'] ?? ''}}</td>
    </tr>
    <tr>
        <td style="width:50%">{{$key14}}</td>
    </tr>
    <tr>
        <td style="width:50%" class="bold"><br>
            <br>{{$transportInfo}}
            <br>{{$quantity.' '.($mdmFields['463'] ?? '').', '.$productsInfo}}
            <br>/__/___________20__
            <br>
        </td>
    </tr>
    <tr>
        <td style="width:100%">{{trans("swis.pdf.{$template}.permit_issued_by")}}</td>
    </tr>
    <tr>
        <td style="width:100%">
            <table>
                <tr>
                    <td class="center bold">
                        {{$agencyName}}
                    </td>
                </tr>
                <tr>
                    <td style="width:100px;" class="bold">{{$regionalOffice}}</td>
                    <td style="width:100px;" class="bold">{{$mdmFields['355'] ?? ''}}</td>
                    <td style="width:150px;" class="bold">{{$mdmFields['382'] ?? ''}}</td>
                    <td style="width:80px;" class="bold">{{trans("swis.pdf.{$template}.key_20")}}</td>
                </tr>
                <tr>
                    <td style="width:430px;line-height: 1.0" class="underline"></td>
                    <td style="width:130px;line-height: 1.0"
                        class="bold right">{{trans("swis.pdf.{$template}.key_21")}}</td>
                </tr>
                <tr>
                    <td style="width:100px;">{{trans("swis.pdf.{$template}.place")}}</td>
                    <td style="width:100px;">{{trans("swis.pdf.{$template}.date")}}</td>
                    <td style="width:300px;">{{trans("swis.pdf.{$template}.name_position")}}</td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td style="width:100%" class="underline"></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<style>
    table > tr > td {
        font-size: 11px;
        line-height: 1.3;
    }

    .bold {
        font-weight: bold;
    }

    .center {
        text-align: center;
    }

    .right {
        text-align: right;
    }

    .fs12 {
        font-size: 12px;
    }

    .underline {
        border-bottom: 1px solid black;
    }
</style>