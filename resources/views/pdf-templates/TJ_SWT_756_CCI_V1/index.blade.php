<?php

use App\Models\BaseModel;
use App\Models\ConstructorTemplates\ConstructorTemplates;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\RegionalOffice\RegionalOffice;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use App\Models\Document\Document;

function getColumnsMaxRowsCountForPrintForms($array) {

    if (!empty($array)) {
        $maxCount = 1;
        foreach ($array as $key => $value) {
            if ($key == 'productBruttoNetto') {
                $countTextSymbol = 4;
            } else {
                $countTextSymbol = count(str_split_unicode($value['text'], $value['rowStringLength']));
            }
            if ($countTextSymbol > $maxCount) {
                $maxCount = $countTextSymbol;
            }
        }
        return $maxCount;
    }
    return 0;
}


//tableRows1.png lines left
//55
//139
//433
//512
// image size 692 395

$mdmFields = getFieldsValueByMDMID($documentId, $customData, ConstructorTemplates::LANGUAGE_CODE_RU);

$gerbImagePath = '/image/gerb_' . env('COUNTRY_MODE') . '_sm_2.png';
$swisImagePath = '/image/tj_swis_logo.png';

// ----- 1.
$safSideNameExportName = $saf->data['saf']['sides']['export']['name'] ?? '';

$safSidesExportCommunity = $saf->data['saf']['sides']['export']['community'] ?? '';
$safSidesExportCommunity = ($safSidesExportCommunity == '-') ? '' : $safSidesExportCommunity;

$safSidesExportAddress = $saf->data['saf']['sides']['export']['address'] ?? '';
$safSidesExportAddress = ($safSidesExportAddress == '-') ? '' : $safSidesExportAddress;

$safSidesExportCountry = $saf->data['saf']['sides']['export']['country'] ?? '';
$safSidesExportCountryName = !empty($safSidesExportCountry) ? optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safSidesExportCountry, false, [], 'get', false, false, [], false, BaseModel::LANGUAGE_CODE_RU))->name : '';

$safSidesExportInfo = $safSideNameExportName .
    ', ' . $safSidesExportCommunity .
    ', ' . $safSidesExportAddress .
    ', ' . $safSidesExportCountryName;

$safSidesExportInfo = mb_strlen($safSidesExportInfo, 'UTF-8') > 182 ? mb_substr($safSidesExportInfo, 0, 182, 'UTF-8') . "..." : $safSidesExportInfo;

// ----- 2.
$safSideNameImportName = $saf->data['saf']['sides']['import']['name'] ?? '';

$safSidesImportCommunity = $saf->data['saf']['sides']['import']['community'] ?? '';
$safSidesImportCommunity = ($safSidesImportCommunity == '-') ? '' : $safSidesImportCommunity;

$safSidesImportAddress = $saf->data['saf']['sides']['import']['address'] ?? '';
$safSidesImportAddress = ($safSidesImportAddress == '-') ? '' : $safSidesImportAddress;

$safSidesImportCountry = $saf->data['saf']['sides']['import']['country'] ?? '';
$safSidesImportCountryName = !empty($safSidesImportCountry) ? optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safSidesImportCountry, false, [], 'get', false, false, [], false, BaseModel::LANGUAGE_CODE_RU))->name : '';

$safSidesImportInfo = $safSideNameImportName .
    ', ' . $safSidesImportCommunity .
    ', ' . $safSidesImportAddress .
    ', ' . $safSidesImportCountryName;

$safSidesImportInfo = mb_strlen($safSidesImportInfo, 'UTF-8') > 284 ? mb_substr($safSidesImportInfo, 0, 284, 'UTF-8') . "..." : $safSidesImportInfo;

// ----- 3.
$transportType = $saf->data['saf']['transportation']['type_of_transport'] ?? '';

if ($transportType) {
    $transportTypeCode = optional(getReferenceRows(ReferenceTable::REFERENCE_TRANSPORT_TYPE, $transportType, false, [], 'get', false, false, [], false, BaseModel::LANGUAGE_CODE_RU))->code;
    $transportTypeName = optional(getReferenceRows(ReferenceTable::REFERENCE_TRANSPORT_TYPE, $transportType, false, [], 'get', false, false, [], false, BaseModel::LANGUAGE_CODE_RU))->name;
    $transportType = $transportTypeCode . ' - ' . $transportTypeName;
}

$transitCountries = $saf->data['saf']['transportation']['transit_country'] ?? [];
$transCountries = [];
$transCountriesList = '';

foreach ($transitCountries as $transitCountry) {
    if (!is_null($transitCountry)) {
        $transCountriesCode = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $transitCountry, false, [], 'get', false, false, [], false, BaseModel::LANGUAGE_CODE_RU))->code;
        $transCountriesName = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $transitCountry, false, [], 'get', false, false, [], false, BaseModel::LANGUAGE_CODE_RU))->name;
        $transCountries[] = $transCountriesCode . ' - ' . $transCountriesName;
    }
}
if (!empty($transCountries)) {
    $transCountriesList = implode(', ', $transCountries);
}

$transportTypeCountriesList = $transportType . ' ' . $transCountriesList;
$transportTypeCountriesList = mb_strlen($transportTypeCountriesList, 'UTF-8') > 130 ? mb_substr($transportTypeCountriesList, 0, 130, 'UTF-8') . "..." : $transportTypeCountriesList;

// -----
$mdmId506 = $mdmFields['506'] ?? '';
$mdmId506 = mb_strlen($mdmId506, 'UTF-8') > 413 ? mb_substr($mdmId506, 0, 413, 'UTF-8') . "..." : $mdmId506;

// ----- 4.->2.
$fieldIDCCI_001_5 = !is_null($customData) ? !empty($customData['CCI_001_5']) && gettype($customData['CCI_001_5']) == 'array' ? $customData['CCI_001_5']['value'] : $customData['CCI_001_5'] ?? '' : '';

// ----- 4.->3.
$keyFixText1_1 = trans("swis.pdf_templates.{$template}.fix_text_1.1");
$keyFixText1_2 = trans("swis.pdf_templates.{$template}.fix_text_1.2");
$keyFixText1 = $keyFixText1_1 . '<br>' . $keyFixText1_2;

// ----- 4.->4.
$safTransportationExportCountry = $saf->data['saf']['transportation']['export_country'] ?? '';
$safTransportationExportCountryName = '';

if (!empty($safTransportationExportCountry)) {
    $safTransportationExportCountry = getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safTransportationExportCountry, false, [], 'get', false, false, [], false, BaseModel::LANGUAGE_CODE_RU);
    $safTransportationExportCountryName = optional($safTransportationExportCountry)->name;
}
$safTransportationExportCountryName1 = mb_strlen($safTransportationExportCountryName, 'UTF-8') > 60 ? mb_substr($safTransportationExportCountryName, 0, 60, 'UTF-8') . "..." : $safTransportationExportCountryName;

// ----- 4.->5.
$safTransportationImportCountry = $saf->data['saf']['transportation']['import_country'] ?? '';
$safTransportationImportCountryName = '';
if (!empty($safTransportationImportCountry)) {
    $safTransportationImportCountry = getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safTransportationImportCountry, false, [], 'get', false, false, [], false, BaseModel::LANGUAGE_CODE_RU);
    $safTransportationImportCountryName = optional($safTransportationImportCountry)->name;
}
$safTransportationImportCountryName = mb_strlen($safTransportationImportCountryName, 'UTF-8') > 43 ? mb_substr($safTransportationImportCountryName, 0, 43, 'UTF-8') . "..." : $safTransportationImportCountryName;

// ----- 11.->1.
$keyFixText2 = trans("swis.pdf_templates.{$template}.fix_text_2");

// ----- 11.->2.
$agencyName = $agency->current()->name;

// ----- 11.->3.
$subdivisionId = optional($subApplication)->agency_subdivision_id;
$subDivisionCode = optional(getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME, $subdivisionId,  false, [], 'get', false, false, [], false, BaseModel::LANGUAGE_CODE_RU))->code;

$regionalOffice = RegionalOffice::select('regional_office_ml.address', 'phone_number', 'regional_office_ml.name')
    ->leftJoin('regional_office_ml', 'regional_office.id', '=', 'regional_office_ml.regional_office_id')
    ->where(['lng_id' => cLng('id'), 'office_code' => $subDivisionCode])
    ->active()
    ->first();

$regionalOfficeAddress = optional($regionalOffice)->address;

// ----- 11.->4.
$fieldIDCCI_001_8 = !is_null($customData) ? !empty($customData['CCI_001_8']) && gettype($customData['CCI_001_8']) == 'array' ? $customData['CCI_001_8']['value'] : $customData['CCI_001_8'] ?? '' : '';

// ----- 11.->5.
$fieldIDCCI_001_6 = !is_null($customData) ? !empty($customData['CCI_001_6']) && gettype($customData['CCI_001_6']) == 'array' ? $customData['CCI_001_6']['value'] : $customData['CCI_001_6'] ?? '' : '';
if (!empty($fieldIDCCI_001_6)) {
    $fieldIDCCI_001_6 = explode('-', $fieldIDCCI_001_6);
    $fieldIDCCI_001_6Arr[0] = $fieldIDCCI_001_6[2];
    $fieldIDCCI_001_6Arr[1] = $fieldIDCCI_001_6[1];
    $fieldIDCCI_001_6Arr[2] = $fieldIDCCI_001_6[0];
    $fieldIDCCI_001_6 = implode('/', $fieldIDCCI_001_6Arr);
}

// ----- 12.->1.
$keyFixText3_1 = trans("swis.pdf_templates.{$template}.fix_text_3.1");
$keyFixText3_2 = trans("swis.pdf_templates.{$template}.fix_text_3.2");

$safTransportationExportCountryNameAndKey = $keyFixText3_1 . ' ' . $safTransportationExportCountryName . ' ' . $keyFixText3_2;

// ----- 12.->2.
$subApplicationSubmittingDate = date('d/m/Y', strtotime($subApplication->status_date)) ?? '';


// ----- 6.
$number = 1;

// ----- 7., 8., 9., 10. only $partData logic. if you want to use $subAppProducts create other foreach
//        @fixme delete
//$subAppProducts = \App\Models\SingleApplicationProducts\SingleApplicationProducts::limit(20)->orderBy('id', 'desc')->get();

$partData = '';
$partDataMaxRowsCount = 17;
$partDataRowsCount = 0;
$createOtherPage = false;
$keySubAppProduct = 0;
$countSubAppProducts = $subAppProducts->count();
$i = 1;

$xxx = '
        <tr>
            <td valign="top" width="1.1cm" class="tc fs9">xxxxxx</td>
            <td valign="top" width="4.9cm" class="tc fs9">xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx</td>
            <td valign="top" width="6.9cm" class="tc fs9">xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx</td>
            <td valign="top" width="2.5cm" class="tc fs9">xxxxxxxxxxxxxxxxxxx</td>
            <td valign="top" width="3cm" class="tc fs9">xxxxxxxxxxxxxxxxxxxxxxxxxx</td>
        </tr>
';

foreach ($subAppProducts as $key => $subAppProduct) {

    // ----- 7.
    $productNumberOfPlaces = (int)$subAppProduct['number_of_places'];

    $productPlaceType = getReferenceRows(ReferenceTable::REFERENCE_PACKAGING_REFERENCE, $subAppProduct['places_type'], false, [], 'get', false, false, [], false, BaseModel::LANGUAGE_CODE_RU);
    $productNumberTypeOfPlaces = ' ' . $productNumberOfPlaces . ' ' . optional($productPlaceType)->name;

    // ----- 8.
    $productComercialDescription = ' ' . $subAppProduct->commercial_description;

    // ----- 9.
    $productBruttoWeight = $subAppProduct->brutto_weight ?? '';
    $productNettoWeight = $subAppProduct->netto_weight ?? '';

    $totalQuantity = optional($subAppProduct)->quantity; // SAF_ID_100

    $refMeasurementUintInfo = '<br>&nbsp;'; // SAF_ID_101
    $refMeasurementUint = getReferenceRows(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $subAppProduct['measurement_unit']);
    if (!empty(optional($refMeasurementUint)) && optional($refMeasurementUint)->code != ReferenceTable::REF_KILOGRAM_CODE) {
        $refMeasurementUintInfo .= '' . $totalQuantity . ' ' . optional($refMeasurementUint)->name;
    }

    $totalQuantity2 = optional($subAppProduct)->quantity_2; // SAF_ID_102

    $refMeasurementUint2Info = '<br>&nbsp;'; // SAF_ID_103
    $refMeasurementUint2 = getReferenceRows(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $subAppProduct['measurement_unit_2']);
    if (!empty($totalQuantity2)) {
        $refMeasurementUint2Info .= '' . $totalQuantity2 . ' ' . optional($refMeasurementUint2)->name;
    }

    $productBruttoNetto = '<span>' . trans("swis.pdf_templates.{$template}.brutto_weight") . ' ' . $productBruttoWeight
        . '<br>' . trans("swis.pdf_templates.{$template}.netto_weight") . ' ' . $productNettoWeight
        . $refMeasurementUintInfo
        . $refMeasurementUint2Info . '</span>';

    // ----- 10.
    $attachedDocumentProducts = SingleApplicationSubApplications::getAttachedDocumentProductsForPDF($subApplication, [$subAppProduct->id]);
    $documentInfo = [];
    foreach ($attachedDocumentProducts as $value) {
        if ($value->document->document_type == Document::DOCUMENT_CODE_FOR_TJ_SWT_756_CCI_V1_PDF_TEMPLATE) {
            $documentInfo[] = $value->document->document_number . ' ' . $value->document->document_release_date;
        }
    }
    $documentInfo = implode(', ', $documentInfo);

    $partDataInfo = [
        'productNumberTypeOfPlaces' => [
            'text' => $productNumberTypeOfPlaces,
            'rowStringLength' => 31
        ],
        'productComercialDescription' => [
            'text' => $productComercialDescription,
            'rowStringLength' => 49
        ],
        'productBruttoNetto' => [
            'text' => $productBruttoNetto,
            'rowStringLength' => 16
        ],
        'documentInfo' => [
            'text' => $documentInfo,
            'rowStringLength' => 21
        ]
    ];
    $partDataRowsCount += getColumnsMaxRowsCountForPrintForms($partDataInfo);

    if ($partDataRowsCount >= $partDataMaxRowsCount) {
        $createOtherPage = true;
        $keySubAppProduct = $key;
        $partData .= $xxx;
        break;
    }

    // ----- 7., 8., 9., 10.
    $partData .= '
        <tr>
            <td valign="top" width="1.1cm" class="pl5 fs9">' . $number . '.</td>
            <td valign="top" width="4.9cm" class="pl5 fs9 ">' . $productNumberTypeOfPlaces . '</td>
            <td valign="top" width="6.9cm" class="pl5 fs9 ">' . $productComercialDescription . '</td>
            <td valign="top" width="2.5cm" class="pl5 fs9 ">' . $productBruttoNetto . '</td>
            <td valign="top" width="3cm" class="pl5 fs9">' . $documentInfo . '</td>
        </tr>
        ';

    if (($partDataRowsCount <= $partDataMaxRowsCount) && ($countSubAppProducts == $i)) {

        $partDataEmptyRowsCount = $partDataMaxRowsCount - $partDataRowsCount;
        $partData .= $xxx;

    }

    $number++;
    $i++;

}

$partDataOtherPages = [];
$partDataOtherPagesRowsCount = $partDataOtherPagesEmptyRowsCount = 0;
$partDataMaxRowsCountOtherPages = 40;
$i = 0;

$mdmId365 = notEmptyForPdfFields($mdmFields, '365');

if ($createOtherPage) {

    $partDataOtherPagesFirstPart = '
    <pagebreak></pagebreak>

    <table>

        {{-----------------------------------------------------block 9------------------------------------------------}}

        <tr>
            <td width="18.6cm" height="2.5cm" class="br2 bt2 bl2 tc" colspan="2">
                <span class="fs16">' . trans("swis.pdf_templates.{$template}.other_page.title1") . ' ' . $mdmId365 . '</span>
                <br>
                <span class="fs14">' . trans("swis.pdf_templates.{$template}.other_page.title2") . '</span>
            </td>
        </tr>

    </table>


    <table>

        {{-----------------------------------------------------block 10-----------------------------------------------}}

        <tr>
            <td width="1.1cm" height="1.2cm" class="tc br fb fs12 bb bt bl2">6.<br>' . trans("swis.pdf_templates.{$template}.list.number") . '</td>
            <td width="4.9cm" height="1.2cm" class="tc br fb fs12 bb bt">7.' . trans("swis.pdf_templates.{$template}.list.count.type_of_package") . '</td>
            <td width="6.9cm" height="1.2cm" class="tc br fb fs12 bb bt">8.' . trans("swis.pdf_templates.{$template}.list.product_description") . '</td>
            <td width="2.5cm" height="1.2cm" class="tc br fb fs12 bb bt">9.' . trans("swis.pdf_templates.{$template}.list.weight.brutto_netto") . '</td>
            <td width="3.2cm" height="1.2cm" class="tc fb fs12 bb bt br2">10.' . trans("swis.pdf_templates.{$template}.list.number_and_date") . '</td>
        </tr>

    </table>

    <table style="background-image: url(\'/image/tableRows1.png\');">

        {{-----------------------------------------------------block 11-----------------------------------------------}}

        <tr>
            <td valign="top" width="18.6cm" height="16.8cm" class="bl2 br2" >
                <table>

    ';

    $partDataOtherPagesLastPart = '

                </table>
            </td>
        </tr>
    </table>

    <table>

        {{-----------------------------------------------------block 12-----------------------------------------------}}

        <tr>
            <td valign="top" width="9.2cm" height="4.5cm" class="bt br bl2">
                <table>
                    <tr>
                        <td valign="top" width="0.8cm" class="fb fs12">&nbsp;&nbsp;&nbsp;11. </td>
                        <td valign="top" class="fb fs12">' . trans("swis.pdf_templates.{$template}.certificate") . '</td>
                    </tr>
                    <tr>
                        <td valign="top" width="0.8cm" class="fb fs12"></td>
                        <td valign="top" class="fb fs12">' . $keyFixText2 . '</td>
                    </tr>
                    <tr>
                        <td valign="top" width="0.8cm" class="fb fs12"></td>
                        <td valign="top" class="fs9">' . $agencyName . '</td>
                    </tr>
                    <tr>
                        <td valign="top" width="0.8cm" class="fb fs12"></td>
                        <td valign="top" class="fs9">' . $regionalOfficeAddress . '</td>
                    </tr>
                    <tr>
                        <td valign="top" width="0.8cm" class="fb fs12"></td>
                        <td valign="top" class="fs9">' . $fieldIDCCI_001_8 . '</td>
                    </tr>
                </table>
            </td>
            <td valign="top" width="9.4cm" height="4.5cm" class="br2 bt ">
                <table>
                    <tr>
                        <td valign="top" width="0.8cm" class="fb fs12">&nbsp;&nbsp;&nbsp;12. </td>
                        <td valign="top" class="fb fs12" colspan="2">' . trans("swis.pdf_templates.{$template}.applicant_declaration") . '</td>
                    </tr>
                    <tr>
                        <td valign="top" width="0.8cm" class="fb fs12"></td>
                        <td valign="top" class="fb fs11" colspan="2">' . $keyFixText3_1 . '</td>
                    </tr>
                    <tr>
                        <td valign="top" width="0.8cm" class="fb fs12"></td>
                        <td valign="top" width="7.8cm" class="fs9 bb tc">' . $safTransportationExportCountryName . '</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td valign="top" width="0.8cm" class="fb fs12"></td>
                        <td valign="top" width="7.8cm" class="fs7 tc">' . trans("swis.pdf_templates.{$template}.list.country_name") . '</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td valign="top" width="0.8cm" class="fb fs12"></td>
                        <td valign="top" class="fb fs11" colspan="2">' . $keyFixText3_2 . '</td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td valign="top" width="9.2cm" height="1.5cm" class="br bl2 bb2" style="padding-top: 0.3cm">
                <table>
                    <tr>
                        <td valign="top" width="0.8cm"></td>
                        <td width="2.5cm" class="tc fs9 bb"></td>
                        <td width="2.5cm" class="tc fs9 bb">' . $fieldIDCCI_001_6 . '</td>
                        <td width="2.5cm" class="tc fs9 bb"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td valign="top" width="0.8cm"></td>
                        <td width="2.5cm" class="tc fs9">' . trans("swis.pdf_templates.{$template}.signature") . '</td>
                        <td width="2.5cm" class="tc fs9">' . trans("swis.pdf_templates.{$template}.date") . '</td>
                        <td width="2.5cm" class="tc fs9">' . trans("swis.pdf_templates.{$template}.seal") . '</td>
                        <td></td>
                    </tr>
                </table>
            </td>
            <td valign="top" width="9.4cm" height="1.5cm" class="br2 bb2" style="padding-top: 0.3cm">
                <table>
                    <tr>
                        <td valign="top" width="0.8cm"></td>
                        <td width="2.5cm" class="tc fs9 bb"></td>
                        <td width="2.5cm" class="tc fs9 bb">' . $subApplicationSubmittingDate . '</td>
                        <td width="2.5cm" class="tc fs9 bb"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td valign="top" width="0.8cm"></td>
                        <td width="2.6cm" class="tc fs9">' . trans("swis.pdf_templates.{$template}.signature") . '</td>
                        <td width="2.6cm" class="tc fs9">' . trans("swis.pdf_templates.{$template}.date") . '</td>
                        <td width="2.6cm" class="tc fs9">' . trans("swis.pdf_templates.{$template}.seal") . '</td>
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    ';

    $partDataOtherPages[$i] = $partDataOtherPagesFirstPart;

    foreach ($subAppProducts as $key => $subAppProduct) {

        if ($keySubAppProduct > $key) {
            continue;
        }

        // ----- 7.
        $productNumberOfPlaces = (int)$subAppProduct['number_of_places'];

        $productPlaceType = getReferenceRows(ReferenceTable::REFERENCE_PACKAGING_REFERENCE, $subAppProduct['places_type'], false, [], 'get', false, false, [], false, BaseModel::LANGUAGE_CODE_RU);
        $productNumberTypeOfPlaces = ' ' . $productNumberOfPlaces . ' ' . optional($productPlaceType)->name;

        // ----- 8.
        $productComercialDescription = ' ' . $subAppProduct->commercial_description;

        // ----- 9.
        $productBruttoWeight = $subAppProduct->brutto_weight ?? '';

        $productNettoWeight = $subAppProduct->netto_weight ?? '';
        $totalQuantity = optional($subAppProduct)->quantity; // SAF_ID_100

        $refMeasurementUintInfo = '<br>&nbsp;'; // SAF_ID_101
        $refMeasurementUint = getReferenceRows(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $subAppProduct['measurement_unit']);
        if (!empty(optional($refMeasurementUint)) && optional($refMeasurementUint)->code != ReferenceTable::REF_KILOGRAM_CODE) {
            $refMeasurementUintInfo .= '' . $totalQuantity . ' ' . optional($refMeasurementUint)->name;
        }

        $totalQuantity2 = optional($subAppProduct)->quantity_2; // SAF_ID_102

        $refMeasurementUint2Info = '<br>&nbsp;'; // SAF_ID_103
        $refMeasurementUint2 = getReferenceRows(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $subAppProduct['measurement_unit_2']);
        if (!empty(optional($refMeasurementUint2))) {
            $refMeasurementUint2Info .= '' . $totalQuantity2 . ' ' . optional($refMeasurementUint2)->name;
        }

        $productBruttoNetto = trans("swis.pdf_templates.{$template}.brutto_weight") . ' ' . $productBruttoWeight
            . '<br>' . trans("swis.pdf_templates.{$template}.netto_weight") . ' ' . $productNettoWeight
            . $refMeasurementUintInfo
            . $refMeasurementUint2Info;

        // ----- 10.
        $attachedDocumentProducts = SingleApplicationSubApplications::getAttachedDocumentProductsForPDF($subApplication, [$subAppProduct->id]);
        $documentInfo = [];
        foreach ($attachedDocumentProducts as $value) {
            if ($value->document->document_type == Document::DOCUMENT_CODE_FOR_TJ_SWT_756_CCI_V1_PDF_TEMPLATE) {
                $documentInfo[] = $value->document->document_number . ' ' . $value->document->document_release_date;
            }
        }
        $documentInfo = implode(', ', $documentInfo);

        $partDataOtherPagesInfo = [
            'productNumberTypeOfPlaces' => [
                'text' => $productNumberTypeOfPlaces,
                'rowStringLength' => 31
            ],
            'productComercialDescription' => [
                'text' => $productComercialDescription,
                'rowStringLength' => 49
            ],
            'productBruttoNetto' => [
                'text' => $productBruttoNetto,
                'rowStringLength' => 16
            ],
            'documentInfo' => [
                'text' => $documentInfo,
                'rowStringLength' => 21
            ]
        ];

        $partDataOtherPagesRowsCount += getColumnsMaxRowsCountForPrintForms($partDataOtherPagesInfo);

        if ($partDataOtherPagesRowsCount > $partDataMaxRowsCountOtherPages) {
            $partDataOtherPages[$i] .= $xxx;
            $partDataOtherPages[$i] .= $partDataOtherPagesLastPart;

            $i++;
            $partDataOtherPagesRowsCount = 0;

            $partDataOtherPages[$i] = $partDataOtherPagesFirstPart;

        }

        // ----- 7., 8., 9., 10.
        $partDataOtherPages[$i] .= '
        <tr>
            <td valign="top" width="1.1cm" class="pl5 pr5 fs9">' . $number . '.</td>
            <td valign="top" width="4.9cm" class="pl5 pr5 fs9">' . $productNumberTypeOfPlaces . '</td>
            <td valign="top" width="6.9cm" class="pl5 pr5 fs9">' . $productComercialDescription . '</td>
            <td valign="top" width="2.5cm" class="pl5 pr5 fs9">' . $productBruttoNetto . '</td>
            <td valign="top" width="3cm" class="pl5 pr5 fs9">' . $documentInfo . '</td>
        </tr>
        ';

        if (($partDataOtherPagesRowsCount <= $partDataMaxRowsCountOtherPages) && ($countSubAppProducts - 1 == $key)) {

            $partDataOtherPagesEmptyRowsCount = $partDataMaxRowsCountOtherPages - $partDataOtherPagesRowsCount;

            $partDataOtherPages[$i] .= $xxx;

            $partDataOtherPages[$i] .= $partDataOtherPagesLastPart;
        }
        $number++;
    }
}

// -----
$keyCertificatePart1 = trans("swis.pdf_templates.{$template}.certificate.part_1");
$keyCertificatePart2 = trans("swis.pdf_templates.{$template}.certificate.part_2");

// 10 10 5  30 30 5
// height: 970px
?>
<style>

    @page {
        margin-left: 1.2cm;
        margin-top: 1.9cm;
        margin-right: 0cm;
        margin-bottom: 0cm;
    }

    table {
        overflow: wrap;
        width: 18.6cm;
        border-collapse: collapse;
    }

    .fb {
        font-weight: bold;
    }

    .tc {
        text-align: center;
    }


    .main-table {
        padding-top: 100px;
    }

    .bordered-table td {
        border: 1px solid #000000;
    }

    .tr {
        text-align: right;
    }

    .pt5 {
        padding-top: 5px;
    }

    .tj {
        text-align: justify;
    }

    .fs7 {
        font-size: 7px;
    }

    .fs8 {
        font-size: 8px;
    }

    .fs9 {
        font-size: 9px;
    }

    .fs10 {
        font-size: 10px;
    }

    .fs11 {
        font-size: 11px;
    }

    .fs11-5 {
        font-size: 11.5px;
    }

    .fs12 {
        font-size: 12px;
    }

    .fs13 {
        font-size: 13px;
    }

    .fs14 {
        font-size: 14px;
    }

    .fs15 {
        font-size: 15px;
    }

    .fs16 {
        font-size: 16px;
    }

    .fs17 {
        font-size: 17px;
    }

    .fs18 {
        font-size: 18px;
    }

    .fs19 {
        font-size: 19px;
    }

    .fs20 {
        font-size: 20px;
    }

    .fs21 {
        font-size: 21px;
    }

    .fs22 {
        font-size: 22px;
    }

    .fs23 {
        font-size: 23px;
    }

    .fs24 {
        font-size: 24px;
    }

    .fs25 {
        font-size: 25px;
    }

    .fs26 {
        font-size: 26px;
    }

    .fs27 {
        font-size: 27px;
    }

    .fs28 {
        font-size: 28px;
    }

    .fs29 {
        font-size: 29px;
    }

    .fs30 {
        font-size: 30px;
    }

    .bt {
        border-top: 1px solid #000000;
    }

    .bb {
        border-bottom: 1px solid #000000;
    }

    .bl {
        border-left: 1px solid #000000;
    }

    .br {
        border-right: 1px solid #000000;
    }

    .border {
        border: 1px solid #000000;
    }


    .bt2 {
        border-top: 2px solid #000000;
    }

    .bb2 {
        border-bottom: 2px solid #000000;
    }

    .bl2 {
        border-left: 2px solid #000000;
    }

    .br2 {
        border-right: 2px solid #000000;
    }

    .pl5 {
        padding-left: 5px;
    }

    .pr5 {
        padding-right: 5px;
    }
</style>


<table>

    {{-----------------------------------------------------block 1----------------------------------------------------}}

    <tr>
        <td height="2.4cm" class="bt2 bl2"><img width="1.4cm" src="<?php echo e($swisImagePath); ?>"/></td>
        <td height="2.4cm" width="14.9cm" class="bt2 tc fb fs26">{!! trans("swis.pdf_templates.{$template}.title") !!}</td>
        <td height="2.4cm" class="bt2 br2"><img width="2.1cm" src="<?php echo e($gerbImagePath); ?>"/></td>
    </tr>

</table>

<table>

    {{-----------------------------------------------------block 2----------------------------------------------------}}

    <tr>
        <td valign="top" width="7.3cm" height="2.7cm" class="br bt bl2" rowspan="2">
            <br><span class="fb fs12">&nbsp;1.{{trans("swis.pdf_templates.{$template}.exporter.text1")}}</span>
            <span class="fs12"><br>&nbsp;&nbsp;&nbsp;&nbsp;{{trans("swis.pdf_templates.{$template}.exporter.text2")}}</span>
            <br>
            <span class="fs9">{{$safSidesExportInfo}}</span>
        </td>
        <td valign="top" width="11.3cm" height="1.1cm" class="bt br2">
            <table>
                <tr>
                    <td width="5.4cm" class="fb fs12"><br>&nbsp;4.{{trans("swis.pdf_templates.{$template}.number")}}</td>
                    <td width="4cm" class="fb fs9 "><br>{{$fieldIDCCI_001_5}}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top" width="9.4cm" height="1.6cm" class="tc br2 fs16 fb ">{!! $keyFixText1 !!}</td>
    </tr>

    {{-----------------------------------------------------block 3----------------------------------------------------}}

    <tr>
        <td valign="top" width="7.3cm" height="2.8cm" class="bt br bl2">
            <span class="fs12 fb">&nbsp;2.{{trans("swis.pdf_templates.{$template}.importer.text1")}}</span>
            <span class="fs12"><br>&nbsp;&nbsp;&nbsp;&nbsp;{{trans("swis.pdf_templates.{$template}.exporter.text2")}}</span>
            <br><span class="fs9">{{$safSidesImportInfo}}</span>
        </td>
        <td valign="top" width="11.3cm" height="2.8cm" class="br2" style="padding-top: 0.3cm;">
            <table>
                <tr>
                    <td width="1.5cm" class="fs12 fb">
                        &nbsp;{{trans("swis.pdf_templates.{$template}.issued.text1")}}</td>
                    <td width="9cm" class="tc bb fs9">{{$safTransportationExportCountryName1}}</td>
                    <td></td>
                </tr>
                <tr>
                    <td width="1.5cm"></td>
                    <td width="9cm" class="tc fs7">{{trans("swis.pdf_templates.{$template}.issued.text2")}}</td>
                    <td></td>
                </tr>
            </table>
            <br>
            <table>
                <tr>
                    <td width="4cm" class="fs12 fb">&nbsp;{{trans("swis.pdf_templates.{$template}.providing.text1")}}</td>
                    <td width="6.5cm" class="tc bb fs9">{{$safTransportationImportCountryName}}</td>
                    <td></td>
                </tr>
                <tr>
                    <td width="4cm"></td>
                    <td width="6.5cm" class="tc fs7">{{trans("swis.pdf_templates.{$template}.issued.text2")}}</td>
                    <td></td>
                </tr>
            </table>
        </td>
    </tr>

</table>

{{---------------------------------------------------------block 4----------------------------------------------------}}

<table>

    <tr>
        <td valign="top" width="7.3cm" height="1.9cm" class="bt br bl2">
            <span class="fs11-5 fb">&nbsp;3.{{trans("swis.pdf_templates.{$template}.transport.type.transit_country.text1")}}</span>
            <span class="fs12"><br>&nbsp;&nbsp;&nbsp;&nbsp;{{trans("swis.pdf_templates.{$template}.transport.type.transit_country.text2")}}</span>
            <br><span class="fs9">{{$transportTypeCountriesList}}</span>
        </td>
        <td valign="top" width="11.3cm" height="1.9cm" class="bt br2">
            <span class="fs12 fb">&nbsp;5.{{trans("swis.pdf_templates.{$template}.service_marks")}}</span>
            <br><span class="fs9">{!! $mdmId506 !!}</span>
        </td>
    </tr>
</table>

{{---------------------------------------------------------block 5----------------------------------------------------}}

<table>

    <tr>
        <td width="1.1cm" height="2cm" class="tc br fb fs12 bb bt bl2">6.<br>{{trans("swis.pdf_templates.{$template}.list.number")}}</td>
        <td width="4.9cm" height="2cm" class="tc br fb fs12 bb bt">7.{{trans("swis.pdf_templates.{$template}.list.count.type_of_package")}}</td>
        <td width="6.9cm" height="2cm" class="tc br fb fs12 bb bt">8.{{trans("swis.pdf_templates.{$template}.list.product_description")}}</td>
        <td width="2.5cm" height="2cm" class="tc br fb fs12 bb bt">9.{{trans("swis.pdf_templates.{$template}.list.weight.brutto_netto")}}</td>
        <td width="3.2cm" height="2cm" class="tc fb fs12 bb bt br2">10.{{trans("swis.pdf_templates.{$template}.list.number_and_date")}}</td>
    </tr>

</table>

{{---------------------------------------------------------block 6----------------------------------------------------}}

<table style="background-image: url('/image/tableRows1.png'); background-repeat: no-repeat;">

    <tr>
        <td valign="top" width="18.6cm" height="6.2cm" class="bl2 br2" >
            <table>
                @if (!empty($partData))
                    {!! $partData !!}
                @endif
            </table>
        </td>
    </tr>

</table>
<table>

    {{-----------------------------------------------------block 7----------------------------------------------------}}

    <tr>
        <td valign="top" width="8.4cm" height="5.6cm" class="br bl2 bt">
            <table>
                <tr>
                    <td valign="top" width="0.8cm" class="fb fs12">&nbsp;&nbsp;&nbsp;11.</td>
                    <td valign="top"
                        class="fb fs12">{{trans("swis.pdf_templates.{$template}.certificate")}}</td>
                </tr>
                <tr>
                    <td valign="top" width="0.8cm" class="fb fs12"></td>
                    <td valign="top" class="fb fs12"><br>{{$keyFixText2}}</td>
                </tr>
                <tr>
                    <td valign="top" width="0.8cm" class="fb fs12"></td>
                    <td valign="top" class="fs9">{{$agencyName}}</td>
                </tr>
                <tr>
                    <td valign="top" width="0.8cm" class="fb fs12"></td>
                    <td valign="top" class="fs9">{{$regionalOfficeAddress}}</td>
                </tr>
                <tr>
                    <td valign="top" width="0.8cm" class="fb fs12"></td>
                    <td valign="top" class="fs9">{{$fieldIDCCI_001_8}}</td>
                </tr>
            </table>
        </td>
        <td valign="top" width="10.2cm" height="5.6cm" class="br2 bt">
            <table>
                <tr>
                    <td valign="top" width="0.8cm" class="fb fs12">&nbsp;&nbsp;&nbsp;12.</td>
                    <td valign="top"
                        class="fb fs12">{{trans("swis.pdf_templates.{$template}.applicant_declaration")}}</td>
                </tr>
                <tr>
                    <td valign="top" width="0.8cm" class="fb fs12"></td>
                    <td valign="top" class="fb fs11"><br>{{$safTransportationExportCountryNameAndKey}}</td>
                </tr>
            </table>
        </td>
    </tr>

</table>

{{---------------------------------------------------------block 8----------------------------------------------------}}

<table>

    <tr>
        <td width="2.9cm" class="tc fs9 bb bl2"></td>
        <td width="2.8cm" class="tc fs9 bb">{{$fieldIDCCI_001_6}}</td>
        <td width="2.7cm" class="tc fs9 bb br"></td>
        <td width="3.4cm" class="tc fs9 bb"></td>
        <td width="3.4cm" class="tc fs9 bb">{{$subApplicationSubmittingDate}}</td>
        <td width="3.4cm" class="tc fs9 bb br2"></td>
    </tr>
    <tr>
        <td width="2.9cm" height="1.5cm" valign="top" class="bb2 tc fs9 bl2">{{trans("swis.pdf_templates.{$template}.signature")}}</td>
        <td width="2.8cm" height="1.5cm" valign="top" class="bb2 tc fs9">{{trans("swis.pdf_templates.{$template}.date")}}</td>
        <td width="2.7cm" height="1.5cm" valign="top" class="bb2 tc fs9 br">{{trans("swis.pdf_templates.{$template}.seal")}}</td>
        <td width="3.4cm" height="1.5cm" valign="top" class="bb2 tc fs9">{{trans("swis.pdf_templates.{$template}.signature")}}</td>
        <td width="3.4cm" height="1.5cm" valign="top" class="bb2 tc fs9">{{trans("swis.pdf_templates.{$template}.date")}}</td>
        <td width="3.4cm" height="1.5cm" valign="top" class="bb2 tc fs9 br2">{{trans("swis.pdf_templates.{$template}.seal")}}</td>
    </tr>

</table>

{{--------------------------------------------------------block 13----------------------------------------------------}}

<table>
    <tr>
        <td width="18.2cm" height="0.7cm" class="fs9">
            {{$keyCertificatePart1}}
            <br>
            {{$keyCertificatePart2}}
        </td>
    </tr>
</table>

@if (!empty($partDataOtherPages))

    @foreach($partDataOtherPages as $valueOP)

        {!! $valueOP !!}

    @endforeach

@endif