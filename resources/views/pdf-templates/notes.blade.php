<?php
$notesSection = '';

$stateName =  trans('swis.saf.notes.state.' . $saf->status_type);
if(!empty($subApplicationCurrentStateName)){
    $stateName = $subApplicationCurrentStateName;
}

$safCurrentStatus = '<tr>
    <td style="
                                border-bottom: 1px solid #E7EAEC;
                                color: #676A6C;
                                font-size: 12px;
                            ">
        •&nbsp;&nbsp;<b>' . trans('swis.saf.notes.current_state') . ' -</b> ' .$stateName. '
        <br />
    </td>
</tr>';

$notesSection = $notesSection . '
<table width="100%" cellpadding="10">
    <tr >
        <td width="10%"></td>
        <td width="80%">
            <table width="100%" cellspacing="15">

                ' . $safCurrentStatus;

if ($notes) {
    foreach ($notes as $note) {

        $notesSection = $notesSection . '
                <tr nobr="true">
                    <td style="border-bottom: 1px solid #E7EAEC;">
                        <span style="
                                    color: #676A6C;
                                    font-size: 12px;
                                    display:block;
                                ">
                            •&nbsp;&nbsp;' . $note['note'] . '
                            </span><br>
                        <span style="
                                    color: #676A6C;
                                    font-size: 10px;
                                ">
                            &nbsp;&nbsp;&nbsp;&nbsp;' . $note['created_at'] . '
                        </span><br>
                    </td>
                </tr>

               ';
    }
    $notesSection = $notesSection . '</table>
        </td>
    </tr>
</table>';
}
?>

<table>
    <table width="100%" cellpadding="5">
        <tr>
            <td style="
			background-color: #92D050;
			color: #000000;
			font-size: 10px;
			font-weight: bold;
			text-align: center;
		"><?php echo trans('swis.pdf_templates.saf.notes_title')?></td>
        </tr>
    </table>
    {!! $notesSection !!}
</table>
