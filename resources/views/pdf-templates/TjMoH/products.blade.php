<?php

use App\Models\BaseModel;

$productsSection = '';

$fieldID1645_4 = $customData['PHS_001_4'] ?? '';
$fieldID1645_5 = $customData['PHS_001_5'] ?? '';

if (!empty($fieldID1645_5)) {
    $fromDay = date('d', strtotime($fieldID1645_5));
    $fromMonth = month(date('m', strtotime($fieldID1645_5)), BaseModel::LANGUAGE_CODE_TJ);
    $fromYear = date('Y', strtotime($fieldID1645_5));
}

if (!empty($subAppProducts)) {
    $totalNettoWeight = 0;
    foreach ($subAppProducts as $key => $product) {

        if (empty($productsNum)) {
            $productsNum = 1;
        }

        $productDescription = $product->commercial_description ?? '';
        $productNettoWeight = $product->netto_weight ?? '';

        $totalNettoWeight += $productNettoWeight;

        $productsSection .= '<tr>
        <td style="
			font-size: 10px;
			line-height: 5px;
			padding: 0;
			text-align: center;
			width: 12%;
		">' . $productsNum . '
        </td>
        <td style="
			font-size: 10px;
			padding: 0;
			text-align: left;
			width: 60%;
		">' . $productDescription . '
        </td>

        <td style="
			font-size: 10px;
			line-height: 5px;
			padding: 0;
			text-align: center;
			width: 12%;
		">' . $productNettoWeight . '</td>

    </tr>';
        $productsNum++;
    }
}
?>
<table>
    <tr>
        <td style="
			font-size: 16px;
			padding: 0;
			text-transform: uppercase;
			text-align: center;
		"></td>
    </tr>
    <tr>
        <td style="
			font-size: 16px;
			text-transform: uppercase;
			text-align: center;
		"></td>
    </tr>
    <tr>
        <td style="
			font-size: 26px;
			text-align: center;
		"></td>
    </tr>
    <tr>
        <td style="
			font-size: 26px;
			text-align: center;
		"></td>
    </tr>
</table>
<table width="100%">
    <tr>
        <td style="
			font-size: 14px;
			padding: 0;
			font-weight: bold;
			text-align: center;
			width: 84%;
		">Служба государственного надзора здравоохранения и<br>
            социальной защиты населения<br>
            Хадамоти назорати давлатии тандурустӣ ва ҳифзи иҷтимоии аҳолӣ
        </td>
    </tr>
</table>
<table width="100%">
    <tr>
        <td style="
			font-size: 12px;
			font-weight: bold;
			padding: 0;
			text-align: center;
			width: 84%;
		">Замима ба сертификати мутобикати, № {{$fieldID1645_4}}
            аз {{$fromDay ??''}} {{$fromMonth??''}} {{$fromYear??''}} сол<br>
            Рӯйхати махсулотҳое, кн барои онхо амали сертиф икат мутобиқат<br>
            мукаррар шудааст
        </td>
    </tr>
</table>
<br/>
<table width="100%" border="1" cellpadding="5">
    <tr>
        <td style="
			font-size: 10px;
			font-weight: bold;
			line-height: 20px;
			padding: 0;
			text-align: center;
			width: 12%;
		">р/б
        </td>
        <td style="
			font-size: 10px;
			font-weight: bold;
			line-height: 20px;
			padding: 0;
			text-align: center;
			width: 60%;
		">Номгӯи маҳсулот
        </td>

        <td style="
			font-size: 10px;
			font-weight: bold;
			line-height: 20px;
			padding: 0;
			text-align: center;
			width: 12%;
		">микдор
        </td>

    </tr>
    {!!  $productsSection  !!}
</table>
<table width="100%" border="0" cellpadding="5">
    <tr>
        <td style="

			font-size: 10px;
			line-height: 20px;
			padding: 0;
			text-align: center;
			width: 12%;
		">
        </td>
        <td style="
			font-size: 10px;
			line-height: 20px;
			padding: 0;
			text-align: right;
			width: 60%;
			font-weight:bold;
		">Ҳамагӣ
        </td>

        <td style="
			font-size: 10px;
			line-height: 20px;
			padding: 0;
			text-align: center;
			width: 12%;
		"> {{$totalNettoWeight }} <span style="font-weight:bold;text-align:right">кг</span></td>

    </tr>
</table>
{!! $signatureHtml !!}






