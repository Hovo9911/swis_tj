<?php
use App\Models\Document\Document;
?>
@extends('layouts.app')

@section('title'){{trans('swis.document.create.title')}}@stop

@section('contentTitle')
    {{trans('swis.document.create.title')}}
@stop

@section('topScripts')
    <script>
        var $disabledFields = "{{(empty($document) || $isDocumentOwner) ? false : true}}";
    </script>
@endsection

<?php

$jsTrans->addTrans([
    'swis.document.label.other',
]);

$gUploader = new \App\GUploader('document_report');

$disabledFieldsStr = '';
$rejectedDisabledFieldsStr = '';

if (isset($document)) {
    if (($document->document_status != Document::DOCUMENT_STATUS_CREATED) && $document->document_status != Document::DOCUMENT_STATUS_REGISTERED) {
        $disabledFieldsStr = 'disabled';
    }

    if (($document->document_status == Document::DOCUMENT_STATUS_SUSPENDED || $document->document_status == Document::DOCUMENT_STATUS_REJECTED)) {
        $rejectedDisabledFieldsStr = 'disabled';
    }
}

?>

@section('form-buttons-top')
    @if($saveMode != Document::VIEW_MODE_VIEW)

        @if($isAgencyApprove)

            {{-- Confirming Agency --}}
            {!! Document::generateButtonsForConfirmingAgency($document) !!}

        @else
            {!! Document::generateDocumentStatusButtons(!empty($document->document_status) ? $document->document_status : '') !!}

            @if($saveMode == Document::VIEW_MODE_EDIT && $document->document_status == Document::DOCUMENT_STATUS_CREATED)
                 {!! renderDeleteButton()  !!}
            @endif

        @endif
    @endif
    <button type="button" class="btn  btn-default button-cancel">{{trans('core.base.button.back')}}</button>
@stop

@section('content')

    <?php
    switch ($saveMode) {
        case 'add':
            $url = 'document';
            break;
        case 'edit':
            $url = 'document/update/' . $document->id;
            break;
        case 'view':
            $url = '';
            break;
    }

    $showInfo = false;
    if ($saveMode == Document::VIEW_MODE_EDIT || $saveMode == Document::VIEW_MODE_VIEW) {
        $showInfo = true;
    }

    $isElectronicType = false;
    if (isset($document) && $document->document_form == Document::DOCUMENT_FORM_ELECTRONIC) {
        $isElectronicType = true;
    }

    $authUserCreator = \Auth::user()->getCreator();
    ?>

    <!-- Modal section -->
    <div class="modal fade" id="permissionDeniedModal" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{trans('swis.document.access_denied.title')}}</h4>
                </div>
                <div class="modal-body">
                    <div class="modal-refresh-confirm-message">{{trans('swis.document.access_denied.access_denied_text')}}</div>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-primary btn-refresh"
                       href="{{urlWithLng('document')}}">{{trans('swis.document.access_denied.back_url')}}</a>
                </div>
            </div>
        </div>
    </div>


    <form class="form-horizontal fill-up" id="form-data" action="{{urlWithLng($url)}}" enctype="multipart/form-data"
          autocomplete="off">

        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-general">{{trans('core.base.tab.general')}}</a></li>
                <li><a data-toggle="tab" href="#tab-sides">{{trans('swis.single_app.sides')}}</a></li>
                @if($saveMode == Document::VIEW_MODE_ADD || $isDocumentOwner)
                    <li><a data-toggle="tab" href="#tab-notes">{{trans('swis.single_app.notes')}}</a></li>
                @endif
                @if($showInfo && $isDocumentOwner)
                    <li><a data-toggle="tab"
                           href="#tab-attached-history">{{trans('swis.single_app.attached_history')}}</a></li>
                @endif
            </ul>
            <div class="tab-content">
                <div id="tab-general" class="tab-pane active">
                    <div class="panel-body">

                        @if($showInfo)
                            <div class="form-group">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{ trans('swis.document.document_type') }}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input value="{{ trans('swis.document.document_type.'.$document->document_form.'.name') }}"
                                                   disabled type="text" class="form-control" autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if($isElectronicType)
                            <div class="form-group">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{ trans('swis.document.electronic.sub_application.state.name') }}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input value="{{ $electronicDocumentStateName }}" disabled type="text"
                                                   class="form-control" autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if($showInfo)
                            <div class="form-group">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{ trans('swis.document.version') }}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input value="{{ $document->version }}" disabled type="text"
                                                   class="form-control"
                                                   autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="form-group"><label
                                    class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.document.add_to_folder')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-lg-10 col-md-8 field__one-col">
                                        <select class="form-control select2"
                                                {{$rejectedDisabledFieldsStr}} name="folders[]"
                                                multiple>
                                            @if(!empty($folders))
                                                @foreach($folders as $folder)
                                                    <option {{(!empty($documentFolders) && in_array($folder->id,$documentFolders)) ? 'selected' : ''}} value="{{$folder->id}}">{{$folder->folder_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <div class="form-error" id="form-error-email"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if($showInfo)

                            <div class="form-group"><label
                                        class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.document.uuid')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input value="{{$document->uuid or ''}}" disabled type="text" placeholder=""
                                                   class="form-control" autocomplete="off">
                                            <div class="form-error" id="form-error-email"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group"><label
                                        class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.document.access_password')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input value="{{$document->document_access_password or ''}}"
                                                   name="access_password"
                                                   disabled
                                                   type="text" placeholder="" class="form-control">
                                            <div class="form-error" id="form-error-access_password"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @if(!$isElectronicType)
                                <div class="form-group"><label
                                            class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.document.regenerate_access_key')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <input {{$rejectedDisabledFieldsStr}} value="1"
                                                       name="regenerate_access_key"
                                                       type="checkbox" placeholder="">
                                                <div class="form-error" id="form-error-regenerate_access_key"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                        @endif

                        <div class="form-group">
                            <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.document.type')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-lg-10 col-md-8 field__one-col">
                                        <input value="{{$document->document_type or ''}}"
                                               {{$disabledFieldsStr}} name="document_type"
                                               data-fields="{{json_encode([['from'=>'name','to'=>'document_name'],['from'=>'id','to'=>'document_type_id']])}}"
                                               type="text" placeholder="{{trans('swis.document.label.autocomplete')}}"
                                               class="form-control documentTypeInput"
                                               data-source="{{\App\Models\ReferenceTable\ReferenceTable::getReferenceIdByTableName(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_DOCUMENT_TYPE_REFERENCE)}}"
                                               autocomplete="off">

                                        <input type="hidden" name="document_type_id"
                                               value="{{$document->document_type_id or ''}}" id="docTypeId">

                                        <div class="form-error" id="form-error-document_type"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.name')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-lg-10 col-md-8 field__one-col">
                                        <input value="{{$document->document_name or ''}}" readonly name="document_name"
                                               type="text" placeholder=""
                                               class="form-control">
                                        <div class="form-error" id="form-error-document_name"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.description')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-lg-10 col-md-8 field__one-col">
                                        <textarea {{$disabledFieldsStr}}
                                                  name="document_description"
                                                  placeholder=""
                                                  class="form-control"
                                                  rows="5">{{$document->document_description or ''}}</textarea>
                                        <div class="form-error" id="form-error-document_description"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.document.number')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-lg-10 col-md-8 field__one-col">
                                        <input {{$disabledFieldsStr}} value="{{$document->document_number or ''}}"
                                               name="document_number" type="text"
                                               placeholder="" class="form-control">
                                        <div class="form-error" id="form-error-document_number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.document.release_date')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                   {{-- <div class="col-lg-10 col-md-8 field__one-col">
                                        <input {{$disabledFieldsStr}} value="{{$document->document_release_date or ''}}"
                                               name="document_release_date"
                                               type="date"
                                               placeholder="" class="form-control">
                                    </div>--}}

                                    <div class="col-lg-10 col-md-8 field__one-col">
                                        <div class='input-group date'>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                            <input autocomplete="off" placeholder="{{setDatePlaceholder()}}" {{$disabledFieldsStr}} value="{{$document->document_release_date ?? ''}}" name="document_release_date" data-end-date="{{currentDate()}}" type='text' class="form-control"/>
                                        </div>
                                        <div class="form-error" id="form-error-document_release_date"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group"><label
                                    class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.document.from_whom')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-lg-10 col-md-8 field__one-col">
                                        @if($saveMode == Document::VIEW_MODE_ADD)

                                            <select {{$disabledFieldsStr}} name="approve_type" id="approveType"
                                                    class="form-control " readonly></select>

                                        @elseif($saveMode == Document::VIEW_MODE_EDIT || $saveMode == Document::VIEW_MODE_VIEW)
                                            <select {{$disabledFieldsStr}} name="approve_type" id="approveType"
                                                    class="form-control" {{!$refDocumentTypeAgencies->count() ? 'readonly' : ''}}>
                                                @if($refDocumentTypeAgencies->count())
                                                    @foreach($refDocumentTypeAgencies as $agency)
                                                        <option
                                                                {{($document->approve_type == Document::APPROVE_AGENCY
                                                                && $agency->agency_code == $document->approve_agency_tax_id) ? 'selected' : ''}}
                                                                value="{{Document::APPROVE_AGENCY}}"
                                                                data-tax-id="{{$agency->agency_code}}">
                                                            {{$agency->agency_name}}
                                                        </option>
                                                    @endforeach
                                                @endif
                                                <option {{($document->approve_type == Document::APPROVE_OTHER) ? 'selected' : ''}}
                                                        value="{{Document::APPROVE_OTHER}}">
                                                    {{trans('swis.document.label.other')}}
                                                </option>
                                            </select>

                                        @endif

                                        <input type="hidden" value="{{!empty($document->approve_agency_tax_id) ? $document->approve_agency_tax_id : ''}}"
                                               name="approve_agency_tax_id" id="approveAgency">
                                        <div class="form-error" id="form-error-approve_type"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.document.scanned_version')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-lg-10 col-md-8 field__one-col">

                                        @if(!$isElectronicType)
                                            @if(isset($document) && $document->filePath())
                                                <div class="documents-download__result">
                                                    <a target="_blank"
                                                       href="{{$document->filePath()}}">{{$document->fileBaseName()}}</a>
                                                    <span class="documents-separate-line">|</span>
                                                    <a class="btn btn-primary btn-xs" download=""
                                                       href="{{$document->filePath()}}"><i
                                                                class="fas fa-download"></i> {{trans('swis.document.label.download')}}
                                                    </a>
                                                </div>
                                            @endif

                                             @if($saveMode != Document::VIEW_MODE_VIEW)
                                                 <div class="g-upload-container">
                                                     <div class="register-form__list">
                                                         <div class="register-form__item">

                                                             <div class="g-upload-box">
                                                                 <label class="btn btn-primary btn-upload">
                                                                     <i class="fa fa-upload"></i>
                                                                     {{trans('swis.core.upload.button')}}
                                                                <input {{$disabledFieldsStr}} id="files"
                                                                       class="dn g-upload" type="file"
                                                                       name="document_file"
                                                                       value="{{$document->document_file or ''}}"
                                                                       {{$gUploader->isMultiple() ? 'multiple' : ''}} data-conf="document_report"
                                                                       data-action="{{urlWithLng('/g-uploader/upload')}}"
                                                                       accept="{{$gUploader->getAcceptTypes()}}"/>
                                                            </label>
                                                        </div>

                                                        <div class="license-form__uploaded-file-list g-uploaded-list"></div>
                                                        <progress class="progressBarFileUploader" value="0"
                                                                  max="100"></progress>
                                                        <div class="form-error-text form-error-files fb fs12"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif

                                            <div class="form-error" id="form-error-document_file"></div>
                                        @else
                                            <a target="_blank"
                                               href="{{$document->filePath()}}">{{trans('swis.document.electronic_document.link')}}</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 col-lg-3 control-label label__title pd-t-0">{{trans('swis.document.subject_for_approvement')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-lg-10 col-md-8 field__one-col">
                                        <?php
                                        //                                dd($document);
                                        //                                    dd(($saveMode == Document::VIEW_MODE_EDIT
                                        //                                        && $document->approve_type == App\Models\Document\Document::APPROVE_AGENCY
                                        //                                        && ($document->document_status == Document::DOCUMENT_STATUS_REGISTERED
                                        //                                            or $document->document_status == Document::DOCUMENT_STATUS_CREATED)));
                                        //                                    dd(( $document->subject_for_approvement  ));
                                        ?>
                                        <input {{$disabledFieldsStr}} value="1"
                                               name="subject_for_approvement"
                                               {{((isset($document) && $document->subject_for_approvement && $document->approve_type != Document::APPROVE_OTHER )  ? 'checked' : "" )}} id="subjectApp"
                                               {{(($saveMode == Document::VIEW_MODE_EDIT
                                               && $document->approve_type == App\Models\Document\Document::APPROVE_AGENCY
                                               && ($document->document_status == Document::DOCUMENT_STATUS_REGISTERED
                                               or $document->document_status == Document::DOCUMENT_STATUS_CREATED))) ? '' : 'disabled'}}
                                               type="checkbox"
                                               placeholder="">
                                        <div class="form-error" id="form-error-subject_for_approvement"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if($showInfo)

                            <div class="form-group">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.document.document_status')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            @if(!empty($document->document_status))
                                                <div>
                                                    <input type="text" disabled class="form-control"
                                                           value="{{trans('swis.document.'.$document->document_status.'.title')}}">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @if(!empty($document->confirmation_date))
                                <div class="form-group">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.document.confirmation_date')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <input value="{{$document->confirmation_date or ''}}" disabled
                                                       type="text"
                                                       placeholder="" class="form-control">
                                                <div class="form-error" id="form-error-confirmation_date"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if(!empty($document->confirming))
                                <div class="form-group">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.document.confirming')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <input value="{{$document->confirming or ''}}" disabled type="text"
                                                       placeholder="" class="form-control">
                                                <div class="form-error" id="form-error-confirming"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                        @endif
                    </div>
                </div>
                <div id="tab-sides" class="tab-pane">
                    <div class="panel-body p-lr-n">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                {{trans('swis.document.holder.title')}}
                            </div>
                            <div class="panel-body" id="holderInfo">
                                <div class="form-group required">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.legal_entity_form')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">

                                                <?php $refData = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_LEGAL_ENTITY_FORM);?>

                                                <select {{$disabledFieldsStr}} class="form-control  legalEntityType"
                                                        data-type="holderTaxType" name="holder_type">
                                                    <option disabled="" selected
                                                            value="">{{trans('swis.document.label.select')}}</option>
                                                    @if(count($refData) > 0)
                                                        @foreach($refData as $ref)
                                                            <option value="{{$ref->id}}" {{(isset($document) && $document->holder_type == $ref->id) ? 'selected' : '' }}>{{$ref->name}}</option>
                                                        @endforeach
                                                    @endif

                                                </select>
                                                <div class="form-error" id="form-error-holder_type"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group required">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.base.label.tax_id_or_ssn')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <input {{$disabledFieldsStr}} value="{{$document->holder_type_value or ''}}"
                                                       name="holder_type_value" type="text" placeholder=""
                                                       data-type="holderTaxType"
                                                       @if(\Auth::user()->isBroker())
                                                       data-fields="{{json_encode([['from'=>'name','to'=>'holder_name'],['from'=>'country','to'=>'holder_country'],['from'=>'address','to'=>'holder_address'],['from'=>'community','to'=>'holder_community']])}}"
                                                       @endif
                                                       class="form-control taxIdOrSSN getPartiesInfo">
                                                <div class="form-error" id="form-error-holder_type_value"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group required">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.passport')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <input {{$disabledFieldsStr}}   value="{{$document->holder_passport or ''}}"
                                                       name="holder_passport" type="text" placeholder=""
                                                       class="form-control passportField getPartiesInfo onlyLatinAlpha">
                                                <div class="form-error" id="form-error-holder_passport"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{-- <div class="form-group required">
                                <label  class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.user.passport')}}</label>
                                <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                 <input {{$disabledFieldsStr}} value="{{$document->holder_passport or ''}}"
                                                        name="holder_passport" type="text" placeholder=""
                                                        data-type="holderPassport"
                                                        class="form-control">
                                                 <div class="form-error" id="form-error-holder_passport"></div>
                                                 </div>
                                                 </div>
                                     </div>
                                 </div>--}}

                                <div class="form-group required">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.base.name')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <input value="{{$document->holder_name or ''}}" readonly
                                                       name="holder_name"
                                                       type="text"
                                                       placeholder=""
                                                       class="form-control f-citizen">
                                                <div class="form-error" id="form-error-holder_name"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php $refData = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_COUNTRIES);?>
                                <div class="form-group required">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.country')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <select name="holder_country"
                                                        class="form-control select2 readonly {{ $saveMode == Document::VIEW_MODE_VIEW ? '':'f-citizen'}}">
                                                    <option value=""></option>
                                                    @if(count($refData) > 0)
                                                        @foreach($refData as $ref)
                                                            <option value="{{$ref->id}}" {{(!empty($document->holder_country) && ($document->holder_country == $ref->id) ? 'selected' : '')}} >{{'('.$ref->code.') '.$ref->name}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                <div class="form-error" id="form-error-holder_country"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.community')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <input value="{{$document->holder_community or ''}}"
                                                       name="holder_community"
                                                       type="text" placeholder=""
                                                       class="form-control">
                                                <div class="form-error" id="form-error-holder_community"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group required">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.address')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <input value="{{$document->holder_address or ''}}"
                                                       name="holder_address"
                                                       type="text" placeholder=""
                                                       class="form-control">
                                                <div class="form-error" id="form-error-holder_address"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row required">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.phone')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <div class="input-group">
                                                    <span class="input-group-addon @if($rejectedDisabledFieldsStr) readonly @endif">+</span>
                                                    <input type="text"
                                                           placeholder=""
                                                           class="form-control onlyNumbers"
                                                           name="holder_phone_number"
                                                           value="{{$document->holder_phone_number or ''}}"
                                                           {{$disabledFieldsStr}}
                                                           autocomplete="off">
                                                </div>
                                                <div class="form-error" id="form-error-holder_phone_number"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group required">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.email')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <input {{$disabledFieldsStr}} value="{{$document->holder_email or ''}}"
                                                       name="holder_email"
                                                       type="text" placeholder=""
                                                       class="form-control">
                                                <div class="form-error" id="form-error-holder_email"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                {{trans('swis.document.creator.title')}}
                            </div>
                            <div class="panel-body">

                                <div class="form-group">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.tin_passport')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <input value="{{$document->creator_type_value or $authUserCreator->ssn}}"
                                                       id="creator_ssn"
                                                       disabled
                                                       type="text" placeholder=""
                                                       class="form-control">
                                                <div class="form-error" id="form-error-creator_type_value"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.base.name')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">

                                                <input value="{{$document->creator_name or $authUserCreator->name}}"
                                                       id="creator_name"
                                                       disabled
                                                       type="text"
                                                       placeholder=""
                                                       class="form-control">
                                                <div class="form-error" id="form-error-creator_name"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.address')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <input value="{{$document->creator_address or $authUserCreator->address}}"
                                                       id="creator_address"
                                                       disabled
                                                       type="text" placeholder=""
                                                       class="form-control">
                                                <div class="form-error" id="form-error-creator_address"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.phone')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <input value="{{$document->creator_phone_number or $authUserCreator->phone_number}}"
                                                       id="creator_phone_number"
                                                       disabled
                                                       type="text" placeholder=""
                                                       class="form-control">
                                                <div class="form-error" id="form-error-creator_phone"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.email')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <input value="{{$document->creator_email or  $authUserCreator->email}}"
                                                       id="creator_email"
                                                       disabled
                                                       type="text" placeholder=""
                                                       class="form-control">
                                                <div class="form-error" id="form-error-creator_phone"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

                @if($saveMode == Document::VIEW_MODE_ADD || $isDocumentOwner)
                    <div id="tab-notes" class="tab-pane">
                        <div class="panel-body p-lr-n">
                            @include('document.document-notes')
                        </div>
                    </div>
                @endif

                @if($showInfo && $isDocumentOwner)
                    <div id="tab-attached-history" class="tab-pane">
                        <div class="panel-body p-lr-n">

                                <div class="row">
                                <div class="col-md-6">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>{{trans('swis.document.attached_history.attached_from')}}</th>
                                            <th>{{trans('swis.document.attached_history.version')}}</th>
                                            <th>{{trans('swis.document.attached_history.user')}}</th>
                                            <th>{{trans('swis.document.attached_history.attached_date')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if (isset($attachedDocHistory) && count($attachedDocHistory) > 0)
                                            @foreach ($attachedDocHistory as $attach)
                                                <tr>
                                                    <td><a href="{{ $attach['attached_link'] }}"><i class="{{ $attach['icon'] }}"></i> {{ $attach['attached_from'] }}</a></td>
                                                    <td>{{ $attach['version'] }}</td>
                                                    <td>{{ $attach['user'] }}</td>
                                                    <td>{{ $attach['attached_date'] }}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                @endif

            </div>
        </div>
        <input type="hidden" value="{{$document->id or 0}}" name="id" class="mc-form-key"/>
        <input type="hidden" id="documentStatus" value="{{$document->document_status or 0}}" name="document_status"/>
    </form>

@endsection

@section('form-buttons-bottom')
    @if($saveMode != Document::VIEW_MODE_VIEW)

        @if($isAgencyApprove)
            {{-- Confirming Agency --}}
            {!! Document::generateButtonsForConfirmingAgency($document) !!}
        @else
            {!! Document::generateDocumentStatusButtons(!empty($document->document_status) ? $document->document_status : '') !!}

            @if($saveMode == Document::VIEW_MODE_EDIT && $document->document_status == Document::DOCUMENT_STATUS_CREATED)
                {!! renderDeleteButton()  !!}
            @endif
        @endif

    @endif
    <button type="button" class="btn  btn-default button-cancel">{{trans('core.base.button.back')}}</button>
@stop

@section('scripts')
    <script src="{{asset('assets/helix/core/plugins/bootstrap-typeahead/bootstrap-typeahead.js')}}"></script>
    <script src="{{asset('js/document/main.js')}}"></script>
    <script src="{{asset('js/userType.js')}}"></script>
    <script src="{{asset('js/guploader.js')}}"></script>
@endsection