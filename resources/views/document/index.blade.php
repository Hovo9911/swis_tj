@extends('layouts.app')

@section('title'){{trans('swis.document.title')}}  @stop

@section('contentTitle') {{trans('swis.document.title')}}@stop

@section('form-buttons-top')
    {!! renderAddButton() !!}
@stop

<?php

$docStatusTrans = [];
foreach ($documentStatuses as $docStatus) {
    $docStatusTrans[] = 'swis.document.'.$docStatus.'.title';
}

$jsTrans->addTrans($docStatusTrans);

?>

@section('content')
    <div class="clearfix search__out">
        <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse" data-target="#collapseSearch" aria-expanded="false" aria-controls="collapseSearch">
            <i class="fa fa-search"> </i><span class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
        </button>
    </div>
    <div class="collapse" id="collapseSearch">
        <div class="card card-body">
            <form class="form-horizontal fill-up" id="search-filter" autocomplete="off">

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">ID</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="number" min="0" class="form-control onlyNumbers" name="id" id="" value="">
                                <input value="folder" name="moduleForDocuments" type="hidden">
                                <div class="form-error" id="form-error-id"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.name')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="document_name" id="" value="">
                                <div class="form-error" id="form-error-document_name"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.document.access_password')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="document_access_password" id="" value="">
                                <div class="form-error" id="form-error-document_access_password"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title ">{{trans('swis.document.document_type')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <select class="select2" name="document_type" id="document_type">
                                    <option></option>
                                    @if($referenceDocumentTypes->count())
                                        @foreach($referenceDocumentTypes as $referenceDocumentType)
                                            <option value="{{$referenceDocumentType->code}}">
                                                ({{$referenceDocumentType->code}}) {{$referenceDocumentType->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <div class="form-error" id="form-error-document_type"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.document.document_description')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="document_description"
                                       id="document_description" value="">
                                <div class="form-error" id="form-error-document_description"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.document.document_number')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="document_number" id="document_number"
                                       value="">
                                <div class="form-error" id="form-error-document_number"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.document.document_release_date')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-4 col-lg-5 field__two-col">
                                <div class='input-group date'>
                                    <span class="input-group-addon"><span
                                                class="glyphicon glyphicon-calendar"></span></span>
                                    <input value="" name="document_release_date_start"
                                           type="text"
                                           placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                    <div class="form-error" id="form-error-document_release_date_start"></div>
                                </div>
                            </div>
                            {{--<div class="guion">-</div>--}}
                            <div class="col-md-4 col-lg-5 field__two-col">

                                <div class='input-group date'>
                                    <span class="input-group-addon"><span
                                                class="glyphicon glyphicon-calendar"></span></span>
                                    <input value="" name="document_release_date_end"
                                           type="text"
                                           placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                    <div class="form-error" id="form-error-document_release_date_end"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.document.document_status')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <select class="select2 form-control" name="document_status">
                                    <option></option>
                                    @if(count($documentStatuses))
                                        @foreach($documentStatuses as $documentStatus)
                                            @if($documentStatus == \App\Models\Document\Document::DOCUMENT_STATUS_SAVED) @continue @endif
                                            <option value="{{$documentStatus}}">{{trans('swis.document.'.$documentStatus.'.title')}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <div class="form-error" id="form-error-document_status"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8 col-lg-offset-3 text-right">
                        <div class="row">
                            <div class="col-lg-10 col-md-8">
                                <button type="submit"
                                        class="btn btn-primary  mr-10">{{trans('core.base.label.search')}}</button>
                                <button type="button" class="btn btn-danger resetButton"
                                        data-submit="true">{{trans('core.base.label.reset')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="data-table">
        <div class="clearfix">
            <table class="table table-striped table-bordered table-hover dataTables-example" id="list-data">
                <thead>
                <tr>
                    <th data-col="actions" data-delete="0" class="th-actions">{{trans('core.base.label.actions')}}</th>
                    <th data-col="id">{{trans('swis.document.id')}}</th>
                    <th data-col="uuid">{{trans('swis.document.uuid')}}</th>
                    <th data-col="document_access_password">{{trans('swis.document.access_password')}}</th>
                    <th data-col="document_name">{{trans('core.base.label.name')}}</th>
                    <th data-col="document_description">{{trans('core.base.label.description')}}</th>
                    <th data-col="document_status">{{trans('core.base.label.status')}}</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/document/main.js')}}"></script>
@endsection