@extends('layouts.app')

@section('title'){{trans('swis.global-notifications.notifications.title')}}@stop

@section('contentTitle') {{trans('swis.global-notifications.notifications.title')}} @stop

@section('form-buttons-top')
    {!! renderAddButton() !!}
@stop

@section('content')

    <div class="clearfix search__out">
        <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse" data-target="#collapseSearch" aria-expanded="false" aria-controls="collapseSearch">
            <i class="fa fa-search"> </i><span class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
        </button>
    </div>

    <div class="collapse" id="collapseSearch">
        <div class="card card-body">
            <div class="row">
                <form class="form-horizontal fill-up" id="search-filter" autocomplete="off">

                    <div class="form-group"><label class="col-md-4 col-lg-3 control-label">ID</label>
                        <div class="col-lg-6">
                            <input type="number" min="0" class="form-control onlyNumbers" name="id" id="" value="">
                            <div class="form-error" id="form-error-id"></div>
                        </div>
                    </div>

                    <div class="form-group"><label
                                class="col-md-4 col-lg-3 control-label">{{trans('swis.global-notifications.search.name')}}</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" name="name" id="" value="">
                            <div class="form-error" id="form-error-name"></div>
                        </div>
                    </div>

                    <div class="form-group text-right">
                        <div class="col-sm-offset-5 col-sm-4">
                            <button type="submit"
                                    class="btn btn-primary">{{trans('core.base.label.search')}}</button>
                            <button type="button" class="btn btn-danger resetButton"
                                    data-submit="true">{{trans('core.base.label.reset')}}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="data-table">
        <div class="clearfix">
            <table class="table table-striped table-bordered table-hover notification-templates-datatable" id="list-data">
                <thead>
                <tr>
                    <th data-col="actions" data-delete="0" class="th-actions">{{trans('core.base.label.actions')}}</th>
                    <th data-col="id">ID</th>
                    <th data-col="name">{{trans('swis.global-notifications.label.name')}}</th>
                    <th data-col="sent_at" data-ordering="0">{{trans('swis.global-notifications.label.sent_at')}}</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    <!-- Opening modal for send notification -->
    <div id="notificationModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal__title fb">{{trans('swis.global-notifications.modal.header_text')}}</h3>
                    <button type="button" class="close modal__close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="successAlert"></div>
                    <div class="form-group row">
                        <label class="col-lg-4 control-label">{{trans('swis.global-notifications.modal.user_types')}}</label>
                        <div class="col-lg-8">
                            @foreach(\App\Models\User\User::USER_TYPES_FOR_GLOBAL_NOTIFICATIONS as $userType)
                                <label>
                                    <input type="radio" name="user_type" value="{{$userType}}" id="{{$userType}}" class="user_type"
                                           @if($userType == \App\Models\User\User::USER_TYPE_ALL_USERS) checked @endif>
                                    {{trans('swis.user_type.'.$userType.'.title')}}
                                </label><br>
                            @endforeach
                        </div>
                    </div>
                    <div class="form-group row agencies hide">
                        <label class="col-lg-4 control-label">{{trans('swis.global-notifications.modal.agency')}}</label>
                        <div class="col-lg-8">
                            @if($agencies->count())
                                <select class="form-control select2" id="agencies" name="agencies[]" multiple>
                                    @foreach($agencies as $agency)
                                        <option value="{{$agency->tax_id}}">{{$agency->name}}</option>
                                    @endforeach
                                </select>
                                <div class="form-error" id="form-error-agency_id"></div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger"
                            data-dismiss="modal">{{trans('swis.global-notifications.modal.cancel')}}</button>
                    <button type="button"
                            class="btn btn-primary sendNotification"><i class="fa fa-send"></i>{{" ".trans('swis.global-notifications.modal.send')}}</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/global-notifications/main.js')}}"></script>
@endsection