@extends('layouts.app')

@section('title'){{trans('swis.global-notifications.notification_title')}}@stop

@section('contentTitle') {{trans('swis.global-notifications.notification_title')}} @stop

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('content')
    <?php

    switch ($saveMode) {
        case 'add':
            $url = 'global-notifications';
            break;
        default:
            $url = $saveMode != 'view' ? 'global-notifications/update/' . $notifications->id : '';

            $mls = $notifications->ml()->notDeleted()->base(['lng_id', 'email_subject', 'email_notification', 'in_app_notification', 'sms_notification', 'show_status'])->keyBy('lng_id');

            break;
    }

    if(isset($clone)){
        $mls = $notifications->ml()->notDeleted()->base(['lng_id', 'email_subject', 'email_notification', 'in_app_notification', 'sms_notification', 'show_status'])->keyBy('lng_id');
    }

    $languages = activeLanguages();

    ?>
    <form class="form-horizontal fill-up" id="form-data" action="{{urlWithLng($url)}}">

        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab"
                                      href="#tab-general"> {{trans('core.base.tab.notifications')}}</a></li>
                @foreach($languages as $language)
                    <li class=""><a data-toggle="tab"
                                    href="#tab-{{$language->code}}">{{ trans("core.base.language.".strtolower($language->name)) }}</a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content">
                <div id="tab-general" class="tab-pane active">
                    <div class="panel-body">

                        <div class="form-group required"><label
                                    class="col-lg-2 control-label">{{trans('swis.global-notifications.label.name')}}</label>
                            <div class="col-lg-10">
                                <input value="{{$notifications->name or ''}}" name="name" type="text"
                                       placeholder="" class="form-control">
                                <div class="form-error" id="form-error-name"></div>
                            </div>
                        </div>

                        @if($saveMode == 'add')
                            <div class="form-group"><label
                                        class="col-lg-2 control-label">{{trans('swis.global-notifications.label.locked')}}</label>
                                <div class="col-lg-10">
                                    <label class="radio-inline">
                                        <input type="radio" name="locked"
                                               value="0"
                                               @if(!isset($notifications->locked) or (isset($notifications->locked) and $notifications->locked == 0)) checked @endif/> {{ trans('swis.global-notifications.locked.no') }}
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="locked"
                                               value="1"
                                               @if(isset($notifications->locked) and $notifications->locked) checked @endif/> {{ trans('swis.global-notifications.locked.yes') }}
                                    </label>
                                </div>
                            </div>
                        @endif

                        <div class="form-group hide">
                            <label class="control-label col-lg-2">{{trans('core.base.label.status')}}</label>
                            <div class="col-lg-10 general-status">
                                <select name="show_status" class="form-show-status">
                                    <option value="{{\App\Models\NotificationTemplates\NotificationTemplates::STATUS_ACTIVE}}"{{(!empty($notifications) && $notifications->show_status == \App\Models\NotificationTemplates\NotificationTemplates::STATUS_ACTIVE) || $saveMode == 'add' ?  ' selected="selected"' : ''}}>{{trans('core.base.label.status.active')}}</option>
                                    <option value="{{\App\Models\NotificationTemplates\NotificationTemplates::STATUS_INACTIVE}}"{{!empty($notifications) &&  $notifications->show_status == \App\Models\NotificationTemplates\NotificationTemplates::STATUS_INACTIVE ? ' selected="selected"': ''}}>{{trans('core.base.label.status.inactive')}}</option>
                                </select>
                                <div class="form-error" id="form-error-show_status"></div>
                            </div>
                        </div>


                    </div>
                </div>

                @foreach($languages as $language)
                    <?php $lngId = $language->id ?>
                    <div id="tab-{{$language->code}}" class="tab-pane">
                        <div class="panel-body">

                            <div class="form-group required"><label
                                        class="col-lg-2 control-label">{{trans('swis.global-notifications.label.email_template_subject')}}</label>
                                <div class="col-lg-10">
                                    <input value="{{isset($mls[$lngId]) ? $mls[$lngId]->email_subject : ''}}"
                                           name="ml[{{$language->id}}][email_subject]" type="text"
                                           placeholder=""
                                           class="form-control">

                                    <div class="form-error"
                                         id="form-error-ml-{{$language->id}}-email_subject"></div>
                                </div>
                            </div>

                            <div class="form-group required"><label
                                        class="col-lg-2 control-label">{{trans('swis.global-notifications.label.email_template_body')}}</label>
                                <div class="col-lg-10">
                                    <textarea rows="6" name="ml[{{$language->id}}][email_notification]"
                                              class="form-control ckeditor"
                                              @if($saveMode != 'view')id="ml_{{$language->id}}_editor" @endif>{{isset($mls[$lngId]) ? $mls[$lngId]->email_notification : ''}}</textarea>
                                    <div class="form-error"
                                         id="form-error-ml-{{$language->id}}-email_notification"></div>
                                </div>
                            </div>

                            <div class="form-group required"><label
                                        class="col-lg-2 control-label">{{trans('swis.global-notifications.label.in_app_notification')}}</label>
                                <div class="col-lg-10">
                                    <textarea rows="6" name="ml[{{$language->id}}][in_app_notification]"
                                              class="form-control">{{isset($mls[$lngId]) ? $mls[$lngId]->in_app_notification : ''}}</textarea>
                                    <div class="form-error"
                                         id="form-error-ml-{{$language->id}}-in_app_notification"></div>
                                </div>
                            </div>
                            @if(config('swis.sms_send'))
                                <div class="form-group required"><label
                                            class="col-lg-2 control-label">{{trans('swis.global-notifications.label.sms_notification')}}</label>
                                    <div class="col-lg-10">
                                    <textarea rows="6" name="ml[{{$language->id}}][sms_notification]"
                                              class="form-control">{{isset($mls[$lngId]) ? $mls[$lngId]->sms_notification : ''}}</textarea>
                                        <div class="form-error"
                                             id="form-error-ml-{{$language->id}}-sms_notification"></div>
                                    </div>
                                </div>
                            @endif

                            <div class="form-group hide">
                                <label class="control-label col-lg-2">{{trans('core.base.label.status')}}</label>
                                <div class="col-lg-10 ml-status general-status">
                                    <select name="ml[{{$lngId}}][show_status]" class="form-show-status">
                                        <option value="{{\App\Models\NotificationTemplates\NotificationTemplates::STATUS_ACTIVE}}"{{(isset($mls[$lngId]) && $mls[$lngId]->show_status == \App\Models\NotificationTemplates\NotificationTemplates::STATUS_ACTIVE) || $saveMode == 'add' ? ' selected="selected"' : ''}}>{{trans('core.base.label.status.active')}}</option>
                                        <option value="{{\App\Models\NotificationTemplates\NotificationTemplates::STATUS_INACTIVE}}"{{isset($mls[$lngId]) && $mls[$lngId]->show_status == \App\Models\NotificationTemplates\NotificationTemplates::STATUS_INACTIVE ? ' selected="selected"': ''}}>{{trans('core.base.label.status.inactive')}}</option>
                                    </select>
                                    <div class="form-error" id="form-error-ml-{{$lngId}}-show_status"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <input type="hidden" value="{{$notifications->id or 0}}" name="id" class="mc-form-key"/>

        {!! csrf_field() !!}
    </form>
@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script>
        var languageList = {{json_encode($languages->pluck('id')->all())}}
    </script>
    <script src="{{asset('js/ckeditor.js')}}"></script>
    <script src="{{asset('js/global-notifications/main.js')}}"></script>
@endsection

