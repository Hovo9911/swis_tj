<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Swis</title>
    <style>
        /* -------------------------------------
    GLOBAL
    A very basic CSS reset
------------------------------------- */
        * {
            margin: 0;
            padding: 0;
            font-family: Arial, sans-serif;
            box-sizing: border-box;
            font-size: 14px;
        }

        img {
            max-width: 100%;
        }

        body {
            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: none;
            width: 100% !important;
            height: 100%;
            line-height: 1.6;
        }

        /* Let's make sure all tables have defaults */
        table td {
            vertical-align: top;
        }

        /* -------------------------------------
            BODY & CONTAINER
        ------------------------------------- */
        body {
            background-color: #f6f6f6;
        }

        .body-wrap {
            background-color: #f6f6f6;
            width: 100%;
        }

        .container {
            display: block !important;
            max-width: 600px !important;
            margin: 0 auto !important;
            /* makes it centered */
            clear: both !important;
        }

        .content {
            max-width: 600px;
            margin: 0 auto;
            display: block;
            padding: 20px;
        }

        /* -------------------------------------
            HEADER, FOOTER, MAIN
        ------------------------------------- */
        .main {
            background: #fff;
            border: 1px solid #e9e9e9;
            border-radius: 3px;
        }

        .content-wrap {
            padding: 20px;
        }

        .content-block {
            padding: 0 0 20px;
        }

        .header {
            width: 100%;
            margin-bottom: 20px;
        }

        .footer {
            width: 100%;
            clear: both;
            color: #999;
        }
        .footer a {
            color: #999;
        }
        .footer p, .footer a, .footer unsubscribe, .footer td {
            font-size: 12px;
        }

        /* -------------------------------------
            TYPOGRAPHY
        ------------------------------------- */
        h1, h2, h3 {
            font-family: Arial, sans-serif;
            color: #000;
            margin: 40px 0 0;
            line-height: 1.2;
            font-weight: 400;
        }

        h1 {
            font-size: 32px;
            font-weight: 500;
        }

        h2 {
            font-size: 24px;
        }

        h3 {
            font-size: 18px;
        }

        h4 {
            font-size: 14px;
            font-weight: 600;
        }

        p, ul, ol {
            margin-bottom: 10px;
            font-weight: normal;
        }
        p li, ul li, ol li {
            margin-left: 5px;
            list-style-position: inside;
        }

        /* -------------------------------------
            LINKS & BUTTONS
        ------------------------------------- */
        a {
            text-decoration: underline;
        }

        .btn-primary {
            text-decoration: none;
            color: #FFF;
            background-color: #1ab394;
            border: solid #1ab394;
            border-width: 5px 30px;
            line-height: 2;
            font-weight: bold;
            text-align: center;
            cursor: pointer;
            display: inline-block;
            border-radius: 5px;
            text-transform: capitalize;
        }

        /* -------------------------------------
            OTHER STYLES THAT MIGHT BE USEFUL
        ------------------------------------- */


        .aligncenter {
            text-align: center;
        }

        .invoice td {
            padding: 5px 0;
        }
        .invoice .invoice-items {
            width: 100%;
        }
        .invoice .invoice-items td {
            border-top: #eee 1px solid;
        }
        .invoice .invoice-items .total td {
            border-top: 2px solid #333;
            border-bottom: 2px solid #333;
            font-weight: 700;
        }

        /* -------------------------------------
            RESPONSIVE AND MOBILE FRIENDLY STYLES
        ------------------------------------- */
        @media only screen and (max-width: 640px) {
            h1, h2, h3, h4 {
                font-weight: 600 !important;
                margin: 20px 0 5px !important;
            }

            h1 {
                font-size: 22px !important;
            }

            h2 {
                font-size: 18px !important;
            }

            h3 {
                font-size: 16px !important;
            }

            .container {
                width: 100% !important;
            }

            .content, .content-wrap {
                padding: 10px !important;
            }

            .invoice {
                width: 100% !important;
            }
        }

    </style>
</head>

<body>
<?php
use App\Http\Controllers\Core\Dictionary\DictionaryManager;
use App\Models\ConstructorTemplates\ConstructorTemplates;
$dictionaryManager = new DictionaryManager();
?>
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" width="600">
            <div class="content">
                <table class="main" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="content-wrap">
                            <table  cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td style="background-color: #1ab394; text-align: center; padding: 21px 0 14px; color: white; text-transform: uppercase;">
                                        <h2 style="font-family: Arial, sans-serif; font-size: 30px;  margin: 0 0 7px; font-weight: bold; color: white;">{{$dictionaryManager->getTransByLngCode("swis.user.register.password_type.single_window", ConstructorTemplates::LANGUAGE_CODE_EN)}}</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="" style="color: #606365; padding-bottom: 10px;">
                                        <h3 style="font-family: Arial, sans-serif; font-weight: bold; margin-bottom: 5px; font-size: 16px; color: #606365; margin-top: 30px;">{{$dictionaryManager->getTransByLngCode("swis.user.register.password_type.dear_user", ConstructorTemplates::LANGUAGE_CODE_EN)}} {{$userName}}</h3>
                                        <p style="font-family: Arial, sans-serif; margin-bottom: 0; font-size: 14px;">{{$dictionaryManager->getTransByLngCode("swis.user.register.password_type.for_setting_password_click_link_below", ConstructorTemplates::LANGUAGE_CODE_EN)}}</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="aligncenter" style="padding-bottom: 10px;">
                                        <h5><a style="color: #337ab7;" href="{{ url($lngCode.'/password/'.$token)}}">{{$dictionaryManager->getTransByLngCode("swis.user.register.password_type.set_password", ConstructorTemplates::LANGUAGE_CODE_EN)}}</a></h5>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="" style="color: #606365; padding-bottom: 10px;">
                                        <p style="font-family: Arial, sans-serif; margin-bottom: 0; font-size: 14px;">{{$dictionaryManager->getTransByLngCode("swis.user.register.password_type.opens_window_enter_password", ConstructorTemplates::LANGUAGE_CODE_EN)}}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table class="main" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="content-wrap">
                            <table  cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td style="background-color: #1ab394; text-align: center; padding: 21px 0 14px; color: white; text-transform: uppercase;">
                                        <h2 style="font-family: Arial, sans-serif; font-size: 30px;  margin: 0 0 7px; font-weight: bold; color: white;">{{$dictionaryManager->getTransByLngCode("swis.user.register.password_type.single_window", ConstructorTemplates::LANGUAGE_CODE_RU)}}</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="" style="color: #606365; padding-bottom: 10px;">
                                        <h3 style="font-family: Arial, sans-serif; font-weight: bold; margin-bottom: 5px; font-size: 16px; color: #606365; margin-top: 30px;">{{$dictionaryManager->getTransByLngCode("swis.user.register.password_type.dear_user", ConstructorTemplates::LANGUAGE_CODE_RU)}} {{$userName}}</h3>
                                        <p style="font-family: Arial, sans-serif; margin-bottom: 0; font-size: 14px;">{{$dictionaryManager->getTransByLngCode("swis.user.register.password_type.for_setting_password_click_link_below", ConstructorTemplates::LANGUAGE_CODE_RU)}}</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="aligncenter" style="padding-bottom: 10px;">
                                        <h5><a style="color: #337ab7;" href="{{ url($lngCode.'/password/'.$token)}}">{{$dictionaryManager->getTransByLngCode("swis.user.register.password_type.set_password", ConstructorTemplates::LANGUAGE_CODE_RU)}}</a></h5>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="" style="color: #606365; padding-bottom: 10px;">
                                       <p style="font-family: Arial, sans-serif; margin-bottom: 0; font-size: 14px;">{{$dictionaryManager->getTransByLngCode("swis.user.register.password_type.opens_window_enter_password", ConstructorTemplates::LANGUAGE_CODE_RU)}}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table class="main" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="content-wrap">
                            <table  cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td style="background-color: #1ab394; text-align: center; padding: 21px 0 14px; color: white; text-transform: uppercase;">
                                        <h2 style="font-family: Arial, sans-serif; font-size: 30px;  margin: 0 0 7px; font-weight: bold; color: white;">{{$dictionaryManager->getTransByLngCode("swis.user.register.password_type.single_window", ConstructorTemplates::LANGUAGE_CODE_TJ)}}</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="" style="color: #606365; padding-bottom: 10px;">
                                        <h3 style="font-family: Arial, sans-serif; font-weight: bold; margin-bottom: 5px; font-size: 16px; color: #606365; margin-top: 30px;">{{$dictionaryManager->getTransByLngCode("swis.user.register.password_type.dear_user", ConstructorTemplates::LANGUAGE_CODE_TJ)}} {{$userName}}</h3>
                                        <p style="font-family: Arial, sans-serif; margin-bottom: 0; font-size: 14px;">{{$dictionaryManager->getTransByLngCode("swis.user.register.password_type.for_setting_password_click_link_below", ConstructorTemplates::LANGUAGE_CODE_TJ)}}</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="aligncenter" style="padding-bottom: 10px;">
                                        <h5><a style="color: #337ab7;" href="{{ url($lngCode.'/password/'.$token)}}">{{$dictionaryManager->getTransByLngCode("swis.user.register.password_type.set_password", ConstructorTemplates::LANGUAGE_CODE_TJ)}}</a></h5>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="" style="color: #606365; padding-bottom: 10px;">
                                        <p style="font-family: Arial, sans-serif; margin-bottom: 0; font-size: 14px;">{{$dictionaryManager->getTransByLngCode("swis.user.register.password_type.opens_window_enter_password", ConstructorTemplates::LANGUAGE_CODE_TJ)}}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div class="footer">
                    <table width="100%">
                        <tr>
                            <td style="padding: 20px;" class="aligncenter content-block">
                                <a href="{{urlWithLng('')}}">{{env('APP_URL')}}</a>
                                <p style="color: #5c5f61; font-size: 12px; margin-bottom: 2px;">
                                    &copy; {{date('Y')}} </p>
                                <p>
                                    <a style="font-family: Arial, sans-serif; color: #5c5f61; font-size: 12px;"
                                       href="mailto:support@swis.am">support@swis.tj</a>
                                </p>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </td>
        <td></td>
    </tr>
</table>

</body>
</html>

