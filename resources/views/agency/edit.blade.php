@extends('layouts.app')

@section('title'){{trans('swis.agency.create.title')}}@stop

@section('contentTitle')
    {{trans('swis.agency.create.title')}}
@stop

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('styles')
    <link rel="stylesheet" href="{{asset('assets/helix/core/plugins/dropzone/dropzone.css')}}">
@stop

@section('content')

    <?php
    switch ($saveMode) {
        case 'add':
            $url = 'agency';
            $workingHours['working_hours_start'] = config('swis.working_hours_start');
            $workingHours['working_hours_end'] = config('swis.working_hours_end');
            $workingHours['lunch_hours_start'] = config('swis.lunch_hours_start');
            $workingHours['lunch_hours_end'] = config('swis.lunch_hours_end');
            break;
        default:
            $url = $saveMode != 'view' ? 'agency/update/' . $agency->id : '';

            $mls = $agency->ml()->notDeleted()->base(['lng_id', 'name', 'short_name', 'description', 'show_status', 'address'])->keyBy('lng_id');
            break;
    }

    $languages = activeLanguages();
    ?>

    <form class="form-horizontal fill-up " id="form-data" action="{{urlWithLng($url)}}" autocomplete="off">

        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-general"> {{trans('core.base.tab.general')}}</a></li>
                @foreach($languages as $language)
                    <li><a data-toggle="tab"
                           href="#tab-{{$language->code}}">{{ trans("core.base.language.".strtolower($language->name)) }}</a>
                    </li>
                @endforeach
                <li class="sphere-tab" style="{{ isset($agency) && $agency->is_lab ? '' : 'display: none;' }}"><a
                            data-toggle="tab" href="#tab-sphere"> {{trans('swis.agency.sphere.tab')}}</a></li>
            </ul>
            <div class="tab-content">
                <div id="tab-general" class="tab-pane active">
                    <div class="panel-body">

                        <div class="form-group required"><label
                                    class="col-lg-2 control-label">{{trans('swis.tax_id')}}</label>
                            <div class="col-lg-10">
                                <input value="{{$agency->tax_id or ''}}"
                                       {{($saveMode == 'edit') ? 'readonly' : ''}} name="tax_id" id="taxID" type="text"
                                       placeholder=""
                                       class="form-control get-info-field">
                                <div class="form-error" id="form-error-tax_id"></div>
                            </div>
                        </div>

                        <div class="form-group"><label
                                    class="col-lg-2 control-label">{{trans('swis.agency_code')}}</label>
                            <div class="col-lg-10">
                                <input value="{{$agency->agency_code or ''}}" name="agency_code" type="text"
                                       placeholder="" class="form-control">
                                <div class="form-error" id="form-error-agency_code"></div>
                            </div>
                        </div>

                        <div class="form-group required"><label
                                    class="col-lg-2 control-label">{{trans('swis.broker.legal_entity_name')}}</label>
                            <div class="col-lg-10">
                                <input value="{{$agency->legal_entity_name or ''}}" id="legalEntityName" readonly
                                       name="legal_entity_name" type="text" placeholder=""
                                       class="form-control">
                                <div class="form-error" id="form-error-legal_entity_name"></div>
                            </div>
                        </div>

                        <div class="form-group"><label class="col-lg-2 control-label"
                                                       readonly="">{{trans('swis.broker.label.address')}}</label>
                            <div class="col-lg-10">
                                <input value="{{$agency->address or ''}}" readonly name="address" type="text"
                                       placeholder="" class="form-control" id="address">
                                <div class="form-error" id="form-error-address"></div>
                            </div>
                        </div>

                        <div class="form-group row {{config('global.phone_number.required') ? 'required' : ''}}">
                            <label class="col-lg-2 control-label">{{trans('core.base.label.phone')}}</label>
                            <div class="col-lg-10">
                                <div class="input-group">
                                    <span class="input-group-addon">+</span>
                                    <input type="text"
                                           class="form-control onlyNumbers"
                                           name="phone_number"
                                           value="{{$agency->phone_number or ''}}"
                                           autocomplete="off"
                                           required>
                                </div>
                                <div class="form-error" id="form-error-phone_number"></div>
                            </div>
                        </div>

                        <div class="form-group required"><label
                                    class="col-lg-2 control-label">{{trans('core.base.label.email')}}</label>
                            <div class="col-lg-10">
                                <input value="{{$agency->email or ''}}" name="email" type="text" placeholder=""
                                       class="form-control">
                                <div class="form-error" id="form-error-email"></div>
                            </div>
                        </div>

                        <div class="form-group"><label
                                    class="col-lg-2 control-label">{{trans('core.base.label.url')}}</label>
                            <div class="col-lg-10">
                                <input value="{{$agency->website_url or ''}}" name="website_url" type="text"
                                       placeholder="" class="form-control">
                                <div class="form-error" id="form-error-website_url"></div>
                            </div>
                        </div>

                        <div class="form-group"><label
                                    class="col-lg-2 control-label">{{trans('swis.base.logo')}}</label>
                            <div class="col-lg-10">
                                @if(!empty($agency->logo))
                                    <div class="form-group current-logo">
                                        <div class='col-lg-2'>
                                            <button type="button" class="btn btn-xs pull-right btn-danger"
                                                    data-toggle="modal" data-target="#delete-logo"
                                                    style="position: absolute;right: -10px" href="#"><i
                                                        class="fa fa-times"></i></button>
                                            <img width="100%" src="/images/agency/{{$agency->logo}}">
                                            <hr/>
                                        </div>
                                    </div>
                                @endif
                                <div class="g-upload-container">
                                    <div class="register-form__list">
                                        <div class="register-form__item">
                                            <div class="g-upload-box">
                                                <label class="btn btn-primary btn-upload">
                                                    <i class="fa fa-upload"></i>
                                                    {{trans('swis.agency.upload.button')}}
                                                    <input class="dn g-upload" type="file" name="logo"
                                                           value="{{$agency->logo or ''}}"
                                                           data-conf="agency"
                                                           data-action="{{urlWithLng('/g-uploader/upload')}}"
                                                           accept="image/png"
                                                    />
                                                </label>
                                            </div>
                                            <div class="license-form__uploaded-file-list g-uploaded-list">
                                                <input type="hidden" class="g-uploaded-file-name" name="logo"
                                                       value="{{$agency->logo or ''}}">
                                            </div>
                                            <progress class="progressBarFileUploader" value="0"
                                                      max="100"></progress>
                                            <div class="form-error form-error-text form-error-files fb fs12"
                                                 id="form-error-logo"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group required"><label
                                    class="col-lg-2 control-label">{{trans('swis.agency.label.subdivision_mark_type')}}</label>
                            <div class="col-lg-10">
                                <label class="radio-inline">
                                    <input type="radio" name="subdivision_mark_type"
                                           value="{{App\Models\Agency\Agency::SUBDIVISION_MARK_TYPE_REQUIRED}}" {{((isset($agency) && $agency->subdivision_mark_type == App\Models\Agency\Agency::SUBDIVISION_MARK_TYPE_REQUIRED) ? 'checked' : '' )}} /> {{ trans('swis.agency.label.required') }}
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="subdivision_mark_type"
                                           value="{{App\Models\Agency\Agency::SUBDIVISION_MARK_TYPE_NOT_REQUIRED}}" {{((isset($agency) && $agency->subdivision_mark_type == App\Models\Agency\Agency::SUBDIVISION_MARK_TYPE_NOT_REQUIRED) ? 'checked' : '' )}}/> {{ trans('swis.agency.label.not_required') }}
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="subdivision_mark_type"
                                           value="{{App\Models\Agency\Agency::SUBDIVISION_MARK_TYPE_OPTIONAL}}" {{((isset($agency) && $agency->subdivision_mark_type == App\Models\Agency\Agency::SUBDIVISION_MARK_TYPE_OPTIONAL) ? 'checked' : '' )}}/> {{ trans('swis.agency.label.optional') }}
                                </label>
                                <div class="form-error" id="form-error-subdivision_mark_type"></div>
                            </div>
                        </div>

                        {{--<div class="form-group"><label
                                    class="col-lg-2 control-label label__title pd-t-0">{{ trans('swis.agency.allow_send_sub_applications_other_agency.label') }}</label>
                            <div class="col-lg-10">
                                <input value="1"
                                       {{isset($agency) && $agency->allow_send_sub_applications_other_agency ? 'checked' :'' }} name="allow_send_sub_applications_other_agency"
                                       type="checkbox" placeholder="">
                                <div class="form-error" id="form-error-allow_send_sub_applications_other_agency"></div>
                            </div>
                        </div>--}}

                        <div class="form-group">
                            <label class="col-lg-2 control-label pd-t-0 label__title">{{ trans('swis.agency.is_lab_label') }}</label>
                            <div class="col-lg-10">
                                <input value="1" id="isLabCheckbox"
                                       {{isset($agency) && $agency->is_lab ? 'checked' :'' }} name="is_lab"
                                       type="checkbox">
                                <div class="form-error" id="form-error-is_lab"></div>
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-lg-2 control-label pd-t-0 label__title">{{ trans('swis.agency.working_hours') }}</label>
                            <div class="col-lg-2">
                                <select class="select2" name="working_hours_start">
                                    @if($hoursInterval->count())
                                        @foreach($hoursInterval as $hourInterval)
                                            <option value="{{$hourInterval->hours}}"
                                                    @if($workingHours['working_hours_start'] == $hourInterval->hours) selected="selected" @endif>{{$hourInterval->hours}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <div class="form-error" id="form-error-working_hours_start"></div>
                            </div>
                            <div class="col-lg-2">
                                <select class="select2" name="working_hours_end">
                                    @if($hoursInterval->count())
                                        @foreach($hoursInterval as $hourInterval)
                                            <option value="{{$hourInterval->hours}}"
                                                    @if($workingHours['working_hours_end'] == $hourInterval->hours) selected="selected" @endif>{{$hourInterval->hours}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <div class="form-error" id="form-error-working_hours_end"></div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-lg-2 control-label pd-t-0 label__title">{{ trans('swis.agency.lunch_hours') }}</label>
                            <div class="col-lg-2">
                                <select class="select2" name="lunch_hours_start">
                                    @if($hoursInterval->count())
                                        @foreach($hoursInterval as $hourInterval)
                                            <option value="{{$hourInterval->hours}}"
                                                    @if($workingHours['lunch_hours_start'] == $hourInterval->hours) selected="selected" @endif>{{$hourInterval->hours}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <div class="form-error" id="form-error-lunch_hours_start"></div>
                            </div>
                            <div class="col-lg-2">
                                <select class="select2" name="lunch_hours_end">
                                    @if($hoursInterval->count())
                                        @foreach($hoursInterval as $hourInterval)
                                            <option value="{{$hourInterval->hours}}"
                                                    @if($workingHours['lunch_hours_end'] == $hourInterval->hours) selected="selected" @endif>{{$hourInterval->hours}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <div class="form-error" id="form-error-lunch_hours_end"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">{{trans('core.base.label.status')}}</label>
                            <div class="col-lg-10 general-status">
                                <select name="show_status" class="form-show-status">
                                    <option value="{{App\Models\Agency\Agency::STATUS_ACTIVE}}"{{(isset($agency) && $agency->show_status == App\Models\Agency\Agency::STATUS_ACTIVE) || $saveMode == 'add' ?  ' selected="selected"' : ''}}>{{trans('core.base.label.status.active')}}</option>
                                    <option value="{{App\Models\Agency\Agency::STATUS_INACTIVE}}"{{isset($agency) &&  $agency->show_status == App\Models\Agency\Agency::STATUS_INACTIVE ? ' selected="selected"': ''}}>{{trans('core.base.label.status.inactive')}}</option>
                                </select>
                                <div class="form-error" id="form-error-show_status"></div>
                            </div>
                        </div>

                    </div>
                </div>
                @foreach($languages as $language)
                    <?php $lngId = $language->id ?>
                    <div id="tab-{{$language->code}}" class="tab-pane">
                        <div class="panel-body">
                            <div class="form-group required"><label
                                        class="col-lg-2 control-label">{{trans('core.base.label.name')}}</label>
                                <div class="col-lg-10">
                                    <input value="{{isset($mls[$lngId]) ? $mls[$lngId]->getOriginal('name') : ''}}"
                                           name="ml[{{$language->id}}][name]" type="text" placeholder=""
                                           class="form-control">
                                    <div class="form-error" id="form-error-ml-{{$language->id}}-name"></div>
                                </div>
                            </div>

                            <div class="form-group"><label
                                        class="col-lg-2 control-label">{{trans('swis.agency.ml.label.short_name')}}</label>
                                <div class="col-lg-10">
                                    <input value="{{isset($mls[$lngId]) ? $mls[$lngId]->short_name : ''}}"
                                           name="ml[{{$language->id}}][short_name]" type="text" placeholder=""
                                           class="form-control">
                                    <div class="form-error" id="form-error-ml-{{$language->id}}-short_name"></div>
                                </div>
                            </div>

                            <div class="form-group"><label
                                        class="col-lg-2 control-label">{{trans('swis.agency.ml.address.label')}}</label>
                                <div class="col-lg-10">
                                    <input value="{{isset($mls[$lngId]) ? $mls[$lngId]->address : ''}}"
                                           name="ml[{{$language->id}}][address]" type="text" placeholder=""
                                           class="form-control">
                                    <div class="form-error" id="form-error-ml-{{$language->id}}-address"></div>
                                </div>
                            </div>

                            <div class="form-group"><label
                                        class="col-lg-2 control-label">{{trans('core.base.description')}}</label>
                                <div class="col-lg-10">
                                    <textarea rows="6" name="ml[{{$language->id}}][description]"
                                              class="form-control">{{isset($mls[$lngId]) ? $mls[$lngId]->description : ''}}</textarea>
                                    <div class="form-error" id="form-error-ml-{{$language->id}}-description"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

                <div id="tab-sphere" class="tab-pane">
                    <div class="panel-body">
                        <div class="form-group required"><label
                                    class="col-lg-2 control-label">{{trans('swis.laboratory_code')}}</label>
                            <div class="col-lg-10">
                                <input value="{{$agency->laboratory_code or ''}}" name="laboratory_code" type="text"
                                       placeholder="" class="form-control">
                                <div class="form-error" id="form-error-laboratory_code"></div>
                            </div>
                        </div>
                        <div class="form-group required"><label
                                    class="col-lg-2 control-label">{{trans('swis.laboratory_certification_number')}}</label>
                            <div class="col-lg-10">
                                <input value="{{$agency->certificate_number or ''}}" name="certificate_number"
                                       type="text"
                                       placeholder="" class="form-control">
                                <div class="form-error" id="form-error-certificate_number"></div>
                            </div>
                        </div>
                        <br/>
                        <div class="form-group required"><label
                                    class="col-lg-2 control-label">{{trans('swis.laboratory_certification_end_date')}}</label>
                            <div class="col-lg-10">
                                <div class='input-group datetime'>
                                             <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                             </span>
                                    <input type="text" placeholder="{{setDatePlaceholder(true)}}"
                                           value="{{ isset($agency->certification_period_validity) ? \Carbon\Carbon::parse($agency->certification_period_validity)->format('d-m-Y H:i') : ''}}"
                                           name="certification_period_validity" class="form-control">
                                </div>

                                <div class="form-error" id="form-error-certification_period_validity"></div>
                            </div>
                        </div>
                        <div class="collapse in" id="laboratoryTable">
                            <div class="row">
                                <button type="button"
                                        class="btn btn-primary visible-buttons pull-right addNewIndicators">{{ trans('swis.agency.new_data_expertise') }}</button>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="table-responsive mainIndicatorsTable">
                                    @include('agency.indicators-table')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" value="{{$agency->id or 0}}" name="id" id="agencyIdField" class="mc-form-key"/>

        {!! csrf_field() !!}
    </form>

    <div class="modal fade" id="addIndicatorsModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal__title fb">{{trans('swis.agency.new_data_expertise.modal.title')}}</h4>
                    <button type="button" class="close modal__close" data-dismiss="modal" aria-hidden="true">&times;
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" id="searchSphereForm">

                        <div class="row">
                            <div class="col-md-4">
                                <label>{{ trans('swis.agency.new_data_expertise.modal.sphere.label') }}</label>

                                <select class="select2" name="sphere">
                                    <option value="">{{trans('core.base.label.select')}}</option>
                                    @foreach($spheres as $sphere)
                                        <option value="{{$sphere->code}}">{{ "({$sphere->code}) {$sphere->name}"}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-4">
                                <label>{{ trans('swis.agency.new_data_expertise.modal.hs_start.label') }}</label>
                                <input type="number" max="10" class="form-control" name="hs_code">
                            </div>

                            <div class="col-md-4">
                                <label>{{ trans('swis.agency.new_data_expertise.modal.indicator.label') }}</label>
                                <input type="text" class="form-control" name="indicator">
                            </div>
                        </div>

                        <br/>

                        <button class="btn btn-primary visible-buttons pull-right searchExpertise">{{ trans('swis.agency.new_data_expertise.modal.search.button') }}</button>

                        <br/>
                        <hr/>

                        <div class="row">
                            <div class="table-responsive">
                                <div class="indicatorsTableBlock">

                                </div>
                            </div>
                        </div>

                    </form>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-default btn-refresh-cancel" data-dismiss="modal">{{trans('core.base.button.refresh.close')}}</button>
                    <button class="btn btn-success addIndicators" disabled>{{trans('swis.agency.new_data_expertise.modal.save.button')}}</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script src="{{asset('js/guploader.js')}}"></script>
    <script src="{{asset('assets/helix/core/plugins/dropzone/dropzone.min.js')}}"></script>
    <script src="{{asset('js/agency/main.js')}}"></script>
@endsection