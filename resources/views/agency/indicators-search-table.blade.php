<table class="table table-bordered">

    <thead>
        <td></td>
        <td>{{trans("swis.agency.new_data_expertise.modal.table.sphere")}}</td>
        <td>{{trans("swis.agency.new_data_expertise.modal.table.hs_code")}}</td>
        <td>{{trans("swis.agency.new_data_expertise.modal.table.indicator")}}</td>
{{--        <td>{{trans("swis.agency.new_data_expertise.modal.table.price")}}</td>--}}
{{--        <td>{{trans("swis.agency.new_data_expertise.modal.table.duration")}}</td>--}}
    </thead>

    <tbody>
        @forelse($data as $item)
            <tr>
                <td>
                    <input type="checkbox" data-id="{{ $item->id }}" class="lab_indicator_checkbox" value="1" />
                </td>
                <td>{{ $item->sphere }}</td>
                <td>{{ $item->hs_code }}</td>
                <td>{{ "({$item->indicator_code}) $item->indicator" }}</td>
{{--                <td>{{ $item->price }}</td>--}}
{{--                <td>{{ $item->duration }}</td>--}}
            </tr>
        @empty
            <tr>
                <td colspan="4" align="center">{{ trans("swis.agency.new_data_expertise.modal.table.not_found") }}</td>
            </tr>
        @endforelse

    </tbody>

</table>
