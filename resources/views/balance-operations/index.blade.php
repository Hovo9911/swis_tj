@extends('layouts.app')

@section('title'){{trans('swis.balance_operations')}}@stop

@section('contentTitle') {{trans('swis.balance_operations')}} @stop

@section('content')
    <?php
        $jsTrans->addTrans([
            'swis.balance_operations.refund.button',
        ]);
    ?>

    @php($companyTaxId = Auth::user()->companyTaxId())
    <div class="tabs-container">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tab-balance"> {{trans('swis.balance_operations.tab.balance')}}</a></li>
            <li class=""><a data-toggle="tab" href="#tab-operations"> {{trans('swis.balance_operations.tab.operations')}}</a></li>
            <li class=""><a data-toggle="tab" href="#tab-pays"> {{trans('swis.monitoring.tab.pays')}}</a></li>
        </ul>

        <div class="tab-content">
            <div id="tab-balance" class="tab-pane active">

                <div class="clearfix search__out">
                    <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse" data-target="#collapseSearch" aria-expanded="false" aria-controls="collapseSearch">
                        <i class="fa fa-search"> </i><span class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
                    </button>
                </div>

                <div class="collapse" id="collapseSearch">
                    <div class="card card-body">
                        <form class="form-horizontal fill-up" id="search-filter" autocomplete="off">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group searchParam id_block_params">
                                        <label class="col-md-4 control-label label__title">ID</label>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-12 field__one-col">
                                                    <input type="number" class="form-control onlyNumbers" min="0" name="balance_search_id" id="" value="">
                                                    <div class="form-error" id="form-error-balance_search_id"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group searchParam id_block_params">
                                        <label class="col-md-4 control-label label__title">{{ trans('swis.balance_operations.balance.ssn.label') }}</label>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-12 field__one-col">
                                                    <input type="text" class="form-control" name="balance_search_ssn" value="">
                                                    <div class="form-error" id="form-error-balance_search_ssn"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group searchParam menu_block_params">
                                        <label class="col-md-4 control-label label__title">{{trans('swis.balance_operations.balance.company.label')}}</label>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-12 field__one-col">
                                                    <select class="select2" name="balance_search_company">
                                                        <option></option>
                                                        @foreach($companies as $company)
                                                            <option value="{{$company->id}}">{{$company->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="form-error" id="form-error-balance_search_company"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group searchParam id_block_params">
                                        <label class="col-md-4 control-label label__title">{{ trans('swis.balance_operations.balance.balance.label') }}</label>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-6 field__one-col">
                                                    <input type="text" class="form-control" name="balance_min" value="">
                                                    <div class="form-error" id="form-error-balance_min"></div>
                                                </div>
                                                <div class="col-md-6 field__one-col">
                                                    <input type="text" class="form-control" name="balance_max" value="">
                                                    <div class="form-error" id="form-error-balance_max"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-8 col-lg-offset-3 text-right">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <button type="submit" class="btn btn-primary mr-10">{{trans('core.base.label.search')}}</button>
                                            <button type="button" class="btn btn-danger mt-10 resetButton" data-submit="true">{{trans('core.base.label.reset')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

                <div class="data-table">
                    <div class="clearfix">
                        <table class="table table-striped table-bordered table-hover dataTables-example" id="balance-data">
                            <thead>
                                <tr>
                                    <th data-col="id">ID</th>
                                    <th data-col="company_name">{{trans('swis.balance_operations.balance.company_name')}}</th>
                                    <th data-col="ssn">{{trans('swis.balance_operations.balance.ssn')}}</th>
                                    <th data-col="balance" data-ordering="false">{{trans('swis.balance_operations.balance.balance')}}</th>
                                    <th data-col="sum" data-ordering="false">{{trans('swis.balance_operations.balance.sum')}}</th>
                                    <th data-col="action" data-ordering="false">{{trans('swis.balance_operations.balance.action')}}</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>

            </div>

            <div id="tab-operations" class="tab-pane">

               <div class="clearfix search__out">
                    <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse" data-target="#collapseSearchTransactions" aria-expanded="false" aria-controls="collapseSearchTransactions">
                        <i class="fa fa-search"> </i><span class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
                    </button>
                </div>

                <div class="collapse" id="collapseSearchTransactions">
                    <div class="card card-body">
                        <form class="form-horizontal fill-up" id="search-transactions-filter" autocomplete="off">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group searchParam id_block_params">
                                        <label class="col-md-4 control-label label__title">ID</label>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-12 field__one-col">
                                                    <input type="number" class="form-control onlyNumbers" min="0" name="transactions_search_id" id="" value="">
                                                    <div class="form-error" id="form-error-transactions_search_id"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group searchParam id_block_params">
                                        <label class="col-md-4 control-label label__title">{{ trans('swis.balance_operations.transactions.ssn.label') }}</label>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-12 field__one-col">
                                                    <input type="text" class="form-control" name="ssn" value="">
                                                    <div class="form-error" id="form-error-ssn"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group searchParam row">
                                        <label class="col-md-4 control-label label__title">{{ trans('swis.balance_operations.transactions.date.label') }}</label>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-6 field__two-col">
                                                    <div class='input-group date'>
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                        <input value="" name="transaction_date_start"
                                                               type="text"
                                                               placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                                    </div>
                                                </div>
                                                <span style=" position: absolute; top: 6px;margin-left: -5px;">-</span>
                                                <div class="col-md-6 field__two-col">
                                                    <div class='input-group date'>
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                        <input value=""  name="transaction_date_end"
                                                               type="text"
                                                               placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group searchParam menu_block_params">
                                        <label class="col-md-4 control-label label__title">{{trans('swis.balance_operations.transactions.company.label')}}</label>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-12 field__one-col">
                                                    <select class="select2" name="company">
                                                        <option></option>
                                                        @foreach($companies as $company)
                                                            <option value="{{$company->id}}">{{$company->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="form-error" id="form-error-company"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group searchParam id_block_params">
                                        <label class="col-md-4 control-label label__title">{{ trans('swis.balance_operations.transactions.sum.label') }}</label>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-6 field__one-col">
                                                    <input type="number" min="0" class="form-control" name="sum_min" value="">
                                                    <div class="form-error" id="form-error-sum_min"></div>
                                                </div>
                                                <div class="col-md-6 field__one-col">
                                                    <input type="number" min="0" class="form-control" name="sum_max" value="">
                                                    <div class="form-error" id="form-error-sum_max"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group searchParam id_block_params">
                                        <label class="col-md-4 control-label label__title">{{ trans('swis.balance_operations.transactions.username.label') }}</label>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-12 field__one-col">
                                                    <input type="text" class="form-control" name="username" value="">
                                                    <div class="form-error" id="form-error-username"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-8 col-lg-offset-3 text-right">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <button type="submit" class="btn btn-primary mr-10" style="margin-top: 10px;">{{trans('core.base.label.search')}}</button>
                                            <button type="button" class="btn btn-danger mt-10 resetButton" data-submit="true">{{trans('core.base.label.reset')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

                <div class="data-table">
                    <div class="clearfix">
                        <table class="table table-striped table-bordered table-hover dataTables-example" id="transactions-data">
                            <thead>
                                <tr>
                                    <th data-col="id">ID</th>
                                    <th data-col="company_name">{{trans('swis.balance_operations.transactions.company_name')}}</th>
                                    <th data-col="ssn">{{trans('swis.balance_operations.transactions.ssn')}}</th>
                                    <th data-col="sum">{{trans('swis.balance_operations.transactions.sum')}}</th>
                                    <th data-col="payment_date">{{trans('swis.balance_operations.transactions.date')}}</th>
                                    <th data-col="username">{{trans('swis.balance_operations.transactions.username')}}</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>

            </div>

            <div id="tab-pays" class="tab-pane">

                <div class="clearfix search__out">
                    <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse" data-target="#collapsePaysSearch" aria-expanded="false" aria-controls="collapseSearch">
                        <i class="fa fa-search"> </i><span class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
                    </button>
                </div>

                <div class="collapse" id="collapsePaysSearch">
                    <div class="card card-body">
                        <form class="form-horizontal fill-up" id="search-filter-pays">

                            <div class="form-group searchParam id_block_params" data-field="id_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">ID</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input type="number" min="0" class="form-control onlyNumbers" name="id" id="" value="">
                                            <div class="form-error" id="form-error-id"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam user_name_block_params" data-field="user_name_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.pays.user_name.title')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input type="text" class="form-control" name="user_name" id="" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam user_ssn_block_params" data-field="user_ssn_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.pays.user_ssn.title')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input type="text" class="form-control" name="user_ssn" id="" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam transaction_id_block_params" data-field="transaction_id_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.pays.transaction_id.title')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input type="number" min="0" class="form-control onlyNumbers" name="transaction_id" id="" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam updated_at_block_params" data-field="updated_at_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.pays.transaction_date.label')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-4 col-lg-5 field__two-col">
                                            <div class='input-group date'>
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                <input value="" name="payment_date_date_start" autocomplete="off"
                                                       type="text"
                                                       placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-5 field__two-col">
                                            <div class='input-group date'>
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                <input value="" name="payment_date_date_end" autocomplete="off"
                                                       type="text"
                                                       placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam company_name_block_params" data-field="company_name_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.pays.company_name.title')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input type="text" class="form-control" name="company_name" id="" value="">
                                            <div class="form-error" id="form-error-company_name"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam company_name_block_params" data-field="company_name_block_params">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.pays.provider.title')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <select class="select2" name="payment_provider_id">
                                                <option></option>
                                                @foreach($paymentProviders as $paymentProvider)
                                                    <option value="{{ $paymentProvider->id }}">{{ $paymentProvider->name }}</option>
                                                @endforeach
                                            </select>
                                            <div class="form-error" id="form-error-payment_provider_id"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-8 col-lg-offset-3 text-right">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8">
                                            <button type="submit" class="btn btn-primary mr-10">{{trans('core.base.label.search')}}</button>
                                            <button type="button" class="btn btn-danger resetButton" style="margin-top: 0px;" data-submit="true">{{trans('core.base.label.reset')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <?php
                $customColumns = json_encode([
                    "id" => "ID",
                    "user_name" => trans('swis.pays.username.title'),
                    "user_ssn" => trans('swis.pays.user_ssn.title'),
                    "transaction_id" => trans('swis.pays.transaction_id.title'),
                    "payment_date" => trans('swis.pays.transaction_date.title'),
                    "amount" => trans('swis.pays.amount.title'),
                    "provider" => trans('swis.pays.provider.title'),
                    "company_name" => trans('swis.pays.company_name.title')
                ]);
                ?>
                <div class="data-table">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example"
                                   id="pays-data"
                                   data-form-id="search-filter-pays"
                                   data-custom-columns="{{ $customColumns }}"
                                   data-module-for-search-params="Monitoring{{ hash('sha256', $companyTaxId.'pays') }}">
                                <thead>
                                <tr>
                                    <th data-col="id">ID</th>
                                    <th data-col="user_name">{{ trans('swis.pays.username.title') }}</th>
                                    <th data-col="user_ssn">{{ trans('swis.pays.user_ssn.title') }}</th>
                                    <th data-col="transaction_id">{{ trans('swis.pays.transaction_id.title') }}</th>
                                    <th data-col="payment_date">{{ trans('swis.pays.transaction_date.title') }}</th>
                                    <th data-col="amount">{{ trans('swis.pays.amount.title') }}</th>
                                    <th data-col="provider">{{ trans('swis.pays.provider.title') }}</th>
                                    <th data-col="company_name">{{ trans('swis.pays.company_name.title') }}</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="refundModal" tabindex="-1" role="dialog" aria-hidden="true"></div>
@endsection

@section('scripts')
    <script src="{{asset('js/balance-operations/main.js')}}"></script>
@endsection