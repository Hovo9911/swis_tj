<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal__title fb">{{trans('swis.balance_operations.refund.modal.title')}}</h4>
            <button type="button" class="close modal__close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body">
            <form action="{{ urlWithLng('/balance-operations/confirm-refund') }}" id="searchSphereForm">
                <div class="row">
                    @if ($success)
                        <input type="hidden" value="{{ $refund }}" id="refundHiddenData"/>
                        <input type="hidden" value="{{ $company->id }}" id="idHiddenData"/>
                        <p class="text-center">{{ trans('swis.balance_operations.refund.modal.success', ['old_balance' => (string)$oldBalance, 'new_balance' => (string)$newBalance]) }}</p>
                    @else
                        <p class="text-center">{{ trans('swis.balance_operations.refund.modal.fail') }}</p>
                    @endif
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button class="btn btn-default btn-refresh-cancel" data-dismiss="modal">{{trans('core.base.button.refresh.close')}}</button>
            @if ($success)
                <button class="btn btn-danger refundConfirm">{{trans('swis.balance_operations.refund.modal.save.button')}}</button>
            @endif
        </div>
    </div>
</div>