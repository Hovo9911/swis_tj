<table border="1">
    <tbody>
    <tr>
        <?php
        unset($products['columns']['module']);
        unset($products['columns']['block_type']);
        ?>
        @foreach ($products['columns'] as $column)
            <td style="text-align: center;font-weight: bold; min-height: 50px;">{{ $column['label'] }}</td>
        @endforeach
    </tr>

    @foreach ($products['data'] as $product)
        <tr>
            @foreach ($products['columns'] as $field => $column)
                <td style="text-align: center;">{{ $product[$field] ?? '' }}</td>
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>