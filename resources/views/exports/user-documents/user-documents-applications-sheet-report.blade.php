<table border="1">
    <tbody>
    <tr>
        <td colspan="{{ count($applications['columns']) }}"
            style="text-align: center;font-weight: bold; height: 40px;">{{ trans('swis.user-documents.report.key1') }}</td>
    </tr>
    <tr>
        <td colspan="{{ count($applications['columns']) }}"
            style="text-align: center;font-weight: bold; height: 40px;">@if (isset($searchParams) && count($searchParams) > 0){{ trans('swis.user-documents.report.key2') }}@endif</td>
    </tr>
    <tr>
        <td colspan="{{ count($applications['columns']) }}"
            style="text-align: center;font-weight: bold; min-height: 50px;">
            @foreach ($searchParams as $key => $item)
                <p>{{ $item['label'] }} = {{ $item['value'] }}, </p>
            @endforeach
        </td>
    </tr>
    <tr>
        @foreach ($applications['columns'] as $column)
            <td style="text-align: center; min-height: 50px;font-weight: bold;">{{ $column }}</td>
        @endforeach
    </tr>

    @foreach ($applications['data'] as $application)
        <tr>
            @foreach ($applications['columns'] as $field => $column)
                <td style="text-align: center;">{{ $application[$field] ??''}}</td>
            @endforeach
        </tr>
    @endforeach

    <tr>
        <td colspan="3" style="font-weight: bold;">{{ trans('swis.swis.user-documents.report.key3') }}</td>
        <td colspan="1" style="font-weight: bold;">{{ Carbon\Carbon::now() }}</td>
        <td colspan="2" style="font-weight: bold;">{{ Auth::user()->name() }}</td>
    </tr>
    </tbody>
</table>