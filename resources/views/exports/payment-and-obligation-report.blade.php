<table border="1">
    <tbody>
        <tr><td colspan="17" style="text-align: center; height: 40px;">{{ trans('swis.payment-and-obligation.report.key1') }}</td></tr>
        <tr><td colspan="17" style="text-align: center; height: 40px;">@if (isset($searchParams) && count($searchParams) > 0){{ trans('swis.payment-and-obligation.report.key2') }}@endif</td></tr>
        <tr>
            <td colspan="17" style="text-align: center; min-height: 50px;">
                @foreach ($searchParams as $key => $item)
                    <p>{{ $fieldTrans[$key] }} = {{ $item }}, </p>
                @endforeach
            </td>
        </tr>
        <tr>
            <td>N</td>
            <td style="width: 10px; height: 40px; text-align: center; font-size: 11px;"><b>{{ trans('swis.payment-and-obligations.obligation_id') }}</b></td>
            <td style="width: 15px; height: 40px; text-align: center; font-size: 11px;"><b>{{ trans('swis.payment-and-obligations.applicant_tin') }}</b></td>
            <td style="width: 30px; height: 40px; text-align: center; font-size: 11px;"><b>{{ trans('swis.payment-and-obligations.tin_importer_exporter') }}</b></td>
            <td style="width: 15px; height: 40px; text-align: center; font-size: 11px;"><b>{{ trans('swis.payment-and-obligations.saf_number') }}</b></td>
            <td style="width: 15px; height: 40px; text-align: center; font-size: 11px;"><b>{{ trans('swis.payment-and-obligations.sub_application_number') }}</b></td>
            <td style="width: 15px; height: 40px; text-align: center; font-size: 11px;"><b>{{ trans('swis.payment-and-obligations.agency') }}</b></td>
            <td style="width: 30px; height: 40px; text-align: center; font-size: 11px;"><b>{{ trans('swis.payment-and-obligations.document') }}</b></td>
            <td style="width: 20px; height: 40px; text-align: center; font-size: 11px;"><b>{{ trans('swis.payment-and-obligations.obligation_type') }}</b></td>
            <td style="width: 20px; height: 40px; text-align: center; font-size: 11px;"><b>{{ trans('swis.payment-and-obligations.budget_line') }}</b></td>
            <td style="width: 20px; height: 40px; text-align: center; font-size: 11px;"><b>{{ trans('swis.payment-and-obligations.obligation_created_date') }}</b></td>
            <td style="width: 20px; height: 40px; text-align: center; font-size: 11px;"><b>{{ trans('swis.payment-and-obligations.obligation_pay_date') }}</b></td>
            <td style="width: 20px; height: 40px; text-align: center; font-size: 11px;"><b>{{ trans('swis.single_app.payer') }}</b></td>
            <td style="width: 20px; height: 40px; text-align: center; font-size: 11px;"><b>{{ trans('swis.payment-and-obligations.payer_person') }}</b></td>
            <td style="width: 20px; height: 40px; text-align: center; font-size: 11px;"><b>{{ trans('swis.payment-and-obligations.obligation') }}</b></td>
            <td style="width: 20px; height: 40px; text-align: center; font-size: 11px;"><b>{{ trans('swis.payment-and-obligations.payment') }}</b></td>
            <td style="width: 20px; height: 40px; text-align: center; font-size: 11px;"><b>{{ trans('swis.payment-and-obligations.balance') }}</b></td>
        </tr>

        @php($total = array_pop($data))
        @foreach($data as $item)
            <tr>
                <td style="height: 60px; text-align: center; word-wrap: break-word; font-size: 8px;">{{ $item['n'] }}</td>
                <td style="height: 60px; text-align: center; word-wrap: break-word; font-size: 8px;">{{ $item['id'] }}</td>
                <td style="height: 60px; text-align: center; word-wrap: break-word; font-size: 8px;">{{ $item['applicant_tin'] }}</td>
                <td style="height: 60px; text-align: center; word-wrap: break-word; font-size: 8px;">{{ $item['importer_exporter_tin'] }}</td>
                <td style="height: 60px; text-align: center; word-wrap: break-word; font-size: 8px;">{{ $item['saf_number'] }}</td>
                <td style="height: 60px; text-align: center; word-wrap: break-word; font-size: 8px;">{{ $item['sub_application_number'] }}</td>
                <td style="height: 60px; text-align: center; word-wrap: break-word; font-size: 8px;">{{ $item['agency'] }}</td>
                <td style="height: 60px; text-align: center; word-wrap: break-word; font-size: 8px;">{{ $item['document'] }}</td>
                <td style="height: 60px; text-align: center; word-wrap: break-word; font-size: 8px;">{{ $item['obligation_type'] }}</td>
                <td style="height: 60px; text-align: center; word-wrap: break-word; font-size: 8px;">{{ (string)$item['budget_line'] }}</td>
                <td style="height: 60px; text-align: center; word-wrap: break-word; font-size: 8px;">{{ $item['obligation_created_date'] }}</td>
                <td style="height: 60px; text-align: center; word-wrap: break-word; font-size: 8px;">{{ $item['obligation_pay_date'] }}</td>
                <td style="height: 60px; text-align: center; word-wrap: break-word; font-size: 8px;">{{ $item['obligation_payer'] }}</td>
                <td style="height: 60px; text-align: center; word-wrap: break-word; font-size: 8px;">{{ $item['obligation_payer_person'] }}</td>
                <td style="height: 60px; text-align: center; word-wrap: break-word; font-size: 8px;">{{ (string)$item['obligation'] }}</td>
                <td style="height: 60px; text-align: center; word-wrap: break-word; font-size: 8px;">{{ (string)$item['payment'] }}</td>
                <td style="height: 60px; text-align: center; word-wrap: break-word; font-size: 8px;">{{ (string)$item['balance'] }}</td>
            </tr>
        @endforeach

        <tr>
            <td colspan="14"></td>
            <td>{{ (string)$total['obligation'] }}</td>
            <td>{{ (string)$total['payment'] }}</td>
            <td>{{ (string)$total['balance'] }}</td>
        </tr>
        <tr>
            <td colspan="3">{{ trans('swis.payment-and-obligation.report.key3') }}</td>
            <td colspan="1">{{ Carbon\Carbon::now() }}</td>
            <td colspan="2">{{ Auth::user()->name() }}</td>
        </tr>
    </tbody>
</table>