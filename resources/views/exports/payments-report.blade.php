<table border="1">
    <tbody>
    <tr><td colspan="11" style="text-align: center; height: 40px;">{{ trans('swis.payments.report.key') }}</td></tr>
    <tr>
        <td style="width: 15px; height: 40px; text-align: center; font-size: 11px;"><b>{{ trans('swis.payments.payment_id.title') }}</b></td>
        <td style="width: 15px; height: 40px; text-align: center; font-size: 11px;"><b>{{ trans('swis.payments.service_provider_name.title') }}</b></td>
        <td style="width: 15px; height: 40px; text-align: center; font-size: 11px;"><b>{{ trans('swis.payments.saf_number.title') }}</b></td>
        <td style="width: 15px; height: 40px; text-align: center; font-size: 11px;"><b>{{ trans('swis.payments.sub_application_number.title') }}</b></td>
        <td style="width: 15px; height: 40px; text-align: center; font-size: 11px;"><b>{{ trans('swis.payments.agency.title') }}</b></td>
        <td style="width: 15px; height: 40px; text-align: center; font-size: 11px;"><b>{{ trans('swis.payments.document.title') }}</b></td>
        <td style="width: 15px; height: 40px; text-align: center; font-size: 11px;"><b>{{ trans('swis.payments.obligation_type.title') }}</b></td>
        <td style="width: 15px; height: 40px; text-align: center; font-size: 11px;"><b>{{ trans('swis.payments.username.title') }}</b></td>
        <td style="width: 15px; height: 40px; text-align: center; font-size: 11px;"><b>{{ trans('swis.payments.amount.title') }}</b></td>
        <td style="width: 15px; height: 40px; text-align: center; font-size: 11px;"><b>{{ trans('swis.payments.transaction_date.title') }}</b></td>
        <td style="width: 15px; height: 40px; text-align: center; font-size: 11px;"><b>{{ trans('swis.payments.total_balance.title') }}</b></td>
    </tr>

    @foreach($data as $item)
        <tr>
            <td style="height: 60px; text-align: center; word-wrap: break-word; font-size: 8px;">{{ $item['payment_id'] }}</td>
            <td style="height: 60px; text-align: center; word-wrap: break-word; font-size: 8px;">{{ $item['service_provider_name'] }}</td>
            <td style="height: 60px; text-align: center; word-wrap: break-word; font-size: 8px;">{{ $item['saf_number'] }}</td>
            <td style="height: 60px; text-align: center; word-wrap: break-word; font-size: 8px;">{{ $item['document_number'] }}</td>
            <td style="height: 60px; text-align: center; word-wrap: break-word; font-size: 8px;">{{ $item['agency'] }}</td>
            <td style="height: 60px; text-align: center; word-wrap: break-word; font-size: 8px;">{{ $item['document'] }}</td>
            <td style="height: 60px; text-align: center; word-wrap: break-word; font-size: 8px;">{{ $item['obligation_type'] }}</td>
            <td style="height: 60px; text-align: center; word-wrap: break-word; font-size: 8px;">{{ $item['username'] }}</td>
            <td style="height: 60px; text-align: center; word-wrap: break-word; font-size: 8px;">{{ $item['amount'] }}</td>
            <td style="height: 60px; text-align: center; word-wrap: break-word; font-size: 8px;">{{ $item['transaction_date'] }}</td>
            <td style="height: 60px; text-align: center; word-wrap: break-word; font-size: 8px;">{{ $item['total_balance'] }}</td>
        </tr>
    @endforeach
    </tbody>
</table>