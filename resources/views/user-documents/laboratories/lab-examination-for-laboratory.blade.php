<div class="lab-examination-product-and-sample-container">
    <?php

    use App\Models\ReferenceTable\ReferenceTable;
    use App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure;

    $lastXML = SingleApplicationDataStructure::lastXml();

    $labExaminationListTable = $lastXML->xpath("//*[@tableClass='labExaminationProductAndSample']");

    $columns = (array)$labExaminationListTable[0]->columns;
    $selectColumns = implode(",", $columns) . ",id";

    $labExaminations = \App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications::select('id')
        ->where('id', $subApplication->id)
        ->with(["productList:{$selectColumns}"])
        ->first();
    $smallColumns = ['sample_quantity', 'sample_number', 'product_code', 'correspond'];
    $allData = json_decode($subApplication->all_data, true);

    $getFromSubApplication = null;
    if (!is_null($subApplication->from_subapplication_id_for_labs)) {
        $getFromSubApplication = \App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications::find($subApplication->from_subapplication_id_for_labs);
    }
    if (isset($tab)) {
        $view = !$tab['editable'];
    }
    ?>

    <div class="panel panel-default">
        <div class="panel-heading" data-toggle="collapse" data-parent="#labUserInfo" href="#labUserInfo" style="cursor: pointer;">
            {{ trans('swis.lab_examination.user.info.title') }}
        </div>
        <div class="panel-body panel-collapse collapse no-padding" id="labUserInfo">

            <div class="m-t-md">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">{{trans('swis.lab_examinations.user.info.inn')}}</label>
                        @if (!is_null($subApplication->from_subapplication_id_for_labs))
                            <input value="{{ $getFromSubApplication->company_tax_id }}" type="text" readonly class="form-control">
                        @else
                            <input value="{{ $allData['data']['SAF_ID_42'] ?? '' }}" type="text" readonly class="form-control">
                        @endif
                    </div>

                    <div class="form-group">
                        <label class="control-label">{{trans('swis.lab_examinations.user.info.name')}}</label>
                        @if (!is_null($subApplication->from_subapplication_id_for_labs))
                            <input value="{{ $getFromSubApplication->agency->current()->name ?? '' }}" type="text" readonly class="form-control">
                        @else
                            <input value="{{ $allData['data']['SAF_ID_43'] ?? '' }}" type="text" readonly class="form-control">
                        @endif
                    </div>

                    <div class="form-group">
                        <label class="control-label">{{trans('swis.lab_examinations.user.info.subdivision')}}</label>
                        @if (!is_null($subApplication->from_subapplication_id_for_labs))
                            <input value="{{ getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME, $getFromSubApplication->agency_subdivision_id)->name ?? '' }}" type="text" readonly class="form-control">
                        @else
                            <input value="" type="text" readonly class="form-control">
                        @endif
                    </div>
                </div>

                <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">{{trans('swis.lab_examinations.user.info.fullname')}}</label>
                    <input value="{{ $subApplication->userInfo->name() ?? '' }}" type="text" class="form-control" readonly>
                </div>

                <div class="form-group">
                    <label class="control-label">{{trans('swis.lab_examinations.user.info.phone')}}</label>
                    @if (!is_null($subApplication->from_subapplication_id_for_labs))
                        <input value="{{ $getFromSubApplication->agency->phone_number ?? '' }}" type="text" readonly class="form-control">
                    @else
                        <input value="{{ $allData['data']['SAF_ID_46'] ?? '' }}" type="text" class="form-control" readonly>
                    @endif
                </div>

                <div class="form-group">
                    <label class="control-label">{{trans('swis.lab_examinations.user.info.email')}}</label>
                    @if (!is_null($subApplication->from_subapplication_id_for_labs))
                        <input value="{{ $getFromSubApplication->agency->email ?? '' }}" type="text" readonly class="form-control">
                    @else
                        <input value="{{ $allData['data']['SAF_ID_47'] ?? '' }}" type="text" class="form-control" readonly>
                    @endif
                </div>
            </div>
            </div>

        </div>
    </div>

    <div class="panel-group formAccordion" id="productsList" role="tablist" aria-multiselectable="true">
        <div class="clearfix product-accordion-heading">
            <div class="col-lg-1"></div>
            @foreach($columns as $column)
                @php($width = in_array($column, $smallColumns) ? '1' : '2')
                <div class="col-lg-{{ ($column == 'product_description') ? '3' : $width }}">{{ trans("swis.saf.lab_examination.product_and_sample.{$column}.column") }}</div>
            @endforeach
        </div>

        @if (isset($labExaminations->productList) && count($labExaminations->productList))
            @foreach ($labExaminations->productList as $product)
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="labProduct{{ $product->id }}">
                        <div class="panel-title">
                            <div class="row m-n">
                                <?php
                                $hsCode = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_HS_CODE_6, $product->product_code, false, ['id', 'code', 'path_name']);
                                $sampleMeasurementUnit = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $product->sample_measurement_unit, false, ['id', 'code', 'name']);
                                ?>
                                <div class="col-lg-1">
                                    <span style="cursor: pointer;" data-toggle="collapse" data-parent="#productsList"
                                          href="#collapseOneLabProduct{{ $product->id }}" aria-expanded="false"
                                          aria-controls="collapseOneLabProduct{{ $product->id }}">
                                        <i class="fas fa-chevron-right"></i>
                                    </span>

                                    @if ($subApplication->lab_type != \App\Models\Laboratory\Laboratory::SELECT_INDICATOR_BY_PRODUCT_INDICATOR && !$view)
                                        <span class="btn btn-sm btn-primary addIndicatorButton"
                                              data-product-id="{{$product->id}}" style="margin-left: 10px;">
                                            <i class="fas fa-plus"></i>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-lg-1"><span title="{{ $product->sample_quantity }}">{{ $product->sample_quantity }}</span></div>
                                <div class="col-lg-2"><span title="{{ optional($sampleMeasurementUnit)->name }}">{{ optional($sampleMeasurementUnit)->name }}</span></div>
                                <div class="col-lg-1"><span title="{{ $product->sample_number }}">{{ $product->sample_number }}</span></div>
                                <div class="col-lg-2"><span title="{{ $product->product_number }}">{{ $product->product_number }}</span></div>
                                <div class="col-lg-1"><span title="{{ $hsCode->code ?? '' }}">{{ $hsCode->code ?? '' }}</span></div>
                                <div class="col-lg-3"><span title="{{ $hsCode->path_name ?? '' }}">{{ $hsCode->path_name ?? '' }}</span></div>
                                <?php
                                $correspond = '';

                                if ($product->correspond == \App\Models\SingleApplicationSubApplicationProducts\SingleApplicationSubApplicationProducts::CORRESPOND_YES) {
                                    $correspond = trans('swis.user_documents.correspond.answer.yes');
                                } elseif ($product->correspond == \App\Models\SingleApplicationSubApplicationProducts\SingleApplicationSubApplicationProducts::CORRESPOND_NO) {
                                    $correspond = trans('swis.user_documents.correspond.answer.no');
                                }
                                ?>
                                <div class="col-lg-1"><span title="{{ $product->correspond }}"
                                                            class="correspond{{$product->id}}">{{ $correspond }}</span>
                                </div>
                                <input type="hidden" class="labExaminationCorrespond{{$product->id}}"
                                       name="lab_examination[{{$product->id}}][correspond]" value="{{ $product->correspond ?? null }}"/>
                            </div>

                        </div>
                    </div>
                    <div id="collapseOneLabProduct{{ $product->id }}" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="collapseOneLabProduct{{ $product->id }}">
                        <div class="panel-body ">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>{{trans("swis.user-documents.lab_examination.indicators.indicator")}}</th>
                                        <th>{{trans("swis.user-documents.lab_examination.indicators.permitted_quantity")}}</th>
                                        <th>{{trans("swis.user-documents.lab_examination.indicators.measurement_unit")}}</th>
                                        <th>{{trans("swis.user-documents.lab_examination.indicators.qualitative")}}</th>
                                        <th>{{trans("swis.user-documents.lab_examination.indicators.found_size")}}</th>
                                        <th>{{trans("swis.user-documents.lab_examination.indicators.found_or_not")}}</th>
                                        <th>{{trans("swis.user-documents.lab_examination.indicators.correspond_or_not")}}</th>
                                        <th>{{trans("swis.user-documents.lab_examination.indicators.duration")}}</th>
                                        <th>{{trans("swis.user-documents.lab_examination.indicators.amount")}}</th>
                                        @if (isset($view) && !$view)
                                            <th></th>
                                        @endif
                                    </tr>
                                    </thead>

                                    <tbody class="tableBody{{ $product->id }}">
                                        @if ($subApplication->lab_type != \App\Models\Laboratory\Laboratory::SELECT_INDICATOR_BY_PRODUCT_INDICATOR)
                                            <?php
                                            $getIndicators = getReferenceRowsWhereIn(ReferenceTable::REFERENCE_LAB_HS_INDICATOR, $subApplication->labIndicators->pluck('indicator_id')->all(), [], ['id', 'code', 'name', 'limit_3', 'measurement_units', 'limit_quality_2'])->keyBy('id')->toArray();

                                            $labIndicators = [];
                                            foreach ($subApplication->labIndicators as $labIndicator) {
                                                $labIndicators[$labIndicator->saf_product_id][$labIndicator->indicator_id] = $labIndicator;
                                            }
                                            ?>
                                            @foreach($labIndicators[$product->id] ?? [] as $indicatorId => $indicatorData)
                                                @if (isset($getIndicators[$indicatorId]))
                                                    <?php $indicator = $getIndicators[$indicatorId] ?>
                                                    @include('user-documents.laboratories.lab-examination-for-laboratory-table-tr', ['add' => false, 'view' => $view])
                                                @endif
                                            @endforeach
                                        @else
                                            @if(isset($subApplication->indicators))
                                                @if (count($subApplication->labIndicators) > 0)
                                                    <?php
                                                    $getIndicators = getReferenceRowsWhereIn(ReferenceTable::REFERENCE_LAB_HS_INDICATOR, $subApplication->labIndicators->pluck('indicator_id')->all(), [], ['id', 'code', 'name', 'limit_3', 'measurement_units', 'limit_quality_2'])->keyBy('id')->toArray();

                                                    $labIndicators = [];
                                                    foreach ($subApplication->labIndicators as $labIndicator) {
                                                        $labIndicators[$labIndicator->saf_product_id][$labIndicator->indicator_id] = $labIndicator;
                                                    }
                                                    ?>
                                                    @foreach($labIndicators[$product->id] ?? [] as $indicatorId => $indicatorData)
                                                        <?php $indicator = $getIndicators[$indicatorId] ?>
                                                        @include('user-documents.laboratories.lab-examination-for-laboratory-table-tr', ['add' => false, 'view' => $view])
                                                    @endforeach
                                                @else
                                                    <?php $indicators = getReferenceRowsWhereIn(ReferenceTable::REFERENCE_LAB_HS_INDICATOR, $subApplication->indicators, [], ['id', 'code', 'name', 'limit_3', 'measurement_units', 'limit_quality_2'])->keyBy('id')->toArray(); ?>
                                                    @foreach($subApplication->indicators as $indicator)
                                                        <?php $indicator = $indicators[$indicator] ?>
                                                        @include('user-documents.laboratories.lab-examination-for-laboratory-table-tr', ['add' => false, 'view' => $view])
                                                    @endforeach
                                                @endif
                                            @endif
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
    <br>

    <div class="col-lg-4 col-lg-offset-8 no-padding">
        <div class="form-group pull-right">
            <label class="col-md-7 control-label label__title text-right">{{trans('swis.lab-examination-for-laboratory.total')}}</label>
            <div class="col-md-5 no-padding">
                <div class="row">
                    <div class="col-md-12 field__one-col">
                        <input type="text" class="form-control labExaminationTotal readonly" name="" id="" value="" disabled readonly>
                    </div>
                </div>
            </div>
        </div>
    </div><br/>

</div>
