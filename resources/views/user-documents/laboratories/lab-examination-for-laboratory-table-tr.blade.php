<tr>
    <td>
        @if ($add)
            <div class="form-group" style="margin: 0;">
                <select class="select2 selectIndicator selectIndicator{{$product->id}}" data-product-id="{{$product->id}}" name="{{ isset($indicator) ? "lab_examination[$product->id][$indicator->id][indicator]" : ""}}" {{(isset($view) && $view) ? 'disabled' : ''}}>
                    <option value=""></option>
                    @foreach($indicators as $item)
                        <option value="{{ $item->id }}">{{ "({$item->code}) $item->name" }}</option>
                    @endforeach
                </select>
                <div class="form-error errorIndicator" id="form-error-lab_examination-{{$product->id}}-{{$indicator->id ?? ''}}-indicator"></div>
            </div>
        @else
            {{ "({$indicator->code}) {$indicator->name}" }}
            <input type="hidden" class="hiddenIndicatorValue{{$product->id}}" name="lab_examination[{{$product->id}}][{{$indicator->id}}][indicator]" value="{{ $indicator->id }}">
            <div class="form-error" id="form-error-lab_examination-{{$product->id}}-{{$indicator->id}}-indicator"></div>
        @endif
        <input type="hidden" class="labProductId" value="{{ $product->id }}">
    </td>
    <td class="limit_3">{{ $indicator->limit_3 ?? '' }}</td>
    <td class="measurement_units">{{ !$add ? optional(getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $indicator->measurement_units, false, ['id', 'code', 'name']))->name : '' }}</td>
    <td class="limit_quality_2">{{ !$add ? optional(getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_LAB_QUALITY_INDICATOR, $indicator->limit_quality_2, false, ['id', 'code', 'name']))->name : '' }}</td>
    <td>
        <div class="form-group col-md-12">
            <input type="number" min="0" class="form-control onlyNumbers found_size" name="{{ isset($indicator) ? "lab_examination[$product->id][$indicator->id][found_size]": ""}}" value="{{ !empty($indicatorData) ? $indicatorData->found_size : ''}}" {{(isset($view) && $view) ? 'disabled' : ''}}>
            <div class="form-error errorFoundSize" id="form-error-lab_examination-{{$product->id}}-{{$indicator->id ?? ''}}-found_size"></div>
        </div>
    </td>
    <td>
        <div class="form-check col-md-12">
            <div class="row">
                {{ trans('swis.user_documents.found_or_not.yes') }} <input type="radio" class="found_or_not" value="1" name="{{ isset($indicator) ? "lab_examination[$product->id][$indicator->id][found_or_not]" : "" }}" {{ (!empty($indicatorData) && $indicatorData->found_or_not == 1) ? 'checked' : ''}} {{(isset($view) && $view) ? 'disabled' : ''}}/><br/>
                {{ trans('swis.user_documents.found_or_not.no') }} <input type="radio" class="found_or_not" value="0" name="{{ isset($indicator) ? "lab_examination[$product->id][$indicator->id][found_or_not]" : "" }}" {{ (!empty($indicatorData) && $indicatorData->found_or_not == 0) ? 'checked' : ''}} {{(isset($view) && $view) ? 'disabled' : ''}}/>
            </div>
            <div class="form-error errorFoundOrNot" id="form-error-lab_examination-{{$product->id}}-{{$indicator->id ?? ''}}-found_or_not"></div>
        </div>
    </td>
    <td>
        <div class="form-check col-md-12">
            <div class="row">
                {{ trans('swis.user_documents.correspond_or_not.yes') }} <input type="radio" class="correspond_or_not" data-product-id="{{$product->id}}" value="1" name="{{ isset($indicator) ? "lab_examination[$product->id][$indicator->id][correspond_or_not]": "" }}" {{ (!empty($indicatorData) && $indicatorData->correspond_or_not == 1) ? 'checked' : ''}} {{(isset($view) && $view) ? 'disabled' : ''}}/><br/>
                {{ trans('swis.user_documents.correspond_or_not.no') }} <input type="radio" class="correspond_or_not" data-product-id="{{$product->id}}" value="0" name="{{ isset($indicator) ? "lab_examination[$product->id][$indicator->id][correspond_or_not]": "" }}" {{ (!empty($indicatorData) && $indicatorData->correspond_or_not == 0) ? 'checked' : ''}} {{(isset($view) && $view) ? 'disabled' : ''}}/>
            </div>
            <div class="form-error errorCorrespondOrNot" id="form-error-lab_examination-{{$product->id}}-{{$indicator->id ?? ''}}-correspond_or_not"></div>
        </div>
    </td>

    <?php
    $price = $duration = null;

    if (!$add) {
        if (is_null($subApplication->from_subapplication_id_for_labs)) {
            $agencyLabIndicator = \App\Models\Agency\AgencyLabIndicators::where('lab_hs_indicator_id', $indicator->id)->where('agency_id', $subApplication->agency_id)->first();
            $price = optional($agencyLabIndicator)->price;
            $duration = optional($agencyLabIndicator)->duration;
        } else {
            $tmpSubApplication = App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications::select('company_tax_id')->find($subApplication->from_subapplication_id_for_labs);
            $labIndicator = \App\Models\LaboratoryIndicator\LaboratoryIndicator::where('indicator_id', $indicator->id)->where('company_tax_id', $tmpSubApplication->company_tax_id)->first();
            $price = optional($labIndicator)->price;
            $duration = optional($labIndicator)->duration;
        }
    }
    ?>

    <td>
        <div class="form-check col-md-12">
            <p class="duration">{{$duration}}</p>

            <input type="hidden" class="labExaminationDuration" value="{{ $duration }}" name="{{ isset($indicator) ? "lab_examination[$product->id][$indicator->id][duration]": ""}}">
        </div>
    </td>
    <td>
        <p class="price">{{$price}}</p>

        <input type="hidden" class="labExaminationPrice" value="{{ $price }}" name="{{ isset($indicator) ? "lab_examination[$product->id][$indicator->id][price]" : "" }}">
    </td>

    @if (isset($view) && !$view)
        <td align="center">
            @if ($add)
                <button class='btn btn-danger btn-circle removeLabIndicator' data-product-id="{{$product->id}}" type='button'>
                    <i class='fa fa-trash'></i>
                </button>
            @endif
        </td>
    @endif

</tr>