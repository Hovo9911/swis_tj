<div class="table-responsive">
    <table class="table table-bordered {{ $block['tableClass'] }}" data-sub-form="{{ $block['tableClass'] }}" data-module="{{ $block['module'] }}" data-sub-block="{!! (!empty($block['subBlock'])) ? 'true' : '' !!}" >
    <thead>
        <tr>
            @foreach ($block['data']['columns'] as $key => $column)
                @if($key ==2 || $key == 3)
                    @continue
                @endif
                <th>{{ $column }}</th>
            @endforeach
            <th></th>
        </tr>
    </thead>

    <tbody id="obligationsTBody">
        @if (isset($block['data']['obligations']) && count($block['data']['obligations']) > 0 )
            @foreach($block['data']['obligations'] as $key=>$obligation)
                <tr>
                    <td>{{$key+1}}</td>
                    <td align="center"><a href="{{urlWithLng("/payment-and-obligations/generatePdf/{$obligation['id']}/{$obligation['pdf_hash_kay']}")}}" target="_blank" >
                            <button class="btn--icon" type="button" data-toggle="tooltip"  title="<?php echo trans('swis.base.tooltip.print.button') ?>"><i class="fas fa-print"></i></button>
                        </a>
                    </td>
                    <td>{{ $obligation['subApplication'] }}</td>
{{--                    <td>{{ $obligation['subApplicationType'] }}</td>--}}
                    <td>{{ $obligation['documentId'] }}</td>
                    <td style="max-width: 220px;width: 220px">{{ $obligation['obligationType'] }}</td>
                    <td style="width: 200px">{{ $obligation['subApplicationName'] }}</td>
                    <td style="width: 100px" class="text-break-word">{{ $obligation['budgetLine'] }}</td>
                    <td>{{ $obligation['paymentDate'] }}</td>
                    <td>{{ $obligation['payer'] }}</td>
                    <td>{{ $obligation['payer_person'] }}</td>
                    <td>
                        {{ $obligation['obligation_sum'] }}
                        <input type="hidden" class="freeSumObligation" value="{{ $obligation['obligation_sum'] }}">
                    </td>
                    <td>{{ $obligation['payments'] }}</td>
                    <td>{{ $obligation['balance'] }}</td>
                    <td class="check-obligation" align="center">
                        @if($obligation['obligation_sum'] == $obligation['payments'])
                            <i class="fas fa-check"></i>
                        @elseif($obligation['isFreeSum'] && $obligation['obligation_sum'] != $obligation['payments'])
                            <button type="button" class="btn-remove obligation-delete-ajax" data-obligation-id="{{ $obligation['id'] }}"><i class="fa fa-times"></i></button>
                        @endif
                    </td>
                </tr>
            @endforeach
        @endif

    </tbody>

    <tr>
        <td colspan="10" class="text-right">{{ trans('swis.saf.obligations.pay.total') }} </td>
        <td class="totalObligationSum">{{ $block['data']['totalSum'] }}</td>
        <td>{{ $block['data']['paymentSum'] }}</td>
        <td class="totalObligationBalanceSum">{{ number_format($block['data']['balanceSum'], 2, '.', '') }}</td>
        <td></td>
    </tr>
</table>
</div>

@if($isHaveMultipleObligation && $showUserMappingList)
    <button type="button" class="btn btn-primary plusMultipleFieldsObligation">
        <i class="fa fa-plus"></i><span class="documents-plusBtn__text">
            {{trans('swis.user_documents.obligations.add_obligation_button')}}
        </span>
    </button>
@endif