<tr id="">
    <td></td>
    <td></td>
    <td class="check-obligation">{{ $data['subApplication'] }}</td>
{{--    <td>{{ $data['subApplicationType'] }}</td>--}}
    <td>{{ $data['documentId'] }}</td>
    <td style="max-width: 200px;width: 250px">
        <div class="form-group m-lr-n">
            <select class="obligation-type" data-type="obligationType" name="free_sum_obligation[{{$incrementNumber}}][obligation_type]">
                <option value=""></option>
                @foreach ($data['obligationTypes'] as $obligation)
                    <option data-account_number="{{$obligation->account_number}}" value="{{ $obligation->id }}">
                        ({{ $obligation->account_number }}) {{ $obligation->code }}, {{ $obligation->name }}
                    </option>
                @endforeach
            </select>
            <div class="form-error" id="form-error-{{'free_sum_obligation-'.$incrementNumber.'-obligation_type'}}"></div>
        </div>
    </td>
    <td style="width: 200px">{{ $data['subApplicationName'] }}</td>
    <td style="width: 100px">
        <input type="text" name="free_sum_obligation[{{$incrementNumber}}][budget_line]"
               class="form-control budget-line" readonly="readonly">
        <div class="form-error" id="form-error-{{'free_sum_obligation-'.$incrementNumber.'-budget_line'}}"></div>
    </td>
    <td></td>
    <td></td>
    <td></td>
    <td>
        <div class="form-group m-lr-n">
        <input type="text" class="form-control onlyWithFloat freeSumObligation"
               name="free_sum_obligation[{{$incrementNumber}}][obligation]"
               placeholder="{{ number_format(0, '2', '.', '') }}">
        <div class="form-error" id="form-error-{{'free_sum_obligation-'.$incrementNumber.'-obligation'}}"></div>
        </div>
    </td>
    <td>{{ number_format(0, '2', '.', '') }}</td>
    <td class="freeSumObligationBalance">{{ number_format(0, '2', '.', '') }}</td>
    <td>
        <button type="button" class="btn-remove obligation-delete"><i class="fa fa-times"></i></button>
    </td>
</tr>