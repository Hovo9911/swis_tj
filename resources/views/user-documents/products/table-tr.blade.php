@php($num = empty($num) ? 1 : $num)
<tr data-id="{{$num}}">
    <input type="hidden" name="batch[{{$batchId}}][{{$num}}][id]" class="batchesId" value="{{ $batchId }}">
    @foreach ($fields as $field)
        <td style="{{ $field['fieldStyle'] }}" >
            @if ($field['origin'] == 'saf')
                @switch($field['fieldType'])
                    @case('select')
                        <select class="form-control select2 {{ $field['fieldDisabled'] }} {{ $field['fieldClass'] }}" {{ $field['fieldDisabled'] }} name="{{ $field['fieldName'] }}">
                            <option value="" disabled selected></option>
                            @foreach($field['options'] as $option)
                                <option value="{{$option['code']}}" {{ $option['selected'] }}>{{ $option['name'] }}</option>
                            @endforeach
                        </select>
                    @break

                    @case('input')
                        <input {{ $field['fieldDisabled'] }} value="{{ $field['fieldValue'] }}" type="{{ $field['fieldInputType'] }}" class="form-control {{ $field['fieldClass'] }}" name="{{ $field['fieldName'] }}">
                    @break

                    @case('textarea')
                        <textarea rows="5" cols="5" name="{{ $field['fieldName'] }}" class="form-control" {{ $field['fieldDisabled'] }}></textarea>
                    @break

                    @case('autocomplete')
                        <input type="{{ $field['fieldInputType'] }}" name="{{ $field['fieldName'] }}" placeholder="autocomplete" class="form-control autocomplete"
                               data-ref="{{ $field['dataAttributes']['dataRef'] }}" data-fields="{{ $field['dataAttributes']['dataFields'] }}" {{ $field['fieldDisabled'] }}>
                    @break

                    @case('actions')
                        @foreach($field['actions'] as $action)
                            <button type="button"  data-id="{{ $action['dataId'] }}" class="{{ $action['class'] }}"><i class="{{ $action['icon'] }}"></i></button>
                        @endforeach
                    @break

                    @case('autoincrement')
                        <span class="incrementNumber">{{ $num }}</span>
                    @break

                    @case('date')
                        <input value="{{ $field['fieldValue'] }}" name="{{ $field['fieldName']  }}" type="date" class="form-control {{ $field['fieldClass'] }}" {{ $field['fieldDisabled'] }}>
                    @break
                @endswitch
                <div class="form-error" id="{{ $field['fieldError'] }}"></div>
            @else

                @if ($field['fieldType'] == 'autocomplete')
                    <select class="form-control select2 {{ $field['fieldDisabled'] }}" id="{{ $field['fieldId'] }}" name="{{ $field['fieldName'] }}"  {{ $field['fieldDisabled'] }}>
                        <option value="">{{ trans('swis.document.label.select') }}</option>

                        @foreach ($field['options'] as $option)
                            <option value="{{ $option['code'] }}" {{ $option['selected'] }} >{{ $option['name'] }}</option>
                        @endforeach
                    </select>
                @else
                    <input data-toggle="tooltip" title="{{ $field['fieldTitle'] }}" type="{{ $field['fieldType'] }}" class="form-control agencyInput {{ $field['fieldDisabled'] }}" name="{{ $field['fieldName'] }}" value="{{ $field['fieldValue'] }}" {{ $field['fieldDisabled'] }}>
                @endif

                <div class="form-error" id="{{ $field['fieldError'] }}"></div>
            @endif
        </td>
    @endforeach
</tr>