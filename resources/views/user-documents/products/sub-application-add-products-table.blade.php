<?php
use App\Models\ReferenceTable\ReferenceTable;
?>
<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th style="width:30px"><input type="checkbox" id="check-all-subApplication-add-products"></th>
            <th>{{trans('swis.single_app.product_number')}}</th>
            <th>{{trans('swis.single_app.product_code')}}</th>
            <th>{{trans('swis.single_app.commercial_description')}}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $value)
            <?php
            $hsCode = getReferenceRows(ReferenceTable::REFERENCE_HS_CODE_6, $value->product_code);
            ?>
            <tr>
                <td><input data-pr-number="{{$productsIdNumbers[$value->id]}}" class="pr" value="{{$value->id}}"
                           type="checkbox"></td>
                <td>{{$productsIdNumbers[$value->id]}}</td>
                <td><span data-toggle="tooltip" data-placement="top"
                          title="{{$value->product_description}}">{{$hsCode->code ?? ''}}</span></td>
                <td>{{$value->commercial_description}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
