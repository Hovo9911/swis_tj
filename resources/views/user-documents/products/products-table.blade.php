@if(!empty($block['data']))
    <div class="products-accordion-option"><a href="javascript:void(0)" class="toggle-accordion active"></a></div>
    <div class="clearfix"></div>
    <div class="panel-group formAccordion" id="productsList" role="tablist" aria-multiselectable="true">
        <div class="clearfix product-accordion-heading">
            @foreach($block['data']['columns'] as $column)
                <div class="{{ $column['class'] }}"> {{ $column['name'] }} </div>
            @endforeach
        </div>

        @foreach($block['data']['products'] as $product)
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="product{{ $product['productId'] }}">
                    <div class="panel-title">
                        <div class="row m-n">
                            @foreach($product['columns'] as $column)
                                <div class="{{ $column['class'] }}" >
                                    <span  title="{{ $column['tooltip'] }}">{{ $column['name'] }}</span></div>
                            @endforeach

                            <div class="col-md-12 user-documents__goodDropdown" data-toggle="collapse" data-parent="#productsList" href="#collapseOne{{ $product['productId'] }}" aria-expanded="false" aria-controls="collapseOne{{ $product['productId'] }}">
                                <i class="fas fa-chevron-down"></i>
                            </div>
                        </div>

                    </div>
                </div>
                <div id="collapseOne{{ $product['productId'] }}" class="panel-collapse collapse" role="tabpanel"
                     aria-labelledby="headingOne{{ $product['productId'] }}">
                    <div class="panel-body ">

                        <?php
                        $block = $product;
                        $raw = null;
                        $productId = $product['productId'];

                        ?>
                        @if($key == 'notes')
                            @include('user-documents.notes.saf-notes-actions')
                        @endif

                        @if (isset($block['fields']) && count($block['fields']) > 0)
                            @foreach ($block['fields'] as $field)
                                <div class="form-group {{ $field['fieldSetClass'] }}">
                                    <label class="col-md-4 col-lg-3 control-label label__title" data-toggle="tooltip"
                                           title="{{ $field['fieldSetTitle'] }}"
                                           data-placement="{{ $field['fieldSetTitlePlacement'] }}"> {{ $field['fieldSetLabel'] }} </label>

                                    <div class="col-md-8">
                                        <div class="{{ $field['fieldSetMultipleClass'] }}"
                                             data-fields="{{ $field['fieldSetData'] }}">
                                            <div class="clearfix row">
                                                @if ($field['fieldSetOrigin'] == 'mdm')
                                                    <div class="col-lg-10 col-md-8 field__one-col">
                                                        @if ($field['fieldType'] == 'autocomplete')
                                                            <select
                                                                    title="{{ $field['fieldTitle'] }}"
                                                                    class="form-control select2 select2-from-mdm {{ $field['fieldClass'] }} {{ $field['fieldDisabled'] }}"
                                                                    id="{{ $field['fieldId'] }}"
                                                                    data-url="{{ $field['fieldDataUrl'] }}"
                                                                    data-ref="{{ $field['fieldDataRef'] }}"
                                                                    data-min-input-length="1"
                                                                    data-one-and-more="true"
                                                                    data-mdm-select="true"
                                                                    name="{{ $field['fieldName'] }}"
                                                                    {{ $field['fieldDisabled'] }}>
                                                                <option value=""
                                                                        selected>{{ trans('core.base.label.select') }}</option>
                                                                @if (isset($field['options']))
                                                                    @foreach ($field['options'] as $option)
                                                                        <option {{ $option['selected'] }} value="{{ $option['value'] }}">{{ $option['name'] }}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        @else
                                                            @if ($field['fieldType'] == 'date')
                                                                <div class='input-group date'>
                                                                    <span class="input-group-addon"><span
                                                                                class="glyphicon glyphicon-calendar"></span></span>
                                                                    <input data-toggle="tooltip" autocomplete="off"
                                                                           title="{{ $field['fieldTitle'] }}"
                                                                           type="text"
                                                                           class="form-control agencyInput {{ $field['fieldDisabled'] }}"
                                                                           name="{{ $field['fieldName'] }}"
                                                                           value="{{ $field['fieldValue'] }}"
                                                                           {{ $field['fieldDisabled'] }}
                                                                           placeholder="{{setDatePlaceholder()}}"/>
                                                                </div>
                                                            @else
                                                                @if ($field['isFieldMdmLine'])
                                                                    <hr class="mdm-line-hr" />
                                                                @else
                                                                    @if ($field['fieldIsContent'])
                                                                        <textarea rows="5" data-toggle="tooltip" name="{{ $field['fieldName'] }}" title="{{ $field['fieldTitle'] }}" class="form-control agencyInput {{ $field['fieldDisabled'] }}" {{ $field['fieldDisabled'] }}>{{ $field['fieldValue'] }}</textarea>
                                                                    @else
                                                                        <input data-toggle="tooltip"
                                                                               title="{{ $field['fieldTitle'] }}"
                                                                               type="{{ $field['fieldType'] }}"
                                                                               class="form-control agencyInput {{ $field['fieldDisabled'] }}"
                                                                               name="{{ $field['fieldName'] }}"
                                                                               value="{{ $field['fieldValue'] }}"
                                                                                {{ $field['fieldDisabled'] }}>
                                                                    @endif
                                                                @endif

                                                            @endif
                                                        @endif

                                                        <div class="form-error"
                                                             id="{{ $field['fieldErrorName'] }}"></div>
                                                    </div>

                                                @else

                                                    @foreach ($field['fieldInFieldSet'] as $fieldInFieldSet)

                                                        <div class="{{ $fieldInFieldSet['fieldBlockClass'] }}">
                                                            @switch($fieldInFieldSet['fieldType'])

                                                                @case('select')
                                                                <select class="form-control select2 {{ $fieldInFieldSet['fieldClass'] }} {{ $fieldInFieldSet['fieldReadOnly'] }} {{ $fieldInFieldSet['fieldDisabled'] }}"
                                                                        name="{{ $fieldInFieldSet['fieldName'] }}"
                                                                        data-type="{{ $fieldInFieldSet['dataAttributes']['dataType'] }}" {{ $fieldInFieldSet['fieldDisabled'] }}>
                                                                    <option value=""
                                                                            selected>{{ trans('core.base.label.select') }}</option>
                                                                    @foreach($fieldInFieldSet['options'] as $option)
                                                                        <option {{ $option['selected'] }} title="{{ $option['name'] }}"
                                                                                value="{{ $option['value'] }}"> {{ $option['name'] }} </option>
                                                                    @endforeach
                                                                </select>
                                                                @break

                                                                @case('input')
                                                                @if ($fieldInFieldSet['fieldIsPhoneNumber'])
                                                                    <div class="input-group"><span
                                                                                class="input-group-addon">+</span>
                                                                        @endif
                                                                        <input type="{{ $fieldInFieldSet['fieldInputType'] }}"
                                                                               class="form-control {{ $fieldInFieldSet['fieldClass'] }} {{ $fieldInFieldSet['fieldReadOnly'] }} {{ $fieldInFieldSet['fieldDisabled'] }}"
                                                                               name="{{ $fieldInFieldSet['fieldName'] }}"
                                                                               value="{{ $fieldInFieldSet['fieldValue'] }}"
                                                                               autocomplete="nope"
                                                                               id="{{ $fieldInFieldSet['fieldId'] }}"
                                                                               data-name="{{ $fieldInFieldSet['dataAttributes']['dataName'] }}"
                                                                               data-type="{{ $fieldInFieldSet['dataAttributes']['dataType'] }}"
                                                                               data-toggle="tooltip" title="{{$fieldInFieldSet['fieldValue']}}"
                                                                                {{ $fieldInFieldSet['fieldDisabled'] }}>
                                                                        @if ($fieldInFieldSet['fieldIsPhoneNumber'])
                                                                    </div>
                                                                @endif
                                                                @break

                                                                @case('textarea')
                                                                <textarea rows="5" cols="5"
                                                                          class="form-control {{ $fieldInFieldSet['fieldReadOnly'] }} {{ $fieldInFieldSet['fieldDisabled'] }}"  data-toggle="tooltip" title="{{$fieldInFieldSet['fieldValue']}}"
                                                                          {{ $fieldInFieldSet['fieldDisabled'] }} name="{{ $fieldInFieldSet['fieldName'] }}">{{ $fieldInFieldSet['fieldValue'] }}</textarea>
                                                                @break

                                                                @case('autocomplete')

                                                                @if(empty($fieldInFieldSet['refType']) || $fieldInFieldSet['refType'] != 'input')
                                                                    <select class="form-control autocomplete {{ $fieldInFieldSet['fieldClass'] }} {{ $fieldInFieldSet['fieldReadOnly'] }} {{ $fieldInFieldSet['fieldDisabled'] }}"
                                                                            name="{{ $fieldInFieldSet['fieldName'] }}"
                                                                            data-url="{{ $fieldInFieldSet['fieldDataUrl'] }}"
                                                                            data-ref="{{ $fieldInFieldSet['fieldDataRef'] }}" {{ $fieldInFieldSet['fieldDisabled'] }}>

                                                                        <option value=""></option>
                                                                        @foreach($fieldInFieldSet['options'] as $option)
                                                                            <option {{ $option['selected'] }} value="{{$option['value']}}"> {{ $option['name'] }} </option>
                                                                        @endforeach
                                                                    </select>
                                                                @endif

                                                                @break

                                                                @case('hidden')
                                                                <input value="{{ $fieldInFieldSet['fieldValue']  }}"
                                                                       type="hidden"
                                                                       name="{{ $fieldInFieldSet['fieldName'] }}" {{ $fieldInFieldSet['fieldDisabled'] }}>
                                                                @break

                                                                @case('autoincrement')
                                                                <input value="{{ $fieldInFieldSet['fieldValue'] }}"
                                                                       type="{{ $fieldInFieldSet['fieldInputType'] }}"
                                                                       readonly class="form-control autoincrement"
                                                                       name="{{ $fieldInFieldSet['fieldName'] }}" {{ $fieldInFieldSet['fieldDisabled'] }}>
                                                                @break

                                                                @case('checkbox')
                                                                <input type="checkbox"
                                                                       name="{{ $fieldInFieldSet['fieldName'] }}" {{ $fieldInFieldSet['fieldDisabled'] }}>
                                                                @break

                                                            @endswitch

                                                            <div class="form-error"
                                                                 id="{{ $fieldInFieldSet['fieldErrorName'] }}"></div>

                                                            {!! $fieldInFieldSet['fieldNeedMdm'] !!}
                                                        </div>

                                                        @if($field['fieldSetMultiple'])
                                                            <button type="button"
                                                                    class="btn btn-primary plusMultipleFields"><i
                                                                        class="fa fa-plus"></i></button>
                                                        @endif

                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            @endforeach
                        @endif

                        @if(!empty($block['subBlock']))
                            @if(!empty($block['subBlock']['fields']) && !empty(reset($block['subBlock']['fields'])))
                                <h3 class="batches--main-title fb">{{ trans('swis.products.more_info.title') }}</h3>
                                <hr/>

                                <div class="batches--block m-b-none" id="goodsProductListsSections">

                                    <h4 class="fb text--batches">{{ trans('swis.single_app.batches') }}</h4>

                                    <div id="goodsProductBatch" class="m-b-none">

                                        <div class="table-responsive m-b-none">
                                            <table class="table table-bordered {{ $block['subBlock']['tableClass'] }}"
                                                   data-module="{{ $block['subBlock']['dataModule'] }}"
                                                   data-sub-block="true">
                                                <thead>
                                                <tr>
                                                    @foreach ($block['subBlock']['columns'] as $column)
                                                        <td>{{ $column }}</td>
                                                    @endforeach
                                                </tr>
                                                </thead>
                                                <tbody>

                                                    @php($i = 0)
                                                    @foreach($block['subBlock']['fields'] as $key => $dataValue)

                                                        <?php
                                                        $fields = $dataValue;
                                                        $num = $i++;
                                                        $batchId = $key;
                                                        ?>
                                                        @php($num = empty($num) ? 1 : $num)
                                                        <tr data-id="{{$num}}">
                                                            <input type="hidden" name="batch[{{$batchId}}][{{$num}}][id]"
                                                                   class="batchesId" value="{{ $batchId }}">
                                                            @foreach ($fields as $field)
                                                                <td style="{{ $field['fieldStyle'] }}">
                                                                    @if ($field['origin'] == 'saf')
                                                                        @switch($field['fieldType'])
                                                                            @case('select')
                                                                            <select class="form-control select2 {{ $field['fieldDisabled'] }} {{ $field['fieldClass'] }}"
                                                                                    {{ $field['fieldDisabled'] }} name="{{ $field['fieldName'] }}">
                                                                                <option value="" disabled selected></option>
                                                                                @foreach($field['options'] as $option)
                                                                                    <option value="{{$option['code']}}" {{ $option['selected'] }}>{{ $option['name'] }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                            @break

                                                                            @case('input')
                                                                            @if ($field['fieldInputType'] == 'date')
                                                                                <div class='input-group date'>
                                                                                    <span class="input-group-addon"><span
                                                                                                class="glyphicon glyphicon-calendar"></span></span>
                                                                                    <input autocomplete="off"
                                                                                           type="text"
                                                                                           class="form-control {{ $field['fieldDisabled'] }} {{ $field['fieldClass'] }}"
                                                                                           name="{{ $field['fieldName'] }}"
                                                                                           value="{{ $field['fieldValue'] }}"
                                                                                           {{ $field['fieldDisabled'] }}
                                                                                           data-toggle="tooltip" title="{{$field['fieldValue']}}"
                                                                                           placeholder="{{setDatePlaceholder()}}"/>
                                                                                </div>
                                                                            @else
                                                                                <input {{ $field['fieldDisabled'] }}
                                                                                       value="{{ $field['fieldValue'] }}"
                                                                                       type="{{ $field['fieldInputType'] }}"
                                                                                       @if (isset($field['dataAttributes'], $field['dataAttributes']['quantity']))
                                                                                           data-quantity="{{$field['dataAttributes']['quantity']}}"
                                                                                       @endif
                                                                                       data-toggle="tooltip" title="{{$field['fieldValue']}}"
                                                                                       class="form-control {{ $field['fieldClass'] }}"
                                                                                       name="{{ $field['fieldName'] }}">
                                                                            @endif
                                                                            {{--                                                                        <input {{ $field['fieldDisabled'] }} value="{{ $field['fieldValue'] }}" type="{{ $field['fieldInputType'] }}" class="form-control {{ $field['fieldClass'] }}" name="{{ $field['fieldName'] }}">--}}
                                                                            @break

                                                                            @case('textarea')
                                                                            <textarea rows="5" cols="5"
                                                                                      name="{{ $field['fieldName'] }}"
                                                                                      class="form-control" {{ $field['fieldDisabled'] }}></textarea>
                                                                            @break

                                                                            @case('autocomplete')
                                                                            <input type="{{ $field['fieldInputType'] }}"
                                                                                   name="{{ $field['fieldName'] }}"
                                                                                   placeholder="autocomplete"
                                                                                   class="form-control autocomplete"
                                                                                   data-ref="{{ $field['dataAttributes']['dataRef'] }}"
                                                                                   data-fields="{{ $field['dataAttributes']['dataFields'] }}" {{ $field['fieldDisabled'] }}>
                                                                            @break

                                                                            @case('actions')
                                                                            @foreach($field['actions'] as $action)
                                                                                <button type="button"
                                                                                        data-id="{{ $action['dataId'] }}"
                                                                                        class="{{ $action['class'] }}"><i
                                                                                            class="{{ $action['icon'] }}"></i>
                                                                                </button>
                                                                            @endforeach
                                                                            @break

                                                                            @case('autoincrement')
                                                                            <span class="incrementNumber">{{ $num }}</span>
                                                                            @break

                                                                            @case('date')
                                                                            <input value="{{ $field['fieldValue'] }}"
                                                                                   name="{{ $field['fieldName']  }}"
                                                                                   data-toggle="tooltip" title="{{$field['fieldValue']}}"
                                                                                   type="date"
                                                                                   class="form-control {{ $field['fieldClass'] }}" {{ $field['fieldDisabled'] }}>
                                                                            @break
                                                                        @endswitch
                                                                        <div class="form-error"
                                                                             id="{{ $field['fieldError'] }}"></div>
                                                                    @else

                                                                        @if ($field['fieldType'] == 'autocomplete')
                                                                            <select class="form-control select2 {{ $field['fieldDisabled'] }}"
                                                                                    id="{{ $field['fieldId'] }}"
                                                                                    name="{{ $field['fieldName'] }}" {{ $field['fieldDisabled'] }}>
                                                                                <option value="">{{ trans('swis.document.label.select') }}</option>

                                                                                @foreach ($field['options'] as $option)
                                                                                    <option value="{{ $option['code'] }}" {{ $option['selected'] }} >{{ $option['name'] }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        @else
                                                                            <input
                                                                                   title="{{ $field['fieldTitle'] }}"
                                                                                   type="{{ $field['fieldType'] }}"
                                                                                   class="form-control agencyInput {{ $field['fieldDisabled'] }}"
                                                                                   data-toggle="tooltip"
                                                                                   name="{{ $field['fieldName'] }}"
                                                                                   value="{{ $field['fieldValue'] }}" {{ $field['fieldDisabled'] }}>
                                                                        @endif

                                                                        <div class="form-error" id="{{ $field['fieldError'] }}"></div>
                                                                    @endif
                                                                </td>
                                                            @endforeach
                                                        </tr>

                                                    @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endif


