<div class="data-block clearfix">

        <?php
        $nonDisabledFiled = (!empty($appInLaboratory) && $appInLaboratory) ? '' : 'not-disabled-field';
        ?>

        <div class="row loading-in-edit-mode hide text-center">
            <i class="fas fa-sync fa-spin" style="font-size: 35px;"></i>
        </div>
        <div class="row">
            @if(empty($laboratory) && !empty($safExpertiseEdit))
                <button class="btn btn-primary visible-buttons pull-right reset_form_edit_laboratory">{{ trans('core.base.label.new_data_expertise') }}</button>
                <br/>
            @endif
        </div>


            <div class="form-group clearfix">
                <label class="col-md-4 col-lg-3 control-label label__title">{{ trans('swis.my_application.laboratories.sample_code') }}</label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-lg-10 col-md-8 field__one-col">
                            <input type="text" class="form-control {{ $nonDisabledFiled }} saveParam" name="sample_code"
                                   value="{{ !empty($safExpertiseEdit) ? $safExpertiseEdit->sample_code : '' }}">
                            <input type="hidden" class="saveParam saf_number_field" name="saf_number"
                                   value="{{ !empty($safExpertiseEdit) ? $safExpertiseEdit->saf_number : $saf->regular_number }}">
                            <input type="hidden" class="saveParam application_id_field" name="sub_application_id"
                                   value="{{ (!empty($safExpertiseEdit) && !is_null($safExpertiseEdit->sub_application_id)) ? $safExpertiseEdit->sub_application_id : $applicationId }}">

                            <div class="form-error" id="form-error-sample_code"></div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="form-group clearfix">
                <label class="col-md-4 col-lg-3 control-label label__title">{{ trans('swis.my_application.laboratories.sample_weight') }}</label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-lg-10 col-md-8 field__one-col">
                            <input type="text" class="form-control {{ $nonDisabledFiled }} saveParam"
                                   name="sampling_weight"
                                   value="{{ !empty($safExpertiseEdit) ? $safExpertiseEdit->sampling_weight : '' }}">

                            <div class="form-error" id="form-error-sampling_weight"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group clearfix">
                <label class="col-md-4 col-lg-3 control-label label__title">{{ trans('swis.my_application.laboratories.goods') }}</label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-lg-10 col-md-8 field__one-col">
                            <select class="select2 {{!empty($safExpertiseEdit) ? 'select2_edit_view' : ''}} saveParam form-control {{ $nonDisabledFiled }}"
                                    name="saf_product_id" id="lab_goods"
                                    data-url="{{ urlWithLng('/user-documents/saf-products') }}">
                                @if (!empty($laboratoriesData) && !empty($laboratoriesData['reference']['products']))
                                    <option value=""></option>
                                    @foreach ($laboratoriesData['reference']['products'] as $product)
                                        <option value="{{ $product['id'] }}" {{ (!empty($safExpertiseEdit) && $safExpertiseEdit->saf_product_id == $laboratoriesData['selected_products']) ? 'selected' : '' }}>{{ $product['name'] }}</option>
                                    @endforeach
                                @endif
                            </select>

                            <div class="form-error" id="form-error-saf_product_id"></div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="form-group clearfix">
                <label class="col-md-4 col-lg-3 control-label label__title">{{ trans('swis.my_application.laboratories.expertising_method') }}</label>
                <div class="col-md-8">

                    <div class="row">
                        <div class="col-lg-10 col-md-8 field__one-col">
                            <select class="select2 {{!empty($safExpertiseEdit) ? 'select2_edit_view' : ''}} saveParam form-control {{ $nonDisabledFiled }} process_param"
                                    name="expertising_method_code" id="">
                                <option value="">{{trans('swis.my_application.laboratories.select')}}</option>
                                @if (!empty($laboratoriesData) && !empty($laboratoriesData['reference']['expertising_method']))
                                    @foreach ($laboratoriesData['reference']['expertising_method'] as $product)
                                        <option value="{{ $product['id'] }}" {{ (!empty($safExpertiseEdit) && $product['id'] == $safExpertiseEdit->expertising_method_code)? 'selected' : '' }}>{{ $product['name'] }}</option>
                                    @endforeach
                                @endif
                            </select>

                            <div class="form-error" id="form-error-expertising_method_code"></div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="form-group clearfix">
                <label class="col-md-4 col-lg-3 control-label label__title">{{ trans('swis.my_application.laboratories.expertising_indicator') }}</label>
                <div class="col-md-8">

                    <div class="row">
                        <div class="col-lg-10 col-md-8 field__one-col">
                            <select class="select2 {{!empty($safExpertiseEdit) ? 'select2_edit_view' : ''}} saveParam form-control {{ $nonDisabledFiled }} process_param"
                                    name="expertising_indicator_code" id="">
                                <option value="">{{trans('swis.my_application.laboratories.select')}}</option>
                                @if (!empty($laboratoriesData) && !empty($laboratoriesData['reference']['expertising_indicator']))
                                    @foreach ($laboratoriesData['reference']['expertising_indicator'] as $product)
                                        <option value="{{ $product['id'] }}" {{ (!empty($safExpertiseEdit) && $product['id'] == $safExpertiseEdit->expertising_indicator_code)? 'selected' : '' }}>{{ $product['name'] }}</option>
                                    @endforeach
                                @endif
                            </select>

                            <div class="form-error" id="form-error-expertising_indicator_code"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group clearfix">
                <label class="col-md-4 col-lg-3 control-label label__title">{{ trans('swis.my_application.laboratories.sample_method') }}</label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-lg-10 col-md-8 field__one-col">
                            <select class="select2 {{!empty($safExpertiseEdit) ? 'select2_edit_view' : ''}} saveParam form-control {{ $nonDisabledFiled }}"
                                    name="sampling_method_code" id="">
                                <option value="">{{trans('swis.my_application.laboratories.select')}}</option>
                                @if (!empty($laboratoriesData) && !empty($laboratoriesData['reference']['sampling_methods']))
                                    @foreach ($laboratoriesData['reference']['sampling_methods'] as $samplingMethod)
                                        <option value="{{ $samplingMethod['id'] }}" {{ (!empty($safExpertiseEdit) && $samplingMethod['id'] == $safExpertiseEdit->sampling_method_code)? 'selected' : '' }}>{{ $samplingMethod['name'] }}</option>
                                    @endforeach
                                @endif
                            </select>

                            <div class="form-error" id="form-error-sampling_method_code"></div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="form-group clearfix">
                <label class="col-md-4 col-lg-3 control-label label__title">{{ trans('swis.my_application.laboratories.sample_sampler') }}</label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-lg-10 col-md-8 field__one-col">
                            <select class="select2 {{!empty($safExpertiseEdit) ? 'select2_edit_view' : ''}} saveParam form-control {{ $nonDisabledFiled }}" name="sampler_code[]" multiple id="">
                                @if (!empty($laboratoriesData) && !empty($laboratoriesData['reference']['sampler']))
                                    @foreach ($laboratoriesData['reference']['sampler'] as $samplingMethod)
                                        <option value="{{ $samplingMethod['id'] }}" {{ (!empty($safExpertiseEdit) && in_array($samplingMethod['id'],$safExpertiseEdit->sampler_code))? 'selected' : '' }}>{{ $samplingMethod['name'] }}</option>
                                    @endforeach
                                @endif
                            </select>

                            <div class="form-error" id="form-error-sampler_code"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group clearfix">
                <label class="col-md-4 col-lg-3 control-label label__title">{{ trans('swis.my_application.laboratories.batches') }}</label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-lg-10 col-md-8 field__one-col">
                            <select class="select2 {{!empty($safExpertiseEdit) ? 'select2_edit_view' : ''}} saveParam form-control {{ $nonDisabledFiled }}"
                                    name="saf_product_batches_ids[]" id="lab_goods_batches" multiple="multiple">
                                @if (!empty($safExpertiseEdit) && !empty($laboratoriesData['reference']['products_batches']))
                                    @foreach ($laboratoriesData['reference']['products_batches'] as $productBatches)
                                        <option value="{{ $productBatches['id'] }}" {{ (!empty($safExpertiseEdit) && !empty($laboratoriesData['selected_products_batches']) && in_array($productBatches['id'], $laboratoriesData['selected_products_batches'])) ? 'selected' : '' }}>{{ trans($productBatches['batch_number']) }}</option>
                                    @endforeach
                                @endif
                            </select>

                            <div class="form-error" id="form-error-saf_product_batches_ids"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group clearfix">
                <label class="col-md-4 col-lg-3 control-label label__title">{{ trans('swis.my_application.laboratories.product_group') }}</label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-lg-10 col-md-8 field__one-col">
                            <select class="select2 {{!empty($safExpertiseEdit) ? 'select2_edit_view' : ''}} saveParam form-control {{ $nonDisabledFiled }} process_param"
                                    name="product_group_code" id="">
                                <option value="">{{trans('swis.my_application.laboratories.select')}}</option>
                                @if (!empty($laboratoriesData) && !empty($laboratoriesData['reference']['product_group']))
                                    @foreach ($laboratoriesData['reference']['product_group'] as $product)
                                        <option value="{{ $product['id'] }}" {{ (!empty($safExpertiseEdit) && $product['id'] == $safExpertiseEdit->product_group_code)? 'selected' : '' }}>{{ $product['name'] }}</option>
                                    @endforeach
                                @endif
                            </select>

                            <div class="form-error" id="form-error-product_group_code"></div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="form-group clearfix">
                <label class="col-md-4 col-lg-3 control-label label__title">{{ trans('swis.my_application.laboratories.product_sub_group') }}</label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-lg-10 col-md-8 field__one-col">
                            <select class="select2 {{!empty($safExpertiseEdit) ? 'select2_edit_view' : ''}} saveParam form-control {{ $nonDisabledFiled }} process_param"
                                    name="product_sub_group_code" id="">
                                <option value="">{{trans('swis.my_application.laboratories.select')}}</option>
                                @if (!empty($laboratoriesData) && !empty($laboratoriesData['reference']['product_sub_group']))
                                    @foreach ($laboratoriesData['reference']['product_sub_group'] as $product)
                                        <option value="{{ $product['id'] }}" {{ (!empty($safExpertiseEdit) && $product['id'] == $safExpertiseEdit->product_sub_group_code)? 'selected' : '' }}>{{ $product['name'] }}</option>
                                    @endforeach
                                @endif
                            </select>

                            <div class="form-error" id="form-error-product_sub_group_code"></div>
                        </div>
                    </div>
                </div>
            </div>



        <div class="row">
        @if(isset($appInLaboratory) && !$appInLaboratory || (!empty($safExpertiseEdit)))
            <button class="btn btn-success visible-buttons " id="processBtn"
                    data-url="{{ urlWithLng('/user-documents/process') }}">{{ trans('swis.my_application.laboratories.process') }} </button>
        @endif
        <hr/>
        </div>

        <div class="form-error" id="form-error-diff_labs"></div>

        <div class="row">
            <div class="text-center">
                <table class="table table-bordered laboratories_actions {{ !empty($safExpertiseEdit) ? '' : 'hide' }}">
                    <thead>
                    <tr>
                        <th>{{ trans('swis.my_application.laboratories.laboratory') }}</th>
                        <th>{{ trans('swis.my_application.laboratories.table.indicators') }}</th>
                        <th>{{ trans('swis.my_application.laboratories.table.limit') }}</th>
                        <th>{{ trans('swis.my_application.laboratories.table.measurement_unit') }}</th>
                        <th>{{ trans('swis.my_application.laboratories.table.price') . " (" . config('swis.default_currency') . ")" }}</th>
                        <th>{{ trans('swis.my_application.laboratories.table.duration') }}</th>
                        <th>{{ trans('swis.my_application.laboratories.table.hour-day') }}</th>
                        <th>{{ trans('core.base.label.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if (!empty($safExpertiseEdit) && !empty($laboratoriesData['indicators']))
                        @include('user-documents.old-laboratories.process-table', ['data' => $laboratoriesData['indicators'], 'editMode' => true])
                    @endif
                    </tbody>
                </table>
                <div class="loading_laboratories_actions hide"><i class="fas fa-sync fa-spin"
                                                                  style="font-size: 35px;"></i></div>
            </div>
        </div>

        <div class="row">
            <button class="btn btn-primary visible-buttons pull-right save_laboratory_indicators hide"
                    data-url="{{ !empty($safExpertiseEdit) ? urlWithLng('/user-documents/update-indicators/'.$safExpertiseEdit->id) : urlWithLng('/user-documents/store-indicators') }}">{{ trans('core.img.uploader.crop_save_btn') }}</button>
            <div class="form-group clearfix m-b-none">
                <div class="col-md-3 text-left">
                    <label>
                        {{ trans('swis.my_application.laboratories.total_amount') . " (" . config('swis.default_currency') . ")" }}
                    </label>
                    <input type="text" name="total_price" id="totalPrice" class="form-control col-md-3 saveParam"
                           value="{{ !empty($safExpertiseEdit) ? $safExpertiseEdit->total_price : 0  }}"
                           placeholder="{{ trans('swis.my_application.laboratories.total_amount') }}"
                           readonly="readonly">
                </div>
            </div>
        </div>
</div>