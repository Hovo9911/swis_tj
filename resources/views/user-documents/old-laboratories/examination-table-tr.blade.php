<?php use App\Models\ReferenceTable\ReferenceTable; ?>

@if(!empty($renderInput))
    @if(empty($examination))
    <tr data-num="{{$num}}" class="{{!empty($examination) ? 'examination-'.$examination->id : '' }}">
    @endif
        <?php
           foreach ($subAppProducts as $product) {
               if (!empty($examination) && $examination->saf_product_id == $product->id) {
                    $measurementUnit = optional(getReferenceRows(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $product->measurement_unit, false, ['code', 'name']))->name;
               }
           }
        ?>
        <td><input type="hidden" class="doc-id" value="">{{$num}}</td>
        <td>
            <select class="form-control select2" name="saf_product_id" id="saf_product_id">
                <option value=""></option>
                @if (!empty($subAppProducts))
                    @foreach ($subAppProducts as $product)
                        <option value="{{ $product->id }}" {{ (!empty($examination) && $examination->saf_product_id == $product->id) ? 'selected' : '' }}
                                data-unit="{{$measurementUnit ?? optional(getReferenceRows(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $product->measurement_unit, false, ['code', 'name']))->name}}">{{$productsIdNumbers[$product->id].' - '. $product->commercial_description }}</option>
                    @endforeach
                @endif
            </select>
            <div class="form-error" id="form-error-saf_product_id-{{$num}}"></div>
        </td>
        <td>
            <select class="form-control select2" name="type_of_good">
                <option value=""></option>
                @if (!empty($typesOfProductsRef))
                    @foreach ($typesOfProductsRef as $typeOfProduct)
                        <option value="{{ $typeOfProduct->id }}" {{ (!empty($examination) && $examination->type_of_good == $typeOfProduct->id) ? 'selected' : '' }}>{{$typeOfProduct->code.', '. $typeOfProduct->name }}</option>
                    @endforeach
                @endif
            </select>
            <div class="form-error" id="form-error-type_of_good-{{$num}}"></div>
        </td>
        <td>
            <select class="form-control select2" name="type_of_metal">
                <option value=""></option>
                @if (!empty($typeOfMetalRef))
                    @foreach ($typeOfMetalRef as $typeOfMetal)
                        <option value="{{ $typeOfMetal->id }}" {{ (!empty($examination) && $examination->type_of_metal == $typeOfMetal->id) ? 'selected' : '' }}>{{$typeOfMetal->code.', '. $typeOfMetal->name }}</option>
                    @endforeach
                @endif
            </select>
            <div class="form-error" id="form-error-type_of_metal-{{$num}}"></div>
        </td>
        <td><input class="form-control" name="quantity" value="{{ !empty($examination) ? $examination->quantity : '' }}">
            <div class="form-error" id="form-error-quantity-{{$num}}"></div>
        </td>
        <td>
            <input class="form-control" name="weight" value="{{ !empty($examination) ? $examination->weight : '' }}">
            <div class="form-error" id="form-error-weight-{{$num}}"></div>

        </td>
        <td><input type="text" class="form-control measurement_unit" name="measurement_unit" readonly="readonly" value="{{ !empty($examination) ? $measurementUnit : '' }}">
            <div class="form-error" id="form-error-measurement_unit-{{$num}}"></div>

        </td>
        <td>
            <?php
                $selectedTypeOfStone = [];
                if (!empty($examination)) {
                    $selectedTypeOfStone = explode(",", $examination->type_of_stone);
                }
            ?>
            <select multiple="multiple" class="form-control select2" name="type_of_stone[]" id="typeOfStones"
                    data-allow-clear="false">
                <option value=""></option>
                @if (!empty($typeOfStonesRef))
                    @foreach ($typeOfStonesRef as $typeOfStone)
                        <option value="{{ $typeOfStone->id }}" {{ in_array($typeOfStone->id, $selectedTypeOfStone) ? 'selected' : '' }}>{{$typeOfStone->code.' - '. $typeOfStone->name }}</option>
                    @endforeach
                @endif
            </select>
            <div class="form-error" id="form-error-type_of_stone-{{$num}}"></div>
        </td>
        <td>
            <input type="number" min="0" class="form-control onlyNumbers" name="piece" value="{{ !empty($examination) ? $examination->piece : '' }}">
            <div class="form-error" id="form-error-piece-{{$num}}"></div>

        </td>
        <td><input type="number" min="0" class="form-control onlyNumbers" name="carat" value="{{ !empty($examination) ? $examination->carat : '' }}">
            <div class="form-error" id="form-error-carat-{{$num}}"></div>

        </td>
        <td class="actions-list text-nowrap">
            @if (!empty($examination))
                <button type="button" class="btn btn-primary btn-xs updateExamination btn--icon" data-toggle="tooltip" data-num="{{ $num }}" data-examination-id="{{$examination->id}}"
                        title="{{trans('swis.base.tooltip.update_document.button')}}"><i class="fa fa-check"></i></button>
                <button type="button" class="delete delete-examination" data-toggle="tooltip" data-num="{{ $num }}" data-examination-id="{{$examination->id}}"
                        title="{{trans('swis.base.tooltip.delete_document.button')}}"><i class="fas fa-times"></i></button>
            @else
                <button type="button" class="btn btn-primary btn-xs storeExamination btn--icon" data-toggle="tooltip"
                    title="{{trans('swis.base.tooltip.store_document.button')}}"><i class="fa fa-check"></i></button>
                <button type="button" class="delete dt-examination" data-toggle="tooltip"
                        title="{{trans('swis.base.tooltip.delete_document.button')}}"><i class="fas fa-times"></i></button>
            @endif
{{--            <button class="btn-remove delete-examination" data-toggle="tooltip"--}}
{{--                    title="{{trans('swis.base.tooltip.delete_examination.button')}}"><i class="fas fa-times"></i></button>--}}
        </td>
        @if(!empty($examination))
        <input type="hidden" name="examinationId" value="{{$examination->id}}">
        @endif
    @if(empty($examination))
    </tr>
    @endif

@endif