<div class="panel-group" id="accordion">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" class="collapseLabCreate" data-parent="#accordion" href="#collapseLabCreate">{{ trans('swis.my_application.laboratories.create_label') }} <i class="fas fa-chevron-down"></i></a>
            </h4>
        </div>
        <div id="collapseLabCreate" class="panel-collapse collapse in">
            <div class="panel-body createEditBlock">
                @include('user-documents.old-laboratories.laboratories-edit')
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>{{ trans('swis.my_application.laboratories.sample_code') }}</th>
                        <th>{{ trans('swis.my_application.laboratories.letter_code') }}</th>
                        <th>{{ trans('swis.my_application.laboratories.alltogether_price') }}</th>
                        <th>{{ trans('swis.my_application.laboratories.currency') }}</th>
                        <th>{{ trans('swis.my_application.laboratories.alltogether_duration') }}</th>
                        <th>{{ trans('swis.my_application.laboratories.table.hour-day') }}</th>
                        <th>{{ trans('swis.my_application.laboratories.table.indicators') }}</th>
                        <th>{{ trans('swis.my_application.laboratories.laboratory') }}</th>
                        @if (!empty($laboratoriesData['has_lab_info']))
                            <th>{{ trans('swis.my_application.laboratories.table.actual_result') }}</th>
                            <th>{{ trans('swis.my_application.laboratories.table.adequacy') }}</th>
                        @endif
                        @if (isset($appInLaboratory) && !$appInLaboratory)
                            <th>{{ trans('core.base.label.actions') }}</th>
                        @endif
                    </tr>
                </thead>
            <tbody>
                @if (!empty($laboratoriesData['list_of_view']))
                    <?php $total = 0; ?>
                    @foreach ($laboratoriesData['list_of_view'] as $listItem)
                        <?php
                            $total += $listItem->total_price or 0;
                            $colCount = 5;
                            $colCount += !empty($laboratoriesData['has_lab_info']) ? 2 : 0;
                            $colCount += (isset($appInLaboratory) && !$appInLaboratory) ? 1 : 0;
                        ?>
                        <tr>
                            <td>{{ $listItem->sample_code  }}</td>
                            <td>{{ $listItem->letter_code  }}</td>
                            <td>{{ $listItem->total_price or 0  }}</td>
                            <td>{{ config('swis.default_currency') }}</td>
                            <td>{{ $listItem->duration  }}</td>
                            <td>{{ $listItem->date_time  }}</td>
                            <td>{{ $listItem->indicators  }}</td>
                            <td>{{ $listItem->lab_name  }}</td>
                            @if (!empty($laboratoriesData['has_lab_info']))
                                <td>{{@$listItem->actual_result}}</td>
                                <td>{{@$listItem->adequacy}}</td>
                            @endif
                            @if (isset($appInLaboratory) && !$appInLaboratory)
                                <td>
                                    @if(!$listItem->status_send)
                                        <button class="btn btn-edit visible-buttons editLabIndicator"
                                                data-url="{{ urlWithLng('/user-documents/edit-indicators/'.$applicationId.'/'.$documentId.'/'.$listItem->id.'/') }}"
                                                data-id="{{ $listItem->id }}" data-toggle="tooltip"
                                                title="{{ trans('core.base.action.edit') }}"><i class="fa fa-pen" data-toggle="tooltip"></i>
                                        </button>
                                    @endif
                                    <button class="btn btn-print visible-buttons" data-id="{{ $listItem->id }}" data-toggle="tooltip"  title="{{ trans('swis.base.tooltip.print.button') }}"><i class="fas fa-print"></i></button>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="3" align="right" style="padding-right: 25px;">{{ $total }}</td>
                        <td colspan="{{ $colCount ?? 0 }}"></td>
                    </tr>
                @endif
            </tbody>
        </table>
        </div>
    </div>
</div>