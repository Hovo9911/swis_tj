@if (count($data))
    @foreach($data as $item)
        <tr>
            <td>{{ @$item->lab_name }}</td>
            <td>{{ $item->name }} <input type="hidden" name="lab_id[]" /></td>
            <td>{{ @$item->limit }}</td>
            <td>{{ $item->measurement_unit }}</td>
            <td class="price_td">{{ $item->indicator_price }}</td>
            <td>{{ $item->laboratory_indicator_duration or $item->duration }}</td>
            <td>{{ $item->date_time or 'hours' }}</td>
            <td><input type="checkbox" class="saveParam checkIndicator" {{ !empty($editMode) ? 'checked' : '' }} data-lab="{{$item->lab_id}}" name="expertising_actions[{{ $item->code }}]"></td>
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="8" class="text-center">{{trans('swis.data_table.sEmptyTable')}}</td>
    </tr>
@endif