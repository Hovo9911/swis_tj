<div class="panel-panel panel-default">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default" id="examinationAddExaminationsList">
                <div class="panel-heading">
                    {{trans('swis.user_documents.examination.examination_info')}}
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered examination-table" id="examinationList">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{{trans('swis.user_documents.examination.product')}}</th>
                                <th>{{trans('swis.user_documents.examination.product_type')}}</th>
                                <th>{{trans('swis.user_documents.examination.metal_type')}}</th>
                                <th>{{trans('swis.user_documents.examination.quantity')}}</th>
                                <th>{{trans('swis.user_documents.examination.weight')}}</th>
                                <th>{{trans('swis.user_documents.examination.measurement_unit')}}</th>
                                <th>{{trans('swis.user_documents.examination.stone_type')}}</th>
                                <th>{{trans('swis.user_documents.examination.piece')}}</th>
                                <th>{{trans('swis.user_documents.examination.carat')}}</th>
                                <th>{{trans('swis.user_documents.examination.actions')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($attachedDocumentExaminations) > 0)
                                <?php $num = $num ?? 1;?>

                                <?php $productNumbers = $subApplication->productsOrderedList(); ?>

                                @foreach($attachedDocumentExaminations as $attachedDocumentExamination)
                                    <?php
                                    $typeOfStoneInfo = [];
                                    $typeOfStones = explode(',', $attachedDocumentExamination->type_of_stone);
                                    $typeOfGood = getReferenceRows(App\Models\ReferenceTable\ReferenceTable::REFERENCE_TYPES_OF_PRODUCTS, $attachedDocumentExamination->type_of_good) ?? '';
                                    $typeOfMetal = getReferenceRows(App\Models\ReferenceTable\ReferenceTable::REFERENCE_TYPE_OF_METAL, $attachedDocumentExamination->type_of_metal) ?? '';
                                    foreach ($typeOfStones as $typeOfStone) {
                                        $typeOfStoneRef = getReferenceRows(App\Models\ReferenceTable\ReferenceTable::REFERENCE_TYPE_OF_STONES, $typeOfStone) ?? '';
                                        $typeOfStoneInfo[] = optional($typeOfStoneRef)->code . ' - ' . optional($typeOfStoneRef)->name;
                                    }
                                    ?>
                                    <tr data-num="{{$num}}" class="examination-{{$attachedDocumentExamination->id}}">
                                        <td><input type="hidden">{{$num}}</td>
                                        <td>{{$productNumbers[$attachedDocumentExamination->saf_product_id].', '.$attachedDocumentExamination->commercial_description}}</td>
                                        <td>{{$typeOfGood->code.', '.$typeOfGood->name}}</td>
                                        <td>{{$typeOfMetal->code.', '.$typeOfMetal->name}}</td>
                                        <td>{{$attachedDocumentExamination->quantity}}</td>
                                        <td>{{$attachedDocumentExamination->weight}}</td>
                                        <td>{{optional(getReferenceRows(App\Models\ReferenceTable\ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $attachedDocumentExamination->measurement_unit))->name}}</td>
                                        <td>{!! implode(', ',$typeOfStoneInfo) !!}</td>
                                        <td>{{$attachedDocumentExamination->piece}}</td>
                                        <td>{{$attachedDocumentExamination->carat}}</td>
                                        <td>
                                            @if (optional($superscribe)->to_user == Auth::id())
                                                <button class="btn-edit edit-examination" data-toggle="tooltip" data-num="{{ $num }}" data-examination-id="{{$attachedDocumentExamination->id}}"
                                                        title="{{trans('swis.base.tooltip.edit_examination.button')}}"><i class="fas fa-pen"></i></button>
                                                <button class="btn-remove delete-examination" data-toggle="tooltip" data-examination-id="{{$attachedDocumentExamination->id}}"
                                                        title="{{trans('swis.base.tooltip.delete_examination.button')}}"><i class="fas fa-times"></i></button>
                                            @endif
                                        </td>
                                    </tr>
                                    <?php $num++ ?>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    @if(!is_null($subApplication->data)  && optional($superscribe)->to_user == Auth::id())
                        <button type="button" class="btn btn-primary plusMultipleFieldsExamination"><i class="fa fa-plus"></i>
                            <span class="documents-plusBtn__text">{{trans('swis.user_documents.examination.add_button')}}</span>
                        </button>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
