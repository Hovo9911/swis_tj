<div class="panel-body">

    <div class="clearfix search__out">
        <div class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse"
             data-target="#documentSearchFilter" aria-expanded="false" aria-controls="collapseSearch">
            <i class="fa fa-search"> </i><span
                    class="search__txt ver-top-box">{{ trans('swis.document.search.title') }}</span>
        </div>
    </div>

    <div class="collapse" id="documentSearchFilter">

        <div class="form-group">
            <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.document.access_password')}}</label>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-lg-10 col-md-8 field__one-col">
                        <input value="" name="document_access_password" type="text" placeholder=""
                               class="form-control document-search-inputs">
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.document.type')}}</label>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-lg-10 col-md-8 field__one-col">
                        <select class="form-control select2-ajax document-search-inputs"
                                id="searchDocumentType"
                                name="document_type"
                                data-show-title="false"
                                data-url="{{urlWithLng('/reference-table/get-document-type')}}"
                                data-min-input-length="1">
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.description')}}</label>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-lg-10 col-md-8 field__one-col">
                        <input name="document_description" placeholder="" class="form-control document-search-inputs">
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.document.number')}}</label>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-lg-10 col-md-8 field__one-col">
                        <input value="" name="document_number" type="text" placeholder=""
                               class="form-control document-search-inputs">
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.document.release_date')}}</label>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-4 col-lg-5 field__two-col">
                        <div class='input-group date'>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            <input value="" name="document_release_date_start" autocomplete="off"
                                   type="text"
                                   placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-5 field__two-col">
                        <div class='input-group date'>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            <input value="" name="document_release_date_end" autocomplete="off"
                                   type="text"
                                   placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.folder_name')}}</label>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-lg-10 col-md-8 field__one-col">
                        <input value="" name="search_folder_name"
                               type="text"
                               placeholder="" class="form-control document-search-inputs">
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.folder_desc')}}</label>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-lg-10 col-md-8 field__one-col">
                        <input placeholder="" name="search_folder_desc" class="form-control document-search-inputs">
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" name="doc_status" value="true">
        <input value="application" name="moduleForDocuments" class="no-reset" type="hidden">
        <input value="{{$subApplication->id or 0}}" name="sub_application_id" class="no-reset" type="hidden">

        <div class="form-group">
            <div class="col-md-offset-4 col-md-8 col-lg-offset-3 text-right">
                <div class="row">
                    <div class="col-lg-10 col-md-8">
                        <button type="button" class="btn btn-primary documentSearch mr-10">{{trans('swis.single-application.docments.label.search')}}</button>
                        <button type="button" data-search-button="documentSearch"
                                class="btn btn-danger resetButton">{{trans('core.base.label.reset')}}</button>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row searchDoc hide">
        <br/>
        <div class="col-md-12">
            <div class="panel panel-default" id="documentSearchResult">
                <div class="panel-heading">
                    {{trans('swis.user_documents.documents.search_result')}}
                </div>
                <div class="panel-body">

                    <table class="table table-hover table-bordered"
                           id="search-document">
                        <thead>
                        <tr>
                            <th data-ordering="false" data-col="document_file"></th>
                            <th data-col="check" class="th-checkbox"><input type="checkbox"></th>
                            <th data-col="id">ID</th>
                            <th data-col="document_form">{{trans('swis.document.document_form')}}</th>
                            <th data-col="document_access_password">{{trans('swis.document.access_password')}}</th>
                            <th data-col="document_type">{{trans('swis.document.type')}}</th>
                            <th data-col="document_description">{{trans('core.base.description')}}</th>
                            <th data-col="document_number">{{trans('swis.document.number')}}</th>
                            <th data-col="document_release_date">{{trans('swis.document.release_date')}}</th>
                        </tr>
                        </thead>
                    </table>

                    <div class="text-center">
                        <button class="btn btn-primary addDocument"><i class="fa fa-plus"></i> <span
                                    class="documents-plusBtn__text">{{trans('swis.folder.document.add_document')}}</span>
                        </button>
                        <button type="button" class="btn btn-secondary closeResultTab" data-div="searchDoc"
                                data-type="class">{{trans('core.base.button.close.search_result')}}</button>
                    </div>

                    <div id="documentErrorInfo"></div>
                </div>
            </div>
        </div>

    </div>

    <?php
    $attachedDocumentProductsCount = isset($attachedDocumentProducts) ? $attachedDocumentProducts->count() : 0;
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default" id="documentAddProductsList">
                <div class="panel-heading">
                    {{trans('swis.user_documents.documents.document_info')}}
                </div>
                <div class="panel-body">
                    <div class="table-responsive {{!$attachedDocumentProductsCount ? 'hide' : ''}}">
                        <table class="table  table-hover table-bordered " id="documentList">
                            <thead>
                            <tr>
                                <th style="width: 30px" class="text-center"><input id="checkAllDocuments" type="checkbox"></th>
                                <th style="width: 30px" class="text-center">#</th>
                                <th style="width: 80px">{{trans('swis.document.document_form')}}</th>
                                <th style="width: 100px">{{trans('swis.document.type')}}</th>
                                <th style="width: 270px">{{trans('core.base.label.name')}}</th>
                                <th style="width: 100px">{{trans('core.base.description')}}</th>
                                <th style="width: 80px">{{trans('swis.document.number')}}</th>
                                <th style="width: 110px">{{trans('swis.document.release_date')}}</th>
                                <th style="width: 100px">{{trans('core.base.label.type.file')}}</th>
                                <th style="width: 100px">{{trans('swis.document.product_number')}}</th>
                                <th style="width: 72px;">{{trans('core.base.label.actions')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($attachedDocumentProductsCount)
                                @include('user-documents.documents.document-table-tr',['attachedDocumentProducts'=>$attachedDocumentProducts])
                            @endif
                            </tbody>
                        </table>
                    </div>

                    <button type="button" class="btn btn-danger m-r {{!$attachedDocumentProductsCount ? 'hide' : ''}}" id="deleteCheckedDocuments"><i class="fa fa-times"></i> {{trans('swis.user_documents.documents.delete.multiple.button')}}</button>

                    <button type="button" class="btn btn-primary plusMultipleFieldsDocument">
                        <i class="fa fa-plus"></i> <span class="documents-plusBtn__text">{{trans('swis.user_documents.documents.add_document_button')}}</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="documentProductsModal" class="modal fade subFormModal" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal__title fb">{{trans('swis.user_documents.documents.modal.title')}}</h3>
                <button type="button" class="close modal__close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="document-products-list"></div>
                <button type="button" class="btn btn-success add-products" id="addProductToDocument">
                    <span class="documents-plusBtn__text">
                            {{trans('swis.user_documents.documents.modal.add_product_button')}}
                   </span>

                </button>
            </div>
        </div>
    </div>
</div>