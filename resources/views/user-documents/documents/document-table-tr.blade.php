@if(empty($renderInput))

    @if(count($attachedDocumentProducts) > 0)
        <?php $num = $num ?? 1 ?>

        @foreach($attachedDocumentProducts as $attachedDocumentProduct)
            <?php
            $document = is_null($attachedDocumentProduct->document) ? $attachedDocumentProduct : $attachedDocumentProduct->document;
            $canDeleteDocument = (\Auth::user()->isLocalAdmin() || $document->user_id == \Auth::id()) && $document->company_tax_id == \Auth::user()->companyTaxId() && $attachedDocumentProduct->added_from_module == \App\Models\Document\Document::DOCUMENT_CREATE_MODULE_APPLICATION || (!empty($addFromDocList) && $addFromDocList)
            ?>

            <tr data-num="{{$num}}">

                <td class="text-center">@if($canDeleteDocument)<input type="checkbox" value="{{$document->id}}">@endif</td>
                <td class="text-center"><input type="hidden" name="document_id" class="document" value="{{$document->id}}">{{$num}}</td>
                <td>{{trans('swis.document.document_form.'.$document->document_form.'.title')}}</td>
                <td>{{$document->document_type}}</td>
                <td class="text-break-word">{{$document->document_name}}</td>
                <td>{{$document->document_description}}</td>
                <td>{{$document->document_number}}</td>
                <td>{{$document->document_release_date}}</td>
                <td class="text-break-word">{{$document->fileBaseName()}}</td>
                <td>
                    <span class="product-ids-list text-break-word">{{!empty($availableProductsIdNumbers) ? $attachedDocumentProduct->productNumbers($availableProductsIdNumbers) : ''}}</span>

                    @if($attachedDocumentProduct->added_from_module == \App\Models\Document\Document::DOCUMENT_CREATE_MODULE_APPLICATION || (!empty($addFromDocList) && $addFromDocList))
                        <input class="product-ids" value="{{!empty($availableProductsIdNumbers) ? $attachedDocumentProduct->productIdList() : ''}}" name="products" type="hidden">
                        <button class="btn btn-success pull-right btn-xs addProductsDocument" data-update="true" data-document="{{$document->id}}" data-toggle="tooltip"  title="{{trans('swis.base.tooltip.add_product.button')}}"><i class="fa fa-plus"></i></button>
                    @endif
                </td>
                <td class="text-nowrap text-center">

                    <a target="_blank" href="{{$document->filePath()}}" class="btn btn-success btn-xs btn--icon" title="{{ trans('swis.base.tooltip.view.button') }}" data-toggle="tooltip"><i class="fas fa-eye"></i></a>

                    @if($canDeleteDocument)
                        <button class="delete dt-document" data-toggle="tooltip"  title="{{trans('swis.base.tooltip.delete_document.button')}}"><i class="fas fa-times"></i></button>
                    @endif
                </td>

            </tr>
            <?php $num++ ?>
        @endforeach

    @endif

@else
    <?php
    $gUploader = new \App\GUploader('document_report');
    ?>
    <tr data-num="{{$num}}">
        <td class="text-center"><input type="checkbox" value="" class="hidden doc-checkbox"></td>
        <td><input type="hidden" name="document_id" class="document" value="">{{$num}}</td>
        <td class="document-form"></td>
        <td>
            <input type="text" autocomplete="off" class="autocomplete form-control"
                   data-ref="{{\App\Models\ReferenceTable\ReferenceTable::getReferenceIdByTableName(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS)}}"
                   data-ref-type="autocomplete"
                   data-fields="{{json_encode([['from'=>'name','to'=>'document_name'],['from'=>'code','to'=>'document_type']])}}"
                   placeholder="autocomplete"
                   name="document_type">

            <input type="hidden" name="document_type_value" value="{{$document->document_type_value or ''}}">
            <div class="form-error" id="form-error-document_type-{{$num}}"></div>
        </td>
        <td>
            <input type="text" class="form-control" name="document_name" readonly="readonly">
            <div class="form-error" id="form-error-document_name-{{$num}}"></div>
        </td>
        <td>
            <input type="text" class="form-control" name="document_description">
            <div class="form-error" id="form-error-document_description-{{$num}}"></div>
        </td>
        <td>
            <input type="text" class="form-control" name="document_number">
            <div class="form-error" id="form-error-document_number-{{$num}}"></div>
        </td>
        <td>
            <div class='input-group date'><span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            <input type="text" class="form-control" placeholder="{{setDatePlaceholder()}}" autocomplete="off" name="document_release_date">
            </div>
            <div class="form-error" id="form-error-document_release_date-{{$num}}"></div>
        </td>
        <td class="text-break-word">
            <div class="g-upload-container">
                <div class="g-upload-box">
                    <label style="width:35px"><i class="fas fa-upload btn btn-success btn-sm"></i><input style="visibility: hidden" class="dn g-upload" type="file" name="document_file" {{$gUploader->isMultiple() ? 'multiple' : ''}} data-conf="document_report" data-action="{{urlWithLng('/g-uploader/upload')}}" accept="{{$gUploader->getAcceptTypes()}}"/></label>
                </div>

                <div class="license-form__uploaded-file-list g-uploaded-list "></div>
                {{-- Progress bar for file uploding process, if you want to show process--}}
                <progress class="progressBarFileUploader" value="0" max="100"></progress>
                {{-- Progress bar for file uploding process --}}
                <div class="form-error-text form-error-files fb fs12"></div>

                <div class="form-error" id="form-error-document_file-{{$num}}"></div>
            </div>
        </td>
        <td>
            <span class="product-ids-list"></span>

            <input class="product-ids" name="document_products" type="hidden">

            <button class="btn btn-success pull-right btn-xs addProductsDocument" data-toggle="tooltip"  title="{{trans('swis.base.tooltip.add_product.button')}}"><i class="fa fa-plus"></i></button>
        </td>
        <td class="actions-list text-nowrap text-center">
            <button type="button" class="btn btn-primary btn-xs storeDocument btn--icon" data-toggle="tooltip"  title="{{trans('swis.base.tooltip.store_document.button')}}"><i class="fa fa-check"></i></button>
            <button class="delete dt-document" data-toggle="tooltip"  title="{{trans('swis.base.tooltip.delete_document.button')}}"><i class="fas fa-times"></i></button>
        </td>
    </tr>

@endif