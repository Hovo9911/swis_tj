<table class="table table-bordered">
    <thead>
    <tr>
        <th width="30px"><input type="checkbox" id="check-all-document-add-products"></th>
        <th>{{trans('swis.single_app.product_number')}}</th>
        <th>{{trans('swis.single_app.product_code')}}</th>
        <th>{{trans('swis.single_app.commercial_description')}}</th>
    </tr>
    </thead>
    <tbody>
        @foreach($products as $value)
            <?php
            $hsCode = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_HS_CODE_6,$value->product_code);
            ?>
            <tr>
                <td><input data-pr-number="{{$availableProductsIdNumbers[$value->id]}}"
                     class="pr" value="{{$value->id}}" type="checkbox"></td>
                <td>{{$availableProductsIdNumbers[$value->id]}}</td>
                <td><span data-toggle="tooltip" data-placement="top" title="{{$value->product_description}}">{{$hsCode->code ?? ''}}</span></td>
                <td>{{$value->commercial_description}}</td>
            </tr>
        @endforeach
    </tbody>
</table>


