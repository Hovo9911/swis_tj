@extends('layouts.app')

@section('title'){{$documentName}}@stop

@section('contentTitle'){{$documentName}}@stop

@section('form-buttons-top')
    <div class="row">
        <div class="col-md-10 text-right no-padding">
            @if($saveMode != 'view' && isset($superscribe) && !is_null($superscribe))
                @if(!is_null($userSuperscribeMapping))
                    <form action="{{urlWithLng("/user-documents/{$documentId}/application/{$applicationId}/superscribe/delete")}}" method="post" id="superscribeUserDelete">
                        <input type="hidden" name="start_state_id" value="{{$userSuperscribeMapping->start_state}}">
                        <input type="hidden" name="end_state_id" value="{{$userSuperscribeMapping->end_state}}">
                        <input type="hidden" name="last_update" value="{{$documentCurrentState->getOriginal('created_at') }}">

                        <b class="superscribe-username">{{\Auth::user()->name($superscribe->to_user)}}</b>

                        <button type="button" id="deleteSuperscribe" class="btn btn-sm btn-danger m-l btn-icon--delete"><i class='fa fa-times mr--8'></i>{{trans('swis.user_documents.remove_superscribing.button')}}</button>
                    </form>
                @else
                    <p style="padding-top: 8px;"><b class="superscribe-username">{{\Auth::user()->name($superscribe->to_user)}}</b></p>
                @endif
            @endif
        </div>

        <div class="col-md-2">
            <a href="{{urlWithLng("/user-documents/{$documentId}")}}" class="btn btn-default mc-nav-button-cancel">{{trans('swis.my_applications.close.button')}}</a>
        </div>
    </div>
@stop

@section('topScripts')
    <script>
        var safRegime = '{{ $saf->regime }}';
        var subApplicationType = '{{$subApplication->current_status or ''}}';
        var labStoreUrl = "{{ urlWithLng('/user-documents/store-indicators') }}";
        var appInLaboratory = '{!! $appInLaboratory !!}';
        var defaultCurrency = "{{ config('swis.default_currency') }}";
        var hasLaboratoryState = "{{ $hasLaboratoryState or '' }}";
    </script>

    <?php
    $jsTrans->addTrans([
        "swis.saf.lab_examination.select_product_placeholder",
        "swis.saf.lab_examination.no_results_found_products",
        "swis.saf.lab_examination.select_laboratory_placeholder",
        "swis.saf.lab_examination.no_results_found_laboratories",
        "swis.saf.lab_examination.select_measurement_unit_placeholder",
        "swis.saf.lab_examination.select_indicator_placeholder",
        "swis.saf.lab_examination.no_results_found_indicators",
        'swis.base.tooltip.edit_examination.button',
        'swis.base.tooltip.delete_examination.button',
        'swis.user-documents-validation-error',
        'swis.user-documents.lab_examinations.correspond.yes',
        'swis.user-documents.lab_examinations.correspond.no',
    ]);
    ?>
@endsection

@section('content')

    {{-- Modal Text Translations --}}
    @include('partials.translations',['transType'=>'modal'])

    @if($appInLaboratory)
        <h2>{{trans('swis.user_documents.application_in_laboratory')}}</h2>
    @endif

    <?php
    $printAllProductsData = [];
    foreach ($availableProductsIdNumbers as $productId => $item) {
        $printAllProductsData[] = "{$productId}-{$item}";
    }
    $printAllProductsData = json_encode($printAllProductsData);
    ?>

    {{-- Signature js --}}
    @if (config('global.digital_signature'))
        <script src="{{asset('js/rutoken/dependencies.js')}}"></script>
        <script src="{{asset('js/rutoken/crypto.js')}}"></script>
    @endif

    <div class="loawithoutaction-userName--out ding-close constructor-buttons">

        @if($showUserMappingList && $saveMode != 'view')
            <div id="document-actions-block" data-action='{{urlWithLng("/user-documents/{$documentId}/application/{$applicationId}/change-state")}}'>
                @php ($showDefaultPrint = false)
                @foreach($documentMappings as $mapping)
                    @if($mapping->rAction->special_type != \App\Models\ConstructorActions\ConstructorActions::SUPERSCRIBE_TYPE && $mapping->rAction->special_type != \App\Models\ConstructorActions\ConstructorActions::PRINT_TYPE)
                        <button type="button" class="btn btn-primary btn-state-change mr-10"
                                data-state="{{ $mapping->end_state }}" data-action="{{ $mapping->action }}"
                                data-mapping-id="{{ $mapping->id }}"
                                id="mappingButton{{$mapping->id}}-{{$mapping->action}}-{{$mapping->end_state}}">
                            {{ $mapping->rAction->currentMl->action_name }}
                        </button>
                    @elseif ($mapping->rAction->special_type != \App\Models\ConstructorActions\ConstructorActions::SUPERSCRIBE_TYPE && $mapping->rAction->special_type == \App\Models\ConstructorActions\ConstructorActions::PRINT_TYPE)
                        @if (!$showDefaultPrint && $mapping->rAction->print_default)
                            @php ($showDefaultPrint = true)
                        @endif
                    @endif
                @endforeach

                @php($isPrintButtonExists = false)
                @php($isPrintAllProductsButtonExists = false)
                @foreach ($documentMappings as $mapping)
                    @if($mapping->rAction->special_type != \App\Models\ConstructorActions\ConstructorActions::SUPERSCRIBE_TYPE && $mapping->rAction->special_type == \App\Models\ConstructorActions\ConstructorActions::PRINT_TYPE)
                        @if ($mapping->rAction->select_all_products)
                            @if (!$isPrintAllProductsButtonExists)
                                <div class="btn-group">
                                    @if (count($printActionOption) > 1)
                                        <a type="button" class="btn btn-primary dropdown-toggle mr--8"
                                           data-toggle="dropdown"
                                           aria-haspopup="true" aria-expanded="false"> <i
                                                    class="fa fa-download"></i>&nbsp;&nbsp;{{trans('swis.user_documents.download_pdf.title')}}
                                        </a>
                                        <div class="dropdown-menu print-forms" >
                                            @foreach ($printActionOption as $print => $template)
                                                @if (isset($template))

                                                    <a class="dropdown-print-forms printWithAllProductsFormButton" data-products="{{$printAllProductsData}}"
                                                       data-id="{{$template}}">
                                                        <i class="fa fa-download" target="_blank"></i>&nbsp;&nbsp;{{ $print }}</a>
                                                @else
                                                    @if ($showDefaultPrint)
                                                        <a class="dropdown-print-forms printWithAllProductsFormButton" data-id="" data-products="{{$printAllProductsData}}">
                                                            <i class="fa fa-download"></i>&nbsp;&nbsp;{{ $print }}</a>
                                                    @endif
                                                @endif
                                            @endforeach
                                        </div>
                                    @else
                                        <a class="btn btn-primary mr-10 printWithAllProductsFormButton" data-id=""  data-products="{{$printAllProductsData}}">
                                            <i class="fa fa-download"></i>&nbsp;&nbsp;{{trans('swis.user_documents.download_pdf.title')}}
                                        </a>
                                    @endif
                                </div>
                                @php($isPrintAllProductsButtonExists = true)
                            @endif
                        @else
                            @if (!$isPrintButtonExists)
                                <div class="btn-group">
                                    @if (count($printActionOption) > 1)
                                        <a type="button" class="btn btn-primary dropdown-toggle mr--8"
                                           data-toggle="dropdown"
                                           aria-haspopup="true" aria-expanded="false"> <i
                                                    class="fa fa-download"></i>&nbsp;&nbsp;{{trans('swis.user_documents.download_pdf.title')}}
                                        </a>
                                        <div class="dropdown-menu print-forms">
                                            @foreach ($printActionOption as $print => $template)
                                                @if (isset($template))

                                                    <a class="dropdown-print-forms addProductsToPrintForm"
                                                       data-id="{{$template}}">
                                                        {{--href="{{urlWithLng("/user-documents/{$documentId}/application/{$applicationId}/defaultPdf/{$template}")}}" target="_blank">--}}
                                                        <i class="fa fa-download"
                                                           target="_blank"></i>&nbsp;&nbsp;{{ $print }}</a>

                                                @else
                                                    @if ($showDefaultPrint)
                                                        <a class="dropdown-print-forms addProductsToPrintForm"
                                                           data-id="">
                                                            {{--                                               href="{{urlWithLng("/user-documents/{$applicationId}/{$pdfHashKey}/{$documentId}/application/pdf")}}" target="_blank">--}}
                                                            <i class="fa fa-download"></i>&nbsp;&nbsp;{{ $print }}</a>
                                                    @endif
                                                @endif
                                            @endforeach
                                        </div>
                                    @else
                                        @if ($showDefaultPrint)
                                            <a
                                                    {{--href='{{urlWithLng("/user-documents/{$applicationId}/{$pdfHashKey}/{$documentId}/application/pdf")}}'--}}
                                                    class="btn btn-primary mr-10 addProductsToPrintForm" data-id="">
                                                <i class="fa fa-download"></i>&nbsp;&nbsp;{{trans('swis.user_documents.download_pdf.title')}}
                                            </a>
                                        @endif
                                    @endif
                                </div>
                                @php($isPrintButtonExists = true)
                            @endif
                        @endif
                    @endif
                @endforeach

                <button type="button" class="btn btn-primary btn-save-change mr-10">{{ trans('swis.user_documents.default.save.button') }}</button>
            </div>
            <hr class="m-t-sm" />
        @endif

        @if(!is_null($userSuperscribeMapping) && $saveMode != 'view')
            <div class="superscribe-info-content clearfix">
                <form action="{{urlWithLng("/user-documents/{$documentId}/application/{$applicationId}/superscribe/store")}}"
                      method="post" id="superscribeUser" autocomplete="off">
                    <input type="hidden" name="start_state_id" value="{{$userSuperscribeMapping->start_state}}">
                    <input type="hidden" name="end_state_id" value="{{$userSuperscribeMapping->end_state}}">
                    <input type="hidden" name="last_update" value="{{$documentCurrentState->getOriginal('created_at') }}">
                    <input type="hidden" name="mapping_id_superscribe" value="{{$userSuperscribeMapping->id or ''}}">

                    <div class="superscribe-info-content clearfix">
                        <div class="form-group m-b-none">
                            <?php $users = App\Models\ConstructorStates\ConstructorStates::getUsersByStateRolesAndAttributes($userSuperscribeMapping->end_state, $documentId, $subApplication->agency_subdivision_id);?>
                            @if($users->count() > 0)
                                <select name="user_id" class="form-control m-r select2"
                                        data-select2-placeholder="{{trans('swis.user_documents.subscriber.select')}}">
                                    <option value="">{{trans('swis.user_documents.subscriber.select')}}</option>
                                    @foreach($users as $user)
                                        <option value="{{$user->id}}">{{$user->name()}}</option>

                                    @endforeach
                                </select>
                            @else
                                <h5><i>{{trans('swis.user_documents.superscribe.user.not_found.message')}}</i></h5>
                            @endif
                            <button class="btn mr-10 btn-primary">{{$userSuperscribeMapping->rAction->current()->action_name}}</button>
                            <div class="form-error" id="form-error-user_id"></div>
                        </div>
                    </div>
                </form>
            </div>
            <hr />
        @endif

    </div>

    {{-- Alerts div --}}
    <div id="alertMessage"></div>

    <form class="form-horizontal fill-up userDocumentList" id="form-data"
          action="{{urlWithLng("/user-documents/{$documentId}/application/{$applicationId}/store")}}"
          data-search-path="/user-documents/{{$documentId}}" data-save-btn="btn-state-change">
        <div class="tabs-container loading-close" data-not-fix="true">

            <ul class="nav nav-tabs">
                @foreach($data['tabs'] as $key => $tab)
                    <li class="{{ ($key == 0) ? 'active' : '' }} {{ $tab['class'] }}">
                        <a data-toggle="tab" data-auto-focus="off" href="{{ $tab['href'] }}"> {{ $tab['name'] }} </a>
                    </li>
                @endforeach
            </ul>

            <div class="tab-content">
                <?php
                reset($data['tabContent']);
                $firstKey = key($data['tabContent']);
                ?>

                @foreach ($data['tabContent'] as $key => $tab)
                    <div id="{{ $tab['tabId'] }}" class="tab-pane {{($key == $firstKey) ? 'active' : ''}}">
                        @if (isset($tab['block']))
                            @foreach($tab['block'] as $block)
                                <div class="panel panel-default" id="{{ $block['blockId'] }}">
                                    @if(!empty($block['blockName']))
                                        <div class="panel-heading">{{ $block['blockName'] }}</div>@endif

                                    <div class="panel-body">
                                        @if ($block['blockType'] != 'dataTable')

                                            @if($key == 'notes')

                                                <div class="row">
                                                    <div class="col-lg-10 col-lg-offset-1">

                                                        <div class="ibox-content no-padding notes__out">
                                                            <ul class="list-group notes-block notes__list saf-notes">
                                                                <li class="list-group-item notes__item">

                                                                    <b>{{ trans('swis.saf.notes.current_state') }}
                                                                        :</b> {{ $documentCurrentState->state->current()->state_name }}
                                                                </li>

                                                                @foreach ($notes as $note)
                                                                    <li class="list-group-item notes__item">
                                                                        <div class="notes__item--inner">
                                                                            <div class="table-cell notes__item--left">
                                                                                <p>{!! $note->note !!}</p>
                                                                            </div>
                                                                            <div class="table-cell text-right notes__item--right">
                                                                                <small class="block text-muted"><i
                                                                                            class="fa fa-clock-o"></i> {{ $note->created_at }}
                                                                                </small>
                                                                                @if ($note->to_saf_holder)
                                                                                    <small class="text-muted"><i
                                                                                                class="fas fa-envelope"></i> {!! $note->sent_to_saf_holder !!}
                                                                                    </small>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                @endforeach

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>

                                            @endif
                                            @if (isset($block['fields']) && count($block['fields']) > 0)
                                                @foreach ($block['fields'] as $field)
                                                    <div class="form-group {{ $field['fieldSetClass'] }}">
                                                        <label class="col-md-4 col-lg-3 control-label label__title {{isset($field['isFieldMdmLine']) && $field['isFieldMdmLine'] ? 'mdm-line-label' : ''}}"><span
                                                                    data-toggle="tooltip"
                                                                    title="{{ trans((string)$field['fieldSetTitle']) }}"
                                                                    data-placement="{{ (string)$field['fieldSetTitlePlacement'] }}"> {{ (string)$field['fieldSetLabel'] }} </span></label>

                                                        <div class="col-md-8">
                                                            <div class="{{ $field['fieldSetMultipleClass'] }}"
                                                                 data-fields="{{ $field['fieldSetData'] }}">
                                                                <div class="clearfix row">
                                                                    @if ($field['fieldSetOrigin'] == 'mdm')
                                                                        <div class="col-lg-10 col-md-8 field__one-col">
                                                                            @if ($field['fieldType'] == 'autocomplete')
                                                                                <select
                                                                                        title="{{ $field['fieldTitle'] }}"
                                                                                        class="form-control select2 select2-from-mdm {{ $field['fieldClass'] }} {{ $field['fieldDisabled'] }}"
                                                                                        id="{{ $field['fieldId'] }}"
                                                                                        data-url="{{ $field['fieldDataUrl'] }}"
                                                                                        data-ref="{{ $field['fieldDataRef'] }}"
                                                                                        data-min-input-length="1"
                                                                                        data-one-and-more="true"
                                                                                        data-mdm-select="true"
                                                                                        data-code-name="true"
                                                                                        data-id-value="true"
                                                                                        name="{{ $field['fieldName'] }}"
                                                                                        {{ $field['fieldDisabled'] }}
                                                                                        {{ $field['fieldAttributes'] }}>
                                                                                    <option value="">{{ trans('core.base.label.select') }}</option>
                                                                                    @if (isset($field['options']))
                                                                                        @foreach ($field['options'] as $option)
                                                                                            <option {{ $option['selected'] }} value="{{ $option['value'] }}">{{ $option['name'] }}</option>
                                                                                        @endforeach
                                                                                    @endif
                                                                                </select>
                                                                            @else
                                                                                @if ($field['fieldType'] == 'date')
                                                                                    <div class='input-group date'>
                                                                                        <span class="input-group-addon"><span
                                                                                                    class="glyphicon glyphicon-calendar"></span></span>
                                                                                        <input data-toggle="tooltip"
                                                                                               autocomplete="off"
                                                                                               title="{{ $field['fieldValue'] }}"
                                                                                               type="text"
                                                                                               class="form-control agencyInput {{ $field['fieldDisabled'] }}"
                                                                                               name="{{ $field['fieldName'] }}"
                                                                                               value="{{ $field['fieldValue'] }}"
                                                                                               {{ $field['fieldDisabled'] }}
                                                                                               placeholder="{{setDatePlaceholder()}}"/>
                                                                                    </div>
                                                                                @else
                                                                                    @if ($field['isFieldMdmLine'])
                                                                                        <hr class="mdm-line-hr"/>
                                                                                    @else

                                                                                        @if ($field['fieldIsContent'])
                                                                                            <textarea rows="5"
                                                                                                      data-toggle="tooltip"
                                                                                                      name="{{ $field['fieldName'] }}"
                                                                                                      title="{{ $field['fieldValue'] }}"
                                                                                                      class="form-control agencyInput {{ $field['fieldDisabled'] }}" {{ $field['fieldDisabled'] }}>{{ $field['fieldValue'] }}</textarea>
                                                                                        @else
                                                                                            <input data-toggle="tooltip"
                                                                                                   title="{{ $field['fieldValue'] }}"
                                                                                                   type="{{ $field['fieldType'] }}"
                                                                                                   class="form-control agencyInput {{ $field['fieldDisabled'] }}"
                                                                                                   name="{{ $field['fieldName'] }}"
                                                                                                   value="{{ $field['fieldValue'] }}"
                                                                                                    {{ $field['fieldDisabled'] }}>
                                                                                        @endif

                                                                                    @endif
                                                                                @endif
                                                                            @endif

                                                                            <div class="form-error"
                                                                                 id="{{ $field['fieldErrorName'] }}"></div>
                                                                        </div>

                                                                    @else

                                                                        @foreach ($field['fieldInFieldSet'] as $fieldInFieldSet)

                                                                            <div class="{{ $fieldInFieldSet['fieldBlockClass'] }}">
                                                                                @switch($fieldInFieldSet['fieldType'])

                                                                                    @case('select')
                                                                                    <select class="form-control select2 {{ $fieldInFieldSet['fieldClass'] }} {{ $fieldInFieldSet['fieldReadOnly'] }} {{ $fieldInFieldSet['fieldDisabled'] }}"
                                                                                            name="{{ $fieldInFieldSet['fieldName'] }}"
                                                                                            data-type="{{ $fieldInFieldSet['dataAttributes']['dataType'] }}" {{ $fieldInFieldSet['fieldDisabled'] }}>
                                                                                        <option value=""
                                                                                                selected>{{ trans('core.base.label.select') }}</option>
                                                                                        @foreach($fieldInFieldSet['options'] as $option)
                                                                                            <option {{ $option['selected'] }} title="{{ $option['name'] }}"
                                                                                                    value="{{ $option['value'] }}"> {{ $option['name'] }} </option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                    @break

                                                                                    @case('input')
                                                                                    @if ($fieldInFieldSet['fieldIsContent'])
                                                                                        <textarea
                                                                                                class="form-control {{ $fieldInFieldSet['fieldClass'] }} {{ $fieldInFieldSet['fieldReadOnly'] }} {{ $fieldInFieldSet['fieldDisabled'] }}"
                                                                                                rows="5"
                                                                                                name="{{ $fieldInFieldSet['fieldName'] }}"
                                                                                                data-toggle="tooltip" title="{{ $fieldInFieldSet['fieldValue'] }}"
                                                                                                id="{{ $fieldInFieldSet['fieldId'] }}"
                                                                                                data-name="{{ $fieldInFieldSet['dataAttributes']['dataName'] }}"
                                                                                                data-type="{{ $fieldInFieldSet['dataAttributes']['dataType'] }}"
                                                                                                      {{ $fieldInFieldSet['fieldDisabled'] }}
                                                                                            >{{ $fieldInFieldSet['fieldValue'] }}</textarea>
                                                                                    @else
                                                                                        @if ($fieldInFieldSet['fieldIsPhoneNumber'])
                                                                                            <div class="input-group">
                                                                                                <span class="input-group-addon">+</span>
                                                                                                @endif
                                                                                                @if ($fieldInFieldSet['fieldName'] == 'saf[general][regime]' && !empty($safGlobalDefaultData))
                                                                                                    <input type="text"
                                                                                                           class="form-control"
                                                                                                           value="{{$safGlobalDefaultData['global']['typeName']}}"
                                                                                                           disabled>
                                                                                                @else
                                                                                                    <input type="{{ $fieldInFieldSet['fieldInputType'] }}"
                                                                                                           class="form-control {{ $fieldInFieldSet['fieldClass'] }} {{ $fieldInFieldSet['fieldReadOnly'] }} {{ $fieldInFieldSet['fieldDisabled'] }}"
                                                                                                           name="{{ $fieldInFieldSet['fieldName'] }}"
                                                                                                           value="{{ $fieldInFieldSet['fieldValue'] }}"
                                                                                                           autocomplete="nope"
                                                                                                           data-toggle="tooltip"
                                                                                                           title="{{$fieldInFieldSet['fieldValue']}}"
                                                                                                           id="{{ $fieldInFieldSet['fieldId'] }}"
                                                                                                           data-name="{{ $fieldInFieldSet['dataAttributes']['dataName'] }}"
                                                                                                           data-type="{{ $fieldInFieldSet['dataAttributes']['dataType'] }}"
                                                                                                            {{ $fieldInFieldSet['fieldDisabled'] }}>
                                                                                                @endif
                                                                                                @if ($fieldInFieldSet['fieldIsPhoneNumber'])
                                                                                            </div>
                                                                                        @endif
                                                                                    @endif

                                                                                    @break

                                                                                    @case('textarea')
                                                                                    <textarea rows="5" cols="5"
                                                                                              class="form-control {{ $fieldInFieldSet['fieldReadOnly'] }} {{ $fieldInFieldSet['fieldDisabled'] }}"
                                                                                              {{ $fieldInFieldSet['fieldDisabled'] }} name="{{ $fieldInFieldSet['fieldName'] }}">{{ $fieldInFieldSet['fieldValue'] }}</textarea>
                                                                                    @break

                                                                                    @case('autocomplete')

                                                                                    @if(empty($fieldInFieldSet['refType']) || $fieldInFieldSet['refType'] != 'input')
                                                                                        <select class="form-control autocomplete {{ $fieldInFieldSet['fieldClass'] }} {{ $fieldInFieldSet['fieldReadOnly'] }} {{ $fieldInFieldSet['fieldDisabled'] }}"
                                                                                                name="{{ $fieldInFieldSet['fieldName'] }}"
                                                                                                data-url="{{ $fieldInFieldSet['fieldDataUrl'] }}"
                                                                                                data-ref="{{ $fieldInFieldSet['fieldDataRef'] }}" {{ $fieldInFieldSet['fieldDisabled'] }}>

                                                                                            <option value=""></option>
                                                                                            @foreach($fieldInFieldSet['options'] as $option)
                                                                                                <option {{ $option['selected'] }} value="{{$option['value']}}"> {{ $option['name'] }} </option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    @endif

                                                                                    @break

                                                                                    @case('hidden')
                                                                                    <input value="{{ $fieldInFieldSet['fieldValue']  }}"
                                                                                           type="hidden"
                                                                                           name="{{ $fieldInFieldSet['fieldName'] }}" {{ $fieldInFieldSet['fieldDisabled'] }}>
                                                                                    @break

                                                                                    @case('autoincrement')
                                                                                    <input value="{{ $fieldInFieldSet['fieldValue'] }}"
                                                                                           type="{{ $fieldInFieldSet['fieldInputType'] }}"
                                                                                           readonly
                                                                                           class="form-control autoincrement"
                                                                                           name="{{ $fieldInFieldSet['fieldName'] }}" {{ $fieldInFieldSet['fieldDisabled'] }}>
                                                                                    @break

                                                                                    @case('checkbox')
                                                                                    <input type="checkbox"
                                                                                           name="{{ $fieldInFieldSet['fieldName'] }}" {{ $fieldInFieldSet['fieldDisabled'] }}>
                                                                                    @break

                                                                                @endswitch

                                                                                <div class="form-error"
                                                                                     id="{{ $fieldInFieldSet['fieldErrorName'] }}"></div>

                                                                                {!! $fieldInFieldSet['fieldNeedMdm'] !!}
                                                                            </div>

                                                                            @if($field['fieldSetMultiple'])
                                                                                <button type="button"
                                                                                        class="btn btn-primary plusMultipleFields">
                                                                                    <i class="fa fa-plus"></i></button>
                                                                            @endif

                                                                        @endforeach
                                                                    @endif
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif

                                        @else
                                            @switch($block['module'])
                                                @case('responsibilities')
                                                @include('user-documents.obligations.responsibilities-table')
                                                @break

                                                @case('products')
                                                @include('user-documents.products.products-table')
                                                @break

                                                @case('instructions')
                                                @include('user-documents.instructions.instructions')
                                                @break

                                                @case('lab_examination')
                                                @if (Auth::user()->islaboratory())
                                                    @include('user-documents.laboratories.lab-examination-for-laboratory', ['view' => false])
                                                @else
                                                    @include('single-application.laboratories.lab-examination')
                                                @endif

                                                @break
                                            @endswitch
                                        @endif

                                        @if($block['multiple'])
                                            <button type="button"
                                                    class="btn btn-primary {{ $block['subForm'] }} {{ $block['renderTr'] }}"
                                                    data-table="{{ $block['tableClass'] }}"
                                                    data-module="{{ $block['module'] }}">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        @endif

                        @if ($key == 'attached_documents')
                            @include('user-documents.documents.document-module')
                        @endif

{{--                        @if ($key == 'laboratories' && $laboratoryStatesCount > 0)--}}
{{--                            @include('user-documents.old-laboratories.laboratories')--}}
{{--                        @endif--}}

                        @if($key == 'examination')
                            @include('user-documents.old-laboratories.examination')
                        @endif
                    </div>
                @endforeach

            </div>
        </div>
        <input type="hidden" name="id" id="documentID" value="{{$documentId or ''}}">
        <input type="hidden" name="safNumber" id="safNumber" value="{{$saf->regular_number}}">
        <input type="hidden" name="state" value="" id="documentStateID">
        <input type="hidden" name="action" value="" id="documentActionID">
        <input type="hidden" name="mapping_id" value="" id="mappingID">
        <input type="hidden" name="is_confirmed" value="false" id="isConfirmed">
        <input type="hidden" name="applicationId" id="appId" value="{{$applicationId}}">
        <input type="hidden" id="currentState" value="{{$documentCurrentState->state_id or ''}}">
        <input type="hidden" name="lastUpdate" id="lastUpdate"
               value="{{$documentCurrentState->getOriginal('created_at') }}">
        <input type="hidden" name="pdfHashKey" id="pdfHashKey" value="{{$pdfHashKey}}">
        <input type="hidden" name="needDigitalSignature" value="true" id="needDigitalSignature">
    </form>

    <!-- Opening modal for choosing products for Print form -->
    <div id="subApplicationProductsModal" class="modal fade subFormModal" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal__title fb">{{trans('swis.user_documents.sub_application.choose_products.title')}}</h3>
                </div>
                <div class="modal-body">
                    <div id="productsErrorList"></div>
                    <div class="sub-application-products-list"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger"
                            data-dismiss="modal">{{trans('swis.user_documents.choose_products.cancel')}}</button>
                    <button type="button"
                            class="btn btn-primary printFormButton">{{trans('swis.user_documents.choose_products.approve')}}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="showWarning" tabindex="-1" role="dialog" aria-labelledby="showWarning"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content panel-danger">
                <div class="modal-header panel-heading">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('swis.user_documents.show_warning.modal.header') }}</h4>
                </div>
                <div class="modal-body" id="showWarningModalBody"></div>
                <div class="modal-footer">
                    <button type="button"
                            class="btn btn-default rejectWarning">{{ trans('swis.user_documents.show_warning.reject') }}</button>
                    <button type="button"
                            class="btn btn-warning confirmWarning">{{ trans('swis.user_documents.show_warning.confirm') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="showWarning2" tabindex="-1" role="dialog" aria-labelledby="showWarning2"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content panel-danger">
                <div class="modal-header panel-heading">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('swis.user_documents.show_warning2.modal.header') }}</h4>
                </div>
                <div class="modal-body" id="showWarning2ModalBody"></div>
                <div class="modal-footer">
                    <button type="button"
                            class="btn btn-default rejectWarning2">{{ trans('swis.user_documents.show_warning2.reject') }}</button>
                    <button type="button"
                            class="btn btn-warning confirmWarning2">{{ trans('swis.user_documents.show_warning2.confirm') }}</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Rutoken modal -->
    <div class="modal fade" id="rutoken-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal__title fb"><img src="/image/digital-signature/check-popup-icon.png"
                                                     class="image_middle"> {{trans('core.base.rutoken.title')}}</h4>
                    <button type="button" class="close modal__close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="rutokenAlertMessage"></div>
                    <div class="form-group row block">
                        <label class="col-lg-3 control-label">{{trans('swis.user_documents.digital_signature.devices_list')}}</label>
                        <div class="col-lg-6">
                            <select name="device-list" id="device-list" class="form-control"></select>
                        </div>
                        <div class="col-lg-3">
                            <button class="btn btn-primary"
                                    id="refresh-dev">{{trans('core.base.button.refresh')}}</button>
                        </div>
                    </div>
                    {{--<div class="form-group row block">--}}
                    {{--<label class="col-lg-3 control-label">Key(s) list:</label>--}}
                    {{--<div class="col-lg-9">--}}
                    {{--<select name="key-list" id="key-list" class="form-control"></select>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    <div class="form-group row block">
                        <label class="col-lg-3 control-label">{{trans('swis.user_documents.digital_signature.certificates_list')}}</label>
                        <div class="col-lg-9">
                            <select name="cert-list" id="cert-list" class="form-control"></select>
                        </div>
                    </div>

                    <div class="form-group row block">
                        <label class="col-lg-3 control-label">{{trans('swis.user_documents.digital_signature.pin')}}</label>
                        <div class="col-lg-9">
                            <input type="text" id="device-pin" class="form-control"/>
                            <div class="form-error"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="block">
                        <input type="hidden" name="subapplicationId" id="subapplicationId" value="{{$applicationId}}">
                        <button id="buttonMapping" name="buttonMapping" value="" style="display:none"></button>
                        <button id="login" class="btn btn-primary">{{trans('login.modal.title')}}</button>
                        {{--<button id="logout" class="btn btn-danger">Logout</button>--}}
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">{{trans('swis.single_app.modal.close')}}</button>
                    </div>
                    <textarea name="" id="message" cols="30" rows="10" style="display:none"></textarea>
                    <input type="hidden" name="digitalSignatureId" id="digitalSignatureId" val="" style="display:none">
                    <button id="sign-button" style="display:none">sign</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Rutoken check modal -->
    <div class="modal fade" id="rutoken-modal-check" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal__title fb">{{trans('core.base.rutoken-check.title')}}</h4>
                    <button type="button" class="close modal__close" data-dismiss="modal" aria-hidden="true">&times;
                    </button>
                </div>
                <div class="modal-body">
                    <div id="checkRutokenAlertMessage"></div>
                    <div id="digitalSignatureInfo"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="laboratoryCrudModal" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 1150px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="text-center">{{trans('swis.saf.lab_examinations.form.title')}}</h2>
                </div>

                <div class="modal-body">

                    <div class="col-md-6 col-md-offset-3">
                        <?php
                        $constructorDocument = \App\Models\ConstructorDocument\ConstructorDocument::select('expertise_type')->find($documentId);
                        $expertiseType = $constructorDocument->expertise_type ?? [];
                        ?>
                        <select class="form-control select2" id="selectLabExpertise" {{ !isset($superscribe) ? 'disabled' : '' }}>
                            <option value="">{{ trans("swis.saf.lab_examination.select_type") }}</option>
                            @foreach(\App\Models\Laboratory\Laboratory::LAB_TYPES as $key => $value)
                                @if (in_array($value, $expertiseType))
                                    <option value="{{ $value }}"> {{ trans("swis.saf.lab_examination.{$key}.option") }} </option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <br/><br/>
                    <hr/>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger alertDivForLabExamination" role="alert"
                                 style="display: none;"></div>
                        </div>
                    </div>

                    <div id="labExpertiseCrudModalBody">

                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="generateLaboratoryApplication"
                            disabled>{{trans('swis.saf.lab_examination.generate.button')}}</button>
                    <button type="button" class="btn btn-default closeMessageSubApp"
                            data-dismiss="modal">{{trans('core.base.button.close')}}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="laboratoryResultModal" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 1150px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="text-center">{{trans('swis.saf.lab_examinations.result.title')}}</h2>
                </div>

                <div class="modal-body">
                    <div class="labExpertiseResultModalBody">

                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default closeMessageSubApp"
                            data-dismiss="modal">{{trans('core.base.button.close')}}</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('form-buttons-bottom')
    <div class="buttons-container">
        <a href='{{urlWithLng("/user-documents/{$documentId}")}}'
           class="btn btn-default mc-nav-button-cancel">{{trans('swis.my_applications.close.button')}}</a>
    </div>
@endsection

@section('footer-part')
    <div class="modal fade" id="PayObligationModal" role="dialog"></div>
@endsection

@section('scripts')
    <script src="{{asset('assets/helix/core/plugins/bootstrap-typeahead/bootstrap-typeahead.js')}}"></script>
    <script src="{{asset("js/user-documents/main.js")}}"></script>
    <script src="{{asset("js/user-documents/obligation.js")}}"></script>
    <script src="{{asset("js/user-documents/old-laboratories.js")}}"></script>
    <script src="{{asset("js/user-documents/document.js")}}"></script>
    <script src="{{asset("js/user-documents/laboratories.js")}}"></script>
    <script src="{{asset('js/guploader.js')}}"></script>
@endsection