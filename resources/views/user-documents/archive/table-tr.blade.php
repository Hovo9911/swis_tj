@php($num = empty($num) ? 1 : $num)

<tr data-id="{{$num}}">

</tr>

<tr data-id="{{$num}}">
    <?php use Illuminate\Support\Facades\DB;$batchId = !empty($data) ? $data->id : "" ?>
    <input type="hidden" name="batch[{{$data->id}}][{{$num}}][id]" class="batchesId" value="{{ $batchId }}">
    @foreach($block->fieldset as $column)
        <?php
            if (!empty($column->attributes()->showInView) && !(string)$column->attributes()->showInView) { continue; }
            if (in_array((string)$column->field->attributes()->dataName, $safHiddenColumns) ) { continue; }

            $name = $column->field->attributes()->name . '';

            if (strpos($name, ']') !== false) {
                $name = str_replace(['][', '['], '.', $name);
                $name = str_replace(']', '', $name);
            }

            if(!empty($column->field->attributes()->dataName) ){
                $name = (string)$column->field->attributes()->dataName;
            }
            $fieldID                 = (string)$column->field->attributes()->id;
            $readOnly                = 'readonly';
            $safListControlledFields = '';


            if (isset($globalHiddenFields, $globalHiddenFields['saf']) && in_array($fieldID, $globalHiddenFields['saf'])) {
                continue;
            } else {
                $checkedInList = (isset($lists) && array_key_exists($fieldID, $lists));
                if ($checkedInList && in_array("non_visible", $lists[$fieldID]) && !in_array("editable", $lists[$fieldID]) && !in_array("mandatory", $lists[$fieldID])) {
                    continue;
                } else {
                    if ($checkedInList && in_array("mandatory", $lists[$fieldID])) {
                        $safListControlledFields = 'saf_list_controlled_fields';
                    }
                    if ($checkedInList && in_array("editable", $lists[$fieldID])) {
                        $readOnly = '';
                        $safListControlledFields = 'saf_list_controlled_fields';
                    }
                }
            }

            $readOnly = ((string)$column->field->attributes()->dataName == 'batch_number' && !empty($data) &&  !empty($data->auto_generated) && $data->auto_generated) ? 'readonly' : '';
            if (isset($incrementNumber) && $incrementNumber > 1) {
                $readOnly = "";
            }
        ?>

        <td style="{{ ($column->field->attributes()->type == 'select') ? 'width: 200px;' : '' }}">
            @switch($column->field->attributes()->type)

                @case('select')
                    <?php
                        $refData = [];
                        $mdm = \App\Models\DataModel\DataModel::select('id', 'reference')->where(DB::raw('id::varchar'), $column->field->attributes()->mdm)->first();
                        $showName = false;

                        if ($column->field->attributes()->ref) {
                            $refData = \Illuminate\Support\Facades\DB::table('reference_' . (string)$column->field->attributes()->ref)->get();
                        } elseif (!empty($mdm->reference)) {
                            $showName = true;
                            $rTable = 'reference_' . $mdm->reference;
                            $rTableMl = 'reference_' . $mdm->reference . '_ml';

                            $refData = \Illuminate\Support\Facades\DB::table($rTable)
                                ->select("{$rTable}.code", "{$rTableMl}.name")
                                ->join($rTableMl, "{$rTable}.id", '=', "{$rTableMl}.{$rTable}_id")->where("{$rTableMl}.lng_id", '=', cLng('id'))
                                ->where("{$rTable}.show_status", '=', \App\Models\ReferenceTable\ReferenceTable::STATUS_ACTIVE)
                                ->get();
                        }
                    ?>

                    <select class="form-control select2 {{ $safListControlledFields }} {{ $readOnly }} {{$column->field->attributes()->defaultClass}}" {{ $readOnly }} name="{{ "product_batches[{$data->id}][{$column->field->attributes()->dataName}]" }}" >
                        <option value="" disabled selected></option>
                        @if(count($refData) > 0)
                            @foreach($refData as $ref)
                                <option {{ (!empty($data->{$name}) && $ref->code == $data->{$name}) ? 'selected' : '' }} value="{{$ref->code}}">{{ ($showName) ? "({$ref->code}) {$ref->name}": $ref->code}}</option>
                            @endforeach
                        @endif
                    </select>

                    @break

                @case('input')
                    <?php

                        if (!empty($data->{$name}) || (!empty($data->{$name}) && $data->{$name} == 0.0)) {
                            $dataNameView = ((string)$column->field->attributes()->dataName == 'batch_number') ? trans($data->{$name}) : $data->{$name};
                        } else {
                            $dataNameView = ($column->field->attributes()->dataName == 'rejected_quantity' or $column->field->attributes()->dataName == 'approved_quantity') ? (int)$column->field->attributes()->defaultValue : $column->field->attributes()->defaultValue;
                        }
                    ?>

                    <input {{ ($readOnly != '' ) ? $readOnly : $column->field->attributes()->readOnly  }}
                           value="{{ $dataNameView }}"
                           type="{{($column->field->attributes()->inputType) ? $column->field->attributes()->inputType : 'text'}}"
                           class="form-control {{ $safListControlledFields }} {{$column->field->attributes()->defaultClass}}"
                           name="{{ "product_batches[{$data->id}][{$column->field->attributes()->dataName}]" }}">
                    @break

                @case('textarea')
                    <textarea rows="5" cols="5"
                              name="{{ "product_batches[{$data->id}][{$column->field->attributes()->dataName}]" }}"
                              class="form-control {{ $safListControlledFields }}"
                              {{ ($column->field->attributes()->readOnly && $readOnly != '') ? $column->field->attributes()->readOnly : ''}}
                              {{ $readOnly }}></textarea>

                    @break

                @case('autocomplete')

                    <?php $mapData = [] ?>
                    @foreach($column->field->mapping->map as $map)
                        <?php $mapData[] = [
                            'from' => (string)$map->attributes()->from,
                            'to' => (string)$map->attributes()->to,
                        ] ?>
                    @endforeach

                    <input value=""
                           type="{{($column->field->attributes()->inputType) ? $column->field->attributes()->inputType : 'text'}}"
                           name="{{ "product_batches[{$data->id}][{$column->field->attributes()->dataName}]" }}"
                           placeholder="autocomplete"
                           class="form-control autocomplete {{ $safListControlledFields }}"
                           data-ref="{{$column->field->attributes()->ref}}"
                           data-fields="{{json_encode($mapData)}}"
                           {{$readOnly}}>
                    @break

                @case('actions')
                    @foreach($column->field->actions->action as $action)
                        @if($action->attributes()->type == 'print')
                            @continue
                        @endif
                        <button type="button"  data-id="{{!empty($data) ? $data->id : ''}}" class="{{(empty($raw)) ? $action->attributes()->type : ' s-table-delete'}}"><i class="{{$action->attributes()->icon}}"></i></button>
                    @endforeach

                    @break

                @case('autoincrement')
                    <span class="incrementNumber">{{empty($incrementNumber) ? $num : $incrementNumber}}</span>
                @break

                @case('date')
                    <input value="{{!empty($data->{$name}) ? $data->{$name} : @$column->field->attributes()->defaultValue }}"
                           name="{{ "product_batches[{$data->id}][{$column->field->attributes()->dataName}]" }}"
                           type="date"
                           class="form-control {{ $safListControlledFields }} {{$column->field->attributes()->defaultClass}}"
                           {{$readOnly}}>
                @break

            @endswitch
            <div class="form-error" id="form-error-product_batches-{{ $data->id }}-{{$column->field->attributes()->dataName}}"></div>

        </td>

    @endforeach

    @if(!empty($userDocumentFields))

        @foreach($userDocumentFields as $userDocumentField)
            @if ($userDocumentField['xpath'] != "tab[@key='products']/block[@name='swis.single_app.products_list']/subBlock[@name='swis.single_app.batches']")
                @continue;
            @endif

            <?php
                $listControlled = listControlled($userDocumentField['field_id'], $globalHiddenFields, $lists, 'mdm', $mandatoryListForView);

                if ($listControlled['continue']) { continue; }
                $requiredField = $listControlled['requiredField'];
                $disabled      = $listControlled['disabled'];
            ?>

            <td>
                @php($inputType = 'text')
                @switch($userDocumentField['type'])
                    @case('varchar')
                        @php($inputType = 'text')
                    @break
                    @case('integer')
                        @php($inputType = 'number')
                    @break
                    @case('date')
                        @php($inputType = 'date')
                    @break
                    @case('datetime')
                        @php($inputType = 'datetime-local')
                    @break
                    @case('autocomplete')
                        @php($inputType = 'autocomplete')
                    @break
                @endswitch

                @if ($inputType == 'autocomplete')
                    <?php
                        $rTable = "reference_{$userDocumentField['reference']}";
                        $rTableMl = "reference_{$userDocumentField['reference']}_ml";

                        $refData = \Illuminate\Support\Facades\DB::table($rTable)
                            ->select("{$rTable}.code", "{$rTableMl}.name")
                            ->join($rTableMl, "{$rTable}.id", '=', "{$rTableMl}.{$rTable}_id")->where("{$rTableMl}.lng_id", '=', cLng('id'))
                            ->where("{$rTable}.show_status", '=', \App\Models\ReferenceTable\ReferenceTable::STATUS_ACTIVE)
                            ->get();
                    ?>
                    <select class="form-control select2" id="select2-{{$batchId.'-'.$fieldID}}" name="customData[{{$batchId.'-'.$fieldID}}]" >

                        <option value="">{{ trans('swis.document.label.select') }}</option>

                        @if (isset($customData))
                            @foreach ($refData as $item)
                                <option value="{{ $item->code }}" {{ (array_key_exists($batchId.'-'.$fieldID, $customData) && $item->code == $customData[$batchId.'-'.$fieldID]) ? 'selected' : '' }} >{{ "(".$item->code.") ".$item->name }}</option>
                            @endforeach
                        @endif
                    </select>
                @else
                    <input data-toggle="tooltip" title="{{$userDocumentField['validation'] or ''}}" type="{{ $inputType }}"
                           class="form-control agencyInput"
                           name="customData[{{$batchId.'-'.$fieldID}}]"
                           value="{{$customData["{$batchId}-{$fieldID}"] or ''}}"
                           {{ $disabled }}>
                @endif

                <div class="form-error" id="form-error-{{'customData['. $batchId .']['.$fieldID.']'}}"></div>
                <div class="form-error" id="form-error-{{'customData-'. $batchId .'-'.$fieldID}}"></div>
            </td>
        @endforeach
    @endif

</tr>