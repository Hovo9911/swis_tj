<?php
$columns = (array)$block->columns;
$hideColumns = (array)$block->columnsNotShowInView ?? [];
$simpleAppTabs = \App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure::lastXml();
$safProducts = \Illuminate\Support\Facades\DB::table($block->attributes()->DbTableName)->select($columns)->where('saf_number', $saf->regular_number)->whereIn('id', $availableProductsIds)->get();
$blockXml = $simpleAppTabs->xpath('/tabs/tab[@key="products"]/block[@type]');

$divColClass = [
    '1' => 2,
    '2' => 1,
    '3' => 2,
    '4' => 3,
    '5' => 1,
    '6' => 1,
    '7' => 1,
    '8' => 1,
]
?>

@if(!empty($safProducts))

    <!--Accordion wrapper-->
    <div class="products-accordion-option">
        <a href="javascript:void(0)"
           class="toggle-accordion active"></a>
    </div>
    <div class="clearfix"></div>
    <div class="panel-group formAccordion" id="productsList" role="tablist"
         aria-multiselectable="true">
        <div class="clearfix product-accordion-heading">
            <?php $j = 1 ?>
            @foreach($columns as $key=>$column)
                @if(!empty($hideColumns) && in_array($column,$hideColumns)) @continue @endif
                <div class="col-md-{{!empty($divColClass[$j]) ? $divColClass[$j] : '1'}}">
                    {{trans('swis.single_app.'.$column)}}
                </div>
                <?php $j++ ?>
            @endforeach
        </div>
        <?php
        $refDataColumns = $block->columnsRefData ?? [];
        $refDataColumnsArr = [];
        if (!empty($refDataColumns)) {
            foreach ($refDataColumns as $refDataColumn) {
                $refAttr = $refDataColumn->attributes();
                $fromRef = (string)$refAttr->from;
                $toColumn = (string)$refAttr->to;
                $refDataColumnsArr[$toColumn] = $fromRef;
            }
        }
        ?>

        @foreach($safProducts as $key=>$safProduct)
            <?php $productId = $safProduct->id ?>
            <div class="panel panel-default">

                <div class="panel-heading" role="tab" id="product{{$productId}}">

                    <div class="panel-title">
                        <a role="button" data-toggle="collapse"
                           data-parent="#productsList"
                           href="#collapseOne{{$productId}}"
                           aria-expanded="true"
                           aria-controls="collapseOne{{$productId}}">
                            <div class="row">
                                <?php $i = 1; ?>
                                @foreach($safProduct as $key=>$val)
                                    @if(!empty($hideColumns) && in_array($key,$hideColumns)) @continue @endif
                                    <div class="col-md-{{!empty($divColClass[$i]) ? $divColClass[$i] : '1'}}">
                                        <?php
                                            if (!empty($refDataColumnsArr) && !empty($val)) {
                                                if (isset($refDataColumnsArr[$key])) {
                                                    $refTableData = getReferenceRows($refDataColumnsArr[$key], false, $val);

                                                    if (!is_null($refTableData)) {
                                                        $val = $refTableData->name ?? '';
                                                    }
                                                }
                                            }

                                            if($key == 'product_number'){
                                                $val = $availableProductsIdNumbers[$productId];
                                            }
                                        ?>

                                        <span>{{$val}}</span>
                                    </div>
                                    <?php $i++ ?>
                                @endforeach
                            </div>
                        </a>
                    </div>
                </div>
                <div id="collapseOne{{$safProduct->id}}"
                     class="panel-collapse collapse" role="tabpanel"
                     aria-labelledby="headingOne{{$safProduct->id}}">
                    <div class="panel-body ">
                        <?php
                        $prRaw = \DB::table($block->attributes()->DbTableName)->where('id', $safProduct->id)->first();
                        ?>
                        @include('user-documents.block-fieldset',['block'=>$blockXml[0], 'raw'=>$prRaw, 'rawId'=>$productId, 'saf'=>null, 'edit'=>true])
                    </div>
                </div>
            </div>
        @endforeach

    </div>
@endif


