<?php
use Illuminate\Support\Facades\DB;$safData = null;

    if (!empty($saf)) {
        $safData = (!empty($updatedSafData)) ? $updatedSafData : $saf->data;
    }
?>

@if(!empty($simpleAppTab) && (string)$simpleAppTab->attributes()->key == 'notes')
    @include('user-documents.notes.saf-notes-actions')
@endif

@if(!empty($userDocumentFields))
    @foreach($userDocumentFields as $userDocumentField)

        <?php
            if ($userDocumentField['xpath'] == "tab[@key='products']/block[@name='swis.single_app.products_list']/subBlock[@name='swis.single_app.batches']") { continue; }

            $listControlled = listControlled($userDocumentField['field_id'], $globalHiddenFields, $lists, 'mdm',$mandatoryListForView);
            if ($listControlled['continue']) { continue; }
            $requiredField = $listControlled['requiredField'];
            $disabled      = $listControlled['disabled'];

            $prName = (!empty($raw) ? '_pr_'.$raw->id : '');
        ?>

        @if(!empty($simpleAppTab))
            <?php $xmlTabKey = $simpleAppTab->attributes()->key ?>
        @endif

        @if(!empty($tabKey))
            <?php $xmlTabKey = $tabKey ?>
        @endif

        @if(!empty($xmlTabKey))
            @if($xmlTabKey == $userDocumentField['tab'])

                @if(!empty($userDocumentField['block']))

                    @if($block->attributes()->name != $userDocumentField['block'])
                        @continue
                    @endif

                @endif

                <div class="form-group {{ $requiredField }}">
                    <label class="col-lg-3 control-label" data-toggle="tooltip"
                           title="{{isset($userDocumentField['mls'][cLng()]) ? $userDocumentField['mls'][cLng()]['tooltip'] : ''}}">{{isset($userDocumentField['mls'][cLng()]) ? $userDocumentField['mls'][cLng()]['label'] : ''}}</label>

                    <div class="col-md-8">
                        <div class="col-md-10">
                            @php($inputType = 'text')
                            @switch($userDocumentField['type'])
                                @case('varchar')
                                    @php($inputType = 'text')
                                @break
                                @case('integer')
                                    @php($inputType = 'number')
                                @break
                                @case('date')
                                    @php($inputType = 'date')
                                @break
                                @case('datetime')
                                    @php($inputType = 'datetime-local')
                                @break
                                @case('autocomplete')
                                    @php($inputType = 'autocomplete')
                                @break
                            @endswitch

                            @if ($inputType == 'autocomplete')
                                <?php
                                    $refAllData = \DB::table('reference_'.$userDocumentField['reference'])->where('show_status','1')->get();
                                ?>

                                @if($refAllData->count() < 251)
                                        <select
                                            title="{{$userDocumentField['mls'][cLng('id')]['tooltip']}}"
                                            class="form-control select2 select2-from-mdm {{($refAllData->count() > 10) ? 'select2' : ''}} {{ ($disabled == '') ? 'not-disabled-field' : '' }}"
                                            id="select2-{{$userDocumentField['reference_id']}}"
                                            data-one-and-more="true"
                                            data-mdm-select="true"
                                            data-select2-allow-clear="true"
                                            name="customData[{{$userDocumentField['field_id'].$prName}}]">

                                        <option value=""></option>
                                        @foreach($refAllData as $refData)
                                            <?php
                                                $refDataMl = \DB::table('reference_'.$userDocumentField['reference'].'_ml')->where('reference_'.$userDocumentField['reference'].'_id',$refData->id)->where('lng_id', '=', cLng('id'))->first();

                                                $selected = '';
                                                if (isset($customData) && array_key_exists($userDocumentField['field_id'].$prName, $customData)) {
                                                    if (is_array($customData[$userDocumentField['field_id'].$prName])) {
                                                        $selected = $customData[$userDocumentField['field_id'].$prName]['value'] == $refData->code ? 'selected' : '';
                                                    } else {
                                                        $selected = $customData[$userDocumentField['field_id'].$prName] == $refData->code ? 'selected' : '';
                                                    }
                                                }
                                            ?>
                                            <option {{ $selected }} value="{{$refData->code}}">
                                                {{$refDataMl->name}}
                                            </option>
                                        @endforeach

                                    </select>
                                @else
                                    <select
                                            title="{{$userDocumentField['mls'][cLng('id')]['tooltip']}}"
                                            class="form-control select2 select2-from-mdm select2-ajax {{ ($disabled == '') ? 'not-disabled-field' : '' }}"
                                            id="select2-{{$userDocumentField['reference_id']}}"
                                            data-url="{{urlWithLng('/reference-table/ref')}}"
                                            data-ref="{{$userDocumentField['reference_id']}}"
                                            data-min-input-length="1"
                                            data-one-and-more="true"
                                            data-mdm-select="true"
                                            name="customData[{{$userDocumentField['field_id'].$prName}}]">

                                        @if (isset($customData) && array_key_exists($userDocumentField['field_id'], $customData) && is_array($customData[$userDocumentField['field_id']]))
                                            <option value="{{$customData[$userDocumentField['field_id']]['value']}}">{{ $customData[$userDocumentField['field_id']]['text'] }}</option>
                                        @endif
                                    </select>
                                @endif
                            @else
                                <input data-toggle="tooltip" title="{{$userDocumentField['mls'][cLng('id')]['tooltip'] or ''}}" type="{{ $inputType }}"
                                       class="form-control agencyInput"
                                       name="customData[{{$userDocumentField['field_id'].$prName}}]"
                                       value="{{$customData[$userDocumentField['field_id'].$prName] or ''}}"
                                       {{ $disabled }}>
                            @endif

                            <div class="form-error" id="form-error-{{'customData['.$userDocumentField['field_id'].$prName.']'}}"></div>
                            <div class="form-error" id="form-error-{{'customData-'.$userDocumentField['field_id'].$prName}}"></div>

                        </div>
                    </div>
                </div>
            @endif
        @endif
    @endforeach
@endif

@foreach($block->fieldset as $fieldSet)
    <?php
        $listControlled = listControlled((string)$fieldSet->field->attributes()->id, $globalHiddenFields, $lists, 'saf', $mandatoryListForView);

        if ($listControlled['continue']) { continue; }
        $requiredField = $listControlled['requiredField'];
        $disabled      = $listControlled['disabled'];

    ?>

    <div class="form-group {{($fieldSet->field->attributes()->required) ? 'required' : $requiredField }}">
        <label
            @if($fieldSet->field->attributes()->tooltipText)
                data-toggle="tooltip"
                data-placement="{{(empty($fieldSet->field->attributes()->tooltipPlacement)) ? 'top' : $fieldSet->field->attributes()->tooltipPlacement}}"
                title="{{trans($fieldSet->field->attributes()->tooltipText)}}"
            @endif
            {{ $disabled }}
            class="col-lg-3 control-label">
            {!!  trans((string)$fieldSet->field->attributes()->title)!!}
        </label>

        <div class="col-md-8">
            <?php
                $fieldData = [];
                $className = '';
                if ($fieldSet->attributes()->multiple) {
                    if ( (string)$fieldSet->field->attributes()->name == "saf[transportation][transit_country][]" ) {
                        if (!empty($subApplicationData->data) && isset($subApplicationData->data['transportation'], $subApplicationData->data['transportation']['transit_country'])) {
                            $fieldData = json_encode($subApplicationData->data['transportation']['transit_country']);
                            $className = "saf_transit_country";
                        } elseif (!empty($saf->data['saf']['transportation']['transit_country'])) {
                            $fieldData = json_encode($saf->data['saf']['transportation']['transit_country']);
                            $className = "saf_transit_country";
                        }
                    } elseif ( (string)$fieldSet->field->attributes()->name == "saf[transportation][state_number_customs_support][1][state_number]" ) {
                        if (!empty($subApplicationData->data && isset($subApplicationData->data['transportation'], $subApplicationData->data['transportation']['transit_country']))) {
                            $fieldData = json_encode($subApplicationData->data['transportation']['state_number_customs_support']);
                            $className = "state_number_customs_support";
                        } elseif (!empty($updatedSafData) && !empty($updatedSafData['saf']['transportation']['state_number_customs_support'])) {
                            $fieldData = json_encode($updatedSafData['saf']['transportation']['state_number_customs_support']);
                            $className = "state_number_customs_support";
                        }

                    }
                }
            ?>
            <div class="{{($fieldSet->attributes()->multiple) ? 'multipleFields '.$className : ''}}" data-fields="{{ (!empty($fieldData)) ? $fieldData : '' }}">
                <div class="clearfix">
                    @foreach($fieldSet->field as $field)

                        <?php
                            if (count($fieldSet->field) > 1) {
                                $listControlled = listControlled((string)$field->attributes()->id, $globalHiddenFields, $lists, 'saf', $mandatoryListForView);

                                if ($listControlled['continue']) { continue; }
                                $requiredField = $listControlled['requiredField'];
                                $disabled      = $listControlled['disabled'];
                            }

                            $refData = [];

                            if ($field->attributes()->mdm) {

                                if (!empty($field->attributes()->mdm)) {

                                    $mdm = \App\Models\DataModel\DataModel::where(DB::raw('id::varchar'), $field->attributes()->mdm)->first();

                                    if (!empty($mdm->reference) && empty($field->attributes()->ajax)) {

                                        $rTable = 'reference_' . $mdm->reference;
                                        $rTableMl = 'reference_' . $mdm->reference . '_ml';

                                        $refData = \Illuminate\Support\Facades\DB::table($rTable)
                                            ->join($rTableMl, "{$rTable}.id", '=', "{$rTableMl}.{$rTable}_id")->where($rTableMl . '.lng_id', '=', cLng('id'))
                                            ->where($rTable . '.show_status', '=', \App\Models\ReferenceTable\ReferenceTable::STATUS_ACTIVE)
                                            ->orderBy("{$rTableMl}.name")
                                            ->get();

                                    }
                                }

                            }

                        ?>

                        <div class="{{(count($fieldSet->field) > 1) ? 'col-md-3' : 'col-md-10'}}">
                            <?php

                                $name = $field->attributes()->name . '';

                                if (strpos($name, ']') !== false) {
                                    $name = str_replace(['][', '['], '.', $name);
                                    $name = str_replace(']', '', $name);
                                }

                                $storeValue = '';

                                if (!empty($safData)) {
                                    $safData = prefixKey('', $safData);
                                    if (!empty($safData[$name])) {
                                        $storeValue = $safData[$name];
                                    }
                                }

                            ?>

                            @switch($field->attributes()->type)

                                @case('select')

                                    <select class="form-control {{$field->attributes()->defaultClass}} {{ ($disabled == '') ? 'not-disabled-field' : '' }}"
                                            name="{{$field->attributes()->name}}"
                                            @if($field->attributes()->dataType)
                                                data-type="{{$field->attributes()->dataType}}"
                                            @endif
                                            {{ (!empty($onlyView) && $onlyView === true) ? 'disabled' : '' }}>
                                        <option value="" selected>select</option>

                                        @if(count($refData) > 0)
                                            @foreach($refData as $rVal)
                                                <option {{(!empty(empty($storeValue) && $field->attributes()->defaultValue) && ($field->attributes()->defaultValue ==  $rVal->id))? 'selected' : ''}}  {{(!empty($safData) && ($storeValue == $rVal->code) ? 'selected' : '')}} value="{{$rVal->code}}">{{$rVal->name}}</option>
                                            @endforeach
                                        @endif

                                    </select>

                                    @break

                                @case('input')

                                    <?php

                                    $defaultValue = '';
                                    if ($field->attributes()->defaultValue) {
                                        $defaultValueParts = explode('::', $field->attributes()->defaultValue);

                                        switch ($defaultValueParts[0]) {
                                            case 'global':
                                                $defaultValue = $defaultValueParts[1];

                                                if ($defaultValueParts[1] == 'regularNumber') {
                                                    $defaultValue = $saf->regular_number;
                                                }

                                                if ($defaultValueParts[1] == 'safDate') {
                                                    $defaultValue = $saf->created_at->format('Y-m-d');
                                                }

                                                if ($defaultValueParts[1] == 'type') {
//                                                    $defaultValue = trans('swis.single_application.regime.'.$regime.'.title');
                                                    $defaultValue = $regime;
                                                }

                                                break;

                                            case 'user':

                                                switch ($defaultValueParts[1]){
                                                    case 'passport':
                                                        $defaultValue = @\Illuminate\Support\Facades\Auth::user()->getCreator()->ssn ?? '';
                                                        break;
                                                    case 'name':
                                                        $defaultValue = @\Illuminate\Support\Facades\Auth::user()->getCreator()->name ?? '';
                                                        break;
                                                    case 'address':
                                                        $defaultValue = @\Illuminate\Support\Facades\Auth::user()->getCreator()->address ?? '';
                                                        break;
                                                    case 'phone_number':
                                                        $defaultValue = @\Illuminate\Support\Facades\Auth::user()->getCreator()->phone_number ?? '';
                                                        break;
                                                    case 'email':
                                                        $defaultValue = @\Illuminate\Support\Facades\Auth::user()->getCreator()->email ?? '';
                                                        break;
                                                }

                                        }

                                    }

                                        $phoneNumberFields = [
                                            'saf[sides][import][phone_number]',
                                            'saf[sides][export][phone_number]',
                                            'saf[sides][applicant][phone_number]',
                                        ];
                                    ?>

                                    @if (in_array($field->attributes()->name, $phoneNumberFields))
                                        <div class="input-group">
                                            <span class="input-group-addon {{$field->attributes()->defaultClass}} {{ ($disabled == '') ? 'not-disabled-field' : '' }}">+</span>
                                            <input type="number"
                                                   class="form-control {{$field->attributes()->defaultClass}} {{ ($disabled == '') ? 'not-disabled-field' : '' }}"
                                                   name="{{$field->attributes()->name}}"
                                                   value="{{(empty($safData)) ? (!empty($raw->{$name})) ? $raw->{$name} : $defaultValue : $storeValue}}"
                                                   autocomplete="nope"
                                                   id="{{$field->attributes()->defaultID or ''}}"
                                                   data-name="{{$field->attributes()->dataName or ''}}"
                                                   {{$field->attributes()->readOnly or ''}}>
                                        </div>
                                    @else
                                        <?php
                                            if (empty($safData)) {
                                                $value = (!empty($raw->{$name})) ? $raw->{$name} : $defaultValue;
                                            } else {
                                                $value = $storeValue;
                                                if ($name == 'saf.general.sub_application_number') {
                                                    $value = $subApplication->document_number or '';
                                                }

                                                if ($name == 'saf.general.subapplication_submitting_date') {

                                                    if(!empty($safData['saf.general.subapplication_submitting_date.date'])){
                                                        $value = \Carbon\Carbon::parse($safData['saf.general.subapplication_submitting_date.date'])->format('Y-m-d H:i:s');
                                                    }else{
                                                        $value = $subApplication->status_date or '';
                                                    }
                                                }

                                                if ($name == 'saf.general.sub_application_access_token') {
                                                    $value = $subApplication->access_token or '';
                                                }
                                            }
                                        ?>
                                        <input {{ ($disabled == '') ? '' : $field->attributes()->readOnly or ''}}
                                               value="{{ $value }}"
                                               type="{{($field->attributes()->inputType) ? $field->attributes()->inputType : 'text'}}"
                                               name="{{$field->attributes()->name}}"
                                               id="{{$field->attributes()->defaultID or ''}}"
                                               @if($field->attributes()->dataType) data-type="{{$field->attributes()->dataType}}" @endif
                                               data-name="{{$field->attributes()->dataName or ''}}"
                                               class="form-control {{$field->attributes()->defaultClass}} {{ ($disabled == '') ? 'not-disabled-field' : '' }}"
                                               autocomplete="nope"
                                               {{ (!empty($onlyView) && $onlyView === true) ? 'disabled' : '' }}>
                                    @endif

                                    @break

                                @case('textarea')
                                    <textarea rows="5" cols="5"
                                              class="form-control {{ ($disabled == '') ? 'not-disabled-field' : '' }}"
                                              {{$field->attributes()->readOnly or ''}}
                                              name="{{$field->attributes()->name}}"
                                              {{ (!empty($onlyView) && $onlyView === true) ? 'disabled' : '' }}>{{!empty($raw->{$name}) ? $raw->{$name} : $storeValue }}</textarea>

                                    @break

                                @case('autocomplete')

                                @if(empty($field->attributes()->refType) || ($field->attributes()->refType != 'input'))

                                    <?php
                                        $rawRef = null;
                                        if (!empty($raw)) {

                                            if (!empty($field->attributes()->ref)) {

                                                $refName = 'reference_' . env($field->attributes()->ref);
                                                $refNameMl = 'reference_' . env($field->attributes()->ref) . '_ml';
                                            }

                                            if (!empty($field->attributes()->mdm)) {
                                                $dataModel = \App\Models\DataModel\DataModel::where(DB::raw('id::varchar'), $field->attributes()->mdm)->first();

                                                if(!empty($dataModel->reference)){
                                                    $refName = 'reference_' . $dataModel->reference;
                                                    $refNameMl = 'reference_' . $dataModel->reference . '_ml';
                                                }
                                            }

                                            $rawRef = \Illuminate\Support\Facades\DB::table($refName)
                                                ->join($refNameMl, $refName . '.id', '=', $refNameMl . '.' . $refName . '_id')->where($refNameMl . '.lng_id', '=', cLng('id'))
                                                ->where('code', $raw->{$name})
                                                ->first();

                                        }

                                        $refSelectOptionColumns = null;
                                        if(!empty($field->attributes()->refSelectOptionColumns)){
                                            $refSelectOptionColumns = explode(',',$field->attributes()->refSelectOptionColumns);
                                        }

                                        $addDissable = '';
                                        if (!empty($regime) && $regime == 'import' && (string)$field->attributes()->name == 'saf[transportation][import_country]') {
                                            $addDissable = "disabled";
                                        } elseif (!empty($regime) && $regime == 'export' && (string)$field->attributes()->name == 'saf[transportation][export_country]') {
                                            $addDissable = "disabled";
                                        }

                                    ?>

                                    <select
                                            {{ $addDissable }}
                                            {{ (!empty($onlyView) && $onlyView === true) ? 'disabled' : '' }}
                                            {{$field->attributes()->readOnly or ''}}
                                            class="form-control  autocomplete {{ ($disabled == '') ? 'not-disabled-field' : '' }} {{(!empty($field->attributes()->ajax)) ? 'select2-ajax select2-ajax-saf' : 'select2'}} {{$field->attributes()->defaultClass}}"
                                            name="{{$field->attributes()->name}}"
                                            @if(!empty($field->attributes()->ajax) && (string)$field->attributes()->ajax == 'true')
                                            data-url="{{urlWithLng('/reference-table/ref')}}"
                                            data-ref="{{\App\Models\ReferenceTable\ReferenceTable::getReferenceIdByTableName(env($field->attributes()->ref))}}"
                                            @endif
                                            >

                                        <option value=""></option>

                                        @if(!empty($raw) && !is_null($rawRef))

                                            <?php

                                            $rawOptionVal = '('.$rawRef->code.')'.' '.$rawRef->name;

                                            if(!empty($refSelectOptionColumns)){

                                                if(!empty($refSelectOptionColumns[0])){
                                                    $rCode = $refSelectOptionColumns[0];
                                                }

                                                if(!empty($refSelectOptionColumns[1])){
                                                    $rName = $refSelectOptionColumns[1];
                                                }


                                                if(!empty($rawRef->$rCode) && !empty($rawRef->$rName)){

                                                    $rawOptionVal = '('.$rawRef->$rCode.')'.' '.$rawRef->$rName;
                                                }
                                            }

                                            ?>

                                            <option selected value="{{@$rawRef->code}}">{{(!empty($field->attributes()->ajax)) ? @$rawRef->code : ''}} {{$rawOptionVal}} </option>
                                        @endif

                                        @if(count($refData) > 0)
                                            @foreach($refData as $rVal)
                                                @if(!empty($raw) && !empty($rawRef) && $rawRef->code == $rVal->code)
                                                    @continue
                                                @endif
                                                @if (!empty($regime) && $regime == 'import' && (string)$field->attributes()->name == 'saf[transportation][export_country]')
                                                    @if (env('COUNTRY_MODE') == $rVal->code)
                                                        @continue
                                                    @endif
                                                @endif
                                                @if(!empty($regime) && $regime == 'export' && (string)$field->attributes()->name == 'saf[transportation][import_country]')
                                                    @if (env('COUNTRY_MODE') == $rVal->code)
                                                        @continue
                                                    @endif
                                                @endif

                                                @php($selected = '')

                                                @if (!empty($regime) && $regime == 'import' && env('COUNTRY_MODE') == $rVal->code && (string)$field->attributes()->name == 'saf[transportation][import_country]')
                                                    @php($selected = 'selected')
                                                @endif
                                                @if (!empty($regime) && $regime == 'export' && env('COUNTRY_MODE') == $rVal->code && (string)$field->attributes()->name == 'saf[transportation][export_country]')
                                                    @php($selected = 'selected')
                                                @endif

                                                    <?php
                                                    $refOptionVal = '('.$rVal->code.') '.$rVal->name;

                                                    if(!empty($refSelectOptionColumns)){

                                                        if(!empty($refSelectOptionColumns[0])){
                                                            $rCode = $refSelectOptionColumns[0];
                                                        }

                                                        if(!empty($refSelectOptionColumns[1])){
                                                            $rName = $refSelectOptionColumns[1];
                                                        }


                                                        if(!empty($rVal->$rCode) && !empty($rVal->$rName)){
                                                            $refOptionVal = '('.$rVal->$rCode.')'.' '.$rVal->$rName;
                                                        }
                                                    }

                                                    ?>

                                                <option
                                                    {{(!empty($safData) && ($storeValue == $rVal->id)) ? 'selected' : $selected}}
                                                    {{(!empty($raw) && ($raw->{$name} == $rVal->id)) ? 'selected' : ''}}
                                                    value="{{$rVal->code}}">
                                                   {{$refOptionVal}}
                                                </option>
                                            @endforeach
                                        @endif
                                    </select>

                                @elseif(($field->attributes()->refType != 'input'))

                                    <input value="{{$raw->{$name} or ''}}"
                                           {{$field->attributes()->readOnly or ''}}
                                           type="{{($field->attributes()->inputType) ? $field->attributes()->inputType : 'text'}}"
                                           name="{{$field->attributes()->name}}"
                                           placeholder="autocomplete"
                                           class="form-control autocomplete {{$field->attributes()->defaultClass}}"
                                           @if($field->attributes()->dataType) data-type="{{$field->attributes()->dataType}}" @endif
                                           autocomplete="nope"
                                           data-ref="{{\App\Models\ReferenceTable\ReferenceTable::getReferenceIdByTableName(env($field->attributes()->ref))}}"
                                           {{ (!empty($onlyView) && $onlyView === true) ? 'disabled' : '' }}>

                                @endif

                                @break

                                @case('hidden')

                                    <input value="{{!empty($raw->{$name}) ? $raw->{$name} : $field->attributes()->defaultValue }} "
                                           type="hidden"
                                           name="{{$field->attributes()->name}}"
                                           {{ (!empty($onlyView) && $onlyView === true) ? 'disabled' : '' }}>
                                    @break


                                @case('autoincrement')

                                    <?php
                                    $incVal = @$raw->{$name};

                                    if(!empty($availableProductsIdNumbers))    {
                                        if(isset($availableProductsIdNumbers[$raw->id])){
                                            $incVal = $availableProductsIdNumbers[$raw->id];
                                        }
                                    }

                                    ?>
                                    <input value="{{$incVal}}"
                                           type="{{($field->attributes()->inputType) ? $field->attributes()->inputType : 'text'}}"
                                           readonly
                                           class="form-control autoincrement"
                                           name="{{$field->attributes()->name}}"
                                           {{ (!empty($onlyView) && $onlyView === true) ? 'disabled' : '' }}>
                                    @break

                                @case('checkbox')
                                    <input type="checkbox"
                                           class="{{ ($disabled == '') ? 'not-disabled-field' : '' }}"
                                           name="{{$field->attributes()->name}}"
                                           {{ (!empty($onlyView) && $onlyView === true) ? 'disabled' : '' }}
                                    >
                                    @break

                            @endswitch

                            <?php
                                $name = str_replace(']', '', $name);
                                $name = str_replace('[', '-', $name);

                                $errorName = str_replace('.', '-', $name)
                            ?>

                            <div class="form-error" id="form-error-{{$errorName}}"></div>

                            @if(empty($fieldSet->field->attributes()->mdm) && $field->attributes()->type != 'hidden')
                                <b style="color: red">Need add MDM field id {{ $fieldSet->field->attributes()->inputType}}</b>
                            @endif
                        </div>

                    @endforeach

                    @if($fieldSet->attributes()->multiple)
                        <button type="button" class="btn btn-primary plusMultipleFields"><i class="fa fa-plus"></i></button>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endforeach

@if(!empty($block->subBlock))

    <?php
        $dataValues = [];
    ?>
    @if(!empty($block->subBlock->attributes()->DbTableName) &&  !empty($edit))

        <?php

            $columns = (array)$block->subBlock->columns;

            $columnsActions = $block->subBlock->columnsActions;

            $dataValues = \Illuminate\Support\Facades\DB::table((string)$block->subBlock->attributes()->DbTableName)->select($columns);

            if (!empty($block->subBlock->attributes()->whereKey)) {
                $dataValues->where($block->subBlock->attributes()->whereKey, $rawId);
            }

            $dataValues = $dataValues->orderBy('id')->get()->toArray();

            $simpleAppTabs = \App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure::lastXml();

            $subBlock = $simpleAppTabs->xpath('/tabs/tab[@key="' . $block->subBlock->attributes()->parentModule . '"]/block/subBlock[@renderTr]');

        ?>

    @endif

    {{-- <h3 class="collapseWithIcon" data-toggle="collapse" data-target="#goodsProductListsSections{{(!empty($raw) ? 'Edit': '')}}">{{ trans('swis.products.more_info.title') }} <i class="fas fa-chevron-down"></i></h3>

 <div class="collapse in" id="goodsProductListsSections{{(!empty($raw) ? 'Edit': '')}}">
     <hr/>

     <h3 class="collapseWithIcon" data-toggle="collapse" data-target="#goodsProductBatch{{(!empty($raw) ? 'Edit': '')}}">{{ trans('swis.single_app.batches') }} <i class="fas fa-chevron-down"></i></h3>

     <div class="collapse in" id="goodsProductBatch{{(!empty($raw) ? 'Edit': '')}}">--}}
    <div class="collapse in" id="goodsProductBatch{{(!empty($raw) ? 'Edit': '')}}">--}}

        <h3 class="fb batches--main-title ">{{ trans('swis.products.more_info.title') }}</h3>
        <hr/>
        <div class="batches--block">
            <h4 class="fb text--batches">{{ trans('swis.single_app.batches') }}</h4>

            <div>

            @foreach($block->subBlock as $block)

             <table class="table table-bordered {{$block->attributes()->tableClass}}"
                    data-module="{{(empty($block->attributes()->tableClass)) ? $block->attributes()->module : $block->attributes()->tableClass}}"
                    data-sub-block="true">
                 <thead>
                     <tr>
                         @foreach($block->fieldset as $column)
                             <?php
                                 $listControlled = listControlled((string)$column->field->attributes()->id, $globalHiddenFields, $lists, 'saf', $mandatoryListForView);

                                 if ($listControlled['continue']) { continue; }
                             ?>
                             <td>{{trans($column->field->attributes()->title)}}</td>
                         @endforeach
                         @if(!empty($userDocumentFields))
                             @foreach($userDocumentFields as $userDocumentField)
                                 @if ($userDocumentField['xpath'] != "tab[@key='products']/block[@name='swis.single_app.products_list']/subBlock[@name='swis.single_app.batches']")
                                     @continue;
                                 @endif
                                     <?php
                                         $listControlled = listControlled($userDocumentField['field_id'], $globalHiddenFields, $lists, 'mdm', $mandatoryListForView);

                                         if ($listControlled['continue']) { continue; }
                                         $requiredField = $listControlled['requiredField'];
                                         $disabled      = $listControlled['disabled'];
                                     ?>
                                 <td>{{isset($userDocumentField['mls'][cLng()]) ? $userDocumentField['mls'][cLng()]['tooltip'] : ''}}</td>
                             @endforeach
                         @endif
                     </tr>
                 </thead>
                 <tbody>
                     @if(empty($block->attributes()->draw) || $block->attributes()->draw != 'false')
                         @include('user-documents.products.table-tr',['block'=>$block])
                     @endif

                     @if(!empty($dataValues))
                         @foreach($dataValues as $key=>$dataValue)
                             @include('user-documents.products.table-tr',['data'=>$dataValue,'block'=>$subBlock[0],'num'=>$key+1, 'onlyView' => (!empty($onlyView) && $onlyView === true), 'isApplication' => true])
                         @endforeach
                     @endif
                 </tbody>
             </table>

             @if($block->attributes()->multiple)
                 @if (!empty($onlyView) && $onlyView === true)
                     @continue
                 @endif

                 <br/>
                 <button type="button" class="btn btn-primary {{(!empty($block->attributes()->subForm)) ? 'subForm' : ''}} {{(!empty($block->attributes()->renderTr)) ? 'renderTr' : ''}}"><i class="fa fa-plus"></i></button>
             @endif

         @endforeach
     </div>
 </div>

 <hr/>
@endif