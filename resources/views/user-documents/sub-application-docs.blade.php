@extends('layouts.app')

@section('title'){{trans('swis.single_application.title')}}@stop

@section('contentTitle')
    {{trans('swis.application_documents.title',['safNumber' => $subApplicationNumber])}}
@stop

@section('content')

    <div class="sub-application-docs">

        <ul class="nav nav-tabs" style="margin-bottom: 20px;">
            <li class="active"><a data-toggle="tab" href="#tab-general">{{trans('core.base.tab.general')}}</a></li>
            <li><a data-toggle="tab" href="#tab-notes">{{trans('swis.subapplication.info.notes')}}</a></li>
        </ul>

        <div class="tab-content">

            <div id="tab-general" class="tab-pane active">
                <h2>&#8470;  {{$subApplication->type}} - <span title="{{$subApplication->long_name}}">{{$subApplication->name}}</span></h2>
                <table class="table table-bordered table-hover m-b-none">
                    <thead>
                    <tr>
                        <th></th>
                        <th>{{trans('swis.electronic_document.name.title')}}</th>
                        <th>{{trans('swis.electronic_document.type.title')}}</th>
                        <th>{{trans('swis.electronic_document.number.title')}}</th>
                        <th>{{trans('swis.electronic_document.status_date.title')}}</th>
                        <th>{{trans('swis.electronic_document.current_state.title')}}</th>
                        <th>{{trans('swis.electronic_document.current_state_date.title')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="width: 45px">
                            <a href="{{urlWithLng('/application/'.$subApplication->id.'/'.$subApplication->pdf_hash_key)}}"
                               target="_blank">
                                <button class="btn--icon" type="button"
                                        title="{{ trans('swis.base.tooltip.view.button') }}"
                                        data-toggle="tooltip"><i class="fas fa-eye"></i></button>
                            </a>
                        </td>
                        <td style="width: 550px">{{$subApplication->name}}</td>
                        <td style="width: 75px">{{$subApplication->type}}</td>
                        <td style="width: 198px">{{$subApplication->document_number}}</td>
                        <td style="width: 254px">{{formattedDate($subApplication->status_date)}}</td>
                        <td style="width: 190px">{{$subApplication->state}}</td>
                        <td style="width: 220px">{{formattedDate($subApplication->current_state_date)}}</td>
                    </tr>
                    </tbody>
                </table>
                <?php $subApplicationDocuments = $subApplication->subApplicationDocuments() ?>
                @if($subApplicationDocuments->count())
                    @php $i=1 @endphp
                    <br/>
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th style="width: 45px"></th>
                            <th>{{trans('swis.document.document_form')}}</th>
                            <th>{{trans('swis.document.type')}}</th>
                            <th>{{trans('core.base.label.name')}}</th>
                            <th>{{trans('core.base.description')}}</th>
                            <th>{{trans('swis.document.number')}}</th>
                            <th>{{trans('swis.document.release_date')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($subApplicationDocuments as $document)
                            <tr>
                                <td style="width: 45px" class="text-center"> {{ $i++ }}</td>
                                <td style="width: 45px">
                                    @if($document->filePath())
                                        <a target="_blank" href="{{$document->filePath()}}"
                                           class="btn btn-success btn-xs btn--icon" title="{{ trans('swis.base.tooltip.view.button') }}" data-toggle="tooltip"><i class="fas fa-eye"></i></a>
                                    @endif
                                </td>
                                <td style="width: 270px">{{trans('swis.document.document_form.'.$document->document_form.'.title')}}</td>
                                <td style="width: 235px">{{$document->document_type}}</td>
                                <td style="width: 273px">{{$document->document_name}}</td>
                                <td style="width: 254px">{{$document->document_description}}</td>
                                <td style="width: 190px">{{$document->document_number}}</td>
                                <td style="width: 220px">{{$document->document_release_date}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
                <hr/>
            </div>
            <div id="tab-notes" class="tab-pane">

                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1">

                        <div class="ibox-content no-padding notes__out">
                            <ul class="list-group notes-block notes__list">

                                @foreach ($notes as $note)
                                    <li class="list-group-item notes__item">
                                        <div class="notes__item--inner">
                                            <div class="table-cell notes__item--left">
                                                <p>{!! $note->note !!}</p>
                                            </div>
                                            <div class="table-cell text-right notes__item--right">
                                                <small class="block text-muted"><i
                                                            class="fa fa-clock-o"></i> {{ $note->created_at }}
                                                </small>
                                                @if ($note->to_saf_holder)
                                                    <small class="text-muted"><i
                                                                class="fas fa-envelope"></i> {!! $note->sent_to_saf_holder !!}
                                                    </small>
                                                @endif
                                            </div>
                                        </div>
                                    </li>
                                @endforeach

                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

@endsection

@section('scripts')
    <script>
        $('.gray-bg').css("min-height", $(window).height() + "px");
    </script>
@endsection