@extends('layouts.app')

@section('title'){{$documentName}}@stop

@section('contentTitle') {{$documentName}} @stop

@section('topScripts')
    <script>
        var showUserMappingList = false;
        var subApplicationType = '';
    </script>
@endsection

@section('content')

    <div class="clearfix search__out">
        <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse" data-target="#collapseSearch" aria-expanded="false" aria-controls="collapseSearch">
            <i class="fa fa-search"> </i><span class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
        </button>
    </div>

    <div class="collapse collapseSearch" id="collapseSearch">
        <div class="card card-body">
            <form class="form-horizontal fill-up" id="search-filter">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group searchParam state_id_block_params" data-field="state_id_block_params">
                            <label class="col-md-4 col-lg-4 control-label label__title">{{trans('swis.constructor_document.state.label')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 field__one-col">
                                        <select class="select2" name="state_id">
                                            <option value=""></option>
                                            @if($documentStates->count())
                                                @foreach($documentStates as $documentState)
                                                    <option value="{{$documentState->id}}">{{$documentState->state_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        @if($searchBySuperscribeField)
                            <div class="form-group searchParam superscribe_id_block_params" data-field="superscribe_id_block_params">
                                <label class="col-md-4 col-lg-4 control-label label__title">{{trans('swis.constructor_document.superscription.label')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-12 field__one-col">
                                            <select class="select2" name="superscribe_id">
                                                <option value=""></option>
                                                @if($usersSuperscribe->count())
                                                    @foreach($usersSuperscribe as $user)
                                                        <option value="{{$user->id}}">{{$user->name()}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam unsuperscribe_sub_applications_block_params" data-field="unsuperscribe_sub_applications_block_params">
                                <label class="col-md-4 col-lg-4 control-label label__title">{{trans('swis.constructor_document.unsuperscribe_sub_applications.label')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-12 field__one-col">
                                            <input type="checkbox" name="unsuperscribe_sub_applications" value="1">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endif
                    </div>
                </div>

                <div class="row">

                    @if ($searchFields && count($searchFields) > 0)
                        @if (!empty($searchFields['text']))
                            <?php
                            $textFieldsSize = count($searchFields['text']) < 2 ? 1 : count($searchFields['text']) / 2;
                            ?>
                            @foreach (array_chunk($searchFields['text'], (int)ceil($textFieldsSize)) as $key => $items)
                                <div class="col-md-6">
                                    @foreach ($items as $field)
                                        @if($field['name'] == 'saf[general][regime]')
                                            <div class="form-group searchParam {{ $field['field_id'] }}_block_params" data-field="{{ $field['field_id'] }}_block_params">
                                                <label class="col-md-4 control-label label__title">{{ $field['title'] }}</label>
                                                <div class="col-md-8">
                                                    <div class="row">
                                                        <div class="col-md-12 field__one-col">
                                                            <select class="select2" name="{{ $field['field_id'] }}">
                                                                <option></option>
                                                                @foreach(\App\Models\SingleApplication\SingleApplication::REGIMES as $regime)
                                                                    <option value="{{$regime}}">{{trans('swis.my_application.search.'. $regime)}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @elseif ($field['type']['type'] == 'autocomplete')
                                            @if (array_key_exists($field['type']['reference'], $referenceWithAgencyColumn))
                                                <?php
                                                    if ($field['field_id'] == 'SAF_ID_141') {
                                                        $constructorDocument = App\Models\ConstructorDocument\ConstructorDocument::select('id', 'document_type_id', 'company_tax_id', 'document_code')->where('id', $documentId)->first();
                                                        $referenceDocumentType = getReferenceRows(App\Models\ReferenceTable\ReferenceTable::REFERENCE_DOCUMENT_TYPE_REFERENCE, $constructorDocument->document_type_id, false, ['document_type']);
                                                        $referenceClassificator = getReferenceRows(App\Models\ReferenceTable\ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS, $referenceDocumentType->document_type);

                                                        $queryExpressWhere = function ($query) use ($referenceClassificator) {
                                                            return $query->where("document", $referenceClassificator->id ?? 0)->orWhereNull('document');
                                                        };

                                                        $getResult = getReferenceRows(App\Models\ReferenceTable\ReferenceTable::REFERENCE_EXPRESS_CONTROL, false, false, ['id', 'code', 'name'], 'get', false, false, $queryExpressWhere);
                                                    } elseif ($field['field_id'] == 'SAF_ID_142') {
                                                        $constructorDocument = App\Models\ConstructorDocument\ConstructorDocument::select('id', 'document_type_id', 'company_tax_id', 'document_code')->where('id', $documentId)->first();
                                                        $availableTaxIds = \App\Models\RegionalOffice\RegionalOffice::select('office_code')
                                                            ->join('regional_office_constructor_documents', function ($q) use ($constructorDocument) {
                                                                $q->on('regional_office_constructor_documents.regional_office_id', '=', 'regional_office.id')->where('constructor_document_code', $constructorDocument->document_code);
                                                            })
                                                            ->pluck('office_code')->all();

                                                        $where = function ($q) use ($availableTaxIds, $activeRefAgencyRow) {
                                                            $q->whereIn('code', $availableTaxIds)->where('agency', optional($activeRefAgencyRow)->id);
                                                        };

                                                        $getResult = getReferenceRows($field['type']['reference'], false, false, ['id', 'code', 'name'], 'get', false, false, $where);
                                                    } else {
                                                        $getResult = getReferenceRows($field['type']['reference'], false, false, ['id', 'code', 'name'], 'get', false, false, ['agency' => optional($activeRefAgencyRow)->id]);
                                                    }
                                                ?>
                                            @else
                                                <?php
                                                    $getResult = [];
                                                    if (getReferenceRows($field['type']['reference'], false, false, ['id', 'code', 'name'], 'count')  <= 250) {
                                                        $getResult = getReferenceRows($field['type']['reference'], false, false, ['id', 'code', 'name']);
                                                    }
                                                ?>
                                            @endif

                                            <?php
                                            if ($field['mdm_type'] == \App\Models\DataModel\DataModel::MDM_ELEMENT_TYPE_MULTIPLE) {
                                                $fieldAttributes = 'multiple';
                                            } else {
                                                $appendName = $fieldAttributes ='';
                                            }
                                            ?>
                                            <div class="form-group searchParam {{ $field['field_id'] }}_block_params" data-field="{{ $field['field_id'] }}_block_params">
                                                <label class="col-md-4 control-label label__title">{{ $field['title'] }}</label>
                                                <div class="col-md-8">
                                                    <div class="row">
                                                        <div class="col-md-12 field__one-col">
                                                            @if($getResult && count($getResult) <= 250)
                                                                <select class="select2" {{ $fieldAttributes }}
                                                                name="{{ $field['field_id'] }}"
                                                                        data-select2-allow-clear="true">
                                                                    <option></option>
                                                                    @foreach($getResult as $result)
                                                                        <option value="{{$result->id}}">{{ "(" . $result->code . ") " . $result->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                            @else
                                                                <select
                                                                        {{ $fieldAttributes }}
                                                                        class="select2-ajax"
                                                                        data-toggle="tooltip"
                                                                        id="select2-{{$field['type']['reference']}}-{{$field['field_id']}}"
                                                                        data-url="{{urlWithLng('user-documents/reference/'.$field['type']['reference'])}}"
                                                                        data-ref="{{$field['type']['reference']}}"
                                                                        data-min-input-length="1"
                                                                        data-one-and-more="true"
                                                                        data-field-id="{{ $field['field_id'] }}"
                                                                        name="{{ $field['field_id'] }}">
                                                                </select>
                                                            @endif
                                                            <div class="form-error" id="form-error-{{ $field['name'] }}"></div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        @else
                                            <div class="form-group searchParam {{ $field['field_id'] }}_block_params" data-field="{{ $field['field_id'] }}_block_params">
                                                <label class="col-md-4 control-label label__title">{{ $field['title'] }}</label>
                                                <div class="col-md-8">
                                                    <div class="row">
                                                        <div class="col-md-12 field__one-col">
                                                            <input type="{{ $field['type']['type'] }}" class="form-control" name="{{$field['field_id']}}" value="{{ old($field['name']) }}">

                                                            <div class="form-error" id="form-error-{{ $field['name'] }}"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            @endforeach
                        @endif
                    @endif
                </div>
                <?php
                $dateFieldsSize = count($searchFields['date'] ?? []) < 2 ? 1 : count($searchFields['date'] ?? []) / 2;
                ?>
                @if ($searchFields && count($searchFields) > 0)
                    @if (!empty($searchFields['date']))
                        @foreach (array_chunk($searchFields['date'], (int)ceil($dateFieldsSize)) as $key => $items)
                            <div class="col-md-6">
                                @foreach ($items as $field)
                                    <div class="form-group searchParam payment_date_row row {{ $field['field_id'] }}_block_params" data-field="{{ $field['field_id'] }}_block_params">
                                        <label class="col-md-4 control-label label__title">{{ $field['title'] }}</label>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-6 field__two-col">
                                                    <div class='input-group date'>
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                        <input value="" name="{{$field['field_id']}}_start_date_filter"
                                                               type="text"
                                                               placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                                    </div>
                                                    <div class="form-error" id="form-error-{{ $field['name'] }}"></div>
                                                </div>
                                                <span style=" position: absolute; top: 6px;margin-left: -5px;">-</span>
                                                <div class="col-md-6 field__two-col">
                                                    <div class='input-group date'>
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                        <input value=""  name="{{$field['field_id']}}_end_date_filter"
                                                               type="text"
                                                               placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                                    </div>

                                                    <div class="form-error" id="form-error-{{ $field['name'] }}"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                    @endif
                @endif

                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8 col-lg-offset-3 text-right">
                        <div class="row">
                            <div class="col-lg-10 col-md-8">
                                <button type="submit"
                                        class="btn btn-primary mr-10">{{trans('core.base.label.search')}}</button>
                                <button type="button" class="btn btn-danger mr-10 resetButton"
                                        data-submit="true">{{trans('core.base.label.reset')}}</button>
                                <button class="btn btn-default hide-or-show-params" type="button" style="margin-top: 10px;"
                                        data-submit="false">{{trans('core.base.label.apply_hidden_or_show_columns')}}</button>
                                <a href="{{ urlWithLng("/user-documents/{$documentId}/generate/xls") }}" class="btn btn-default m-l-sm generate-xls" style="margin-top: 10px;">{{trans('swis.payment-and-obligation.xls.title')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" value="{{$searchBySuperscribeField}}" class="default-search" name="search_by_superscribe_field">
            </form>
        </div>
    </div>

    <?php
        $searchColumns = ['state' => trans('swis.user_documents.state.label')];
        if ($searchBySuperscribeField) {
            $searchColumns['superscribe_field'] = trans('swis.user_documents.superscribe_field.label');
        }
        foreach ($searchResultFields as $field) {
            $searchColumns[$field['xml_field_name']] = !empty(optional($field->current)->label) ? optional($field->current)->label : trans($field['title_trans_key']);
        }
        foreach ($searchResultFieldsMdm as $field) {
            if (isset($field['current'])) {
                $searchColumns[$field['field_id']] = trans($field['current']['label']);
            }
        }
    ?>

    @php($companyTaxId = Auth::user()->companyTaxId())
    <div class="data-table">
        <div class="clearfix">

            <table class="table table-striped table-bordered table-hover dataTables-example userDocTable" id="list-data" data-action="user-documents/{{$documentId}}/table" data-search-action="user-documents/{{$documentId}}/application" data-module-for-search-params="UserDocuments{{ hash('sha256', $companyTaxId . $documentId) }}">
                <thead>
                    <tr>

                        <th style="min-width: 70px;" data-col-title="{{trans('core.base.label.actions')}}" data-delete="0" data-edit="1" data-col="actions" class="th-actions" id="actions_search_column">{{trans('core.base.label.actions')}}</th>

                        <th data-col-title="{{trans('swis.user_documents.state.label')}}" data-col="state" data-order-name="state" id="state_search_column">{{trans('swis.user_documents.state.label')}}</th>

                        @if($searchBySuperscribeField)
                            <th data-ordering="0" data-col-title="{{ trans('swis.user_documents.superscribe_field.label') }}" data-col="superscribe_field" data-order-name="superscribe_field" id="superscribe_field_search_column">{{trans('swis.user_documents.superscribe_field.label')}}</th>
                        @endif
                        <!-- Fields from saf -->
                        @foreach($searchResultFields as $field)
                            <?php
                                $tooltip = optional($field->current)->tooltip;
                                $label = optional($field->current)->label;
                            ?>
                            <th data-col-title="{{ !empty($tooltip) ? $tooltip : trans($field['title_trans_key']) }}" data-col="{{ $field['xml_field_name'] }}" data-order-name="{{ $field['orderFieldName'] }}" id="{{ $field['xml_field_name'] }}_search_column">{{ !empty($label) ? $label : trans($field['title_trans_key']) }}</th>
                        @endforeach
                        <!-- End fields from saf -->

                        <!-- Fields from mdm -->
                        @foreach($searchResultFieldsMdm as $field)
                            @if (isset($field['current']))
                                <th data-col-title="{{ trans($field['current']['tooltip'] ?? '') }}" data-col="{{ $field['field_id'] }}" data-order-name="{{ $field['orderFieldName'] }}" id="{{ $field['field_id'] }}_search_column">{{ trans($field['current']['label'] ?? '') }}</th>
                            @endif
                        @endforeach
                        <!-- End fields from saf -->

                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset("js/user-documents/main.js")}}"></script>
    <script src="{{asset("js/user-documents/obligation.js")}}"></script>
    <script src="{{asset("js/user-documents/old-laboratories.js")}}"></script>
    <script src="{{asset("js/user-documents/laboratories.js")}}"></script>
@endsection