<div class="row">
    <div class="col-lg-10 col-lg-offset-1">

        <div class="ibox-content no-padding notes__out">
            <ul class="list-group notes-block notes__list">
                <li class="list-group-item notes__item">

                    <b>{{ trans('swis.saf.notes.current_state') }}
                        :</b> {{ $documentCurrentState->state->current()->state_name }}
                </li>

                @foreach ($notes as $note)
                    <li class="list-group-item notes__item">
                        <div class="notes__item--inner">
                            <div class="table-cell notes__item--left">
                                <p>{!! $note->note !!}</p>
                            </div>
                            <div class="table-cell text-right notes__item--right">
                                <small class="block text-muted"><i class="fa fa-clock-o"></i> {{ $note->created_at }}
                                </small>
                                @if ($note->to_saf_holder)
                                    <small class="text-muted"><i
                                                class="fas fa-envelope"></i> {!! $note->sent_to_saf_holder !!}</small>
                                @endif
                            </div>
                        </div>
                    </li>
                @endforeach

            </ul>
        </div>
    </div>
</div>