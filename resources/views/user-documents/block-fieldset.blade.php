@if($key == 'notes')
    @include('user-documents.notes.saf-notes-actions')
@endif

@if (isset($block['fields']) && count($block['fields']) > 0)
    @foreach ($block['fields'] as $field)
        <div class="form-group {{ $field['fieldSetClass'] }}">
            <label class="col-md-4 col-lg-3 control-label label__title" data-toggle="tooltip" title="{{ $field['fieldSetTitle'] }}" data-placement="{{ $field['fieldSetTitlePlacement'] }}"> {{ $field['fieldSetLabel'] }} </label>

            <div class="col-md-8">
                <div class="{{ $field['fieldSetMultipleClass'] }}" data-fields="{{ $field['fieldSetData'] }}">
                    <div class="clearfix row">
                        @if ($field['fieldSetOrigin'] == 'mdm')
                            <div class="col-lg-10 col-md-8 field__one-col">
                                @if ($field['fieldType'] == 'autocomplete')
                                    <select
                                            title="{{ $field['fieldTitle'] }}"
                                            class="form-control select2 select2-from-mdm {{ $field['fieldClass'] }} {{ $field['fieldDisabled'] }}"
                                            id="{{ $field['fieldId'] }}"
                                            data-url="{{ $field['fieldDataUrl'] }}"
                                            data-ref="{{ $field['fieldDataRef'] }}"
                                            data-min-input-length="1"
                                            data-one-and-more="true"
                                            data-mdm-select="true"
                                            name="{{ $field['fieldName'] }}"
                                            {{ $field['fieldDisabled'] }}>
                                        <option value="" selected>{{ trans('core.base.label.select') }}</option>
                                        @if (isset($field['options']))
                                            @foreach ($field['options'] as $option)
                                                <option {{ $option['selected'] }} value="{{ $option['value'] }}">{{ $option['name'] }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                @else
                                    <input data-toggle="tooltip"
                                           title="{{ $field['fieldTitle'] }}"
                                           type="{{ $field['fieldType'] }}"
                                           class="form-control agencyInput {{ $field['fieldDisabled'] }}"
                                           name="{{ $field['fieldName'] }}"
                                           value="{{ $field['fieldValue'] }}"
                                            {{ $field['fieldDisabled'] }}>
                                @endif

                                <div class="form-error" id="{{ $field['fieldErrorName'] }}"></div>
                            </div>

                        @else

                            @foreach ($field['fieldInFieldSet'] as $fieldInFieldSet)

                                <div class="{{ $fieldInFieldSet['fieldBlockClass'] }}">
                                    @switch($fieldInFieldSet['fieldType'])

                                        @case('select')
                                        <select class="form-control select2 {{ $fieldInFieldSet['fieldClass'] }} {{ $fieldInFieldSet['fieldReadOnly'] }} {{ $fieldInFieldSet['fieldDisabled'] }}" name="{{ $fieldInFieldSet['fieldName'] }}" data-type="{{ $fieldInFieldSet['dataAttributes']['dataType'] }}" {{ $fieldInFieldSet['fieldDisabled'] }}>
                                            <option value="" selected>{{ trans('core.base.label.select') }}</option>
                                            @foreach($fieldInFieldSet['options'] as $option)
                                                <option {{ $option['selected'] }} title="{{ $option['name'] }}" value="{{ $option['value'] }}"> {{ $option['name'] }} </option>
                                            @endforeach
                                        </select>
                                        @break

                                        @case('input')
                                        @if ($fieldInFieldSet['fieldIsPhoneNumber'])
                                            <div class="input-group"> <span class="input-group-addon">+</span>
                                                @endif
                                                <input type="{{ $fieldInFieldSet['fieldInputType'] }}"
                                                       class="form-control {{ $fieldInFieldSet['fieldClass'] }} {{ $fieldInFieldSet['fieldReadOnly'] }} {{ $fieldInFieldSet['fieldDisabled'] }}"
                                                       name="{{ $fieldInFieldSet['fieldName'] }}"
                                                       value="{{ $fieldInFieldSet['fieldValue'] }}"
                                                       autocomplete="nope"
                                                       id="{{ $fieldInFieldSet['fieldId'] }}"
                                                       data-name="{{ $fieldInFieldSet['dataAttributes']['dataName'] }}"
                                                       data-type="{{ $fieldInFieldSet['dataAttributes']['dataType'] }}"
                                                        {{ $fieldInFieldSet['fieldDisabled'] }}>
                                                @if ($fieldInFieldSet['fieldIsPhoneNumber'])
                                            </div>
                                        @endif
                                        @break

                                        @case('textarea')
                                        <textarea rows="5" cols="5" class="form-control {{ $fieldInFieldSet['fieldReadOnly'] }} {{ $fieldInFieldSet['fieldDisabled'] }}" {{ $fieldInFieldSet['fieldDisabled'] }} name="{{ $fieldInFieldSet['fieldName'] }}">{{ $fieldInFieldSet['fieldValue'] }}</textarea>
                                        @break

                                        @case('autocomplete')

                                        @if(empty($fieldInFieldSet['refType']) || $fieldInFieldSet['refType'] != 'input')
                                            <select class="form-control autocomplete {{ $fieldInFieldSet['fieldClass'] }} {{ $fieldInFieldSet['fieldReadOnly'] }} {{ $fieldInFieldSet['fieldDisabled'] }}"
                                                    name="{{ $fieldInFieldSet['fieldName'] }}"
                                                    data-url="{{ $fieldInFieldSet['fieldDataUrl'] }}"
                                                    data-ref="{{ $fieldInFieldSet['fieldDataRef'] }}" {{ $fieldInFieldSet['fieldDisabled'] }}>

                                                <option value=""></option>
                                                @foreach($fieldInFieldSet['options'] as $option)
                                                    <option {{ $option['selected'] }} value="{{$option['value']}}"> {{ $option['name'] }} </option>
                                                @endforeach
                                            </select>
                                        @endif

                                        @break

                                        @case('hidden')
                                        <input value="{{ $fieldInFieldSet['fieldValue']  }}" type="hidden" name="{{ $fieldInFieldSet['fieldName'] }}" {{ $fieldInFieldSet['fieldDisabled'] }}>
                                        @break

                                        @case('autoincrement')
                                        <input value="{{ $fieldInFieldSet['fieldValue'] }}" type="{{ $fieldInFieldSet['fieldInputType'] }}" readonly class="form-control autoincrement" name="{{ $fieldInFieldSet['fieldName'] }}" {{ $fieldInFieldSet['fieldDisabled'] }}>
                                        @break

                                        @case('checkbox')
                                        <input type="checkbox"  name="{{ $fieldInFieldSet['fieldName'] }}" {{ $fieldInFieldSet['fieldDisabled'] }}>
                                        @break

                                    @endswitch

                                    <div class="form-error" id="{{ $fieldInFieldSet['fieldErrorName'] }}"></div>

                                    {!! $fieldInFieldSet['fieldNeedMdm'] !!}
                                </div>

                                @if($field['fieldSetMultiple'])
                                    <button type="button" class="btn btn-primary plusMultipleFields"><i class="fa fa-plus"></i></button>
                                @endif

                            @endforeach
                        @endif
                    </div>
                </div>

            </div>
        </div>
    @endforeach
@endif

@if(!empty($block['subBlock']))

    <h3 class="batches--main-title fb">{{ trans('swis.products.more_info.title') }}</h3>
    <hr/>
    <div  class="batches--block" id="goodsProductListsSections">

        <h4 class="fb text--batches">{{ trans('swis.single_app.batches') }}</h4>

        <div id="goodsProductBatch">

            <div class="table-responsive">
                <table class="table table-bordered {{ $block['subBlock']['tableClass'] }}" data-module="{{ $block['subBlock']['dataModule'] }}" data-sub-block="true">
                    <thead>
                    <tr>
                        @foreach ($block['subBlock']['columns'] as $column)
                            <td>{{ $column }}</td>
                        @endforeach
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($block['subBlock']['fields']))
                        @php($i = 0)
                        @foreach($block['subBlock']['fields'] as $key => $dataValue)
                            @include('user-documents.products.table-tr', [
                                'fields' => $dataValue,
                                'num' => $i++,
                                'batchId' => $key
                            ])
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div><hr/>
@endif