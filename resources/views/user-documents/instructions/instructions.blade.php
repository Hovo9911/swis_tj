<div class="table-responsive">
    <table class="table table-bordered {{ $block['tableClass'] }}" data-sub-form="{{ $block['tableClass'] }}" data-module="{{ $block['module'] }}" >
        <thead>
            <tr>
                <th>{{ trans('swis.my_application.instructions.profiles') }}</th>
                <th>{{ trans('swis.my_application.instructions.description') }}</th>
                <th>{{ trans('swis.my_application.instructions.feedback') }}</th>
            </tr>
        </thead>

        <tbody>
            @php($editable = $data['tabContent'][$block['module']]['editable'])
            @foreach ($block['data']['instructions'] ?? [] as $item)
                <tr>
                    <td>
                        @foreach ($item->subApplicationInstructionsProfile as $subApplicationInstructionsProfile)
                            {{ $subApplicationInstructionsProfile->profile->name }} <br />
                        @endforeach
                    </td>
                    <td>{{ $item->description }}</td>
                    <td>
                        @php($selectName = "instructions_feedback[$item->id][feedback]")
                        <select class="form-control select2" name="{{ ($editable) ? $selectName : '' }}" {{ ($editable) ? '' : 'disabled' }} >
                            <option></option>
                            @if (isset($block['data']['feedback'], $block['data']['feedback'][$item->instructions_id]))
                                @foreach ($block['data']['feedback'][$item->instructions_id] as $instructionFeedback)
                                    <option value="{{ $instructionFeedback['value'] }}" {{ $instructionFeedback['selected'] }}>{{ $instructionFeedback['name'] }}</option>
                                @endforeach
                            @endif
                        </select>

                        @php($textareaName = "instructions_feedback[$item->id][note]")
                        <textarea class="form-control m-t" name="{{ ($editable) ? $textareaName : '' }}" {{ ($editable) ? '' : 'disabled' }}>{{$item->feedback_note}}</textarea>
                    </td>
                </tr>
            @endforeach
    </table>
</div>