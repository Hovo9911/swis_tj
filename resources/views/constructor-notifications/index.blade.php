@extends('layouts.app')

@section('title'){{trans('swis.constructor-notifications.title')}}@stop

@section('contentTitle') {{trans('swis.constructor-notifications.notifications.title')}} @stop

@section('form-buttons-top')
    {!! renderAddButton() !!}
@stop

@section('content')

    <div class="clearfix search__out">
        <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse"
                data-target="#collapseSearch" aria-expanded="false" aria-controls="collapseSearch">
            <i class="fa fa-search"> </i><span
                    class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
        </button>
    </div>

    <div class="collapse" id="collapseSearch">
        <div class="card card-body">
            <div class="row">
                <form class="form-horizontal fill-up" id="search-filter" autocomplete="off">

                    <div class="form-group"><label class="col-md-4 col-lg-3 control-label">ID</label>
                        <div class="col-lg-6">
                            <input type="number" min="0" class="form-control onlyNumbers" name="id" id="" value="">
                            <div class="form-error" id="form-error-id"></div>
                        </div>
                    </div>

                    <div class="form-group"><label
                                class="col-md-4 col-lg-3 control-label">{{trans('swis.notifications.search.name')}}</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" name="name" id="" value="">
                            <div class="form-error" id="form-error-name"></div>
                        </div>
                    </div>

                    <div class="form-group text-right">
                        <div class="col-sm-offset-5 col-sm-4">
                            <button type="submit"
                                    class="btn btn-primary">{{trans('core.base.label.search')}}</button>
                            <button type="button" class="btn btn-danger resetButton"
                                    data-submit="true">{{trans('core.base.label.reset')}}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="data-table">
        <div class="clearfix">
            <table class="table table-striped table-bordered table-hover dataTables-example" id="list-data">
                <thead>
                <tr>
                    <th data-col="actions" data-delete="0" class="th-actions">{{trans('core.base.label.actions')}}</th>
                    <th data-col="id">ID</th>
                    <th data-col="name">{{trans('swis.notifications.label.name')}}</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/constructor-notifications/main.js')}}"></script>
@endsection