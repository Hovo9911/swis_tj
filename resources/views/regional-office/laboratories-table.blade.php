<table class="table table-bordered">

    <thead>
        <td style="width: 20px;"></td>
        <td>{{trans("swis.regional_office.laboratory.tab.table.lab_name")}}</td>
{{--        <td>{{trans("swis.regional_office.laboratory.tab.table.start_date")}}</td>--}}
{{--        <td>{{trans("swis.regional_office.laboratory.tab.table.end_date")}}</td>--}}
    </thead>
    <?php
        $existsLabs = (!empty($regionalOffice)) ? $regionalOffice->laboratories->keyBy('laboratory_id')->toArray() : []
    ?>
    <tbody>
        @forelse($laboratories as $laboratory)
            <tr>

                <div class="form-group" style="display: none">
                    <div class="col-lg-12">
                        <div class='input-group date'>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            <input value="{{ isset($existsLabs[$laboratory->id]) ? $existsLabs[$laboratory->id]['start_date'] : '' }}" name="laboratories[{{$laboratory->id}}][start_date]" autocomplete="off" type="text" placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                        </div>
                        <div class="form-error" id="form-error-laboratories-{{$laboratory->id}}-start_date"></div>
                    </div>
                </div>
                <td>
                    <input name="laboratories[{{$laboratory->id}}][enable]" value="{{ $laboratory->id }}" type="checkbox" {{ isset($existsLabs[$laboratory->id]) ? 'checked' : '' }}/>
                </td>
                <td>
                    {{ $laboratory->name }}
                </td>
{{--                <td>--}}
{{--                    <div class="form-group">--}}
{{--                        <div class="col-lg-12">--}}
{{--                            <div class='input-group date'>--}}
{{--                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>--}}
{{--                                <input value="{{ isset($existsLabs[$laboratory->id]) ? $existsLabs[$laboratory->id]['start_date'] : '' }}" name="laboratories[{{$laboratory->id}}][start_date]" autocomplete="off" type="text" placeholder="{{setDatePlaceholder()}}" class="form-control"/>--}}
{{--                            </div>--}}
{{--                            <div class="form-error" id="form-error-laboratories-{{$laboratory->id}}-start_date"></div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </td>--}}
{{--                <td>--}}
{{--                    <div class="form-group">--}}
{{--                        <div class="col-lg-12">--}}
{{--                            <div class='input-group date'>--}}
{{--                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>--}}
{{--                                <input value="{{ isset($existsLabs[$laboratory->id]) ? $existsLabs[$laboratory->id]['end_date'] : '' }}" name="laboratories[{{$laboratory->id}}][end_date]" autocomplete="off" type="text" placeholder="{{setDatePlaceholder()}}" class="form-control"/>--}}
{{--                            </div>--}}
{{--                            <div class="form-error" id="form-error-laboratories-{{$laboratory->id}}-end_date"></div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </td>--}}
            </tr>
        @empty
            <tr>
                <td colspan="4" align="center">{{ trans("swis.regional_office.laboratory.not_found") }}</td>
            </tr>
        @endforelse
    </tbody>

</table>