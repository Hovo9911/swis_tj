@extends('layouts.app')

@section('title'){{trans('swis.regionalOffice.title')}}@stop

@section('contentTitle') {{trans('swis.regionalOffice.title')}} @stop

@section('form-buttons-top')
    {!! renderAddButton() !!}
@stop

@section('content')
    <div class="clearfix search__out">
        <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse" data-target="#collapseSearch" aria-expanded="false" aria-controls="collapseSearch">
            <i class="fa fa-search"> </i><span class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
        </button>
    </div>

    <div class="collapse" id="collapseSearch">
        <div class="card card-body">
            <form class="form-horizontal fill-up" id="search-filter" autocomplete="off">

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">ID</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="number" min="0" class="form-control onlyNumbers" name="id" id="" value="">
                                <div class="form-error" id="form-error-id"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.office_name')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="name" id="" value="">
                                <div class="form-error" id="form-error-name"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.code')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="office_code" id="" value="">
                                <div class="form-error" id="form-error-office_code"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.regional_office.region.label')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="region" id="" value="">
                                <div class="form-error" id="form-error-region"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.regional_office.city.label')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="city" id="" value="">
                                <div class="form-error" id="form-error-city"></div>
                            </div>
                        </div>
                    </div>
                </div>

                @if(Auth::user()->isSwadmin())

                    <div class="form-group">
                        <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.regional_office.agency.label')}}</label>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-lg-10 col-md-8 field__one-col">
                                    <select class="select2" name="agency" id="agency">
                                        <option value=""></option>
                                        @foreach($agencies as $agency)
                                            <option value="{{$agency->tax_id}}">{{$agency->name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="form-error" id="form-error-agency"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.regional_office.constructor_document.label')}}</label>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-lg-10 col-md-8 field__one-col">
                                    <select class="select2" name="constructor_document" id="constructorDocuments">
                                        <option value=""></option>
                                    </select>
                                    <div class="form-error" id="form-error-constructor_document"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                @endif

                @if(Auth::user()->isAgency())
                    <div class="form-group">
                        <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.regional_office.constructor_document.label')}}</label>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-lg-10 col-md-8 field__one-col">
                                    <select class="select2" name="constructor_document" id="constructorDocuments">
                                        <option value=""></option>
                                        @if($constructorDocuments->count())
                                            @foreach($constructorDocuments as $constructorDocument)
                                                <option value="{{$constructorDocument->document_code}}">{{$constructorDocument->document_name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <div class="form-error" id="form-error-constructor_document"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.show_status')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <select class="form-control select2 default-search" name="show_status">
                                    <option value="{{\App\Models\BaseModel::STATUS_ACTIVE}}">{{trans('core.base.label.status.active')}}</option>
                                    <option value="{{\App\Models\BaseModel::STATUS_INACTIVE}}">{{trans('core.base.label.status.inactive')}}</option>
                                </select>
                                <div class="form-error" id="form-error-show_status"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8 col-lg-offset-3 text-right">
                        <div class="row">
                            <div class="col-lg-10 col-md-8">
                                <button type="submit"
                                        class="btn btn-primary mr-10">{{trans('core.base.label.search')}}</button>
                                <button type="button" class="btn btn-danger resetButton"
                                        data-submit="true">{{trans('core.base.label.reset')}}</button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>

    <div class="data-table">
        <div class="clearfix">
            <table class="table table-striped table-bordered table-hover dataTables-example" id="list-data">
                <thead>
                <tr>
                    <th data-col="actions"  data-delete="0" class="th-actions">{{trans('core.base.label.actions')}}</th>
                    <th data-col="id">ID</th>
                    <th data-col="agency_name">{{trans('swis.agency_name')}}</th>
                    <th data-ordering="0" data-col="constructor_document_name">{{trans('swis.regional_office.constructor_documents')}}</th>
                    <th data-col="name">{{trans('swis.office_name')}}</th>
                    <th data-col="show_status">{{trans('core.base.label.status')}}</th>
                    <th data-col="address">{{trans('swis.regional_office.label.address')}}</th>
                    <th data-col="office_code">{{trans('swis.office_code')}}</th>
                    <th data-col="email">{{trans('core.base.label.email')}}</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/regional-office/main.js')}}"></script>
@endsection