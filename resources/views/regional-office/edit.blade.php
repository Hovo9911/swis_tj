@extends('layouts.app')

@section('title'){{trans('swis.regionalOffice.create.title')}}@stop

@section('contentTitle')
    {{trans('swis.regionalOffice.create.title')}}
@stop

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('content')
    <?php

    switch ($saveMode) {
        case 'add':
            $url = 'regional-office';
            break;
        default:
            $url = $saveMode != 'view' ? 'regional-office/update/' . $regionalOffice->id : '';

            $mls = $regionalOffice->ml()->notDeleted()->get()->keyBy('lng_id');

            $regionalOfficeConstructorDocuments = $regionalOffice->constructorDocuments->pluck('document_code')->toArray();
            break;
    }

    $languages = activeLanguages();
    ?>

    <form class="form-horizontal fill-up" id="form-data" action="{{urlWithLng($url)}}" autocomplete="off">

        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-general"> {{trans('core.base.tab.general')}}</a></li>
                @foreach($languages as $language)
                    <li>
                        <a data-toggle="tab" href="#tab-{{$language->code}}">{{ trans("core.base.language.".strtolower($language->name)) }}</a>
                    </li>
                @endforeach
                <li><a data-toggle="tab" href="#tab-laboratories"> {{trans('swis.regional_office.laboratories.tab')}}</a></li>
            </ul>
            <div class="tab-content">
                <div id="tab-general" class="tab-pane active">
                    <div class="panel-body">
                        <div class="form-group required"><label class="col-lg-2 control-label">{{trans('swis.agency')}}</label>
                            @if(count($agencies))

                                <div class="col-lg-10">
                                    @if(count($agencies) == 1)
                                        <input value="{{$agencies[0]->name}}" disabled class="form-control">
                                    @else
                                        <select class="form-control select2" name="company_tax_id" id="agency">
                                            @if(empty($regionalOffice) && count($agencies) > 1)
                                                <option></option>
                                            @endif
                                            @foreach($agencies as $agency)
                                                <option {{(isset($regionalOffice) && $agency->tax_id == $regionalOffice->company_tax_id) ? 'selected' : ''}} value="{{$agency->tax_id}}">{{$agency->name}}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                    <div class="form-error" id="form-error-company_tax_id"></div>
                                </div>

                            @else
                                <i class="no-more">{{trans('swis.regional_office.agencies.no_more.message')}}</i>
                            @endif

                        </div>

                        <div class="form-group required"><label class="col-lg-2 control-label">{{trans('swis.regional_office.constructor_document.label')}}</label>
                            <div class="col-lg-10">
                                <select class="form-control select2" multiple name="constructor_document[]"
                                        data-select2-allow-clear="false" id="constructorDocuments">
                                    @if($constructorDocuments->count())
                                        @foreach($constructorDocuments as $constructorDocument)
                                            <option {{(isset($regionalOfficeConstructorDocuments) && in_array($constructorDocument->document_code,$regionalOfficeConstructorDocuments)) ? 'selected' : ''}} value="{{$constructorDocument->document_code}}">{{$constructorDocument->document_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <div class="form-error" id="form-error-constructor_document"></div>
                            </div>
                        </div>

                        <div class="form-group required"><label class="col-lg-2 control-label">{{trans('swis.office_code')}}</label>
                            <div class="col-lg-10">
                                <input value="{{$regionalOffice->office_code or ''}}"
                                       {{$saveMode == 'edit' ? 'disabled' : ''}} name="office_code" type="text"
                                       placeholder="" class="form-control">
                                <div class="form-error" id="form-error-office_code"></div>
                            </div>
                        </div>

                        <div class="form-group"><label class="col-lg-2 control-label">{{trans('swis.reiognal_office.accreditation_number.label')}}</label>
                            <div class="col-lg-10">
                                <input value="{{$regionalOffice->accreditation_number or ''}}"
                                       name="accreditation_number" type="text"
                                       placeholder="" class="form-control">
                                <div class="form-error" id="form-error-accreditation_number"></div>
                            </div>
                        </div>

                        <div class="form-group"><label class="col-lg-2 control-label">{{trans('swis.reiognal_office.accreditation_date_of_issue.label')}}</label>
                            <div class="col-lg-10">
                                <div class='input-group date'>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                    <input autocomplete="off" placeholder="{{setDatePlaceholder()}}" data-start-date="{{currentDate()}}" value="{{$regionalOffice->accreditation_date_of_issue ?? ''}}" name="accreditation_date_of_issue" type='text' class="form-control"/>
                                </div>
                                <div class="form-error" id="form-error-accreditation_date_of_issue"></div>
                            </div>
                        </div>

                        <div class="form-group"><label class="col-lg-2 control-label">{{trans('swis.reiognal_office.accreditation_valid_until.label')}}</label>
                            <div class="col-lg-10">
                                <div class='input-group date'>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                    <input autocomplete="off" data-start-date="{{currentDate()}}" placeholder="{{setDatePlaceholder()}}" value="{{$regionalOffice->accreditation_valid_until ?? ''}}" name="accreditation_valid_until" type='text' class="form-control"/>
                                </div>
                                <div class="form-error" id="form-error-accreditation_valid_until"></div>
                            </div>
                        </div>


                        <div class="form-group required"><label class="col-lg-2 control-label">{{trans('swis.regional_office.region.label')}}</label>
                            <div class="col-lg-10">
                                <input value="{{$regionalOffice->region or ''}}" name="region" type="text" placeholder="" class="form-control">
                                <div class="form-error" id="form-error-region"></div>
                            </div>
                        </div>

                        <div class="form-group required"><label class="col-lg-2 control-label">{{trans('swis.regional_office.city.label')}}</label>
                            <div class="col-lg-10">
                                <input value="{{$regionalOffice->city or ''}}" name="city" type="text" placeholder="" class="form-control">
                                <div class="form-error" id="form-error-city"></div>
                            </div>
                        </div>

                        <div class="form-group required"><label class="col-lg-2 control-label">{{trans('swis.regional_office.area.label')}}</label>
                            <div class="col-lg-10">
                                <input value="{{$regionalOffice->area or ''}}" name="area" type="text" placeholder="" class="form-control">
                                <div class="form-error" id="form-error-area"></div>
                            </div>
                        </div>

                        <div class="form-group required"><label class="col-lg-2 control-label">{{trans('swis.regional_office.street.label')}}</label>
                            <div class="col-lg-10">
                                <input value="{{$regionalOffice->street or ''}}" name="street" type="text" placeholder="" class="form-control">
                                <div class="form-error" id="form-error-street"></div>
                            </div>
                        </div>

                        <div class="form-group row {{config('global.phone_number.required') ? 'required' : ''}}">
                            <label class="col-lg-2 control-label">{{trans('core.base.label.phone')}}</label>
                            <div class="col-lg-10">
                                <div class="input-group ">
                                    <span class="input-group-addon">+</span>
                                    <input type="text"
                                           class="form-control onlyNumbers"
                                           name="phone_number"
                                           value="{{$regionalOffice->phone_number or ''}}"
                                           autocomplete="off"
                                           required>
                                </div>
                                <div class="form-error" id="form-error-phone_number"></div>
                            </div>
                        </div>

                        <div class="form-group"><label
                                    class="col-lg-2 control-label">{{trans('swis.regional_office.fax.label')}}</label>
                            <div class="col-lg-10">
                                <input value="{{$regionalOffice->fax or ''}}" name="fax" type="text"
                                       placeholder=""
                                       class="form-control">
                                <div class="form-error" id="form-error-fax"></div>
                            </div>
                        </div>

                        <div class="form-group"><label
                                    class="col-lg-2 control-label">{{trans('core.base.label.email')}}</label>
                            <div class="col-lg-10">
                                <input value="{{$regionalOffice->email or ''}}" name="email" type="text" placeholder=""
                                       class="form-control">
                                <div class="form-error" id="form-error-email"></div>
                            </div>
                        </div>

                        <div class="form-group required"><label class="col-lg-2 control-label">{{trans('core.base.label.sort')}}</label>
                            <div class="col-lg-10">
                                <input value="{{$regionalOffice->sort or 0}}" name="sort" type="number" placeholder=""
                                       class="form-control">
                                <div class="form-error" id="form-error-sort"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">{{trans('core.base.label.status')}}</label>
                            <div class="col-lg-10 general-status">
                                <select name="show_status" class="form-show-status">
                                    <option value="{{\App\Models\RegionalOffice\RegionalOffice::STATUS_ACTIVE}}"
                                            {{(isset($regionalOffice) && $regionalOffice->show_status ==
                                        \App\Models\RegionalOffice\RegionalOffice::STATUS_ACTIVE) || $saveMode == 'add'
                                        ? ' selected="selected"' :
                                        ''}}>{{trans('core.base.label.status.active')}}</option>
                                    <option value="{{\App\Models\RegionalOffice\RegionalOffice::STATUS_INACTIVE}}"
                                            {{isset($regionalOffice) && $regionalOffice->show_status ==
                                        \App\Models\RegionalOffice\RegionalOffice::STATUS_INACTIVE ? '
                                        selected="selected"': ''}}>{{trans('core.base.label.status.inactive')}}</option>
                                </select>
                                <div class="form-error" id="form-error-show_status"></div>
                            </div>
                        </div>

                    </div>
                </div>

                @foreach($languages as $language)
                    <?php $lngId = $language->id ?>
                    <div id="tab-{{$language->code}}" class="tab-pane">
                        <div class="panel-body">

                            <div class="form-group required"><label
                                        class="col-lg-2 control-label">{{trans('core.base.label.name')}}</label>
                                <div class="col-lg-10">
                                    <input value="{{isset($mls[$lngId]) ? $mls[$lngId]->name : ''}}"
                                           name="ml[{{$language->id}}][name]" type="text" placeholder=""
                                           class="form-control">
                                    <div class="form-error" id="form-error-ml-{{$language->id}}-name"></div>
                                </div>
                            </div>

                            <div class="form-group"><label
                                        class="col-lg-2 control-label">{{trans('swis.regional_office.label.short_name')}}</label>
                                <div class="col-lg-10">
                                    <input value="{{isset($mls[$lngId]) ? $mls[$lngId]->short_name : ''}}"
                                           name="ml[{{$language->id}}][short_name]" type="text" placeholder=""
                                           class="form-control">
                                    <div class="form-error" id="form-error-ml-{{$language->id}}-short_name"></div>
                                </div>
                            </div>

                            <div class="form-group"><label
                                        class="col-lg-2 control-label">{{trans('swis.regional_office.ml.address.label')}}</label>
                                <div class="col-lg-10">
                                    <input value="{{isset($mls[$lngId]) ? $mls[$lngId]->address : ''}}"
                                           name="ml[{{$language->id}}][address]" type="text" placeholder=""
                                           class="form-control">
                                    <div class="form-error" id="form-error-ml-{{$language->id}}-address"></div>
                                </div>
                            </div>

                            <div class="form-group"><label
                                        class="col-lg-2 control-label">{{trans('swis.regional_office.ml.supervisor.label')}}</label>
                                <div class="col-lg-10">
                                    <input value="{{isset($mls[$lngId]) ? $mls[$lngId]->supervisor : ''}}"
                                           name="ml[{{$language->id}}][supervisor]" type="text" placeholder=""
                                           class="form-control">
                                    <div class="form-error" id="form-error-ml-{{$language->id}}-supervisor"></div>
                                </div>
                            </div>

                            <div class="form-group"><label
                                        class="col-lg-2 control-label">{{trans('swis.regional_office.ml.department_head.label')}}</label>
                                <div class="col-lg-10">
                                    <input value="{{isset($mls[$lngId]) ? $mls[$lngId]->department_head : ''}}"
                                           name="ml[{{$language->id}}][department_head]" type="text" placeholder=""
                                           class="form-control">
                                    <div class="form-error" id="form-error-ml-{{$language->id}}-department_head"></div>
                                </div>
                            </div>

                            <div class="form-group"><label class="col-lg-2 control-label">{{trans('core.base.description')}}</label>
                                <div class="col-lg-10">
                                    <textarea rows="6" name="ml[{{$language->id}}][description]"
                                              class="form-control">{{isset($mls[$lngId]) ? $mls[$lngId]->description : ''}}</textarea>
                                    <div class="form-error" id="form-error-ml-{{$language->id}}-description"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                @endforeach

                <div id="tab-laboratories" class="tab-pane">
                    <div class="panel-body laboratoriesList" >
                        @include('regional-office.laboratories-table')
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" value="{{$regionalOffice->id or 0}}" name="id" id="regionalOfficeId" class="mc-form-key"/>

        {!! csrf_field() !!}
    </form>

@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script src="{{asset('js/regional-office/main.js')}}"></script>
    <script>
        var regionalOfficeStatus = '{{$regionalOffice->show_status or ''}}';
    </script>
@endsection