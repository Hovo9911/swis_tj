@extends('layouts.app')

@section('title'){{trans('swis.payments.new_title')}}  @endsection

@section('contentTitle') {{trans('swis.payments.new_title')}} @endsection

@section('topScripts')
    <script>
        var currency = '{!! config('swis.default_currency') !!}';
    </script>
@endsection

@section('content')
    <form action="{{ config('payment.alif.url') }}" method="post" id="form-data-refile">
        <input type="hidden" name="token" id="token" value="">
        <input type="hidden" name="key" id="key" value="{{ $params->key }}">
        <input type="hidden" name="callbackUrl" id="callbackUrl" value="{{ $params->callbackUrl }}">
        <!-- callback url where alif sends information about status of transactions -->
        <input type="hidden" name="returnUrl" id="returnUrl" value="{{ urlWithLng('/payments') }}">
        <input type="hidden" name="orderId" id="orderId" value="">

        <input type="hidden" name="info" id="info" value="{{ $params->info }}">

        @if(!\Auth::user()->isAgency())
            <h1 class="count-info">
                <i class="fa fa-money text-navy"></i>
                <span class="text-navy">{{ Auth::user()->balance() . ' ' . config('swis.default_currency') }}</span>
            </h1>
        @endif

        @if(Auth::user()->hasAccessToEditModule())
            <div class="form-group row">
                <div class="col-lg-3">
                    <input type="number" class="form-control" name="amount" id="amountField">
                    <div class="form-error" id="form-error-amount"></div>
                </div>

                <button class="btn btn-primary" disabled id="refile">{{ trans('swis.payments.refill.title') }}</button>
            </div>
        @endif
        <hr/>
    </form>

    <div class="clearfix search__out">
        <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse"
                data-target="#collapseSearch" aria-expanded="false" aria-controls="collapseSearch">
            <i class="fa fa-search"> </i><span
                    class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
        </button>
    </div>

    <div class="collapse" id="collapseSearch">
        <div class="card card-body">
            <form class="form-horizontal fill-up" id="search-filter" autocomplete="off">

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.payments.service_provider_name.title')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="service_provider_name" id="" value="">
                                <div class="form-error" id="form-error-service_provider_name"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.payments.saf_number.title')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="saf_number" id="" value="">
                                <div class="form-error" id="form-error-saf_number"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.payments.sub_application_number.title')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="sub_application_number" id="" value="">
                                <div class="form-error" id="form-error-sub_application_number"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.payments.agency.title')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <select class="select2" name="agency" id="agency">
                                    <option value=""></option>
                                    @if($agencies->count())
                                        @foreach($agencies as $agency)
                                            <option value="{{$agency->tax_id}}">({{$agency->tax_id}}) {{$agency->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <div class="form-error" id="form-error-agency"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.payments.document.title')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <select class="select2" name="constructor_document">
                                    <option value=""></option>
                                    @if($constructorDocuments->count())
                                        @foreach($constructorDocuments as $constructorDocument)
                                            <option value="{{$constructorDocument->id}}">{{$constructorDocument->document_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <div class="form-error" id="form-error-constructor_document"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.payments.obligation_type.title')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <select class="select2" name="obligation_type">
                                    <option value=""></option>
                                    @if($refObligationTypes->count())
                                        @foreach($refObligationTypes as $refObligationType)
                                            <option value="{{$refObligationType->id}}">({{$refObligationType->code}}) {{$refObligationType->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <div class="form-error" id="form-error-obligation_type"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.payments.transaction_date.title')}}</label>

                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-5">
                                <div class='input-group date'>
                                    <span class="input-group-addon"><span
                                                class="glyphicon glyphicon-calendar"></span></span>
                                    <input value="" name="transaction_date_start" autocomplete="off" type="text"
                                           placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                </div>
                                <div class="form-error" id="form-error-transaction_date_start"></div>
                            </div>

                            <div class="col-lg-5">
                                <div class='input-group date'>
                                    <span class="input-group-addon"><span
                                                class="glyphicon glyphicon-calendar"></span></span>
                                    <input value="" name="transaction_date_end" autocomplete="off" type="text"
                                           placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                </div>
                                <div class="form-error" id="form-error-transaction_date_end"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.payments.username.title')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="transaction_username" id="" value="">
                                <div class="form-error" id="form-error-transaction_username"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8 col-lg-offset-3 text-right">
                        <div class="row">
                            <div class="col-lg-10 col-md-8">
                                <button type="submit"
                                        class="btn btn-primary mr-10">{{trans('core.base.label.search')}}</button>
                                <button type="button" class="btn btn-danger resetButton"
                                        data-submit="true">{{trans('core.base.label.reset')}}</button>
                                <a href="{{ urlWithLng('/payments/generate/xls') }}"
                                   class="btn btn-default generate-xls"
                                   style="margin-top: 10px;">{{trans('swis.payment-and-obligation.xls.title')}}</a>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>

    <table class="table table-striped table-bordered table-hover dataTables-example" id="list-data">
        <thead>
           <tr>
               <th data-col="id">{{trans('swis.payments.incremented_id.title')}}</th>
               <th data-col="payment_id" data-ordering="0">{{ trans('swis.payments.payment_id.title') }}</th>
               <th data-col="service_provider_name" data-ordering="0">{{ trans('swis.payments.service_provider_name.title') }}</th>
               <th data-col="saf_number">{{ trans('swis.payments.saf_number.title') }}</th>
               <th data-col="document_number">{{ trans('swis.payments.sub_application_number.title') }}</th>
               <th data-col="agency">{{ trans('swis.payments.agency.title') }}</th>
               <th data-col="document">{{ trans('swis.payments.document.title') }}</th>
               <th data-col="obligation_type">{{ trans('swis.payments.obligation_type.title') }}</th>
               <th data-col="username">{{ trans('swis.payments.username.title') }}</th>
               <th data-col="amount">{{ trans('swis.payments.amount.title') }}</th>
               <th data-col="transaction_date">{{ trans('swis.payments.transaction_date.title') }}</th>
               <th data-col="total_balance">{{ trans('swis.payments.total_balance.title') }}</th>
           </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
@endsection

@section('scripts')
    <script src="{{asset('js/payments/main.js')}}"></script>
@endsection
