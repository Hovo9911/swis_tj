<div style="cursor: pointer" id="roundingDown" data-toggle="collapse" data-target="#roundingDownCollapse">
    <h3> RoundingDown</h3>
    <p> Returns the rounded fractions down</p>

    <div id="roundingDownCollapse" class="collapse">
        <p>Arguments: </p><hr>
        <p>Number = (int, float) </p>
        <p>FieldId = SAF_ID_? or FIELD_ID_? </p><hr>
        <p>Example: </p>
        <pre class="prettyprint"><div>roundingDown(1.7, 1000) // 588 // true
roundingDown(1.8, 1000) // 555 // true
roundingDown(9.9, 324) // 32 // true
roundingDown(9.8, 100) // 10 // true

RULE CONDITION:
    roundingDown(1.7, SAF_ID_?]) > 500 or
    roundingDown(1.7, SAF_ID_?]) == 500 or
    roundingDown(1.7, SAF_ID_?]) >= 500 or
    roundingDown(1.7, SAF_ID_?]) or
    FIELD_ID_?==roundingDown(1.7, SAF_ID_?]) or
    FIELD_ID_? + SAF_ID_? >= roundingDown(1.7, SAF_ID_?]) + FIELD_ID_?
RULE BODY:
    FIELD_ID_9004_17='something' or inRef() or show Error
        </div></pre>
    </div>
</div>