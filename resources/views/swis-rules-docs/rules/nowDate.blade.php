<div style="cursor: pointer" id="nowDate" data-toggle="collapse" data-target="#nowDateCollapse">
    <h3> NowDate</h3>
    <p>Return Date with give format or default ("Y-m-d"). More example <a href="https://www.php.net/manual/ru/function.date.php" target="_blank"> Here</a></p>

    <div id="nowDateCollapse" class="collapse">
        <p>Arguments: </p><hr>
        <p>Format = (string) Not Required</p><hr>
        <p>Example: </p>
        <pre class="prettyprint"><div>nowDate() // "2020-08-18"
nowDate("Y-m-d H:i:s") // "2020-08-18 10:12:00"
FIELD_ID_?? == nowDate() // false or true, depends FIELD_ID_??
FIELD_ID_?? > nowDate() // false or true, depends FIELD_ID_??
FIELD_ID_?? < nowDate() // false or true, depends FIELD_ID_??
SAF_ID_?? == nowDate() // false or true, depends SAF_ID_??
SAF_ID_?? > nowDate() // false or true, depends SAF_ID_??
SAF_ID_?? < nowDate() // false or true, depends SAF_ID_??

RULE CONDITION:
    FIELD_ID_?? > nowDate()
RULE BODY:
    FIELD_ID_??=nowDate()
        </div></pre>
    </div>
</div>