<div style="cursor: pointer" id="currentUserInfo" data-toggle="collapse" data-target="#currentUserInfoCollapse">
    <h3> CurrentUserInfo</h3>
    <p> Return current user info by field name </p>

    <div id="currentUserInfoCollapse" class="collapse">
        <p>Arguments: </p><hr>
        <p>Key (name of field). Available Keys below.</p><hr>
        <pre class="prettyprint">id, first_name, last_name, patronymic_name, english_first_name, english_last_name,
genus,birth_date,community,ssn,email,username,passport,passport_issue_date,
passport_validity_date,death_date,phone_number,address,registration_address,
city_village,post_address,registered_at,passport_type,passport_issued_by,english_patronymic_name
        </pre>
        <p>Example: </p>
        <pre class="prettyprint"><div>currentUserInfo('first_name') // "Vardan"
currentUserInfo('username') // "Vardan1"
currentUserInfo('phone_number') // "098608010"
currentUserInfo() // ""

RULE CONDITION:
    isEmpty(FIELD_ID_??)
RULE BODY:
    FIELD_ID_??=concat(currentUserInfo('first_name'),currentUserInfo('last_name'))
        </div></pre>
    </div>
</div>