<div style="cursor: pointer" id="unansweredInstructionType" data-toggle="collapse" data-target="#unansweredInstructionTypeCollapse">
    <h3> unansweredInstructionType</h3>
    <p> Function without arguments check if sub application has unanswered instruction. With arguments check if sub application have unanswered instruction with following type.</p>

    <div id="unansweredInstructionTypeCollapse" class="collapse">
        <p>Arguments: </p><hr>
        <p>InstructionType (string) NOT REQUIRED <small>Instruction module "code" field</small> </p><hr>

        <p>Example: </p>
        <pre class="prettyprint"><div>unansweredInstructionType() // true if exists or false
unansweredInstructionType('instructionCode') // true if exists or false

RULE CONDITION:
    unansweredInstructionType('instructionCode')
RULE BODY:
    generateObligation("obligationCode", 100)
        </div></pre>
    </div>
</div>