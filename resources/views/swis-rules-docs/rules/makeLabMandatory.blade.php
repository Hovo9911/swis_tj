<div style="cursor: pointer" id="makeLabMandatory" data-toggle="collapse" data-target="#makeLabMandatoryCollapse">
    <h3> MakeLabMandatory</h3>
    <p> Function make lab examination tab require.</p>

    <div id="makeLabMandatoryCollapse" class="collapse">
        <p>Example: </p>
        <pre class="prettyprint"><div>makeLabMandatory() // make tab mandatory

RULE CONDITION:
    unansweredExamination()
RULE BODY:
    makeLabMandatory()
        </div></pre>
    </div>
</div>