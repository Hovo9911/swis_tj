<div style="cursor: pointer" id="negativeAnsweredInstructionType" data-toggle="collapse" data-target="#negativeAnsweredInstructionTypeCollapse">
    <h3> NegativeAnsweredInstructionType</h3>
    <p> Function without arguments check if sub application has negative answered instruction.
        With arguments check if sub application have negative answered instruction with following type.</p>
    <p>!!! NOTE: negative answer is answer witch "digital_indicator" field in reference "risk_feedback" table value less or equal 0.</p>

    <div id="negativeAnsweredInstructionTypeCollapse" class="collapse">
        <p>Arguments: </p><hr>
        <p>InstructionType (string) NOT REQUIRED <small>Instruction module "code" field</small> </p><hr>

        <p>Example: </p>
        <pre class="prettyprint"><div>negativeAnsweredInstructionType() // true if exists or false
negativeAnsweredInstructionType('instructionCode') // true if exists or false

RULE CONDITION:
    negativeAnsweredInstructionType('instructionCode')
RULE BODY:
    generateObligation("obligationCode", 100)
        </div></pre>
    </div>
</div>