<div style="cursor: pointer" id="hoursLeft" data-toggle="collapse" data-target="#hoursLeftCollapse">
    <h3> HoursLeft</h3>
    <p> Function calculate how hours left.</p>
    <p> !!! NOTE: We get active time period from reference "classificator_of_documents" for each document. </p>

    <div id="hoursLeftCollapse" class="collapse">
        <p>Example: </p>
        <pre class="prettyprint"><div>hoursLeft()

RULE CONDITION:
    hoursLeft() < 8
RULE BODY:
    'Do something'
        </div></pre>
    </div>
</div>