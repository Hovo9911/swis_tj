<div style="cursor: pointer" id="generateObligation" data-toggle="collapse" data-target="#generateObligationCollapse">
    <h3> GenerateObligation</h3>
    <p> Function for generate obligation.</p>

    <div id="generateObligationCollapse" class="collapse">
        <p>Arguments: </p><hr>
        <p>ObligationCode (string) <small>reference obligation_budget_line table "code" field</small></p>
        <p>Sum (integer, float, double) <small>Sum of obligation.</small></p><hr>

        <p>Example: </p>
        <pre class="prettyprint"><div>generateObligation("obligationCode", 200) // generate 200 obligation with obligationCode
generateObligation("obligationCode", 100) // generate 100 obligation with obligationCode

RULE CONDITION:
    hoursLeft() < 10
RULE BODY:
    generateObligation("obligationCode", 100)
        </div></pre>
    </div>
</div>