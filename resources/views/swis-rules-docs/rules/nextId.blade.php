<div style="cursor: pointer" id="nextId" data-toggle="collapse" data-target="#nextIdCollapse">
    <h3> NextId</h3>
    <p> Function get field as parameter and return his max value in this document +1.</p>

    <div id="nextIdCollapse" class="collapse">
        <p>Arguments: </p><hr>
        <p>Field (string) </p>
        <p>Length (integer) NOT REQUIRED <small>total length.</small></p><hr>

        <p>Example: </p>
        <pre class="prettyprint"><div>nextId("FIELD_ID_??") // if max value for FIELD_ID_?? is 10 function will return 11
nextId("FIELD_ID_??", 5) // if max value for FIELD_ID_?? is 10 function will return 00011
nextId("FIELD_ID_", 5) // if max value for FIELD_ID_?? is 5 function will return 00006

RULE CONDITION:
    hoursLeft() < 8
RULE BODY:
    FIELD_ID_??=nextId("FIELD_ID_??", 5)
        </div></pre>
    </div>
</div>