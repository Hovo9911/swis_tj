<div style="cursor: pointer" id="formatNumber" data-toggle="collapse" data-target="#formatNumberCollapse">
    <h3> FormatNumber</h3>
    <p> Change number format</p>

    <div id="formatNumberCollapse" class="collapse">
        <p>Arguments: </p><hr>
        <p>Number (integer, float, double) </p>
        <p>Decimal (integer) </p><hr>
        <p>Example: </p>
        <pre class="prettyprint"><div>formatNumber(100,2) // 100.00
formatNumber(100,5) // 100.00000
formatNumber("100", "2") // 100.00
formatNumber(5.5,2) // 5.50
formatNumber(101+5,6) // 106.000000
formatNumber(FIELD_ID_??,2) // number
formatNumber(SAF_ID_??+FIELD_ID_??,3) // number

RULE CONDITION:
    isEmpty(FIELD_ID_??)
RULE BODY:
    FIELD_ID_??=formatNumber(100,5)
        </div></pre>
    </div>
</div>