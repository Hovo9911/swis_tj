<div style="cursor: pointer" id="roundingUp" data-toggle="collapse" data-target="#roundingUpCollapse">
    <h3> RoundingUp</h3>
    <p> Returns the next highest integer value by rounding up value if necessary</p>

    <div id="roundingUpCollapse" class="collapse">
        <p>Arguments: </p><hr>
        <p>Number = (int, float) </p>
        <p>FieldId = SAF_ID_? or FIELD_ID_? </p><hr>
        <p>Example: </p>
        <pre class="prettyprint"><div>roundingUp(1.7, 1000) // 589 // true
roundingUp(1.8, 1000) // 556 // true
roundingUp(12, 324) // 27 // true

RULE CONDITION:
    roundingUp(1.7, SAF_ID_?]) > 500 or
    roundingUp(1.7, SAF_ID_?]) == 500 or
    roundingUp(1.7, SAF_ID_?]) >= 500 or
    roundingUp(1.7, SAF_ID_?]) or
    FIELD_ID_?==roundingUp(1.7, SAF_ID_?]) or
    FIELD_ID_? + SAF_ID_? >= roundingUp(1.7, SAF_ID_?]) + FIELD_ID_?
RULE BODY:
    FIELD_ID_9004_17='something' or inRef() or show Error
                    </div></pre>
    </div>
</div>