<div style="cursor: pointer" id="yearEnd" data-toggle="collapse" data-target="#yearEndCollapse">
    <h3> YearEnd</h3>
    <p> Return last day of year.</p>

    <div id="yearEndCollapse" class="collapse">
        <p>Example: </p>
        <pre class="prettyprint"><div>yearEnd() // "2020-12-31"

RULE CONDITION:
    isEmpty(FIELD_ID_??)
RULE BODY:
    FIELD_ID_??=yearEnd()
        </div></pre>
    </div>
</div>