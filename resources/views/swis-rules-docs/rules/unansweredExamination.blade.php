<div style="cursor: pointer" id="unansweredExamination" data-toggle="collapse" data-target="#unansweredExaminationCollapse">
    <h3> UnansweredExamination</h3>
    <p> Function check if lab application have (product indicator) which is not answered "correspond" field </p>
    <hr>

    <div id="unansweredExaminationCollapse" class="collapse">
        <p>Example: </p>
        <pre class="prettyprint"><div>unansweredExamination() // true if exists or false

RULE CONDITION:
    unansweredExamination()
RULE BODY:
    generateObligation("obligationCode", 100)
        </div></pre>
    </div>
</div>