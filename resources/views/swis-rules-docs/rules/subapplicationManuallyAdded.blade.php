<div style="cursor: pointer" id="subapplicationManuallyAdded" data-toggle="collapse" data-target="#subapplicationManuallyAddedCollapse">
    <h3> SubapplicationManuallyAdded</h3>
    <p> Return True if Sub application is created manually.</p>

    <div id="subapplicationManuallyAddedCollapse" class="collapse">
        <p>Example: </p>
        <pre class="prettyprint"><div>subapplicationManuallyAdded() // true or false

RULE CONDITION:
    subapplicationManuallyAdded()
RULE BODY:
    FIELD_ID_??=concat(SAF_ID_89,"-","10-",nowDate(),currentUser())
        </div></pre>
    </div>
</div>