<div style="cursor: pointer" id="currentAgencyInfo" data-toggle="collapse" data-target="#currentAgencyInfoCollapse">
    <h3> CurrentAgencyInfo</h3>
    <p> Return current agency info by field name </p>

    <div id="currentAgencyInfoCollapse" class="collapse">
        <p>Arguments: </p><hr>
        <p>Key (name of field). Available Keys below.</p><hr>
        <pre class="prettyprint">id,ssn,leg_name,leg_address,phone_number,email,website,name_ml,address_ml,
description_ml,name_ml_en,description_ml_en,address_ml_en,name_ml_ru,
description_ml_ru,address_ml_ru,name_ml_tj,description_ml_tj,address_ml_tj
        </pre>
        <p>Example: </p>
        <pre class="prettyprint"><div>currentAgencyInfo('ssn') // "030000230"
currentAgencyInfo('email') // "info@standard.tj"
currentAgencyInfo('name_ml') // "Агентство по стандартизации, метрологии, сертификации и торговой инспекции при Правительстве РТ"
currentAgencyInfo('description_ml') // "Агентство по стандартизации, метрологии, сертификации и торговой инспекции при Правительстве РТ"
currentAgencyInfo() // ""

RULE CONDITION:
    isEmpty(FIELD_ID_??)
RULE BODY:
    FIELD_ID_??=concat(currentAgencyInfo('ssn'),currentAgencyInfo('leg_name'))
        </div></pre>
    </div>
</div>