<div style="cursor: pointer" id="currentUserShortName" data-toggle="collapse" data-target="#currentUserShortNameCollapse">
    <h3> CurrentUserShortName</h3>
    <p> Return Current user Short Name. Example` "V. Saratikyan"</p>

    <div id="currentUserShortNameCollapse" class="collapse">
        <p>Example: </p>
        <pre class="prettyprint"><div>currentUserShortName() // "V. Saratikyan"

RULE CONDITION:
    isEmpty(FIELD_ID_??)
RULE BODY:
    FIELD_ID_??=concat(SAF_ID_89,"-","10-",nowDate(),currentUserShortName())
    or
    FIELD_ID_??=currentUserShortName()
        </div></pre>
    </div>
</div>