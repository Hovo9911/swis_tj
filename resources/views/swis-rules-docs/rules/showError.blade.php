<div style="cursor: pointer" id="showError" data-toggle="collapse" data-target="#showErrorCollapse">
    <h3> ShowError</h3>
    <p> Function Show error and block action. if pass second argument field will be bordered red.</p>

    <div id="showErrorCollapse" class="collapse">
        <p>Arguments: </p><hr>
        <p>Key (string) </p>
        <p>Fields (array) NOT REQUIRED </p><hr>

        <p>Example: </p>
        <pre class="prettyprint"><div>showError('key1', ['FIELD_ID_CC_061_4', 'FIELD_ID_CC_061_16', 'FIELD_ID_CC_061_17']) // make mandatory following fields and write key in errore message section
showError('key1') // show error in error section and block next action

RULE CONDITION:
    isIdentic('SAF_ID_89', 4)
RULE BODY:
    showError('key1', ['FIELD_ID_CC_061_4'])
        </div></pre>
    </div>
</div>