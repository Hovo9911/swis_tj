<div style="cursor: pointer" id="getLabObligations" data-toggle="collapse" data-target="#getLabObligationsCollapse">
    <h3> GetLabObligations</h3>
    <p> Function have 2 logic.</p>
    <p> First when we call function in sub application.</p>
    <p> When we call function in sub application we get total sum of all indicators in lab applications witch sent from this application.</p>
    <hr>
    <p> Second when we call function in lab sub application</p>
    <p> in this case we just get total of all indicators in lab application</p>
    <hr>

    <div id="getLabObligationsCollapse" class="collapse">
        <p>Example: </p>
        <pre class="prettyprint"><div>getLabObligations() // 0 or (total sum)

RULE CONDITION:
    getLabObligations() > 100
RULE BODY:
    generateObligation("obligationCode", 100)
        </div></pre>
    </div>
</div>