<div style="cursor: pointer" id="contains" data-toggle="collapse" data-target="#containsCollapse">
    <h3> Contains</h3>
    <p> For check if string (field value) contains some other string or character</p>

    <div id="containsCollapse" class="collapse">
        <p>Arguments: </p><hr>
        <p>Field or string = (string) </p>
        <p>Value = (string) </p><hr>
        <p>Example: </p>
        <pre class="prettyprint"><div>contains(SAF_ID_??, "23") // true if SAF_ID_?? contains "23"
contains(SAF_ID_??, " ") // true if SAF_ID_?? contains " "
contains(FIELD_ID_??, "qwerty") // true if FIELD_ID_?? contains "qwerty"
contains("Hello World !!", "World") // true because "Hello World !!" contains "World" word
contains("123456789", "987") // false because "123456789" not contains "987" word

RULE CONDITION:
    contains("FIELD_ID_??", "AM")
RULE BODY:
    FIELD_ID_??='something' or inRef()
        </div></pre>
    </div>
</div>