<div style="cursor: pointer" id="exchangeRate" data-toggle="collapse" data-target="#exchangeRateCollapse">
    <h3> ExchangeRate</h3>
    <p> Function for calculate currency by given field and from currency to currency</p>

    <div id="exchangeRateCollapse" class="collapse">
        <p>Arguments: </p><hr>
        <p>Field (string) </p>
        <p>FromCurrency (string) </p>
        <p>ToCurrency (string) </p><hr>

        <p>Example: </p>
        <pre class="prettyprint"><div>exchangeRate("SAF_ID_84", "RUB", "USD")
exchangeRate("SAF_ID_84", "RUB", "EUR")
exchangeRate("SAF_ID_84", "USD", "RUB")

RULE CONDITION:
    exchangeRate("SAF_ID_84", "RUB", "EUR") > 12
RULE BODY:
    FIELD_ID_??=exchangeRate("SAF_ID_84", "RUB", "EUR")
        </div></pre>
    </div>
</div>