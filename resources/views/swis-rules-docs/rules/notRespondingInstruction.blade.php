<div style="cursor: pointer" id="notRespondingInstruction" data-toggle="collapse" data-target="#notRespondingInstructionCollapse">
    <h3> NotRespondingInstruction</h3>
    <p> Function check if sub application have not responding instruction</p>

    <div id="notRespondingInstructionCollapse" class="collapse">
        <p>Example: </p>
        <pre class="prettyprint"><div>notRespondingInstruction() // true or false

RULE CONDITION:
    notRespondingInstruction()
RULE BODY:
    generateObligation("obligationCode", 1100)
        </div></pre>
    </div>
</div>