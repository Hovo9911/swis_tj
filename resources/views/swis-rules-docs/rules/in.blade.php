<div style="cursor: pointer" id="in" data-toggle="collapse" data-target="#inCollapse">
    <h3> In</h3>
    <p> For check in array (Function) arguments (value, array)</p>

    <div id="inCollapse" class="collapse">
        <p>Arguments: </p><hr>
        <p>Value = (int, string, boolean, float) </p>
        <p>Data = (array) </p><hr>
        <p>Example: </p>
        <pre class="prettyprint"><div>$data = ['qwery', 1, 'qazwsx'];

in("qwerty", $data) // true
in(1, $data) // true
in('1', $data) // false
in("rt", $data) // false
in("SAF_ID_?", $data) // false or true if exists
in("FIELD_ID_?", $data) // false or true if exists

RULE CONDITION:
    in("SAF_ID_?", ['1', '2', '3'])
RULE BODY:
    FIELD_ID_?='something' or inRef()
        </div></pre>
    </div>
</div>