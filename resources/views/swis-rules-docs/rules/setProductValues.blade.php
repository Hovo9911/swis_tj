<div style="cursor: pointer" id="setProductValues" data-toggle="collapse" data-target="#setProductValuesCollapse">
    <h3> SetProductValues</h3>
    <p style="color: red;"> !!! NOTE, PLEASE SEE API RULES BEFORE !!!</p>
    <p> Function find products by condition and set new values in founded mdm field. (SetProductValues not change values for saf fields)</p>
    <p> Function also can calculate math action in second argument</p>
    <pre class="prettyprint">+, -, *, /, %</pre>

    <div id="setProductValuesCollapse" class="collapse">
        <p>Arguments: </p><hr>
        <p>Conditions (array) SEE API RULES </p>
        <p>Values (array). See example below. </p>
        <pre class="prettyprint">['FIELD_ID_??' => '1000', 'FIELD_ID_??' => 'qwerty']</pre>
        <pre class="prettyprint">['FIELD_ID_??' => 'action(FIELD_ID_??+FIELD_ID_??)']</pre>
        <pre class="prettyprint">['FIELD_ID_??' => 'action(FIELD_ID_??*FIELD_ID_??-100)']</pre>
        <pre class="prettyprint">['FIELD_ID_??' => 'action(SAF_ID_??-FIELD_ID_??-100)']</pre><hr>
        <p>Example: </p>
        <pre class="prettyprint"><div>setProductValues([['SAF_ID_100', '>', 200]], ['FIELD_ID_??' => '1000']) // Set FIELD_ID_?? to 1000 where SAF_ID_100 > 200
setProductValues([['SAF_ID_100', '=', 200]], ['FIELD_ID_??' => '1000']) // Set FIELD_ID_?? to 1000 where SAF_ID_100 = 200
setProductValues([['SAF_ID_89', 'startWith', '10']], ['FIELD_ID_??' => '1000']) // Set FIELD_ID_?? to 1000 where SAF_ID_89 startWith '10'
setProductValues([['SAF_ID_100', '>', 200]], ['FIELD_ID_??' => 'qwerty']) // Set FIELD_ID_?? to 'qwerty' where SAF_ID_100 > 200

RULE CONDITION:
    getProducts([['SAF_ID_100', '>', 200]], 'count()') > 5
RULE BODY:
    FIELD_ID_??=setProductValues([['SAF_ID_100', '=', 200]], ['FIELD_ID_??' => 1000])

        </div></pre>
    </div>
</div>