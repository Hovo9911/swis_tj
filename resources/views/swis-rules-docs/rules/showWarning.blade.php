<div style="cursor: pointer" id="showWarning" data-toggle="collapse" data-target="#showWarningCollapse">
    <h3> ShowWarning</h3>
    <p> Function Show warning modal with following error message but not block action. So you can confirm action.</p>

    <div id="showWarningCollapse" class="collapse">
        <p>Arguments: </p><hr>
        <p>Key (string) </p>
        <p>Fields (array) NOT REQUIRED </p><hr>

        <p>Example: </p>
        <pre class="prettyprint"><div>showWarning('key1', ['FIELD_ID_CC_061_4', 'FIELD_ID_CC_061_16', 'FIELD_ID_CC_061_17']) // make mandatory following fields and write key in errore message section
showWarning('key1') // show error in error section and block next action

RULE CONDITION:
    isIdentic('SAF_ID_89', 4)
RULE BODY:
    showWarning('key1', ['FIELD_ID_CC_061_4'])
        </div></pre>
    </div>
</div>