<div style="cursor: pointer" id="startWith" data-toggle="collapse" data-target="#startWithCollapse">
    <h3> StartWith</h3>
    <p> For check if the HS code start with </p>

    <div id="startWithCollapse" class="collapse">
        <p>Arguments: </p><hr>
        <p>Field = (string) </p>
        <p>Value = (string) </p><hr>
        <p>Example: </p>
        <pre class="prettyprint"><div>startWith(SAF_ID_89, "3001") // true if hs code start with "3001"
startWith(SAF_ID_89, "10") // true if hs code start with "10"
startWith(SAF_ID_89, "89124587") // true if hs code start with "89124587"
startWith(FIELD_ID_??, "abs") // true if FIELD_ID_?? start with "abs"

RULE CONDITION:
    startWith("SAF_ID_?", "123")
        </div></pre>
    </div>
</div>