<div style="cursor: pointer" id="hoursSpent" data-toggle="collapse" data-target="#hoursSpentCollapse">
    <h3> HoursSpent</h3>
    <p> Function calculate how much time has passed in calculated states</p>
    <p> !!! NOTE: calculated state is state witch is enable "Subject to calculation" checkbox in Constructor Statuses Module. In other cases state is not calculable.  </p>

    <div id="hoursSpentCollapse" class="collapse">
        <p>Example: </p>
        <pre class="prettyprint"><div>hoursSpent()

RULE CONDITION:
    hoursSpent() > 24
RULE BODY:
    FIELD_ID_??=hoursSpent()
        </div></pre>
    </div>
</div>