<div style="cursor: pointer" id="endWith" data-toggle="collapse" data-target="#endWithCollapse">
    <h3> EndWith</h3>
    <p> For check if the HS code end with something</p>

    <div id="endWithCollapse" class="collapse">
        <p>Arguments: </p><hr>
        <p>Field = (string) </p>
        <p>Value = (string) </p><hr>
        <p>Example: </p>
        <pre class="prettyprint"><div>endWith(SAF_ID_89, "3001") // true if hs code end with "3001"
endWith(SAF_ID_89, "10") // true if hs code end with "10"
endWith(SAF_ID_89, "89124587") // true if hs code end with "89124587"
startWith(FIELD_ID_??, "abs") // true if FIELD_ID_?? end with "abs"

RULE CONDITION:
    endWith("SAF_ID_?", "123")
        </div></pre>
    </div>
</div>