<div style="cursor: pointer" id="isSafType" data-toggle="collapse" data-target="#isSafTypeCollapse">
    <h3> IsSafType</h3>
    <p> Return is given parameter equal to Saf type</p>

    <div id="isSafTypeCollapse" class="collapse">
        <p>Arguments: </p><hr>
        <p>Type (string) </p><hr>
        <p>Example: </p>
        <pre class="prettyprint"><div>isSafType("import") // true if saf type is import
isSafType('export') // true if saf type is export
isSafType('transit') // true if saf type is transit

RULE CONDITION:
    isSafType('import')
RULE BODY:
    FIELD_ID_??=currentUser()
        </div></pre>
    </div>
</div>