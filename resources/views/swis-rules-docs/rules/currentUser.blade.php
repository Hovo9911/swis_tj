<div style="cursor: pointer" id="currentUser" data-toggle="collapse" data-target="#currentUserCollapse">
    <h3> CurrentUser</h3>
    <p>Return Current User First name and Last name</p>

    <div id="currentUserCollapse" class="collapse">
        <p>Example: </p>
        <pre class="prettyprint"><div>currentUser() // "Vardan Saratikyan"

RULE CONDITION:
    FIELD_ID_?? > nowDate()
RULE BODY:
    FIELD_ID_??=currentUser()
        </div></pre>
    </div>
</div>