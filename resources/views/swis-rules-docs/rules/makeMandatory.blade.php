<div style="cursor: pointer" id="makeMandatory" data-toggle="collapse" data-target="#makeMandatoryCollapse">
    <h3> MakeMandatory</h3>
    <p> Function make fields mandatory</p>

    <div id="makeMandatoryCollapse" class="collapse">
        <p>Arguments: </p><hr>
        <p>Fields (array) </p><hr>

        <p>Example: </p>
        <pre class="prettyprint"><div>makeMandatory(['FIELD_ID_CC_061_4', 'FIELD_ID_CC_061_16', 'FIELD_ID_CC_061_17']) // make mandatory following fields
makeMandatory([]) // make mandatory nothing

RULE CONDITION:
    isIdentic('SAF_ID_89', 4)
RULE BODY:
    makeMandatory(['FIELD_ID_CC_061_4', 'FIELD_ID_CC_061_16', 'FIELD_ID_CC_061_17'])
        </div></pre>
    </div>
</div>