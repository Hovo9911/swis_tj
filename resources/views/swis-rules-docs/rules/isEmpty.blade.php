<div style="cursor: pointer" id="isEmpty" data-toggle="collapse" data-target="#isEmptyCollapse">
    <h3> IsEmpty</h3>
    <p> For check if given value (string, array or field) is empty</p>

    <div id="isEmptyCollapse" class="collapse">
        <p>Arguments: </p><hr>
        <p>Value = (string, array or field) </p><hr>
        <p>Example: </p>
        <pre class="prettyprint"><div>isEmpty(SAF_ID_??)
isEmpty(FIELD_ID_??)
isEmpty(0) // false
isEmpty(0.0) // false
isEmpty("0") // false
isEmpty("") // false
isEmpty(null) // false
isEmpty(false) // false
isEmpty([]) // false
isEmpty("dsfsd") // true
isEmpty("125") // true
isEmpty(125) // true

RULE CONDITION:
    isEmpty(FIELD_ID_??)
RULE BODY:
    FIELD_ID_??='something' or inRef()
        </div></pre>
    </div>
</div>