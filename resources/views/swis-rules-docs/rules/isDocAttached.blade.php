<div style="cursor: pointer" id="isDocAttached" data-toggle="collapse" data-target="#isDocAttachedCollapse">
    <h3> IsDocAttached</h3>
    <p> Function for check if document with following code is attached to sub application</p>

    <div id="isDocAttachedCollapse" class="collapse">
        <p>Arguments: </p><hr>
        <p>DocumentCode (string) <small>Document Module "document_type" field</small></p><hr>

        <p>Example: </p>
        <pre class="prettyprint"><div>isDocAttached("documentCode") // true if attached or false

RULE CONDITION:
    isDocAttached("documentCode")
RULE BODY:
    generateObligation("obligationCode", 1100)
        </div></pre>
    </div>
</div>