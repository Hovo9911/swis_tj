<div style="cursor: pointer" id="concat" data-toggle="collapse" data-target="#concatCollapse">
    <h3> Concat</h3>
    <p> Concat all argument. See examples</p>

    <div id="concatCollapse" class="collapse">
        <p>Arguments: </p><hr>
        <p>Multiple (function get infinity parameters) </p><hr>
        <p>Example: </p>
        <pre class="prettyprint"><div>concat("2334", "3001") // "23343001"
concat(SAF_ID_89,"-","10-",nowDate()) // "8910203265-10-2020-08-18"
concat(FIELD_ID_??+FIELD_ID_??, "1000") // "?????1000"

RULE CONDITION:
    isEmpty(FIELD_ID_??)
RULE BODY:
    FIELD_ID_??=concat(SAF_ID_89,"-","10-",nowDate(),currentUser())
        </div></pre>
    </div>
</div>