<div style="cursor: pointer" id="currentSubdivisionInfo" data-toggle="collapse" data-target="#currentSubdivisionInfoCollapse">
    <h3> CurrentSubdivisionInfo</h3>
    <p> Return current agency info by field name </p>

    <div id="currentSubdivisionInfoCollapse" class="collapse">
        <p>Arguments: </p><hr>
        <p>Key (name of field). Available Keys below.</p><hr>
        <pre class="prettyprint">phone_number,accreditation_number,accreditation_start_date,accreditation_end_date,
department_head_ml_tj,head_ml_tj,address_ml_tj,name_ml_tj,department_head_ml_en,head_ml_en,
address_ml_en,name_ml_en,department_head_ml_ru,head_ml_ru,address_ml_ru,name_ml_ru
        </pre>
        <p>Example: </p>
        <pre class="prettyprint"><div>currentSubdivisionInfo('phone_number') // "11111117878"
currentSubdivisionInfo('accreditation_number') // "TJ762.37100.02.0015-2015"
currentSubdivisionInfo('name_ml_en') // "Regional Testing Center of Tajikstandard Agency in Lakhsh District"
currentSubdivisionInfo('address_ml_ru') // "н. Лахш,  куч. Сомониён 34"
currentSubdivisionInfo() // ""

RULE CONDITION:
    isEmpty(FIELD_ID_??)
RULE BODY:
    FIELD_ID_??=concat(currentSubdivisionInfo('name_ml_en'),currentSubdivisionInfo('accreditation_number'))
        </div></pre>
    </div>
</div>