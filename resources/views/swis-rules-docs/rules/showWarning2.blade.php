<div style="cursor: pointer" id="showWarning2" data-toggle="collapse" data-target="#showWarning2Collapse">
    <h3> ShowWarning2</h3>
    <p> Function Show warning modal with following error message but not block action. So you can confirm action.</p>
    <p> !!! NOTE: diff between ShowWarning and ShowWarning2 is modal header key. Other logic is identic with ShowWarning</p>

    <div id="showWarning2Collapse" class="collapse">
        <p>Arguments: </p><hr>
        <p>Key (string) </p>
        <p>Fields (array) NOT REQUIRED </p><hr>

        <p>Example: </p>
        <pre class="prettyprint"><div>showWarning2('key1', ['FIELD_ID_CC_061_4', 'FIELD_ID_CC_061_16', 'FIELD_ID_CC_061_17']) // make mandatory following fields and write key in errore message section
showWarning2('key1') // show error in error section and block next action

RULE CONDITION:
    isIdentic('SAF_ID_89', 4)
RULE BODY:
    showWarning('key1', ['FIELD_ID_CC_061_4'])
        </div></pre>
    </div>
</div>