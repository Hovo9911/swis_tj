<div style="cursor: pointer" id="isIdentic" data-toggle="collapse" data-target="#isIdenticCollapse">
    <h3> IsIdentic</h3>
    <p> Function for check if in sub application all products is identic or if HsCodeGroup is given first characters in all products is identic. </p>

    <div id="isIdenticCollapse" class="collapse">
        <p>Arguments: </p><hr>
        <p>Field (string) </p>
        <p>HsCodeGroup (integer) NOT REQUIRED </p><hr>

        <p>Example: </p>
        <pre class="prettyprint"><div>isIdentic('SAF_ID_89') // true if all products SAF_ID_89 fields is identic or false
isIdentic('SAF_ID_89', 4) // true if all products SAF_ID_89 field first 4 characters is identic or false
isIdentic('SAF_ID_89', 6) // true if all products SAF_ID_89 field first 6 characters is identic or false

RULE CONDITION:
    isIdentic('SAF_ID_89', 4)
RULE BODY:
    generateObligation("obligationCode", 100)
        </div></pre>
    </div>
</div>