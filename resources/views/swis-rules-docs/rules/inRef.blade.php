<div style="cursor: pointer" id="inRef" data-toggle="collapse" data-target="#inRefCollapse">
    <h3> InRef</h3>
    <p style="color: red;"> !!! NOTE, PLEASE SEE API RULES BEFORE !!!</p>
    <p> Function able to check if row by your condition exist or not. But also function able to return any field from founded row.</p>
    <p> inRef function works only with active rows</p>

    <div id="inRefCollapse" class="collapse">
        <p>Arguments: </p><hr>
        <p>TableName (string) name is reference table name</p>
        <p>Conditions (array) SEE API RULES </p>
        <p>ReturnField (string) NOT REQUIRED</p><hr>
        <p>Example: </p>
        <pre class="prettyprint"><div>inRef("countries", [['code', '=', 'AM']]) // true if found active row where code = AM or false if not found
inRef("countries", [['code', '=', 'AM']], 'name') // 'Armenia' // return name of found row
inRef("countries", [['code', '=', 'AM'], ['name', 'startWith', 'Arm']], 'name') // // 'Armenia' // return name of found row

RULE CONDITION:
    inRef("countries", [['code', '=', 'AM']])
RULE BODY:
    FIELD_ID_??=inRef("countries", [['code', '=', 'AM']], 'name')

OR

RULE CONDITION:
    inRef("countries", [['code', '=', 'AM']], 'country_code') == '374'
RULE BODY:
    FIELD_ID_??=inRef("countries", [['code', '=', 'AM']], 'name')
        </div></pre>
    </div>
</div>