<div style="cursor: pointer" id="getBatches" data-toggle="collapse" data-target="#getBatchesCollapse">
    <h3> GetBatches</h3>
    <p style="color: red;"> !!! NOTE, PLEASE SEE API RULES BEFORE !!!</p>
    <p> Function find batches in products by condition and run one of the following aggregate functions and return the result.</p>
    <pre class="prettyprint">count(), sum($field), max($field), min($field), avg($field)</pre>

    <div id="getBatchesCollapse" class="collapse">
        <p>Arguments: </p><hr>
        <p>Conditions (array) SEE API RULES </p>
        <p>AggregateFunction  (string) </p><hr>
        <p>Example: </p>
        <pre class="prettyprint"><div>getBatches([['SAF_ID_100', '>', 200]], 'sum(FIELD_ID_??)') // Sum of FIELD_ID_?? where SAF_ID_100 > 200
getBatches([['SAF_ID_100', '=', 200]], 'sum(SAF_ID_99)') // Sum of SAF_ID_99 where SAF_ID_100 = 200
getBatches([['SAF_ID_89', 'startWith', '10']], 'sum(FIELD_ID_??)') // Sum of FIELD_ID_?? where SAF_ID_89 startWith '10'
getBatches([['SAF_ID_100', '>', 200]], 'count()') // Sum of FIELD_ID_?? where SAF_ID_100 > 200
getBatches([['SAF_ID_100', '>', 200]], 'min(FIELD_ID_??)') // min of FIELD_ID_?? where SAF_ID_100 > 200
getBatches([['SAF_ID_100', '>', 200]], 'max(FIELD_ID_??)') // max of FIELD_ID_?? where SAF_ID_100 > 200
getBatches([['SAF_ID_100', '>', 200]], 'avg(FIELD_ID_??)') // average sum of FIELD_ID_?? where SAF_ID_100 > 200
getBatches([['SAF_ID_100', '=', [200, 1000]],['SAF_ID_89', 'startWith', '8900']], 'avg(FIELD_ID_??)') // average sum of FIELD_ID_?? where (SAF_ID_100 = 200 or SAF_ID_100 = 1000) and SAF_ID_89 startWith '8900'

RULE CONDITION:
    getBatches([['SAF_ID_100', '>', 200]], 'count()') > 100
RULE BODY:
    FIELD_ID_??=getBatches([['SAF_ID_100', '=', 200]], 'sum(SAF_ID_99)')

        </div></pre>
    </div>
</div>