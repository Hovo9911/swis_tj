<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Swis Rules - User Documentation</title>
    <link href="http://fonts.googleapis.com/css?family=Raleway:700,300" rel="stylesheet"
          type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>body, html {    margin: 0;    padding: 0;}body, input,h1,h2,h3,h4,h5,h6 {    font-family: "Raleway", Helvetica, Arial, "Lucida Grande", sans-serif;     font-weight: 300;    font-size: 18px;}body {    line-height: 2;    color: #444;    background: #fff;}img, iframe {    max-width: 100%;}iframe {    max-height: 100%;}img {    height:auto;}iframe {    border: 0 !important;}header {    width: 100%;    overflow: hidden;}.container {    width: 95%;    max-width: 950px;    margin: 0 auto;    position: relative;}strong, dt, h3, h4 {    font-weight: 700;}h1,h2,h3,h4,h5,h6 {    margin: 10px 0;    line-height: 20px;    color: inherit;    text-rendering: optimizelegibility;}h1,h2,h3 {    line-height: 40px;}h2 {    font-size: 50px;    line-height: 1.2;}h3 {    font-size: 24.5px;}h4 {    font-size: 17.5px;}h5 {    font-size: 14px;}h6 {    font-size: 11.9px;}hr {    background: #EEE;    border: 0;    height: 1px;    margin: 40px 0 40px;}blockquote {    margin: 1em 0;    border-left: 3px solid #ccc;    padding-left: 20px;    text-align: left;}dt {}dd {    padding: 0;    margin: 0 0 25px 0;}a {    -webkit-transition: all ease 150ms;    -moz-transition: all ease 150ms;    -o-transition: all ease 150ms;    transition: all ease 150ms;    text-decoration: none;    color: #409f89;}a:hover {    text-decoration: underline;}a:active {    color: #47b5e2;}/* Header Styles */header {    padding: 2em 0 2em 0;     text-align: center;     background: #409f89;     color: #fff;}h1 {    margin: 0;    padding: 0;    float: left;	color:#fff;	font-weight:bold;	font-size:35px;	text-transform:uppercase;	padding-top: 10px;	margin-right:30px;}header h2 {    margin: 0 0 1em 0;}header h2.docs-header {    margin: 0;}footer {    text-align: center;    padding: 1.5em 0;}footer p {    margin: 0;    color: #999;}/* Navigation Styles */nav {    background: #444 ;    padding: 10px 0;    min-height: 60px;}nav ul, nav li {    margin: 0;    padding: 0;    list-style: none;}nav a {    padding: 0 1em;    color: #EEE;    font-size: .9em;    height: 60px;    line-height: 60px;    display: block;	background:#444;}nav h1 a {    padding: 7px 1em;    height: 46px;    line-height: 0;	height: 45px;	line-height: 45px;}nav a:hover {    background: #409f89;    text-decoration: none;}nav a:active {    background: #27637e;}nav a:active {    color: #fff;    cursor: default;}nav ul.toplinks {    padding: 20px 0 0 0;}nav #menu {    overflow: hidden;    max-height: 0;    clear: left;}nav #menu-toggle {    position: absolute;    right: 0;    top: 0;    font-size: 1.5em;    padding: 0 16px;}@media only screen and (min-width: 680px) {    nav, nav #menu {        height: 60px !important;    }    nav li, nav a {        float: left;    }    nav ul.toplinks {        float: left;        padding: 0;        clear: none;    }    nav ul.toplinks li {        margin: 0 0 0 10px;    }    nav #menu-toggle {        display: none !important;    }    nav #menu {        max-height: 9999px;        clear: none;    }}/* Content Styles */section { padding: 1em 0 3em; text-align: center; }section.vibrant { background: #222; color: #fff; }nav:before, nav:after, header:before, header:after, section:before, section:after {    content: " ";    display: table;}nav:after, header:after, section:after  { clear: both; }nav, header, section { *zoom: 1; }/* Form Styles */input {    display: block;    vertical-align: middle;    line-height: 30px;    margin: 0 auto;    width: 100%;    max-width: 400px;       -moz-box-sizing: border-box;    -webkit-box-sizing: border-box;            box-sizing: border-box;    -webkit-transition: all linear 0.2s;    -moz-transition: all linear 0.2s;    -o-transition: all linear 0.2s;    transition: all linear 0.2s;}input:focus {    border-color: #007eb2;    outline: 0;}.docs-nav {    background-color: #f5f5f5;    list-style: none;    margin: 0 0 0 20px;    padding: 15px 20px;    font-size: 0.97em;}.docs-nav a {    display: block;    margin: 0 -20px;    padding: 0 20px;    text-decoration: none;    border-right: 2px solid transparent;}@media only screen and (min-width: 960px) {        .docs-nav {        position: absolute;        top: 0;        width: 300px;        -webkit-transition: top linear 50ms;        -moz-transition: top linear 50ms;        -o-transition: top linear 50ms;        transition: top linear 50ms;    }    .docs-nav a:hover {        background: #eee;    }    .docs-nav a:active,     .docs-nav .active {        background: #eee;        border-right: 2px solid #ccc;    }    .docs-nav .separator {        height: 20px;    }    .docs-content {        padding-left: 360px;    }    header {        padding: 4em 0 4em 0;     }    .container {        max-width: 1200px;        padding: 0 20px;    }    section { padding: 3em 0; text-align: left; }    section.centered {        text-align: center;    }    input {        display: inline-block;    }}</style>
    <style>.pln {  color: #333333;}.prettyprint{overflow:auto!important;}@media screen { .str {    color: #dd1144;  }  .kwd {    color: #333333;  }  /* a comment */  .com {    color: #999988;  }  /* a type name */  .typ {    color: #445588;  }  /* a literal value */  .lit {    color: #445588;  }  /* punctuation */  .pun {    color: #333333;  }  /* lisp open bracket */  .opn {    color: #333333;  }  /* lisp close bracket */  .clo {    color: #333333;  }  /* a markup tag name */  .tag {    color: navy;  }  /* a markup attribute name */  .atn {    color: teal;  }  /* a markup attribute value */  .atv {    color: #dd1144;  }  /* a declaration */  .dec {    color: #333333;  }  /* a variable name */  .var {    color: teal;  }  /* a function name */  .fun {    color: #990000;  }}/* Use higher contrast and text-weight for printable form. */@media print, projection {  .str {    color: #006600;  }  .kwd {    color: #006;    font-weight: bold;  }  .com {    color: #600;    font-style: italic;  }  .typ {    color: #404;    font-weight: bold;  }  .lit {    color: #004444;  }  .pun, .opn, .clo {    color: #444400;  }  .tag {    color: #006;    font-weight: bold;  }  .atn {    color: #440044;  }  .atv {    color: #006600;  }}/* Style */pre.prettyprint {  background: #F0F0F0;  font-family: Menlo, "Bitstream Vera Sans Mono", "DejaVu Sans Mono", Monaco, Consolas, monospace;  font-size: 12px;  line-height: 1.5;  border: 1px solid #cccccc;  padding: 10px !important;}/* Specify class=linenums on a pre to get line numbering */ol.linenums {  margin-top: 0;  margin-bottom: 0;  color: #888;}.linenums li {  background: #FAFAFA;  padding-left: 10px;  border-left: 1px solid #CCC;}.linenums li {  padding-top: 5px;}.linenums li + li {  padding-top: 0;}.linenums li:last-child {  padding-bottom: 5px;}/* IE indents via margin-left */li.L0,li.L1,li.L2,li.L3,li.L4,li.L5,li.L6,li.L7,li.L8,li.L9 {  /* */}/* Alternate shading for lines */li.L1,li.L3,li.L5,li.L7,li.L9 {  /* */}</style>
</head>
<body>
<header>
    <div class="container">
        <h2 class="docs-header"> Swis Rules - User Documentation </h2>
    </div>
</header>
<section>
    <div class="container">

        <ul class="docs-nav" id="menu-left">
            <li><strong>Calculation Rules</strong></li>
            <li class="separator"></li>
            <li><a href="#in" class="">In</a></li>
            <li><a href="#roundingUp">RoundingUp</a></li>
            <li><a href="#roundingDown">RoundingDown</a></li>
            <li><a href="#startWith">StartWith</a></li>
            <li><a href="#endWith">EndWith</a></li>
            <li><a href="#contains">Contains</a></li>
            <li><a href="#isEmpty">IsEmpty</a></li>
            <li><a href="#nowDate">NowDate</a></li>
            <li><a href="#currentUser">CurrentUser</a></li>
            <li><a href="#concat">Concat</a></li>
            <li><a href="#subapplicationManuallyAdded">SubapplicationManuallyAdded</a></li>
            <li><a href="#currentUserShortName">CurrentUserShortName</a></li>
            <li><a href="#yearEnd">YearEnd</a></li>
            <li><a href="#isSafType">IsSafType</a></li>
            <li><a href="#formatNumber">FormatNumber</a></li>
            <li><a href="#currentUserInfo">CurrentUserInfo</a></li>
            <li><a href="#currentAgencyInfo">CurrentAgencyInfo</a></li>
            <li><a href="#currentSubdivisionInfo">CurrentSubdivisionInfo</a></li>
            <li><a href="#inRef">InRef</a></li>
            <li><a href="#getProducts">GetProducts</a></li>
            <li><a href="#setProductValues">SetProductValues</a></li>
            <li><a href="#getBatches">GetBatches</a></li>
            <li><a href="#exchangeRate">ExchangeRate</a></li>
            <li><a href="#hoursSpent">HoursSpent</a></li>
            <li><a href="#hoursLeft">HoursLeft</a></li>
            <li><a href="#nextId">NextId</a></li>
            <li><a href="#generateObligation">GenerateObligation</a></li>
            <li><a href="#isDocAttached">IsDocAttached</a></li>
            <li><a href="#notRespondingInstruction">NotRespondingInstruction</a></li>
            <li><a href="#isIdentic">IsIdentic</a></li>
            <li><a href="#unansweredInstructionType">UnansweredInstructionType</a></li>
            <li><a href="#negativeAnsweredInstructionType">NegativeAnsweredInstructionType</a></li>
            <li><a href="#getLabObligations">GetLabObligations</a></li>
            <li><a href="#unansweredExamination">UnansweredExamination</a></li>
            <li class="separator"></li>
            <li><strong>Validation Rules</strong></li>
            <li class="separator"></li>
            <li><a href="#makeMandatory">MakeMandatory</a></li>
            <li><a href="#showError">ShowError</a></li>
            <li><a href="#showWarning">ShowWarning</a></li>
            <li><a href="#showWarning2">ShowWarning2</a></li>
            <li><a href="#makeLabMandatory">MakeLabMandatory</a></li>

        </ul>

        <div class="docs-content">
            <h2> Getting Started</h2>
            <hr style="height:2px;background-color:black">
            @include('swis-rules-docs.rules.in')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.roundingUp')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.roundingDown')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.startWith')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.endWith')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.contains')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.isEmpty')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.nowDate')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.currentUser')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.concat')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.subapplicationManuallyAdded')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.currentUserShortName')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.yearEnd')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.isSafType')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.formatNumber')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.currentUserInfo')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.currentAgencyInfo')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.currentSubdivisionInfo')
            <hr style="height:2px;background-color:black">
            <h2>Api Rules</h2>
            <hr style="height:2px;background-color:black">

            <p>InRef, GetProducts, SetProductValues, GetBatches api rules use same syntax for fetch data, So I want to show that syntax for writing sql conditions before start.</p>

            <pre class="prettyprint">[] this is empty condition</pre>
            <pre class="prettyprint">[[],[]] every array in main array works as "AND" for condition ( SQL AND )</pre>
            <pre class="prettyprint">this is simple example where we want to get.</pre>
            <pre class="prettyprint">[['id', '=', 11]] where id = 11, Or [['code', '=', '0009']] where code = '0009'</pre>
            <pre class="prettyprint">[['id', '=', 11],['code', '=', '0009']] where id = 11 and code = '0009'</pre>
            <pre class="prettyprint">We also can use "OR" condition in value part. See example bellow`</pre>
            <pre class="prettyprint">[['id', '=', [11, 12, 13]]] where (id = 11 or id = 12 or id = 13)</pre>
            <pre class="prettyprint">[['id', '=', [11, 12, 13]],['code', '=', '0009']] where (id = 11 or id = 12 or id = 13) and code = '0009'</pre>
            <pre class="prettyprint">[['id', '=', [11, 12, 13]],['code', '=', ['0009', 'code2']]]
where (id = 11 or id = 12 or id = 13) and (code = '0009' or code = 'code2')</pre>
            <pre class="prettyprint">Also we have other operators for conditions. See bellow</pre>
            <pre class="prettyprint">[['id', '>', 11]] where id > 11</pre>
            <pre class="prettyprint">[['code', 'startWith', 'abc']] where code ilike "abc%"</pre>
            <pre class="prettyprint">[['id', 'endWith', 'rew']] where code ilike "%rew"</pre>
            <pre class="prettyprint">[['id', '>=', 11]] where id >= 11</pre>
            <pre class="prettyprint">[['SAF_ID_98', '>=', 100]] where id >= 11</pre>
            <pre class="prettyprint">[['FIELD_ID_??', '=', 'qwerty']] where id >= 11</pre>

            <p>!!! F.I.Y !!!</p>
            <p>If the field in condition has reference, the rule automatically connect with that reference and check code</p>
            <p>For Example`</p>

            <pre class="prettyprint">if FIELD_ID_?? in mdm has reference to any ref table we can just write rule like example in below.
[['FIELD_ID_??', '=', 'AM']]
Rule make join with reference and check code. Also that is working for saf field
[['SAF_ID_??', '=', 'KG']]
            </pre>


            <p>That's it. Lets start. ;) </p>

            <hr style="height:2px;background-color:black">
            @include('swis-rules-docs.rules.inRef')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.getProducts')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.setProductValues')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.getBatches')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.exchangeRate')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.hoursSpent')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.hoursLeft')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.nextId')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.generateObligation')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.isDocAttached')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.notRespondingInstruction')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.isIdentic')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.unansweredInstructionType')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.negativeAnsweredInstructionType')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.getLabObligations')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.unansweredExamination')
            <hr style="height:2px;background-color:black">

            <h2>Validation Rules</h2>

            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.makeMandatory')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.showError')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.showWarning')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.showWarning2')
            <hr style="height:2px;background-color:black">

            @include('swis-rules-docs.rules.makeLabMandatory')
            <hr style="height:2px;background-color:black">
        </div>
    </div>
</section>
<section class="vibrant centered">
    <div class="container">
        <h4> This is documentation for swis Routing, Calculation, Validation and Api rules</h4>
    </div>
</section>
<footer>
    <div class="container">
        <p> &copy; SWIS RULE USER DOCUMENTATION </p>
    </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</body>
</html>
