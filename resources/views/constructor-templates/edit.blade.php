@extends('layouts.app')

@section('title'){{trans('swis.constructor_templates.forms_title')}}@endsection

@section('contentTitle') {{trans('swis.constructor_templates.forms_title')}} @endsection

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@endsection

@section('content')
    <?php
    switch ($saveMode) {
        case 'add':
            $url = 'constructor-templates';
            break;
        default:
            $url = 'constructor-templates/update/' . $constructorTemplate->id;

            $mls = $constructorTemplate->ml()->notDeleted()->base(['lng_id', 'template_name', 'show_status'])->keyBy('lng_id');

            break;
    }

    $languages = activeLanguages();
    ?>

    <form class="form-horizontal fill-up " id="form-data" action="{{urlWithLng($url)}}" autocomplete="off">
        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-general"> {{trans('core.base.tab.general')}}</a></li>
                @foreach($languages as $language)
                    <li>
                        <a data-toggle="tab" href="#tab-{{$language->code}}">{{ trans("core.base.language.".strtolower($language->name)) }}</a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content">
                <div id="tab-general" class="tab-pane active">
                    <div class="panel-body">

                        <div class="form-group required">
                            <label class="col-lg-2 control-label">{{trans('swis.constructor_templates.description.label')}}</label>
                            <div class="col-lg-8">
                                <input value="{{ isset($constructorTemplate) ? $constructorTemplate->description : ''}}"
                                       type="text" name="description" placeholder="" class="form-control">
                                <div class="form-error" id="form-error-description"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('swis.constructor_templates.qr_code.label')}}</label>
                            <div class="col-lg-8">
                                <input type="checkbox" name="qr_code"
                                       value="1" {{isset($constructorTemplate) && $constructorTemplate->qr_code ? 'checked' : ''}}>
                            </div>
                        </div>
                        <div class="form-group  {{(isset($constructorTemplate) && $constructorTemplate->qr_code == 1) ? '' :'hide'}}">
                            <label class="control-label col-lg-2">{{ trans('swis.constructor_state.authorization_without_login') }}</label>
                            <div class="col-lg-8">
                                <label>
                                    <input type="checkbox" name="authorization_without_login" value="1"
                                           id="authorizationWithoutLogin"
                                           @if(isset($constructorTemplate) && $constructorTemplate->authorization_without_login) checked @endif
                                    >
                                </label>
                                <div class="form-error" id="form-error-authorization_without_login"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">{{trans('swis.constructor_templates.show_status.label')}}</label>
                            <div class="col-lg-10 general-status">
                                @php
                                    $activeStatus   = \App\Models\ConstructorDocument\ConstructorDocument::STATUS_ACTIVE;
                                    $inactiveStatus = \App\Models\ConstructorDocument\ConstructorDocument::STATUS_INACTIVE;
                                @endphp
                                <select name="show_status" class="form-show-status ">
                                    <option value="{{$activeStatus}}"{{(isset($constructorTemplate) && $constructorTemplate->show_status == $activeStatus) || $saveMode == 'add' ?  ' selected' : ''}}>{{trans('core.base.label.status.active')}}</option>
                                    <option value="{{$inactiveStatus}}"{{isset($constructorTemplate) &&  $constructorTemplate->show_status == $inactiveStatus ? 'selected': ''}}>{{trans('core.base.label.status.inactive')}}</option>
                                </select>
                                <div class="form-error" id="form-error-show_status"></div>
                            </div>
                        </div>
                    </div>
                </div>
                @foreach($languages as $language)
                    <?php $lngId = $language->id ?>
                    <div id="tab-{{$language->code}}" class="tab-pane">
                        <div class="panel-body">

                            <div class="form-group required">
                                <label class="col-lg-2 control-label">{{trans('swis.constructor_templates.name.label')}}</label>
                                <div class="col-lg-8">
                                    <input value="{{ isset($mls[$lngId]) ? $mls[$lngId]->template_name : ''}}"
                                           type="text" name="ml[{{$language->id}}][template_name]" placeholder=""
                                           class="form-control">
                                    <div class="form-error" id="form-error-ml-{{$language->id}}-template_name"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </form>
@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script src="{{asset('js/constructor-templates/main.js')}}"></script>
@endsection