@extends('layouts.app')

@section('title'){{trans('swis.language.title')}}@stop

@section('contentTitle')
    @switch($saveMode)

        @case('add')
        {{trans('swis.language.create.title')}}
        @break
        @case('edit')
        {{trans('swis.base.edit')}}
        @break

    @endswitch
@stop

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! $saveMode == 'edit' ? renderDeleteButton() : '' !!}
    {!! renderCancelButton() !!}
@stop

@section('content')
<?php

switch ($saveMode) {
    case 'add':
        $url = 'language';
        break;
    default:
        $url = $saveMode != 'view' ? 'language/update/' . $language->id : '';

        break;
}
?>

<div class="tab-content">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form class="form-horizontal fill-up" id="form-data" action="{{urlWithLng($url)}}">

                <div class="form-group required"><label class="col-lg-2 control-label">{{trans('core.base.label.name')}}</label>
                    <div class="col-lg-10">
                        <input value="{{$language->name or ''}}" name="name" type="text" placeholder="Armenian" class="form-control">
                        <div class="form-error" id="form-error-name"></div>
                    </div>
                </div>

                <div class="form-group required"><label class="col-lg-2 control-label">{{trans('core.base.label.code')}}</label>
                    <div class="col-lg-10">
                        <input  name="code" value="{{$language->code or ''}}" type="text" placeholder="am" class="form-control">
                        <div class="form-error" id="form-error-code"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2">{{trans('core.base.label.status')}}</label>
                    <div class="col-lg-10 general-status">
                        <select name="show_status" class="form-show-status">
                            <option value="{{\App\Models\Languages\Language::STATUS_ACTIVE}}"{{(!empty($language) && $language->show_status == \App\Models\Languages\Language::STATUS_ACTIVE) || $saveMode == 'add' ?  ' selected="selected"' : ''}}>{{trans('core.base.label.status.active')}}</option>
                            <option value="{{\App\Models\Languages\Language::STATUS_INACTIVE}}"{{!empty($language) &&  $language->show_status == \App\Models\Languages\Language::STATUS_INACTIVE ? ' selected="selected"': ''}}>{{trans('core.base.label.status.inactive')}}</option>
                        </select>
                        <div class="form-error" id="form-error-show_status"></div>
                    </div>
                </div>

                <input type="hidden" value="{{$language->id or 0}}" name="id" class="mc-form-key" />

                {!! csrf_field() !!}
            </form>
        </div>
    </div>
</div>
@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! $saveMode == 'edit' ? renderDeleteButton() : '' !!}
    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script src="{{asset('js/languages/main.js')}}"></script>
@endsection