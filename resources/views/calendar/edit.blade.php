@extends('layouts.app')

@section('title'){{trans('swis.calendar.title')}}@stop

@section('contentTitle') {{trans('swis.calendar.title')}} @stop

@section('form-buttons-top')
    {!! renderWeekendDaysButton() !!}
@stop

@section('content')

    <div class="tabs-container">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tab-{{ now()->year}}"
                                  data-year="{{now()->year}}"> {{ now()->year}}</a></li>
            <li class=""><a data-toggle="tab" href="#tab-{{ now()->year+1}}"
                            data-year="{{now()->year+1}}">{{ now()->year+1 }}</a></li>
        </ul>
        <div class="tab-content">
            <div id="tab-{{ now()->year}}" class="tab-pane active">
                <h2>{{trans('swis.calendar.title.select_non_working_days')}}</h2>
                <div class="alert alert-danger hide">{{trans('swis.calendar.invalid_date')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="panel-body">
                    <div class="row calendar-content calendar-1"></div>
                </div>
            </div>
            <div id="tab-{{now()->year+1}}" class="tab-pane">
                <h2>{{trans('swis.calendar.title.select_non_working_days')}}</h2>
                <div class="alert alert-danger hide">{{trans('swis.calendar.invalid_date')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="panel-body">
                    <div class="row calendar-content calendar-2"></div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
@stop

@section('scripts')
    <script>
        let dateFormat = '<?=config('swis.date_format_front')?>';
        let currentYearNonWorkingDays = ["<?=implode('","', $currentYearNonWorkingDays)?>"];
        let nextYearNonWorkingDays = ["<?=implode('","', $nextYearNonWorkingDays)?>"];
        let currentYear = '<?=now()->year?>';
        let nextYear = '<?=(int)(now()->year) + 1?>';
        let language = '<?=currentLanguage()->code?>';
    </script>
    <script src="{{asset('assets/helix/core/js/Calendar.js')}}"></script>
    <script src="{{asset('js/calendar/main.js')}}"></script>
@endsection
