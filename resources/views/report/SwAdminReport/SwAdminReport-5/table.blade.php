@if($renderHtml)
    @if(count($reportData['sub_division_info']) > 0)
        <?php

        $headName = $data['head_name'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $downloadType = $data['download_type'];
        ?>
        <table class="table table-striped table-bordered table-hover" border="1" id="data-table"
               @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="{{count($reportData['constructor_states']) + 3}}"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                    ['first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end'])),
                    'document'=>$reportData['constructor_doc_name'],
                    'agency_name'=>$reportData['agency_name'],
                    'regime_import'=> isset($data['regime'][0]) ? trans('swis.'.$data['regime'][0].'_title')  : '',
                    'regime_export'=> isset($data['regime'][1]) ? trans('swis.'.$data['regime'][1].'_title')  : '',
                    'regime_transit'=> isset($data['regime'][2]) ? trans('swis.'.$data['regime'][2].'_title')  : '',
                    ])
                     }}</h3>
                </th>
            </tr>
            <tr>
                <td>№</td>
                <td class="text-left">{{trans('swis.report.'.$reportCode.'.subdivision')}}</td>
                @foreach($reportData['constructor_states'] as $state)
                    <td>{{$state->state_name}}</td>
                @endforeach
                <td>{{trans('swis.report.'.$reportCode.'.all_states')}}</td>
            </tr>
            </thead>
            <tbody>
            <?php  $i = 1;$stateTotal = [];$allStateTotal = 0 ?>
            @foreach($reportData['sub_division_info'] as $code => $subDivision)
                <?php
                $rowTotal = 0;
                $refSubDivision = getReferenceLastRowByCode(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS,$code);
                ?>
                <tr>
                    <td>{{$i}}</td>
                    <td class="text-left">{{$refSubDivision->name ?? ''}}</td>
                    @foreach($reportData['constructor_states'] as $state)
                        <?php
                        $stateCountSubApplications = 0;
                        if (isset($subDivision[$state->id])) {
                            $stateCountSubApplications = $subDivision[$state->id];
                            $stateTotal[$state->id] = isset($stateTotal[$state->id]) ? $stateTotal[$state->id] + $stateCountSubApplications : $stateCountSubApplications;
                        }

                        $rowTotal += $stateCountSubApplications;
                        ?>
                        <td>{{$stateCountSubApplications}}</td>
                    @endforeach
                    <td>{{$rowTotal}}</td>
                </tr>
                <?php $i++?>
            @endforeach
            <tr>
                <td colspan="2">{{trans('swis.report.'.$reportCode.'.all_total')}}</td>
                @foreach($reportData['constructor_states'] as $state)
                    <?php
                    $currentTotal = isset($stateTotal[$state->id]) ? $stateTotal[$state->id] : 0;
                    $allStateTotal += $currentTotal;
                    ?>
                    <td>{{$currentTotal}}</td>
                @endforeach
                <td><b>{{$allStateTotal}}</b></td>
            </tr>
            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@if(count($reportData['sub_division_info']) < 1)
    <h2 class="text-center gray-bg p-m">{{trans('swis.report.not_data.available')}}</h2>
@endif

@include('report.partials.download-pdf-link')