@extends('layouts.app')

@section('title') {{trans('swis.'.$reportCode.'.menu.title')}} @endsection

@section('contentTitle') {{trans('swis.'.$reportCode.'.menu.title')}} @endsection

@section('content')
    <div class="search-data">
        <form class="form-horizontal fill-up" id="form-data" action="{{urlWithLng('report/'.$reportCode.'/generate')}}">
            <input type="hidden" name="report_code" value="{{$reportCode}}">

            <div class="row">
                <div class="col-md-6">

                    @include('report.partials.first-period',['class'=>'col-md-4','label'=>trans('swis.reports.'.$reportCode.'.request_start_date_start.label')])

                    @if(\Auth::user()->isSwAdmin())
                        <div class="form-group required">
                            <label class="col-lg-4 control-label">{{trans('swis.document.select_agency')}}</label>
                            <div class="col-lg-8">
                                <select class="form-control select2 agency-list {{ (Auth::user()->isAgency()) ? 'readonly' : '' }}" id="agency" name="agency" data-statuses="1" {{ (Auth::user()->isAgency()) ? 'readonly' : '' }}>
                                    <option value=""></option>
                                    @if ($agencies->count())
                                        @foreach ($agencies as $agency)
                                            <option value="{{ $agency->tax_id }}" {{ (Auth::user()->companyTaxId() == $agency->tax_id) ? 'selected' :'' }}>{{ "({$agency->tax_id}) {$agency->current()->name}" }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <div class="form-error" id="form-error-agency"></div>
                            </div>
                        </div>
                    @endif

                    <div class="form-group required">
                        <label class="col-lg-4 control-label">{{trans('swis.document.select_document')}}</label>
                        <div class="col-lg-8">
                            <select class="form-control select2 document-list" id="document_id" name="document_id" data-statuses="1">
                                <option value=""></option>
                                @if(!\Auth::user()->isSwAdmin())
                                    @if ($documents->count())
                                        @foreach ($documents as $document)
                                            <option value="{{ $document->id }}">{{ $document->document_name }}</option>
                                        @endforeach
                                    @endif
                                @endif
                            </select>
                            <div class="form-error" id="form-error-document_id"></div>
                        </div>
                    </div>

                    @include('report.partials.document-status',['multiple'=>true,'required'=>false])

                </div>

                <div class="col-md-6">

                    @include('report.partials.saf-regimes',['multiple'=>true,'required'=>false])

                    @include('report.partials.subdivisions',['noData'=>true,'multiple'=>true])

                    @include('report.partials.download-types')

                </div>

                <div class="col-md-12">
                    <div class="text-center">
                        <button class="btn btn-primary">{{trans('swis.report.generate_output.button')}}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="data-table">
        <hr/>
        <div id="reportResult"></div>
    </div>

@endsection
@section('scripts')
    <script>
        var getSubdivisions = true
    </script>
    <script src="{{asset('js/report/main.js')}}"></script>
@endsection