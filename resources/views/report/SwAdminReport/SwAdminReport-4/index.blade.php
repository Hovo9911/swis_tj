@extends('layouts.app')

@section('title') {{trans('swis.'.$reportCode.'.menu.title')}} @endsection

@section('contentTitle') {{trans('swis.'.$reportCode.'.menu.title')}} @endsection

@section('content')
    <div class="search-data">
        <form class="form-horizontal fill-up" id="form-data" action="{{urlWithLng('report/'.$reportCode.'/generate')}}">
            <input type="hidden" name="report_code" value="{{$reportCode}}">

            <div class="row">
                <div class="col-md-6">

                    @include('report.partials.first-period',['class'=>'col-md-4'])

                    <div class="form-group">
                        <label class="col-lg-4 control-label">{{trans('swis.report.'.$reportCode.'.obligation.company_payer')}}</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="company_payer"  value="">
                            <div class="form-error" id="form-error-company_payer"></div>
                        </div>
                    </div>

                    @include('report.partials.saf-regimes',['multiple'=>true])

                    @include('report.partials.head-name')

                </div>

                <div class="col-md-6">

                    <div class="form-group">
                        <label class="col-lg-4 control-label">{{trans('swis.report.'.$reportCode.'.saf_number')}}</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="saf_number" value="">
                            <div class="form-error" id="form-error-saf_number"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-4 control-label">{{trans('swis.report.'.$reportCode.'.natural_person_payer')}}</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="natural_person_payer" value="">
                            <div class="form-error" id="form-error-natural_person_payer"></div>
                        </div>
                    </div>

                    @include('report.partials.download-types')

                </div>

                <div class="col-md-12">
                    <div class="text-center">
                        <button class="btn btn-primary">{{trans('swis.report.generate_output.button')}}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="data-table">
        <hr/>
        <div id="reportResult"></div>
    </div>

@endsection

@section('scripts')
    <script>
        var getSubdivisions = true
    </script>
    <script src="{{asset('js/report/main.js')}}"></script>
@endsection