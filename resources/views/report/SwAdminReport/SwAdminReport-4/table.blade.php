@if($renderHtml)
    @if(count($reportData['obligations']) > 0)
        <?php
        $headName = $data['head_name'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $downloadType = $data['download_type'];
        ?>
        <table class="table table-striped table-bordered table-hover" border="1" id="data-table"
               @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="7"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                [
                    'first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end'])),
                    'all_sum'=>$reportData['allSum'],
                ])}} </h3>
                </th>
            </tr>
            <tr>
                <th></th>
                <th>{{ trans("swis.report.{$reportCode}.saf.regime") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.saf_number") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.payment_date") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.sum") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.company_payer") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.natural_person_payer") }}</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 1; ?>
            @foreach ($reportData['obligations'] as $data)
                <tr>
                    <td>{{$i}}</td>
                    <td>{{$data['regime']}}</td>
                    <td>{{$data['regular_number']}}</td>
                    <td>{{$data['obligation_pay_date']}}</td>
                    <td>{{$data['obligation'] ?: ''}}</td>
                    <td>{{$data['obligation_company_payer'] ?: ''}}</td>
                    <td>{{$data['obligation_payer_person'] ?: ''}}</td>
                </tr>
                <?php $i++ ?>
            @endforeach
            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@if(count($reportData['obligations']) < 1)
    <h2 class="text-center gray-bg p-m">{{trans('swis.report.not_data.available')}}</h2>
@endif

@include('report.partials.download-pdf-link')