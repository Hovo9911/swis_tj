@if($renderHtml)
    @if(count($reportData) > 0)
        <?php
        $headName = $data['head_name'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $downloadType = $data['download_type'];
        ?>
        <table class="table table-striped table-bordered table-hover" border="1" id="data-table"
               @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="8"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                ['first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end']))])
                 }}</h3>
                </th>
            </tr>
            <tr>
                <th>{{ trans("swis.report.{$reportCode}.number") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.user_fullname") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.user_email") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.user_telephone") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.user_status") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.user_registration_date") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.user_companies") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.user_registered_by") }}</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 1; ?>

            @foreach ($reportData as $data)

                <?php

                $userCompanies = $data->companies()->toArray();

                $userCompaniesList = '';
                $userCompaniesArray = [];

                foreach($userCompanies as $userCompany){
                    if(!\Auth::user()->isSwAdmin() && \Auth::user()->companyTaxId() != $userCompany['tax_id']){
                        continue;
                    }
                    $userCompaniesArray[] =  $userCompany['name'];
                }

                $userCompaniesArray = array_unique($userCompaniesArray);

                $userCompaniesList = implode(', ', $userCompaniesArray);


                switch ($data['show_status']) {
                    case '1':
                        $userStatus = trans('core.base.label.status.active');
                        break;
                    case '2':
                        $userStatus = trans('core.base.label.status.inactive');
                        break;
                    case '0':
                        $userStatus = trans('core.base.label.status.deleted');
                        break;
                }

                ?>
                <tr>
                    <td>{{$i++}}</td>
                    <td>{{$data['first_name']. '  ' .$data['last_name']}}</td>
                    <td>{{$data['email']}}</td>
                    <td>{{$data['phone_number']}}</td>
                    <td>{{$userStatus}}</td>
                    <td>{{$data['registered_at']}}</td>
                    <td>{{$userCompaniesList}}</td>
                    <td>{{$data['authorized_user']}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')