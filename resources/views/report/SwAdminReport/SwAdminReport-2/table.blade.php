@if($renderHtml)
    @if(count($reportData) > 0)
        <?php
        $headName = $data['head_name'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $downloadType = $data['download_type'];
        ?>
        <table class="table table-striped table-bordered table-hover" border="1" id="data-table"
               @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="14"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                ['first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end']))])
                 }}</h3>
                </th>
            </tr>
            <tr>
                <th></th>
                <th>{{ trans("swis.report.{$reportCode}.agency.name") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.agency.subdivision.name") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.superscribe.name") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.document.type") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.saf.regime") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.subapplication.state") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.subapplication.request_date") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.subapplication.number") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.product.code") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.product.commercial_description") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.saf.applicant") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.saf.applicant_inn") }}</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 1;
            $hsCodeRef = getReferenceRowsKeyValue(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_HS_CODE_6);
            ?>

            @foreach ($reportData as $data)

                <?php
                    $safData = json_decode($data['saf_data'],true);
                    $subApplicationData = json_decode($data['sub_application_data'],true);

                    $K = $safData['saf']['sides']['import']['name'];
                    if($data['regime'] == \App\Models\SingleApplication\SingleApplication::REGIME_EXPORT){
                        $K = $safData['saf']['sides']['export']['name'];
                    }

                    $G = '';
                    if($subApplicationData && isset($subApplicationData[$data['request_date_field_id']])){
                        $G = $subApplicationData[$data['request_date_field_id']];
                    }
                ?>
                <tr>
                    <td>{{$i++}}</td>
                    <td>{{$data['agency_name']}}</td>
                    <td>{{$data['subdivision_name']}}</td>
                    <td>{{$data['superscribe_name'] ?? ''}}</td>
                    <td>{{$data['ref_classificator_name']}}</td>
                    <td>{{$data['regime']}}</td>
                    <td>{{$data['state_name']}}</td>
                    <td>{{$G}}</td>
                    <td>{{$data['document_number']}}</td>
                    <td>{{isset($hsCodeRef[$data['product_code']]) ? $hsCodeRef[$data['product_code']]['code'] : ''}}</td>
                    <td>{{$data['commercial_description']}}</td>
                    <td>{{$K}}</td>
                    <td>{{$safData['saf']['sides']['applicant']['name']}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')