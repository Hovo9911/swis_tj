@extends('layouts.app')

@section('title') {{trans('swis.'.$reportCode.'.menu.title')}} @endsection

@section('contentTitle') {{trans('swis.'.$reportCode.'.menu.title')}} @endsection

@section('content')
    <div class="search-data">
        <form class="form-horizontal fill-up" id="form-data" action="{{urlWithLng('report/'.$reportCode.'/generate')}}">
            <input type="hidden" name="report_code" value="{{$reportCode}}">

            <div class="row">
                <div class="col-md-6">

                    @include('report.partials.first-period',['class'=>'col-md-4'])

                    @include('report.partials.saf-regimes',['multiple'=>true])

                    <div class="form-group">
                        <label class="col-lg-4 control-label">{{trans('swis.reports.request_start_date_start.label')}}</label>
                        <div class="col-lg-4">
                            <div class='input-group date'>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                <input value="" name="request_start_date_start" autocomplete="off" type="text" placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                            </div>

                            <div class="form-error" id="form-error-request_start_date_start"></div>
                        </div>

                        <div class="col-lg-4">
                            <div class='input-group date'>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                <input value="" name="request_start_date_end" autocomplete="off" type="text" placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                            </div>

                            <div class="form-error" id="form-error-request_start_date_end"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-4 control-label">{{trans('swis.report.'.$reportCode.'.aplicant_type_value')}}</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="applicant_type_value"  value="">
                            <div class="form-error" id="form-error-applicant_type_value"></div>
                        </div>
                    </div>

                    @include('report.partials.head-name')

                </div>

                <div class="col-md-6">

                    @include('report.partials.document-list',['class'=>'col-md-8','multiple'=>true])

                    @include('report.partials.document-status',['class'=>'col-md-8','multiple'=>true,'required'=>false])

                    @include('report.partials.subdivisions',['multiple'=>true,'noData'=>true])

                    @include('report.partials.hs-code')

                    @include('report.partials.download-types')

                </div>

                <div class="col-md-12">
                    <div class="text-center">
                        <button class="btn btn-primary">{{trans('swis.report.generate_output.button')}}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="data-table">
        <hr/>
        <div id="reportResult"></div>
    </div>

@endsection

@section('scripts')
    <script>
        var getSubdivisions = true
    </script>
    <script src="{{asset('js/report/main.js')}}"></script>
@endsection