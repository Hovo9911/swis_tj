@if($renderHtml)
    @if(isset($reportData['data']) && count($reportData['data']) > 0)

        <?php
        $headName = $data['head_name'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $transactionDate = date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end']));
        $downloadType = $data['download_type'];
        ?>
        <table class="table table-striped table-bordered table-hover" border="1" id="data-table"
               @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="7"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                    ['first_period' => $transactionDate,
                    ])
                     }}</h3>
                </th>
            </tr>
            <tr>
                <th>№</th>
                <th>{{trans('swis.report.'.$reportCode.'.company')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.saf_count')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.transaction_date')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.transaction_amount')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.obligations_sum')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.difference')}}</th>
            </tr>
            </thead>
            <tbody>
            <?php
                $transactionAmountAll = $obligationSumAll = $safCountAll = 0;
            ?>
            @foreach($reportData['data'] as $key=>$value)
                <?php
                    $safCount = $value['saf_count'];

                    $transactionAmount = $value['transaction_amount'];
                    $obligationSum = $value['obligations_sum'];
                    $diff = $transactionAmount - $obligationSum;

                    $transactionAmountAll += $transactionAmount;
                    $obligationSumAll += $obligationSum;
                    $safCountAll += $safCount;
                ?>
                <tr>
                    <td>{{$key+1}}</td>
                    <td>({{$value['company_tax_id']}}) {{$value['company_name']}}</td>
                    <td>{{$safCount ?? 0}}</td>
                    <td>{{$transactionDate}}</td>
                    <td>{{$transactionAmount}}</td>
                    <td>{{$obligationSum ?? 0}}</td>
                    <td>{{number_format($diff, 2, '.', '')}}</td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="2"></td>
                    <td><b>{{$safCountAll}}</b></td>
                    <td></td>
                    <td><b>{{number_format($transactionAmountAll, 2, '.', '')}}</b></td>
                    <td><b>{{number_format($obligationSumAll, 2, '.', '')}}</b></td>
                    <td></td>
                </tr>
            </tfoot>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@if(empty($reportData['data']))
    <h2 class="text-center gray-bg p-m">{{trans('swis.report.not_data.available')}}</h2>
@endif

@include('report.partials.download-pdf-link')