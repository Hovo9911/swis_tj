@if($renderHtml)
    @if(count($reportData) > 0)
        <?php
        $headName = $data['head_name'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $downloadType = $data['download_type'];
        ?>
        <table class="table table-striped table-bordered table-hover" border="1" id="data-table"
               @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="16"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                ['first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end']))])
                 }}</h3>
                </th>
            </tr>
            <tr>
                <th></th>
                <th>{{ trans("swis.report.{$reportCode}.agency.name") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.agency.subdivision.name") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.superscribe.name") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.document.type") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.registration_number") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.rejection_number") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.saf.applicant") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.saf.applicant_inn") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.subapplication.request_date") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.registration_date") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.rejection_or_permission_date") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.status_end_date") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.rejection_reason") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.subapplication.state") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.saf.regime") }}</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 1;
            $hsCodeRef = getReferenceRowsKeyValue(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_HS_CODE_6);
            ?>

            @foreach ($reportData as $data)

                <?php
                $safData = json_decode($data['saf_data'], true);

                $isApproved = \App\Models\ConstructorStates\ConstructorStates::checkEndStateStatus($data['state_id'], \App\Models\ConstructorStates\ConstructorStates::END_STATE_APPROVED);
                $isRejected = \App\Models\ConstructorStates\ConstructorStates::checkEndStateStatus($data['state_id'], \App\Models\ConstructorStates\ConstructorStates::END_STATE_NOT_APPROVED);

                $M = '-';
                if ($isRejected) {
                    $M = $data['rejected_reason'] ?? '-';
                    $M = optional(getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_REJECTION_REASON, $M, false))->name ?: '-';
                }

                $G = $safData['saf']['sides']['import']['name'];
                if ($data['regime'] == \App\Models\SingleApplication\SingleApplication::REGIME_EXPORT) {
                    $G = $safData['saf']['sides']['export']['name'];
                }

                $L = $data['expiration_date'] ? formattedDate($data['expiration_date']) : '-';

                $F = '-';
                if ($isApproved) {
                    $F = $data['permission_number'];
                }

                if ($isRejected) {
                    $F = $data['rejected_number'];
                }

                ?>
                <tr>
                    <td>{{$i++}}</td>
                    <td>{{$data['agency_name']}}</td>
                    <td>{{$data['subdivision_name']}}</td>
                    <td>{{$data['superscribe_name'] ?? ''}}</td>
                    <td>{{$data['ref_classificator_name']}}</td>
                    <td>{{$data['registration_number']}}</td>
                    <td>{{$F}}</td>
                    <td>{{$G}}</td>
                    <td>{{'(' . $safData['saf']['sides']['applicant']['type_value'].')' . $safData['saf']['sides']['applicant']['name']}}</td>
                    <td>{{$data['request_date'] ? formattedDate($data['request_date'],false,true) : '-'}}</td>
                    <td>{{$data['registration_date'] ? formattedDate($data['registration_date'],false,true) : '-'}}</td>
                    <td>{{$data['rejection_or_permission_date']  ? formattedDate($data['registration_date'],false,true) : '-'}}</td>
                    <td>{{$L}}</td>
                    <td>{{$M}}</td>
                    <td>{{$data['state_name']}}</td>
                    <td>{{$data['regime']}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')