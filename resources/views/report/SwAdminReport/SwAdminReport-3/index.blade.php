@extends('layouts.app')

@section('title') {{trans('swis.'.$reportCode.'.menu.title')}} @endsection

@section('contentTitle') {{trans('swis.'.$reportCode.'.menu.title')}} @endsection

@section('content')
    <div class="search-data">
        <form class="form-horizontal fill-up" id="form-data" action="{{urlWithLng('report/'.$reportCode.'/generate')}}">
            <input type="hidden" name="report_code" value="{{$reportCode}}">

            <div class="row">
                <div class="col-md-6">

                    @include('report.partials.first-period',['class'=>'col-md-4'])

                    @include('report.partials.saf-regimes',['multiple'=>true])

                    <div class="form-group">
                        <label class="col-lg-4 control-label">{{trans('swis.reports.request_start_date_start.label')}}</label>
                        <div class="col-lg-4">
                            <div class='input-group date'>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                <input value="" name="request_start_date_start" autocomplete="off" type="text" placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                            </div>

                            <div class="form-error" id="form-error-request_start_date_start"></div>
                        </div>

                        <div class="col-lg-4">
                            <div class='input-group date'>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                <input value="" name="request_start_date_end" autocomplete="off" type="text" placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                            </div>

                            <div class="form-error" id="form-error-request_start_date_end"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-4 control-label">{{trans('swis.report.'.$reportCode.'.aplicant_type_value')}}</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="applicant_type_value"  value="">
                            <div class="form-error" id="form-error-applicant_type_value"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-4 control-label">{{trans('swis.reports.registration_start_date.label')}}</label>
                        <div class="col-lg-4">
                            <div class='input-group date'>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                <input value="" name="registration_start_date_start" autocomplete="off" type="text" placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                            </div>

                            <div class="form-error" id="form-error-registration_start_date_start"></div>
                        </div>

                        <div class="col-lg-4">
                            <div class='input-group date'>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                <input value="" name="registration_start_date_end" autocomplete="off" type="text" placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                            </div>

                            <div class="form-error" id="form-error-registration_start_date_end"></div>
                        </div>
                    </div>

                </div>

                <div class="col-md-6">

                    <div class="form-group">
                        <label class="col-lg-4 control-label">{{trans('swis.document.select_document')}}</label>
                        <div class="col-lg-8">
                            <select class="form-control select2 document-list" name="document_id[]"  multiple data-statuses="1">
                                @if ($documents->count())
                                    @foreach ($documents as $document)
                                        <option value="{{ $document->id }}">{{ $document->document_name }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div class="form-error" id="form-error-document_id"></div>
                        </div>
                    </div>

                    @include('report.partials.document-status',['multiple'=>true,'required'=>false])

                    @include('report.partials.subdivisions',['multiple'=>true,'noData'=>true])

                    @include('report.partials.hs-code')

                    <div class="form-group">
                        <label class="col-lg-4 control-label">{{trans('swis.report.'.$reportCode.'.registration_number')}}</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="registration_number" value="">
                            <div class="form-error" id="form-error-registration_number"></div>
                        </div>
                    </div>

                    @include('report.partials.download-types')

                </div>

                <div class="col-md-12">
                    <div class="text-center">
                        <button class="btn btn-primary">{{trans('swis.report.generate_output.button')}}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="data-table">
        <hr/>
        <div id="reportResult"></div>
    </div>

@endsection

@section('scripts')
    <script>
        var getSubdivisions = true
    </script>
    <script src="{{asset('js/report/main.js')}}"></script>
@endsection