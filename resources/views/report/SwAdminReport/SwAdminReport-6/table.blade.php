@if($renderHtml)
    @if(isset($reportData['data']) && count($reportData['data']) > 0)

        <?php
        $headName = $data['head_name'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $downloadType = $data['download_type'];
        ?>
        <table class="table table-striped table-bordered table-hover" border="1" id="data-table"
               @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="6"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                    ['first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end'])),
                    'broker_name'=>$reportData['saf_brokers_name'],
                    ])
                     }}</h3>
                </th>
            </tr>
            <tr>
                <th>№</th>
                <th>{{trans('swis.report.'.$reportCode.'.saf_regime')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.saf_regular_number')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.sub_application_status_date')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.sub_application_sender_user')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.saf_side_data')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($reportData['data'] as $value)
                <tr>
                    <td>{{$value['s_n']}}</td>
                    <td>{{$value['saf_regime']}}</td>
                    <td>{{$value['saf_number']}}</td>
                    <td>{{$value['sub_application_status_date']}}</td>
                    <td>{{$value['sub_application_send_user_info']}}</td>
                    <td>{{$value['side_data']}}</td>
                </tr>
            @endforeach

            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@if(empty($reportData['data']))
    <h2 class="text-center gray-bg p-m">{{trans('swis.report.not_data.available')}}</h2>
@endif

@include('report.partials.download-pdf-link')