@extends('layouts.app')

@section('contentTitle') {{trans('swis.'.$reportCode.'.menu.title')}} @endsection

@section('content')
    <div class="search-data">
        <form class="form-horizontal fill-up" id="form-data" action="{{urlWithLng('report/'.$reportCode.'/generate')}}">
            <input type="hidden" name="report_code" value="{{$reportCode}}">

            <div class="row">
                <div class="col-md-6">

                    @include('report.partials.first-period')

                    <div class="form-group">
                        <label class="col-lg-4 control-label">{{trans('swis.report.'.$reportCode.'.tax_id')}}</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="tax_id" id="taxId" value="">
                            <div class="form-error" id="form-error-tax_id"></div>
                        </div>
                    </div>

                    @php($obligationTypes = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_OBLIGATION_TYPE, false, false, ['id', 'code', 'name']))
                    <div class="form-group">
                        <label class="col-lg-4 control-label">{{trans('swis.report.'.$reportCode.'.obligation_type')}}</label>
                        <div class="col-lg-8">
                            <select class="form-control regime-type select2" data-allow-clear="false"
                                    name="obligation_type[]" multiple>
                                @foreach ($obligationTypes as $item)
                                    <option value="{{ $item->code }}"> {{ $item->name }}</option>
                                @endforeach
                            </select>
                            <div class="form-error" id="form-error-obligation_type"></div>
                        </div>
                    </div>

                    @include('report.partials.saf-regimes', ['multiple'=>true])

                    @include('report.partials.head-name')
                </div>

                <div class="col-md-6">

                    @include('report.partials.document-list',['class'=>'col-md-8','required'=>false])

                    @include('report.partials.document-status',['class'=>'col-md-8','required'=>false])

                    @include('report.partials.download-types')

                </div>

                <div class="col-md-12">
                    <div class="text-center">
                        <button class="btn btn-primary">{{trans('swis.report.generate_output.button')}}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="data-table">
        <hr/>
        <div id="reportResult"></div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/report/main.js')}}"></script>
@endsection