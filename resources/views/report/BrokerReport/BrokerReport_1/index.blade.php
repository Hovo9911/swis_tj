@extends('layouts.app')

@section('contentTitle') {{trans('swis.'.$reportCode.'.menu.title')}} @endsection

@section('content')
    <div class="search-data">
        <form class="form-horizontal fill-up" id="form-data" action="{{urlWithLng('report/'.$reportCode.'/generate')}}">
            <input type="hidden" name="report_code" value="{{$reportCode}}">

            <div class="row">

                <div class="col-md-6">
                    <div class="form-group">

                        <label class="col-md-4 control-label">{{trans('swis.reports.'.$reportCode.'.submitting_date.label')}}</label>

                        <div class="col-md-4">
                            <div class='input-group date'>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                <input value="" name="submitting_date_start" autocomplete="off" type="text" placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                            </div>
                            <div class="form-error" id="form-error-submitting_date_start"></div>
                        </div>

                        <div class="col-md-4">
                            <div class='input-group date'>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                <input value="" name="submitting_date_end" autocomplete="off" type="text" placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                            </div>
                            <div class="form-error" id="form-error-submitting_date_end"></div>
                        </div>

                    </div>

                    @include('report.partials.first-period',['required'=>false])

                    @include('report.partials.subdivisions',['multiple'=>true])

                    @include('report.partials.saf-regimes',['multiple'=>true])

                    @include('report.partials.head-name')

                </div>

                <div class="col-md-6">

                    @include('report.partials.document-list' , ['class'=>'col-lg-8'])

                    @include('report.partials.document-status' , ['class'=>'col-lg-8','required'=>false])

                    <div class="form-group">
                        <label class="col-lg-4 control-label">{{trans('swis.report.'.$reportCode.'.tax_id')}}</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="tax_id" id="taxId" value="">
                            <div class="form-error" id="form-error-tax_id"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-4 control-label">{{trans('swis.report.'.$reportCode.'.importer_exporter.phone_number')}}</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="phone_number" value="">
                            <div class="form-error" id="form-error-phone_number"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-4 control-label">{{trans('swis.report.'.$reportCode.'.importer_exporter.email')}}</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="email" value="">
                            <div class="form-error" id="form-error-email"></div>
                        </div>
                    </div>

                    @include('report.partials.download-types')

                </div>

                <div class="col-md-12">
                    <div class="text-center">
                        <button class="btn btn-primary">{{trans('swis.report.generate_output.button')}}</button>
                    </div>
                </div>

            </div>
        </form>
    </div>

    <div class="data-table">
        <hr/>
        <div id="reportResult"></div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/report/main.js')}}"></script>
@endsection