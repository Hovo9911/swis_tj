@if($renderHtml)
    @if(count($reportData) > 0)
        <?php

        $constructorDocument = App\Models\ConstructorDocument\ConstructorDocumentMl::select('document_name')->where('constructor_documents_id', $data['document_id'])->where('lng_id', cLng('id'))->first();

        $constructorDocumentName =  optional($constructorDocument)->document_name;

        $headName = $data['head_name'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $downloadType = $data['download_type'];

        ?>
        <table class="table table-striped table-bordered table-hover" border="1" id="data-table"
               @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="16"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                ['first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end'])),
                 'document_name' => $constructorDocumentName])
                 }}</h3>
                </th>
            </tr>
            <tr>
                <th>{{ trans("swis.report.{$reportCode}.number") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.saf_regime") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.saf_sub_application_number") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.saf_side_info") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.subdivision") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.document") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.saf_applicant_type_value") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.saf_side_type_value") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.saf_sub_application_submitting_date") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.state_name") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.saf_sub_application_permission_or_rejection_date") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.saf_sub_application_permission_or_rejection_number") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.saf_sub_application_permission_or_rejection_reason") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.saf_sub_application_request_date") }}</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 1; $reportData = array_unique($reportData, SORT_REGULAR); ?>

            @foreach ($reportData as $data)

                <tr>
                    <td>{{$i++}}</td>
                    <td>{{$data['saf_regime']}}</td>
                    <td>{{$data['saf_sub_application_number']}}</td>
                    <td>{{$data['subdivision']}}</td>
                    <td>{!! $data['subDivision_names'] !!}</td>
                    <td>{{$data['document']}}</td>
                    <td>{{$data['saf_applicant_type_value']}}</td>
                    <td>{{$data['saf_side_type_value']}}</td>
                    <td>{{$data['saf_sub_application_submitting_date']}}</td>
                    <td>{{$data['state_name']}}</td>
                    <td>{{$data['saf_sub_application_permission_or_rejection_date']}}</td>
                    <td>{{$data['saf_sub_application_permission_or_rejection_number']}}</td>
                    <td>{{$data['saf_sub_application_permission_or_rejection_reason']}}</td>
                    <td>{{$data['saf_sub_application_request_date']}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')