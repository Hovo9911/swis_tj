@if($renderHtml)
    @if(count($reportData) > 0)
        <?php
        $headName = $data['head_name'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $downloadType = $data['download_type'];
        ?>
        <table class="table table-striped table-bordered table-hover" border="1" id="data-table"
               @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="7"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                    ['first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end']))]
                    )}}</h3>
                </th>
            </tr>
            <tr>
                <th>#</th>
                <th>{{ trans("swis.report.{$reportCode}.permission_date") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.expiration_date") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.permission_number") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.export_country") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.importer") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.commercial_description") }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($reportData as $key=>$data)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$data['permission_date'] ? formattedDate($data['permission_date']) : ''}}</td>
                    <td>{{$data['expiration_date'] ? formattedDate($data['expiration_date']) : '' }}</td>
                    <td>{{$data['permission_number']}}</td>
                    <td>{{$data['export_country']}}</td>
                    <td>{{$data['importer']}}</td>
                    <td>{{$data['product_commercial_description']}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if($downloadType != \App\Models\Reports\Reports::DOWNLOAD_TYPE_HTML)
            <table style="width: 100%;padding-top: 5px">
                <tbody>
                <tr>
                    <td></td>
                    <td>{{$headName ?? '' }}</td>
                    <td>{{$borderHeadName ?? \Auth::user()->name()}}</td>
                </tr>
                </tbody>
            </table>
        @endif
    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')
