@extends('layouts.app')

@section('contentTitle') {{trans('swis.'.$reportCode.'.menu.title')}} @endsection

@section('content')

    <div class="search-data">
        <form class="form-horizontal fill-up" id="form-data" action="{{urlWithLng('report/'.$reportCode.'/generate')}}">
            <input type="hidden" name="report_code" value="{{$reportCode}}">

            <div class="row">
                <div class="col-md-6">

                    @include('report.partials.first-period',['label'=>trans('swis.reports.permission_date.first_period.label')])

                    @include('report.partials.subdivisions',['noData'=>false])

                    @include('report.partials.saf-importer')

                    @include('report.partials.head-name')
                </div>

                <div class="col-md-6">

                    @include('report.partials.product-commercial-description')

                    @include('report.partials.countries-list',['label'=>trans('swis.report.'.$reportCode.'.producer.country'),'name'=>'producer_country'])

                    @include('report.partials.download-types')

                </div>

                <div class="col-md-12">
                    <div class="text-center">
                        <button class="btn btn-primary">{{trans('swis.report.generate_output.button')}}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="data-table">
        <hr/>
        <div id="reportResult"></div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/report/main.js')}}"></script>
@endsection