@if($renderHtml)
    @if(count($reportData) > 0)
        <?php
        $headName = $data['head_name'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $downloadType = $data['download_type']
        ?>
        <table class="table table-striped table-bordered table-hover" border="1" id="data-table"
               @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="9"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                    ['first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end']))]
                    )}}</h3>
                </th>
            </tr>
            <tr>
                <th class="border-left border-top" width="4%">№</th>
                <th class="border-left border-top "
                    width="26%">{{ trans("swis.report.{$reportCode}.export_company") }}</th>
                <th class="border-left border-top"
                    width="10%">{{ trans("swis.report.{$reportCode}.product_name") }}</th>
                <th class="border-left border-top"
                    width="10%">{{ trans("swis.report.{$reportCode}.unit_measurement") }}</th>
                <th class="border-left border-top" width="10%">{{ trans("swis.report.{$reportCode}.amount") }}</th>
                <th class="border-left border-top"
                    width="10%">{{ trans("swis.report.{$reportCode}.product_national_value") }}</th>
                <th class="border-left border-top"
                    width="15%">{{ trans("swis.report.{$reportCode}.importer_country") }}</th>
                <th class="border-left border-top border-right"
                    width="15%">{{ trans("swis.report.{$reportCode}.sub_division") }}</th>
            </tr>
            </thead>
            <tbody>
            <?php  $i = 1;$totalAmount = 0 ?>
            @foreach ($reportData as $data)
                <?php $totalAmount += $data['F'] ?>
                <tr>
                    <td class="border-left border-top border-bottom" width="4%">{{$i++}}</td>
                    <td class="border-left border-top border-bottom tl" width="26%">{{$data['B']}}</td>
                    <td class="border-left border-top border-bottom tl" width="10%">{{$data['C']}}</td>
                    <td class="border-left border-top border-bottom" width="10%">{!! $data['D'] !!}</td>
                    <td class="border-left border-top border-bottom" width="10%">{{$data['E']}}</td>
                    <td class="border-left border-top border-bottom" width="10%">{{$data['F']}}</td>
                    <td class="border-left border-top border-bottom tl" width="15%">{{$data['G']}}</td>
                    <td class="border-left border-top border-bottom border-right" width="15%">{{$data['H']}}</td>
                </tr>
            @endforeach
            <tr>
                <td class="border-left border-top border-bottom tr"
                    colspan="5">{{ trans("swis.report.{$reportCode}.total_amount") }}</td>
                <td class="border-left border-top border-bottom">{{$totalAmount}}</td>
                <td class="border-left border-top border-bottom"></td>
                <td class="border-left border-top border-bottom border-right"></td>
            </tr>
            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')

<style>
    .tl {
        text-align: left;
    }

    .tr {
        text-align: right;
    }

    .border-left {
        border-left: 1px solid black;
    }

    .border-right {
        border-right: 1px solid black;
    }

    .border-top {
        border-top: 1px solid black;
    }

    .border-bottom {
        border-bottom: 1px solid black;
    }
</style>