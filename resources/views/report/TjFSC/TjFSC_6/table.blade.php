@if($renderHtml)
    @if(count($reportData) > 0)
        <?php
        $safRegimes = $data['regime'];
        $headName = $data['head_name'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $downloadType = $data['download_type'];

        $safRegimesTrans = [];
        foreach ($safRegimes as $safRegime) {
            $safRegimesTrans[] = trans('swis.' . $safRegime . '_title');
        }

        $secondPeriodStartDate = (!is_null($data['end_date_start'])) ? date('d.m.Y', strtotime($data['end_date_start'])) : ' ';
        $secondPeriodEndDate = (!is_null($data['end_date_end'])) ? date('d.m.Y', strtotime($data['end_date_end'])) : '';

        ?>

        <table class="table table-striped table-bordered table-hover" id="data-table" border="1"
               @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="7">
                    <h3>{{trans('swis.report.'.$reportCode.'.table.name',
                    ['first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end'])),
                     'second_period' => $secondPeriodStartDate.' - '.$secondPeriodEndDate]
                    )}}</h3>
                </th>
            </tr>
            <tr>
                <th class="border-left border-top" width="4%">№</th>
                <th class="border-left border-top"
                    width="30%">{{ trans("swis.report.{$reportCode}.product_code") }}</th>
                <th class="border-left border-top"
                    width="10%">{{ trans("swis.report.{$reportCode}.unit_measurement") }}</th>
                <th class="border-left border-top"
                    width="10%">{{ trans("swis.report.{$reportCode}.first_period.netto" ,['period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end']))]) }}</th>
                <th class="border-left border-top"
                    width="10%">{{ trans("swis.report.{$reportCode}.second_period.netto" ,['period' => $secondPeriodStartDate.' - '.$secondPeriodEndDate]) }}</th>
                <th class="border-left border-top"
                    width="10%">{{ trans("swis.report.{$reportCode}.difference") }}</th>
                <th class="border-left border-top border-right"
                    width="26%">{{ trans("swis.report.{$reportCode}.producer_countries") }}</th>
            </tr>
            <tr>
                <th colspan="7"
                    class="border-left border-top border-bottom border-right">{{implode(', ',$safRegimesTrans)}}</th>
            </tr>
            </thead>
            <tbody>
            <?php  $i = 1;$sumD = $sumE = $sumF = 0  ?>
            @foreach ($reportData as $data)
                <?php
                $sumD += $data['D'];
                $sumE += $data['E'];
                $sumF += $data['F']
                ?>
                <tr>
                    <td class="border-left border-top border-bottom" width="4%">{{$i++}}</td>
                    <td class="border-left border-top border-bottom tl" width="30%">{{$data['B']}}</td>
                    <td class="border-left border-top border-bottom tl" width="10%">{{$data['C']}}</td>
                    <td class="border-left border-top border-bottom tr" width="10%">{!! $data['D'] !!}</td>
                    <td class="border-left border-top border-bottom tr" width="10%">{{$data['E']}}</td>
                    <td class="border-left border-top border-bottom tr" width="10%">{{$data['F']}}</td>
                    <td class="border-left border-top border-bottom border-right tl" width="26%">{{$data['G']}}</td>
                </tr>
            @endforeach
            <tr>
                <td class="border-left border-top border-bottom"></td>
                <td class="border-left border-top border-bottom tl">
                    <b>{{ trans("swis.report.{$reportCode}.total_amount") }}</b></td>
                <td class="border-left border-top border-bottom tr">
                    <b>{{ trans("swis.report.{$reportCode}.unit_measurement") }}</b></td>
                <td class="border-left border-top border-bottom tr"><b>{{$sumD}}</b></td>
                <td class="border-left border-top border-bottom tr"><b>{{$sumE}}</b></td>
                <td class="border-left border-top border-bottom tr"><b>{{$sumF}}</b></td>
                <td class="border-left border-top border-bottom border-right"></td>
            </tr>
            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')

<style>
    .tl {
        text-align: left;
    }

    .tr {
        text-align: right;
    }

    .border-left {
        border-left: 1px solid black;
    }

    .border-right {
        border-right: 1px solid black;
    }

    .border-top {
        border-top: 1px solid black;
    }

    .border-bottom {
        border-bottom: 1px solid black;
    }
</style>