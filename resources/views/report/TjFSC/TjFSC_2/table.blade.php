@if($renderHtml)
    @if(count($reportData) > 0)
        <?php
        $userPosition = $data['user_position'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $downloadType = $data['download_type']
        ?>
        <table class="table table-striped table-bordered table-hover" border="1" id="data-table"
               @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="7"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                    ['first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end']))])}}</h3>
                </th>
            </tr>
            <tr>
                <th class="border-left border-top" width="4%">№</th>
                <th class="border-left border-top" width="12%"
                    style="">{{ trans("swis.report.{$reportCode}.permission_date") }}</th>
                <th class="border-left border-top"
                    width="10%">{{ trans("swis.report.{$reportCode}.expiration_date") }}</th>
                <th class="border-left border-top"
                    width="11%">{{ trans("swis.report.{$reportCode}.permission_number") }}</th>
                <th class="border-left border-top"
                    width="16%">{{ trans("swis.report.{$reportCode}.export_country") }}</th>
                <th class="border-left border-top"
                    width="22%">{{ trans("swis.report.{$reportCode}.saf_side_name") }}</th>
                <th class="border-left border-top border-right"
                    width="25%">{{ trans("swis.report.{$reportCode}.product_commercial_description") }}</th>
            </tr>
            </thead>
            <tbody>
            <?php  $i = 1 ?>
            @foreach ($reportData as $data)
                <tr>
                    <td class="border-left border-top border-bottom" width="4%">{{$i++}}</td>
                    <td class="border-left border-top border-bottom" width="12%">{{formattedDate($data['permission_date'])}}</td>
                    <td class="border-left border-top border-bottom" width="10%">{{formattedDate($data['expiration_date'])}}</td>
                    <td class="border-left border-top border-bottom" width="11%">{{$data['permission_number']}}</td>
                    <td class="border-left border-top border-bottom tl" width="16%">&nbsp;{{$data['export_country']}}</td>
                    <td class="border-left border-top border-bottom tl" width="22%">&nbsp;{{$data['import_name']}}</td>
                    <td class="border-left border-top border-right border-bottom tl" width="25%">&nbsp;{{$data['product_names']}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if($downloadType != \App\Models\Reports\Reports::DOWNLOAD_TYPE_HTML)
            <table style="width: 100%;padding-top: 5px">
                <tbody>
                <tr>
                    <td></td>
                    <td>{{$userPosition }}</td>
                    <td>{{$borderHeadName ?? \Auth::user()->name()}}</td>
                </tr>
                </tbody>
            </table>
        @endif

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')

<style>
    .tl {
        text-align: left;
    }

    .border-left {
        border-left: 1px solid black;
    }

    .border-right {
        border-right: 1px solid black;
    }

    .border-top {
        border-top: 1px solid black;
    }

    .border-bottom {
        border-bottom: 1px solid black;
    }
</style>