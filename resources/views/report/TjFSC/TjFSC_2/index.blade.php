@extends('layouts.app')

@section('contentTitle') {{trans('swis.'.$reportCode.'.menu.title')}} @endsection

@section('content')

    <div class="search-data">
        <form class="form-horizontal fill-up" id="form-data" action="{{urlWithLng('report/'.$reportCode.'/generate')}}">
            <input type="hidden" name="report_code" value="{{$reportCode}}">

            <div class="row">
                <div class="col-md-6">

                    @include('report.partials.first-period',['label'=>trans('swis.reports.permission_date.label')])

                    <div class="form-group">

                        <label class="col-lg-4 control-label">{{trans('swis.reports.expiration_date.label')}}</label>
                        <div class="col-lg-4">

                            <div class='input-group date'>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                <input value="" name="expiration_date_start" autocomplete="off" type="text" placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                            </div>

                            <div class="form-error" id="form-error-expiration_date_start"></div>
                        </div>

                        <div class="col-lg-4">

                            <div class='input-group date'>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                <input value="" name="expiration_date_end" autocomplete="off" type="text" placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                            </div>

                            <div class="form-error" id="form-error-expiration_date_end"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-4 control-label">{{trans('swis.report.'.$reportCode.'.user.position')}}</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="user_position" id="user_position" value="">
                            <div class="form-error" id="form-error-user_position"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-4 control-label">{{trans('swis.report.label.state_border_head')}}</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="state_border_head" id="state_border_head" value="">
                            <div class="form-error" id="form-error-state_border_head"></div>
                        </div>
                    </div>

                </div>

                <div class="col-md-6">

                    <div class="form-group">
                        <label class="col-lg-4 control-label">{{trans('swis.report.'.$reportCode.'.import.name')}}</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="import_name" id="import_name" value="">
                            <div class="form-error" id="form-error-import_name"></div>
                        </div>
                    </div>

                    @include('report.partials.countries-list',['label'=>trans('swis.report.'.$reportCode.'.export.country'),'name'=>'export_country'])

                    @include('report.partials.download-types')


                </div>

                <div class="col-md-12">
                    <div class="text-center">
                        <button class="btn btn-primary">{{trans('swis.report.generate_output.button')}}</button>
                    </div>
                </div>

            </div>
        </form>
    </div>

    <div class="data-table">
        <hr/>
        <div id="reportResult"></div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/report/main.js')}}"></script>
@endsection