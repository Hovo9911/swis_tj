@if($renderHtml)
    @if(count($reportData) > 0)
        <?php
        $headName = $data['head_name'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $downloadType = $data['download_type'];
        ?>
        <table class="table table-striped table-bordered table-hover" border="1" id="data-table"
               @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="9"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                ['first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end']))])
                 }}</h3>
                </th>
            </tr>
            <tr>
                <th rowspan="2">{{ trans("swis.report.{$reportCode}.number") }}</th>
                <th rowspan="2">{{ trans("swis.report.{$reportCode}.document_status.date") }}</th>
                <th rowspan="2">{{ trans("swis.report.{$reportCode}.product_sender_user") }}</th>
                <th rowspan="2">{{ trans("swis.report.{$reportCode}.product_receiver_user") }}</th>
                <th rowspan="2">{{ trans("swis.report.{$reportCode}.certificate_number") }}</th>
                <th rowspan="2">{{ trans("swis.report.{$reportCode}.product_name") }}</th>
                <th colspan="2">{{ trans("swis.report.{$reportCode}.product_weight") }}</th>
                <th rowspan="2">{{ trans("swis.report.{$reportCode}.certificate_provider") }}</th>
            </tr>
            <tr>
                <td>{{ trans("swis.report.{$reportCode}.product_brutto_weight") }}</td>
                <td>{{ trans("swis.report.{$reportCode}.product_netto_weight") }}</td>
            </tr>
            </thead>
            <tbody>
            <?php $i = 1;
            $hsCodeRef = getReferenceRowsKeyValue(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_HS_CODE_6);
            $documentId = $data['document_id'];
            ?>

            @foreach ($reportData as $data)

                <?php

                $certificateProviderFieldName = \App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm::select('field_id')->where('document_id', $documentId)->where('mdm_field_id', 382)->pluck('field_id')->first();

                $safData = json_decode($data->safData, true);

                $isApproved = \App\Models\ConstructorStates\ConstructorStates::checkEndStateStatus($data['state_id'], \App\Models\ConstructorStates\ConstructorStates::END_STATE_APPROVED);
                $isRejected = \App\Models\ConstructorStates\ConstructorStates::checkEndStateStatus($data['state_id'], \App\Models\ConstructorStates\ConstructorStates::END_STATE_NOT_APPROVED);


                $A = '-';
                $D = '-';
                if ($isApproved) {
                    $A = $data['permission_date'];
                    $D = $data['permission_number'];
                }

                if ($isRejected) {
                    $A = $data['rejection_date'];
                    $D = $data['rejected_number'];
                }

                $B = '-';
                if (isset($safData['saf']['sides']['export']['name'])) {
                    $B = $safData['saf']['sides']['export']['name'];
                }

                $C = '-';
                if (isset($safData['saf']['sides']['import']['name'])) {
                    $C = $safData['saf']['sides']['import']['name'];
                }

                $productCode = isset($hsCodeRef[$data['productCode']]) ? $hsCodeRef[$data['productCode']]['code'] : '';
                $E = '('.$productCode.')' .' '. $data['productDescription'];


                $G = '-';
                if(isset($data->custom_data[$certificateProviderFieldName])){
                    $G =  $data->custom_data[$certificateProviderFieldName];
                }

                ?>
                <tr>
                    <td>{{$i++}}</td>
                    <td>{{formattedDate($A)}}</td>
                    <td>{{$B}}</td>
                    <td>{{$C}}</td>
                    <td>{{$D}}</td>
                    <td>{{$E}}</td>
                    <td>{{$data['productBruttoWeight']}}</td>
                    <td>{{$data['productNettoWeight']}}</td>
                    <td>{{$G}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')