@if(count($reportData) < 1)
    <h2 class="text-center gray-bg p-m">{{trans('swis.report.not_data.available')}}</h2>
@endif