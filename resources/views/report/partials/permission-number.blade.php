<div class="form-group">
    <label class="col-lg-4 control-label">{{trans('swis.report.'.$reportCode.'.permission_number')}}</label>
    <div class="col-lg-8">
        <input type="text" class="form-control" name="permission_number" id="permission_number" value="">
        <div class="form-error" id="form-error-permission_number"></div>
    </div>
</div>