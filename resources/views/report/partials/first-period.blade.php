<div class="form-group @if(isset($required) && !$required) @else required @endif">

    <label class="col-lg-4 control-label">{{$label or trans('swis.reports.start_day.label')}}</label>

    <div class="{{$dateClass or 'col-lg-4'}}">
        <div class='input-group date'>
            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            <input value="" name="start_date_start" autocomplete="off" type="text" placeholder="{{setDatePlaceholder()}}" class="form-control"/>
        </div>
        <div class="form-error" id="form-error-start_date_start"></div>
    </div>

    <div class="{{$dateClass or 'col-lg-4'}}">
        <div class='input-group date'>
            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            <input value="" name="start_date_end" autocomplete="off" type="text" placeholder="{{setDatePlaceholder()}}" class="form-control"/>
        </div>
        <div class="form-error" id="form-error-start_date_end"></div>
    </div>

</div>