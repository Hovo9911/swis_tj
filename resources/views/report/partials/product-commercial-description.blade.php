<div class="form-group">
    <label class="col-lg-4 control-label">{{trans('swis.report.'.$reportCode.'.commercial_description')}}</label>
    <div class="col-lg-8">
        <input type="text" class="form-control" name="commercial_description" value="">
        <div class="form-error" id="form-error-commercial_description"></div>
    </div>
</div>