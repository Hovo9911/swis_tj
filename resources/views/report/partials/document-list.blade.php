
@if(!isset($multiple))

    <div class="form-group {{!isset($required) ? 'required': ''}}">
        <label class="col-lg-4 control-label">{{trans('swis.document.select_document')}}</label>
        <div class="{{$class or 'col-lg-8'}}">
            <select class="form-control select2 document-list"  name="document_id" data-statuses="1">
                <option value=""></option>
                @if ($documents->count())
                    @foreach ($documents as $document)
                        <option value="{{ $document->id }}">{{ $document->document_name }}</option>
                    @endforeach
                @endif
            </select>
            <div class="form-error" id="form-error-document_id"></div>
        </div>
    </div>

@else

    <div class="form-group">
        <label class="col-lg-4 control-label">{{trans('swis.document.select_document')}}</label>
        <div class="col-lg-8">
            <select class="form-control select2 document-list" name="document_id[]"  multiple data-statuses="1">
                @if ($documents->count())
                    @foreach ($documents as $document)
                        <option value="{{ $document->id }}">{{ $document->document_name }}</option>
                    @endforeach
                @endif
            </select>
            <div class="form-error" id="form-error-document_id"></div>
        </div>
    </div>

@endif




