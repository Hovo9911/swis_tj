<div class="form-group {{isset($required) ? 'required' : ''}}">
    <label class="col-lg-4 control-label">{{trans('swis.report.hs_code_input.label')}}</label>
    <div class="{{$class or 'col-lg-8'}}">
        <input type="text" class="form-control" name="hs_code" id="hsCode" value="">
        <div class="form-error" id="form-error-hs_code"></div>
    </div>
</div>