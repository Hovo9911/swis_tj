<div class="form-group">
    <label class="col-lg-4 control-label">{{trans('swis.report.'.$reportCode.'.importer')}}</label>
    <div class="col-lg-8">
        <input type="text" class="form-control" name="importer" value="">
        <div class="form-error" id="form-error-importer"></div>
    </div>
</div>