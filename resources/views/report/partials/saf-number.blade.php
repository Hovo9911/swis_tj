<div class="form-group {{!isset($required) ? 'required': ''}}">
    <label class="col-lg-4 control-label">{{trans('swis.report.'.$reportCode.'.saf_number')}}</label>
    <div class="col-lg-8">
        <input type="text" class="form-control" name="saf_number" value="">
        <div class="form-error" id="form-error-saf_number"></div>
    </div>
</div>