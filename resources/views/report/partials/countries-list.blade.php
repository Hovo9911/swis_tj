@php($countries = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_COUNTRIES))
<div class="form-group">
    <label class="col-lg-4 control-label">{{$label or trans('swis.report.'.$reportCode.'.country')}}</label>
    <div class="col-lg-8">
        <select class="form-control select2" name="{{$name or 'country'}}" data-statuses="1">
            <option value=""></option>
            @if ($countries->count())
                @foreach ($countries as $country)
                    <option value="{{ $country->id }}">{{ "(".$country->code.") ".$country->name }}</option>
                @endforeach
            @endif
        </select>
        <div class="form-error" id="form-error-{{$name or 'country'}}"></div>
    </div>
</div>