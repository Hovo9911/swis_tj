<div class="form-group required">
    <label class="col-lg-4 control-label">{{trans('swis.report.hs_codes.list')}}</label>
    <div class="col-lg-8">
        <select class="form-control select2" name="hs_code_count">
            <option value=""></option>
            @for($i=2;$i < 11;$i++)
                <option value="{{$i}}">{{$i}}</option>
            @endfor
        </select>
        <div class="form-error" id="form-error-hs_code_count"></div>
    </div>
</div>