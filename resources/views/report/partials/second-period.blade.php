<div class="form-group {{!isset($required) ? 'required': ''}}">

    <label class="col-lg-4 control-label">{{$label or trans('swis.reports.end_day.label')}}</label>

    <div class="col-lg-4">
        <div class='input-group date'>
            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            <input value="" name="end_date_start" autocomplete="off" type="text" placeholder="{{setDatePlaceholder()}}" class="form-control"/>
        </div>
        <div class="form-error" id="form-error-end_date_start"></div>
    </div>

    <div class="col-lg-4">
        <div class='input-group date'>
            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            <input value="" name="end_date_end" autocomplete="off" type="text" placeholder="{{setDatePlaceholder()}}" class="form-control"/>
        </div>
        <div class="form-error" id="form-error-end_date_end"></div>
    </div>

</div>