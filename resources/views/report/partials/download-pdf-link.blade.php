@php($userId = \Auth::id())
@if(!empty($reportFileName))
    <a href="{{url("files/reports/{$reportCode}/{$userId}/{$reportFileName}")}}" id="downloadFile" download></a>
@endif