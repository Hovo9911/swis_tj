@if(!isset($multiple))

    <div class="form-group">
        <label class="col-lg-4 control-label">{{trans('swis.reports.'.$reportCode.'.select_sub_division.label')}}</label>
        <div class="col-lg-8">
            <select class="select2 document-subdivisions" name="sub_division_id">
                <option value=""></option>
                @if(isset($subDivisions) && $subDivisions->count())
                    @foreach($subDivisions as $subDivision)
                        <option value="{{$subDivision->id}}"> {{ $subDivision->name }}</option>
                    @endforeach
                @endif
            </select>
            <div class="form-error" id="form-error-sub_division_id"></div>
        </div>
    </div>

@elseif(isset($multiple) && $multiple)

    @if(isset($noData) && $noData)

        <div class="form-group"><label class="col-lg-4 control-label">{{trans('swis.reports.'.$reportCode.'.select_sub_division.label')}}</label>
            <div class="col-lg-8">
                <select class="select2 document-subdivisions" disabled multiple name="sub_division_ids[]"  data-allow-clear="false"></select>
                <div class="form-error" id="form-error-sub_division_ids"></div>
            </div>
        </div>

    @else

        <div class="form-group">
            <label class="col-lg-4 control-label">{{trans('swis.reports.'.$reportCode.'.select_sub_division.label')}}</label>
            <div class="{{$class or 'col-lg-8'}}">
                <select class="select2 document-subdivisions" multiple name="sub_division_ids[]" data-allow-clear="false">
                    <option value=""></option>
                    @if(isset($subDivisions) && $subDivisions->count())
                        @foreach($subDivisions as $subDivision)
                            <option value="{{$subDivision->id}}"> {{ $subDivision->name }}</option>
                        @endforeach
                    @endif
                </select>
                <div class="form-error" id="form-error-sub_division_id"></div>
            </div>
        </div>

    @endif

@endif

