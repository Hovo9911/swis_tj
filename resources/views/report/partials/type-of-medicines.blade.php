@php($medicineTypes = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_TYPE_OF_MEDICINE))
<div class="form-group">
    <label class="col-lg-4 control-label">{{$label or trans('swis.report.'.$reportCode.'.medicine_type')}}</label>
    <div class="col-lg-8">
        <select class="form-control select2" name="medicine_type">
            <option value=""></option>
            @if ($medicineTypes->count())
                @foreach ($medicineTypes as $item)
                    <option value="{{ $item->id }}">{{ "(".$item->code.") ".$item->name }}</option>
                @endforeach
            @endif
        </select>
        <div class="form-error" id="form-error-medicine_type"></div>
    </div>
</div>