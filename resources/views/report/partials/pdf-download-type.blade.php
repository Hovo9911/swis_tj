@if($downloadType != \App\Models\Reports\Reports::DOWNLOAD_TYPE_HTML)
<table style="width: 100%;padding-top: 5px">
    <tbody>
    <tr>
        <td></td>
        <td>{{$headName ?? ''}}</td>
        <td>{{$borderHeadName ?? ''}}</td>
        <td></td>
    </tr>
    </tbody>
</table>
@endif