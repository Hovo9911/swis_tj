<div class="form-group">
    <label class="col-lg-4 control-label">{{trans('swis.report.'.$reportCode.'.applicant')}}</label>
    <div class="col-lg-8">
        <input type="text" class="form-control" name="applicant" value="">
        <div class="form-error" id="form-error-applicant"></div>
    </div>
</div>