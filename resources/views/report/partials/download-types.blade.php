<div class="form-group required">
    <label class="col-lg-4 control-label">{{trans('swis.report.download_type')}}</label>
    <div class="col-lg-8 radio-list-report">

        <label>
            <input type="radio" name="download_type" value="pdf"/> {{ trans('swis.report.download_pdf.title') }}
        </label>

        <label>
            <input type="radio" name="download_type" value="xls"/> {{ trans('swis.report.download_xls.title') }}
        </label>

        <label>
            <input type="radio" name="download_type" value="html"/> {{ trans('swis.report.download_html.title') }}
        </label>

        <div class="form-error" id="form-error-download_type"></div>
    </div>
</div>