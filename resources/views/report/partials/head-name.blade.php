<div class="form-group">
    <label class="col-lg-4 control-label">{{trans('swis.report.label.head_name')}}</label>
    <div class="{{$class or 'col-lg-8'}}">
        <input type="text" class="form-control" name="head_name" id="head_name" value="">
        <div class="form-error" id="form-error-head_name"></div>
    </div>
</div>

<div class="form-group">
    <label class="col-lg-4 control-label">{{trans('swis.report.label.state_border_head')}}</label>
    <div class="{{$class or 'col-lg-8'}}">
        <input type="text" class="form-control" name="state_border_head" id="state_border_head" value="">
        <div class="form-error" id="form-error-state_border_head"></div>
    </div>
</div>