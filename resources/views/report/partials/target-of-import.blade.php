@php($importTargets = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_TARGET_OF_IMPORT))
<div class="form-group">
    <label class="col-lg-4 control-label">{{$label or trans('swis.report.'.$reportCode.'.import_target')}}</label>
    <div class="col-lg-8">
        <select class="form-control select2" name="import_target">
            <option value=""></option>
            @if ($importTargets->count())
                @foreach ($importTargets as $item)
                    <option value="{{ $item->id }}">{{ "(".$item->code.") ".$item->name }}</option>
                @endforeach
            @endif
        </select>
        <div class="form-error" id="form-error-import_target"></div>
    </div>
</div>