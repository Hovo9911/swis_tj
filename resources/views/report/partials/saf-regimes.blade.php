
@if(!isset($multiple))

    <div class="form-group {{!isset($required) ? 'required': ''}}">
        <label class="col-lg-4 control-label">{{trans('swis.constructor_routing.regime')}}</label>
        <div class="col-lg-8 radio-list-report">
            @foreach(\App\Models\SingleApplication\SingleApplication::REGIMES as $regime)
                <label>
                    <input type="radio" name="regime" value="{{ $regime }}"/> {{ trans('swis.'.$regime.'_title') }}
                </label>
            @endforeach
            <div class="form-error" id="form-error-regime"></div>
        </div>
    </div>

@elseif(isset($multiple) && $multiple)

    <div class="form-group {{!isset($required) ? 'required': ''}}">
        <label class="col-lg-4 control-label">{{trans('swis.constructor_routing.regime')}}</label>
        <div class="col-lg-8 radio-list-report">
            <select class="form-control regime-type select2" data-allow-clear="false" name="regime[]" multiple>
                @foreach (\App\Models\SingleApplication\SingleApplication::REGIMES as $regime)
                    <option name="regime" value="{{ $regime }}"> {{ trans('swis.'.$regime.'_title') }}</option>
                @endforeach
            </select>
            <div class="form-error" id="form-error-regime"></div>
        </div>
    </div>

@endif

