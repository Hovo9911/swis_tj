
@if(!isset($multiple))

    <div class="form-group {{!isset($required) ? 'required': ''}}">
        <label class="col-lg-4 control-label">{{trans('swis.report.constructor_document.status')}}</label>
        <div class="{{$class or 'col-lg-8'}}">
            <select class="form-control document-status select2" disabled name="document_status">
                <option value=""></option>
            </select>
            <div class="form-error" id="form-error-document_status"></div>
        </div>
    </div>

@else

    <div class="form-group {{!isset($required) ? 'required': ''}}">
        <label class="col-lg-4 control-label">{{trans('swis.report.constructor_document.status')}}</label>
        <div class="{{$class or 'col-lg-8'}}">
            <select class="form-control document-status select2" disabled data-allow-clear="false" name="document_status[]" multiple></select>
            <div class="form-error" id="form-error-document_status"></div>
        </div>
    </div>

@endif