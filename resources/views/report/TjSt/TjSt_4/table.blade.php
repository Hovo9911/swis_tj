@if($renderHtml)
    @if(count($reportData) > 0)
        <?php
        $sum_B = 0;$sum_D = 0; $c = 0;$b = 0;
        $allSum = [];
        foreach ($reportData as $key => $report) {
            foreach ($report as $measurement => $value) {
                $allSum[$measurement]['sum_B'] = isset($allSum[$measurement]['sum_B']) ? $allSum[$measurement]['sum_B'] : 0;
                $allSum[$measurement]['sum_D'] = isset($allSum[$measurement]['sum_D']) ? $allSum[$measurement]['sum_D'] : 0;

                $allSum[$measurement]['sum_B'] = $allSum[$measurement]['sum_B'] + $value['approvedQty_B'];
                $allSum[$measurement]['sum_D'] = $allSum[$measurement]['sum_D'] + $value['approvedQty_D'];
            }
        }

        $headName = $data['head_name'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $downloadType = $data['download_type'];
        ?>

        <table class="table table-striped table-bordered table-hover" border="1" id="data-table" @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="9"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                ['regime'=>trans('swis.'.$data['regime'].'_title'),
                'first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end'])),
                'second_period' => date('d.m.Y', strtotime($data['end_date_start'])).' - '.date('d.m.Y', strtotime($data['end_date_end']))
                ])}}</h3> </th>
            </tr>
            <tr>
                <th rowspan="{{count($allSum)+1}}"></th>
                <th rowspan="{{count($allSum)+1}}">{{trans('swis.report.calculation.product_name')}}</th>
                <th>{{trans('swis.report.units.title')}}</th>
                <th>{{trans('swis.report.period_a.title')}} <br/>{{$data['start_date_start']}}/ {{$data['start_date_end']}}</th>
                <th>%</th>
                <th>{{trans('swis.report.period_b.title')}} <br/>{{$data['end_date_start']}} / {{$data['end_date_end']}}</th>
                <th>%</th>
                <th>{{trans('swis.report.'.$reportCode.'.fark.title')}} +;-</th>
                <th>%</th>
            </tr>
            @foreach($allSum as $key=>$sum)
            <?php
               $measurementUnit = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT,$key);
            ?>
            <tr>
                <td style="padding: 10px">{{$measurementUnit->name ?? ''}}</td>
                <td>{{$sum['sum_B']}}</td>
                <td>100</td>
                <td>{{$sum['sum_D']}}</td>
                <td>100</td>
                <td>{{$sum['sum_D'] - $sum['sum_B']}}</td>
                <td>0</td>
            </tr>
            @endforeach
            </thead>
            <tbody>

            <?php  $i = 1 ?>
            @foreach($reportData as $key=>$report)

                <?php
                $hsCodeRef = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_HS_CODE_6,$key,false,['code','long_name'] );
                ?>
                @foreach($report as $measurement=>$value)

                    <?php
                    $B = $value['approvedQty_B'];
                    $D = $value['approvedQty_D'];

                    $sum_B = $allSum[$measurement]['sum_B'];
                    $sum_D = $allSum[$measurement]['sum_D'];

                    $sum_B == 0 ? $sum_B = 1 : $sum_B;
                    $sum_D == 0 ? $sum_D = 1 : $sum_D;

                    $C = round(($B*100)/$sum_B , 2);
                    $E = round(($D*100)/$sum_D , 2);

                    $F = $D - $B;
                    $G = $E - $C;

                    ?>
                    <tr>
                        <td>{{$i}}</td>
                        <td>{{'('.optional($hsCodeRef)->code .') '.optional($hsCodeRef)->long_name ?? '-'}}</td>
                        <td>{{$value['measurement_name']}}</td>
                        <td>{{$B}}</td>
                        <td>{{$C}}</td>
                        <td>{{$D}}</td>
                        <td>{{$E}}</td>
                        <td>{{$F}}</td>
                        <td>{{$G}}</td>
                    </tr>
                    <?php  $i++ ?>
                @endforeach
            @endforeach
            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')