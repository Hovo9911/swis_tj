@if($renderHtml)
    <?php
    $downloadType = $data['download_type'];
    $firstPeriodReports = $reportData['firstPeriod'];
    $secondPeriodReports = $reportData['secondPeriod'];
    $allSubDivisions = $reportData['allSubDivisions'];

    $headName = $data['head_name'] ?? '';
    $borderHeadName = $data['state_border_head'] ?? '';

    $allActiveSubDivisions = array_diff($reportData['allActiveSubDivisions'],$allSubDivisions);

    ?>
    <table class="table table-striped table-bordered table-hover" border="1" id="data-table"
           @if($renderHtml) style="text-align: center" @endif>
        <thead>
        <tr>
            <th colspan="14"><h3>{{trans('swis.report.'.$reportCode.'.table.name',[
                'first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end'])),
                'second_period' => date('d.m.Y', strtotime($data['end_date_start'])).' - '.date('d.m.Y', strtotime($data['end_date_end']))
                ])}}</h3></th>
        </tr>
        <tr>
            <th rowspan="3">{{trans('swis.report.'.$reportCode.'.serial_number')}}</th>
            <th rowspan="3">{{trans('swis.report.'.$reportCode.'.sub_division_name')}}</th>
        </tr>
        <tr>
            <th colspan="2">{{trans('swis.report.'.$reportCode.'.import_type')}}</th>
            <th>{{trans('swis.report.'.$reportCode.'.difference')}}</th>
            <th colspan="2">{{trans('swis.report.'.$reportCode.'.export_type')}}</th>
            <th>{{trans('swis.report.'.$reportCode.'.difference')}}</th>
            <th colspan="2">{{trans('swis.report.'.$reportCode.'.together_type')}}</th>
            <th>{{trans('swis.report.'.$reportCode.'.difference')}}</th>
            <th colspan="2">{{trans('swis.report.'.$reportCode.'.sum_type')}}</th>
            <th>{{trans('swis.report.'.$reportCode.'.difference')}}</th>
        </tr>
        <tr>
            @for($i = 0;$i<4;$i++)
                <th>{{trans('swis.report.period_a.title')}} <br/>{{date('d.m.Y', strtotime($data['start_date_start']))}}
                    - {{date('d.m.Y', strtotime($data['start_date_end']))}}</th>
                <th>{{trans('swis.report.period_b.title')}} <br/>{{date('d.m.Y', strtotime($data['end_date_start']))}}
                    - {{date('d.m.Y', strtotime($data['end_date_end']))}}</th>
                <th>{{date('Y',strtotime($data['start_date_start']))}}
                    - {{date('Y',strtotime($data['end_date_end']))}}</th>
            @endfor
        </tr>
        </thead>
        <tbody>
        <?php
        $i = 1;
        $SUM_C = 0;
        $SUM_D = 0;
        $SUM_E = 0;

        $SUM_F = 0;
        $SUM_G = 0;
        $SUM_H = 0;

        $SUM_I = 0;
        $SUM_J = 0;
        $SUM_K = 0;

        $SUM_L = 0;
        $SUM_M = 0;
        $SUM_N = 0;
        ?>
        @foreach($allSubDivisions as $sub_division_id)

            <?php $refSubDivision = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME, $sub_division_id);

            $C = isset($firstPeriodReports[$sub_division_id]['import']) ? $firstPeriodReports[$sub_division_id]['import']['certificates_count'] : 0;
            $D = isset($secondPeriodReports[$sub_division_id]['import']) ? $secondPeriodReports[$sub_division_id]['import']['certificates_count'] : 0;
            $E = $D - $C;

            $F = isset($firstPeriodReports[$sub_division_id]['export']) ? $firstPeriodReports[$sub_division_id]['export']['certificates_count'] : 0;
            $G = isset($secondPeriodReports[$sub_division_id]['export']) ? $secondPeriodReports[$sub_division_id]['export']['certificates_count'] : 0;
            $H = $G - $F;

            $I = $C + $F;
            $J = $G + $D;

            $K = $J - $I;

            $firstPeriodSumImport = isset($firstPeriodReports[$sub_division_id]['import']) ? $firstPeriodReports[$sub_division_id]['import']['certificates_sum'] : 0;
            $firstPeriodSumExport = isset($firstPeriodReports[$sub_division_id]['export']) ? $firstPeriodReports[$sub_division_id]['export']['certificates_sum'] : 0;

            $secondPeriodSumImport = isset($secondPeriodReports[$sub_division_id]['import']) ? $secondPeriodReports[$sub_division_id]['import']['certificates_sum'] : 0;
            $secondPeriodSumExport = isset($secondPeriodReports[$sub_division_id]['export']) ? $secondPeriodReports[$sub_division_id]['export']['certificates_sum'] : 0;

            $L = $firstPeriodSumImport + $firstPeriodSumExport;
            $M = $secondPeriodSumImport + $secondPeriodSumExport;
            $N = $M - $L;

            // Totals
            $SUM_C += $C;
            $SUM_D += $D;
            $SUM_E += $E;

            $SUM_F += $F;
            $SUM_G += $G;
            $SUM_H += $H;

            $SUM_I += $I;
            $SUM_J += $J;
            $SUM_K += $K;

            $SUM_L += $L;
            $SUM_M += $M;
            $SUM_N += $N;
            ?>
            <tr>
                <td>{{$i}}</td>
                <td>{{$refSubDivision->name ?? ''}}</td>
                <td>{{$C}}</td>
                <td>{{$D}}</td>
                <td>{{$E}}</td>

                <td>{{$F}}</td>
                <td>{{$G}}</td>
                <td>{{$H}}</td>

                <td>{{$I}}</td>
                <td>{{$J}}</td>
                <td>{{$K}}</td>

                <td>{{$L}}</td>
                <td>{{$M}}</td>
                <td>{{$N}}</td>
            </tr>
            <?php $i++ ?>
        @endforeach
        @foreach($allActiveSubDivisions as $activeSubDivison)
            <?php $refActiveSubDivision = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME, $activeSubDivison);?>
            <tr>
                <td>{{$i}}</td>
                <td>{{$refActiveSubDivision->name ?? ''}}</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php $i++ ?>
        @endforeach

        </tbody>
        <tfoot>
        <tr>
            <td colspan="2">{{trans('swis.report.'.$reportCode.'.totals')}}</td>
            <td>{{$SUM_C}}</td>
            <td>{{$SUM_D}}</td>
            <td>{{$SUM_E}}</td>
            <td>{{$SUM_F}}</td>
            <td>{{$SUM_G}}</td>
            <td>{{$SUM_H}}</td>
            <td>{{$SUM_I}}</td>
            <td>{{$SUM_J}}</td>
            <td>{{$SUM_K}}</td>
            <td>{{$SUM_L}}</td>
            <td>{{$SUM_M}}</td>
            <td>{{$SUM_N}}</td>
        </tr>
        </tfoot>
    </table>

   @include('report.partials.pdf-download-type')

@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')