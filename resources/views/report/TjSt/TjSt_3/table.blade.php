@if($renderHtml)
    @if(count($reportData) > 0)
        <?php
        $headName = $data['head_name'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $downloadType = $data['download_type'];
        ?>
        <table class="table table-striped table-bordered table-hover" id="data-table" border="1" @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="5"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                ['first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end'])),
                'second_period' => date('d.m.Y', strtotime($data['end_date_start'])).' - '.date('d.m.Y', strtotime($data['end_date_end'])),
                'document_status' => trans('swis.report'.$reportCode.'.'.$data['document_status'].'.custom_name')
                ])}}</h3></th>
            </tr>
            <tr>
                <th>{{trans('swis.report.'.$reportCode.'.certificate_compliance')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.units')}}</th>
                <th>{{trans('swis.report.period_a.title')}} <br/>{{$data['start_date_start']}}
                    / {{$data['start_date_end']}}
                </th>
                <th>{{trans('swis.report.period_b.title')}} <br/>{{$data['end_date_start']}} / {{$data['end_date_end']}}
                </th>
                <th>{{trans('swis.report.'.$reportCode.'.difference')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($reportData as $key=>$report)
                <?php
                $C = $report['C'];
                $D = $report['D'];
                $E = $D - $C;
                ?>
                <tr>
                    <td>{{trans('swis.report.'.$reportCode.'.'.$key)}}</td>
                    <td>{{trans('swis.report.'.$reportCode.'.pieces')}}</td>
                    <td>{{$C}}</td>
                    <td>{{$D}}</td>
                    <td>{{$E}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')
