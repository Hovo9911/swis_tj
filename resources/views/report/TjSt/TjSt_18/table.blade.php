@if($renderHtml)
    @if(count($reportData) > 0)
        <?php
        $documentStatusTrans = '';
        foreach ($data['document_status'] as $documentStatus) {
            $documentStatusTrans .= $documentStatus;
        }

        $headName = $data['head_name'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $downloadType = $data['download_type'];

        ?>
        <table class="table table-striped table-bordered table-hover" border="1" id="data-table"
               @if($renderHtml) style="text-align: center" @endif>
            <thead>

            <tr>
                <th colspan="6"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                ['regime'=>trans('swis.'.$data['regime'].'_title'),
                'first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end'])),
                'document_status' => trans('swis.report'.$reportCode.'.'.$documentStatusTrans.'.custom_name'),
                'HS Code' => $data['hs_code']
                ])}}</h3></th>
            </tr>
            <tr>
                <th>{{ trans("swis.report.{$reportCode}.number") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.importer_exporter") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.certificates_count") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.product_name") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.weight") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.country") }}</th>
            </tr>
            </thead>
            <tbody>

            <?php  $i = 1;$totalCertificatesCount = $totalProductsBatchWeight = 0; ?>

            @foreach ($reportData as $subappNumber => $data)
                @if($subappNumber != 'type_value')
                    @foreach($data as $key => $value)
                        <?php
                        $certificatesCount = count(array_keys($reportData['type_value'], $data['saf_side_type_value']));
                        ?>
                        @if($key == 'products')
                            <?php $totalCertificatesCount += $certificatesCount; ?>
                            <tr>
                                <td @if(count($value) != 1) rowspan="{{count($value)+1}}" @endif>{{$i++}}</td>
                                <td @if(count($value) != 1) rowspan="{{count($value)+1}}" @endif>{{$data['columnB']}}</td>
                                <td @if(count($value) != 1) rowspan="{{count($value)+1}}" @endif>{{$certificatesCount}}</td>

                                @if(count($value) == 1)
                                    @foreach ($value as $items)

                                        <td>{{$items['product_info']}}</td>
                                        <td>{{$items['quantity']}}</td>
                                        <?php
                                        $totalProductsBatchWeight += $items['quantity'];
                                        $producerCountries = implode(',', array_unique($items['producer_country']));
                                        ?>

                                        <td>{{$producerCountries}}</td>
                                    @endforeach
                                @endif
                            </tr>
                            @if(count($value) != 1)
                                @foreach ($value as $items)
                                    <tr>
                                        <td>{{$items['product_info']}}</td>
                                        <td>{{$items['quantity']}}</td>
                                        <?php
                                        $totalProductsBatchWeight += $items['quantity'];
                                        $producerCountries = implode(',', array_unique($items['producer_country']));
                                        ?>

                                        <td>{{$producerCountries}}</td>
                                    </tr>
                                @endforeach
                            @endif

                        @endif
                    @endforeach
                @endif
            @endforeach
            <tr>
                <th colspan="2">{{trans("swis.report.{$reportCode}.total")}}</th>
                <th>{{$totalCertificatesCount}}</th>
                <th></th>
                <th>{{$totalProductsBatchWeight}}</th>
                <th></th>
            </tr>
            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')