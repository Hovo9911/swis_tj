@if($renderHtml)
    @if(count($reportData) > 0)
        <?php
        $sum_D = $sum_E = $D = $E = 0;
        $allSum = [];

        $allSumD = $allSumE = '';

        foreach ($reportData as $key => $report) {
            foreach ($report as $measurement => $value) {

                $allSum[$measurement]['sum_D'] = isset($allSum[$measurement]['sum_D']) ? $allSum[$measurement]['sum_D'] : 0;
                $allSum[$measurement]['sum_E'] = isset($allSum[$measurement]['sum_E']) ? $allSum[$measurement]['sum_E'] : 0;

                $allSum[$measurement]['sum_D'] = $allSum[$measurement]['sum_D'] + $value['approvedQty_D'];
                $allSum[$measurement]['sum_E'] = $allSum[$measurement]['sum_E'] + $value['approvedQty_E'];
            }

        }

        $headName = $data['head_name'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $downloadType = $data['download_type'];
        ?>
        <table class="table table-striped table-bordered table-hover" border="1" id="data-table"
               @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="6"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                ['regime'=>trans('swis.'.$data['regime'].'_title'),
                'first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end'])),
                'second_period' => date('d.m.Y', strtotime($data['end_date_start'])).' - '.date('d.m.Y', strtotime($data['end_date_end'])),
                'hs_code' => $data['hs_code']
                ])}}</h3> </th>
            </tr>
            <tr>
                <th>{{trans('swis.report.'.$reportCode.'.number')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.product_name')}}</th>
                <th>{{trans('swis.report.units.title')}}</th>
                <th>{{trans('swis.report.period_a.title')}} <br/>{{date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end']))}}</th>
                <th>{{trans('swis.report.period_b.title')}} <br/>{{date('d.m.Y', strtotime($data['end_date_start'])).' - '.date('d.m.Y', strtotime($data['end_date_end']))}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.countries')}}</th>
            </tr>
            </thead>
            <tbody>

            <?php  $i = 1 ?>
            @foreach($reportData as $key=>$report)

                <?php
                $hsCodeRef = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_HS_CODE_6, $key, false, ['code', 'long_name']);
                ?>
                @foreach($report as $measurement=>$value)

                    <?php

                    $D = $value['approvedQty_D'];
                    $E = $value['approvedQty_E'];

                    ?>
                    <tr>
                        <td>{{$i}}</td>
                        <td>{{'('.optional($hsCodeRef)->code .') '.optional($hsCodeRef)->long_name ?? '-'}}</td>
                        <td>{{$value['measurement_name']}}</td>
                        <td>{{$D}}</td>
                        <td>{{$E}}</td>
                        <td>{{$value['countries_list']}}</td>
                    </tr>
                    <?php  $i++ ?>
                @endforeach

            @endforeach
            @foreach ($allSum as $key => $value)
                <tr>
                    <td colspan="2">
                        <b>{{trans('swis.report.total.title')}} {{ optional(getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $key))->name }}</b>
                    </td>
                    <td></td>
                    <td><b>{{ $value['sum_D'] }}</b></td>
                    <td><b>{{ $value['sum_E'] }}</b></td>
                    <td></td>
                </tr>
            @endforeach
            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')