@if($renderHtml)

    @if(count($reportData) > 0)

        <?php

        $sum_G = $c = $b = 0;
        $allSum = $productSum = [];

        foreach ($reportData as $key => $report) {
            foreach ($report as $measurement => $value) {
                $productSum[$key][$measurement]['sum_D'] = $productSum[$key][$measurement]['sum_E'] = $allSum[$measurement]['sum_D'] = $allSum[$measurement]['sum_E'] = 0;
                foreach ($value as $country => $approvedQuantityData) {

                    is_numeric($approvedQuantityData['approved_quantity']) ? $productSum[$key][$measurement]['sum_D'] = $productSum[$key][$measurement]['sum_D'] + $approvedQuantityData['approved_quantity'] : '';
                    is_numeric($approvedQuantityData['approved_quantity_2']) ? $productSum[$key][$measurement]['sum_E'] = $productSum[$key][$measurement]['sum_E'] + $approvedQuantityData['approved_quantity_2'] : '';

                }
            }
        }
        $headName = $data['head_name'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $downloadType = $data['download_type'];
        ?>

        <table class="table table-striped table-bordered table-hover" id="data-table" border="1"
               @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="8"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                ['regime'=>trans('swis.'.$data['regime'].'_title'),
                'first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end'])),
                'second_period' => date('d.m.Y', strtotime($data['end_date_start'])).' - '.date('d.m.Y', strtotime($data['end_date_end']))
                ])}}</h3></th>
            </tr>
            <tr>
                <th rowspan="2">{{trans('swis.report.'.$reportCode.'.number')}}</th>
                <th rowspan="2">{{trans('swis.report.'.$reportCode.'.product_name')}}</th>
                <th rowspan="2">{{trans('swis.report.units.title')}}</th>
                <th>{{date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end']))}}</th>
                <th>{{date('d.m.Y', strtotime($data['end_date_start'])).' - '.date('d.m.Y', strtotime($data['end_date_end']))}}</th>
                <th colspan="2">{{trans('swis.report.'.$reportCode.'.fark.title')}}</th>
                <th rowspan="2">{{trans('swis.report.'.$reportCode.'.country_name')}}</th>
            </tr>
            <tr>
                <th>{{trans('swis.report.'.$reportCode.'.xachm')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.xachm')}}</th>
                <th>+;-</th>
                <th>%</th>
            </tr>
            </thead>
            <tbody>
            <?php  $i = 1 ?>

            @foreach($reportData as $hsCode => $report)

                <?php
                $hsCodeRef = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_HS_CODE_6, $hsCode, false, ['code', 'long_name']);
                ?>
                <tr>
                    <td rowspan="{{(count($report,COUNT_RECURSIVE)-count($report))/4}}"
                        style="vertical-align: middle">{{$i}}</td>
                    <td rowspan="{{(count($report,COUNT_RECURSIVE)-count($report))/4}}"
                        style="vertical-align: middle">{{'('.optional($hsCodeRef)->code .') '.optional($hsCodeRef)->long_name ?? '-'}}</td>

                    @foreach($report as $meauserementUnit => $countryData)
                        <?php

                        $refMeasurement = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $meauserementUnit);
                        $typeName = optional($refMeasurement)->name;

                        foreach($countryData as $producerCountryName => $approvedQuantityData){

                        is_numeric($approvedQuantityData['approved_quantity']) ? $D = $approvedQuantityData['approved_quantity'] : $D = '-';
                        is_numeric($approvedQuantityData['approved_quantity_2']) ? $E = $approvedQuantityData['approved_quantity_2'] : $E = '-';

                        if($D != '-' && $E != '-'){
                            $F = $E - $D;
                        }elseif($D == '-'){
                            $F = $E;
                        }elseif($E == '-'){
                            $F = -$D;
                        }else{
                            $F = '-';
                        }

                        ($D == 0 || $D == '-' || $E == '-') ? $G = '-' : $G = round((($E * 100) / $D),2);

                        ?>
                        <td>{{$typeName}}</td>
                        <td>{{$D}}</td>
                        <td>{{$E}}</td>
                        <td>{{round($F, 2)}}</td>
                        <td>{{$G}}</td>
                        <td>{{$producerCountryName}}</td>
                </tr>
                <?php
                }
                ?>
            @endforeach
            @foreach($productSum[$hsCode] as $key => $value)
                <?php

                $refMeasurement = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $key);
                $typeName = optional($refMeasurement)->name;

                $value['sum_D'] == 0 ? $G = '-' : $G = (round((($value['sum_E'] * 100) / $value['sum_D']),2));

                $allSum[$key]['sum_D'] = $allSum[$key]['sum_D'] + $value['sum_D'];
                $allSum[$key]['sum_E'] = $allSum[$key]['sum_E'] + $value['sum_E'];
                ?>
                <tr>
                    <td colspan="2"><b>{{trans('swis.report.total.title')}}</b></td>
                    <td> {{ $typeName }}</td>
                    <td>{{$value['sum_D']}}</td>
                    <td>{{$value['sum_E']}}</td>
                    <td>{{$value['sum_E'] - $value['sum_D']}}</td>
                    <td>{{$G}}</td>
                    <td></td>
                </tr>
            @endforeach

            <?php $i++ ?>
            @endforeach

            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')