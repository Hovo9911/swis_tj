@if($renderHtml)
    @if(count($reportData) > 0)
        <?php
        $headName = $data['head_name'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $downloadType = $data['download_type'];
        ?>
        <table class="table table-striped table-bordered table-hover" border="1" id="data-table"
               @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="8"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                ['regime'=>trans('swis.'.$data['regime'].'_title'),
                'first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end'])),
                'document_status' => trans('swis.report'.$reportCode.'.document_status_'.$data['document_status'].'.custom_name')
                ])}}</h3></th>
            </tr>
            <tr>
                <th>{{ trans("swis.report.{$reportCode}.number") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.permission_number") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.permission_date") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.expiration_date") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.saf_side_name") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.products") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.total_netto_weight") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.producer_countries") }}</th>
            </tr>
            </thead>
            <tbody>
            <?php  $i = 1 ?>
            @foreach ($reportData as $data)
                <tr>
                    <td>{{$i++}}</td>
                    <td>{{$data['permission_number']}}</td>
                    <td>{{$data['permission_date']}}</td>
                    <td>{{$data['expiration_date']}}</td>
                    <td>{{$data['saf_side_name']}}</td>
                    <td>{{$data['products']}}</td>
                    <td>{{$data['total_netto_weight']}}</td>
                    <td>{{$data['producer_countries']}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')