@if($renderHtml)
    @if(count($reportData) > 0)
        <?php
        $headName = $data['head_name'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $downloadType = $data['download_type'];
        ?>
        <table class="table table-striped table-bordered table-hover" border="1" id="data-table"
               @if($renderHtml) style="text-align: center" @endif>
            <thead>

            <tr>
                <th colspan="2"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                ['regime'=>trans('swis.'.$data['regime'].'_title'),
                'first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end'])),
                'document_status' => trans('swis.report'.$reportCode.'.'.$data['document_status'].'.custom_name')
                ])}}</h3></th>
            </tr>
            <tr>
                <th>{{ trans("swis.report.{$reportCode}.number") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.importer_exporter") }}</th>
            </tr>
            </thead>
            <tbody>
            <?php  $i = 1 ?>
            @foreach ($reportData as $data)
                <tr>
                    <td>{{$i++}}</td>
                    <td>{{$data['columnB']}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')