@if($renderHtml)
    <?php
    $agency = \App\Models\Agency\Agency::where('tax_id', \Auth::user()->companyTaxId())->active()->first();

    $reportYear_1 = $reportData['year_1'];
    $reportYear_2 = $reportData['year_2'];
    $reportYear_2_PreviousMonth = $reportData['year_2_previous_month'];

    $headName = $data['head_name'] ?? '';
    $borderHeadName = $data['state_border_head'] ?? '';
    $downloadType = $data['download_type'];
    ?>
    <table class="table table-striped table-bordered table-hover" border="1" id="data-table"
           @if($renderHtml) style="text-align: center" @endif>
        <thead>
        <tr>
            <th colspan="14"><h3>{{trans('swis.report.'.$reportCode.'.table.name',[
                'year_1' => $data['year_1'],
                'year_2' => $data['year_2'],
                'month' => $data['month']
                ])}}</h3></th>
        </tr>
        <tr>
            <th rowspan="4">{{trans('swis.report.'.$reportCode.'.serial_number')}}</th>
            <th rowspan="4">{{trans('swis.report.'.$reportCode.'.agency_name')}}</th>
            <th colspan="4">{{trans('swis.report.'.$data['month'].'.month')}}</th>
            <th colspan="2" rowspan="2" >{{trans('swis.report.'.$reportCode.'.difference')}}</th>
            <th colspan="2">{{trans('swis.report.'.$reportYear_2_PreviousMonth['month'].'.month')}}</th>
            <th colspan="2">{{trans('swis.report.'.$data['month'].'.month')}}</th>
            <th colspan="2" rowspan="2" >{{trans('swis.report.'.$reportCode.'.difference')}}</th>
        </tr>
        <tr>
            <th colspan="2">{{trans('swis.report.period_a.title')}} {{$data['year_1']}}</th>
            <th colspan="2">{{trans('swis.report.period_a.title')}} {{$data['year_2']}}</th>
            <th colspan="2"> {{trans('swis.report.period_a.title')}} {{$reportYear_2_PreviousMonth['year']}}</th>
            <th colspan="2">{{trans('swis.report.period_a.title')}} {{$data['year_2']}}</th>
        </tr>
        <tr>
            @for($i = 0;$i < 6;$i++)
                <th>{{trans('swis.report.'.$reportCode.'.certificates_count')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.certificates_sum')}}</th>
            @endfor
        </tr>

        </thead>
        <tbody>
        <tr>
            <td>1</td>
            <td>{{$agency->current()->name ?? '-'}} </td>
            <td>{{$reportYear_1['certificates_all_count']}}</td>
            <td>{{$reportYear_1['certificates_all_sum']}}</td>

            <td>{{$reportYear_2['certificates_all_count']}}</td>
            <td>{{$reportYear_2['certificates_all_sum']}}</td>

            {{-- All Total--}}
            <td>{{$reportYear_2['certificates_all_count'] - $reportYear_1['certificates_all_count']}}</td>
            <td>{{$reportYear_2['certificates_all_sum'] - $reportYear_2['certificates_all_sum']}}</td>

            {{---------------start->  2-nd period --------------}}

            <td>{{$reportYear_2_PreviousMonth['certificates_all_count']}}</td>
            <td>{{$reportYear_2_PreviousMonth['certificates_all_sum']}}</td>

            <td>{{$reportYear_2['certificates_all_count']}}</td>
            <td>{{$reportYear_2['certificates_all_sum']}}</td>

            <td>{{$reportYear_2['certificates_all_count'] - $reportYear_2_PreviousMonth['certificates_all_count']}}</td>
            <td>{{$reportYear_2['certificates_all_sum'] - $reportYear_2_PreviousMonth['certificates_all_sum']}}</td>
        </tr>
        <tr>
            <td></td>
            <td>{{trans('swis.report.'.$reportCode.'.regime_type')}}</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>

            {{---------------start->  2-nd period --------------}}

            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>{{trans('swis.report.'.$reportCode.'.regime_type.import')}}</td>
            <td>{{$reportYear_1['import']['certificates_count']}}</td>
            <td>{{$reportYear_1['import']['certificates_sum']}}</td>

            <td>{{$reportYear_2['import']['certificates_count']}}</td>
            <td>{{$reportYear_2['import']['certificates_sum']}}</td>

            {{-- Import Total--}}
            <td>{{$reportYear_2['import']['certificates_count'] - $reportYear_1['import']['certificates_count'] }}</td>
            <td>{{$reportYear_2['import']['certificates_sum'] - $reportYear_1['import']['certificates_sum']}}</td>

            {{---------------start->  2-nd period --------------}}

            <td>{{$reportYear_2_PreviousMonth['import']['certificates_count']}}</td>
            <td>{{$reportYear_2_PreviousMonth['import']['certificates_sum']}}</td>

            <td>{{$reportYear_2['import']['certificates_count']}}</td>
            <td>{{$reportYear_2['import']['certificates_sum']}}</td>

            <td>{{$reportYear_2['import']['certificates_count'] - $reportYear_2_PreviousMonth['import']['certificates_count']}}</td>
            <td>{{$reportYear_2['import']['certificates_sum'] - $reportYear_2_PreviousMonth['import']['certificates_sum']}}</td>
        </tr>

        <tr>
            <td></td>
            <td>{{trans('swis.report.'.$reportCode.'.regime_type.export')}}</td>
            <td>{{$reportYear_1['export']['certificates_count']}}</td>
            <td>{{$reportYear_1['export']['certificates_sum']}}</td>

            <td>{{$reportYear_2['export']['certificates_count']}}</td>
            <td>{{$reportYear_2['export']['certificates_sum']}}</td>

            {{-- Export Total--}}
            <td>{{$reportYear_2['export']['certificates_count'] - $reportYear_1['export']['certificates_count'] }}</td>
            <td>{{$reportYear_2['export']['certificates_sum'] - $reportYear_1['export']['certificates_sum']}}</td>

            {{---------------start->  2-nd period --------------}}

            <td>{{$reportYear_2_PreviousMonth['export']['certificates_count']}}</td>
            <td>{{$reportYear_2_PreviousMonth['export']['certificates_sum']}}</td>

            <td>{{$reportYear_2['export']['certificates_count']}}</td>
            <td>{{$reportYear_2['export']['certificates_sum']}}</td>

            <td>{{$reportYear_2['export']['certificates_count'] - $reportYear_2_PreviousMonth['export']['certificates_count'] }}</td>
            <td>{{$reportYear_2['export']['certificates_sum'] - $reportYear_2_PreviousMonth['export']['certificates_sum']}}</td>
        </tr>
        </tbody>

    </table>

   @include('report.partials.pdf-download-type')

@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')