@extends('layouts.app')

@section('contentTitle') {{trans('swis.'.$reportCode.'.menu.title')}} @endsection

@section('content')
    <div class="search-data">
        <form class="form-horizontal fill-up" id="form-data" action="{{urlWithLng('report/'.$reportCode.'/generate')}}">
            <input type="hidden" name="report_code" value="{{$reportCode}}">

            <div class="row">
                <div class="col-md-6">

                    <div class="form-group required"><label class="col-lg-4 control-label">{{trans('swis.reports.select_year.label')}}</label>
                        <div class="col-lg-4">
                            <input type="text" placeholder="yyyy" autocomplete="off"  class="form-control onlyYear" name="year" id="" value="">
                            <div class="form-error" id="form-error-year"></div>
                        </div>
                    </div>

                    <div class="form-group "><label class="col-lg-4 control-label">{{trans('swis.reports.'.$reportCode.'.select_sub_division.label')}}</label>
                        <div class="col-lg-8">
                            <select class="select2 document-subdivisions" multiple name="sub_division_ids[]" data-allow-clear="false">
                                <option value=""></option>
                            </select>
                            <div class="form-error" id="form-error-sub_division_id"></div>
                        </div>
                    </div>

                    <div class="form-group required"><label class="col-lg-4 control-label">{{trans('swis.reports.'.$reportCode.'.select_count_type.label')}}</label>
                        <div class="col-lg-8">
                            <select class="form-control select2" name="count_type">
                                <option value="quantity">{{trans('swis.report'.$reportCode.'.count_type.quantity')}}</option>
                                <option value="sum">{{trans('swis.report'.$reportCode.'.count_type.sum')}}</option>
                            </select>
                            <div class="form-error" id="form-error-count_type"></div>
                        </div>
                    </div>

                    @include('report.partials.saf-regimes')

                    @include('report.partials.head-name')

                </div>
                <div class="col-md-6">

                    @include('report.partials.document-list')

                    @include('report.partials.document-status')

                    @include('report.partials.download-types')

                </div>

                <div class="col-md-12">
                    <div class="text-center">
                        <button class="btn btn-primary">{{trans('swis.report.generate_output.button')}}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="data-table">
        <hr/>
        <div id="reportResult"></div>
    </div>

@endsection

@section('scripts')
    <script>
        var getSubdivisions = true
    </script>
    <script src="{{asset('js/report/main.js')}}"></script>
@endsection