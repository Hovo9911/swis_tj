@if($renderHtml)

    @if(count($reportData))
        <?php
        $headName = $data['head_name'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $downloadType = $data['download_type'];
        ?>
        <table class="table table-striped table-bordered table-hover" border="1" id="data-table"
               @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="6"><h3>{{trans('swis.report.'.$reportCode.'.table.name',[
                'first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end'])),
                ])}}</h3> </th>
            </tr>
            <tr>
                <th>{{trans('swis.report.'.$reportCode.'.serial_number')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.sub_division_name')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.count.import.type')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.count.export.type')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.certificates_count')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.sub_division_certificates.sum')}}</th>
            </tr>

            </thead>
            <tbody>
            <?php  $i = 1; $SUM_C = 0;$SUM_D = 0;$SUM_E = 0;$SUM_F = 0?>
            @foreach($reportData as $sub_division_id=>$report)

                <?php
                $refSubDivision = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME, $sub_division_id);
                $C = isset($report['import']) ? $report['import']['certificates_count'] : 0;
                $D = isset($report['export']) ? $report['export']['certificates_count'] : 0;

                $sumImport = isset($report['import']) ? $report['import']['certificates_sum'] : 0;
                $sumExport = isset($report['export']) ? $report['export']['certificates_sum'] : 0;

                $E = $D + $C;
                $F = $sumImport + $sumExport;

                // Totals
                $SUM_C += $C;
                $SUM_D += $D;
                $SUM_E += $E;
                $SUM_F += $F;

                ?>
                <tr>
                    <td>{{$i}}</td>
                    <td>{{$refSubDivision->name ?? ''}}</td>
                    <td>{{$C}}</td>
                    <td>{{$D}}</td>
                    <td>{{$E}}</td>
                    <td>{{$F}}</td>
                </tr>
                <?php $i++ ?>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td></td>
                <td>{{trans('swis.report.'.$reportCode.'.totals')}}</td>
                <td>{{$SUM_C}}</td>
                <td>{{$SUM_D}}</td>
                <td>{{$SUM_E}}</td>
                <td>{{$SUM_F}}</td>
            </tr>
            </tfoot>
        </table>

       @include('report.partials.pdf-download-type')

    @endif

@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')