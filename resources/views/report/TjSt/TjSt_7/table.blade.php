@if($renderHtml)
    @if(count($reportData) > 0)
        <?php
        $headName = $data['head_name'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $downloadType = $data['download_type'];
        ?>
        <table class="table table-striped table-bordered table-hover" border="1" id="data-table" @if($renderHtml) style="text-align: center" @endif>
            <thead>
                <tr>
                    <th>{{ trans("swis.report.{$reportCode}.pt") }}</th>
                    <th>{{ trans("swis.report.{$reportCode}.enterprise_name") }}</th>
                    <th>{{ trans("swis.report.{$reportCode}.certificate_count") }}</th>
                    <th>{{ trans("swis.report.{$reportCode}.product_name") }}</th>
                    <th>{{ trans("swis.report.{$reportCode}.product_weight") }}</th>
                    <th>{{ trans("swis.report.{$reportCode}.producer_country") }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($reportData as $key => $data)
                    @if ($key !== 'total')
                        <tr>
                            <td rowspan="{{ count($data['products']) + 1 }}">{{ $data['incrementNumber'] }}</td>
                            <td rowspan="{{ count($data['products']) + 1 }}">{{ $data['enterpriseNames'] }}</td>
                            <td rowspan="{{ count($data['products']) + 1 }}">{{ $data['certificateCount'] }}</td>
                            @if (!count($data['products']))
                                <td></td>
                                <td></td>
                                <td></td>
                            @endif
                        </tr>
                        <tr>
                            @foreach ($data['products'] as $product)
                                <td>{{ $product['hs_code_name'] }}</td>
                                <td>{{ $product['approved_quantity'] }}</td>
                                <td>{{ $product['name'] }}</td>
                            @endforeach
                        </tr>
                    @else
                        <tr>
                            <td align="left" colspan="2">{{ trans("swis.report.{$reportCode}.total") }}</td>
                            <td>{{ $data['certificateCount'] }}</td>
                            <td></td>
                            <td>{{ $data['approved_quantity'] }}</td>
                            <td></td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')