@extends('layouts.app')

@section('contentTitle') {{trans('swis.'.$reportCode.'.menu.title')}} @endsection

@section('content')
    <div class="search-data">
        <form class="form-horizontal fill-up" id="form-data" action="{{urlWithLng('report/'.$reportCode.'/generate')}}">
            <input type="hidden" name="report_code" value="{{$reportCode}}">

            <div class="row">

                <div class="col-md-6">

                    @include('report.partials.first-period')

                    @include('report.partials.second-period')

                    <div class="form-group required">
                        <label class="col-lg-4 control-label">{{trans('swis.report.'.$reportCode.'.laboratory')}}</label>
                        <div class="col-lg-8">
                            <select class="form-control select2" name="laboratory">
                                <option></option>
                                @if (isset($laboratories) && $laboratories->count())
                                    @foreach ($laboratories as $laboratory)
                                        <option value="{{ $laboratory->id }}">{{ $laboratory->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div class="form-error" id="form-error-laboratory"></div>
                        </div>
                    </div>

                    @include('report.partials.head-name')

                </div>

                <div class="col-md-6">

                    @include('report.partials.document-list')

                    @include('report.partials.hs-code')

                    @include('report.partials.download-types')

                </div>

                <div class="col-md-12">
                    <div class="text-center">
                        <button class="btn btn-primary">{{trans('swis.report.generate_output.button')}}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="data-table">
        <hr/>
        <div id="reportResult"></div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/report/main.js')}}"></script>
@endsection