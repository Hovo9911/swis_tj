@if($renderHtml)
    @if(count($reportData['startSubApp']) > 0 || count($reportData['endSubApp']) > 0)
        <?php
        $headName = $data['head_name'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $downloadType = $data['download_type'];
        ?>
        <table class="table table-striped table-bordered table-hover" border="1" id="data-table"
               @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="6">

                    {{trans('swis.report.'.$reportCode.'.table.name',['first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end'])),
             'labName'=>$reportData['labName']])}}
                </th>
            </tr>
            <tr>
                <th rowspan="4">No</th>
                <th rowspan="4">{{ trans("swis.report.{$reportCode}.product_name") }}</th>
            </tr>
            <tr>
                <th colspan="4">{{ trans("swis.report.{$reportCode}.info_of_count") }}</th>
            </tr>
            <tr>
                <th colspan="2">{{ trans("swis.report.{$reportCode}.import") }}</th>
                <th colspan="2">{{ trans("swis.report.{$reportCode}.export") }}</th>
            </tr>
            <tr>
                <th>{{ trans("swis.report.{$reportCode}.batch") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.weight") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.batch") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.weight") }}</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 1; $SUM_C = $SUM_D = $SUM_E = $SUM_F = 0; ?>
            @foreach($reportData['startSubApp'] as $hsCode => $subApplication)
                <?php

                $hsCodeRef = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_HS_CODE_6, $hsCode, false, ['code', 'long_name']);

                $C = $subApplication['import']['filter_count'] ?? 0;
                $D = $subApplication['import']['netto_weight'] ?? 0;

                $E = $subApplication['export']['filter_count'] ?? 0;
                $F = $subApplication['export']['netto_weight'] ?? 0;

                $SUM_C += $C;
                $SUM_D += $D;
                $SUM_E += $E;
                $SUM_F += $F;

                ?>
                <tr>
                    <td>{{$i}}</td>
                    <td>{{'('.optional($hsCodeRef)->code .') '.optional($hsCodeRef)->long_name ?? '-'}}</td>
                    <td>{{$C}}</td>
                    <td>{{$D}}</td>
                    <td>{{$E}}</td>
                    <td>{{$F}}</td>
                </tr>
                <?php $i++ ?>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td></td>
                <td>
                    {{trans('swis.report.'.$reportCode.'.footer.first_period',['first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end'])),
                 ])}}
                </td>
                <td>{{$SUM_C}}</td>
                <td>{{$SUM_D}}</td>
                <td>{{$SUM_E}}</td>
                <td>{{$SUM_F}}</td>
            </tr>
            <tr>
                <td></td>
                <td>
                    {{trans('swis.report.'.$reportCode.'.footer.second_period',['first_period' => date('d.m.Y', strtotime($data['end_date_start'])).' - '.date('d.m.Y', strtotime($data['end_date_end'])),
                        ])}}
                </td>
                <td>{{$reportData['endSubApp']['import']['sum_filter_count']}}</td>
                <td>{{$reportData['endSubApp']['import']['sum_netto_weight']}}</td>
                <td>{{$reportData['endSubApp']['export']['sum_filter_count']}}</td>
                <td>{{$reportData['endSubApp']['export']['sum_netto_weight']}}</td>
            </tr>
            </tfoot>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')