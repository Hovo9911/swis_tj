@if($renderHtml)

    <?php
    $allReportData = $reportData['data'];
    $allReportDataTotals = $reportData['totals'];

    $headName = $data['head_name'] ?? '';
    $borderHeadName = $data['state_border_head'] ?? '';
    $downloadType = $data['download_type'];
    ?>
    @if(count($allReportData) > 0)

        <table class="table table-striped table-bordered table-hover" id="data-table" border="1"
               @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="7"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                ['regime'=>trans('swis.'.$data['regime'].'_title'),
                'first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end'])),
                'second_period' => date('d.m.Y', strtotime($data['end_date_start'])).' - '.date('d.m.Y', strtotime($data['end_date_end']))
                ])}}</h3></th>
            </tr>
            <tr>
                <th rowspan="3">{{trans('swis.report.'.$reportCode.'.serial_number')}}</th>
                <th rowspan="3">{{trans('swis.report.'.$reportCode.'.state_production.title')}}</th>
                <th rowspan="3">{{trans('swis.report.units.title')}}</th>
            </tr>
            <tr>
                <th colspan="2">{{trans('swis.report.'.$reportCode.'.quantity')}}</th>
                <th colspan="2">{{trans('swis.report.'.$reportCode.'.difference')}} , {{$data['start_date_start']}}
                    / {{$data['end_date_end']}}</th>
            </tr>
            <tr>
                <th>{{date('d.m.Y', strtotime($data['start_date_start']))}}
                    - {{date('d.m.Y', strtotime($data['start_date_end']))}}</th>
                <th>{{date('d.m.Y', strtotime($data['end_date_start']))}}
                    - {{date('d.m.Y', strtotime($data['end_date_end']))}}</th>
                <th>+;-</th>
                <th>%</th>
            </tr>
            </thead>
            <tbody>
            <?php  $i = 1; ?>
            @foreach($allReportData as $hsCode =>$measurements)
                <?php
                 $hsCodeReference = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_HS_CODE_6, $hsCode, false, ['code', 'long_name']);
                $hsCodeName = '';
                if (!is_null($hsCodeReference)) {
                    $hsCodeName = $hsCodeReference->long_name  .'('. $hsCodeReference->code .')';
                }
                ?>
                <?php $j = 1 ?>
                <?php $approved_D1_total = $approved_D2_total = $E_total = 0 ?>
                @foreach($measurements as $measurementId => $countries)

                    <?php
                    $measurementRef = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $measurementId);

                    $approved_D1_total = isset($allReportDataTotals[$hsCode][$measurementId]['total_D1']) ? $allReportDataTotals[$hsCode][$measurementId]['total_D1'] + $approved_D1_total : $approved_D1_total;
                    $approved_D2_total = isset($allReportDataTotals[$hsCode][$measurementId]['total_D2']) ? $allReportDataTotals[$hsCode][$measurementId]['total_D2'] + $approved_D2_total : $approved_D2_total;


                    $E_total = $approved_D2_total - $approved_D1_total;
                    ?>
                    <tr>
                        <td><b>{{$i}}</b></td>
                        <td><b>{{$hsCodeName}} ({{trans('swis.report.'.$reportCode.'.total')}})</b></td>
                        <td><b>{{optional($measurementRef)->name}}</b></td>
                        <td><b>{{$approved_D1_total}}</b></td>
                        <td><b>{{$approved_D2_total}}</b></td>
                        <td><b>{{$E_total}}</b></td>
                        <td><b>
                                @if((int)$approved_D1_total === 0 || (int)$approved_D2_total === 0  )
                                    0
                                @else
                                    {{ round(($E_total*100/$approved_D1_total),2) }}
                                @endif
                            </b>
                        </td>
                    </tr>

                    @foreach($countries as $countryId => $approved)
                        <?php
                        $countryRef = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_COUNTRIES, $countryId);

                        $D1 = $approved['approvedQty_D1'];
                        $D2 = $approved['approvedQty_D2'];
                        $E = $D2 - $D1;
                        ?>
                        <tr>
                            <td>{{$j}}</td>
                            <td>{{optional($countryRef)->name}}</td>
                            <td>{{optional($measurementRef)->name}}</td>
                            <td>{{$D1}}</td>
                            <td>{{$D2}}</td>
                            <td>{{$E}}</td>
                            <td>

                                @if((int)$D1 === 0 || (int)$D2 === 0  )
                                    0
                                @else
                                    {{ round($E*100/$D1 ,2) }}
                                @endif

                            </td>
                        </tr>
                        <?php  $j++ ?>

                    @endforeach

                @endforeach
                <?php  $i++ ?>
            @endforeach
            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')