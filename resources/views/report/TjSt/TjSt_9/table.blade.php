@if($renderHtml)

    @if(count($reportData) > 0)
        <?php

        $allSum = [];

        foreach ($reportData as $measurementUnit => $report) {

            foreach ($report as $key => $value) {

                if ($value['approved_quantity'] != '-') {
                    $allSum[$measurementUnit]['sum_D'] = isset($allSum[$measurementUnit]['sum_D']) ? $allSum[$measurementUnit]['sum_D'] + $value['approved_quantity'] : $value['approved_quantity'];

                }

                if ($value['approved_quantity_2'] != '-') {
                    $allSum[$measurementUnit]['sum_E'] = isset($allSum[$measurementUnit]['sum_E']) ? $allSum[$measurementUnit]['sum_E'] + $value['approved_quantity_2'] : $value['approved_quantity_2'];

                }
            }

            $allSum[$measurementUnit]['sum_D'] = !isset($allSum[$measurementUnit]['sum_D']) ? '-' : $allSum[$measurementUnit]['sum_D'];
            $allSum[$measurementUnit]['sum_E'] = !isset($allSum[$measurementUnit]['sum_E']) ? '-' : $allSum[$measurementUnit]['sum_E'];

            if (is_numeric($allSum[$measurementUnit]['sum_D']) && is_numeric($allSum[$measurementUnit]['sum_E'])) {
                $allSum[$measurementUnit]['sum_F'] = $allSum[$measurementUnit]['sum_E'] - $allSum[$measurementUnit]['sum_D'];
            } else if (!is_numeric($allSum[$measurementUnit]['sum_D'])) {
                $allSum[$measurementUnit]['sum_F'] = $allSum[$measurementUnit]['sum_E'];
            } else if (!is_numeric($allSum[$measurementUnit]['sum_E'])) {
                $allSum[$measurementUnit]['sum_F'] = ($allSum[$measurementUnit]['sum_D'] == 0) ? 0 : -$allSum[$measurementUnit]['sum_D'];
            } else if (!is_numeric($allSum[$measurementUnit]['sum_D'])) {
                $allSum[$measurementUnit]['sum_F'] = '-';
            }
        }

        $headName = $data['head_name'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $downloadType = $data['download_type'];

        ?>

        <table class="table table-striped table-bordered table-hover" border="1" id="data-table"
               @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="6"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                ['regime'=>trans('swis.'.$data['regime'].'_title'),
                'first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end'])),
                'second_period' => date('d.m.Y', strtotime($data['end_date_start'])).' - '.date('d.m.Y', strtotime($data['end_date_end'])),
                'HS Code' => $data['hs_code']
                ])}}</h3></th>
            </tr>
            <tr>
                <th></th>
                <th>{{trans('swis.report.'.$reportCode.'.product_name')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.product_type')}}</th>
                <th>{{trans('swis.report.period_a.title')}}
                    <br/> {{date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end']))}}
                </th>
                <th>{{trans('swis.report.period_b.title')}}
                    <br/> {{date('d.m.Y', strtotime($data['end_date_start'])).' - '.date('d.m.Y', strtotime($data['end_date_end']))}}
                </th>
                <th>{{trans('swis.report.'.$reportCode.'.difference')}}</th>
            </tr>
            </thead>
            <tbody>
            <?php  $i = 1 ?>
            @foreach($reportData as $measurementUnit=>$report)

                <?php
                $refMeasurement = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $measurementUnit);
                $typeName = optional($refMeasurement)->name;
                ?>
                <tr>
                    <td><b>{{$i++}}</b></td>
                    <td><b>{{trans('swis.report.'.$reportCode.'.total')}}</b></td>
                    <td><b>{{$typeName}}</b></td>
                    <td><b>{{$allSum[$measurementUnit]['sum_D']}}</b></td>
                    <td><b>{{$allSum[$measurementUnit]['sum_E']}}</b></td>
                    <td><b>{{$allSum[$measurementUnit]['sum_F']}}</b></td>
                </tr>

                <?php $j = 1; ?>

                @foreach($report as $hsCode=>$value)

                    <?php

                    $hsCodeRef = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_HS_CODE_6, $hsCode, false, ['code', 'long_name']);

                    $D = $value['approved_quantity'];
                    $E = $value['approved_quantity_2'];

                    if (is_numeric($D) && is_numeric($E)) {
                        $F = $E - $D;
                    } else if (!is_numeric($D)) {
                        $F = $E;
                    } else if (!is_numeric($E)) {
                        $F = ($D == 0) ? 0 : -$D;
                    } else {
                        $F = '-';
                    }

                    ?>
                    <tr>
                        <td>{{$j++}}</td>
                        <td> @if(!is_null($hsCodeRef)){{'('.$hsCodeRef->code .') '.$hsCodeRef->long_name}} @else
                                - @endif</td>
                        <td>{{$typeName}}</td>
                        <td>{{$D}}</td>
                        <td>{{$E}}</td>
                        <td>{{$F}}</td>
                    </tr>
                @endforeach
            @endforeach
            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')