@if($renderHtml)

    @if(count($reportData) > 0)

        <?php

        $hsCodeReference = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_HS_CODE_6,$data['hs_code'] ,false,['code','long_name']);
        $hsCodeName = '';
        if (!is_null($hsCodeReference)) {
            $hsCodeName = "'$hsCodeReference->long_name  ( $hsCodeReference->code )' ";
        }

        $sum_G = 0; $c = 0;$b = 0;
        $allSum = [];
        foreach ($reportData as $key => $report) {
            if (!empty($report['types'])) {
                foreach ($report['types'] as $type => $value) {
                    $allSum[$type]['sum_C'] = isset($allSum[$type]['sum_C']) ? $allSum[$type]['sum_C'] : 0;
                    $allSum[$type]['sum_E'] = isset($allSum[$type]['sum_E']) ? $allSum[$type]['sum_E'] : 0;
                    $allSum[$type]['sum_C'] = $allSum[$type]['sum_C'] + $value['approvedQty_A'];
                    $allSum[$type]['sum_E'] = $allSum[$type]['sum_E'] + $value['approvedQty_B'];
                }
            }
        }

        $headName = $data['head_name'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $downloadType = $data['download_type'];
        ?>

        <table class="table table-striped table-bordered table-hover" id="data-table" border="1" @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="8"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                ['regime'=>trans('swis.'.$data['regime'].'_title'),
                'first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end'])),
                'second_period' => date('d.m.Y', strtotime($data['end_date_start'])).' - '.date('d.m.Y', strtotime($data['end_date_end'])),
                'HS Code' => $data['hs_code']
                ])}}</h3></th>
            </tr>
            <tr>
                <th></th>
                <th>{{trans('swis.report.'.$reportCode.'.country_name')}}</th>
                <th>{{trans('swis.report.units.title')}}</th>
                <th>{{trans('swis.report.period_a.title')}} <br/>{{$data['start_date_start']}}
                    / {{$data['start_date_end']}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.percent')}}</th>
                <th>{{trans('swis.report.period_b.title')}} <br/>{{$data['end_date_start']}} / {{$data['end_date_end']}}
                </th>
                <th>{{trans('swis.report.'.$reportCode.'.percent')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.difference')}}</th>
            </tr>
            </thead>
            <tbody>
            <?php  $i = 1 ?>
            @foreach($reportData as $key=>$report)

                @if(!empty($report['types']))
                    @foreach($report['types'] as $type=>$value)
                        <?php
                        $C = $value['approvedQty_A'];
                        $E = $value['approvedQty_B'];

                        $D = isset($allSum[$type]) && $allSum[$type]['sum_C'] ? ($C / $allSum[$type]['sum_C']) * 100 : 0;
                        $F = isset($allSum[$type]) && $allSum[$type]['sum_E'] ? ($E / $allSum[$type]['sum_E']) * 100 : 0;
                        $G = $E - $C;

                        $sum_G += $G;

                        $typeName = $type;
                        $refMeasurement = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT,  $type);

                        $typeName = optional($refMeasurement)->name;

                        ?>
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{$report['country_name']}}</td>
                            <td>{{$typeName}}</td>
                            <td>{{$value['approvedQty_A']}}</td>
                            <td>{{round($D, 2)}}</td>
                            <td>{{$value['approvedQty_B']}}</td>
                            <td>{{round($F, 2)}}</td>
                            <td>{{$G}}</td>
                        </tr>
                        <?php $i++ ?>
                    @endforeach
                @else
                    <tr>
                        <td>{{$i}}</td>
                        <td>{{$report['country_name']}}</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                @endif
            @endforeach
            @foreach($allSum as $key=>$sum)
                <?php
                $typeName = $key;
                $refMeasurement = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT,  $key);

                $typeName = optional($refMeasurement)->name;

                ?>
                <tr>
                    <td colspan="2"><b>{{trans('swis.report.total.title')}} </b></td>
                    <td>{{ $typeName }}</td>
                    <td>{{$sum['sum_C']}}</td>
                    <td></td>
                    <td>{{$sum['sum_E']}}</td>
                    <td></td>
                    <td>{{$sum_G}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')