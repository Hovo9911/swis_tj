@if($renderHtml)

    @if(count($reportData) > 0)

        <?php

        $sum_G = $c = $b = 0;
        $allSum = $productSum = [];

        foreach ($reportData as $key => $report) {
            foreach ($report as $measurement => $value) {
                $productSum[$key][$measurement]['sum_D'] = $productSum[$key][$measurement]['sum_F'] = $allSum[$measurement]['sum_D'] = $allSum[$measurement]['sum_F'] = 0;
                foreach ($value as $country => $approvedQuantityData) {

                    is_numeric($approvedQuantityData['approved_quantity']) ? $productSum[$key][$measurement]['sum_D'] = $productSum[$key][$measurement]['sum_D'] + $approvedQuantityData['approved_quantity'] : '';
                    is_numeric($approvedQuantityData['approved_quantity_2']) ? $productSum[$key][$measurement]['sum_F'] = $productSum[$key][$measurement]['sum_F'] + $approvedQuantityData['approved_quantity_2'] : '';

                }
            }
        }

        foreach ($productSum as $key => $value) {
            foreach ($value as $measurement => $item) {

                $allSum[$measurement]['sum_D'] = $allSum[$measurement]['sum_D'] + $item['sum_D'];
                $allSum[$measurement]['sum_F'] = $allSum[$measurement]['sum_F'] + $item['sum_F'];
            }
        }

        $headName = $data['head_name'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $downloadType = $data['download_type'];

        ?>

        <table class="table table-striped table-bordered table-hover" id="data-table" border="1"
               @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="9"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                ['regime'=>trans('swis.'.$data['regime'].'_title'),
                'first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end'])),
                'second_period' => date('d.m.Y', strtotime($data['end_date_start'])).' - '.date('d.m.Y', strtotime($data['end_date_end']))
                ])}}</h3></th>
            </tr>
            <tr>
                <th>{{trans('swis.report.'.$reportCode.'.number')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.country_name')}}</th>
                <th>{{trans('swis.report.units.title')}}</th>
                <th>{{date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end']))}}</th>
                <th>%</th>
                <th>{{date('d.m.Y', strtotime($data['end_date_start'])).' - '.date('d.m.Y', strtotime($data['end_date_end']))}}</th>
                <th>%</th>
                <th>{{trans('swis.report.'.$reportCode.'.fark.title')}} +;-</th>
                <th>{{trans('swis.report.'.$reportCode.'.product_name')}}</th>
            </tr>
            </thead>
            <tbody>
            <?php  $i = 1 ?>

            @foreach($reportData as $hsCode => $report)

                <?php
                $k = 0;
                $hsCodeRef = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_HS_CODE_6, $hsCode, false, ['code', 'long_name']);
                ?>

                @foreach($report as $meauserementUnit => $countryData)
                    <?php

                    $refMeasurement = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $meauserementUnit);
                    $typeName = optional($refMeasurement)->name;

                    foreach($countryData as $producerCountryName => $approvedQuantityData){

                    is_numeric($approvedQuantityData['approved_quantity']) ? $D = $approvedQuantityData['approved_quantity'] : $D = '-';
                    is_numeric($approvedQuantityData['approved_quantity_2']) ? $F = $approvedQuantityData['approved_quantity_2'] : $F = '-';

                    if ($D != '-' && $F != '-') {
                        $H = $F - $D;
                    } elseif ($D == '-') {
                        $H = $F;
                    } elseif ($F == '-') {
                        $H = -$D;
                    } else {
                        $H = 0;
                    }

                    $sum_D = $allSum[$meauserementUnit]['sum_D'];
                    $sum_F = $allSum[$meauserementUnit]['sum_F'];

                    $sum_D == 0 ? $sum_D = 1 : $sum_D;
                    $sum_F == 0 ? $sum_F = 1 : $sum_F;

                    is_numeric($D) ? $E = round(($D / $sum_D) * 100 ,2) : $E = '-';
                    is_numeric($F) ? $G = round(($F / $sum_F) * 100 ,2) : $G = '-';

                    ?>
                    <tr>
                        <td>{{$i}}</td>
                        <td>{{$producerCountryName}}</td>
                        <td>{{$typeName}}</td>
                        <td>{{$D}}</td>
                        <td>{{$E}}</td>
                        <td>{{$F}}</td>
                        <td>{{$G}}</td>
                        <td>{{$H}}</td>
                        @if($k == 0)
                            <td rowspan="{{(count($report,COUNT_RECURSIVE)-count($report))/4}}" style="vertical-align: middle">{{'('.optional($hsCodeRef)->code .') '.optional($hsCodeRef)->long_name ?? '-'}}</td>
                        @endif
                    </tr>

                    <?php
                    $i++;$k++;
                    }

                    ?>
                @endforeach
            @endforeach
            @foreach($allSum as $key=>$sum)
                <?php
                $typeName = $key;
                $refMeasurement = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $key);
                $typeName = optional($refMeasurement)->name;
                ?>
                <tr>
                    <td colspan="2"><b>{{trans('swis.report.total.title')}} </b></td>
                    <td>{{ $typeName }}</td>
                    <td>{{$sum['sum_D']}}</td>
                    <td></td>
                    <td>{{$sum['sum_F']}}</td>
                    <td></td>
                    <td>{{$sum['sum_F'] - $sum['sum_D']}}</td>
                    <td></td>
                </tr>
                @endforeach
                </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')