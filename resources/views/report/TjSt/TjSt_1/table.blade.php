@if($renderHtml)
    <?php
    $allSum = [];
    foreach ($reportData as $key => $report) {
        if (!empty($report['types'])) {
            foreach ($report['types'] as $type => $value) {
                $allSum[$type]['sum_A'] = isset($allSum[$type]['sum_A']) ? $allSum[$type]['sum_A'] : 0;
                $allSum[$type]['sum_B'] = isset($allSum[$type]['sum_B']) ? $allSum[$type]['sum_B'] : 0;
                $allSum[$type]['sum_A'] = $allSum[$type]['sum_A'] + $value['approvedQty_A'];
                $allSum[$type]['sum_B'] = $allSum[$type]['sum_B'] + $value['approvedQty_B'];
            }
        }
    }

    $measurementReferenceTable = \App\Models\ReferenceTable\ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT;

    $headName = $data['head_name'] ?? '';
    $borderHeadName = $data['state_border_head'] ?? '';
    $downloadType = $data['download_type'];
    ?>
    @if(count($reportData) > 0)
        <a href=""></a>
        <table class="table table-striped table-bordered table-hover" border="1" id="data-table"
               @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="7"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                ['regime'=>trans('swis.'.$data['regime'].'_title'),
                'first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end'])),
                'second_period' => date('d.m.Y', strtotime($data['end_date_start'])).' - '.date('d.m.Y', strtotime($data['end_date_end'])),
                'document_status' => trans('swis.report'.$reportCode.'.'.$data['document_status'].'.custom_name'),
                'digit' =>$data['hs_code_count']
                ])}}</h3></th>
            </tr>
            <tr>
                <th rowspan="2"></th>
                <th rowspan="2">{{trans('swis.report.calculation.product_name')}}</th>
                <th rowspan="2">{{trans('swis.report.units.title')}}</th>
                <th rowspan="2">{{trans('swis.report.period_a.title')}}
                    <br/>{{date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end']))}}
                </th>
                <th rowspan="2">{{trans('swis.report.period_b.title')}}
                    <br/>{{date('d.m.Y', strtotime($data['end_date_start'])).' - '.date('d.m.Y', strtotime($data['end_date_end']))}}
                </th>
                <th colspan="2">{{trans('swis.report.calculation.title')}}</th>
            </tr>
            <tr>
                <th>+;-</th>
                <th>%</th>
            </tr>
            </thead>
            <tbody>
            <?php  $i = 1 ?>
            @foreach($reportData as $key=>$report)
                @if(!empty($report['types']))
                    @foreach($report['types'] as $type=>$value)

                        <?php
                        $A = $value['approvedQty_A'];
                        $B = $value['approvedQty_B'];

                        $D = $B - $A;
                        $E = 0;

                        $typeName = $type;
                        $refMeasurement = getReferenceRows($measurementReferenceTable, $type);
                        if (!empty($refMeasurement)) {
                            $typeName = $refMeasurement->name;
                        }
                        ?>
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{$report['hsCodeName']}}</td>
                            <td>{{$typeName}}</td>
                            <td>{{$A}}</td>
                            <td>{{$B}}</td>
                            <td>{{$D}}</td>
                            <td>
                                @if($A == 0 )
                                    -
                                @else
                                    {{ round(($D/$A)*100, 2) }}
                                @endif
                            </td>
                        </tr>
                        <?php  $i++ ?>
                    @endforeach
                @endif

            @endforeach
            @foreach($allSum as $key=>$sum)
                <?php
                $typeName = $key;
                $refMeasurement = getReferenceRows($measurementReferenceTable, $key);
                if (!empty($refMeasurement)) {
                    $typeName = $refMeasurement->name;
                }
                $sumA = $sum['sum_A'];
                $sumB = $sum['sum_B'];
                ?>
                <tr>
                    <td colspan="3"><b>{{trans('swis.report.total.title')}} {{ $typeName }}</b></td>
                    <td>{{$sumA}}</td>
                    <td>{{$sumB}}</td>
                    <td></td>
                    <td></td>
                </tr>
            @endforeach
            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')