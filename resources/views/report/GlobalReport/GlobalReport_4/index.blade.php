@extends('layouts.app')

@section('title') {{trans('swis.'.$reportCode.'.menu.title')}} @endsection

@section('contentTitle') {{trans('swis.'.$reportCode.'.menu.title')}} @endsection

@section('content')

    <?php
    use App\Models\PaymentProviders\PaymentProviders;

    $paymentProviders = PaymentProviders::select('id', DB::raw("CONCAT(name,' - ',description) AS name"))->active()->get();
    ?>

    <div class="search-data">
        <form class="form-horizontal fill-up" id="form-data" action="{{urlWithLng('report/'.$reportCode.'/generate')}}">
            <input type="hidden" name="report_code" value="{{$reportCode}}">

            <div class="row">

                <div class="col-md-6">

                    @include('report.partials.first-period' ,['label'=>trans("swis.report.$reportCode.payment_date"),'required'=>false])

                    <div class="form-group">
                        <label class="col-lg-4 control-label">{{trans("swis.report.$reportCode.company_name")}}</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="company_name" id="company_name" value="">
                            <div class="form-error" id="form-error-company_name"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-4 control-label">{{trans("swis.report.$reportCode.payment_provider")}}</label>
                        <div class="col-md-8">
                            <select class="select2" name="payment_provider_id">
                                <option></option>
                                @foreach($paymentProviders as $paymentProvider)
                                    <option value="{{ $paymentProvider->id }}">{{ $paymentProvider->name }}</option>
                                @endforeach
                            </select>
                            <div class="form-error" id="form-error-payment_provider_id"></div>
                        </div>
                    </div>

                    @include('report.partials.head-name')

                </div>

                <div class="col-md-6">

                    <div class="form-group">
                        <label class="col-lg-4 control-label">{{trans("swis.report.$reportCode.id")}}</label>
                        <div class="col-md-8">
                            <input type="number" min="0" class="form-control" name="id" id="id" value="">
                            <div class="form-error" id="form-error-id"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-4 control-label">{{trans("swis.report.$reportCode.user_name")}}</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="user_name" id="user_name" value="">
                            <div class="form-error" id="form-error-user_name"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-4 control-label">{{trans("swis.report.$reportCode.user_ssn")}}</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="user_ssn" id="user_ssn" value="">
                            <div class="form-error" id="form-error-user_ssn"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-4 control-label">{{trans("swis.report.$reportCode.transaction_id")}}</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="transaction_id" id="transaction_id" value="">
                            <div class="form-error" id="form-error-transaction_id"></div>
                        </div>
                    </div>

                    @include('report.partials.download-types')

                </div>

                <div class="col-md-12">
                    <div class="text-center">
                        <button class="btn btn-primary">{{trans('swis.report.generate_output.button')}}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="data-table">
        <hr/>
        <div id="reportResult"></div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/report/main.js')}}"></script>
@endsection