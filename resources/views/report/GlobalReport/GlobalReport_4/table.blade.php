@if($renderHtml)
    @if(count($reportData) > 0)
        <?php

        $headName = $data['head_name'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $downloadType = $data['download_type'];

        ?>
        <table class="table table-striped table-bordered table-hover" border="1" id="data-table"
               @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="8"><h3>{{trans('swis.report.'.$reportCode.'.table.name')
                 }}</h3>
                </th>
            </tr>
            <tr>
                <th>ID</th>
                <th>{{ trans("swis.report.{$reportCode}.user_name") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.user_ssn") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.transaction_id") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.payment_date") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.amount") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.payment_provider") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.company_name") }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($reportData as $data)
                <tr>
                    <td>{{$data['id']}}</td>
                    <td>{{$data['user_name']}}</td>
                    <td>{{$data['user_ssn']}}</td>
                    <td>{{$data['transaction_id']}}</td>
                    <td>{{formattedDate($data['payment_date'],true)}}</td>
                    <td>{{$data['amount']}}</td>
                    <td>{{$data['provider']}}</td>
                    <td>{{$data['company_name']}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')