@if($renderHtml)
    @if(count($reportData) > 0)
        <?php

        $constructorDocument = App\Models\ConstructorDocument\ConstructorDocumentMl::select('document_name')->where('constructor_documents_id', $data['document_id'])->where('lng_id', cLng('id'))->first();

        $constructorDocumentName = optional($constructorDocument)->document_name;

        $headName = $data['head_name'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $downloadType = $data['download_type'];

        ?>
        <table class="table table-striped table-bordered table-hover" border="1" id="data-table"
               @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="10"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                ['first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end'])),
                 'document_name' => $constructorDocumentName])
                 }}</h3>
                </th>
            </tr>
            </thead>
            <tbody>
            <?php  $reportData = array_unique($reportData, SORT_REGULAR); ?>

            @foreach ($reportData as $subDivisionCode => $subDivision)
                <?php $i = 1; ?>

                <tr>
                    <td colspan="10">
                        {{$subDivision[0]['subdivision']}}
                    </td>
                </tr>
                <tr>
                    <th>{{ trans("swis.report.{$reportCode}.number") }}</th>
                    <th>{{ trans("swis.report.{$reportCode}.saf_side_info") }}</th>
                    <th>{{ trans("swis.report.{$reportCode}.saf_sub_application_permission_or_rejection_number")  }}</th>
                    <th>{{ trans("swis.report.{$reportCode}.saf_side_type_value") }}</th>
                    <th>{{ trans("swis.report.{$reportCode}.saf_sub_application_request_date") }}</th>
                    <th>{{ trans("swis.report.{$reportCode}.saf_sub_application_submission_date") }}</th>
                    <th>{{ trans("swis.report.{$reportCode}.saf_sub_application_permission_or_rejection_date") }}</th>
                    <th>{{ trans("swis.report.{$reportCode}.saf_regime") }}</th>
                    <th>{{ trans("swis.report.{$reportCode}.state_name") }}</th>
                    <th>{{ trans("swis.report.{$reportCode}.superscribe") }}</th>
                </tr>
                @foreach($subDivision as $data)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$data['saf_side_info']}}</td>
                        <td>{{$data['saf_sub_application_permission_or_rejection_number']}}</td>
                        <td>{{$data['saf_applicant_type_value']}}</td>
                        <td>{{$data['saf_sub_application_request_date'] ?? '-'}}</td>
                        <td>{{$data['saf_sub_application_submission_date'] ?? '-'}}</td>
                        <td>{{$data['saf_sub_application_permission_or_rejection_date'] ?? '-'}}</td>
                        <td>{{$data['saf_regime']}}</td>
                        <td>{{$data['state_name']}}</td>
                        <td>{{$data['superscribe']}}</td>
                    </tr>
                @endforeach
            @endforeach
            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')