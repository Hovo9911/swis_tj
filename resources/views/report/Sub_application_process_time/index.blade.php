@extends('layouts.app')

@section('title') {{trans('swis.'.$reportCode.'.menu.title')}} @endsection

@section('contentTitle') {{trans('swis.'.$reportCode.'.menu.title')}} @endsection

@section('content')
    <div class="search-data">
        <form class="form-horizontal fill-up" id="form-data" action="{{urlWithLng('report/'.$reportCode.'/generate')}}">
            <input type="hidden" name="report_code" value="{{$reportCode}}">

            <div class="row">

                <div class="col-md-6">

                    @include('report.partials.first-period')

                    @include('report.partials.saf-regimes', ['multiple'=>true,'required'=>false])

                </div>

                <div class="col-md-6">
                    <?php
                        $agencies = App\Models\Agency\Agency::orderByDesc()->active()->get();
                        $user = Auth::user();
                        $companyTaxId = $user->companyTaxId();
                    ?>

                    <div class="form-group required">
                        <label class="col-lg-4 control-label">{{trans('swis.document.select_agency')}}</label>
                        <div class="col-lg-8">
                            <select class="form-control select2 agency-list {{ ($user->isAgency()) ? 'readonly' : '' }}" id="agency" name="agency" data-statuses="1" {{ ($user->isAgency()) ? 'readonly' : '' }}>
                                <option value=""></option>
                                @if (isset($agencies) && $agencies->count())
                                    @foreach ($agencies as $agency)
                                        <option value="{{ $agency->tax_id }}" {{ ($companyTaxId == $agency->tax_id) ? 'selected' :'' }}>{{ "({$agency->tax_id}) {$agency->legal_entity_name}" }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div class="form-error" id="form-error-agency"></div>
                        </div>
                    </div>

                    @include('report.partials.document-list',['class'=>'col-md-8','required'=>false])

                    <div class="form-group">
                        <label class="col-lg-4 control-label">{{trans('swis.report.'.$reportCode.'.sub_application_number')}}</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" name="sub_application_number" id="sub_application_number" value="">
                            <div class="form-error" id="form-error-sub_application_number"></div>
                        </div>
                    </div>

                    @include('report.partials.download-types',['class'=>'col-md-8'])

                </div>

                <div class="col-md-12">
                    <div class="text-center">
                        <button class="btn btn-primary">{{trans('swis.report.generate_output.button')}}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="data-table">
        <hr/>
        <div id="reportResult"></div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/report/main.js')}}"></script>
@endsection