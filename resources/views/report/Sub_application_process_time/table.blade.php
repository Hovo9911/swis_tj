@if($renderHtml)
    @if(count($reportData) > 0)
        <table class="table table-striped table-bordered table-hover" border="1" id="data-table" @if($renderHtml) style="text-align: center" @endif>
            <thead>
                <tr><td colspan="10" style="text-align: center; height: 40px;">{{ trans("swis.report.{$reportCode}.key1") }}</td></tr>
                <tr><td colspan="10" style="text-align: center; height: 40px;">@if (isset($data) && count($data) > 0){{ trans("swis.report.{$reportCode}.key2") }}@endif</td></tr>
                <tr>
                    <td colspan="10" style="text-align: center; min-height: 50px;">
                        @foreach ($data ?? [] as $key => $item)
                            @if (is_array($item))
                                <?php
                                    if ($key == 'regime') {
                                        $item = array_map(function ($value) {
                                            return $item = trans("swis.{$value}_title");
                                        }, $item);
                                    }
                                ?>
                                <p>{{ trans("swis.report.{$reportCode}.{$key}") }} = {{ implode(',', $item) }}, </p>
                            @else
                                <?php
                                if ($key == 'agency') {
                                        $agency = App\Models\Agency\Agency::select('id', 'tax_id', 'agency_ml.name as name')
                                            ->joinMl()
                                            ->where(['agency.tax_id' => $item])
                                            ->first();
                                        $item = optional($agency)->name;
                                    } elseif ($key == 'document_id') {
                                        $document = App\Models\ConstructorDocument\ConstructorDocument::select('id', 'document_code', 'constructor_documents_ml.document_name as name')
                                            ->joinMl()
                                            ->where(['constructor_documents.id' => $item])
                                            ->first();
                                        $item = optional($document)->name;
                                    }
                                ?>
                                <p>{{ trans("swis.report.{$reportCode}.{$key}") }} = {{ $item ?? '-' }}, </p>
                            @endif
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>{{ trans("swis.report.{$reportCode}.number") }}</th>
                    <th>{{ trans("swis.report.{$reportCode}.saf_number") }}</th>
                    <th>{{ trans("swis.report.{$reportCode}.agency") }}</th>
                    <th>{{ trans("swis.report.{$reportCode}.document") }}</th>
                    <th>{{ trans("swis.report.{$reportCode}.regime") }}</th>
                    <th>{{ trans("swis.report.{$reportCode}.saf_sub_application_number") }}</th>
                    <th>{{ trans("swis.report.{$reportCode}.saf_sub_application_status") }}</th>
                    <th>{{ trans("swis.report.{$reportCode}.saf_sub_application_start_date") }}</th>
                    <th>{{ trans("swis.report.{$reportCode}.saf_sub_application_end_date") }}</th>
                    <th>{{ trans("swis.report.{$reportCode}.spend_time") }}</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($reportData as $key => $data)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$data['saf_number']}}</td>
                        <td>{{$data['agency']}}</td>
                        <td>{{$data['document']}}</td>
                        <td>{{trans("swis.{$data['regime']}_title")}}</td>
                        <td>{{$data['saf_sub_application_number']}}</td>
                        <td>{{$data['saf_sub_application_status']}}</td>
                        <td>{{formattedDate($data['saf_sub_application_start_date'],false,true)}}</td>
                        <td>{{formattedDate($data['saf_sub_application_end_date'],false.true)}}</td>
                        <td>{{$data['spend_time']}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')