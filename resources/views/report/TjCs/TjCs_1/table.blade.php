@if($renderHtml)
    @if(count($reportData) > 0)
        <?php
        $headName = $data['head_name'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $downloadType = $data['download_type'];
        ?>
        <table class="table table-striped table-bordered table-hover" border="1" id="data-table"
               @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <?php
                $documentStatusTrans = '';
                foreach($data['document_status'] as $documentStatus){
                    $documentStatusTrans .= $documentStatus;
                }
                ?>
                <th colspan="6"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                ['regime'=>trans('swis.'.$data['regime'].'_title'),
                'first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end'])),
                'document_status' => trans('swis.report'.$reportCode.'.document_status_'.$documentStatusTrans.'.custom_name')
                ])}}</h3> </th>
            </tr>
            <tr>
                <th>{{ trans("swis.report.{$reportCode}.number") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.sub_app_number_and_date") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.importer_exporter") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.product_name") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.cert_number") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.invoice") }}</th>
            </tr>
            </thead>
            <tbody>
            <?php  $i = 1 ?>
            @foreach ($reportData as $data)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$data['columnB']}}</td>
                        <td>{{$data['columnC']}}</td>
                        <td>{!!$data['columnD']  !!}</td>
                        <td>{{$data['columnE']}}</td>
                        <td>{{round($data['columnF'],2)}}</td>
                    </tr>
            @endforeach
            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')