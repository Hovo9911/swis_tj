@if($renderHtml)
    <?php
    $allSum = [];

    $months = array();
    for ($x = 1; $x <=  12; $x++) {
        $months[date('m', mktime(0, 0, 0, $x, 1))] = date('F', mktime(0, 0, 0, $x, 1));
    }

    $countType = $data['count_type'];

    $monthTotals = [];
    $allReportSUM = 0;

    $headName = $data['head_name'] ?? '';
    $borderHeadName = $data['state_border_head'] ?? '';
    $downloadType = $data['download_type'];
    ?>
    @if(count($reportData) > 0)
        <a href=""></a>
        <table class="table table-striped table-bordered table-hover" border="1" id="data-table" @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="14"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                ['regime'=>trans('swis.'.$data['regime'].'_title'),
                'year' =>  $data['year'],
                'result.type' => trans('swis.report'.$reportCode.'.'.$data['count_type'].'.custom_name')
                ])}}</h3> </th>
            </tr>
            <tr>
                <th rowspan="2">{{trans('swis.report.'.$reportCode.'.sub_division_name')}}</th>
                <th colspan="13">{{trans('swis.report.'.$reportCode.'.month_list')}}</th>
            </tr>
            <tr >
                @foreach($months as $key=>$month)
                    <th>{{trans('swis.reports.month.'.$key.'.name')}}</th>
                @endforeach
                <th>{{trans('swis.report.'.$reportCode.'.sub_division_sum')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($reportData as $subDivisionId=>$report)
                <?php
                $regSubDivision = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME,$subDivisionId)
                ?>
                <tr>
                    <td>{{$regSubDivision->name}}</td>
                    @foreach($months as $key=>$month)
                        <?php
                        $sum = isset($report[$key]) ? $report[$key]['certificates_'.$countType] : 0;

                        $monthTotals[$key] = isset($monthTotals[$key]) ? $monthTotals[$key] + $sum : $sum;

                        $allReportSUM += $sum;

                        ?>
                        <td>{{ $sum != 0 ? $sum : '-'}}</td>
                    @endforeach
                    <td>{{$report['certificates_all_'.$countType]}}</td>
                </tr>
            @endforeach
            <tr>
                <td>{{trans('swis.report.'.$reportCode.'.total')}}</td>
                @foreach($months as $key=>$month)
                    <td>{{isset($monthTotals[$key]) ?  $monthTotals[$key] : 0}}</td>
                @endforeach
                <td>{{$allReportSUM}}</td>
            </tr>
            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')

