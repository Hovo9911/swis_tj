@if($renderHtml)
    @if(count($reportData) > 0)
        <?php
        $headName = $data['head_name'] ?? '';
        $borderHeadName = $data['state_border_head'] ?? '';
        $downloadType = $data['download_type'];
        ?>
        <table class="table table-striped table-bordered table-hover" border="1" id="data-table"
               @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="11"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                ['regime'=>trans('swis.'.$data['regime'].'_title'),
                'first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end'])),
                'document_status' => trans('swis.report'.$reportCode.'.document_status_'.$data['document_status'].'.custom_name'),
                'HS Code' => $data['hs_code']
                ])}}</h3></th>
            </tr>
            <tr>
                <th>{{ trans("swis.report.{$reportCode}.number") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.product_name") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.product_batch") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.product_batch_country") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.product_batch_producer_name") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.product_batch_netto_weight") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.product_batch_production_date") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.product_batch_expiration_date") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.product_batch_transport_type") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.product_batch_permission_number") }}</th>
                <th>{{ trans("swis.report.{$reportCode}.product_batch_approved_type") }}</th>
                {{--<th>{{ trans("swis.report.{$reportCode}.sub_app_id") }}</th>--}}
            </tr>
            </thead>
            <tbody>
            <?php  $i = 1;$totalNettoWeight = 0; ?>
            @foreach ($reportData as $data)
                <?php ($data['product_netto_weight'] != '-') ? $totalNettoWeight += $data['product_netto_weight'] : ''; ?>
                <tr>
                    <td>{{$i++}}</td>
                    <td>{{$data['product_code']}}</td>
                    <td>{{$data['product_batch_number']}}</td>
                    <td>{{$data['product_producer_country']}}</td>
                    <td>{{$data['product_producer_name']}}</td>
                    <td>{{$data['product_netto_weight']}}</td>
                    <td>{{$data['product_batch_production_date']}}</td>
                    <td>{{$data['product_batch_expiration_date']}}</td>
                    <td>{{$data['product_transport_type']}}</td>
                    <td>{{$data['product_permission_number']}}</td>
                    <td>{{$data['product_batch_approved_type']}}</td>
                    {{--<td>{{$data['sub_app_id']}}</td>--}}
                </tr>
            @endforeach
            <tr>
                <td></td>
                <td>{{trans("swis.report.{$reportCode}.netto_weight_total")}}</td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{$totalNettoWeight}}</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')