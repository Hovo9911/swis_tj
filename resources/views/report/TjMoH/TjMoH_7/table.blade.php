@if($renderHtml)
    <?php
    $headName = $data['head_name'] ?? '';
    $borderHeadName = $data['state_border_head'] ?? '';
    $downloadType = $data['download_type'];
    ?>
    @if(count($reportData) > 0)

        <table class="table table-striped table-bordered table-hover" border="1" id="data-table" @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="11"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                ['first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end'])),
                ])}}</h3> </th>
            </tr>
            <tr>
                <th>#</th>
                <th>{{trans('swis.report.'.$reportCode.'.submitting_date')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.importer')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.producer_countries')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.exporter')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.purpose_of_import')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.type_of_medicines')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.brutto_weight')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.total')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.approved_date')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.permission_number')}}</th>
            </tr>

            </thead>
            <tbody>
            @foreach($reportData as $key=>$report)
               <tr>
                   <td>{{$report['s_n']}}</td>
                   <td>{{$report['submitting_date']}}</td>
                   <td>{{$report['importer']}}</td>
                   <td>{{$report['producer_countries']}}</td>
                   <td>{{$report['exporter']}}</td>
                   <td>{{$report['purpose_of_import']}}</td>
                   <td>{{$report['type_of_medicines']}}</td>
                   <td>{{$report['brutto_weight']}}</td>
                   <td>{{$report['total']}}</td>
                   <td>{{$report['approved_date']}}</td>
                   <td>{{$report['permission_number']}}</td>
               </tr>
            @endforeach
            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')