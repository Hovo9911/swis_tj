@if($renderHtml)
    <?php

    $allSubDivisions = $reportData['allActiveSubDivisions'];
    $reportData = $reportData['reportData'];
    $totalImportCertificates = $totalExportCertificates = $totalAllCertificates = $totalObligationSum = 0;

    $headName = $data['head_name'] ?? '';
    $borderHeadName = $data['state_border_head'] ?? '';
    $downloadType = $data['download_type'];
    ?>
    @if(count($reportData) > 0)
        <a href=""></a>
        <table class="table table-striped table-bordered table-hover" border="1" id="data-table"
               @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="5"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                ['first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end'])),
                'hs_code' => $data['hs_code'],
                ])}}</h3></th>
            </tr>
            <tr>
                <th>{{trans('swis.report.'.$reportCode.'.sub_division_name')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.import.title')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.export.title')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.sub_app_count.title')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.sum.title')}}</th>
            </tr>

            </thead>
            <tbody>
            @foreach($reportData as $subdivisionId=>$items)
                @foreach($items as $item)
                    <?php
                    $importCertificatesCount = isset($reportData[$subdivisionId]['import']) ? $reportData[$subdivisionId]['import']['certificates_count'] : 0;
                    $exportCertificatesCount = isset($reportData[$subdivisionId]['export']) ? $reportData[$subdivisionId]['export']['certificates_count'] : 0;

                    $obligationSumImport = isset($reportData[$subdivisionId]['import']) ? $reportData[$subdivisionId]['import']['certificates_sum'] : 0;
                    $obligationSumExport = isset($reportData[$subdivisionId]['export']) ? $reportData[$subdivisionId]['export']['certificates_sum'] : 0;

                    $totalImportCertificates += $importCertificatesCount;
                    $totalExportCertificates += $exportCertificatesCount;

                    $totalObligationSum += $obligationSumImport + $obligationSumExport;

                    $refSubDivision = optional(getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME, $subdivisionId))->name;
                    ?>
                    <tr>
                        <td>{{$refSubDivision ?? ''}}</td>
                        <td>{{$importCertificatesCount}}</td>
                        <td>{{$exportCertificatesCount}}</td>
                        <td>{{$importCertificatesCount + $exportCertificatesCount}}</td>
                        <td>{{$obligationSumImport + $obligationSumExport}}</td>
                    </tr>
                @endforeach
            @endforeach

            <tr>
                <td>{{trans('swis.report.'.$reportCode.'.total')}}</td>
                <td>{{$totalImportCertificates}}</td>
                <td>{{$totalExportCertificates}}</td>
                <td>{{$totalImportCertificates + $totalExportCertificates}}</td>
                <td>{{$totalObligationSum}}</td>
            </tr>
            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')

