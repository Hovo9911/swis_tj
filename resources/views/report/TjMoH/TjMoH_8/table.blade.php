@if($renderHtml)
    <?php
    $headName = $data['head_name'] ?? '';
    $borderHeadName = $data['state_border_head'] ?? '';
    $downloadType = $data['download_type'];
    ?>
    @if(count($reportData) > 0)

        <table class="table table-striped table-bordered table-hover" border="1" id="data-table" @if($renderHtml) style="text-align: center" @endif>
            <thead>
            <tr>
                <th colspan="7"><h3>{{trans('swis.report.'.$reportCode.'.table.name',
                ['first_period' => date('d.m.Y', strtotime($data['start_date_start'])).' - '.date('d.m.Y', strtotime($data['start_date_end'])),
                ])}}</h3> </th>
            </tr>
            <tr>
                <th>#</th>
                <th>{{trans('swis.report.'.$reportCode.'.saf_number')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.import_target')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.medicine_type')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.product_quantity')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.total_amount')}}</th>
                <th>{{trans('swis.report.'.$reportCode.'.currency')}}</th>
            </tr>

            </thead>
            <tbody>
            <?php $allTotalAmount = 0 ?>
            <?php $allProductQuantity = 0 ?>
            @foreach($reportData as $key=>$report)
                <?php
                $allTotalAmount += $report['total_amount'] ;
                $allProductQuantity += $report['products_amount']
                ?>
               <tr>
                   <td>{{$key+1}}</td>
                   <td>{{$report['saf_number']}}</td>
                   <td>{{$report['import_target']}}</td>
                   <td>{{$report['medicine_type']}}</td>
                   <td>{{$report['products_amount']}}</td>
                   <td>{{$report['total_amount']}}</td>
                   <td>{{$report['currency']}}</td>
               </tr>
            @endforeach
            <tr>
                <td colspan="4"></td>
                <td><b>{{$allProductQuantity}}</b></td>
                <td><b>{{$allTotalAmount}}</b></td>
                <td></td>
            </tr>
            </tbody>
        </table>

       @include('report.partials.pdf-download-type')

    @endif
@endif

@include('report.partials.no-count-message')

@include('report.partials.download-pdf-link')