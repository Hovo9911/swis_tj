<?php
$langDataTable = [
    "sEmptyTable" => trans('swis.data_table.sEmptyTable'),//"No data available in table",
    "sInfo" => trans('swis.data_table.sInfo'), //"Showing _START_ to _END_ of _TOTAL_ entries",
    "sInfoEmpty" => trans('swis.data_table.sInfoEmpty'), //"Showing 0 to 0 of 0 entries",
    "sInfoFiltered" => trans('swis.data_table.sInfoFiltered'), //"(filtered from _MAX_ total entries)",
    "sInfoPostFix" => "",
    "sInfoThousands" => ",",
    "sLengthMenu" => trans('swis.data_table.sLengthMenu'), //"Show _MENU_ entries",
    "sLoadingRecords" => trans('swis.data_table.sLoadingRecords'), //"Loading...",
    "sProcessing" => trans('swis.data_table.sProcessing'), //"Processing...",
    "sSearch" => trans('swis.data_table.sSearch'), //"Search:",
    "sZeroRecords" => trans('swis.data_table.sZeroRecords'), //"No matching records found",
    "oPaginate" => [
        "sFirst" => trans('swis.data_table.oPaginate'), //"First",
        "sLast" => trans('swis.data_table.sLast'), //"Last",
        "sNext" => trans('swis.data_table.sNext'), //"Next",
        "sPrevious" => trans('swis.data_table.sPrevious'), // "Previous"
    ],
    "oAria" => [
        "sSortAscending" => trans('swis.data_table.sSortAscending'), //": activate to sort column ascending",
        "sSortDescending" => trans('swis.data_table.sSortDescending'), //": activate to sort column descending"
    ]

]

?>

{{-- Mainly scripts --}}
<script src="{{asset('assets/helix/core/js/jquery-3.1.1.min.js')}}"></script>

@if(!currentRouteNameIs('login'))
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            complete: function(result) {
                if(typeof result.responseJSON != 'undefined' && typeof result.responseJSON.errors != 'undefined' && typeof result.responseJSON.errors.hasNoRoleAjax != 'undefined' && result.responseJSON.errors.hasNoRoleAjax){
                    location.href = result.responseJSON.errors.url;
                }

                if(typeof result.responseJSON != 'undefined' && typeof result.responseJSON.errors != 'undefined' && typeof result.responseJSON.errors.needRefreshUserRolesSession != 'undefined' && result.responseJSON.errors.needRefreshUserRolesSession){
                    location.href = result.responseJSON.errors.url;
                }

                if(typeof result.responseJSON != 'undefined' && typeof result.responseJSON.errors != 'undefined' && typeof result.responseJSON.errors.needLogin != 'undefined' && result.responseJSON.errors.needLogin){
                    location.href = $cjs.admPath('login');
                }
            }
        });
    </script>
@endif

{{--  Global Variables --}}
<script>
    var $cLng = '{{cLng('code')}}';
    var $jsTrans = {"trans":<?php echo json_encode($jsTrans->getTranslations())?>};
    var $selectLabel = "{{ trans('core.base.label.select') }}";
    var $select2AutoCompleteLabel = "{{ trans('swis.document.label.autocomplete') }}";
    var $ssnMinLength = "{{config('global.ssn.min')}}";
    var $ssnMaxLength = "{{config('global.ssn.max')}}";
    var $taxIdMinLength = "{{config('global.tax_id.min')}}";
    var $taxIdMaxLength = "{{config('global.tax_id.max')}}";
    var $passportMinLength = "{{config('global.passport.min')}}";
    var $passportMaxLength = "{{config('global.passport.max')}}";
    var $viewModeView = "{{\App\Models\BaseModel::VIEW_MODE_VIEW}}";
    var $viewModeEdit = "{{\App\Models\BaseModel::VIEW_MODE_EDIT}}";
    var $viewModeAdd = "{{\App\Models\BaseModel::VIEW_MODE_ADD}}";
    var $countryMode = '{!! env('COUNTRY_MODE') !!}';
    var $datePlaceHolder = "{{setDatePlaceholder()}}";
    var $dateTimePlaceHolder = "{{setDatePlaceholder(true)}}";
    var $dataTableLanguages = {!! json_encode($langDataTable) !!};

    @if(isset($saveMode) && $saveMode)
        var $saveMode = "{{$saveMode}}";
    @endif
</script>

@yield('topScripts')

{{-- Info getting from ViewServiceProvider --}}
@auth
    @if(isset($checkAccessForEdit) && $checkAccessForEdit)
    <script>
        var $onlyViewMode = "{{\Auth::user()->hasAccessToEditModule() ?: 0}}";
    </script>
    @endif

    @if(isset($legalEntityValues))
    <script>
        var $legalEntityValues = {!! json_encode($legalEntityValues) !!}
    </script>
    @endif

    @if(isset($authUserIsBroker))
    <script>
        var $authUserIsBroker = +"{{$authUserIsBroker}}";
    </script>
    @endif
@endauth

<script src="{{asset('assets/helix/core/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/helix/core/js/trans.js')}}"></script>
<script src="{{asset('assets/helix/core/js/cjs.js')}}"></script>
<script src="{{asset('assets/helix/core/plugins/toastr/toastr.min.js')}}"></script>
<script src="{{asset('assets/helix/core/js/FormRequest2.js')}}"></script>
<script src="{{asset('assets/helix/core/plugins/select2/select2.full.min.js')}}"></script>
<script src="{{asset('assets/helix/core/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
<script src="{{asset('assets/helix/core/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('assets/helix/core/js/inspinia.js')}}"></script>
<script src="{{asset('assets/helix/core/plugins/pace/pace.min.js')}}"></script>

@auth
    <script src="{{asset('assets/helix/core/plugins/dataTables/datatables.min.js')}}"></script>
{{--    <script src="{{asset('assets/helix/core/plugins/dataTables/fixedHeader.datatables.min.js')}}"></script>--}}
    <script src="{{asset('assets/helix/core/js/DataTables.js')}}"></script>
    <script src="{{asset('assets/helix/core/js/Modal.js')}}"></script>
    <script src="{{asset('assets/helix/core/js/AutoComplete.js')}}"></script>
    <script src="{{asset('assets/helix/core/plugins/jasny/input-mask.js')}}"></script>
    <script src="{{asset('assets/helix/core/plugins/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/helix/core/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}" charset="UTF-8"></script>
    <script src="{{asset('assets/helix/core/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js')}}" charset="UTF-8"></script>
    <script src="{{asset('assets/helix/core/plugins/bootstrap-datepicker/bootstrap-datepicker-locales.js')}}"></script>
@endauth

@yield('scripts')

<script src="{{asset('js/web-fonts.js')}}"></script>
<script src="{{asset("js/main.js")}}"></script>
