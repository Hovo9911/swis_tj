<nav class="navbar-default navbar-static-side" role="navigation">
<div class="sidebar-collapse">
    @auth
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="logo-text text-center">
                    <a class="ver-top-box" href="{{urlWithLng('dashboard')}}">
                    <span class="ver-top-box logo-img">
                            <img src="/image/gerb_TJ_sm.png" alt="">
                    </span>
                        <span class="db text-uppercase fb">{{trans('swis.core.single_window.title')}}</span>
                    </a>
                </div>
            </li>

            <li class="hide--web">
                <a href="{{urlWithLng('')}}"><i class="fas fa-home"></i>
                    <span class="nav-label">{{trans('swis.home.url')}}</span>
                </a>
            </li>

            <?php
            $requestRouteId = 0;
            ?>
            @if(isset($menus))
                <?php
                $urlSegment_2 = request()->segment(2);
                $requestRouteId = request()->route('id');
                $isSwAdmin = \Auth::user()->isSwAdmin();
                ?>
                @foreach($menus as $key=>$menu)

                    @if(!empty($menu->name))
                        @if($menu->name == \App\Models\Menu\Menu::CONSTRUCTOR_MENU_NAME)
                            @if(empty(session('enable_constructor')) && !session('enable_constructor'))
                                @continue
                            @endif
                        @endif

                        <?php
                        $setActive = '';
                        if (!empty($menu->sub)) {

                            foreach ($menu->sub as $menuLevel):
                                if ($urlSegment_2 == $menuLevel->href || $urlSegment_2 == $menu->href) {
                                    $setActive = 'active';
                                }
                            endforeach;

                        } else {
                            if ($urlSegment_2 == $menu->href) {
                                $setActive = 'active';
                            }
                        }
                        ?>

                        @if(!empty($menu->sub) && empty($menu->title))
                            @continue
                        @endif
                        <li class="{{$setActive}}" title="{{!empty($menu->tooltip) ? trans($menu->tooltip) : ''}}"
                            data-toggle="tooltip">
                            <a href="{{(empty($menu->href) || !empty($menu->sub)) ? '#' : urlWithLng($menu->href) }}">{!! $menu->icon !!}
                                <span class="nav-label">{{trans($menu->title)}}</span>
                                <span class="fa arrow {{(empty($menu->sub)) ? 'hide' : '' }}"></span>
                            </a>

                            @if(!empty($menu->sub))

                                @if(count($menu->sub) > 4)
                                    <div class="searchDivMenu">
                                        <input type="text" class="searchMenu form-control" placeholder="{{trans('core.base.label.search')}}" value="">
                                    </div>
                                @endif

                                <ul class="nav nav-second-level collapse">

                                    @foreach($menu->sub as $menuLevel)
                                        <li class="{{(request()->is(cLng('code').'/'.$menuLevel->href) ||
                                                      request()->is(cLng('code').'/'.$menuLevel->href.'/*') ||
                                                      (($menu->href == 'single-application' && request()->segment(4)) && (strpos($menuLevel->href, request()->segment(4)) !== false) && (strpos(request()->getPathInfo(),'single-application') !== false))
                                                      ) ? 'active': '' }}">
                                            <a href="{{urlWithLng($menuLevel->href)}}"> {!! $menuLevel->icon !!} {{trans($menuLevel->title)}}</a>
                                        </li>
                                    @endforeach

                                    @if($isSwAdmin && $menu->name == \App\Models\Menu\Menu::REFERENCE_TABLE_FORM_MENU_NAME)
                                        <?php
                                           $inactiveRefs =  \App\Models\ReferenceTable\ReferenceTable::getInactiveRef()
                                        ?>
                                        @if($inactiveRefs->count())

                                            <li class="inactive-ref-title"><span>{{trans('swis.menu.reference_table_form.inactive.title')}}</span><hr /></li>

                                            @foreach($inactiveRefs as $inactiveRef)

                                                    <li class="@if($requestRouteId == $inactiveRef->id) active @endif">

                                                        <a href="{{urlWithLng('/reference-table-form/'.$inactiveRef->id)}}">
                                                            {{trans('swis.reference.title.' . $inactiveRef->table_name)}}
                                                        </a>
                                                    </li>
                                            @endforeach

                                        @endif
                                    @endif
                                </ul>
                            @endif
                        </li>

                    @endif
                @endforeach
            @endif

            @if(\Auth::user()->isAgency())

                <?php
                $agencyReferences = \Auth::user()->getUserAgencyReferenceTables();
                ?>

                @if($agencyReferences->count())

                    <?php
                    $setRefActive = '';
                    foreach ($agencyReferences as $key => $value) {
                        if (request()->is(cLng('code').'/reference-table-form/'.$value->id) || request()->is(cLng('code').'/reference-table-form/'.$value->id.'/*')) {
                            $setRefActive = 'active';
                            break;
                        }
                    }
                    ?>
                    <li class="{{$setRefActive}}" title="{{trans('swis.reference_table.form.tooltip')}}"
                        data-toggle="tooltip">

                        <a href="#">
                            <i class="fas fa-clipboard-list"></i>
                            <span class="nav-label">{{trans('swis.reference_table.form.title')}}</span>
                            <span class="fa arrow"></span>
                        </a>

                        @if($agencyReferences->count() > 4)
                        <div class="searchDivMenu">
                            <input type="text" class="searchMenu form-control" placeholder="{{trans('core.base.label.search')}}" value="">
                        </div>
                        @endif

                        <ul class="nav nav-second-level collapse">
                        @foreach($agencyReferences as $agencyReference)

                                <li class="@if($requestRouteId == $agencyReference->id) active @endif">

                                    <a href="{{urlWithLng('/reference-table-form/'.$agencyReference->id)}}">
                                        {{trans('swis.reference.title.' . $agencyReference->table_name)}}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>

                @endif

                <?php
                $userDocuments = \Auth::user()->getUserDocuments();
                if($userDocuments->count()):

                $setMyApplicationActive = '';
                foreach ($userDocuments as $key => $value) {
                    if ($requestRouteId == $value->id) {
                        $setMyApplicationActive = 'active';
                        break;
                    }
                }
                ?>

                <li class="{{$setMyApplicationActive}}" title="{{trans('swis.user_documents.menu.tooltip')}}"
                    data-toggle="tooltip">
                    <a href="#">
                        <i class="fas fa-file-signature"></i>
                        <span class="nav-label">{{trans('swis.user_documents.menu.label')}}</span>
                        <span class="fa arrow"></span>
                    </a>

                    @if($userDocuments->count() > 3)
                    <div class="searchDivMenu">
                        <input type="text" class="searchMenu form-control" placeholder="{{trans('core.base.label.search')}}" value="">
                    </div>
                    @endif

                    <ul class="nav nav-second-level collapse">
                        @foreach($userDocuments as $document)
                            <li class="@if($requestRouteId == $document->id) active @endif">
                                <a href="{{urlWithLng('/user-documents/'.$document->id)}}">
                                    {{$document->document_name}}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li>
                <?php endif ?>
            @endif
            <?php $authUserReports = \Auth::user()->getUserReports();?>
            @if($authUserReports->count() > 0)
                <?php

                $setReportActive = '';
                foreach ($authUserReports as $key => $value) {
                    if (request()->route('code') == $value->code) {
                        $setReportActive = 'active';
                        break;
                    }
                }
                ?>
                <li class="{{$setReportActive}}" title="{{trans('swis.reports.menu.tooltip')}}" data-toggle="tooltip">
                    <a href="#">
                        <i class="fa fa-info-circle"></i>
                        <span class="nav-label">{{trans('swis.reports.menu.label')}}</span>
                        <span class="fa arrow"></span>
                    </a>

                    @if($authUserReports->count() > 3)
                    <div class="searchDivMenu">
                        <input type="text" class="searchMenu form-control" placeholder="{{trans('core.base.label.search')}}" value="">
                    </div>
                    @endif
                    <ul class="nav nav-second-level collapse">
                        @if($authUserReports->count() > 0)
                            @foreach($authUserReports as $report)
                                <li class="{{request()->route('code') == $report->code ? 'active' : ''}}">
                                    <a href="{{urlWithLng('/report/'.$report->code)}}">{{trans('swis.'.$report->code.'.menu.title')}}
                                    </a>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                </li>
            @endif
        </ul>
    @endauth
</div>
</nav>
