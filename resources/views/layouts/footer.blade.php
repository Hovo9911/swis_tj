<!-- Footer Start -->
<div class="footer">
    <div>
        <strong>Copyright</strong> SWIS&copy; 2018 - {{date('Y')}} <span class="version-date">{{trans('swis.app_last_update')}} {{formattedDate(env('APP_LAST_UPDATE'))}} , {{trans('swis.app_version')}} {{env('APP_VERSION')}}</span>
    </div>
</div>
<!-- Footer End -->