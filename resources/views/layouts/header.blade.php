@include('partials.translations',['transType'=>'global'])

<header class="header">
    <div class="header--fixed clearfix closeRefreshGroup">

        @auth
            <a title="{{trans('swis.navbar.btn_burger.title')}}" class="header__burger navbar-minimalize btn--burger"
               href="#"><i class="fa fa-bars"></i> </a>
        @endauth

        <div class="pull-left header__left">
            @if(!\Auth::check() && (currentRouteNameIs(['help','reset-password','set-password'])))
                <div class="home-icon__out ver-top-box">
                    <a class="icon--home text-uppercase fb" href="{{urlWithLng('')}}">
                        <i class="fa fa-home"></i>{{trans('swis.home.url')}}
                    </a>
                </div>
            @endif

            <a title="{{trans('core.form.faq.tooltip')}}" class="icon--faq fb ver-top-box" href="{{urlWithLng('help')}}"><i class="fa fa-comments"></i>{{trans('swis.header.faq.title')}}</a>

            <?php
            $languages = activeLanguages();
            $lngManager = \App\Http\Controllers\Core\Language\LanguageManager::getInstance();
            $currentLanguage = currentLanguage();
            ?>
            <div class="languages pr select-none ver-top-box">

                <div title="{{trans('core.base.language.'.strtolower($currentLanguage->name))}}"  class="languages__title pr">
                    <span class="ver-top-box languages__img">
                        <img src="{{asset('image/'.$currentLanguage->icon)}}" alt="">
                    </span>
                    <span class="ver-top-box languages__txt fb">{{trans('core.base.language.'.$currentLanguage->code,[],$currentLanguage->code)}}</span>
                </div>
                <div class="languages__list dn">
                    @foreach($languages as $language)
                        @if($currentLanguage->id == $language->id) @continue @endif
                        <a title="{{trans('core.base.language.'.strtolower($language->name))}}" href="{{$lngManager->getUrl($language)}}" class="languages__link db">
                            <span class="ver-top-box languages__img">
                                <img src="{{asset('image/'.$language->icon)}}" alt="">
                            </span>
                            <span class="ver-top-box languages__txt">{{trans('core.base.language.'.$language->code,[],$language->code)}}</span>
                        </a>
                    @endforeach
                </div>
            </div>
            @if(\Auth::check() && !is_null(Auth::user()->getCurrentUserRole())  )

                <div class="ver-top-box login-type">
                    <form action="{{urlWithLng('role/set')}}" method="post">
                        <label class="ver-top-box login__text">{{trans('swis.core.your_are.title')}}</label>
                        {{csrf_field()}}
                        @include('role.roles-list',['class'=>'select2-no-loading'])
                    </form>
                </div>
            @endif
        </div>

        <?php
            $closeModulesForCurrentUsers = false;

            if(Auth::check() && ((Auth::user()->isNaturalPerson()) || Auth::user()->isCustomsOperator())){
                $closeModulesForCurrentUsers = true;
            }
        ?>

        @auth
        <div class="pull-right header__right">
            @if(!currentRouteNameIs('roleSet') && !$closeModulesForCurrentUsers)
                @if(Auth::user()->hasPaymentModuleAccess() && (!Auth::user()->isAgency() && !Auth::user()->isSwAdmin()))
                    <div class="header-balance ver-top-box">
                        <p class="header-balance__txt">{{trans('swis.header.balance.title')}}</p>
                        <a class="header-balance__link fb ver-top-box" href="{{urlWithLng('payments')}}">
                            <span id="userBalance">{{ Auth::user()->balance() }}</span>
                            <span>{{ config('swis.default_currency') }}</span>
                        </a>
                    </div>
                @endif
            @endif
            <div class="ver-top-box header__notificaion-profile">
                @if(!currentRouteNameIs('roleSet') && !$closeModulesForCurrentUsers)
                    <div class="ver-top-box header-notification pr">
                        <a class="ver-top-box header-notification__link" href="#" id="navbarDropdownMenuLink"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bell"></i>
                            @if($unreadNotificationsCount > 0)
                                <span class="label label-warning-light notifications-count header-notification__count" data-count="{{$unreadNotificationsCount}}">{{$unreadNotificationsCount}}</span>
                            @endif
                        </a>
                        <ul class="dropdown-menu dropdown-alerts animated fadeInRight header-notification__sub m-t-xs"
                            aria-labelledby="navbarDropdownMenuLink">
                            @foreach(App\Models\Notifications\Notifications::newestNotifications()  as $newestNotification)
                                @if(!is_null($newestNotification->in_app_notification))
                                <li class="header-notification__item {{ ($newestNotification->read == \App\Models\Notifications\Notifications::NOTIFICATION_STATUS_UNREAD) ? 'header-notification__item--unread' : '' }}">
                                    <div>
                                        <div class="media-body">
                                            <span>{!! $newestNotification->in_app_notification !!}</span><br>
                                            <small class="text-muted header-notification__date db">{{$newestNotification->created_at}}</small>
                                        </div>
                                    </div>
                                </li>
                                <li class="dropdown-divider header-notification__divider"></li>
                                @endif
                            @endforeach
                            <li>
                                <div class="text-center link-block header-notification__seeAll">
                                    <a href="{{urlWithLng('notifications')}}"
                                       class="dropdown-item fb header-notification__seeAll--link">
                                        {{trans('swis.header.button.show_all_notifications')}}
                                        <i class="fa fa-angle-right header-notification__arrow"></i>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                @endif

                <div class="dropdown profile-element ver-top-box header-profile">
                    <a data-toggle="dropdown" class="dropdown-toggle ver-top-box header-profile__name" href="#"
                       aria-expanded="true">
                <span class="header-profile__nameTxt three-dot__oneLine db"
                      title="{{Auth::user()->name()}}">{{Auth::user()->name()}}</span>
                        <b class="header-profile__arrow caret"></b>
                        <i class="fa fa-user header-profile__user" aria-hidden="true"></i>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        @if(!currentRouteNameIs('roleSet'))
                            <li>
                                <a href="{{urlWithLng('profile-settings')}}">{{trans('swis.user.profile_settings.title')}}</a>
                            </li>
                            <li class="dropdown-divider"></li>
                        @endif
                        <li>
                            <a href="#"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out-alt"></i> {{trans('swis.core.logout')}}
                            </a>
                            <form id="logout-form" action="{{ urlWithLng('/logout') }}" method="POST"
                                  style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
       @endauth
    </div>
</header>