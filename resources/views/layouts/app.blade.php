<!DOCTYPE html>
<head>
    <!-- TITLE -->
    <title>@yield('title', trans('swis.core.single_window.title'))</title>

    @include('layouts.head')
</head>

<body class="@if(!Auth::check())main--page logout--page @endif wrapper--materialDesign {{ config('swis.theme_type') }}">

@include('partials.alerts')

<div id="wrapper" class="main-content custom-wrapper">

    <!-- Header Start -->
    @include('layouts.header')
    <!-- Header End -->

    @auth
        @include('layouts.left-navbar')
    @endauth

    <div @auth  id="page-wrapper" @else style="position: relative !important;" @endauth class="gray-bg">

        <div class="row wrapper border-bottom white-bg page-heading custom-wrapper__main"> <!-- withoutaction-userName--out -->
            <div class="col-sm-6">
                <h2>@yield('contentTitle',trans('swis.dashboard.content.title'))</h2>
            </div>
            @auth
                <div class="col-sm-6 text-right buttons__out">
                    @yield('form-buttons-top')
                </div>
            @endauth
        </div>

        <!-- Content Start -->
        <div class="wrapper wrapper-content animated fadeIn clearfix">
            <div class="ibox">
                <div class="ibox-content">
                    <div id="showAlertsMessages"></div>
                    @yield('content')
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-bottom-buttons">
                                @yield('form-buttons-bottom')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Content End -->

        @include('layouts.footer')
    </div>
</div>

@yield('footer-part')

@include('layouts.main-scripts')

@include('partials.modals')

</body>
</html>
<?php
//    sort($_SESSION['sql'])
?>
{{--{{dd($_SESSION)}}--}}