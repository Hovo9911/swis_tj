    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">

    <meta property="og:title" content="@yield('title', trans('swis.core.single_window.title'))" />
    <meta property="og:image" content="{{url('image/gerb_tj.png')}}" />
    <meta property="og:type" content="website" />

    <!-- Always force latest IE rendering engine or request Chrome Frame -->
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Main Styles -->
    <link rel="stylesheet" href="{{asset('assets/helix/core/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/helix/core/plugins/fontawesome-old/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/helix/core/plugins/fontawesome/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/helix/core/plugins/toastr/toastr.min.css')}}">

    @auth
        <link rel="stylesheet" href="{{asset('assets/helix/core/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" >
        <link rel="stylesheet" href="{{asset('assets/helix/core/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/helix/core/css/plugins/dataTables/datatables.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/helix/core/css/animate.css')}}">
        <link rel="stylesheet" href="{{asset('assets/helix/core/plugins/select2/select2.min.css')}}">
    @endauth

    <link rel="stylesheet" href="{{asset('assets/helix/core/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/hover.css')}}">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" href="{{asset('css/media.css')}}">

    @yield('styles')