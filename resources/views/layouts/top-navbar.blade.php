@include('partials.translations',['transType'=>'global'])

<div class="row border-bottom">
    @if(\Auth::check() && \Auth::user()->getCurrentUserRole())
        <div class="navbar-header">
            <a title="{{trans('swis.navbar.btn_burger.title')}}"
               class="navbar-minimalize minimalize-styl-2 btn btn-primary btn--burger" href="#"><i
                        class="fa fa-bars"></i> </a>
        </div>
    @endif

    <nav class="navbar navbar-static-top " role="navigation" style="margin-bottom: 0">
        @if(!\Auth::check() && currentRouteNameIs('help'))
            <div class="pull-left login-type">
                <a href="{{urlWithLng('')}}" class="btn btn-primary  m-l"><i class="fas fa-home"></i> {{trans('swis.home.url')}}</a>
            </div>
        @endif

        @include('partials.languages-list')

        @if(\Auth::check() && !is_null(Auth::user()->getCurrentUserRole())  )

            <div class="pull-left login-type">
                <form action="{{urlWithLng('role/set')}}" method="post">
                    {{csrf_field()}}
                    @include('role.roles-list')
                </form>
            </div>
        @endif
    </nav>
</div>