<!DOCTYPE html>
<head>
    @include('layouts.head')
</head>
<body class="gray-bg">

    <div class="middle-box text-center animated fadeInDown">
        <h1>500</h1>
{{--        <h3 class="font-bold"></h3>--}}
        <div class="error-desc">
           <h3> {{trans('swis.db.exception.message')}}</h3>
            <a href="{{urlWithLng('dashboard')}}" class="btn btn-primary m-t">{{trans('swis.home.url')}}</a>
        </div>
    </div>

    @include('layouts.main-scripts')
</body>
</html>