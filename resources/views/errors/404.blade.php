<!DOCTYPE html>
<head>
    @include('layouts.head')
</head>
<body class="gray-bg">

    <div class="middle-box text-center animated fadeInDown">
        <h1>404</h1>
        <h3 class="font-bold">{{ trans('swis.global.page_not_found') }}</h3>

        <div class="error-desc">
            {{ trans('swis.global.page_not_found_reason') }}
            <br />
            {{ trans('swis.global.page_not_found_reason.wrong_url') }}
            <br />
            {{ trans('swis.global.page_not_found_reason.access_denied') }}
            <br />
            {{ trans('swis.global.page_not_found_reason.wrong_role') }}
            <br />
            {{ trans('swis.global.page_not_found_reason.status_issue') }}
            <br />
            <br />
            <a class="btn btn-primary" href="{{ urlWithLng('dashboard') }}">{{trans('swis.404.page.back.button')}}</a>
        </div>
    </div>

    @include('layouts.main-scripts')
</body>
</html>
