<!DOCTYPE html>
<head>
    @include('layouts.head')
</head>
<body class="gray-bg">

<div class="middle-box text-center animated fadeInDown" style="max-width: 600px;line-height: 32px;">
    <h2 class="font-bold">{{ trans('swis.global.saf.not_found.message') }}</h2>

    <div class="error-desc">
        <hr />
        <a class="btn btn-primary" href="{{ urlWithLng('dashboard') }}">{{trans('swis.home.url')}}</a>
    </div>
</div>

</body>
</html>
