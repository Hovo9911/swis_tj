<!DOCTYPE html>
<head>
    @include('layouts.head')
</head>
<body class="gray-bg">
<div class="middle-box text-center animated fadeInDown">
    <div class="error-desc">
        <h3>
            @if($exception->getMessage())
                {{$exception->getMessage()}}
            @else
                {{trans('swis.maintenance_mode.message')}}
            @endif
        </h3>
    </div>
</div>
@include('layouts.main-scripts')
</body>
</html>