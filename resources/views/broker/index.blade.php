@extends('layouts.app')

@section('title'){{trans('swis.broker.title')}}@stop

@section('contentTitle') {{trans('swis.broker.title')}} @stop

@section('form-buttons-top')
    {!! renderAddButton() !!}
@stop

@section('content')

    <div class="clearfix search__out">
        <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse" data-target="#collapseSearch" aria-expanded="false" aria-controls="collapseSearch">
            <i class="fa fa-search"> </i><span class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
        </button>
    </div>

    <div class="collapse" id="collapseSearch">
        <div class="card card-body">
            <form class="form-horizontal fill-up" id="search-filter" autocomplete="off">
                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">ID</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="number" class="form-control onlyNumbers" min="0" name="id" id="" value="">
                                <div class="form-error" id="form-error-id"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.tax_id')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="tax_id" id="" value="">
                                <div class="form-error" id="form-error-tax_id"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.base.name')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="name" id="" value="">
                                <div class="form-error" id="form-error-name"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.broker.legal_entity_name')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="legal_entity_name" id="" value="">
                                <div class="form-error" id="form-error-legal_entity_name"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.broker.certification.number')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="certification_number" id="" value="">
                                <div class="form-error" id="form-error-certification_number"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.broker.label.address')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="address" id="" value="">
                                <div class="form-error" id="form-error-address"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.show_status')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <select class="form-control select2 default-search" name="show_status">
                                    <option value="{{\App\Models\BaseModel::STATUS_ACTIVE}}">{{trans('core.base.label.status.active')}}</option>
                                    <option value="{{\App\Models\BaseModel::STATUS_INACTIVE}}">{{trans('core.base.label.status.inactive')}}</option>
                                </select>
                                <div class="form-error" id="form-error-show_status"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.broker.certification.period_of_validity')}}</label>

                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-5">
                                <div class='input-group date'>
                                    <span class="input-group-addon"><span
                                                class="glyphicon glyphicon-calendar"></span></span>
                                    <input value="" name="certification_period_validity_date_start" autocomplete="off" type="text"
                                           placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                </div>
                                <div class="form-error" id="form-error-certification_period_validity_date_start"></div>
                            </div>

                            <div class="col-lg-5">
                                <div class='input-group date'>
                                    <span class="input-group-addon"><span
                                                class="glyphicon glyphicon-calendar"></span></span>
                                    <input value="" name="certification_period_validity_date_end" autocomplete="off" type="text"
                                           placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                </div>
                                <div class="form-error" id="form-error-certification_period_validity_date_end"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8 col-lg-offset-3 text-right">
                        <div class="row">
                            <div class="col-lg-10 col-md-8">
                                <button type="submit" class="btn btn-primary mr-10">{{trans('core.base.label.search')}}</button>
                                <button type="button" class="btn btn-danger resetButton" data-submit="true">{{trans('core.base.label.reset')}}</button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>

    <div class="data-table">
        <div class="clearfix">
            <table class="table table-striped table-bordered table-hover dataTables-example" id="list-data">
                <thead>
                <tr>
                    <th data-delete="0" data-col="actions" class="th-actions">{{trans('core.base.label.actions')}}</th>
                    <th data-col="id">ID</th>
                    <th data-col="tax_id">{{trans('swis.tax_id')}}</th>
                    <th data-col="name">{{trans('swis.base.name')}}</th>
                    <th data-col="legal_entity_name">{{trans('swis.broker.legal_entity_name')}}</th>
                    <th data-col="certification_number">{{trans('swis.broker.certification.number')}}</th>
                    <th data-col="certification_period_validity">{{trans('swis.broker.certification.period_of_validity')}}</th>
                    <th data-col="show_status">{{trans('core.base.label.status')}}</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/broker/main.js')}}"></script>
@endsection