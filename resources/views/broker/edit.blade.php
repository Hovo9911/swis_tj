@extends('layouts.app')

@section('title'){{trans('swis.broker.create.title')}}@stop

@section('contentTitle')
    {{trans('swis.broker.create.title')}}
@stop

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('content')
    <?php

    switch ($saveMode) {
        case 'add':
            $url = 'broker';
            break;
        default:
            $url = $saveMode != 'view' ? 'broker/update/' . $broker->id: '';

            $mls = $broker->ml()->notDeleted()->base(['lng_id', 'name', 'description', 'show_status'])->keyBy('lng_id');

            break;
    }

    $languages = activeLanguages();

    ?>

    <form class="form-horizontal fill-up " id="form-data" action="{{urlWithLng($url)}}">

        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-general"> {{trans('core.base.tab.general')}}</a></li>
                @foreach($languages as $language)
                    <li>
                        <a data-toggle="tab" href="#tab-{{$language->code}}">{{ trans("core.base.language.".strtolower($language->name)) }}</a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content">
                <div id="tab-general" class="tab-pane active">
                    <div class="panel-body">

                        <div class="form-group required"><label
                                    class="col-lg-2 control-label">{{trans('swis.tax_id')}}</label>
                            <div class="col-lg-10">
                                <input value="{{$broker->tax_id or ''}}"
                                       {{($saveMode == 'edit') ? 'readonly' : ''}} id="taxID" name="tax_id" type="text"
                                       placeholder=""
                                       class="form-control get-info-field">
                                <div class="form-error " id="form-error-tax_id"></div>
                            </div>
                        </div>

                        <div class="form-group"><label
                                    class="col-lg-2 control-label">{{trans('swis.broker.certification.number')}}</label>
                            <div class="col-lg-10">
                                <input value="{{$broker->certification_number or ''}}" name="certification_number"
                                       type="text"
                                       placeholder="" class="form-control">
                                <div class="form-error" id="form-error-certification_number"></div>
                            </div>
                        </div>

                        <div class="form-group required"><label
                                    class="col-lg-2 control-label">{{trans('swis.broker.certification.period_of_validity')}}</label>
                            <div class="col-lg-10">
                                <div class='input-group date'>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                <input value="{{$broker->certification_period_validity or ''}}" data-start-date="{{currentDate()}}"
                                       name="certification_period_validity" type="text" autocomplete="off"
                                       placeholder="{{setDatePlaceholder()}}" class="form-control">
                                </div>
                                <div class="form-error" id="form-error-certification_period_validity"></div>
                            </div>
                        </div>

                        <div class="form-group required"><label
                                    class="col-lg-2 control-label">{{trans('swis.broker.legal_entity_name')}}</label>
                            <div class="col-lg-10">
                                <input value="{{$broker->legal_entity_name or ''}}" id="legalEntityName" readonly
                                       name="legal_entity_name" type="text" placeholder=""
                                       class="form-control">
                                <div class="form-error" id="form-error-legal_entity_name"></div>
                            </div>
                        </div>

                        <div class="form-group"><label class="col-lg-2 control-label"
                                                       readonly="">{{trans('swis.broker.label.address')}}</label>
                            <div class="col-lg-10">
                                <input value="{{$broker->address or ''}}" readonly name="address" type="text"
                                       placeholder="" class="form-control" id="address">
                                <div class="form-error" id="form-error-address"></div>
                            </div>
                        </div>

                        <div class="form-group required"><label
                                    class="col-lg-2 control-label">{{trans('core.base.label.phone')}}</label>
                            <div class="col-lg-10">
                                <div class="input-group">
                                    <span class="input-group-addon">+</span>
                                    <input type="text"
                                           placeholder=""
                                           class="form-control onlyNumbers"
                                           name="phone_number"
                                           value="{{$broker->phone_number or ''}}"
                                           autocomplete="off">
                                </div>
                                <div class="form-error" id="form-error-phone_number"></div>
                            </div>
                        </div>

                        <div class="form-group required"><label
                                    class="col-lg-2 control-label">{{trans('core.base.label.email')}}</label>
                            <div class="col-lg-10">
                                <input value="{{$broker->email or ''}}" name="email" type="text" placeholder=""
                                       class="form-control">
                                <div class="form-error" id="form-error-email"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">{{trans('core.base.label.status')}}</label>
                            <div class="col-lg-10 general-status">
                                <select name="show_status" class="form-show-status">
                                    <option value="{{\App\Models\Broker\Broker::STATUS_ACTIVE}}"{{(isset($broker) && $broker->show_status == \App\Models\Broker\broker::STATUS_ACTIVE) || $saveMode == 'add' ?  ' selected="selected"' : ''}}>{{trans('core.base.label.status.active')}}</option>
                                    <option value="{{\App\Models\Broker\Broker::STATUS_INACTIVE}}"{{isset($broker) &&  $broker->show_status == \App\Models\Broker\broker::STATUS_INACTIVE ? ' selected="selected"': ''}}>{{trans('core.base.label.status.inactive')}}</option>
                                </select>
                                <div class="form-error" id="form-error-show_status"></div>
                            </div>
                        </div>

                    </div>
                </div>
                @foreach($languages as $language)
                    <?php $lngId = $language->id ?>
                    <div id="tab-{{$language->code}}" class="tab-pane">
                        <div class="panel-body">
                            <div class="form-group required"><label
                                        class="col-lg-2 control-label">{{trans('swis.broker.name.label')}}</label>
                                <div class="col-lg-10">
                                    <input value="{{isset($mls[$lngId]) ? $mls[$lngId]->name : ''}}"
                                           required name="ml[{{$language->id}}][name]" type="text" placeholder=""
                                           class="form-control">
                                    <div class="form-error" id="form-error-ml-{{$language->id}}-name"></div>
                                </div>
                            </div>

                            <div class="form-group"><label
                                        class="col-lg-2 control-label">{{trans('core.base.description')}}</label>
                                <div class="col-lg-10">
                                    <textarea rows="6" name="ml[{{$language->id}}][description]"
                                              class="form-control">{{isset($mls[$lngId]) ? $mls[$lngId]->description : ''}}</textarea>
                                    <div class="form-error" id="form-error-ml-{{$language->id}}-description"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <input type="hidden" value="{{$broker->id or 0}}" name="id" class="mc-form-key"/>

        {!! csrf_field() !!}
    </form>

@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script src="{{asset('js/broker/main.js')}}"></script>
@endsection