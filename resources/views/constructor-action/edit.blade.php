@extends('layouts.app')

@section('title'){{trans('swis.constructor_action.action_title')}}@stop

@section('contentTitle') {{trans('swis.constructor_action.action_title')}} @stop

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('styles')
    <link rel="stylesheet" href="{{asset('assets/helix/core/plugins/iCheck/custom.css')}}">
@stop

@section('topScripts')
    <script>
        var constructorDocumentId  = "{{$selectedConstructorDocumentId or $constructorAction->constructor_document_id}}";
    </script>
@endsection

@section('content')
    <?php

    switch ($saveMode) {
        case 'add':
            $url = 'constructor-actions';
            break;
        default:
            $url = 'constructor-actions/update/' . $constructorAction->id;

            $mls = $constructorAction->ml()->notDeleted()->base(['lng_id', 'action_name', 'description', 'show_status'])->keyBy('lng_id');
            break;
    }

    $languages = activeLanguages();
    ?>

    <form class="form-horizontal fill-up " id="form-data" action="{{urlWithLng($url)}}" autocomplete="off">

        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-general"> {{trans('core.base.tab.general')}}</a></li>
                @foreach($languages as $language)
                    <li class=""><a data-toggle="tab" href="#tab-{{$language->code}}">{{ trans("core.base.language.".strtolower($language->name)) }}</a></li>
                @endforeach
            </ul>
            <div class="tab-content">
                <div id="tab-general" class="tab-pane active">
                    <div class="panel-body">

                        <div class="form-group required"><label
                                    class="col-lg-2 control-label">{{ trans('swis.document.create.title') }}</label>
                            <div class="col-lg-10">
                                <select class="form-control select2" name="constructor_document_id">
                                    <option></option>
                                    @foreach($constructorDocuments as $constructorDocument)
                                        <option {{ (isset($constructorAction) && $constructorAction->constructor_document_id == $constructorDocument->id) || (isset($selectedConstructorDocumentId) && $selectedConstructorDocumentId == $constructorDocument->id) ? 'selected': '' }} value="{{$constructorDocument->id}}">{{$constructorDocument->current()->document_name}}</option>
                                    @endforeach
                                </select>
                                <div class="form-error" id="form-error-constructor_document_id"></div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-lg-2 control-label">{{ trans('swis.constructor.actions.special_types') }}</label>
                            <div class="col-lg-10">
                                <select class="form-control select2" name="special_type" id="special-types-select">
                                    @foreach($specialTypes as $key => $type)
                                        <option {{(isset($constructorAction) && $constructorAction->special_type == $key) ? 'selected': '' }} value="{{$key}}">{{$type}}</option>
                                    @endforeach
                                </select>
                                <div class="form-error" id="form-error-special_type"></div>
                            </div>
                        </div>

                        <div class="{{ (isset($constructorAction) && $constructorAction->special_type == 2) ? '' : 'hide' }}" id="templateSection">

                            <div class="form-group">
                                <label class="col-lg-2 control-label">{{ trans('swis.constructor.actions.choose_template') }}</label>

                                <div class="col-lg-10">
                                    <div class="i-checks col-md-2">
                                        <label>
                                            <input type="checkbox" class="template-checkbox" name="print_default" value="1" {{ (isset($constructorAction) && $constructorAction->print_default) ? 'checked' : '' }}> <i></i> {{ trans('swis.constructor.actions.choose_default_template') }}
                                        </label>
                                    </div>
                                    @if ($constructorTemplates->count())
                                        @foreach ($constructorTemplates as $template)
                                            <div class="i-checks col-md-2">
                                                <label>
                                                    <input type="checkbox" class="template-checkbox" name="templates[]" value="{{ $template['id'] }}" {{ (isset($constructorAction) && in_array($template->id, $checked)) ? 'checked' : '' }}> <i></i> {{ $template->currentMl->template_name }}
                                                </label>
                                            </div>
                                        @endforeach
                                    @else
                                    {{--    <div class="col-lg-10">
                                            {{ trans('swis.constructor_template_not_available') }}
                                        </div>--}}
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="i-checks">
                                    <label class="col-lg-2 control-label">
                                        {{ trans('swis.constructor.actions.select_all_products') }}
                                    </label>
                                    <div class="col-lg-10">
                                        <input type="checkbox" class="template-checkbox" name="select_all_products" value="1" {{ (isset($constructorAction) && $constructorAction->select_all_products ) ? 'checked' : '' }}> <i></i>
                                    </div>
                                </div>
                            </div>

                        </div>

                        @if($saveMode == 'edit')
                            <div class="form-group "><label class="col-lg-2 control-label">{{ trans('swis.constructor.actions.action_id') }}</label>
                                <div class="col-lg-10">
                                    <input value="{{$constructorAction->action_id or ''}}" disabled="" type="text" placeholder="" class="form-control">
                                </div>
                            </div>
                        @endif

                        <div class="form-group"><label class="col-lg-2 control-label">{{ trans('swis.constructor.actions.order') }}</label>
                            <div class="col-lg-10">
                                <input value="{{$constructorAction->order or ''}}" name="order" type="number" class="form-control">

                                <div class="form-error" id="form-error-order"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">{{trans('core.base.label.status')}}</label>
                            <div class="col-lg-10 general-status">
                                <select name="show_status" class="form-show-status ">
                                    <option value="{{\App\Models\ConstructorActions\ConstructorActions::STATUS_ACTIVE}}"{{(isset($constructorAction) && $constructorAction->show_status == \App\Models\ConstructorActions\ConstructorActions::STATUS_ACTIVE) || $saveMode == 'add' ?  ' selected="selected"' : ''}}>{{trans('core.base.label.status.active')}}</option>
                                    <option value="{{\App\Models\ConstructorActions\ConstructorActions::STATUS_INACTIVE}}"{{isset($constructorAction) &&  $constructorAction->show_status == \App\Models\ConstructorActions\ConstructorActions::STATUS_INACTIVE ? ' selected="selected"': ''}}>{{trans('core.base.label.status.inactive')}}</option>
                                </select>
                                <div class="form-error" id="form-error-show_status"></div>
                            </div>
                        </div>

                    </div>

                </div>
                @foreach($languages as $language)
                    <?php $lngId = $language->id ?>
                    <div id="tab-{{$language->code}}" class="tab-pane">
                        <div class="panel-body">
                            <div class="form-group required"><label
                                        class="col-lg-2 control-label">{{ trans('swis.constructor.actions.action_name') }}</label>
                                <div class="col-lg-10">
                                    <input value="{{isset($mls[$lngId]) ? $mls[$lngId]->action_name : ''}}"
                                           name="ml[{{$language->id}}][action_name]" type="text"
                                           placeholder=""
                                           class="form-control">

                                    <div class="form-error"
                                         id="form-error-ml-{{$language->id}}-action_name"></div>
                                </div>
                            </div>

                            <div class="form-group"><label
                                        class="col-lg-2 control-label">{{trans('core.base.description')}}</label>
                                <div class="col-lg-10">
                                    <textarea rows="6" name="ml[{{$language->id}}][description]"
                                              class="form-control">{{isset($mls[$lngId]) ? $mls[$lngId]->description : ''}}</textarea>
                                    <div class="form-error"
                                         id="form-error-ml-{{$language->id}}-description"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <input type="hidden" value="{{$constructorAction->id or 0}}" name="id" class="mc-form-key"/>

            {!! csrf_field() !!}
        </div>
    </form>

@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script src="{{asset('assets/helix/core/plugins/iCheck/icheck.min.js')}}"></script>
    <script src="{{asset('js/constructor-action/main.js')}}"></script>
@endsection