<?php

use App\Models\Role\Role;
use App\Models\User\User;

$authUserAllRoles = \Auth::user()->roles();
$authUserCurrentRole = \Auth::user()->getCurrentUserRole();
$companyNames = $authUserAllRoles->companyNames;
$agencyNames = $authUserAllRoles->agencyNames;

?>

<select name="user_role" data-select2-allow-clear="false" class="select2 select--theme login-select__field ver-top-box {{!empty($class) ? $class : ''}}" id="userRoles">

    {{-- Natural Person Roles --}}
    @foreach($authUserAllRoles->default->natural_persons as $userRole)

        @php
            if($userRole->authorized_by_id){
                 $roleName = trans('swis.role.login_as_from_user') . ' ' .'('.User::getUserNameByID($userRole->authorized_by_id).')';
            }else{
                $roleName = trans('swis.role.login_as_natural_person');
            }

            $roleValue = $userRole->authorized_by_id ?? 0;
        @endphp

        <option {{($authUserCurrentRole &&  $authUserCurrentRole->role_type == Role::ROLE_TYPE_NATURAL_PERSON && $authUserCurrentRole->role == Role::ROLE_LEVEL_TYPE_DEFAULT && $authUserCurrentRole->authorized_by_id == $roleValue) ? 'selected' : ''}} title="{{$roleName}}" data-role="{{Role::ROLE_LEVEL_TYPE_DEFAULT}}"
                data-role-type="{{Role::ROLE_TYPE_NATURAL_PERSON}}"
                value="{{$roleValue}}">{{$roleName}}</option>
    @endforeach


    {{-- Sw Admin Roles --}}
    @foreach($authUserAllRoles->default->sw_admin as $userRole)

        @php
            $roleName = trans('swis.role.login_as_sw_admin');

            $roleValue = $userRole->authorized_by_id ?? 0;
        @endphp

        <option
                {{($authUserCurrentRole && $authUserCurrentRole->role_type == Role::ROLE_TYPE_SW_ADMIN &&  $authUserCurrentRole->role == Role::ROLE_LEVEL_TYPE_DEFAULT && $authUserCurrentRole->authorized_by_id == $roleValue) ? 'selected' : ''}}
                title="{{$roleName}}" data-role="{{Role::ROLE_LEVEL_TYPE_DEFAULT}}"
                data-role-type="{{Role::ROLE_TYPE_SW_ADMIN}}"
                value="{{$roleValue}}">{{$roleName}}</option>
    @endforeach


    {{-- Default Company Roles --}}
    @foreach($authUserAllRoles->default->company as $userRole)

        @php
        $companyName = '';
        if(!is_null($userRole->company_id) && isset($companyNames[$userRole->company_id])){

            if(isset($agencyNames[$companyNames[$userRole->company_id]['tax_id']])){
                $companyName = $agencyNames[$companyNames[$userRole->company_id]['tax_id']]['name'];
            }else{
               $companyName = $companyNames[$userRole->company_id]['name'];
            }
        }

         $roleName = trans('swis.role.login_as_head') .  ' ('.$companyName.')'
        @endphp

        <option
                {{($authUserCurrentRole && $authUserCurrentRole->role == Role::ROLE_LEVEL_TYPE_DEFAULT && $authUserCurrentRole->company_id == $userRole->company_id) ? 'selected' : ''}}
                title="{{$roleName}}" data-role="{{Role::ROLE_LEVEL_TYPE_DEFAULT}}"
                data-role-type="{{Role::ROLE_TYPE_COMPANY}}"
                value="{{$userRole->company_id}}">{{$roleName}}</option>
    @endforeach


    {{-- Authorized Company Roles --}}
    @foreach($authUserAllRoles->custom->company as $userRole)

        @php
        $roleName = '';
        if(!is_null($userRole->company_id) && isset($companyNames[$userRole->company_id])){
              if(isset($agencyNames[$companyNames[$userRole->company_id]['tax_id']])){
                  $roleName = $agencyNames[$companyNames[$userRole->company_id]['tax_id']]['name'];
              }else{
                  $roleName = $companyNames[$userRole->company_id]['name'];
              }
        }

        @endphp

        <option
                {{($authUserCurrentRole && $authUserCurrentRole->role == Role::ROLE_LEVEL_TYPE_CUSTOM && $authUserCurrentRole->company_id == $userRole->company_id) ? 'selected' : ''}}
                title="{{$roleName}}" data-role="{{Role::ROLE_LEVEL_TYPE_CUSTOM}}"
                data-role-type="{{Role::ROLE_TYPE_COMPANY}}"
                value="{{$userRole->company_id}}">{{$roleName}}</option>
    @endforeach


    {{-- Custom Portal Roles --}}
    @foreach($authUserAllRoles->default->customs_portal as $userRole)
        @php
            $roleName = trans('swis.role.login_as_customs_portal');
        @endphp
        <option {{($authUserCurrentRole &&  $authUserCurrentRole->role_type == Role::ROLE_TYPE_CUSTOMS_OPERATOR) ? 'selected' : ''}} title="{{$roleName}}" data-role="{{Role::ROLE_LEVEL_TYPE_DEFAULT}}"
                data-role-type="{{Role::ROLE_TYPE_CUSTOMS_OPERATOR}}"
                value="0">{{$roleName}}</option>
    @endforeach

</select>


<input type="hidden" name="role" id="currentRole" value="{{Role::ROLE_LEVEL_TYPE_DEFAULT}}">
<input type="hidden" name="role_type" id="currentRoleType" value="{{Role::ROLE_TYPE_NATURAL_PERSON}}">

