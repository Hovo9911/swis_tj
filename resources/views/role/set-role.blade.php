<!DOCTYPE html>
<head>
    @include('layouts.head')
    <!-- TITLE -->
    <title>{{trans('swis.core.single_window.title')}}</title>

    <style>
        .select2-container--default .select2-selection--single{
            border-radius: 0;
            height: 31px;
        }
    </style>
</head>
<?php
$theme = config('swis.theme_type');
?>
<body class="main--page role-set--page wrapper--materialDesign {{ $theme }}">
<div class="select-role">

    @include('layouts.header')

    <div class="col-md-12" style="margin-top: 70px">
        @include('partials.alerts')
    </div>
    <section class="account__block dn">
        <h2 class="account__title fs20 text-uppercase text-center fb">{{trans('swis.select_role.page_title')}}</h2>
        <p class="fs14 account__txt">{{trans('swis.select_role.page_text')}}</p>

        <form action="{{urlWithLng('role/set')}}" method="post">
            {{csrf_field()}}
            <div class="form-box"> <!-- add class form--error -->
                @include('role.roles-list')
            </div>

            <button type="submit" class="text-uppercase btn btn--green full--width fs14 arian-amu-bold btn--access">
                <i class="fa"></i> {{trans('swis.select_role.submit')}}
            </button> <!--  btn--loading -->
        </form>
    </section>
</div>

@include('layouts.main-scripts')
<script src="{{asset('js/role/set-role.js')}}"></script>

</body>
</html>