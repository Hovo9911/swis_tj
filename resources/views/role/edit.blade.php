@extends('layouts.app')

@section('title'){{trans('swis.role.create.title')}}@stop

@section('contentTitle')
    {{trans('swis.role.create.title')}}
@stop

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('content')

    <?php
    switch ($saveMode) {
        case 'add':
            $url = 'role';
            break;
        default :
            $url = $saveMode != 'view' ? 'role/update/' . $role->id : '';

            $mls = $role->ml()->notDeleted()->base(['lng_id', 'name' , 'description'])->keyBy('lng_id');

            break;
    }

    $languages = activeLanguages();
    ?>

    <form class="form-horizontal fill-up " id="form-data" action="{{urlWithLng($url)}}">

        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-general"> {{trans('core.base.tab.general')}}</a></li>
                @foreach($languages as $language)
                    <li>
                        <a data-toggle="tab" href="#tab-{{$language->code}}">{{ trans("core.base.language.".strtolower($language->name)) }}</a>
                    </li>
                @endforeach
            </ul>

            <div class="tab-content">
                <div id="tab-general" class="tab-pane active">
                    <div class="panel-body">
                        <div class="form-group required">
                            <label class="col-lg-2 control-label label__title">{{trans('swis.module_title')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-lg-10 col-md-8 field__one-col">
                                        @if(!isset($role))
                                            <select class="form-control select2" name="menu_id" {{($saveMode == 'edit') ? 'readonly':''}}>
                                                <option value="">{{trans('swis.role.select_module')}}</option>
                                                @foreach($menuModules as $menu)
                                                    @if($menu->id == \App\Models\Menu\Menu::AUTHORIZE_PERSON_MENU_ID)
                                                        @continue
                                                    @endif
                                                    <option value="{{$menu->id}}">{{trans($menu->title)}}</option>
                                                @endforeach
                                            </select>
                                            <div class="form-error" id="form-error-menu_id"></div>
                                        @else
                                            <input value="{{$menuName or ''}}" disabled type="text" placeholder="" class="form-control">
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2 col-lg-offset-2">
                                <label class="checkbox-inline">
                                    <input name="create_permission" {{isset($role) ? ($role->create_permission) ? 'checked' : '' : 'checked'}} type="checkbox" value="1">{{trans('swis.role.permission.create')}}
                                </label>
                           {{--     <label class="checkbox-inline">
                                    <input name="read_permission" {{isset($role) ? ($role->read_permission) ? 'checked' : '' : 'checked'}} type="checkbox" value="1">{{trans('swis.role.permission.read')}}
                                </label>--}}
                                <label class="checkbox-inline">
                                    <input name="update_permission" {{isset($role) ? ($role->update_permission) ? 'checked' : '' : 'checked'}} type="checkbox" value="1">{{trans('swis.role.permission.update')}}
                                </label>
                                <label class="checkbox-inline">
                                    <input name="delete_permission" {{isset($role) ? ($role->delete_permission) ? 'checked' : '' : 'checked'}} type="checkbox" value="1">{{trans('swis.role.permission.delete')}}
                                </label>
                            </div>
                        </div>

                        @if($saveMode != 'add')
                            <div class="form-group ">
                                <label class="col-lg-2 control-label label__title">{{trans('core.base.created_at')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input value="{{$role->created_at}}" disabled type="text" placeholder="" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="form-group">
                            <label class="col-lg-2 control-label label__title">{{trans('core.base.label.status')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-lg-10 col-md-8 field__one-col general-status">
                                        <select name="show_status" class="form-show-status">
                                            <option value="{{\App\Models\Role\Role::STATUS_ACTIVE}}"{{(!empty($role) && $role->show_status == App\Models\Role\Role::STATUS_ACTIVE) || $saveMode == 'add' ?  ' selected="selected"' : ''}}>{{trans('core.base.label.status.active')}}</option>
                                            <option value="{{\App\Models\Role\Role::STATUS_INACTIVE}}"{{!empty($role) &&  $role->show_status ==  App\Models\Role\Role::STATUS_INACTIVE ? ' selected="selected"': ''}}>{{trans('core.base.label.status.inactive')}}</option>
                                        </select>
                                        <div class="form-error" id="form-error-show_status"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @foreach($languages as $language)
                    <?php $lngId = $language->id ?>
                    <div id="tab-{{$language->code}}" class="tab-pane">
                        <div class="panel-body">

                            <div class="form-group required"><label class="col-lg-2 control-label">{{trans('core.base.label.name')}}</label>
                                <div class="col-lg-10">
                                    <input value="{{isset($mls[$lngId]) ? $mls[$lngId]->name : ''}}" name="ml[{{$language->id}}][name]" type="text" placeholder="" class="form-control">
                                    <div class="form-error" id="form-error-ml-{{$language->id}}-name"></div>
                                </div>
                            </div>

                            <div class="form-group"><label class="col-lg-2 control-label">{{trans('core.base.description')}}</label>
                                <div class="col-lg-10">
                                    <textarea rows="6" name="ml[{{$language->id}}][description]" class="form-control">{{isset($mls[$lngId]) ? $mls[$lngId]->description : ''}}</textarea>
                                    <div class="form-error" id="form-error-ml-{{$language->id}}-description"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <input type="hidden" value="{{$role->id or 0}}" name="id" class="mc-form-key"/>

        {!! csrf_field() !!}
    </form>

@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script src="{{asset('js/role/main.js')}}"></script>
@endsection