@if(count(Auth::user()->roles()['default']) != 1 || count(Auth::user()->roles()['company']) != 0 || count(Auth::user()->roles()['naturalPerson']) != 0)
    <?php
    $authUserCurrentRoleType = Auth::user()->getCurrentRole('role_type');
    $authUserAllRoles = Auth::user()->roles();

    ?>

    <select name="role" class="{{!empty($class) ? $class : 'form-control'}}" id="userRoles">

        {{-- DEFAULT ROLES AND SW ADMIN ROLES --}}
        @foreach($authUserAllRoles['default'] as $userRole)

            @php $defaultRole =  $userRole->name . ' ' .(!is_null($userRole->pivot->company_id) ? '( '.getCompanyNameByID($userRole->pivot->company_id).' )' : '') @endphp

            <option title="{{$defaultRole}}" data-type="{{\App\Models\Role\Role::ROLE_TYPE_ROLE}}"
                    value="{{$userRole->pivot->id}}" {{( $authUserCurrentRoleType != \App\Models\Role\Role::ROLE_TYPE_AGENCY && $userRole->pivot->id == \Auth::user()->getCurrentRole('id')) ? 'selected' : ''}}>{{$defaultRole}}</option>
        @endforeach

        {{-- AGENCY AND COMPANY ROLES --}}
        @foreach($authUserAllRoles['company'] as $userRole)

            @if(!is_null($userRole->agency))
                <option title="{{$userRole->company->name}}" data-type="{{\App\Models\Role\Role::ROLE_TYPE_AGENCY}}"
                        data-agency="{{$userRole->company_tax_id}}"
                        {{( $authUserCurrentRoleType == \App\Models\Role\Role::ROLE_TYPE_AGENCY && (\Auth::user()->companyTaxId() == $userRole->company_tax_id)) ? 'selected' : ''}} value="{{$userRole->company_tax_id}}">{{$userRole->company->name}}
                </option>
            @endif

            @if(!is_null($userRole->company) && is_null($userRole->agency))
                <option title="{{$userRole->company->name}}" data-type="{{\App\Models\Role\Role::ROLE_TYPE_COMPANY}}"
                        data-company="{{$userRole->company_tax_id}}"
                        {{($authUserCurrentRoleType == \App\Models\Role\Role::ROLE_TYPE_COMPANY && (\Auth::user()->getCurrentRole('company_tax_id') == $userRole->company_tax_id)) ? 'selected' : ''}} value="{{$userRole->company_tax_id}}">{{$userRole->company->name}}</option>
            @endif

        @endforeach

        {{-- NATURAL PERSONS ROLES --}}
        @foreach($authUserAllRoles['naturalPerson'] as $userId => $userRole)

            @php $fromNaturalPersonRole = trans('swis.role.login_as_from_user') . ' ' .'('.\App\Models\User\User::getUserNameByID($userId).')' @endphp

            <option data-type="{{\App\Models\Role\Role::ROLE_TYPE_NATURAL_PERSON}}" title="{{$fromNaturalPersonRole}}"
                    value="{{$userRole['user_role_id']}}" {{( $userRole['user_role_id'] == \Auth::user()->getCurrentRole('id')) ? 'selected' : ''}}>{{$fromNaturalPersonRole}}</option>
        @endforeach
    </select>

    <input type="hidden" name="role_type" id="current_role_type" value="role">
@endif