<header class="header">
    <div class="header--fixed clearfix closeRefreshGroup">
        <div class="pull-left header__left">

            <div class="home-icon__out ver-top-box">
                <a class="icon--home text-uppercase fb" href="{{urlWithLng('')}}">
                    <i class="fa fa-home"></i>{{trans('swis.home.url')}}
                </a>
            </div>

        <?php
            $languages = activeLanguages();
            $lngManager = \App\Http\Controllers\Core\Language\LanguageManager::getInstance();
            $currentLanguage = currentLanguage();

            ?>
            <div class="languages pr select-none ver-top-box">

                <div title="{{trans('core.base.language.'.strtolower($currentLanguage->name))}}"  class="languages__title pr">
                    <span class="ver-top-box languages__img">
                        <img src="{{asset('image/'.$currentLanguage->icon)}}" alt="">
                    </span>
                    <span class="ver-top-box languages__txt fb">{{trans('core.base.language.'.$currentLanguage->code,[],$currentLanguage->code)}}</span>
                </div>
                <div class="languages__list dn">
                    @foreach($languages as $language)
                        @if($currentLanguage->id == $language->id) @continue @endif
                        <a title="{{trans('core.base.language.'.strtolower($language->name))}}" href="{{$lngManager->getUrl($language)}}" class="languages__link db">
                            <span class="ver-top-box languages__img">
                                <img src="{{asset('image/'.$language->icon)}}" alt="">
                            </span>
                            <span class="ver-top-box languages__txt">{{trans('core.base.language.'.$language->code,[],$language->code)}}</span>
                        </a>
                    @endforeach
                </div>
            </div>

        </div>

        <div class="pull-right header__right">

            <div class="ver-top-box header__notificaion-profile">

                @auth
                    <div class="dropdown profile-element ver-top-box header-profile">
                        <a data-toggle="dropdown" class="dropdown-toggle ver-top-box header-profile__name" href="#"
                           aria-expanded="true">
                    <span class="header-profile__nameTxt three-dot__oneLine db"
                          title="{{Auth::user()->name()}}">{{Auth::user()->name()}}</span>
                            <b class="header-profile__arrow caret"></b>
                            <i class="fa fa-user header-profile__user" aria-hidden="true"></i>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li>
                                <a href="#"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <i class="fa fa-sign-out-alt"></i> {{trans('swis.core.logout')}}
                                </a>
                                <form id="logout-form" action="{{ urlWithLng('/logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </div>
                @endauth
            </div>
        </div>
    </div>
</header>