<!DOCTYPE html>
<head>
    @include('layouts.head')
</head>
<body class="gray-bg">
<?php

$jsTrans->addTrans([
    'core.loading.saved',
]);
?>

@include('partials.alerts')
<div class="row border-bottom">
    <nav class="navbar navbar-static-top " role="navigation" style="margin-bottom: 0">
        @include('partials.languages-list')
    </nav>
</div>

<div class="middle-box text-center  animated fadeInDown">

    <div>
        @include('errors.errors')

        <h2>Verify your Phone number</h2>
        <br/><br/>
        <div class="tab-content">
            <div id="phoneNumber" class="tab-pane fade {{(empty($userActivationPhoneNumber) ? 'in active' : '')}}">

                <div class="main-content">
                    <form class="m-t" id="form-data" method="post" role="form" action="{{ routeWithLng('phone-verify') }}">

                        {{csrf_field()}}
                        <div class="form-error" id="form-error-credentials"></div>

                        <div class="form-group">
                            <label>Phone number</label>
                            <input type="text" name="phone_number" class="form-control" placeholder="+374 " required="">
                            <div class="form-error" id="form-error-phone_number"></div>

                        </div>

                        <button type="button" class="btn btn-primary block full-width m-b mc-nav-button-save">Verify
                        </button>
                        <hr />
                        <a href="{{urlWithLng('dashboard')}}" class="btn btn-success pull-right">Skip</a>
                        @if(!empty($userActivationPhoneNumber))
                            <br />

                            <hr />
                        <a data-toggle="tab" href="#phoneVerify">Send Again</a>
                        @endif
                    </form>
                </div>
            </div>
            @if(!empty($userActivationPhoneNumber))
                <div id="phoneVerify" class="tab-pane fade in active">
                    <div class="main-content">
                        <form class="m-t" id="form-data-number" data-save-btn="mc-nav-button-save-number" method="post" role="form" action="{{ routeWithLng('phone-verify-code') }}">
                    <div class="form-group">
                        <label>Activation Code</label>
                        <input type="text" name="activation_code" class="form-control" placeholder="468713" required="">
                        <div class="form-error" id="form-error-activation_code"></div>
                    </div>

                    <div class="text-right">
                        <span class="email-token">( {{Auth::user()->phone_number}} )</span>
                        <button type="button" id="sendAgain" data-url="{{urlWithLng('phone/verify-again')}}"
                                class="btn btn-success clearfix">Send Again
                        </button>
                    </div>
                    <hr />

                    <button type="button" class="btn btn-primary block full-width m-b mc-nav-button-save-number">Activate</button>

                    <br />
                        <a data-toggle="tab" href="#phoneNumber">Verify new phone number</a>


                        </form>
                    </div>
                </div>

            @endif
        </div>
    </div>
</div>

@include('layouts.main-scripts')
<script src="{{asset('js/auth/verify.js')}}"></script>

</body>
</html>