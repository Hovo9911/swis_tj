<!DOCTYPE html>
<head>
    @include('layouts.head')
</head>
<body class="gray-bg">

<div class="wrapper-out">

    <div class="white-bg">
        @include('layouts.top-navbar')
    </div>

    <div class="reg__block center-block">
        <div class="reg__top">
            <div class="gerb-pic center-block">
                <img src="{{env('LOGIN_PAGE_LOGO_PATH')}}" alt="">
            </div>
            <h2 class="text-uppercase text-center reg__title fb fs20">{{trans('swis.login_page.title')}}</h2>
        </div>

        <div class="col-md-6 col-md-offset-3">
            <form  id="form-data" method="post" role="form" action="{{ urlWithLng('reset') }}" data-save-btn="mc-nav-button-save">
                {{csrf_field()}}
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="{{trans('core.base.label.reset.new_password')}}" required="">
                    <div class="form-error" id="form-error-password"></div>
                </div>
                <div class="form-group">
                    <input type="password" name="password_confirmation" class="form-control" placeholder="{{trans('core.base.label.reset.confirm_password')}}" required="">
                    <div class="form-error" id="form-error-password_confirmation"></div>
                </div>
                <input type="hidden" name="token" value="{{$token}}" />
                <button type="submit" class="btn btn-primary block full-width m-b mc-nav-button-save save--password"><i class="fa"></i>  {{trans('core.base.label.reset.save_password')}}</button>
            </form>
        </div>
    </div>
</div>

<footer class="sticky-footer  center-block">
    <div class="footer__inner">
        <div class="row">
            <div class="col-sm-7 col-md-8">
                <div class="footer__left">
                    <p class="fs13 footer__txt">{{trans('swis.login_page.footer_text')}}</p>
                    <p class="fs13 footer__txt">{{trans('swis.app_last_update')}} {{env('APP_LAST_UPDATE')}} , {{trans('swis.app_version')}} {{env('APP_VERSION')}}</p>
                    <ul class="footer__list list-inline" >
                        <li class="footer__item">
                             <a class="fs11 footer__link" href="">{{trans('swis.login_page.footer_email')}}</a>
                        </li>

                    </ul>
                </div>
            </div>
            <div class="col-sm-5 col-md-4">
                <p class="text-right fs11 copyright__txt">&copy; {{date('Y')}} {{trans('swis.login_page.footer_copyright')}}</p>
            </div>
        </div>
    </div>
</footer>
@include('layouts.main-scripts')

<script src="{{asset('js/auth/main.js')}}"></script>
</body>
</html>