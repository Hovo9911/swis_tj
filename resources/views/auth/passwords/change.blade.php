<!DOCTYPE html>
<head>
    @include('layouts.head')
</head>
<body class="wrapper--materialDesign {{ config('swis.theme_type') }}">

@include('partials.translations',['transType'=>'global'])

@include('auth.layouts.header')

<div class="gray-bg" id="wrapper">
    <div class="row wrapper border-bottom white-bg page-heading custom-wrapper__main">
        <div class="col-sm-6">
            <h2>{{trans('user.change_password.title')}}</h2>
        </div>
    </div>
    <div>
        <div class=" main-content">
            <div class="wrapper wrapper-content animated fadeIn clearfix">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="animated fadeInDown main-content ">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="main-content">
                                        <form class="form-horizontal fill-up" id="form-data" method="post" role="form" action="{{urlWithLng('user-password-change')}}">
                                            {{csrf_field()}}
                                            <div class="form-error" id="form-error-credentials"></div>


                                            <div class="form-group "><label
                                                        class="col-lg-3 control-label">{{trans('core.base.label.old_password')}}</label>
                                                <div class="col-lg-9">
                                                    <input autocomplete="off" name="old_password" type="password" placeholder=""
                                                           class="form-control">
                                                    <div class="form-error" id="form-error-old_password"></div>
                                                </div>
                                            </div>

                                            <div class="form-group "><label
                                                        class="col-lg-3 control-label">{{trans('core.base.label.new_password')}}</label>
                                                <div class="col-lg-9">
                                                    <input autocomplete="off" name="password" type="password" placeholder=""
                                                           class="form-control">
                                                    <div class="form-error" id="form-error-password"></div>
                                                </div>
                                            </div>

                                            <div class="form-group "><label
                                                        class="col-lg-3 control-label">{{trans('core.base.label.confirm_password')}}</label>
                                                <div class="col-lg-9">
                                                    <input autocomplete="off" name="password_confirmation" type="password"
                                                           placeholder=""
                                                           class="form-control">
                                                    <div class="form-error" id="form-error-password_confirmation"></div>
                                                </div>
                                            </div>

                                            <div class="text-right">
                                            <button type="button" class="btn btn-primary mc-nav-button-save change--password"><i class="fa"></i> {{trans('swis.email_verify.verify')}}</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            @include('layouts.footer')

        </div>
    </div>
</div>


@include('layouts.main-scripts')
<script src="{{asset('js/auth/change-password.js')}}"></script>

<script>
    $('.gray-bg').css("min-height", $(window).height() + "px");
</script>

</body>
</html>