<!DOCTYPE html>
<head>
    @include('layouts.head')
</head>
<body class="main--page logout--page gray-bg wrapper--materialDesign  green--theme">

<div class="wrapper-out">

    @include('layouts.header')

    <div class="reg__block center-block">
        <div class="reg__top">
            <div class="gerb-pic center-block">
                <img src="{{env('LOGIN_PAGE_LOGO_PATH')}}" alt="">
            </div>
            <h2 class="text-uppercase text-center reg__title fb fs20">{{trans('swis.login_page.title')}}</h2>
        </div>

        <div class="col-md-6 col-md-offset-3">
            <form  id="form-data" method="post" role="form" action="{{ urlWithLng('reset-password') }}" data-save-btn="mc-nav-button-save">
                {{csrf_field()}}
                <div class="form-group">
                    <input type="text" name="search" class="form-control" placeholder="{{trans('core.base.label.reset.ssn-or-username')}}" required="">
                    <div class="form-error" id="form-error-search"></div>
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b mc-nav-button-save reset--password"><i class="fa"></i> {{trans('swis.core.base.reset_password')}}</button>
            </form>
        </div>
    </div>
</div>

<footer class="sticky-footer  center-block">
    <div class="footer__inner">
        <div class="row">
            <div class="col-sm-7 col-md-8">
                <div class="footer__left">
                    <p class="fs13 footer__txt">{{trans('swis.login_page.footer_text')}}</p>
                    <p class="fs13 footer__txt">{{trans('swis.app_last_update')}} {{env('APP_LAST_UPDATE')}} , {{trans('swis.app_version')}} {{env('APP_VERSION')}}</p>
                    <ul class="footer__list list-inline" >
                        <li class="footer__item">
                             <a class="fs11 footer__link" href="">{{trans('swis.login_page.footer_email')}}</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-5 col-md-4">
                <p class="text-right fs11 copyright__txt">&copy; {{date('Y')}} {{trans('swis.login_page.footer_copyright')}}</p>
            </div>
        </div>
    </div>
</footer>
@include('layouts.main-scripts')

<script src="{{asset('js/auth/main.js')}}"></script>
</body>
</html>