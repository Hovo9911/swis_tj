<!DOCTYPE html>
<head>
    <!-- TITLE -->
    <title>{{trans('swis.core.single_window.title')}}</title>

    <meta name="keywords" content="{{trans('swis.login_page.meta_keywords')}}" />
    <meta name="description" content="{{trans('swis.login_page.meta_description')}}" />
    <meta property="og:description" content="{{trans('swis.login_page.title')}}" />

    @include('layouts.head')

</head>

<?php
$theme = config('swis.theme_type');
?>
<body class="main--page logout--page gray-bg wrapper--materialDesign {{ $theme }}">

<div class="wrapper-out">

    @include('layouts.header')

    <div class="tj-reg__block  reg__block center-block">

        <div class="m-b">
            @include('partials.alerts')
        </div>

        <div class="reg__top">
            <div class="gerb-pic center-block">
                <img src="{{env('LOGIN_PAGE_LOGO_PATH')}}" alt="">
            </div>
            <h2 class="text-uppercase text-center reg__title fb fs20">{{trans('swis.login_page.title')}}</h2>

            <p class="reg__text fs14 reg__text--sub">{{trans('swis.login_page.desc_bottom')}}</p>
        </div>

        <div class="row">
            <div class="col-md-6 col-md-offset-3 tj-reg__inner col-sm-8 col-sm-offset-2 ">
                <form id="form-data" method="post" autocomplete="off" role="form" action="{{ urlWithLng('login') }}"
                      data-save-btn="mc-nav-button-save">
                    {{csrf_field()}}
                    <div class="clearfix tj-login__error">
                        <div class="form-error table-cell" id="form-error-credentials"></div>
                        <div class="form-error table-cell text-right" id="form-error-fail_count_message"></div>
                    </div>

                    <div class="form-group">
                        <input type="text" name="username" class="form-control"
                               placeholder="{{trans('core.base.label.username')}}" required="">
                        <div class="form-error" id="form-error-username"></div>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control"
                               placeholder="{{trans('core.base.label.password')}}" required="">
                        <div class="form-error" id="form-error-password"></div>
                    </div>
                    <div class="clearfix forgot_password__out">
                        <a href="{{ urlWithLng('reset-password') }}"
                           class="pull-right forgot_password__link">{{ trans('swis.core.base.forgot_password') }}</a><br/>
                    </div>

                    <button type="submit"
                            class="btn btn-primary block full-width m-b mc-nav-button-save btn--login"><i class="fa"></i> {{trans('login.modal.title')}}</button>
                </form>
            </div>
        </div>

        <br/>
    </div>
</div>

<footer class="sticky-footer  center-block">
    <div class="footer__inner">
        <div class="row">
            <div class="col-sm-7 col-md-8">
                <div class="footer__left">
                    <p class="fs13 footer__txt">{{trans('swis.login_page.footer_text')}}</p>
                    <p class="fs13 footer__txt">{{trans('swis.app_last_update')}} {{formattedDate(env('APP_LAST_UPDATE'))}}
                        , {{trans('swis.app_version')}} {{env('APP_VERSION')}}</p>
                </div>
            </div>
            <div class="col-sm-5 col-md-4">
                <p class="text-right fs11 copyright__txt">
                    &copy; {{date('Y')}} {{trans('swis.login_page.footer_copyright')}}</p>
            </div>
        </div>
    </div>
</footer>

@include('layouts.main-scripts')

<script src="{{asset('js/auth/main.js')}}"></script>
</body>
</html>

