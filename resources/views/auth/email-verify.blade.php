<!DOCTYPE html>
<head>
    @include('layouts.head')
</head>
<body class="wrapper--materialDesign {{ config('swis.theme_type') }}">
<?php

$jsTrans->addTrans([
    'core.loading.saved',
]);

?>

@include('partials.translations',['transType'=>'global'])

<header class="header">
    <div class="header--fixed clearfix closeRefreshGroup">
        <div class="pull-left header__left">
            <a title="{{trans('core.form.faq.tooltip')}}" class="icon--faq fb ver-top-box" href="{{urlWithLng('help')}}"><i class="fa fa-comments"></i>{{trans('swis.header.faq.title')}}</a>

            <?php
            $languages = activeLanguages();
            $lngManager = \App\Http\Controllers\Core\Language\LanguageManager::getInstance();
            $currentLanguage = currentLanguage();

            ?>
            <div class="languages pr select-none ver-top-box">

                <div title="{{trans('core.base.language.'.strtolower($currentLanguage->name))}}"  class="languages__title pr">
                    <span class="ver-top-box languages__img">
                        <img src="{{asset('image/'.$currentLanguage->icon)}}" alt="">
                    </span>
                    <span class="ver-top-box languages__txt fb">{{trans('core.base.language.'.$currentLanguage->code,[],$currentLanguage->code)}}</span>
                </div>
                <div class="languages__list dn">
                    @foreach($languages as $language)
                        @if($currentLanguage->id == $language->id) @continue @endif
                        <a title="{{trans('core.base.language.'.strtolower($language->name))}}" href="{{$lngManager->getUrl($language)}}" class="languages__link db">
                            <span class="ver-top-box languages__img">
                                <img src="{{asset('image/'.$language->icon)}}" alt="">
                            </span>
                            <span class="ver-top-box languages__txt">{{trans('core.base.language.'.$language->code,[],$language->code)}}</span>
                        </a>
                    @endforeach
                </div>
            </div>

        </div>

        <div class="pull-right header__right">

            <div class="ver-top-box header__notificaion-profile">

                @auth
                    <div class="dropdown profile-element ver-top-box header-profile">
                        <a data-toggle="dropdown" class="dropdown-toggle ver-top-box header-profile__name" href="#"
                           aria-expanded="true">
                    <span class="header-profile__nameTxt three-dot__oneLine db"
                          title="{{Auth::user()->name()}}">{{Auth::user()->name()}}</span>
                            <b class="header-profile__arrow caret"></b>
                            <i class="fa fa-user header-profile__user" aria-hidden="true"></i>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li>
                                <a href="#"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <i class="fa fa-sign-out-alt"></i> {{trans('swis.core.logout')}}
                                </a>
                                <form id="logout-form" action="{{ urlWithLng('/logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </div>
                @endauth
            </div>
        </div>
    </div>
</header>

<div class="gray-bg" id="wrapper" >
    <div class="row wrapper border-bottom white-bg page-heading custom-wrapper__main">
        <div class="col-sm-6">
            <h2>{{trans('swis.email_verify.title')}}</h2>
        </div>
    </div>
    <div>
        <div class=" main-content">
            <div class="wrapper wrapper-content animated fadeIn clearfix">
                <div class="ibox">
                    <div class="ibox-content">
                        @include('errors.errors')
                        @include('partials.alerts')
                        <div class="loginscreen animated fadeInDown main-content ">
                            <div class="row">
                                <div class="col-md-4 col-md-offset-4">
                                    <div class=" main-content">
                                        <form class="m-t" id="form-data" method="post" role="form" action="{{ routeWithLng('email-verify') }}">
                                            {{csrf_field()}}
                                            <div class="form-error" id="form-error-credentials"></div>

                                            <div class="form-group">
                                                <label>{{trans('core.base.label.email')}}</label>
                                                <input type="email" name="email" class="form-control" placeholder="{{trans('core.base.label.email')}}" required="">
                                                <div class="form-error" id="form-error-email"></div>
                                            </div>

                                            @if(!is_null($userActivationTokens))
                                                <div class="text-right">
                                                    <span class="email-token">{{Auth::user()->email}}</span> <button type="button" id="sendAgain" data-url="{{urlWithLng('email/verify-again')}}" class="btn btn-success clearfix">{{trans('swis.email_verify.send_again')}}</button>
                                                </div>
                                                <hr/>
                                            @endif

                                            <button type="button" class="btn btn-primary block full-width m-b mc-nav-button-save">{{trans('swis.email_verify.verify')}}</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="footer">
                <div>
                    <strong>Copyright</strong> SWIS&copy; 2018 - {{date('Y')}} <span class="version-date">{{trans('swis.app_last_update')}} {{formattedDate(env('APP_LAST_UPDATE'))}} , {{trans('swis.app_version')}} {{env('APP_VERSION')}}</span>
                </div>
            </div>
        </div>
    </div>
</div>


@include('layouts.main-scripts')
<script src="{{asset('js/auth/verify.js')}}"></script>

<script>
    $('.gray-bg').css("min-height", $(window).height() + "px");
</script>

</body>
</html>