@extends('layouts.app')

@section('title'){{trans('swis.risk_profile.title')}}@endsection

@section('contentTitle') {{trans('swis.risk_profile.title')}} @endsection

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@endsection

<?php
    switch ($saveMode) {
        case 'add':
            $url = 'risk-profiles';
            break;
        default :
            $url = $saveMode != 'view' ? 'risk-profiles/update/' . $riskProfile->id : '';

            $mls = $riskProfile->ml()->notDeleted()->base(['lng_id', 'description', 'show_status'])->keyBy('lng_id');

            break;
    }

    $languages = activeLanguages();
?>

@section('content')

    <form class="form-horizontal fill-up " id="form-data" action="{{urlWithLng($url)}}">

        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-general"> {{trans('core.base.tab.general')}}</a></li>
                <li class=""><a data-toggle="tab" href="#tab-instructions"> {{trans('core.base.tab.instructions')}}</a></li>
                @foreach($languages as $language)
                    <li><a data-toggle="tab" href="#tab-{{$language->code}}">{{ trans("core.base.language.".strtolower($language->name)) }}</a></li>
                @endforeach
            </ul>
            <div class="tab-content">
                <div id="tab-general" class="tab-pane active">
                    <div class="panel-body">

                        <div class="form-group required">
                            <label class="col-lg-2 control-label">{{trans('swis.risk_profile.document.label')}}</label>
                            <div class="col-lg-8">
                                <select class="form-control select2" name="document_id">
                                    <option value="">{{trans('swis.risk_profile.document.select.label')}}</option>
                                    @foreach($documents as $document)
                                        <option {{ (isset($riskProfile) && $riskProfile->document_id == $document->id) ? 'selected' : '' }} value="{{$document->id}}">{{$document->document_name}}</option>
                                    @endforeach
                                </select>
                                <div class="form-error" id="form-error-document_id"></div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-lg-2 control-label">{{trans('swis.risk_profile.name.label')}}</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" name="name" value="{{ isset($riskProfile) ? $riskProfile->name : '' }}" {{ isset($riskProfile) ? "readonly" : '' }}>
                                <div class="form-error" id="form-error-name"></div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-lg-2 control-label">{{trans('swis.risk_profile.weight.label')}}</label>
                            <div class="col-lg-8">
                                <input type="number" class="form-control" name="weight" value="{{ isset($riskProfile) ? $riskProfile->weight : '' }}">
                                <div class="form-error" id="form-error-weight"></div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-lg-2 control-label">{{trans('swis.risk_profile.frequency.label')}}</label>
                            <div class="col-lg-8">
                                <input type="number" class="form-control" name="frequency" value="{{ isset($riskProfile) ? $riskProfile->frequency : '' }}">
                                <div class="form-error" id="form-error-frequency"></div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-lg-2 control-label">{{trans('swis.risk_profile.condition.label')}}</label>
                            <div class="col-lg-8">
                                <textarea rows="6" name="condition" class="form-control">{{ isset($riskProfile) ? $riskProfile->condition : ''}}</textarea>
                                <div class="form-error" id="form-error-condition"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">{{trans('swis.risk_profile.risk_run_type.label')}}</label>
                            <div class="col-lg-8 general-status ">
                                @php
                                    $withCondition = \App\Models\RiskProfile\RiskProfile::RISK_RUN_TYPE_WITH_CONDITION;
                                    $withoutCondition = \App\Models\RiskProfile\RiskProfile::RISK_RUN_TYPE_WITHOUT_CONDITION;
                                @endphp
                                <select name="risk_run_type" class="form-show-status">
                                    <option value="{{$withCondition}}" {{ empty($riskProfile) ? 'selected' : '' }} {{(isset($riskProfile) && $riskProfile->risk_run_type == $withCondition) ?  'selected' : ''}}>{{trans('swis.risk_profile.risk_run_type_with_condition.label')}}</option>
                                    <option value="{{$withoutCondition}}" {{(isset($riskProfile) && $riskProfile->risk_run_type == $withoutCondition) ? 'selected' : ''}}>{{trans('swis.risk_profile.risk_run_type_without_condition.label')}}</option>
                                </select>
                                <div class="form-error" id="form-error-risk_run_type"></div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-lg-2 control-label">{{trans('swis.risk_profile.active_date_from.label')}}</label>
                            <div class="col-lg-8">

                                <div class='input-group date'>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                    <input value="{{ isset($riskProfile) ? $riskProfile->active_date_from : '' }}" name="active_date_from"
                                           type="text" autocomplete="off" data-start-date="{{currentDate()}}"
                                           placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                </div>

                                <div class="form-error" id="form-error-active_date_from"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('swis.risk_profile.active_date_to.label')}}</label>
                            <div class="col-lg-8">

                                <div class='input-group date'>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                    <input data-start-date="{{currentDate()}}" value="{{ isset($riskProfile) ? $riskProfile->active_date_to : '' }}" name="active_date_to"
                                           type="text" autocomplete="off"
                                           placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                </div>

                                <div class="form-error" id="form-error-active_date_to"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2">{{trans('core.base.label.status')}}</label>
                            <div class="col-lg-8 general-status ">
                                <select name="show_status" class="form-show-status">
                                    <option value="{{\App\Models\Role\Role::STATUS_ACTIVE}}" {{ empty($riskProfile) ? 'selected' : '' }} {{(isset($riskProfile) && $riskProfile->show_status == App\Models\RolesGroup\RolesGroup::STATUS_ACTIVE) ?  'selected' : ''}}>{{trans('core.base.label.status.active')}}</option>
                                    <option value="{{\App\Models\Role\Role::STATUS_INACTIVE}}" {{(isset($riskProfile) && $riskProfile->show_status == App\Models\RolesGroup\RolesGroup::STATUS_INACTIVE) ?  'selected' : ''}}>{{trans('core.base.label.status.inactive')}}</option>
                                </select>
                                <div class="form-error" id="form-error-show_status"></div>
                            </div>
                        </div>

                    </div>

                </div>

                <div id="tab-instructions" class="tab-pane">
                    <div class="panel-body">
                        <h2>{{ trans('swis.risk_profile.instructions.label') }}</h2>
                        @forelse($instructions as $instruction)
                            <label for="instruction-{{ $instruction->code }}">
                                <input type="checkbox" {{ (isset($riskProfile) && in_array($instruction->code, $attachedInstructions)) ? 'checked' : '' }} id="instruction-{{ $instruction->code }}" name="instructions[]" value="{{ $instruction->code }}"> {{ $instruction->current()->name}}
                            </label><br />
                        @empty
                            <p>{{ trans('swis.risk_profile.instruction.not_found') }}</p>
                        @endforelse
                        <div class="form-error" id="form-error-instructions"></div>
                    </div>
                </div>

                @foreach($languages as $language)
                    <div id="tab-{{$language->code}}" class="tab-pane">
                        <div class="panel-body">
                            <div class="form-group"><label class="col-lg-2 control-label">{{trans('core.base.description')}}</label>
                                <div class="col-lg-8">
                                    <textarea rows="6" name="ml[{{$language->id}}][description]" class="form-control">{{isset($mls[$language->id]) ? $mls[$language->id]->description : ''}}</textarea>
                                    <div class="form-error" id="form-error-ml-{{$language->id}}-description"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <input type="hidden" value="{{$riskProfile->id or 0}}" name="id" class="mc-form-key"/>
            {!! csrf_field() !!}
        </div>
    </form>
@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@endsection


@section('scripts')
    <script src="{{asset('js/risk-profiles/main.js')}}"></script>
@endsection