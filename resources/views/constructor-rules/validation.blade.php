<div class="form-group required">
    <label class="col-lg-2 col-lg-offset-1 control-label">{{trans('swis.constructor_rules.name.title')}}</label>
    <div class="col-lg-6">
        <input type="text" class="form-control" name="name" value="{{ (!empty($rule)) ? $rule->name : '' }}">
        <div class="form-error" id="form-error-name"></div>
    </div>
</div>

<div class="form-group required">
    <label class="col-lg-2 col-lg-offset-1 control-label">{{trans('swis.constructor_rules.condition')}}</label>
    <div class="col-lg-6">
        <textarea rows="12" name="condition" class="form-control">{{ (!empty($rule)) ? $rule->condition : '' }}</textarea>
        <div class="form-error" id="form-error-condition"></div>
    </div>
</div>

<div class="form-group required">
    <label class="col-lg-2 col-lg-offset-1 control-label">{{trans('swis.constructor_rules.rule')}}</label>
    <div class="col-lg-6">
        <textarea rows="12" name="rule" class="form-control">{{ (!empty($rule)) ? $rule->rule : '' }}</textarea>
        <div class="form-error" id="form-error-rule"></div>
    </div>
</div>