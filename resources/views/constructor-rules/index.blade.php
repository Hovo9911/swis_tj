@extends('layouts.app')

@section('title') {{trans('swis.constructor_rules.title')}} @endsection

@section('contentTitle') {{(isset($constructorRuleTypes) && isset($constructorRuleTypes[$type])) ? $constructorRuleTypes[$type] : trans('swis.constructor_rules.title') }}  @endsection

@section('form-buttons-top')
    @if (!is_null($type))
        {!! renderAddButton() !!}
    @endif
@endsection

@section('topScripts')
    <script>
        var constructorDocumentId = '{{ $selectedConstructorDocumentId }}';
        var ruleType = '{{ $type }}';
    </script>
@endsection

@section('content')
    <form class="form-horizontal fill-up" id="search-filter">
        <div class="form-group">
            <label class="col-md-4 col-lg-3  control-label">{{trans('swis.document.select_document')}}</label>
            <div class="col-md-6 col-lg-6">
                <select class="form-control select2 document-list default-search" name="constructor_document_id">
                    <option value=""></option>
                    @if ($constructorDocuments->count())
                        @foreach ($constructorDocuments as $constructorDocument)
                            <option value="{{ $constructorDocument->id }}" {{ $selectedConstructorDocumentId == $constructorDocument->id ? 'selected' : '' }}>{{ $constructorDocument->document_name }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>

        @if ($selectedConstructorDocumentId)
            <div class="form-group">
                <label class="col-md-4 col-lg-3 control-label">{{trans('swis.constructor_rules.select_type')}}</label>
                <div class="col-md-6 col-lg-6">
                    <select class="form-control select2 type-list  default-search" name="type">
                        <option></option>
                        @if (!empty($constructorRuleTypes))
                            @foreach ($constructorRuleTypes as $key => $item)
                                <option value="{{ $key }}" {{ (!is_null($type) && $type == $key) ? 'selected' : '' }}>{{ $item }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div> <hr />
        @endif
    </form>

    @if (!is_null($type))
        <div class="data-table table-block">
            <div class="clearfix">
                <table class="table table-striped table-bordered table-hover dataTables-example" id="list-data">
                    <thead>
                        <tr>
                            <th data-col="actions" data-delete="0" class="th-actions">{{trans('core.base.label.actions')}}</th>
                            <th data-col="id">ID</th>
                            <th data-col="name">{{trans('swis.constructor_rules.table.name')}}</th>
                            <th data-col="type">{{trans('swis.constructor_rules.table.type')}}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    @endif

@endsection

@section('scripts')
    <script src="{{asset('js/constructor-rules/main.js')}}"></script>
@endsection
