<div class="form-group required">
    <label class="col-lg-2 col-lg-offset-1 control-label">{{trans('swis.constructor_rules.name.title')}}</label>
    <div class="col-lg-6">
        <input type="text" class="form-control" name="name" value="{{ (!empty($rule)) ? $rule->name : '' }}">
        <div class="form-error" id="form-error-name"></div>
    </div>
</div>

<div class="form-group required">
    <label class="col-lg-2 col-lg-offset-1 control-label">{{trans('swis.constructor_rules.select_obligation')}}</label>
    <div class="col-lg-6">
        <select class="form-control select2 obligation-list" name="obligation_code">
            <option></option>
            @foreach ($obligationList as $obligation)
                <?php
                    if (!empty($rule)) {
                        $selected = ($rule->obligation_code == $obligation->code) ? 'selected' : '';
                        if ($selected == 'selected') {
                            $obligationType = $obligation->type;
                        }
                    }
                ?>
                <option value="{{ $obligation->code }}" data-type="{{ $obligation->type }}" {{ (!empty($rule) && $rule->obligation_code == $obligation->code) ? 'selected' : '' }}>{{ $obligation->name }}</option>
            @endforeach
        </select>
        <div class="form-error" id="form-error-obligation_code"></div>
    </div>
</div>

<div class="form-group required ruleBlock {{ (!empty($obligationType) && $obligationType == 'rule') ? '' : 'hide' }}">
    <label class="col-lg-2 col-lg-offset-1 control-label">{{trans('swis.constructor_rules.rule')}}</label>
    <div class="col-lg-6">
        <textarea rows="12" name="rule" class="form-control">{{ !empty($rule) ? $rule->rule : '' }}</textarea>
        <div class="form-error" id="form-error-rule"></div>
    </div>
</div>