<?php
$constructorRuleTypes = App\Models\ConstructorRules\ConstructorRules::getTypes();
$title = (isset($constructorRuleTypes) && isset($constructorRuleTypes[$type])) ? $constructorRuleTypes[$type] : trans('swis.constructor_rules.title');
?>

@extends('layouts.app')

@section('title'){{$title}}@endsection

@section('contentTitle') {{$title}} @endsection

@section('topScripts')
    <script>
        var constructorDocumentId = '{{ $constructorDocumentId }}';
        var ruleType = '{{ $type }}';
    </script>
@endsection

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@endsection

@section('content')
    <?php
        switch ($saveMode) {
            case 'add':
                $url = "constructor-rules/{$constructorDocumentId}/{$type}/store";
                break;
            case 'edit':
                $url = "constructor-rules/{$constructorDocumentId}/{$type}/update/{$rule->id}";
                break;
        }
    ?>
    <form class="form-horizontal fill-up " id="form-data" action="{{urlWithLng($url)}}">
        @include("constructor-rules.{$type}")
        <input type="hidden" name="document_id" value="{{ $constructorDocumentId }}" />
        <input type="hidden" name="type" value="{{ $type }}" />
    </form>
@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@endsection

@section('scripts')
    <script src="{{asset('js/constructor-rules/main.js')}}"></script>
@endsection