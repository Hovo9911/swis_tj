@extends('layouts.app')

@section('title'){{trans('swis.user_manuals.title')}}@stop

@section('contentTitle') {{trans('swis.user_manuals.title')}} @stop

@section('form-buttons-top')
    @if(!(isset($saveMode) && $saveMode == 'view'))
        {!! renderAddButton() !!}
    @endif
@stop

@if(Auth::guest())
@section('styles')
    <link rel="stylesheet" href="{{asset('assets/helix/core/css/plugins/dataTables/datatables.min.css')}}">
@endsection
@endif

@section('content')

    <div class="clearfix search__out">
        <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse" data-target="#collapseSearch" aria-expanded="false" aria-controls="collapseSearch">
            <i class="fa fa-search"> </i><span class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
        </button>
    </div>

    <div class="collapse" id="collapseSearch">
        <div class="card card-body">
            <form class="form-horizontal fill-up" id="search-filter" autocomplete="off">

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">ID</label>
                    <div class="col-md-8  field__one-col">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="number" min="0" class="form-control onlyNumbers" name="id" id="" value="">
                                <div class="form-error" id="form-error-id"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.module')}}</label>
                    <div class="col-md-8  field__one-col">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="module" value="">
                                <div class="form-error" id="form-error-module"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.description')}}</label>
                    <div class="col-md-8  field__one-col">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="description" value="">
                                <div class="form-error" id="form-error-description"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.type')}}</label>
                    <div class="col-md-8  field__one-col">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <select class="form-control select2" name="type">
                                    <option></option>
                                    @foreach($userManualTypes as $userManualType)
                                        <option value="{{ $userManualType }}" {{ (!empty($userManuals) && $userManuals->type == $userManualType) ? 'selected' : '' }}> {{ trans('swis.user_manual_type.'.$userManualType) }}</option>
                                    @endforeach
                                </select>
                                <div class="form-error" id="form-error-type"></div>
                            </div>
                        </div>
                    </div>
                </div>
                @if($page == \App\Models\UserManuals\UserManuals::PAGE_TYPE_USER_MANUALS)
                    <div class="form-group">
                        <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.user_manuals.label.permission')}}</label>
                        <div class="col-md-8  field__one-col">
                            <div class="row">
                                <div class="col-lg-10 col-md-8 field__one-col">
                                    <select class="form-control select2" data-select2-allow-clear="false"
                                            name="permission">
                                        <option></option>
                                        @foreach($roleTypes as $roleType)
                                            <option value="{{$roleType}}">{{trans('swis.user_manuals.label.role_type_'.$roleType)}}</option>
                                        @endforeach
                                    </select>
                                    <div class="form-error" id="form-error-permission"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8 col-lg-offset-3 text-right">
                        <div class="row">
                            <div class="col-lg-10 col-md-8">
                                <button type="submit"
                                        class="btn btn-primary mr-10">{{trans('core.base.label.search')}}</button>
                                <button type="button" class="btn btn-danger resetButton"
                                        data-submit="true">{{trans('core.base.label.reset')}}</button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="data-table">
        <div class="clearfix">
            <table class="table table-striped table-bordered table-hover dataTables-example" id="list-data">
                <thead>
                <tr>
                    @if($page == \App\Models\UserManuals\UserManuals::PAGE_TYPE_USER_MANUALS)
                        <th style="min-width: 90px" data-col="actions" class="th-actions"
                            data-delete="0">{{trans('core.base.label.actions')}}</th>
                    @endif
                    <th data-ordering="1" data-col="id">ID</th>
                    <th data-col="module">{{trans('core.base.label.module')}}</th>
                    <th data-col="description">{{trans('core.base.label.description')}}</th>
                    <th data-col="type">{{trans('core.base.label.type')}}</th>
                    <th data-col="version">{{trans('core.base.label.version')}}</th>
                    <th data-col="updated_at">{{trans('core.base.label.version_date')}}</th>
                    <th data-col="file">{{trans('core.base.label.file')}}</th>
                    @if($page == \App\Models\UserManuals\UserManuals::PAGE_TYPE_USER_MANUALS)
                        <th data-col="creator_user_id">
                            {{trans('swis.user_manuals.label.creator_user_id')}}
                        </th>
                    @endif
                    <th data-col="url">{{trans('core.base.label.url')}}</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

@endsection

@section('scripts')

    <script>
        var pageType = "{{$page}}";

        $('.gray-bg').css("min-height", $(window).height() + "px");
    </script>

    @if(Auth::guest())
        <script src="{{asset('assets/helix/core/plugins/dataTables/datatables.min.js')}}"></script>
        <script src="{{asset('assets/helix/core/js/DataTables.js')}}"></script>
    @endif

    <script src="{{asset('js/user-manuals/main.js')}}"></script>

@endsection