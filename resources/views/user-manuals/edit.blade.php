@extends('layouts.app')

@section('title'){{trans('swis.user_manuals.title')}}@stop

@section('contentTitle')

    @switch($saveMode)

        @case('add')
        {{trans('swis.user-manuals.create.title')}}
        @break
        @case('edit')
        {{trans('swis.user-manuals.edit.title')}}
        @break

    @endswitch

@stop

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('content')
    <?php
    use App\Models\UserManualsRoles\UserManualsRoles;
    $gUploader = new \App\GUploader('user_manuals');

    $docDisabled = $showAgency = false;

    switch ($saveMode) {
        case 'add':
            $url = 'user-manuals';
            break;
        default:
            $url = $saveMode != 'view' ? 'user-manuals/update/' . $userManuals->id: '';

            $mls = $userManuals->ml()->notDeleted()->base(['lng_id', 'module', 'description', 'show_status'])->keyBy('lng_id');
            break;
    }

    $languages = activeLanguages();

    if ((isset($permissions) && in_array(UserManualsRoles::USER_MANUAL_ROLE_TYPE_AGENCY, $permissions))) {
        $showAgency = true;
    }
    ?>
    <form class="form-horizontal fill-up" id="form-data" action="{{urlWithLng($url)}}">

        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-general"> {{trans('core.base.tab.user-manuals')}}</a></li>
                @foreach($languages as $language)
                    <li>
                        <a data-toggle="tab" href="#tab-{{$language->code}}">{{ trans("core.base.language.".strtolower($language->name)) }}</a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content">
                <div id="tab-general" class="tab-pane active">
                    <div class="panel-body">
                        <div class="form-group required"><label
                                    class="col-lg-2 control-label">{{trans('swis.user_manuals.label.type')}}</label>
                            <div class="col-lg-10">
                                <select class="form-control select2" name="type">
                                    <option></option>
                                    @foreach($userManualTypes as $userManualType)
                                        <option value="{{ $userManualType }}" {{ (!empty($userManuals) && $userManuals->type == $userManualType) ? 'selected' : '' }}> {{ trans('swis.user_manual_type.'.$userManualType) }}</option>
                                    @endforeach
                                </select>
                                <div class="form-error" id="form-error-type"></div>
                            </div>
                        </div>
                        <div class="form-group required"><label
                                    class="col-lg-2 control-label">{{trans('swis.user_manuals.label.version')}}</label>
                            <div class="col-lg-10">
                                <input value="{{$userManuals->version or ''}}" name="version" type="text" placeholder="" class="form-control">
                                <div class="form-error" id="form-error-version"></div>
                            </div>
                        </div>
                        <div class="form-group"><label
                                    class="col-lg-2 control-label">{{trans('swis.user_manuals.label.file')}}</label>
                            <div class="col-lg-10">
                                @if(isset($userManuals) && $userManuals->filePath())
                                    <div class="documents-download__result">
                                        <a target="_blank" href="{{$userManuals->filePath()}}">{{$userManuals->fileBaseName()}}</a>
                                        <span class="documents-separate-line">|</span>
                                        <a class="btn btn-primary btn-xs" download="" href="{{$userManuals->filePath()}}">{{trans('swis.document.label.download')}}</a>
                                    </div>
                                @endif
                                <div class="g-upload-container">
                                    <div class="register-form__list">
                                        <div class="register-form__item">
                                            <div class="g-upload-box">
                                                <label class="btn btn-primary btn-upload">
                                                    <i class="fa fa-upload"></i>
                                                    {{trans('swis.user_manuals.upload.button')}}
                                                    <input {{!empty($userManuals->file) && $docDisabled ? 'disabled' : ''}} id="files"
                                                           class="dn g-upload" type="file" name="file"
                                                           value="{{$userManuals->file or ''}}"
                                                           {{$gUploader->isMultiple() ? 'multiple' : ''}} data-conf="user_manuals"
                                                           data-action="{{urlWithLng('/g-uploader/upload')}}"
                                                           accept="{{$gUploader->getAcceptTypes()}}"/>
                                                </label>
                                            </div>

                                            <div class="license-form__uploaded-file-list g-uploaded-list"></div>
                                            <progress class="progressBarFileUploader" value="0" max="100"></progress>
                                            <div class="form-error-text form-error-files fb fs12"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group required"><label
                                    class="col-lg-2 control-label">{{trans('swis.user_manuals.label.available_without_auth')}}</label>
                            <div class="col-lg-10">
                                <label class="radio-inline">
                                    <input type="radio" name="available_without_auth" value="1" {{(((!empty($userManuals) && $userManuals->available_without_auth == 1) ) ? 'checked' : '' )}} /> {{ trans('swis.user_manuals.label.yes') }}
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="available_without_auth" value="0" {{((!empty($userManuals) && $userManuals->available_without_auth == 0) || !isset($userManuals) ? 'checked' : '' )}}/> {{ trans('swis.user_manuals.label.no') }}
                                </label>
                                <div class="form-error" id="form-error-available_without_auth"></div>
                            </div>
                        </div>
                        <div class="form-group roles"><label
                                    class="col-lg-2 control-label">{{trans('swis.user_manuals.label.permission')}}</label>
                            <div class="col-lg-10">
                                <select class="form-control select2" id="roles" name="roles[]" multiple>
                                    @if(!empty($roleTypes))
                                        @foreach($roleTypes as $roleType)
                                            <option {{(isset($permissions) && in_array($roleType,$permissions)) ? 'selected' : ''}} value="{{$roleType}}">{{trans('swis.user_manuals.label.role_type_'.$roleType)}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <div class="form-error" id="form-error-permission"></div>
                            </div>
                        </div>
                        <div class="form-group agencies @if(!$showAgency) hide @endif">
                            <label class="col-lg-2 control-label">{{trans('swis.user_manuals.label.agency')}}</label>
                            <div class="col-lg-10">
                                @if($agencies->count())
                                    <select class="form-control select2" id="agency" name="agencies[]" multiple>
                                        @foreach($agencies as $agency)
                                            <option {{(isset($selectedAgencies) && in_array($agency->tax_id,$selectedAgencies)) ? 'selected' : ''}}  value="{{$agency->tax_id}}">{{$agency->name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="form-error" id="form-error-agencies"></div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group"><label
                                    class="col-lg-2 control-label">{{trans('swis.user_manuals.label.url')}}</label>
                            <div class="col-lg-10">
                                <input value="{{$userManuals->url or ''}}" name="url" type="text"
                                       placeholder="" class="form-control">
                                <div class="form-error" id="form-error-url"></div>
                            </div>
                        </div>
                    </div>
                </div>

                @foreach($languages as $language)
                    <?php $lngId = $language->id ?>
                    <div id="tab-{{$language->code}}" class="tab-pane">
                        <div class="panel-body">

                            <div class="form-group required"><label
                                        class="col-lg-2 control-label">{{trans('swis.user-manuals.label.module')}}</label>
                                <div class="col-lg-10">
                                    <input value="{{isset($mls[$lngId]) ? $mls[$lngId]->module : ''}}"
                                           name="ml[{{$language->id}}][module]" type="text"
                                           placeholder=""
                                           class="form-control">

                                    <div class="form-error"
                                         id="form-error-ml-{{$language->id}}-module"></div>
                                </div>
                            </div>
                            <div class="form-group"><label
                                        class="col-lg-2 control-label">{{trans('swis.user-manuals.label.description')}}</label>
                                <div class="col-lg-10">
                                    <textarea rows="6" name="ml[{{$language->id}}][description]"
                                              class="form-control"
                                              id="ml_{{$language->id}}_editor">{{isset($mls[$lngId]) ? $mls[$lngId]->description : ''}}</textarea>
                                    <div class="form-error"
                                         id="form-error-ml-{{$language->id}}-description"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <input type="hidden" value="{{$userManuals->id or 0}}" name="id" class="mc-form-key"/>

        {!! csrf_field() !!}
    </form>
@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script src="{{asset('js/user-manuals/main.js')}}"></script>
    <script src="{{asset('js/guploader.js')}}"></script>
@endsection

