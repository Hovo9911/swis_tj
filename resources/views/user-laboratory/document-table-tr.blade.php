@if(empty($renderInput))

    @if(count($attachedDocumentProducts) > 0)
        <?php $num = $num ?? 1 ?>
        @foreach($attachedDocumentProducts as $attachedDocumentProduct)

            <?php
            $document = (is_null($attachedDocumentProduct->document)) ? $attachedDocumentProduct : $attachedDocumentProduct->document;
            ?>
            <tr  data-num="{{$num}}">
                <td><input type="hidden" name="document_id" value="{{$document->id}}">{{$num}}</td>
                <td>{{trans('swis.document.document_form.'.$document->document_form.'.title')}}</td>
                <td>{{$document->document_type}}</td>
                <td class="text-break-word" title="{{$document->document_name}}" data-toggle="tooltip">{{$document->document_name}}</td>
                <td>{{$document->document_description}}</td>
                <td>{{$document->document_number}}</td>
                <td>{{$document->document_release_date}}</td>
                <td class="text-break-word">{{$document->fileBaseName()}}</td>
                <td>
                    <span class="product-ids-list">1</span>
                    <input class="product-ids" value="" name="documentProducts[{{$num}}][products]" type="hidden">
                </td>
                <td class="actions-list">
                    @if(!empty($document->document_file))
                        <a target="_blank" href="{{$document->filePath()}}"
                           class="btn btn-success btn-xs btn--icon" title="{{ trans('swis.base.tooltip.view.button') }}"
                           data-toggle="tooltip"><i class="fas fa-eye"></i></a>
                    @endif

                    @if((\Illuminate\Support\Facades\Auth::user()->isLocalAdmin()  || $document->user_id == \Auth::id()) && $document->company_tax_id == \Auth::user()->companyTaxId())
                        @if(isset($expertiseId) && $attachedDocumentProduct->expertise_id == $expertiseId)
                            <button class="dt-document"><i class="fas fa-times"></i></button>
                        @endif
                    @endif
                </td>
            </tr>
            <?php $num++ ?>
        @endforeach

    @endif

@else
    <?php
    $gUploader = new \App\GUploader('document_report');
    ?>
    <tr data-num="{{$num}}">
        <td><input type="hidden" name="document_id" class="doc-id" value="">{{$num}}</td>
        <td class="document-form"></td>
        <td>
            <input type="text" autocomplete="off" class="autocomplete form-control"
                   data-ref="{{\App\Models\ReferenceTable\ReferenceTable::getReferenceIdByTableName(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS)}}"
                   data-ref-type="autocomplete"
                   data-fields="{{json_encode([['from'=>'name','to'=>'document_name'],['from'=>'code','to'=>'document_type']])}}"
                   placeholder="autocomplete"
                   name="document_type">

            <input type="hidden" name="document_type_value" value="{{$document->document_type_value or ''}}">
            <div class="form-error" id="form-error-document_type-{{$num}}"></div>
        </td>
        <td>
            <input type="text" class="form-control" name="document_name" readonly="readonly">
            <div class="form-error" id="form-error-document_name-{{$num}}"></div>
        </td>
        <td>
            <input type="text" class="form-control" name="document_description">
            <div class="form-error" id="form-error-document_description-{{$num}}"></div>
        </td>
        <td>
            <input type="text" class="form-control" name="document_number">
            <div class="form-error" id="form-error-document_number-{{$num}}"></div>
        </td>
        <td>
            <div class='input-group date'><span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                <input type="text" autocomplete="off" placeholder="{{setDatePlaceholder()}}" class="form-control" name="document_release_date">
            </div>
            <div class="form-error" id="form-error-document_release_date-{{$num}}"></div>
        </td>
        <td class="text-break-word">
            <div class="g-upload-container">
                <div class="g-upload-box">
                    <label style="width:35px"><i class="fas fa-upload btn btn-success btn-sm"></i> <input
                                style="visibility: hidden"
                                class="dn g-upload" type="file" name="document_file"
                                {{$gUploader->isMultiple() ? 'multiple' : ''}} data-conf="document_report"
                                data-action="{{urlWithLng('/g-uploader/upload')}}"
                                accept="{{$gUploader->getAcceptTypes()}}"/></label>
                </div>
                <div class="license-form__uploaded-file-list g-uploaded-list "></div>
                {{-- Progress bar for file uploding process, if you want to show process--}}
                <progress class="progressBarFileUploader" value="0" max="100"></progress>
                {{-- Progress bar for file uploding process --}}
                <div class="form-error-text form-error-files fb fs12"></div>

                <div class="form-error" id="form-error-document_file-{{$num}}"></div>
            </div>
        </td>
        <td> <span class="product-ids-list">1</span>

        <td class="actions-list">
            <button type="button" class="btn btn-primary btn-xs storeDocument btn--icon" title="{{ trans('swis.base.tooltip.view.button') }}" data-toggle="tooltip"><i class="fa fa-check"></i></button>
            <button class="dt-document"><i class="fas fa-times"></i></button>
        </td>
    </tr>

@endif