@extends('layouts.app')

@section('title'){{trans('swis.user_laboratory.title')}}@stop

@section('contentTitle') {{trans('swis.user_laboratory.title')}} @stop

@section('content')

    <div class="clearfix search__out">
        <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse"
                data-target="#collapseSearch" aria-expanded="false" aria-controls="collapseSearch">
            <i class="fa fa-search"> </i><span
                    class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
        </button>
    </div>

    <div class="collapse" id="collapseSearch">
        <div class="card card-body">
            <form class="form-horizontal fill-up" id="search-filter" autocomplete="off">

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.user_laboratory.sample_code.title')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="sample_code" id="" value="">
                                <div class="form-error" id="form-error-sample_code"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.user_laboratory.product_numbers.title')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="number" min="0" class="form-control onlyNumbers" name="saf_product_id" id="" value="">
                                <div class="form-error" id="form-error-saf_product_id"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.user_laboratory.sampler.title')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <select class="select2" name="sampler" id="sampler">
                                    @if($samplers->count())
                                        <option value=""></option>
                                        @foreach($samplers as $sampler)
                                            <option value="{{$sampler->id}}">{{$sampler->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <div class="form-error" id="form-error-sampler"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.user_laboratory.submission_date.title')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-4 col-lg-5 field__two-col">
                                <div class='input-group date'>
                                    <span class="input-group-addon"><span
                                                class="glyphicon glyphicon-calendar"></span></span>
                                    <input value="" name="created_at_date_start" autocomplete="off"
                                           type="text"
                                           placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                    <div class="form-error" id="form-error-created_at_date_start"></div>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-5 field__two-col">
                                <div class='input-group date'>
                                    <span class="input-group-addon"><span
                                                class="glyphicon glyphicon-calendar"></span></span>
                                    <input value="" name="created_at_date_end" autocomplete="off"
                                           type="text"
                                           placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                    <div class="form-error" id="form-error-created_at_date_end"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.user_laboratory.return_date.title')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-4 col-lg-5 field__two-col">
                                <div class='input-group date'>
                                    <span class="input-group-addon"><span
                                                class="glyphicon glyphicon-calendar"></span></span>
                                    <input value="" name="return_date_date_start" autocomplete="off"
                                           type="text"
                                           placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                    <div class="form-error" id="form-error-return_date_date_start"></div>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-5 field__two-col">
                                <div class='input-group date'>
                                    <span class="input-group-addon"><span
                                                class="glyphicon glyphicon-calendar"></span></span>
                                    <input value="" name="return_date_date_end" autocomplete="off"
                                           type="text"
                                           placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                    <div class="form-error" id="form-error-return_date_date_end"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8 col-lg-offset-3 text-right">
                        <div class="row">
                            <div class="col-lg-10 col-md-8">
                                <button type="submit"
                                        class="btn btn-primary mr-10">{{trans('core.base.label.search')}}</button>
                                <button type="button" class="btn btn-danger resetButton"
                                        data-submit="true">{{trans('core.base.label.reset')}}</button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>

    <div class="data-table">
        <div class="clearfix">
            <table class="table table-striped table-bordered table-hover dataTables-example" id="list-data"
                   data-action="user-laboratory/{{$labId}}/table"
                   data-search-path="user-laboratory/{{$labId}}/expertise">
                <thead>
                <tr>
                    <th data-delete="0" data-col="actions" class="th-actions">{{trans('core.base.label.actions')}}</th>
                    <th data-col="created_at">{{trans('swis.user_laboratory.submission_date.title')}}</th>
                    <th data-col="return_date">{{trans('swis.user_laboratory.return_date.title')}}</th>
                    <th data-ordering="0" data-col="sampler">{{trans('swis.user_laboratory.sampler.title')}}</th>
                    <th data-col="agency_name">{{trans('swis.user_laboratory.agency_name.title')}}</th>
                    <th data-col="sample_code">{{trans('swis.user_laboratory.sample_code.title')}}</th>
                    <th data-col="sampling_weight">{{trans('swis.user_laboratory.sampling_weight.title')}}</th>
                    <th data-col="saf_product_id">{{trans('swis.user_laboratory.product_numbers.title')}}</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    <input type="hidden" value="{{$labId or 0}}" id="labId" class="mc-form-key"/>

@endsection

@section('scripts')
    <script src="{{asset('js/user-laboratory/main.js')}}"></script>
@endsection