@extends('layouts.app')

@section('title'){{trans('swis.user_laboratory.title')}}@stop

@section('contentTitle') {{trans('swis.user_laboratory.expertise.title')}} @stop

@section('topScripts')
    <script>
        var $viewRegime = '{{ $viewRegime or ''}}';
    </script>
@endsection

@section('content')
    @if(is_null($safExpertise->received_date))
        <form class="form-horizontal fill-up" method="post"
              action="{{urlWithLng('/user-laboratory/'.$labId.'/expertise/'.$expertiseId.'/receive')}}">
            {{csrf_field()}}
            <button class="btn btn-primary m-b">{{trans('swis.saf_expertise.received.button')}}</button>
        </form>
    @else
        <h2>{{trans('swis.saf_expertise.received.title')}} {{ $safExpertise->received_date }}</h2>
    @endif

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" class="collapseLabCreate" data-parent="#accordion"
                   href="#collapseLabCreate">{{trans('swis.my_application.laboratories.create_label')}} </a>
            </h4>
        </div>
        <div class="panel-body">
            @include('user-laboratory.laboratory-module',['safExpertise'=>$safExpertise])
        </div>
    </div>
    <div class="lab-all-content">
        <form class="form-horizontal fill-up " id="form-data" data-search-path="{{'/user-laboratory/'.$labId}}"
              action="{{urlWithLng('/user-laboratory/'.$labId.'/expertise/'.$expertiseId.'/update')}}">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a>{{trans('swis.my_application.laboratories.indicators')}} </a>
                    </h4>
                </div>
                <div class="panel-body indicators-list">
                    <div class="table-responsive">
                        @if($indicators->count() > 0)

                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>{{ trans('swis.my_application.laboratories.table.indicators') }}</th>
                                    <th>{{ trans('swis.my_application.laboratories.table.limit') }}</th>
                                    <th>{{ trans('swis.my_application.laboratories.table.measurement_unit') }}</th>
                                    <th>{{ trans('swis.my_application.laboratories.table.actual_result') }}</th>
                                    <th>{{ trans('swis.my_application.laboratories.table.adequacy') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($indicators as $key=>$indicator)
                                    <tr>
                                        <td>{{$indicator->indicator_name or ''}}</td>
                                        <td>{{$indicator->limit or ''}}</td>
                                        <td>{{$indicator->unit_name or ''}}</td>
                                        <td>
                                            <div class="form-group m-b-none">
                                                <input class="form-control" value="{{$indicator->actual_result or ''}}"
                                                       type="text" name="data[{{$indicator->id}}][actual_result]">
                                                <div class="form-error"
                                                     id="form-error-data-{{$indicator->id}}-actual_result"></div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group m-b-none">
                                                @if(!empty($adequacyRef))
                                                    <select class="form-control select2"
                                                            name="data[{{$indicator->id}}][adequacy]">
                                                        <option selected disabled value="">select</option>
                                                        @foreach($adequacyRef as $value)
                                                            <option {{!empty($indicator) && $indicator->adequacy == $value->id ? 'selected' : ''}} value="{{$value->id}}">{{$value->name}}</option>
                                                        @endforeach
                                                    </select>
                                                @endif
                                                <div class="form-error"
                                                     id="form-error-data-{{$indicator->id}}-adequacy"></div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </form>

        <div class="tabs-container form-horizontal ">

            @include('user-laboratory.document-module')

            <input type="hidden" value="{{$expertiseId or 0}}" id="expertiseId" class="mc-form-key"/>
            <input type="hidden" value="{{$labId or 0}}" id="labId" class="mc-form-key"/>

        </div>
    </div>
@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script src="{{asset('assets/helix/core/plugins/bootstrap-typeahead/bootstrap-typeahead.js')}}"></script>
    <script src="{{asset('js/user-laboratory/main.js')}}"></script>
    <script src="{{asset('js/guploader.js')}}"></script>
@endsection