<div class="form-horizontal">

    <div class="row loading-in-edit-mode hide text-center">
        <i class="fas fa-sync fa-spin" style="font-size: 35px;"></i>
    </div>

    <div class="data-block">

        <div class="form-group clearfix">
            <label class="col-md-4 col-lg-3 control-label label__title">{{ trans('swis.my_application.laboratories.sample_code') }}</label>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-lg-10 col-md-8 field__one-col">
                        <input class="form-control" type="text" disabled value="{{$safExpertise->sample_code or ''}}">
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group clearfix">
            <label class="col-md-4 col-lg-3 control-label label__title">{{ trans('swis.my_application.laboratories.sample_weight') }}</label>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-lg-10 col-md-8 field__one-col">
                        <input class="form-control" type="text" disabled
                               value="{{$safExpertise->sampling_weight or ''}}">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group clearfix">
            <label class="col-md-4 col-lg-3 control-label label__title">{{ trans('swis.my_application.laboratories.goods') }}</label>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-lg-10 col-md-8 field__one-col">

                        <?php
                        $productCode = \App\Models\SingleApplicationProducts\SingleApplicationProducts::where('id', $safExpertise->saf_product_id)->pluck('product_code')->first();
                        $hsCode = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_HS_CODE_6, $productCode, false, ['id', 'code', 'name', 'long_name']);

                        if (!is_null($hsCode)) {
                            $hsCodeName = "({$hsCode->code}) {$hsCode->long_name}";
                        }

                        ?>
                        <input class="form-control" type="text" disabled value="{{ $hsCodeName or ''}}" data-toggle="tooltip" title="{{ $hsCode->long_name or '' }}">
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group clearfix">
            <label class="col-md-4 col-lg-3 control-label label__title">{{ trans('swis.my_application.laboratories.expertising_method') }}</label>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-lg-10 col-md-8 field__one-col">
                        <?php $refTechRegul = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_TECH_REGUL, $safExpertise->expertising_method_code);

                        if (!is_null($refTechRegul)) {
                            $expertise_method_code = '(' . $refTechRegul->code . ') ' . $refTechRegul->name;
                        }
                        ?>

                        <input class="form-control" type="text" disabled value="{{$expertise_method_code or ''}}">
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group clearfix">
            <label class="col-md-4 col-lg-3 control-label label__title">{{ trans('swis.my_application.laboratories.expertising_indicator') }}</label>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-lg-10 col-md-8 field__one-col">
                        <?php $refLabExpertise = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_LABORATORY_EXPERTISE, $safExpertise->expertising_indicator_code);

                        if (!is_null($refLabExpertise)) {
                            $expertise_indicator_code = '(' . $refLabExpertise->code . ') ' . $refLabExpertise->name;
                        }

                        ?>
                        <input class="form-control" type="text" disabled value="{{$expertise_indicator_code or ''}}">
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group clearfix">
            <label class="col-md-4 col-lg-3 control-label label__title">{{ trans('swis.my_application.laboratories.sample_method') }}</label>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-lg-10 col-md-8 field__one-col">
                        <?php $refSamplingTech = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_SAMPLING_TECH, $safExpertise->sampling_method_code);

                        if (!is_null($refSamplingTech)) {
                            $sampling_method_code = '(' . $refSamplingTech->code . ') ' . $refSamplingTech->name;
                        }

                        ?>

                        <input class="form-control" type="text" disabled value="{{ $sampling_method_code or ''}}">
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group clearfix">
            <label class="col-md-4 col-lg-3 control-label label__title">{{ trans('swis.my_application.laboratories.sample_sampler') }}</label>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-lg-10 col-md-8 field__one-col">
                        <?php

                        if (is_array($safExpertise->sampler_code)) {
                            $refSampler = getReferenceRowsWhereIn(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_LABORATORY_SAMPLERS, $safExpertise->sampler_code);

                            $samplers = [];
                            if($refSampler->count()){
                                $samplers =  $refSampler->pluck('name')->all();
                            }

                            $sampler_code = implode(', ', $samplers);

                        } else {
                            $refSamplers = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_LABORATORY_SAMPLERS, $safExpertise->sampler_code);

                            if (!is_null($refSamplers)) {
                                $sampler_code = $refSamplers->name;
                            }
                        }

                        ?>

                        <input class="form-control" type="text" disabled value="{{$sampler_code or ''}}">
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group clearfix">
            <label class="col-md-4 col-lg-3 control-label label__title">{{ trans('swis.my_application.laboratories.batches') }}</label>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-lg-10 col-md-8 field__one-col">

                        <select class="select2 {{!empty($safExpertiseEdit) ? 'select2_edit_view' : ''}} saveParam form-control  readonly"
                                disabled multiple="multiple">
                            @if(!is_null($safExpertise->saf_product_batches_ids))
                                <?php
                                $batches = \App\Models\SingleApplicationProductsBatch\SingleApplicationProductsBatch::select('batch_number')->whereIn('id', $safExpertise->saf_product_batches_ids)->get();
                                ?>

                                @if(!empty($batches))
                                    @foreach($batches as $batch)
                                        <option selected>{{trans($batch->batch_number)}}</option>
                                    @endforeach
                                @endif
                            @endif
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group clearfix">
            <label class="col-md-4 col-lg-3 control-label label__title">{{ trans('swis.my_application.laboratories.product_group') }}</label>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-lg-10 col-md-8 field__one-col">
                        <?php $refTechReg = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_TECH_REG, $safExpertise->product_group_code);

                        if (!is_null($refTechReg)) {
                            $product_group_code = '(' . $refTechReg->code . ') ' . $refTechReg->name;
                        }
                        ?>
                        <input class="form-control" type="text" disabled value="{{$product_group_code or ''}}">
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group clearfix">
            <label class="col-md-4 col-lg-3 control-label label__title">{{ trans('swis.my_application.laboratories.product_sub_group') }}</label>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-lg-10 col-md-8 field__one-col">
                        <?php $refTechChapter = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_TECH_REG_CHAPTERS, $safExpertise->product_sub_group_code);

                        if (!is_null($refTechChapter)) {
                            $product_sub_group_code = '(' . $refTechChapter->code . ') ' . $refTechChapter->name;
                        }

                        ?>

                        <input class="form-control" type="text" disabled value="{{$product_sub_group_code or ''}}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
