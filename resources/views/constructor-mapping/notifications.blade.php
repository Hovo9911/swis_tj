<div id="tab-notifications" class="tab-pane">
    <div class="panel-body">
        @foreach($fixRoles as $role)
            <div class="form-group">
                <label class="col-lg-2 control-label">{{ trans('swis.constructor.mapping.notification.'.$role) }}</label>
                <div class="col-lg-8">
                    <select class="form-control select2" name="{{ $role }}">
                        <option value="">{{ trans('swis.constructor.mapping.notification.template.select') }}</option>
                        @foreach($notesTemplate as $template)
                            <option value="{{ $template->id }}" {{ (!empty($constructorMapping) && $constructorMapping->{$role} == $template->id) ? 'selected' : '' }}>{{ $template->name }}</option>
                        @endforeach
                    </select>
                    <div class="form-error" id="form-error-{{$role}}"></div>
                </div>
            </div>
        @endforeach
    </div>
</div>