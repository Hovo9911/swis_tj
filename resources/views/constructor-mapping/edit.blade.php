@extends('layouts.app')

@section('title'){{trans('swis.constructor_mapping.title')}}@stop

@section('contentTitle') {{trans('swis.constructor_mapping.mapping_title')}} @stop

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('topScripts')
    <script>
        var constructorDocumentId  = "{{$selectedConstructorDocumentId or $constructorMapping->constructor_document_id}}";
    </script>
@endsection

@section('content')
    <?php

    switch ($saveMode) {
        case 'add':
            $url = 'constructor-mapping';
            break;
        default:
            $url = 'constructor-mapping/update/' . $constructorMapping->id;

            $mls = $constructorMapping->ml()->notDeleted()->base(['lng_id', 'description', 'show_status'])->keyBy('lng_id');
            break;
    }

    $languages = activeLanguages();

    $constructorDocumentId = $selectedConstructorDocumentId ?? $constructorMapping->document_id;
    $constructorDocument = \App\Models\ConstructorDocument\ConstructorDocument::with(['states','roles','actions'])->where('id', $constructorDocumentId)->first();

    ?>

    <form class="form-horizontal fill-up " id="form-data" action="{{urlWithLng($url)}}">

        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-general"> {{trans('core.base.tab.general')}}</a></li>
                <li><a data-toggle="tab" href="#tab-notifications"> {{trans('swis.constructor.mapping.tab.notification.title')}}</a></li>
                <li><a data-toggle="tab" href="#tab-rules"> {{trans('swis.constructor.mapping.tab.rules.title')}}</a></li>
                @foreach($languages as $language)
                    <li>
                        <a data-toggle="tab" href="#tab-{{$language->code}}">{{ trans("core.base.language.".strtolower($language->name)) }}</a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content">
                <div id="tab-general" class="tab-pane active">
                    <div class="panel-body">

                        <div class="form-group required">
                            <label class="col-lg-2 control-label">{{trans('swis.constructor_mapping.document.label')}}</label>
                            <div class="col-lg-8">
                                <select class="form-control select2" name="document_id" id="document-id">
                                    <option value="">{{trans('swis.constructor_mapping.document.select.label')}}</option>
                                    @foreach($constructorDocuments as $document)
                                        <option {{($constructorDocumentId == $document->id) ? 'selected': '' }} value="{{$document->id}}">{{$document->current()->document_name}}</option>
                                    @endforeach
                                </select>
                                {!! linkToModule('constructor-documents',$constructorDocumentId) !!}
                                <div class="form-error" id="form-error-document_id"></div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-lg-2 control-label">{{trans('swis.constructor_mapping.start_state.label')}}</label>
                            <div class="col-lg-8">
                                <select class="form-control select2" name="start_state">
                                    <option value="">{{trans('swis.constructor_mapping.start_state.select.label')}}</option>
                                    @if(!is_null($constructorDocument))
                                        @foreach($constructorDocument->states as $state)
                                            <option {{(isset($constructorMapping) && $state->id == $constructorMapping->start_state) ? 'selected': '' }} value="{{$state->id}}">{{$state->current()->state_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                {!! linkToModule('constructor-states',$constructorMapping->start_state ?? '') !!}
                                <div class="form-error" id="form-error-start_state"></div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-lg-2 control-label">{{trans('swis.constructor_mapping.role.label')}}</label>
                            <div class="col-lg-8">
                                <select class="form-control select2" name="role">
                                    <option value="">{{trans('swis.constructor_mapping.role.select.label')}}</option>
                                    @if(!is_null($constructorDocument))
                                        @foreach($constructorDocument->roles as $role)
                                            <option {{(isset($constructorMapping) && $role->role_id == $constructorMapping->role) ? 'selected': '' }} value="{{$role->role_id}}">{{!is_null($role->role) ? $role->role->name : ''}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if($constructorDocumentId)
                                {!! linkToModuleByHref('constructor-roles?documentID='.$constructorDocumentId) !!}
                                @endif
                                <div class="form-error" id="form-error-role"></div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-lg-2 control-label">{{trans('swis.constructor_mapping.actions.label')}}</label>
                            <div class="col-lg-8">
                                <select class="form-control select2" name="action">
                                    <option value="">{{trans('swis.constructor_mapping.actions.select.label')}}</option>
                                    @if(!is_null($constructorDocument))
                                        @foreach($constructorDocument->actions as $action)
                                            <option {{(isset($constructorMapping) && $action->id == $constructorMapping->action) ? 'selected': '' }} value="{{$action->id}}">{{$action->current()->action_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                {!! linkToModule('constructor-actions',$constructorMapping->action ?? '') !!}
                                <div class="form-error" id="form-error-action"></div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-lg-2 control-label">{{trans('swis.constructor_mapping.end_state.label')}}</label>
                            <div class="col-lg-8">
                                <select class="form-control select2" name="end_state">
                                    <option value="">{{trans('swis.constructor_mapping.end_state.select.label')}}</option>
                                    @if(!is_null($constructorDocument))
                                        @foreach($constructorDocument->states as $state)
                                            <option {{(isset($constructorMapping) && $state->id == $constructorMapping->end_state) ? 'selected': '' }} value="{{$state->id}}">{{$state->current()->state_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                {!! linkToModule('constructor-states',$constructorMapping->end_state ?? '') !!}
                                <div class="form-error" id="form-error-end_state"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('swis.constructor_mapping.non_visible_list.label')}}</label>
                            <div class="col-lg-8">
                                <select class="form-control select2" name="non_visible_list">
                                    <option value="">{{trans('swis.constructor_mapping.non_visible_list.select.label')}}</option>
                                    @if(isset($lists['nonVisibleList']) && $constructorDocument)
                                        @foreach($lists['nonVisibleList'] as $list)
                                            <option {{ (isset($constructorMapping) && $constructorMapping->non_visible_list == $list->id) ? 'selected' : ''}} value="{{$list->id}}">{{"(".$list->id.") ".$list->description}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                {!! linkToModule('constructor-lists',$constructorMapping->non_visible_list ?? '') !!}
                                <div class="form-error" id="form-error-non_visible_list"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('swis.constructor_mapping.editable_list.label')}}</label>
                            <div class="col-lg-8">
                                <select class="form-control select2" name="editable_list">
                                    <option value="">{{trans('swis.constructor_mapping.editable_list.select.label')}}</option>
                                    @if(isset($lists['editableList']) && $constructorDocument)
                                        @foreach($lists['editableList'] as $list)
                                            <option {{ (isset($constructorMapping) && $constructorMapping->editable_list == $list->id) ? 'selected' : ''}} value="{{$list->id}}">{{"(".$list->id.") ".$list->description}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                {!! linkToModule('constructor-lists',$constructorMapping->editable_list ?? '') !!}
                                <div class="form-error" id="form-error-editable_list"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label">{{trans('swis.constructor_mapping.mandatory_list.label')}}</label>
                            <div class="col-lg-8">
                                <select class="form-control select2" name="mandatory_list">
                                    <option value="">{{trans('swis.constructor_mapping.mandatory_list.select.label')}}</option>
                                    @if(isset($lists['mandatoryList']) && $constructorDocument)
                                        @foreach($lists['mandatoryList'] as $list)
                                            <option {{ (isset($constructorMapping) && $constructorMapping->mandatory_list == $list->id) ? 'selected' : ''}} value="{{$list->id}}">{{"(".$list->id.") ".$list->description}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                {!! linkToModule('constructor-lists',$constructorMapping->mandatory_list ?? '') !!}
                                <div class="form-error" id="form-error-mandatory_list"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label"
                                   for="obligationExist">{{trans('swis.constructor_mapping.deny_action_obligation_exist.label')}}</label>
                            <div class="col-lg-8">
                                <input type="checkbox" id="obligationExist"
                                       name="obligation_exist" {{isset($constructorMapping) && $constructorMapping->obligation_exist ? 'checked' : ''}}>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label"
                                   for="enableRisks">{{trans('swis.constructor_mapping.enable_risks.label')}}</label>
                            <div class="col-lg-8">
                                <input type="checkbox" id="enableRisks"
                                       name="enable_risks" {{isset($constructorMapping) && $constructorMapping->enable_risks ? 'checked' : ''}}>
                            </div>
                        </div>
                        @if(config('global.digital_signature'))
                            <div class="form-group">
                                <label class="col-lg-2 control-label">{{trans('swis.constructor_mapping.digital_signature.label')}}</label>
                                <div class="col-lg-8">
                                    <input type="checkbox"
                                           name="digital_signature" {{isset($constructorMapping) && $constructorMapping->digital_signature ? 'checked' : ''}}>
                                </div>
                            </div>
                        @endif

                        <div class="form-group">
                            <label class="control-label col-lg-2">{{trans('core.base.label.status')}}</label>
                            <div class="col-lg-8 general-status">
                                <select name="show_status" class="form-show-status">
                                    <option value="{{\App\Models\ConstructorMapping\ConstructorMapping::STATUS_ACTIVE}}"{{(isset($constructorMapping) && $constructorMapping->show_status == \App\Models\ConstructorMapping\ConstructorMapping::STATUS_ACTIVE) || $saveMode == 'add' ?  ' selected="selected"' : ''}}>{{trans('core.base.label.status.active')}}</option>
                                    <option value="{{\App\Models\ConstructorMapping\ConstructorMapping::STATUS_INACTIVE}}"{{isset($constructorMapping) &&  $constructorMapping->show_status == \App\Models\ConstructorMapping\ConstructorMapping::STATUS_INACTIVE ? ' selected="selected"': ''}}>{{trans('core.base.label.status.inactive')}}</option>
                                </select>
                                <div class="form-error" id="form-error-show_status"></div>
                            </div>
                        </div>
                    </div>
                </div>

                {{----------------- Notification part --------------------}}
                @include('constructor-mapping.notifications')

                {{----------------- Rules part --------------------}}
                @include('constructor-mapping.rules')

                @foreach($languages as $language)
                    <?php $lngId = $language->id ?>
                    <div id="tab-{{$language->code}}" class="tab-pane">
                        <div class="panel-body">
                            <div class="form-group"><label
                                        class="col-lg-2 control-label">{{trans('core.base.description')}}</label>
                                <div class="col-lg-8">
                                    <textarea rows="6" name="ml[{{$language->id}}][description]"
                                              class="form-control">{{isset($mls[$lngId]) ? $mls[$lngId]->description : ''}}</textarea>
                                    <div class="form-error"
                                         id="form-error-ml-{{$language->id}}-description"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <input type="hidden" value="{{$constructorMapping->id or 0}}" name="id" class="mc-form-key"/>

            {!! csrf_field() !!}
        </div>
    </form>

@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script src="{{asset('js/constructor-mapping/main.js')}}"></script>
@endsection