<div id="tab-rules" class="tab-pane">
    <div class="panel-body">
        @if (!empty($rules))
            @foreach ($rules as $group => $item)
                @if (!$loop->first)<hr /> @endif
                <h2>{{ trans('swis.constructor_rules.'.$group) }}</h2>
                @if (!empty($item))
                    @foreach ($item as $rule)
                        <label for="rule-{{ $rule['id'] }}">
                            <input type="checkbox" id="rule-{{ $rule['id'] }}" name="rules[]" {{ (isset($rule['checked']) && $rule['checked']) ? 'checked' : '' }} value="{{ $rule['id'] }}"> <a href="{{urlWithLng('constructor-rules/'.$constructorDocumentId.'/'.$group.'/'.$rule['id'].'/edit')}}" target="_blank">({{ $rule['id'] }})</a> {{ $rule['name'] }}
                        </label><br />
                    @endforeach
                @endif
            @endforeach
        @endif
    </div>
</div>