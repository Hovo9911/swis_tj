@extends('layouts.app')

@section('title'){{trans('swis.folder.title')}}@stop

@section('contentTitle') {{trans('swis.folder.title')}} @stop

@section('form-buttons-top')
    {!! renderAddButton() !!}
    {!! renderDeleteButton() !!}
@stop

@section('content')

    <div class="clearfix search__out">
        <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse" data-target="#collapseSearch" aria-expanded="false" aria-controls="collapseSearch">
            <i class="fa fa-search"> </i><span class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
        </button>
    </div>

    <div class="collapse" id="collapseSearch">
        <div class="card card-body">
            <form class="form-horizontal fill-up" id="search-filter" autocomplete="off">

                        {{--<div class="form-group"><label class="col-lg-4 control-label">ID</label>--}}
                            {{--<div class="col-lg-4">--}}
                                {{--<input type="text" class="form-control" name="id" id="" value="">--}}
                                {{--<div class="form-error" id="form-error-id"></div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.name')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="name" id="" value="">
                                <div class="form-error" id="form-error-name"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.folder.access_password')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="access_password" id="" value="">
                                <div class="form-error" id="form-error-access_password"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.description')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="text" class="form-control" name="description" id="" value="">
                                <div class="form-error" id="form-error-description"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8 col-lg-offset-3 text-right">
                        <div class="row">
                            <div class="col-lg-10 col-md-8">
                                <button type="submit" class="btn btn-primary mr-10">{{trans('core.base.label.search')}}</button>
                                <button type="button" class="btn btn-danger resetButton" data-submit="true">{{trans('core.base.label.reset')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="data-table">
        <div class="clearfix">
            <table class="table table-striped table-bordered table-hover dataTables-example" id="list-data">
                <thead>
                    <tr>
                        <th data-col="actions" class="th-actions">{{trans('core.base.label.actions')}}</th>
                        <th data-col="check" class="th-checkbox"><input type="checkbox" name=""></th>
                        <th data-col="incrementId">{{trans('swis.folder.regular_number')}}</th>
                        <th data-col="folder_access_password">{{trans('swis.folder.access_password')}}</th>
                        <th data-ordering="0" data-col="folder_documents_count">{{trans('swis.folder.documents_count')}}</th>
                        <th data-col="folder_name">{{trans('core.base.label.name')}}</th>
                        <th data-col="folder_description">{{trans('core.base.label.description')}}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('js/folder/main.js')}}"></script>
@endsection