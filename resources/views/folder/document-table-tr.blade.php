<?php
$gUploader = new \App\GUploader('document_report');
?>
@if(empty($renderInput))

    {{--  Active Docs  --}}
    @if(count($documents))
        <?php $num = $num ?? 1 ?>
        @foreach($documents as $document)

            <?php $docEditType = 'edit'; ?>
            @if($document->document_form == \App\Models\Document\Document::DOCUMENT_FORM_ELECTRONIC)
                <?php $docEditType = 'view'; ?>
            @endif

            <tr data-num="{{$num}}">
                <td class="text-center"><input type="checkbox" value="{{$document->id}}"></td>
                <td><input type="hidden" name="documents[]" class="document" value="{{$document->id}}">{{$num}}</td>
                <td>{{trans('swis.document.document_form.'.$document->document_form.'.title')}}</td>
                <td class="text-break-word">{{$document->document_access_password}} </td>
                <td>{{$document->document_type}}</td>
                <td class="text-break-word">{{$document->document_name}}</td>
                <td>{{$document->document_description}}</td>
                <td>{{$document->document_number}}</td>
                <td>{{$document->document_release_date}} </td>
                <td>{{$document->document_status}}</td>
                <td class="text-break-word">{{$document->fileBaseName()}}</td>
                <td class="text-nowrap text-center">

                    @if($document->isDocumentOwnerOrCreator())
                        <a target="_blank" href="{{urlWithLng('/document/'.$document->id.'/'.$docEditType)}}"  class="btn btn-edit" data-toggle="tooltip" title="{{trans('swis.base.tooltip.edit.button')}}"><i class="fa fa-pen"></i></a>
                    @endif

                    <a target="_blank" href="{{$document->filePath()}}" class="btn btn-success btn-xs btn--icon viewPDF" title="{{ trans('swis.base.tooltip.view.button') }}" data-toggle="tooltip"><i class="fas fa-eye"></i></a>

                    <button class="btn-remove rowData delete" data-id="{{$document->id}}" title="{{ trans('swis.base.tooltip.unlink.button') }}" data-toggle="tooltip"><i class="fas fa-times"></i></button>
                    <button class="rowData hide cancelAction" title="{{ trans('swis.base.tooltip.unlink.button') }}" data-toggle="tooltip"><i class="fas fa-times"></i></button>
                </td>
            </tr>

            <?php $num++ ?>
        @endforeach

    @endif

    {{--  Inactive Docs  --}}
    @if (count($inactiveDocuments))
        <?php empty($num) ? $numInactive = 1 : $numInactive = $num; ?>

        @foreach ($inactiveDocuments as $inactiveDocument)
            @if ($inactiveDocument->isDocumentAccessPasswordChanged($inactiveDocument->uuid) && !($inactiveDocument->isDocumentOwnerOrCreator()))
                <tr data-num="{{$num}}" style="background-color: #fbcaca">
                    <td>{{$numInactive}}</td>
                    <td>{{trans('swis.document.document_form.'.$inactiveDocument->document_form.'.title')}}</td>
                    <td class="text-break-word">{{$inactiveDocument->document_access_password}} </td>
                    <td>{{$inactiveDocument->document_type}}</td>
                    <td class="text-break-word">{{$inactiveDocument->document_name}}</td>
                    <td>{{$inactiveDocument->document_description}}</td>
                    <td>{{$inactiveDocument->document_number}}</td>
                    <td>{{$inactiveDocument->document_release_date}} </td>
                    <td>{{$inactiveDocument->document_status}}</td>
                    <td class="text-break-word">{{$inactiveDocument->fileBaseName()}}</td>
                    <td class="text-nowrap">
                        <a target="_blank" href="{{$inactiveDocument->filePath()}}" class="btn btn-success btn-xs btn--icon viewPDF" title="{{ trans('swis.base.tooltip.view.button') }}" data-toggle="tooltip"><i class="fas fa-eye"></i></a>
                        {{trans('swis.folder.document.document_access_denied')}}
                    </td>
                </tr>
                <?php
                $numInactive++; ?>
            @endif
        @endforeach

    @endif

@else

    <tr data-num="{{$num}}">
        <td class="text-center"><input type="checkbox" value="" class="hidden doc-checkbox"></td>
        <td><input type="hidden" name="documents[]" class="document" value="">{{$num}}</td>
        <td class="document-form"></td>
        <td class="access-password"></td>
        <td>
            <input type="text" autocomplete="off" class="autocomplete form-control"
                   data-ref="{{\App\Models\ReferenceTable\ReferenceTable::getReferenceIdByTableName(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS)}}"
                   data-ref-type="autocomplete"
                   data-fields="{{json_encode([['from'=>'name','to'=>'document_name'],['from'=>'code','to'=>'document_type']])}}"
                   placeholder="autocomplete"
                   name="document_type">

            <input type="hidden" name="document_type_value" value="{{$document->document_type_value or ''}}">
            <div class="form-error" id="form-error-document_type-{{$num}}"></div>
        </td>
        <td>
            <input type="text" class="form-control" name="document_name" readonly="readonly">
            <div class="form-error" id="form-error-document_name-{{$num}}"></div>
        </td>
        <td>
            <input type="text" class="form-control" name="document_description">
            <div class="form-error" id="form-error-document_description-{{$num}}"></div>
        </td>
        <td>
            <input type="text" class="form-control" name="document_number">
            <div class="form-error" id="form-error-document_number-{{$num}}"></div>
        </td>
        <td style="min-width: 200px">
            <div class='input-group date'>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                <input type="text" class="form-control" placeholder="{{setDatePlaceholder()}}" autocomplete="off" name="document_release_date">
            </div>
            <div class="form-error" id="form-error-document_release_date-{{$num}}"></div>
        </td>
        <td><span class="doc-status"></span>
        </td>
        <td class="text-break-word">
            <div class="g-upload-container">
                <div class="g-upload-box">
                    <label style="width:35px" title="{{ trans('swis.base.tooltip.upload_file.button') }}" data-toggle="tooltip"><i class="fas fa-upload btn btn-success btn-sm"></i> <input style="visibility: hidden" class="dn g-upload" type="file" name="document_file" {{$gUploader->isMultiple() ? 'multiple' : ''}} data-conf="document_report" data-action="{{urlWithLng('/g-uploader/upload')}}" accept="{{$gUploader->getAcceptTypes()}}"/></label>
                </div>

                <div class="license-form__uploaded-file-list g-uploaded-list "></div>
                {{-- Progress bar for file uploding process, if you want to show process--}}
                <progress class="progressBarFileUploader" value="0" max="100"></progress>
                {{-- Progress bar for file uploding process --}}
                <div class="form-error-text form-error-files fb fs12"></div>

                <div class="form-error" id="form-error-document_file-{{$num}}"></div>
            </div>
        </td>
        <td class="actions-list text-nowrap text-center">
            <button type="button" class="btn btn-primary btn-xs storeDocument btn--icon" title="{{ trans('swis.base.tooltip.store_document.button') }}" data-toggle="tooltip"><i class="fa fa-check"></i></button>
            <button class="delete" title="{{ trans('swis.base.tooltip.unlink_document.button') }}" data-toggle="tooltip"><i class="fas fa-times"></i></button>
        </td>
    </tr>

@endif