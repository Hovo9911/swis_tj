@if($saveMode == 'edit')
    <p class="notes__title ver-top-box"><b>{{ trans('swis.folders.notes.current_state') }}
            -</b> {{ trans("swis.documents.notes.state.saved") }}</p>
    @if(count($notes) > 0)
        <div class="ibox-content no-padding notes__out">
            <div class="notes-block">
                <ul class="list-group notes__list">
                    @foreach ($notes as $note)
                        <li class="list-group-item notes__item">
                            <div class="notes__item--inner">
                                <div class="table-cell notes__item--left">
                                    <p>{!! $note->note !!}</p>
                                </div>

                                <small class="ver-top-box text-navy show_diff_text collapseWithIcon"
                                       data-toggle="collapse" data-target="#notes-{{ $note->version }}">
                                    {!! (isset($note->old_data, $note->new_data)) ? '<i class="fas fa-chevron-down"></i>' : ''  !!}
                                </small>
                                {{-- {{ $note->note }}
                                 <small class="text-navy show_diff_text collapseWithIcon"  data-toggle="collapse" data-target="#notes-{{ $note->version }}">
                                     {!! (isset($note->old_data, $note->new_data)) ? 'Show Diff ( '. trans('swis.folder.notes.version') .' '. ($note->version - 1) .' - ' . $note->version . ' ) <i class="fas fa-chevron-down"></i>' : ''  !!}
                                 </small>

                                 --}}
                                <div class="table-cell text-right notes__item--right">
                                    <small class="ver-top-box text-muted"><i
                                                class="fa fa-clock-o"></i> {{ $note->created_at}}</small>
                                </div>
                            </div>


                            @if (isset($note->old_data, $note->new_data))
                                <div class="collapse" id="notes-{{$note->version}}">
                                    <div class="table-notes__out">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                            <tr>
                                                <td>{{trans('core.base.label.field')}}</td>
                                                <td>{{trans('core.base.label.old_data')}}</td>
                                                <td>{{trans('core.base.label.new_data')}}</td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {!! recursiveRenderLog($note->old_data, $note->new_data) !!}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
@endif

<div class="form-group">
    <label class="col-md-4 col-lg-3 control-label label__title">{{ trans('swis.folders.notes.textarea_title') }}</label>
    <div class="col-md-8">
        <div class="row">
            <div class="col-lg-10 col-md-8 field__one-col">
                <textarea rows="5" cols="5" name="notes" class="form-control"></textarea>
                <div class="form-error" id="form-error-notes"></div>
            </div>
        </div>
    </div>
</div>