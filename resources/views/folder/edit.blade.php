@extends('layouts.app')

@section('title'){{trans('swis.folder.create.title')}}@stop

@section('contentTitle')
    {{trans('swis.folder.create.title')}}
@stop

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('content')

    <?php

    switch ($saveMode) {
        case 'add':
            $url = 'folder';
            break;
        case 'edit':
            $url = "folder/update/{$folder->id}";
            break;
        case 'view':
            $url = '';
            break;
    }

    $showInfo = false;
    if ($saveMode ==  \App\Models\Document\Document::VIEW_MODE_EDIT || $saveMode == \App\Models\Document\Document::VIEW_MODE_VIEW) {
        $showInfo = true;
    }

    $authUserCreator = Auth::user()->getCreator();
    ?>

    <form class="form-horizontal fill-up" id="form-data" action="{{urlWithLng($url)}}" autocomplete="off">
        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-general">{{trans('core.base.tab.general')}}</a></li>
                <li><a data-toggle="tab" href="#tab-sides">{{trans('swis.single_app.sides')}}</a></li>
                <li><a data-toggle="tab" href="#tab-documents">{{trans('swis.document.title')}}</a></li>
                @if($saveMode == \App\Models\Document\Document::VIEW_MODE_ADD || $folder->isFolderOwnerOrCreator())
                <li><a data-toggle="tab" href="#tab-notes">{{trans('swis.single_app.notes')}}</a></li>
                @endif
            </ul>
            <div class="tab-content">
                <div id="tab-general" class="tab-pane active">
                    <div class="panel-body">

                        @if($showInfo)
                            <div class="form-group">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.document.access_password')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input value="{{$folder->folder_access_password or ''}}"
                                                   name="folder_access_password"
                                                   disabled
                                                   type="text" placeholder="" class="form-control" autocomplete="off">
                                            <div class="form-error" id="form-error-access_password"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.document.regenerate_access_key')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input value="1"
                                                   name="regenerate_access_key"
                                                   type="checkbox" placeholder="" autocomplete="off">
                                            <div class="form-error" id="form-error-regenerate_access_key"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="form-group required">
                            <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.name')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-lg-10 col-md-8 field__one-col">
                                        <input value="{{$folder->folder_name or ''}}" name="folder_name" type="text"
                                               placeholder=""
                                               class="form-control">
                                        <div class="form-error" id="form-error-folder_name"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.description')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-lg-10 col-md-8 field__one-col">
                                        <textarea name="folder_description"
                                                  placeholder="" class="form-control"
                                                  rows="5">{{$folder->folder_description or ''}}</textarea>
                                        <div class="form-error" id="form-error-folder_description"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div id="tab-sides" class="tab-pane">
                    <div class="panel-body  p-lr-n">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                {{trans('swis.document.holder.title')}}
                            </div>
                            <div class="panel-body" id="holderInfo">
                                <div class="form-group required">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.legal_entity_form')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <?php $legalEntities = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_LEGAL_ENTITY_FORM); ?>

                                                <select class="form-control legalEntityType "
                                                        data-type="holderTaxType" name="holder_type">
                                                    <option disabled="" selected value="">select</option>
                                                    @if(count($legalEntities) > 0)
                                                        @foreach($legalEntities as $entity)
                                                            <option value="{{$entity->id}}" {{(!empty($folder) && $folder->holder_type == $entity->id) ? 'selected' : '' }}>{{$entity->name}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                <div class="form-error" id="form-error-holder_type"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group required">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.base.label.tax_id_or_ssn')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <input value="{{$folder->holder_type_value or ''}}"
                                                       name="holder_type_value" type="text" placeholder=""
                                                       data-type="holderTaxType"
                                                       @if(\Auth::user()->isBroker())
                                                       data-fields="{{json_encode([['from'=>'name','to'=>'holder_name'],['from'=>'country','to'=>'holder_country'],['from'=>'address','to'=>'holder_address'],['from'=>'community','to'=>'holder_community']])}}"
                                                       @endif
                                                       class="form-control taxIdOrSSN getPartiesInfo">
                                                <div class="form-error" id="form-error-holder_type_value"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group required">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.passport')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <input value="{{$folder->holder_passport or ''}}" name="holder_passport" type="text"  placeholder="" class="form-control passportField getPartiesInfo onlyLatinAlpha">
                                                <div class="form-error" id="form-error-holder_passport"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group required">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.base.name')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <input value="{{$folder->holder_name or ''}}" readonly name="holder_name"
                                                       type="text"
                                                       placeholder=""
                                                       class="form-control f-citizen">
                                                <div class="form-error" id="form-error-holder_name"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php $refData = getReferenceRows(\App\Models\ReferenceTable\ReferenceTable::REFERENCE_COUNTRIES); ?>

                                <div class="form-group required">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.country')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <select name="holder_country"
                                                        class="form-control select2 readonly f-citizen">
                                                    <option value=""></option>
                                                    @if(count($refData) > 0)
                                                        @foreach($refData as $rVal)
                                                            <option {{(!empty($folder->holder_country) && ($folder->holder_country == $rVal->id) ? 'selected' : '')}} value="{{$rVal->id}}">{{'('.$rVal->code.') '.$rVal->name}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                <div class="form-error" id="form-error-holder_country"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.community')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <input value="{{$folder->holder_community or ''}}"
                                                       name="holder_community"
                                                       type="text" placeholder=""
                                                       class="form-control">
                                                <div class="form-error" id="form-error-holder_community"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group required">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.address')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <input value="{{$folder->holder_address or ''}}"
                                                       name="holder_address"
                                                       type="text" placeholder=""
                                                       class="form-control">
                                                <div class="form-error" id="form-error-holder_address"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row required">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.phone')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <div class="input-group">
                                                    <span class="input-group-addon">+</span>
                                                    <input type="text"
                                                           placeholder=""
                                                           class="form-control onlyNumbers"
                                                           name="holder_phone_number"
                                                           value="{{$folder->holder_phone_number or ''}}"
                                                           autocomplete="off">
                                                </div>
                                                <div class="form-error" id="form-error-holder_phone_number"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{--<div class="form-group required"><label--}}
                                {{--class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.phone')}}</label>--}}
                                    {{--<div class="col-md-8">--}}
                                        {{--<div class="row">--}}
                                            {{--<div class="col-lg-10 col-md-8 field__one-col">--}}
                                {{--<input value="{{$folder->holder_phone_number or ''}}"--}}
                                {{--name="holder_phone_number"--}}
                                {{--type="text" placeholder=""--}}
                                {{--class="form-control">--}}
                                {{--<div class="form-error" id="form-error-holder_phone_number"></div>--}}
                                {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                <div class="form-group required">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.email')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <input value="{{$folder->holder_email or ''}}" name="holder_email"
                                                       type="text" placeholder=""
                                                       class="form-control">
                                                <div class="form-error" id="form-error-holder_email"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                {{trans('swis.document.creator.title')}}
                            </div>

                            <div class="panel-body">

                                <div class="form-group">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.tax_id_or_id_card')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <input value="{{$folder->creator_type_value or $authUserCreator->ssn}}"
                                                       id="creator_ssn"
                                                       disabled
                                                       type="text" placeholder=""
                                                       class="form-control">
                                                <div class="form-error" id="form-error-creator_type_value"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.base.name')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">

                                                <input value="{{$folder->creator_name or $authUserCreator->name}}"
                                                       id="creator_name"
                                                       disabled type="text"
                                                       placeholder=""
                                                       class="form-control">
                                                <div class="form-error" id="form-error-creator_name"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.address')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <input value="{{$folder->creator_address or $authUserCreator->address}}"
                                                       id="creator_address"
                                                       disabled
                                                       type="text" placeholder=""
                                                       class="form-control">
                                                <div class="form-error" id="form-error-creator_address"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.phone')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <input value="{{$folder->creator_phone_number or $authUserCreator->phone_number}}"
                                                       id="creator_phone_number"
                                                       disabled
                                                       type="text" placeholder=""
                                                       class="form-control">
                                                <div class="form-error" id="form-error-creator_phone"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.email')}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <input value="{{$folder->creator_email or $authUserCreator->email}}"
                                                       id="creator_email"
                                                       disabled
                                                       type="text" placeholder=""
                                                       class="form-control">
                                                <div class="form-error" id="form-error-creator_phone"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

                <div id="tab-documents" class="tab-pane">
                    <div class="panel-body  p-lr-n">

                        <div class="search__out clearfix">
                            <button class="btn pull-right search__btn" type="button" data-toggle="collapse" data-target="#documentSearchFilter" aria-expanded="false" aria-controls="collapseSearch">
                                <i class="fa fa-search"> </i><span class="search__txt ver-top-box">{{ trans('swis.document.search.title') }}</span>
                            </button>
                        </div>

                        <div class="collapse" id="documentSearchFilter">

                            <div class="form-group">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.document.access_password')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input value="" name="document_access_password" type="text" placeholder="" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.document.type')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <select class="form-control select2-ajax document-search-inputs"
                                                    id="searchDocumentType"
                                                    name="document_type"
                                                    data-url="{{urlWithLng('/reference-table/get-document-type')}}"
                                                    data-min-input-length="1">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.description')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input name="document_description" placeholder="" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.document.number')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input value="" name="document_number" type="text" placeholder="" class="form-control">
                                            <input value="folder" name="moduleForDocuments" class="no-reset" type="hidden">
                                            <input value="{{$folder->id or 0}}" name="folder_id" class="no-reset" type="hidden">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.document.release_date')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-4 col-lg-5 field__two-col">
                                            <div class='input-group date'>
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                <input value="" name="document_release_date_start" autocomplete="off" type="text" placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                            </div>
                                        </div>


                                        <div class="col-md-4 col-lg-5 field__two-col">
                                            <div class='input-group date'>
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                <input value="" name="document_release_date_end" autocomplete="off" type="text" placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.folder_name')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input value="" name="search_folder_name" type="text" placeholder="" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 col-lg-3 control-label label__title">{{trans('swis.folder_desc')}}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8 field__one-col">
                                            <input placeholder="" name="search_folder_desc" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-8 col-lg-offset-3 text-right">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-8">
                                            <button type="button" class="btn btn-primary documentSearch mr-10">{{trans('core.base.label.search')}}</button>
                                            <button type="button" data-search-button="documentSearch" class="btn btn-danger resetButton">{{trans('core.base.label.reset')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row searchDoc hide">
                            <br/>
                            <div class="col-md-12">
                                <div class="panel panel-default" id="documentSearchResult">
                                    <div class="panel-heading">
                                        {{trans('swis.folder.document.search_result')}}
                                    </div>
                                    <div class="panel-body">

                                        <table class="table table-hover table-bordered" id="search-document">
                                            <thead>
                                                <tr>
                                                    <th data-ordering="false" data-col="document_file"></th>
                                                    <th data-col="check" class="th-checkbox"><input type="checkbox"></th>
                                                    <th data-col="id">ID</th>
                                                    <th data-col="document_form">{{trans('swis.document.document_form')}}</th>
                                                    <th data-col="document_access_password">{{trans('swis.document.access_password')}}</th>
                                                    <th data-col="document_type">{{trans('swis.document.type')}}</th>
                                                    <th data-col="document_description">{{trans('core.base.description')}}</th>
                                                    <th data-col="document_number">{{trans('swis.document.number')}}</th>
                                                    <th data-col="document_release_date">{{trans('swis.document.release_date')}}</th>
                                                    <th data-col="document_status">{{trans('core.base.label.show_status')}}</th>
                                                </tr>
                                            </thead>
                                        </table>

                                        <div class="text-center">
                                            <button class="btn btn-primary addDocument"><i class="fa fa-plus"></i><span class="documents-plusBtn__text"> {{trans('swis.folder.document.add_document')}}</span></button>
                                            <button type="button" class="btn btn-secondary closeResultTab" data-div="searchDoc" data-type="class">{{trans('core.base.button.close.search_result')}}</button>
                                        </div>

                                        <div id="documentErrorInfo"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php
                        $folderCount = isset($folder) ? $folder->documents->count() : 0;
                        ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default doc-panel" id="documentAddProductsList">
                                    <div class="panel-heading">
                                        {{trans('swis.folder.document.document_info')}}
                                    </div>
                                    <div class="panel-body">
                                        <div class="table-responsive {{!$folderCount ? 'hide' : ''}}">
                                        <table class="table table-hover table-bordered" id="documentList">
                                            <thead>
                                                <tr>
                                                    <th style="width: 30px" class="text-center"><input id="checkAllDocuments" type="checkbox"></th>
                                                    <th style="width: 30px">#</th>
                                                    <th style="width: 90px">{{trans('swis.document.document_form')}}</th>
                                                    <th style="width: 120px">{{trans('swis.document.access_password')}}</th>
                                                    <th style="width: 100px">{{trans('swis.document.type')}}</th>
                                                    <th style="width: 250px">{{trans('core.base.label.name')}}</th>
                                                    <th style="width: 100px">{{trans('core.base.description')}}</th>
                                                    <th style="width: 100px">{{trans('swis.document.number')}}</th>
                                                    <th style="width: 110px">{{trans('swis.document.release_date')}}</th>
                                                    <th style="width: 100px">{{trans('core.base.label.show_status')}}</th>
                                                    <th style="width: 100px">{{trans('core.base.label.type.file')}}</th>
                                                    <th style="width: 110px;">{{trans('core.base.label.actions')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @if($folderCount)
                                                @include('folder.document-table-tr',['documents'=>$folder->documents,'inactiveDocuments' =>$folder->inactiveDocuments])
                                            @endif
                                            </tbody>
                                        </table>
                                        </div>

                                        <button type="button" class="btn btn-danger m-r {{!$folderCount ? 'hide' : ''}}" id="deleteCheckedDocuments"><i class="fa fa-times"></i> {{trans('swis.folder.documents.delete.multiple.button')}}</button>

                                        <button type="button" class="btn btn-primary plusMultipleFieldsDocument"><i class="fa fa-plus"></i><span class="documents-plusBtn__text"> {{trans('swis.folder.document.add_document_button_title')}}</span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>

                @if($saveMode == \App\Models\Document\Document::VIEW_MODE_ADD || $folder->isFolderOwnerOrCreator())
                <div id="tab-notes" class="tab-pane">
                    <div class="panel-body  p-lr-n">
                        @include('folder.folder-notes')
                    </div>
                </div>
                @endif

                <input type="hidden" value="{{ $folder->id or 0 }}" id="folderId" name="folder_id"/>
            </div>
        </div>
    </form>

@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script src="{{asset('assets/helix/core/plugins/bootstrap-typeahead/bootstrap-typeahead.js')}}"></script>
    <script src="{{asset('js/folder/main.js')}}"></script>
    <script src="{{asset('js/folder/document.js')}}"></script>
    <script src="{{asset('js/userType.js')}}"></script>
    <script src="{{asset('js/guploader.js')}}"></script>
@endsection