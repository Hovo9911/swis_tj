@extends('layouts.app')

@section('title'){{trans('swis.payment-and-obligations.title')}}  @endsection

@section('contentTitle') {{trans('swis.payment-and-obligations.title')}} @endsection

@section('topScripts')
    <script>
        var defaultCurrency = "{{ config('swis.default_currency') }}";
    </script>
@endsection

@section('content')

    <?php $hidePayButtons = false;
    if (!\Auth::user()->isAgency()) {
        $hidePayButtons = true;
    }

    ?>

    <div class="clearfix search__out">
        <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse"
                data-target="#collapseSearch" aria-expanded="false" aria-controls="collapseSearch">
            <i class="fa fa-search"> </i><span
                    class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
        </button>
        @if(!$hidePayButtons && Auth::user()->hasAccessToEditModule())
            <button class="btn btn-primary " type="button" id="payAllObligations" disabled="">
                <i class="fas fa-money-bill mr-10"> </i> {{trans('swis.payment-and-obligations.pay_all_obligations')}}
                <span id="allTotalPrice" data-value="0"></span> {{ config('swis.default_currency') }}
            </button>
            <button class="btn btn-primary m-l" id="pay-obligation-modal"
                    disabled>{{ trans('swis.payment-and-obligations.pay.btn') }} <span id="totalPrice"
                                                                                       data-value="0">{{ number_format(0, '2', '.', '') .' '. config('swis.default_currency')}}</span>
            </button>
        @endif
    </div>

    <div class="collapse collapseSearch" id="collapseSearch">
        <div class="card card-body">
            <form class="form-horizontal fill-up" id="search-filter" autocomplete="off">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group searchParam saf_number_block_params"
                             data-field="saf_number_block_params">
                            <label class="col-md-4 control-label label__title">{{ trans('swis.payment-and-obligations.saf_number') }}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <input value="" type="text" class="form-control" name="saf_number">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam sub_application_number_block_params"
                             data-field="sub_application_number_block_params">
                            <label class="col-md-4 control-label label__title">{{ trans('swis.payment-and-obligations.sub_application_number') }}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <input value="" type="text" class="form-control" name="sub_application_number">
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if (Auth::user()->isSwAdmin())

                            <div class="form-group searchParam applicant_tin_block_params"
                                 data-field="applicant_tin_block_params">
                                <label class="col-md-4 control-label label__title">{{ trans('swis.payment-and-obligations.applicant_tin') }}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-12 field__one-col">
                                            <input value="" type="text" class="form-control" name="applicant_tin">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam applicant_block_params"
                                 data-field="applicant_block_params">
                                <label class="col-md-4 control-label label__title">{{ trans('swis.payment-and-obligations.applicant') }}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-12 field__one-col">
                                            <input value="" type="text" class="form-control" name="applicant">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam agency_block_params" data-field="agency_block_params">
                                <label class="col-md-4 control-label label__title">{{ trans('swis.payment-and-obligations.agency') }}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-12 field__one-col">
                                            <select class="select2" name="agency">
                                                <option></option>
                                                @if (isset($agencies) && count($agencies) > 0)
                                                    @foreach($agencies as $agency)
                                                        <option value="{{ $agency->tax_id }}">{{ $agency->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @elseif (( Auth::user()->isLocalAdmin() && Auth::user()->isAgency()) || Auth::user()->isNaturalPerson())

                            <div class="form-group searchParam applicant_tin_block_params"
                                 data-field="applicant_tin_block_params">
                                <label class="col-md-4 control-label label__title">{{ trans('swis.payment-and-obligations.applicant_tin') }}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-12 field__one-col">
                                            <input value="" type="text" class="form-control" name="applicant_tin">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam applicant_block_params"
                                 data-field="applicant_block_params">
                                <label class="col-md-4 control-label label__title">{{ trans('swis.payment-and-obligations.applicant') }}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-12 field__one-col">
                                            <input value="" type="text" class="form-control" name="applicant">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="form-group searchParam pay_block_params" data-field="pay_block_params">
                            <label class="col-md-4 control-label label__title">{{ trans('swis.payment-and-obligations.pay') }}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <select class="select2 obligation_pay" name="pay">
                                            <option></option>
                                            @if (isset($booleanValues) && count($booleanValues) > 0)
                                                @foreach ($booleanValues as $booleanValue)
                                                    <option value="{{ $booleanValue->code }}">{{ $booleanValue->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">
                        @if(!Auth::user()->isNaturalPerson())

                            <div class="form-group searchParam tin_importer_exporter_block_params"
                                 data-field="tin_importer_exporter_block_params">
                                <label class="col-md-4 control-label label__title">{{ trans('swis.payment-and-obligations.tin_importer_exporter') }}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-12 field__one-col">
                                            <input value="" type="text" class="form-control"
                                                   name="tin_importer_exporter">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group searchParam importer_exporter_block_params"
                                 data-field="importer_exporter_block_params">
                                <label class="col-md-4 control-label label__title">{{ trans('swis.payment-and-obligations.importer_exporter') }}</label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-12 field__one-col">
                                            <input value="" type="text" class="form-control" name="importer_exporter">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endif

                        <div class="form-group searchParam obligation_type_block_params"
                             data-field="obligation_type_block_params">
                            <label class="col-md-4 control-label label__title">{{ trans('swis.payment-and-obligations.obligation_type') }}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <select class="select2" name="obligation_type">
                                            <option></option>
                                            @if (isset($obligationType) && count($obligationType) > 0)
                                                @foreach ($obligationType as $type)
                                                    @if($type->code == \App\Models\SingleApplicationObligations\SingleApplicationObligations::SAF_OBLIGATIONS_PAYMENT_FOR_SINGLE_WINDOW_SERVICE_CODE)
                                                        @continue
                                                    @endif
                                                    <option value="{{ $type->code }}">{{ "{$type->code}, {$type->name}" }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam budget_line_block_params"
                             data-field="budget_line_block_params">
                            <label class="col-md-4 control-label label__title">{{ trans('swis.payment-and-obligations.budget_line') }}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <input value="" type="text" class="form-control onlyNumbers" name="budget_line">
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if(Auth::user()->isAgency())

                        <div class="form-group searchParam document_block_params" data-field="document_block_params">
                                    <label class="col-md-4 control-label label__title">{{ trans('swis.payment-and-obligations.document') }}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-12 field__one-col">
                                                <select class="select2" name="document">
                                                    <option></option>
                                                    @if (isset($constructorDocuments) && count($constructorDocuments) > 0)
                                                        @foreach ($constructorDocuments as $constructorDocument)
                                                            <option value="{{ $constructorDocument->document_code }}">{{ $constructorDocument->document_name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                        <div class="form-group searchParam subdivision_block_params"
                             data-field="subdivision_block_params">
                            <label class="col-md-4 control-label label__title">{{ trans('swis.payment-and-obligations.subdivision') }}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <select class="select2" name="subdivision_id">
                                            <option value=""></option>
                                            @if (count($subDivisions))
                                                @foreach ($subDivisions as $subDivision)
                                                    <option value="{{ $subDivision->id }}">{{ $subDivision->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>

                <hr/>

                <div class="row">

                    <div class="col-md-12">

                        <div class="form-group searchParam payment_date_row row obligation_pay_date_block_params"
                             data-field="obligation_pay_date_block_params">
                            <label class="col-md-2 control-label label__title">{{ trans('swis.payment-and-obligations.obligation_pay_date_start') }}</label>
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="col-md-6 field__two-col">
                                        <div class='input-group date'>
                                            <span class="input-group-addon"><span
                                                        class="glyphicon glyphicon-calendar"></span></span>
                                            <input value="" name="obligation_pay_date_start" autocomplete="off"
                                                   type="text" disabled placeholder="{{setDatePlaceholder()}}"
                                                   class="form-control obligation_pay_date"/>
                                        </div>
                                    </div>
                                    <div class="col-md-6 field__two-col">
                                        <div class='input-group date'>
                                            <span class="input-group-addon"><span
                                                        class="glyphicon glyphicon-calendar"></span></span>
                                            <input value="" name="obligation_pay_date_end" autocomplete="off"
                                                   type="text" disabled placeholder="{{setDatePlaceholder()}}"
                                                   class="form-control obligation_pay_date"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam obligation_created_date_block_params"
                             data-field="obligation_created_date_block_params">
                            <label class="col-md-2 control-label label__title">{{ trans('swis.payment-and-obligations.obligation_created_date_start') }}</label>
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="col-md-6 field__two-col">
                                        <div class='input-group date'>
                                            <span class="input-group-addon"><span
                                                        class="glyphicon glyphicon-calendar"></span></span>
                                            <input value="" name="obligation_created_date_start" autocomplete="off"
                                                   type="text" placeholder="{{setDatePlaceholder()}}"
                                                   class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="col-md-6 field__two-col">
                                        <div class='input-group date'>
                                            <span class="input-group-addon"><span
                                                        class="glyphicon glyphicon-calendar"></span></span>
                                            <input value="" name="obligation_created_date_end" autocomplete="off"
                                                   type="text" placeholder="{{setDatePlaceholder()}}"
                                                   class="form-control"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam payment_date_row row balance_block_params"
                             data-field="balance_block_params">
                            <label class="col-md-2 control-label label__title">{{ trans('swis.payment-and-obligations.balance_start') }}</label>
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="col-md-6 field__two-col">
                                        <input value="" type="number" class="form-control balance" step="0.01"
                                               name="balance_start">
                                    </div>
                                    <div class="col-md-6 field__two-col">
                                        <input value="" type="number" class="form-control balance" step="0.01"
                                               name="balance_end">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-8 col-lg-offset-3 text-right">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit"
                                                class="btn btn-primary mr-10">{{trans('core.base.label.search')}}</button>
                                        <button type="button" class="btn btn-danger resetButton"
                                                data-submit="true">{{trans('core.base.label.reset')}}</button>
                                        <button type="button" class="btn btn-default hide-or-show-params"
                                                style="margin-top: 10px;">{{trans('core.base.label.apply_hidden_columns')}}</button>
                                        <a href="{{ urlWithLng('/payment-and-obligations/generate/xls') }}"
                                           class="btn btn-default generate-xls"
                                           style="margin-top: 10px;">{{trans('swis.payment-and-obligation.xls.title')}}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>

    @php($companyTaxId = Auth::user()->companyTaxId())
    <div class="data-table">
        <div class="clearfix payment-and-obligation-table">
            @php($colSpan = 10)
            <table class="table table-striped table-bordered table-hover dataTables-example" id="list-data"
                   data-module-for-search-params="PaymentAndObligation{{ hash('sha256', $companyTaxId) }}">
                <thead>
                <tr>
                    @if(!$hidePayButtons)
                        <th data-col="obligation-check" class="th-checkbox" data-key-for-value="">
                            <input type="checkbox"/>
                        </th>
                    @endif
                    <th data-col="print" data-ordering="false" class="text-nowrap"></th>
                    @if(config('swis.obligation_receipt_mode'))
                        <th data-col="saf_obligation_receipt">{{ trans('swis.payment_and_obligations.obligation_receipt') }}</th>
                    @endif
                    <th data-col="id">{{ trans('swis.payment-and-obligations.obligation_id') }}</th>
                    @if (Auth::user()->isSwAdmin())
                        <th data-col="applicant_tin">{{ trans('swis.payment-and-obligations.applicant_tin') }}</th>
                    @elseif (( Auth::user()->isLocalAdmin() && Auth::user()->isAgency()) || (Auth::user()->isNaturalPerson() ))
                        <th data-col="applicant_tin">{{ trans('swis.payment-and-obligations.applicant_tin') }}</th>
                    @endif

                    @if(!Auth::user()->isNaturalPerson())
                        @php($colSpan++)
                        <th data-col="importer_exporter_tin">{{ trans('swis.payment-and-obligations.tin_importer_exporter') }}</th>
                    @endif

                    <th data-col="saf_number">{{ trans('swis.payment-and-obligations.saf_number') }}</th>
                    <th data-col="sub_application_number">{{ trans('swis.payment-and-obligations.sub_application_number') }}</th>
                    <th data-col="agency">{{ trans('swis.payment-and-obligations.agency') }}</th>
                    <th data-col="document">{{ trans('swis.payment-and-obligations.document') }}</th>
                    <th data-col="obligation_type">{{ trans('swis.payment-and-obligations.obligation_type') }}</th>
                    <th data-col="budget_line">{{ trans('swis.payment-and-obligations.budget_line') }}</th>
                    <th data-col="obligation_created_date">{{ trans('swis.payment-and-obligations.obligation_created_date') }}</th>
                    <th data-col="obligation_pay_date">{{ trans('swis.payment-and-obligations.obligation_pay_date') }}</th>
                    <th data-col="obligation_payer" data-ordering="0">{{ trans('swis.single_app.payer') }}</th>
                    <th data-col="obligation_payer_person"
                        data-ordering="0">{{ trans('swis.payment-and-obligations.payer_person') }}</th>
                    <th data-col="obligation">{{ trans('swis.payment-and-obligations.obligation') }}</th>
                    <th data-col="payment">{{ trans('swis.payment-and-obligations.payment') }}</th>
                    <th data-col="balance">{{ trans('swis.payment-and-obligations.balance') }}</th>
                </tr>
                </thead>

                <tbody>

                </tbody>
            </table>
        </div>
    </div>
    <div class="modal fade" id="PayObligationModal" data-backdrop="static" role="dialog"></div>

    <?php
    $searchColumns = [
        'applicant_tin' => trans('swis.payment-and-obligations.applicant_tin'),
        'importer_exporter_tin' => trans('swis.payment-and-obligations.tin_importer_exporter'),
        'saf_number' => trans('swis.payment-and-obligations.saf_number'),
        'sub_application_number' => trans('swis.payment-and-obligations.sub_application_number'),
        'agency' => trans('swis.payment-and-obligations.agency'),
        'document' => trans('swis.payment-and-obligations.document'),
        'obligation_type' => trans('swis.payment-and-obligations.obligation_type'),
        'budget_line' => trans('swis.payment-and-obligations.budget_line'),
        'obligation_created_date' => trans('swis.payment-and-obligations.obligation_created_date'),
        'obligation_pay_date' => trans('swis.payment-and-obligations.obligation_pay_date'),
        'obligation_payer' => trans('swis.single_app.payer'),
        'obligation_payer_person' => trans('swis.payment-and-obligations.payer_person'),
        'obligation' => trans('swis.payment-and-obligations.obligation'),
        'payment' => trans('swis.payment-and-obligations.payment'),
        'balance' => trans('swis.payment-and-obligations.balance')
    ];
    ?>
@endsection

@section('scripts')
    <script src="{{asset('js/payment-and-obligations/main.js')}}"></script>
@endsection
