@if ($saveMode != 'add')
    <div class="col-md-8 col-md-offset-2">
        <div class="ibox-content no-padding notes__out">
            <ul class="list-group notes-block notes__list">

                @foreach ($notes as $note)
                    <?php
                    $showNote = false;
                    if($note->version == 1){
                        $showNote = true;
                    }

                    $recursiveRenderLog = '';
                    if(isset($note->old_data, $note->new_data)){
                        $recursiveRenderLog = recursiveRenderLog($note->old_data, $note->new_data);
                        if($recursiveRenderLog){
                            $showNote = true;
                        }
                    }
                    ?>

                    @if($showNote)
                        <li class="list-group-item notes__item">

                            <div class="notes__item--inner">
                                <div class="table-cell notes__item--left">
                                    <p>{!!$note->note !!}</p>
                                </div>

                                <div class="table-cell text-right notes__item--right">
                                    <small class="block text-muted"><i class="fa fa-clock-o"></i> {{ $note->created_at }}
                                    </small>
                                </div>
                            </div>

                            @if($recursiveRenderLog)
                            <div class="collapse" id="notes-{{$note->version}}">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <td>{{trans('core.base.label.field')}}</td>
                                        <td>{{trans('core.base.label.old_data')}}</td>
                                        <td>{{trans('core.base.label.new_data')}}</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        {!! $recursiveRenderLog !!}
                                    </tbody>
                                </table>
                            </div>
                            @endif
                        </li>
                    @endif
                @endforeach
            </ul>
            <br/>
        </div>
    </div>
@endif