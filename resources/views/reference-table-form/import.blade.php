@extends('layouts.app')

@section('title') {{trans('core.base.button.import')}} @stop

@section('contentTitle')
    {{trans('core.base.button.import')}} {{ $referenceTable->table_name }}
@stop

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('styles')
    <link rel="stylesheet" href="{{asset('assets/helix/core/plugins/jasny/jasny-bootstrap.min.css')}}">
@stop

@php
    $gUploader = new \App\GUploader('import_excel');
@endphp

@section('content')
    <form class="form-horizontal fill-up" enctype="multipart/form-data" id="form-data-import" action="{{urlWithLng("reference-table-form/{$referenceTable->id}/import")}}">
        <div class="tabs-container">
            <div class="tab-content">
                <div id="tab-general" class="tab-pane active">
                    <div class="panel-body">
                        <div class="g-upload-container">
                            <div class="register-form__list">
                                <div class="register-form__item">
                                    <div class="g-upload-box">

                                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                            <div class="form-control" data-trigger="fileinput">
                                                <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                                <span class="fileinput-filename"></span>
                                            </div>
                                            <span class="input-group-addon btn btn-default btn-file">
                                                <span class="fileinput-new">{{trans('swis.reference_table_form.select_file')}}</span>
                                                <span class="fileinput-exists">{{trans('swis.reference_table_form.change_file')}}</span>
                                                <input
                                                    id="files"
                                                    type="file"
                                                    name="uploded_file"
                                                    class="g-upload"
                                                    {{$gUploader->isMultiple() ? 'multiple' : ''}}
                                                    data-action="{{urlWithLng('/g-uploader/upload')}}"
                                                    accept="{{$gUploader->getAcceptTypes()}}"
                                                    data-conf="import_excel"
                                                />
                                            </span>
                                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">{{trans('swis.reference_table_form.remove_file')}}</a>
                                        </div>
                                    </div>
                                    <div class="license-form__uploaded-file-list g-uploaded-list"></div>
                                    {{-- Progress bar for file uploding process, if you want to show process--}}
                                    <progress class="progressBarFileUploader" value="0" max="100"></progress>
                                    {{-- Progress bar for file uploding process --}}
                                    <div class="form-error-text form-error-files fb fs12"></div>
                                    <div class="form-error" id="form-error-uploded_file"></div>
                                </div>
                            </div>
                        </div>
                        <div id="importErrorMessages"></div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <input type="hidden" value="{{$referenceTable->id or 0}}" name="id" class="mc-form-key"/>
        {!! csrf_field() !!}
    </form>
@stop

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script src="{{asset('assets/helix/core/plugins/jasny/jasny-bootstrap.min.js')}}"></script>
    <script src="{{asset('js/reference-table-form/main.js')}}"></script>
    <script src="{{asset('js/guploader.js')}}"></script>
@endsection