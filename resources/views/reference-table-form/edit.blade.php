<?php
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\ReferenceTable\ReferenceTableStructure;

$refName = $referenceTable->current()->name;
?>

@extends('layouts.app')

@section('title'){{$refName}}@stop

@section('contentTitle'){{$refName}}@stop

@section('form-buttons-top')

    @if($referenceTable->show_status == ReferenceTable::STATUS_ACTIVE && $saveMode != 'view')
        {!! renderEditButton() !!}
    @endif

    {!! renderCancelButton() !!}
@stop

@section('topScripts')
    <script>
        var referenceTableStatus = '{{$referenceTableForm->show_status or ''}}';
        var referenceParentStatus = '{{$referenceTable->show_status or ''}}';
    </script>
@endsection

@section('content')

    <?php

    switch ($saveMode) {
        case 'add':
            $url = 'reference-table-form/' . $referenceTable->id;
            break;
        default:
            $url = $saveMode != 'view' ? 'reference-table-form/' . $referenceTable->id . '/update/' . $referenceTableForm->id : '';
            break;
    }

    $languages = activeLanguages();

    ?>

    <div class="form-group m-b-none clearfix">
        <div class="form-error alert alert-danger reference-form-message" id="form-error-message"></div>
    </div>

    <form class="form-horizontal fill-up referenceFormData" id="form-data" action="{{urlWithLng($url)}}" autocomplete="off">

        <div class="tabs-container">

            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-general"> {{trans('core.base.tab.general')}}</a></li>
                @foreach($languages as $language)
                    <li><a data-toggle="tab" href="#tab-{{$language->code}}">{{ trans("core.base.language.".strtolower($language->name)) }}</a></li>
                @endforeach
                @if ($saveMode != 'add')
                    <li><a data-toggle="tab" href="#tab-audit">{{trans('swis.reference_table.audit.title')}}</a></li>
                @endif
            </ul>

            <div class="tab-content">
                <div id="tab-general" class="tab-pane active">
                    <div class="panel-body">

                        <div class="form-group">
                            <label class="col-lg-3 control-label">{{trans('swis.reference_table.table_name')}}</label>
                            <div class="col-lg-8">
                                <input value="{{$referenceTable->table_name or ''}}" disabled type="text" class="form-control">
                            </div>
                        </div>

                        @foreach($referenceTable->structure as $structure)

                            @if($structure->show_status == ReferenceTable::STATUS_INACTIVE && ($saveMode == 'add' || ($saveMode == 'edit' && $structure->updated_at < $referenceTableForm->created_at)))
                                @continue
                            @endif

                            @if(!$structure->has_ml)
                                <?php $fieldName = $structure->field_name ?>

                                <div class="form-group {{($structure->required) ? 'required' : ''}} {{($structure->show_status == ReferenceTable::STATUS_INACTIVE ) ? 'inactive' : ''}}">

                                    <label class="col-lg-3 control-label">{{getReferenceTableColumnLabel($structure->field_name,$referenceTable->table_name)}}</label>

                                    <div class="col-lg-8">

                                        @switch($structure->type)

                                            {{-- Select --}}
                                            @case(ReferenceTableStructure::COLUMN_TYPE_SELECT)

                                                <?php $source = $structure->parameters['source']; ?>

                                                @if(!Auth::user()->isSwAdmin() && $source == ReferenceTable::getReferenceAgencyId() && $structure->show_status != ReferenceTable::STATUS_INACTIVE)

                                                    @include('reference-table-form.partials.source-agency-input')

                                                @else

                                                    <?php
                                                    $refTable = ReferenceTable::select('id', 'table_name', 'show_status')->where('id', $source)->first();
                                                    $sourceSelectRef = getReferenceRows($refTable->table_name);
                                                    ?>

                                                    <select class="form-control select2 field-type-select" name="{{$structure->field_name}}">
                                                        <option></option>
                                                        @if($sourceSelectRef->count())
                                                            @foreach($sourceSelectRef as $item)
                                                                <option {{isset($referenceTableForm) && $referenceTableForm->{$fieldName} == $item->id ? 'selected' : ''}} value="{{$item->id}}">({{$item->code}}) {{$item->name}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>

                                                    <input type="hidden" name="ref_table_status" class="ref_table_status" value="{{$refTable->show_status or 0}}">

                                                    {{-- If Source Reference row is inactive show message --}}
                                                    @if(isset($referenceTableForm,$referenceTableForm->{$fieldName}))
                                                        <?php
                                                        $sourceCurrentReference = getReferenceRows($refTable->table_name,$referenceTableForm->{$fieldName});
                                                        ?>

                                                        @if($sourceCurrentReference && $sourceCurrentReference->show_status != ReferenceTableStructure::STATUS_ACTIVE)
                                                            <small class="text-danger">{{trans('swis.reference_table.ref_row_inactive',['id'=>$referenceTableForm->{$fieldName} ?? 0])}}</small>
                                                        @endif
                                                    @endif

                                                @endif

                                            @break

                                            {{-- Autocomplete --}}
                                            @case(ReferenceTableStructure::COLUMN_TYPE_AUTOCOMPLETE)

                                                <?php $source = $structure->parameters['source']; ?>

                                                @if(!Auth::user()->isSwAdmin() && $source == ReferenceTable::getReferenceAgencyId() && $structure->show_status != ReferenceTable::STATUS_INACTIVE)

                                                    @include('reference-table-form.partials.source-agency-input')

                                                @else

                                                    <?php

                                                    $sourceAutocompleteRef = null;
                                                    if (isset($referenceTableForm,$referenceTableForm->{$fieldName})) {
                                                        $refTable = ReferenceTable::select('table_name')->where('id', $source)->first();
                                                        $sourceAutocompleteRef = getReferenceRows($refTable->table_name, $referenceTableForm->{$fieldName});
                                                    }

                                                    ?>

                                                    <input type="text" placeholder="autocomplete" value="{{ !is_null($sourceAutocompleteRef) ? '('.$sourceAutocompleteRef->code.') '.$sourceAutocompleteRef->name : '' }}"
                                                           data-source="{{$structure->parameters['source']}}"
                                                           data-name="{{$structure->field_name}}"
                                                           class="form-control autocomplete">

                                                    <input type="hidden" name="{{$structure->field_name}}" value="{{ isset($referenceTableForm) ? $referenceTableForm->{$fieldName} : '' }}">

                                                    {{-- If Source Reference row is inactive show message --}}
                                                    @if($sourceAutocompleteRef && $sourceAutocompleteRef->show_status != ReferenceTableStructure::STATUS_ACTIVE)
                                                        <small class="text-danger">{{trans('swis.reference_table.ref_row_inactive',['id'=>$sourceAutocompleteRef->id])}}</small>
                                                    @endif

                                                @endif

                                            @break

                                            {{-- Datetime --}}
                                            @case(ReferenceTableStructure::COLUMN_TYPE_DATETIME)

                                                <div class='input-group datetime'><span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                    <input type="text" placeholder="{{setDatePlaceholder(true)}}" value="{{isset($referenceTableForm) ? formattedDate($referenceTableForm->{$fieldName},true) : ''}}" name="{{$structure->field_name}}" min="0" class="form-control">
                                                </div>

                                            @break

                                            {{-- Date --}}
                                            @case(ReferenceTableStructure::COLUMN_TYPE_DATE)

                                                <?php
                                                $value = '';
                                                if (isset($referenceTableForm) && !empty($referenceTableForm->{$fieldName})) {
                                                    $value = $referenceTableForm->{$fieldName};
                                                } elseif($structure->field_name == ReferenceTable::REFERENCE_DEFAULT_FIELD_START_DATE) {
                                                    $value = currentDate();
                                                }
                                                ?>

                                                <div class='input-group date'>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                    <input @if($structure->field_name == ReferenceTable::REFERENCE_DEFAULT_FIELD_START_DATE)
                                                           {{(isset($referenceTableForm) && \Carbon\Carbon::now() >= $referenceTableForm->{$fieldName}) ? 'readonly' : ''}}
                                                           @endif
                                                           name="{{$structure->field_name}}"
                                                           value="{{formattedDate($value)}}" autocomplete="off" type="text"
                                                           placeholder="{{setDatePlaceholder()}}" class="form-control "/>
                                                </div>

                                            @break

                                            {{-- Decimal --}}
                                            @case(ReferenceTableStructure::COLUMN_TYPE_DECIMAL)

                                                <input value="{{$referenceTableForm->{$fieldName} or ''}}" {{($saveMode != 'add' && $fieldName == 'code') ? 'readonly' : ''}} name="{{$structure->field_name}}" type="number" placeholder="" class="form-control">

                                            @break

                                            @default

                                                <input value="{{$referenceTableForm->{$fieldName} or ''}}" {{($saveMode != 'add' && $fieldName == 'code' && !is_null($referenceTableForm->code)) ? 'readonly' : ''}} name="{{$structure->field_name}}" type="{{ $structure->type }}" placeholder="" class="form-control">

                                        @endswitch

                                        <div class="form-error" id="form-error-{{$structure->field_name}}"></div>
                                    </div>
                                </div>
                            @endif

                        @endforeach

                        <div class="form-group">
                            <label class="control-label col-lg-3">{{trans('core.base.label.status')}}</label>
                            <div class="col-lg-8 general-status">
                                <select name="show_status" class="form-show-status ">
                                    <option value="{{ReferenceTable::STATUS_ACTIVE}}"{{(isset($referenceTableForm) && $referenceTableForm->show_status == ReferenceTable::STATUS_ACTIVE) || $saveMode == 'add' ?  ' selected="selected"' : ''}}>{{trans('core.base.label.status.active')}}</option>
                                    <option value="{{ReferenceTable::STATUS_INACTIVE}}"{{isset($referenceTableForm) &&  $referenceTableForm->show_status == ReferenceTable::STATUS_INACTIVE ? ' selected="selected"': ''}}>{{trans('core.base.label.status.inactive')}}</option>
                                </select>
                                <div class="form-error" id="form-error-show_status"></div>
                            </div>
                        </div>
                    </div>

                </div>

                @foreach($languages as $language)
                    <?php $lngId = $language->id ?>
                    <div id="tab-{{$language->code}}" class="tab-pane">
                        <div class="panel-body">
                            @foreach($referenceTable->structure as $structure)
                                @if($structure->has_ml)

                                    <?php $fieldName = $structure->field_name ?>

                                    <div class="form-group {{($structure->required) ? 'required' : ''}} {{($structure->show_status == ReferenceTable::STATUS_INACTIVE ) ? 'inactive' : ''}}"><label class="col-lg-3 control-label">{{getReferenceTableColumnLabel($fieldName,$referenceTable->table_name)}}</label>
                                        <div class="col-lg-8">
                                            <input value="{{(isset($referenceTableFormMl[$lngId]) && !empty($referenceTableFormMl[$lngId]->{$fieldName})) ? $referenceTableFormMl[$lngId]->{$fieldName} : ''}}" name="ml[{{$language->id}}][{{$structure->field_name}}]" type="text" placeholder="" class="form-control">
                                            <div class="form-error" id="form-error-ml-{{$language->id}}-{{$fieldName}}"></div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach

                            @if($saveMode != 'view')
                                <div class="form-group">
                                    <div class="col-lg-8 col-lg-offset-3">
                                        <div class="duplicate-buttons m-t">
                                            @foreach(activeLanguages() as $activeLanguage)
                                                @if($lngId == $activeLanguage->id) @continue @endif
                                                <button type="button" class="btn btn-success duplicate-info-btn" data-current-lang-id="{{$lngId}}" data-to-lang-code="{{$activeLanguage->code}}" data-to-lang-id="{{$activeLanguage->id}}">{{trans('swis.info.duplicate.to.'.$activeLanguage->code)}}</button>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                @endforeach

                <div id="tab-audit" class="tab-pane">
                    <div class="panel-body">
                        @include('reference-table-form.audit')
                    </div>
                </div>

            </div>
        </div>

        <input type="hidden" value="{{$referenceTable->id or ''}}" class="mc-form-key"/>

        {!! csrf_field() !!}
    </form>

@endsection

@section('form-buttons-bottom')

    @if($referenceTable->show_status == ReferenceTable::STATUS_ACTIVE && $saveMode != 'view')
        {!! renderEditButton() !!}
    @endif

    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script src="{{asset('assets/helix/core/plugins/bootstrap-typeahead/bootstrap-typeahead.js')}}"></script>
    <script src="{{asset('js/reference-table-form/main.js')}}"></script>
@endsection