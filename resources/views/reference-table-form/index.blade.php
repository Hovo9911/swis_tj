<?php
use \App\Models\ReferenceTable\ReferenceTableStructure;
$refName = $referenceTable->current()->name;
?>

@extends('layouts.app')

@section('title'){{$refName}}@stop

@section('contentTitle'){{$refName}}@stop

@section('form-buttons-top')

    @if(!in_array($referenceTable->table_name, \App\Models\ReferenceTable\ReferenceTable::NONE_EDITABLE_REFERENCE_TABLE_NAMES))
        @if ($userHasAccess && ($referenceTable->manually_edit || $referenceTable->show_status == \App\Models\ReferenceTable\ReferenceTable::STATUS_ACTIVE))
            <a href='{{ urlWithLng("/reference-table-form/{$referenceTable->id}/import") }}' class="btn  btn-primary">{{trans('core.base.button.import')}}</a>
        @endif
        <a target="_blank" href='{{ urlWithLng("/reference-table-form/{$referenceTable->id}/export") }}' class="btn btn-warning">{{trans('core.base.button.export')}}</a>
    @endif

    @if($userHasAccess && $referenceTable->show_status == \App\Models\ReferenceTable\ReferenceTable::STATUS_ACTIVE)
        {!! renderAddButton() !!}
    @endif

@stop

@section('topScripts')
    <script>
        var $userHasAccess = '{{$userHasAccess or ''}}';
    </script>
@endsection

@section('content')

    <div class="clearfix search__out">
        <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse" data-target="#collapseSearch" aria-expanded="false" aria-controls="collapseSearch">
            <i class="fa fa-search"> </i><span class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
        </button>
    </div>

    <div class="collapse" id="collapseSearch">
        <div class="card card-body">

            <form class="form-horizontal fill-up form-group-no-margin" id="search-filter" autocomplete="off">

                <div class="form-group"><label class="col-md-4 col-lg-3 control-label label__title">ID</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <input type="number" min="0" class="form-control onlyNumbers" name="id" id="" value="">
                                <div class="form-error" id="form-error-id"></div>
                            </div>
                        </div>
                    </div>
                </div>

                @foreach($referenceTable->structure as $structure)
                    @if($structure->search_filter)
                        @switch($structure->type)

                            {{--  Date   --}}
                            @case(ReferenceTableStructure::COLUMN_TYPE_DATE)
                                <div class="clearfix">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{ getReferenceTableColumnLabel($structure->field_name,$referenceTable->table_name)}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-4 col-lg-5 field__two-col">
                                                <div class="form-group">
                                                    <div class='input-group date'>
                                                        <span class="input-group-addon">
                                                             <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input value="" name="{{$structure->field_name}}_start"
                                                               autocomplete="off"
                                                               type="text"
                                                               placeholder="{{setDatePlaceholder()}}"
                                                               class="form-control"/>
                                                    </div>
                                                    <div class="form-error" id="form-error-{{$structure->field_name.'_start'}}"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-lg-5 field__two-col">
                                                <div class="form-group">
                                                    <div class='input-group date'>
                                                        <span class="input-group-addon">
                                                              <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input value="" name="{{$structure->field_name}}_end"
                                                               autocomplete="off"
                                                               type="text"
                                                               placeholder="{{setDatePlaceholder()}}"
                                                               class="form-control"/>
                                                    </div>
                                                    <div class="form-error" id="form-error-{{$structure->field_name.'_end'}}"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @break

                            {{--  DateTime   --}}
                            @case(ReferenceTableStructure::COLUMN_TYPE_DATETIME)
                                <div class="clearfix">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{ getReferenceTableColumnLabel($structure->field_name,$referenceTable->table_name) }}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-4 col-lg-5 field__two-col">
                                                <div class="form-group">
                                                    <div class='input-group datetime'>
                                                     <span class="input-group-addon">
                                                     <span class="glyphicon glyphicon-calendar"></span>
                                                     </span>
                                                        <input type="text" placeholder="{{setDatePlaceholder(true)}}"
                                                               value="{{$laboratory->certification_period_validity ?? ''}}"
                                                               name="{{$structure->field_name}}_start"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-error" id="form-error-{{$structure->field_name.'_start'}}"></div>
                                                </div>
                                            </div>

                                            <div class="col-md-4 col-lg-5 field__two-col">
                                                <div class="form-group">
                                                    <div class='input-group datetime'>
                                                     <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                        <input type="text" placeholder="{{setDatePlaceholder(true)}}"
                                                               value="{{$laboratory->certification_period_validity ?? ''}}"
                                                               name="{{$structure->field_name}}_end"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-error" id="form-error-{{$structure->field_name.'_end'}}"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @break

                            {{--  Number   --}}
                            @case(ReferenceTableStructure::COLUMN_TYPE_NUMBER)
                                <div class="clearfix">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{getReferenceTableColumnLabel($structure->field_name,$referenceTable->table_name)}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-4 col-lg-5 field__two-col">
                                                <div class="form-group">
                                                    <div class='input-group'>
                                                     <span class="input-group-addon">
                                                         {{trans('swis.reference_table.min.label')}}
                                                     </span>
                                                        <input type="{{$structure->type}}" class="form-control" name="{{$structure->field_name}}_start" id="" value="">
                                                    </div>
                                                    <div class="form-error" id="form-error-{{$structure->field_name.'_start'}}"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-lg-5 field__two-col">
                                                <div class="form-group">
                                                    <div class='input-group'>
                                                     <span class="input-group-addon">
                                                        {{trans('swis.reference_table.max.label')}}
                                                     </span>
                                                        <input type="{{$structure->type}}" class="form-control" name="{{$structure->field_name}}_end" id="" value="">
                                                    </div>
                                                    <div class="form-error" id="form-error-{{$structure->field_name.'_end'}}"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @break

                            {{--  Decimal   --}}
                            @case(ReferenceTableStructure::COLUMN_TYPE_DECIMAL)
                                <div class="clearfix">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{getReferenceTableColumnLabel($structure->field_name,$referenceTable->table_name)}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-4 col-lg-5 field__two-col">
                                                <div class="form-group">
                                                    <div class='input-group'>
                                                     <span class="input-group-addon">
                                                         {{trans('swis.reference_table.min.label')}}
                                                     </span>
                                                        <input type="{{$structure->type}}" class="form-control" name="{{$structure->field_name}}_start" id="" value="">
                                                    </div>
                                                    <div class="form-error" id="form-error-{{$structure->field_name.'_start'}}"></div>
                                                </div>
                                            </div>

                                            <div class="col-md-4 col-lg-5 field__two-col">
                                                <div class="form-group">
                                                    <div class='input-group'>
                                                     <span class="input-group-addon">
                                                        {{trans('swis.reference_table.max.label')}}
                                                     </span>
                                                        <input type="{{$structure->type}}" class="form-control" name="{{$structure->field_name}}_end" id="" value="">
                                                    </div>
                                                </div>
                                                <div class="form-error" id="form-error-{{$structure->field_name.'_end'}}"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @break

                            {{--  Autocomplete   --}}
                            @case(ReferenceTableStructure::COLUMN_TYPE_AUTOCOMPLETE)
                                <div class="form-group">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{getReferenceTableColumnLabel($structure->field_name,$referenceTable->table_name)}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <select class="form-control select2-ajax"
                                                        id="select-role"
                                                        data-url="{{urlWithLng('reference-table/ref')}}"
                                                        data-min-input-length="1"
                                                        data-id-value="true"
                                                        data-code-name="true"
                                                        data-ref="{{$structure->parameters['source'] ?? null}}"
                                                        name="{{$structure->field_name}}"
                                                ></select>
                                                <div class="form-error" id="form-error-{{$structure->field_name}}"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @break

                            {{--  Select   --}}
                            @case(ReferenceTableStructure::COLUMN_TYPE_SELECT)
                                <?php
                                $relatedRefTable = App\Models\ReferenceTable\ReferenceTable::select('id', 'table_name')->find($structure->parameters['source']);
                                $refTableRows = getReferenceRows($relatedRefTable->table_name);
                                ?>
                                <div class="form-group">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{getReferenceTableColumnLabel($structure->field_name,$referenceTable->table_name)}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <select class="form-control select2" name="{{$structure->field_name}}">
                                                    <option value=""></option>
                                                    @if(count($refTableRows))
                                                        @foreach ($refTableRows as $refTableRow)
                                                            <option value="{{ $refTableRow->name }}">({{$refTableRow->code}}) {{$refTableRow->name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                <div class="form-error" id="form-error-{{$structure->field_name}}"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @break

                            {{--  Text   --}}
                            @default
                                <div class="form-group">
                                    <label class="col-md-4 col-lg-3 control-label label__title">{{getReferenceTableColumnLabel($structure->field_name,$referenceTable->table_name)}}</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-lg-10 col-md-8 field__one-col">
                                                <input type="{{$structure->type}}" class="form-control" name="{{$structure->field_name}}" id="" value="">
                                                <div class="form-error" id="form-error-{{$structure->field_name}}"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                        @endswitch
                    @endif
                @endforeach

                <div class="form-group"><label class="col-md-4 col-lg-3 control-label label__title">{{trans('core.base.label.ref_search_status')}}</label>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-lg-10 col-md-8 field__one-col">
                                <select class="form-control select2 default-search" name="show_status">
                                    <option selected value="{{App\Models\ReferenceTable\ReferenceTable::STATUS_ACTIVE}}">{{trans('core.base.label.status.active')}}</option>
                                    <option value="{{App\Models\ReferenceTable\ReferenceTable::STATUS_INACTIVE}}">{{trans('core.base.label.status.inactive')}}</option>
                                </select>
                                <div class="form-error" id="form-error-show_status"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8 col-lg-offset-3 text-right">
                        <div class="row">
                            <div class="col-lg-10 col-md-8">
                                <button type="submit" class="btn btn-primary mr-10">{{trans('core.base.label.search')}}</button>
                                <button type="button" class="btn btn-danger resetButton" data-submit="true">{{trans('core.base.label.reset')}}</button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>

    <div class="data-table">
        <div class="clearfix">
            <?php
            $orderAbleFields = $referenceTable->orderAbleFields();
            ?>
            <table class="table table-striped table-bordered table-hover dataTables-example" id="list-data">
                <thead>
                <tr>
                    <th data-col="actions" class="th-actions" data-delete="0">{{trans('core.base.label.actions')}}</th>
                    <th data-col="id">ID</th>
                    @foreach($referenceTable->structure as $structure)
                        {{--@if($structure->searchable)--}}
                        @if($structure->search_result)
                            <th data-col="{{$structure->field_name}}" class="{{ in_array($structure->field_name, $orderAbleFields) ? 'no-sorting': '' }}">{{getReferenceTableColumnLabel($structure->field_name,$referenceTable->table_name)}}</th>
                        @endif
                        {{--@endif--}}
                    @endforeach

                    <th data-col="show_status">{{trans('core.base.label.status')}}</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    <input type="hidden" value="{{$referenceTable->id or 0}}" name="id" class="mc-form-key"/>
@endsection

@section('scripts')
    <script src="{{asset('js/reference-table-form/main.js')}}"></script>
@endsection