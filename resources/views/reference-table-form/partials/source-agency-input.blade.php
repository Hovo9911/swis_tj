<?php $referenceAgency = \App\Models\ReferenceTable\ReferenceTable::getAuthUserAgencyRefRow();  ?>

@if(!is_null($referenceAgency))
    <input type="text" class="form-control" disabled value="{{$referenceAgency->code.' '.$referenceAgency->name}}">
    <input type="hidden" name="{{$structure->field_name}}" value="{{$referenceAgency->id or 0}}">
@else
    <div class="alert alert-danger">{{trans('swis.reference_table_form.add_active_agency.message')}}</div>
@endif