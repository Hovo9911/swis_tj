@extends('layouts.app')

@section('title'){{trans('swis.authorize_person.title')}}@stop

@section('contentTitle') {{trans('swis.authorize_person.title')}} @stop

<?php

$jsTrans->addTrans([
    'core.base.button.close',
    'news.field.approved.yes',
    'news.field.approved.no',
]);

?>

@section('content')

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form class="form-horizontal fill-up" id="form-data" action="{{urlWithLng('authorize-person')}}">

                <div class="form-group required"><label class="col-lg-3 control-label">
                        {{trans('swis.tax_id')}}
                    </label>
                    <div class="col-lg-9">
                        <input value="" name="authorized_ssn" maxlength="{{config('global.ssn.max')}}" type="text"
                               placeholder="" class="form-control get-info-field get-authorized-info">
                        <div class="form-error" id="form-error-authorized_ssn"></div>
                        <div class="form-error" id="form-error-message"></div>
                    </div>
                </div>
            {{--    <div class="form-group required"><label
                            class="col-lg-3 control-label">{{trans('core.base.label.passport')}}</label>
                    <div class="col-lg-9">
                        <input value="" type="text" name="authorized_passport" placeholder=""
                               class="form-control get-authorized-info onlyLatinAlpha">
                        <div class="form-error" id="form-error-authorized_passport"></div>
                    </div>
                </div>--}}
                <hr/>

                <div class="form-group"><label
                            class="col-lg-3 control-label">{{trans('swis.authorized_first_name')}}</label>
                    <div class="col-lg-9">
                        <input value="" disabled type="text" name="authorized_first_name" placeholder=""
                               class="form-control">
                    </div>
                </div>

                <div class="form-group"><label
                            class="col-lg-3 control-label">{{trans('swis.authorized_last_name')}}</label>
                    <div class="col-lg-9">
                        <input value="" name="authorized_last_name" disabled type="text" placeholder=""
                               class="form-control">
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script src="{{asset('js/authorize-person/main.js')}}"></script>
@endsection