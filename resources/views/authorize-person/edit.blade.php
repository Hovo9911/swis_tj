@extends('layouts.app')

@section('title'){{trans('swis.authorize_person.provide_role')}}@stop

@section('contentTitle') {{trans('swis.authorize_person.provide_role')}} @stop

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('topScripts')
    <script>
        var myApplicationMenu = '{{$menuIds['myApplication'] or 0}}';
        var reportMenu = '{{$menuIds['report'] or 0}}';
        var paymentAndObligationsMenu = '{{$menuIds['paymentAndObligations'] or 0}}';
        var isAgency = '{{Auth::user()->isAgency()}}';
    </script>
@endsection

@section('content')
    <form class="form-horizontal fill-up" id="provide-role"
          action="{{$saveMode != 'view' ? urlWithLng('authorize-person/update/'.$user->id) : ''}}" autocomplete="off">

        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="{{!count($userRoles) ? 'active' : ''}}"><a data-toggle="tab" href="#tab-general"> {{trans('core.base.tab.general')}}</a></li>
                <li class="{{count($userRoles) ? 'active' : ''}}"><a data-toggle="tab" href="#tab-roles">{{trans('swis.authorize_person.roles.tab.title')}}</a></li>
            </ul>
            <div class="tab-content">
                <div id="tab-general" class="tab-pane {{!count($userRoles) ? 'active' : ''}}">
                    <div class="panel-body">

                        <div class="form-group "><label class="col-lg-3 control-label">{{trans('swis.authorized_ssn')}}</label>
                            <div class="col-lg-9">
                                <input value="{{$user['ssn']}}" disabled type="text" placeholder="" class="form-control">
                                <div class="form-error" id="form-error-authorized_ssn"></div>
                            </div>
                        </div>

                        <div class="form-group "><label class="col-lg-3 control-label">{{trans('core.base.label.passport')}}</label>
                            <div class="col-lg-9">
                                <input value="{{$user['passport'] ?? ''}}" disabled type="text" placeholder="" class="form-control">
                                <div class="form-error" id="form-error-authorized_ssn"></div>
                            </div>
                        </div>

                        <div class="form-group"><label class="col-lg-3 control-label">{{trans('swis.authorized_first_name')}}</label>
                            <div class="col-lg-9">
                                <input value="{{$user['first_name']}}" disabled type="text" name="authorized_first_name" placeholder="" class="form-control">
                            </div>
                        </div>

                        <div class="form-group"><label class="col-lg-3 control-label">{{trans('swis.authorized_last_name')}}</label>
                            <div class="col-lg-9">
                                <input value="{{$user['last_name']}}" name="authorized_last_name" disabled type="text" placeholder="" class="form-control">
                            </div>
                        </div>

                    </div>
                </div>

                <div id="tab-roles" class="tab-pane {{count($userRoles) ? 'active' : ''}}">
                    <div class="panel-body p-lr-n">
                        <div class="multiple">
                            <div class="role-row table-responsive">
                                <table class="table table-bordered table-hover" id="roleTable">
                                    <thead>
                                        <tr>
                                            <th style="width: 180px">{{trans('swis.authorzied_person.authorizer.name')}}</th>
                                            <th style="width: 190px">{{trans('swis.module_title')}}</th>
                                            <th style="width: 190px">{{trans('swis.authorzied_person.role.title')}}</th>
                                            @if(!\Auth::user()->isSwAdmin())
                                                <th style="width: 190px">{{trans('swis.role_attributes.title')}}</th>
                                            @endif
                                            <th style="width: 130px">{{trans('core.base.label.accessibility')}}</th>
                                            <th style="width: 180px">{{trans('swis.authorzied_person.label.ip_access')}}</th>
                                            <th style="width: 170px">{{trans('core.base.label.start')}}</th>
                                            <th style="width: 170px">{{trans('core.base.label.end')}}</th>
                                            <th style="width: 140px">{{trans('swis.authorized_persons.role_status.label')}}</th>
                                            <th style="width: 20px"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($userRoles))

                                        @foreach($userRoles as $key=>$userRole)

                                            @if($userRole->isBlocked)
                                                @include('authorize-person.role-table-blocked',['i'=>$key+1])
                                            @else
                                                @include('authorize-person.role-table',['i'=>$key+1,'mode'=>$saveMode,'menuIds'=>$menuIds])
                                            @endif

                                        @endforeach
                                    @else
                                        @include('authorize-person.role-table',['i'=>1])
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <button class="btn-more btn-primary btn" type="button"><i class="fa fa-plus"></i></button>
                    </div>
                </div>

                <input type="hidden" value="{{$user->id or 0}}" name="id" class="mc-form-key"/>
                <input type="hidden" value="" name="changed_modules" id="changedModules"/>
            </div>
        </div>
    </form>
@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script src="{{asset('js/authorize-person/main.js')}}"></script>
@endsection