<tr  data-id="{{$i}}">
    <td>
        @if(isset($userRole) && !is_null($userRole->authorizer))
            <span>{{$userRole->authorizer->first_name.' '.$userRole->authorizer->last_name . ' ('.$userRole->authorizer->ssn.')'}}</span>
        @endif
    </td>

    <td>
        <div class="form-group">
            <input type="text" data-toggle="tooltip" title="{{trans($userRole->menu->title ?? '')}}" class="form-control" disabled value="{{trans($userRole->menu->title ?? '')}}">
        </div>
    </td>

    <td>
        <div class="form-group">
            <input type="text" class="form-control" disabled data-toggle="tooltip" title="{{(!$userRole->role_id) ? ($userRole->role_group_id) ? $userRole->roleGroup->name ?? '' : '' : $userRole->role->name }}" value="{{(!$userRole->role_id) ? ($userRole->role_group_id) ? $userRole->roleGroup->name ?? '' : '' : $userRole->role->name }}">
        </div>
    </td>

    @if(!\Auth::user()->isSwAdmin())

        <td>
            <?php
            $attrValueData = [];
            if (!is_null($userRole->attribute_field_id)) {
                $attrValueData = \App\Models\Role\Role::getRoleAttributeValue($userRole->attribute_field_id, $userRole->attribute_type, $userRole->attribute_value);
            }
            ?>

            <input class="form-control" title="{{$attrValueData['attribute'] or ''}}" data-toggle="tooltip" type="text" value="{{$attrValueData['attribute'] or trans('core.base.label.select')}}" disabled>
            @if($userRole->menu->id == \App\Models\Menu\Menu::getMenuIdByName(\App\Models\Menu\Menu::USER_DOCUMENTS_MENU_NAME))
                @if(isset($attrValueData['attribute_value']))
                    <hr/>
                    <label>{{trans('swis.roles.attribute-values.label')}}</label>
                    <input class="form-control" title="{{$attrValueData['attribute_value'] or ''}}" data-toggle="tooltip" type="text" value="{{$attrValueData['attribute_value']}}" disabled>
                @endif
            @endif
        </td>
    @endif

    <td>
        <select class="form-control select2" disabled>
            <option value="1" {{(isset($userRole) && $userRole->show_only_self) ? 'selected' : ''}}>{{trans('swis.authorize_person.accessibility.only_self')}}</option>
            <option value="0" {{(isset($userRole) && !$userRole->show_only_self) ? 'selected' : ''}}>{{trans('swis.authorize_person.accessibility.others_too')}}</option>
        </select>
    </td>

    <td>
        <div class="form-group">
            <input type="text" class="form-control" disabled value="{{$userRole->ip_acess or ''}}">
        </div>
    </td>

    <td>
        <div class="input-group">
            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            <input class="form-control" disabled placeholder="{{setDatePlaceholder()}}" data-toggle="tooltip" title="{{ $userRole->available_at or ''}}" value="{{$userRole->available_at or ''}}" type="text">
        </div>
    </td>

    <td>
        <div class="input-group">
        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
        <input class="form-control" disabled placeholder="{{setDatePlaceholder()}}" data-toggle="tooltip" title="{{ $userRole->available_to or ''}}" value="{{$userRole->available_to or ''}}" type="text">
        </div>
    </td>

    <td>
        <div class="form-group">
            <input class="form-control" disabled data-toggle="tooltip" title="{{ trans('core.base.label.status.blocked')}}" value="{{trans('core.base.label.status.blocked')}}" type="text">
        </div>
    </td>

    <td></td>
</tr>