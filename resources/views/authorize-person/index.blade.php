@extends('layouts.app')

@section('title'){{trans('swis.authorize_persons')}}@stop

@section('contentTitle') {{trans('swis.authorize_persons')}} @stop

@section('form-buttons-top')
    {!! renderAddButton() !!}
@stop

<?php
$jsTrans->addTrans([
    'swis.laboratory.title',
    'swis.regionalOffice.title',
    'swis.authorize_person',
    'swis.roles_group.title',
    'swis.agency.title',
    'swis.broker.title',
    'swis.roles.title',
    'swis.reference_table'
]);
?>

@section('content')
    <div class="clearfix search__out">
        <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse" data-target="#collapseSearch" aria-expanded="false" aria-controls="collapseSearch">
            <i class="fa fa-search"> </i><span class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
        </button>
    </div>
    <div class="collapse" id="collapseSearch">
        <div class="card card-body">
            <form class="form-horizontal fill-up" id="search-filter" autocomplete="off">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group searchParam id_block_params" data-field="id_block_params">
                            <label class="col-md-4 control-label label__title">ID</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <input type="number" class="form-control onlyNumbers" min="0" name="id" id="" value="">
                                        <div class="form-error" id="form-error-id"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam first_name_block_params" data-field="first_name_block_params">
                            <label class="col-md-4 control-label label__title">{{trans('core.base.label.first_name')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <input type="text" class="form-control" name="first_name" id="" value="">
                                        <div class="form-error" id="form-error-first_name"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam last_name_block_params" data-field="last_name_block_params">
                            <label class="col-md-4 control-label label__title">{{trans('core.base.label.last_name')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <input type="text" class="form-control" name="last_name" id="" value="">
                                        <div class="form-error" id="form-error-last_name"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam ssn_block_params" data-field="ssn_block_params">
                            <label class="col-md-4 control-label label__title">{{trans('core.base.label.ssn')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <input type="text" class="form-control" name="ssn" id="" value="">
                                        <div class="form-error" id="form-error-ssn"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam passport_block_params" data-field="passport_block_params">
                            <label class="col-md-4 control-label label__title">{{trans('core.base.label.passport')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <input type="text" class="form-control" name="passport" id="" value="">
                                        <div class="form-error" id="form-error-passport"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam authorizer_ssn_block_params" data-field="authorizer_ssn_block_params">
                            <label class="col-md-4 control-label label__title">{{trans('swis.authorzied_person.authorizer.snn')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <input type="text" class="form-control" name="authorizer_ssn" id="" value="">
                                        <div class="form-error" id="form-error-authorizer_ssn"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group searchParam authorizer_first_name_block_params" data-field="authorizer_first_name_block_params">
                            <label class="col-md-4 control-label label__title">{{trans('swis.authorzied_person.authorizer.authorizer_first_name')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <input type="text" class="form-control" name="authorizer_first_name" id="" value="">
                                        <div class="form-error" id="form-error-authorizer_first_name"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam authorizer_last_name_block_params" data-field="authorizer_last_name_block_params">
                            <label class="col-md-4 control-label label__title">{{trans('swis.authorzied_person.authorizer.authorizer_last_name')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <input type="text" class="form-control" name="authorizer_last_name" id="" value="">
                                        <div class="form-error" id="form-error-authorizer_last_name"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam menu_block_params" data-field="menu_block_params">
                            <label class="col-md-4 control-label label__title">{{trans('swis.module_title')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <select class="select2" name="menu" id="menu">
                                            <option></option>
                                            @foreach(\App\Models\Menu\MenuManager::menuModules() as $menu)
                                                <option value="{{$menu->id}}">{{trans($menu->title)}}</option>
                                            @endforeach
                                        </select>
                                        <div class="form-error" id="form-error-menu"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam role_block_params" data-field="role_block_params">
                            <label class="col-md-4 control-label label__title">{{trans('swis.role.title')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <select class="select2" name="role" id="roles">
                                            <option></option>
                                            @if($roles->count())
                                                @foreach($roles as $role)
                                                    <option value="{{$role->id}}">{{$role->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <div class="form-error" id="form-error-role"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group searchParam sub_division_id_block_params" data-field="sub_division_id_block_params">
                            <label class="col-md-4 control-label label__title">{{trans('swis.authorized_persons.select_sub_division.label')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <select class="select2" name="sub_division_id">
                                            <option value=""></option>
                                            @if($subDivisions->count())
                                                @foreach($subDivisions as $subDivision)
                                                    <option value="{{$subDivision->id}}"> {{ $subDivision->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-error" id="form-error-sub_division_id"></div>
                            </div>
                        </div>
                        <div class="form-group searchParam role_status_block_params" data-field="role_status_block_params">
                            <label class="col-md-4 control-label label__title">{{trans('swis.authorized_persons.role_status.label')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12 field__one-col">
                                        <select class="select2 default-search" name="role_status">
                                            <option></option>
                                            <option selected value="{{\App\Models\BaseModel::STATUS_ACTIVE}}">{{trans('core.base.label.status.active')}}</option>
                                            <option value="{{\App\Models\BaseModel::STATUS_INACTIVE}}">{{trans('core.base.label.status.inactive')}}</option>
                                        </select>
                                        <div class="form-error" id="form-error-role_status"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8 col-lg-offset-3 text-right">
                        <div class="row">
                            <div class="col-md-10">
                                <button type="submit" class="btn btn-primary mr-10">{{trans('core.base.label.search')}}</button>
                                <button type="button" class="btn btn-danger resetButton" data-submit="true">{{trans('core.base.label.reset')}}</button>
                                <button type="button" class="btn btn-default hide-or-show-params" style="margin-top: 10px;">{{trans('core.base.label.apply_hidden_columns')}}</button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>

    @php($companyTaxId = Auth::user()->companyTaxId())
    <div class="data-table">
        <div class="clearfix">
            <table class="table table-striped table-bordered table-hover dataTables-example" id="list-data" data-module-for-search-params="AuthorizePerson{{ hash('sha256', $companyTaxId) }}">
                <thead>
                    <tr>
                        <th data-col="actions" data-delete="0" class="th-actions">{{trans('core.base.label.actions')}}</th>
                        <th data-col="id">ID</th>
                        <th data-col="name">{{trans('core.base.label.first_name')}} {{trans('core.base.label.last_name')}}</th>
                        <th data-col="ssn">{{trans('core.base.label.ssn')}}</th>
                        <th data-col="passport">{{trans('core.base.label.passport')}}</th>
                        <th data-col="registered_at">{{trans('core.base.label.start')}}</th>
                        <th data-col="role_status">{{trans('swis.authorized_persons.role_status.label')}}</th>
                        <th data-col="available_to">{{trans('swis.user.available_to.label')}}</th>
                        <th data-ordering="0" data-col="first_role_created_username">{{trans('swis.authorized_persons.user.first_role_created.label')}}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <?php
        $searchColumns = [
            'id' => 'ID',
            'name' => trans('core.base.label.first_name'),
            'ssn' => trans('core.base.label.ssn'),
            'passport' => trans('core.base.label.passport'),
            'registered_at' => trans('core.base.label.start'),
            'role_status' => trans('swis.user.role_status.label'),
            'available_to' => trans('swis.user.available_to.label')
        ];
    ?>
@endsection

@section('scripts')
    <script src="{{asset('js/authorize-person/main.js')}}"></script>
@endsection