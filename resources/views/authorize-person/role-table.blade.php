<tr data-id="{{$i}}">
    <td>
        @if(isset($userRole) && !is_null($userRole->authorizer))
            <span>{{$userRole->authorizer->first_name.' '.$userRole->authorizer->last_name . ' ('.$userRole->authorizer->ssn.')'}}</span>
        @endif

        <input type="hidden" name="data[{{$i}}][id]" value="{{$userRole->id or 0}}">
    </td>

    <td>
        <div class="form-group">

            @if(!isset($userRole))
                <select class="form-control menu select2" data-select2-allow-clear="false" name="data[{{$i}}][menu]">
                    <option></option>
                    @if(count($menuModules))
                        @foreach($menuModules as $menu)
                            <option value="{{$menu->id}}">{{trans($menu->title)}}</option>
                        @endforeach
                    @endif
                </select>
            @else
                <input type="text" data-toggle="tooltip" title="{{trans($userRole->menu->title ?? '')}}" class="form-control" disabled value="{{trans($userRole->menu->title ?? '')}}">
                <input type="hidden" class="menu" value="{{$userRole->menu->id ?? 0}}">
            @endif

            <div class="form-error" id="form-error-data-{{$i}}-menu"></div>
        </div>
    </td>

    <td>
        <div class="form-group">
            @if(!isset($userRole))
                <select class="form-control select2 rolesList" data-select2-allow-clear="false">
                    <option></option>
                </select>

                <input type="hidden" class="role" name="data[{{$i}}][role]" value="{{$userRole->role_id or ''}}">
                <input type="hidden" class="role-group"  name="data[{{$i}}][role_group_id]" value="{{$userRole->role_group_id or ''}}">

                <div class="form-error" id="form-error-data-{{$i}}-role"></div>
                <div class="form-error" id="form-error-data-{{$i}}-role_group_id"></div>
            @else
                @if($userRole->role_id)
                    <input type="text" data-toggle="tooltip" title="{{$userRole->role->description ?? trans($userRole->role->name)}}"  class="form-control" disabled value="{{$userRole->role->name}}">
                @endif

                @if($userRole->role_group_id)
                    <input type="text" class="form-control" data-toggle="tooltip" title="{{$userRole->roleGroup->description ?? $userRole->roleGroup->name ?? ''}}" disabled value="{{$userRole->roleGroup->name ?? ''}}">
                @endif
            @endif
        </div>
    </td>

    @if(!\Auth::user()->isSwAdmin())

        <td>
            @if(!isset($userRole))

                <div class="form-group">
                    <select class="form-control select2 attribute" name="data[{{$i}}][attribute_field_id]" disabled>
                        <option></option>
                    </select>
                    <div class="form-error" id="form-error-data-{{$i}}-attribute_field_id"></div>
                </div>

                <div class="form-group hide attribute-values">
                    <hr/>
                    <label>{{trans('swis.roles.attribute-values.label')}}</label>
                    <select class="form-control select2 attribute-value" name="data[{{$i}}][attribute_value]" disabled>
                        <option></option>
                    </select>
                    <div class="form-error" id="form-error-data-{{$i}}-attribute_value"></div>
                </div>

                <input type="hidden" name="data[{{$i}}][attribute_type]" class="attribute_type" value="">

                {{--  Multiple  --}}
                <div class="form-group hide">
                    <select class="form-control select2  attribute-multiple" multiple  name="data[{{$i}}][multiple_attributes][]" disabled data-allow-clear="false">
                        <option></option>
                    </select>
                    <div class="form-error" id="form-error-data-{{$i}}-attribute"></div>
                </div>
            @else

                @switch($userRole->menu->id ?? 0)

                    {{-- My Application --}}
                    @case($menuIds['myApplication'])

                        <?php
                            $attrValueData = [];
                            if (isset($userRole->attribute_field_id)) {
                                $attrValueData = \App\Models\Role\Role::getRoleAttributeValue($userRole->attribute_field_id, $userRole->attribute_type, $userRole->attribute_value);
                            }
                        ?>

                        @if(isset($attrValueData['attribute_value']))
                            <input class="form-control" title="{{$attrValueData['attribute'] or ''}}" data-toggle="tooltip" type="text" value="{{$attrValueData['attribute'] or trans('core.base.label.select')}}" disabled>
                            <hr/>
                            <label>{{trans('swis.roles.attribute-values.label')}}</label>
                            <input class="form-control" title="{{$attrValueData['attribute_value'] or ''}}" data-toggle="tooltip" type="text" value="{{$attrValueData['attribute_value']}}" disabled>
                        @else
                            <input class="form-control" type="text" value="{{trans('core.base.label.select')}}" disabled>
                        @endif

                    @break

                    {{-- Report --}}
                    @case($menuIds['report'])

                        @if(!is_null($userRole->multiple_attributes))
                            <?php
                            $reports = \App\Models\Reports\Reports::select('code')->whereIn('id',$userRole->multiple_attributes)->get();
                            ?>
                            @if($reports->count())

                                <select class="form-control select2  attribute-multiple" multiple  disabled data-allow-clear="false">
                                    @foreach($reports as $report)
                                        <option selected>{{trans('swis.'.$report->code.'.menu.title')}}</option>
                                    @endforeach
                                </select>
                            @endif

                        @else
                            <input class="form-control" type="text" value="{{trans('core.base.label.select')}}" disabled>
                        @endif

                    @break

                    {{-- Payment and Obligation --}}
                    @case($menuIds['paymentAndObligations'])
                        <?php
                            $attrValueData = [];
                            if (isset($userRole->attribute_field_id)) {
                                $attrValueData = \App\Models\Role\Role::getRoleAttributeValue($userRole->attribute_field_id, $userRole->attribute_type, $userRole->attribute_value);
                            }
                        ?>

                        @if(isset($attrValueData['attribute_value']))
                            <input class="form-control" title="{{$attrValueData['attribute'] or ''}}" data-toggle="tooltip" type="text" value="{{$attrValueData['attribute'] or trans('core.base.label.select')}}" disabled>
                            <hr/>
                            <label>{{trans('swis.roles.attribute-values.label')}}</label>
                            <input class="form-control" title="{{trans($attrValueData['attribute_value']) or ''}}" data-toggle="tooltip" type="text" value="{{trans($attrValueData['attribute_value'])}}" disabled>
                        @else
                            <input class="form-control" type="text" value="{{trans('core.base.label.select')}}" disabled>
                        @endif

                    @break

                @default
                    <input class="form-control" data-toggle="tooltip" type="text" value="{{trans('core.base.label.select')}}" disabled>
                @endswitch

            @endif

        </td>
    @endif

    <td>
        <select class="form-control select2 show-only-self" {{!isset($userRole) ? 'disabled' : ''}} data-name="show_only_self" data-select2-allow-clear="false" name="data[{{$i}}][show_only_self]">

            @if(isset($userRole))
                @if(!in_array($userRole->menu->name ?? '',\App\Models\Menu\Menu::SHOW_ONLY_SELF_ALL_MENU_NAMES))
                <option value="1" {{($userRole->show_only_self) ? 'selected' : ''}}>{{trans('swis.authorize_person.accessibility.only_self')}}</option>
                @endif

                <option value="0" {{(!$userRole->show_only_self) ? 'selected' : ''}}>{{trans('swis.authorize_person.accessibility.others_too')}}</option>
            @endif

        </select>
        <div class="form-error" id="form-error-data-{{$i}}-show_only_self"></div>
    </td>

    <td>
        <div class="form-group">
            <input type="text" class="form-control" name="data[{{$i}}][ip_access]" value="{{$userRole->ip_access or ''}}">
            <div class="form-error" id="form-error-data-{{$i}}-ip_access"></div>
        </div>
    </td>

    <td>
        <div class="form-group">
            <div class='input-group date'>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                <input placeholder="{{setDatePlaceholder()}}" autocomplete="off" name="data[{{$i}}][available_at]" data-start-date="{{currentDate()}}" value="{{isset($userRole) ? $userRole->available_at : currentDateFront()}}" {{(isset($userRole) && $userRole->getOriginal('available_at') < currentDate()) ? 'disabled' : ''}} data-toggle="tooltip" title="{{ $userRole->available_at or ''}}" data-name="available_at" class="form-control" type="text">
            </div>
            <div class="form-error" id="form-error-data-{{$i}}-available_at"></div>
        </div>
    </td>

    <td>
        <div class='input-group date'>
            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            <input placeholder="{{setDatePlaceholder()}}" autocomplete="off" name="data[{{$i}}][available_to]" data-start-date="{{currentDate()}}" data-name="available_to" class="form-control" value="{{$userRole->available_to or ''}}" data-toggle="tooltip" title="{{ $userRole->available_to or ''}}" type="text">
        </div>
        <div class="form-error" id="form-error-data-{{$i}}-available_to"></div>
    </td>

    <td>
        <div class="form-group">
            <select class="form-control select2" name="data[{{$i}}][role_status]" data-select2-allow-clear="false" data-name="role_status">
                <option {{(isset($userRole) && $userRole->role_status == \App\Models\User\User::STATUS_ACTIVE) ? 'selected' : ''}} value="{{\App\Models\User\User::STATUS_ACTIVE}}">{{trans('core.base.label.status.active')}}</option>
                <option {{(isset($userRole) && $userRole->role_status == \App\Models\User\User::STATUS_INACTIVE) ? 'selected' : ''}} value="{{\App\Models\User\User::STATUS_INACTIVE}}">{{trans('core.base.label.status.inactive')}}</option>
                <option {{(isset($userRole) && $userRole->role_status == \App\Models\User\User::STATUS_DELETED) ? 'selected' : ''}} value="{{\App\Models\User\User::STATUS_DELETED}}">{{trans('core.base.label.status.blocked')}}</option>
            </select>
            <div class="form-error" id="form-error-data-{{$i}}-role_status"></div>
        </div>
    </td>

    <td class="text-center">
        @if(isset($mode) && $mode == 'add')
            <button class="btn-remove btn-danger"><i class="fa fa-minus"></i></button>
        @endif
    </td>
</tr>