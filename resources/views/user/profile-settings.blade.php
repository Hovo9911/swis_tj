@extends('layouts.app')

@section('title'){{trans('swis.user_profile_settings_title')}}@stop

@section('contentTitle') {{trans('swis.user_profile_settings_title')}} @stop

@section('form-buttons-top')
    {!! renderEditButton() !!}
    <a href="{{urlWithLng('dashboard')}}" class="btn btn-default mc-nav-button-cancel">{{ trans('core.base.button.back') }}</a>
@stop

@section('content')

    <?php
    $languages = activeLanguages();
    ?>

    <div class="tabs-container">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tab-general"> {{trans('swis.user.profile_settings.tab.profile_settings')}}</a></li>
            <li><a data-toggle="tab" href="#tab-companies"> {{trans('swis.user.profile_settings.tab.user_companies')}}</a></li>
            <li><a data-toggle="tab" href="#tab-roles"> {{trans('swis.user.profile_settings.tab.user_roles')}}</a></li>
        </ul>
        <div class="tab-content">
            <div id="tab-general" class="tab-pane active">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <form class="form-horizontal fill-up" id="form-data"
                                  action="{{urlWithLng('profile-settings')}}">

                                <div class="form-group required"><label
                                            class="col-lg-3 control-label">{{trans('core.base.label.first_name')}}</label>
                                    <div class="col-lg-9">

                                        <input autocomplete="off" value="{{Auth::user()->first_name}}" disabled
                                               type="text"
                                               placeholder=""
                                               class="form-control">
                                        <div class="form-error" id="form-error-first_name"></div>
                                    </div>
                                </div>

                                <div class="form-group required"><label
                                            class="col-lg-3 control-label">{{trans('core.base.label.last_name')}}</label>
                                    <div class="col-lg-9">
                                        <input value="{{Auth::user()->last_name}}" autocomplete="off" disabled
                                               type="text"
                                               placeholder=""
                                               class="form-control">
                                        <div class="form-error" id="form-error-last_name"></div>
                                    </div>
                                </div>

                                <div class="form-group required"><label
                                            class="col-lg-3 control-label">{{trans('core.base.label.passport')}}</label>
                                    <div class="col-lg-9">
                                        <input autocomplete="off" value="{{Auth::user()->passport}}" disabled
                                               type="text" placeholder=""
                                               class="form-control">
                                        <div class="form-error" id="form-error-ssn"></div>
                                    </div>
                                </div>


                                <div class="form-group required"><label
                                            class="col-lg-3 control-label">{{trans('core.base.label.ssn')}}</label>
                                    <div class="col-lg-9">
                                        <input autocomplete="off" value="{{Auth::user()->ssn}}" disabled type="text"
                                               placeholder=""
                                               class="form-control">
                                        <div class="form-error" id="form-error-ssn"></div>
                                    </div>
                                </div>


                                <div class="form-group "><label
                                            class="col-lg-3 control-label">{{trans('core.base.label.registration_address')}}</label>
                                    <div class="col-lg-9">
                                        <input autocomplete="off" value="{{Auth::user()->registration_address}}"
                                               disabled type="text"
                                               placeholder=""
                                               class="form-control">
                                        <div class="form-error" id="form-error-registration_address"></div>
                                    </div>
                                </div>

                                <div class="form-group"><label
                                            class="col-lg-3 control-label">{{trans('core.base.label.residence_address')}}</label>
                                    <div class="col-lg-9">
                                        <input autocomplete="off" value="{{Auth::user()->address}}" name="address"
                                               type="text"
                                               placeholder=""
                                               class="form-control">
                                        <div class="form-error" id="form-error-address"></div>
                                    </div>
                                </div>

                                <div class="form-group"><label
                                            class="col-lg-3 control-label">{{trans('swis.city_village')}}</label>

                                    <div class="col-lg-9">
                                        <input autocomplete="off" name="city_village"
                                               value="{{Auth::user()->city_village}}" type="text"
                                               placeholder="" class="form-control">
                                        <div class="form-error" id="form-error-address"></div>
                                    </div>
                                </div>

                                <div class="form-group"><label
                                            class="col-lg-3 control-label">{{trans('swis.post_address')}}</label>
                                    <div class="col-lg-9">
                                        <input autocomplete="off" name="post_address"
                                               value="{{Auth::user()->post_address}}" type="text"
                                               placeholder="" class="form-control">
                                        <div class="form-error" id="form-error-address"></div>
                                    </div>
                                </div>

                                <div class="form-group row {{config('global.phone_number.required') ? 'required' : ''}}">
                                    <label class="col-lg-3 control-label">{{trans('core.base.label.phone')}}</label>
                                    <div class="col-lg-9">
                                        <div class="input-group">
                                            <span class="input-group-addon">+</span>
                                            <input type="text"
                                                   class="form-control onlyNumbers"
                                                   name="phone_number"
                                                   value="{{Auth::user()->phone_number}}"
                                                   {{isset($document) && $docDisabled ? 'disabled' : ''}}
                                                   autocomplete="off"
                                                   required>
                                        </div>
                                        <div class="form-error" id="form-error-phone_number"></div>
                                    </div>
                                </div>

                                {{--<div class="form-group {{config('global.phone_number.required') ? 'required' : ''}}"><label--}}
                                {{--class="col-lg-3 control-label">{{trans('core.base.label.phone')}}</label>--}}
                                {{--<div class="col-lg-9">--}}
                                {{--<input value="{{Auth::user()->phone_number}}" required name="phone_number" type="text"--}}
                                {{--placeholder=""--}}
                                {{--class="form-control">--}}
                                {{--<div class="form-error" id="form-error-phone_number"></div>--}}
                                {{--</div>--}}
                                {{--</div>--}}

                                <div class="form-group required"><label
                                            class="col-lg-3 control-label">{{trans('core.base.label.email')}}</label>
                                    <div class="col-lg-9">
                                        <input autocomplete="off" value="{{Auth::user()->email}}" name="email"
                                               type="email"
                                               placeholder=""
                                               class="form-control">
                                        <div class="form-error" id="form-error-email"></div>
                                    </div>
                                </div>

                                <div class="form-group"><label
                                            class="col-lg-3 control-label">{{trans('swis.default_language')}}</label>
                                    @if(count($languages) > 0)
                                        <div class="col-lg-9">
                                            <select class="form-control select2" name="lng_id">
                                                @foreach($languages as $language)
                                                    <option {{(Auth::user()->lng_id == $language->id) ? 'selected' : ''}} value="{{$language->id}}"> {{ trans("core.base.language.".strtolower($language->name)) }} </option>
                                                @endforeach
                                            </select>
                                            <div class="form-error" id="form-error-agency_id"></div>
                                        </div>
                                    @endif
                                </div>

                                <div class="form-group"><label
                                            class="col-lg-3 control-label">{{trans('swis.user.profile_settings.label.enable_notifications')}}</label>
                                    <div class="col-lg-9">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="email_notification" value="1"
                                                   @if(Auth::user()->email_notification)
                                                   checked
                                                    @endif
                                            >{{trans('swis.user.profile_settings.label.by_email')}}
                                        </label>
                                        @if(config('swis.sms_send'))
                                            <label class="checkbox-inline">
                                                <input type="checkbox" name="sms_notification" value="1"
                                                       @if(Auth::user()->sms_notification)
                                                       checked
                                                        @endif
                                                >{{trans('swis.user.profile_settings.label.by_sms')}}
                                            </label>
                                        @endif
                                    </div>
                                </div>

                                    <hr/>

                                    <div class="form-group "><label
                                                class="col-lg-3 control-label">{{trans('core.base.label.old_password')}}</label>
                                        <div class="col-lg-9">
                                            <input autocomplete="off" name="old_password" type="password" placeholder=""
                                                   class="form-control">
                                            <div class="form-error" id="form-error-old_password"></div>
                                        </div>
                                    </div>

                                    <div class="form-group "><label
                                                class="col-lg-3 control-label">{{trans('core.base.label.new_password')}}</label>
                                        <div class="col-lg-9">
                                            <input autocomplete="off" name="password" type="password" placeholder=""
                                                   class="form-control">
                                            <div class="form-error" id="form-error-password"></div>
                                        </div>
                                    </div>

                                    <div class="form-group "><label
                                                class="col-lg-3 control-label">{{trans('core.base.label.confirm_password')}}</label>
                                        <div class="col-lg-9">
                                            <input autocomplete="off" name="password_confirmation" type="password"
                                                   placeholder=""
                                                   class="form-control">
                                            <div class="form-error" id="form-error-password_confirmation"></div>
                                        </div>
                                    </div>

                                {!! csrf_field() !!}
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div id="tab-roles" class="tab-pane">
                <div class="panel-body">
                    <div class="row" style="overflow-x: auto;">
                       @include('user.user-roles-list')
                    </div>
                </div>
            </div>

            <div id="tab-companies" class="tab-pane">
                <div class="panel-body">
                    <div class="row" style="overflow-x: auto;">
                        @include('user.user-companies-table')
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    <a href="{{urlWithLng('dashboard')}}" class="btn btn-sm btn-default mc-nav-button-cancel">{{ trans('core.base.button.back') }}</a>
@stop

@section('scripts')
    <script src="{{asset('js/user/main.js')}}"></script>
@endsection