@extends('layouts.app')

@section('title'){{trans('swis.user.title')}}@stop

@section('contentTitle') {{trans('swis.user.title')}} @stop

@section('form-buttons-top')
    {!! renderCancelButton() !!}
@stop

@section('content')
    <?php $languages = activeLanguages(); ?>

    <div class="tabs-container">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tab-general"> {{trans('swis.user.module.tab.user_settings')}}</a></li>
            <li><a data-toggle="tab" href="#tab-roles"> {{trans('swis.user.module.tab.user_roles')}}</a></li>
        </ul>

        <div class="tab-content">
            <div id="tab-general" class="tab-pane active">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <form class="form-horizontal fill-up" id="viewForm">

                                <div class="form-group">
                                    <label class="col-lg-3 control-label pd-t-0">{{trans('swis.user_management.label.add_manually')}}</label>
                                    <div class="col-lg-9">
                                        <input name="added_manually" disabled {{isset($user) && $user->added_manually ? 'checked' : ''}} value="1" id="addedManually" type="checkbox"
                                               placeholder=""
                                               class="">
                                        <div class="form-error" id="form-error-added_manually"></div>
                                    </div>
                                </div>

                                <div class="form-group required"><label
                                            class="col-lg-3 control-label">{{trans('swis.tax_id')}}</label>
                                    <div class="col-lg-9">
                                        <input value="{{$user->ssn or ''}}" name="ssn"
                                               maxlength="{{config('global.tax_id.max')}}" type="text"
                                               placeholder=""
                                               class="form-control get-info-field">
                                        <div class="form-error" id="form-error-ssn"></div>
                                    </div>
                                </div>

                                <div class="form-group required"><label
                                            class="col-lg-3 control-label">{{trans('core.base.label.first_name')}}</label>
                                    <div class="col-lg-9">
                                        <input readonly value="{{$user->first_name or ''}}" id="firstName"
                                               name="first_name"
                                               type="text"
                                               placeholder=""
                                               class="form-control">
                                        <div class="form-error" id="form-error-first_name"></div>
                                    </div>
                                </div>

                                <div class="form-group required"><label
                                            class="col-lg-3 control-label">{{trans('core.base.label.last_name')}}</label>
                                    <div class="col-lg-9">
                                        <input readonly value="{{$user->last_name or ''}}" id="lastName"
                                               name="last_name"
                                               type="text"
                                               placeholder=""
                                               class="form-control">
                                        <div class="form-error" id="form-error-last_name"></div>
                                    </div>
                                </div>

                                <div class="form-group"><label
                                            class="col-lg-3 control-label">{{trans('core.base.label.passport')}}</label>
                                    <div class="col-lg-9">
                                        <input readonly value="{{$user->passport or ''}}" name="passport" type="text"
                                               placeholder=""
                                               class="form-control">
                                        <div class="form-error" id="form-error-passport"></div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">{{trans('core.base.label.passport_issuance_date')}}</label>
                                    <div class="col-lg-9">
                                        <div class='input-group date'>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                            <input readonly autocomplete="off" placeholder="{{setDatePlaceholder()}}" value="{{$user->passport_issue_date ?? ''}}" name="passport_issue_date" type='text' class="form-control"/>
                                        </div>
                                        <div class="form-error" id="form-error-passport_issue_date"></div>
                                    </div>
                                </div>

                                <div class="form-group"><label
                                            class="col-lg-3 control-label">{{trans('core.base.label.passport_issued_by')}}</label>
                                    <div class="col-lg-9">
                                        <input readonly value="{{$user->passport_issued_by or ''}}"
                                               id="passport_issued_by" name="passport_issued_by"
                                               type="text" placeholder=""
                                               class="form-control">
                                        <div class="form-error" id="form-error-passport_issued_by"></div>
                                    </div>
                                </div>

                                <div class="form-group required" id="username-form-group">
                                    <label class="col-lg-3 control-label">{{trans('swis.user_management.label.username')}}</label>
                                    <div class="col-lg-9">
                                        <input value="{{$user->username or ''}}"
                                               name="username"
                                               id="checkUsername"
                                               class="form-control onlyLatinAlpha"
                                               type="text">
                                        <div class="form-error" id="form-error-username"></div>
                                    </div>
                                </div>

                                <div class="username-pass hide">
                                    <div class="form-group ">
                                        <label class="col-lg-3 control-label">{{trans('swis.user_management.label.password')}}</label>
                                        <div class="col-lg-9">
                                            <input value="" name="password" type="text" placeholder="" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group required"
                                     id="emailForm">
                                    <label class="col-lg-3 control-label">{{trans('core.base.label.email')}}</label>
                                    <div class="col-lg-9">
                                        <input value="{{$user->email or ''}}" name="email" type="email" placeholder="" id="email" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group required"><label
                                            class="col-lg-3 control-label">{{trans('core.base.label.phone')}}</label>
                                    <div class="col-lg-9">
                                        <div class="input-group">
                                            <span class="input-group-addon">+</span>
                                            <input value="{{$user->phone_number or ''}}" id="phoneNumber" required
                                                   name="phone_number" type="text"
                                                   placeholder=""
                                                   class="form-control onlyNumbers">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group required">
                                    <label class="col-lg-3 control-label">{{trans('core.base.label.address')}}</label>
                                    <div class="col-lg-9">
                                        <input value="{{$user->registration_address or ''}}" id="address" name="registration_address" type="text" placeholder="" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group"><label class="col-lg-3 control-label">{{trans('swis.default_language')}}</label>
                                    @if(count($languages))
                                        <div class="col-lg-9">
                                            <select class="form-control select2" name="lng_id" id="userLangId">
                                                @foreach($languages as $language)
                                                    <option {{(isset($user) && $user->lng_id == $language->id) ? 'selected' : ''}} value="{{$language->id}}">{{ trans("core.base.language.".strtolower($language->name)) }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endif
                                </div>

                                <div class="multiple tax-id" data-name="company">
                                    <?php $num = 1 ?>
                                     @if(isset($user))
                                            @foreach($user->companies() as $key => $company)
                                                <?php
                                                $userIsHead = $user->isMainLocalAdminOfCompany($company->tax_id);
                                                ?>
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label">{{trans('swis.company.tax_id.label')}}</label>
                                                    @if(is_null($userIsHead))
                                                        <div class="col-lg-9">
                                                            <input autocomplete="off" value="{{$company->tax_id}}"
                                                                   required
                                                                   maxlength="{{config('global.tax_id.max')}}"
                                                                   name="company[{{$num}}][tax_id]" type="text"
                                                                   placeholder=""
                                                                   class="form-control get-info-field">
                                                        </div>

                                                        <label class="col-lg-3 col-lg-offset-3">{{trans('swis.user_management.label.is_company_head')}}</label>
                                                        <div class="col-md-5">
                                                            <input autocomplete="off"
                                                                   data-company-tax-id="{{$company->tax_id}}"
                                                                   name="company[{{$num}}][is_head]"
                                                                   class="company-head"
                                                                   value="1"
                                                                   type="checkbox">
                                                            <span class="company-name">{{(!is_null($company)) ? $company->name : ''}}</span>
                                                        </div>
                                                    @else
                                                        <div class="col-lg-9">
                                                            <input autocomplete="off" value="{{$company->tax_id}}"
                                                                   readonly
                                                                   name="company[{{$num}}][tax_id]" type="text"
                                                                   placeholder="" class="form-control">
                                                        </div>
                                                        <label class="col-lg-3 col-lg-offset-3">{{trans('swis.user_management.label.is_company_head')}}</label>
                                                        <div class="col-md-5">
                                                            <input disabled value="1" checked type="checkbox">
                                                            <span class="company-name">{{(!is_null($company)) ? $company->name : ''}}</span>
                                                        </div>
                                                    @endif

                                                </div>

                                                <?php $num++ ?>
                                        @endforeach
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-lg-3">{{trans('core.base.label.status')}}</label>
                                    <div class="col-lg-9  general-status">
                                        <select name="show_status" class="form-show-status">
                                            <option value="{{\App\Models\User\User::STATUS_ACTIVE}}"{{(!empty($user) && $user->show_status == \App\Models\User\User::STATUS_ACTIVE) ?  ' selected="selected"' : ''}}>{{trans('core.base.label.status.active')}}</option>
                                            <option value="{{\App\Models\User\User::STATUS_INACTIVE}}"{{!empty($user) &&  $user->show_status == \App\Models\User\User::STATUS_INACTIVE ? ' selected="selected"': ''}}>{{trans('core.base.label.status.inactive')}}</option>
                                        </select>
                                        <div class="form-error" id="form-error-show_status"></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>

            <div id="tab-roles" class="tab-pane">
                <div class="panel-body">
                    <div class="row" style="overflow-x: auto;">
                        @include('user.user-roles-list')
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/user/main.js')}}"></script>
@endsection