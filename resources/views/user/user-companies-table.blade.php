<?php
$userCompanies = Auth::user()->companies();
?>
@if(count($userCompanies))
    <table class="table table-bordered table-hover">
        <thead>
        <tr>
            <th>{{trans('swis.tax_id')}}</th>
            <th>{{trans('swis.company_name')}}</th>
            <th>{{trans('swis.position')}}</th>
            <th>{{trans('swis.legal_address')}}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($userCompanies as $key=>$company)
            <tr>
                <td>{{$company->tax_id}}</td>
                <td>{{$company->agency_name or $company->name ?? ''}}</td>
                <td>
                    @if(Auth::user()->isMainLocalAdminOfCompany($company->tax_id))
                        {{trans('swis.user.profile_settings.is_head.title')}}
                    @else
                        {{trans('swis.user.profile_settings.is_authorized_person.title')}}
                    @endif
                </td>
                <td>{{$company->address or '-'}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif
