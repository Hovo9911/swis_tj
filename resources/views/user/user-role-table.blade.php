<table class="table table-bordered table-striped table-bordered table-hover">
    <thead>
    <tr>
        @if($type != 'default')
            <th style="width: 200px">{{trans('swis.dashboard.from_user')}}</th>
        @endif
        <th style="width: 200px">{{trans('swis.module_title')}}</th>
        <th style="width: 300px">{{trans('swis.role.title')}}</th>
        <th>{{trans('swis.user.role.role_attribute.title')}}</th>
        <th style="width: 100px">{{trans('core.base.label.accessibility')}}</th>
        <th style="width: 100px">{{trans('core.base.label.start')}}</th>
        <th style="width: 100px">{{trans('core.base.label.end')}}</th>
        <th style="width: 100px">{{trans('swis.dashboard.roles.status')}}</th>
    </tr>
    </thead>
    <tbody>
    @foreach($dataUserRoles as $userRole)

        @if($userRole->role_id)
            <tr>
                @if($type != \App\Models\Role\Role::ROLE_LEVEL_TYPE_DEFAULT)
                    <?php

                    $authorizerName = '';
                    if (!is_null($userRole->authorizer)) {
                        $authorizerName = $userRole->authorizer->first_name . ' ' . $userRole->authorizer->last_name . ' (' . $userRole->authorizer->ssn . ')';
                    }
                    ?>
                    <td style="width: 200px">{{$authorizerName}}</td>
                @endif
                <td>{{(!empty($userRole->menu)) ? trans($userRole->menu->title) : ''}}</td>
                <td>
                    @if($userRole->role)
                        {{$userRole->role->name or ''}}
                    @endif
                    @if($userRole->role_id == \App\Models\Role\Role::HEAD_OF_COMPANY)
                        <?php $company = $userRole->company; ?>

                        ({{(optional($company)->name)}})
                    @endif
                </td>
                <td>
                    @if(!is_null($userRole->attribute_field_id))
                        <?php
                            $attrValueData = [];
                            if (isset($userRole->attribute_field_id)) {
                                $attrValueData = \App\Models\Role\Role::getRoleAttributeValue($userRole->attribute_field_id, $userRole->attribute_type, $userRole->attribute_value);
                            }
                        ?>

                        @if(isset($attrValueData['attribute_value']))
                            {{$attrValueData['attribute'] }}
                            <hr/>
                            <label>{{trans('swis.roles.attribute-values.label')}}</label>
                            {{$attrValueData['attribute_value']}}
                        @endif

                    @endif

                    @if(!is_null($userRole->multiple_attributes))
                        <?php
                        $reports = \App\Models\Reports\Reports::select('code')->whereIn('id',$userRole->multiple_attributes)->get();
                        ?>
                        @if($reports->count())
                            <?php $reportsList = []; ?>
                            @foreach($reports as $report)
                                 <?php $reportsList[] = trans('swis.'.$report->code.'.menu.title') ?>
                            @endforeach

                            {{implode(', ',$reportsList)}}
                        @endif
                    @endif
                </td>
                <td>{{($userRole->show_only_self) ?  trans("swis.role.show_only_self.only_self") : trans("swis.role.show_only_self.others_too")  }}</td>
                <td>{{$userRole->available_at or ''}}</td>
                <td>{{$userRole->available_to or ''}}</td>
                <td>
                    @if(!empty($userRole->available_to) && $userRole->available_to < currentDate())
                        {{trans('core.base.label.status.blocked')}}
                    @else
                        @if($userRole->role_status == \App\Models\User\User::STATUS_ACTIVE)
                            {{trans('core.base.label.status.active')}}
                        @elseif($userRole->role_status == \App\Models\User\User::STATUS_INACTIVE)
                            {{trans('core.base.label.status.inactive')}}
                        @elseif($userRole->role_status == \App\Models\User\User::STATUS_DELETED)
                            {{trans('core.base.label.status.blocked')}}
                        @endif
                    @endif
                </td>
            </tr>
        @endif

        @if($userRole->role_group_id)
            <?php
            $roleGroup = \App\Models\RolesGroup\RolesGroup::where('id', $userRole->role_group_id)->first();
            ?>
            @if(!is_null($roleGroup) && $roleGroup->roles->count() > 0)
                @foreach($roleGroup->roles as $key=>$role)
                    <tr>
                        @if($type == \App\Models\Role\Role::ROLE_TYPE_COMPANY)
                            <?php

                            $authorizerName = '';
                            if (!is_null($userRole->authorizer)) {
                                $authorizerName = $userRole->authorizer->first_name . ' ' . $userRole->authorizer->last_name . ' (' . $userRole->authorizer->ssn . ')';
                            }
                            ?>

                            <td style="width: 200px">{{$authorizerName}}</td>
                        @endif
                        <td>{{(!empty($userRole->menu)) ? trans($userRole->menu->title) : ''}}</td>
                        <td>
                            {{$role->name or ''}}
                        </td>
                        <td></td>
                        <td>{{($userRole->show_only_self) ?  trans("swis.role.show_only_self.only_self") : trans("swis.role.show_only_self.others_too") }}</td>
                        <td>{{$userRole->available_at or ''}}</td>
                        <td>{{$userRole->available_to or ''}}</td>
                        <td>
                            @if(!empty($userRole->available_to) && $userRole->available_to < currentDate())
                                {{trans('core.base.label.status.inactive')}}
                            @else
                                @if($userRole->role_status == \App\Models\User\User::STATUS_ACTIVE)
                                    {{trans('core.base.label.status.active')}}
                                @elseif($userRole->role_status == \App\Models\User\User::STATUS_INACTIVE)
                                    {{trans('core.base.label.status.inactive')}}
                                @elseif($userRole->role_status == \App\Models\User\User::STATUS_DELETED)
                                    {{trans('core.base.label.status.blocked')}}
                                @endif
                            @endif
                        </td>
                    </tr>

                @endforeach
            @endif
        @endif

    @endforeach
    </tbody>
</table>
