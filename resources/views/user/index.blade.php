@extends('layouts.app')

@section('title'){{trans('swis.user.title')}}@stop

@section('contentTitle') {{trans('swis.user.title')}} @stop

@section('form-buttons-top')
    {!! renderAddButton() !!}
    <a class="m-l btn btn-info btn--plus" href="{{urlWithLng('user_customs_operator/create')}}"><i class="fa fa-plus"></i>{{trans('swis.user.add_custom_operator.button')}}</a>

    <a class="m-l btn btn--cancel btn--plus" href="{{urlWithLng('user/service-info')}}"></i>{{trans('swis.user.service_info.button')}}</a>
@stop

@section('content')

    <div class="clearfix search__out">
        <button class="btn btn-success pull-right search__btn" type="button" data-toggle="collapse"
                data-target="#collapseSearch" aria-expanded="false" aria-controls="collapseSearch">
            <i class="fa fa-search"> </i><span
                    class="search__txt ver-top-box">{{trans('core.base.button.search')}}</span>
        </button>
    </div>
    <div class="collapse" id="collapseSearch">
        <div class="card card-body">
            <form class="form-horizontal fill-up" id="search-filter" autocomplete="off">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-4 control-label label__title">ID</label>
                            <div class="col-md-8">

                                <input type="number" min="0" class="form-control onlyNumbers" name="id" id="" value="">
                                <div class="form-error" id="form-error-id"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label label__title">{{trans('core.base.label.passport')}}</label>
                            <div class="col-md-8">

                                <input type="text" class="form-control" name="passport" id="" value="">
                                <div class="form-error" id="form-error-passport"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label label__title">{{trans('core.base.label.ssn')}}</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="ssn" id="" value="">
                                <div class="form-error" id="form-error-ssn"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label label__title">{{trans('swis.company_tax_id')}}</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="company_tax_id" id="" value="">
                                <div class="form-error" id="form-error-company_tax_id"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label label__title">{{trans('swis.user.is_head.label')}}</label>
                            <div class="col-md-8">
                                <input type="checkbox" value="1" name="is_head" style="margin-top: 10px">
                                <div class="form-error" id="form-error-is_head"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label label__title">{{trans('swis.user.name_of_company_label')}}</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="company_name" id="" value="">
                                <div class="form-error" id="form-error-company_name"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label label__title">{{trans('swis.authorzied_person.authorizer.snn')}}</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="authorizer_ssn" id="" value="">
                                <div class="form-error" id="form-error-authorizer_ssn"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label label__title">{{trans('swis.authorzied_person.authorizer.authorizer_first_name')}}</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="authorizer_first_name" id="" value="">
                                <div class="form-error" id="form-error-authorizer_first_name"></div>
                            </div>
                        </div>

                        <div class="form-group m-b-none">
                            <label class="col-md-4 control-label label__title">{{trans('swis.authorzied_person.authorizer.authorizer_last_name')}}</label>
                            <div class="col-md-8">

                                <input type="text" class="form-control" name="authorizer_last_name" id="" value="">
                                <div class="form-error" id="form-error-authorizer_last_name"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">

                        <div class="form-group">
                            <label class="col-md-4 control-label label__title">{{trans('core.base.label.first_name')}}</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="first_name" id="" value="">
                                <div class="form-error" id="form-error-first_name"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label label__title">{{trans('core.base.label.last_name')}}</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="last_name" id="" value="">
                                <div class="form-error" id="form-error-last_name"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">{{trans('swis.user.registered_at.label')}}</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class='input-group date'>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                            <input value="" name="registered_at_start" autocomplete="off"
                                                   type="text" placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                        </div>
                                        <div class="form-error" id="form-error-registered_at_start"></div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class='input-group date'>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                            <input value="" name="registered_at_end" autocomplete="off"
                                                   type="text" placeholder="{{setDatePlaceholder()}}" class="form-control"/>
                                        </div>
                                        <div class="form-error" id="form-error-registered_at_end"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label label__title">{{trans('swis.user.register_username.name')}}</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="registered_username" id="" value="">
                                <div class="form-error" id="form-error-register_username"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label label__title">{{trans('swis.user.module.title')}}</label>
                            <div class="col-md-8">
                                @if($modules->count())
                                    <select class="select2" name="menu" id="menu">
                                        <option></option>
                                        @foreach($modules as $menu)
                                            <option value="{{$menu->id}}">{{trans($menu->title)}}</option>
                                        @endforeach
                                    </select>
                                @endif
                                <div class="form-error" id="form-error-menu"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label label__title">{{trans('swis.user.role.label')}}</label>
                            <div class="col-md-8">
                                <select class="select2" name="role" id="roleList">
                                    <option value="">{{trans('core.base.label.select')}}</option>
                                </select>
                                <div class="form-error" id="form-error-role"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label label__title">{{trans('swis.user.select_sub_division.label')}}</label>
                            <div class="col-md-8">
                                <select class="select2" name="sub_division_id">
                                    <option value=""></option>
                                    @if($subDivisions->count())
                                        @foreach($subDivisions as $subDivision)
                                            <option value="{{$subDivision->id}}"> {{ $subDivision->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <div class="form-error" id="form-error-sub_division_id"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label label__title">{{trans('swis.user.user_type.label')}}</label>
                            <div class="col-md-8">
                                <select class="select2" name="user_type">
                                    <option></option>
                                    @foreach(\App\Models\User\User::USER_TYPES as $userType)
                                        <option value="{{$userType}}">{{trans('swis.user.user_type.'.$userType.'.option')}}</option>
                                    @endforeach
                                </select>
                                <div class="form-error" id="form-error-user_type"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label label__title">{{trans('swis.user_management.label.custom_bodies.label')}}</label>
                            <div class="col-md-8">
                                <select class="select2" name="ref_custom_body">
                                    <option></option>
                                    @foreach($refCustomBodies as $customBody)
                                        <option value="{{$customBody->code}}">{{'('.$customBody->code.')'}} {{$customBody->name}}</option>
                                    @endforeach
                                </select>
                                <div class="form-error" id="form-error-ref_custom_body"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label label__title">{{trans('core.base.label.show_status')}}</label>
                            <div class="col-md-8">
                                <select class="form-control select2 default-search" name="show_status">
                                    <option value="{{\App\Models\BaseModel::STATUS_ACTIVE}}">{{trans('core.base.label.status.active')}}</option>
                                    <option value="{{\App\Models\BaseModel::STATUS_INACTIVE}}">{{trans('core.base.label.status.inactive')}}</option>
                                </select>
                                <div class="form-error" id="form-error-show_status"></div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-offset-4 col-md-8 col-lg-offset-3 text-right">
                            <div class="row">
                                <div class="col-lg-10 col-md-8">
                                    <button type="submit"
                                            class="btn btn-primary mr-10">{{trans('core.base.label.search')}}</button>
                                    <button type="button" class="btn btn-danger resetButton"
                                            data-submit="true">{{trans('core.base.label.reset')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="data-table">
        <div class="clearfix">
            <table class="table table-striped table-bordered table-hover dataTables-example" id="list-data">
                <thead>
                <tr>
                    <th data-col="actions" class="th-actions" data-delete="0">{{trans('core.base.label.actions')}}</th>
                    <th data-col="id">ID</th>
                    <th data-col="ssn">{{trans('core.base.label.ssn')}}</th>
                    <th data-col="passport">{{trans('core.base.label.passport')}}</th>
                    <th data-col="first_name">{{trans('core.base.label.first_name')}}</th>
                    <th data-col="last_name">{{trans('core.base.label.last_name')}}</th>
                    <th data-col="email">{{trans('core.base.label.email')}}</th>
                    <th data-col="registered_at">{{trans('swis.user.registered_at.label')}}</th>
                    <th data-col="registered_username">{{trans('swis.user.register_username.name')}}</th>
                    <th data-col="phone_number">{{trans('core.base.label.phone')}}</th>
                    <th data-col="show_status">{{trans('core.base.label.status')}}</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/user/main.js')}}"></script>
@endsection