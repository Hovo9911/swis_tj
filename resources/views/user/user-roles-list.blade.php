{{--Default Roles--}}
@if(count($userRolesList['defaultRoles']))
    <div class="col-md-12">
        <h3>{{trans('swis.dashboard.default_role.title')}}</h3>
        @include('user.user-role-table',['dataUserRoles'=>$userRolesList['defaultRoles'],'type'=>'default'])
    </div>
@endif

{{--Sw Admin Roles--}}
@if(count($userRolesList['swAdminRoles']))
    <div class="col-md-12">
        <h3>{{trans('swis.dashboard.sw_admin_role.title')}}</h3>
        @include('user.user-role-table',['dataUserRoles'=>$userRolesList['swAdminRoles'],'type'=>'swAdmin'])
    </div>
@endif

{{--User Roles--}}
@if(count($userRolesList['userAuthorizedRoles']))
    <div class="col-md-12">
        @foreach($userRolesList['userAuthorizedRoles'] as $userId=>$dataUserRoles)
            <h3>{{\App\Models\User\User::getUserNameByID($userId)}}</h3>
            @include('user.user-role-table',['dataUserRoles'=>$dataUserRoles,'type'=>'user'])
        @endforeach
    </div>
@endif

{{--Company Roles--}}
@if(count($userRolesList['companyAuthorizedRoles']))
    <div class="col-md-12">
        @foreach($userRolesList['companyAuthorizedRoles'] as $companyTaxID=>$dataUserRoles)
            <?php $company = \App\Models\Company\Company::getAuthorizedCompanyInfo($companyTaxID) ?>
            <h3>{{!is_null($company) ? $company->company_name . ' ('.$company->tax_id.')' : ''}}</h3>
            @include('user.user-role-table',['dataUserRoles'=>$dataUserRoles,'type'=>'company'])
        @endforeach
    </div>
@endif