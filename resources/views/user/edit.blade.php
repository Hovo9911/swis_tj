@extends('layouts.app')

@section('title'){{trans('swis.user.title')}}@stop

@section('contentTitle')

    @switch($saveMode)
        @case('add')
        {{trans('swis.user.create.title')}}
        @break
        @case('edit')
        {{trans('swis.base.edit')}}
        @break

    @endswitch

@stop

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('content')
    <?php
    switch ($saveMode) {
        case 'add':
            $url = 'user';
            break;
        case 'edit':
            $url = 'user/update/' . $user->id;
            break;
    }

    $languages = activeLanguages();

    ?>

    <div class="tabs-container">
        <ul class="nav nav-tabs">
            <li class="active">
                <a data-toggle="tab" href="#tab-general"> {{trans('swis.user.module.tab.user_settings')}}</a></li>
            @if($saveMode == 'edit')
                <li><a data-toggle="tab" href="#tab-roles"> {{trans('swis.user.module.tab.user_roles')}}</a></li>
            @endif
        </ul>

        <div class="tab-content">
            <div id="tab-general" class="tab-pane active">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <form class="form-horizontal fill-up" autocomplete="off" id="form-data" action="{{urlWithLng($url)}}">

                                <div class="form-group">
                                    <div class="col-md-9 col-md-offset-3">
                                        <div id="userInfoServiceMessage" class="cjs-messages"></div>
                                    </div>
                                </div>

                                @if($saveMode == 'edit')
                                    <div class="form-group">
                                        <div class="col-md-9 col-md-offset-3">
                                            <div id="userInfoServiceMessage" class="cjs-messages"></div>

                                            <button class="btn btn-success" id="updateUserInfoFromService" type="button">
                                                <i class="fa"></i> {{trans('swis.user.update_info.button')}}
                                            </button>
                                            <hr/>
                                        </div>
                                    </div>
                                @endif

                                <div class="form-group">
                                    <label class="col-lg-3 control-label pd-t-0">{{trans('swis.user_management.label.add_manually')}}</label>
                                    <div class="col-lg-9">
                                        <input name="added_manually" {{isset($user) && $user->added_manually ? 'checked' : ''}} value="1" id="addedManually" type="checkbox"
                                               placeholder=""
                                               class="">
                                        <div class="form-error" id="form-error-added_manually"></div>
                                    </div>
                                </div>

                                <div class="form-group required"><label
                                            class="col-lg-3 control-label">{{trans('swis.tax_id')}}</label>
                                    <div class="col-lg-9">
                                        <input value="{{$user->ssn or ''}}" name="ssn" id="userSSN"
                                               @if($saveMode != 'add')  readonly
                                               @endif maxlength="{{config('global.tax_id.max')}}"
                                               type="text" placeholder=""
                                               class="form-control get-info-field">
                                        <div class="form-error" id="form-error-ssn"></div>
                                    </div>
                                </div>

                                <div class="form-group required"><label
                                            class="col-lg-3 control-label">{{trans('core.base.label.first_name')}}</label>
                                    <div class="col-lg-9">
                                        <input readonly value="{{$user->first_name or ''}}" id="firstName"
                                               name="first_name" type="text"
                                               placeholder=""
                                               class="form-control">
                                        <div class="form-error" id="form-error-first_name"></div>
                                    </div>
                                </div>

                                <div class="form-group required"><label
                                            class="col-lg-3 control-label">{{trans('core.base.label.last_name')}}</label>
                                    <div class="col-lg-9">
                                        <input readonly value="{{$user->last_name or ''}}" id="lastName"
                                               name="last_name" type="text"
                                               placeholder=""
                                               class="form-control">
                                        <div class="form-error" id="form-error-last_name"></div>
                                    </div>
                                </div>

                                <div class="form-group required"><label
                                            class="col-lg-3 control-label">{{trans('core.base.label.passport')}}</label>
                                    <div class="col-lg-9">
                                        <input value="{{$user->passport or ''}}" id="passport" readonly
                                               name="passport"
                                               type="text" placeholder=""
                                               class="form-control">
                                        <div class="form-error" id="form-error-passport"></div>
                                    </div>
                                </div>

                                <div id="passportFields" class="{{$saveMode == 'add' ? 'hide' : ''}}">
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">{{trans('core.base.label.passport_issuance_date')}}</label>
                                        <div class="col-lg-9">
                                            <div class='input-group date'>
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                <input readonly autocomplete="off" placeholder="{{setDatePlaceholder()}}" value="{{$user->passport_issue_date ?? ''}}" id="passport_issue_date" name="passport_issue_date" type='text' class="form-control"/>
                                            </div>
                                            <div class="form-error" id="form-error-passport_issue_date"></div>
                                        </div>
                                    </div>

                                    <div class="form-group"><label
                                                class="col-lg-3 control-label">{{trans('core.base.label.passport_issued_by')}}</label>
                                        <div class="col-lg-9">
                                            <input readonly value="{{$user->passport_issued_by or ''}}"
                                                   id="passport_issued_by" name="passport_issued_by"
                                                   type="text" placeholder=""
                                                   class="form-control">
                                            <div class="form-error" id="form-error-passport_issued_by"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group"><label
                                            class="col-lg-3 control-label pd-t-0">{{trans('swis.user_management.label.send_for_confirmation')}}</label>
                                    <div class="col-lg-9">
                                        <input name="send_for_confirmation" value="1" id="sendConfirmation"
                                               {{($saveMode == 'add' || isset($user->username)) ? 'checked' : '' }} type="checkbox"
                                               placeholder=""
                                               class="">
                                        <div class="form-error" id="form-error-send_for_confirmation"></div>
                                    </div>
                                </div>

                                <div class="form-group required" id="username-form-group">
                                    <label class="col-lg-3 control-label">{{trans('swis.user_management.label.username')}}</label>
                                    <div class="col-lg-9">
                                        <input value="{{$user->username or ''}}"
                                               name="username"
                                               id="checkUsername"
                                               class="form-control onlyLatinAlpha"
                                               type="text">
                                        <div class="form-error" id="form-error-username"></div>
                                    </div>
                                </div>

                                <div class="form-group hidden-suggestion-list-block">
                                    <label class="col-lg-3 control-label"></label>
                                    <div class="col-lg-9">
                                        <div class="list-group hidden-suggestion-list"></div>
                                    </div>
                                </div>

                                <div class="username-pass hide">
                                    <div class="form-group {{($saveMode == 'edit') ? '' : 'required'}}">

                                        <label class="col-lg-3 control-label">{{trans('swis.user_management.label.password')}}</label>
                                        <div class="col-lg-9">
                                            <input {{($saveMode == 'edit') ? '' : 'disabled'}} value="" name="password"
                                                   type="text"
                                                   placeholder=""
                                                   class="form-control">
                                            <div class="form-error" id="form-error-password"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group required"
                                     id="emailForm" {!! ($saveMode == 'edit') ? ' data-edit="true"' : '' !!}><label
                                            class="col-lg-3 control-label">{{trans('core.base.label.email')}}</label>
                                    <div class="col-lg-6">
                                        <input value="{{$user->email or ''}}"
                                               name="email"
                                               type="email" placeholder=""
                                               id="email"
                                               class="form-control">
                                        <div class="form-error" id="form-error-email"></div>
                                    </div>
                                    <div class="col-md-3">
                                        <button class="btn btn-primary" id="emailSendAgain"
                                                type="button">{{trans('swis.user.email_send_again.button')}}</button>
                                    </div>
                                </div>

                                <div class="form-group required"><label
                                            class="col-lg-3 control-label">{{trans('core.base.label.phone')}}</label>
                                    <div class="col-lg-9">
                                        <div class="input-group">
                                            <span class="input-group-addon">+</span>
                                            <input value="{{$user->phone_number or ''}}" id="phoneNumber" required
                                                   name="phone_number" type="text"
                                                   placeholder=""
                                                   class="form-control onlyNumbers">
                                        </div>
                                        <div class="form-error" id="form-error-phone_number"></div>
                                    </div>
                                </div>

                                <div class="form-group required"><label
                                            class="col-lg-3 control-label">{{trans('core.base.label.address')}}</label>
                                    <div class="col-lg-9">
                                        <input value="{{$user->registration_address or ''}}" id="address"
                                               name="registration_address"
                                               type="text"
                                               placeholder=""
                                               class="form-control">
                                        <div class="form-error" id="form-error-registration_address"></div>
                                    </div>
                                </div>

                                <div class="form-group"><label class="col-lg-3 control-label">{{trans('swis.default_language')}}</label>
                                    @if(count($languages) > 0)
                                        <div class="col-lg-9">
                                            <select class="form-control select2" name="lng_id" id="userLangId">
                                                @foreach($languages as $language)
                                                    <option {{(isset($user) && $user->lng_id == $language->id) ? 'selected' : ''}} value="{{$language->id}}">{{ trans("core.base.language.".strtolower($language->name)) }}</option>
                                                @endforeach
                                            </select>
                                            <div class="form-error" id="form-error-agency_id"></div>
                                        </div>
                                    @endif
                                </div>

                                <div class="form-group"><label class="col-lg-3 control-label">{{trans('swis.user.additional_information')}}</label>
                                    <div class="col-lg-9">
                                        <textarea class="form-control" name="additional_information" rows="3">{{$user->additional_information or ''}}</textarea>
                                        <div class="form-error" id="form-error-additional_information"></div>
                                    </div>
                                </div>

                                <div class="multiple tax-id" data-name="company">
                                    <?php $num = 1 ?>
                                    @if(isset($user))
                                        @foreach($user->companies() as $key => $company)
                                            <?php
                                            $userIsHead = $user->isMainLocalAdminOfCompany($company->tax_id);
                                            ?>
                                            <div class="form-group "><label class="col-lg-3 control-label">{{trans('swis.company.tax_id.label')}}</label>
                                                @if(!$userIsHead)
                                                    <div class="col-lg-9">
                                                        <div class="input-group ">
                                                            <input autocomplete="off" value="{{$company->tax_id}}"
                                                                   required
                                                                   maxlength="{{config('global.tax_id.max')}}"
                                                                   name="company[{{$num}}][tax_id]" type="text"
                                                                   placeholder=""
                                                                   class="form-control get-info-field">
                                                            <span class="input-group-btn"><button
                                                                        class="btn btn-remove btn-danger"
                                                                        type="button"><span
                                                                            class="glyphicon  glyphicon-minus"></span></button></span>
                                                        </div>
                                                        <div class="form-error"
                                                             id="form-error-company-{{$num}}-tax_id"></div>
                                                    </div>

                                                    <label class="col-lg-3 col-lg-offset-3">{{trans('swis.user_management.label.is_company_head')}}</label>
                                                    <div class="col-md-5">
                                                        <input autocomplete="off"
                                                               data-company-tax-id="{{$company->tax_id}}"
                                                               name="company[{{$num}}][is_head]"
                                                               class="company-head" value="1"
                                                               type="checkbox">
                                                        <span class="company-name">{{$company->name}}</span>
                                                    </div>
                                                @else
                                                    <div class="col-lg-9">
                                                        <input autocomplete="off" value="{{$company->tax_id}}"
                                                               readonly name="company[{{$num}}][tax_id]" type="text"
                                                               placeholder="" class="form-control">
                                                    </div>
                                                    <label class="col-lg-3 col-lg-offset-3">{{trans('swis.user_management.label.is_company_head')}}</label>
                                                    <div class="col-md-5">
                                                        <input disabled value="1" checked type="checkbox">
                                                        <span class="company-name">{{$company->name}}</span>
                                                    </div>
                                                @endif

                                            </div>

                                            <?php $num++ ?>
                                        @endforeach
                                    @endif
                                    <div class="form-group "><label
                                                class="col-lg-3 control-label">{{trans('swis.company.tax_id.label')}}</label>
                                        <div class=" col-lg-9">
                                            <div class="input-group">
                                                <input value="" autocomplete="off" required
                                                       name="company[{{$num}}][tax_id]"
                                                       maxlength="{{config('global.tax_id.max')}}" type="text"
                                                       placeholder=""
                                                       class="form-control get-info-field">

                                                <span class="input-group-btn">
                                    <button class="btn btn-primary btn-add" type="button"><span
                                                class="glyphicon glyphicon-plus"></span></button>
                                </span>
                                            </div>
                                            <div class="form-error" id="form-error-company-{{$num}}-tax_id"></div>
                                        </div>
                                        <label class="col-lg-3 col-lg-offset-3">{{trans('swis.user_management.label.is_company_head')}}</label>
                                        <div class="col-md-5">
                                            <input class="company-head" disabled name="company[{{$num}}][is_head]"
                                                   value="1" type="checkbox">
                                            <span class="company-name"></span>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-3">{{trans('core.base.label.status')}}</label>
                                    <div class="col-lg-9  general-status">
                                        <select name="show_status" class="form-show-status">
                                            <option value="{{\App\Models\User\User::STATUS_ACTIVE}}"{{(isset($user) && $user->show_status == \App\Models\User\User::STATUS_ACTIVE) || $saveMode == 'add' ?  ' selected="selected"' : ''}}>{{trans('core.base.label.status.active')}}</option>
                                            <option value="{{\App\Models\User\User::STATUS_INACTIVE}}"{{isset($user) && $user->show_status == \App\Models\User\User::STATUS_INACTIVE ? ' selected="selected"': ''}}>{{trans('core.base.label.status.inactive')}}</option>
                                        </select>
                                        <div class="form-error" id="form-error-show_status"></div>
                                    </div>
                                </div>

                                <input type="hidden" value="{{$user->id or 0}}" name="id" class="mc-form-key"/>

                                {!! csrf_field() !!}
                            </form>
                        </div>
                    </div>

                </div>
            </div>
            @if($saveMode == 'edit')
                <div id="tab-roles" class="tab-pane">
                    <div class="panel-body">
                        <div class="row" style="overflow-x: auto;">
                            @include('user.user-roles-list')
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>

    <div class="modal fade" id="companyAdminModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{trans('swis.user.change_company_admin.title')}}</h4>
                </div>
                <div class="modal-body">
                    <p>{{trans('swis.user.change_company_admin.current_admin.name')}}: <span class="admin-name"></span>
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success"
                            data-dismiss="modal">{{trans('swis.user.change_company_admin.save')}}</button>
                    <button class="btn btn-default"
                            id="cancelChangeAdmin">{{trans('swis.user.change_company_admin.cancel')}}</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script src="{{asset('js/user/main.js')}}"></script>
@endsection