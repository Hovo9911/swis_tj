@extends('layouts.app')

@section('title'){{trans('swis.user.service_info.title')}}@stop

@section('contentTitle')

    {{trans('swis.user.service_info.title')}}

@stop

@section('form-buttons-top')
    <button type="submit" class="btn mr-10 btn-success mc-nav-button-save">{{trans('swis.user.get_service_info.btn')}}</button>
    {!! renderCancelButton() !!}
@stop

@section('content')

    <div class="tabs-container">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-10 ">
                <form class="form-horizontal fill-up" autocomplete="off" id="form-data" action="{{urlWithLng('user/service-info')}}">

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <div id="userInfoServiceMessage" class="cjs-messages"></div>
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-lg-3 control-label">{{trans('swis.tax_id')}}</label>
                        <div class="col-lg-9">
                            <input value="" name="tin" id="tin" maxlength="{{config('global.tax_id.max')}}"
                                   type="text" placeholder=""
                                   class="form-control">
                            <div class="form-error" id="form-error-tin"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">{{trans('swis.user.service_info.result')}}</label>
                        <div class="col-lg-9">
                            <textarea class="form-control" rows="30" id="serviceInfo" readonly></textarea>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    </div>

@endsection

@section('form-buttons-bottom')
    <button type="submit" class="btn mr-10 btn-success mc-nav-button-save">{{trans('swis.user.get_service_info.btn')}}</button>
    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script src="{{asset('js/user/service-info.js')}}"></script>
@endsection