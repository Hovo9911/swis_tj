@extends('layouts.app')

@section('title'){{trans('swis.user.title')}}@stop

@section('contentTitle')
    {{trans('swis.base.view')}}
@stop

@section('content')
    <?php $languages = activeLanguages(); ?>

    <div class="tabs-container">

        <div class="tab-content">
            <div id="tab-general" class="tab-pane active">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <form class="form-horizontal fill-up" autocomplete="off" id="viewForm">

                                <div class="form-group"><label
                                            class="col-lg-3 control-label">{{trans('core.base.label.first_name')}}</label>
                                    <div class="col-lg-9">
                                        <input value="{{$user->first_name or ''}}"  type="text"
                                               placeholder=""
                                               class="form-control">
                                    </div>
                                </div>

                                <div class="form-group"><label
                                            class="col-lg-3 control-label">{{trans('core.base.label.last_name')}}</label>
                                    <div class="col-lg-9">
                                        <input value="{{$user->last_name or ''}}" type="text"
                                               placeholder=""
                                               class="form-control">
                                    </div>
                                </div>

                                <div class="form-group"><label
                                            class="col-lg-3 control-label">{{trans('core.base.label.phone')}}</label>
                                    <div class="col-lg-9">
                                        <input value="{{$user->phone_number or ''}}" type="text"
                                               placeholder=""
                                               class="form-control">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-lg-3 control-label label__title">{{trans('swis.user_management.label.custom_bodies.label')}}</label>
                                    <div class="col-lg-9 field__one-col">
                                        <input value="@if(!is_null($refCustomBody)){{'('.$refCustomBody->code.')'}} {{$refCustomBody->name}} @endif" type="text"
                                               placeholder=""
                                               class="form-control">
                                    </div>
                                </div>

                                <div class="form-group" id="username-form-group">
                                    <label class="col-lg-3 control-label">{{trans('swis.user_management.label.username')}}</label>
                                    <div class="col-lg-9">
                                        <input value="{{$user->username or ''}}"
                                               name="username"
                                               class="form-control onlyLatinAlpha"
                                               type="text">
                                    </div>
                                </div>

                                <div class="form-group"><label
                                            class="col-lg-3 control-label">{{trans('swis.default_language')}}</label>
                                    @if(count($languages) > 0)
                                        <div class="col-lg-9">
                                            <select class="form-control select2" >
                                                @foreach($languages as $language)
                                                    <option {{(isset($user) && $user->lng_id == $language->id) ? 'selected' : ''}} value="{{$language->id}}">{{ trans("core.base.language.".strtolower($language->name)) }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endif

                                </div>

                                <div class="form-group">
                                    <label class="control-label col-lg-3">{{trans('core.base.label.status')}}</label>
                                    <div class="col-lg-9  general-status">
                                        <select class="form-show-status">
                                            <option value="{{\App\Models\User\User::STATUS_ACTIVE}}"{{(!empty($user) && $user->show_status == \App\Models\User\User::STATUS_ACTIVE) ?  ' selected="selected"' : ''}}>{{trans('core.base.label.status.active')}}</option>
                                            <option value="{{\App\Models\User\User::STATUS_INACTIVE}}"{{!empty($user) &&  $user->show_status == \App\Models\User\User::STATUS_INACTIVE ? ' selected="selected"': ''}}>{{trans('core.base.label.status.inactive')}}</option>
                                        </select>
                                    </div>
                                </div>


                                {!! csrf_field() !!}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/user/main.js')}}"></script>
@endsection