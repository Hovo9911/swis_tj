@extends('layouts.app')

@section('title'){{trans('swis.user.title')}}@stop

@section('contentTitle')

    @switch($saveMode)
        @case('add')
        {{trans('swis.user.customs_operator.create.title')}}
        @break
        @case('edit')
        {{trans('swis.base.edit')}}
        @break

    @endswitch

@stop

@section('form-buttons-top')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('content')
    <?php
    switch ($saveMode) {
        case 'add':
            $url = 'user_customs_operator';
            break;
        case 'edit':
            $url = 'user_customs_operator/update/' . $user->id;
            break;
    }

    $languages = activeLanguages();

    ?>

    <div class="tabs-container">

        <div class="tab-content">
            <div id="tab-general" class="tab-pane active">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <form class="form-horizontal fill-up" autocomplete="off" id="form-data-customs-operator"
                                  action="{{urlWithLng($url)}}">

                                <div class="form-group required"><label
                                            class="col-lg-3 control-label">{{trans('core.base.label.first_name')}}</label>
                                    <div class="col-lg-9">
                                        <input value="{{$user->first_name or ''}}" name="first_name" type="text"
                                               placeholder=""
                                               class="form-control">
                                        <div class="form-error" id="form-error-first_name"></div>
                                    </div>
                                </div>

                                <div class="form-group required"><label
                                            class="col-lg-3 control-label">{{trans('core.base.label.last_name')}}</label>
                                    <div class="col-lg-9">
                                        <input value="{{$user->last_name or ''}}" name="last_name" type="text"
                                               placeholder=""
                                               class="form-control">
                                        <div class="form-error" id="form-error-last_name"></div>
                                    </div>
                                </div>

                                <div class="form-group"><label
                                            class="col-lg-3 control-label">{{trans('core.base.label.phone')}}</label>
                                    <div class="col-lg-9">
                                        <div class="input-group">
                                            <span class="input-group-addon">+</span>
                                            <input value="{{$user->phone_number or ''}}"
                                                   name="phone_number" type="text"
                                                   placeholder=""
                                                   class="form-control onlyNumbers">
                                        </div>
                                        <div class="form-error" id="form-error-phone_number"></div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label label__title">{{trans('swis.user_management.label.custom_bodies.label')}}</label>
                                    <div class="col-lg-9 field__one-col">
                                        <select class="select2" multiple name="ref_custom_body[]">
                                            @foreach($refCustomBodies as $customBody)
                                                <option {{isset($user) && $user->ref_custom_body &&  in_array($customBody->code,$user->ref_custom_body) ? 'selected' : ''}} value="{{$customBody->code}}">{{'('.$customBody->code.')'}} {{$customBody->name}}</option>
                                            @endforeach
                                        </select>
                                        <div class="form-error" id="form-error-ref_custom_body"></div>
                                    </div>
                                </div>

                                <div class="form-group required" id="username-form-group">
                                    <label class="col-lg-3 control-label">{{trans('swis.user_management.label.username')}}</label>
                                    <div class="col-lg-9">
                                        <input value="{{$user->username or ''}}"
                                               name="username"
                                               class="form-control onlyLatinAlpha"
                                               type="text">
                                        <div class="form-error" id="form-error-username"></div>
                                    </div>
                                </div>

                                <div class="username-pass">
                                    <div class="form-group {{($saveMode == 'edit') ? '' : 'required'}}">

                                        <label class="col-lg-3 control-label">{{trans('swis.user_management.label.password')}}</label>
                                        <div class="col-lg-9">
                                            <input value="" name="password" type="text"
                                                   placeholder=""
                                                   class="form-control">
                                            <div class="form-error" id="form-error-password"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group"><label
                                            class="col-lg-3 control-label">{{trans('swis.default_language')}}</label>
                                    @if(count($languages) > 0)
                                        <div class="col-lg-9">
                                            <select class="form-control select2" name="lng_id" id="userLangId">
                                                @foreach($languages as $language)
                                                    <option {{(isset($user) && $user->lng_id == $language->id) ? 'selected' : ''}} value="{{$language->id}}">{{ trans("core.base.language.".strtolower($language->name)) }}</option>
                                                @endforeach
                                            </select>
                                            <div class="form-error" id="form-error-agency_id"></div>
                                        </div>
                                    @endif

                                </div>

                                <div class="form-group">
                                    <label class="control-label col-lg-3">{{trans('core.base.label.status')}}</label>
                                    <div class="col-lg-9  general-status">
                                        <select name="show_status" class="form-show-status">
                                            <option value="{{\App\Models\User\User::STATUS_ACTIVE}}"{{(!empty($user) && $user->show_status == \App\Models\User\User::STATUS_ACTIVE) || $saveMode == 'add' ?  ' selected="selected"' : ''}}>{{trans('core.base.label.status.active')}}</option>
                                            <option value="{{\App\Models\User\User::STATUS_INACTIVE}}"{{!empty($user) &&  $user->show_status == \App\Models\User\User::STATUS_INACTIVE ? ' selected="selected"': ''}}>{{trans('core.base.label.status.inactive')}}</option>
                                        </select>
                                        <div class="form-error" id="form-error-show_status"></div>
                                    </div>
                                </div>

                                <input type="hidden" value="{{$user->id or 0}}" name="id" class="mc-form-key"/>

                                {!! csrf_field() !!}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('form-buttons-bottom')
    {!! renderEditButton() !!}
    {!! renderCancelButton() !!}
@stop

@section('scripts')
    <script src="{{asset('js/user/main.js')}}"></script>
@endsection