<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        TaxServiceTjInnNotFoundException::class,
        TaxServiceTjCompanyInactiveException::class,
        TaxServiceTjInvalidResponseException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param \Exception $exception
     * @return void
     *
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param Exception $exception
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector|mixed|\Symfony\Component\HttpFoundation\Response|void
     * @throws Exception
     */
    public function render($request, Exception $exception)
    {
        if ($this->isHttpException($exception)) {
            switch ($exception->getStatusCode()) {
                case 404:
                    return response()->view('errors.404');
                    break;
            }
        }

        if ($request->is('*/table*') && $exception->getCode() == '22P02') {
            return response()->json([
                "draw" => 0,
                "recordsTotal" => 0,
                "recordsFiltered" => 0,
                "data" => []
            ]);
        }

        // Tax Service TJ system
        if ($exception instanceof TaxServiceTjInvalidResponseException) {

            if ($request->ajax()) {
                $errors['message'] = trans('swis.tax_service_tj.connection_failed.message');
                $errors['taxService'] = true;

                return responseResult('INVALID_DATA', '', '',$errors);
            }

            return redirect(urlWithLng('login'));
        }

        // Tax Service TJ Company is Inactive
        if ($exception instanceof TaxServiceTjCompanyInactiveException) {

            if ($request->ajax()) {
                $errors['message'] = trans('swis.tax_service_tj.company_is_inactive.message');
                $errors['taxService'] = true;

                return responseResult('INVALID_DATA', '', '',$errors);
            }

            return redirect(urlWithLng('login'));
        }

        // Tax Service TJ Tin data is invalid Format
        if ($exception instanceof TaxServiceTjInnNotFoundException) {

            if ($request->ajax()) {
                $errors['message'] = trans('swis.tax_service_tj.tin_not_found.message');
                $errors['taxService'] = true;

                return responseResult('INVALID_DATA', '', '',$errors);
            }

            return redirect(urlWithLng('login'));
        }

        // Token Mismatch
        if ($exception instanceof \Illuminate\Session\TokenMismatchException) {

            if ($request->ajax()) {
                Session::flash('error', trans('swis.core.token_mismatch_exception'));

                $errors['needLogin'] = true;
//                return responseResult('INVALID_DATA', urlWithLng('/'), '', ['message' => trans('swis.core.token_mismatch_exception')], 419);
                return responseResult('OK', urlWithLng('login'),'',$errors);
            }

            return redirect(urlWithLng('login'));
        }

        // 404
        if ($exception instanceof \Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException) {
            return abort('404');
        }

        return parent::render($request, $exception);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param AuthenticationException $exception
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        Session::flash('error', trans('swis.core.token_mismatch_exception'));

        if ($request->expectsJson()) {
            $errors['needLogin'] = true;
            return responseResult('OK', '', '', $errors);
        }

        return redirect()->guest(urlWithLng('login'));

        /*     return $request->expectsJson()
                 ? responseResult('OK', urlWithLng('/login'),'','',401)
                 ? response()->json(['message' => $exception->getMessage()], 401)
                 : redirect()->guest(urlWithLng('login'));*/
    }
}
