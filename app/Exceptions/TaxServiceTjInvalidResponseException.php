<?php

namespace App\Exceptions;

use Exception;

class TaxServiceTjInvalidResponseException extends Exception
{
    /**
     * Any extra data to send with the response.
     *
     * @var array
     */
//    public $data = [];

    /**
     * Create a new exception instance.
     *
     * @param string $message
     */
    /*public function __construct($message = null)
    {
        parent::__construct($message);
    }*/


    /**
     * @param $request
     * @return \Illuminate\Http\RedirectResponse|mixed
     */
    /*public function render($request)
    {
        if ($request->expectsJson()) {
            return $this->handleAjax();
        }

        return redirect()->back()
            ->withInput()
            ->withErrors($this->getMessage());
    }*/

    /**
     * Handle an ajax response.
     */
    /*private function handleAjax()
    {
        dd($this->data);

        $errors['message'] = trans('swis.tax_service_tj.connection_failed.message');

        return responseResult('INVALID_DATA', '', '',$errors);

        return response()->json([
            'error'   => true,
            'message' => $this->getMessage(),
            'data'    => $this->data
        ], $this->status);
    }*/

    /**
     * Set the extra data to send with the response.
     *
     * @param array $data
     *
     * @return $this
     */
    /*public function withData(array $data)
    {
        $this->data = $data;

        return $this;
    }*/
}
