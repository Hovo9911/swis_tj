<?php


namespace App\Exceptions;

use Exception;

class CantSendSmsException extends Exception
{
    protected $statusCode = 'CANT_SEND_SMS';

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
    }

}
