<?php

namespace App\Providers;

use App\Models\Notifications\Notifications;
use Illuminate\Support\ServiceProvider;

/**
 * Class NotificationsServiceProvider
 * @package App\Providers
 */
class NotificationsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        view()->composer(['dashboard', 'notifications', 'layouts.header'], function ($view) {
            $view->with('unreadNotificationsCount', Notifications::unreadInAppNotificationsCount());
        });
    }
}
