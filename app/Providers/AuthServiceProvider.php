<?php

namespace App\Providers;

use App\Http\Controllers\Auth\Providers\AuthUserProvider;
use App\Models\User\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;

/**
 * Class AuthServiceProvider
 * @package App\Providers
 */
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Auth::provider('users', function ($app, array $config) {

            // Return an instance of Illuminate\Contracts\Auth\UserProvider...
            $authModel = new User();
            if (request()->has('ssn')) {
                $authModel->setType('ssn');
            } else if (request()->has('email')) {

                $authModel->setType('email');
            }

            return new AuthUserProvider($app['hash'], $authModel);
        });
    }
}
