<?php

namespace App\Providers;

use App\Mailer\Mailer;
use Illuminate\Support\ServiceProvider;

/**
 * Class FacadeServiceProvider
 * @package App\Providers
 */
class FacadeServiceProvider extends ServiceProvider
{
    const FACADE_FOLDER_PATH = 'App\Facades';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Mailer
        $this->app->singleton(self::FACADE_FOLDER_PATH . '\Mailer', function ($app) {
            return new Mailer();
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
