<?php

namespace App\Providers;

use App\Http\Controllers\Core\Dictionary\Translator;
use Illuminate\Support\ServiceProvider;

/**
 * Class TranslatorServiceProvider
 * @package App\Providers
 */
class TranslatorServiceProvider extends ServiceProvider
{
    /**
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Http\Controllers\Core\Interfaces\DictionaryContract', 'App\Http\Controllers\Core\Dictionary\DictionaryManager');

        $this->app->singleton('translator', function ($app) {

            $appType = $app['config']['app.type'];

            return new Translator($appType);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['translator'];
    }
}