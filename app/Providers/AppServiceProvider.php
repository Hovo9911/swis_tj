<?php

namespace App\Providers;

use Illuminate\Contracts\Logging\Log;
use Illuminate\Support\ServiceProvider;
use Psr\Log\LoggerInterface;

/**
 * Class AppServiceProvider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        if (php_sapi_name() == 'cli') {
//            return;
//        }

        /*$_SESSION['duplicates'] = [];
        $_SESSION['sql'] = [];

        \DB::listen(function ($query) {

            $key = md5($query->sql.implode('', $query->bindings));
            if(!empty($_SESSION[$key])){
                $_SESSION[$key]['count']++;
                if($_SESSION[$key]['count'] > 3){
                    $_SESSION['duplicates'][$key] = $_SESSION[$key];
                }

                $_SESSION['sql'][] = $_SESSION[$key]['query'][0][0];

            } else {
                $_SESSION[$key] = [
                    'count' => 1,
                    'query' => [
                        [
                            $query->sql,
                            $query->bindings,
                            $query->time
                        ]
                    ]
                ];

                $_SESSION['sql'][] = $query->sql;
            }
        });*/

//        $request = request();
//
//        if ($request->path() === '/') {
//
//            header("HTTP/1.1 301 Moved Permanently");
//            header('Location: /'.env('DEFAULT_LANGUAGE_CODE', 'en'), true, 301);
//            exit();
//        }
//
//        try {
//            $lngCode = $request->segment(1);
//
//            if (strlen($lngCode) === 2) {
//                $lngManager = new ApplicationLanguageManager();
//                $lngManager->setApplicationLanguageByCode($request->segment(1));
//            } else {
//                abort(404);
//            }
//        } catch (\Exception $e){
//            abort(404);
//        };
//
//        if (cLng('code') == 'hy') {
//            setlocale(LC_ALL, 'hy_AM.UTF-8');
//        } else if (cLng('code') == 'ru') {
//            setlocale(LC_ALL, 'ru_RU.UTF-8');
//        } else if (cLng('code') == 'tj') {
//            setlocale(LC_ALL, 'ru_RU.UTF-8');
//        }

        //
//        Carbon::setToStringFormat(config('swis.date_format_front'));

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->alias('bugsnag.multi', Log::class);
        $this->app->alias('bugsnag.multi', LoggerInterface::class);
    }
}
