<?php

namespace App\Providers;

use App\Http\Controllers\Core\Translation\JsTrans;
use App\Models\ReferenceTable\ReferenceTable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

/**
 * Class ViewServiceProvider
 * @package App\Providers
 */
class ViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // saf/folder/doc
        view()->composer(['single-application.edit', 'folder.edit', 'document.edit'], function ($view) {
            $view->with('legalEntityValues', ReferenceTable::legalEntityValues());
            $view->with('authUserIsBroker', Auth::user()->isBroker() ?: 0);
        });

        // Trans add to js
        view()->share('jsTrans', new JsTrans());

        // If module has only view regime from blade add js variable for datatable
        // datatable will remove all actions and will show only view action
        view()->composer(['*.index'], function ($view) {
            $view->with('checkAccessForEdit', true);
        });
    }
}
