<?php

namespace App\Providers;

use App\Models\DataModel\DataModel;
use App\Models\Instructions\Instructions;
use App\Models\Laboratory\Laboratory;
use App\Models\RiskProfile\RiskProfile;
use App\Models\UserPasswords\UserPasswordsManager;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

/**
 * Class ValidatorServiceProvider
 * @package App\Providers
 */
class ValidatorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $maxStringLength = 250;
        $minIntegerLength = -2000000000;
        $maxIntegerLength = 2000000000;

        // Max String
        Validator::extend('string_with_max', function ($attribute, $value, $parameters) use ($maxStringLength) {
            $data = [];

            if (str_contains($attribute, '.')) {
                $data[explode('.', $attribute)[0]][explode('.', $attribute)[1]] = $value;
            } else {
                $data[$attribute] = $value;
            }

            $rules = [$attribute => "string|max:" . $maxStringLength];

            return $this->validator($data, $rules);
        }, trans('core.base.field.max_characters', ['max' => $maxStringLength]));

        // Max Integer
        Validator::extend('integer_with_max', function ($attribute, $value, $parameters) use ($minIntegerLength, $maxIntegerLength) {
            $data = [];

            if (str_contains($attribute, '.')) {
                $data = $this->dataArray(explode('.', $attribute), $value);
            } else {
                $data[$attribute] = $value;
            }

            $rules = [$attribute => "integer|between:$minIntegerLength,$maxIntegerLength"];

            return $this->validator($data, $rules);
        }, trans('core.loading.invalid_data'));

        // Phone Number
        Validator::extend('phone_number_validator', function ($attribute, $value, $parameters) {
            $data = [];

            if (str_contains($attribute, '.')) {
                $data = $this->dataArray(explode('.', $attribute), $value);
            } else {
                $data[$attribute] = $value;
            }

            $rules = [$attribute => "between:7,15|regex:/^([0-9\s\(\)]*)$/"];

            return $this->validator($data, $rules);

        }, trans('core.loading.invalid_data'));

        // Tax ID
        $taxIdConfigs = config('global.tax_id');

        Validator::extend('tax_id_validator', function ($attribute, $value, $parameters) use ($taxIdConfigs) {
            $data = [];

            if (str_contains($attribute, '.')) {
                $data = $this->dataArray(explode('.', $attribute), $value);
            } else {
                $data[$attribute] = $value;
            }

            $rules = [$attribute => "digits_between:" . $taxIdConfigs['min'] . "," . $taxIdConfigs['max'] . "|string"];

            return $this->validator($data, $rules);

        }, trans('core.loading.invalid_data'));

        // SSN
        $ssnConfigs = config('global.ssn');

        Validator::extend('ssn_validator', function ($attribute, $value, $parameters) use ($ssnConfigs) {
            $data = [];

            if (str_contains($attribute, '.')) {
                $data = $this->dataArray(explode('.', $attribute), $value);
            } else {
                $data[$attribute] = $value;
            }

            $rules = [$attribute => "digits_between:" . $ssnConfigs['min'] . "," . $ssnConfigs['max'] . "|string"];

            return $this->validator($data, $rules);

        }, trans('core.loading.invalid_data'));

        // Passport
        $passportConfigs = config('global.passport');

        Validator::extend('passport_validator', function ($attribute, $value, $parameters) use ($passportConfigs) {
            $data[$attribute] = replaceToLatinCharacters($value);

            $rules = [$attribute => "min:" . $passportConfigs['min'] . "|max:" . $passportConfigs['max'] . "|string|regex:/^[a-zA-Z0-9]*$/"];

            return $this->validator($data, $rules);

        }, trans('core.loading.invalid_data'));

        // Only Latin words
        Validator::extend('only_latin', function ($attribute, $value, $parameters) use ($ssnConfigs) {
            $data[$attribute] = $value;

            $rules = [$attribute => "regex:/^[a-zA-Z0-9\_]*$/"];

            return $this->validator($data, $rules);

        }, trans('core.base.field.only_latin_characters'));

        // Default message core invalid
        Validator::extend('default', function ($attribute, $value, $parameters) {
            return true;

        }, trans('core.loading.invalid_data'));

        // Default message core invalid
        Validator::extend('invalid_data', function ($attribute, $value, $parameters) {
            return false;

        }, trans('core.loading.invalid_data'));

        //Sort Order
        Validator::extend('sort_order', function ($attribute, $value, $parameters) {
            return false;

        }, trans('core.loading.sort_order_invalid_data'));

        //Unique List
        Validator::extend('unique_list', function ($attribute, $value, $parameters) {
            return false;

        }, trans('core.base.field.unique'));

        //Ref not string add ML
        Validator::extend('field_type_cant_be_ml', function ($attribute, $value, $parameters) {
            return false;

        }, trans('swis.reference.field.cant_be_ml.message'));

        //
        Validator::extend('producer_country_with_origin', function ($attribute, $value, $parameters) {
            return false;

        }, trans('swis.saf.product.producer_country_with_origin.not_valid'));

        // Default message for alpha fields
        Validator::extend('mdm_alpha', function ($attribute, $value, $parameters) {

            $data[$attribute] = $value;

            $rules = [$attribute => "string"];

            return $this->validator($data, $rules);

        });

        // Default message for alpha fields
        Validator::extend('mdm_alphanumeric', function ($attribute, $value, $parameters) {

            $data[$attribute] = $value;

            $rules = [$attribute => "string"];

            return $this->validator($data, $rules);

        });

        // Default message for numeric fields
        Validator::extend('mdm_numeric', function ($attribute, $value, $parameters) {

            $data[$attribute] = $value;

            $rules = [$attribute => "integer"];

            return $this->validator($data, $rules);

        });

        // Array with numeric values
        Validator::extend('numeric_array', function ($attribute, $value, $parameters) {
            foreach ($value as $v) {
                if (!is_int($v)) return false;
            }
            return true;
        });

        // Array with numeric values
        Validator::extend('unique_with_agency', function ($attribute, $value, $parameters) {
            list ($table, $field, $id) = $parameters;
            $result = DB::table($table)->where($field, $value)->where('company_tax_id', Auth::user()->companyTaxId());

            if (!empty($id)) {
                if ($table == 'instructions') {
                    $instruction = Instructions::find($id);
                    $whereNotIn = Instructions::where('code', $instruction->code)->pluck('id')->all();
                    $result->whereNotIn('id', $whereNotIn);
                } elseif ($table == 'risk_profile') {
                    $riskProfile = RiskProfile::find($id);
                    $whereNotIn = RiskProfile::where('name', $riskProfile->name)->pluck('id')->all();
                    $result->whereNotIn('id', $whereNotIn);
                }
            }
            return !$result->exists();
        });

        // Regex validator
        Validator::extend('regex_validator', function ($attribute, $value, $parameters) {
            if (@preg_match($value, null) === false) {
                return false;
            }

            return true;
        }, trans('swis.regex_not_valid.message'));

        Validator::extend('unique_with_agency_return', function ($attribute, $value, $parameters) {
            return false;
        });

        // insensitive unique check
        Validator::extend('insensitive_unique', function ($attribute, $value, $parameters) {

            list ($table, $field) = $parameters;
            $result = DB::table($table)->where(DB::raw("lower({$field})"), strtolower($value));
            if (isset($parameters[2])) {
                $result->where('id', '!=', $parameters[2]);
            }
            return !$result->exists();
        });

        // insensitive unique check
        Validator::extend('unique_lab_code', function ($attribute, $value, $parameters) {
            list($lab, $agency) = $parameters;

            return !Laboratory::where('laboratory_code', $value)->where('tax_id', $lab)->where('company_tax_id', $agency)->exists();
        });

        Validator::extend('validate_batches_netto_weight', function ($attribute, $value, $parameters) {
            return ($parameters[0] == $parameters[1]);
        });

        Validator::extend('validate_netto_weight', function ($attribute, $value, $parameters) {
            return ($parameters[1] <= $parameters[0]);
        });

        Validator::extend('validate_batches_quantity', function ($attribute, $value, $parameters) {
            return ($parameters[0] == $parameters[1]);
        });

        Validator::extend('validate_batches_expiration_date', function ($attribute, $value, $parameters) {
            if (isset($parameters[0])) {
                return ($parameters[0] < $parameters[1]);
            }

            return false;
        });

        // Password validator
        Validator::extend('validate_password', function ($attribute, $value, $parameters) {

            $data[$attribute] = $value;
            $passwordMinCharacters = config('global.password_characters.min');
            $passwordMaxCharacters = config('global.password_characters.max');
            $rules = [$attribute => "regex:/^.*(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X]).{" . $passwordMinCharacters . "," . ($passwordMaxCharacters - 1) . "}$/"];

            return $this->validator($data, $rules);

        }, trans('swis.profile_settings.password_regex_incorrect', ['min' => config('global.password_characters.min'), 'max' => config('global.password_characters.max')]));


        // Old password validator
        Validator::extend('validate_old_password', function ($attribute, $value, $parameters) {

            if (!Hash::check($value, Auth::user()->getAuthPassword()) && !empty($value)) {
                return false;
            }
            return true;
        });

        // Set different passwords
        Validator::extend('set_different_passwords', function ($attribute, $value, $parameters) {

            if (UserPasswordsManager::checkUserNewPassIsDifferentFromOlds($value)) {
                return true;
            }
            return false;
        });

        Validator::extend('without_spaces', function($attr, $value){
            return preg_match('/^\S*$/u', $value);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * @param $data
     * @param $rules
     * @return bool
     */
    private function validator($data, $rules)
    {
        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return false;
        }

        return true;
    }

    /**
     * @param $format
     * @return false|int
     */
    private function returnMatch($format)
    {
        /*
         * 1 - type
         * 2 - between
         * 3 - length
         * 5 - floating point
         */
        preg_match('/^([a,n]{1,2})([\.]{0,3})([\d]+)([\,]([\d]+))?$/', $format, $match);

        return $match;
    }

    /**
     * @param $array
     * @param $value
     * @return array
     */
    private function dataArray($array, $value)
    {
        $resultArray = $value;
        for ($i = count($array) - 1; $i >= 0; $i--) {
            $resultArray = array($array[$i] => $resultArray);
        }

        return $resultArray;
    }

}
