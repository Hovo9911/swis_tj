<?php

namespace App\Migration;

use App\Models\BaseModel;
use Illuminate\Database\Schema\Blueprint as IlluminateBlueprint;
use Illuminate\Support\Facades\DB;

class Blueprint extends IlluminateBlueprint
{
    public function showStatus()
    {
        $this->enum('show_status', [
            BaseModel::STATUS_ACTIVE,
            BaseModel::STATUS_INACTIVE,
            BaseModel::STATUS_DELETED,
        ])->default(BaseModel::STATUS_ACTIVE);
    }

    public function adminInfo()
    {
        $this->integer('created_admin_id')->unsigned()->default(0);
        $this->string('created_admin_ip')->default('');
        $this->integer('updated_admin_id')->unsigned()->default(0);
        $this->string('updated_admin_ip')->default('');
        $this->integer('deleted_admin_id')->unsigned()->default(0);
        $this->string('deleted_admin_ip')->default('');
    }

    public function sortOrder()
    {
        $this->integer('sort_order')->unsigned()->default(0);
    }

    public function getSchemaBuilder()
    {
        $schema = DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function ($table, $callback) {
            return new Blueprint($table, $callback);
        });

        return $schema;
    }

    public function getDB()
    {
        return DB::connection();
    }
}