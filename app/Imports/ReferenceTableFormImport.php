<?php

namespace App\Imports;

use App\Models\ReferenceTable\ReferenceTable;
use App\Models\ReferenceTable\ReferenceTableStructure;
use App\Models\ReferenceTableForm\ReferenceTableFormManager;
use App\Rules\ValidateDecimalRule;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

/**
 * Class ReferenceTableFormImport
 * @package App\Imports
 */
class ReferenceTableFormImport implements ToModel, WithHeadingRow, WithValidation, WithBatchInserts
{
    /**
     * Function to import reference form data
     *
     * @param array $row
     */
    public function model(array $row)
    {
        $referenceTableStructure = ReferenceTableStructure::where('reference_table_id', request()->input('id'))->get();

        $referenceData = [];
        $languagesIdCode = activeLanguagesIdCode();
        $referenceTableFormManager = new ReferenceTableFormManager();

        foreach ($referenceTableStructure as $structure) {

            if (array_key_exists($structure->field_name, $row)) {
                $referenceData[$structure->field_name] = $row[$structure->field_name];

                continue;
            }

            if ($structure->has_ml) {
                foreach ($languagesIdCode as $lngId => $lngCode) {
                    if (array_key_exists($structure->field_name . '_' . $lngCode, $row)) {
                        $referenceData['ml'][$lngId][$structure->field_name] = $row[$structure->field_name . '_' . $lngCode];
                    }
                }
            }
        }

        if (!isset($referenceData['start_date']) || !$referenceData['start_date']) {
            $referenceData['start_date'] = currentDate();
        }

        $referenceData['show_status'] = ReferenceTable::STATUS_ACTIVE;
        $referenceData['is_imported'] = true;

        $referenceTableFormManager->store($referenceData, request()->input('id'));
    }

    public function rules(): array
    {
        $languagesIdCode = activeLanguagesIdCode();
        $referenceTableStructure = ReferenceTableStructure::where('reference_table_id', request()->input('id'))->get();

        $rules = [];
        foreach ($referenceTableStructure as $structure) {
            $parameters = $structure->parameters;
            $fieldName = $structure->field_name;

            $isRequired = 'nullable';
            if ($structure->required) {
                $isRequired = 'required';
            }

            if (!$structure->has_ml) {

                switch ($structure->type) {

                    // Text
                    case ReferenceTableStructure::COLUMN_TYPE_TEXT:
                        $rules[$fieldName] = $isRequired . "|min:{$parameters['min']}|max:{$parameters['max']}";

                        break;

                    // Number
                    case ReferenceTableStructure::COLUMN_TYPE_NUMBER:
                        $rules[$fieldName] = $isRequired . "|integer|min:{$parameters['min']}|max:{$parameters['max']}";

                        break;

                    // Decimal
                    case ReferenceTableStructure::COLUMN_TYPE_DECIMAL:
                        $rules[$fieldName] = [$isRequired, 'numeric', new ValidateDecimalRule($parameters['left'], $parameters['right'])];

                        break;

                    // Select
                    case ReferenceTableStructure::COLUMN_TYPE_SELECT:
                    case ReferenceTableStructure::COLUMN_TYPE_AUTOCOMPLETE:

                        $sourceReferenceTable = ReferenceTable::where('id', $structure->parameters['source'])->pluck('table_name')->first();

                        $rules[$fieldName] = $isRequired . "|integer_with_max|exists:reference_{$sourceReferenceTable},id";
                        break;

                    // Date, Datetime
                    case ReferenceTableStructure::COLUMN_TYPE_DATETIME:
                    case ReferenceTableStructure::COLUMN_TYPE_DATE:
                        $rules[$fieldName] = $isRequired . "|date";

                        if ($fieldName == ReferenceTable::REFERENCE_DEFAULT_FIELD_START_DATE || $fieldName == ReferenceTable::REFERENCE_DEFAULT_FIELD_END_DATE) {
                            $rules[$fieldName] .= '|after_or_equal:' . currentDate();
                        }

                        break;
                }

            } else {

                // Text
                if ($structure->type == ReferenceTableStructure::COLUMN_TYPE_TEXT) {
                    foreach ($languagesIdCode as $lngId => $lngCode) {
                        $rules[$fieldName . '_' . $lngCode] = $isRequired . "|string|min:{$parameters['min']}|max:{$parameters['max']}";
                    }
                }

            }
        }

        $rules['code'] .= '|without_spaces';

        return $rules;
    }

    public function customValidationMessages()
    {
        $languagesIdCode = activeLanguagesIdCode();
        $referenceTableStructure = ReferenceTableStructure::where('reference_table_id', request()->input('id'))->get();

        $messages = [];
        foreach ($referenceTableStructure as $structure) {

            $fieldName = $structure->field_name;

            if (!$structure->has_ml) {

                $messages [$fieldName . '.required'] = trans('core.base.field.required');

                switch ($structure->type) {

                    // Text
                    case ReferenceTableStructure::COLUMN_TYPE_TEXT:

                        $messages[$fieldName . '.min'] = trans("core.base.field.min_characters", ['min' => $structure->parameters["min"]]);
                        $messages[$fieldName . '.max'] = trans("core.base.field.max_characters", ['max' => $structure->parameters["max"]]);

                        break;

                    case ReferenceTableStructure::COLUMN_TYPE_NUMBER:

                        $messages[$fieldName . '.min'] = trans("core.base.field.min_number", ['min' => $structure->parameters["min"]]);
                        $messages[$fieldName . '.max'] = trans("core.base.field.max_number", ['max' => $structure->parameters["max"]]);

                        break;

                    // Date, Datetime
                    case ReferenceTableStructure::COLUMN_TYPE_DATETIME:
                    case ReferenceTableStructure::COLUMN_TYPE_DATE:

                        if ($fieldName == ReferenceTable::REFERENCE_DEFAULT_FIELD_START_DATE || $fieldName == ReferenceTable::REFERENCE_DEFAULT_FIELD_END_DATE) {
                            $messages[$fieldName . '.after_or_equal'] = trans('swis.reference_table_form.start_date.invalid', ['start' => currentDateFront()]);
                        }

                        break;

                }

                $messages[$fieldName . '*'] = trans('core.loading.invalid_data');

            } else {

                // Text
                if ($structure->type == ReferenceTableStructure::COLUMN_TYPE_TEXT) {
                    foreach ($languagesIdCode as $lngId => $lngCode) {
                        $messages[$fieldName . '_' . $lngCode . '.required'] = trans('core.base.field.required');
                        $messages[$fieldName . '_' . $lngCode . '.min'] = trans("core.base.field.min_characters", ['min' => $structure->parameters["min"]]);
                        $messages[$fieldName . '_' . $lngCode . '.max'] = trans("core.base.field.max_characters", ['max' => $structure->parameters["max"]]);
                        $messages[$fieldName . '_' . $lngCode . '.*'] = trans('core.loading.invalid_data');
                    }
                }
            }
        }

        return $messages;
    }

    public function batchSize(): int
    {
        return 2000;
    }

    /*  public function chunkSize(): int
      {
          return 10000;
      }*/

}