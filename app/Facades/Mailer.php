<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class MailerFacade
 * @package App\Mailer
 */
class Mailer extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'App\Facades\Mailer';
    }
}