<?php

namespace App\Http\Controllers;

use App\Http\Requests\Api\GetDocumentArchiveInfoRequest;
use App\Http\Requests\Api\GetSubApplicationInfoRequest;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\Document\Document;
use App\Models\Folder\Folder;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use DateTime;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

/**
 * Class ApiController
 * @package App\Http\Controllers
 */
class ApiController extends BaseController
{
    /**
     * Function to get Sub application data
     *
     * @param GetSubApplicationInfoRequest $request
     * @return JsonResponse
     */
    public function getSubApplicationInfo(GetSubApplicationInfoRequest $request)
    {
        $taxIds = $request->input('tax_ids');
        $refClassificator = ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS;
        $refClassificatorMl = $refClassificator . "_ml";

        $subApplication = SingleApplicationSubApplications::select([
            'saf_sub_applications.id',
            'saf_sub_applications.document_number',
            "{$refClassificator}.code",
            "{$refClassificatorMl}.name",
            "{$refClassificatorMl}.long_name",
            "saf_sub_applications.access_token",
            DB::raw("COALESCE(saf_sub_applications.rejection_date, saf_sub_applications.permission_date) as provision_date"),
        ])
            ->join('saf', "saf.regular_number", "=", "saf_sub_applications.saf_number")
            ->join($refClassificator, "{$refClassificator}.id", "=", "saf_sub_applications.reference_classificator_id")
            ->join($refClassificatorMl, function ($query) use ($refClassificator, $refClassificatorMl) {
                $query->on("{$refClassificator}.id", "=", "{$refClassificatorMl}.{$refClassificator}_id")->where("{$refClassificatorMl}.lng_id", cLng('id'));
            })
            ->where('document_number', $request->input('number'))
            ->where("{$refClassificator}.code", $request->input('type'))
            ->where(function ($q) use ($taxIds) {
                $q->when(isset($taxIds['importer']), function ($query) use ($taxIds) {
                    $query->orWhere('importer_value', $taxIds['importer']);
                    $query->orWhere("saf_sub_applications.data->saf->sides->import->>passport", '>=', $taxIds['importer']);
                })->when(isset($taxIds['exporter']), function ($query) use ($taxIds) {
                    $query->orWhere('exporter_value', $taxIds['exporter']);
                    $query->orWhere("saf_sub_applications.data->saf->sides->export->>passport", '>=', $taxIds['exporter']);
                })->when(isset($taxIds['applicant']), function ($query) use ($taxIds) {
                    $query->orWhere('applicant_value', $taxIds['applicant']);
                });
            })
            ->first();

        if (is_null($subApplication)) {
            return response()->json([
                'status' => 'NOT_FOUND',
                'description' => trans('swis.api.error.sub_application_not_fount'), // 'Subapplication not found'
            ]);
        }
        $currentState = $subApplication->applicationCurrentState;

        if (!is_null($currentState)) {
            if (!is_null($currentState->state) && ($currentState->state->state_type != ConstructorStates::END_STATE_TYPE && $currentState->state->state_type != ConstructorStates::CANCELED_STATE_TYPE)) {
                return response()->json([
                    'status' => "INVALID_DOCUMENT_STATUS",
                    'description' => trans('swis.api.error.not_final_status') //'The subapplication exists but not in the final status',
                ]);
            }
        }

        if ($currentState->state->state_type == ConstructorStates::CANCELED_STATE_TYPE) {
            $currentState = 'canceled';
        } else {
            $currentState = ($currentState->state->state_approved_status) ? 'confirmed' : 'rejected';
        }

        $productNumber = '';
        $products = $subApplication->productList()->pluck('id')->all();
        foreach ($products as $key => $item) {
            $productNumber .= ++$key . ',';
        }
        return response()->json([
            'status' => 'OK',
            'data' => [
                "number" => $subApplication->document_number,
                "type" => $subApplication->code,
                "name" => $subApplication->name,
                "description" => $subApplication->long_name,
                "status" => $currentState,
                "provision_date" => $subApplication->provision_date,
                "product_number" => rtrim($productNumber, ","),
                "access_token" => $subApplication->access_token
            ],
            'url' => '/en/subapplication/info/?access_token=' . $subApplication->access_token . '&id=' . $subApplication->document_number
        ]);
    }

    /**
     * @param GetDocumentArchiveInfoRequest $request
     * @return JsonResponse
     */
    public function getDocumentArchiveInfo(GetDocumentArchiveInfoRequest $request)
    {
        $validatedData = $request->validated();

        if (isset($validatedData['access_token'])) {
            $documents = $this->getDocumentArchiveByAccessToken($validatedData['access_token']);
        } else {
            $documents = $this->getDocumentArchive($validatedData);
        }

        if (empty($documents)) {
            return response()->json([
                'status' => 'NOT_FOUND',
                'description' => trans('swis.api.error.document_not_found'), // 'Document not found'
            ]);
        }

        return response()->json([
            'status' => 'OK',
            'data' => ['documents' => $documents]
        ]);
    }

    /**
     * Function to get document archive  by access token
     *
     * @param $accessToken
     * @return array
     */
    private function getDocumentArchiveByAccessToken($accessToken)
    {
        $data = [];
        $folder = Folder::where('folder_access_password', $accessToken)->with(['documents:document_access_password,document_type,document_number,document_name,document_description,document_release_date,document_status'])->first();
        if (!is_null($folder)) {
            foreach ($folder->documents as $document) {
                $data[] = $this->generateDocumentData($document);
            }
            return $data;
        }

        $document = Document::select(['document_access_password', 'document_type', 'document_number', 'document_name', 'document_description', 'document_release_date', 'document_status'])->where('document_access_password', $accessToken)->first();
        if (!is_null($document)) {
            return [$this->generateDocumentData($document)];
        }

        return $data;
    }

    /**
     * Function to search documents
     *
     * @param $data
     * @return array
     */
    private function getDocumentArchive($data)
    {
        $taxIds = $data['tax_ids'];
        $documents = Document::select(['document_access_password', 'document_type', 'document_number', 'document_name', 'document_description', 'document_release_date', 'document_status'])
            ->when(isset($data['type']), function ($query) use ($data) {
                $query->where('document_type', $data['type']);
            })
            ->when(isset($data['number']), function ($query) use ($data) {
                $query->where('document_number', $data['number']);
            })
            ->when(isset($data['name']), function ($query) use ($data) {
                $query->where('document_name', 'ILIKE', "%{$data['name']}%");
            })
            ->when(isset($data['description']), function ($query) use ($data) {
                $query->where('document_description', 'ILIKE', "%{$data['description']}%");
            })
            ->when(isset($data['provision_date']), function ($query) use ($data) {
                $query->where('document_release_date', $data['provision_date']);
            })
            ->where(function ($q) use ($taxIds) {
                $q->when(isset($taxIds['creator']), function ($query) use ($taxIds) {
                    $query->orWhere('creator_type_value', $taxIds['creator']);
                })->when(isset($taxIds['holder']), function ($query) use ($taxIds) {
                    $query->orWhere('holder_type_value', $taxIds['holder']);
                    $query->orWhere('holder_passport', $taxIds['holder']);
                });
            })
            ->get();

        $result = [];
        foreach ($documents as $document) {
            $result[] = $this->generateDocumentData($document);
        }

        return $result;
    }

    /**
     * Function to remove duplicate array generate
     *
     * @param $document
     * @return array
     */
    private function generateDocumentData($document)
    {
        $datetime = DateTime::createFromFormat(config('swis.date_format_front'), $document->document_release_date);

        return [
            "access_token" => $document->document_access_password,
            "type" => $document->document_type,
            "number" => $document->document_number,
            "name" => $document->document_name,
            "description" => $document->document_description,
            "provision_date" => $datetime->format('Y-m-d'),
            "status" => $document->document_status,
            "uri" => "/en/document_archive/get_file/{$document->document_access_password}"
        ];
    }
}