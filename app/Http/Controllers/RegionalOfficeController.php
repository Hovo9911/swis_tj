<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegionOffice\RegionalOfficeSearchRequest;
use App\Http\Requests\RegionOffice\RegionOfficeRequest;
use App\Models\Agency\Agency;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\Laboratory\Laboratory;
use App\Models\RegionalOffice\RegionalOffice;
use App\Models\RegionalOffice\RegionalOfficeManager;
use App\Models\RegionalOffice\RegionalOfficerSearch;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Throwable;

/**
 * Class RegionalOfficeController
 * @package App\Http\Controllers
 */
class RegionalOfficeController extends BaseController
{
    /**
     * @var RegionalOfficeManager
     */
    protected $manager;

    /**
     * RegionalOfficeController constructor.
     *
     * @param RegionalOfficeManager $manager
     */
    public function __construct(RegionalOfficeManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Function to show index page
     *
     * @return View
     */
    public function index()
    {
        return view('regional-office.index')->with([
            'agencies' => Agency::allData([], false, true),
            'constructorDocuments' => ConstructorDocument::getConstructorDocumentForAgency()
        ]);
    }

    /**
     * Function to show data dataTable
     *
     * @param RegionalOfficeSearchRequest $regionalOfficeSearchRequest
     * @param RegionalOfficerSearch $agencySearch
     * @return JsonResponse
     */
    public function table(RegionalOfficeSearchRequest $regionalOfficeSearchRequest, RegionalOfficerSearch $agencySearch)
    {
        $data = $this->dataTableAll($regionalOfficeSearchRequest, $agencySearch);

        return response()->json($data);
    }

    /**
     * Function to show create view
     *
     * @return View
     */
    public function create()
    {
        $laboratories = [];
        if (!Auth::user()->isSwAdmin()) {
            $laboratories = Laboratory::select(['id', 'tax_id', 'name'])->where('company_tax_id', Auth::user()->companyTaxId())->joinMl()->active()->get();
        }

        return view('regional-office.edit')->with([
            'saveMode' => 'add',
            'agencies' => Agency::allData([], false, true),
            'laboratories' => $laboratories,
            'constructorDocuments' => ConstructorDocument::getConstructorDocumentForAgency(),
        ]);
    }

    /**
     * Function to store validated data
     *
     * @param RegionOfficeRequest $request
     * @return JsonResponse
     */
    public function store(RegionOfficeRequest $request)
    {
        $regionalOffice = $this->manager->store($request->validated());

        return responseResult('OK', null, ['id' => $regionalOffice->id]);
    }

    /**
     * Function to show current regional office by id
     *
     * @param $lngCode
     * @param $id
     * @param $viewType
     * @return View
     */
    public function edit($lngCode, $id, $viewType)
    {
        $regionalOffice = RegionalOffice::where('id', $id)->checkUserHasRole()->firstOrFail();
        $laboratories = Laboratory::select(['id', 'tax_id', 'name'])->where('company_tax_id', $regionalOffice->company_tax_id)->joinMl()->active()->get();

        return view('regional-office.edit')->with([
            'regionalOffice' => $regionalOffice,
            'agencies' => Agency::allData([], false, true),
            'laboratories' => $laboratories,
            'constructorDocuments' => ConstructorDocument::getConstructorDocumentForAgency($regionalOffice->company_tax_id),
            'saveMode' => $viewType
        ]);
    }

    /**
     * Function to update data
     *
     * @param RegionOfficeRequest $request
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     */
    public function update(RegionOfficeRequest $request, $lngCode, $id)
    {
        $this->manager->update($request->validated(), $id);

        return responseResult('OK');
    }

    /**
     * Function to get agencies by documents
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function constructorDocumentsOfAgency(Request $request)
    {
        $this->validate($request, [
            'company_tax_id' => 'required|string_with_max|exists:agency,tax_id',
            'regional_office_id' => 'nullable|integer_with_max'
        ]);

        $regionalOffice = RegionalOffice::where('id', $request->input('regional_office_id'))->checkUserHasRole()->first();
        $constructorDocuments = ConstructorDocument::getConstructorDocumentForAgency($request->input('company_tax_id'));

        $values = [];
        if (Auth::user()->isSwAdmin() && $constructorDocuments->count()) {
            foreach ($constructorDocuments as $item) {
                $values['items'][] = [
                    'code' => $item->document_code,
                    'name' => $item->document_name
                ];
            }
        }

        $laboratories = Laboratory::select(['id', 'tax_id', 'name'])->where('company_tax_id', $request->input('company_tax_id'))->joinMl()->active()->get();

        $values['laboratories'] = view('regional-office.laboratories-table')->with([
            'laboratories' => $laboratories,
            'regionalOffice' => $regionalOffice
        ])->render();

        return responseResult('OK', '', $values);
    }

}
