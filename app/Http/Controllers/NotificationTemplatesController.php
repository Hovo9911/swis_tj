<?php

namespace App\Http\Controllers;

use App\Http\Requests\NotificationTemplates\NotificationTemplatesRequest;
use App\Http\Requests\NotificationTemplates\NotificationTemplatesSearchRequest;
use App\Models\Agency\Agency;
use App\Models\NotificationTemplates\NotificationTemplates;
use App\Models\NotificationTemplates\NotificationTemplatesManager;
use App\Models\NotificationTemplates\NotificationTemplatesSearch;
use App\Models\User\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class NotificationTemplatesController
 * @package App\Http\Controllers
 */
class NotificationTemplatesController extends BaseController
{
    /**
     * @var NotificationTemplatesManager
     */
    protected $manager;

    /**
     * NotificationTemplatesController constructor.
     *
     * @param NotificationTemplatesManager $manager
     */
    public function __construct(NotificationTemplatesManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @return View
     */
    public function index()
    {
        $notifications = NotificationTemplates::notificationTemplateOwner()->active()->get();

        return view('global-notifications.index')->with([
            'notifications' => $notifications,
            'agencies' => Agency::allData()
        ]);
    }

    /**
     * Function to showing all data in datatable
     *
     * @param NotificationTemplatesSearchRequest $notificationTemplatesSearchRequest
     * @param NotificationTemplatesSearch $notificationSearch
     * @return JsonResponse
     */
    public function table(NotificationTemplatesSearchRequest $notificationTemplatesSearchRequest, NotificationTemplatesSearch $notificationSearch)
    {
        $data = $this->dataTableAll($notificationTemplatesSearchRequest, $notificationSearch);

        return response()->json($data);
    }

    /**
     * Function to show index page
     *
     * @return View
     */
    public function create()
    {
        return view('global-notifications.edit')->with([
            'saveMode' => 'add',
        ]);
    }

    /**
     * Function to store data
     *
     * @param NotificationTemplatesRequest $request
     * @return JsonResponse
     */
    public function store(NotificationTemplatesRequest $request)
    {
        $this->manager->store($request->validated());

        return responseResult('OK');
    }

    /**
     * Function to show current by id
     *
     * @param $lngCode
     * @param $id
     * @param $viewType
     * @return View
     */
    public function edit($lngCode, $id, $viewType)
    {
        $notifications = NotificationTemplates::where('id', $id)->notificationTemplateOwner()->firstOrFail();

        return view('global-notifications.edit')->with([
            'notifications' => $notifications,
            'saveMode' => $viewType
        ]);
    }

    /**
     * Function to update data
     *
     * @param NotificationTemplatesRequest $request
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     */
    public function update(NotificationTemplatesRequest $request, $lngCode, $id)
    {
        $this->manager->update($request->validated(), $id);

        return responseResult('OK');
    }

    /**
     * Function to clone Template
     *
     * @param $lngCode
     * @param $id
     * @return View
     */
    public function cloneTemplate($lngCode, $id)
    {
        $notifications = NotificationTemplates::where('id', $id)->notificationTemplateOwner()->firstOrFail();

        return view('global-notifications.edit')->with([
            'notifications' => $notifications,
            'saveMode' => 'add',
            'clone' => true
        ]);
    }

    /**
     * Function to Delete by id(array)
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function delete(Request $request)
    {
        $this->manager->delete($request->input('deleteItems'));

        return responseResult('OK');
    }

    /**
     * Function to send global notification
     *
     * @param Request $request
     * @param $lngCode
     * @return JsonResponse
     */
    public function sendGlobalNotification(Request $request, $lngCode)
    {
        $notificationTemplateId = $request->input('notificationTemplateId') ?? null;

        $userType = $request->input('userType');
        $agencies = $request->input('agencies') ?? [];

        //
        $usersGroup = User::getUsersGroupForNotifications($userType, $agencies);

        $this->manager->sendGlobalNotifications($usersGroup, $userType, $notificationTemplateId);

        $this->manager->globalNotificationSentAt($notificationTemplateId);

        return responseResult('OK');
    }

}
