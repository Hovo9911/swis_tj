<?php

namespace App\Http\Controllers;

use App\Http\Requests\PaymentProviders\PaymentProviderSearchRequest;
use App\Http\Requests\PaymentProviders\PaymentProvidersRequest;
use App\Models\PaymentProviders\PaymentProviders;
use App\Models\PaymentProviders\PaymentProvidersManager;
use App\Models\PaymentProviders\PaymentProvidersSearch;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class PaymentProvidersController
 * @package App\Http\Controllers
 */
class PaymentProvidersController extends BaseController
{
    /**
     * @var PaymentProvidersManager
     */
    protected $manager;

    /**
     * PaymentProvidersController constructor.
     *
     * @param PaymentProvidersManager $manager
     */
    public function __construct(PaymentProvidersManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Function to return index View
     *
     * @return View
     */
    public function index()
    {
        return view('payment-providers.index');
    }

    /**
     * Function to return create View
     *
     * @return View
     */
    public function create()
    {
        return view('payment-providers.edit')->with([
            'saveMode' => 'add'
        ]);
    }

    /**
     * Function to get table data
     *
     * @param PaymentProviderSearchRequest $paymentProviderSearchRequest
     * @param PaymentProvidersSearch $PaymentProvidersSearch
     * @return JsonResponse
     */
    public function table(PaymentProviderSearchRequest $paymentProviderSearchRequest, PaymentProvidersSearch $PaymentProvidersSearch)
    {
        $data = $this->dataTableAll($paymentProviderSearchRequest, $PaymentProvidersSearch);

        return response()->json($data);
    }

    /**
     * Function to store data to constructor_templates
     *
     * @param PaymentProvidersRequest $request
     * @return JsonResponse
     */
    public function store(PaymentProvidersRequest $request)
    {
        $paymentProviders = $this->manager->store($request->validated());

        return responseResult('OK', null, ['id' => $paymentProviders->id]);
    }

    /**
     * Function to return edit view
     *
     * @param $lngCode
     * @param $id
     * @param $viewType
     * @return View
     */
    public function edit($lngCode, $id, $viewType)
    {
        $paymentProviders = PaymentProviders::where('id', $id)->firstOrFail();

        return view('payment-providers.edit')->with([
            'saveMode' => $viewType,
            'paymentProviders' => $paymentProviders
        ]);
    }

    /**
     * Function to store update
     *
     * @param PaymentProvidersRequest $request
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     */
    public function update(PaymentProvidersRequest $request, $lngCode, $id)
    {
        $this->manager->update($request->validated(), $id);

        return responseResult('OK');
    }

    /**
     * Function to Delete by id(array)
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function delete(Request $request)
    {
        $this->manager->delete($request->input('deleteItems'));

        return responseResult('OK');
    }
}
