<?php

namespace App\Http\Controllers\Auth;

use App\Facades\Mailer;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\EmailVerifyRequest;
use App\Http\Requests\Auth\PhoneActivationCodeRequest;
use App\Http\Requests\Auth\PhoneVerifyRequest;
use App\Models\User\User;
use App\Models\User\UserVerify;
use App\Sms\SmsManager;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

/**
 * Class VerifyController
 * @package App\Http\Controllers\Auth
 */
class VerifyController extends Controller
{
    /**
     * Function to show email verify viw
     *
     * @return RedirectResponse|View
     */
    public function emailVerify()
    {
        if (!config('global.auth_verify.email.mode') || Auth::user()->is_email_verified) {
            return redirect(urlWithLng('dashboard'));
        }

        $userActivationTokens = UserVerify::where(['type' => UserVerify::VERIFY_TYPE_EMAIL, 'user_id' => Auth::id()])->whereDate('created_at', Carbon::today())->orderByDesc('created_at')->first();

        return view('auth.email-verify', compact('userActivationTokens'));
    }

    /**
     * Function to Send Activation token to email of user
     *
     * @param EmailVerifyRequest $request
     * @return RedirectResponse|JsonResponse
     */
    public function sendActivationToken(EmailVerifyRequest $request)
    {
        if (!config('global.auth_verify.email.mode') || Auth::user()->is_email_verified) {
            return null;
        }

        $userVerifyTokens = UserVerify::where(['user_id' => Auth::id(), 'type' => UserVerify::VERIFY_TYPE_EMAIL])->whereDate('created_at', Carbon::today())->count();
        if ($userVerifyTokens >= config('global.auth_verify.email.token_count')) {

            $errors['email'] = trans('swis.verify.mail.token_expired');

            return responseResult('INVALID_DATA', '', '', $errors);
        }

        $token = uniqid() . '' . str_random(12) . '' . time();
        $userName = Auth::user()->name();

        UserVerify::create([
            'user_id' => Auth::id(),
            'type' => UserVerify::VERIFY_TYPE_EMAIL,
            'token' => $token
        ]);

        $data = [
            'subject' => trans('swis.verify.mail.send_activation_code.subject'),
            'to' => $request->email,
            'body' => view('mailer.verification-code', compact('token', 'userName')),
        ];

        //
        Mailer::send($data);

        //
        Auth::user()->update(['email' => $request->email]);

        session()->put('success', trans('swis.verify.check_your_email'));

        return responseResult('OK', urlWithLng('/'));
    }

    /**
     * Function to activate User by token
     *
     * @param $lngCode
     * @param $token
     * @return RedirectResponse
     */
    public function activateEmail($lngCode, $token)
    {
        if (!empty($token)) {

            if (!config('global.auth_verify.email.mode')) {
                return redirect(urlWithLng('login'));
            }

            $userVerifyToken = UserVerify::where(['token' => $token, 'type' => UserVerify::VERIFY_TYPE_EMAIL])->whereDate('created_at', Carbon::today())->orderByDesc('created_at')->first();

            if (!is_null($userVerifyToken)) {

                $user = User::select('id', 'is_email_verified', 'phone_number')->where('id', $userVerifyToken->user_id)->first();

                if ($user->is_email_verified) {
                    if (Auth::guest()) {
                        return redirect(urlWithLng('login'));
                    }

                    return redirect(urlWithLng('dashboard'));
                }

                $user->update(['is_email_verified' => User::TRUE]);

                session()->put('success', trans('swis.verify.email_successfully_verified'));

                $userVerifyToken->delete();

                //
                if (config('global.auth_verify.phone.mode')) {
                    if (!Auth::guest() && empty($user->phone_number)) {
                        return redirect(urlWithLng('phone-number/verify'));
                    }
                }

                if (Auth::guest()) {
                    return redirect(urlWithLng('login'));
                }

                return redirect(urlWithLng('dashboard'));
            }

            if (Auth::guest()) {
                return redirect(urlWithLng('login'))->with('error', trans('swis.verify.token_not_found'));
            }

            return redirect(urlWithLng('email/verify'))->with('error', trans('swis.verify.token_not_found'));
        }

        return redirect(urlWithLng('email/verify'));
    }

    /**
     * Function to resend Token to user email
     *
     * @return RedirectResponse|JsonResponse
     */
    public function sendTokenAgain()
    {
        if (!config('global.auth_verify.email.mode') || Auth::user()->is_email_verified) {
            return redirect(urlWithLng('dashboard'));
        }

        $todaySendMailsCount = DB::table('emails')->whereDate('created_at', Carbon::today())->where(['to' => Auth::user()->email])->count();
        if ($todaySendMailsCount >= config('global.auth_verify.email.send_again_count')) {

            $errors['email'] = trans('swis.verify.email.too_many_token');

            return responseResult('INVALID_DATA', '', '', $errors);
        }

        $userLastActivationToken = UserVerify::select('token')->where(['user_id' => Auth::id(), 'type' => UserVerify::VERIFY_TYPE_EMAIL])->whereDate('created_at', Carbon::today())->orderByDesc('created_at')->first();
        if (!is_null($userLastActivationToken)) {

            $token = $userLastActivationToken->token;
            $userName = Auth::user()->name();

            $data = [
                'subject' => trans('swis.verify.mail.send_activation_code_again.subject'),
                'to' => Auth::user()->email,
                'body' => view('mailer.verification-code', compact('token', 'userName')),
            ];

            //
            Mailer::send($data);

            session()->put('success', trans('swis.verify.check_your_email_token_send_again'));

            return responseResult('OK', urlWithLng('/'));
        }

        $errors['email'] = trans('swis.verify.mail.token_not_exist');

        return responseResult('INVALID_DATA', '', '', $errors);
    }

    /**
     * Function to show phone verify view
     *
     * @return RedirectResponse|View
     */
    public function phoneNumberVerify()
    {
        if (!config('global.auth_verify.phone.mode') || Auth::user()->is_phone_verified) {
            return redirect(urlWithLng('dashboard'));
        }

        $userActivationPhoneNumber = UserVerify::where(['type' => UserVerify::VERIFY_TYPE_PHONE, 'user_id' => Auth::id()])->whereDate('created_at', Carbon::today())->orderByDesc('created_at')->first();

        return view('auth.phone-verify', compact('userActivationPhoneNumber'));
    }

    /**
     * Function to Verify User Phone Number
     *
     * @param PhoneVerifyRequest $request
     * @return RedirectResponse|JsonResponse
     */
    public function sendActivationSMS(PhoneVerifyRequest $request)
    {
        if (!config('global.auth_verify.phone.mode') || Auth::user()->is_phone_verified) {
            return redirect(urlWithLng('dashboard'));
        }

        $todaySendSmsCount = DB::table('sms')->whereDate('created_at', Carbon::today())->where(['user_id' => Auth::id()])->count();
        if ($todaySendSmsCount >= config('global.auth_verify.phone.send_again_count')) {

            $errors['phone_number'] = trans('swis.verify.phone_number.too_many_token');

            return responseResult('INVALID_DATA', '', '', $errors);
        }

        //
        $userVerify = UserVerify::where(['user_id' => Auth::id(), 'type' => UserVerify::VERIFY_TYPE_PHONE])->whereDate('created_at', Carbon::today())->count();
        if ($userVerify >= config('global.auth_verify.phone.sms_count')) {

            $errors['phone_number'] = trans('swis.verify.phone.token_expired');

            return responseResult('INVALID_DATA', '', '', $errors);
        }

        $sms_token = mt_rand(100000, 999999);
        UserVerify::create([
            'user_id' => Auth::id(),
            'type' => UserVerify::VERIFY_TYPE_PHONE,
            'token' => $sms_token
        ]);

        User::find(Auth::id())->update(['phone_number' => $request->phone_number]);

        $data = [
            'user_id' => Auth::id(),
            'phone' => trim($request->phone_number),
            'message' => 'Activation code is: ' . $sms_token,
        ];

        //
        $sms = new SmsManager();
        $sms->storeSms($data);
        $sms->sendSms(Auth::id());

        session()->put('success', 'swis.verify.check_your_phone_activation_code_send');

        return responseResult('OK', urlWithLng('phone-verify'));
    }

    /**
     * Function to Activate User Phone Number by activation code
     *
     * @param PhoneActivationCodeRequest $request
     * @return RedirectResponse|JsonResponse
     */
    public function activatePhoneNumber(PhoneActivationCodeRequest $request)
    {
        if (!config('global.auth_verify.phone.mode') || Auth::user()->is_phone_verified) {
            return redirect(urlWithLng('dashboard'));
        }

        $userVerifyCode = UserVerify::where(['user_id' => Auth::id(), 'type' => UserVerify::VERIFY_TYPE_PHONE])->whereDate('created_at', Carbon::today())->orderBy('created_at', 'desc')->first();
        if (!is_null($userVerifyCode) && ($userVerifyCode->token == $request->activation_code)) {

            Auth::user()->update(['is_phone_verified' => User::TRUE]);
            session()->put('success', trans('swis.verify.phone.successfully_verified'));
            UserVerify::where(['user_id' => Auth::id(), 'type' => UserVerify::VERIFY_TYPE_PHONE])->delete();

            return responseResult('OK', '', '', urlWithLng('dashboard'));
        }

        $errors['activation_code'] = trans('swis.verify.activation_code_not_found');

        return responseResult('INVALID_DATA', '', '', $errors);
    }

    /**
     * Function to Resend Code to user phone
     *
     * @return RedirectResponse|JsonResponse
     */
    public function sendCodeAgain()
    {
        if (!config('global.auth_verify.phone.mode') || Auth::user()->is_phone_verified) {
            return redirect(urlWithLng('dashboard'));
        }

        $userLastActivationToken = UserVerify::where(['user_id' => Auth::id(), 'type' => UserVerify::VERIFY_TYPE_PHONE])->whereDate('created_at', Carbon::today())->orderByDesc('created_at')->first();

        if (!is_null($userLastActivationToken)) {

            $sms_token = mt_rand(100000, 999999);

            UserVerify::create([
                'user_id' => Auth::id(),
                'type' => UserVerify::VERIFY_TYPE_PHONE,
                'token' => $sms_token
            ]);

            $data = [
                'user_id' => Auth::id(),
                'phone' => Auth::user()->phone_number,
                'message' => 'Activation code is: ' . $sms_token,
            ];

            //
            $sms = new SmsManager();
            $sms->storeSms($data);
            $sms->sendSms(Auth::id());

            session()->put('success', trans('swis.verify.check_your_phone_activation_code_send_again'));
        }

        return responseResult('OK', urlWithLng('phone-verify'));
    }
}
