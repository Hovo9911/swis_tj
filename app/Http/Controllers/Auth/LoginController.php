<?php

namespace App\Http\Controllers\Auth;

use App\Facades\Mailer;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\BaseModel;
use App\Models\Languages\Language;
use App\Models\Role\Role;
use App\Models\User\User;
use App\Models\UserAuthFail\UserAuthFail;
use App\Models\UserRoles\UserRoles;
use App\Models\UserSession\UserSessionManager;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class LoginController
 * @package App\Http\Controllers\Auth
 */
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/role/set';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Function to Login user (check user exist in TAX SERVICE)
     *
     * @param LoginRequest $request
     * @param $lngCode
     * @return JsonResponse
     */
    public function login(LoginRequest $request, $lngCode)
    {
        $data = $request->validated();
        $username = $data['username'];

        // if user login with username password
        $user = User::select('id', 'show_status')->where(DB::raw("lower(username)"), strtolower($username))->first();
        if (!is_null($user) && $user->show_status != User::STATUS_ACTIVE) {
            $errors['credentials'] = trans('swis.sw_admin.inactivated_user');

            return responseResult('INVALID_DATA', '', '', $errors);
        }

        $authData = [
            'username' => strtolower($username),
            'password' => $data['password']
        ];

        // if user blocked
        $isBlocked = UserAuthFail::isBlocked($request, $username);
        if ($isBlocked) {
            $now = now()->toDateTimeString();
            $updatedAt = UserAuthFail::where('username', $username)->pluck('updated_at')->first();
            $minutesLeft = datesDifferenceInMinutes($now, $updatedAt);
            $blockTime = config('global.user_login.block_time');

            // Notify user about blocking account

            if (!is_null($user) && $isBlocked->is_email_send == 0 && $user->email_notification && !is_null($user->email) && !empty($user->email)) {
                $ip = request()->ip();

                $mail = [
                    'subject' => trans('swis.email.block-user.subject'),
                    'to' => $user->email,
                    'body' => view('mailer.block-user', compact('now', 'ip', 'blockTime')),
                ];

                Mailer::send($mail);

                // Update UserAuthFail set is_email_send=1
                UserAuthFail::where(['username' => $username])->update(['is_email_send' => BaseModel::TRUE]);
            }

            $errors['credentials'] = trans('swis.user_blocked', ['USER_LOGIN_BLOCK_TIME' => ($blockTime - $minutesLeft)]);

            return responseResult('INVALID_DATA', '', '', $errors);
        }

        // Login
        $result = Auth::guard('web')->attempt($authData);

        // if user successfully login
        if ($result) {

            $user = auth()->user();
            $user->lastlogin_at = Carbon::now();
            $user->lastlogin_ip = request()->ip();

            $user->save();

            // Save User Session Id
            UserSessionManager::store(Auth::user()->id, session()->getId());

            $lCode = cLng('code');
            if (Auth::check()) {

                // Check if user need to change password
                if (Auth::user()->hasToChangePassword()) {
                    return responseResult('OK', url('/' . $lCode . '/user-password-change'));
                }

                $userLanguageCode = Language::where(['id' => Auth::user()->lng_id])->pluck('code')->first();

                if ($userLanguageCode) {
                    $lCode = $userLanguageCode;
                }

                if (in_array(Auth::user()->ssn, User::SW_ADMIN_SSN)) {

                    $userRoles = UserRoles::where(['user_id' => Auth::id(), 'role_id' => Role::SW_ADMIN, 'role_status' => Role::STATUS_ACTIVE]);

                    $currentRoleData = [
                        'user_role_ids' => $userRoles->pluck('id')->all(),
                        'role_id' => Role::SW_ADMIN,
                        'role' => Role::ROLE_LEVEL_TYPE_DEFAULT,
                        'role_type' => Role::ROLE_TYPE_SW_ADMIN,
                        'authorized_by_id' => Role::FALSE
                    ];

                    Auth::user()->setCurrentRole((object)$currentRoleData);
                }
            }

            UserAuthFail::where([
                'username' => $username,
                'hash' => sha1($username . $request->header('User-Agent') . $request->ip()),
            ])->delete();

            return responseResult('OK', url('/' . $lCode . '/dashboard'));
        }

        // Fail count
        $fail = $this->failsCount($request);
        $totalFailedCount = config('global.user_login.failed_count') - $fail['count'];

        $errors['fail_count_message'] = trans('swis.auth.fail.count', ['count' => $totalFailedCount < 0 ? 0 : $totalFailedCount]);
        $errors['fail_count'] = $fail['count'];
        $errors['credentials'] = trans('auth.failed');

        return responseResult('INVALID_DATA', '', '', $errors);
    }

    /**
     * Function to log the user out of the application.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function logout(Request $request)
    {
        // Delete User Session Id
        UserSessionManager::delete(Auth::user()->id, session()->getId());

        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/');
    }

    /**
     * Function to store fails count and increment
     *
     * @param $request
     * @return array
     */
    private function failsCount($request)
    {
        $user = UserAuthFail::firstOrCreate([
            'username' => $request->input('username'),
            'hash' => sha1($request->input('username') . $request->header('User-Agent') . $request->ip()),
        ]);

        if ($user->count >= (config('global.user_login.failed_count') - 1)) {
            $user->increment('count');
            $user->is_blocked = 1;
            $user->save();
        } else {
            $user->increment('count');
            $user->last_increment_date = Carbon::now();
            $user->save();
        }

        return ['count' => $user->count];
    }
}
