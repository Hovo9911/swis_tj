<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Password\PasswordRequest;
use App\Models\User\User;
use App\Models\User\UserVerify;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *k
     * @param array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * @param $lng_code
     * @param $token
     * @return View
     */
    public function showPasswordForm($lng_code, $token)
    {
        $userVerify = UserVerify::where(['type' => 'password', 'token' => $token])->whereDate('created_at', Carbon::today())->first();

        if (empty($token) || is_null($userVerify)) {
            return view('errors.404');
        }

        return view('auth.passwords.set', compact('token'));
    }

    /**
     * @param PasswordRequest $request
     * @return JsonResponse
     */
    public function password(PasswordRequest $request)
    {
        $data = $request->validated();

        $userVerify = UserVerify::where(['type' => User::VERIFY_TYPE_PASSWORD, 'token' => $data['token']])->first();

        if (!is_null($userVerify)) {
            User::where(['id' => $userVerify->user_id])->update([
                'is_email_verified' => '1',
                'is_phone_verified' => '1',
                'password' => Hash::make($data['password'])
            ]);

            $userVerify->delete();
        }

        return responseResult('OK', urlWithLng('login'), '', []);

    }
}
