<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\BaseController;
use App\Http\Requests\User\PasswordChangeRequest;
use App\Models\User\UserManager;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

/**
 * Class ChangePasswordController
 * @package App\Http\Controllers\Auth
 */
class ChangePasswordController extends BaseController
{
    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * ChangePasswordController constructor.
     *
     * @param UserManager $userManager
     */
    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * Function to show view
     *
     * @return View|RedirectResponse
     */
    public function showPasswordForm()
    {
        if(!Auth::user()->password_need_change){
            return redirect(urlWithLng('dashboard'));
        }

        return view('auth.passwords.change');
    }

    /**
     * Function to change password
     *
     * @param PasswordChangeRequest $passwordChangeRequest
     * @return JsonResponse
     */
    public function changePassword(PasswordChangeRequest $passwordChangeRequest)
    {
        $newPassword = $passwordChangeRequest->get('password');

        $this->userManager->changePassword($newPassword);

        return responseResult('OK', urlWithLng('role/set'));
    }
}