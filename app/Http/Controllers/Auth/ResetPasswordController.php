<?php

namespace App\Http\Controllers\Auth;

use App\Facades\Mailer;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ResetPasswordRequest;
use App\Models\User\User;
use App\Models\User\UserVerify;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;

/**
 * Class ResetPasswordController
 * @package App\Http\Controllers\Auth
 */
class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Display the password reset view for the given token.
     *
     * @return View
     */
    public function showResetForm()
    {
        return view('auth.passwords.reset');
    }

    /**
     * Function to send token
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function sendToken(Request $request)
    {
        if (empty($request->input('search'))) {
            $errors['search'] = trans('core.base.field.required');
            return responseResult('INVALID_DATA', '', '', $errors);
        }

        $search = $request->input('search');
        $user = User::select('id', 'ssn', 'email')
            ->where(function ($query) use ($search) {
                $query->orWhere('ssn', $search)->orWhere('username', $search);
            })
            ->first();


        if (is_null($user)) {
            $errors['search'] = trans('swis.core.base.not-found-user');
            return responseResult('INVALID_DATA', '', '', $errors);
        }

        if (!$user->email) {
            $errors['search'] = trans('swis.reset_password.user_email_is_empty.message');
            return responseResult('INVALID_DATA', '', '', $errors);
        }

        // Save token in to database
        $token = uniqid() . '' . time() . '' . str_random(12);
        UserVerify::create([
            'user_id' => $user->id,
            'type' => 'password',
            'token' => $token
        ]);

        // Send Activation Code
        $lngCode = cLng('code');
        $data = [
            'subject' => trans('swis.reset_password_mail.subject'),
            'to' => $user->email,
            'body' => view('mailer.forgot-password', compact('lngCode', 'token')),
        ];

        Mailer::send($data);

        Session::put('success', trans('core.base.label.reset.password_reset_mail_session'));

        return responseResult('OK', urlWithLng('/'), '', []);
    }

    /**
     * Function to reset the given user's password.
     *
     * @param Request $request
     * @param $lngCode
     * @param $token
     * @return View
     */
    public function reset(Request $request, $lngCode, $token)
    {
        if (empty($token)) abort(404);
        $verify = UserVerify::where('type', 'password')->where('token', $token)->whereDate('created_at', Carbon::today())->orderByDesc()->first();
        if (is_null($verify)) {
            abort(404);
        }

        return view('auth.passwords.new', compact('token'));
    }

    /**
     * Function to set new password
     *
     * @param ResetPasswordRequest $request
     * @return JsonResponse
     */
    public function setNewPassword(ResetPasswordRequest $request)
    {
        $verify = UserVerify::where('type', 'password')->where('token', $request->input('token'))->whereDate('created_at', Carbon::today())->orderByDesc()->first();

        if (!is_null($verify)) {
            $user = User::find($verify->user_id);
            $user->password = Hash::make($request->input('password'));
            $user->save();
            UserVerify::where('type', 'password')->where('token', $request->input('token'))->delete();

            Session::put('success', trans('swis.reset_password.updated'));
            return responseResult('OK', urlWithLng('login'), '', '');
        } else {
            return responseResult('INVALID_DATA', '', '', '');
        }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
}
