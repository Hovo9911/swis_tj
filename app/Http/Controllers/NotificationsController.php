<?php

namespace App\Http\Controllers;

use App\Http\Requests\Notifications\NotificationsRequest;
use App\Models\Notifications\NotificationsManager;
use App\Models\Notifications\NotificationsSearch;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

/**
 * Class NotificationsController
 * @package App\Http\Controllers
 */
class NotificationsController extends BaseController
{
    /**
     * @var NotificationsManager
     */
    protected $manager;

    /**
     * NotificationsController constructor.
     * @param NotificationsManager $manager
     */
    public function __construct(NotificationsManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Function to show index page
     *
     * @return RedirectResponse|View
     */
    public function index()
    {
        if (Auth::user()->isNaturalPerson()) {
            return redirect(urlWithLng('/'));
        }

        return view('notifications.index');
    }

    /**
     * Function to get all data and showing in datatable
     *
     * @param Request $request
     * @param NotificationsSearch $notificationSearch
     * @return JsonResponse
     */
    public function table(Request $request, NotificationsSearch $notificationSearch)
    {
        $data = $this->dataTableAll($request, $notificationSearch);

        return response()->json($data);
    }

    /**
     * Function to update data
     *
     * @param NotificationsRequest $request
     * @return JsonResponse
     */
    public function update(NotificationsRequest $request)
    {
        $this->manager->update($request->validated());

        return responseResult('OK');
    }

    /**
     * Function to mark as read all unread notifications
     *
     * @param NotificationsRequest $request
     * @return JsonResponse
     */
    public function markAsReadAllUnreadNotifications(NotificationsRequest $request)
    {
        $this->manager->markAsReadAllUnreadNotifications();

        return responseResult('OK');
    }

}
