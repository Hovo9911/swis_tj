<?php

namespace App\Http\Controllers;

use App\Exports\PaymentsExport;
use App\Http\Requests\Payments\PaymentsRequest;
use App\Http\Requests\Payments\PaymentsSearchRequest;
use App\Http\Requests\Pays\PaysRequest;
use App\Models\Agency\Agency;
use App\Models\Company\Company;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\OnlineTransactions\OnlineTransactions;
use App\Models\OnlineTransactions\OnlineTransactionsManager;
use App\Models\Payments\PaymentsNotificationsManager;
use App\Models\Pays\Pays;
use App\Models\Pays\PaysManager;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\Transactions\Transactions;
use App\Models\Transactions\TransactionsSearch;
use App\Models\User\User;
use App\Payments\PaymentsManager;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Class PaymentController
 * @package App\Http\Controllers
 */
class PaymentController extends BaseController
{
    /**
     * @var PaysManager
     */
    protected $manager;

    /**
     * @var OnlineTransactionsManager
     */
    protected $onlineTransactionsManager;

    /**
     * @var PaymentsNotificationsManager
     */
    protected $paymentsNotificationsManager;

    /**
     * PaymentController constructor.
     *
     * @param PaysManager $manager
     * @param OnlineTransactionsManager $onlineTransactionsManager
     * @param PaymentsNotificationsManager $paymentsNotificationsManager
     */
    public function __construct(PaysManager $manager, OnlineTransactionsManager $onlineTransactionsManager, PaymentsNotificationsManager $paymentsNotificationsManager)
    {
        $this->manager = $manager;
        $this->onlineTransactionsManager = $onlineTransactionsManager;
        $this->paymentsNotificationsManager = $paymentsNotificationsManager;
    }

    /**
     * Function to return index page
     *
     * @return View
     */
    public function index()
    {
        return view('payments.index')->with([
            'params' => PaymentsManager::createAlifPayment(),
            'agencies' => Agency::allData(),
            'constructorDocuments' => ConstructorDocument::getConstructorDocuments([], false),
            'refObligationTypes' => getReferenceRows(ReferenceTable::REFERENCE_OBLIGATION_TYPE)
        ]);
    }

    /**
     * Function to all data show in dataTable
     *
     * @param PaymentsSearchRequest $paymentsSearchRequest
     * @param TransactionsSearch $transactionsSearch
     * @return JsonResponse
     */
    public function table(PaymentsSearchRequest $paymentsSearchRequest, TransactionsSearch $transactionsSearch)
    {
        $data = $this->dataTableAll($paymentsSearchRequest, $transactionsSearch);

        return response()->json($data);
    }

    /**
     * Function to export payments table structure
     *
     * @param TransactionsSearch $transactionsSearch
     * @param Request $request
     * @return BinaryFileResponse
     */
    public function paymentsTableExport(TransactionsSearch $transactionsSearch, Request $request)
    {
        $query = $transactionsSearch->callToConstructorQuery($request->all());

        //
        $transactionExport = new PaymentsExport();
        $transactionExport->setData($query->orderByDesc('transactions.id')->get());

        return Excel::download($transactionExport, "payments.xls");
    }

    /**
     * Function to correct amount format and token
     *
     * @param PaymentsRequest $request
     * @return JsonResponse
     */
    public function getDynamicParams(PaymentsRequest $request)
    {
        $onlineTransaction = $this->onlineTransactionsManager->store([
            'user_id' => Auth::user()->id,
            'company_tax_id' => (Auth::user()->companyTaxId()) ? Auth::user()->companyTaxId() : Auth::user()->ssn,
            'type' => 'alif',
            'token' => '',
            'amount' => $request->input('amount'),
            'status' => 'pending'
        ]);

        $token = PaymentsManager::getAlifDynamicParams($request->input('amount'), $onlineTransaction->id);
        $onlineTransaction->token = $token;
        $onlineTransaction->save();

        return responseResult('OK', null, ['token' => $token, 'orderId' => $onlineTransaction->id]);
    }

    //------------------------------- API PART -------------------------------//

    /**
     *  Function to get alif response and send to payment/add method
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws GuzzleException
     */
    public function alifAdd(Request $request)
    {
        $contentRaw = file_get_contents("php://input");
        $content = json_decode($contentRaw, false);

        if (!empty($content)) {
            $token = PaymentsManager::getCallbackToken($content);

            if ($token === $content->token) {
                $onlineTransaction = OnlineTransactions::where('id', $content->orderId)->first();

                $status = 'ok';
                $code = 200;
                $description = '';

                if (!is_null($onlineTransaction)) {
                    try {
                        if ($content->status == 'ok') { // if from alif posted ok status
                            $client = new Client();
                            $request = $client->request("POST", urlWithLng(config('payment.api.add')), [
                                'form_params' => [
                                    'token' => config('payment.api.token'),
                                    'document_id' => $content->transactionId,
                                    'payment_done_date' => time(),
                                    'amount' => $content->amount,
                                    'beneficiary_name' => $content->phone,
                                    'tax_id' => $onlineTransaction->company_tax_id,
                                    'user_id' => $onlineTransaction->user_id,
                                    'description' => ''
                                ]
                            ]);

                            $resultRaw = $request->getBody()->getContents();
                            $result = json_decode($resultRaw);

                            if ($result->status == 'ok') {
                                $onlineTransaction->status = $content->status;
                                $onlineTransaction->amount = $content->amount;
                                $onlineTransaction->transaction_id = $content->transactionId;
                            } else {

                                Bugsnag::notifyError('alif-payment-error', 'Payment not created in Transaction, Online Transaction id: ' . $content->orderId);

                                Log::info('alif-payment-error: ' . __LINE__ . ' ' . print_r($resultRaw . '', 1));

                                $onlineTransaction->status = $content->status;
                                $status = $result->status;
                                $code = $result->code;
                                $description = $result->description;
                            }
                        } else {
                            Bugsnag::notifyError('alif-payment-error', 'Payment not created in Transaction, Online Transaction id: ' . $content->orderId);

                            Log::info('alif-payment-error: ' . __LINE__ . ' ' . print_r($contentRaw . '', 1));

                            $onlineTransaction->status = $content->status;
                        }

                    } catch (Exception $exception) {

                        Bugsnag::notifyError('alif-payment-error', 'Exception message: ' . $exception->getMessage());

                        Log::info('alif-payment-error: ' . __LINE__ . ' ' . $exception->getMessage());

                        return response()->json([
                            'status' => 'error',
                            'code' => 500,
                            'description' => 'The system is not available, please try later.',
                        ]);
                    }

                    $onlineTransaction->save();

                    $successResult = [
                        'status' => $status,
                        'code' => $code,
                        'description' => $description
                    ];

                    Log::info('alif-payment-result: ' . __LINE__ . ' ' . json_encode($successResult));

                    return response()->json($successResult);
                } else {

                    Log::info('alif-payment-error: ' . __LINE__ . ' onlineTransaction not found');

                    Bugsnag::notifyError('alif-payment-error', 'OnlineTransaction not found');

                    return response()->json([
                        'status' => 'error',
                        'code' => 500,
                        'description' => 'Record not found.'
                    ]);
                }

            } else {
                Log::info('alif-payment-error: ' . __LINE__ . ' token mismatch "' . $token . '" === "' . $content->token . '"');

                Bugsnag::notifyError('alif-payment-error', 'Token mismatch');
            }

            return response()->json([
                'status' => 'error',
                'code' => 500,
                'description' => 'Callback token not correct.'
            ]);

        } else {
            Log::info('alif-payment-error: ' . __LINE__ . ' empty request');

            Bugsnag::notifyError('alif-payment-error', 'Empty request');

            return response()->json([
                'status' => 'error',
                'code' => 500,
                'description' => 'Request body is empty.'
            ]);
        }
    }

    /**
     * Function for validate and store data in db
     *
     * @param PaysRequest $request
     * @return JsonResponse
     */
    public function add(PaysRequest $request)
    {
        $ipAddress = request()->ip();
        $provider = $this->manager->getProvider($request->input('token'));
        $documentId = $request->input('document_id');

        $checkDoc = Pays::select('id')->where('document_id', $request->input('document_id'))->first();
        if (!is_null($checkDoc)) {
            Log::info('swis-payment-error: ' . __LINE__ . ' Document_id is not unique for payment provider ' . $documentId);
            return response()->json([
                'status' => 'error',
                'code' => 422,
                'description' => 'Document_id is not unique for payment provider',
            ]);
        }

        $tin = $request->input('tax_id');
        if (is_null($tin)) {

            Log::info('swis-payment-error: ' . __LINE__ . ' Invalid tin ' . $tin);

            return response()->json([
                'status' => 'error',
                'code' => 422,
                'description' => 'Invalid Data',
            ]);

        }

        return DB::transaction(function () use ($ipAddress, $provider, $documentId, $request, $tin) {

            try {

                $user = (!empty($request->input('user_id'))) ? $request->input('user_id') : User::select('id')->where('ssn', $tin)->pluck('id')->first();
                $amount = number_format($request->input('amount'), 2, '.', '');

                $payment = Pays::create([
                    'ip' => $ipAddress,
                    'service_provider_id' => $provider->id,
                    'document_id' => $documentId,
                    'payment_done_date' => date(config('swis.date_time_format'), $request->input('payment_done_date')),
                    'amount' => $amount,
                    'beneficiary_name' => $request->input('beneficiary_name'),
                    'tax_id' => $tin,
                    'description' => $request->input('description'),
                    'status' => 'ok',
                    'sended_into_balance' => is_null($user) ? 'no' : 'yes'
                ]);

                $userBalance = User::getBalance($tin, $user);
                $userBalance = number_format($userBalance + $amount, 2, '.', '');

                Transactions::create([
                    'payment_id' => $payment->id,
                    'user_id' => is_null($user) ? 0 : $user,
                    'company_tax_id' => $tin,
                    'transaction_date' => date(config('swis.date_time_format'), $request->input('payment_done_date')),
                    'type' => 'input',
                    'amount' => $amount,
                    'total_balance' => $userBalance
                ]);

                $this->paymentsNotificationsManager->sendPaymentNotification($user, $tin, $amount, $userBalance);

                return response()->json([
                    'status' => 'ok',
                    'code' => 200
                ]);

            } catch (Exception $exception) {

                Log::info('swis-payment-error: ' . __LINE__ . ' ' . $exception->getMessage());
                return response()->json([
                    'status' => 'error',
                    'code' => 422,
                    'description' => 'Invalid Data',
                ]);

            }
        });
    }

    /**
     * Function for get pay status
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function status(Request $request)
    {
        $checkDoc = Pays::select('id')->where('document_id', $request->input('document_id'))->exists();

        if (!$checkDoc) {
            return response()->json([
                'status' => 'error',
                'code' => 200,
                'description' => 'Document_id does not exists',
            ]);
        }

        return response()->json([
            'status' => 'ok',
            'code' => 200
        ]);
    }

    /**
     * Function for check user tin
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function checkTin(Request $request)
    {
        $tin = $request->input('tin');
        if (is_null($tin)) {
            return response()->json([
                'status' => 'error',
                'code' => 200,
                'description' => 'Invalid Data',
            ]);
        }

        $company = Company::where('tax_id', $tin)->exists();
        if (!$company) {
            $user = User::where('ssn', $tin)->exists();
            if (!$user) {
                return response()->json([
                    'status' => 'error',
                    'code' => 200,
                    'description' => 'Invalid TIN',
                ]);
            }
        }

        return response()->json([
            'status' => 'ok',
            'code' => 200
        ]);
    }

}
