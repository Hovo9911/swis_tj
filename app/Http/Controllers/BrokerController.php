<?php

namespace App\Http\Controllers;

use App\EntityTypesInfo\LegalEntityForms;
use App\Exceptions\TaxServiceTjCompanyInactiveException;
use App\Exceptions\TaxServiceTjInnNotFoundException;
use App\Exceptions\TaxServiceTjInvalidResponseException;
use App\Http\Requests\Broker\BrokerSearchRequest;
use App\Http\Requests\Broker\BrokerStoreRequest;
use App\Http\Requests\Broker\BrokerUpdateRequest;
use App\Models\Broker\Broker;
use App\Models\Broker\BrokerManager;
use App\Models\Broker\BrokerSearch;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

/**
 * Class BrokerController
 * @package App\Http\Controllers
 */
class BrokerController extends BaseController
{
    /**
     * @var BrokerManager
     */
    protected $manager;

    /**
     * @var LegalEntityForms
     */
    protected $legalEntityForms;

    /**
     * BrokerController constructor.
     *
     * @param BrokerManager $manager
     * @param LegalEntityForms $legalEntityForms
     */
    public function __construct(BrokerManager $manager, LegalEntityForms $legalEntityForms)
    {
        $this->manager = $manager;
        $this->legalEntityForms = $legalEntityForms;
    }

    /**
     * Function to show index page
     *
     * @return View
     */
    public function index()
    {
        return view('broker.index');
    }

    /**
     * Function to show data in dataTable
     *
     * @param BrokerSearchRequest $brokerSearchRequest
     * @param BrokerSearch $brokerSearch
     * @return JsonResponse
     */
    public function table(BrokerSearchRequest $brokerSearchRequest, BrokerSearch $brokerSearch)
    {
        $data = $this->dataTableAll($brokerSearchRequest, $brokerSearch);

        return response()->json($data);
    }

    /**
     * Function to show create form
     *
     * @return View
     */
    public function create()
    {
        return view('broker.edit')->with([
            'saveMode' => 'add'
        ]);
    }

    /**
     * Function to Store Data
     *
     * @param BrokerStoreRequest $request
     * @return JsonResponse
     *
     * @throws TaxServiceTjCompanyInactiveException
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function store(BrokerStoreRequest $request)
    {
        $company = $this->legalEntityForms->getCompanyInfo($request->input('tax_id'));

        if (!$company) {
            $errors['tax_id'] = trans('swis.tax_id.not_found');

            return responseResult('INVALID_DATA', '', '', $errors);
        }

        //
        $data = $request->validated();

        $data['address'] = $company['address'];
        $data['legal_entity_name'] = $company['name'];

        $broker = $this->manager->store($data);

        return responseResult('OK', null, ['id' => $broker->id]);
    }

    /**
     * Function to show edit page broker by id
     *
     * @param $lngCode
     * @param $id
     * @param $viewType
     * @return View
     */
    public function edit($lngCode, $id, $viewType)
    {
        $broker = Broker::where('id', $id)->checkUserHasRole()->firstOrFail();

        return view('broker.edit')->with([
            'broker' => $broker,
            'saveMode' => $viewType
        ]);
    }

    /**
     * Function to update data
     *
     * @param BrokerUpdateRequest $request
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     *
     * @throws TaxServiceTjCompanyInactiveException
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function update(BrokerUpdateRequest $request, $lngCode, $id)
    {
        $company = $this->legalEntityForms->getCompanyInfo($request->input('tax_id'));

        if (!$company) {
            $errors['tax_id'] = trans('swis.tax_id.not_found');

            return responseResult('INVALID_DATA', '', '', $errors);
        }

        //
        $data = $request->validated();

        $data['address'] = $company['address'];
        $data['legal_entity_name'] = $company['name'];

        $this->manager->update($data, $id);

        return responseResult('OK');
    }

    /**
     * Function to If user is not Broker! get AUTH user DATA
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function brokerData(Request $request)
    {
        $userData = [];

        $status = 'INVALID_DATA';
        if (!Auth::user()->isBroker()) {

            $authUserCreator = Auth::user()->getCreator();

            $userData = [
                'name' => $authUserCreator->name,
                'type_value' => $authUserCreator->ssn,
                'passport' => $authUserCreator->passport,
                'address' => $authUserCreator->address,
                'phone_number' => $authUserCreator->phone_number,
                'email' => $authUserCreator->email,
                'country' => $authUserCreator->country,
                'type' => $authUserCreator->type,
                'community' => $authUserCreator->community ?? '-',
            ];

            $status = 'OK';
        }

        $data['userData'] = $userData;

        return responseResult($status, '', $data);
    }
}