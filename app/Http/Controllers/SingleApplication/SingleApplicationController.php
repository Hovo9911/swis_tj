<?php

namespace App\Http\Controllers\SingleApplication;

use App\Http\Controllers\BaseController;
use App\Http\Requests\SingleApplication\SingleApplicationRequest;
use App\Http\Requests\SingleApplication\SingleApplicationSearchRequest;
use App\Http\Requests\SingleApplication\SingleApplicationSendMessageRequest;
use App\Models\Agency\Agency;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\DataModel\DataModel;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\Routing\Routing;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplication\SingleApplicationManager;
use App\Models\SingleApplication\SingleApplicationSearch;
use App\Models\SingleApplication\SingleApplicationXmlManager;
use App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure;
use App\Models\SingleApplicationDocuments\SingleApplicationDocumentsManager;
use App\Models\SingleApplicationNotes\SingleApplicationNotes;
use App\Models\SingleApplicationProducts\SingleApplicationProducts;
use App\Models\SingleApplicationProducts\SingleApplicationProductsManager;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplicationsManager;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;
use Throwable;

/**
 * Class SingleApplicationController
 * @package App\Http\Controllers\SingleApplication
 */
class SingleApplicationController extends BaseController
{
    /**
     * @var SingleApplicationManager
     */
    protected $manager;

    /**
     * @var SingleApplicationXmlManager
     */
    protected $xmlManager;

    /**
     * @var SingleApplicationProductsManager
     */
    protected $productManager;

    /**
     * @var SingleApplicationSubApplicationsManager
     */
    protected $subApplicationsManager;

    /**
     * @var SingleApplicationDocumentsManager
     */
    protected $singleApplicationDocumentsManager;

    /**
     * SingleApplicationController constructor.
     *
     * @param SingleApplicationManager $manager
     * @param SingleApplicationXmlManager $singleApplicationXmlManager
     * @param SingleApplicationProductsManager $productManagers
     * @param SingleApplicationSubApplicationsManager $singleApplicationSubApplicationsManager
     * @param SingleApplicationDocumentsManager $singleApplicationDocumentsManager
     */
    public function __construct(SingleApplicationManager $manager, SingleApplicationXmlManager $singleApplicationXmlManager, SingleApplicationProductsManager $productManagers, SingleApplicationSubApplicationsManager $singleApplicationSubApplicationsManager, SingleApplicationDocumentsManager $singleApplicationDocumentsManager)
    {
        $this->manager = $manager;
        $this->xmlManager = $singleApplicationXmlManager;
        $this->productManager = $productManagers;
        $this->subApplicationsManager = $singleApplicationSubApplicationsManager;
        $this->singleApplicationDocumentsManager = $singleApplicationDocumentsManager;
    }

    /**
     * Function to show index page
     *
     * @param $lngCode
     * @param $regime
     * @return RedirectResponse|View
     */
    public function index($lngCode, $regime)
    {
        if (!in_array($regime, SingleApplication::REGIMES)) {
            return redirect('404');
        }

        $safSubApplicationDocuments = SingleApplicationSubApplications::where(['user_company_tax_id' => Auth::user()->companyTaxId()])->where('constructor_document_id', '!=', '0')->pluck('constructor_document_id')->unique()->all();

        return view('single-application.index')->with([
            'regime' => $regime,
            'customBodyReference' => getReferenceRows('custom_body_reference', false, false, ['id', 'code', 'name', 'address']),
            'countries' => getReferenceRows('countries'),
            'constructorDocuments' => $constructorDocuments = ConstructorDocument::getConstructorDocuments($safSubApplicationDocuments),
            'safCreatedType' => SingleApplication::CREATED_TYPE,
            'safVoidType' => SingleApplication::VOID_TYPE
        ]);
    }

    /**
     * Function to get all saf data
     *
     * @param SingleApplicationSearchRequest $singleApplicationSearchRequest
     * @param SingleApplicationSearch $singleApplicationSearch
     * @return JsonResponse
     */
    public function table(SingleApplicationSearchRequest $singleApplicationSearchRequest, SingleApplicationSearch $singleApplicationSearch)
    {
        $data = $this->dataTableAll($singleApplicationSearchRequest, $singleApplicationSearch);

        return response()->json($data);
    }

    /**
     * Function to create new saf in db and redirect to saf edit page
     *
     * @param $lngCode
     * @param $regime
     * @param bool $regularNumber
     * @return RedirectResponse
     */
    public function create($lngCode, $regime, $regularNumber = false)
    {
        if (!in_array($regime, SingleApplication::REGIMES)) {
            return redirect('404');
        }

        if (!$regularNumber) {
            DB::beginTransaction();

            $regularNumber = $this->manager->returnRegularNumber();

            $safData = [
                'regular_number' => $regularNumber,
                'regime' => $regime,
                'saf_data_structure_id' => SingleApplicationDataStructure::lastXml('id'),
                'status_type' => SingleApplication::CREATED_TYPE,
            ];

            $this->manager->store($safData);

            DB::commit();
        }

        return redirect(urlWithLng('single-application/edit/' . $regime . '/' . $regularNumber));
    }

    /**
     * Function to store SAF all data
     *
     * @param SingleApplicationRequest $singleApplicationRequest
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function store(SingleApplicationRequest $singleApplicationRequest)
    {
        $data = $singleApplicationRequest->validated();

        $safNumber = $data['saf']['general']['regular_number'];
        $safRegime = $data['saf']['general']['regime'];
        $statusType = $data['status_type'];

        $saf = SingleApplication::select('id', 'regular_number', 'status_type', 'data')->where(['regular_number' => $safNumber, 'regime' => $safRegime])->firstOrFail();

        $allSafData = $data['saf'];
        if ($saf->hasSubmittedSubApplication()) {
            $allSafData = array_merge($allSafData, $saf->data['saf']);
        }

        //
        if ($statusType == SingleApplication::VOID_TYPE) {
            $saf->update(['status_type' => SingleApplication::VOID_TYPE]);

            SingleApplicationNotes::storeNotes(['saf_number' => $safNumber, 'note' => 'swis.saf.status_type.' . SingleApplication::VOID_TYPE], SingleApplicationNotes::NOTE_TRANS_KEY_SAF_UPDATE_STATUS_TYPE);

            return responseResult('OK', urlWithLng('single-application/' . $safRegime));
        }

        //
        if ($statusType == SingleApplication::SEND_TYPE || $statusType == SingleApplication::VERIFIED_TYPE) {
            $safProductsCount = SingleApplicationProducts::where('saf_number', $safNumber)->count();

            if ($safProductsCount == 0) {
                $errors['messages'] = [trans('swis.saf.products_count.count')];

                return responseResult('INVALID_DATA', '', '', $errors);
            }
        }

        // Check what documents need to attach for sub application
        if ($statusType == SingleApplication::VERIFIED_TYPE) {

            $needAttachDocumentForProductsSubApplications = $this->productManager->productSubApplications($safNumber, $safRegime, $allSafData, true);

            if (count($needAttachDocumentForProductsSubApplications)) {

                $errors['messages'] = $needAttachDocumentForProductsSubApplications;

                return responseResult('INVALID_DATA', '', '', $errors);
            }

            $dataResult['message'] = trans('swis.saf.verify_operation.ok');

            return responseResult('OK', '', $dataResult, []);
        }

        //
        if ($statusType == SingleApplication::SEND_TYPE) {

            // if rule write in product fields
            $needAttachDocumentForProductsSubApplications = $this->productManager->productSubApplications($safNumber, $safRegime, $allSafData);

            if (count($needAttachDocumentForProductsSubApplications)) {
                Session::flash($safNumber . '_need_attach_docs', $needAttachDocumentForProductsSubApplications);
            }

            // NOT_USED // if rule write saf tabs inputs not product
//            $this->safFieldsSubApplications($data);
        }

        // Store Tabs inputs all Data as json (saf controlled fields too)
        $this->manager->storeDataJson($data);

        if ($statusType == SingleApplication::SEND_TYPE) {
            return responseResult('OK', urlWithLng('single-application/edit/' . $safRegime . '/' . $safNumber));
        }

        return responseResult('OK', urlWithLng('single-application/edit/' . $safRegime . '/' . $safNumber));
    }

    /**
     * Function to edit Saf by regular number
     *
     * @param $lngCode
     * @param $regime
     * @param $viewType
     * @param bool $regularNumber
     * @return RedirectResponse|View
     */
    public function edit($lngCode, $viewType, $regime, $regularNumber = false)
    {
        if (!in_array($regime, SingleApplication::REGIMES)) {
            return redirect('404');
        }

        $saf = SingleApplication::select('id', 'regular_number', 'data', 'created_at', 'regime', 'saf_data_structure_id', 'status_type', 'saf_controlled_fields')->where(['regular_number' => $regularNumber, 'regime' => $regime])->safOwnerOrCreator();

        if ($viewType == SingleApplication::VIEW_MODE_EDIT) {
            $saf->where('status_type', '!=', SingleApplication::VOID_TYPE);
        }

        $saf = $saf->firstOrFail();
        $saf->update(['need_reform' => SingleApplication::FALSE]);

        // Update products national currency
        $safDisabledProducts = SingleApplicationProducts::where('saf_number', $regularNumber)->whereNotIn('id', SingleApplicationSubApplications::subApplicationDisabledProducts($regularNumber))->pluck('id')->toArray();

        if (count($safDisabledProducts)) {
//            $this->productManager->updateProductReferenceValues($safNotUsedProducts, $regularNumber);
            $this->productManager->updateProductNationalAmount($safDisabledProducts, $regularNumber);
        }

        return view('single-application.edit')->with([
            'saveMode' => $viewType,
            'regime' => $regime,

            'saf' => $saf,
            'simpleAppTabs' => $saf->xmlDataStructure(),
            'safControlledFields' => $saf->getSafControlledFields(),
            'safHiddenColumns' => SingleApplication::getSafHiddenElements(),
            'safProductsNotFormedSubApplication' => $saf->productsCodesNotFormedSubApplication(),

            'hasSubmittedOrRequestedSubApplication' => $saf->hasSubmittedOrRequestedSubApplication(),
            'hasSubmittedSubApplication' => $saf->hasSubmittedSubApplication(),

            'attachedDocumentProducts' => $this->singleApplicationDocumentsManager->getSafDocuments($regularNumber),
            'subAppDisabledProducts' => SingleApplicationSubApplications::subApplicationDisabledProducts($regularNumber),

            'notes' => SingleApplicationNotes::getSafNotes($regularNumber),

            // ----
            'safCreatedType' => SingleApplication::CREATED_TYPE,
            'safSendType' => SingleApplication::SEND_TYPE,
            'safDraftType' => SingleApplication::DRAFT_TYPE,
            'safRegimeImport' => SingleApplication::REGIME_IMPORT,
            'safRegimeExport' => SingleApplication::REGIME_EXPORT,
            'safRegimeTransit' => SingleApplication::REGIME_TRANSIT
        ]);
    }

    /**
     * Function to Delete Saf
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function delete(Request $request)
    {
        $safNumber = $request->header('sNumber');
        if (!is_null($request->input('deleteItems')) && count($request->input('deleteItems'))) {
            $safNumber = $request->input('deleteItems')[0];
        }

        $this->manager->deleteSaf($safNumber);

        if (!is_null($request->header('sNumber'))) {
            return responseResult('OK', urlWithLng('single-application/' . $request->input('type')));
        }

        return responseResult('OK');
    }

    /**
     * Function to cloning saf Data
     *
     * @param $lngCode
     * @param $safNumber
     * @return RedirectResponse
     */
    public function cloneSaf($lngCode, $safNumber)
    {
        $newCreatedSaf = $this->manager->cloneSaf($safNumber);

        return redirect(urlWithLng('single-application/edit/' . $newCreatedSaf->regime . '/' . $newCreatedSaf->regular_number));
    }

    /**
     * Function to send message when sub application will send
     *
     * @param SingleApplicationSendMessageRequest $singleApplicationSendMessageRequest
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function sendMessageToSubApplication(SingleApplicationSendMessageRequest $singleApplicationSendMessageRequest)
    {
        $safNumber = $singleApplicationSendMessageRequest->header('sNumber');
        $subApplicationId = $singleApplicationSendMessageRequest->get('subAppId');
        $message = $singleApplicationSendMessageRequest->get('message');

        SingleApplication::where('regular_number', $safNumber)->safOwnerOrCreator()->firstOrFail();
        SingleApplicationSubApplications::where(['id' => $subApplicationId, 'saf_number' => $safNumber])->firstOrFail();

        if ($message) {

            SingleApplicationManager::sendMessageToSubApplication($safNumber, $subApplicationId, $message);

            $data['html'] = view('single-application.notes.saf-notes-list')->with(['notes' => SingleApplicationNotes::getSafNotes($safNumber)])->render();

            return responseResult('OK', '', $data);
        }

        return responseResult('OK', '', []);
    }

    /**
     * Function to export Xml Saf data
     *
     * @param $lngCode
     * @param $safNumber
     */
    public function exportXmlSafData($lngCode, $safNumber)
    {
        $saf = SingleApplication::where('regular_number', $safNumber)->safOwnerOrCreator()->firstOrFail();

        $xml = $this->xmlManager->exportXmlSafData($safNumber);

        $name = $saf->regime . '_' . $saf->regular_number . '_' . time() . '.xml';
        header('Content-Disposition: attachment;filename=' . $name);
        header('Content-Type: text/xml');

        echo $xml->saveXML();

        /*$xml2 = new \DOMDocument();
        $xml2->loadXML($xml->saveXML());

        if (!$xml2->schemaValidate(storage_path('saf_xsd/' . $regimeShortName . '15TJ.xsd'))) {

            $errors = libxml_display_errors();
            dd($errors);

            if (count($errors)) {
                foreach ($errors as $error) {
                    preg_match('/(?<=\:)(.*?)(?=\.)/', $error, $matches);
                    dd($errors, $error, $matches);
                }

            }
        }*/
    }

    /**
     * Function to checking Routing exist and get constructor document
     *
     * @param $data
     * @return array
     */
    // NOT USED
    private function checkRoutingConstructorDocuments($data, $saf)
    {
        $safRegime = $data['saf']['general']['regime'];

        $constructorRouting = Routing::activeRoutings($safRegime);

        $simpleAppTabs = $saf->xmlDataStructure();

        $modules = SingleApplication::XML_MODULES;

        $safDataValues = [];

        foreach ($modules as $module) {
            $blocks = $simpleAppTabs->xpath('/tabs/tab[@key="' . $module . '"]');
            foreach ($blocks[0]->block as $block) {
                foreach ($block->fieldset as $fieldset) {

                    foreach ($fieldset as $field) {
                        $attr = $field->attributes();
                        $name = (string)$attr['name'];
                        if (!empty($attr['id']) && !isset($attr['safControlledField'])) {

                            if (strpos($name, ']') === false) {
                                continue;
                            }

                            $name = str_replace(']', '', $name);
                            $name = str_replace('[', '-', $name);
                            $name = explode('-', $name);

                            if (!empty((string)$attr['id']) && !empty($data[$name[0]][$name[1]][$name[2]])) {

                                $mdm = DataModel::where(DB::raw('id::varchar'), (string)$attr->mdm)->first();

                                $defaultVal = $data[$name[0]][$name[1]][$name[2]];
                                if (!is_null($mdm) && !empty($mdm->reference)) {
                                    if (is_array($data[$name[0]][$name[1]][$name[2]])) {
                                        $refData = getReferenceRowsWhereIn($mdm->reference, $data[$name[0]][$name[1]][$name[2]]);
                                    } else {
                                        $refData = getReferenceRows($mdm->reference, $data[$name[0]][$name[1]][$name[2]]);

                                        if (!is_null($refData)) {
                                            $defaultVal = optional($refData)->code;
                                        }
                                        $safDataValues[(string)$attr['id']] = $defaultVal;
                                    }
                                }

                            }
                        }
                    }
                }
            }
        }

        $documentIds = [];
        if (!empty($constructorRouting)) {
            foreach ($constructorRouting as $value) {

                $condition = $value->rule;

                $isValidSyntax = validateSyntax($condition);
                if ($isValidSyntax === true) {

//                echo "condition syntax is valid ;)\n";
                    $isMatchCondition = checkCondition($safDataValues, $condition);

                    if (is_bool($isMatchCondition) && $isMatchCondition == true) {

//                    echo "condition matched ;)\n";
                        $documentIds[] = $value->constructor_document_id;

                    } elseif ($isMatchCondition == false) {
//                    echo "condition not matched \n";
                    } else {
//                    echo 'condition syntax is not valid: ' . $isMatchCondition;
                    }
                } else {
//                echo 'condition syntax is not valid: ' . $isValidSyntax;
                }

            }
        }

        $documentIds = array_unique($documentIds);

        $documents = null;
        if (!empty($documentIds)) {
            $documents = ConstructorDocument::whereIn('id', $documentIds)->get();
        }

        return $documents;

    }

    /**
     * Function to generate sub applications if routing rule write to saf fields inputs (not products modal)
     *
     * @param $data
     * @param bool $getWhatDocsNeedAttach
     * @return array
     */
    // NOT USED
    public function safFieldsSubApplications($data, $getWhatDocsNeedAttach = false)
    {
        return [];
        $safNumber = $data['saf']['general']['regular_number'];

        $saf = SingleApplication::select('id', 'regular_number', 'regime', 'saf_data_structure_id')->where(DB::raw('regular_number::varchar'), $safNumber)->firstOrFail();

        $constructorDocumentRoutings = $this->checkRoutingConstructorDocuments($data, $saf);

        $subApplications = [];

        if (count($constructorDocumentRoutings)) {

            $constructorDocuments = ConstructorDocument::select('id', 'document_code', 'company_tax_id', 'document_type_id')->whereIn('id', array_keys($constructorDocumentRoutings))->active()->get();

            if ($constructorDocuments->count()) {

                foreach ($constructorDocuments as $constructorDocument) {

                    $products = SingleApplicationProducts::where('saf_number', $safNumber)->pluck('id')->toArray();

                    if (!$getWhatDocsNeedAttach) {

                        $referenceDocumentType = DB::table(ReferenceTable::REFERENCE_DOCUMENT_TYPE_REFERENCE)->select('document_type')->where('id', $constructorDocument->document_type_id)->first();

                        $agency = Agency::select('id', 'tax_id')->where('tax_id', $constructorDocument->company_tax_id)->first();

                        $subApplications[] = [
                            'saf_number' => $safNumber,
                            'saf_id' => $saf->id,
                            'agency_id' => $agency->id,
                            'company_tax_id' => optional($agency)->tax_id,
                            'reference_classificator_id' => $referenceDocumentType->document_type,
                            'reference_document_type_code' => null,
                            'subtitle_type' => null,
                            'document_code' => optional($constructorDocument)->document_code,
                            'name' => '',
                            'products' => $products,
                            'current_status' => SingleApplicationSubApplications::STATUS_FORM,
                            'manual_created' => SingleApplication::FALSE,
                            'routings' => array_unique($constructorDocumentRoutings[$constructorDocument->id]),
                            'note' => ''
                        ];

                    }
                }
            }

            if ($getWhatDocsNeedAttach) {

                $allNeedDocuments = [];

                if (count($subApplications)) {
                    foreach ($subApplications as $subApplication) {

                        $needDocuments = $this->subApplicationsManager->checkSubApplicationRoutingsDocuments($safNumber, $subApplication['routings']);

                        if (count($needDocuments)) {

                            $referenceClassificator = getReferenceRows(ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS, $subApplication['reference_classificator_id']);

                            $allNeedDocuments [] = [
                                'subAppName' => optional($referenceClassificator)->name,
                                'subAppCode' => optional($referenceClassificator)->code,
                                'documents' => implode(',', $needDocuments),
                            ];
                        }
                    }
                }

                return $allNeedDocuments;
            }

            $this->subApplicationsManager->store($subApplications);
        }

        return [];
    }
}
