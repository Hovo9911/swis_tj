<?php

namespace App\Http\Controllers\SingleApplication;

use App\Http\Controllers\BaseController;
use App\Http\Requests\SingleApplication\GenerateSingleApplicationLabExaminationRequest;
use App\Http\Requests\SingleApplication\UpdateSingleApplicationLabExaminationRequest;
use App\Models\Agency\Agency;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\Laboratory\Laboratory;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplication\SingleApplicationManager;
use App\Models\SingleApplicationNotes\SingleApplicationNotes;
use App\Models\SingleApplicationProducts\SingleApplicationProducts;
use App\Models\SingleApplicationSubApplicationProducts\SingleApplicationSubApplicationProducts;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplicationCompleteData;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplicationsManager;
use App\Models\SingleApplicationSubApplicationStates\SingleApplicationSubApplicationStates;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Class SingleApplicationLabExaminationController
 * @package App\Http\Controllers\SingleApplication
 */
class SingleApplicationLabExaminationController extends BaseController
{
    /**
     * @var SingleApplicationManager
     */
    protected $manager;

    /**
     * @var SingleApplicationSubApplicationsManager
     */
    protected $subApplicationsManager;

    /**
     * SingleApplicationController constructor.
     *
     * @param SingleApplicationManager $manager
     * @param SingleApplicationSubApplicationsManager $singleApplicationSubApplicationsManager
     */
    public function __construct(SingleApplicationManager $manager, SingleApplicationSubApplicationsManager $singleApplicationSubApplicationsManager)
    {
        $this->manager = $manager;
        $this->subApplicationsManager = $singleApplicationSubApplicationsManager;
    }

    /**
     * Function to return lab examination modal view
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function renderLaboratoryExaminationView(Request $request)
    {
        $isEdit = ($request->input('edit') !== "false");

        $byProductIndicator = Laboratory::SELECT_INDICATOR_BY_PRODUCT_INDICATOR;
        $bySphere = Laboratory::SELECT_PRODUCT_BY_SPHERE;
        $customSend = Laboratory::SELECT_CUSTOM_SEND_TO_LAB;

        $rules = ['saf_number' => "required|exists:saf,regular_number"];
        if ($isEdit) {
            $rules['application_id'] = "required|exists:saf_sub_applications,id";
        } else {
            $rules['type'] = "required|integer_with_max|in:{$byProductIndicator},{$bySphere},{$customSend}";
        }

        //
        $validatedData = $request->validate($rules);

        $type = ($isEdit) ? $this->getLabApplicationType($validatedData['application_id']) : $validatedData['type'];
        $products = $this->manager->getHsCodesForLabExpertise($validatedData);
        $indicators = getReferenceRows(ReferenceTable::REFERENCE_LAB_HS_INDICATOR, false, false, ['id', 'name', 'code']);

        $labs = Agency::select('id', 'tax_id', 'name')->isLab()->joinMl()->get();

        $data = ($isEdit) ? $this->getLabApplicationData($validatedData['application_id']) : [];

        $html = $this->renderCrudView($products, $indicators, $type, $labs, $data);

        return responseResult('OK', '', ['viewHtml' => $html]);
    }

    /**
     * Function to get lab type
     *
     * @param $applicationId
     * @return JsonResponse
     */
    private function getLabApplicationType($applicationId)
    {
        $labApplication = SingleApplicationSubApplications::select('id', 'lab_type')->where('id', $applicationId)->first();

        return $labApplication->lab_type;
    }

    /**
     * Function to get lab type
     *
     * @param $applicationId
     * @return array
     */
    private function getLabApplicationData($applicationId)
    {
        $data = $selectedProducts = $selectedProductIds = [];

        $labApplication = SingleApplicationSubApplications::select('id', 'lab_type', 'saf_number', 'indicators', 'agency_id')
            ->where('id', $applicationId)
            ->with(['productList:id,product_code,sample_quantity,sample_measurement_unit,sample_number'])
            ->first();

        if (!is_null($labApplication->productList)) {
            foreach ($labApplication->productList as $product) {
                $selectedProductIds[] = $product->id;
                $selectedProducts[] = [
                    'product_id' => $product->id,
                    'sample_quantity' => $product->sample_quantity ?? '',
                    'sample_measurement_unit' => $product->sample_measurement_unit ?? '',
                    'sample_number' => $product->sample_number ?? ''
                ];
            }
        }

        $data['products'] = $selectedProducts;
        $data['indicators'] = $labApplication->indicators ?? [];
        $data['agency_id'] = $labApplication->agency_id;
        $data['labApplicationId'] = $labApplication->id;
        $data['available_indicators'] = [];
        $data['disabled'] = false;
        $data['editable'] = true;

        if ($labApplication->lab_type == Laboratory::SELECT_INDICATOR_BY_PRODUCT_INDICATOR) {

            $data['available_indicators'] = $this->manager->getIndicatorsByProduct(['product_id' => $selectedProductIds[0]]);
            $data['available_labs'] = $this->manager->getLabsByIndicators(['indicators' => $labApplication->indicators]);

        } elseif ($labApplication->lab_type == Laboratory::SELECT_PRODUCT_BY_SPHERE) {

            $labs = $this->manager->getLaboratoryExaminationGetLaboratories(['saf_number' => $labApplication->saf_number, 'product_ids' => $selectedProductIds])->pluck('laboratory_id')->all();
            $data['available_labs'] = Agency::select('id', 'tax_id', 'name')->whereIn('id', array_unique($labs))->joinMl()->get();
        }

        return $data;
    }

    /**
     * Function to get lab type
     *
     * @param $products
     * @param $indicators
     * @param $type
     * @param array $labs
     * @param array $data
     * @return mixed
     *
     * @throws Throwable
     */
    private function renderCrudView($products, $indicators, $type, $labs, $data = [])
    {
        return view('single-application.laboratories.lab-examination-crud')->with([
            'products' => $products,
            'indicators' => $indicators,
            'type' => $type,
            'labs' => $labs,
            'data' => $data,
            'labApplicationId' => $data['labApplicationId'] ?? null,
            'disabled' => $data['disabled'] ?? true,
            'editable' => $data['editable'] ?? false
        ])->render();
    }

    /**
     * Function to return indicators for hs code
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function renderLaboratoryExaminationGetLaboratories(Request $request)
    {
        $validatedData = $request->validate([
            'saf_number' => "required|exists:saf,regular_number",
            'product_ids' => "required|array",
            'product_id' => "required|exists:saf_products,id"
        ]);

        $labs = $this->manager->getLaboratoryExaminationGetLaboratories($validatedData);

        $labs = Agency::select('id', 'tax_id', 'name')->whereIn('id', array_unique($labs->pluck('laboratory_id')->all()))->joinMl()->get();

        $product = SingleApplicationProducts::find($validatedData['product_id']);

        return responseResult('OK', '', ['labs' => $labs, 'measurement_unit' => $product->measurement_unit ?? '']);
    }

    /**
     * Function to store lav examination
     *
     * @param GenerateSingleApplicationLabExaminationRequest $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function generateLaboratoryExaminationLaboratories(GenerateSingleApplicationLabExaminationRequest $request)
    {
        $validatedData = $request->validated();
        $byProductIndicator = Laboratory::SELECT_INDICATOR_BY_PRODUCT_INDICATOR;

        if (isset($validatedData['products']) && count($validatedData['products']) > 0) {
            $checkedProducts = [];
            $hasSameProduct = false;
            foreach ($validatedData['products'] as $product) {
                if (in_array($product['product_id'], $checkedProducts)) {
                    $hasSameProduct = true;
                    break;
                } else {
                    $checkedProducts[] = $product['product_id'];
                }
            }

            if ($hasSameProduct) {
                $errors['laboratory_or_document_not_valid'] = trans('swis.saf.lab_examination.choose_different_products');
                return responseResult('INVALID_DATA', '', '', $errors);
            }
        }

        $now = Carbon::now();
        $saf = SingleApplication::select('id', 'regular_number')->where('regular_number', $validatedData['saf_number'])->first();
        $lab = Agency::select('id', 'tax_id', 'certification_period_validity')->where('id', $validatedData['laboratory_id'])->first();

        $constructorDocument = ConstructorDocument::select('*')->where('company_tax_id', $lab->tax_id)->first();

        if (is_null($constructorDocument) || $now >= Carbon::parse($lab->certification_period_validity)) {
            $errors['laboratory_or_document_not_valid'] = trans('swis.saf.laboratory_or_document_not_valid');
            return responseResult('INVALID_DATA', '', '', $errors);
        }

        $referenceDocumentType = DB::table(ReferenceTable::REFERENCE_DOCUMENT_TYPE_REFERENCE)->select('document_type')->where('id', $constructorDocument->document_type_id)->first();
        $constructorState = ConstructorStates::select('id')->where(['constructor_document_id' => $constructorDocument->id, 'state_type' => ConstructorStates::START_STATE_TYPE])->active()->first();

        $subApplications = [
            'saf_number' => $saf->regular_number,
            'saf_id' => $saf->id,
            'company_tax_id' => optional($lab)->tax_id,
            'reference_document_type_code' => null,
            'reference_classificator_id' => $referenceDocumentType->document_type,
            'agency_id' => optional($lab)->id,
            'document_code' => optional($constructorDocument)->document_code,
            'constructor_document_id' => optional($constructorDocument)->id,
            'current_status' => SingleApplicationSubApplications::STATUS_FORM,
            'manual_created' => SingleApplication::FALSE,
            'products' => $validatedData['products'],
            'is_lab' => true,
            'lab_type' => $validatedData['type']
        ];
        if ($validatedData['type'] == $byProductIndicator) {
            $subApplications['indicators'] = $validatedData['indicators'];
        }

        $this->subApplicationsManager->store([$subApplications]);

        $laboratoriesView = view('single-application.laboratories.lab-examination-table')->with(['saf' => $saf])->render();

        return responseResult('OK', '', ['laboratoriesView' => $laboratoriesView]);
    }

    /**
     * Function to update lab application
     *
     * @param UpdateSingleApplicationLabExaminationRequest $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function updateLaboratoryExaminationLaboratories(UpdateSingleApplicationLabExaminationRequest $request)
    {
        $validatedData = $request->validated();
        $byProductIndicator = Laboratory::SELECT_INDICATOR_BY_PRODUCT_INDICATOR;
        $labApplication = SingleApplicationSubApplications::select('id', 'lab_type', 'saf_number', 'indicators', 'agency_id')->where('id', $request->input('lab_application_id'))->first();

        if (isset($validatedData['products']) && count($validatedData['products']) > 0) {
            $checkedProducts = [];
            $hasSameProduct = false;
            foreach ($validatedData['products'] as $product) {
                if (in_array($product['product_id'], $checkedProducts)) {
                    $hasSameProduct = true;
                    break;
                } else {
                    $checkedProducts[] = $product['product_id'];
                }
            }

            if ($hasSameProduct) {
                $errors['laboratory_or_document_not_valid'] = trans('swis.saf.lab_examination.choose_different_products');
                return responseResult('INVALID_DATA', '', '', $errors);
            }
        }

        $saf = SingleApplication::select('id', 'regular_number')->where('regular_number', $validatedData['saf_number'])->first();
        $lab = Agency::select('id', 'tax_id')->where('id', $validatedData['laboratory_id'])->first();

        $subApplication = [
            'saf_number' => $saf->regular_number,
            'saf_id' => $saf->id,
            'company_tax_id' => optional($lab)->tax_id,
            'agency_id' => optional($lab)->id,
            'current_status' => SingleApplicationSubApplications::STATUS_FORM,
            'manual_created' => SingleApplication::FALSE,
            'products' => $validatedData['products'],
            'is_lab' => true,
            'lab_type' => $labApplication->lab_type
        ];

        if ($labApplication->lab_type == $byProductIndicator) {
            $subApplication['indicators'] = $validatedData['indicators'];
        }

        $this->subApplicationsManager->labApplicationUpdate($validatedData, $subApplication, $validatedData['lab_application_id']);

        $laboratoriesView = view('single-application.laboratories.lab-examination-table')->with(['saf' => $saf])->render();

        return responseResult('OK', '', ['laboratoriesView' => $laboratoriesView]);
    }

    /**
     * Function to return indicators for this product
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getProductIndicators(Request $request)
    {
        $validatedData = $request->validate([
            'product_id' => "required|integer_with_max|exists:saf_products,id",
        ]);

        $indicators = $this->manager->getIndicatorsByProduct($validatedData);
        $product = SingleApplicationProducts::find($validatedData['product_id']);

        return responseResult('OK', '', ['indicators' => $indicators, 'measurement_unit' => $product->measurement_unit ?? '']);
    }

    /**
     * Function to get labs by indicators
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getLaboratoriesByIndicators(Request $request)
    {
        $validatedData = $request->validate([
            'indicators' => "required|array"
        ]);

        $labs = $this->manager->getLabsByIndicators($validatedData);

        return responseResult('OK', '', ['labs' => $labs]);
    }

    /**
     * Function to delete laboratory application
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteLabExamination(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required|integer_with_max|exists:saf_sub_applications,id'
        ]);

        SingleApplication::where('regular_number', $request->header('sNumber'))->safOwnerOrCreator()->firstOrFail();

        $subApplication = SingleApplicationSubApplications::select('id')->where('id', $validatedData['id'])->notFormedSubApplication()->first();

        $status = 'INVALID_DATA';
        if (!is_null($subApplication)) {
            $subApplication->delete();
            $status = 'OK';
        }

        return responseResult($status, '', '');
    }

    /**
     * Function to send application to lab
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function sendToLab(Request $request)
    {
        $validatedData = $request->validate([
            'sub_application_id' => "required|integer_with_max|exists:saf_sub_applications,id"
        ]);

        $subApplicationId = $validatedData['sub_application_id'];
        $safNumber = $request->header('sNumber');

        $saf = SingleApplication::select('id', 'regular_number', 'data', 'regime')->where('regular_number', $safNumber)->safOwnerOrCreator()->firstOrFail();
        $subApplication = SingleApplicationSubApplications::find($subApplicationId);

        if (Auth::user()->isAgency()) {
            $currentAgencyAllowSendToOtherAgencies = Agency::where('id', Auth::user()->getCurrentUserRole()->agency_id)->pluck('allow_send_sub_applications_other_agency')->first();

            if (!$currentAgencyAllowSendToOtherAgencies && Auth::user()->getCurrentUserRole()->agency_id != $subApplication->agency_id) {
                $errors['valid_message'] = true;
                $errors['disable_send'] = true;
                $errors['message'] = trans('swis.saf.sub_application.auth.user.cant_send_to_agency.message');

                return responseResult('INVALID_DATA', '', '', $errors);
            }
        }

        $agency = Agency::select('tax_id')->where('id', $subApplication->agency_id)->first();

        $constructorDocument = ConstructorDocument::select('id')->where(['company_tax_id' => $agency->tax_id, 'is_lab_flow' => true])->first();

        if (is_null($constructorDocument)) {
            $errors['valid_message'] = true;
            $errors['disable_send'] = true;
            $errors['message'] = trans('swis.saf.sub_application.auth.user.cant_send_to_agency.message');

            return responseResult('INVALID_DATA', '', '', $errors);
        }

        if (!is_null($constructorDocument)) {
            $constructorState = ConstructorStates::select('id')->where(['constructor_document_id' => $constructorDocument->id, 'state_type' => ConstructorStates::START_STATE_TYPE])->active()->first();

            if (!is_null($constructorState) && $subApplication->current_status == SingleApplicationSubApplications::STATUS_FORM || $subApplication->current_status == SingleApplicationSubApplications::STATUS_REQUESTED) {

                // Add SubApplication next possible number
                if ($subApplication->current_status == SingleApplicationSubApplications::STATUS_FORM) {
                    $subApplicationNumber = $this->subApplicationsManager->returnUniqueDocumentNumber($safNumber);
                } else {
                    $subApplicationNumber = $subApplication->document_number;
                }

                // Save sub application data on data json for my application
                $safData = $saf->data;

                $subApplicationProducts = SingleApplicationSubApplicationProducts::select('saf_products.*')
                    ->join('saf_products', 'saf_products.id', '=', 'saf_sub_application_products.product_id')
                    ->where('saf_sub_application_products.subapplication_id', $subApplicationId)
                    ->get();

                $safData['saf']['product']['brutto_weight'] = $subApplicationProducts->sum('brutto_weight');
                $safData['saf']['product']['netto_weight'] = $subApplicationProducts->sum('netto_weight');
                $safData['saf']['product']['product_count'] = count($subApplicationProducts);
                $safData['saf']['product']['total_value'] = $subApplicationProducts->sum('total_value_national');
                $safData['saf']['product']['total_value_all'] = $subApplicationProducts->sum('total_value');

                $safData['saf']['general']['subapplication_submitting_date'] = currentDateTime()->format(config('swis.date_format'));
                $safData['saf']['general']['sub_application_number'] = $subApplicationNumber;

                $updatedData = ['data' => $safData];
                if ($subApplication->current_status != SingleApplicationSubApplications::STATUS_REQUESTED) {
                    $updatedData['status_date'] = currentDate();
                }

                $subApplicationUpdateData = [
                    'current_status' => SingleApplicationSubApplications::STATUS_SUBMITTED,
                    'sender_user_id' => Auth::id(),
                    'document_number' => $subApplicationNumber,
                    'send_lab_company_tax_id' => $agency->tax_id
                ];

                $subApplicationUpdateData = array_merge($subApplicationUpdateData, $updatedData);

                // Update SubApplication
                $subApplication->update($subApplicationUpdateData);

                // Sub Application States
                $subAppStatesData = [];
                if ($subApplication->current_status != SingleApplicationSubApplications::STATUS_REQUESTED) {
                    $subAppStatesData['status_date'] = currentDateTime();
                }

                $subAppStatesDataCheck = [
                    'user_id' => Auth::id(),
                    'saf_number' => $safNumber,
                    'saf_id' => $saf->id,
                    'saf_subapplication_id' => $subApplicationId,
                    'state_id' => $constructorState->id,
                    'is_current' => '1'
                ];

                $subAppStatesData = array_merge($subAppStatesData, $subAppStatesDataCheck);

                SingleApplicationSubApplicationStates::where(['saf_number' => $safNumber, 'saf_subapplication_id' => $subApplicationId])->update(['is_current' => '0']);

                SingleApplicationSubApplicationStates::updateOrCreate($subAppStatesDataCheck, $subAppStatesData);

                // update All data
                $subApplication = SingleApplicationSubApplications::select('id', 'saf_number', 'data', 'custom_data', 'routings', 'routing_products', 'constructor_document_id')->with(['saf:id,saf_controlled_fields,regular_number', 'productList'])->where('id', $subApplicationId)->first();

                $singleApplicationSubApplicationCompleteData = new SingleApplicationSubApplicationCompleteData();
                $singleApplicationSubApplicationCompleteData->getParams();
                $singleApplicationSubApplicationCompleteData->setApplicationVariable($subApplication);

                list($safData, $productData, $productBatches) = $singleApplicationSubApplicationCompleteData->setData();
                $subApplication->all_data = json_encode(['data' => $safData, 'products' => array_values($productData), 'batches' => array_values($productBatches)]);
                $subApplication->save();

                // ========================== Create Sub Application Created Notes Start =========================== //

                $subApplicationNotes = [
                    'saf_number' => $safNumber,
                    'sub_application_id' => $subApplicationId,
                    'user_id' => Auth::id(),
                ];

                SingleApplicationNotes::storeNotes($subApplicationNotes, SingleApplicationNotes::NOTE_TRANS_KEY_CREATED_SUB_APPLICATION);

                // ========================== Create Sub Application Created Notes End =========================== //

            }
        }

        return responseResult('OK', urlWithLng("single-application/edit/{$saf->regime}/{$safNumber}#tab-lab_examination"));
    }

    /**
     * Function to return product view
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function getProductsView(Request $request)
    {
        $validatedData = $request->validate([
            'selectedProducts' => "required|array",
            'saf_number' => "required|exists:saf,regular_number",
            'type' => "required|in:add,remove"
        ]);

        $products = $this->manager->getHsCodesForLabExpertise($validatedData);
        $measurementUnits = getReferenceRows(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, false, false, ['id', 'code', 'name']);

        if (count($validatedData['selectedProducts']) < count($products)) {
            $productsView = view('single-application.laboratories.lab-examination-crud-products')->with([
                'products' => $products,
                'measurementUnits' => $measurementUnits,
                'data' => [],
                'selectedProducts' => $validatedData['selectedProducts'],
                'action' => $validatedData['type'],
                'single' => false,
                'addNew' => true
            ])->render();
            return responseResult('OK', '', ['productsView' => $productsView]);
        }
        return responseResult('INVALID_DATA', '', '', []);
    }

    /**
     * Function to return lab result
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function getLabResult(Request $request)
    {
        $validatedData = $request->validate([
            'sub_application_id' => "required|integer_with_max|exists:saf_sub_applications,id"
        ]);

        $subApplication = SingleApplicationSubApplications::find($validatedData['sub_application_id']);

        $result = view('user-documents.laboratories.lab-examination-for-laboratory')->with(['subApplication' => $subApplication, 'view' => true])->render();

        return responseResult('OK', '', ['result' => $result]);
    }
}
