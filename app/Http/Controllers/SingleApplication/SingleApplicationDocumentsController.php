<?php

namespace App\Http\Controllers\SingleApplication;

use App\Exceptions\TaxServiceTjCompanyInactiveException;
use App\Exceptions\TaxServiceTjInnNotFoundException;
use App\Exceptions\TaxServiceTjInvalidResponseException;
use App\Http\Controllers\BaseController;
use App\Http\Requests\SingleApplication\DocumentStoreRequest;
use App\Models\Agency\Agency;
use App\Models\Document\Document;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationDocuments\SingleApplicationDocuments;
use App\Models\SingleApplicationDocuments\SingleApplicationDocumentsManager;
use App\Models\SingleApplicationDocumentSubApplications\SingleApplicationDocumentSubApplications;
use App\Models\SingleApplicationProducts\SingleApplicationProducts;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use App\Rules\ValidateDelimited;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Class SingleApplicationDocumentsController
 * @package App\Http\Controllers\SingleApplication
 */
class SingleApplicationDocumentsController extends BaseController
{
    /**
     * @var SingleApplicationDocumentsManager
     */
    protected $manager;

    /**
     * SingleApplicationDocumentsController constructor.
     *
     * @param SingleApplicationDocumentsManager $manager
     */
    public function __construct(SingleApplicationDocumentsManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Function to Render documents inputs for creating document
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function renderDocumentInputs(Request $request)
    {
        $this->validate($request, [
            'num' => 'required|integer_with_max',
        ]);

        $data['html'] = view('single-application.documents.document-table-tr')->with(['renderInput' => true, 'num' => $request->get('num'), 'addFromDocList' => false])->render();

        return responseResult('OK', '', $data);
    }

    /**
     * Function to save SAF document to folder
     *
     * @param DocumentStoreRequest $request
     * @return JsonResponse
     *
     * @throws TaxServiceTjCompanyInactiveException
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function storeDocument(DocumentStoreRequest $request)
    {
        $dataValidated = $request->validated();

        $safNumber = $request->header('sNumber');
        SingleApplication::where('regular_number', $safNumber)->safOwnerOrCreator()->firstOrFail();

        $dataValidated['document_status'] = Document::DOCUMENT_STATUS_REGISTERED;

        // Store Document
        $newDocument = $this->manager->storeDocumentsFromSaf($dataValidated, $safNumber);

        // Need To Reform Saf because saf data changed
        SingleApplication::needReForm($safNumber);

        $data['docId'] = $newDocument->id;
        $data['accessPassword'] = $newDocument->document_access_password;
        $data['documentForm'] = trans('swis.document.document_form.' . $newDocument->document_form . '.title');
        $data['docStatus'] = Document::DOCUMENT_STATUS_REGISTERED;

        $data['documentFile'] = $newDocument->filePath();
        $data['documentFileName'] = $newDocument->fileBaseName();

        return responseResult('OK', '', $data);
    }

    /**
     * Function to add new Document to SAF
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function addDocument(Request $request)
    {
        $this->validate($request, [
            'documentItems' => 'array',
            'documentItems.*' => 'integer_with_max|exists:document,id',
            'num' => 'required|integer_with_max'
        ]);

        $documentItems = $request->get('documentItems');
        $safNumber = $request->header('sNumber');
        $num = $request->get('num');

        SingleApplication::where('regular_number', $safNumber)->safOwnerOrCreator()->firstOrFail();

        $safDocs = [];
        $newDocumentIds = [];
        $existDocuments = [];
        foreach ($documentItems as $documentIncrementedId => $documentId) {

            $docData = [
                'saf_number' => $safNumber,
                'document_id' => $documentId,
                'added_from_module' => SingleApplicationDocuments::ADDED_FROM_MODULE_SAF
            ];

            $documentExist = SingleApplicationDocuments::where($docData)->first();

            if (is_null($documentExist)) {
                $newDocumentIds[] = $documentId;

                $docData['state_id'] = $request->input('stateId');

                $safDocs[] = $docData;
            } else {
                $existDocuments[] = $documentId;
                $existDocumentsIncrementIds[] = $documentIncrementedId;
            }
        }

        // Store Document Products
        $this->manager->storeDocumentProducts($safDocs, $safNumber);

        // Need To Reform Saf because saf data changed
        SingleApplication::needReForm($safNumber);

        $documents = Document::whereIn('id', $newDocumentIds)->get();

        if (count($existDocuments)) {
            $data['error'] = [
                'docIds' => $existDocuments,
                'message' => implode(',', $existDocumentsIncrementIds) . ' ' . trans('swis.documents.already_exist.info')
            ];
        }

        $data['newDocumentCount'] = count($newDocumentIds);
        $data['html'] = view('single-application.documents.document-table-tr')->with(['attachedDocumentProducts' => $documents, 'num' => $num, 'addFromDocList' => true, 'saveMode' => 'add'])->render();

        return responseResult('OK', '', $data);
    }

    /**
     * Function to render document products
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function renderDocumentProducts(Request $request)
    {
        $this->validate($request, [
            'documentId' => 'nullable|integer_with_max|exists:document,id',
        ]);

        $safNumber = $request->header('sNumber');

        $saf = SingleApplication::select('id', 'regular_number')->where('regular_number', $safNumber)->safOwnerOrCreator()->firstOrFail();
        $products = SingleApplicationProducts::select(['id', 'saf_number', 'product_number', 'product_code', 'product_description', 'commercial_description'])->where('saf_number', $saf->regular_number)->orderByAsc()->get();

        $selectedProducts = $this->manager->getSafDocumentProductIds($safNumber, $request->input('documentId'));
        $productsIdNumbers = $saf->productsOrderedList();

        $data['html'] = view('single-application.documents.document-add-products-table')->with(['products' => $products, 'productsIdNumbers' => $productsIdNumbers, 'selectedProducts' => $selectedProducts])->render();
        $data['productsCount'] = $products->count();

        return responseResult('OK', '', $data);
    }

    /**
     * Function to update products in document
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function updateDocumentProducts(Request $request)
    {
        $this->validate($request, [
            'documentId' => 'required|integer_with_max|exists:document,id',
            'products' => ['nullable', 'string', new ValidateDelimited(ValidateDelimited::VALIDATION_TYPE_SAF_PRODUCTS, request()->header('sNumber'))]
        ]);

        $safNumber = $request->header('sNumber');

        SingleApplication::where('regular_number', $safNumber)->safOwnerOrCreator()->firstOrFail();

        // Need To Reform Saf because saf data changed
        SingleApplication::needReForm($safNumber);

        $data[] = [
            'document_id' => $request->input('documentId'),
            'products' => $request->input('products'),
            'added_from_module' => SingleApplicationDocuments::ADDED_FROM_MODULE_SAF
        ];

        // Store Document Products
        $this->manager->storeDocumentProducts($data, $safNumber);

        return responseResult('OK');
    }

    /**
     * Function to Render Sub Application table for Documents
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function renderDocumentSubApplications(Request $request)
    {
        $this->validate($request, [
            'documentId' => 'nullable|integer_with_max|exists:document,id'
        ]);

        $saf = SingleApplication::select('id', 'regular_number')->where('regular_number', $request->header('sNumber'))->safOwnerOrCreator()->firstOrFail();

        $subApplicationsInfo = SingleApplicationSubApplications::select('id', 'reference_classificator_id', 'agency_id', 'agency_subdivision_id', 'document_number', 'current_status')
            ->where('saf_number', $saf->regular_number)
            ->orderByRaw('document_number ASC NULLS last')
            ->orderByAsc()
            ->get();

        $refClassificators = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS);
        $refSubDivision = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS);

        $allFormedSubApplications = SingleApplicationSubApplications::where('saf_number', $saf->regular_number)->formedSubApplication()->pluck('id')->toArray();
        $safDocumentId = SingleApplicationDocuments::where(['saf_number' => $saf->regular_number, 'document_id' => $request->input('documentId')])->pluck('id')->first();
        $documentSubApplications = SingleApplicationDocumentSubApplications::where('saf_document_id', $safDocumentId)->pluck('sub_application_id')->toArray();

        $subApplications = [];
        $incrementNumber = 0;
        foreach ($subApplicationsInfo as $subApplication) {
            $agency = Agency::where('id', $subApplication->agency_id)->select(DB::raw('COALESCE(short_name, name) as name'))->joinMl()->first();

            if (!is_null($subApplication->current_status)) {
                $incrementNumber++;
            }

            $subApplications[] = [
                'id' => $subApplication->id,
                'increment_number' => !is_null($subApplication->current_status) ? $incrementNumber : '',
                'agency_name' => !is_null($agency) ? $agency->name : '',
                'sub_division_name' => isset($refSubDivision[$subApplication->agency_subdivision_id]) ? $refSubDivision[$subApplication->agency_subdivision_id]['name'] : '',
                'sub_application_products' => $subApplication->productNumbers($saf->productsOrderedList()),
                'sub_application_name' => isset($refClassificators[$subApplication->reference_classificator_id]) ? $refClassificators[$subApplication->reference_classificator_id]['name'] : '',
                'sub_application_type' => isset($refClassificators[$subApplication->reference_classificator_id]) ? $refClassificators[$subApplication->reference_classificator_id]['code'] : '',
                'sub_application_number' => $subApplication->document_number
            ];
        }

        $data['html'] = view('single-application.documents.document-add-sub-applications-table')->with(['subApplications' => $subApplications, 'allFormedSubApplications' => $allFormedSubApplications, 'documentSubApplications' => $documentSubApplications, 'saveMode' => 'edit'])->render();
        $data['subApplicationsCount'] = count($subApplications);

        return responseResult('OK', '', $data);

    }

    /**
     * Function to update Sub Applications in document
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function updateDocumentSubApplications(Request $request)
    {
        $safNumber = $request->header('sNumber');

        $this->validate($request, [
            'documentId' => 'required|integer_with_max|exists:document,id',
            'subApplications' => ['nullable', 'string', new ValidateDelimited(ValidateDelimited::VALIDATION_TYPE_SAF_SUB_APPLICATIONS, $safNumber)]
        ]);

        SingleApplication::where('regular_number', $safNumber)->safOwnerOrCreator()->firstOrFail();

        $data[] = [
            'document_id' => $request->input('documentId'),
            'sub_applications' => $request->input('subApplications'),
            'added_from_module' => SingleApplicationDocuments::ADDED_FROM_MODULE_SAF
        ];

        // Update Document Sub applications
        $this->manager->storeDocumentSubApplications($data, $safNumber);

        return responseResult('OK', '');
    }

    /**
     * Function to Delete document from SAF
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteDocument(Request $request)
    {
        $this->validate($request, [
            'documentIds' => 'required|array',
            'documentIds.*' => 'integer_with_max|exists:document,id'
        ]);

        $safNumber = $request->header('sNumber');

        SingleApplication::where('regular_number', $safNumber)->safOwnerOrCreator()->firstOrFail();

        // Need To Reform Saf because saf data changed
        SingleApplication::needReForm($safNumber);

        $this->manager->deleteDocumentsFromSaf($request->input('documentIds'), $safNumber);

        // TODO check
        /*if($singleAppDocument->canDeleteDocument()){

        }*/

        return responseResult('OK');
    }

}