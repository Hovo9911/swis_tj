<?php

namespace App\Http\Controllers\SingleApplication;

use App\Http\Controllers\BaseController;
use App\Http\Requests\SingleApplication\SingleApplicationSendSubApplicationRequest;
use App\Http\Requests\SingleApplication\SingleApplicationSubApplicationRequest;
use App\Models\Agency\Agency;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\DataModel\DataModel;
use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm;
use App\Models\Notifications\NotificationsManager;
use App\Models\NotifySubApplicationExpirationDate\NotifySubApplicationExpirationDateManager;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\RegionalOffice\RegionalOffice;
use App\Models\Routing\Routing;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationNotes\SingleApplicationNotes;
use App\Models\SingleApplicationObligations\SingleApplicationObligations;
use App\Models\SingleApplicationObligations\SingleApplicationObligationsManager;
use App\Models\SingleApplicationProducts\SingleApplicationProducts;
use App\Models\SingleApplicationProducts\SingleApplicationProductsManager;
use App\Models\SingleApplicationSubApplicationDigitalSignature\SingleApplicationSubApplicationDigitalSignature;
use App\Models\SingleApplicationSubApplicationDigitalSignature\SingleApplicationSubApplicationDigitalSignatureManager;
use App\Models\SingleApplicationSubApplicationProducts\SingleApplicationSubApplicationProducts;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplicationsManager;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Throwable;

/**
 * Class SingleApplicationSubApplicationController
 * @package App\Http\Controllers\SingleApplication
 */
class SingleApplicationSubApplicationController extends BaseController
{
    /**
     * @var SingleApplicationSubApplicationsManager
     */
    protected $manager;

    /**
     * @var NotificationsManager
     */
    protected $notificationsManager;

    /**
     * @var SingleApplicationObligationsManager
     */
    protected $singleApplicationObligationsManager;

    /**
     * @var SingleApplicationSubApplicationDigitalSignatureManager
     */
    protected $singleApplicationSubApplicationDigitalSignatureManager;

    /**
     * @var SingleApplicationProductsManager
     */
    protected $productManager;

    /**
     * @var NotifySubApplicationExpirationDateManager
     */
    protected $notifySubApplicationExpirationDateManager;

    /**
     * SingleApplicationSubApplicationController constructor.
     *
     * @param SingleApplicationSubApplicationsManager $manager
     * @param NotificationsManager $notificationsManager
     * @param SingleApplicationObligationsManager $singleApplicationObligationsManager
     * @param SingleApplicationSubApplicationDigitalSignatureManager $singleApplicationSubApplicationDigitalSignatureManager
     * @param SingleApplicationProductsManager $productManager
     * @param NotifySubApplicationExpirationDateManager $notifySubApplicationExpirationDateManager
     */
    public function __construct(SingleApplicationSubApplicationsManager $manager, NotificationsManager $notificationsManager, SingleApplicationObligationsManager $singleApplicationObligationsManager, SingleApplicationProductsManager $productManager, NotifySubApplicationExpirationDateManager $notifySubApplicationExpirationDateManager,SingleApplicationSubApplicationDigitalSignatureManager $singleApplicationSubApplicationDigitalSignatureManager)
    {
        $this->manager = $manager;
        $this->notificationsManager = $notificationsManager;
        $this->singleApplicationObligationsManager = $singleApplicationObligationsManager;
        $this->singleApplicationSubApplicationDigitalSignatureManager = $singleApplicationSubApplicationDigitalSignatureManager;
        $this->productManager = $productManager;
        $this->notifySubApplicationExpirationDateManager = $notifySubApplicationExpirationDateManager;
    }

    /**
     * Function to Render documents inputs for creating document
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function renderSubApplicationInputs(Request $request)
    {
        $saf = SingleApplication::select('id', 'regular_number', 'regime')->safOwnerOrCreator()->where('regular_number', $request->header('sNumber'))->firstOrFail();

        $agenciesAndConstructorDocs = $this->manager->manualRoutingsAvailableAgenciesAndConstructorDocuments($saf->regime);

        $data['html'] = view('single-application.sub-applications.sub-application-table-tr', ['constructorDocuments' => $agenciesAndConstructorDocs['constructorDocuments'], 'agencies' => $agenciesAndConstructorDocs['agencies'], 'renderInputs' => true])->render();

        return responseResult('OK', '', $data);
    }

    /**
     * Function to get Constructor Documents(sub application types) with Agencies
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getSubApplicationConstructorDocuments(Request $request)
    {
        $saf = SingleApplication::select('id', 'regular_number', 'regime')->safOwnerOrCreator()->where('regular_number', $request->header('sNumber'))->firstOrFail();

        $agenciesAndConstructorDocs = $this->manager->manualRoutingsAvailableAgenciesAndConstructorDocuments($saf->regime);

        $data = [
            'agencies' => $agenciesAndConstructorDocs['agencies']->toArray(),
            'subApplicationTypes' => $agenciesAndConstructorDocs['constructorDocuments']->toArray(),
        ];

        return responseResult('OK', '', $data);
    }

    /**
     * Function to get constructor all document info for sub application
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getSubApplicationAgency(Request $request)
    {
        $this->validate($request, [
            'sub_application_type' => 'nullable|integer_with_max|exists:constructor_documents,id'
        ]);

        $saf = SingleApplication::select('id', 'regular_number', 'regime')->safOwnerOrCreator()->where('regular_number', $request->header('sNumber'))->firstOrFail();

        $subApplicationAgencies = $this->manager->getAgenciesByConstructorType($request->get('sub_application_type'), $saf->regime);

        $data = [
            'agenciesIds' => $subApplicationAgencies['agenciesIds'],
            'agencies' => $subApplicationAgencies['agencies'],
            'subApplicationName' => $subApplicationAgencies['subApplicationName'],
        ];

        return responseResult('OK', '', $data);
    }

    /**
     * Function to get subApplication types by agency for front select
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getSubApplicationType(Request $request)
    {
        $this->validate($request, [
            'agency_id' => 'nullable|integer_with_max|exists:agency,id',
            'sub_application_type' => 'nullable|integer_with_max'
        ]);

        $saf = SingleApplication::select('id', 'regular_number', 'regime')->safOwnerOrCreator()->where('regular_number', $request->header('sNumber'))->firstOrFail();

        $data = [
            'subApplicationTypes' => $this->manager->getConstructorDocumentsByAgency($request->get('agency_id'), $request->get('sub_application_type'), $saf->regime),
        ];

        return responseResult('OK', '', $data);
    }

    /**
     * Function to render sub application products modal
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function renderSubApplicationProducts(Request $request)
    {
        $subApplicationId = $request->header('applicationId');

        $productColumns = ['id', 'product_number', 'product_code', 'product_description', 'commercial_description'];

        $saf = SingleApplication::select('id', 'regular_number')->where('regular_number', $request->header('sNumber'));

        if (!$subApplicationId) {
            $saf->safOwnerOrCreator();
        }

        $saf = $saf->firstOrFail();

        if (is_null($subApplicationId)) {

            $products = SingleApplicationProducts::select($productColumns)->where('saf_number', $saf->regular_number)->orderByAsc()->get();

            $productsIdNumbers = $saf->productsOrderedList();

        } else {

            $subApplication = SingleApplicationSubApplications::where('id', $subApplicationId)->where('saf_number', $saf->regular_number)->firstOrFail();
            $subApplicationProductIds = SingleApplicationSubApplicationProducts::where('subapplication_id', $subApplicationId)->pluck('product_id');
            $products = SingleApplicationProducts::select($productColumns)->where('saf_number', $saf->regular_number)->whereIn('id', $subApplicationProductIds)->orderByAsc()->get();

            $productsIdNumbers = $subApplication->productsOrderedList();
        }

        $data['html'] = view('single-application.sub-applications.sub-application-add-products-table')->with(['products' => $products, 'productsIdNumbers' => $productsIdNumbers])->render();

        return responseResult('OK', '', $data);

    }

    /**
     * Function to store Sub application
     *
     * @param SingleApplicationSubApplicationRequest $singleApplicationSubApplicationRequest
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function storeManualSubApplication(SingleApplicationSubApplicationRequest $singleApplicationSubApplicationRequest)
    {
        $dataValidated = $singleApplicationSubApplicationRequest->validated();
        $safNumber = $singleApplicationSubApplicationRequest->header('sNumber');
        $products = explode(',', $dataValidated['sub_application_products']);

        $saf = SingleApplication::select('id', 'regular_number', 'regime')->where('regular_number', $safNumber)->safOwnerOrCreator()->firstOrFail();
        $constructorDocument = ConstructorDocument::select('id', 'document_type_id', 'company_tax_id', 'document_code')->where('id', $dataValidated['sub_application_type'])->first();

        $referenceDocumentType = getReferenceRows(ReferenceTable::REFERENCE_DOCUMENT_TYPE_REFERENCE, $constructorDocument->document_type_id, false, ['document_type']);
        $referenceClassificator = getReferenceRows(ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS, $referenceDocumentType->document_type);

        //
        $agency = Agency::select('id', 'tax_id', 'subdivision_mark_type', DB::raw('COALESCE(short_name, name) as name'))->joinML()->where(['id' => $dataValidated['agency_id'], 'tax_id' => $constructorDocument->company_tax_id])->active()->first();
        if (is_null($agency)) {

            $errors['agency_not_found_message'] = trans('swis.saf.send_subapplication.agency.not_found.message');

            return responseResult('INVALID_DATA', '', '', $errors);
        }

        // Get routings
        $routingIds = Routing::where(['agency_id' => $agency->id, 'constructor_document_id' => $constructorDocument->id, 'regime' => $saf->regime])->manualRouting()->pluck('id')->all();

        $routingProductIds = [];
        if (count($routingIds)) {
            foreach ($routingIds as $routingId) {
                $routingProductIds[$routingId] = $products;
            }
        }

        $subApplication = [
            'user_id' => Auth::id(),
            'user_company_tax_id' => Auth::user()->companyTaxId(),
            'saf_number' => $safNumber,
            'saf_id' => $saf->id,
            'company_tax_id' => $agency->tax_id,
            'reference_classificator_id' => optional($referenceClassificator)->id,
            'reference_document_type_code' => null,
            'agency_id' => $agency->id,
            'subtitle_type' => null,
            'document_code' => optional($constructorDocument)->document_code,
            'constructor_document_id' => optional($constructorDocument)->id,
            'name' => '',
            'current_status' => SingleApplicationSubApplications::STATUS_FORM,
            'document_number' => null,
            'status_date' => null,
            'manual_created' => SingleApplication::TRUE,
            'routings' => $routingIds ?: null,
            'routing_products' => $routingProductIds ?: null,
            'pdf_hash_key' => str_random(64),
        ];

        $createdSubApplicationId = $this->manager->storeManual($subApplication, $products);

        $productNumbers = [];
        $safProducts = $saf->productsOrderedList();
        foreach ($products as $product) {
            $productNumbers[] = $safProducts[$product];
        }

        $productNumbers = implode(',', $productNumbers);

        // Check What Documents need to attach
        $routings = Routing::where(['agency_id' => optional($agency)->id, 'constructor_document_id' => $constructorDocument->id, 'regime' => $saf->regime])->manualRouting()->whereNotNull('ref_classificator_codes')->pluck('id')->all();

        $needAttachDocuments = $this->manager->checkSubApplicationRoutingsDocuments($safNumber, $routings, $createdSubApplicationId);
        $documentsNeedAttachMessage = '';

        if (count($needAttachDocuments['requiredDocs']) || count($needAttachDocuments['requiredDocsWithOr'])) {

            $subApplication = SingleApplicationSubApplications::select('id')->where('id', $createdSubApplicationId)->first();

            $subApplicationNeedAttachDocs = [
                'subAppName' => optional($referenceClassificator)->name,
                'subAppCode' => optional($referenceClassificator)->code,
                'documents' => '<ul>' . implode('', $needAttachDocuments['requiredDocs']) . '</ul>',
                'products' => $subApplication->productNumbers($saf->productsOrderedList())
            ];

            $documentsNeedAttachMessage = trans('swis.saf.documents_need_to_attach_to_products.message', $subApplicationNeedAttachDocs);

            if (count($needAttachDocuments['requiredDocsWithOr'])) {
                foreach ($needAttachDocuments['requiredDocsWithOr'] as $documentsWithOr) {
                    $documentsNeedAttachMessage .= '<br />';
                    $documentsNeedAttachMessage .= trans('swis.saf.documents_with_or_need_to_attach_to_products.message', ['documents' => implode('', $documentsWithOr), 'products' => $subApplication->productNumbers($saf->productsOrderedList())]);
                }
            }
        }

        //
        $agencySubDivisions = collect();
        if ($agency->subdivision_mark_type != Agency::SUBDIVISION_MARK_TYPE_NOT_REQUIRED) {
            $agencySubDivisions = RegionalOffice::getRefSubdivisionsByConstructorDocument($constructorDocument->document_code);
        }

        //
        $queryExpressWhere = function ($query) use ($referenceClassificator) {
            return $query->where("document", $referenceClassificator->id ?? 0)->orWhereNull('document');
        };

        $refExpressControl = getReferenceRows(ReferenceTable::REFERENCE_EXPRESS_CONTROL, false, false, ['id', 'code', 'name'], 'get', false, false, $queryExpressWhere);

        //
        $subApplicationData = [
            'id' => $createdSubApplicationId,
            'agencyName' => $agency->name,
            'agencySubDivisions' => $agencySubDivisions,
            'refExpressControl' => $refExpressControl,
            'subApplicationName' => optional($referenceClassificator)->name,
            'subApplicationType' => optional($referenceClassificator)->code,
            'productNumbers' => $productNumbers,
            'pdf_hash_key' => $subApplication['pdf_hash_key']
        ];

        $data['html'] = view('single-application.sub-applications.sub-application-table-tr')->with(['subApplicationData' => $subApplicationData, 'renderInputs' => false])->render();

        $safControlledFields = Routing::select('id')->whereIn('id', $routingIds)->whereNotNull('saf_controlled_fields')->first();
        if (!is_null($safControlledFields)) {

            if ($documentsNeedAttachMessage) {
                Session::flash($safNumber . '_need_attach_docs', [$documentsNeedAttachMessage]);
            }

            $data['needRefresh'] = true;
            $data['refreshUrl'] = urlWithLng('single-application/edit/' . $saf->regime . '/' . $safNumber);
        } else {
            $data['needAttachDocMessage'] = $documentsNeedAttachMessage;
        }

        return responseResult('OK', '', $data);
    }

    /**
     * Function to delete MANUAL sub application
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteManualSubApplication(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer_with_max|exists:saf_sub_applications,id'
        ]);

        $safNumber = $request->header('sNumber');
        $subApplicationId = $request->input('id');

        SingleApplication::where('regular_number', $safNumber)->safOwnerOrCreator()->firstOrFail();

        $subApplication = SingleApplicationSubApplications::select('id')->where(['id' => $subApplicationId, 'saf_number' => $safNumber])->manualSubApplication()->notFormedSubApplication()->first();

        $status = 'INVALID_DATA';
        if (!is_null($subApplication)) {
            $status = 'OK';

            $this->manager->deleteManualSubApplication($subApplicationId, $safNumber);
        }

        return responseResult($status, '', '');
    }

    /**
     * Function to Update Sub Application status
     *
     * @param SingleApplicationSendSubApplicationRequest $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function sendSubApplication(SingleApplicationSendSubApplicationRequest $request)
    {
        $subApplicationId = $request->get('id');
        $expressControlId = $request->get('express_control_id');
        $agencySubdivisionId = $request->get('agency_subdivision_id');
        $needDigitalSignature = $request->get('need_digital_signature');
        $confirmed = $request->get('confirmed');
        $safNumber = $request->header('sNumber');

        $saf = SingleApplication::select('id', 'regular_number', 'data', 'regime')->where('regular_number', $safNumber)->safOwnerOrCreator()->firstOrFail();

        $subApplication = SingleApplicationSubApplications::where(['id' => $subApplicationId, 'saf_number' => $safNumber])->firstOrFail();

        // Auth user can't send subApplication to current Agency
        if (Auth::user()->isAgency()) {
            $currentAgencyAllowSendToOtherAgencies = Agency::where('id', Auth::user()->getCurrentUserRole()->agency_id)->pluck('allow_send_sub_applications_other_agency')->first();

            if (!$currentAgencyAllowSendToOtherAgencies && Auth::user()->getCurrentUserRole()->agency_id != $subApplication->agency_id) {
                $errors['valid_message'] = true;
                $errors['disable_send'] = true;
                $errors['message'] = trans('swis.saf.sub_application.auth.user.cant_send_to_agency.message');

                return responseResult('INVALID_DATA', '', '', $errors);
            }
        }

        // Check sub application is valid
        $isValidSubApplication = $this->checkSubApplicationIsValid($subApplicationId, $safNumber);

        if (!$isValidSubApplication) {

            $errors['valid_message'] = true;
            $errors['message'] = trans('swis.saf.sub_application.not_valid.message');

            return responseResult('INVALID_DATA', '', '', $errors);
        }

        // Saf controlled field validation
        if (!is_null($subApplication->routings)) {

            $safControlledNotValidFields = $saf->getNotValidSafControlledField($subApplication->id, true, true);

            if (count($safControlledNotValidFields['inSaf']) || count($safControlledNotValidFields['inProducts'])) {

                // If Has disabled product with saf_controlled_field (can't manage it)
                $subApplicationCantSendMessage = '';
                if ($saf->hasSubmittedSubApplication()) {
                    $subApplicationDisabledProducts = SingleApplicationSubApplications::subApplicationDisabledProducts($safNumber, $subApplication->id);
                    $hasNotValidFieldInDisabledProduct = array_intersect($safControlledNotValidFields['inProducts'], $subApplicationDisabledProducts);

                    if (count($safControlledNotValidFields['inSaf']) || count($hasNotValidFieldInDisabledProduct)) {
                        $subApplicationCantSendMessage = trans('swis.saf.sub_application.can_send.for_saf_controlled_fields.message');
                    }
                }

                $errors['not_valid_saf_controlled_fields'] = true;
                $errors['fields'] = $safControlledNotValidFields;
                $errors['not_valid_products_message'] = trans('swis.saf.sub_application.has_not_valid_products.message');
                $errors['sub_application_cant_send_message'] = $subApplicationCantSendMessage;

                return responseResult('INVALID_DATA', '', '', $errors);
            }

            // What Documents need to attach
            if ($subApplication->current_status == SingleApplicationSubApplications::STATUS_FORM) {

                $needAttachDocuments = $this->manager->checkSubApplicationRoutingsDocuments($safNumber, $subApplication->routings, $subApplicationId);

                if (count($needAttachDocuments['requiredDocs']) || count($needAttachDocuments['requiredDocsWithOr'])) {

                    $referenceClassificator = getReferenceRows(ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS, $subApplication->reference_classificator_id);

                    $subApplicationNeedAttachDocs = [
                        'subAppName' => optional($referenceClassificator)->name,
                        'subAppCode' => optional($referenceClassificator)->code,
                        'documents' => '<ul>' . implode('', $needAttachDocuments['requiredDocs']) . '</ul>',
                        'products' => $subApplication->productNumbers($saf->productsOrderedList())
                    ];

                    $documentsNeedAttachMessage = trans('swis.saf.documents_need_to_attach_to_products.message', $subApplicationNeedAttachDocs);

                    if (count($needAttachDocuments['requiredDocsWithOr'])) {
                        foreach ($needAttachDocuments['requiredDocsWithOr'] as $documentsWithOr) {
                            $documentsNeedAttachMessage .= '<br />';
                            $documentsNeedAttachMessage .= trans('swis.saf.documents_with_or_need_to_attach_to_products.message', ['documents' => '<ul>' . implode('', $documentsWithOr) . '</ul>', 'products' => $subApplication->productNumbers($saf->productsOrderedList())]);
                        }
                    }

                    $errors['valid_message'] = true;
                    $errors['message'] = $documentsNeedAttachMessage;

                    return responseResult('INVALID_DATA', '', '', $errors);
                }
            }
        } else {
            if ($subApplication->isGeneratedSubApplication()) {
                return Bugsnag::notifyError('sub-application-routing-error', 'SubApplication has not routings. SUB APP ID -> ' . $subApplication->id);
            }
        }

        // Digital Signature
        $agency = Agency::select('tax_id', 'allow_send_sub_applications_other_agency')->where('id', $subApplication->agency_id)->first();
        $constructorDocument = ConstructorDocument::select('id', 'digital_signature')->where(['document_code' => $subApplication->document_code, 'company_tax_id' => $agency->tax_id])->first();

        if (config('global.digital_signature') && $constructorDocument->digital_signature == ConstructorDocument::NEED_DIGITAL_SIGNATURE && $needDigitalSignature != 'false') {
            $errors['need_digital_signature'] = true;
            $errors['button'] = "#{$subApplicationId}";

            return responseResult('INVALID_DATA', '', '', $errors);
        }

        // Generate Obligation
        $safFixObligation = SingleApplicationObligations::where(['saf_number' => $safNumber, 'obligation_code' => SingleApplicationObligations::SAF_OBLIGATIONS_FIX_CODE_FOR_SEND_APPLICATION_OBLIGATION])->first();
        if (is_null($safFixObligation)) {

            $constructorState = ConstructorStates::select('id')->where(['constructor_document_id' => $subApplication->constructor_document_id, 'state_type' => ConstructorStates::START_STATE_TYPE])->active()->first();

            if (!is_null($constructorState)) {

                // First Pay for Saf
                $userBalance = Auth::user()->balance();
                $sum = SingleApplicationObligations::SAF_OBLIGATIONS_PRICE_FOR_SEND_APPLICATION_OBLIGATION;

                if (is_null($confirmed)) {

                    if (Auth::user()->isAgency()) {
                        $userBalance = 99999999;
                    }

                    if ($userBalance < $sum) {

                        $errors['message'] = trans('swis.saf.sub_application.send_application_not_enough');
                        $errors['valid_message'] = true;

                        return responseResult('INVALID_DATA', '', '', $errors);

                    } else {

                        $html = view('single-application.obligations.pay-sub-application-modal')->with([
                            'subApplicationId' => $subApplicationId,
                            'expressControlId' => $expressControlId,
                            'agencySubdivisionId' => $agencySubdivisionId,
                            'sum' => $sum,
                        ])->render();

                        return responseResult('OK', '', ['needConfirm' => true, 'html' => $html]);
                    }
                } else {
                    $this->singleApplicationObligationsManager->storeSubApplicationObligationAndTransaction($safNumber, $subApplicationId, $constructorState->id);
                }
            }
        }

        // Send SubApplication
        $this->manager->sendSubApplication($subApplication, $saf, $agencySubdivisionId, $expressControlId);

        return responseResult('OK', urlWithLng('single-application/edit/' . $saf->regime . '/' . $safNumber));
    }

    /**
     * Function to check Sub Application is valid or need to generate from scratch
     *
     * @param $subApplicationId
     * @param $safNumber
     * @return bool
     */
    public function checkSubApplicationIsValid($subApplicationId, $safNumber)
    {
        $saf = SingleApplication::select('regular_number', 'regime', 'need_reform')->where('regular_number', $safNumber)->firstOrFail();

        if ($saf->need_reform == SingleApplication::TRUE) {
            return false;
        }

        $subApplication = SingleApplicationSubApplications::select('agency_id', 'manual_created', 'document_code', 'routings', 'current_status')->where('id', $subApplicationId)->first();

        if (is_null($subApplication)) {
            return false;
        }

        $agency = Agency::select('id')->where('id', $subApplication->agency_id)->active()->first();

        if (is_null($agency)) {
            return false;
        }

        if ($subApplication->current_status == SingleApplicationSubApplications::STATUS_REQUESTED) {
            return true;
        }

        $subApplicationProductIds = SingleApplicationSubApplicationProducts::where('subapplication_id', $subApplicationId)->pluck('product_id')->all();

        $safProducts = SingleApplicationProducts::whereIn('id', $subApplicationProductIds)->count();

        // Check Products exist or not
        $isValid = true;
        if ($safProducts != count($subApplicationProductIds)) {
            $isValid = false;
        }

        // Constructor document exist and active
        $constructorDocument = ConstructorDocument::where('document_code', $subApplication->document_code)->active()->first();

        if (is_null($constructorDocument)) {
            $isValid = false;
        } else {

            if ($subApplication->manual_created != SingleApplication::TRUE) {

                // Routing Exist or Not active
                $constructorRouting = Routing::where(['regime' => $saf->regime, 'constructor_document_id' => $constructorDocument->id])->whereIn('id', $subApplication->routings)->active()->count();

                if (!$constructorRouting) {
                    $isValid = false;
                }
            }

        }

        return $isValid;
    }

    /**
     * Function to show Sub Application attached documents
     *
     * @param $lngCode
     * @param $safNumber
     * @return View
     */
    public function subApplicationDocs($lngCode, $safNumber)
    {
        $saf = SingleApplication::select('id', 'regular_number')->where('regular_number', $safNumber);

        $importer = $_GET['importer'] ?? '';
        $exporter = $_GET['exporter'] ?? '';
        $applicant = $_GET['applicant'] ?? '';

        $saf->where(function ($q) use ($importer, $exporter, $applicant) {
            $q->where('importer_value', $importer)
                ->orWhere('exporter_value', $exporter)
                ->orWhere('applicant_value', $applicant);
        });

        $saf = $saf->first();
        if (is_null($saf)) {
            return view('errors.saf-not-found');
        }

        //
        $safProductsNotFormedSubApplication = $saf->productsCodesNotFormedSubApplication();

        $subApplications = SingleApplicationSubApplications::select('id', 'reference_classificator_id', 'pdf_hash_key', 'saf_number', 'document_number', 'status_date', 'document_code', 'constructor_document_id', 'custom_data', 'auto_released')->formedSubApplication()->where('saf_number', $safNumber)->orderByDesc()->get();

        $refClassificators = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS);

        foreach ($subApplications as &$subApplication) {

            $constructDocumentId = $subApplication->constructor_document_id;
            if (!$constructDocumentId) {
                $constructDocumentId = ConstructorDocument::where('document_code', $subApplication->document_code)->pluck('id')->first();
            }

            $isApproved = false;
            if (!empty($subApplication->applicationCurrentState->state)) {
                $isApproved = ConstructorStates::checkEndStateStatus($subApplication->applicationCurrentState->state->id, ConstructorStates::END_STATE_APPROVED);
            }

            $fieldIdOfPermissionDate = DocumentsDatasetFromMdm::where('document_id', $constructDocumentId)->where('mdm_field_id', DataModel::getMdmFieldIDByConstant(DataModel::PERMISSION_DATE_MDM_ID))->pluck('field_id')->first();

            $subApplication->name = isset($refClassificators[$subApplication->reference_classificator_id]) ? $refClassificators[$subApplication->reference_classificator_id]['name'] : '';
            $subApplication->type = isset($refClassificators[$subApplication->reference_classificator_id]) ? $refClassificators[$subApplication->reference_classificator_id]['code'] : '';
            $subApplication->state = !empty($subApplication->applicationCurrentState->state) && !empty($subApplication->applicationCurrentState->state->current()) ? $subApplication->applicationCurrentState->state->current()->state_name : '';
            $subApplication->current_state_date = !is_null($fieldIdOfPermissionDate) && !empty($subApplication->custom_data[$fieldIdOfPermissionDate]) ? $subApplication->custom_data[$fieldIdOfPermissionDate] : '';
            $subApplication->isApproved = $isApproved;
        }

        return view('single-application.sub-applications.sub-application-docs', compact('safNumber', 'subApplications', 'refClassificators', 'safProductsNotFormedSubApplication'));
    }

    /**
     * Function to check application digital signature
     *
     * @param $lngCode
     * @param Request $request
     * @return JsonResponse
     */
    public function signDigitalSignature($lngCode, Request $request)
    {
        $applicationId = $request->get('subApplicationId') ?? '';
        $data['id'] = $request->get('digitalSignatureId') ?? '';
        $data['fileDigestHash'] = $request->get('fileDigestHash') ?? '';

        if (!empty($data['id']) && !empty($data['fileDigestHash'])) {

            $this->singleApplicationSubApplicationDigitalSignatureManager->update($data);

            $safNumber = SingleApplicationSubApplications::where('id', $applicationId)->pluck('saf_number')->first();
            $safSubApplicationDigitalSignature = SingleApplicationSubApplicationDigitalSignature::where('id', $data['id'])->firstOrFail();

            $noteData = [
                'saf_number' => $safNumber,
                'note' => '|SIGNATURE_' . $safSubApplicationDigitalSignature->id . '|',
                'trans_key' => SingleApplicationNotes::NOTE_TRANS_KEY_SIGNATURE_BTN,
                'sub_application_id' => $applicationId,
                'send_from_module' => SingleApplicationNotes::NOTE_SEND_MODULE_SAF
            ];

            SingleApplicationNotes::storeNotes($noteData);
            $errors['need_digital_signature'] = false;

            return responseResult('INVALID_DATA', '', '', $errors);
        }

        // my data storage location is project_root/storage/app/{fileName} file.
        $digitalSignatureDirPrefix = env('DIGITAL_SIGNATURE_FILE_PATH') . '/appplications/';
        $digitalSignatureDirSecondPart = date('Y') . '/' . date('m') . '/' . date('d') . '/';
        $digitalSignatureDir = $digitalSignatureDirPrefix . $digitalSignatureDirSecondPart;

        if (!is_dir($digitalSignatureDir)) {
            mkdir($digitalSignatureDir, 0775, true);
        }

        $digitalSignatureFileName = $applicationId . '_' . date('Y') . date('m') . date('d') . rand() . '.json';
        $digitalSignatureFile = $digitalSignatureDir . '/' . $digitalSignatureFileName;

//        $subApplicationAllData = SingleApplicationSubApplications::where('id', $applicationId)->pluck('all_data')->first();
        $subApplicationAllData = $this->manager->getSAFDataForSubApplication($applicationId);
        Storage::disk('digital_signature')->put($digitalSignatureFileName, json_encode($subApplicationAllData));

        $fileDigest = hash_file('gost-crypto', $digitalSignatureFile);

        $digitalSignatureData = [
            'subapplication_id' => $applicationId,
            'user_id' => Auth::user()->id,
            'company_tax_id' => Auth::user()->companyTaxId(),
            'file_path' => $digitalSignatureDirSecondPart . $digitalSignatureFileName,
            'file_digest' => $fileDigest
        ];

        $digitalSignatureId = $this->singleApplicationSubApplicationDigitalSignatureManager->store($digitalSignatureData);

        //
        $data['fileDigest'] = $fileDigest;
        $data['digitalSignatureId'] = $digitalSignatureId;

        return responseResult('OK', '', $data);
    }

    /**
     * Function to check digital signature
     *
     * @param $lngCode
     * @param Request $request
     * @return JsonResponse
     */
    public function checkDigitalSignature($lngCode, Request $request)
    {
        $this->validate($request, ['digitalSignatureId' => 'required|integer_with_max']);

        $digitalSignatureId = $request['digitalSignatureId'];

        $digitalSignature = SingleApplicationSubApplicationDigitalSignature::select('file_digest_hash', 'file_path')->where('id', $digitalSignatureId)->firstOrFail();

        $verificationFileStart = "-----BEGIN CMS-----\n";
        $verificationFileHash = optional($digitalSignature)->file_digest_hash;
        $verificationFileEnd = "\n-----END CMS-----";

        $verificationFileContent = $verificationFileStart . $verificationFileHash . $verificationFileEnd;

        $tmpDirectory = env('DIGITAL_SIGNATURE_FILE_TMP_PATH');
        $validationFileName = 'digitalSignature_' . rand() . '.pem';
        $validationFileFullName = $tmpDirectory . '/' . $validationFileName;

        $tmpFileName = '/tmpDigitalSignature_' . rand() . '.pem';
        $tmpFileFullName = $tmpDirectory . '/' . $tmpFileName;

        Storage::disk('tmp')->put($validationFileName, $verificationFileContent);
        Storage::disk('tmp')->put($tmpFileName, '');

        $cmd = "/usr/local/openssl/bin/openssl cms -verify -binary -in {$validationFileFullName} -inform PEM -CAfile /var/www/shared/content/ca-bundle.pem -crl_check_all -certsout {$tmpFileFullName}";

        exec(escapeshellcmd($cmd), $output, $returnCode);
        // if $returnCode === 0 command executed successful
        // else error
        // if $output == 'disgn of json file' then verified
        //

        join('', $output) . "\n\n";

        $cert = openssl_x509_parse(file_get_contents($tmpFileFullName));

        $cmd = "/usr/local/openssl/bin/openssl asn1parse -i -in {$validationFileFullName}";
        exec(escapeshellcmd($cmd), $output);
        $str = join('', $output);
        $str = substr($str, strpos($str, ':signingTime'));
        $str = substr($str, strpos($str, "UTCTIME"));
        $str = substr($str, 0, strpos($str, 'Z') + 1);
        $str = '20' . substr($str, strpos($str, ':') + 1);

        $data['command'] = $cert;
        $data['signing_time'] = date('d/m/Y H:i:s', strtotime($str));

        // Deleting files
        unlink($validationFileFullName);
        unlink($tmpFileFullName);

        return responseResult('OK', '', $data);
    }
}