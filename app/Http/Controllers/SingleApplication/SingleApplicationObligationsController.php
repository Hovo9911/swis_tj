<?php

namespace App\Http\Controllers\SingleApplication;

use App\Http\Controllers\BaseController;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationObligations\SingleApplicationObligations;
use App\Models\SingleApplicationObligations\SingleApplicationObligationsManager;
use App\Payments\PaymentsManager;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Throwable;

/**
 * Class SingleApplicationObligationsController
 * @package App\Http\Controllers\SingleApplication
 */
class SingleApplicationObligationsController extends BaseController
{
    /**
     * @var SingleApplicationObligationsManager
     */
    protected $manager;

    /**
     * SingleApplicationObligationsController constructor.
     *
     * @param SingleApplicationObligationsManager $manager
     */
    public function __construct(SingleApplicationObligationsManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Function to check and return modal body
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function payObligationsModal(Request $request)
    {
        $this->validate($request, [
            'data' => 'required',
            'regular_number' => 'required|exists:saf,regular_number',
        ]);

        $saf = SingleApplication::select('id', 'regime', 'regular_number')->where('regular_number', $request->input('regular_number'))->safOwnerOrCreator()->first();
        $obligationIds = json_decode($request->input('data'));
        $sum = SingleApplicationObligations::getObligationsSum('saf_obligations.obligation', null, $obligationIds);

        $userBalance = Auth::user()->balance();
        $params = ['pay' => true, 'sum' => $sum, 'obligationIds' => $obligationIds, 'params' => [], 'saf' => $saf];

        if ($userBalance >= $sum) {
            $data['html'] = view('single-application.obligations.pay-obligation-modal-content')->with($params)->render();
        } else {
            $params['params'] = PaymentsManager::createAlifPayment();
            $params['pay'] = 'refill';
            $data['html'] = view('single-application.obligations.pay-obligation-modal-content')->with($params)->render();
        }

        return responseResult('OK', '', $data);
    }

    /**
     * Function to pay for obligation and update user balance
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function payObligations(Request $request)
    {
        $this->validate($request, [
            'sum' => 'required',
            'obligations' => 'required',
            'regular_number' => 'required|exists:saf,regular_number',
        ]);

        $getSum = $request->input('sum');
        $obligationIds = $request->input('obligations');
        $userBalance = Auth::user()->balance();

        $saf = SingleApplication::select('id', 'regime', 'regular_number')->where('regular_number', $request->input('regular_number'))->safOwnerOrCreator()->first();
        $sum = SingleApplicationObligations::getObligationsSum('saf_obligations.obligation', null, $obligationIds);

        if ($getSum == $sum && $userBalance >= $sum) {
            $this->manager->paySafObligations($saf->regular_number, $sum, $obligationIds);

            return responseResult('OK', urlWithLng("/single-application/edit/{$saf->regime}/{$saf->regular_number}#tab-responsibilities"));
        } else {

            $errors['invalid_sum'] = trans('swis.saf.modal.invalid_sum.title');

            return responseResult('INVALID_DATA', '', '', $errors);
        }
    }

    /**
     * Function to delete free sum obligation before pay
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function deleteFreeSumObligation(Request $request)
    {
        $this->validate($request, [
            'obligationId' => 'required|exists:saf_obligations,id'
        ]);

        $obligation = SingleApplicationObligations::find($request->input('obligationId'));

        if (!is_null($obligation) && $obligation->obligation != $obligation->payment) {
            $obligation->delete();
        }

        return responseResult('OK');
    }

    /**
     * Function to store/update obligation receipt
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function storeObligationReceipt(Request $request)
    {
        if (!config('swis.obligation_receipt_mode')) {
            return responseResult('OK');
        }

        $this->validate($request, [
            'file_name' => 'required|string_with_max',
            'obligation_id' => 'required|integer_with_max|exists:saf_obligations,id'
        ]);

        $obligationId = $request->get('obligation_id');

        SingleApplication::select('id')->where('regular_number', $request->header('sNumber'))->safOwnerOrCreator()->firstOrFail();
        SingleApplicationObligations::where(['id' => $obligationId, 'saf_number' => $request->header('sNumber')])->firstOrFail();

        //
        $fileName = $request->get('file_name');

        // Store or Update receipt
        $this->manager->storeObligationReceipts([
            'file_name' => $fileName,
            'saf_obligation_id' => $obligationId
        ]);

        $data['fileUrl'] = url('files/uploads/files/' . $fileName);

        return responseResult('OK', '', $data);
    }

    /**
     * Function to delete obligation receipt
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteObligationReceipt(Request $request)
    {
        if (!config('swis.obligation_receipt_mode')) {
            return responseResult('OK');
        }

        $this->validate($request, [
            'obligation_id' => 'required|integer_with_max|exists:saf_obligations,id'
        ]);

        $obligationId = $request->get('obligation_id');

        SingleApplication::select('id')->where('regular_number', $request->header('sNumber'))->safOwnerOrCreator()->firstOrFail();
        $safObligation = SingleApplicationObligations::select('obligation', 'payment')->where(['id' => $obligationId, 'saf_number' => $request->header('sNumber')])->firstOrFail();

        //
        if ($safObligation->isObligationPayed()) {
            $data['message'] = trans('swis.saf.obliagtion.is_payed.cant_modify_info.message');

            return responseResult('INVALID_DATA', '', '', $data);
        }

        // Delete
        $this->manager->deleteObligationReceipts($obligationId);

        return responseResult('OK');
    }

}