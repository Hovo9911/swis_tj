<?php

namespace App\Http\Controllers\SingleApplication;

use App\Http\Controllers\BaseController;
use App\Http\Requests\SingleApplication\SingleApplicationProductsRequest;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\ReferenceTableForm\ReferenceTableFormManager;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure;
use App\Models\SingleApplicationProducts\SingleApplicationProducts;
use App\Models\SingleApplicationProducts\SingleApplicationProductsManager;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Throwable;

/**
 * Class SingleApplicationProductsController
 * @package App\Http\Controllers\SingleApplication
 */
class SingleApplicationProductsController extends BaseController
{
    /**
     * @var SingleApplicationProductsManager
     */
    protected $manager;

    /**
     * @var ReferenceTableFormManager
     */
    protected $referenceTableFormManager;

    /**
     * SingleApplicationProductsController constructor.
     *
     * @param SingleApplicationProductsManager $manager
     * @param ReferenceTableFormManager $referenceTableFormManager
     */
    public function __construct(SingleApplicationProductsManager $manager, ReferenceTableFormManager $referenceTableFormManager)
    {
        $this->manager = $manager;
        $this->referenceTableFormManager = $referenceTableFormManager;
    }

    /**
     * Function to clone saf product
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function cloneProduct(Request $request)
    {
        $dataValidated = $this->validate($request, [
            'id' => 'required|integer_with_max|exists:saf_products,id'
        ]);

        $safNumber = $request->header('sNumber');

        SingleApplication::select('id')->where('regular_number', $safNumber)->safOwnerOrCreator()->firstOrFail();

        // clone product
        DB::beginTransaction();

        $newProduct = $this->manager->cloneProduct($safNumber, $dataValidated['id']);

        $data = $this->productTableRender($safNumber, $newProduct->id);
        $data['productId'] = $newProduct->id;

        DB::commit();

        return responseResult('OK', '', $data);
    }

    /**
     * Function to store data
     *
     * @param SingleApplicationProductsRequest $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function productStore(SingleApplicationProductsRequest $request)
    {
        $data = $request->all();

        $data['saf_number'] = $request->header('sNumber');

        SingleApplication::select('id')->where('regular_number', $data['saf_number'])->safOwnerOrCreator()->firstOrFail();

        //
        $newProduct = $this->manager->store($data);

        // Need To Reform Saf because saf data changed
        SingleApplication::needReForm($data['saf_number']);

        return responseResult('OK', '', $this->productTableRender($data['saf_number'], $newProduct->id));
    }

    /**
     * Function to edit data
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function productEdit(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer_with_max|exists:saf_products,id',
        ]);

        $safNumber = $request->header('sNumber');
        $id = $request->get('id');

        $saf = SingleApplication::select('id', 'regular_number', 'regime', 'saf_data_structure_id', 'status_type')->where('regular_number', $safNumber)->safOwnerOrCreator()->firstOrFail();

        //
        $this->manager->updateProductNationalAmount((array)$id, $safNumber);

        //
        $product = SingleApplicationProducts::where('id', $id)->where('saf_number', $safNumber)->first();

        $simpleAppTabs = $saf->xmlDataStructure();
        $block = $simpleAppTabs->xpath('/tabs/tab[@key="' . SingleApplication::XML_MODULE_PRODUCTS . '"]/block[@type]');

        // From Routing get Saf Controlled Fields
        $safControlledFields = $saf->getSafProductControlledFields($id);

        if (isset($product->saf_controlled_fields)) {
            foreach ($product->saf_controlled_fields as $key => $value) {
                $product->{'saf_controlled_fields.' . $key} = $value;
            }
        }

        $data['html'] = view('single-application.products.products-edit-form')->with([
            'edit' => true,
            'block' => $block[0],
            'productsIdNumbers' => $saf->productsOrderedList(),
            'product' => $product,
            'onlyView' => ($request->input('viewType') === 'view'),
            'isDisabledProduct' => $product->isDisabled(),
            'safHiddenColumns' => SingleApplication::getSafHiddenElements(),
            'hasSubmittedSubApplication' => $saf->hasSubmittedSubApplication(),
            'safControlledFields' => $safControlledFields,
            'saf' => $saf,
        ])->render();

        $data['productsTotals'] = $this->manager->getSafProductsTotalValues($safNumber);

        return response()->json($data);
    }

    /**
     * Function to update data
     *
     * @param SingleApplicationProductsRequest $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function productUpdate(SingleApplicationProductsRequest $request)
    {
        $dataValidated = $this->validate($request, [
            'id' => 'required|integer_with_max|exists:saf_products,id',
        ]);

        $safNumber = $request->header('sNumber');

        $data = $request->all();
        $data['saf_number'] = $safNumber;

        SingleApplication::select('id')->where('regular_number', $safNumber)->safOwnerOrCreator()->firstOrFail();

        //
        $this->manager->update($data, $dataValidated['id']);

        // Need To Reform Saf because saf data changed
        SingleApplication::needReForm($safNumber);

        return responseResult('OK', '', $this->productTableRender($safNumber, $dataValidated['id']));
    }

    /**
     * Function to delete sub form row (product)
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function productDelete(Request $request)
    {
        $dataValidated = $this->validate($request, [
            'ids' => 'required|array',
            'ids.*' => 'integer_with_max|exists:saf_products,id',
        ]);

        $safNumber = $request->header('sNumber');

        //
        $saf = SingleApplication::select('regime', 'status_type', 'data')->where('regular_number', $safNumber)->safOwnerOrCreator()->firstOrFail();

        //
        $this->manager->deleteProducts($dataValidated['ids'], $safNumber);

        // Need To Reform Saf because saf data changed
        SingleApplication::needReForm($safNumber);

        return responseResult('OK', urlWithLng('single-application/edit/' . $saf->regime . '/' . $safNumber));
    }

    /**
     * Function to render table view
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function productBatchesInputRender(Request $request)
    {
        $dataValidated = $this->validate($request, [
            'incrementNumber' => 'nullable|integer_with_max',
            'num' => 'nullable|integer_with_max'
        ]);

        $simpleAppTabs = SingleApplicationDataStructure::lastXml();

        $block = $simpleAppTabs->xpath('/tabs/tab[@key="' . SingleApplication::XML_MODULE_PRODUCTS . '"]/block/subBlock[@renderTr]');

        $data['html'] = view('single-application.products.products-batch-table-tr')->with([
            'block' => $block[0],
            'num' => $dataValidated['num'],
            'incrementNumber' => $dataValidated['incrementNumber'],
            'safHiddenColumns' => SingleApplication::getSafHiddenElements()
        ])->render();

        return response()->json($data);
    }

    /**
     * Function to get product Hs codes
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function productHsCodes(Request $request)
    {
        $this->validate($request, [
            'q' => 'required|string_with_max',
        ]);

        $data['items'] = $this->referenceTableFormManager->getProductHsCodes($request->get('q'));

        return response()->json($data);
    }

    /**
     * Function to get info of producer code with producer country id
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function productProducerCode(Request $request)
    {
        $this->validate($request, [
            'country_id' => 'required|integer_with_max|exists:reference_countries,id'
        ]);

        $data['items'] = getReferenceRows(ReferenceTable::REFERENCE_REGISTRY_OF_PRODUCERS, false, false, false, 'get', false, false, ['producer_registry_operating_country' => $request->input('country_id')]);

        return responseResult('OK', '', $data);
    }

    /**
     * Function to render sub form(batches) table render
     *
     * @param $safNumber
     * @param $productId
     * @return array
     *
     * @throws Throwable
     */
    private function productTableRender($safNumber, $productId)
    {
        $saf = SingleApplication::select('id', 'regular_number')->where('regular_number', $safNumber)->firstOrFail();
        $product = SingleApplicationProducts::select(['id', 'product_code', 'commercial_description', 'producer_country', 'total_value', 'total_value_code', 'quantity', 'measurement_unit', 'brutto_weight', 'netto_weight'])->where('id', $productId)->first();

        if (is_null($product)) {
            Log::info('Saf product clone - product id ' . $productId . ', saf-number: ' . $safNumber);
        }

        $result['html'] = view('single-application.products.products-table-tr')->with([
            'product' => $product,
            'productNumbers' => $saf->productsOrderedList(),
            'refHsCodeAllData' => getReferenceRowsKeyValue(ReferenceTable::REFERENCE_HS_CODE_6),
            'refCountriesAllData' => getReferenceRowsKeyValue(ReferenceTable::REFERENCE_COUNTRIES),
            'refCurrenciesAllData' => getReferenceRowsKeyValue(ReferenceTable::REFERENCE_CURRENCIES),
            'refUnitOfMeasurementAllData' => getReferenceRowsKeyValue(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT),
            'saveMode' => 'edit'
        ])->render();

        $result['productsTotals'] = $this->manager->getSafProductsTotalValues($safNumber);
        $result['productId'] = $productId;

        return $result;
    }

    /**
     * Function to SUM product Total to National currency
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function productTotalValueNationalCurrency(Request $request)
    {
        $this->validate($request, [
            'total_value' => 'required|numeric',
            'total_value_code' => 'required|numeric'
        ]);

        $totalValue = $request->input('total_value');
        $totalValueId = $request->input('total_value_code');

        $currentCurrencyRef = getReferenceRows(ReferenceTable::REFERENCE_CURRENCIES, $totalValueId);

        $exchangeRef = ReferenceTable::REFERENCE_EXCHANGE_RATES;
        $totalValueNational = null;

        if (!is_null($currentCurrencyRef)) {

            $currentExchange = DB::table($exchangeRef)->where(['code' => $currentCurrencyRef->code, $exchangeRef . '.show_status' => SingleApplication::STATUS_ACTIVE])->first();

            if (!is_null($currentExchange) && !is_null($currentExchange->number_of_units) && !is_null($currentExchange->rate)) {
                $totalValueNational = ($currentExchange->rate / $currentExchange->number_of_units) * $totalValue;
            }

            if (is_null($totalValueNational)) {
                $errors['total_value_code'] = trans('swis.saf.product.exchange_rate.not_found');

                return responseResult('INVALID_DATA', '', '', $errors);
            }
        }

        $data['totalNationalCurrency'] = round($totalValueNational, 2);

        return responseResult('OK', '', $data);
    }
}