<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserManuals\UserManualsRequest;
use App\Http\Requests\UserManuals\UserManualsSearchRequest;
use App\Models\Agency\Agency;
use App\Models\UserManuals\UserManuals;
use App\Models\UserManuals\UserManualsManager;
use App\Models\UserManuals\UserManualsSearch;
use App\Models\UserManualsRoles\UserManualsRoles;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class UserManualsController
 * @package App\Http\Controllers
 */
class UserManualsController extends BaseController
{
    /**
     * @var UserManualsManager
     */
    protected $manager;

    /**
     * UserManualsController constructor.
     *
     * @param UserManualsManager $manager
     */
    public function __construct(UserManualsManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Function to show index page
     *
     * @return View
     */
    public function index()
    {
        return view('user-manuals.index')->with([
            'roleTypes' => UserManualsRoles::USER_MANUAL_ROLE_TYPES,
            'userManualTypes' => UserManuals::USER_MANUAL_TYPES,
            'page' => UserManuals::PAGE_TYPE_USER_MANUALS,
        ]);
    }

    /**
     * Function to showing all data in dataTable
     *
     * @param UserManualsSearchRequest $userManualsSearchRequest
     * @param UserManualsSearch $userManualsSearch
     * @return JsonResponse
     */
    public function table(UserManualsSearchRequest $userManualsSearchRequest, UserManualsSearch $userManualsSearch)
    {
        $data = $this->dataTableAll($userManualsSearchRequest, $userManualsSearch);

        return response()->json($data);
    }

    /**
     * Function to show create page
     *
     * @return View
     */
    public function create()
    {
        return view('user-manuals.edit')->with([
            'roleTypes' => UserManualsRoles::USER_MANUAL_ROLE_TYPES,
            'userManualTypes' => UserManuals::USER_MANUAL_TYPES,
            'page' => UserManuals::PAGE_TYPE_USER_MANUALS,
            'agencies' => Agency::allData(),
            'saveMode' => 'add',
        ]);
    }

    /**
     * Function to store data
     *
     * @param UserManualsRequest $request
     * @return JsonResponse
     */
    public function store(UserManualsRequest $request)
    {
        $this->manager->store($request->validated());

        return responseResult('OK');
    }

    /**
     * Function to show current by id
     *
     * @param $lngCode
     * @param $id
     * @param $viewType
     * @return View
     */
    public function edit($lngCode, $id, $viewType)
    {
        $userManuals = UserManuals::where('id', $id)->firstOrFail();

        return view('user-manuals.edit')->with([
            'userManuals' => UserManuals::where('id', $id)->firstOrFail(),
            'roleTypes' => UserManualsRoles::USER_MANUAL_ROLE_TYPES,
            'userManualTypes' => UserManuals::USER_MANUAL_TYPES,
            'permissions' => $userManuals->userManualsRoles()->pluck('role_id')->toArray(),
            'agencies' => Agency::allData(),
            'selectedAgencies' => $userManuals->userManualsAgencies()->pluck('company_tax_id')->toArray(),
            'page' => UserManuals::PAGE_TYPE_USER_MANUALS,
            'saveMode' => $viewType
        ]);
    }

    /**
     * Function to update data
     *
     * @param UserManualsRequest $request
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     */
    public function update(UserManualsRequest $request, $lngCode, $id)
    {
        $this->manager->update($request->validated(), $id);

        return responseResult('OK');
    }

    /**
     * Function to Delete by id(array)
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function delete(Request $request)
    {
        $this->manager->delete($request->input('deleteItems'));

        return responseResult('OK');
    }

    /**
     * Function to show user manuals when page=help
     *
     * @return View
     */
    public function showUserManuals()
    {
        return view('user-manuals.index')->with([
            'saveMode' => 'view',
            'page' => UserManuals::PAGE_TYPE_HELP,
            'roleTypes' => UserManualsRoles::USER_MANUAL_ROLE_TYPES,
            'userManualTypes' => UserManuals::USER_MANUAL_TYPES,
        ]);
    }

}
