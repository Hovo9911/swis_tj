<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\UserCustomsOperatorStoreRequest;
use App\Http\Requests\User\UserCustomsOperatorUpdateRequest;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\User\User;
use App\Models\User\UserManager;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

/**
 * Class UserCustomsOperator
 * @package App\Http\Controllers
 */
class UserCustomsOperator extends BaseController
{
    /**
     * @var UserManager
     */
    protected $manager;

    /**
     * UserController constructor.
     *
     * @param UserManager $manager
     */
    public function __construct(UserManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Function to show create page
     *
     * @return View
     */
    public function create()
    {
        return view('user.edit-customs-operator')->with([
            'saveMode' => 'add',
            'refCustomBodies' => getReferenceRows(ReferenceTable::REFERENCE_CUSTOM_BODY_REFERENCE)
        ]);
    }

    /**
     * Function to store customs operator
     *
     * @param UserCustomsOperatorStoreRequest $request
     * @return JsonResponse
     */
    public function store(UserCustomsOperatorStoreRequest $request)
    {
        $user = $this->manager->storeCustomsOperator($request->validated());

        return responseResult('OK', urlWithLng('user_customs_operator/' . $user->id . '/edit'));
    }

    /**
     * Function to show current by id
     *
     * @param $lngCode
     * @param $id
     * @return RedirectResponse|View
     */
    public function edit($lngCode, $id)
    {
        $user = User::where('id', $id)->firstOrFail();

        if (!$user->is_customs_operator) {
            return redirect(urlWithLng('user/' . $user->id . '/edit'));
        }

        return view('user.edit-customs-operator')->with([
            'user' => $user,
            'refCustomBodies' => getReferenceRows(ReferenceTable::REFERENCE_CUSTOM_BODY_REFERENCE),
            'saveMode' => 'edit'
        ]);
    }

    /**
     * Function to view user
     *
     * @param $lngCode
     * @param $id
     * @return RedirectResponse|View
     */
    public function view($lngCode, $id)
    {
        $user = User::where('id', $id)->firstOrFail();

        return view('user.view-customs-operator')->with([
            'user' => $user,
            'refCustomBody' => getReferenceRows(ReferenceTable::REFERENCE_CUSTOM_BODY_REFERENCE, $user->ref_custom_body),
            'saveMode' => 'view',
        ]);
    }

    /**
     * Function to update customs operator
     *
     * @param UserCustomsOperatorUpdateRequest $request
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     */
    public function update(UserCustomsOperatorUpdateRequest $request, $lngCode, $id)
    {
        $this->manager->updateCustomsOperator($request->validated(), $id);

        return responseResult('OK');
    }
}