<?php

namespace App\Http\Controllers;

use App\Http\Requests\BalanceOperations\BalanceOperationsSearchRequest;
use App\Http\Requests\BalanceOperations\BalanceTransactionsSearchRequest;
use App\Http\Requests\Monitoring\TransactionsMonitoringSearchRequest;
use App\Models\BalanceOperations\BalanceOperationsNotificationsManager;
use App\Models\BalanceOperations\BalanceOperationsSearch;
use App\Models\BalanceOperations\BalanceTransactionsSearch;
use App\Models\BaseModel;
use App\Models\Company\Company;
use App\Models\Monitoring\TransactionsMonitoringSearch;
use App\Models\PaymentProviders\PaymentProviders;
use App\Models\Transactions\TransactionsManager;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Throwable;

/**
 * Class BalanceOperationsController
 * @package App\Http\Controllers
 */
class BalanceOperationsController extends BaseController
{
    /**
     * @var BalanceOperationsNotificationsManager
     */
    protected $balanceNotificationManager;

    /**
     * @var TransactionsManager
     */
    protected $transactionManager;

    /**
     * BalanceOperationsController constructor.
     *
     * @param BalanceOperationsNotificationsManager $balanceNotificationManager
     * @param TransactionsManager $transactionManager
     */
    public function __construct(BalanceOperationsNotificationsManager $balanceNotificationManager, TransactionsManager $transactionManager)
    {
        $this->balanceNotificationManager = $balanceNotificationManager;
        $this->transactionManager = $transactionManager;
    }

    /**
     * Function to return index page
     *
     * @return View
     */
    public function index()
    {
        $companies = Company::select('company.id', DB::raw("COALESCE(agency_ml.name, company.name) as name"), 'company.tax_id')
            ->leftJoin('agency', function ($q) {
                $q->on('agency.tax_id', '=', 'company.tax_id')->where('show_status', BaseModel::STATUS_ACTIVE);
            })->leftJoin('agency_ml', function ($q) {
                $q->on('agency_ml.agency_id', '=', 'agency.id')->where('lng_id', cLng('id'));
            })->get();

        $paymentProviders = PaymentProviders::select('id', DB::raw("CONCAT(name,' - ',description) AS name"))->active()->get();

        return view('balance-operations.index', compact('companies', 'paymentProviders'));
    }

    /**
     * Function to search balances
     *
     * @param BalanceOperationsSearchRequest $balanceOperationsRequest
     * @param BalanceOperationsSearch $balanceOperationsSearch
     * @return JsonResponse
     */
    public function table(BalanceOperationsSearchRequest $balanceOperationsRequest, BalanceOperationsSearch $balanceOperationsSearch)
    {
        $data = $this->dataTableAll($balanceOperationsRequest, $balanceOperationsSearch);

        return response()->json($data);
    }

    /**
     * Function to search transactions
     *
     * @param BalanceTransactionsSearchRequest $balanceTransactionsRequest
     * @param BalanceTransactionsSearch $balanceTransactionsSearch
     * @return JsonResponse
     */
    public function transactionsTable(BalanceTransactionsSearchRequest $balanceTransactionsRequest, BalanceTransactionsSearch $balanceTransactionsSearch)
    {
        $data = $this->dataTableAll($balanceTransactionsRequest, $balanceTransactionsSearch);

        return response()->json($data);
    }

    /**
     * Function to all data showing in datatable
     *
     * @param TransactionsMonitoringSearchRequest $request
     * @param TransactionsMonitoringSearch $transactionsSearch
     * @return JsonResponse
     */
    public function paysTable(TransactionsMonitoringSearchRequest $request, TransactionsMonitoringSearch $transactionsSearch)
    {
        $data = $this->dataTableAll($request, $transactionsSearch);

        return response()->json($data);
    }

    /**
     * Function to get company balance
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function getBalance(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required|integer_with_max|exists:company,id',
            'val' => 'required|integer_with_max'
        ]);

        $company = Company::select('id', 'tax_id')->find($validatedData['id']);
        $balance = $company->balance($company->tax_id);

        if ($balance >= $validatedData['val']) {
            $modal = view('balance-operations.refund-modal')->with(['success' => true, 'company' => $company, 'oldBalance' => $balance, 'newBalance' => $balance - $validatedData['val'], 'refund' => $validatedData['val']])->render();
        } else {
            $modal = view('balance-operations.refund-modal')->with(['success' => false, 'company' => $company,])->render();
        }

        return responseResult('OK', '', ['modal' => $modal]);
    }

    /**
     * Function to confirm refund
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function confirmRefund(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required|integer_with_max|exists:company,id',
            'refund' => 'required|integer_with_max'
        ]);

        $company = Company::select('id', 'tax_id')->find($validatedData['id']);
        $balance = $company->balance($company->tax_id);
        if ($balance >= $validatedData['refund']) {

            $this->transactionManager->store([
                'payment_id' => null,
                'saf_number' => null,
                'user_id' => Auth::user()->id,
                'company_tax_id' => $company->tax_id,
                'type' => 'output',
                'transaction_date' => Carbon::now(),
                'amount' => -$validatedData['refund'],
                'total_balance' => number_format($balance - $validatedData['refund'], 2, '.', ''),
                'obligation_id' => null,
                'is_refund' => true
            ]);

            //send notification
            $this->balanceNotificationManager->storeNotification($company->tax_id, $validatedData['refund']);

            return responseResult('OK', urlWithLng('balance-operations'));
        } else {
            return responseResult('INVALID_DATA', '', '', []);
        }
    }
}
