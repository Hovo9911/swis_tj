<?php

namespace App\Http\Controllers\RoleManagement;

use App\Http\Controllers\BaseController;
use App\Http\Requests\Role\RoleRequest;
use App\Http\Requests\Role\RoleSearchRequest;
use App\Models\ConstructorMapping\ConstructorMapping;
use App\Models\Menu\Menu;
use App\Models\Menu\MenuManager;
use App\Models\Role\Role;
use App\Models\Role\RoleManager;
use App\Models\Role\RoleSearch;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

/**
 * Class RolesController
 * @package App\Http\Controllers\RoleManagement
 */
class RolesController extends BaseController
{
    /**
     * @var RoleManager
     */
    protected $manager;

    /**
     * RolesController constructor.
     *
     * @param RoleManager $manager
     */
    public function __construct(RoleManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Function to show index page
     *
     * @return View
     */
    public function index()
    {
        return view('role.index');
    }

    /**
     * Function to get all data and showing in datatable
     *
     * @param RoleSearchRequest $roleSearchRequest
     * @param RoleSearch $roleSearch
     * @return JsonResponse
     */
    public function table(RoleSearchRequest $roleSearchRequest, RoleSearch $roleSearch)
    {
        $data = $this->dataTableAll($roleSearchRequest, $roleSearch);

        return response()->json($data);
    }

    /**
     * Function to show create page
     *
     * @return View
     */
    public function create()
    {
        return view('role.edit')->with([
            'saveMode' => 'add',
            'menuModules' => MenuManager::menuModules(),
        ]);
    }

    /**
     * Function to store validated data
     *
     * @param RoleRequest $request
     * @return JsonResponse
     */
    public function store(RoleRequest $request)
    {
        $role = $this->manager->store($request->validated());

        return responseResult('OK', null, ['id' => $role->id]);
    }

    /**
     * Function to show edit page by current id
     *
     * @param $lngCode
     * @param $id
     * @param $viewType
     * @return View
     */
    public function edit($lngCode, $id, $viewType)
    {
        $role = Role::where('id', $id)->checkUserHasRole()->firstOrFail();
        $menuName = Menu::where('id', $role->menu_id)->pluck('title')->first();

        return view('role.edit')->with([
            'menuModules' => MenuManager::menuModules(),
            'role' => $role,
            'menuName' => trans($menuName),
            'saveMode' => $viewType
        ]);
    }

    /**
     * Function to update data
     *
     * @param RoleRequest $request
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     */
    public function update(RoleRequest $request, $lngCode, $id)
    {
        if ($request->input('show_status') == Role::STATUS_INACTIVE && Role::find($id)->show_status == Role::STATUS_ACTIVE) {

            $constructorMapping = ConstructorMapping::select('id')->where('role', $id)->active()->get();
            if ($constructorMapping->count()) {
                $errors['show_status'] = trans('core.base.used.title');

                return responseResult('INVALID_DATA', '', '', $errors);
            }
        }

        $this->manager->update($request->validated(), $id);

        return responseResult('OK');
    }

    /**
     * Function to set AUTH user role
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function setRole(Request $request)
    {
        $this->validate($request, [
            'role' => 'required|string_with_max|in:' . implode(',', Role::ROLE_LEVEL_TYPES_ALL),
            'role_type' => 'required|string_with_max|in:' . implode(',', Role::ROLE_TYPES_ALL),
            'user_role' => 'required|integer_with_max',
        ]);

        //
        $currentRoleData = $this->manager->currentRoleData($request->get('role'), $request->get('role_type'), $request->get('user_role'));

        if (count($currentRoleData)) {
            Auth::user()->setCurrentRole((object)$currentRoleData);

            if ($currentRoleData['role_type'] == Role::ROLE_TYPE_CUSTOMS_OPERATOR) {
                return redirect(urlWithLng('customs-portal'));
            }

            return redirect(urlWithLng('dashboard'));
        }

        return redirect(urlWithLng('role/set'));
    }

    /**
     * Function to get attributes of the current role
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function roleAttributes(Request $request)
    {
        $isRoleGroup = (bool)$request->get('isRoleGroup');
        $existTableCondition = $isRoleGroup ? 'roles_group' : 'roles';

        $this->validate($request, [
            'role' => "required|integer_with_max|exists:$existTableCondition,id",
            'isRoleGroup' => "nullable|in:true"
        ]);

        $data['attributes'] = $this->manager->getAttributes($request->get('role'), (bool)$request->get('isRoleGroup'));

        return responseResult('OK', '', $data);
    }

    /**
     * Function to get current attribute values
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function roleAttributeValues(Request $request)
    {
        $isRoleGroup = (bool)$request->get('isRoleGroup');
        $existTableCondition = $isRoleGroup ? 'roles_group' : 'roles';

        $this->validate($request, [
            'attribute' => 'required|string_with_max',
            'type' => 'required|in:' . implode(',', Role::ROLE_ATTRIBUTES_TYPE_ALL),
            'role' => "required|integer_with_max|exists:$existTableCondition,id",
            'isRoleGroup' => 'nullable|in:true'
        ]);

        $data['attributeValues'] = $this->manager->getAttributeValues($request->get('attribute'), $request->get('type'), $request->get('role'), $isRoleGroup);

        return responseResult('OK', '', $data);
    }

    /**
     * Function to get payment and obligation attributes
     *
     * @return JsonResponse
     */
    public function paymentAndObligationRoleAttributes()
    {
        if (!Auth::user()->isAgency()) {
            return responseResult('INVALID_DATA');
        }

        $data['attributes'] = [
            Role::ROLE_ATTRIBUTES_PAYMENT_AND_OBLIGATION_SUBDIVISION => trans('swis.role.attributes.subdivision'),
            Role::ROLE_ATTRIBUTES_PAYMENT_AND_OBLIGATION_DOCUMENT => trans('swis.role.attributes.document')
        ];

        return responseResult('OK', '', $data);
    }

    /**
     * Function to get multiple items for role(Reports list)
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function roleMultipleAttribute(Request $request)
    {
        $this->validate($request, [
            'menu_id' => 'required|exists:menu,id',
        ]);

        $data = [];
        if (Menu::getMenuIdByName(Menu::REPORTS_MENU_NAME) == $request->get('menu_id')) {
            $reports = Auth::user()->getUserReports();

            if ($reports->count()) {
                foreach ($reports as $report) {
                    $data['reports'][] = [
                        'id' => $report->id,
                        'name' => trans('swis.' . $report->code . '.menu.title'),
                    ];
                }
            }
        }

        return responseResult('OK', '', $data);
    }
}
