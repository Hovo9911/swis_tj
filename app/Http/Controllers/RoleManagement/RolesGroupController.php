<?php

namespace App\Http\Controllers\RoleManagement;

use App\Http\Controllers\BaseController;
use App\Http\Requests\RolesGroup\RolesGroupRequest;
use App\Http\Requests\RolesGroup\RolesGroupSearchRequest;
use App\Models\Menu\Menu;
use App\Models\Menu\MenuManager;
use App\Models\Role\Role;
use App\Models\Role\RoleManager;
use App\Models\RolesGroup\RolesGroup;
use App\Models\RolesGroup\RolesGroupManager;
use App\Models\RolesGroup\RolesGroupSearch;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class RolesGroupController
 * @package App\Http\Controllers\RoleManagement
 */
class RolesGroupController extends BaseController
{
    /**
     * @var RolesGroupManager
     */
    protected $manager;

    /**
     * @var RoleManager
     */
    protected $roleManager;

    /**
     * RolesGroupController constructor.
     *
     * @param RolesGroupManager $manager
     * @param RoleManager $roleManager
     */
    public function __construct(RolesGroupManager $manager, RoleManager $roleManager)
    {
        $this->manager = $manager;
        $this->roleManager = $roleManager;
    }

    /**
     * Function to show index page
     *
     * @return View
     */
    public function index()
    {
        return view('roles-group.index');
    }

    /**
     * Function too get all data and showing in datatable
     *
     * @param RolesGroupSearchRequest $rolesGroupSearchRequest
     * @param RolesGroupSearch $roleSearch
     * @return JsonResponse
     */
    public function table(RolesGroupSearchRequest $rolesGroupSearchRequest, RolesGroupSearch $roleSearch)
    {
        $data = $this->dataTableAll($rolesGroupSearchRequest, $roleSearch);

        return response()->json($data);
    }

    /**
     * Function to show create page
     *
     * @return View
     */
    public function create()
    {
        return view('roles-group.edit')->with([
            'menuModules' => MenuManager::menuModules(),
            'myApplicationMenuId' => Menu::getMenuIdByName(Menu::USER_DOCUMENTS_MENU_NAME),
            'saveMode' => 'add',
        ]);
    }

    /**
     * Function to store validated data
     *
     * @param RolesGroupRequest $request
     * @return JsonResponse
     */
    public function store(RolesGroupRequest $request)
    {
        $this->manager->store($request->validated());

        return responseResult('OK');
    }

    /**
     * Function to show edit page of role group
     *
     * @param $lngCode
     * @param $id
     * @param $viewType
     * @return View
     */
    public function edit($lngCode, $id, $viewType)
    {
        $roleGroup = RolesGroup::with(['attributes', 'roles'])->where('id', $id)->checkUserHasRole()->firstOrFail();
        $roles = Role::with(['current'])->where('menu_id', $roleGroup->menu_id)->checkUserHasRole()->orderByDesc()->get();

        $checkedRoles = [];
        if (count($roleGroup->roles) > 0) {
            foreach ($roleGroup->roles as $value) {
                $checkedRoles[] = $value->pivot->role_id;
            }
        }

        $checkedAttributes = [];
        if (count($roleGroup->attributes)) {
            foreach ($roleGroup->attributes as $attributes) {
                if (isset($checkedAttributes[$attributes->attribute_type]) && in_array($attributes->attribute_id, $checkedAttributes[$attributes->attribute_type])) {
                    continue;
                }

                $checkedAttributes[$attributes->attribute_type][] = $attributes->attribute_id;
            }
        }

        $menuName = Menu::where('id', $roleGroup->menu_id)->pluck('title')->first();

        return view('roles-group.edit')->with([
            'menuModules' => MenuManager::menuModules(),
            'roleGroup' => $roleGroup,
            'roles' => $roles,
            'rolesAttributes' => $this->roleManager->getAttributes($roleGroup->roles->pluck('id')->all()),
            'checkedRoles' => $checkedRoles,
            'checkedAttributes' => $checkedAttributes,
            'menuName' => trans($menuName),
            'myApplicationMenuId' => Menu::getMenuIdByName(Menu::USER_DOCUMENTS_MENU_NAME),
            'saveMode' => $viewType
        ]);
    }

    /**
     * Function to update data
     *
     * @param RolesGroupRequest $request
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     */
    public function update(RolesGroupRequest $request, $lngCode, $id)
    {
        $this->manager->update($request->validated(), $id);

        return responseResult('OK');
    }

    /**
     * Function to get roles of menu(module)
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getMenuRoles(Request $request)
    {
        $this->validate($request, [
            'menu_id' => 'required|integer_with_max|exists:menu,id'
        ]);

        $data['roles'] = Role::select('id', 'role_ml.name', 'description')->joinMl(['t_ml' => 'role_ml', 'f_k' => 'role_id'])->active()->checkUserHasRole()->where('menu_id', $request->input('menu_id'))->orderByDesc()->get();

        return responseResult('OK', '', $data);
    }

    /**
     * Function to get roles attributes
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getRolesAttributes(Request $request)
    {
        $this->validate($request, [
            'role_ids' => 'required|array',
            'role_ids.*' => 'integer_with_max|exists:roles,id',
        ]);

        $data['attributes'] = $this->roleManager->getAttributes($request->get('role_ids'));

        return responseResult('OK', '', $data);
    }
}
