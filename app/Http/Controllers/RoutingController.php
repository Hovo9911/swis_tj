<?php

namespace App\Http\Controllers;

use App\Http\Requests\Routing\RoutingRequest;
use App\Http\Requests\Routing\RoutingSearchRequest;
use App\Models\Agency\Agency;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\Routing\Routing;
use App\Models\Routing\RoutingManager;
use App\Models\Routing\RoutingSearch;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class ConstructorRoutingController
 * @package App\Http\Controllers
 */
class RoutingController extends BaseController
{
    /**
     * @var RoutingManager
     */
    protected $manager;

    /**
     * RoutingController constructor.
     *
     * @param RoutingManager $manager
     */
    public function __construct(RoutingManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Function to show index page
     *
     * @return View
     */
    public function index()
    {
        return view('routing.index')->with([
            'agencies' => Agency::allData(),
            'classificatorDocuments' => getReferenceRows(ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS)
        ]);
    }

    /**
     * Function to get all data showing in datatable
     *
     * @param RoutingSearchRequest $routingSearchRequest
     * @param RoutingSearch $routingSearch
     * @return JsonResponse
     */
    public function table(RoutingSearchRequest $routingSearchRequest, RoutingSearch $routingSearch)
    {
        $data = $this->dataTableAll($routingSearchRequest, $routingSearch);

        return response()->json($data);
    }

    /**
     * Function to show create page
     *
     * @return View
     */
    public function create()
    {
        return view('routing.edit')->with([
            'classificatorDocuments' => getReferenceRows(ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS),
            'saveMode' => 'add',
            'agencies' => Agency::allData(),
            'safFields' => getSafXmlControlledFields(),
        ]);
    }

    /**
     * Function to save data
     *
     * @param RoutingRequest $request
     * @return JsonResponse
     */
    public function store(RoutingRequest $request)
    {
        $this->manager->store($request->validated());

        return responseResult('OK');
    }

    /**
     * Function to show current by id
     *
     * @param $lngCode
     * @param $id
     * @param $viewType
     * @return View
     */
    public function edit($lngCode, $id, $viewType)
    {
        $routing = Routing::select('routing.*', 'constructor_documents.document_code', 'constructor_documents_ml.document_name')
            ->join('constructor_documents', 'constructor_documents.id', '=', 'routing.constructor_document_id')
            ->join('constructor_documents_ml', 'constructor_documents_ml.constructor_documents_id', '=', 'routing.constructor_document_id')
            ->where('constructor_documents_ml.lng_id', clng('id'))
            ->where('routing.id', $id)
            ->checkUserHasRole()
            ->firstOrFail();

        return view('routing.edit')->with([
            'routing' => $routing,
            'classificatorDocuments' => getReferenceRows(ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS),
            'saveMode' => $viewType,
            'agencies' => Agency::allData(),
            'safFields' => getSafXmlControlledFields(),
        ]);
    }

    /**
     * Function to update data
     *
     * @param RoutingRequest $request
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     */
    public function update(RoutingRequest $request, $lngCode, $id)
    {
        $this->manager->update($request->validated(), $id);

        return responseResult('OK');
    }

    /**
     * Function to get agencies by documents
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function constructorDocumentsOfAgency(Request $request)
    {
        $this->validate($request, [
            'agency_id' => 'required|integer_with_max|exists:agency,id',
        ]);

        $agency = Agency::select('tax_id')->where('id', $request->input('agency_id'))->first();

        //
        $constructorDocuments = ConstructorDocument::getConstructorDocumentForAgency($agency->tax_id);

        //
        $data = [];
        if ($constructorDocuments->count()) {
            foreach ($constructorDocuments as $item) {
                $data['items'][] = [
                    'id' => $item->id,
                    'name' => "({$item->document_code}) {$item->document_name}"
                ];
            }
        }

        return responseResult('OK', '', $data);
    }

}
