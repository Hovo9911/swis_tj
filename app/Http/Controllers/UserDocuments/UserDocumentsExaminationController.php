<?php

namespace App\Http\Controllers\UserDocuments;

use App\Http\Controllers\BaseController;
use App\Http\Requests\UserDocumentsRequest\ExaminationStoreRequest;
use App\Models\BaseModel;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplicationExaminations\SingleApplicationExaminations;
use App\Models\SingleApplicationProducts\SingleApplicationProducts;
use App\Models\SingleApplicationSubApplicationProducts\SingleApplicationSubApplicationProducts;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

/**
 * Class UserDocumentsExaminationController
 * @package App\Http\Controllers\UserDocuments
 */
class UserDocumentsExaminationController extends BaseController
{
    /**
     * @var array
     */
    const EXAMINATION_DATA = [
        "definition_of_carats_of_stones" => 0,
        "platinium" => 0,
        "gold" => 0,
        "palladium" => 0,
        "metal_with_silver" => 0,
        "silver" => 0,
        "bijouterie" => 0,
        "stones" => 0,
        "total" => 0
    ];

    /**
     * Function to render examination inputs
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function renderExaminationInputs(Request $request)
    {
        $subApplication = SingleApplicationSubApplications::where('saf_sub_applications.id', $request->get('applicationId'))->firstOrFail();
        $subAppProductsIds = SingleApplicationSubApplicationProducts::where(['subapplication_id' => $request->get('applicationId')])->pluck('product_id')->all();
        $subAppProducts = SingleApplicationProducts::select('id', 'product_number', 'commercial_description', 'measurement_unit')->whereIn('id', $subAppProductsIds)->get();

        $data['html'] = view('user-documents.old-laboratories.examination-table-tr')->with([
            'renderInput' => true,
            'num' => $request->get('num'),
            'subAppProducts' => $subAppProducts,
            'typesOfProductsRef' => getReferenceRows(ReferenceTable::REFERENCE_TYPES_OF_PRODUCTS),
            'typeOfMetalRef' => getReferenceRows(ReferenceTable::REFERENCE_TYPE_OF_METAL),
            'typeOfStonesRef' => getReferenceRows(ReferenceTable::REFERENCE_TYPE_OF_STONES),
            'productsIdNumbers' => $subApplication->productsOrderedList(),
            'isApplication' => true
        ])->render();

        return responseResult('OK', '', $data);
    }

    /**
     * Function to Store examination info
     *
     * @param $lngCode
     * @param $documentId
     * @param $applicationId
     * @param ExaminationStoreRequest $examinationRequest
     * @return JsonResponse
     */
    public function storeExamination($lngCode, $documentId, $applicationId, ExaminationStoreRequest $examinationRequest)
    {
        $dataValidated = $examinationRequest->validated();

        $dataValidated['saf_number'] = $examinationRequest->header('sNumber');
        $dataValidated['document_id'] = $documentId;
        $dataValidated['type_of_stone'] = $examinationRequest->header('typeOfStones');
        $dataValidated['measurement_unit'] = SingleApplicationProducts::where('id', $dataValidated['saf_product_id'])->pluck('measurement_unit')->first();
        $dataValidated['saf_subapplication_id'] = $applicationId;

        $subApplicationData = SingleApplicationSubApplications::where('id', $applicationId)->pluck('data')->first();

        if ($subApplicationData) {

            $pricesForSAF = $this->examinationCalculationRule($dataValidated);

            if (!array_key_exists('examination', $subApplicationData['saf'])) {
                $subApplicationData['saf']['examination'] = [
                    'platinium' => $pricesForSAF['platinium'],
                    'gold' => $pricesForSAF['gold'],
                    'palladium' => $pricesForSAF['palladium'],
                    'metal_with_silver' => $pricesForSAF['metal_with_silver'],
                    'silver' => $pricesForSAF['silver'],
                    'bijouterie' => $pricesForSAF['bijouterie'],
                    'stones' => $pricesForSAF['stones'],
                    'total' => $pricesForSAF['total'],
                    'definition_of_carats_of_stones' => $pricesForSAF['definition_of_carats_of_stones']
                ];
            } else {
                $safExaminationData = $subApplicationData['saf']['examination'];
                $safExaminationData['platinium'] = (float)$safExaminationData['platinium'] + (float)$pricesForSAF['platinium'];
                $safExaminationData['gold'] = (float)$safExaminationData['gold'] + (float)$pricesForSAF['gold'];
                $safExaminationData['palladium'] = (float)$safExaminationData['palladium'] + (float)$pricesForSAF['palladium'];
                $safExaminationData['metal_with_silver'] = (float)$safExaminationData['metal_with_silver'] + (float)$pricesForSAF['metal_with_silver'];
                $safExaminationData['silver'] = (float)$safExaminationData['silver'] + (float)$pricesForSAF['silver'];
                $safExaminationData['bijouterie'] = (float)$safExaminationData['bijouterie'] + (float)$pricesForSAF['bijouterie'];
                $safExaminationData['stones'] = (float)$safExaminationData['stones'] + (float)$pricesForSAF['stones'];
                $safExaminationData['total'] = (float)$safExaminationData['total'] + (float)$pricesForSAF['total'];
                $safExaminationData['definition_of_carats_of_stones'] = (float)$safExaminationData['definition_of_carats_of_stones'] + (float)$pricesForSAF['definition_of_carats_of_stones'];
                $subApplicationData['saf']['examination'] = $safExaminationData;
            }

            SingleApplicationSubApplications::where('id', $applicationId)->update(['data' => json_encode($subApplicationData)]);

            $examination = SingleApplicationExaminations::create($dataValidated);

            $prices = [
                'platinumPrice' => $subApplicationData['saf']['examination']['platinium'],
                'goldPrice' => $subApplicationData['saf']['examination']['gold'],
                'palladiumPrice' => $subApplicationData['saf']['examination']['palladium'],
                'percentSilverPrice' => $subApplicationData['saf']['examination']['metal_with_silver'],
                'silverPrice' => $subApplicationData['saf']['examination']['silver'],
                'bijouteriePrice' => $subApplicationData['saf']['examination']['bijouterie'],
                'stonePrice' => $subApplicationData['saf']['examination']['stones'],
                'caratPrice' => $subApplicationData['saf']['examination']['definition_of_carats_of_stones'],
                'totalPrice' => $subApplicationData['saf']['examination']['total'],
                'examinationId' => $examination->id
            ];

            return responseResult('OK', '', $prices);
        }

        return responseResult('INVALID_DATA', '', '', ['invalid_state' => trans('core.base.invalid.state')]);
    }

    /**
     * Function to update examination
     *
     * @param $lngCode
     * @param $documentId
     * @param $applicationId
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function editExamination($lngCode, $documentId, $applicationId, Request $request)
    {
        $this->validate($request, [
            'examinationId' => 'required|integer_with_max'
        ]);

        $examination = SingleApplicationExaminations::where('id', $request->input('examinationId'))->active()->first();
        $subApplication = SingleApplicationSubApplications::where('saf_sub_applications.id', $applicationId)->firstOrFail();
        $subAppProductsIds = SingleApplicationSubApplicationProducts::where(['subapplication_id' => $applicationId])->pluck('product_id')->all();
        $subAppProducts = SingleApplicationProducts::select('id', 'product_number', 'commercial_description', 'measurement_unit')->whereIn('id', $subAppProductsIds)->get();

        $data['html'] = view('user-documents.old-laboratories.examination-table-tr')->with([
            'renderInput' => true,
            'num' => $request->input('num'),
            'subAppProducts' => $subAppProducts,
            'typesOfProductsRef' => getReferenceRows(ReferenceTable::REFERENCE_TYPES_OF_PRODUCTS, false, false, ['id', 'code', 'name']),
            'typeOfMetalRef' => getReferenceRows(ReferenceTable::REFERENCE_TYPE_OF_METAL, false, false, ['id', 'code', 'name']),
            'typeOfStonesRef' => getReferenceRows(ReferenceTable::REFERENCE_TYPE_OF_STONES, false, false, ['id', 'code', 'name']),
            'productsIdNumbers' => $subApplication->productsOrderedList(),
            'examination' => $examination,
            'isApplication' => true])->render();

        return responseResult('OK', '', $data);
    }

    /**
     * Function to update examination data
     *
     * @param $lngCode
     * @param $documentId
     * @param $applicationId
     * @param ExaminationStoreRequest $examinationRequest
     * @return JsonResponse
     */
    public function updateExamination($lngCode, $documentId, $applicationId, ExaminationStoreRequest $examinationRequest)
    {
        $dataValidated = $examinationRequest->validated();
        $dataValidated['saf_number'] = $examinationRequest->header('sNumber');
        $dataValidated['saf_subapplication_id'] = $applicationId;
        $dataValidated['document_id'] = $documentId;
        $dataValidated['type_of_stone'] = $examinationRequest->header('typeOfStones');
        $dataValidated['measurement_unit'] = SingleApplicationProducts::where('id', $dataValidated['saf_product_id'])->pluck('measurement_unit')->first();
        $examinationId = $dataValidated['examinationId'];

        unset($dataValidated['examinationId']);

        $subApplicationData = SingleApplicationSubApplications::where('id', $applicationId)->pluck('data')->first();

        if ($subApplicationData) {

            SingleApplicationExaminations::where('id', $examinationId)->update($dataValidated);

            $pricesForSAF = $this->updateExaminationCalculations($applicationId);

            $subApplicationData['saf']['examination']['platinium'] = (float)$pricesForSAF['platinium'];
            $subApplicationData['saf']['examination']['gold'] = (float)$pricesForSAF['gold'];
            $subApplicationData['saf']['examination']['palladium'] = (float)$pricesForSAF['palladium'];
            $subApplicationData['saf']['examination']['metal_with_silver'] = (float)$pricesForSAF['metal_with_silver'];
            $subApplicationData['saf']['examination']['silver'] = (float)$pricesForSAF['silver'];
            $subApplicationData['saf']['examination']['bijouterie'] = (float)$pricesForSAF['bijouterie'];
            $subApplicationData['saf']['examination']['stones'] = (float)$pricesForSAF['stones'];
            $subApplicationData['saf']['examination']['definition_of_carats_of_stones'] = (float)$pricesForSAF['definition_of_carats_of_stones'];
            $subApplicationData['saf']['examination']['total'] = (float)$pricesForSAF['total'];

            SingleApplicationSubApplications::where('id', $applicationId)->update(['data' => json_encode($subApplicationData)]);

            return responseResult('OK', '', [
                'platinumPrice' => $subApplicationData['saf']['examination']['platinium'],
                'goldPrice' => $subApplicationData['saf']['examination']['gold'],
                'palladiumPrice' => $subApplicationData['saf']['examination']['palladium'],
                'percentSilverPrice' => $subApplicationData['saf']['examination']['metal_with_silver'],
                'silverPrice' => $subApplicationData['saf']['examination']['silver'],
                'bijouteriePrice' => $subApplicationData['saf']['examination']['bijouterie'],
                'stonePrice' => $subApplicationData['saf']['examination']['stones'],
                'caratPrice' => $subApplicationData['saf']['examination']['definition_of_carats_of_stones'],
                'totalPrice' => $subApplicationData['saf']['examination']['total'],
                'examinationId' => $examinationId
            ]);
        }
    }

    /**
     * Function to delete examination
     *
     * @param $lngCode
     * @param $documentId
     * @param $applicationId
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteExamination($lngCode, $documentId, $applicationId, Request $request)
    {
        $this->validate($request, [
            'examinationId' => 'required|integer_with_max'
        ]);

        $examination = SingleApplicationExaminations::where('id', $request->input('examinationId'))->active()->first();

        $subApplicationData = SingleApplicationSubApplications::where('id', $applicationId)->pluck('data')->first();

        if ($subApplicationData && $examination) {

            $pricesForSAF = $this->examinationCalculationRule($examination);

            if (array_key_exists('examination', $subApplicationData['saf'])) {
                $subApplicationData['saf']['examination']['platinium'] = (float)$subApplicationData['saf']['examination']['platinium'] - (float)$pricesForSAF['platinium'];
                $subApplicationData['saf']['examination']['gold'] = (float)$subApplicationData['saf']['examination']['gold'] - (float)$pricesForSAF['gold'];
                $subApplicationData['saf']['examination']['palladium'] = (float)$subApplicationData['saf']['examination']['palladium'] - (float)$pricesForSAF['palladium'];
                $subApplicationData['saf']['examination']['metal_with_silver'] = (float)$subApplicationData['saf']['examination']['metal_with_silver'] - (float)$pricesForSAF['metal_with_silver'];
                $subApplicationData['saf']['examination']['silver'] = (float)$subApplicationData['saf']['examination']['silver'] - (float)$pricesForSAF['silver'];
                $subApplicationData['saf']['examination']['bijouterie'] = (float)$subApplicationData['saf']['examination']['bijouterie'] - (float)$pricesForSAF['bijouterie'];
                $subApplicationData['saf']['examination']['stones'] = (float)$subApplicationData['saf']['examination']['stones'] - (float)$pricesForSAF['stones'];
                $subApplicationData['saf']['examination']['definition_of_carats_of_stones'] = (float)$subApplicationData['saf']['examination']['definition_of_carats_of_stones'] - (float)$pricesForSAF['definition_of_carats_of_stones'];
                $subApplicationData['saf']['examination']['total'] = (float)$subApplicationData['saf']['examination']['total'] - (float)$pricesForSAF['total'];
            }

            SingleApplicationSubApplications::where('id', $applicationId)->update(['data' => json_encode($subApplicationData)]);

            $updateExamination = SingleApplicationExaminations::where('id', $request->input('examinationId'))->update(['show_status' => SingleApplicationExaminations::STATUS_DELETED]);
            if ($updateExamination) {
                SingleApplicationSubApplications::where('id', $applicationId)->update(['data' => json_encode($subApplicationData)]);
            }

            return responseResult('OK', '', [
                'platinumPrice' => $subApplicationData['saf']['examination']['platinium'],
                'goldPrice' => $subApplicationData['saf']['examination']['gold'],
                'palladiumPrice' => $subApplicationData['saf']['examination']['palladium'],
                'percentSilverPrice' => $subApplicationData['saf']['examination']['metal_with_silver'],
                'silverPrice' => $subApplicationData['saf']['examination']['silver'],
                'bijouteriePrice' => $subApplicationData['saf']['examination']['bijouterie'],
                'stonePrice' => $subApplicationData['saf']['examination']['stones'],
                'caratPrice' => $subApplicationData['saf']['examination']['definition_of_carats_of_stones'],
                'totalPrice' => $subApplicationData['saf']['examination']['total']
            ]);
        }

    }

    /**
     * Function to re calculate examinations
     *
     * @param $applicationId
     * @return array
     */
    private function updateExaminationCalculations($applicationId)
    {
        $examinationsData = self::EXAMINATION_DATA;
        $examinations = SingleApplicationExaminations::where('saf_subapplication_id', $applicationId)->where('show_status', BaseModel::STATUS_ACTIVE)->get();

        foreach ($examinations as $examination) {
            $tmpData = $this->examinationCalculationRule($examination);
            $examinationsData["platinium"] += $tmpData["platinium"];
            $examinationsData["gold"] += $tmpData["gold"];
            $examinationsData["palladium"] += $tmpData["palladium"];
            $examinationsData["metal_with_silver"] += $tmpData["metal_with_silver"];
            $examinationsData["silver"] += $tmpData["silver"];
            $examinationsData["bijouterie"] += $tmpData["bijouterie"];
            $examinationsData["stones"] += $tmpData["stones"];
            $examinationsData["definition_of_carats_of_stones"] += $tmpData["definition_of_carats_of_stones"];
            $examinationsData["total"] += $tmpData["total"];
        }
        return $examinationsData;
    }

    /**
     * Function to calculate examination
     *
     * @param $dataValidated
     * @return array
     */
    private function examinationCalculationRule($dataValidated)
    {
        $examinationData = self::EXAMINATION_DATA;
        $typeOfMetalRef = getReferenceRows(ReferenceTable::REFERENCE_TYPE_OF_METAL, $dataValidated['type_of_metal'], false, ['code', 'payment_for_unit']);

        switch (substr(optional($typeOfMetalRef)->code, 0, 2)) {
            case ReferenceTable::REFERENCE_METAL_TYPE_PLATINUM:
                $examinationData['platinium'] = optional($typeOfMetalRef)->payment_for_unit * $dataValidated['quantity'];
                break;
            case ReferenceTable::REFERENCE_METAL_TYPE_GOLD:
                $examinationData['gold'] = optional($typeOfMetalRef)->payment_for_unit * $dataValidated['quantity'];
                break;
            case ReferenceTable::REFERENCE_METAL_TYPE_PALLADIUM:
                $examinationData['palladium'] = optional($typeOfMetalRef)->payment_for_unit * $dataValidated['quantity'];
                break;
            case ReferenceTable::REFERENCE_METAL_TYPE_30_PERCENT_SILVER:
                $examinationData['metal_with_silver'] = optional($typeOfMetalRef)->payment_for_unit * $dataValidated['quantity'];
                break;
            case ReferenceTable::REFERENCE_METAL_TYPE_SILVER:
                $examinationData['silver'] = optional($typeOfMetalRef)->payment_for_unit * $dataValidated['quantity'];
                break;
            case ReferenceTable::REFERENCE_METAL_TYPE_BIJOUTERIE:
                $examinationData['bijouterie'] = optional($typeOfMetalRef)->payment_for_unit * $dataValidated['quantity'];
                break;
        }

        $typeOfStones = explode(',', $dataValidated['type_of_stone']);

        if (!empty($dataValidated['type_of_stone'])) {

            foreach ($typeOfStones as $typeOfStone) {
                $valueOfStonesRef[] = optional(getReferenceRows(ReferenceTable::REFERENCE_TYPE_OF_STONES, $typeOfStone, false, ['value_of_stones']))->value_of_stones;
            }

            if (count($valueOfStonesRef ?? []) > 0) {
                foreach ($valueOfStonesRef as $valueOfStoneRef) {
                    $priceForStoneUnit[] = optional(getReferenceRows(ReferenceTable::REFERENCE_VALUE_OF_STONES, $valueOfStoneRef, false, ['payment_for_unit']))->payment_for_unit;
                }
                $examinationData['stones'] = max($priceForStoneUnit ?? []) * $dataValidated['quantity'];
            }

        }

        if (!is_null($dataValidated['carat']) && !is_null($dataValidated['piece']) && !empty($dataValidated['type_of_stone'])) {

            foreach ($typeOfStones as $typeOfStone) {
                $costForCaratRef[] = optional(getReferenceRows(ReferenceTable::REFERENCE_TYPE_OF_STONES, $typeOfStone, false, ['cost_for_determining_carat']))->cost_for_determining_carat;
            }

            if (count($costForCaratRef ?? []) > 0) {
                $examinationData['definition_of_carats_of_stones'] = max($costForCaratRef) * $dataValidated['piece'];
            }

        }

        unset($examinationData['total']);
        $examinationData['total'] = array_sum($examinationData);

        return $examinationData;

    }
}
