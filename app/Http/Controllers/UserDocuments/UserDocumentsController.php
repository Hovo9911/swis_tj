<?php

namespace App\Http\Controllers\UserDocuments;

use App\Exports\UserDocuments\UserDocumentsXlsExport;
use App\Http\Controllers\BaseController;
use App\Http\Requests\UserDocumentsRequest\UserDocumentsRequest;
use App\Http\Requests\UserDocumentsRequest\UserDocumentsStoreDefaultRequest;
use App\Models\Agency\Agency;
use App\Models\BaseModel;
use App\Models\ConstructorActions\ConstructorActions;
use App\Models\ConstructorActionSuperscribes\ConstructorActionSuperscribes;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorLists\ConstructorLists;
use App\Models\ConstructorMapping\ConstructorMapping;
use App\Models\ConstructorRules\ConstructorRules;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\DataModel\DataModel;
use App\Models\Document\Document;
use App\Models\Document\DocumentManager;
use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm;
use App\Models\DocumentsDatasetFromSaf\DocumentsDatasetFromSaf;
use App\Models\Instructions\Instructions;
use App\Models\Laboratory\Laboratory;
use App\Models\Menu\Menu;
use App\Models\Notifications\NotificationsManager;
use App\Models\NotifySubApplicationExpirationDate\NotifySubApplicationExpirationDateManager;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\RiskProfile\RiskProfile;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure;
use App\Models\SingleApplicationExaminations\SingleApplicationExaminations;
use App\Models\SingleApplicationExpertise\SingleApplicationExpertise;
use App\Models\SingleApplicationNotes\SingleApplicationNotes;
use App\Models\SingleApplicationObligations\SingleApplicationObligations;
use App\Models\SingleApplicationProducts\SingleApplicationProducts;
use App\Models\SingleApplicationProductsBatch\SingleApplicationProductsBatch;
use App\Models\SingleApplicationSubApplicationCertificates\SingleApplicationSubApplicationCertificates;
use App\Models\SingleApplicationSubApplicationInstructionFeedback\SingleApplicationSubApplicationInstructionFeedback;
use App\Models\SingleApplicationSubApplicationInstructions\SingleApplicationSubApplicationInstructions;
use App\Models\SingleApplicationSubApplicationProducts\SingleApplicationSubApplicationProducts;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplicationCompleteData;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use App\Models\SingleApplicationSubApplicationsLaboratory\SingleApplicationSubApplicationsLaboratoryManager;
use App\Models\SingleApplicationSubApplicationStates\SingleApplicationSubApplicationStates;
use App\Models\UserDocuments\UserDocuments;
use App\Models\UserDocuments\UserDocumentsManager;
use App\Models\UserDocuments\UserDocumentsSearch;
use App\Pdf\PdfManager;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Throwable;

/**
 * Class UserDocumentsController
 * @package App\Http\Controllers\UserDocuments
 */
class UserDocumentsController extends BaseController
{
    /**
     * @var UserDocumentsManager
     */
    protected $manager;

    /**
     * @var PdfManager
     */
    protected $pdfManager;

    /**
     * @var DocumentManager
     */
    protected $documentManager;

    /**
     * @var NotifySubApplicationExpirationDateManager
     */
    protected $notifySubApplicationExpirationDateManager;

    /**
     * UserDocumentsController constructor.
     *
     * @param UserDocumentsManager $userDocumentsManager
     * @param PdfManager $pdfManager
     * @param DocumentManager $documentManager
     * @param NotifySubApplicationExpirationDateManager $notifySubApplicationExpirationDateManager
     */
    public function __construct(UserDocumentsManager $userDocumentsManager, PdfManager $pdfManager, DocumentManager $documentManager, NotifySubApplicationExpirationDateManager $notifySubApplicationExpirationDateManager)
    {
        $this->manager = $userDocumentsManager;
        $this->pdfManager = $pdfManager;
        $this->documentManager = $documentManager;
        $this->notifySubApplicationExpirationDateManager = $notifySubApplicationExpirationDateManager;
    }

    /**
     * Function to return user document home page
     *
     * @param $lngCode
     * @param $documentId
     * @return View
     */
    public function index($lngCode, $documentId)
    {
        $dataSetSaf = DocumentsDatasetFromSaf::select('id', 'xml_field_name', 'title_trans_key', 'show_in_search_results', 'saf_field_id')->where('document_id', $documentId)->where('show_in_search_results', 1)->with('current')->get();
        $dataSetMdm = DocumentsDatasetFromMdm::select('id', 'field_id', 'mdm_field_id', 'show_in_search_results', 'field_id as orderFieldName')->where('document_id', $documentId)->where('show_in_search_results', 1)->with('current')->get();

        $constructorDocument = ConstructorDocument::select('id', 'constructor_documents_ml.document_name')->where(['id' => $documentId, 'company_tax_id' => Auth::user()->companyTaxId()])->joinMl()->firstOrFail();
        $documentMapping = ConstructorMapping::where('document_id', $documentId)->active()->get();

        $documentStates = collect();
        if ($documentMapping->isNotEmpty()) {
            $allDocumentStatesIds = array_unique(array_merge($documentMapping->pluck('end_state')->unique()->all(), $documentMapping->pluck('start_state')->unique()->all()));
            $documentStates = ConstructorStates::joinMl()->whereIn('id', $allDocumentStatesIds)->active()->get();
        }

        $lastXML = SingleApplicationDataStructure::lastXml();
        foreach ($dataSetSaf as $key => &$value) {
            $safFieldId = getSafFieldById($lastXML, $value->saf_field_id);
            $value['xml_field_name'] = str_replace(['][', '['], '_', $value['xml_field_name']);
            $value['xml_field_name'] = str_replace(']', '', $value['xml_field_name']);
            $value['tooltip_trans_key'] = is_null(optional($safFieldId[0])->attributes()->tooltipText) ? trans($value->title_trans_key) : optional($safFieldId[0])->attributes()->tooltipText;
            $value['orderFieldName'] = (string)optional($safFieldId[0])->attributes()->name;
        }

        $actionSuperscribe = ConstructorActions::whereIn('id', $documentMapping->pluck('action'))->where('special_type', ConstructorActions::SUPERSCRIBE_TYPE)->first();

        $usersSuperscribe = collect();
        if (!is_null($actionSuperscribe)) {
            $usersSuperscribe = ConstructorStates::getUsersByStateRoles(false, $documentId);
        }

        $activeRefAgencyRow = getReferenceRows(ReferenceTable::REFERENCE_AGENCY,false,Auth::user()->companyTaxId());

        return view('user-documents.index')->with([
            'documentId' => $documentId,
            'searchResultFields' => $dataSetSaf,
            'searchResultFieldsMdm' => $dataSetMdm,
            'searchBySuperscribeField' => (!is_null($actionSuperscribe)),
            'usersSuperscribe' => $usersSuperscribe,
            'searchFields' => UserDocuments::getSearchFields($documentId, $lastXML),
            'documentStates' => $documentStates,
            'documentName' => optional($constructorDocument)->document_name,
            'referenceWithAgencyColumn' => ReferenceTable::getReferenceWithAgencyColumn(),
            'activeRefAgencyRow' => $activeRefAgencyRow,
            'agencyId' => optional(Agency::select('id')->where('tax_id', Auth::user()->companyTaxId())->first())->id
        ]);
    }

    /**
     * Function to show all data in dataTable
     *
     * @param Request $request
     * @param UserDocumentsSearch $userDocumentsSearch
     * @return JsonResponse
     */
    public function table(Request $request, UserDocumentsSearch $userDocumentsSearch)
    {
        return response()->json($this->dataTableAll($request, $userDocumentsSearch));
    }

    /**
     * Function to return edit page with mappings
     *
     * @param $lngCode
     * @param $documentId
     * @param $applicationId
     * @param $viewType
     * @return View|RedirectResponse
     */
    public function edit($lngCode, $documentId, $applicationId, $viewType)
    {
        // @fixme
        ini_set('max_execution_time', 180);

        $constructorDocument = ConstructorDocument::select('id', 'constructor_documents_ml.document_name')
            ->joinMl()
            ->where(['id' => $documentId, 'company_tax_id' => Auth::user()->companyTaxId()])
            ->active()
            ->firstOrFail();

        $subApplication = SingleApplicationSubApplications::select([
            'saf_sub_applications.*',
            DB::raw("COALESCE(saf_sub_applications.data, saf.data) as data"),
            DB::raw("COALESCE(saf_sub_applications.custom_data, saf.custom_data) as custom_data")
        ])
            ->join('saf', 'saf.regular_number', '=', 'saf_sub_applications.saf_number')
            ->where('saf_sub_applications.id', $applicationId)
            ->myApplicationRoles()
            ->firstOrFail();

        $saf = SingleApplication::where('regular_number', $subApplication->saf_number)->firstOrFail();

        $roles = Auth::user()->getCurrentRoles(Menu::USER_DOCUMENTS_MENU_NAME);
        $documentCurrentState = SingleApplicationSubApplicationStates::with(['state'])->where('saf_subapplication_id', $applicationId)->orderByDesc()->firstOrFail();
        $documentMappings = UserDocuments::getDocumentMappings($documentId, $roles, $documentCurrentState, $applicationId);

        if ($documentMappings->count() < 1) {
            return redirect(urlWithLng("user-documents/{$documentId}"));
        }

        // Laboratory active states count start
        $mappingStates = [];
        if ($documentMappings) {

            $documentAllMappings = ConstructorMapping::select('start_state', 'end_state')->where('document_id', $documentId)
                ->whereIn('role', $roles)
                ->active()
                ->get();

            if ($documentAllMappings) {
                foreach ($documentAllMappings as $documentMapping) {
                    $mappingStates[] = $documentMapping->start_state;
                    $mappingStates[] = $documentMapping->end_state;
                }
                $mappingStates = array_unique($mappingStates);
            }
        }

        $laboratoryStatesCount = ConstructorStates::whereIn('id', $mappingStates)->active()
            ->where('state_type', ConstructorStates::LABORATORY_STATE_TYPE)
            ->count();

        $datasetFromMdm = DocumentsDatasetFromMdm::join('documents_dataset_from_mdm_ml', 'documents_dataset_from_mdm_ml.documents_dataset_from_mdm_id', '=', 'documents_dataset_from_mdm.id')->where('lng_id', cLng('id'))->where('document_id', $documentId)->orderBy('order', 'asc')->get();

        $mdmFields = DataModel::joinMl(['f_k' => 'data_model_id'])
            ->whereIn('id', $datasetFromMdm->pluck('mdm_field_id')->all())
            ->get()->keyBy('id');

        $mappingWithLists = ConstructorMapping::getMappingsWithLists($documentMappings);
        $lists = ConstructorLists::margeMappingLists($documentMappings, $mappingWithLists);
        $globalHiddenFields = UserDocuments::getGlobalHiddenElements($documentId);
        $fields = DataModel::getMdmFieldsType(getUserDocuments($datasetFromMdm, $mdmFields));
        $tabLists = ConstructorMapping::getTabLists($documentMappings);
        $tabLists = ConstructorLists::margeTabLists($documentMappings, $tabLists);

        $availableProductsIds = $subApplication->productList->pluck('id')->all();
        $availableProductsIdNumbers = $subApplication->productsOrderedList();
        $singleAppProducts = SingleApplicationProducts::whereIn('id', $availableProductsIds)->where(['saf_number' => $saf->regular_number])->get();

        if (!empty($availableProductsIds)) {
            $subApplicationData = $subApplication->data;
            $subApplicationData['saf']['product']['brutto_weight'] = $singleAppProducts->sum('brutto_weight');
            $subApplicationData['saf']['product']['netto_weight'] = $singleAppProducts->sum('netto_weight');
            $subApplicationData['saf']['product']['product_count'] = count($availableProductsIds);
            $subApplicationData['saf']['product']['total_value'] = $singleAppProducts->sum('total_value_national');
            $subApplicationData['saf']['product']['total_value_all'] = $singleAppProducts->sum('total_value');
        }

        // Attached Documents
        $attachedDocumentProducts = SingleApplicationSubApplications::getAttachedDocumentProducts($subApplication, $availableProductsIds);

        $stateHasSuperscribeMapping = ConstructorMapping::select('action')->join('constructor_actions', 'constructor_mapping.action', '=', 'constructor_actions.id')->active()->where('document_id', $documentId)
            ->where(['start_state' => $documentCurrentState->state_id, 'special_type' => ConstructorActions::SUPERSCRIBE_TYPE])->first();

        $userSuperscribeMapping = $documentMappings->where('action_type', ConstructorActions::SUPERSCRIBE_TYPE)->first();

        $superscribe = ConstructorActionSuperscribes::where([
            'constructor_document_id' => $documentId,
            'saf_subapplication_id' => $applicationId,
            'company_tax_id' => Auth::user()->companyTaxId(),
        ])->orderByDesc()->active()->first();

        $showUserMappingList = true;
        if (!is_null($stateHasSuperscribeMapping)) {
            if (is_null($superscribe) || (!is_null($superscribe) && $superscribe->to_user != Auth::id())) {
                $showUserMappingList = false;
            }
        }

        //
        $safGlobalDefaultData = $saf->getSafGlobalDefaultValue();

        $generateDataParams = [
            'saf' => $saf,
            'singleApplicationSubApp' => $subApplication,
            'laboratoryStatesCount' => $laboratoryStatesCount,
            'availableProductsIds' => $availableProductsIds,
            'availableProductsIdNumbers' => $availableProductsIdNumbers,
            'fields' => $fields,
            'globalHiddenFields' => $globalHiddenFields,
            'lists' => $lists,
            'mandatoryListForView' => ConstructorLists::generateMandatoryListForView($documentMappings),
            'hiddenTabs' => UserDocuments::getHiddensTabs($documentId, $lists, $globalHiddenFields, $fields),
            'showUserMappingList' => $showUserMappingList,
            'documentId' => $documentId,
            'tabLists' => $tabLists,
            'superscribe' => $superscribe,
            'stateHasSuperscribeMapping' => $stateHasSuperscribeMapping,
        ];

        $printActionOption = [];
        foreach ($documentMappings as $mapping) {
            $action = $mapping->rAction;

            if ($action->special_type != ConstructorActions::SUPERSCRIBE_TYPE && $action->special_type == ConstructorActions::PRINT_TYPE && $actionTemplates = $action->actionTemplates) {
                if (isset($actionTemplates) && count($actionTemplates) > 0) {
                    $printActionOption[trans('swis.user_documents.default_pdf')] = null;
                    foreach ($actionTemplates as $actionTemplate) {
                        if (isset($actionTemplate->template)) {
                            $printActionOption[$actionTemplate->template->currentMl->template_name] = $actionTemplate->template->code;
                        }
                    }
                }
            }
        }

        // Update first time edit set 1 and save edited user_id
        if (!$subApplication->is_edited) {
            SingleApplicationSubApplications::where('id', $subApplication->id)->update(['is_edited' => SingleApplication::TRUE, 'edited_user_id' => Auth::id()]);
        }

        // in active document mappings has lab document
        $labStateFromCurrentMappings = ConstructorStates::whereIn('id', $documentMappings->pluck('end_state')->all())->active()
            ->where('state_type', ConstructorStates::LABORATORY_STATE_TYPE)
            ->count();

        $hasLaboratoryState = false;
        if ($labStateFromCurrentMappings) {
            $hasLaboratoryState = true;
        }

        // Page Data
        $attachedDocumentExaminations = SingleApplicationExaminations::select('saf_examinations.*', 'saf_products.product_number', 'saf_products.commercial_description')
            ->leftJoin('saf_products', 'saf_products.id', '=', 'saf_examinations.saf_product_id')
            ->where('saf_subapplication_id', $applicationId)->active()->get();

        return view('user-documents.edit')->with([

            'data' => UserDocuments::getPageData($generateDataParams),

            'superscribe' => $superscribe,
            'userSuperscribeMapping' => $userSuperscribeMapping,

            'saf' => $saf,
            'safGlobalDefaultData' => $safGlobalDefaultData,
            'subApplication' => $subApplication,
            'attachedDocumentProducts' => $attachedDocumentProducts,
            'attachedDocumentExaminations' => $attachedDocumentExaminations,
            'availableProductsIdNumbers' => $availableProductsIdNumbers,

            'showUserMappingList' => $showUserMappingList,
            'documentMappings' => $documentMappings,
            'documentId' => $documentId,
            'applicationId' => $applicationId,
            'documentCurrentState' => $documentCurrentState,
            'documentName' => $constructorDocument->document_name,

            'laboratoriesData' => Laboratory::getLaboratoriesData($singleAppProducts, $subApplication->saf_number, $applicationId),
            'laboratoryStatesCount' => $laboratoryStatesCount,
            'hasLaboratoryState' => $hasLaboratoryState,
            'appInLaboratory' => $documentCurrentState->state->state_type == ConstructorStates::LABORATORY_STATE_TYPE,

            'printActionOption' => $printActionOption,
            'pdfHashKey' => $subApplication->pdf_hash_key,

            'digitalSignature' => ConstructorDocument::where('id', $documentId)->pluck('digital_signature')->first(),
            'notes' => SingleApplicationNotes::getApplicationNotes($applicationId),
            'isHaveMultipleObligation' => ConstructorMapping::isHaveMultipleObligation($documentId, $roles, $documentCurrentState),

            'saveMode' => $viewType,
        ]);
    }

    /**
     * Function to store data in db
     *
     * @param $lngCode
     * @param $documentId
     * @param $applicationId
     * @param UserDocumentsRequest $userDocumentsRequest
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function store($lngCode, $documentId, $applicationId, UserDocumentsRequest $userDocumentsRequest)
    {
        //@fixme
        $subApplication = SingleApplicationSubApplications::select('saf_sub_applications.id', 'saf_sub_applications.company_tax_id', 'saf_sub_applications.saf_number', 'saf_sub_applications.saf_id', 'data', 'custom_data', 'express_control_id', 'agency_subdivision_id')->where('saf_sub_applications.id', $applicationId)->myApplicationRoles()->firstOrFail();
        $dataFromFront = $userDocumentsRequest->all();

        // Check Document State is Changed
        $documentLastUpdate = $userDocumentsRequest->input('lastUpdate');
        $documentCurrentState = SingleApplicationSubApplicationStates::where('saf_subapplication_id', $applicationId)->orderByDesc()->firstOrFail();

        if ($documentLastUpdate != $documentCurrentState->getOriginal('created_at')) {
            $error['state_changed'] = true;

            return responseResult('INVALID_DATA', '', '', $error);
        }

        // Obligation check part
        $mapping = ConstructorMapping::select('id', 'editable_list', 'obligation_exist', 'enable_risks', 'end_state', 'applicant', 'importer_exporter', 'agency_admin', 'persons_in_next_state', 'digital_signature')->where('id', $userDocumentsRequest->input('mapping_id'))->first();

        if ($mapping->obligation_exist) {
            $safNotPayedObligations = SingleApplicationObligations::where(['subapplication_id' => $applicationId])
                ->where(function ($query) {
                    return $query->whereNull('payment')->orWhere('payment', '=', '0')->where('obligation', '>', '0');
                })->count();

            if ($safNotPayedObligations) {
                $error['obligation_exist'] = trans('swis.user_documents.obligation_exist.message');

                return responseResult('INVALID_DATA', '', '', $error);
            }
        }

        // Laboratory check part
        if (optional($mapping->endState)->state_type == ConstructorStates::LABORATORY_STATE_TYPE) {
            $expertiseCount = SingleApplicationExpertise::select('id')
                ->join('saf_expertise_indicators', function ($q) {
                    $q->on('saf_expertise_indicators.saf_expertise_id', '=', 'saf_expertise.id')->where('status_send', false);
                })
                ->where('sub_application_id', $applicationId)
                ->where('send_to_lab', 0)
                ->count();

            if ($expertiseCount < 1) {
                $error['laboratory_doesnt_exist'] = trans('swis.user_documents.laboratory_doesnt_exist.message');

                return responseResult('INVALID_DATA', '', '', $error);
            }
        }

        // Save batches tmp data
        $this->saveBatchesTmpData($dataFromFront);
        $this->saveTmpData($dataFromFront, $applicationId);

        $roles = Auth::user()->getCurrentRoles(Menu::USER_DOCUMENTS_MENU_NAME);
        $documentMappings = UserDocuments::getDocumentMappings($documentId, $roles, $documentCurrentState, $applicationId);
        $mappingWithLists = ConstructorMapping::getMappingsWithLists($documentMappings, false);
        $nonVisibleFields = ConstructorLists::margeMappingLists($documentMappings, $mappingWithLists, true);

        $saf = SingleApplication::select('id', 'user_id', 'regime', 'custom_data', 'company_tax_id', 'data', 'created_at')->where('regular_number', $subApplication->saf_number)->firstOrFail();
        $stateId = $userDocumentsRequest->input('state');
        $singleApplicationSubApp = SingleApplicationSubApplications::select('saf_sub_applications.*', 'saf.regime')->where('saf_sub_applications.id', $applicationId)->leftJoin('saf', 'saf.regular_number', '=', 'saf_sub_applications.saf_number')->first();
        $customData = (!empty($singleApplicationSubApp->custom_data)) ? $singleApplicationSubApp->custom_data : [];
        $dataFromFront['customData'] = array_merge($customData, $dataFromFront['customData'] ?? []);
        $safData = ['saf' => array_replace_recursive($dataFromFront['saf'] ?? [], $subApplication->data['saf'] ?? [])];
        $safControlledFields = $dataFromFront['saf_controlled_fields'] ?? [];
        $safDataForValidation = getJsonDataAsArray($safData, $dataFromFront['customData'], $singleApplicationSubApp->productListSelect->toArray(), $safControlledFields);

        // Editable Lists
        $isInstructionTabEditable = !is_null($mapping->editableTabs->where('tab_key', 'instructions')->first());
        if (!$isInstructionTabEditable) {
            unset($dataFromFront['instructions_feedback']);
        }

        list($dataFromFront['customData'], $safData) = ConstructorMapping::getOnlyEditableData($mapping, $dataFromFront);
        $dataFromFront['saf'] = isset($safData['saf']) ? $safData['saf'] : ['saf' => []];

        // Risk profile
        RiskProfile::generateInstructions($mapping, $safDataForValidation, $singleApplicationSubApp, $stateId, $documentId);

        // Validation Rules
        list($hasError, $errors) = UserDocuments::runValidationRules($userDocumentsRequest, $safDataForValidation, $singleApplicationSubApp, $mapping);

        if ($hasError) {
            return responseResult('INVALID_DATA', '', '', $errors);
        }

        // Check Digital Signature
        if ($mapping->digital_signature == ConstructorMapping::NEED_DIGITAL_SIGNATURE && config('global.digital_signature') && $userDocumentsRequest->input('needDigitalSignature') == 'true') {
            $errors['need_digital_signature'] = true;
            $errors['button'] = "#mappingButton{$mapping->id}-{$userDocumentsRequest->input('action')}-{$mapping->end_state}";

            return responseResult('INVALID_DATA', '', '', $errors);
        }

        // Rule part
        $freeSumObligation = isset($userDocumentsRequest->validated()['free_sum_obligation']) ? $userDocumentsRequest->validated()['free_sum_obligation'] : [];
        $notUpdatedFields = SingleApplicationObligations::generateObligation($subApplication, $singleApplicationSubApp, $mapping->id, $safDataForValidation, $stateId, $freeSumObligation);

        // State
        $state = ConstructorStates::select('state_type', 'is_countable')->where('id', $stateId)->first();

        if ($state->state_type == ConstructorStates::REQUEST_STATE_TYPE) {
            SingleApplicationSubApplications::where(['saf_number' => $subApplication->saf_number, 'id' => $applicationId])->update(['current_status' => SingleApplicationSubApplications::STATUS_REQUESTED]);
        }

        $subApplicationAccessToken = $this->getUserDocumentAccessToken($state, $singleApplicationSubApp->access_token);

        SingleApplicationSubApplications::where(['saf_number' => $subApplication->saf_number, 'id' => $applicationId])->update([
            'access_token' => $subApplicationAccessToken,
            'express_control_id' => isset($userDocumentsRequest['saf']['general']['sub_application_express_control_id']) ? $userDocumentsRequest['saf']['general']['sub_application_express_control_id'] : $subApplication->express_control_id,
            'agency_subdivision_id' => isset($userDocumentsRequest['saf']['general']['sub_application_agency_subdivision_id']) ? $userDocumentsRequest['saf']['general']['sub_application_agency_subdivision_id'] : $subApplication->agency_subdivision_id,
        ]);

        // If status is canceled save current time pdf with current data in files and in db file_name
        if ($state->state_type == ConstructorStates::CANCELED_STATE_TYPE) {
            SingleApplicationSubApplications::where(['saf_number' => $subApplication->saf_number, 'id' => $applicationId])->update(['current_status' => SingleApplicationSubApplications::STATUS_CANCELED]);

            $subApplicationFileName = 'certificate_' . randomNumberDigits(16) . '.pdf';

            SingleApplicationSubApplicationCertificates::create([
                'sub_application_id' => $applicationId,
                'user_id' => Auth::id(),
                'file_name' => $subApplicationFileName,
                'sub_application_status' => ConstructorStates::CANCELED_STATE_TYPE
            ]);

        }

        //  Document part(electronic)
        if ($state->state_type == ConstructorStates::END_STATE_TYPE) {
            $this->documentManager->storeElectronicDocument($applicationId, $stateId, $subApplicationAccessToken);
        } else {
            Document::where('sub_application_id', $applicationId)->update(['show_status' => Document::STATUS_INACTIVE]);
        }

        // Laboratory state
        if ($state->state_type == ConstructorStates::LABORATORY_STATE_TYPE) {
            SingleApplicationExpertise::where(['saf_number' => $subApplication->saf_number, 'send_to_lab' => SingleApplicationExpertise::STATUS_DELETED])->update(['send_to_lab' => SingleApplicationExpertise::STATUS_ACTIVE]);
        }

        $subAppData = [
            'user_id' => Auth::id(),
            'saf_number' => $subApplication->saf_number,
            'saf_id' => $subApplication->saf_id,
            'saf_subapplication_id' => $applicationId,
            'mapping_id' => $mapping->id,
            'state_id' => $stateId,
            'is_current' => '1'
        ];

        DB::transaction(function () use ($subApplication, $dataFromFront, $subAppData, $customData, $nonVisibleFields, $notUpdatedFields, $singleApplicationSubApp, $state, $documentId, $isInstructionTabEditable) {

            SingleApplicationSubApplicationStates::where(['saf_number' => $subApplication->saf_number, 'saf_subapplication_id' => $singleApplicationSubApp->id])->update(['is_current' => '0']);

            SingleApplicationSubApplicationStates::store($subAppData, $dataFromFront, ($state->state_type == ConstructorStates::CANCELED_STATE_TYPE));

            $this->manager->updateStoreData($dataFromFront, $singleApplicationSubApp, $customData, $nonVisibleFields, $notUpdatedFields, $state, $documentId, false, $isInstructionTabEditable);

            // Start notify about sub application expiration date
            if ($state->state_type == ConstructorStates::REQUEST_STATE_TYPE) {

                $notifiedData = [
                    'subapplication_id' => $singleApplicationSubApp->id,
                    'agency_id' => $singleApplicationSubApp->agency_id,
                    'document_id' => $documentId
                ];

                $this->notifySubApplicationExpirationDateManager->updateData($notifiedData);
            }

        });

        // Notification part
        UserDocuments::sendNotifications($mapping, $saf, $documentId, $applicationId, $userDocumentsRequest);

        // -----------
        if (Auth::user()->isLocalAdmin()) {
            return responseResult('OK', back()->getTargetUrl());
        }

        $needRoles = ConstructorMapping::where(['document_id' => $documentId, 'start_state' => $stateId])->pluck('role')->unique()->all();
        $userHasRole = array_intersect($needRoles, Auth::user()->getCurrentRoles(Menu::USER_DOCUMENTS_MENU_NAME));

        if (count($userHasRole) > 0) {
            return responseResult('OK', back()->getTargetUrl());
        }

        return responseResult('OK', urlWithLng('user-documents/' . $documentId));
    }

    /**
     * Function to store data into db as default save
     *
     * @param $lngCode
     * @param $documentId
     * @param $applicationId
     * @param UserDocumentsStoreDefaultRequest $userDocumentsStoreDefaultRequest
     * @return JsonResponse
     */
    public function storeDefault($lngCode, $documentId, $applicationId, UserDocumentsStoreDefaultRequest $userDocumentsStoreDefaultRequest)
    {
        $subApplication = SingleApplicationSubApplications::select('saf_sub_applications.id', 'saf_sub_applications.company_tax_id', 'saf_sub_applications.saf_number', 'saf_sub_applications.saf_id', 'data', 'custom_data', 'express_control_id', 'agency_subdivision_id')->where('saf_sub_applications.id', $applicationId)->myApplicationRoles()->firstOrFail();

        $dataFromFront = $userDocumentsStoreDefaultRequest->all();

        $this->saveBatchesTmpData($dataFromFront);

        $documentCurrentState = SingleApplicationSubApplicationStates::select('state_id')->where('saf_subapplication_id', $applicationId)->orderByDesc()->firstOrFail();

        $roles = Auth::user()->getCurrentRoles(Menu::USER_DOCUMENTS_MENU_NAME);
        $documentMappings = UserDocuments::getDocumentMappings($documentId, $roles, $documentCurrentState, $applicationId);
        $mappingWithLists = ConstructorMapping::getMappingsWithLists($documentMappings, false);
        $nonVisibleFields = ConstructorLists::margeMappingLists($documentMappings, $mappingWithLists, true);


        $singleApplicationSubApp = SingleApplicationSubApplications::select('saf_sub_applications.*', 'saf.regime')->where('saf_sub_applications.id', $applicationId)->leftJoin('saf', 'saf.regular_number', '=', 'saf_sub_applications.saf_number')->first();
        $dataFromFront['customData'] = array_merge($singleApplicationSubApp->custom_data ?? [], $dataFromFront['customData'] ?? []);

        $singleApplicationSubApp->express_control_id = isset($userDocumentsStoreDefaultRequest['saf']['general']['sub_application_express_control_id']) ? $userDocumentsStoreDefaultRequest['saf']['general']['sub_application_express_control_id'] : $subApplication->express_control_id;
        $singleApplicationSubApp->agency_subdivision_id = isset($userDocumentsStoreDefaultRequest['saf']['general']['sub_application_agency_subdivision_id']) ? $userDocumentsStoreDefaultRequest['saf']['general']['sub_application_agency_subdivision_id'] : $subApplication->agency_subdivision_id;
        $singleApplicationSubApp->save();

        $this->saveBatchesTmpData($dataFromFront);
        $this->saveTmpData($dataFromFront, $applicationId);

        DB::transaction(function () use ($dataFromFront, $nonVisibleFields, $singleApplicationSubApp, $documentId, $subApplication, $documentCurrentState) {
            if ((!empty($dataFromFront['customData']) || !empty($dataFromFront['saf']))) {
                $dataCustomData = $dataFromFront['customData'] ?? [];
                $customData = $this->manager->setNonVisibleFieldsValues($singleApplicationSubApp->custom_data ?? [], $nonVisibleFields, $dataCustomData);

                $this->manager->updateColumnFields($customData, $documentId, $singleApplicationSubApp);

                $subApplicationData = $singleApplicationSubApp->data;
                if (isset($dataFromFront['saf']['transportation']['state_number_customs_support'], $subApplicationData['saf']['transportation']['state_number_customs_support'])) {
                    $stateNumberCustomsSupport = $dataFromFront['saf']['transportation']['state_number_customs_support'];
                    unset($dataFromFront['saf']['transportation']['state_number_customs_support']);
                    $i = 1;
                    foreach ($subApplicationData['saf']['transportation']['state_number_customs_support'] as &$item) {
                        $item['track_container_temperature'] = $stateNumberCustomsSupport[$i]['track_container_temperature'] ?? '';
                        $i++;
                    }
                }

                // ---------------- Save Free Sum Obligations --------------- //

                if (isset($dataFromFront['free_sum_obligation']) && count($dataFromFront['free_sum_obligation']) > 0) {

                    $obligationBudgetLineTable = ReferenceTable::REFERENCE_OBLIGATION_BUDGET_LINE;
                    $obligationCreationTypeTable = ReferenceTable::REFERENCE_OBLIGATION_CREATION_TYPE;

                    $freeSubObligationCode = DB::table($obligationBudgetLineTable)
                        ->select("{$obligationBudgetLineTable}.id", "{$obligationBudgetLineTable}.code")
                        ->join($obligationCreationTypeTable, "{$obligationCreationTypeTable}.id", "=", "{$obligationBudgetLineTable}.obligation_creation_type")
                        ->where("{$obligationCreationTypeTable}.code", ConstructorRules::CREATION_TYPE_FREE_SUM_CODE)
                        ->first();

                    foreach ($dataFromFront['free_sum_obligation'] as $freeSum) {
                        $obligationData[] = [
                            'user_id' => Auth::id(),
                            'saf_number' => $subApplication->saf_number,
                            'state_id' => $documentCurrentState->state_id,
                            'company_tax_id' => $subApplication->company_tax_id,
                            'creator_company_tax_id' => optional($subApplication->saf)->company_tax_id,
                            'subapplication_id' => $subApplication->id,
                            'budget_line' => $freeSum['budget_line'],
                            'free_sum_budget_line_code' => $freeSum['obligation_type'],
                            'ref_budget_line_id' => $freeSum['obligation_type'],
                            'is_free_sum' => true,
                            'obligation_code' => $freeSubObligationCode->code,
                            'obligation' => $freeSum['obligation'],
                            'payment' => 0,
                            'created_at' => currentDateTime(),
                            'pdf_hash_key' => str_random(64)
                        ];
//                      $this->sendObligationNotification(Auth::id(), $subApplication->company_tax_id, $singleApplicationSubApp->document_number, $singleApplicationSubApp);
                    }

                    SingleApplicationObligations::insert($obligationData);
                }

                // ---------------- Save Free Sum Obligations --------------- //


                // check if tab is editable update instruction id to active (if have not active id don't update)
                $getSubApplicationInstructions = SingleApplicationSubApplicationInstructions::where('sub_application_id', $singleApplicationSubApp->id)->get();
                foreach ($getSubApplicationInstructions as $subAppInstruction) {
                    $getActiveInstruction = Instructions::where('code', $subAppInstruction->instruction_code)->where('show_status', BaseModel::STATUS_ACTIVE)->orderByDesc()->first();

                    if (!is_null($getActiveInstruction) && $getActiveInstruction->id != $subAppInstruction->instruction_id) {

                        SingleApplicationSubApplicationInstructionFeedback::where('saf_sub_application_instruction_id', $subAppInstruction->id)->where('instruction_id', $subAppInstruction->instruction_id)->update(['instruction_id' => $getActiveInstruction->id]);

                        $subAppInstruction->instruction_id = $getActiveInstruction->id;
                        $subAppInstruction->save();
                    }
                }

                if (!empty($dataFromFront['instructions_feedback'])) {
                    $subApplicationInstructions = SingleApplicationSubApplicationInstructions::select('id', 'instruction_id', 'instruction_code')->whereIn('id', array_keys($dataFromFront['instructions_feedback']))->get()->keyBy('id')->toArray();

                    foreach ($dataFromFront['instructions_feedback'] as $subApplicationInstructionId => $feedback) {
                        $getFeedback = getReferenceRows(ReferenceTable::REFERENCE_RISK_FEEDBACK, $feedback['feedback']);

                        SingleApplicationSubApplicationInstructionFeedback::updateOrCreate([
                            'saf_sub_application_instruction_id' => $subApplicationInstructionId,
                            'instruction_id' => $subApplicationInstructions[$subApplicationInstructionId]['instruction_id'],
                        ], [
                            'instruction_code' => $subApplicationInstructions[$subApplicationInstructionId]['instruction_code'],
                            'feedback_id' => $feedback['feedback'],
                            'note' => $feedback['note'],
                            'feedback_code' => isset($getFeedback) ? $getFeedback->code : null
                        ]);
                    }
                }

                $safNote = $dataFromFront['saf']['notes']['notes'] ?? null;

                //--------------------- SAF notes part ---------------------//

                if ($safNote) {
                    $noteTransKey = SingleApplicationNotes::NOTE_TRANS_KEY_SUB_APP_SAVE_NOT;
                    if (isset($dataFromFront['saf']['notes']['to_saf_holder'])) {
                        $noteTransKey = SingleApplicationNotes::NOTE_TRANS_KEY_SEND_FROM_SUB_APP_TO_SAF;
                    }

                    $note = $safNote;
                    $toSafHolder = isset($dataFromFront['saf']['notes']['to_saf_holder']) ? SingleApplicationNotes::TRUE : SingleApplicationNotes::FALSE;

                    $noteData = [
                        'saf_number' => $singleApplicationSubApp->saf_number,
                        'note' => $note,
                        'sub_application_id' => $singleApplicationSubApp->id,
                        'to_saf_holder' => $toSafHolder,
                        'send_from_module' => SingleApplicationNotes::NOTE_SEND_MODULE_APPLICATION
                    ];

                    SingleApplicationNotes::storeNotes($noteData, $noteTransKey);
                    if ($toSafHolder) {
                        NotificationsManager::storeSafSubApplicationMessages($noteData, 'sub-application', $documentId);
                    }

                    unset($dataFromFront['saf']['notes']);
                }

                $dataFromFront['saf'] = array_replace_recursive($subApplicationData['saf'] ?? [], $dataFromFront['saf'] ?? []);
                $safData['saf'] = $dataFromFront['saf'];
                $safData = json_encode($safData);

                $updatedInfo = ['custom_data' => json_encode($customData), 'data' => $safData];

                SingleApplicationSubApplications::where('id', $singleApplicationSubApp->id)->update($updatedInfo);

                if (isset($dataFromFront['lab_examination']) && !empty($dataFromFront['lab_examination'])) {
                    $subApplicationLaboratoryManger = new SingleApplicationSubApplicationsLaboratoryManager();
                    $subApplicationLaboratoryManger->store($dataFromFront['lab_examination'], $singleApplicationSubApp->id);
                }

                // update All data
                $subApplication = SingleApplicationSubApplications::select('id', 'saf_number', 'data', 'custom_data', 'routings', 'routing_products', 'constructor_document_id')->with(['saf:id,saf_controlled_fields,regular_number', 'productList'])->where('id', $singleApplicationSubApp->id)->first();

                $singleApplicationSubApplicationCompleteData = new SingleApplicationSubApplicationCompleteData();
                $singleApplicationSubApplicationCompleteData->getParams();
                $singleApplicationSubApplicationCompleteData->setApplicationVariable($subApplication);

                list($safData, $productData, $productBatches) = $singleApplicationSubApplicationCompleteData->setData();
                $subApplication->all_data = json_encode(['data' => $safData, 'products' => array_values($productData), 'batches' => array_values($productBatches)]);
                $subApplication->save();
            }

            if (!empty($dataFromFront['batch']) && isset($dataFromFront['product_batches']) && count($dataFromFront['product_batches']) > 0) {
                $safListsControlledFields = $this->manager->checkNonVisibleBatchFields(reset($dataFromFront['product_batches']));

                foreach ($dataFromFront['batch'] as $productId => $batches) {
                    foreach ($batches as $batch) {
                        if (isset($dataFromFront['product_batches'])) {

                            $getBatch = SingleApplicationProductsBatch::where('id', $batch['id'])->first();
                            $mdmData = is_null($getBatch->mdm_data) ? [] : $getBatch->mdm_data;
                            $dataFromView = isset($dataFromFront['mdmData']) ? $dataFromFront['mdmData'] : [];
                            $getBatch->mdm_data = array_replace_recursive($mdmData, $dataFromView);
                            $getBatch->save();
                            SingleApplicationProductsBatch::where('id', $batch['id'])->update(array_merge($safListsControlledFields, $dataFromFront['product_batches'][$batch['id']]));
                        }
                    }
                }
            }
        });

        return responseResult('OK', urlWithLng("user-documents/{$documentId}/application/{$applicationId}/edit"));
    }

    /**
     * Function to store tmp data
     *
     * @param $data
     * @param array $tmpData
     */
    private function saveBatchesTmpData($data, $tmpData = [])
    {
        if (isset($data['product_batches']) && count($data['product_batches']) > 0) {
            foreach ($data['product_batches'] as $id => $item) {
                $tmpData[$id] = $item;
            }
        }

        if (isset($data['mdmData']) && count($data['mdmData']) > 0) {
            foreach ($data['mdmData'] as $id => $item) {
                if (array_key_exists($id, $data['mdmData'])) {
                    $tmpData[$id] = $tmpData[$id] + $item;
                } else {
                    $tmpData[$id] = $item;
                }
            }
        }

        foreach ($tmpData as $id => $item) {
            SingleApplicationProductsBatch::where('id', $id)->update(['tmp_data' => json_encode($item)]);
        }
    }

    /**
     * Function to store tmp data
     *
     * @param $data
     * @param $id
     */
    private function saveTmpData($data, $id)
    {
        $tmpData = [];
        if (isset($data['instructions_feedback']) && count($data['instructions_feedback']) > 0) {
            $tmpData['instructions_feedback'] = $data['instructions_feedback'];
        }
        if (isset($data['lab_examination']) && count($data['lab_examination']) > 0) {
            $tmpData['lab_examination'] = $data['lab_examination'];
        }
        if (isset($data['customData']) && count($data['customData']) > 0) {
            $tmpData['custom_data'] = $data['customData'];
        }
        SingleApplicationSubApplications::where('id', $id)->update(['tmp_data' => json_encode($tmpData)]);
    }

    /**
     * Function to unset unnecessary fields
     *
     * @param $updatedFields
     * @param $data
     * @return array
     */
    private function setOnlyUpdatedSafFields($updatedFields, $data)
    {
        foreach ($data['saf'] as $safLevelKey => $safLevel) {
            if (is_array($safLevel)) {
                foreach ($safLevel as $tabLevelKey => $tabLevel) {
                    if (is_array($tabLevel)) {
                        foreach ($tabLevel as $blockLevelKey => $blockLevel) {
                            if (is_array($blockLevel)) {
                                foreach ($blockLevel as $subBlockKey => $subBlock) {
                                    if (!in_array($subBlockKey, $updatedFields)) {
                                        unset($data['saf'][$safLevelKey][$tabLevelKey][$blockLevelKey][$subBlockKey]);
                                    }
                                }
                            } else {
                                if (!in_array($blockLevelKey, $updatedFields)) {
                                    unset($data['saf'][$safLevelKey][$tabLevelKey][$blockLevelKey]);
                                }
                            }
                        }
                    } else {
                        if (!in_array($tabLevelKey, $updatedFields)) {
                            unset($data['saf'][$safLevelKey][$tabLevelKey]);
                        }
                    }
                }
            }
        }

        foreach ($data['product_batches'] as $batchId => $productBatch) {
            foreach ($productBatch as $field => $batch) {
                if (!in_array($field, $updatedFields)) {
                    unset($data['product_batches'][$batchId][$field]);
                }
            }
        }

        return $data;
    }

    /**
     * Function to regenerate access token
     *
     * @param Request $request
     * @return JsonResponse
     */
    // NOT USED FOR NOW
    public function reGenerateAccessToken(Request $request)
    {
        return;

        $this->validate($request, [
            'applicationId' => 'required|exists:saf_sub_applications,id'
        ]);
        $subApp = SingleApplicationSubApplications::find($request->input('applicationId'));

        $saf = SingleApplication::select('id')->where('regular_number', $subApp->saf_number)->safOwnerOrCreator()->first();

        if (is_null($subApp->access_token) || is_null($saf)) {
            return responseResult('INVALID_DATA', '', '', []);
        }

        $oldAccessToken = $subApp->access_token;

        $state = (object)[];
        $state->state_type = ConstructorStates::END_STATE_TYPE;
        $accessToken = $this->getUserDocumentAccessToken($state, $oldAccessToken);

        $subApp->access_token = $accessToken;
        $subApp->save();

        return responseResult('OK', '', ['token' => $accessToken]);
    }

    /**
     * Function to generate unique access token
     *
     * @param $state
     * @param $previousToken
     * @param null $accessToken
     * @return string
     */
    public function getUserDocumentAccessToken($state, $previousToken, $accessToken = null)
    {
        if ($state->state_type == ConstructorStates::END_STATE_TYPE) {
            if (!empty($previousToken)) {
                $accessToken = $previousToken;
            } else {

                $accessToken = Document::DOCUMENT_TYPE_PREFIX_ELECTRONIC_DOCUMENT . '' . Str::random(25);

                $subApplication = SingleApplicationSubApplications::select('id')->where('access_token', $accessToken)->first();
                $document = Document::select('id')->where('document_access_password', $accessToken)->orWhere('uuid', $accessToken)->first();

                if (!is_null($subApplication) || !is_null($document)) {
                    $this->getUserDocumentAccessToken($state, $previousToken);
                }
            }
        }

        return $accessToken;
    }

    /**
     * Function to return saf products
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function safProducts(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer_with_max',
        ]);

        $products = SingleApplicationProductsBatch::select('id', 'batch_number')->where('saf_product_id', $request->input('id'))->get();

        return response()->json($products);
    }

    /**
     * Function to get reference
     *
     * @param $lngCode
     * @param $reference
     * @param Request $request
     * @return JsonResponse
     */
    public function reference($lngCode, $reference, Request $request)
    {
        $this->validate($request, [
            'q' => 'required|string_with_max'
        ]);

        $query = $request->get('q');
        $refTableName = "reference_{$reference}";
        $refTableNameML = "{$refTableName}_ml";

        $referenceData = DB::table($refTableName)->select('id', DB::raw("CONCAT(code, ' , ', name) as name"), "{$refTableNameML}.long_name")
            ->join($refTableNameML, "{$refTableName}.id", "=", "{$refTableNameML}.{$refTableName}_id")
            ->where("lng_id", cLng('id'))
            ->where(function ($q) use ($query, $refTableName, $refTableNameML) {
                $q->orWhere("{$refTableNameML}.name", "ILIKE", "%{$query}%")
                    ->orWhere("{$refTableName}.code", "ILIKE", "%{$query}%");
            })
            ->where($refTableName . '.show_status', UserDocuments::STATUS_ACTIVE)
            ->get();

        return response()->json(['items' => $referenceData]);
    }

    /**
     * Function to generate pdf
     *
     * @param $lngCode
     * @param $documentId
     * @param $subApplicationId
     * @param $template
     * @param null $productsIds
     * @param null $pdfHashKey
     *
     * @throws Throwable
     */
    public function generatePdf($lngCode, $documentId, $subApplicationId, $pdfHashKey = null, $template = null, $productsIds = null)
    {
        $this->pdfManager->generatePdf($documentId, $subApplicationId, $template, $productsIds, $pdfHashKey);
    }

    /**
     * Function to generate pdf without login
     *
     * @param $lngCode
     * @param $subApplicationId
     * @param $pdfHashKey
     * @param null $documentId
     * @param null $template
     * @param null $productsIds
     *
     * @throws Throwable
     */
    public function generateGuestPdf($lngCode, $subApplicationId, $pdfHashKey, $documentId = null, $template = null, $productsIds = null)
    {
        $this->pdfManager->generatePdf($documentId, $subApplicationId, $template, $productsIds, $pdfHashKey);
    }

    /**
     * Function to render sub application products modal
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function renderSubApplicationProducts(Request $request)
    {
        $safNumber = $request->header('sNumber');
        $subApplicationId = $request->header('applicationId');
        $productColumns = ['id', 'product_number', 'product_code', 'product_description', 'commercial_description'];

        $saf = SingleApplication::select('id', 'regular_number')
            ->where('regular_number', $safNumber)
            ->when(!$subApplicationId, function ($q) {
                $q->safOwnerOrCreator();
            })
            ->firstOrFail();

        if (is_null($subApplicationId)) {

            $products = SingleApplicationProducts::select($productColumns)->where('saf_number', $saf->regular_number)->get();
            $productsIdNumbers = $saf->productsOrderedList();

        } else {

            $subApplication = SingleApplicationSubApplications::where('saf_sub_applications.id', $subApplicationId)->firstOrFail();
            $subApplicationProductIds = SingleApplicationSubApplicationProducts::where('saf_sub_application_products.subapplication_id', $subApplicationId)->pluck('product_id');

            $products = SingleApplicationProducts::select($productColumns)->where('saf_number', $saf->regular_number)->whereIn('id', $subApplicationProductIds)->get();
            $productsIdNumbers = $subApplication->productsOrderedList();
        }

        $data['html'] = view('user-documents.products.sub-application-add-products-table')->with([
            'data' => $products,
            'columns' => $productColumns,
            'productsIdNumbers' => $productsIdNumbers,
            'viewType' => 'edit'
        ])->render();

        return responseResult('OK', '', $data);

    }

    /**
     * Function to return application only view
     *
     * @param $lngCode
     * @param Request $request
     * @return View
     */
    public function subApplicationInfo($lngCode, Request $request)
    {
        if (empty($request->input('access_token')) || empty($request->input('id'))) {
            abort(404);
        }

        $accessToken = $request->input('access_token');
        $subApplicationNumber = $request->input('id');

        $subApplication = SingleApplicationSubApplications::select('id', 'reference_classificator_id', 'pdf_hash_key', 'saf_number', 'document_number', 'status_date', 'document_code', 'constructor_document_id', 'custom_data')
            ->where('access_token', $accessToken)
            ->where('document_number', $subApplicationNumber)
            ->firstOrFail();

        $refClassificator = getReferenceRows(ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS, $subApplication->reference_classificator_id, false, ['id', 'code', 'name', 'long_name']);

        $subApplication->name = $refClassificator->name ?? '';
        $subApplication->long_name = $refClassificator->long_name ?? '';
        $subApplication->type = $refClassificator->code ?? '';
        $subApplication->state = !empty($subApplication->applicationCurrentState->state) && !empty($subApplication->applicationCurrentState->state->current()) ? $subApplication->applicationCurrentState->state->current()->state_name : '';
        $subApplication->current_state_date = !empty($fieldIdOfPermissionDate) && !empty($subApplication->custom_data[$fieldIdOfPermissionDate]) ? $subApplication->custom_data[$fieldIdOfPermissionDate] : '';

        $notes = SingleApplicationNotes::getApplicationNotes($subApplication->id);

        return view('user-documents.sub-application-docs', compact('subApplicationNumber', 'subApplication', 'notes'));
    }

    /**
     * Function to generate xls
     *
     * @param Request $request
     * @return BinaryFileResponse
     */
    public function generateXls(Request $request)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);

        $userDocumentsXlsExport = new UserDocumentsXlsExport();

        $searchParams = $userDocumentsXlsExport->getXlsFormSearchParams($request->all());

        $data = $this->dataTableSearchDataXlsReport($request->all());

        $userDocumentsXlsExport->setData($data, $searchParams);
        ob_end_clean();
        ob_start();
        return Excel::download($userDocumentsXlsExport, "user-documents-report-xls.xlsx");
        ob_flush();
    }

    /**
     * Function to get datatable search result
     *
     * @param $data
     * @return \array[][]
     */
    private function dataTableSearchDataXlsReport($data)
    {
        $lastXml = SingleApplicationDataStructure::lastXml();

        $userDocumentsSearch = new UserDocumentsSearch();

        $query = $userDocumentsSearch->callToConstructorQuery($data);
//        $returnArray = DB::select($query->toSql(), $query->getBindings());

        $returnArray = $query->get();

        $result = [
            'applications' => [
                'columns' => ['state' => trans('swis.constructor_document.state.label'), 'superscribe_field' => trans('swis.constructor_document.superscription.label')],
                'data' => []
            ],
            'products' => ['columns' => [], 'data' => []]
        ];
        $documentId = request()->route('id');

        $searchResultFieldsSaf = DocumentsDatasetFromSaf::select('xml_field_name', 'saf_field_id')->where('document_id', $documentId)->where('show_in_search_results', 1)->with('current')->get();

        $fieldNames = [];
        foreach ($searchResultFieldsSaf as $item) {
            $result['applications']['columns'][str_replace(']', '', str_replace(['][', '['], '_', $item->xml_field_name))] = optional($item->current)->label ?? trans(optional(getSafFieldById($lastXml, $item->saf_field_id)[0])->attributes()->title ?? '-');
            $fieldNames[] = [
                'field' => str_replace(']', '', str_replace(['][', '['], '_', $item->xml_field_name)),
                'fieldId' => $item->saf_field_id
            ];
        }

        $dataSetMdm = DocumentsDatasetFromMdm::select('id', 'field_id', 'mdm_field_id')->where(['show_in_search_results' => true, 'document_id' => $documentId])->with('current')->get();

        foreach ($dataSetMdm as $field) {
            $result['applications']['columns'][$field->field_id] = optional($field->current)->label ?? '-';
            $fieldNames[] = ['field' => $field->field_id];
        }

        $result['applications']['data'] = $userDocumentsSearch->generateResult($returnArray, $fieldNames, $data);

        $products = [];
        $productColumns['sub_application_number'] = [
            'label' => trans("swis.user_documents.saf_sub_application_number"),
            'mdm' => 0
        ];

        $elements = $lastXml->xpath("//*[@name='swis.single_app.products_list']");
        if (isset($elements[0])) {
            foreach ($elements[0]->fieldset as $element) {
                if (count($element->field) <= 1) {
                    $productColumns[(string)$element->field->attributes()->name] = [
                        'label' => trans((string)optional($element->field->attributes())->title ?? ''),
                        'mdm' => (int)optional($element->field->attributes())->mdm
                    ];
                } else {
                    foreach ($element->field as $field) {
                        $productColumns[(string)$field->attributes()->name] = [
                            'label' => trans((string)optional($field->attributes())->title ?? ''),
                            'mdm' => (int)optional($field->attributes())->mdm
                        ];

                        if ((string)optional($field->attributes())->id == SingleApplication::SAF_PLACES_TYPE_ID) {
                            $productColumns[(string)$field->attributes()->name]['label'] = trans('swis.user_documents.xls.places_type.column');
                        }
                    }
                }
            }
        }

        $dataModel = DataModel::allData();
        $refHsCodeAllData = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_HS_CODE_6);
        $refCountriesAllData = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_COUNTRIES);
        $refPackageAllData = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_PACKAGING_REFERENCE);
        $refCurrenciesAllData = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_CURRENCIES_TABLE_NAME);
        $refMeasurementUnitsAllData = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT);

        foreach ($returnArray as $application) {
            foreach ($application->productListInSearch as $product) {
                $tmpProduct = [];
                foreach ($productColumns as $field => $item) {
                    if ($field == 'sub_application_number') {
                        $tmpProduct[$field] = $application->document_number;
                    } else {
                        if (isset($dataModel[$item['mdm']], $dataModel[$item['mdm']]['reference'])) {
                            if ($dataModel[$item['mdm']]['reference'] == ReferenceTable::REFERENCE_HS_CODE_6_TABLE_NAME) {
                                /*  $refData = getReferenceRows(ReferenceTable::REFERENCE_HS_CODE_6_TABLE_NAME, $product->{$field}, false, ['id', 'code', 'name', 'long_name']);
                                  if (!is_null($refData)) {
                                      $tmpProduct[$field] = "({$refData->code}) {$refData->long_name}" ?? '';
                                  }*/

                                if (isset($refHsCodeAllData[$product->{$field}])) {
                                    $tmpProduct[$field] = "(".$refHsCodeAllData[$product->{$field}]['code'].') '. $refHsCodeAllData[$product->{$field}]['path_name'];
                                }
                            } else {

                                switch ($dataModel[$item['mdm']]['reference']) {
                                    case ReferenceTable::REFERENCE_COUNTRIES_TABLE_NAME:

                                        if (isset($refCountriesAllData[$product->{$field}])) {
                                            $refData = (object)$refCountriesAllData[$product->{$field}];
                                        }

                                        break;

                                    case ReferenceTable::REFERENCE_PACKAGING_REFERENCE_TABLE_NAME:

                                        if (isset($refPackageAllData[$product->{$field}])) {
                                            $refData = (object)$refPackageAllData[$product->{$field}];
                                        }

                                        break;
                                    case ReferenceTable::REFERENCE_CURRENCIES_TABLE_NAME:

                                        if (isset($refCurrenciesAllData[$product->{$field}])) {
                                            $refData = (object)$refCurrenciesAllData[$product->{$field}];
                                        }

                                        break;
                                    case ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT_TABLE_NAME:

                                        if (isset($refMeasurementUnitsAllData[$product->{$field}])) {
                                            $refData = (object)$refMeasurementUnitsAllData[$product->{$field}];
                                        }

                                        break;

                                    default :
                                        $refData = getReferenceRows($dataModel[$item['mdm']]['reference'], $product->{$field}, false, ['id', 'code', 'name']);
                                }

                                $tmpProduct[$field] = $refData->name ?? '';
                            }
                        } else {
                            $tmpProduct[$field] = $product->{$field};
                        }
                    }
                }

                $products[] = $tmpProduct;
            }
        }

        $result['products']['columns'] = $productColumns;
        $result['products']['data'] = $products;

        return $result;
    }

}
