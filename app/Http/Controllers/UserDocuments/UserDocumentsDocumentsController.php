<?php

namespace App\Http\Controllers\UserDocuments;

use App\Http\Controllers\BaseController;
use App\Http\Requests\UserDocumentsRequest\DocumentStoreRequest;
use App\Models\Document\Document;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationDocuments\SingleApplicationDocuments;
use App\Models\SingleApplicationDocuments\SingleApplicationDocumentsManager;
use App\Models\SingleApplicationProducts\SingleApplicationProducts;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

/**
 * Class UserDocumentsDocumentsController
 * @package App\Http\Controllers\UserDocuments
 */
class UserDocumentsDocumentsController extends BaseController
{
    /**
     * @var SingleApplicationDocumentsManager
     */
    protected $manager;

    /**
     * UserDocumentsDocumentsController constructor.
     * @param SingleApplicationDocumentsManager $singleApplicationDocumentsManager
     */
    public function __construct(SingleApplicationDocumentsManager $singleApplicationDocumentsManager)
    {
        $this->manager = $singleApplicationDocumentsManager;
    }

    /**
     * Function to render document inputs
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function renderDocumentInputs(Request $request)
    {
        $this->validate($request, [
            'num' => 'required|integer_with_max',
        ]);

        $data['html'] = view('user-documents.documents.document-table-tr')->with([
            'renderInput' => true,
            'num' => $request->get('num'),
            'isApplication' => true
        ])->render();

        return responseResult('OK', '', $data);
    }

    /**
     * Function render document products
     *
     * @param $lngCode
     * @param $documentId
     * @param $applicationId
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function renderDocumentProducts($lngCode, $documentId, $applicationId, Request $request)
    {
        $productColumns = ['id', 'product_number', 'product_code', 'product_description', 'commercial_description'];
        $singleApplicationSubApp = SingleApplicationSubApplications::select('saf_sub_applications.*')->where('saf_sub_applications.id', $applicationId)->myApplicationRoles()->firstOrFail();

        $availableProducts = $singleApplicationSubApp->productList->pluck('id')->all();
        $products = SingleApplicationProducts::select((array)$productColumns)->whereIn('id', $availableProducts)->where('saf_number', $singleApplicationSubApp->saf_number)->get();
        $availableProductsIdNumbers = $singleApplicationSubApp->productsOrderedList();

        $data['html'] = view('user-documents.documents.document-add-products-table')->with([
            'products' => $products,
            'columns' => $productColumns,
            'availableProductsIdNumbers' => $availableProductsIdNumbers
        ])->render();

        return responseResult('OK', '', $data);
    }

    /**
     * Function update products in document
     *
     * @param $lngCode
     * @param $documentId
     * @param $applicationId
     * @param Request $request
     * @return JsonResponse
     */
    public function updateDocumentProducts($lngCode, $documentId, $applicationId, Request $request)
    {
        $this->validate($request, [
            'documentId' => 'required|integer_with_max',
            'stateId' => 'required|integer_with_max',
            'products' => 'nullable|string_with_max'
        ]);

        $safNumber = $request->header('sNumber');

        $data[] = [
            'document_id' => $request->input('documentId'),
            'state_id' => $request->input('stateId'),
            'subapplication_id' => $applicationId,
            'products' => $request->input('products'),
            'added_from_module' => SingleApplicationDocuments::ADDED_FROM_MODULE_APPLICATION
        ];

        // Store Document
        $this->manager->storeDocumentProducts($data, $safNumber);

        return responseResult('OK');
    }

    /**
     * Function add document to a sub application
     *
     * @param $lngCode
     * @param $documentId
     * @param $applicationId
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function addDocument($lngCode, $documentId, $applicationId, Request $request)
    {
        $this->validate($request, [
            'documentItems' => 'array',
            'num' => 'required|integer_with_max',
            'stateId' => 'required|integer_with_max'
        ]);

        $documentItems = $request->get('documentItems');
        $safNumber = $request->header('sNumber');

        SingleApplication::where('regular_number', $safNumber)->firstOrFail();

        $applicationDocs = $newDocumentIds = $existDocuments = [];

        foreach ($documentItems as $documentIncrementedId => $documentId) {

            $docData = [
                'document_id' => $documentId,
                'subapplication_id' => $applicationId,
                'added_from_module' => SingleApplicationDocuments::ADDED_FROM_MODULE_APPLICATION
            ];

            $documentExist = SingleApplicationDocuments::where($docData)->first();

            if (is_null($documentExist)) {

                $newDocumentIds[] = $documentId;
                $docData['state_id'] = $request->input('stateId');
                $applicationDocs[] = $docData;

            } else {

                $existDocuments[] = $documentId;
                $existDocumentsIncrementIds[] = $documentIncrementedId;

            }
        }

        // Store Document Products
        $this->manager->storeDocumentProducts($applicationDocs, $safNumber);

        if (count($existDocuments)) {
            $data['error'] = [
                'docIds' => $existDocuments,
                'message' => implode(',', $existDocumentsIncrementIds) . ' ' . trans('swis.documents.already_exist.info')
            ];
        }

        $data['newDocumentCount'] = count($newDocumentIds);
        $data['html'] = view('user-documents.documents.document-table-tr')->with([
            'attachedDocumentProducts' => Document::whereIn('id', $newDocumentIds)->get(),
            'num' => $request->get('num'),
            'addFromDocList' => true
        ])->render();

        return responseResult('OK', '', $data);
    }

    /**
     * Function to Store document
     *
     * @param $lngCode
     * @param $documentId
     * @param $applicationId
     * @param DocumentStoreRequest $request
     * @return JsonResponse
     */
    public function storeDocument($lngCode, $documentId, $applicationId, DocumentStoreRequest $request)
    {
        $safNumber = $request->header('sNumber');
        $dataValidated = $request->validated();

        $dataValidated['state_id'] = $request->header('stateId');
        $dataValidated['subapplication_id'] = $applicationId;
        $dataValidated['added_from_module'] = SingleApplicationDocuments::ADDED_FROM_MODULE_APPLICATION;
        $dataValidated['document_status'] = Document::DOCUMENT_STATUS_REGISTERED;

        // Store Documents
        $newDocument = $this->manager->storeDocumentsFromUserDocument($dataValidated, $safNumber);

        return responseResult('OK', '', [
            'docId' => $newDocument->id,
            'documentForm' => trans("swis.document.document_form.{$newDocument->document_form}.title"),
            'docStatus' => Document::DOCUMENT_STATUS_REGISTERED,
            'documentFile' => $newDocument->filePath(),
            'documentFileName' => $newDocument->fileBaseName()
        ]);
    }

    /**
     * Function to delete document
     *
     * @param $lngCode
     * @param $documentId
     * @param $applicationId
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteDocument($lngCode, $documentId, $applicationId, Request $request)
    {
        $this->validate($request, [
            'documentIds' => 'required|array',
            'documentIds.*' => 'integer_with_max|exists:document,id'
        ]);

        $this->manager->deleteDocumentsFromMyApplication($request->input('documentIds'), $request->header('sNumber'), $applicationId);

        return responseResult('OK');
    }
}