<?php

namespace App\Http\Controllers\UserDocuments;

use App\Http\Controllers\BaseController;
use App\Http\Requests\SingleApplicationExpertise\SingleApplicationExpertiseRequest;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\Laboratory\Laboratory;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationExpertise\SingleApplicationExpertise;
use App\Models\SingleApplicationExpertise\SingleApplicationExpertiseManager;
use App\Models\SingleApplicationProducts\SingleApplicationProducts;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

/**
 * Class UserDocumentsOldLaboratoryController
 * @package App\Http\Controllers\UserDocuments
 */
class UserDocumentsOldLaboratoryController extends BaseController
{
    /**
     * @var string
     */
    protected $managerLabExpertise;

    /**
     * UserDocumentsOldLaboratoryController constructor.
     * @param SingleApplicationExpertiseManager $expertiseManager
     */
    public function __construct(SingleApplicationExpertiseManager $expertiseManager)
    {
        $this->managerLabExpertise = $expertiseManager;
    }

    /**
     * Function to return result of laboratory process
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function process(Request $request)
    {
        $this->validate($request, [
            'product_group' => 'nullable|string_with_max',
            'product_sub_group' => 'nullable|string_with_max',
            'expertising_method' => 'nullable|string_with_max',
            'expertising_indicator' => 'nullable|string_with_max'
        ]);

        $data = Laboratory::getProcessingResult($request->all());

        $returnData = view('user-documents.old-laboratories.process-table')->with([
            'data' => $data,
        ])->render();

        return response()->json($returnData);
    }

    /**
     * Function to store indicator
     *
     * @param SingleApplicationExpertiseRequest $request
     * @return JsonResponse
     */
    public function storeIndicators(SingleApplicationExpertiseRequest $request)
    {
        if (!$this->validateSelectedLab($request)) {
            return responseResult('INVALID_DATA', '', '', ['diff_labs' => trans('swis.my_application.laboratories.different_labs')]);
        }

        $this->managerLabExpertise->store($request->validated());

        return responseResult('OK');
    }

    /**
     * Function to edit indicator
     *
     * @param $lng
     * @param $applicationId
     * @param $documentId
     * @param $id
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function editIndicators($lng, $applicationId, $documentId, $id)
    {
        $safExpertise = SingleApplicationExpertise::select('saf_expertise.*')
            ->join('saf_expertise_indicators', 'saf_expertise_indicators.saf_expertise_id', '=', 'saf_expertise.id')
            ->where('saf_expertise.id', $id)
            ->first();

        $saf = SingleApplication::select('data', 'regular_number')->where('regular_number', $safExpertise->saf_number)->firstOrFail();
        $singleApplicationSubApp = SingleApplicationSubApplications::where('id', $applicationId)->firstOrFail();

        $constructorDocument = ConstructorDocument::where('id', $documentId)->first();
        $availableProducts = $singleApplicationSubApp->productList->pluck('id')->all();

        if (!empty($saf->data)) {
            $subApplications = SingleApplicationSubApplications::where('saf_number', $safExpertise->saf_number)->where('id', $applicationId)->where('document_code', $constructorDocument->document_code)->get();
            if ($subApplications->count() > 0) {
                foreach ($subApplications as $subApplication) {
                    if (!empty($subApplication->products)) {
                        $availableProducts = array_merge($availableProducts, explode(',', $subApplication->products));
                    }
                }
            }

            if (!empty($availableProducts)) {
                $singleAppProducts = SingleApplicationProducts::whereIn('id', $availableProducts)->where(['saf_number' => $saf->regular_number])->get();
            }
        }
        $laboratoriesData = Laboratory::getLaboratoriesData($singleAppProducts, $safExpertise->saf_number, $applicationId, true, $id);

        $returnData['body'] = view('user-documents.old-laboratories.laboratories-edit')->with([
            'safExpertiseEdit' => $safExpertise,
            'laboratoriesData' => $laboratoriesData,
            'applicationId' => $applicationId
        ])->render();

        $returnData['status'] = "OK";

        return response()->json($returnData);
    }

    /**
     * Function to save updated indicator
     *
     * @param SingleApplicationExpertiseRequest $request
     * @param $lng
     * @param $id
     * @return JsonResponse
     */
    public function updateIndicators(SingleApplicationExpertiseRequest $request, $lng, $id)
    {
        if (!$this->validateSelectedLab($request)) {
            return responseResult('INVALID_DATA', '', '', ['diff_labs' => trans('swis.my_application.laboratories.different_labs')]);
        }

        $this->managerLabExpertise->update($request->validated(), $id);

        return responseResult('OK');
    }

    /**
     * Function to validate selected laboratory
     *
     * @param $request
     * @return bool
     */
    private function validateSelectedLab($request)
    {
        return !(!empty($request->input('lab_id')) && count(array_unique($request->input('lab_id'))) > 1);
    }


}