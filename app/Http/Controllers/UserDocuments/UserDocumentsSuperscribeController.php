<?php

namespace App\Http\Controllers\UserDocuments;

use App\Http\Controllers\BaseController;
use App\Models\ConstructorActionSuperscribes\ConstructorActionSuperscribes;
use App\Models\ConstructorLists\ConstructorLists;
use App\Models\ConstructorMapping\ConstructorMapping;
use App\Models\Menu\Menu;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationNotes\SingleApplicationNotes;
use App\Models\SingleApplicationObligations\SingleApplicationObligations;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use App\Models\SingleApplicationSubApplicationStates\SingleApplicationSubApplicationStates;
use App\Models\User\User;
use App\Models\UserDocuments\UserDocuments;
use App\Models\UserDocuments\UserDocumentsManager;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

/**
 * Class UserDocumentsSuperscribeController
 * @package App\Http\Controllers\UserDocuments
 */
class UserDocumentsSuperscribeController extends BaseController
{
    /**
     * @var UserDocumentsManager
     */
    protected $manager;

    /**
     * UserDocumentsSuperscribeController constructor.
     * @param UserDocumentsManager $documentsManager
     */
    public function __construct(UserDocumentsManager $documentsManager)
    {
        $this->manager = $documentsManager;
    }

    /**
     * Function to Superscribe user
     *
     * @param $lngCode
     * @param $documentId
     * @param $applicationId
     * @param Request $request
     * @return JsonResponse
     */
    public function superscribe($lngCode, $documentId, $applicationId, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|integer_with_max',
            'start_state_id' => 'required|integer_with_max',
            'end_state_id' => 'required|integer_with_max',
            'last_update' => 'required|date',
            'mapping_id_superscribe' => 'required|integer_with_max'
        ]);

        if ($validator->fails()) {
            if (empty($request->input('user_id'))) {
                $errors['user_id'] = trans('core.base.field.required');
            }

            return responseResult('INVALID_DATA', '', '', $errors ?? []);
        }

        $documentLastUpdate = $request->input('last_update');
        $documentCurrentState = SingleApplicationSubApplicationStates::where(['saf_subapplication_id' => $applicationId, 'is_current' => '1'])->first();

        if ($documentLastUpdate != $documentCurrentState->getOriginal('created_at')) {
            return responseResult('INVALID_DATA', '', '', ['state_changed' => true]);
        }

        $subApplication = SingleApplicationSubApplications::find($applicationId);

        if (isset($request->all()['saf'])) {
            $safDataForValidation = getJsonDataAsArray(['saf' => $request->all()['saf']], $request->all()['customData'] ?? [], $subApplication->productListSelect->toArray());
        } else {
            $subData = $subApplication->data;
            if (isset($subData['status_type'])) {
                unset($subData['status_type']);
            }
            if (isset($subData['form_is_changed'])) {
                unset($subData['form_is_changed']);
            }
            $safDataForValidation = getJsonDataAsArray($subData, $subApplication->custom_data, $subApplication->productListSelect->toArray());
        }

        $customData = !empty($subApplication->custom_data) ? UserDocuments::generateCustomData($subApplication->custom_data) : [];

        $notUpdatedFields = SingleApplicationObligations::generateObligation($subApplication, $subApplication, $request->input('mapping_id_superscribe'), $safDataForValidation, $request->input('start_state_id'), []);

        $roles = Auth::user()->getCurrentRoles(Menu::USER_DOCUMENTS_MENU_NAME);
        $documentMappings = UserDocuments::getDocumentMappings($documentId, $roles, $documentCurrentState, $applicationId);
        $mappingWithLists = ConstructorMapping::getMappingsWithLists($documentMappings, false);
        $nonVisibleFields = ConstructorLists::margeMappingLists($documentMappings, $mappingWithLists, true);

        $endStateId = $request->input('end_state_id');

        $storeData = [
            'company_tax_id' => Auth::user()->companyTaxId(),
            'constructor_document_id' => $documentId,
            'saf_subapplication_id' => $applicationId,
            'start_state_id' => $request->input('start_state_id'),
            'end_state_id' => $endStateId,
            'to_user' => $request->input('user_id'),
            'from_user' => Auth::id(),
            'is_current' => ConstructorActionSuperscribes::TRUE
        ];

        $subAppData = [
            'user_id' => Auth::id(),
            'saf_number' => $documentCurrentState->saf_number,
            'saf_id' => $documentCurrentState->saf_id,
            'saf_subapplication_id' => $applicationId,
            'state_id' => $endStateId,
            'is_current' => '1'
        ];

        DB::transaction(function () use ($request, $subApplication, $subAppData, $storeData, $customData, $notUpdatedFields, $nonVisibleFields, $documentCurrentState, $documentId, $applicationId, $endStateId) {

            ConstructorActionSuperscribes::where([
                'company_tax_id' => Auth::user()->companyTaxId(),
                'constructor_document_id' => $documentId,
                'saf_subapplication_id' => $applicationId,
                'is_current' => ConstructorActionSuperscribes::TRUE
//                'start_state_id' => $request->input('start_state_id'),
//                'end_state_id' => $endStateId
            ])->update(['is_current' => ConstructorActionSuperscribes::FALSE, 'show_status' => ConstructorActionSuperscribes::STATUS_INACTIVE]);

            ConstructorActionSuperscribes::create($storeData);

            $documentCurrentState->update(['is_current' => '0']);

            //------

            SingleApplicationSubApplicationStates::where(['saf_number' => $subApplication->saf_number, 'saf_subapplication_id' => $subApplication->id])->update(['is_current' => '0']);

            SingleApplicationSubApplicationStates::store($subAppData, $request->all());

            $this->manager->updateStoreData($request->all(), $subApplication, $customData, $nonVisibleFields, $notUpdatedFields, $documentCurrentState, $documentId, true);
        });

        $mapping = ConstructorMapping::where('document_id', $documentId)->where('id', $request->input('mapping_id_superscribe'))->first();

        $saf = SingleApplication::where('regular_number', $documentCurrentState->saf_number)->first();

        UserDocuments::sendNotifications($mapping, $saf, $documentId, $applicationId, $request->all());

        if (Auth::user()->isLocalAdmin()) {
            return responseResult('OK', back()->getTargetUrl());
        }


        $userCurrentRoles = Auth::user()->getCurrentRoles(Menu::USER_DOCUMENTS_MENU_NAME);
        $needRoles = ConstructorMapping::where(['document_id' => $documentId, 'start_state' => $endStateId])->pluck('role')->unique()->all();
        $userHasRole = array_intersect($needRoles, $userCurrentRoles);

        if (count($userHasRole) > 0) {
            return responseResult('OK', back()->getTargetUrl());
        }

        return responseResult('OK', urlWithLng('user-documents/' . $documentId));
    }

    /**
     * Function to delete Superscribe user
     *
     * @param $lngCode
     * @param $documentId
     * @param $applicationId
     * @param Request $request
     * @return JsonResponse
     */
    public function superscribeDelete($lngCode, $documentId, $applicationId, Request $request)
    {
        $this->validate($request, [
            'start_state_id' => 'required|integer_with_max',
            'end_state_id' => 'required|integer_with_max',
            'last_update' => 'required|date',
        ]);

        $documentLastUpdate = $request->input('last_update');
        $documentCurrentState = SingleApplicationSubApplicationStates::where(['saf_subapplication_id' => $applicationId, 'is_current' => '1'])->first();

        if ($documentLastUpdate != $documentCurrentState->getOriginal('created_at')) {
            return responseResult('INVALID_DATA', '', '', ['state_changed' => true]);
        }

        $data = [
            'company_tax_id' => Auth::user()->companyTaxId(),
            'constructor_document_id' => $documentId,
            'saf_subapplication_id' => $applicationId,
            'start_state_id' => $request->input('start_state_id'),
            'end_state_id' => $request->input('end_state_id'),
        ];

        $superscribe = ConstructorActionSuperscribes::where($data)->orderByDesc()->first();

        if (!is_null($superscribe)) {

            //--------------------- Notification part ---------------------//

            $user = User::select('id', 'first_name', 'last_name')->where('id', $superscribe->to_user)->first();

            SingleApplicationNotes::storeNotes(['saf_number' => $documentCurrentState->saf_number, 'sub_application_id' => $applicationId, 'note' => $user->name()], SingleApplicationNotes::NOTE_TRANS_KEY_SUB_APP_SUPERSCRIBE_DELETE);

            //--------------------- Notification part End ---------------------//

            $superscribe->update(['show_status' => ConstructorActionSuperscribes::STATUS_DELETED, 'is_current' => ConstructorActionSuperscribes::FALSE]);

        }

        return responseResult('OK', back()->getTargetUrl());
    }
}