<?php

namespace App\Http\Controllers\UserDocuments;

use App\Http\Controllers\BaseController;
use App\Models\SingleApplicationNotes\SingleApplicationNotes;
use App\Models\SingleApplicationSubApplicationDigitalSignature\SingleApplicationSubApplicationDigitalSignature;
use App\Models\SingleApplicationSubApplicationDigitalSignature\SingleApplicationSubApplicationDigitalSignatureManager;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

/**
 * Class UserDocumentsDigitalSignatureController
 * @package App\Http\Controllers
 */
class UserDocumentsDigitalSignatureController extends BaseController
{
    /**
     * @var SingleApplicationSubApplicationDigitalSignatureManager
     */
    protected $singleApplicationSubApplicationDigitalSignatureManager;

    /**
     * UserDocumentsDigitalSignatureController constructor.
     * @param SingleApplicationSubApplicationDigitalSignatureManager $singleApplicationSubApplicationDigitalSignature
     */
    public function __construct(SingleApplicationSubApplicationDigitalSignatureManager $singleApplicationSubApplicationDigitalSignature)
    {
        $this->singleApplicationSubApplicationDigitalSignatureManager = $singleApplicationSubApplicationDigitalSignature;
    }

    /**
     * Function to check application digital signature
     *
     * @param $lngCode
     * @param Request $request
     * @return JsonResponse
     */
    public function signDigitalSignature($lngCode, Request $request)
    {
        $applicationId = $request->get('subApplicationId') ?? '';
        $data['id'] = $request->get('digitalSignatureId') ?? '';
        $data['fileDigestHash'] = $request->get('fileDigestHash') ?? '';

        if (!empty($data['id']) && !empty($data['fileDigestHash'])) {

            $this->singleApplicationSubApplicationDigitalSignatureManager->update($data);

            $safNumber = SingleApplicationSubApplications::where('id', $applicationId)->pluck('saf_number')->first();
            $safSubApplicationDigitalSignature = SingleApplicationSubApplicationDigitalSignature::where('id', $data['id'])->firstOrFail();

            SingleApplicationNotes::storeNotes([
                'saf_number' => $safNumber,
                'note' => '|SIGNATURE_' . $safSubApplicationDigitalSignature->id . '|',
                'trans_key' => SingleApplicationNotes::NOTE_TRANS_KEY_SIGNATURE_BTN,
                'sub_application_id' => $applicationId,
                'send_from_module' => SingleApplicationNotes::NOTE_SEND_MODULE_APPLICATION
            ]);
            return responseResult('INVALID_DATA', '', '', ['need_digital_signature' => false]);
        }

        // my data storage location is project_root/storage/app/{fileName} file.
        $digitalSignatureDirPrefix = env('DIGITAL_SIGNATURE_FILE_PATH') . '/appplications/';
        $digitalSignatureDirSecondPart = date('Y') . '/' . date('m') . '/' . date('d') . '/';
        $digitalSignatureDir = $digitalSignatureDirPrefix . $digitalSignatureDirSecondPart;

        if (!is_dir($digitalSignatureDir)) {
            mkdir($digitalSignatureDir, 0775, true);
        }

        $digitalSignatureFileName = $applicationId . '_' . date('Y') . date('m') . date('d') . rand() . '.json';
        $digitalSignatureFile = $digitalSignatureDir . '/' . $digitalSignatureFileName;

        $subApplicationAllData = SingleApplicationSubApplications::where('id', $applicationId)->pluck('all_data')->first();

        Storage::disk('digital_signature')->put($digitalSignatureFileName, json_encode($subApplicationAllData));

        $fileDigest = hash_file('gost-crypto', $digitalSignatureFile);

        $digitalSignatureData = [
            'subapplication_id' => $applicationId,
            'user_id' => Auth::user()->id,
            'company_tax_id' => Auth::user()->companyTaxId(),
            'file_path' => $digitalSignatureDirSecondPart . $digitalSignatureFileName,
            'file_digest' => $fileDigest
        ];

        $digitalSignatureId = $this->singleApplicationSubApplicationDigitalSignatureManager->store($digitalSignatureData);

        //
        $data['fileDigest'] = $fileDigest;
        $data['digitalSignatureId'] = $digitalSignatureId;

        return responseResult('OK', '', $data);
    }

    /**
     * Function to check digital signature
     *
     * @param $lngCode
     * @param Request $request
     * @return JsonResponse
     */
    public function checkDigitalSignature($lngCode, Request $request)
    {
        $this->validate($request, ['digitalSignatureId' => 'required|integer_with_max']);

        $digitalSignatureId = $request['digitalSignatureId'];

        $digitalSignature = SingleApplicationSubApplicationDigitalSignature::select('file_digest_hash', 'file_path')->where('id', $digitalSignatureId)->firstOrFail();

        $verificationFileStart = "-----BEGIN CMS-----\n";
        $verificationFileHash = optional($digitalSignature)->file_digest_hash;
        $verificationFileEnd = "\n-----END CMS-----";

        $verificationFileContent = $verificationFileStart . $verificationFileHash . $verificationFileEnd;

        $tmpDirectory = env('DIGITAL_SIGNATURE_FILE_TMP_PATH');
        $validationFileName = 'digitalSignature_' . rand() . '.pem';
        $validationFileFullName = $tmpDirectory . '/' . $validationFileName;

        $tmpFileName = '/tmpDigitalSignature_' . rand() . '.pem';
        $tmpFileFullName = $tmpDirectory . '/' . $tmpFileName;

        Storage::disk('tmp')->put($validationFileName, $verificationFileContent);
        Storage::disk('tmp')->put($tmpFileName, '');

        $cmd = "/usr/local/openssl/bin/openssl cms -verify -binary -in {$validationFileFullName} -inform PEM -CAfile /var/www/shared/content/ca-bundle.pem -crl_check_all -certsout {$tmpFileFullName}";

        exec(escapeshellcmd($cmd), $output, $returnCode);
        // if $returnCode === 0 command executed successful
        // else error
        // if $output == 'disgn of json file' then verified
        //

        join('', $output) . "\n\n";

        $cert = openssl_x509_parse(file_get_contents($tmpFileFullName));

        $cmd = "/usr/local/openssl/bin/openssl asn1parse -i -in {$validationFileFullName}";
        exec(escapeshellcmd($cmd), $output);
        $str = join('', $output);
        $str = substr($str, strpos($str, ':signingTime'));
        $str = substr($str, strpos($str, "UTCTIME"));
        $str = substr($str, 0, strpos($str, 'Z') + 1);
        $str = '20' . substr($str, strpos($str, ':') + 1);

        $data['command'] = $cert;
        $data['signing_time'] = date('d/m/Y H:i:s', strtotime($str));

        // Deleting files
        unlink($validationFileFullName);
        unlink($tmpFileFullName);

        return responseResult('OK', '', $data);
    }

}