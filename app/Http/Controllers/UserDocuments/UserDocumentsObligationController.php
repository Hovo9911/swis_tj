<?php

namespace App\Http\Controllers\UserDocuments;

use App\Http\Controllers\BaseController;
use App\Models\BaseModel;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorMapping\ConstructorMapping;
use App\Models\ConstructorRules\ConstructorRules;
use App\Models\Menu\Menu;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use App\Models\SingleApplicationSubApplicationStates\SingleApplicationSubApplicationStates;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Throwable;

/**
 * Class UserDocumentsObligationController
 * @package App\Http\Controllers\UserDocuments
 */
class UserDocumentsObligationController extends BaseController
{
    /**
     * Function to get html rendered field for free sum obligation
     *
     * @param $lngCode
     * @param $documentId
     * @param $applicationId
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function getObligationRow($lngCode, $documentId, $applicationId, Request $request)
    {
        $this->validate($request, [
            'incrementNumber' => 'required|numeric|min:1',
        ]);

        $constructorDocument = ConstructorDocument::select('id', 'document_type_id')->where('id', $documentId)->firstOrFail();
        $subApplication = SingleApplicationSubApplications::select('saf_sub_applications.document_number')->where('id', $applicationId)->firstOrFail();

        $roles = Auth::user()->getCurrentRoles(Menu::USER_DOCUMENTS_MENU_NAME);
        $documentCurrentState = SingleApplicationSubApplicationStates::select('id', 'state_id')->where('saf_subapplication_id', $applicationId)->orderByDesc()->firstOrFail();

        $obligationTypes = ConstructorMapping::select('reference_obligation_budget_line.id', 'reference_obligation_type.code', 'reference_obligation_budget_line.account_number', 'reference_obligation_type_ml.name')
            ->join('constructor_actions', 'constructor_mapping.action', '=', 'constructor_actions.id')
            ->join('constructor_mapping_to_rule', 'constructor_mapping_to_rule.constructor_mapping_id', '=', 'constructor_mapping.id')
            ->join('constructor_rules', 'constructor_rules.id', '=', 'constructor_mapping_to_rule.rule_id')
            ->join('reference_obligation_budget_line', function ($q) {
                $q->on('reference_obligation_budget_line.code', '=', 'constructor_rules.obligation_code')->where('reference_obligation_budget_line.show_status', BaseModel::STATUS_ACTIVE);
            })
            ->join('reference_obligation_type', 'reference_obligation_type.id', '=', 'reference_obligation_budget_line.budget_line')
            ->join('reference_obligation_type_ml', function ($q) {
                $q->on('reference_obligation_type_ml.reference_obligation_type_id', '=', 'reference_obligation_type.id')->where('reference_obligation_type_ml.lng_id', cLng('id'));
            })
            ->join('reference_obligation_creation_type', function ($q) {
                $q->on('reference_obligation_creation_type.id', '=', 'reference_obligation_budget_line.obligation_creation_type')->where('reference_obligation_creation_type.code', ConstructorRules::CREATION_TYPE_FREE_SUM_CODE);
            })
            ->whereIn('role', $roles)
            ->where(['start_state' => $documentCurrentState->state_id, 'constructor_mapping.document_id' => $documentId])
            ->groupBy('reference_obligation_budget_line.id', 'reference_obligation_type.code', 'reference_obligation_budget_line.account_number', 'reference_obligation_type_ml.name')
            ->active()
            ->get();

        // Data for View
        $id = isset(explode('/', $subApplication->document_number)[1]) ? explode('/', $subApplication->document_number)[1] : '';

        $dataForView = [
            'subApplication' => $id,
            'subApplicationType' => $constructorDocument->document_code,
            'documentId' => $subApplication->document_number,
            'subApplicationName' => $constructorDocument->current()->document_name,
            'obligationTypes' => $obligationTypes,
        ];

        $data['html'] = view('user-documents.obligations.new-obligation-row')->with([
            'data' => $dataForView,
            'incrementNumber' => $request->input('incrementNumber')
        ])->render();

        return responseResult('OK', '', $data);
    }
}