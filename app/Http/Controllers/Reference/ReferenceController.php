<?php

namespace App\Http\Controllers\Reference;

use App\Exports\ReferenceTableStructureExport;
use App\Http\Controllers\BaseController;
use App\Http\Requests\ReferenceTable\ReferenceTableRequest;
use App\Http\Requests\ReferenceTable\ReferenceTableSearchRequest;
use App\Models\Agency\Agency;
use App\Models\NotesOfReference\NotesOfReference;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\ReferenceTable\ReferenceTableManager;
use App\Models\ReferenceTable\ReferenceTableSearch;
use App\Models\ReferenceTable\ReferenceTableStructure;
use App\Models\ReferenceTableForm\ReferenceTableFormManager;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Throwable;

/**
 * Class ReferenceController
 * @package App\Http\Controllers\Reference
 */
class ReferenceController extends BaseController
{
    /**
     * @var ReferenceTableManager
     */
    protected $manager;

    /**
     * @var ReferenceTableFormManager
     */
    protected $referenceFormManager;

    /**
     * ReferenceController constructor.
     *
     * @param ReferenceTableManager $manager
     * @param ReferenceTableFormManager $referenceTableFormManager
     */
    public function __construct(ReferenceTableManager $manager, ReferenceTableFormManager $referenceTableFormManager)
    {
        $this->manager = $manager;
        $this->referenceFormManager = $referenceTableFormManager;
    }

    /**
     * Function to show index page
     *
     * @return View
     */
    public function index()
    {
        return view('reference-table.index');
    }

    /**
     * Function to get all data and showing in datatable
     *
     * @param ReferenceTableSearchRequest $referenceTableSearchRequest
     * @param ReferenceTableSearch $referenceTableSearch
     * @return JsonResponse
     */
    public function table(ReferenceTableSearchRequest $referenceTableSearchRequest, ReferenceTableSearch $referenceTableSearch)
    {
        $data = $this->dataTableAll($referenceTableSearchRequest, $referenceTableSearch);

        return response()->json($data);
    }

    /**
     * Function to show create page
     *
     * @return View
     */
    public function create()
    {
        return view('reference-table.edit')->with([
            'defaultDataStructure' => $this->manager->defaultStructures(),
            'agencies' => Agency::allData([], 'all'),
            'clone' => false,
            'saveMode' => 'add',
        ]);
    }

    /**
     * Function to store all data
     *
     * @param ReferenceTableRequest $request
     * @return JsonResponse
     */
    public function store(ReferenceTableRequest $request)
    {
        if (Schema::hasTable('reference_' . $request->get('table_name'))) {
            $errors['table_name'] = trans('swis.reference.table_name.exist.error');

            return responseResult('INVALID_DATA', '', '', $errors);
        }

        //
        $uniqueFieldName = $errors = [];
        $structureData = $request->validated()['structure'] ?? [];

        foreach ($structureData as $key => $structure) {
            if (isset($uniqueFieldName[$structure['field_name']])) {
                $errors['structure-' . $key . '-field_name'] = trans('swis.reference.structure.duplicate.field_name.error');
            } else {
                $uniqueFieldName[$structure['field_name']] = $key;
            }
        }

        if (count($errors)) {
            return responseResult('INVALID_DATA', '', '', $errors);
        }

        //
        $referenceTable = $this->manager->store($request->validated());

        return responseResult('OK', null, ['id' => $referenceTable->id]);
    }

    /**
     * Function to show edit page by current id
     *
     * @param $lngCode
     * @param $id
     * @param $viewType
     * @return View
     */
    public function edit($lngCode, $id, $viewType)
    {
        $referenceTable = ReferenceTable::with(['customStructure', 'agencies'])->where('id', $id)->firstOrFail();

        return view('reference-table.edit')->with([
            'referenceTable' => $referenceTable,
            'referenceTables' => $this->manager->allDataWithoutCurrent($id),
            'agencies' => Agency::allData([], 'all'),
            'notes' => NotesOfReference::getNotes($id),
            'defaultDataStructure' => $this->manager->defaultStructures(),
            'clone' => false,
            'saveMode' => $viewType,
        ]);
    }

    /**
     * Function to clone ref
     *
     * @param $lngCode
     * @param $id
     * @return View
     */
    public function cloneRef($lngCode, $id)
    {
        $referenceTable = ReferenceTable::with(['customStructure', 'agencies'])->where('id', $id)->firstOrFail();

        return view('reference-table.edit')->with([
            'referenceTable' => $referenceTable,
            'referenceTables' => $this->manager->allDataWithoutCurrent($id),
            'agencies' => Agency::allData([], 'all'),
            'defaultDataStructure' => $this->manager->defaultStructures(),
            'clone' => true,
            'saveMode' => 'add'
        ]);
    }

    /**
     * Function to update data
     *
     * @param ReferenceTableRequest $request
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     */
    public function update(ReferenceTableRequest $request, $lngCode, $id)
    {
        $this->manager->update($request->validated(), $id);

        return responseResult('OK');
    }

    /**
     * Function to render structure table row
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function renderTable(Request $request)
    {
        $this->validate($request, [
            'i' => 'required|integer_with_max'
        ]);

        $data['html'] = view('reference-table.structure-table')->with(['i' => $request->get('i'), 'mode' => 'edit', 'field_type' => 'text'])->render();

        return responseResult('OK', '', $data);
    }

    /**
     * Function to render table parameters
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function renderTableParameter(Request $request)
    {
        $this->validate($request, [
            'i' => 'required|integer_with_max',
            'type' => 'required|in:' . implode(',', ReferenceTableStructure::COLUMN_TYPES),
            'current' => 'nullable|integer_with_max',
        ]);

        $fieldType = $request->get('type');

        $referenceTables = [];
        if ($fieldType == ReferenceTableStructure::COLUMN_TYPE_AUTOCOMPLETE || $fieldType == ReferenceTableStructure::COLUMN_TYPE_SELECT) {
            $referenceTables = $this->manager->allDataWithoutCurrent($request->get('current'));
        }

        $data['html'] = view('reference-table.structure-parameter')->with(['field_type' => $fieldType, 'i' => $request->get('i'), 'referenceTables' => $referenceTables])->render();

        return responseResult('OK', '', $data);
    }

    /**
     * Function to render table all fields
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function renderTableFields(Request $request)
    {
        $this->validate($request, [
            'i' => 'required|integer_with_max',
            'id' => 'required|integer_with_max|exists:reference_table,id'
        ]);

        $fields = ReferenceTableStructure::select('id', 'field_name')->where('reference_table_id', $request->get('id'))->where('type', '!=', ReferenceTableStructure::COLUMN_TYPE_AUTOCOMPLETE)->active()->orderByDesc()->get();

        $data['html'] = view('reference-table.partials.structure-fields')->with(['fields' => $fields, 'i' => $request->get('i')])->render();

        return responseResult('OK', '', $data);
    }

    /**
     * Function to GET reference rows(and autocomplete)
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function referenceTableData(Request $request)
    {
        $this->validate($request, [
            'ref' => 'required|integer_with_max|exists:reference_table,id',
            'q' => 'nullable|string_with_max',
            'refType' => 'nullable|in:' . ReferenceTableStructure::COLUMN_TYPE_AUTOCOMPLETE,
            'idValue' => 'nullable|in:true',
            'codeName' => 'nullable|in:true'
        ]);

        $data = $this->referenceFormManager->getReferenceFormDataValues($request->get('ref'), $request->get('q'), $request->get('idValue'), $request->get('refType'), $request->get('codeName'));

        return response()->json($data);
    }

    /**
     * Function to get Document type in attached document
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getDocumentType(Request $request)
    {
        $this->validate($request, [
            'q' => 'required|string_with_max',
            'refType' => 'string_with_max'
        ]);

        $data = $this->referenceFormManager->getClassificatorDocumentTypes($request->get('q'), $request->get('refType'));

        return response()->json($data);
    }

    /**
     * Function to export reference table structure
     *
     * @param $lngCode
     * @param $referenceId
     * @return BinaryFileResponse
     */
    public function exportReferenceTableStructure($lngCode, $referenceId)
    {
        $referenceTable = ReferenceTable::where('id', $referenceId)->firstorFail();

        $referenceTableExport = new ReferenceTableStructureExport();
        $referenceTableExport->getData($referenceTable);

        return Excel::download($referenceTableExport, "{$referenceTable->table_name}.csv");
    }
}
