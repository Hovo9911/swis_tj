<?php

namespace App\Http\Controllers\Reference;

use App\Http\Controllers\BaseController;
use App\Http\Requests\ReferenceTable\ReferenceFormRequest;
use App\Http\Requests\ReferenceTable\ReferenceFormSearchRequest;
use App\Imports\ReferenceTableFormImport;
use App\Models\BaseModel;
use App\Models\NotesOfReference\NotesOfReference;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\ReferenceTableForm\ReferenceTableFormExportManager;
use App\Models\ReferenceTableForm\ReferenceTableFormManager;
use App\Models\ReferenceTableForm\ReferenceTableFormSearch;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Class ReferenceTableFormController
 * @package App\Http\Controllers\Reference
 */
class ReferenceTableFormController extends BaseController
{
    /**
     * @var ReferenceTableFormManager
     */
    protected $manager;

    /**
     * @var ReferenceTableFormExportManager
     */
    protected $exportManager;

    /**
     * ReferenceTableFormController constructor.
     *
     * @param ReferenceTableFormManager $manager
     * @param ReferenceTableFormExportManager $referenceTableFormExportManager
     */
    public function __construct(ReferenceTableFormManager $manager, ReferenceTableFormExportManager $referenceTableFormExportManager)
    {
        $this->manager = $manager;
        $this->exportManager = $referenceTableFormExportManager;
    }

    /**
     * Function to return view with data
     *
     * @param $lngCode
     * @param $id
     * @return View
     */
    public function index($lngCode, $id)
    {
        $referenceTable = ReferenceTable::with('structure')->where('id', $id)->firstorFail();

        return view('reference-table-form.index')->with([
            'referenceTable' => $referenceTable,
            'userHasAccess' => $referenceTable->authUserCanEditReference(),
        ]);
    }

    /**
     * Function to showing data in dataTable
     *
     * @param ReferenceFormSearchRequest $referenceFormSearchRequest
     * @param ReferenceTableFormSearch $referenceTableFormSearch
     * @return JsonResponse
     */
    public function table(ReferenceFormSearchRequest $referenceFormSearchRequest, ReferenceTableFormSearch $referenceTableFormSearch)
    {
        $data = $this->dataTableAll($referenceFormSearchRequest, $referenceTableFormSearch);

        return response()->json($data);
    }

    /**
     * Function to create ref table
     *
     * @param $lngCode
     * @param $id
     * @return View|void
     */
    public function create($lngCode, $id)
    {
        $referenceTable = ReferenceTable::with('structure')->where('id', $id)->firstorFail();

        if (!$referenceTable->authUserCanEditReference()) {
            return abort('404');
        }

        return view('reference-table-form.edit')->with([
            'referenceTable' => $referenceTable,
            'saveMode' => 'add'
        ]);
    }

    /**
     * Function to store data
     *
     * @param ReferenceFormRequest $request
     * @param $lngCode
     * @param $referenceId
     * @return JsonResponse
     */
    public function store(ReferenceFormRequest $request, $lngCode, $referenceId)
    {
        $referenceTable = ReferenceTable::select('id', 'table_name', 'show_status')->where('id', $referenceId)->firstOrFail();

        if ($referenceTable->authUserCanEditReference()) {
            $refCreatedTable = $this->manager->store($request->validated(), $referenceId);

            return responseResult('OK', null, ['id' => $refCreatedTable->id]);
        }

        return responseResult('OK');
    }

    /**
     * Function to Show current by id
     *
     * @param $lngCode
     * @param $referenceId
     * @param $referenceFormId
     * @param $viewType
     * @return RedirectResponse|View|void
     */
    public function edit($lngCode, $referenceId, $referenceFormId, $viewType)
    {
        $referenceTable = ReferenceTable::where('id', $referenceId)->firstOrFail();

        // If not Editable Reference redirect to View
        if ($viewType == BaseModel::VIEW_MODE_EDIT) {
            if (in_array($referenceTable->table_name, ReferenceTable::NONE_EDITABLE_REFERENCE_TABLE_NAMES)) {
                return redirect(urlWithLng('reference-table-form/' . $referenceId . '/' . $referenceFormId . '/view'));
            }

            if (!$referenceTable->authUserCanEditReference()) {
                return abort('404');
            }
        }

        //
        $referenceTableForm = DB::table('reference_' . $referenceTable->table_name)->where('id', $referenceFormId);

        if (!Auth::user()->isSwAdmin()) {
            $hasAgencyColumnGetName = ReferenceTable::hasAgencyColumnGetName($referenceId);

            if ($hasAgencyColumnGetName) {
                $referenceTableForm->whereIn("reference_$referenceTable->table_name" . '.' . $hasAgencyColumnGetName, ReferenceTable::getUserReferenceAgencyIds());
            }

            if ($referenceTable->table_name == ReferenceTable::REFERENCE_AGENCY_TABLE_NAME) {
                $referenceTableForm->where("reference_$referenceTable->table_name" . ".code", Auth::user()->companyTaxId());
            }
        }

        $referenceTableForm = $referenceTableForm->first();
        $referenceTableFormMl = DB::table('reference_' . $referenceTable->table_name . '_ml')->where('reference_' . $referenceTable->table_name . '_id', $referenceFormId)->get()->keyBy('lng_id')->toArray();

        if (is_null($referenceTableForm)) {
            return abort('404');
        }

        return view('reference-table-form.edit')->with([
            'referenceTable' => $referenceTable,
            'referenceTableForm' => $referenceTableForm,
            'referenceTableFormMl' => $referenceTableFormMl,
            'notes' => NotesOfReference::getNotes($referenceId, $referenceFormId, $referenceTableForm->code),
            'saveMode' => $viewType
        ]);
    }

    /**
     * Function to Update data
     *
     * @param ReferenceFormRequest $request
     * @param $lngCode
     * @param $referenceId
     * @param $referenceFormId
     * @return JsonResponse
     */
    public function update(ReferenceFormRequest $request, $lngCode, $referenceId, $referenceFormId)
    {
        $referenceTable = ReferenceTable::where('id', $referenceId)->firstOrFail();

        if ($referenceTable->authUserCanEditReference()) {
            $this->manager->update($request->validated(), $referenceId, $referenceFormId);
        }

        return responseResult('OK');
    }

    /**
     * Function to autocomplete ref data
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function autocompleteReferenceField(Request $request)
    {
        $this->validate($request, [
            'query' => 'required|string_with_max',
            'r' => 'required|integer_with_max|exists:reference_table,id',
        ]);

        $data['items'] = $this->manager->getRefDataByAutocomplete($request->get('query'), $request->get('r'));

        return response()->json($data);
    }

    /**
     * Function to Export CVS file by table structure
     *
     * @param $lngCode
     * @param $referenceId
     * @return BinaryFileResponse
     */
    public function export($lngCode, $referenceId)
    {
        $referenceTable = ReferenceTable::where('id', $referenceId)->firstorFail();

        ini_set('max_execution_time', -1);
        ini_set('memory_limit', '1024M');

        $referenceTableExportData = $this->exportManager->export($referenceId);

        return Excel::download($referenceTableExportData, "{$referenceTable->table_name}.csv");
    }

    /**
     * Function to return import view
     *
     * @param $lngCode
     * @param $referenceId
     * @return View|void
     */
    public function importShow($lngCode, $referenceId)
    {
        $referenceTable = ReferenceTable::where('id', $referenceId)->firstorFail();

        if (!$referenceTable->authUserCanEditReference()) {
            return abort('404');
        }

        return view('reference-table-form.import')->with([
            'referenceTable' => $referenceTable
        ]);
    }

    /**
     * Function to validate, truncate and store excel file
     *
     * @param Request $request
     * @param $lngCode
     * @param $referenceId
     * @return JsonResponse
     */
    public function import(Request $request, $lngCode, $referenceId)
    {
        ReferenceTable::where('id', $referenceId)->firstorFail();

        if (is_null($request->input('uploded_file'))) {
            return responseResult('INVALID_DATA', '', '', ['uploded_file' => "File is required"]);
        }

        ini_set('max_execution_time', -1);
        ini_set('memory_limit', '2048M');

        try {

            $pendingUploadFilePath = Storage::disk('shared_pending')->path($request->input('uploded_file'));

            Excel::import(new ReferenceTableFormImport(), Storage::disk('shared_pending')->path($request->input('uploded_file')));

            deleteFile($pendingUploadFilePath);

        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();

            $errorMessages = [];
            foreach ($failures as $key => $failure) {
                $errorMessages[$failure->row()]['rowInfo'] = trans('swis.reference_table_form.import.error.row.message', ['row' => $failure->row()]);

                foreach ($failure->errors() as $errorMessage) {
                    $errorMessages[$failure->row()]['errorMessages'][] = trans('swis.reference_table_form.import.error.field.message', ['field' => $failure->attribute()]) . '' . $errorMessage;
                }
            }

            ksort($errorMessages);

            return responseResult('INVALID_DATA', null, [], $errorMessages);
        }

        return responseResult('OK', urlWithLng("/reference-table-form/{$referenceId}/"));
    }

}
