<?php

namespace App\Http\Controllers;

use App\EntityTypesInfo\LegalEntityForms;
use App\Exceptions\TaxServiceTjCompanyInactiveException;
use App\Exceptions\TaxServiceTjInnNotFoundException;
use App\Exceptions\TaxServiceTjInvalidResponseException;
use App\Http\Requests\Agency\AgencySearchRequest;
use App\Http\Requests\Agency\AgencyStoreRequest;
use App\Http\Requests\Agency\AgencyUpdateRequest;
use App\Models\Agency\Agency;
use App\Models\Agency\AgencyManager;
use App\Models\Agency\AgencySearch;
use App\Models\ReferenceTable\ReferenceTable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Throwable;

/**
 * Class AgencyController
 * @package App\Http\Controllers
 */
class AgencyController extends BaseController
{
    /**
     * @var AgencyManager
     */
    protected $manager;

    /**
     * @var LegalEntityForms
     */
    protected $legalEntityForms;

    /**
     * AgencyController constructor.
     *
     * @param AgencyManager $manager
     * @param LegalEntityForms $legalEntityForms
     */
    public function __construct(AgencyManager $manager, LegalEntityForms $legalEntityForms)
    {
        $this->manager = $manager;
        $this->legalEntityForms = $legalEntityForms;
    }

    /**
     * Function to show all agencies info
     *
     * @return View
     */
    public function index()
    {
        return view('agency.index');
    }

    /**
     * Function to all data showing in datatable
     *
     * @param AgencySearchRequest $request
     * @param AgencySearch $agencySearch
     * @return JsonResponse
     */
    public function table(AgencySearchRequest $request, AgencySearch $agencySearch)
    {
        $data = $this->dataTableAll($request, $agencySearch);

        return response()->json($data);
    }

    /**
     * Function to show create view
     *
     * @return View
     */
    public function create()
    {
        return view('agency.edit')->with([
            'saveMode' => 'add',
            'spheres' => getReferenceRows(ReferenceTable::REFERENCE_SPHERE_TABLE_NAME),
            'hoursInterval' => getReferenceRows(ReferenceTable::REFERENCE_HOURS_INTERVAL, false, false, 'hours', '', 'id'),
            'indicators' => []
        ]);
    }

    /**
     * Function to Store data
     *
     * @param AgencyStoreRequest $request
     * @return JsonResponse
     *
     * @throws TaxServiceTjCompanyInactiveException
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function store(AgencyStoreRequest $request)
    {
        $company = $this->legalEntityForms->getCompanyInfo($request->input('tax_id'));

        if (!$company) {
            $errors['tax_id'] = trans('swis.tax_id.not_found');

            return responseResult('INVALID_DATA', '', '', $errors);
        }

        //
        $data = $request->validated();

        $data['address'] = $company['address'];
        $data['legal_entity_name'] = $company['name'];

        $agency = $this->manager->store($data);

        return responseResult('OK', null, ['id' => $agency->id]);
    }

    /**
     * Function to Show current agency by id
     *
     * @param $lngCode
     * @param $id
     * @param $viewType
     * @return View
     */
    public function edit($lngCode, $id, $viewType)
    {
        $this->manager->deleteWrongIndicators($id);

        return view('agency.edit')->with([
            'agency' => Agency::where('id', $id)->checkUserHasRole()->firstOrFail(),
            'spheres' => getReferenceRows(ReferenceTable::REFERENCE_SPHERE_TABLE_NAME),
            'hoursInterval' => getReferenceRows(ReferenceTable::REFERENCE_HOURS_INTERVAL, false, false, 'hours', '', 'id'),
            'workingHours' => Agency::select('working_hours_start', 'working_hours_end', 'lunch_hours_start', 'lunch_hours_end')->where('id', $id)->first(),
            'indicators' => $this->manager->getIndicators($id),
            'saveMode' => $viewType,
        ]);
    }

    /**
     * Function to Update data
     *
     * @param AgencyUpdateRequest $request
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     *
     * @throws TaxServiceTjCompanyInactiveException
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function update(AgencyUpdateRequest $request, $lngCode, $id)
    {
        $company = $this->legalEntityForms->getCompanyInfo($request->input('tax_id'));

        if (!$company) {
            $errors['tax_id'] = trans('swis.tax_id.not_found');

            return responseResult('INVALID_DATA', '', '', $errors);
        }

        //
        $data = $request->validated();

        $data['address'] = $company['address'];
        $data['legal_entity_name'] = $company['name'];

        $this->manager->update($data, $id);

        return responseResult('OK');
    }

    /**
     * Function to get indicators as search result
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function getIndicators(Request $request)
    {
        $validatedData = $request->validate([
            'sphere' => 'nullable|string_with_max',
            'indicator' => 'nullable|string_with_max',
            'hs_code' => 'nullable|string_with_max',
            'agency' => 'nullable|integer_with_max'
        ]);

        $table = view('agency.indicators-search-table')->with([
            'data' => $this->manager->getIndicatorsSearch($validatedData),
        ])->render();

        return responseResult('OK', null, $table);
    }

    /**
     * Function to store indicators in to db
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function saveIndicators(Request $request)
    {
        $validatedData = $request->validate([
            'indicators' => 'nullable|array',
            'agency' => 'nullable|integer_with_max'
        ]);

        $insertedFields = $this->manager->saveIndicators($validatedData);

        $table = view('agency.indicators-table')->with([
            'indicators' => $this->manager->getIndicators($validatedData['agency'], $insertedFields),
        ])->render();

        return responseResult('OK', null, $table);
    }

    /**
     * Function to delete indicator from db
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function deleteIndicators(Request $request)
    {
        $validatedData = $request->validate([
            'indicator' => 'required|exists:agency_lab_indicators,id',
            'agencyId' => 'required|exists:agency,id'
        ]);

        $this->manager->deleteIndicator($validatedData);

        return responseResult('OK');
    }
}
