<?php

namespace App\Http\Controllers;

use App\Models\Notifications\Notifications;
use Illuminate\View\View;

/**
 * Class DashboardController
 * @package App\Http\Controllers
 */
class DashboardController extends BaseController
{
    /**
     * Function to get user roles
     *
     * @return View
     */
    public function index()
    {
        return view('dashboard')->with([
            'notifications' => Notifications::dashboardNotifications()
        ]);
    }
}
