<?php

namespace App\Http\Controllers\Core\Translation;

/**
 * Class JsTrans
 * @package App\Http\Controllers\Core\Translation
 */
class JsTrans
{
    /**
     * @var array
     */
    private $data;

    /**
     * @param $keys
     */
    public function addTrans($keys)
    {
        foreach ($keys as $key) {
            $this->data[$key] = trans($key);
        }
    }

    /**
     * @return array
     */
    public function getTranslations()
    {
        return $this->data;
    }
}