<?php

namespace App\Http\Controllers\Core\Language;

use App\Http\Controllers\Core\Exceptions\LocalNotSetException;
use App\Models\Languages\Language;

/**
 * Class ApplicationLanguage
 * @package App\Http\Controllers\Core\Language
 */
class ApplicationLanguageManager
{
    /**
     * @var null
     */
    protected static $currentLanguage = null;

    /**
     * @return ApplicationLanguageManager|null
     */
    public static function getInstance()
    {
        if (self::$currentLanguage === null) {
            self::$currentLanguage = new self();
        }

        return self::$currentLanguage;
    }

    /**
     * @param $code
     * @return null|void
     *
     * @throws LocalNotSetException
     */
    public function setApplicationLanguageByCode($code)
    {
        $language = null;
        if (self::getCurrentLanguage() !== null) {
            return self::$currentLanguage;
        }

        $activeLanguages = activeLanguages();
        foreach ($activeLanguages as $lang) {
            if ($lang->code == $code) {
                $language = $lang;
            }
        }

        if (!$language || $language->show_status == Language::STATUS_INACTIVE) {
            throw new LocalNotSetException("Current application don't support this laguage");
        }

        self::$currentLanguage = $language;
    }

    /**
     * @return null
     */
    public function getCurrentLanguage()
    {
        return self::$currentLanguage;
    }
}