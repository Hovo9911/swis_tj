<?php

namespace App\Http\Controllers\Core\Language;

use App\Models\Application\ApplicationLanguages;
use Illuminate\Contracts\Routing\UrlGenerator;

/**
 * Class LanguageManager
 * @package App\Http\Controllers\Core\Language
 */
class LanguageManager
{
    /**
     * @var null
     */
    private static $instanceLngManager = null;
    private static $activeLanguages = null;

    /**
     * @var array
     */
    private $languages = [];

    /**
     * @var array
     */
    private $availableLanguages = [];

    /**
     * @return LanguageManager|null
     */
    public static function getInstance()
    {
        if (self::$instanceLngManager === null) {
            self::$instanceLngManager = new self();
        }

        return self::$instanceLngManager;
    }

    /**
     * @return array
     */
    public function getLanguages()
    {
        return self::getActiveLanguages();
    }

    /**
     * @param $lng
     * @return UrlGenerator|string
     */
    public function getUrl($lng)
    {
        $availableLanguages = $this->getAvailableLanguageMap();
        if (isset($availableLanguages[$lng->id])) {
            return url($lng->code . '/' . $availableLanguages[$lng->id]);
        }

        return url('/' . $lng->code);
    }

    /**
     * @return array
     */
    public function getAvailableLanguageMap()
    {
        if (!empty($this->availableLanguages)) {
            return $this->availableLanguages;
        }

        $requestBaseUrl = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
        $requestBaseUrl = str_replace(array("<", ">", ";", "\"", "alert", "(", ")", "'", "%22", "%27"), "", $requestBaseUrl);
        $requestBaseUrl = explode('/', $requestBaseUrl);
        if (isset($requestBaseUrl[1])) {
            unset($requestBaseUrl[1]);
        }
        $requestBaseUrl = implode('/', $requestBaseUrl);
        $languages = $this->getLanguages();
        foreach ($languages as $lng) {
            $this->availableLanguages[$lng->id] = ltrim($requestBaseUrl, '/');
        }

        return $this->availableLanguages;
    }

    /**
     * @return null
     */
    public static function getActiveLanguages()
    {
        if (self::$activeLanguages === null) {
            (new self)->setActiveLanguages();
        }

        return self::$activeLanguages;
    }

    /**
     * Function to set active languages
     */
    public function setActiveLanguages()
    {
        $application = ApplicationLanguages::with(['language'])->where(['application_id' => 1])->get();

        $languages = [];
        foreach ($application as $value) {
            $languages[] = (object)$value->language->toArray();
        }

        self::$activeLanguages = collect($languages)->sortBy('sort_order');
    }


}