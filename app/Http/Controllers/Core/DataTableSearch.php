<?php

namespace App\Http\Controllers\Core;

use App\Http\Controllers\Core\Interfaces\DataTableSearchContract;

/**
 * Class DataTableSearch
 * @package App\Http\Controllers
 */
abstract class DataTableSearch implements DataTableSearchContract
{
    /**
     *  Search by data
     *
     * @var  array
     */
    protected $searchData = array();

    /**
     * If true, set pagingCount max value 100
     *
     * and not allow higher values( to prevent DDOS attacks )
     * @var boolean
     */
    protected $limitPagination = true;

    /**
     * @var null
     */
    protected $orderByCol = null;
    protected $orderByType = null;

    /**
     * @var array
     */
    protected $selectData = array();

    /**
     * @var int
     */
    protected $pagingStart = 0;
    protected $pagingCount = 100;

    /**
     * @param $adapterClass
     * @param $searchClass
     * @return mixed
     */
    protected static function createInstance($adapterClass, $searchClass)
    {
        $searchObj = null;
        if (class_exists($adapterClass)) {
            $searchObj = new $adapterClass();
            if (!is_subclass_of($searchObj, $searchClass)) {
                throw new UIS_Application_Exception(" $adapterClass  not  instance of  <{$searchClass}> class ");
            }
        } else {
            throw new UIS_Application_Exception(" $adapterClass  not  found ");
        }
        return $searchObj;
    }

    /**
     * @param $coll
     */
    public function addSelectData($coll)
    {
        if (is_array($coll)) {
            foreach ($coll as $collName) {
                $this->selectData[$collName] = $collName;
            }
        } else {
            $coll = strval($coll);
            $this->selectData[$coll] = $coll;
        }

    }

    /**
     * @param $byCol
     * @param string $orderType
     * @param null $orderTable
     */
    public function orderBy($byCol, $orderType = 'desc', $orderTable = null)
    {

        if ($orderTable != null) {
            $this->orderByCol = $orderTable . '.' . $byCol;
        } else {
            $this->orderByCol = $byCol;
        }
        $this->orderByType = $orderType;
    }

    /**
     * @param $start
     * @param $count
     */
    public function limit($start, $count)
    {
        $this->pagingStart = intval($start);
        $this->pagingCount = intval($count);
        if ($this->limitPagination === true) {
            $this->pagingCount = $count >= 0 && $count <= 100 ? $count : 100;
        }
    }

    /**
     * limitPagination
     */
    public function enablePaginationLimit()
    {
        $this->limitPagination = true;
    }

    /**
     * limitPagination
     */
    public function disablePaginationLimit()
    {
        $this->limitPagination = false;
    }

    /**
     *  Set search by value, if value is not false,
     *  else   return $key value
     * @param string $key
     * @param bool $value
     * @return  null|array|void
     */
    public function searchBy($key = null, $value = false)
    {
        if ($key === null) {
            return $this->searchData;
        }
        if (is_array($key)) {
            foreach ($key as $filterKey => $filterVal) {
                $this->searchBy($filterKey, $filterVal);
            }
        } else {
            if ($value !== false && (!is_string($value) || trim($value) != '')) {
                $this->searchData[$key] = $value;
            } else if (isset($this->searchData[$key])) {
                return $this->searchData[$key];
            } else {
                return null;
            }
        }
    }

    /**
     * Remove search by data by it key
     *
     * @param string $key
     * @return  boolean  true if key exists, else - false
     */
    public function removeSearchBy($key)
    {
        if (isset($this->searchData[$key])) {
            unset($this->searchData[$key]);
            return true;
        }

        return false;
    }

}