<?php
/**
 * Created by PhpStorm.
 * User: gev
 * Date: 8/6/18
 * Time: 4:55 PM
 */

namespace App\Http\Controllers\Core\ModelManager;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DynamicModel
 * @package App\Http\Controllers\Core\ModelManager
 */
class DynamicModel extends Model
{
    public $currentClass;

    /**
     * DynamicModel constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @param string $table
     * @return DynamicModel|void
     */
    public function setTable($table)
    {
        $this->table = $table;
    }

    /**
     * @param $className
     */
    public function setCurrentClass($className)
    {
        $this->currentClass = $className;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function current()
    {
        return $this->hasOne($this->currentClass, $this->table . '_id', 'id')->where('lng_id');
    }
}