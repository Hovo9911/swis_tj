<?php

namespace App\Http\Controllers\Core\ModelManager;

/**
 * Class ModelManager
 * @package App\Http\Controllers\Core\ModelManager
 */
class ModelManager
{
    /**
     * @param $tableName
     * @param null $currentClass
     * @return DynamicModel
     */
    public static function getModel($tableName, $currentClass = null)
    {
        $model = new DynamicModel();

        $model->setTable($tableName);

        if (!is_null($currentClass)) {

            $model->setCurrentClass($currentClass);
        }

        return $model;
    }
}