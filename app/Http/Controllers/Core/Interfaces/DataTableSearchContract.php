<?php

namespace App\Http\Controllers\Core\Interfaces;


interface DataTableSearchContract
{
    public function searchRowsCount();

    public function rowsCount();

    public function search();

    public function limit($start, $length);

    public function orderBy($coll, $orderType);

    public function searchBy($filterData);
}