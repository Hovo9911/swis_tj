<?php

namespace App\Http\Controllers\Core\Interfaces;


interface DictionaryContract
{
    public function editKey($key, $mls, $appType);

    public function deleteKey($keys, $appType);

    public function parseTransFile($appType, $lngCode, $onlyFile = false);

    public function getUpdatedAt($appType, $lngCode);
}