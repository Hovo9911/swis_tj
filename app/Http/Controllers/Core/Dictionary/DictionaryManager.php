<?php

namespace App\Http\Controllers\Core\Dictionary;

use App\Http\Controllers\Core\Interfaces\DictionaryContract;
use App\Models\Languages\Language;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class DictionaryManager
 * @package App\Http\Controllers\Core\Dictionary
 */
class DictionaryManager implements DictionaryContract
{
    /**
     * Function to delete key
     *
     * @param $deleteItems
     * @param $appType
     */
    public function deleteKey($deleteItems, $appType)
    {
//        DB::transaction(function () use ($deleteItems, $appType) {
//
//            Dictionary::whereIn('id', $deleteItems)->where(['application_type' => $appType])->delete();
//            DictionaryMl::whereIn('dictionary_id', $deleteItems)->delete();
//
//        });
    }

    /**
     * Function to edit key
     *
     * @param $key
     * @param $mls
     * @param $appType
     */
    public function editKey($key, $mls, $appType)
    {
        $dictionaryData = [
            'key' => $key,
            'application_type' => $appType
        ];

        $dictionary = Dictionary::firstOrNew($dictionaryData);
        $dictionary->updated_at = Carbon::now();
        $dictionary->save();

        $updatedLngCodes = [];
        foreach ($mls as $lngCode => $translate) {
            $lngId = Language::where(['code' => $lngCode])->pluck('id')->first();

            $updatedLngCodes[] = $lngCode;
            DictionaryMl::updateOrCreate(
                [
                    'dictionary_id' => $dictionary->id,
                    'lng_id' => $lngId,
                ]
                ,
                [
                    'dictionary_id' => $dictionary->id,
                    'lng_id' => $lngId,
                    'value' => empty($translate) ? '' : $translate
                ]);
        }

        if (count($updatedLngCodes)) {
            foreach ($updatedLngCodes as $locale) {
                $trans = $this->getTransFromDB($appType, $locale);
                $this->updateTransJsonFile($trans, $locale);
            }
        }
    }

    /**
     * Function to get trans
     *
     * @param $appType
     * @param $lngCode
     * @return Dictionary[]|Builder[]|Collection
     */
    private function getTrans($appType, $lngCode)
    {
        $lngId = Language::where(['code' => $lngCode])->pluck('id')->first();

        return Dictionary::with(['ml' => function ($q) use ($lngId) {
            $q->where(['lng_id' => $lngId])->get();
        }])->where(['application_type' => $appType])->orderBy('id', 'desc')->get();
    }

    /**
     * Function to get translates for lang
     *
     * @param $appType
     * @param $locale
     * @return array|null
     */
    public function getTranslatesForLng($appType, $locale)
    {
        if ($this->getTransJsonFile($locale)) {
            return $this->getTransJsonFile($locale);
        }

        $trans = $this->getTransFromDB($appType, $locale);

        if (count($trans)) {
            $this->updateTransJsonFile($trans, $locale);
        }

        return $trans;
    }

    /**
     * Function to parse trans file
     *
     * @param $appType
     * @param $lngCode
     * @param bool $onlyFile
     * @return array
     */
    public function parseTransFile($appType, $lngCode, $onlyFile = false)
    {
        $transFile = $this->getTrans($appType, $lngCode);

        $data = [];
        foreach ($transFile as $key => $trans) {
            if (empty($data[$trans['key']])) {
                foreach ($trans->ml as $ml) {
                    $data[$trans['id']][$lngCode] = $ml->value;
                    $data[$trans['id']]['key'] = $trans['key'];
                }
            }
        }

        return $data;
    }

    /**
     * Function to getUpdatedAt
     *
     * @param $appType
     * @param $lngCode
     */
    public function getUpdatedAt($appType, $lngCode)
    {

    }

    /**
     * Function to get trans from database
     *
     * @param $appType
     * @param $locale
     * @return array
     */
    public function getTransFromDB($appType, $locale)
    {
        $language = Language::select('id')->where('code', $locale)->first();
        if (!is_null($language)) {
            $translations = DB::table('dictionary')
                ->join('dictionary_ml', 'dictionary.id', '=', 'dictionary_ml.dictionary_id')
                ->select('dictionary.key', 'dictionary_ml.lng_id', 'dictionary_ml.value')
                ->where(['dictionary.application_type' => $appType, 'dictionary_ml.lng_id' => $language->id])
                ->get();

            return $translations->pluck('value', 'key');
        }

        return [];
    }

    /**
     * Function to get trans json file from storage
     *
     * @param $lngCode
     * @return bool|mixed
     */
    public function getTransJsonFile($lngCode)
    {
        $fileName = storage_path("trans/dictionary_$lngCode.json");
        if (file_exists($fileName)) {
            return json_decode(file_get_contents($fileName), true);
        }

        return false;
    }

    /**
     * Function to update trans json file
     *
     * @param $trans
     * @param $lngCode
     */
    public function updateTransJsonFile($trans, $lngCode)
    {
        $fileName = "dictionary_$lngCode.json";
        $fileDir = storage_path("trans");

        if (!is_dir($fileDir)) {
            mkdir($fileDir, 0775, true);
        }

        $fileName = $fileDir . '/' . $fileName;

        file_put_contents($fileName, $trans);
    }

    /**
     * Function to get translation by language code
     *
     * @param null $key
     * @param $locale
     * @return array
     */
    public function getTransByLngCode($key = null, $locale = false)
    {
        if (!is_null($locale)) {

            $locale = $locale ?: cLng('id');

            $translations = DB::table('dictionary')
                ->join('dictionary_ml', 'dictionary.id', '=', 'dictionary_ml.dictionary_id')
                ->select('dictionary_ml.value')
                ->where(['dictionary.key' => $key, 'dictionary_ml.lng_id' => $locale])
                ->first();

            return optional($translations)->value ?? $key;
        }

        return [];
    }

    /**
     * Function to get translation by language code
     *
     * @param null $template
     * @param $lngId
     * @return array
     */
    public function getTransKeysByLng($template = null, $lngId = false)
    {

        $translations = [];
        if ($lngId) {

            $lngId = $lngId ?: cLng('id');

            $dictionaryInfo = DB::table('dictionary')
                ->join('dictionary_ml', 'dictionary.id', '=', 'dictionary_ml.dictionary_id')
                ->select('dictionary.key', 'dictionary_ml.value')
                ->where('dictionary.key', 'like', "%{$template}%")
                ->where('dictionary_ml.lng_id', $lngId)
                ->get();

            foreach ($dictionaryInfo as $dictionary) {
                $translations[$dictionary->key] = optional($dictionary)->value ?? $dictionary->key;
            }
        }

        return $translations;
    }

    /**
     * Function to get logged data
     *
     * @param null $key
     * @param null $parentId
     * @return array
     */
    public function getLogData($key = null, $parentId = null)
    {
        $dictionary = Dictionary::where('key', $key)->first();

        if (!is_null($dictionary)) {
            $logData = $dictionary;
            $logData['mls'] = $dictionary->ml()->get()->toArray();

            return $logData;
        }

        return [];
    }
}