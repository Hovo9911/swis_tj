<?php

namespace App\Http\Controllers\Core\Dictionary;

use App\Http\Controllers\Core\Language\LanguageManager;
use Exception;
use Illuminate\Contracts\Translation\Translator as TranslatorInterface;
use Illuminate\Support\Facades\DB;
use InvalidArgumentException;

/**
 * Class Translator
 * @package App\Http\Controllers\Core\Dictionary
 */
class Translator implements TranslatorInterface
{
    /**
     * @var array
     */
    private $dictionary = [];

    private $appName;

    /**
     * Translator constructor.
     *
     * @param $appName
     */
    public function __construct($appName)
    {
        $this->appName = $appName;
    }

    /**
     * @param string $key
     * @param array $parameters
     * @param null $locale
     * @return string
     */
    public function trans($key, array $parameters = [], $locale = null)
    {
        try {
            DB::connection()->getPdo();
        } catch (Exception $e) {
            return $key;
        }

        $manager = new DictionaryManager();
        if ($locale === null) {
            $locale = request()->segment(1);
            $activeLanguages = LanguageManager::getActiveLanguages();

            $languagesArr = $activeLanguages->keyBy('code')->toArray();
            if (!isset($languagesArr[$locale])) {
                $locale = config('app.locale');
            }
        }

        if (empty($this->dictionary)) {
            $this->dictionary = $manager->getTranslatesForLng($this->appName, $locale);
        }

        if (!empty($this->dictionary[(string)$key])) {
            $transStr = $this->dictionary[(string)$key];

            if (!empty($parameters)) {
                foreach ($parameters as $repKey => $repStr) {
                    if (is_string($repStr) || is_int($repStr)) {
                        $transStr = str_replace('{' . $repKey . '}', $repStr, $transStr);
                    }
                }
            }
            return $transStr;
        }

        return $key;

    }

    /**
     * @return array
     */
    public function getDictionary()
    {
        return $this->dictionary;
    }

    /**
     * Translates the given choice message by choosing a translation according to a number.
     *
     * @param string $id The message id (may also be an object that can be cast to string)
     * @param int $number The number to use to find the indice of the message
     * @param array $parameters An array of parameters for the message
     * @param string|null $domain The domain for the message or null to use the default
     * @param string|null $locale The locale or null to use the default
     *
     * @return string The translated string
     *
     * @throws InvalidArgumentException If the locale contains invalid characters
     *
     * @api
     */
    public function transChoice($id, $number, array $parameters = array(), $domain = null, $locale = null)
    {
        // TODO: Implement transChoice() method.
    }

    /**
     * Sets the current locale.
     *
     * @param string $locale The locale
     *
     * @throws InvalidArgumentException If the locale contains invalid characters
     *
     * @api
     */
    public function setLocale($locale)
    {
        // TODO: Implement setLocale() method.
    }

    /**
     * Returns the current locale.
     *
     * @return string The locale
     *
     * @api
     */
    public function getLocale()
    {
        // TODO: Implement getLocale() method.
    }

}