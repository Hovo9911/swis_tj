<?php

namespace App\Http\Controllers\Core\Dictionary;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Language
 * @package App\Models\Languages
 */
class Dictionary extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'dictionary';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var bool
     */
    protected $hasAdminInfo = true;

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'key',
        'application_type',
    ];

    /**
     * @var array
     */
    protected $defaultColumns = [
        'id',
        'key',
        'application_type',
    ];

    /**
     * @var int
     */
    const APP_TYPE_ADM_ID = 1;

    /**
     * @return HasMany
     */
    public function ml()
    {
        return $this->hasMany(DictionaryMl::class, 'dictionary_id', 'id');
    }

}
