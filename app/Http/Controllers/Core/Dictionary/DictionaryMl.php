<?php

namespace App\Http\Controllers\Core\Dictionary;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

/**
 * Class Language
 * @package App\Models\Languages
 */
class DictionaryMl extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var string
     */
    public $table = 'dictionary_ml';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var bool
     */
    protected $hasAdminInfo = true;

    /**
     * @var array
     */
    protected $attributes = [];

    /**
     * @var array
     */
    protected $fillable = [
        'dictionary_id',
        'lng_id',
        'value'
    ];

    /**
     * @var array
     */
    protected $primaryKey = ['dictionary_id', 'lng_id'];

    /**
     * @var string
     */
    public $incrementing = 'false';


}
