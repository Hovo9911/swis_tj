<?php

namespace App\Http\Controllers\Core\Dictionary;

use App\Http\Controllers\Core\DataTableSearch;
use App\Http\Controllers\Core\Interfaces\DictionaryContract;
use App\Models\Application\Application;

/**
 * Class DictionarySearch
 * @package App\Http\Controllers\Core\Dictionary
 */
class DictionarySearch extends DataTableSearch
{
    /**
     * @var DictionaryContract
     */
    protected $dictionaryManager;

    /**
     * DictionarySearch constructor.
     * @param DictionaryContract $dictionaryManager
     */
    public function __construct(DictionaryContract $dictionaryManager)
    {
        $this->dictionaryManager = $dictionaryManager;
    }

    /**
     * @return integer
     */
    public function searchRowsCount()
    {
        return 0;
    }

    /**
     * @return integer
     */
    public function rowsCount()
    {
        return 0;
    }

    /**
     * Search dictionaries by appType and lngCode
     *
     * @return array
     */
    public function search()
    {
        if (empty($this->searchData['appType']) || !is_string($this->searchData['appType'])) {
            return [];
        }

        $app = $this->searchData['appType'];
        $q = empty($this->searchData['q']) || !is_string($this->searchData['q']) ? '' : $this->searchData['q'];
        $app = Application::where('name', $app)->with('languages')->first();

        if (empty($app)) {
            return [];
        }

        $languages = $app->languages;

        $data = [];

        foreach ($languages as $lng) {
            $transFile = Dictionary::select('id', 'key', 'lng_id', 'value')
                ->leftJoin('dictionary_ml', 'dictionary.id', '=', 'dictionary_ml.dictionary_id')
                ->where(['application_type' => $app->name, 'dictionary_ml.lng_id' => $lng->id])
                ->orderBy('id', 'desc');

            if(empty($this->searchData['q']) && request()->input('length') != '-1'){
                $transFile->limit($this->pagingCount);
            }

            $transFile = $transFile->get();

            foreach ($transFile as $key => $trans) {

                $data[$key][$lng->code] = $trans['value'] ?? '';
                $data[$key]['key'] = $trans['key'] ?? '';
                $data[$key]['id'] = $key;
            }
        }

        if (!empty($this->searchData['q']) && is_string($this->searchData['q'])) {
            $this->filterByQuery($q, $data);
        }

        return array_values($data);
    }

    /**
     * filterByQuery
     *
     * @param $q
     * @param $data
     */
    private function filterByQuery($q, &$data)
    {
        foreach ($data as $key => $transes) {
            if (stripos($key, $q) !== false) {
                continue;
            }

            $foundInTranses = false;
            foreach ($transes as $trans) {
                if (stripos($trans, $q) !== false) {
                    $foundInTranses = true;
                }
            }

            if ($foundInTranses) {
                continue;
            }

            unset($data[$key]);
        }
    }

}