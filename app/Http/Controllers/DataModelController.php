<?php

namespace App\Http\Controllers;

use App\Http\Requests\DataModel\DataModelRequest;
use App\Http\Requests\DataModel\DataModelSearchRequest;
use App\Models\DataModel\DataModel;
use App\Models\DataModel\DataModelManager;
use App\Models\DataModel\DataModelSearch;
use App\Models\ReferenceTable\ReferenceTableManager;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

/**
 * Class DataModelController
 * @package App\Http\Controllers
 */
class DataModelController extends BaseController
{
    /**
     * @var DataModelManager
     */
    protected $manager;

    /**
     * @var ReferenceTableManager
     */
    protected $referenceTableManager;

    /**
     * DataModelController constructor.
     *
     * @param DataModelManager $manager
     * @param ReferenceTableManager $referenceTableManager
     */
    public function __construct(DataModelManager $manager, ReferenceTableManager $referenceTableManager)
    {
        $this->manager = $manager;
        $this->referenceTableManager = $referenceTableManager;
    }

    /**
     * Function to return view data-model page with data
     *
     * @return View
     */
    public function index()
    {
        return view('data-model.index')->with([
            'dataModels' => DataModel::all()
        ]);
    }

    /**
     * Function to showing all data in datatable
     *
     * @param DataModelSearchRequest $dataModelSearchRequest
     * @param DataModelSearch $agencySearch
     * @return JsonResponse
     */
    public function table(DataModelSearchRequest $dataModelSearchRequest, DataModelSearch $agencySearch)
    {
        $data = $this->dataTableAll($dataModelSearchRequest, $agencySearch);

        return response()->json($data);
    }

    /**
     * Function to return view to data-model page
     *
     * @return View
     */
    public function create()
    {
        return view('data-model.edit')->with([
            'saveMode' => 'add'
        ]);
    }

    /**
     * Function to Store Data
     *
     * @param DataModelRequest $request
     * @return JsonResponse
     */
    public function store(DataModelRequest $request)
    {
        $dataModel = $this->manager->store($request->validated());

        return responseResult('OK', null, ['id' => $dataModel->id]);
    }

    /**
     * Function to show current data-model by id
     *
     * @param $lngCode
     * @param $id
     * @param $viewType
     * @return View
     */
    public function edit($lngCode, $id, $viewType)
    {
        $dataModel = DataModel::where(DB::raw('id::varchar'), $id)->firstOrFail();

        $wcoClassNames = [];
        $wcoClassNamesAndCodes = [];

        if (!empty($dataModel->wco)) {

            $classInfo = DB::connection('wco')
                ->table('wco_dataset')
                ->select('wco_dataset.class_name as class_name', 'wco_data_model.wco_id as wco_id')
                ->leftJoin('wco_data_model', 'wco_dataset.class_name', '=', 'wco_data_model.class_name')
                ->where('wco_dataset.wco_id', $dataModel->wco)
                ->get()
                ->toArray();

            $classInfo = array_map(function ($value) {
                return (array)$value;
            }, $classInfo);

            foreach ($classInfo as $classItem) {
                $wcoClassNamesAndCodes[] = $classItem['wco_id'] . ' - ' . $classItem['class_name'];
                $wcoClassNames[] = $classItem['class_name'];
            }
        }

        return view('data-model.edit')->with([
            'wcoClassNames' => $wcoClassNames,
            'wcoClassNamesAndCodes' => $wcoClassNamesAndCodes,
            'dataModel' => $dataModel,
            'saveMode' => $viewType
        ]);
    }

    /**
     * Function to update data
     *
     * @param DataModelRequest $request
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     */
    public function update(DataModelRequest $request, $lngCode, $id)
    {
        $this->manager->update($request->validated(), $id);

        return responseResult('OK');
    }

    /**
     * Function to autocomplete get field all info
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function autocomplete(Request $request)
    {
        $this->validate($request, [
            'type' => 'required|in:' . implode(',', DataModel::STANDARDS_ALL),
            'query' => 'nullable|string_with_max',
            'eaeuType' => 'nullable|string_with_max'
        ]);

        $data = $this->manager->getFieldTypeAndAllInfo($request->get('type'), $request->get('query'), $request->get('eaeuType'));

        return response()->json($data);
    }

    /**
     * Function to get info from reference table
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function referenceTableSearch(Request $request)
    {
        $this->validate($request, [
            'q' => 'required|string_with_max',
            'ref' => 'nullable|string_with_max'
        ]);

        $data['items'] = $this->referenceTableManager->searchReferenceTables($request->get('q'), $request->get('ref'));

        return response()->json($data);
    }
}
