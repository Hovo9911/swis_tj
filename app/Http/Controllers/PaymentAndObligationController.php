<?php

namespace App\Http\Controllers;

use App\Exports\PaymentAndObligationXlsExport;
use App\Http\Controllers\Core\Interfaces\DataTableSearchContract;
use App\Http\Requests\PaymentAndObligation\PaymentAndObligationSearchRequest;
use App\Models\Agency\Agency;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\PaymentAndObligation\PaymentAndObligationSearch;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplicationNotes\SingleApplicationNotes;
use App\Models\SingleApplicationObligations\SingleApplicationObligations;
use App\Models\Transactions\TransactionsManager;
use App\Models\UserDocuments\UserDocumentsNotificationsManager;
use App\Payments\PaymentsManager;
use App\Pdf\PdfManager;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Throwable;

/**
 * Class PaymentAndObligationController
 * @package App\Http\Controllers
 */
class PaymentAndObligationController extends BaseController
{
    /**
     * @var PdfManager
     */
    protected $pdfManager;

    /**
     * PaymentAndObligationController constructor.
     * @param PdfManager $pdfManager
     */
    public function __construct(PdfManager $pdfManager)
    {
        $this->pdfManager = $pdfManager;
    }

    /**
     * Function to return index page
     *
     * return View
     */
    public function index()
    {
        $companyTaxId = Auth::user()->isSwAdmin() ? null : Auth::user()->companyTaxId();
        $getObligationSum = number_format(SingleApplicationObligations::getObligationsSum('obligation', $companyTaxId), '2', '.', '');
        $getPaymentSum = number_format(SingleApplicationObligations::getObligationsSum('payment', $companyTaxId), '2', '.', '');
        $getBalanceSum = number_format(SingleApplicationObligations::getObligationsSum('balance', $companyTaxId), '2', '.', '');

        $subDivisions = collect();
        $constructorDocuments = collect();

        if (Auth::user()->isSwAdmin()) {
            $constructorDocuments = ConstructorDocument::getConstructorDocuments();
            $subDivisions = getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS);
        } elseif (Auth::user()->isAgency()) {
            $constructorDocuments = ConstructorDocument::getConstructorDocumentForAgency();
            $subDivisions = Auth::user()->getPaymentObligationsAvailableRefSubdivisions();
        }

        $obligationType = getReferenceRows('obligation_type');
        $booleanValues = getReferenceRows('bulian_values');

        return view('payment-and-obligations.index')->with([
            'getObligationSum' => $getObligationSum,
            'getPaymentSum' => $getPaymentSum,
            'getBalanceSum' => $getBalanceSum,
            'agencies' => Agency::allData([],'all'),
            'subDivisions' => $subDivisions,
            'constructorDocuments' => $constructorDocuments,
            'obligationType' => $obligationType,
            'booleanValues' => $booleanValues,
        ]);
    }

    /**
     * Function to get all data showing in datatable
     *
     * @param PaymentAndObligationSearchRequest $paymentAndObligationSearchRequest
     * @param PaymentAndObligationSearch $paymentAndObligationSearch
     * @return JsonResponse
     */
    public function table(PaymentAndObligationSearchRequest $paymentAndObligationSearchRequest, PaymentAndObligationSearch $paymentAndObligationSearch)
    {
        $data = $this->dataTableAll($paymentAndObligationSearchRequest, $paymentAndObligationSearch);

        return response()->json($data);
    }

    /**
     * Function will be write for override the BaseController method and return custom parameter
     *
     * @param Request $request
     * @param DataTableSearchContract $searchManager
     * @return array
     */
    public function dataTableAll(Request $request, DataTableSearchContract $searchManager)
    {
        $draw = $request->get('draw');
        $start = $request->get('start');
        $length = $request->get('length');
        $filterData = isset($request['f']) ? $request['f'] : [];
        $sSearch = empty($request['search']['value']) || !is_string($request['search']['value']) ? '' : $request['search']['value'];

        // limit offset
        $searchManager->limit($start, $length);

        // Order part
        $order = $request->input('order');
        if (!empty($order)) {

            $columns = $request->input('columns');

            $searchManager->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir']);
        }

        if (count($filterData) > 0) {
            $searchManager->searchBy($filterData);
        }

        if (!empty($sSearch)) {
            $searchManager->searchBy('q', $sSearch);
        }

        $result = $searchManager->search();
        $total = number_format($searchManager->searchTotal(), 2, '.', '');

        return [
            'draw' => $draw,
            'recordsTotal' => $searchManager->rowsCount(),
            'recordsFiltered' => $searchManager->searchRowsCount(),
            'data' => $result,
            'total' => $total
        ];
    }

    /**
     * Function to check and return modal body
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function payObligationsModal(Request $request)
    {
        $this->validate($request, [
            'data' => 'required',
        ]);

        $obligationIds = json_decode($request->input('data'));
        $sum = SingleApplicationObligations::getObligationsSum('saf_obligations.obligation', null, $obligationIds);

        $userBalance = Auth::user()->balance();
        $params = ['pay' => true, 'sum' => $sum, 'obligationIds' => $obligationIds, 'params' => []];

        $data['html'] = view('single-application.obligations.pay-obligation-modal-content')->with($params)->render();

        /*if ($userBalance >= $sum) {

        } else {
            $params['params'] = PaymentsManager::createAlifPayment();
            $params['pay'] = 'refill';
            $data['html'] = view('single-application.obligations.pay-obligation-modal-content')->with($params)->render();
        }*/

        return responseResult('OK', '', $data);
    }

    /**
     * Function to pay for obligation and update user balance
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function payObligations(Request $request)
    {
        $this->validate($request, [
            'sum' => 'required',
            'obligations' => 'required'
        ]);

        $userBalance = 10000000.00; // Auth::user()->balance();
        $getSum = $request->input('sum');

        $obligationIds = $request->input('obligations');
        $sum = SingleApplicationObligations::getObligationsSum('saf_obligations.obligation', null, $obligationIds);

        $saf = SingleApplicationObligations::select('saf_obligations.id', 'saf.regular_number', 'saf_obligations.obligation', 'saf_obligations.subapplication_id')
            ->join('saf', 'saf.regular_number', '=', 'saf_obligations.saf_number')
            ->whereIn('saf_obligations.id', $obligationIds)
            ->get();

        if ($getSum == $sum && $userBalance >= $sum) {
            $transactionsManager = new TransactionsManager();

            DB::transaction(function () use ($transactionsManager, $request, $sum, $userBalance, $saf) {

                $userType = SingleApplicationObligations::SAF_OBLIGATIONS_TYPE_NATURAL_PERSON;
                if (Auth::user()->isSwAdmin()) {
                    $userType = SingleApplicationObligations::SAF_OBLIGATIONS_TYPE_SW_ADMIN;
                } elseif (Auth::user()->isAgency() || Auth::user()->isCompany()) {
                    $userType = SingleApplicationObligations::SAF_OBLIGATIONS_TYPE_AGENCY;
                } elseif (Auth::user()->isOnlyBroker()) {
                    $userType = SingleApplicationObligations::SAF_OBLIGATIONS_TYPE_BROKER;
                }

                foreach ($saf as $item) {
                    $userBalance = Auth::user()->balance();

                    $transactionsManager->store([
                        'payment_id' => null,
                        'saf_number' => $item->regular_number,
                        'user_id' => Auth::user()->id,
                        'company_tax_id' => Auth::user()->companyTaxId(),
                        'type' => 'output',
                        'transaction_date' => Carbon::now(),
                        'amount' => -$item->obligation,
                        'total_balance' => number_format($userBalance - $item->obligation, 2, '.', ''),
                        'obligation_id' => $item->id
                    ]);

                    $obligation = SingleApplicationObligations::find($item->id);
                    $obligation->payment = $item->obligation;
                    $obligation->payment_date = Carbon::now();
                    $obligation->payer_user_type = $userType;
                    $obligation->payer_company_tax_id = Auth::user()->companyTaxId();
                    $obligation->payer_user_id = Auth::id();
                    $obligation->is_paid = true;
                    $obligation->save();

                    SingleApplicationNotes::storeNotes([
                        'saf_number' => $item->regular_number,
                        'sub_application_id' => $item->subapplication_id,
                        'obligation_id' => $item->id,
                    ], SingleApplicationNotes::NOTE_TRANS_PAY_OBLIGATION_TYPE);

                    $userDocumentsNotificationsManager = new UserDocumentsNotificationsManager();
                    $userDocumentsNotificationsManager->storeNotification($obligation);
                }
            });

            return responseResult('OK', urlWithLng("/payment-and-obligations"));
        } else {
            $errors['invalid_sum'] = trans('swis.saf.modal.invalid_sum.title');
            return responseResult('INVALID_DATA', '', '', $errors);
        }
    }

    /**
     * Function to get Table result total pay
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function getTableTotalPay(Request $request)
    {
        $query = SingleApplicationObligations::select('saf_obligations.id as id')
            ->join('saf_sub_applications', 'saf_sub_applications.id', '=', 'saf_obligations.subapplication_id')
            ->join('saf', 'saf.regular_number', '=', 'saf_obligations.saf_number')
            ->leftJoin('users', 'users.id', '=', 'saf.creator_user_id')
            ->join('agency', 'agency.tax_id', '=', 'saf.company_tax_id')
            ->join('constructor_documents', function ($query) {
                $query->on('constructor_documents.document_code', '=', 'saf_sub_applications.document_code')->where('constructor_documents.show_status', '1');
            })
            ->join('reference_obligation_budget_line', function ($query) {
                $query->on('reference_obligation_budget_line.id', '=', 'saf_obligations.ref_budget_line_id')->where('reference_obligation_budget_line.show_status', '1');
            })
            ->join('reference_obligation_creation_type', 'reference_obligation_creation_type.id', '=', 'reference_obligation_budget_line.obligation_creation_type')
            ->join('reference_obligation_budget_line_ml', 'reference_obligation_budget_line_ml.reference_obligation_budget_line_id', '=', 'reference_obligation_budget_line.id')
            ->where('reference_obligation_budget_line_ml.lng_id', cLng('id'));

        if (!Auth::user()->isSwAdmin()) {
            $query->where(function ($q) {
                $q->safOwnerOrCreator()->orWhere('saf_sub_applications.company_tax_id', Auth::user()->companyTaxId());
            });
        }

        $query->when($request->input('saf_number'), function ($q) use ($request) {
            $q->where('saf.regular_number', 'ILIKE', "%{$request->input('saf_number')}%");
        })->when($request->input('sub_application_number'), function ($q) use ($request) {
            $q->where('saf_sub_applications.document_number', 'ILIKE', "%{$request->input('sub_application_number')}%");
        })->when($request->input('tin_importer_exporter'), function ($q) use ($request) {
            $q->whereRaw("(CASE 
                     WHEN saf.regime='import' THEN saf.data->'$.saf.sides.import.type_value' like '%{$request->input('tin_importer_exporter')}%'
                     WHEN saf.regime='export' THEN saf.data->'$.saf.sides.export.type_value' like '%{$request->input('tin_importer_exporter')}%'
                END)");
        })->when($request->input('importer_exporter'), function ($q) use ($request) {
            $q->whereRaw("(CASE 
                     WHEN saf.regime='import' THEN saf.data->'$.saf.sides.import.name' like '%{$request->input('importer_exporter')}%'
                     WHEN saf.regime='export' THEN saf.data->'$.saf.sides.export.name' like '%{$request->input('importer_exporter')}%'
                END)");
        })->when($request->input('applicant_tin'), function ($q) use ($request) {
            $q->where('users.ssn', 'ILIKE', "%{$request->input('applicant_tin')}%");
        })->when($request->input('applicant'), function ($q) use ($request) {
            $q->where(function ($where) use ($request) {
                $where->orWhere('users.first_name', 'ILIKE', "%{$request->input('applicant')}%")->orWhere('users.last_name', 'ILIKE', "%{$request->input('applicant')}%");
            });
        })->when($request->input('agency'), function ($q) use ($request) {
            $q->where('agency.tax_id', 'ILIKE', "%{$request->input('agency')}%");
        })->when($request->input('document'), function ($q) use ($request) {
            $q->where('constructor_documents.document_code', 'ILIKE', "%{$request->input('document')}%");
        })->when($request->input('obligation_type'), function ($q) use ($request) {
            $q->where('obligation_type.code', 'ILIKE', "%{$request->input('obligation_type')}%");
        })->when($request->input('budget_line'), function ($q) use ($request) {
            $q->whereRaw("(CASE
                        WHEN saf_obligations.is_free_sum = 1 THEN saf_obligations.budget_line like '%{$request->input('budget_line')}%'
                        WHEN saf_obligations.is_free_sum = 0 THEN reference_obligation_budget_line.account_number like '%{$request->input('budget_line')}%'
                    END)");
        })->when($request->input('pay'), function ($q) use ($request) {
            if ($request->input('pay') === "1") {
                $q->whereRaw('saf_obligations.obligation = saf_obligations.payment');
            } elseif ($request->input('pay') === "0" && $request->input('pay') !== "001") {
                $q->whereRaw('saf_obligations.obligation != saf_obligations.payment');
            }
        })->when($request->input('balance_start'), function ($q) use ($request) {
            $q->whereRaw("CAST(saf_obligations.obligation - saf_obligations.payment AS DECIMAL(10,2)) >= {$request->input('balance_start')}");
        })->when($request->input('balance_end'), function ($q) use ($request) {
            $q->whereRaw("CAST(saf_obligations.obligation - saf_obligations.payment AS DECIMAL(10,2)) <= {$request->input('balance_end')}");
        })->when($request->input('obligation_pay_date_start'), function ($q) use ($request) {
            $q->where('saf_obligations.payment_date', '>=', "{$request->input('obligation_pay_date_start')} 00:00:00");
        })->when($request->input('obligation_pay_date_end'), function ($q) use ($request) {
            $q->where('saf_obligations.payment_date', '<=', "{$request->input('obligation_pay_date_end')} 23:59:59");
        })->when($request->input('obligation_created_date_start'), function ($q) use ($request) {
            $q->where('saf_obligations.created_at', '>=', "{$request->input('obligation_created_date_start')} 00:00:00");
        })->when($request->input('obligation_created_date_end'), function ($q) use ($request) {
            $q->where('saf_obligations.created_at', '<=', "{$request->input('obligation_created_date_end')} 23:59:59");
        });

        $query->where(function ($query) {
            return $query->whereNull('payment')->orWhere('payment', '=', '0');
        });

        $sum = $query->sum('saf_obligations.obligation');

        $obligationIds = $query->pluck('saf_obligations.id');

        $userBalance = Auth::user()->balance();

        $params = ['pay' => true, 'sum' => $sum, 'obligationIds' => $obligationIds, 'params' => []];

        if ($userBalance >= $sum) {
            $data['html'] = view('single-application.obligations.pay-obligation-modal-content')->with($params)->render();
        } else {
            $params['params'] = PaymentsManager::createAlifPayment();
            $params['pay'] = 'refill';
            $data['html'] = view('single-application.obligations.pay-obligation-modal-content')->with($params)->render();
        }

        return responseResult('OK', null, $data);
    }

    /**
     * Function to generate obligation pdf
     *
     * @param $lngCode
     * @param $obligationId
     * @param $pdfHashKey
     *
     * @throws Throwable
     */
    public function generateObligationPDF($lngCode, $obligationId, $pdfHashKey)
    {
        $query = SingleApplicationObligations::select([
            'saf_obligations.id as id',
            'saf_obligations.id as print',
            'saf.regime as regime',
            'saf_sub_applications.id as sub_application_id',
            'constructor_documents.id as constructor_document_id',
            'saf_obligations.created_at as obligation_created_date',
            'saf_obligations.payment_date as obligation_pay_date',
            'users.ssn as applicant_tin',
            'saf.regular_number as saf_number',
            'saf_sub_applications.document_number as sub_application_number',
            'agency_ml.name as agency',
            'constructor_documents_ml.document_name as document',
            'saf_obligations.payer_user_id', 'saf_obligations.payer_user_type', 'saf_obligations.payer_company_tax_id',
            'saf.importer_value',
            'saf.exporter_value',
            'saf.creator_company_tax_id as saf_creator_company_tax_id',
            'saf.company_tax_id as saf_company_tax_id',
            'reference_obligation_budget_line.account_number as budget_line',
            'saf_obligations.obligation_code',
            DB::raw("CONCAT('(', reference_obligation_type.code , ') ', reference_obligation_type_ml.name) AS obligation_type"),
            DB::raw("(CASE
                WHEN saf.regime='import' THEN saf.importer_value
                WHEN saf.regime='export' THEN saf.exporter_value
                ELSE saf.company_tax_id END
            ) as importer_exporter_tin"),
            DB::raw('CAST(saf_obligations.obligation AS NUMERIC(10,2)) as obligation'),
            DB::raw('CAST(saf_obligations.payment AS NUMERIC(10,2)) as payment'),
            DB::raw('CAST(saf_obligations.obligation - saf_obligations.payment AS NUMERIC(10,2)) as balance')
        ])
            ->leftJoin('saf_sub_applications', 'saf_sub_applications.id', '=', 'saf_obligations.subapplication_id')
            ->leftJoin('saf', 'saf.regular_number', '=', 'saf_obligations.saf_number')
            ->leftJoin('users', 'users.id', '=', 'saf.creator_user_id')
            ->leftJoin('agency', 'agency.tax_id', '=', 'saf_sub_applications.company_tax_id')
            ->leftJoin('agency_ml', function ($query) {
                $query->on('agency_ml.agency_id', '=', 'agency.id')->where('agency_ml.lng_id', cLng('id'));
            })
            ->leftJoin('constructor_documents', function ($query) {
                $query->on('constructor_documents.document_code', '=', 'saf_sub_applications.document_code')->where('constructor_documents.show_status', '1');
            })
            ->leftJoin('constructor_documents_ml', function ($query) {
                $query->on('constructor_documents_ml.constructor_documents_id', '=', 'constructor_documents.id')->where('constructor_documents_ml.lng_id', cLng('id'));
            })
            ->leftJoin('reference_obligation_budget_line', 'reference_obligation_budget_line.id', '=', 'saf_obligations.ref_budget_line_id')
            ->leftJoin('reference_obligation_budget_line_ml', function ($q) {
                $q->on('reference_obligation_budget_line_ml.reference_obligation_budget_line_id', '=', 'reference_obligation_budget_line.id')->where('reference_obligation_budget_line_ml.lng_id', cLng('id'));
            })
            ->leftJoin('reference_obligation_type', 'reference_obligation_type.id', '=', 'reference_obligation_budget_line.budget_line')
            ->leftJoin('reference_obligation_type_ml', function ($q) {
                $q->on('reference_obligation_type_ml.reference_obligation_type_id', '=', 'reference_obligation_type.id')->where('reference_obligation_type_ml.lng_id', cLng('id'));
            });

        $obligation = $query->where('saf_obligations.id', $obligationId)->first();

        if (!is_null($obligation)) {
            $this->pdfManager->generateObligationPdf($obligation, $pdfHashKey);
        }
    }

    /**
     * Function to generate Xls report
     *
     * @param PaymentAndObligationSearchRequest $paymentAndObligationSearchRequest
     * @return BinaryFileResponse
     */
    public function generateXls(PaymentAndObligationSearchRequest $paymentAndObligationSearchRequest)
    {
        ini_set('memory_limit', '128M');
        ini_set('max_execution_time', 300);
        $referenceSearchParams = ['obligation_type' => 'obligation_type', 'document' => '', 'agency' => '', 'pay' => 'bulian_values'];
        $paymentAndObligationXlsExport = new PaymentAndObligationXlsExport();
        $searchParams = [];
        $data = $this->dataTableSearchDataXlsReport($paymentAndObligationSearchRequest->all(), $paymentAndObligationXlsExport)->toArray();
        foreach ($paymentAndObligationSearchRequest->all() as $field => $item) {
            if (array_key_exists($field, $referenceSearchParams)) {
                if ($field == 'document') {
                    $document = ConstructorDocument::select('id', 'document_code', 'constructor_documents_ml.document_name as name')
                        ->join('constructor_documents_ml', 'constructor_documents.id', '=', 'constructor_documents_ml.constructor_documents_id')
                        ->where(['lng_id' => cLng('id'), 'constructor_documents.show_status' => '1', 'constructor_documents.document_code' => $item])
                        ->first();
                    $value = optional($document)->name;
                } elseif ($field == 'agency') {
                    $agency = Agency::select('id', 'tax_id', 'agency_ml.name as name')
                        ->join('agency_ml', 'agency.id', '=', 'agency_ml.agency_id')
                        ->where(['lng_id' => cLng('id'), 'agency.show_status' => '1', 'agency.tax_id' => $item])
                        ->first();
                    $value = optional($agency)->name;
                } else {
                    $value = optional(getReferenceRows($referenceSearchParams[$field], false, $item, ['id', 'code', 'name']))->name;
                }
                $searchParams[$field] = $value;
            } else {
                $searchParams[$field] = $item;
            }
        }
        $paymentAndObligationXlsExport->setData($data, $searchParams);

        return Excel::download($paymentAndObligationXlsExport, "payment-and-obligation-report-xls.xlsx");
    }

    /**
     * Function to get datatable search result
     *
     * @param $data
     * @param $paymentAndObligationXlsExport
     * @return Collection
     */
    private function dataTableSearchDataXlsReport($data, $paymentAndObligationXlsExport)
    {
        $getObligationSum = $getPaymentSum = $getBalanceSum = 0;
        $paymentAndObligationSearch = new PaymentAndObligationSearch();
        $query = $paymentAndObligationSearch->callToConstructorQuery($data, $paymentAndObligationXlsExport->getSelectField());
        $returnArray = $query->get();

        foreach ($returnArray as $key => &$item) {
            $item->n = $key + 1;
            $item->sub_application_number = ($item->obligation_code == SingleApplicationObligations::SAF_OBLIGATIONS_FIX_CODE_FOR_SEND_APPLICATION_OBLIGATION) ? '-' : $item->sub_application_number;
            $item->obligation_payer_person = ($item->obligation_payer_person == '()  ') ? '' : $item->obligation_payer_person;
            $item->obligation_payer = $paymentAndObligationSearch->getObligationPayer($item);
            $item->budget_line = (string)"'$item->budget_line'";

            if ($item['obligation_pay_date'])
                $item['obligation_pay_date'] = formattedDate(date(config('swis.date_time_format'), strtotime($item['obligation_pay_date'])), true);

            if ($item['obligation_created_date'])
                $item['obligation_created_date'] = formattedDate($item['obligation_created_date'], true);

            $getObligationSum += $item['obligation'];
            $getPaymentSum += $item['payment'];
            $getBalanceSum += $item['balance'];
            unset($item->payer_user_id);
            unset($item->payer_user_type);
            unset($item->payer_company_tax_id);
        }

        $returnArray[] = $paymentAndObligationSearch->returnTotalRow($getObligationSum, $getPaymentSum, $getBalanceSum, true);

        return $returnArray;
    }
}