<?php

namespace App\Http\Controllers;

use App\GUploader;
use App\Http\Requests\UserLaboratories\DocumentStoreRequest;
use App\Http\Requests\UserLaboratories\UserLaboratoriesRequest;
use App\Http\Requests\UserLaboratories\UserLaboratoriesSearchRequest;
use App\Models\ConstructorMapping\ConstructorMapping;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\Document\Document;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationDocuments\SingleApplicationDocuments;
use App\Models\SingleApplicationDocuments\SingleApplicationDocumentsManager;
use App\Models\SingleApplicationExpertise\SingleApplicationExpertise;
use App\Models\SingleApplicationExpertiseIndicators\SingleApplicationExpertiseIndicators;
use App\Models\SingleApplicationSubApplicationStates\SingleApplicationSubApplicationStates;
use App\Models\UserLaboratories\UserLaboratoriesManager;
use App\Models\UserLaboratories\UserLaboratoriesSearch;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Throwable;

/**
 * Class UserLaboratoriesController
 * @package App\Http\Controllers
 */
// NOT USED
class UserLaboratoriesController extends BaseController
{
    /**
     * @var UserLaboratoriesManager
     */
    protected $manager;

    /**
     * @var SingleApplicationDocumentsManager
     */
    protected $safDocumentsManager;

    /**
     * UserLaboratoriesController constructor.
     *
     * @param UserLaboratoriesManager $manager
     * @param SingleApplicationDocumentsManager $singleApplicationDocumentsManager
     */
    public function __construct(UserLaboratoriesManager $manager, SingleApplicationDocumentsManager $singleApplicationDocumentsManager)
    {
        // If Auth User Has Not Access To Module
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->getUserLaboratories()->count()) {
                return redirect('404');
            }

            return $next($request);
        });

        $this->manager = $manager;
        $this->safDocumentsManager = $singleApplicationDocumentsManager;
    }

    /**
     * Function to show index page
     *
     * @param $lngCode
     * @param $labId
     * @return View
     */
    public function index($lngCode, $labId)
    {
        $activeRefAgencyRow = getReferenceRows(ReferenceTable::REFERENCE_AGENCY_TABLE_NAME, false, Auth::user()->companyTaxId());
        $samplers = getReferenceRows(ReferenceTable::REFERENCE_LABORATORY_SAMPLERS, false, false, ['id', 'code', 'name'], 'get', false, 'DESC', ['agency' => optional($activeRefAgencyRow)->id]);

        return view('user-laboratory.index')->with([
            'labId' => $labId,
            'samplers' => $samplers
        ]);
    }

    /**
     * Function to get all data and showing in datatable
     *
     * @param UserLaboratoriesSearchRequest $userLaboratoriesSearchRequest
     * @param UserLaboratoriesSearch $userLaboratoriesSearch
     * @return JsonResponse
     */
    public function table(UserLaboratoriesSearchRequest $userLaboratoriesSearchRequest, UserLaboratoriesSearch $userLaboratoriesSearch)
    {
        $data = $this->dataTableAll($userLaboratoriesSearchRequest, $userLaboratoriesSearch);

        return response()->json($data);
    }

    /**
     * Function to show edit page with current expertise id
     *
     * @param $lngCode
     * @param $labId
     * @param $expertiseId
     * @return View
     */
    public function edit($lngCode, $labId, $expertiseId)
    {
        $expertiseIndicators = SingleApplicationExpertiseIndicators::where(['saf_expertise_id' => $expertiseId, 'status_send' => SingleApplicationExpertiseIndicators::STATUS_DELETED])->first();

        if (is_null($expertiseIndicators)) {
            return view('errors.404');
        }

        $safExpertise = SingleApplicationExpertise::where(['id' => $expertiseId, 'laboratory_id' => $labId])->safExpertiseOwner()->firstOrFail();

        $attachedDocumentProducts = SingleApplicationDocuments::select('saf_documents.added_from_module', 'saf_documents.document_id', 'saf_documents.id', 'saf_documents.expertise_id')
            ->join('document', 'saf_documents.document_id', '=', 'document.id')
            ->join('saf_document_products', 'saf_documents.id', '=', 'saf_document_products.saf_document_id')
            ->where(['saf_documents.saf_number' => $safExpertise->saf_number, 'added_from_module' => SingleApplicationDocuments::ADDED_FROM_MODULE_LABORATORY])
            ->where('saf_document_products.product_id', $safExpertise->saf_product_id)
            ->where('expertise_id', $expertiseId)
            ->orderBy('saf_documents.id')
            ->get();

        $adequacyRefTableName = ReferenceTable::REFERENCE_USER_LAB_ADEQUACY;
        $adequacyRefTableMlName = ReferenceTable::REFERENCE_USER_LAB_ADEQUACY . '_ml';

        $adequacyRef = DB::table($adequacyRefTableName)->join($adequacyRefTableMlName, $adequacyRefTableName . '.id', '=', $adequacyRefTableMlName . '.' . $adequacyRefTableName . '_id')->where($adequacyRefTableMlName . '.lng_id', '=', cLng('id'))->where($adequacyRefTableName . '.show_status', ReferenceTable::STATUS_ACTIVE)->get();

        $indicators = SingleApplicationExpertise::select('saf_expertise_indicators.*', 'reference_laboratory_expertise_ml.name as indicator_name', 'reference_laboratory_expertise.limit', DB::raw("CONCAT(reference_measurement_units.code, ' , ', reference_measurement_units_ml.name) AS unit_name"))
            ->where(['laboratory_id' => $labId, 'saf_expertise_id' => $expertiseId])
            ->join('saf_expertise_indicators', 'saf_expertise_indicators.saf_expertise_id', '=', 'saf_expertise.id')
            ->join('reference_laboratory_expertise', 'reference_laboratory_expertise.code', '=', 'saf_expertise_indicators.laboratory_expertise_code')
            ->where('reference_laboratory_expertise.show_status', ReferenceTable::STATUS_ACTIVE)
            ->join('reference_measurement_units', 'reference_laboratory_expertise.ind_m_unit', '=', 'reference_measurement_units.id')
            ->where('reference_measurement_units.show_status', ReferenceTable::STATUS_ACTIVE)
            ->join('reference_measurement_units_ml', function ($query) {
                $query->on('reference_measurement_units_ml.reference_measurement_units_id', '=', 'reference_measurement_units.id')->where('reference_measurement_units_ml.lng_id', cLng('id'));
            })
            ->join('reference_laboratory_expertise_ml', function ($query) {
                $query->on('reference_laboratory_expertise_ml.reference_laboratory_expertise_id', '=', 'reference_laboratory_expertise.id')->where('reference_laboratory_expertise_ml.lng_id', cLng('id'));
            })->get();

        return view('user-laboratory.edit')->with([
            'labId' => $labId,
            'adequacyRef' => $adequacyRef,
            'expertiseId' => $expertiseId,
            'safExpertise' => $safExpertise,
            'indicators' => $indicators,
            'attachedDocumentProducts' => $attachedDocumentProducts,
            'saveMode' => 'edit'
        ]);
    }

    /**
     * Function to update data
     *
     * @param $lngCode
     * @param $labId
     * @param $expertiseId
     * @param UserLaboratoriesRequest $laboratoriesRequest
     * @return JsonResponse
     */
    public function update($lngCode, $labId, $expertiseId, UserLaboratoriesRequest $laboratoriesRequest)
    {
        $expertiseIndicators = SingleApplicationExpertiseIndicators::where(['saf_expertise_id' => $expertiseId, 'status_send' => SingleApplicationExpertiseIndicators::STATUS_DELETED])->first();

        if (!is_null($expertiseIndicators)) {
            $this->manager->updateIndicators($laboratoriesRequest->validated());

            return responseResult('OK', urlWithLng('user-laboratory/' . $labId));
        }

        return responseResult('STATUS_INVALID', '', '', '');
    }

    /**
     * Function to return show all info in view regime
     *
     * @param $lngCode
     * @param $labId
     * @param $expertiseId
     * @return RedirectResponse|View
     */
    public function view($lngCode, $labId, $expertiseId)
    {
        $safExpertise = SingleApplicationExpertise::where(['id' => $expertiseId, 'laboratory_id' => $labId])->safExpertiseOwner()->firstOrFail();

        $attachedDocumentProducts = SingleApplicationDocuments::select('saf_documents.added_from_module', 'saf_documents.document_id', 'saf_documents.id', 'saf_documents.expertise_id')->join('document', 'saf_documents.document_id', '=', 'document.id')
            ->where(['saf_documents.saf_number' => $safExpertise->saf_number, 'added_from_module' => SingleApplicationDocuments::ADDED_FROM_MODULE_LABORATORY])
            ->where('expertise_id', $expertiseId)
            ->orderBy('saf_documents.id')
            ->get();

        $adequacyRefTableName = ReferenceTable::REFERENCE_USER_LAB_ADEQUACY;
        $adequacyRefTableMlName = ReferenceTable::REFERENCE_USER_LAB_ADEQUACY . '_ml';

        $adequacyRef = DB::table($adequacyRefTableName)->join($adequacyRefTableMlName, $adequacyRefTableName . '.id', '=', $adequacyRefTableMlName . '.' . $adequacyRefTableName . '_id')->where($adequacyRefTableMlName . '.lng_id', '=', cLng('id'))->get();

        $indicators = SingleApplicationExpertise::select('saf_expertise_indicators.*', 'reference_laboratory_expertise_ml.name as indicator_name', 'reference_laboratory_expertise.limit', DB::raw("CONCAT(reference_measurement_units.code, ' , ', reference_measurement_units_ml.name) AS unit_name"))
            ->where(['laboratory_id' => $labId, 'saf_expertise_id' => $expertiseId])
            ->join('saf_expertise_indicators', 'saf_expertise_indicators.saf_expertise_id', '=', 'saf_expertise.id')
            ->join('reference_laboratory_expertise', 'reference_laboratory_expertise.code', '=', 'saf_expertise_indicators.laboratory_expertise_code')
            ->where('reference_laboratory_expertise.show_status', ReferenceTable::STATUS_ACTIVE)
            ->join('reference_measurement_units', 'reference_laboratory_expertise.ind_m_unit', '=', 'reference_measurement_units.id')
            ->where('reference_measurement_units.show_status', ReferenceTable::STATUS_ACTIVE)
            ->join('reference_measurement_units_ml', function ($query) {
                $query->on('reference_measurement_units_ml.reference_measurement_units_id', '=', 'reference_measurement_units.id')->where('reference_measurement_units_ml.lng_id', cLng('id'));
            })
            ->join('reference_laboratory_expertise_ml', function ($query) {
                $query->on('reference_laboratory_expertise_ml.reference_laboratory_expertise_id', '=', 'reference_laboratory_expertise.id')->where('reference_laboratory_expertise_ml.lng_id', cLng('id'));
            })->get();

        return view('user-laboratory.edit')->with([
            'labId' => $labId,
            'adequacyRef' => $adequacyRef,
            'expertiseId' => $expertiseId,
            'safExpertise' => $safExpertise,
            'indicators' => $indicators,
            'attachedDocumentProducts' => $attachedDocumentProducts,
            'saveMode' => 'edit',
            'viewRegime' => true,
        ]);
    }

    /**
     * Function to set receive Date
     *
     * @param $lngCode
     * @param $labId
     * @param $expertiseId
     * @return RedirectResponse
     */
    public function receive($lngCode, $labId, $expertiseId)
    {
        SingleApplicationExpertise::where('id', $expertiseId)->whereNull('received_date')->safExpertiseOwner()->update(['received_date' => currentDate()]);

        return redirect()->back();
    }

    /**
     * Function to render for creating document inputs (return html)
     *
     * @param $lngCode
     * @param $labId
     * @param $expertiseId
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function renderDocumentInputs($lngCode, $labId, $expertiseId, Request $request)
    {
        $this->validate($request, [
            'num' => 'required|integer_with_max'
        ]);

        $data['html'] = view('user-laboratory.document-table-tr')->with(['renderInput' => true, 'num' => $request->get('num'), 'productNumber' => 1])->render();

        return responseResult('OK', '', $data);
    }

    /**
     * Function add document to a expertise
     *
     * @param $lngCode
     * @param $labId
     * @param $expertiseId
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function addDocument($lngCode, $labId, $expertiseId, Request $request)
    {
        $this->validate($request, [
            'documentItems' => 'array',
            'num' => 'required|integer_with_max',
        ]);

        $documentItems = $request->get('documentItems');
        $num = $request->get('num');

        $safExpertise = SingleApplicationExpertise::select('id', 'saf_product_id', 'saf_number')->where('id', $expertiseId)->firstOrFail();

        SingleApplication::where('regular_number', $safExpertise->saf_number)->firstOrFail();

        $laboratoryDocs = [];
        $newDocumentumentIds = [];
        $existDocuments = [];
        foreach ($documentItems as $documentIncrementedId => $documentId) {

            $docData = [
                'document_id' => $documentId,
                'expertise_id' => $expertiseId,
                'added_from_module' => SingleApplicationDocuments::ADDED_FROM_MODULE_LABORATORY,
            ];

            $documentExist = SingleApplicationDocuments::where($docData)->first();

            $docData['products'] = $safExpertise->saf_product_id;

            if (is_null($documentExist)) {
                $newDocumentumentIds[] = $documentId;
                $laboratoryDocs[] = $docData;
            } else {
                $existDocuments[] = $documentId;
                $existDocumentsIncrementIds[] = $documentIncrementedId;
            }
        }

        // Store Document Products
        $this->safDocumentsManager->storeDocumentProducts($laboratoryDocs, $safExpertise->saf_number);

        $documents = Document::whereIn('id', $newDocumentumentIds)->get();

        if (count($existDocuments)) {
            $data['error'] = [
                'docIds' => $existDocuments,
                'message' => implode(',', $existDocumentsIncrementIds) . ' ' . trans('swis.documents.already_exist.info')
            ];
        }

        $data['newDocumentCount'] = count($newDocumentumentIds);
        $data['html'] = view('user-laboratory.document-table-tr')->with([
            'attachedDocumentProducts' => $documents,
            'expertiseId' => $expertiseId,
            'addFromDocList' => true,
            'num' => $num
        ])->render();

        return responseResult('OK', '', $data);
    }

    /**
     * Function to store document
     *
     * @param $lngCode
     * @param $labId
     * @param $expertiseId
     * @param DocumentStoreRequest $documentStoreRequest
     * @return JsonResponse
     */
    public function storeDocument($lngCode, $labId, $expertiseId, DocumentStoreRequest $documentStoreRequest)
    {
        $dataValidated = $documentStoreRequest->validated();

        if (!empty($dataValidated['document_file'])) {
            $gUploader = new GUploader('document_report');
            $gUploader->storePerm($dataValidated['document_file']);
        }

        $safExpertise = SingleApplicationExpertise::select('id', 'saf_product_id', 'saf_number')->where('id', $expertiseId)->firstOrFail();

        $dataValidated['expertise_id'] = $expertiseId;
        $dataValidated['document_products'] = $safExpertise->saf_product_id;
        $dataValidated['added_from_module'] = SingleApplicationDocuments::ADDED_FROM_MODULE_LABORATORY;

        // Store Document
        $newDocument = $this->safDocumentsManager->storeDocumentsFromLaboratory($dataValidated, $safExpertise->saf_number);

        $data['docId'] = $newDocument->id;
        $data['documentForm'] = trans('swis.document.document_form.' . $newDocument->document_form . '.title');
        $data['documentFile'] = $newDocument->filePath();
        $data['documentFileName'] = $newDocument->fileBaseName();

        return responseResult('OK', '', $data);
    }

    /**
     * Function to delete document
     *
     * @param $lngCode
     * @param $labId
     * @param $expertiseId
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteDocument($lngCode, $labId, $expertiseId, Request $request)
    {
        $this->validate($request, [
            'doc_id' => 'required|integer_with_max'
        ]);

        $safExpertise = SingleApplicationExpertise::where('id', $expertiseId)->firstOrFail();
        $singleAppDocument = SingleApplicationDocuments::where(['company_tax_id' => Auth::user()->companyTaxId(), 'document_id' => $request->input('doc_id'), 'saf_number' => $safExpertise->saf_number, 'added_from_module' => SingleApplicationDocuments::ADDED_FROM_MODULE_LABORATORY]);

        if (!Auth::user()->isLocalAdmin()) {
            $singleAppDocument->where('user_id', Auth::id());
        }

        $singleAppDocument->delete();

        return responseResult('OK');
    }

    /**
     * Function to update expertise status send back to sub application
     *
     * @param $lngCode
     * @param $labId
     * @param $expertiseId
     * @return JsonResponse
     */
    public function expertiseStatusUpdate($lngCode, $labId, $expertiseId)
    {
        SingleApplicationExpertiseIndicators::where(['saf_expertise_id' => $expertiseId])->update(['status_send' => SingleApplicationExpertiseIndicators::STATUS_ACTIVE]);
        SingleApplicationExpertise::where('id', $expertiseId)->update(['return_date' => Carbon::now()]);

        $application_id = SingleApplicationExpertise::where('id', $expertiseId)->pluck('sub_application_id')->first();

        $safAllProductsLaboratoriesExpertise = SingleApplicationExpertise::select('saf_expertise.id', 'saf_number', 'saf_expertise_indicators.status_send')->where(['sub_application_id' => $application_id])
            ->join('saf_expertise_indicators', 'saf_expertise.id', '=', 'saf_expertise_indicators.saf_expertise_id')
            ->where('saf_expertise_indicators.status_send', SingleApplicationExpertise::STATUS_DELETED)->first();

        if (is_null($safAllProductsLaboratoriesExpertise)) {
            $safCurrentState = SingleApplicationSubApplicationStates::where(['saf_subapplication_id' => $application_id, 'is_current' => '1'])->first();

            if (!is_null($safCurrentState) && $safCurrentState->state->state_type == ConstructorStates::LABORATORY_STATE_TYPE) {
                $endState = ConstructorMapping::where('end_state', $safCurrentState->state->id)->active()->pluck('start_state')->first();
                if (!is_null($endState)) {
                    SingleApplicationSubApplicationStates::where(['saf_subapplication_id' => $application_id])->update(['is_current' => '0']);

                    $subAppData = [
                        'user_id' => Auth::id(),
                        'saf_number' => $safCurrentState->saf_number,
                        'saf_id' => $safCurrentState->saf_id,
                        'saf_subapplication_id' => $safCurrentState->saf_subapplication_id,
                        'state_id' => $endState,
                        'is_current' => '1'
                    ];

                    SingleApplicationSubApplicationStates::create($subAppData);
                }
            }
        }

        return responseResult('OK', back()->getTargetUrl());
    }
}
