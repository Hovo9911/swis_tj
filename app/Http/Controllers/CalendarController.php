<?php

namespace App\Http\Controllers;

use App\Http\Requests\Calendar\CalendarRequest;
use App\Models\Calendar\Calendar;
use App\Models\Calendar\CalendarManager;
use Illuminate\Http\JsonResponse;
use Illuminate\View\View;

/**
 * Class CalendarController
 * @package App\Http\Controllers
 */
class CalendarController extends BaseController
{
    /**
     * @var CalendarManager
     */
    protected $manager;

    /**
     * CalendarController constructor.
     *
     * @param CalendarManager $manager
     */
    public function __construct(CalendarManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Function to show create form
     *
     * @return View
     */
    public function edit()
    {
        return view('calendar.edit')->with([
            'currentYearNonWorkingDays' => Calendar::getNonWorkingDays(now()->year),
            'nextYearNonWorkingDays' => Calendar::getNonWorkingDays((int)(now()->year) + 1)
        ]);
    }

    /**
     * Function to update data
     *
     * @param CalendarRequest $request
     * @param $lngCode
     * @return JsonResponse
     */
    public function store(CalendarRequest $request, $lngCode)
    {
        $this->manager->store($request->validated());

        return responseResult('OK');
    }
}