<?php

namespace App\Http\Controllers\DocumentsCloud;

use App\Exceptions\TaxServiceTjCompanyInactiveException;
use App\Exceptions\TaxServiceTjInnNotFoundException;
use App\Exceptions\TaxServiceTjInvalidResponseException;
use App\Http\Controllers\BaseController;
use App\Http\Requests\Folder\DocumentStoreRequest;
use App\Http\Requests\Folder\FolderRequest;
use App\Http\Requests\Folder\FolderSearchRequest;
use App\Models\Document\Document;
use App\Models\Document\DocumentManager;
use App\Models\Document\DocumentSearch;
use App\Models\Folder\Folder;
use App\Models\Folder\FolderManager;
use App\Models\Folder\FolderSearch;
use App\Models\FolderDocuments\FolderDocuments;
use App\Models\FolderNotes\FolderNotes;
use App\Models\ReferenceTable\ReferenceTable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Throwable;

/**
 * Class FolderController
 * @package App\Http\Controllers\DocumentsCloud
 */
class FolderController extends BaseController
{
    /**
     * @var FolderManager
     */
    protected $manager;

    /**
     * @var DocumentManager
     */
    protected $documentManager;

    /**
     * FolderController constructor.
     *
     * @param FolderManager $manager
     * @param DocumentManager $documentManager
     */
    public function __construct(FolderManager $manager, DocumentManager $documentManager)
    {
        $this->manager = $manager;
        $this->documentManager = $documentManager;
    }

    /**
     * Function to show index page
     *
     * @return View
     */
    public function index()
    {
        return view('folder.index');
    }

    /**
     * Function to get all data and showing in dataTable
     *
     * @param FolderSearchRequest $folderSearchRequest
     * @param FolderSearch $folderSearch
     * @return JsonResponse
     */
    public function table(FolderSearchRequest $folderSearchRequest, FolderSearch $folderSearch)
    {
        $data = $this->dataTableAll($folderSearchRequest, $folderSearch);

        return response()->json($data);
    }

    /**
     * Function to show create page
     *
     * @return View
     */
    public function create()
    {
        return view('folder.edit')->with([
            'saveMode' => 'add'
        ]);
    }

    /**
     * Function to store data
     *
     * @param FolderRequest $folderRequest
     * @return JsonResponse
     *
     * @throws TaxServiceTjCompanyInactiveException
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function store(FolderRequest $folderRequest)
    {
        $folder = $this->manager->store($folderRequest->validated());

        return responseResult('OK', null, ['id' => $folder->id]);
    }

    /**
     * Function to show edit page by current id
     *
     * @param $lngCode
     * @param $id
     * @param $viewType
     * @return View
     */
    public function edit($lngCode, $id, $viewType)
    {
        $folder = Folder::with(['documents'])->where('id', $id);

        $accessKey = $_GET['accesskey'] ?? false;

        if ($viewType != Folder::VIEW_MODE_VIEW || ($viewType == Folder::VIEW_MODE_VIEW && !$accessKey)) {
            $folder->folderOwnerOrCreator();
        }

        if ($accessKey) {
            $folder->where('folder_access_password', $accessKey);
        }

        $folder = $folder->firstOrFail();

        foreach ($folder->documents as $key => $document) {
            if (!($document->isDocumentOwnerOrCreator()) and $document->document_status == Document::DOCUMENT_STATUS_SUSPENDED) {
                $folder->documents->forget($key);
            }
        }

        return view('folder.edit')->with([
            'folder' => $folder,
            'saveMode' => $viewType,
            'notes' => FolderNotes::getNotes($folder->id)
        ]);
    }

    /**
     * Function to update data
     *
     * @param FolderRequest $request
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     *
     * @throws TaxServiceTjCompanyInactiveException
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function update(FolderRequest $request, $lngCode, $id)
    {
        $this->manager->update($request->validated(), $id);

        return responseResult('OK');
    }

    /**
     * Function to delete by id(array)
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function delete(Request $request)
    {
        $this->manager->delete($request->input('deleteItems'));

        return responseResult('OK');
    }

    /**
     * Function to search documents in folder
     *
     * @param Request $request
     * @param $lngCode
     * @param null $folderId
     * @return JsonResponse
     */
    public function documentFolderSearch(Request $request, $lngCode, $folderId = null)
    {
        $documentSearch = new DocumentSearch($folderId);

        $dataRequest = $request->all();
        $searchFilters = $dataRequest['f'] ?? "";

        $docFolderAccessPass = '';
        if (isset($searchFilters['document_access_password'])) {
            $docFolderAccessPass = $searchFilters['document_access_password'];
        }

        $data = [];
        $folderExist = false;
        if ($docFolderAccessPass) {

            $folder = Folder::where(['folder_access_password' => $docFolderAccessPass])->first();

            if (!is_null($folder)) {
                $folderExist = true;

                $documentIds = FolderDocuments::where('folder_id', $folder->id)->pluck('document_id')->all();

                $request->request->add(["f" => ['docIds' => $documentIds]]);

                $data = $this->dataTableAll($request, $documentSearch);
            }
        }

        if (!$folderExist) {
            $data = $this->dataTableAll($request, $documentSearch);
        }

        $start = $request->get('start');
        if (isset($data['data']) && count($data['data']) > 0) {
            foreach ($data['data'] as $incrementedId => &$item) {
                $item->incrementedId = ++$start;
            }
        }

        return response()->json($data);
    }

    /**
     * Function to add document in folder
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function addDocument(Request $request)
    {
        $documentItems = $request->get('documentItems');
        $num = $request->get('num');
        $folderId = $request->get('folder_id');

        $validateData = [
            'documentItems' => 'array',
            'documentItems.*' => 'integer_with_max|exists:document,id',
            'num' => 'required|integer_with_max',
        ];

        if ($folderId) {
            $validateData['folder_id'] = 'required|integer_with_max|exists:folder,id';
        }

        $this->validate($request, $validateData);

        $newDocumentIds = $existDocuments = [];

        foreach ($documentItems as $documentIncrementedId => $documentId) {
            $documentExist = FolderDocuments::where('folder_id', $folderId)->where('document_id', $documentId)->first();
            if (is_null($documentExist)) {
                $newDocumentIds[] = $documentId;
            } else {
                $existDocuments[] = $documentId;
                $existDocumentsIncrementIds[] = $documentIncrementedId;
            }
        }

        if ($folderId) {
            $this->manager->updateFolderDocuments($folderId, $newDocumentIds);
        }

        if (count($existDocuments)) {
            $data['error'] = [
                'docIds' => $existDocuments,
                'message' => implode(',', $existDocumentsIncrementIds) . ' ' . trans('swis.documents.already_exist.info')
            ];
        }

        //
        $documents = Document::whereIn('id', $newDocumentIds)->get();

        $data['newDocumentCount'] = count($newDocumentIds);
        $data['html'] = view('folder.document-table-tr')->with(['documents' => $documents, 'inactiveDocuments' => $documents, 'num' => $num])->render();

        return responseResult('OK', '', $data);
    }

    /**
     * Function to render document input for creating document from folder
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function renderDocumentInputs(Request $request)
    {
        $this->validate($request, [
            'num' => 'required|integer_with_max'
        ]);

        $data['html'] = view('folder.document-table-tr')->with(['renderInput' => true, 'num' => $request->get('num')])->render();

        return responseResult('OK', '', $data);
    }

    /**
     * Function to store Document from folder
     *
     * @param DocumentStoreRequest $request
     * @param $lngCode
     * @param $folderId
     * @return JsonResponse
     *
     * @throws TaxServiceTjCompanyInactiveException
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function storeDocument(DocumentStoreRequest $request, $lngCode, $folderId)
    {
        $dataValidated = $request->validated();

        $referenceClassificator = getReferenceRows(ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS, false, $dataValidated['document_type']);

        $dataValidated['document_status'] = Document::DOCUMENT_STATUS_REGISTERED;
        $dataValidated['approve_type'] = Document::APPROVE_OTHER;
        $dataValidated['created_from_module'] = Document::DOCUMENT_CREATE_MODULE_FOLDER;
        $dataValidated['document_type_id'] = optional($referenceClassificator)->id ?? 0;
        $dataValidated['document_name'] = optional($referenceClassificator)->name ?? '';

        //
        $newDocument = $this->documentManager->store($dataValidated);

        //
        $this->manager->updateFolderDocuments($folderId, [$newDocument->id]);

        $data['docId'] = $newDocument->id;
        $data['documentForm'] = trans('swis.document.document_form.' . $newDocument->document_form . '.title');
        $data['accessPassword'] = $newDocument->document_access_password;
        $data['docStatus'] = Document::DOCUMENT_STATUS_REGISTERED;

        $data['documentFile'] = $newDocument->filePath();
        $data['documentFileName'] = $newDocument->fileBaseName();

        return responseResult('OK', '', $data);
    }

    /**
     * Function to Delete document from SAF
     *
     * @param Request $request
     * @param $lngCode
     * @param $folderId
     * @return JsonResponse
     */
    public function deleteDocument(Request $request, $lngCode, $folderId)
    {
        $this->validate($request, [
            'documentIds' => 'required|array',
            'documentIds.*' => 'integer_with_max|exists:document,id'
        ]);

        $this->manager->deleteFolderDocuments($request->input('documentIds'), $folderId);

        return responseResult('OK');
    }
}
