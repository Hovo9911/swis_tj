<?php

namespace App\Http\Controllers\DocumentsCloud;

use App\Exceptions\TaxServiceTjCompanyInactiveException;
use App\Exceptions\TaxServiceTjInnNotFoundException;
use App\Exceptions\TaxServiceTjInvalidResponseException;
use App\Http\Controllers\BaseController;
use App\Http\Requests\Document\DocumentRequest;
use App\Http\Requests\Document\DocumentSearchRequest;
use App\Models\BaseModel;
use App\Models\Document\Document;
use App\Models\Document\DocumentManager;
use App\Models\Document\DocumentSearch;
use App\Models\DocumentsNotes\DocumentsNotes;
use App\Models\Folder\Folder;
use App\Models\FolderDocuments\FolderDocuments;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\ReferenceTableForm\ReferenceTableFormManager;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Class DocumentController
 * @package App\Http\Controllers\DocumentsCloud
 */
class DocumentController extends BaseController
{
    /**
     * @var DocumentManager
     */
    protected $manager;

    /**
     * @var ReferenceTableFormManager
     */
    protected $referenceTableFormManager;

    /**
     * DocumentController constructor.
     *
     * @param DocumentManager $manager
     * @param ReferenceTableFormManager $referenceTableFormManager
     */
    public function __construct(DocumentManager $manager, ReferenceTableFormManager $referenceTableFormManager)
    {
        $this->manager = $manager;
        $this->referenceTableFormManager = $referenceTableFormManager;
    }

    /**
     * Function to show index page
     *
     * @return View
     */
    public function index()
    {
        return view('document.index')->with([
            'referenceDocumentTypes' => getReferenceRows(ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS),
            'documentStatuses' => Document::DOCUMENT_STATUSES
        ]);
    }

    /**
     * Function to get all data and showing in datatable
     *
     * @param DocumentSearchRequest $documentSearchRequest
     * @param DocumentSearch $documentSearch
     * @return JsonResponse
     */
    public function table(DocumentSearchRequest $documentSearchRequest, DocumentSearch $documentSearch)
    {
        $data = $this->dataTableAll($documentSearchRequest, $documentSearch);

        return response()->json($data);
    }

    /**
     * Function to show create page
     *
     * @return View
     */
    public function create()
    {
        $folders = Folder::folderOwnerOrCreator()->groupBy('folder.id')->orderbyDesc()->active()->get();

        return view('document.edit')->with([
            'saveMode' => 'add',
            'isAgencyApprove' => false,
            'isDocumentOwner' => false,
            'folders' => $folders,
        ]);
    }

    /**
     * Function to store data in db
     *
     * @param DocumentRequest $request
     * @return JsonResponse
     *
     * @throws TaxServiceTjCompanyInactiveException
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function store(DocumentRequest $request)
    {
        $data = $request->validated();
        $data['created_from_module'] = Document::DOCUMENT_CREATE_MODULE_DOCUMENT;

        $document = $this->manager->store($data);

        return responseResult('OK', null, ['id' => $document->id]);
    }

    /**
     * Function to show edit page by current id
     *
     * @param $lngCode
     * @param $id
     * @param $viewType
     * @return RedirectResponse|View
     */
    public function edit($lngCode, $id, $viewType)
    {
        $document = Document::where('id', $id);

        $accessKey = $_GET['accesskey'] ?? false;

        if ($viewType != Document::VIEW_MODE_VIEW || ($viewType == Document::VIEW_MODE_VIEW && !$accessKey)) {
            $document->where(function ($q) {
                $q->where(function ($where) {
                    $where->agencyNeedToApproveDocument();
                })->orWhere(function ($where) {
                    $where->documentOwnerOrCreator();
                });
            });
        }

        if ($accessKey) {
            $document->where('document_access_password', $accessKey);
        }

        $document = $document->active()->firstOrFail();

        if ($document->show_status == BaseModel::STATUS_INACTIVE) {
            $document = Document::where('uuid', $document->uuid)->orderByDesc()->firstOrFail();

            $showModeType = Document::VIEW_MODE_VIEW;
            if ($document->document_form == Document::DOCUMENT_FORM_PAPER) {
                $showModeType = Document::VIEW_MODE_EDIT;
            }

            return redirect(urlWithLng("/document/{$document->id}/$showModeType"));
        }

        if ($document->document_form == Document::DOCUMENT_FORM_ELECTRONIC && $viewType == Document::VIEW_MODE_EDIT) {
            return redirect(urlWithLng("/document/{$document->id}/view"));
        }

        // User folders
        $folders = Folder::folderOwnerOrCreator()->active()->get();
        $documentFolders = FolderDocuments::where('document_id', $id)->pluck('folder_id')->all();

        // Electronic doc state name
        $electronicDocumentStateName = $document->getElectronicDocumentStateName();

        // reference document types
        $refDocumentTypeAgencies = $this->referenceTableFormManager->getDocumentTypeReference($document->document_type_id);

        return view('document.edit')->with([
            'document' => $document,
            'electronicDocumentStateName' => $electronicDocumentStateName,
            'refDocumentTypeAgencies' => $refDocumentTypeAgencies,
            'documentFolders' => $documentFolders,
            'folders' => $folders,
            'attachedDocHistory' => $this->manager->getAttachedDocHistory($document->uuid),

            // if approve type agency need to show (confirm,reject) buttons
            'isAgencyApprove' => Document::isAgencyApprove($id),

            'isDocumentOwner' => $document->isDocumentOwnerOrCreator(),
            'notes' => DocumentsNotes::getNotes($document->uuid),

            'saveMode' => $viewType,
        ]);
    }

    /**
     * Function to update data
     *
     * @param DocumentRequest $request
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     *
     * @throws TaxServiceTjCompanyInactiveException
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function update(DocumentRequest $request, $lngCode, $id)
    {
        $document = Document::where(function ($q) {
            $q->documentOwnerOrCreator()->orWhere(function ($q) {
                $q->agencyNeedToApproveDocument();
            });
        })->where('id', $id)->firstOrFail();

        //
        if (($request->get('document_status') == Document::DOCUMENT_STATUS_REJECTED || $request->get('document_status') == Document::DOCUMENT_STATUS_CONFIRMED) && $document->show_status == Document::STATUS_INACTIVE) {
            $error['permission_denied'] = true;

            return responseResult('INVALID_DATA', '', '', $error);
        }

        //
        $this->manager->update($request->validated(), $id);

        return responseResult('OK');
    }

    /**
     * Function to delete by id(array)
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function delete(Request $request)
    {
        $this->manager->delete($request->input('deleteItems'));

        return responseResult('OK');
    }

    /**
     * Function to get Document type and return agency
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getTypeOfDocument(Request $request)
    {
        $this->validate($request, [
            'query' => 'required|string_with_max',
        ]);

        $data['items'] = $this->referenceTableFormManager->getDocumentClassificatorsWithAgencies($request->get('query'));

        return response()->json($data);
    }

    /**
     * Function to download file from api
     *
     * @param $lngCode
     * @param $accessToken
     * @return BinaryFileResponse|void
     */
    public function uploadByAccessKey($lngCode, $accessToken)
    {
        $document = Document::select('id', 'document_file')->where('document_access_password', $accessToken)->first();
        if (!$document) {
            abort(404);
        }

        try {
            $file = Storage::disk('uploads')->path($document->document_file);
            $fileShortNamePath = explode('/', $document->document_file);
            $fileName = end($fileShortNamePath);

            $headers = ['Content-Type: application/pdf'];

            return response()->download($file, $fileName, $headers);
        } catch (Exception $e) {
            abort(500);
        }
    }
}
