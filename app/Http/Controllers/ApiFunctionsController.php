<?php

namespace App\Http\Controllers;

use App\Models\Agency\Agency;
use App\Models\DataModel\DataModel;
use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm;
use App\Models\Instructions\Instructions;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure;
use App\Models\SingleApplicationObligations\SingleApplicationObligations;
use App\Models\SingleApplicationProducts\SingleApplicationProducts;
use App\Models\SingleApplicationSubApplicationInstructions\SingleApplicationSubApplicationInstructions;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class ApiFunctionsController
 * @package App\Http\Controllers
 */
class ApiFunctionsController extends BaseController
{
    /**
     * Function to check in ref
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function inRef(Request $request)
    {
        $tableName = is_string($request->input('table-name')) ? $request->input('table-name') : '';
        $tableName = "reference_" . $tableName;
        $conditions = $request->input('conditions');
        $select = (is_null($request->input('return-field'))) ? "*" : $request->input('return-field');

        $result = DB::table($tableName)->select($select)->join("{$tableName}_ml", "{$tableName}_ml.{$tableName}_id", "=", "{$tableName}.id");

        foreach (json_decode($conditions) as $condition) {
            list ($field, $operator, $value) = $condition;

            if (is_array($value) && count($value) > 0) {
                $result->where(function ($q) use ($field, $operator, $value) {
                    foreach ($value as $item) {
                        $values = SingleApplicationSubApplications::getOperatorAndValue($operator, $item);
                        $q->orWhere(DB::raw("{$field}::varchar"), $values['operator'], $values['value']);
                    }
                });
            } else {
                $values = SingleApplicationSubApplications::getOperatorAndValue($operator, $value);
                $result->where($field, $values['operator'], $values['value']);
            }
        }
        $result = $result->where("{$tableName}.show_status", '1')->first();

        if (is_null($result)) {
            $result = false;
        } else {
            $result = (isset($result->{$select})) ? $result->{$select} : true;
        }

        try {
            return response()->json($this->getResponse("OK", $result));
        } catch (Exception $exception) {
            return response()->json($this->getResponse($exception->getCode(), '', $exception->getMessage()));
        }
    }

    /**
     * Function to check exchange rate
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function exchangeRate(Request $request)
    {
        $fieldValue = floatval(str_replace(',', '.', str_replace('.', '', $request->input('field'))));
        $fromCurrency = $request->input('from-currency');
        $toCurrency = $request->input('to-currency');

        try {
            return response()->json($this->getResponse("OK", $this->calculateCurrency($fieldValue, $fromCurrency, $toCurrency)));
        } catch (Exception $exception) {
            return response()->json($this->getResponse($exception->getCode(), '', $exception->getMessage()));
        }
    }

    /**
     * Function to check calculate currency
     *
     * @param $fieldValue
     * @param $fromCurrency
     * @param $toCurrency
     * @return float|int
     */
    private function calculateCurrency($fieldValue, $fromCurrency, $toCurrency)
    {
        $fromValueNational = $this->getRate($fieldValue, $fromCurrency);
        $toValueNational = $this->getRate($fieldValue, $toCurrency);

        return ($fromValueNational / $toValueNational) * $toValueNational;
    }

    /**
     * Function to check get rate
     *
     * @param $totalValue
     * @param $currency
     * @return float|int|null
     */
    private function getRate($totalValue, $currency)
    {
        $currenciesTable = ReferenceTable::REFERENCE_CURRENCIES;
        $currenciesTableMl = ReferenceTable::REFERENCE_CURRENCIES . "_ml";

        $currentCurrency = DB::table($currenciesTable)->select('id', 'code', 'name')
            ->join($currenciesTableMl, "{$currenciesTableMl}.{$currenciesTable}_id", "=", "{$currenciesTable}.id")
            ->where("{$currenciesTableMl}.name", $currency)
            ->where("{$currenciesTable}.show_status", "1")
            ->first();

        $currencyRef = ReferenceTable::REFERENCE_EXCHANGE_RATES;
        $totalValueNational = null;

        if (!is_null($currentCurrency)) {
            $currentExchange = DB::table($currencyRef)->where(['code' => $currentCurrency->code, "{$currencyRef}.show_status" => '1'])->first();

            if (!is_null($currentExchange->number_of_units) && !is_null($currentExchange->rate)) {
                $totalValueNational = ($currentExchange->rate / $currentExchange->number_of_units) * $totalValue;
            }
        }

        return $totalValueNational;
    }

    /**
     * Function to get application hour left
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function hourLeft(Request $request)
    {
        $subApplicationId = $request->input('sub-application-id');

        try {
            $documentTypeReference = ReferenceTable::REFERENCE_DOCUMENT_TYPE_REFERENCE;
            $subApplication = SingleApplicationSubApplications::select('saf_sub_applications.id', "{$documentTypeReference}.control_period_hours", 'saf_sub_applications.hour_spend', 'saf_sub_applications.agency_id')
                ->join('constructor_documents', "constructor_documents.document_code", "=", "saf_sub_applications.document_code")
                ->join($documentTypeReference, "{$documentTypeReference}.id", "=", "constructor_documents.document_type_id")
                ->where('saf_sub_applications.id', $subApplicationId)
                ->first();

            if (!is_null($subApplication)) {

                if (!is_null($subApplication->control_period_hours)) {
                    $hourSpend = (int)$subApplication->hour_spend;

                    if (isset($subApplication->last_action_date)) {
//                        $diff = Carbon::now()->diffInHours(Carbon::parse($subApplication->last_action_date));
                        $diff = Agency::getWorkingHoursBetweenDates($subApplication->last_action_date, now(), $subApplication->agency_id);
                        $hourSpend = $hourSpend + $diff;
                    }
                    $hourLeft = $subApplication->control_period_hours - $hourSpend;
                }
                $result = isset($hourLeft) ? $hourLeft : false;

                return response()->json($this->getResponse("OK", $result));
            } else {
                return response()->json($this->getResponse("OK", false, 'application not found'));
            }


        } catch (Exception $exception) {
            return response()->json($this->getResponse($exception->getCode(), '', $exception->getMessage()));
        }
    }

    /**
     * Function to get application hour spend
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function hourSpend(Request $request)
    {
        $subApplicationId = $request->input('sub-application-id');

        try {
            $subApplication = SingleApplicationSubApplications::select('id', 'hour_spend', 'last_action_date', 'agency_id')->where('id', $subApplicationId)->first();
            $hourSpend = (int)$subApplication->hour_spend;

            if (isset($subApplication->last_action_date)) {
                $now = Carbon::now();
                $lastActionDate = Carbon::parse($subApplication->last_action_date);
//                $diff = $now->diffInHours($lastActionDate);
                $diff = Agency::getWorkingHoursBetweenDates($lastActionDate, $now, $subApplication->agency_id);
                $hourSpend = $hourSpend + $diff;
            }

            return response()->json($this->getResponse("OK", $hourSpend));

        } catch (Exception $exception) {
            return response()->json($this->getResponse($exception->getCode(), '', $exception->getMessage()));
        }
    }

    /**
     * Function to get products by condition
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getProducts(Request $request)
    {
        $subApplicationId = $request->input('sub-application-id');
        $conditions = $request->input('conditions');
        $functions = $request->input('function');

        try {
            $subApplication = SingleApplicationSubApplications::select('id', 'hour_spend', 'last_action_date', 'custom_data')->where('id', $subApplicationId)->first();
            $conditions = $this->changeConditionFields(json_decode($conditions));
            $result = $this->getResult($functions, $subApplication, $conditions, "productListApi");

            return response()->json($this->getResponse("OK", $result));

        } catch (Exception $exception) {
            return response()->json($this->getResponse($exception->getCode(), '', $exception->getMessage()));
        }
    }

    /**
     * Function to set product values by condition
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function setProductValues(Request $request)
    {
        $subApplicationId = $request->input('sub-application-id');
        $conditions = $request->input('conditions');
        $values = json_decode($request->input('values'));

        try {
            $lastXML = SingleApplicationDataStructure::lastXml();
            $subApplication = SingleApplicationSubApplications::select('id', 'hour_spend', 'last_action_date', 'custom_data')->find($subApplicationId);
            $conditions = $this->changeConditionFields(json_decode($conditions));
            $products = $this->getProductsByCondition($subApplication, $conditions);

            foreach ($values as $field => $value) {
                if (strpos($value, "action(") !== false) {
                    $value = str_replace(')', "", str_replace('action(', "", $value));
                    if (strpos($field, "SAF_ID_") === false) {
                        $field = str_replace("$", "", $field);
                        $fieldName = str_replace("FIELD_ID_", "", $field);
                        foreach ($products as $product) {
                            $getSavedValues = $this->getSavedValue($product);

                            // MDM FIELDS
                            $variableValues = [];
                            preg_match_all('/FIELD_ID_[\w]*/ius', $value, $variables); // match all variables inside the condition
                            foreach ($variables[0] ?? [] as $variable) {
                                $fieldId = str_replace('FIELD_ID_', "", $variable);
                                $getSavedValue = isset($getSavedValues->{$fieldId . "_pr_" . $product->id}) ? $getSavedValues->{$fieldId . "_pr_" . $product->id} : null;
                                $variableValues[] = $getSavedValue;
                            }
                            $tmpValue = str_replace($variables[0], $variableValues, $value);

                            // SAF FIELDS
                            preg_match_all('/SAF_ID_[\w]*/ius', $value, $variables); // match all variables inside the condition
                            $variableValues = [];
                            foreach ($variables[0] as $variable) {
                                $xmlField = getSafFieldById($lastXML, $variable)[0];
                                $safFieldName = str_replace($variable, (string)$xmlField->attributes()->name, $variable);

                                $variableValues[] = (int)$product->{$safFieldName};
                            }
                            $tmpValue = str_replace($variables[0], $variableValues, $tmpValue);
                            $tmpValue = str_replace("$", "", $tmpValue);

                            $result= eval('return '.$tmpValue.';');
                            $tmpValue = $value;
                            $customData = $customData ?? collect($getSavedValues)->toArray();
                            $customData["{$fieldName}_pr_{$product->id}"] = $result;

                            $subApplication->custom_data = $customData;
                            $subApplication->save();
                        }
                    }

                } else {
                    $field = str_replace("$", "", $field);

                    if (strpos($field, "SAF_ID_") !== false) {

                        $xmlField = getSafFieldById($lastXML, $field)[0];
                        $fieldName = str_replace($field, (string)$xmlField->attributes()->name, $field);

                        foreach ($products as $product) {
                            SingleApplicationProducts::select('id', "{$fieldName}")->where('id', $product->id)->update(["{$fieldName}" => $value]);
                        }

                    } else {

                        $fieldName = str_replace("FIELD_ID_", "", $field);
                        $customData = $subApplication->custom_data ?? [];

                        foreach ($products as $product) {
                            $customData["{$fieldName}_pr_{$product->id}"] = $value;
                        }

                        $subApplication->custom_data = $customData;
                        $subApplication->save();

                    }
                }
            }

            return response()->json($this->getResponse("OK", $customData ?? []));

        } catch (Exception $exception) {
            return response()->json($this->getResponse($exception->getCode(), false, $exception->getMessage()));
        }
    }

    /**
     * Function to run condition for custom fields in product
     *
     * @param $conditions
     * @param $data
     * @return mixed
     */
    private function filterMdmFieldCondition($conditions, $data)
    {
        // Works only for mdm fields witch have a reference in goods
        $mdmRefs = $conditions['mdmRefs'];
        $conditions = array_filter($conditions['conditions'], function ($value) use ($conditions) {
            return (isset($conditions['mdmRefs'][$value[0]]) || strpos($value[0], 'FIELD_ID_') !== false);
        });

        return $data->filter(function ($value, $key) use ($conditions, $mdmRefs) {
            $result = true;
            $productId = $value->id;
            foreach ($conditions as $condition) {
                $fieldId = $condition[3];
                $getSavedValue = $this->getSavedValue($value);
                $getSavedValue = isset($getSavedValue->{$fieldId . "_pr_" . $productId}) ? $getSavedValue->{$fieldId . "_pr_" . $productId} : null;
                if (isset($mdmRefs[$fieldId])) {
                    $getValue = getReferenceRows($mdmRefs[$fieldId], $getSavedValue);
                    if (is_array($condition[2])) {
                        if (!in_array($getValue->code ?? '', $condition[2])) {
                            $result = false;
                            break;
                        }
                    } else {
                        if ($condition[2] != $getValue->code ?? '') {
                            $result = false;
                            break;
                        }
                    }
                } else {
                    if (is_array($condition[2])) {
                        foreach ($condition[2] as $item) {
                            $result = $this->compare($getSavedValue, $condition[1], $item);
                            if ($result) {
                                $result = true;
                                break;
                            }
                        }
                    } else {
                        $result = $this->compare($getSavedValue, $condition[1], $condition[2]);
                        if (!$result) {
                            $result = false;
                            break;
                        }
                    }

                }
            }
            return $result;
        });
    }

    /**
     * Function to get batches by condition
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getBatches(Request $request)
    {
        $subApplicationId = $request->input('sub-application-id');
        $conditions = $request->input('conditions');
        $functions = $request->input('function');

        try {
            $subApplication = SingleApplicationSubApplications::select('id', 'hour_spend', 'last_action_date')->where('id', $subApplicationId)->first();
            $conditions = $this->changeConditionFields(json_decode($conditions), true);

            $result = $this->getResult($functions, $subApplication, $conditions, "productBatches", true);

            return response()->json($this->getResponse("OK", $result));

        } catch (Exception $exception) {
            return response()->json($this->getResponse($exception->getCode(), '', $exception->getMessage()));
        }
    }

    /**
     * Function to get result
     *
     * @param $functions
     * @param $subApplication
     * @param $conditions
     * @param $relationName
     * @param bool $batch
     * @return int
     */
    private function getResult($functions, $subApplication, $conditions, $relationName, $batch = false)
    {
        $lastXML = SingleApplicationDataStructure::lastXml();

        switch ($functions) {
            case strpos($functions, "count("):
                $productIds = $this->filterMdmFieldCondition($conditions, $subApplication->{$relationName}($conditions)->get())->pluck('id')->all();

                return $subApplication->{$relationName}($conditions)->whereIn('saf_products.id', $productIds)->count();

            case strpos($functions, "sum("):
                $field = $this->getFieldNameInResult("sum", $batch, $lastXML, $functions);

                if (strpos($field, "FIELD_ID_") !== false && $field = str_replace("FIELD_ID_", "", $field)) {

                    $sum = 0;
                    if ($batch) {
                        $sum = $subApplication->{$relationName}($conditions)->sum(DB::raw("({$field})::numeric"));
                    } else {
                        $customData = isset($subApplication->custom_data) ? $subApplication->custom_data : [];
                        foreach ($customData as $key => $item) {
                            if (strpos($key, $field) !== false) {
                                $sum += $item;
                            }
                        }
                    }
                    return $sum;
                } else {
                    $productIds = $this->filterMdmFieldCondition($conditions, $subApplication->{$relationName}($conditions)->get())->pluck('id')->all();

                    return $subApplication->{$relationName}($conditions)->whereIn('saf_products.id', $productIds)->sum(DB::raw("cast({$field} as double precision)"));
                }

            case strpos($functions, "max("):
                $field = $this->getFieldNameInResult("max", $batch, $lastXML, $functions);

                if (strpos($field, "FIELD_ID_") !== false && $field = str_replace("FIELD_ID_", "", $field)) {
                    $max = 0;
                    if ($batch) {
                        $max = $subApplication->{$relationName}($conditions)->max(DB::raw("({$field})::numeric"));
                    } else {
                        $customData = isset($subApplication->custom_data) ? $subApplication->custom_data : [];
                        foreach ($customData as $key => $item) {
                            if (strpos($key, $field) !== false && $item > $max) {
                                $max = $item;
                            }
                        }
                    }
                    return $max;
                } else {
                    $productIds = $this->filterMdmFieldCondition($conditions, $subApplication->{$relationName}($conditions)->get())->pluck('id')->all();

                    return $subApplication->{$relationName}($conditions)->whereIn('saf_products.id', $productIds)->max(DB::raw("cast({$field} as double precision)"));
                }

            case strpos($functions, "min("):
                $field = $this->getFieldNameInResult("min", $batch, $lastXML, $functions);
                if (strpos($field, "FIELD_ID_") !== false && $field = str_replace("FIELD_ID_", "", $field)) {
                    $min = null;
                    if ($batch) {
                        $min = $subApplication->{$relationName}($conditions)->min(DB::raw("({$field})::numeric"));
                    } else {
                        $customData = isset($subApplication->custom_data) ? $subApplication->custom_data : [];
                        foreach ($customData as $key => $item) {
                            if (strpos($key, $field) !== false) {
                                if (!isset($min)) {
                                    $min = $item;
                                } else {
                                    if ($item < $min) {
                                        $min = $item;
                                    }
                                }
                            }
                        }
                    }
                    return $min;
                } else {
                    $productIds = $this->filterMdmFieldCondition($conditions, $subApplication->{$relationName}($conditions)->get())->pluck('id')->all();

                    return $subApplication->{$relationName}($conditions)->whereIn('saf_products.id', $productIds)->min(DB::raw("cast({$field} as double precision)"));
                }

            case strpos($functions, "avg("):
                $field = $this->getFieldNameInResult("avg", $batch, $lastXML, $functions);

                if (strpos($field, "FIELD_ID_") !== false && $field = str_replace("FIELD_ID_", "", $field)) {
                    $count = $sum = 0;
                    if ($batch) {
                        $avg = $subApplication->{$relationName}($conditions)->avg(DB::raw("({$field})::numeric"));
                    } else {
                        $customData = isset($subApplication->custom_data) ? $subApplication->custom_data : [];
                        foreach ($customData as $key => $item) {
                            if (strpos($key, $field) !== false) {
                                $count++;
                                $sum += $item;
                            }
                        }
                        $avg = $sum / $count;
                    }
                    return $avg;
                } else {
                    $productIds = $this->filterMdmFieldCondition($conditions, $subApplication->{$relationName}($conditions)->get())->pluck('id')->all();

                    if ($subApplication->{$relationName}($conditions)->whereIn('saf_products.id', $productIds)->count() == 1) {
                        return $subApplication->{$relationName}($conditions)->whereIn('saf_products.id', $productIds)->sum(DB::raw("cast({$field} as double precision)"));
                    } elseif ($subApplication->{$relationName}($conditions)->whereIn('saf_products.id', $productIds)->count() == 0) {
                        return 0;
                    }
                    return $subApplication->{$relationName}($conditions)->whereIn('saf_products.id', $productIds)->avg(DB::raw("cast({$field} as double precision)"));
                }
        }
    }

    /**
     * Function to get products by condition
     *
     * @param $subApplication
     * @param $conditions
     * @return array
     */
    private function getProductsByCondition($subApplication, $conditions)
    {
        $data = $subApplication->productListApi($conditions)->get();

        return $this->filterMdmFieldCondition($conditions, $data);
    }

    /**
     * Function to get custom data or tmp data
     *
     * @param $product
     * @return array|mixed
     */
    private function getSavedValue($product)
    {
        $getSavedValue = json_decode($product->custom_data);
        if (isset($product->tmp_data)) {
            $getSavedValue = json_decode($product->tmp_data);
            $getSavedValue = $getSavedValue->custom_data ?? [];
        }
        return $getSavedValue;
    }

    /**
     * Function to get field name result
     *
     * @param $functionName
     * @param $batch
     * @param $lastXML
     * @param $functions
     * @return string
     */
    private function getFieldNameInResult($functionName, $batch, $lastXML, $functions)
    {
        $field = str_replace("{$functionName}($", "", $functions);
        $field = str_replace(")", "", $field);
        if (strpos($field, "SAF_ID_") !== false) {
            $field = (string)getSafFieldById($lastXML, $field)[0]->attributes()->name;
        }

        if ($batch) {
            $field = str_replace("batch[_index_][", "", $field);
            $field = "saf_products_batch.tmp_data->>'" . str_replace("]", "", $field) . "'";
        }

        return $field;
    }

    /**
     * Function to parse condition to sql
     *
     * @param $conditions
     * @param $batch
     * @return array
     */
    public function changeConditionFields($conditions, $batch = false)
    {
        $refs = $mdmRefs = [];
        $lastXML = SingleApplicationDataStructure::lastXml();

        foreach ($conditions as &$condition) {
            $conditionField = str_replace("$", "", $condition[0]);
            if (strpos($condition[0], "SAF_ID_") !== false) {
                $xmlField = getSafFieldById($lastXML, $conditionField)[0];
                $mdm = DataModel::select('reference')->where(DB::raw('id::varchar'), (string)$xmlField->attributes()->mdm)->first();

                if (!is_null($mdm) && !empty($mdm->reference)) {
                    $refs[(string)$xmlField->attributes()->name] = $mdm->reference;
                }
                if ($batch) {
                    $name = str_replace("batch[_index_][", "", (string)$xmlField->attributes()->name);
                    $name = "saf_products_batch.tmp_data->" . str_replace("]", "", $name);
                } else {
                    $name = (string)$xmlField->attributes()->name;
                }
                $condition[0] = str_replace($conditionField, $name, $conditionField);
                $condition[3] = str_replace("$", "", $conditionField);
            } else {
                $conditionField = str_replace('$FIELD_ID_', "", $condition[0]);
                $mdm = DocumentsDatasetFromMdm::select('mdm_field_id')->where('field_id', $conditionField)->first();

                if ($mdm) {
                    $mdm = DataModel::select('reference')->where(DB::raw('id::varchar'), $mdm->mdm_field_id)->first();

                    if (!is_null($mdm) && !empty($mdm->reference)) {
                        $mdmRefs[$conditionField] = $mdm->reference;
                    }
                }

                if ($batch) {
                    $conditionField = str_replace("$", "", $conditionField);
                    $condition[0] = str_replace($conditionField, "saf_products_batch.tmp_data->{$conditionField}", $conditionField);
                } else {
                    $condition[0] = str_replace($conditionField, "FIELD_ID_{$conditionField}", $conditionField);
                }
                $condition[3] = str_replace("$", "", $conditionField);
            }

        }

        return [
            'refs' => $refs,
            'mdmRefs' => $mdmRefs,
            'conditions' => $conditions
        ];
    }

    /**
     * Function to get nex field id
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getNextId(Request $request)
    {
        $field = $request->input('field');
        $length = $request->input('length');

        try {
            if (strpos($field, '$SAF_ID_') === false) {
                $field = str_replace('$FIELD_ID_', '', $field);
                $result = SingleApplicationSubApplications::select('custom_data')->whereNotNull("custom_data->{$field}")->get();
                $max = 0;

                foreach ($result as $item) {
                    if ((int)$item->custom_data[$field] > $max) {
                        $max = (int)$item->custom_data[$field];
                    }
                }

                if (is_null($length)) {
                    return response()->json($this->getResponse("OK", ++$max));
                } else {
                    $prefix = '';
                    $max = ++$max;
                    for ($i = 0; $i < $length-strlen($max); $i++) {
                        $prefix .= '0';
                    }
                    return response()->json($this->getResponse("OK", $prefix.$max));
                }

            }

            return response()->json($this->getResponse("OK", 1));

        } catch (Exception $exception) {
            return response()->json($this->getResponse($exception->getCode(), "", $exception->getMessage()));
        }
    }

    /**
     * Function to get obligations
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function generateObligation(Request $request)
    {
        $sum = $request->input('sum');
        $obligationCode = $request->input('obligation-code');
        $subApplicationId = $request->input('sub-application-id');
        $stateId = $request->input('state_id');
        $userId = $request->input('user_id');

        try {
            $subApplication = SingleApplicationSubApplications::where('id', $subApplicationId)->first();

            $refTableName = ReferenceTable::REFERENCE_OBLIGATION_BUDGET_LINE;
            $obligation = DB::table($refTableName)->select([
                "{$refTableName}.id",
                "{$refTableName}.code",
                "{$refTableName}.budget_line",
                "{$refTableName}.obligation_sum",
                "reference_obligation_creation_type.obligation_creation_type"
            ])
                ->join("reference_obligation_creation_type", "reference_obligation_creation_type.id", "=", "{$refTableName}.obligation_creation_type")
                ->where("{$refTableName}.code", $obligationCode)
                ->where("{$refTableName}.show_status", '1')
                ->first();

            if (is_null($obligation)) {
                throw new Exception('not found');
            }

            $obligationData = [
                'user_id' => $userId,
                'saf_number' => $subApplication->saf_number,
                'state_id' => $stateId,
                'company_tax_id' => $subApplication->company_tax_id,
                'creator_company_tax_id' => optional($subApplication->data['saf'])->company_tax_id,
                'subapplication_id' => $subApplication->id,
                'budget_line' => $obligation->budget_line,
                'free_sum_budget_line_code' => null,
                'ref_budget_line_id' => $obligation->id,
                'is_free_sum' => false,
                'obligation_code' => $obligation->code,
                'obligation' => $sum,
                'payment' => 0,
                'created_at' => currentDateTime(),
                'pdf_hash_key' => str_random(64)
            ];

            return response()->json($this->getResponse("OK", SingleApplicationObligations::insert($obligationData)));

        } catch (Exception $exception) {
            return response()->json($this->getResponse($exception->getCode(), false, $exception->getMessage()));
        }

    }

    /**
     * Function to check is document attached
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function isDocAttached(Request $request)
    {
        $documentCode = $request->input('document-code');

        try {
            $subApplication = SingleApplicationSubApplications::select('saf_sub_applications.*')
                ->join('saf', 'saf.regular_number', '=', 'saf_sub_applications.saf_number')
                ->where('saf_sub_applications.id', $request->input('sub-application-id'))
                ->firstOrFail();
            $availableProductsIds = $subApplication->productList->pluck('id')->all();
            $attachedDocumentProducts = SingleApplicationSubApplications::getAttachedDocumentProducts($subApplication, $availableProductsIds, $documentCode);

            return response()->json($this->getResponse("OK", !($attachedDocumentProducts->isEmpty())));

        } catch (Exception $exception) {
            return response()->json($this->getResponse($exception->getCode(), '', $exception->getMessage()));
        }
    }

    /**
     * Function to not responding instruction
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function notRespondingInstruction(Request $request)
    {
        $subApplicationId = $request->input('sub-application-id');

        try {

            $notRespondingInstruction = SingleApplicationSubApplicationInstructions::leftJoin('saf_sub_application_instruction_feedback', 'saf_sub_application_instruction_feedback.saf_sub_application_instruction_id', '=', 'saf_sub_application_instructions.id')
                ->where('sub_application_id', $subApplicationId)
                ->whereNull('saf_sub_application_instruction_feedback.id')
                ->count();

            return response()->json($this->getResponse("OK", ($notRespondingInstruction == 0)));

        } catch (Exception $exception) {
            return response()->json($this->getResponse($exception->getCode(), '', $exception->getMessage()));
        }
    }

    /**
     * Function to check is identic
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function isIdentic(Request $request)
    {
        $subApplicationId = $request->input('sub-application-id');
        $field = $request->input('field_id');
        $hsCodeGroup = $request->input('hs_code_group');

        try {
            $result = false;
            $subApplication = SingleApplicationSubApplications::find($subApplicationId);
            $availableProductsIds = $subApplication->productList->pluck('id')->all();
            $singleAppProducts = SingleApplicationProducts::whereIn('id', $availableProductsIds)->where('saf_number', $subApplication->saf_number)->get();

            if (strpos($field, "SAF_ID_") !== false) {

                $lastXml = SingleApplicationDataStructure::lastXml();
                $field = str_replace("$", "", $field);
                $fieldAttributes = getSafFieldById($lastXml, $field)[0]->attributes();
                $fieldName = optional($fieldAttributes)->name;

                if (!is_null($fieldName)) {

                    if (is_null($hsCodeGroup)) {
                        $result = (count($singleAppProducts->groupBy((string)$fieldName)) == 1);
                    } else {
                        $resultArray = [];
                        foreach ($singleAppProducts as $singleAppProduct) {
                            if (isset($singleAppProduct->{$fieldName})) {
                                $reference = optional(DataModel::select('reference')->where('id', $fieldAttributes->mdm)->first())->reference;

                                if (!is_null($reference)) {
                                    $refData = getReferenceRows($reference, $singleAppProduct->{$fieldName});
                                    $resultArray[] = substr($refData->code, 0, $hsCodeGroup);
                                }
                            }
                        }
                        $result = (count(array_unique($resultArray)) == 1);
                    }
                }
            } else {
                $field = str_replace('$FIELD_ID_', "", $field);
                $resultArray = [];
                foreach ($singleAppProducts as $singleAppProduct) {
                    if (isset($subApplication->custom_data[$field . "_pr_" . $singleAppProduct->id])) {
                        $resultArray[] = $subApplication->custom_data[$field . "_pr_" . $singleAppProduct->id];
                    }
                }

                if (is_null($hsCodeGroup)) {
                    $result = (count(array_unique($resultArray)) == 1);
                } else {
                    $tmpData = [];
                    foreach ($resultArray as $value) {
                        $tmpData[] = substr($value, 0, $hsCodeGroup);
                    }
                    $result = (count(array_unique($tmpData)) == 1);
                }
            }

            return response()->json($this->getResponse("OK", $result));

        } catch (Exception $exception) {
            return response()->json($this->getResponse($exception->getCode(), '', $exception->getMessage()));
        }
    }

    /**
     * Function to get unanswered instructions
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function unansweredInstructionType(Request $request)
    {
        $subApplicationId = $request->input('sub-application-id');
        $instructionType = $request->input('instruction_type');

        $subApplication = SingleApplicationSubApplications::select('id', 'tmp_data')->find($subApplicationId);

        $tmpData = $subApplication->tmp_data;
        if (isset($tmpData['instructions_feedback'])) {
            foreach ($tmpData['instructions_feedback'] as $instructionsId => &$item) {
                $item['instructions_id'] = $instructionsId;
            }
        }
        $instructions = $tmpData['instructions_feedback'];
        if (is_null($instructionType)) {
            foreach ($instructions as $instruction) {
                if (is_null($instruction['feedback'])) {
                    return response()->json($this->getResponse("OK", true));
                }
            }
        } else {
            $subApplicationInstructionIds = SingleApplicationSubApplicationInstructions::whereIn('id', array_keys($instructions))->pluck('instruction_id', 'id')->all();
            $getInstructions = Instructions::select(['id', 'code', 'type'])->whereIn('id', $subApplicationInstructionIds)->get()->keyBy('id')->toArray();

            foreach ($getInstructions as $instructionId => $instruction) {
                $getInstructionType = getReferenceRows(ReferenceTable::REFERENCE_RISK_INSTRUCTION_TYPE, $instruction['type'], false, ['id', 'name', 'code']);

                if (isset($getInstructionType) && $getInstructionType->code == $instructionType) {
                    $findIndex = array_search($instructionId, $subApplicationInstructionIds);
                    if (isset($instructions[$findIndex]) && is_null($instructions[$findIndex]['feedback'])) {
                        return response()->json($this->getResponse("OK", true));
                    }
                }
            }
        }

        return response()->json($this->getResponse("OK", false));
    }


    /**
     * Function to get negative unanswered instructions
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function negativeAnsweredInstructionType(Request $request)
    {
        $subApplicationId = $request->input('sub-application-id');
        $instructionType = $request->input('instruction_type');

        $subApplication = SingleApplicationSubApplications::select('id', 'tmp_data')->find($subApplicationId);
        $tmpData = $subApplication->tmp_data;
        if (isset($tmpData['instructions_feedback'])) {
            foreach ($tmpData['instructions_feedback'] as $instructionsId => &$item) {
                $item['instructions_id'] = $instructionsId;
            }
        }

        $instructions = $tmpData['instructions_feedback'];

        if (is_null($instructionType)) {
            foreach ($instructions as $instruction) {
                if (!is_null($instruction['feedback'])) {
                    $getFeedBack = getReferenceRows(ReferenceTable::REFERENCE_RISK_FEEDBACK, $instruction['feedback'], false, ['id', 'name', 'code', 'digital_indicator']);

                    if (!is_null($getFeedBack) && $getFeedBack->digital_indicator < 0) {
                        return response()->json($this->getResponse("OK", true));
                    }
                }
            }
        } else {
            $subApplicationInstructionIds = SingleApplicationSubApplicationInstructions::whereIn('id', array_keys($instructions))->pluck('instruction_id', 'id')->all();
            $getInstructions = Instructions::select(['id', 'code', 'type'])->whereIn('id', $subApplicationInstructionIds)->get()->keyBy('id')->toArray();

            foreach ($getInstructions as $instructionId => $instruction) {
                $getInstructionType = getReferenceRows(ReferenceTable::REFERENCE_RISK_INSTRUCTION_TYPE, $instruction['type'], false, ['id', 'name', 'code']);

                if (isset($getInstructionType) && $getInstructionType->code == $instructionType) {
                    $findIndex = array_search($instructionId, $subApplicationInstructionIds);
                    if (isset($instructions[$findIndex]) && !is_null($instructions[$findIndex]['feedback'])) {

                        $getFeedBack = getReferenceRows(ReferenceTable::REFERENCE_RISK_FEEDBACK, $instructions[$findIndex]['feedback'], false, ['id', 'name', 'code', 'digital_indicator']);

                        if (!is_null($getFeedBack) && $getFeedBack->digital_indicator < 0) {
                            return response()->json($this->getResponse("OK", true));
                        }

                    }
                }
            }
        }

        return response()->json($this->getResponse("OK", false));
    }

    /**
     * Function to get laboratories total sum
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getLabObligations(Request $request)
    {
        $subApplicationId = $request->input('sub-application-id');

        $subApplication = SingleApplicationSubApplications::find($subApplicationId);

        $price = 0;
        // true if rule was run from laboratory application
        if (isset($subApplication->send_lab_company_tax_id)) {
            $data = [];
            $tmpLabExaminations = $subApplication->tmp_data['lab_examination'] ?? [];
            $labExaminations = $subApplication->labIndicators->keyBy('saf_product_id')->toArray();

            foreach ($labExaminations as $productId => $labExamination) {
                $data[$productId][$labExamination['indicator_id']] = [
                    'indicator' => (string)$labExamination['indicator_id'],
                    'found_size' => (string)$labExamination['found_size'],
                    'found_or_not' => (string)$labExamination['found_or_not'],
                    'correspond_or_not' => (string)$labExamination['correspond_or_not'],
                    'duration' => (string)$labExamination['duration'],
                    'price' => (string)$labExamination['price']
                ];
            }
            $labProducts = arrayMergeRecursiveDeep($data, $tmpLabExaminations);
            foreach ($labProducts as $labProduct) {
                foreach ($labProduct as $labIndicator) {
                    $price += $labIndicator['price'];
                }
            }
        } else {
            $labApplications = SingleApplicationSubApplications::where('from_subapplication_id_for_labs', $subApplicationId)->whereNotNull('send_lab_company_tax_id')->get();

            foreach ($labApplications as $labApplication) {
                $data = [];
                $tmpLabExaminations = $subApplication->tmp_data['lab_examination'] ?? [];
                $labExaminations = $labApplication->labIndicators->keyBy('saf_product_id')->toArray();

                foreach ($labExaminations as $productId => $labExamination) {
                    $data[$productId][$labExamination['indicator_id']] = [
                        'indicator' => (string)$labExamination['indicator_id'],
                        'found_size' => (string)$labExamination['found_size'],
                        'found_or_not' => (string)$labExamination['found_or_not'],
                        'correspond_or_not' => (string)$labExamination['correspond_or_not'],
                        'duration' => (string)$labExamination['duration'],
                        'price' => (string)$labExamination['price']
                    ];
                }
                $labProducts = arrayMergeRecursiveDeep($data, $tmpLabExaminations);
                foreach ($labProducts as $labProduct) {
                    foreach ($labProduct as $labIndicator) {
                        $price += $labIndicator['price'];
                    }
                }
            }
        }

        return response()->json($this->getResponse("OK", $price));
    }

    /**
     * Function to get return true if have a unanswered examinations
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function unansweredExamination(Request $request)
    {
        $subApplicationId = $request->input('sub-application-id');

        $subApplication = SingleApplicationSubApplications::select('id', 'tmp_data')->find($subApplicationId);

        $response = false;

        if ($subApplication && isset($subApplication->tmp_data['lab_examination'])) {
            foreach ($subApplication->tmp_data['lab_examination'] ?? [] as $labExaminations) {
                unset($labExaminations['correspond']);

                foreach ($labExaminations as $labExamination) {
                    if (!isset($labExamination['found_size']) || !isset($labExamination['found_or_not']) || !isset($labExamination['correspond_or_not'])) {
                        $response = true;
                        break 2;
                    }
                }
            }

        }

        return response()->json($this->getResponse("OK", $response));
    }

    /**
     * Function to compare
     *
     * @param $var1
     * @param $operator
     * @param $var2
     * @return bool
     */
    private function compare($var1, $operator, $var2)
    {
        switch ($operator) {
            case "=":  return $var1 == $var2;
            case "!=": return $var1 != $var2;
            case ">=": return $var1 >= $var2;
            case "<=": return $var1 <= $var2;
            case ">":  return $var1 >  $var2;
            case "<":  return $var1 <  $var2;
            default:   return true;
        }
    }

    /**
     * Function to get Response Array
     *
     * @param $status
     * @param $response
     * @param $message
     * @return array
     */
    private function getResponse($status, $response, $message = "")
    {
        return [
            'status' => $status,
            'response' => $response,
            'message' => $message,
        ];
    }
}
