<?php

namespace App\Http\Controllers;

use App\EntityTypesInfo\LegalEntityForms;
use App\Exceptions\TaxServiceTjCompanyInactiveException;
use App\Exceptions\TaxServiceTjInnNotFoundException;
use App\Exceptions\TaxServiceTjInvalidResponseException;
use App\Models\BaseModel;
use App\Models\ReferenceTable\ReferenceTable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * Class DocumentController
 * @package App\Http\Controllers
 */
class EntityTypeController extends BaseController
{
    /**
     * @var LegalEntityForms
     */
    protected $legalEntityForms;

    /**
     * CompanyController constructor.
     *
     * @param LegalEntityForms $legalEntityForms
     */
    public function __construct(LegalEntityForms $legalEntityForms)
    {
        $this->legalEntityForms = $legalEntityForms;
    }

    /**
     * Function to getting user with ssn or company by tax id
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws TaxServiceTjCompanyInactiveException
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function entityTypeInfo(Request $request)
    {
        $userType = $request->get('type');
        $taxIdOrSSN = $request->get('taxIdSSN');

        $validateFields = [
            'type' => 'required|integer_with_max|exists:reference_legal_entity_form_reference,id',
            'taxIdSSN' => 'required|string_with_max',
        ];

        // Company
        if (ReferenceTable::isLegalEntityType($userType, BaseModel::ENTITY_TYPE_TAX_ID)) {
            $validateFields['taxIdSSN'] .= '|tax_id_validator';
        }

        // User
        if (ReferenceTable::isLegalEntityType($userType, BaseModel::ENTITY_TYPE_USER_ID)) {
            $validateFields['taxIdSSN'] .= '|ssn_validator';
//            $validateFields['passport'] = 'required|passport_validator';
        }

        // Validate
        $validator = Validator::make($request->all(), $validateFields);
        if ($validator->fails()) {

            $errors['message'] = trans('core.loading.invalid_data');

            return responseResult('INVALID_DATA', '', '', $errors);
        }

        //
        $referenceLegalEntity = ReferenceTable::legalEntityValues();
        $legalEntityType = array_search($userType, $referenceLegalEntity);

        $result = null;
        if ($legalEntityType == BaseModel::ENTITY_TYPE_TAX_ID) {
            $result = $this->legalEntityForms->getCompanyInfo($taxIdOrSSN);
        }

        if ($legalEntityType == BaseModel::ENTITY_TYPE_USER_ID) {
            $result = $this->legalEntityForms->getUserInfo($taxIdOrSSN, $request->get('passport'), false);

            if ($result) {
                $notUsedInfoKeys = ["passport_type", "passport_issue_date", "passport_issued_by", "phone_number"];
                foreach ($notUsedInfoKeys as $notUsedInfoKey) {
                    unset($result[$notUsedInfoKey]);
                }
            }
        }

        if ($result) {

            $data['info'] = $result;

            return responseResult('OK', '', $data);
        }

        $errors['message'] = trans('swis.' . $legalEntityType . '.not_found');

        return responseResult('INVALID_DATA', '', '', $errors);
    }

    /**
     * Function to get company info by tax id
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws TaxServiceTjCompanyInactiveException
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function getCompanyInfoByTaxId(Request $request)
    {
        $this->validate($request, [
            'tax_id' => 'required|tax_id_validator'
        ]);

        $company = $this->legalEntityForms->getCompanyInfo($request->input('tax_id'));
        if ($company) {

            $data['company'] = [
                'name' => $company['name'],
                'address' => $company['address'],
            ];

            return responseResult('OK', '', $data);
        }

        $errors['message'] = trans('swis.tax_id.not_found');

        return responseResult('INVALID_DATA', '', '', $errors);
    }
}