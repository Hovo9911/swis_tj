<?php

namespace App\Http\Controllers;

use App\EntityTypesInfo\LegalEntityForms;
use App\Exceptions\TaxServiceTjCompanyInactiveException;
use App\Exceptions\TaxServiceTjInnNotFoundException;
use App\Exceptions\TaxServiceTjInvalidResponseException;
use App\Http\Requests\ProfileSettings\ProfileSettingsRequest;
use App\Http\Requests\User\UserSearchRequest;
use App\Http\Requests\User\UserStoreRequest;
use App\Http\Requests\User\UserUpdateRequest;
use App\Models\Languages\Language;
use App\Models\Menu\Menu;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\RegionalOffice\RegionalOffice;
use App\Models\Role\Role;
use App\Models\User\User;
use App\Models\User\UserManager;
use App\Models\User\UserSearch;
use App\Models\UserRoles\UserRoles;
use App\Services\TaxService\TaxService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends BaseController
{
    /**
     * @var UserManager
     */
    protected $manager;

    /**
     * @var LegalEntityForms
     */
    protected $legalEntityForms;

    /**
     * UserController constructor.
     *
     * @param UserManager $manager
     * @param LegalEntityForms $legalEntityForms
     */
    public function __construct(UserManager $manager, LegalEntityForms $legalEntityForms)
    {
        $this->manager = $manager;
        $this->legalEntityForms = $legalEntityForms;
    }

    /**
     * Function to show index page
     *
     * @return View
     */
    public function index()
    {
        return view('user.index')->with([
            'modules' => Menu::isModule()->orderBy('sort_order')->get(),
            'subDivisions' => RegionalOffice::getRefSubdivisions(),
            'refCustomBodies' => getReferenceRows(ReferenceTable::REFERENCE_CUSTOM_BODY_REFERENCE)
        ]);
    }

    /**
     * Function to get all data and showing in datatable
     *
     * @param UserSearchRequest $userSearchRequest
     * @param UserSearch $userSearch
     * @return JsonResponse
     */
    public function table(UserSearchRequest $userSearchRequest, UserSearch $userSearch)
    {
        $data = $this->dataTableAll($userSearchRequest, $userSearch);

        return response()->json($data);
    }

    /**
     * Function to show create page
     *
     * @return View
     */
    public function create()
    {
        return view('user.edit')->with([
            'saveMode' => 'add'
        ]);
    }

    /**
     * Function to store data
     *
     * @param UserStoreRequest $request
     * @return JsonResponse
     *
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function store(UserStoreRequest $request)
    {
        $data = $request->validated();
        $addedManually = $data['added_manually'] ?? User::FALSE;

        if (!$addedManually) {
            $userInfo = $this->legalEntityForms->getUserInfo($data['ssn'], $data['passport']);

            if (!$userInfo) {
                $errors['ssn'] = trans('swis.ssn.field.not_found');

                return responseResult('INVALID_DATA', '', '', $errors);
            }
        }

        //
        $companies = $request->input('company');
        if (isset($companies)) {

            $errors = [];
            foreach ($companies as $key => $company) {

                if (!isset($company['is_head']) && $company['tax_id']) {
                    $companyHasHeadOfCompany = User::getCompanyHeads($company['tax_id'], true);

                    if (!$companyHasHeadOfCompany->count()) {
                        $errors['company-' . $key . '-tax_id'] = trans('swis.user.company.head.not.exist.message');
                    }
                }
            }

            if (count($errors) > 0) {
                return responseResult('INVALID_DATA', '', '', $errors);
            }
        }

        // User Info
        if (!$addedManually) {
            $data['first_name'] = $userInfo['first_name'];
            $data['last_name'] = $userInfo['last_name'];
            $data['passport_type'] = $userInfo['passport_type'];
            $data['passport'] = $userInfo['passport'];
            $data['passport_issued_by'] = $userInfo['passport_issued_by'];
            $data['passport_issue_date'] = $userInfo['passport_issue_date'];
        }

        $user = $this->manager->store($data);

        return responseResult('OK', urlWithLng('user/' . $user->id . '/edit'));
    }

    /**
     * Function to show current by id
     *
     * @param $lngCode
     * @param $id
     * @return RedirectResponse|View
     */
    public function edit($lngCode, $id)
    {
        $user = User::where('id', $id)->firstOrFail();

        if ($user->is_customs_operator) {
            return redirect(urlWithLng('user_customs_operator/' . $user->id . '/edit'));
        }

        return view('user.edit')->with([
            'userRolesList' => $this->getUserAuthorizedRolesList($id),
            'user' => $user,
            'saveMode' => 'edit'
        ]);
    }

    /**
     * Function to view user
     *
     * @param $lngCode
     * @param $id
     * @return RedirectResponse|View
     */
    public function view($lngCode, $id)
    {
        $user = User::where('id', $id)->firstOrFail();

        if ($user->is_customs_operator) {
            return redirect(urlWithLng('user_customs_operator/' . $user->id . '/view'));
        }

        return view('user.view')->with([
            'user' => $user,
            'userRolesList' => $this->getUserAuthorizedRolesList($id),
            'saveMode' => 'view',
        ]);
    }

    /**
     * Function to update data
     *
     * @param UserUpdateRequest $request
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     *
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function update(UserUpdateRequest $request, $lngCode, $id)
    {
        $user = User::select('ssn', 'added_manually')->where('id', $id)->firstOrFail();

        //
        $data = $request->validated();
        $data['added_manually'] = $data['added_manually'] ?? User::FALSE;

        //
        if (!$data['added_manually']) {
            $userInfo = $this->legalEntityForms->getUserInfo($user->ssn, $data['passport']);
        }

        $companies = $data['company'];
        if (count($companies)) {
            $errors = [];

            foreach ($companies as $key => $company) {

                if (!isset($company['is_head']) && $company['tax_id']) {
                    $companyHasHeadOfCompany = User::getCompanyHeads($company['tax_id'], true);

                    if (!$companyHasHeadOfCompany->count()) {
                        $errors['company-' . $key . '-tax_id'] = trans('swis.user.company.head.not.exist.message');
                    }
                }
            }

            if (count($errors) > 0) {
                return responseResult('INVALID_DATA', '', '', $errors);
            }
        }

        if (!$data['added_manually']) {
            $data['first_name'] = $userInfo['first_name'];
            $data['last_name'] = $userInfo['last_name'];
            $data['passport_type'] = $userInfo['passport_type'];
            $data['passport'] = $userInfo['passport'];
            $data['passport_issued_by'] = $userInfo['passport_issued_by'];
            $data['passport_issue_date'] = $userInfo['passport_issue_date'];
        }

        $this->manager->update($data, $id);

        return responseResult('OK', urlWithLng("user/$id/edit"));
    }

    /**
     * Function to get user info by ssn
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function getUserInfoBySSN(Request $request)
    {
        $this->validate($request, [
            'ssn' => 'required|string_with_max|ssn_validator',
        ]);

        $ssn = $request->input('ssn');

        $user = User::select('ssn')->where('ssn', $ssn)->first();

        // User is exist no need to register
        if (!is_null($user)) {
            $errors['message'] = trans('swis.user.ssn.exist.message');

            return responseResult('INVALID_DATA', '', '', $errors);
        }

        //
        $userInfo = $this->legalEntityForms->getUserInfo($ssn, false, false);

        $data = [
            'first_name' => $userInfo['first_name'],
            'last_name' => $userInfo['last_name'],
            'passport' => $userInfo['passport'],
            'address' => $userInfo['address'],
            'email' => '',
            'phone_number' => $userInfo['phone_number'],
        ];

        return responseResult('OK', '', $data);

    }

    /**
     * Function to check company tax id is exist or not
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws TaxServiceTjCompanyInactiveException
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function getCompanyInfoByTaxId(Request $request)
    {
        $this->validate($request, [
            'tax_id' => 'required|string_with_max|tax_id_validator'
        ]);

        $company = $this->legalEntityForms->getCompanyInfo($request->get('tax_id'), true);

        if ($company) {
            $data['company_name'] = $company['name'];
        } else {
            return responseResult('INVALID_DATA', '', '', ['message' => trans('swis.company.not_found.message')]);
        }

        return responseResult('OK', '', $data);
    }

    /**
     * Function to check username and if it's exist return suggestion
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function checkUsername(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|string_with_max'
        ]);

        $suggestionList = [];
        $username = $request->input('username');

        if (User::where(DB::Raw('lower(username)'), strtolower($username))->exists()) {
            $suggestionList = $this->getFreeUsernameList($username);
        }

        return responseResult('OK', null, ['suggestionList' => $suggestionList]);
    }

    /**
     * Function to return 3 free username
     *
     * @param $username
     * @param array $suggestionList
     * @return array
     */
    public function getFreeUsernameList($username, $suggestionList = [])
    {
        if (count($suggestionList) >= 3) {
            return $suggestionList;
        } else {
            $generatedUsername = $username . rand(100, 999);

            if (User::where('username', $generatedUsername)->exists()) {
                return $this->getFreeUsernameList($username);
            } else {
                $suggestionList[] = $generatedUsername;
                return $this->getFreeUsernameList($username, $suggestionList);
            }
        }
    }

    /**
     * Function to show profile settings view
     *
     * @return View
     */
    public function profileSettingsShow()
    {
        $userRolesList = $this->getUserAuthorizedRolesList();

        return view('user.profile-settings', compact('userRolesList'));
    }

    /**
     * Function to store user profile settings
     *
     * @param ProfileSettingsRequest $request
     * @return View
     */
    public function profileSettingsStore(ProfileSettingsRequest $request)
    {
        $data = $request->validated();
        $this->manager->storeProfileSettings($data);

        $lngCode = Language::where('id', $data['lng_id'])->pluck('code')->first() ?? 'en';

        return responseResult('OK', url($lngCode . '/profile-settings'));
    }

    /**
     * Function to get company heads
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getCompanyHead(Request $request)
    {
        $this->validate($request, [
            'companyTaxId' => 'required|string_with_max'
        ]);

        $companyHead = UserRoles::select('user_id')->where(['company_tax_id' => $request->get('companyTaxId'), 'role_id' => Role::HEAD_OF_COMPANY])->whereNull('authorized_by_id')->activeRole()->first();

        if (is_null($companyHead)) {
            $data['companyHasAdmin'] = false;

            return responseResult('OK', '', $data);
        }

        $user = User::select('first_name', 'last_name')->where('id', $companyHead->user_id)->first();

        if (!is_null($user)) {
            $data = [
                'companyHasAdmin' => true,
                'companyAdminName' => $user->first_name . ' ' . $user->last_name
            ];

            return responseResult('OK', '', $data);
        }

        $error['message'] = 'User Not Exist';

        return responseResult('INVALID_DATA', '', '', $error);
    }

    /**
     * Function to send email again
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function emailSendAgain(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required|integer_with_max|exists:users,id',
            'lng_id' => 'nullable|integer_with_max|exists:languages,id',
        ]);

        $this->manager->emailSendAgain($request->input('lng_id'), $request->input('user_id'));

        return responseResult('OK', back()->getTargetUrl());
    }

    /**
     * Function to get what roles user have with sorting
     *
     * @param bool $userId
     * @return array
     */
    private function getUserAuthorizedRolesList($userId = false)
    {
        if (!$userId) {
            $userId = Auth::id();
        }

        $defaultRoles = UserRoles::with(['authorizer', 'menu', 'company', 'role'])->where('user_id', $userId)->where('role_id', '!=', Role::NATURAL_PERSON)->whereNull('authorized_by_id')->orderByDesc()->get();
        $swAdminRoles = UserRoles::with(['authorizer', 'menu', 'company', 'role'])->where('user_id', $userId)->where('role_id', Role::SW_ADMIN)->join('menu', 'user_roles.menu_id', '=', 'menu.id')->whereNull('parent_id')->orderByDesc('user_roles.id')->get();
        $userAuthorizedRoles = UserRoles::with(['authorizer', 'menu', 'company', 'role'])->where('user_id', $userId)->whereNotNull('authorized_by_id')->where('company_tax_id', '=', '0')->where('role_id', '!=', Role::SW_ADMIN)->orderByDesc()->get();
        $companyAuthorizedRoles = UserRoles::with(['authorizer', 'menu', 'company', 'role'])->where('user_id', $userId)->where('company_tax_id', '!=', '0')->whereNotNull('authorized_by_id')->orderByDesc()->get();

        $userAuthorized = [];
        foreach ($userAuthorizedRoles as $value) {
            $userAuthorized[$value->authorized_by_id][] = $value;
        }

        $companyAuthorized = [];
        foreach ($companyAuthorizedRoles as $value) {
            $companyAuthorized[$value->company_tax_id][] = $value;
        }

        return [
            'defaultRoles' => $defaultRoles,
            'swAdminRoles' => $swAdminRoles,
            'userAuthorizedRoles' => $userAuthorized,
            'companyAuthorizedRoles' => $companyAuthorized,
        ];
    }

    /**
     * Function to get User info and check what data is changed
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function updateUserInfoFromService(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required|integer_with_max|exists:users,id',
        ]);

        $user = User::select('id', 'ssn', 'passport', 'passport_issue_date', 'passport_issued_by', 'phone_number', 'registration_address')->where('id', $request->input('user_id'))->first();

        //
        $userInfo = $this->legalEntityForms->getUserInfo($user->ssn, false, false);

        $userCurrentInfo = [
            'passport' => $user->passport,
            'passport_issue_date' => $user->passport_issue_date ?? '',
            'passport_issued_by' => $user->passport_issued_by ?? '',
            'address' => $user->registration_address,
            'phoneNumber' => $user->phone_number ?? '',
        ];

        $userInfoFromService = [
            'passport' => $userInfo['passport'],
            'passport_issue_date' => $userInfo['passport_issue_date'],
            'passport_issued_by' => $userInfo['passport_issued_by'],
            'address' => $userInfo['address'],
            'phoneNumber' => $userInfo['phone_number']
        ];

        $changedInfo = array_diff($userInfoFromService, $userCurrentInfo);

        if (count($changedInfo)) {

            $changedFieldNameKeys = [];
            foreach (array_keys($changedInfo) as $value) {
                if ($value == 'passport_issue_date') {
                    $changedInfo['passport_issue_date'] = date(config('swis.date_format_front'), strtotime($changedInfo['passport_issue_date']));
                }

                $changedFieldNameKeys [] = trans('swis.user.from_service.field.' . $value . '.is_changed');
            }

            $data = [
                'changedInfo' => $changedInfo,
                'message' => trans('swis.user.from_service.info.is_changed.message', ['fields' => implode(', ', $changedFieldNameKeys)])
            ];

            return responseResult('OK', '', $data);
        }

        $data['message'] = trans('swis.user.from_service.info.not_changed.message');

        return responseResult('OK', '', $data);
    }

    /**
     * Function to get service info of user/company from TaxService
     *
     * @return View
     */
    public function showServiceInfo()
    {
        return view('user.service-info');
    }

    /**
     * Function to get service info of user/company from TaxService
     *
     * @param Request $request
     * @param TaxService $taxService
     * @return JsonResponse
     *
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function serviceInfo(Request $request, TaxService $taxService)
    {
        $validator = Validator::make($request->all(), [
            'tin' => 'required|string_with_max|max:12',
        ]);

        if ($validator->fails()) {
            $errors['tin'] = trans('core.loading.invalid_data');

            return responseResult('INVALID_DATA', '', '', $errors);
        }

        $data['info'] = $taxService->getXmlResponse($request->get('tin'));

        return responseResult('OK', '', $data);
    }

    /**
     * Function to clear user roles session
     *
     * @return RedirectResponse
     */
    public function clearUserRoleSession()
    {
        Auth::user()->removeCurrentUserRole();

        return redirect(urlWithLng('role/set'));
    }
}
