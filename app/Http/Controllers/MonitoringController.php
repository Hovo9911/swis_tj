<?php

namespace App\Http\Controllers;

use App\Exports\MonitoringPaymentsExport;
use App\Exports\MonitoringPaysExport;
use App\Exports\MonitoringSubApplicationsExport;
use App\Models\Agency\Agency;
use App\Models\Company\Company;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\Monitoring\TransactionsMonitoringSearch;
use App\Models\Monitoring\TransactionsSearch;
use App\Models\Monitoring\UserDocumentsSearch;
use App\Models\Monitoring\UserRolesSearch;
use App\Models\Monitoring\UserSearch;
use App\Models\PaymentProviders\PaymentProviders;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\Transactions\Transactions;
use App\Models\User\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Class AgencyController
 * @package App\Http\Controllers
 */
class MonitoringController extends BaseController
{
    /**
     * Function to show index page
     *
     * @return View
     */
    public function index()
    {
        $paymentProviders = PaymentProviders::select('id', DB::raw("CONCAT(name,' - ',description) AS name"))->active()->get();

        return view('monitoring.index')->with([
            'constructorDocuments' => ConstructorDocument::getConstructorDocuments([], false),
            'expressControls' => getReferenceRows(ReferenceTable::REFERENCE_EXPRESS_CONTROL, false, false, ['id', 'name']),
            'agencies' => Agency::allData(),
            'paymentProviders' => $paymentProviders
        ]);
    }

    /**
     * Function to all data showing in datatable
     *
     * @param Request $request
     * @param UserSearch $userSearch
     * @return JsonResponse
     */
    public function usersTable(Request $request, UserSearch $userSearch)
    {
        $data = $this->dataTableAll($request, $userSearch);

        return response()->json($data);
    }

    /**
     * Function to all data showing in datatable
     *
     * @param Request $request
     * @param UserRolesSearch $userRolesSearch
     * @return JsonResponse
     */
    public function userRolesTable(Request $request, UserRolesSearch $userRolesSearch)
    {
        $data = $this->dataTableAll($request, $userRolesSearch);

        return response()->json($data);
    }

    /**
     * Function to all data showing in datatable
     *
     * @param Request $request
     * @param UserDocumentsSearch $userDocumentsSearch
     * @return JsonResponse
     */
    public function applicationsTable(Request $request, UserDocumentsSearch $userDocumentsSearch)
    {
        $data = $this->dataTableAll($request, $userDocumentsSearch);

        return response()->json($data);
    }

    /**
     * Function to export payments table structure
     *
     * @param $lngCode
     * @param Request $request
     * @return BinaryFileResponse
     */
    public function applicationsTableExport($lngCode, Request $request)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);

        //
        $userDocumentSearch = new UserDocumentsSearch();
        $data = $userDocumentSearch->callToConstructorQuery($request->all())->orderByDesc('saf_sub_application_id')->get();

        $referenceExpressControlAll = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_EXPRESS_CONTROL);
        $referenceSubDivisionAll = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS);

        //
        $users = User::select('id', 'ssn', DB::raw("users.first_name || ' ' || users.last_name AS username"))->get()->keyBy('id')->toArray();
        $usersKeyBySSN = User::select('id', 'ssn', DB::raw("users.first_name || ' ' || users.last_name AS username"))->get()->keyBy('ssn')->toArray();

        //
        $states = ConstructorStates::select('id', 'constructor_states_ml.state_name')->joinMl()->get()->keyBy('id')->toArray();

        //
        $agencies = Agency::select('agency.id', 'agency_ml.name', 'tax_id')->joinMl()->get()->keyBy('tax_id')->toArray();

        //
        $companies = Company::select('name', 'tax_id')->get()->keyBy('tax_id')->toArray();

        $result = [];
        foreach ($data as $key => $value) {

            $safData = $value->data;

            $applicantName = $applicantUserName = '';
            if (isset($safData['saf']['sides']) && isset($safData['saf']['sides']['applicant'])) {
                $applicantTin = $safData['saf']['sides']['applicant']['type_value'] ?? '';

                if ($applicantTin) {

                    $applicantName = '(' . $applicantTin . ') ';

                    if (isset($usersKeyBySSN[$applicantTin])) {
                        $applicantName .= $usersKeyBySSN[$applicantTin]['username'];
                    } else {
                        if (isset($companies[$applicantTin])) {
                            $applicantName .= $companies[$applicantTin]['name'];
                        }
                    }
                }
            }

            //
            $expressControl = '';
            if ($value->express_control_id && isset($referenceExpressControlAll[$value->express_control_id])) {
                $expressControl = $referenceExpressControlAll[$value->express_control_id]['name'];
            }

            $subDivision = '';
            if ($value->sub_application_agency_subdivision && isset($referenceSubDivisionAll[$value->sub_application_agency_subdivision])) {
                $subDivision = $referenceSubDivisionAll[$value->sub_application_agency_subdivision]['name'];
            }

            $subApplicationAgencyName = '';
            if ($value->sub_application_tax_id) {
                if (isset($agencies[$value->sub_application_tax_id])) {
                    $subApplicationAgencyName = '(' . $agencies[$value->sub_application_tax_id]['tax_id'] . ') ' . $agencies[$value->sub_application_tax_id]['name'];
                }
            }

            if ($value->user_id) {
                if (isset($users[$value->user_id])) {
                    $applicantUserName = '(' . $users[$value->user_id]['ssn'] . ') ' . $users[$value->user_id]['username'];
                }
            }

            $result[] = [
                'id' => $value->saf_sub_application_id,
                'state' => isset($states[$value->state_id]) ? $states[$value->state_id]['state_name'] : '',
                'saf_status_type' => $value->saf_status_type,
                'saf_number' => $value->saf_number,
                'sub_application_send_date' => formattedDate($value->sub_application_send_date),
                'sub_application_number' => $value->sub_application_number,
                'document_type' => $value->document_type,
                'sub_application_express_control' => $expressControl,
                'sub_application_agency' => $subApplicationAgencyName,
                'sub_application_agency_subdivision' => $subDivision,
                'sub_application_registration_number' => $value->sub_application_registration_number,
                'sub_application_registration_date' => formattedDate($value->sub_application_registration_date),
                'sub_application_permission_number' => $value->sub_application_registration_number,
                'sub_application_permission_date' => formattedDate($value->sub_application_permission_date),
                'sub_application_expiration_date' => formattedDate($value->sub_application_expiration_date),
                'saf_applicant_tin' => $applicantName,
                'saf_applicant_user' => $applicantUserName,
            ];

        }

        //
        $userDocumentSearchExport = new MonitoringSubApplicationsExport();
        $userDocumentSearchExport->setData($result);

        return Excel::download($userDocumentSearchExport, "monitoring_payment_events.xls");
    }

    /**
     * Function to all data showing in datatable
     *
     * @param Request $request
     * @param TransactionsSearch $transactionsSearch
     * @return JsonResponse
     */
    public function paymentsTable(Request $request, TransactionsSearch $transactionsSearch)
    {
        $data = $this->dataTableAll($request, $transactionsSearch);

        return response()->json($data);
    }

    /**
     * Function to export payments table structure
     *
     * @param TransactionsSearch $transactionsSearch
     * @param Request $request
     * @return BinaryFileResponse
     */
    public function paymentsTableExport(TransactionsSearch $transactionsSearch, Request $request)
    {
        $query = $transactionsSearch->callToConstructorQuery($request->all());
        $returnArray = $query->orderByDesc('transactions.id')->get();

        foreach ($returnArray as &$value) {
            if (is_null($value->payment_id) && $value->type == Transactions::PAYMENT_TYPE_OUTPUT) {
                $value->payment_id = 'SW' . str_pad($value->increment_id, 10, '0', STR_PAD_LEFT);
            }

            unset($value->type);
            unset($value->increment_id);
        }

        //
        $transactionExport = new MonitoringPaymentsExport();
        $transactionExport->setData($returnArray);

        return Excel::download($transactionExport, "monitoring_payment_events.xls");
    }

    /**
     * Function to all data showing in datatable
     *
     * @param Request $request
     * @param TransactionsMonitoringSearch $transactionsSearch
     * @return JsonResponse
     */
    public function paysTable(Request $request, TransactionsMonitoringSearch $transactionsSearch)
    {
        $data = $this->dataTableAll($request, $transactionsSearch);

        return response()->json($data);
    }

    /**
     * Function to export pays table structure
     *
     * @param $lngCode
     * @param Request $request
     * @return BinaryFileResponse
     */
    public function paysTableExport($lngCode, Request $request)
    {
        $transactionSearch = new TransactionsMonitoringSearch();
        $query = $transactionSearch->callToConstructorQuery($request->all());
        $returnArray = $query->orderByDesc('payments.id')->get();

        //
        $transactionExport = new MonitoringPaysExport();
        $transactionExport->setData($returnArray);

        return Excel::download($transactionExport, "monitoring_replenishment_events.xls");
    }

    /**
     * Function  to get document statuses
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getDocumentStatues(Request $request)
    {
        $document = ConstructorDocument::select('id')->where('document_code', $request->input('documentCode'))->active()->firstOrFail();

        $states = ConstructorStates::select('id', 'state_name')->joinMl()->where('constructor_document_id', $document->id)->active()->get();

        return response()->json($states);
    }

    /**
     * Function to get Agency Subdivisions
     *
     * @param Request $request
     * @return mixed
     */
    public function getAgencySubdivisions(Request $request)
    {
        $this->validate($request, [
            'agency_tax_id' => 'required|string_with_max|exists:agency,tax_id',
        ]);

        $data['subdivisions'] = Agency::getSubdivisions($request->input('agency_tax_id'));

        return responseResult('OK', '', $data);
    }
}
