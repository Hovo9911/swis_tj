<?php

namespace App\Http\Controllers\Risks;

use App\Http\Controllers\BaseController;
use App\Http\Requests\RiskProfiles\RiskProfilesRequest;
use App\Http\Requests\RiskProfiles\RiskProfilesSearchRequest;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\Instructions\Instructions;
use App\Models\RiskProfile\RiskProfile;
use App\Models\RiskProfile\RiskProfileManager;
use App\Models\RiskProfile\RiskProfileSearch;
use Illuminate\Http\JsonResponse;
use Illuminate\View\View;

/**
 * Class RiskProfileController
 * @package App\Http\Controllers\Risks
 */
class RiskProfileController extends BaseController
{
    /**
     * @var RiskProfileManager
     */
    protected $manager;

    /**
     * RiskProfileController constructor.
     *
     * @param RiskProfileManager $manager
     */
    public function __construct(RiskProfileManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Function to show index page
     *
     * @return View
     */
    public function index()
    {
        return view('risk-profiles.index')->with([
            'constructorDocuments' => ConstructorDocument::getConstructorDocumentForAgency()
        ]);
    }

    /**
     * Function to all data showing in dataTable
     *
     * @param RiskProfilesSearchRequest $riskInstructionsSearchRequest
     * @param RiskProfileSearch $riskProfileSearch
     * @return JsonResponse
     */
    public function table(RiskProfilesSearchRequest $riskInstructionsSearchRequest, RiskProfileSearch $riskProfileSearch)
    {
        $data = $this->dataTableAll($riskInstructionsSearchRequest, $riskProfileSearch);

        return response()->json($data);
    }

    /**
     * Function to show create form
     *
     * @return View
     */
    public function create()
    {
        $instructions = Instructions::select('id', 'code')->checkUserHasRole()->active()->get();

        return view('risk-profiles.edit')->with([
            'saveMode' => 'add',
            'documents' => ConstructorDocument::getConstructorDocumentForAgency(),
            'instructions' => $instructions
        ]);
    }

    /**
     *  Function to data into database
     *
     * @param RiskProfilesRequest $request
     * @return JsonResponse
     */
    public function store(RiskProfilesRequest $request)
    {
        $riskProfile = $this->manager->store($request->validated());

        return responseResult('OK', null, ['id' => $riskProfile->id]);
    }

    /**
     * Function to edit page by current id
     *
     * @param $lngCode
     * @param $id
     * @param $viewType
     * @return View
     */
    public function edit($lngCode, $id, $viewType)
    {
        $riskProfile = RiskProfile::where('id', $id)->checkUserHasRole()->firstOrFail();
        $instructions = Instructions::select('id', 'code')->checkUserHasRole()->active()->get();

        $attachedInstructions = [];
        foreach ($riskProfile->instructions as $instruction) {
            $attachedInstructions[] = $instruction->instruction_code;
        }

        return view('risk-profiles.edit')->with([
            'documents' => ConstructorDocument::getConstructorDocumentForAgency(),
            'riskProfile' => $riskProfile,
            'attachedInstructions' => $attachedInstructions,
            'instructions' => $instructions,
            'saveMode' => $viewType,
        ]);
    }

    /**
     * Function to update data
     *
     * @param RiskProfilesRequest $request
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     */
    public function update(RiskProfilesRequest $request, $lngCode, $id)
    {
        $this->manager->update($request->validated(), $id);

        return responseResult('OK');
    }

}
