<?php

namespace App\Http\Controllers\Risks;

use App\Http\Controllers\BaseController;
use App\Http\Requests\RiskInstructions\RiskInstructionsRequest;
use App\Http\Requests\RiskInstructions\RiskInstructionsRequestSearchRequest;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\Instructions\Instructions;
use App\Models\Instructions\InstructionsManager;
use App\Models\Instructions\InstructionsSearch;
use App\Models\ReferenceTable\ReferenceTable;
use Illuminate\Http\JsonResponse;
use Illuminate\View\View;

/**
 * Class RiskInstructionsController
 * @package App\Http\Controllers\Risks
 */
class RiskInstructionsController extends BaseController
{
    /**
     * @var InstructionsManager
     */
    protected $manager;

    /**
     * RiskInstructionsController constructor.
     *
     * @param InstructionsManager $manager
     */
    public function __construct(InstructionsManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Function to show index page
     *
     * @return View
     */
    public function index()
    {
        return view('risk-instructions.index')->with([
            'constructorDocuments' => ConstructorDocument::getConstructorDocumentForAgency()
        ]);
    }

    /**
     * Function to all data showing in dataTable
     *
     * @param RiskInstructionsRequestSearchRequest $riskInstructionsSearchRequest
     * @param InstructionsSearch $instructionsSearch
     * @return JsonResponse
     */
    public function table(RiskInstructionsRequestSearchRequest $riskInstructionsSearchRequest, InstructionsSearch $instructionsSearch)
    {
        $data = $this->dataTableAll($riskInstructionsSearchRequest, $instructionsSearch);

        return response()->json($data);
    }

    /**
     * Function to show create form
     *
     * @return View
     */
    public function create()
    {
        $this->viewConstVariables();

        return view('risk-instructions.edit')->with([
            'saveMode' => 'add',
            'documents' => ConstructorDocument::getConstructorDocumentForAgency(),
        ]);
    }

    /**
     *  Function to data into database
     *
     * @param RiskInstructionsRequest $request
     * @return JsonResponse
     */
    public function store(RiskInstructionsRequest $request)
    {
        $instructions = $this->manager->store($request->validated());

        return responseResult('OK', null, ['id' => $instructions->id]);
    }

    /**
     * Function to edit page by current id
     *
     * @param $lngCode
     * @param $id
     * @param $viewType
     * @return View
     */
    public function edit($lngCode, $id, $viewType)
    {
        $riskInstruction = Instructions::where('id', $id)->checkUserHasRole()->firstOrFail();

        //
        $this->viewConstVariables();

        //
        $attachedFeedback = [];
        foreach ($riskInstruction->feedbacks as $feedback) {
            $attachedFeedback[] = $feedback->code;
        }

        return view('risk-instructions.edit')->with([
            'documents' => ConstructorDocument::getConstructorDocumentForAgency(),
            'riskInstruction' => $riskInstruction,
            'attachedFeedback' => $attachedFeedback,
            'saveMode' => $viewType,
        ]);
    }

    /**
     * Function to update data
     *
     * @param RiskInstructionsRequest $request
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     */
    public function update(RiskInstructionsRequest $request, $lngCode, $id)
    {
        $this->manager->update($request->validated(), $id);

        return responseResult('OK');
    }

    /**
     * Function to pass variables to view
     *
     * @return void
     */
    private function viewConstVariables()
    {
        view()->composer(['risk-instructions.edit'], function ($view) {

            $agency = getReferenceRows(ReferenceTable::REFERENCE_AGENCY, false, false, [], 'first', false, false, ['code' => Auth()->user()->companyTaxId()]);

            $where = ['agency' => $agency->id];
            $types = getReferenceRows(ReferenceTable::REFERENCE_RISK_INSTRUCTION_TYPE, false, false, [], 'get', false, false, $where);
            $feedbacks = getReferenceRows(ReferenceTable::REFERENCE_RISK_FEEDBACK, false, false, [], 'get', false, false, $where);

            $view->with([
                'types' => $types,
                'feedbacks' => $feedbacks
            ]);
        });
    }
}
