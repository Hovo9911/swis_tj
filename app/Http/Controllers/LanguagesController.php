<?php

namespace App\Http\Controllers;

use App\Http\Requests\Language\LanguageRequest;
use App\Models\Languages\Language;
use App\Models\Languages\LanguageManager;
use App\Models\Languages\LanguageSearch;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class LanguagesController
 * @package App\Http\Controllers
 */
class LanguagesController extends BaseController
{
    /**
     * @var LanguageManager
     */
    protected $manager;

    /**
     * LanguagesController constructor.
     * @param LanguageManager $manager
     */
    public function __construct(LanguageManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @return View
     */
    public function index()
    {
        return view('languages.index')->with([
            'activeLng' => json_encode(Language::getActiveLng())
        ]);
    }

    /**
     * Function to all data showing in datatable
     *
     * @param Request $request
     * @param LanguageSearch $languageSearch
     * @return JsonResponse
     */
    public function table(Request $request, LanguageSearch $languageSearch)
    {
        $data = $this->dataTableAll($request, $languageSearch);

        return response()->json($data);
    }

    /**
     * @return View
     */
    public function create()
    {
        return view('languages.edit')->with([
            'saveMode' => 'add'
        ]);
    }

    /**
     * Function to store validated data
     *
     * @param LanguageRequest $request
     * @return JsonResponse
     */
    public function store(LanguageRequest $request)
    {
        $this->manager->store($request->validated());

        return responseResult('OK');
    }

    /**
     * Show current by id
     *
     * @param $lngCode
     * @param $id
     * @param $viewType
     * @return View
     */
    public function edit($lngCode, $id, $viewType)
    {
        $language = Language::where('id', $id)->firstOrFail();

        return view('languages.edit')->with([
            'language' => $language,
            'saveMode' => $viewType
        ]);
    }

    /**
     * Update data
     *
     * @param LanguageRequest $request
     * @param $id
     * @param $lngCode
     * @return JsonResponse
     */
    public function update(LanguageRequest $request, $lngCode, $id)
    {
        $this->manager->update($request->validated(), $id);

        return responseResult('OK');
    }

    /**
     * Delete by id(array)
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function delete(Request $request)
    {
        $this->manager->delete($request->input('deleteItems'));

        return responseResult('OK');
    }
}
