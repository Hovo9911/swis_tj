<?php

namespace App\Http\Controllers\Constructor;

use App\Http\Controllers\BaseController;
use App\Http\Requests\ConstructorStates\ConstructorStatesRequest;
use App\Http\Requests\ConstructorStates\ConstructorStatesSearchRequest;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\ConstructorStates\ConstructorStatesManager;
use App\Models\ConstructorStates\ConstructorStatesSearch;
use Illuminate\Http\JsonResponse;
use Illuminate\View\View;

/**
 * Class ConstructorStatesController
 * @package App\Http\Controllers\Constructor
 */
class ConstructorStatesController extends BaseController
{
    /**
     * @var ConstructorStatesManager
     */
    protected $manager;

    /**
     * ConstructorStatesController constructor.
     *
     * @param ConstructorStatesManager $manager
     */
    public function __construct(ConstructorStatesManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Function to show index page
     *
     * @param $lngCode
     * @param null $selectedConstructorDocumentId
     * @return View
     */
    public function index($lngCode, $selectedConstructorDocumentId = null)
    {
        return view('constructor-state.index')->with([
            'constructorDocuments' => ConstructorDocument::getConstructorDocumentForAgency(),
            'selectedConstructorDocumentId' => $selectedConstructorDocumentId
        ]);
    }

    /**
     * Function to show data in dataTable
     *
     * @param ConstructorStatesSearchRequest $constructorStatesSearchRequest
     * @param ConstructorStatesSearch $constructorStateSearch
     * @return JsonResponse
     */
    public function table(ConstructorStatesSearchRequest $constructorStatesSearchRequest, ConstructorStatesSearch $constructorStateSearch)
    {
        $data = $this->dataTableAll($constructorStatesSearchRequest, $constructorStateSearch);

        return response()->json($data);
    }

    /**
     * Function to show create page
     *
     * @param $lngCode
     * @param $selectedConstructorDocumentId
     * @return View
     */
    public function create($lngCode, $selectedConstructorDocumentId)
    {
        return view('constructor-state.edit')->with([
            'selectedConstructorDocumentId' => $selectedConstructorDocumentId,
            'constructorDocuments' => ConstructorDocument::getConstructorDocumentForAgency(),
            'saveMode' => 'add',
        ]);
    }

    /**
     * Function to store validated data
     *
     * @param ConstructorStatesRequest $request
     * @return JsonResponse
     */
    public function store(ConstructorStatesRequest $request)
    {
        $constructorStates = $this->manager->store($request->validated());

        return responseResult('OK', null, ['id' => $constructorStates->id]);
    }

    /**
     * Function to Show current by id
     *
     * @param $lngCode
     * @param $id
     * @param $viewType
     * @return View
     */
    public function edit($lngCode, $id, $viewType)
    {
        $constructorState = ConstructorStates::where('id', $id)->constructorStatesOwner()->firstOrFail();

        return view('constructor-state.edit')->with([
            'constructorState' => $constructorState,
            'saveMode' => $viewType,
            'constructorDocuments' => ConstructorDocument::getConstructorDocumentForAgency()
        ]);
    }

    /**
     * Function to Update data
     *
     * @param ConstructorStatesRequest $request
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     */
    public function update(ConstructorStatesRequest $request, $lngCode, $id)
    {
        $this->manager->update($request->validated(), $id);

        return responseResult('OK');
    }

}
