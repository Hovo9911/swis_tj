<?php

namespace App\Http\Controllers\Constructor;

use App\Http\Controllers\BaseController;
use App\Http\Requests\ConstructorMapping\ConstructorMappingRequest;
use App\Http\Requests\ConstructorMapping\ConstructorMappingSearchRequest;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorLists\ConstructorLists;
use App\Models\ConstructorMapping\ConstructorMapping;
use App\Models\ConstructorMapping\ConstructorMappingManager;
use App\Models\ConstructorMapping\ConstructorMappingSearch;
use App\Models\ConstructorRules\ConstructorRules;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\NotificationTemplates\NotificationTemplates;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

/**
 * Class ConstructorMappingController
 * @package App\Http\Controllers\Constructor
 */
class ConstructorMappingController extends BaseController
{
    /**
     * @var ConstructorMappingManager
     */
    protected $manager;

    /**
     * ConstructorMappingController constructor.
     *
     * @param ConstructorMappingManager $manager
     */
    public function __construct(ConstructorMappingManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Function to return mapping index page
     *
     * @param $lngCode
     * @param null $selectedConstructorDocumentId
     * @return View
     */
    public function index($lngCode, $selectedConstructorDocumentId = null)
    {
        return view('constructor-mapping.index')->with([
            'constructorDocuments' => ConstructorDocument::getConstructorDocumentForAgency(),
            'selectedConstructorDocumentId' => $selectedConstructorDocumentId
        ]);
    }

    /**
     * Function to showing all data in dataTable
     *
     * @param ConstructorMappingSearchRequest $constructorMappingSearchRequest
     * @param ConstructorMappingSearch $constructorMappingSearch
     * @return JsonResponse
     */
    public function table(ConstructorMappingSearchRequest $constructorMappingSearchRequest, ConstructorMappingSearch $constructorMappingSearch)
    {
        $data = $this->dataTableAll($constructorMappingSearchRequest, $constructorMappingSearch);

        return response()->json($data);
    }

    /**
     * Function to return create view
     * @param $lngCode
     * @param $selectedConstructorDocumentId
     * @return View
     */
    public function create($lngCode, $selectedConstructorDocumentId)
    {
        $notesTemplate = NotificationTemplates::select('id', 'name')->notificationTemplateOwner()->get();

        return view('constructor-mapping.edit')->with([
            'selectedConstructorDocumentId' => $selectedConstructorDocumentId,
            'constructorDocuments' => ConstructorDocument::getConstructorDocumentForAgency(),
            'lists' => ConstructorLists::generateListsArray($selectedConstructorDocumentId),
            'fixRoles' => ConstructorMapping::getFixRoles(),
            'notesTemplate' => $notesTemplate,
            'rules' => ConstructorRules::getRules($selectedConstructorDocumentId),
            'saveMode' => 'add',
        ]);
    }

    /**
     * Function to store data
     *
     * @param ConstructorMappingRequest $request
     * @return JsonResponse
     */
    public function store(ConstructorMappingRequest $request)
    {
        $data = $request->validated();

        if ($checkMapping = $this->checkMappingStartEndStates($data['start_state'], $data['end_state'])) {
            return responseResult('INVALID_DATA', '', '', $checkMapping);
        }

        $constructorMapping = $this->manager->store($data);

        return responseResult('OK', null, ['id' => $constructorMapping->id]);
    }

    /**
     * Function to Show current by id
     *
     * @param $lngCode
     * @param $id
     * @param $viewType
     * @return View
     */
    public function edit($lngCode, $id, $viewType)
    {
        $constructorMapping = ConstructorMapping::where('id', $id)->constructorMappingOwner()->firstOrFail();
        $notesTemplate = NotificationTemplates::select('id', 'name')->notificationTemplateOwner()->get();

        return view('constructor-mapping.edit')->with([
            'constructorMapping' => $constructorMapping,
            'constructorDocuments' => ConstructorDocument::getConstructorDocumentForAgency(),
            'lists' => ConstructorLists::generateListsArray($constructorMapping->document_id),
            'fixRoles' => ConstructorMapping::getFixRoles(),
            'notesTemplate' => $notesTemplate,
            'rules' => ConstructorRules::getRules($constructorMapping->document_id, $constructorMapping->id),
            'saveMode' => $viewType,
        ]);
    }

    /**
     * Function to Update data
     *
     * @param ConstructorMappingRequest $request
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     */
    public function update(ConstructorMappingRequest $request, $lngCode, $id)
    {
        $data = $request->validated();

        if ($checkMapping = $this->checkMappingStartEndStates($data['start_state'], $data['end_state'])) {
            return responseResult('INVALID_DATA', '', '', $checkMapping);
        }

        $this->manager->update($data, $id);

        $user = Auth::user();
        $companyTaxId = $user->companyTaxId();
        $path = storage_path();
        $data = json_encode($data);
        $message = $str = <<<EOF

page === constructor-mapping
user === $user->first_name $user->last_name
company === $companyTaxId
action === update

data === $data

EOF;
        file_put_contents("{$path}/logs/constructor_debug_log.txt", $message, FILE_APPEND);

        return responseResult('OK', urlWithLng("constructor-mapping/$id/edit"));
    }

    /**
     * Function to Check if state type=request  mapping startState equal endState or not
     *
     * @param $start_state_id
     * @param $end_state_id
     * @return bool|array
     */
    private function checkMappingStartEndStates($start_state_id, $end_state_id)
    {
        $startState = ConstructorStates::where('id', $start_state_id)->firstOrFail();
        $endState = ConstructorStates::where('id', $end_state_id)->firstOrFail();

        if ($startState->state_type == ConstructorStates::REQUEST_STATE_TYPE && $startState->id != $endState->id) {
            $errors['end_state'] = trans('swis.constructor_states.request_type_not_valid');

            return $errors;
        }

        return false;
    }
}
