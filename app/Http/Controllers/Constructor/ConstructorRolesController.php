<?php

namespace App\Http\Controllers\Constructor;

use App\Http\Controllers\BaseController;
use App\Http\Requests\ConstructorRoles\ConstructorRolesRequest;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorDocumentsRoles\ConstructorDocumentsRoles;
use App\Models\ConstructorRoles\ConstructorRolesManager;
use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm;
use App\Models\DocumentsDatasetFromSaf\DocumentsDatasetFromSaf;
use App\Models\Role\Role;
use App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class ConstructorRolesController
 * @package App\Http\Controllers\Constructor
 */
class ConstructorRolesController extends BaseController
{
    /**
     * @var ConstructorRolesManager
     */
    protected $manager;

    /**
     * ConstructorRolesController constructor.
     *
     * @param ConstructorRolesManager $manager
     */
    public function __construct(ConstructorRolesManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Function to return view with data
     *
     * @param Request $request
     * @return View|RedirectResponse
     */
    public function index(Request $request)
    {
        $selectedConstructorDocumentId = $request->get('documentID');

        if ($selectedConstructorDocumentId) {
            ConstructorDocument::where('id', $selectedConstructorDocumentId)->firstOrFail();
        }

        $roles = [];
        $forbiddenIDs = [];
        $getAttributes = $this->getAttributeFields($selectedConstructorDocumentId);
        $getRoles = ConstructorDocumentsRoles::select('constructor_documents_roles.id', 'constructor_documents_roles.role_id', 'role_ml.name')
            ->where('document_id', $selectedConstructorDocumentId)
            ->where('constructor_documents_roles.role_id', '!=', Role::HEAD_OF_COMPANY)
            ->join('roles', 'roles.id', '=', 'constructor_documents_roles.role_id')
            ->join('role_ml', 'roles.id', '=', 'role_ml.role_id')
            ->where('role_ml.lng_id', '=', cLng('id'))
            ->get();

        foreach ($getRoles as $role) {
            $forbiddenIDs[] = $role->role_id;
            $attributes = [];
            $getAttr = $role->attributes;

            if (is_object($getAttr)) {
                foreach ($getAttr as $attr) {
                    $attributes[$attr->field_id] = false;
                }

                foreach ($getAttributes as $attrID => $attr) {
                    $attributes[$attrID] = [
                        'type' => $attr['type'],
                        'name' => $attr['name'],
                        'path' => getSafAttrPath($attr['path']),
                        'check' => isset($attributes[$attrID]),
                    ];
                }
            }

            $roles[] = [
                'name' => $role->name,
                'roleID' => $role->role_id,
                'attributes' => $attributes
            ];
        }

        return view('constructor-role.index')->with([
            'selectedConstructorDocumentId' => $selectedConstructorDocumentId,
            'constructorDocuments' => ConstructorDocument::getConstructorDocumentForAgency(),
            'roles' => $roles,
            'forbiddenIDs' => json_encode($forbiddenIDs)
        ]);
    }

    /**
     * Function to store data
     *
     * @param ConstructorRolesRequest $request
     * @return JsonResponse
     */
    public function store(ConstructorRolesRequest $request)
    {
        $this->manager->store($request->validated());

        return responseResult('OK', urlWithLng('constructor-roles?documentID=' . $request->get('documentID')));
    }

    /**
     * Function to return agency roles list
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function roles(Request $request)
    {
        $this->validate($request, [
            'q' => 'required',
            'documentID' => 'required|integer_with_max|exists:constructor_documents,id',
        ]);

        ConstructorDocument::select('id')->where('id', $request->input('documentID'))->constructorDocumentOwner()->firstOrFail();

        $values = [];
        $forbiddenIDs = is_null($request->input('forbiddenIDs')) ? [] : json_decode($request->input('forbiddenIDs'), true);

        $document = ConstructorDocument::select('company_tax_id')->find($request->input('documentID'));
        if ($document) {
            $roles = Role::select('id', 'role_ml.name')->where('company_tax_id', $document->company_tax_id)
                ->join('role_ml', 'roles.id', '=', 'role_ml.role_id')
                ->where('role_ml.lng_id', '=', cLng('id'))
                ->where('role_ml.name', 'ILIKE', "%{$request->input('q')}%")
                ->whereNotIn('id', $forbiddenIDs)
                ->active()
                ->get();

            foreach ($roles as $role) {
                $values['items'][] = [
                    'id' => $role->id,
                    'name' => "({$role->id}) {$role->name}"
                ];
            }
        }

        return response()->json($values);
    }

    /**
     * Function to get html for each role
     *
     * @param Request $request
     * @return array
     */
    public function getRoleField(Request $request)
    {
        $this->validate($request, [
            'roleID' => 'required|integer_with_max|exists:roles,id',
            'documentID' => 'required|integer_with_max|exists:constructor_documents,id'
        ]);

        ConstructorDocument::select('id')->where('id', $request->input('documentID'))->constructorDocumentOwner()->firstOrFail();

        $returnFields = $this->getAttributeFields($request->input('documentID'));
        $role = Role::select('id', 'name')->find($request->input('roleID'));

        $attributeText = trans('swis.attributes_title');
        $returnHtml = "<div class='col-md-3 text-left' id='block-{$role->id}'>";
        $returnHtml .= "<h3 class='text-center'>{$role->name} {$attributeText} <i class='far fa-times-circle remove-block' style='cursor: pointer;' data-id='{$role->id}'></i></h3>";
        $returnHtml .= "<table class='table table-striped table-bordered table-hover dataTables-example'>";
        $returnHtml .= "<thead>";
        $returnHtml .= "<th></th>";
        $returnHtml .= "<th>Name</th>";
        $returnHtml .= "<th>Path</th>";
        $returnHtml .= "</thead>";
        $returnHtml .= "<tbody>";
        foreach ($returnFields as $itemID => $field) {
            $type = $field['type'];
            $attrField = getSafAttrPath($field['path']);

            $returnHtml .= "<tr>";
            $returnHtml .= "<td><input type='checkbox' id='checkbox-{$role->id}-{$itemID}' name='attributes[{$role->id}][{$type}][{$itemID}]'></td>";
            $returnHtml .= "<td><label for='checkbox-{$role->id}-{$itemID}'> {$field['name']} </label></td>";
            $returnHtml .= "<td><label for='checkbox-{$role->id}-{$itemID}'> {$attrField} </label></td>";
            $returnHtml .= "</tr>";
        }
        $returnHtml .= "</tbody>";
        $returnHtml .= "<input type='hidden' name='roles[]' value='{$role->id}' />";
        $returnHtml .= "</div>";

        return [
            'roleID' => $role->id,
            'html' => $returnHtml
        ];
    }

    /**
     * Function to return attributes for document
     *
     * @param $documentID
     * @return array
     */
    private function getAttributeFields($documentID)
    {
        if ($documentID) {
            ConstructorDocument::select('id')->where('id', $documentID)->constructorDocumentOwner()->firstOrFail();
        }

        $returnFields = [];
        $dataSetSaf = DocumentsDatasetFromSaf::where('document_id', $documentID)->where('is_attribute', true)->get();
        $dataSetMdm = DocumentsDatasetFromMdm::select('documents_dataset_from_mdm.*', 'documents_dataset_from_mdm_ml.label')
            ->leftJoin('documents_dataset_from_mdm_ml', 'documents_dataset_from_mdm_ml.documents_dataset_from_mdm_id', '=', 'documents_dataset_from_mdm.id')
            ->where('lng_id', cLng('id'))
            ->where('document_id', $documentID)
            ->where('is_attribute', true)
            ->get();

        $lastXml = SingleApplicationDataStructure::lastXml();
        foreach ($dataSetSaf as $item) {
            $xmlAttr = $lastXml->xpath("//*[@id='{$item->saf_field_id}']");
            $attributes = $xmlAttr[0]->attributes();
            $returnFields[$item->saf_field_id] = [
                'type' => 'saf',
                'path' => $item->group,
                'name' => isset($attributes, $attributes->title) ? trans($attributes->title) : ''
            ];
        }

        foreach ($dataSetMdm as $item) {
            $returnFields[$item->field_id] = [
                'type' => 'mdm',
                'path' => $item->group,
                'name' => $item->label
            ];
        }

        return $returnFields;
    }
}
