<?php

namespace App\Http\Controllers\Constructor;

use App\Http\Controllers\BaseController;
use App\Http\Requests\ConstructorDocument\ConstructorDocumentRequest;
use App\Http\Requests\ConstructorDocument\ConstructorDocumentSearchRequest;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorDocument\ConstructorDocumentManager;
use App\Models\ConstructorDocument\ConstructorDocumentSearch;
use App\Models\DataModel\DataModel;
use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm;
use App\Models\ReferenceTable\ReferenceTable;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\View\View;

/**
 * Class ConstructorDocumentController
 * @package App\Http\Controllers\Constructor
 */
class ConstructorDocumentController extends BaseController
{
    /**
     * @var ConstructorDocumentManager
     */
    protected $manager;

    /**
     * ConstructorDocumentController constructor.
     *
     * @param ConstructorDocumentManager $manager
     */
    public function __construct(ConstructorDocumentManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Function to show index page
     *
     * @return View
     */
    public function index()
    {
        return view('constructor-document.index');
    }

    /**
     * Function to show all data in dataTable
     *
     * @param ConstructorDocumentSearchRequest $constructorDocumentSearchRequest
     * @param ConstructorDocumentSearch $constructorSearch
     * @return JsonResponse
     */
    public function table(ConstructorDocumentSearchRequest $constructorDocumentSearchRequest, ConstructorDocumentSearch $constructorSearch)
    {
        $data = $this->dataTableAll($constructorDocumentSearchRequest, $constructorSearch);

        return response()->json($data);
    }

    /**
     * Function to show create page
     *
     * @return View
     */
    public function create()
    {
        return view('constructor-document.edit')->with([
            'saveMode' => 'add',
            'typeOfDocuments' => $this->getTypeOfDocuments()
        ]);
    }

    /**
     * Function to Store Data
     *
     * @param ConstructorDocumentRequest $request
     * @return JsonResponse
     */
    public function store(ConstructorDocumentRequest $request)
    {
        $constructor = $this->manager->store($request->validated());

        return responseResult('OK', null, ['id' => $constructor->id]);
    }

    /**
     * Function to edit page by current id
     *
     * @param $lngCode
     * @param $id
     * @return View
     */
    public function edit($lngCode, $id)
    {
        $constructorDocument = ConstructorDocument::where('id', $id)->constructorDocumentOwner()->firstOrFail();

        return view('constructor-document.edit')->with([
            'constructorDocument' => $constructorDocument,
            'typeOfDocuments' => $this->getTypeOfDocuments(),
            'saveMode' => 'edit'
        ]);
    }

    /**
     * Function to update data
     *
     * @param ConstructorDocumentRequest $request
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     */
    public function update(ConstructorDocumentRequest $request, $lngCode, $id)
    {
        $this->manager->update($request->validated(), $id);

        return responseResult('OK');
    }

    /**
     * Function to get current agency constructor documents
     *
     * @return Collection
     */
    public function getTypeOfDocuments()
    {
        $currentAgencyRefRows = DB::table(ReferenceTable::REFERENCE_AGENCY)->where('code', Auth::user()->companyTaxId())->where('show_status', ReferenceTable::STATUS_ACTIVE)->pluck('id');

        return getReferenceRows(ReferenceTable::REFERENCE_DOCUMENT_TYPE_REFERENCE, false, false, ['id', 'code', 'name', 'according_agency'])->whereIn('according_agency', $currentAgencyRefRows);
    }

    /**
     * Function to Export CVS file by table structure
     *
     * @param $lngCode
     * @param $documentId
     * @return mixed
     */
    public function export($lngCode, $documentId)
    {
        $headers = array(
            "Content-Encoding" => "utf-8",
            "Content-type" => "text/csv; charset=UTF-8",
            "Content-Disposition" => "attachment; filename=file.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $mdmFieldData = DocumentsDatasetFromMdm::mdmFieldsForExport($documentId);
        $languages = activeLanguages();
        $columns = array('MDM ID', 'MDM Name');
        foreach ($languages as $language) {
            array_push($columns, 'MDM description_' . $language->code);
        }
        array_push($columns, 'documents_dataset_from_mdm_id', 'lng_id', 'label', 'tooltip');

        $callback = function () use ($mdmFieldData, $columns) {
            $file = fopen('php://output', 'w');
            fprintf($file, chr(0xEF) . chr(0xBB) . chr(0xBF));
            fputcsv($file, $columns);

            foreach ($mdmFieldData as $mdmField) {

                $dataModel = new DataModel();

                $descriptions = $dataModel->getDescription($mdmField->id);

                $exportArray = array(
                    $mdmField->id,
                    $mdmField->name
                );

                if (is_array($descriptions) && count($descriptions)) {
                    foreach ($descriptions as $dataModelDescription) {
                        array_push($exportArray, $dataModelDescription->description);
                    }
                }

                array_push($exportArray,
                    $mdmField->documents_dataset_from_mdm_id,
                    $mdmField->lng_id,
                    $mdmField->label,
                    $mdmField->tooltip
                );

                fputcsv($file, $exportArray);
            }
            fclose($file);
        };

        return Response::stream($callback, 200, $headers);
    }
}
