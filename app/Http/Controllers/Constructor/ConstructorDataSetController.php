<?php

namespace App\Http\Controllers\Constructor;

use App\Http\Controllers\BaseController;
use App\Http\Requests\ConstructorDataSet\ConstructorDataSetRequest;
use App\Models\ConstructorDataSet\ConstructorDataSet;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\DataModel\DataModel;
use App\Models\DataModel\DataModelManager;
use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm;
use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdmManager;
use App\Models\DocumentsDatasetFromSaf\DocumentsDatasetFromSaf;
use App\Models\DocumentsDatasetFromSaf\DocumentsDatasetFromSafManager;
use App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Throwable;

/**
 * Class ConstructorDataSetController
 * @package App\Http\Controllers\Constructor
 */
class ConstructorDataSetController extends BaseController
{
    /**
     * @var DocumentsDatasetFromSafManager
     */
    protected $managerSAF;

    /**
     * @var DocumentsDatasetFromMdmManager
     */
    protected $managerMDM;

    /**
     * @var DataModelManager
     */
    protected $dataModelManager;

    /**
     * ConstructorDataSetController constructor.
     *
     * @param DocumentsDatasetFromSafManager $managerSAF
     * @param DocumentsDatasetFromMdmManager $managerMDM
     * @param DataModelManager $dataModelManager
     */
    public function __construct(DocumentsDatasetFromSafManager $managerSAF, DocumentsDatasetFromMdmManager $managerMDM, DataModelManager $dataModelManager)
    {
        $this->managerSAF = $managerSAF;
        $this->managerMDM = $managerMDM;
        $this->dataModelManager = $dataModelManager;
    }

    /**
     * Function to show index page
     *
     * @param Request $request
     * @return RedirectResponse|View
     */
    public function index(Request $request)
    {
        $selectedConstructorDocumentId = $request->get('documentID');

        if ($selectedConstructorDocumentId) {
            ConstructorDocument::select('id')->where('id', $selectedConstructorDocumentId)->constructorDocumentOwner()->firstOrFail();
        }

        $hiddenFields = $previousDataForDocuments = $safFields = [];

        $getPreviousDataForDocumentsSaf = DocumentsDatasetFromSaf::where('document_id', $selectedConstructorDocumentId)->with('ml')->get();
        $getPreviousDataForDocumentsMdm = DocumentsDatasetFromMdm::where('document_id', $selectedConstructorDocumentId)->with('ml')->orderByAsc('sort_order_id')->get();

        $mdmData = DataModel::select('id', 'name', 'definition', 'reference')->whereIn('id', $getPreviousDataForDocumentsMdm->pluck('mdm_field_id')->all())
            ->joinMl(['f_k' => 'data_model_id'])
            ->get()->keyBy('id')
            ->toArray();

        foreach ($getPreviousDataForDocumentsSaf as $item) {

            $previousDataForDocuments['saf'][$item->saf_field_id] = [
                'is_attribute' => $item->is_attribute,
                'is_hidden' => $item->is_hidden,
                'is_search_param' => $item->is_search_param,
                'show_in_search_results' => $item->show_in_search_results,
                'is_unique' => $item->is_unique
            ];

            foreach ($item->ml as $safLngField) {
                $previousDataForDocuments['saf'][$item->saf_field_id]['field_label'][$safLngField->lng_id] = $safLngField->label;
                $previousDataForDocuments['saf'][$item->saf_field_id]['tooltip'][$safLngField->lng_id] = $safLngField->tooltip;
            }
        }
        $getPreviousDataForDocumentsMdm = $getPreviousDataForDocumentsMdm->toArray();

        foreach ($getPreviousDataForDocumentsMdm as &$item) {
            $hiddenFields[] = $item['mdm_field_id'];
            $item['name'] = (isset($mdmData[$item['mdm_field_id']])) ? $mdmData[$item['mdm_field_id']]['name'] : '';
            $item['description'] = (isset($mdmData[$item['mdm_field_id']])) ? $mdmData[$item['mdm_field_id']]['definition'] : '';
            $item['reference'] = (isset($mdmData[$item['mdm_field_id']]) && !is_null($mdmData[$item['mdm_field_id']]['reference'])) ? '' : 'disabled';

            foreach ($item['ml'] as $lngField) {
                $item['field_label'][$lngField['lng_id']] = $lngField['label'];
                $item['tooltip'][$lngField['lng_id']] = $lngField['tooltip'];
            }
        }

        $lastXML = SingleApplicationDataStructure::lastXml();
        $xmlTabs = $lastXML->xpath('/tabs/tab');

        if ($xmlTabs && count($xmlTabs) > 0) {
            foreach ($xmlTabs as $tab) {
                if (isset($tab->block, $tab->block->fieldset)) {
                    if (count($tab->block) > 1) {
                        foreach ($tab->block as $block) {
                            if (isset($block->fieldset)) {
                                foreach ($block->fieldset as $fieldSet) {
                                    $safFields = ConstructorDataSet::returnSafFields($tab, $block, $fieldSet, $safFields, 'block');
                                }
                            }

                            if (isset($block->subBlock, $block->subBlock->fieldset)) {
                                foreach ($block->subBlock->fieldset as $fieldSet) {
                                    $safFields = ConstructorDataSet::returnSafFields($tab, $block, $fieldSet, $safFields, 'subBlock');
                                }
                            }
                        }
                    } else {
                        foreach ($tab->block->fieldset as $fieldSet) {
                            $safFields = ConstructorDataSet::returnSafFields($tab, $tab->block, $fieldSet, $safFields, 'tabBlock');
                        }
                    }

                } elseif (isset($tab->typeBlock)) {
                    foreach ($tab->typeBlock as $typeBlock) {
                        if (isset($typeBlock->block)) {
                            foreach ($typeBlock->block as $block) {
                                if (isset($block->fieldset)) {
                                    foreach ($block->fieldset as $fieldSet) {
                                        $safFields = ConstructorDataSet::returnSafFields($tab, $block, $fieldSet, $safFields, 'typeBlock', $typeBlock);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return view('constructor-data-set.index')->with([
            'selectedConstructorDocumentId' => $selectedConstructorDocumentId,
            'constructorDocuments' => ConstructorDocument::getConstructorDocumentForAgency(),
            'safFields' => $safFields,
            'groupPath' => ConstructorDataSet::getGroupPaths()['groupPath'],
            'hiddenFields' => json_encode($hiddenFields),
            'previousDataForDocuments' => $previousDataForDocuments,
            'getPreviousDataForDocumentsMdm' => $getPreviousDataForDocumentsMdm
        ]);
    }

    /**
     * Function to store Constructor data set
     *
     * @param ConstructorDataSetRequest $request
     * @return RedirectResponse|JsonResponse
     */
    public function store(ConstructorDataSetRequest $request)
    {
        DB::transaction(function () use ($request) {
            $this->managerSAF->store($request->all());
            $this->managerMDM->store($request->all());
        });

        return responseResult('OK', urlWithLng("constructor-data-set?documentID={$request->get('documentID')}"), [], []);
    }

    /**
     * Function to return MDM field
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function search(Request $request)
    {
        $this->validate($request, [
            'q' => 'required|string_with_max'
        ]);

        $values = $this->dataModelManager->searchInDataModel($request->input('q'));

        return response()->json($values);
    }

    /**
     * Function to generate row for mdm field
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function getMDMField(Request $request)
    {
        $this->validate($request, [
            'mdmFieldID' => 'required|integer_with_max|exists:data_model_excel,id',
            'documentID' => 'required|integer_with_max|exists:constructor_documents,id',
            'lastIncrementedID' => 'nullable|integer_with_max'
        ]);

        $constructorDocument = ConstructorDocument::select('document_code')->where('id', $request->input('documentID'))->constructorDocumentOwner()->firstOrFail();

        $mdmField = DataModel::where('id', $request->input('mdmFieldID'))
            ->joinMl(['f_k' => 'data_model_id'])
            ->first();

        $getLastFieldID = DocumentsDatasetFromMdm::select('sort_order_id')->where('document_id', $request->input('documentID'))->orderBy('sort_order_id', 'desc')->first();
        $lastIncrementedID = isset($getLastFieldID) ? (int)$getLastFieldID->sort_order_id : 0;

        $lastIncrementedItemRequest = $request->input('lastIncrementedID');
        if (isset($lastIncrementedItemRequest)) {
            $lastIncrementedID = (int)$request->input('lastIncrementedID') + 1;
        } else {
            ++$lastIncrementedID;
        }

        $viewData = [
            'titleAttr' => trans('swis.constructor_data_set.is_attribute'),
            'titleHide' => trans('swis.constructor_data_set.hide_on_system'),
            'titleParam' => trans('swis.constructor_data_set.search_param'),
            'titleActive' => trans('swis.constructor_data_set.is_active'),
            'titleIsUnique' => trans('swis.constructor_data_set.is_unique'),
            'titleIncludeSearch' => trans('swis.constructor_data_set.include_search_result'),
            'titleDelete' => trans('core.base.action.delete'),
            'disabled' => (!is_null($mdmField->reference)) ? 'disabled' : ''
        ];

        $returnData['row'] = view('constructor-data-set.new-field')->with([
            'incrementedCode' => "{$constructorDocument->document_code}_{$lastIncrementedID}",
            'groupPath' => ConstructorDataSet::getGroupPaths(),
            'mdmField' => $mdmField,
            'viewData' => $viewData,
        ])->render();

        $returnData['mdmID'] = $mdmField->id;
        $returnData['lastIncrementedID'] = $lastIncrementedID;
        $returnData['field_id'] = $constructorDocument->document_code . '_' . $lastIncrementedID;

        return response()->json($returnData);
    }
}


