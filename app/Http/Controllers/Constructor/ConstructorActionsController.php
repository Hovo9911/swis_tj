<?php

namespace App\Http\Controllers\Constructor;

use App\Http\Controllers\BaseController;
use App\Http\Requests\ConstructorActions\ConstructorActionsRequest;
use App\Http\Requests\ConstructorActions\ConstructorActionsSearchRequest;
use App\Models\ConstructorActions\ConstructorActions;
use App\Models\ConstructorActions\ConstructorActionsManager;
use App\Models\ConstructorActions\ConstructorActionsSearch;
use App\Models\ConstructorActionToTemplate\ConstructorActionToTemplate;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorTemplates\ConstructorTemplates;
use Illuminate\Http\JsonResponse;
use Illuminate\View\View;

/**
 * Class ConstructorActionsController
 * @package App\Http\Controllers\Constructor
 */
class ConstructorActionsController extends BaseController
{
    /**
     * @var ConstructorActionsManager
     */
    protected $manager;

    /**
     * ConstructorActionsController constructor.
     *
     * @param ConstructorActionsManager $manager
     */
    public function __construct(ConstructorActionsManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Function to show index page
     *
     * @param $lngCode
     * @param null $selectedConstructorDocumentId
     * @return View
     */
    public function index($lngCode, $selectedConstructorDocumentId = null)
    {
        return view('constructor-action.index')->with([
            'constructorDocuments' => ConstructorDocument::getConstructorDocumentForAgency(),
            'selectedConstructorDocumentId' => $selectedConstructorDocumentId
        ]);
    }

    /**
     * Function to show all data in dataTable
     *
     * @param ConstructorActionsSearchRequest $constructorActionsSearchRequest
     * @param ConstructorActionsSearch $constructorActionSearch
     * @return JsonResponse
     */
    public function table(ConstructorActionsSearchRequest $constructorActionsSearchRequest, ConstructorActionsSearch $constructorActionSearch)
    {
        $data = $this->dataTableAll($constructorActionsSearchRequest, $constructorActionSearch);

        return response()->json($data);
    }

    /**
     * Function to show create form
     * @param $lngCode
     * @param $selectedConstructorDocumentId
     * @return View
     */
    public function create($lngCode, $selectedConstructorDocumentId)
    {
        $constructorTemplates = ConstructorTemplates::with('currentMl')->select('id')->constructorTemplateOwner()->active()->orderBy('name')->get();

        return view('constructor-action.edit')->with([
            'selectedConstructorDocumentId' => $selectedConstructorDocumentId,
            'constructorDocuments' => ConstructorDocument::getConstructorDocumentForAgency(),
            'specialTypes' => ConstructorActions::$specialTypes,
            'constructorTemplates' => $constructorTemplates,
            'saveMode' => 'add',
        ]);
    }

    /**
     * Function to Store Data
     *
     * @param ConstructorActionsRequest $request
     * @return JsonResponse
     */
    public function store(ConstructorActionsRequest $request)
    {
        $constructorActions = $this->manager->store($request->validated());

        return responseResult('OK', null, ['id' => $constructorActions->id]);
    }

    /**
     * Function to show edit page constructor action by id
     *
     * @param $lngCode
     * @param $id
     * @param $viewType
     * @return View
     */
    public function edit($lngCode, $id, $viewType)
    {
        $constructorAction = ConstructorActions::where('id', $id)->constructorActionOwner()->firstOrFail();
        $constructorTemplates = ConstructorTemplates::with('currentMl')->select('id')->constructorTemplateOwner()->active()->orderBy('name')->get();

        return view('constructor-action.edit')->with([
            'constructorAction' => $constructorAction,
            'constructorDocuments' => ConstructorDocument::getConstructorDocumentForAgency(),
            'specialTypes' => ConstructorActions::$specialTypes,
            'constructorTemplates' => $constructorTemplates,
            'checked' => ConstructorActionToTemplate::getAlreadyChecked($id),
            'saveMode' => $viewType,
        ]);
    }

    /**
     * Function to update data
     *
     * @param ConstructorActionsRequest $request
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     */
    public function update(ConstructorActionsRequest $request, $lngCode, $id)
    {
        $this->manager->update($request->validated(), $id);

        return responseResult('OK');
    }
}
