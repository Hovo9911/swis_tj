<?php

namespace App\Http\Controllers\Constructor;

use App\Http\Controllers\BaseController;
use App\Http\Requests\ConstructorRules\ConstructorRulesRequest;
use App\Http\Requests\ConstructorRules\ConstructorRulesSearchRequest;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorRules\ConstructorRules;
use App\Models\ConstructorRules\ConstructorRulesManager;
use App\Models\ConstructorRules\ConstructorRulesSearch;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

/**
 * Class ConstructorRulesController
 * @package App\Http\Controllers\Constructor
 */
class ConstructorRulesController extends BaseController
{
    /**
     * @var ConstructorRulesManager
     */
    protected $manager;

    /**
     * ConstructorRulesController constructor.
     *
     * @param ConstructorRulesManager $manager
     */
    public function __construct(ConstructorRulesManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Function to return rules index
     *
     * @param $lngCode
     * @param null $selectedConstructorDocumentId
     * @param null $type
     * @return RedirectResponse|View
     */
    public function index($lngCode, $selectedConstructorDocumentId = null, $type = null)
    {
        if (!is_null($type) && !in_array($type, ConstructorRules::ALL_RULE_TYPES)) {
            return redirect(urlWithLng("constructor-rules/{$selectedConstructorDocumentId}"));
        }

        return view('constructor-rules.index')->with([
            'selectedConstructorDocumentId' => $selectedConstructorDocumentId,
            'type' => $type,
            'constructorDocuments' => ConstructorDocument::getConstructorDocumentForAgency(),
            'constructorRuleTypes' => ConstructorRules::getTypes()
        ]);
    }

    /**
     * Function to return table info
     *
     * @param ConstructorRulesSearchRequest $constructorRulesSearchRequest
     * @param ConstructorRulesSearch $rulesSearch
     * @return JsonResponse
     */
    public function table(ConstructorRulesSearchRequest $constructorRulesSearchRequest, ConstructorRulesSearch $rulesSearch)
    {
        $data = $this->dataTableAll($constructorRulesSearchRequest, $rulesSearch);

        return response()->json($data);
    }

    /**
     * Function to return index page of constructor rules
     *
     * @param $lngCode
     * @param $constructorDocumentId
     * @param $type
     * @return View
     */
    public function create($lngCode, $constructorDocumentId, $type)
    {
        $obligationList = [];
        if (!is_null($type) && $type == ConstructorRules::OBLIGATION_TYPE) {
            $obligationList = ConstructorRules::getObligations();
        }

        return view("constructor-rules.edit")->with([
            'type' => $type,
            'obligationList' => $obligationList,
            'constructorDocumentId' => $constructorDocumentId,
            'saveMode' => 'add'
        ]);
    }

    /**
     * Function to validate and store new rule
     *
     * @param ConstructorRulesRequest $request
     * @param $lngCode
     * @param $constructorDocumentId
     * @param $type
     * @return JsonResponse
     */
    public function store(ConstructorRulesRequest $request, $lngCode, $constructorDocumentId, $type)
    {
        $constructorRules = $this->manager->store($request->validated());

        return responseResult('OK', urlWithLng("/constructor-rules/{$constructorDocumentId}/{$type}"), ['id' => $constructorRules->id ?? '']);
    }

    /**
     * Function to return Edit page
     *
     * @param $lngCode
     * @param $constructorDocumentId
     * @param $type
     * @param $ruleId
     * @return RedirectResponse|View
     */
    public function edit($lngCode, $constructorDocumentId, $type, $ruleId)
    {
        $rule = ConstructorRules::where('id', $ruleId)->constructorRulesOwner()->firstOrFail();

        $obligationList = [];
        if (!is_null($type) && $type == ConstructorRules::OBLIGATION_TYPE) {
            $obligationList = ConstructorRules::getObligations();
        }

        return view("constructor-rules.edit")->with([
            'rule' => $rule,
            'type' => $type,
            'obligationList' => $obligationList,
            'constructorDocumentId' => $constructorDocumentId,
            'saveMode' => 'edit'
        ]);
    }

    /**
     * Function to validate and update new rule
     *
     * @param ConstructorRulesRequest $request
     * @param $lngCode
     * @param $constructorDocumentId
     * @param $type
     * @param $id
     * @return JsonResponse
     */
    public function update(ConstructorRulesRequest $request, $lngCode, $constructorDocumentId, $type, $id)
    {
        $this->manager->update($request->validated(), $id);

        return responseResult('OK', urlWithLng("/constructor-rules/{$constructorDocumentId}/{$type}"));
    }

}
