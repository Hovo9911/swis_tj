<?php

namespace App\Http\Controllers\Constructor;

use App\Http\Controllers\BaseController;
use App\Http\Requests\ConstructorLists\ConstructorListSearchRequest;
use App\Http\Requests\ConstructorLists\ConstructorListsRequest;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorLists\ConstructorLists;
use App\Models\ConstructorLists\ConstructorListsManager;
use App\Models\ConstructorLists\ConstructorListsSearch;
use App\Models\ConstructorListToDataSetFromSaf\ConstructorListToDataSetFromSaf;
use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm;
use App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Throwable;

/**
 * Class ConstructorListsController
 * @package App\Http\Controllers\Constructor
 */
class ConstructorListsController extends BaseController
{
    /**
     * @var ConstructorListsManager
     */
    protected $manager;

    /**
     * @var array
     */
    public $defaultChecked = [
        'saf' => [],
        'mdm' => [],
        'tabs' => [],
        'isCheckedAllSaf' => false,
        'isCheckedAllMdm' => false,
        'isCheckedAllTabs' => false
    ];

    /**
     * ConstructorListsController constructor.
     *
     * @param ConstructorListsManager $manager
     */
    public function __construct(ConstructorListsManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Function to return constructor list index page
     *
     * @param $lngCode
     * @param $selectedConstructorDocumentId
     * @return View
     */
    public function index($lngCode, $selectedConstructorDocumentId = null)
    {
        $lists = ConstructorLists::when($selectedConstructorDocumentId, function ($query) use ($selectedConstructorDocumentId) {
            $query->where('document_id', $selectedConstructorDocumentId);
        })->get();

        return view('constructor-lists.index')->with([
            'constructorDocuments' => ConstructorDocument::getConstructorDocumentForAgency(),
            'selectedConstructorDocumentId' => $selectedConstructorDocumentId,
            'lists' => $lists
        ]);
    }

    /**
     * Function to get all data showing in datatable
     *
     * @param ConstructorListSearchRequest $constructorListSearchRequest
     * @param ConstructorListsSearch $constructorListsSearch
     * @return JsonResponse
     */
    public function table(ConstructorListSearchRequest $constructorListSearchRequest, ConstructorListsSearch $constructorListsSearch)
    {
        $data = $this->dataTableAll($constructorListSearchRequest, $constructorListsSearch);

        return response()->json($data);
    }

    /**
     * Function to show create page
     *
     * @param $lngCode
     * @param $selectedConstructorDocumentId
     * @return View
     */
    public function create($lngCode, $selectedConstructorDocumentId)
    {
        return view('constructor-lists.edit')->with([
            'selectedConstructorDocumentId' => $selectedConstructorDocumentId,
            'mdmData' => DocumentsDatasetFromMdm::mdmFieldData($selectedConstructorDocumentId),
            'safData' => ConstructorListToDataSetFromSaf::generateSafFields(null, false),
            'constructorDocuments' => ConstructorDocument::getConstructorDocumentForAgency(),
            'checked' => $this->defaultChecked,
            'tabs' => $this->getTabs(),
            'saveMode' => 'add',
        ]);
    }

    /**
     * Function to store lists
     *
     * @param ConstructorListsRequest $request
     * @return JsonResponse
     */
    public function store(ConstructorListsRequest $request)
    {
        $constructorLists = $this->manager->store($request->validated());

        return responseResult('OK', null, ['id' => $constructorLists->id]);
    }

    /**
     * Function to return edit view
     *
     * @param $lngCode
     * @param $id
     * @param $viewType
     * @return View
     */
    public function edit($lngCode, $id, $viewType)
    {
        $constructorList = ConstructorLists::where('id', $id)->constructorListOwner()->firstOrFail();

        $mdmData = DocumentsDatasetFromMdm::mdmFieldData($constructorList->document_id);
        $safData = ($constructorList->list_type != ConstructorLists::NON_VISIBLE) ? ConstructorListToDataSetFromSaf::generateSafFields($constructorList->list_type, true) : ConstructorListToDataSetFromSaf::generateSafFields($constructorList->list_type, false);

        $tabs = $this->getTabs();
        $checked = $this->generateCheckedsArray($constructorList, $mdmData->count(), count($safData), count($tabs));

        return view('constructor-lists.edit')->with([
            'constructorDocuments' => ConstructorDocument::getConstructorDocumentForAgency(),
            'constructorList' => $constructorList,
            'mdmData' => $mdmData,
            'safData' => $safData,
            'checked' => $checked,
            'tabs' => $tabs,
            'saveMode' => $viewType,
        ]);
    }

    /**
     * Function to store edited data
     *
     * @param ConstructorListsRequest $request
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     */
    public function update(ConstructorListsRequest $request, $lngCode, $id)
    {
        $this->manager->update($request->validated(), $id);

        return responseResult('OK');
    }

    /**
     * Function to generate checked array
     *
     * @param $constructorList
     * @param $countMdm
     * @param $countSaf
     * @param $countTabs
     * @return array
     */
    private function generateCheckedsArray($constructorList, $countMdm, $countSaf, $countTabs)
    {
        $returnArray = $this->defaultChecked;

        foreach ($constructorList->safFields as $saf) {
            $returnArray['saf'][] = $saf->saf_field_id;
        }

        foreach ($constructorList->mdmFields as $mdm) {
            $returnArray['mdm'][] = $mdm->constructor_data_set_field_id;
        }

        foreach ($constructorList->tabs as $tab) {
            $returnArray['tabs'][] = $tab->tab_key;
        }

        $returnArray['isCheckedAllMdm'] = (count($returnArray['mdm']) == $countMdm);
        $returnArray['isCheckedAllSaf'] = (count($returnArray['saf']) == $countSaf);
        $returnArray['isCheckedAllTabs'] = (count($returnArray['tabs']) == $countTabs);

        return $returnArray;
    }

    /**
     * Function to get Saf Fields
     *
     * @param Request $request
     * @return string
     *
     * @throws Throwable
     */
    public function getSafFields(Request $request)
    {
        $this->validate($request, [
            'typeID' => 'required|integer_with_max',
            'constructorListId' => 'nullable|integer_with_max',
        ]);

        $onlyShowFields = ($request->input('typeID') != ConstructorLists::NON_VISIBLE);
        $constructorList = ConstructorLists::where('id', $request->input('constructorListId'))->constructorListOwner()->where('list_type', $request->input('typeID'))->first();
        $mdmData = [];
        $safData = $onlyShowFields ? ConstructorListToDataSetFromSaf::generateSafFields($request->input('typeID'), true) : ConstructorListToDataSetFromSaf::generateSafFields($request->input('typeID'), false);
        $tabs = $this->getTabs();
        $checked = !empty($constructorList) ? $this->generateCheckedsArray($constructorList, count($mdmData), count($safData), count($tabs)) : [];
        $return['view'] = view('constructor-lists.saf-field-table')->with(['safData' => $safData, 'checked' => $checked, 'constructorList' => $constructorList])->render();

        return $return;
    }

    /**
     * Function to get Saf tabs lists
     *
     * @param array $tabs
     * @return array
     */
    private function getTabs($tabs = [])
    {
        foreach (SingleApplicationDataStructure::lastXml()->xpath('/tabs/tab') as $tab) {
            $tabs[] = [
                'name' => trans((string)$tab->attributes()->name),
                'key' => (string)$tab->attributes()->key
            ];
        }

        return $tabs;
    }
}
