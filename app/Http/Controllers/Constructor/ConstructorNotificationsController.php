<?php

namespace App\Http\Controllers\Constructor;

use App\Http\Controllers\BaseController;
use App\Http\Requests\NotificationTemplates\NotificationTemplatesRequest;
use App\Http\Requests\NotificationTemplates\NotificationTemplatesSearchRequest;
use App\Models\NotificationTemplates\NotificationTemplates;
use App\Models\NotificationTemplates\NotificationTemplatesManager;
use App\Models\NotificationTemplates\NotificationTemplatesSearch;
use Illuminate\Http\JsonResponse;
use Illuminate\View\View;

/**
 * Class ConstructorNotificationsController
 * @package App\Http\Controllers\Constructor
 */
class ConstructorNotificationsController extends BaseController
{
    /**
     * @var NotificationTemplatesManager
     */
    protected $manager;

    /**
     * ConstructorNotificationsController constructor.
     *
     * @param NotificationTemplatesManager $manager
     */
    public function __construct(NotificationTemplatesManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @return View
     */
    public function index()
    {
        $notifications = NotificationTemplates::notificationTemplateOwner()->active()->get();

        return view('constructor-notifications.index')->with([
            'notifications' => $notifications
        ]);
    }

    /**
     * Function to showing all data in datatable
     *
     * @param NotificationTemplatesSearchRequest $notificationTemplatesSearchRequest
     * @param NotificationTemplatesSearch $notificationSearch
     * @return JsonResponse
     */
    public function table(NotificationTemplatesSearchRequest $notificationTemplatesSearchRequest, NotificationTemplatesSearch $notificationSearch)
    {
        $data = $this->dataTableAll($notificationTemplatesSearchRequest, $notificationSearch);

        return response()->json($data);
    }

    /**
     * Function to show create view
     *
     * @return View
     */
    public function create()
    {
        return view('constructor-notifications.edit')->with([
            'saveMode' => 'add',
        ]);
    }

    /**
     * Function to store data
     *
     * @param NotificationTemplatesRequest $request
     * @return JsonResponse
     */
    public function store(NotificationTemplatesRequest $request)
    {
        $this->manager->store($request->validated());

        return responseResult('OK');
    }

    /**
     * Function to show current by id
     *
     * @param $lngCode
     * @param $id
     * @param $viewType
     * @return View
     */
    public function edit($lngCode, $id, $viewType)
    {
        $notifications = NotificationTemplates::where('id', $id)->notificationTemplateOwner()->firstOrFail();

        return view('constructor-notifications.edit')->with([
            'notifications' => $notifications,
            'saveMode' => $viewType
        ]);
    }

    /**
     * Function to update data
     *
     * @param NotificationTemplatesRequest $request
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     */
    public function update(NotificationTemplatesRequest $request, $lngCode, $id)
    {
        $this->manager->update($request->validated(), $id);

        return responseResult('OK');
    }
}
