<?php

namespace App\Http\Controllers\Constructor;

use App\Http\Controllers\BaseController;
use App\Http\Requests\ConstructorTemplates\ConstructorTemplatesRequest;
use App\Http\Requests\ConstructorTemplates\ConstructorTemplatesSearchRequest;
use App\Models\ConstructorTemplates\ConstructorTemplates;
use App\Models\ConstructorTemplates\ConstructorTemplatesManager;
use App\Models\ConstructorTemplates\ConstructorTemplatesSearch;
use Illuminate\Http\JsonResponse;
use Illuminate\View\View;

/**
 * Class ConstructorTemplatesController
 * @package App\Http\Controllers\Constructor
 */
class ConstructorTemplatesController extends BaseController
{
    /**
     * @var ConstructorTemplatesManager
     */
    protected $manager;

    /**
     * ConstructorTemplatesController constructor.
     *
     * @param ConstructorTemplatesManager $manager
     */
    public function __construct(ConstructorTemplatesManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Function to return index View
     *
     * @return View
     */
    public function index()
    {
        return view('constructor-templates.index');
    }

    /**
     * Function to return create View
     *
     * @return View
     */
    public function create()
    {
        return view('constructor-templates.edit')->with([
            'saveMode' => 'add'
        ]);
    }

    /**
     * Function to get table data
     *
     * @param ConstructorTemplatesSearchRequest $constructorTemplatesSearchRequest
     * @param ConstructorTemplatesSearch $constructorTemplatesSearch
     * @return JsonResponse
     */
    public function table(ConstructorTemplatesSearchRequest $constructorTemplatesSearchRequest, ConstructorTemplatesSearch $constructorTemplatesSearch)
    {
        $data = $this->dataTableAll($constructorTemplatesSearchRequest, $constructorTemplatesSearch);

        return response()->json($data);
    }

    /**
     * Function to store data to constructor_templates
     *
     * @param ConstructorTemplatesRequest $request
     * @return JsonResponse
     */
    public function store(ConstructorTemplatesRequest $request)
    {
        $constructorTemplates = $this->manager->store($request->validated());

        return responseResult('OK', null, ['id' => $constructorTemplates->id]);
    }

    /**
     * Function to return edit view
     *
     * @param $lngCode
     * @param $id
     * @param $viewType
     * @return View
     */
    public function edit($lngCode, $id, $viewType)
    {
        $constructorTemplate = ConstructorTemplates::where('id', $id)->constructorTemplateOwner()->firstOrFail();

        return view('constructor-templates.edit')->with([
            'saveMode' => $viewType,
            'constructorTemplate' => $constructorTemplate
        ]);
    }

    /**
     * Function to store update
     *
     * @param ConstructorTemplatesRequest $request
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     */
    public function update(ConstructorTemplatesRequest $request, $lngCode, $id)
    {
        $this->manager->update($request->validated(), $id);

        return responseResult('OK');
    }
}
