<?php

namespace App\Http\Controllers;

use App\Http\Requests\Laboratory\LaboratorySearchRequest;
use App\Http\Requests\Laboratory\LaboratoryStoreRequest;
use App\Http\Requests\Laboratory\LaboratoryUpdateRequest;
use App\Models\Agency\Agency;
use App\Models\BaseModel;
use App\Models\Laboratory\Laboratory;
use App\Models\Laboratory\LaboratoryManager;
use App\Models\Laboratory\LaboratorySearch;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\Role\Role;
use App\Models\User\User;
use App\Models\UserRoles\UserRoles;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Throwable;

/**
 * Class LaboratoryController
 * @package App\Http\Controllers
 */
class LaboratoryController extends BaseController
{
    /**
     * @var LaboratoryManager
     */
    protected $manager;

    /**
     * LaboratoryController constructor.
     *
     * @param LaboratoryManager $manager
     */
    public function __construct(LaboratoryManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Function to show index page
     *
     * @return View
     */
    public function index()
    {
        return view('laboratory.index')->with([
            'agencies' => Agency::allData([], false, true)
        ]);
    }

    /**
     * Function to show all data in datatable
     *
     * @param LaboratorySearchRequest $laboratorySearchRequest
     * @param LaboratorySearch $laboratorySearch
     * @return JsonResponse
     */
    public function table(LaboratorySearchRequest $laboratorySearchRequest, LaboratorySearch $laboratorySearch)
    {
        $data = $this->dataTableAll($laboratorySearchRequest, $laboratorySearch);

        return response()->json($data);
    }

    /**
     * Function to show create page
     *
     * @return View
     */
    public function create()
    {
        return view('laboratory.edit')->with([
            'agencies' => Agency::allData([], false, true),
            'laboratories' => $this->manager->getLabs(),
            'indicators' => [],
            'spheres' => getReferenceRows(ReferenceTable::REFERENCE_SPHERE_TABLE_NAME),
            'saveMode' => 'add'
        ]);
    }

    /**
     * Function to store data
     *
     * @param LaboratoryStoreRequest $request
     * @return JsonResponse
     */
    public function store(LaboratoryStoreRequest $request)
    {
        $laboratory = $this->manager->store($request->validated());

        return responseResult('OK', null, ['id' => $laboratory->id]);
    }

    /**
     * Function to show edit page by current id
     *
     * @param $lngCode
     * @param $id
     * @param $viewType
     * @return View
     */
    public function edit($lngCode, $id, $viewType)
    {
        $laboratory = Laboratory::where('id', $id)->checkUserHasRole()->firstOrFail();
        $labInfo = Agency::select('id')->where('tax_id', $laboratory->tax_id)->first();

        // Remove Indicators
        $this->manager->removeWrongIndicators($laboratory->company_tax_id, $labInfo);

        return view('laboratory.edit')->with([
            'laboratory' => $laboratory,
            'agencies' => Agency::allData([], false, true),
            'laboratories' => $this->manager->getLabs($laboratory->tax_id),
            'indicators' => $this->manager->getIndicators($laboratory->company_tax_id, $labInfo),
            'spheres' => getReferenceRows(ReferenceTable::REFERENCE_SPHERE_TABLE_NAME),
            'saveMode' => $viewType
        ]);
    }

    /**
     * Function to update data
     *
     * @param LaboratoryUpdateRequest $request
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     */
    public function update(LaboratoryUpdateRequest $request, $lngCode, $id)
    {
        $this->manager->update($request->validated(), $id);

        return responseResult('OK');
    }

    /**
     * Function to get Agency users that have one or more laboratory role
     *
     * @param Request $request
     * @return JsonResponse
     */
    // NOT USED FOR NOW
    public function getAgencyUsers(Request $request)
    {
        $this->validate($request, [
            'tax_id' => 'required|tax_id_validator'
        ]);

        if (Auth::user()->isSwAdmin()) {
            $agency = Agency::where('tax_id', $request->input('tax_id'))->first();

            if (!is_null($agency)) {
                $usersRoles = UserRoles::where(['company_tax_id' => $agency->tax_id])->where('role_id', '!=', Role::HEAD_OF_COMPANY)->whereNotNull('authorized_by_id')->pluck('user_id');

                $data['users'] = User::select('id', DB::raw("CONCAT(users.first_name,' ',users.last_name) as name"))->whereIn('id', $usersRoles)->orderByDesc()->get();

                return responseResult('OK', '', $data);
            }
        }

        return responseResult('INVALID_DATA');
    }

    /**
     * Function to get Lab data
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function getLabData(Request $request)
    {
        $validatedData = $request->validate([
            'agency' => 'required|exists:agency,tax_id',
            'tax_id' => 'required|exists:agency,tax_id',
        ]);

        $lab = Agency::select('id', 'legal_entity_name', 'laboratory_code', 'certificate_number', 'certification_period_validity', 'address', 'phone_number', 'email', 'website_url')->where('tax_id', $validatedData['tax_id'])->where('show_status', BaseModel::STATUS_ACTIVE)->first();
        $lab->certification_period_validity = formattedDate($lab->certification_period_validity, true);

        $table = view('laboratory.indicators-table')->with([
            'indicators' => $this->manager->getIndicators($validatedData['agency'], $lab),
        ])->render();

        return responseResult('OK', null, ['info' => $lab, 'table' => $table]);
    }


    /**
     * Function to get indicators as search result
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function getIndicators(Request $request)
    {
        $validatedData = $request->validate([
            'sphere' => 'nullable|string_with_max',
            'indicator' => 'nullable|string_with_max',
            'hs_code' => 'nullable|string_with_max',
            'agency' => 'required|string_with_max|exists:agency,tax_id',
            'lab' => 'required|string_with_max|exists:agency,tax_id'
        ]);

        $table = view('agency.indicators-search-table')->with([
            'data' => $this->manager->getIndicatorsSearch($validatedData),
        ])->render();

        return responseResult('OK', null, $table);
    }

    /**
     * Function to store indicators into db
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function saveIndicators(Request $request)
    {
        $validatedData = $request->validate([
            'indicators' => 'nullable|array',
            'lab' => 'required|string_with_max|exists:agency,tax_id',
            'agency' => 'required|string_with_max|exists:agency,tax_id'
        ]);
        $laboratory = Agency::select('id')->where('tax_id', $validatedData['lab'])->where('show_status', BaseModel::STATUS_ACTIVE)->first();

        $this->manager->saveIndicators($validatedData, $laboratory);

        $table = view('laboratory.indicators-table')->with([
            'indicators' => $this->manager->getIndicators($validatedData['agency'], $laboratory),
        ])->render();

        return responseResult('OK', null, $table);
    }

    /**
     * Function to remove from db
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteIndicators(Request $request)
    {
        $validatedData = $request->validate([
            'indicator' => 'required|exists:laboratory_indicator,id'
        ]);

        $this->manager->deleteIndicator($validatedData);

        return responseResult('OK');
    }

    /**
     * Function to get agency available labs
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getAgencyAvailableLabs(Request $request)
    {
        $validatedData = $request->validate([
            'agency' => 'required|exists:agency,tax_id'
        ]);

        $labs = [['value' => '', 'name' => trans('core.base.label.select')]];
        $data = $this->manager->getAgencyAvailableLabs($validatedData);

        foreach ($data as $item) {
            $labs[] = ['value' => $item->tax_id, 'name' => "($item->tax_id) {$item->name}"];
        }

        return responseResult('OK', null, $labs);
    }

}
