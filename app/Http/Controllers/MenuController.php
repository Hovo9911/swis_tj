<?php

namespace App\Http\Controllers;

use App\Models\Menu\Menu;
use App\Models\Role\Role;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class MenuController
 * @package App\Http\Controllers
 */
class MenuController extends BaseController
{
    /**
     * Function to get menu roles
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function menuRoles(Request $request)
    {
        $this->validate($request, [
            'menu_id' => 'required|integer_with_max|exists:menu,id'
        ]);

        $menuName = Menu::where('id', $request->get('menu_id'))->pluck('name')->first();
        $userCanAuthorizeRoles = Auth::user()->authorizeModuleRoles($menuName);
        $showOnlySelfInfo = Menu::getMenuShowOnlySelfPermissions($menuName);

        // Roles
        $roles = [];
        if ($userCanAuthorizeRoles->roles->count()) {
            foreach ($userCanAuthorizeRoles->roles as $role) {
                $roles[] = [
                    'role_group' => Menu::FALSE,
                    'id' => $role->id,
                    'name' => $role->nameTrans(),
                    'description' => $role->descriptionTrans(),
                ];
            }
        }

        // Roles Groups
        if ($userCanAuthorizeRoles->roleGroups->count()) {
            foreach ($userCanAuthorizeRoles->roleGroups as $roleGroup) {
                $roles[] = [
                    'role_group' => Menu::TRUE,
                    'id' => $roleGroup->id,
                    'name' => $roleGroup->name,
                    'description' => $roleGroup->description,
                ];
            }
        }

        // Show only self
        $showOnlySelfPermissions = [];
        foreach ($showOnlySelfInfo as $key => $value) {
            $showOnlySelfPermissions[] = [
                'id' => $value,
                'name' => $key,
            ];
        }

        $data['showOnlySelf'] = $showOnlySelfPermissions;
        $data['roles'] = $roles;

        return responseResult('OK', '', $data);
    }

    /**
     * Function to get all module Roles
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getModuleAllRoles(Request $request)
    {
        $this->validate($request, [
            'menu_id' => 'required|integer_with_max|exists:menu,id'
        ]);

        $data['roles'] = Role::select('id', 'name')->customRoles()->where('menu_id', $request->get('menu_id'))->active()->orderByDesc()->get();

        return responseResult('OK', '', $data);
    }
}
