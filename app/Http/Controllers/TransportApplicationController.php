<?php

namespace App\Http\Controllers;

use App\GUploader;
use App\Http\Requests\TransportApplication\TransportApplicationInvoiceRequest;
use App\Http\Requests\TransportApplication\TransportApplicationRejectReasonRequest;
use App\Http\Requests\TransportApplication\TransportApplicationRequest;
use App\Http\Requests\TransportApplication\TransportApplicationSearchRequest;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\TransportApplication\TransportApplication;
use App\Models\TransportApplication\TransportApplicationManager;
use App\Models\TransportApplication\TransportApplicationSearch;
use App\Pdf\MPdf\CustomizeMPdf;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Mpdf\MpdfException;
use Throwable;

/**
 * Class TransportApplicationController
 * @package App\Http\Controllers
 */
class TransportApplicationController extends BaseController
{
    /**
     * @var TransportApplicationManager
     */
    protected $manager;

    /**
     * AgencyController constructor.
     *
     * @param TransportApplicationManager $manager
     */
    public function __construct(TransportApplicationManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Function to show all agencies info
     *
     * @return View
     */
    public function index()
    {
        return view('transport-application.index')->with([
            'refTransportPermitTypes' => getReferenceRows(ReferenceTable::REFERENCE_TRANSPORT_PERMIT_TYPES, false, false, ['id', 'code', 'name', 'short_name']),
        ]);
    }

    /**
     * Function to all data showing in datatable
     *
     * @param TransportApplicationSearchRequest $transportApplicationSearchRequest
     * @param TransportApplicationSearch $transportApplicationSearch
     * @return JsonResponse
     */
    public function table(TransportApplicationSearchRequest $transportApplicationSearchRequest, TransportApplicationSearch $transportApplicationSearch)
    {
        $data = $this->dataTableAll($transportApplicationSearchRequest, $transportApplicationSearch);

        return response()->json($data);
    }

    /**
     * Function to show create view
     *
     * @return View
     */
    public function create()
    {
        // Static Values
        $this->viewConstVariables();

        return view('transport-application.edit')->with([
            'saveMode' => 'add',
            'currentStatus' => null,
        ]);
    }

    /**
     * Function to store data
     *
     * @param TransportApplicationRequest $transportApplicationRequest
     * @return JsonResponse
     */
    public function store(TransportApplicationRequest $transportApplicationRequest)
    {
        $transportId = $this->manager->store($transportApplicationRequest->validated());

        return responseResult('OK', urlWithLng('transport-application/' . $transportId . '/edit'));
    }

    /**
     * Function to Show current agency by id
     *
     * @param $lngCode
     * @param $id
     * @param $viewType
     * @return View
     */
    public function edit($lngCode, $id, $viewType)
    {
        $transportApplication = TransportApplication::with(['invoice', 'states'])->where('id', $id)->checkUserHasRole()->firstOrFail();

        // Static Values
        $this->viewConstVariables();

        return view('transport-application.edit')->with([
            'saveMode' => $viewType,
            'transportApplication' => $transportApplication,
            'currentStatus' => $transportApplication->current_status,
        ]);
    }

    /**
     * Function to Update data
     *
     * @param TransportApplicationRequest $transportApplicationRequest
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     */
    public function update(TransportApplicationRequest $transportApplicationRequest, $lngCode, $id)
    {
        if (!empty($transportApplicationRequest->get('transport_application_receipt_file'))) {
            $gUploader = new GUploader('transport_application_receipt_file');
            $gUploader->storePerm($transportApplicationRequest->get('transport_application_receipt_file'));
        }

        $this->manager->update($transportApplicationRequest->validated(), $id);

        return responseResult('OK', urlWithLng('transport-application/' . $id . '/edit'));
    }

    /**
     * Function to application set to reject status
     *
     * @param TransportApplicationRejectReasonRequest $transportApplicationRejectReasonRequest
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     */
    public function reject(TransportApplicationRejectReasonRequest $transportApplicationRejectReasonRequest, $lngCode, $id)
    {
        $this->manager->reject($transportApplicationRejectReasonRequest->validated(), $id);

        return responseResult('OK', urlWithLng('transport-application/' . $id . '/edit'));
    }

    /**
     * Function to create invoice
     *
     * @param TransportApplicationInvoiceRequest $transportApplicationInvoiceRequest
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     */
    public function invoice(TransportApplicationInvoiceRequest $transportApplicationInvoiceRequest, $lngCode, $id)
    {
        $this->manager->storeInvoice($transportApplicationInvoiceRequest->validated(), $id);

        return responseResult('OK', urlWithLng('transport-application/' . $id . '/edit'));
    }

    /**
     * Function to print invoice
     *
     * @param $lngCode
     * @param $id
     *
     * @throws Throwable
     */
    public function printInvoice($lngCode, $id)
    {
        $transportApplication = TransportApplication::with(['invoice'])->where('id', $id)->checkUserHasRole()->firstOrFail();

        $mpdf = new CustomizeMPdf([
            'mode' => 'utf-8'
        ]);

        $mpdf->SetCreator('MPDF');
        $mpdf->SetAuthor('');
        $mpdf->SetTitle('Invoice');
        $mpdf->SetSubject('Invoice');

        $invoiceDate = Carbon::parse($transportApplication->invoice->getOriginal('created_at'));
        $permitRefInfo = getReferenceRows(ReferenceTable::REFERENCE_TRANSPORT_PERMIT_TYPES, $transportApplication->ref_transport_permit_type);
        $template = TransportApplication::TEMPLATE_FOLDER;
        $trackingNumber = $transportApplication->invoice->tracking_number;;

        $pdfData = [
            'template' => $template,
            'inspector_name' => Auth::user()->name(),
            'payment_amount' => $transportApplication->invoice->payment_amount,
            'tracking_number' => $trackingNumber,
            'invoice_year' => $invoiceDate->format('y'),
            'invoice_month' => $invoiceDate->month,
            'invoice_day' => $invoiceDate->day,
            'payment_purpose' => $permitRefInfo->name . ' (' . $permitRefInfo->code . ') - ' . trans("swis.pdf.{$template}.number") . ' ' . $transportApplication->permission_number
        ];

        $html = view("pdf-templates/" . $template . "/invoice")->with($pdfData)->render();

        //
        $mpdf->WriteHTML($html);
        $mpdf->Output("invoice_$trackingNumber.pdf", 'I');
    }

    /**
     * Function to print permit doc
     *
     * @param $lngCode
     * @param $id
     * @throws Throwable
     * @throws MpdfException
     */
    public function printPermitDoc($lngCode, $id)
    {
        $transportApplication = TransportApplication::with(['invoice'])->where('id', $id)->checkUserHasRole()->firstOrFail();

        $mpdf = new CustomizeMPdf([
            'mode' => 'utf-8',
        ]);

        $mpdf->SetCreator('MPDF');
        $mpdf->SetAuthor('');
        $mpdf->SetTitle('Permit Document');
        $mpdf->SetSubject('Permit Document');

        $permitRefInfo = getReferenceRows(ReferenceTable::REFERENCE_TRANSPORT_PERMIT_TYPES, $transportApplication->ref_transport_permit_type);
        $template = TransportApplication::TEMPLATE_FOLDER;
        $permitDocs = TransportApplication::PERMIT_DOCS_FOLDER;

        $refOriginCountry = getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $transportApplication->ref_origin_country);
        $refDestinationCountry = getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $transportApplication->ref_destination_country);
        $refBorderCrossing = getReferenceRows(ReferenceTable::REFERENCE_TRANSPORT_BORDER_CHECKPOINTS, $transportApplication->ref_border_crossing, false, ['id', 'code', 'name', 'short_name']);
        $refTransportCarrier = getReferenceRows(ReferenceTable::REFERENCE_TRANSPORT_PERMIT_CARRIERS, $transportApplication->ref_transport_carriers_id, false, '*');
        $refTransportCarrierCountry = getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $refTransportCarrier->countries);
        $permission_number = $transportApplication->permission_number;

        $refTransportCarrierAddress = '';
        if (!is_null($refTransportCarrierCountry)) {
            $refTransportCarrierAddress .= $refTransportCarrierCountry->name;
        }

        if ($refTransportCarrier->city) {
            $refTransportCarrierAddress .= ', ' . $refTransportCarrier->city;
        }

        if ($refTransportCarrier->address_postal_code) {
            $refTransportCarrierAddress .= ', ' . $refTransportCarrier->address_postal_code;
        }

        if ($refTransportCarrier->address_street) {
            $refTransportCarrierAddress .= ', ' . $refTransportCarrier->address_street;
        }

        if ($refTransportCarrier->address_house_flat) {
            $refTransportCarrierAddress .= ', ' . $refTransportCarrier->address_house_flat;
        }

        $pdfData = [
            'refOriginCountry' => $refOriginCountry,
            'refDestinationCountry' => $refDestinationCountry,
            'periodValidityFrom' => $transportApplication->permit_validity_period_from,
            'periodValidityTo' => $transportApplication->permit_validity_period_to,
            'periodValidity' => $transportApplication->period_validity,
            'arrivalDate' => $transportApplication->arrival_date,
            'departureDate' => $transportApplication->departure_date,
            'refBorderCrossing' => $refBorderCrossing->short_name ?? '',
            'refTransportCarrierCountry' => $refTransportCarrierCountry,
            'refTransportCarrierName' => $refTransportCarrier->name,
            'refTransportCarrierAddress' => $refTransportCarrierAddress,
            'vehicleBrand' => $transportApplication->vehicle_brand,
            'vehicleRegNumber' => $transportApplication->vehicle_reg_number,
            'trailerRegNumber' => $transportApplication->trailer_reg_number,
            'vehicleCarryingCapacity' => $transportApplication->vehicle_carrying_capacity,
            'vehicleEmptyCapacity' => $transportApplication->vehicle_empty_capacity,
            'trailerCarryingCapacity' => $transportApplication->trailer_carrying_capacity,
            'trailerEmptyCapacity' => $transportApplication->trailer_empty_capacity,
            'entryGoodsWeight' => $transportApplication->entry_goods_weight,
            'departureGoodsWeight' => $transportApplication->departure_goods_weight,
            'placeOfUnloading' => $transportApplication->place_of_unloading,
            'natureGoods' => $transportApplication->nature_of_goods,
        ];

        $page1 = view("pdf-templates/" . $template . "/" . $permitDocs . "/" . $permitRefInfo->code . '/page-1')->with($pdfData)->render();
        $page2 = view("pdf-templates/" . $template . "/" . $permitDocs . "/" . $permitRefInfo->code . '/page-2')->with($pdfData)->render();

        //
        $mpdf->WriteHTML($page1);
        $mpdf->WriteHTML($page2);
        $mpdf->Output("permit_doc_$permission_number.pdf", 'I');
    }

    /**
     * Function to pass variables to view
     *
     * @return void
     */
    private function viewConstVariables()
    {
        view()->composer(['transport-application.edit'], function ($view) {
            $view->with([

                'refTransportPermitTypes' => getReferenceRows(ReferenceTable::REFERENCE_TRANSPORT_PERMIT_TYPES, false, false, ['id', 'code', 'name', 'short_name']),
                'refTransportBorderCheckpoints' => getReferenceRows(ReferenceTable::REFERENCE_TRANSPORT_BORDER_CHECKPOINTS, false, false, ['id', 'code', 'name', 'short_name']),
                'refCountries' => getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES),

                'registeredStatus' => TransportApplication::TRANSPORT_APPLICATION_STATUS_REGISTERED,
                'approvedStatus' => TransportApplication::TRANSPORT_APPLICATION_STATUS_APPROVED,
                'rejectedStatus' => TransportApplication::TRANSPORT_APPLICATION_STATUS_REJECTED,
                'waitingPaymentStatus' => TransportApplication::TRANSPORT_APPLICATION_STATUS_WAITING_PAYMENT_CONFIRMATION,
                'issuedStatus' => TransportApplication::TRANSPORT_APPLICATION_STATUS_ISSUED,

                'transportPermitTypeP1' => ReferenceTable::REFERENCE_TRANSPORT_PERMIT_TYPE_P1,
                'transportPermitTypeP3' => ReferenceTable::REFERENCE_TRANSPORT_PERMIT_TYPE_P3,
                'transportPermitTypeP_BP' => ReferenceTable::REFERENCE_TRANSPORT_PERMIT_TYPE_P_BP

            ]);
        });
    }
}