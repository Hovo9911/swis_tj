<?php

namespace App\Http\Controllers;

use App\Http\Requests\LaboratoryIndicator\LaboratoryIndicatorRequest;
use App\Http\Requests\LaboratoryIndicator\LaboratoryIndicatorSearchRequest;
use App\Models\Laboratory\Laboratory;
use App\Models\LaboratoryIndicator\LaboratoryIndicator;
use App\Models\LaboratoryIndicator\LaboratoryIndicatorManager;
use App\Models\LaboratoryIndicator\LaboratoryIndicatorSearch;
use App\Models\ReferenceTable\ReferenceTable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

/**
 * Class LaboratoryIndicatorsController
 * @package App\Http\Controllers
 */
// NOT USED
class LaboratoryIndicatorsController extends BaseController
{
    /**
     * @var LaboratoryIndicatorManager
     */
    protected $manager;

    /**
     * LaboratoryIndicatorsController constructor.
     *
     * @param LaboratoryIndicatorManager $manager
     */
    public function __construct(LaboratoryIndicatorManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @return View
     */
    public function index()
    {
        $laboratories = Laboratory::active()->checkUserHasRole()->get();

        return view('laboratory-indicator.index', compact('laboratories'));
    }

    /**
     * Function to  data showing in datatable
     *
     * @param LaboratoryIndicatorSearchRequest $laboratoryIndicatorSearchRequest
     * @param LaboratoryIndicatorSearch $laboratoryIndicatorSearch
     * @return JsonResponse
     */
    public function table(LaboratoryIndicatorSearchRequest $laboratoryIndicatorSearchRequest, LaboratoryIndicatorSearch $laboratoryIndicatorSearch)
    {
        $data = $this->dataTableAll($laboratoryIndicatorSearchRequest, $laboratoryIndicatorSearch);

        return response()->json($data);
    }

    /**
     * @param $lngCode
     * @param bool $labId
     * @return View
     */
    public function create($lngCode, $labId = false)
    {
        $laboratories = Laboratory::active()->checkUserHasRole()->get();

        return view('laboratory-indicator.edit')->with([
            'saveMode' => 'add',
            'labId' => $labId,
            'laboratories' => $laboratories
        ]);
    }

    /**
     * Function to store validated data
     *
     * @param LaboratoryIndicatorRequest $request
     * @return JsonResponse
     */
    public function store(LaboratoryIndicatorRequest $request)
    {
        $this->manager->store($request->validated());

        return responseResult('OK');
    }

    /**
     * Show current by id
     *
     * @param $lngCode
     * @param $id
     * @return View
     */
    public function edit($lngCode, $id)
    {
        $laboratoryIndicator = LaboratoryIndicator::where('id', $id)->checkUserHasRole()->firstOrFail();
        $laboratories = Laboratory::active()->checkUserHasRole()->get();

        $rTable = 'reference_lab_expertise_indicator';
        $rTableMl = 'reference_lab_expertise_indicator_ml';

        $indicator = DB::table($rTable)->select(DB::raw("CONCAT(code, ' , ', name) as name"), $rTable . '.id')
            ->join($rTableMl, $rTable . '.id', '=', $rTableMl . '.' . $rTable . '_id')
            ->where('id', $laboratoryIndicator->indicator_id)
            ->where('lng_id', cLng('id'))
            ->first();

        return view('laboratory-indicator.edit')->with([
            'laboratoryIndicator' => $laboratoryIndicator,
            'laboratories' => $laboratories,
            'indicator' => $indicator,
            'saveMode' => 'edit'
        ]);
    }

    /**
     * Function to Update data
     *
     * @param LaboratoryIndicatorRequest $request
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     */
    public function update(LaboratoryIndicatorRequest $request, $lngCode, $id)
    {
        $this->manager->update($request->validated(), $id);

        return responseResult('OK');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function autocomplete(Request $request)
    {
        $this->validate($request, [
            'q' => 'required|string_with_max'
        ]);

        $searchQuery = $request->input('q');

        $rTable = ReferenceTable::REFERENCE_LAB_EXPERTISE_INDICATOR;
        $rTableMl = ReferenceTable::REFERENCE_LAB_EXPERTISE_INDICATOR . '_ml';

        $query = DB::table($rTable)->select(DB::raw("CONCAT(code, ' , ', name) as name"), $rTable . '.*')
            ->join($rTableMl, $rTable . '.id', '=', $rTableMl . '.' . $rTable . '_id')
            ->where('lng_id', cLng('id'))
            ->where("{$rTable}.show_status", 1)
            ->limit(25);

        $query->where(function ($q) use ($query, $searchQuery) {
            $q->where('name', 'ILIKE', '%' . $searchQuery . '%')
                ->orWhere('code', 'ILIKE', '%' . $searchQuery . '%');
        });

        $result = $query->get();

        $data['items'] = $result->toArray();

        return response()->json($data);
    }
}
