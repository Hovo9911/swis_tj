<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Core\Interfaces\DataTableSearchContract;
use Illuminate\Http\Request;

/**
 * Class RolesController
 * @package App\Http\Controllers
 */
class BaseController extends Controller
{
    /**
     * Function to select all data for dataTable (pagination,search)
     *
     * @param Request $request
     * @param DataTableSearchContract $searchManager
     * @return array
     */
    public function dataTableAll(Request $request, DataTableSearchContract $searchManager)
    {
        $draw = $request->get('draw');
        $start = $request->get('start');
        $length = $request->get('length');
        $filterData = isset($request['f']) ? $request['f'] : [];
        $sSearch = empty($request['search']['value']) || !is_string($request['search']['value']) ? '' : $request['search']['value'];

        // limit offset
        $searchManager->limit($start, $length);

        // Order part
        $order = $request->input('order');
        if (!empty($order)) {
            if (!empty($request->input('orderColumnName'))) {
                $columns = $request->input('orderColumnName');
                $searchManager->orderBy($columns, $order[0]['dir']);
            } else {
                $columns = $request->input('columns');
                $searchManager->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir']);
            }
        }

        if (count($filterData) > 0) {
            $searchManager->searchBy($filterData);
        }

        if (!empty($sSearch)) {
            $searchManager->searchBy('q', $sSearch);
        }

        $result = $searchManager->search();
        $rowsCount = $searchManager->rowsCount();

        return [
            'draw' => $draw,
            'recordsTotal' => $rowsCount,
            'recordsFiltered' => $rowsCount,
            'data' => $result,
        ];
    }
}