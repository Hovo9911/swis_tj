<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Core\Dictionary\Dictionary;
use App\Http\Controllers\Core\Dictionary\DictionarySearch;
use App\Http\Controllers\Core\Interfaces\DictionaryContract;
use App\Http\Requests\Dictionary\DictionaryRequest;
use App\Models\Application\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\View\View;

/**
 * Class DictionaryController
 * @package App\Http\Controllers
 */
class DictionaryController extends BaseController
{
    /**
     * @var DictionaryContract
     */
    protected $manager;

    /**
     * DictionaryController constructor.
     *
     * @param DictionaryContract $manager
     */
    public function __construct(DictionaryContract $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param Request $request
     * @return View
     */
    public function index(Request $request)
    {
        $apps = Application::with('notDeletedLanguages')->where('id', Dictionary::APP_TYPE_ADM_ID)->get();

        $appType = $request->get('appType');

        if (empty($appType)) {
            $appType = Config::get('app.type');
        }

        return view('dictionary.index', compact('apps', 'appType'));
    }

    /**
     * Functionto show data showing in datatable
     *
     * @param Request $request
     * @param DictionarySearch $dictionarySearch
     * @return JsonResponse
     */
    public function table(Request $request, DictionarySearch $dictionarySearch)
    {
        $data = $this->dataTableAll($request, $dictionarySearch);

        return response()->json($data);
    }

    /**
     * Create new if not exist || update ml data
     *
     * @param DictionaryRequest $request
     * @return JsonResponse
     */
    public function edit(DictionaryRequest $request)
    {
        $this->manager->editKey($request->get('key'), $request->get('ml'), $request->get('appType'));

        return responseResult('OK');
    }

    /**
     * Delete Data
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function delete(Request $request)
    {
        $this->manager->deleteKey($request->input('deleteItems'), $request->input('appType'));

        return responseResult('OK');
    }
}
