<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\BaseController;
use App\Http\Requests\ReportsAccess\ReportsAccessRequest;
use App\Http\Requests\ReportsAccess\ReportsAccessSearchRequest;
use App\Models\Agency\Agency;
use App\Models\ReportsAccess\ReportsAccess;
use App\Models\ReportsAccess\ReportsAccessManager;
use App\Models\ReportsAccess\ReportsAccessSearch;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class ReportsAccessController
 * @package App\Http\Controllers\Report
 */
class ReportsAccessController extends BaseController
{
    /**
     * @var ReportsAccessManager
     */
    protected $manager;

    /**
     * ReportsAccessManager constructor.
     *
     * @param ReportsAccessManager $manager
     */
    public function __construct(ReportsAccessManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Function to return index View
     *
     * @return View
     */
    public function index()
    {
        return view('reports-access.index');
    }

    /**
     * Function to showing all data in dataTable
     *
     * @param ReportsAccessSearchRequest $reportsAccessSearchRequest
     * @param ReportsAccessSearch $reportsAccessSearch
     * @return JsonResponse
     */
    public function table(ReportsAccessSearchRequest $reportsAccessSearchRequest, ReportsAccessSearch $reportsAccessSearch)
    {
        $data = $this->dataTableAll($reportsAccessSearchRequest, $reportsAccessSearch);

        return response()->json($data);
    }

    /**
     * Function to show create page
     *
     * @return View
     *
     * @throws GuzzleException
     */
    public function create()
    {
        return view('reports-access.edit')->with([
            'agencies' => Agency::allData([],'all'),
            'jasperReports' => $this->manager->getJasperReports(),
            'saveMode' => 'add'
        ]);
    }

    /**
     * Function to store data
     *
     * @param ReportsAccessRequest $request
     * @return JsonResponse
     */
    public function store(ReportsAccessRequest $request)
    {
        $this->manager->store($request->validated());

        return responseResult('OK');
    }

    /**
     * Function to show current by id
     *
     * @param $lngCode
     * @param $id
     * @param $viewType
     * @return View
     *
     * @throws GuzzleException
     */
    public function edit($lngCode, $id, $viewType)
    {
        $reportsAccess = ReportsAccess::where('id', $id)->firstOrFail();

        return view('reports-access.edit')->with([
            'agencies' => Agency::allData([],'all'),
            'reportsAccess' => $reportsAccess,
            'selectedAgencies' => $reportsAccess->reportsAccessAgencies()->pluck('company_tax_id')->toArray(),
            'jasperReports' => $this->manager->getJasperReports(),
            'saveMode' => $viewType
        ]);
    }

    /**
     * Function to update data
     *
     * @param ReportsAccessRequest $request
     * @param $lngCode
     * @param $id
     * @return JsonResponse
     */
    public function update(ReportsAccessRequest $request, $lngCode, $id)
    {
        $this->manager->update($request->validated(), $id);

        return responseResult('OK');
    }

    /**
     * Function to Delete by id(array)
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function delete(Request $request)
    {
        $this->manager->delete($request->input('deleteItems'));

        return responseResult('OK');
    }

}