<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\BaseController;
use App\Http\Requests\Reports\ReportsRequest;
use App\Models\Agency\Agency;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorMapping\ConstructorMapping;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\RegionalOffice\RegionalOffice;
use App\Models\RegionalOfficeConstructorDocuments\RegionalOfficeConstructorDocuments;
use App\Models\Reports\ReportManagerFactory;
use App\Models\Reports\Reports;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use TCPDF;
use Throwable;

/**
 * Class ReportsController
 * @package App\Http\Controllers\Report
 */
class ReportsController extends BaseController
{
    /**
     * @var string
     */
    private $folderName = 'report';
    private $tableName = 'table';

    /**
     * ReportsController constructor.
     */
    public function __construct()
    {
        ini_set('memory_limit', '550M');
    }

    /**
     * Function to show index
     *
     * @param $lngCode
     * @param $reportCode
     * @return View
     */
    public function index($lngCode, $reportCode)
    {
        $viewPath = $this->getViewName($reportCode);

        $report = Reports::where('code', $reportCode)->firstOrFail();

        // documents all -> Sw Admin, Broker
        $documents = ConstructorDocument::getConstructorDocuments();

        if (!Auth::user()->isSwAdmin()) {

            if (Auth::user()->isAgency()) {
                $documents = ConstructorDocument::getConstructorDocumentForAgency();
            }

            $subDivisions = $this->getSubdivisions($reportCode);

            $laboratories = [];
            if ($reportCode == Reports::REPORT_TJST_20_CODE) {
                $laboratories = getReferenceRows(ReferenceTable::REFERENCE_LABORATORIES);
            }

            return view($viewPath, compact('documents', 'reportCode', 'report', 'subDivisions', 'laboratories'));
        }

        $agencies = collect();
        if ($reportCode == Reports::REPORT_SW_ADMIN_REPORT_5_CODE) {
            $agencies = Agency::orderByDesc()->active()->get();
        }

        return view($viewPath, compact('documents', 'reportCode', 'report', 'agencies'));
    }

    /**
     * Function to get each report and generate (html,pdf,excel)
     *
     * @param $lngCode
     * @param $reportCode
     * @param ReportsRequest $reportsRequest
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function generate($lngCode, $reportCode, ReportsRequest $reportsRequest)
    {
        $data = $reportsRequest->validated();

        // Report Result Data
        $reportManagerFactory = new ReportManagerFactory();
        $reportManager = $reportManagerFactory->initializeManager($reportCode);
        $reportData = $reportManager->getData($reportCode, $data);

        // View
        $viewPath = $this->getViewName($reportCode, true);

        // -----------

        $renderHtml = true;
        $reportFileName = '';

        if (!empty($reportData)) {

            $html = view($viewPath)->with(['data' => $data, 'reportData' => $reportData, 'reportCode' => $reportCode, 'renderHtml' => $renderHtml])->render();

            // PDF
            if ($data['download_type'] == Reports::DOWNLOAD_TYPE_PDF) {

                ini_set('memory_limit', '1G');
                ini_set('max_execution_time', 300);

                $subject = date(config('swis.date_time_format')) . ' ' . (isset($data['user_position']) ? $data['user_position'] . ' ' : '') . Auth::user()->name();

                $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
                $pdf->SetCreator(Auth::user()->name());
                $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
                $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

                if ($reportCode != Reports::REPORT_TJFSC_2_CODE && $reportCode != Reports::REPORT_TJFSC_3_CODE && $reportCode != Reports::REPORT_TJFSC_4_CODE && $reportCode != Reports::REPORT_TJFSC_5_CODE && $reportCode != Reports::REPORT_TJFSC_6_CODE):
                    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
                endif;

                $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
                $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
                $pdf->setPrintHeader(false);
                $pdf->setPrintFooter(false);
                $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
                $pdf->SetFont('freeserif', '', 10);
                $pdf->SetSubject($subject);
                $pdf->AddPage('L');
                $this->setPdfCellPaddings($pdf, $reportCode);
                $pdf->writeHTML($html, true, 0, true, 0);
                $pdf->setPrintHeader(false);
                $pdf->setPrintFooter(false);
                $pdf->lastPage();
//                $reportFileName = $reportCode . '_' . currentDateTimeNameFormat() . '.pdf';
                $reportFileName = str_replace('/', ' ', trans('swis.' . $reportCode . '.menu.title')) . '.pdf';
                $reportsDir = Storage::disk('shared')->path('files/reports/' . $reportCode . '/' . Auth::id());

                if (!is_dir($reportsDir)) {
                    mkdir($reportsDir, 0775, true);
                }

                $reportFile = $reportsDir . '/' . $reportFileName;
                $pdf->Output($reportFile, 'F');

                $renderHtml = false;

                ini_restore('memory_limit');
                ini_restore('max_execution_time');
            }

            // XLS
            if ($data['download_type'] == Reports::DOWNLOAD_TYPE_XLS) {

                $subject = trans("swis.report.{$reportCode}.key3") . ' ' . date(config('swis.date_time_format')) . ' ' . (isset($data['user_position']) ? $data['user_position'] . ' ' : '') . Auth::user()->name();
                $html .= "<table><tr><td>" . $subject . "</td></tr></table>";
                $content = '<meta charset="utf-8" />' . $html;

                $reportFileName = str_replace('/', ' ', trans('swis.' . $reportCode . '.menu.title')) . '.xls';
                $reportsDir = Storage::disk('shared')->path('files/reports/' . $reportCode . '/' . Auth::id());

                if (!is_dir($reportsDir)) {
                    mkdir($reportsDir, 0775, true);
                }

                $reportsDir = $reportsDir . '/' . $reportFileName;
                file_put_contents($reportsDir, $content);

                $renderHtml = false;
            }
        }

        $result['html'] = view($viewPath)->with(['data' => $data, 'reportData' => $reportData, 'reportCode' => $reportCode, 'reportFileName' => $reportFileName, 'renderHtml' => $renderHtml])->render();

        return responseResult('OK', '', $result, []);
    }

    /**
     * Function to set tcPdf cell paddings
     *
     * @param $pdf
     * @param $reportCode
     */
    public function setPdfCellPaddings($pdf, $reportCode)
    {
        switch ($reportCode) {
            case Reports::REPORT_TJFSC_2_CODE:
            case Reports::REPORT_TJFSC_3_CODE:
            case Reports::REPORT_TJFSC_4_CODE:
                $pdf->setCellPaddings($left = '0', $top = '0', $right = '0', $bottom = '0');
                break;
            default:
                $pdf->setCellPaddings($left = '1', $top = '2', $right = '1', $bottom = '1');
                break;
        }
    }

    /**
     * Function to return view name for render view
     *
     * @param $reportCode
     * @param bool $tableBlade
     * @return string
     */
    public function getViewName($reportCode, $tableBlade = false)
    {
        $bladeName = ($tableBlade) ? $this->tableName : 'index';

        switch ($reportCode) {

            //
            case substr($reportCode, 0, strlen(Reports::REPORTS_TJST_CODE)) === Reports::REPORTS_TJST_CODE:
                $viewPath = $this->folderName . '/' . Reports::REPORTS_TJST_CODE . '/' . $reportCode . '/' . $bladeName;
                break;

            //
            case substr($reportCode, 0, strlen(Reports::REPORT_TJMOH_CODE)) === Reports::REPORT_TJMOH_CODE:
                $viewPath = $this->folderName . '/' . Reports::REPORT_TJMOH_CODE . '/' . $reportCode . '/' . $bladeName;
                break;

            //
            case substr($reportCode, 0, strlen(Reports::REPORT_TJFSC_CODE)) === Reports::REPORT_TJFSC_CODE:
                $viewPath = $this->folderName . '/' . Reports::REPORT_TJFSC_CODE . '/' . $reportCode . '/' . $bladeName;
                break;

            //
            case substr($reportCode, 0, strlen(Reports::REPORT_GLOBAL_REPORT_CODE)) === Reports::REPORT_GLOBAL_REPORT_CODE :
                $viewPath = $this->folderName . '/' . Reports::REPORT_GLOBAL_REPORT_CODE . '/' . $reportCode . '/' . $bladeName;
                break;

            //
            case substr($reportCode, 0, strlen(Reports::REPORT_SW_ADMIN_CODE)) === Reports::REPORT_SW_ADMIN_CODE :
                $viewPath = $this->folderName . '/' . Reports::REPORT_SW_ADMIN_CODE . '/' . $reportCode . '/' . $bladeName;
                break;

            //
            case substr($reportCode, 0, strlen(Reports::REPORT_BROKER_CODE)) === Reports::REPORT_BROKER_CODE :
                $viewPath = $this->folderName . '/' . Reports::REPORT_BROKER_CODE . '/' . $reportCode . '/' . $bladeName;
                break;

            //
            case substr($reportCode, 0, strlen(Reports::REPORT_TJCS_CODE)) === Reports::REPORT_TJCS_CODE :
                $viewPath = $this->folderName . '/' . Reports::REPORT_TJCS_CODE . '/' . $reportCode . '/' . $bladeName;
                break;

            //
            case substr($reportCode, 0, strlen(Reports::REPORT_TJ_CCI_TPP_CODE)) === Reports::REPORT_TJ_CCI_TPP_CODE :
                $viewPath = $this->folderName . '/' . Reports::REPORT_TJ_CCI_TPP_CODE . '/' . $reportCode . '/' . $bladeName;
                break;

            //
            case Reports::REPORT_SUB_APPLICATION_PROCESS_TIME:
                $viewPath = $this->folderName . '/' . Reports::REPORT_SUB_APPLICATION_PROCESS_TIME . '/' . $bladeName;
                break;

            default:
                $viewPath = 'errors.404';
                break;
        }

        return $viewPath;
    }

    /**
     * Function to get constructor documents
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getConstructorDocuments(Request $request)
    {
        $this->validate($request, [
            'company_tax_id' => 'required|string_with_max'
        ]);

        $documents = ConstructorDocument::getConstructorDocumentForAgency($request->input('company_tax_id'));

        return responseResult('OK', '', ['documents' => $documents]);
    }

    /**
     * Function to return subdivisions
     *
     * @param $reportCode
     * @return array|Collection|null
     */
    public function getSubdivisions($reportCode)
    {
        switch ($reportCode) {
            case Reports::REPORT_TJST_5_CODE:
            case Reports::REPORT_TJMOH_3_CODE:
            case Reports::REPORT_GLOBAL_REPORT_2_CODE:
                $subDivisions = getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS, false, false, false, false, false, false, ['company_tax_id' => Auth::user()->companyTaxId()], true);
                break;
            case  Reports::REPORT_TJFSC_5_CODE:
            case  Reports::REPORT_TJFSC_9_CODE:
                $regionalOfficeIds = RegionalOfficeConstructorDocuments::where('constructor_document_code', Reports::PHYTOSANITARY_CERTIFICATE_CODE)->pluck('regional_office_id')->all();
                $regionalOfficeCodes = RegionalOffice::whereIn('id', $regionalOfficeIds)->where('company_tax_id', Auth::user()->companyTaxId())->active()->pluck('office_code')->all();

                $subDivisions = getReferenceRowsWhereIn(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS, [], $regionalOfficeCodes)->keyBy('id');
                break;
            case Reports::REPORT_BROKER_REPORT_1_CODE:
                $subDivisions = getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS);
                break;
            default:
                $subDivisions = collect();
                break;
        }

        return $subDivisions;
    }

    /**
     * Function to get constructor documents all active end statuses
     *
     * @param $lngCode
     * @param $documentId
     * @param Request $request
     * @return JsonResponse
     */
    public function getDocumentStatus(Request $request, $lngCode, $documentId)
    {
        if ($request->input('allStatuses')) {
            $allEndStates = ConstructorStates::select('id')->where('constructor_document_id', $documentId)->active()->get();
        } else {
            $allEndStates = ConstructorStates::select('id')->where(['constructor_document_id' => $documentId, 'state_type' => ConstructorStates::END_STATE_TYPE])->active()->get();
        }

        $endStates = [];
        if ($allEndStates->count() > 0) {
            foreach ($allEndStates as $endState) {
                $documentMapping = ConstructorMapping::select('id')->where(['document_id' => $documentId, 'end_state' => $endState->id])->active()->first();
                if (!is_null($documentMapping)) {
                    $endStates[$endState->id] = $endState->current()->state_name;
                }
            }
        }

        return responseResult('OK', '', ['statuses' => $endStates]);
    }

    /**
     * Function to get  document all active subdivisions
     *
     * @param Request $request
     * @param $lngCode
     * @return JsonResponse
     */
    public function getDocumentSubdivisions(Request $request, $lngCode)
    {
        $this->validate($request, [
            'document_id' => 'required'
        ]);

        $constructorDocuments = ConstructorDocument::select('company_tax_id', 'document_code')->whereIn('id', (array)$request->input('document_id'));
        if (Auth::user()->isAgency()) {
            $constructorDocuments->where('company_tax_id', Auth::user()->companyTaxId());
        }

        $constructorDocuments = $constructorDocuments->get();

        $activeSubdivisions = [];
        if ($constructorDocuments->count()) {

            $constructorDocumentsCodes = $constructorDocuments->pluck('document_code')->all();
            $constructorDocumentsCompanyTaxIds = $constructorDocuments->pluck('company_tax_id')->all();

            $regionalOfficeIds = RegionalOfficeConstructorDocuments::whereIn('constructor_document_code', $constructorDocumentsCodes)->pluck('regional_office_id')->all();
            $regionalOfficeCodes = RegionalOffice::whereIn('id', $regionalOfficeIds)->whereIn('company_tax_id', $constructorDocumentsCompanyTaxIds)->active()->pluck('office_code')->all();

            if (count($regionalOfficeCodes)) {
                $activeSubdivisions = getReferenceRowsWhereIn(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS, [], $regionalOfficeCodes)->keyBy('id')->toArray();
            }
        }

        return responseResult('OK', '', ['subdivisions' => $activeSubdivisions]);
    }
}