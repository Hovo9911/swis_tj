<?php

namespace App\Http\Controllers;

use App\Http\Requests\CustomsPortal\CustomsPortalSearchRequest;
use App\Models\Agency\Agency;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\CustomsPortal\CustomsPortalSearch;
use App\Models\ReferenceTable\ReferenceTable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class AgencyController
 * @package App\Http\Controllers
 */
class CustomsPortalController extends BaseController
{
    /**
     * @return View
     */
    public function index()
    {
        return view('customs-portal.index')->with([
            'agencies' => Agency::allData(),
        ]);
    }

    /**
     * Function to all data showing in datatable
     *
     * @param CustomsPortalSearchRequest $customsPortalSearchRequest
     * @param CustomsPortalSearch $agencySearch
     * @return JsonResponse
     */
    public function table(CustomsPortalSearchRequest $customsPortalSearchRequest, CustomsPortalSearch $agencySearch)
    {
        $data = $this->dataTableAll($customsPortalSearchRequest, $agencySearch);

        return response()->json($data);
    }

    /**
     * Function to get agency constructor documents and subdivisions
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getAgencyConstructDocumentsAndSubdivisions(Request $request)
    {
        $this->validate($request, [
            'agency_id' => 'required|integer_with_max|exists:agency,id',
        ]);

        $agency = Agency::select('tax_id')->where('id', $request->input('agency_id'))->first();

        //
        $constructorDocuments = ConstructorDocument::getConstructorDocumentForAgency($agency->tax_id);

        $constructorDocs = [];
        if ($constructorDocuments->count()) {
            foreach ($constructorDocuments as $item) {
                $constructorDocs[] = [
                    'code' => $item->document_code,
                    'name' => "({$item->document_code}) {$item->document_name}"
                ];
            }
        }

        //
        $refSubDivisions = getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS, false, false, ['id', 'name', 'short_name'], false, 'sort', 'desc', ['company_tax_id' => $agency->tax_id]);

        $subDivisions = [];
        if ($refSubDivisions->count()) {
            foreach ($refSubDivisions as $item) {
                $subDivisions[] = [
                    'id' => $item->id,
                    'name' => !empty($item->short_name) ? $item->short_name : $item->name
                ];
            }
        }

        $data['constructorDocs'] = $constructorDocs;
        $data['subdivisions'] = $subDivisions;

        return responseResult('OK', '', $data);
    }

}
