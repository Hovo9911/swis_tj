<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthorizePerson\AuthorizePersonRequest;
use App\Http\Requests\AuthorizePerson\AuthorizePersonSearchRequest;
use App\Http\Requests\AuthorizePerson\ProvideRoleRequest;
use App\Models\AuthorizePerson\AuthorizePersonManager;
use App\Models\AuthorizePerson\AuthorizePersonSearch;
use App\Models\Menu\Menu;
use App\Models\Menu\MenuManager;
use App\Models\RegionalOffice\RegionalOffice;
use App\Models\Role\Role;
use App\Models\Role\RoleManager;
use App\Models\User\User;
use App\Models\UserRoles\UserRoles;
use App\Models\UserRoles\UserRolesManager;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Throwable;

/**
 * Class AuthorizePersonController
 * @package App\Http\Controllers
 */
class AuthorizePersonController extends BaseController
{
    /**
     * @var AuthorizePersonManager
     */
    protected $manager;

    /**
     * @var RoleManager
     */
    protected $roleManager;

    /**
     * AuthorizePersonController constructor.
     *
     * @param AuthorizePersonManager $manager
     * @param RoleManager $roleManager
     */
    public function __construct(AuthorizePersonManager $manager, RoleManager $roleManager)
    {
        $this->manager = $manager;
        $this->roleManager = $roleManager;
    }

    /**
     * Function to show all info
     *
     * @return View
     */
    public function index()
    {
        $roles = Role::with(['current'])->whereIn('id', Auth::user()->getUserCreatedRoles())->orWhereIn('id', [Role::CRUD_ALL_FUNCTIONALITY, Role::CRUD_ONLY_READ, Role::AUTHORIZE_USER_ADD_MY_PERMISSIONS, Role::ALLOW_AUTHORIZE_USER_ADD_MY_PERMISSIONS])->get();

        return view('authorize-person.index')->with([
            'roles' => $roles,
            'subDivisions' => RegionalOffice::getRefSubdivisions()
        ]);
    }

    /**
     * Function to all data showing in datatable
     *
     * @param AuthorizePersonSearchRequest $authorizePersonSearchRequest
     * @param AuthorizePersonSearch $authorizePersonSearch
     * @return JsonResponse
     */
    public function table(AuthorizePersonSearchRequest $authorizePersonSearchRequest, AuthorizePersonSearch $authorizePersonSearch)
    {
        $data = $this->dataTableAll($authorizePersonSearchRequest, $authorizePersonSearch);

        return response()->json($data);
    }

    /**
     * Function to show create view
     *
     * @return View
     */
    public function create()
    {
        return view('authorize-person.create');
    }

    /**
     * Function to Store data
     *
     * @param AuthorizePersonRequest $request
     * @return JsonResponse
     */
    public function store(AuthorizePersonRequest $request)
    {
        $userSSN = $request->get('authorized_ssn');
//        $userPassport = $request->get('authorized_passport');

//        $user = User::where(['ssn' => $userSSN, 'passport' => $userPassport])->first();
        $user = User::where(['ssn' => $userSSN])->first();

        if (is_null($user)) {
            $errors['authorized_ssn'] = trans('swis.user.by_ssn.not_found');

            return responseResult('INVALID_DATA', '', '', $errors);
        }

        return responseResult('OK', urlWithLng('authorize-person/' . $user->id . '/edit'));
    }

    /**
     * Function to Show current by id
     *
     * @param $lngCode
     * @param $userId
     * @param $saveMode
     * @return View
     */
    public function edit($lngCode, $userId, $saveMode)
    {
        $userRoles = UserRoles::with(['role' => function ($q) {
            $q->with(['current']);
        }, 'menu', 'authorizer'])->where(['user_id' => $userId])->userRolesByType()->active()->orderByAsc()->get();

        foreach ($userRoles as &$userRole) {
            if ($userRole->role_id == Role::HEAD_OF_COMPANY || $userRole->role_id == Role::NATURAL_PERSON) {
                $userRole->menu_id = Menu::AUTHORIZE_PERSON_MENU_ID;
                $userRole->menu = Menu::find(Menu::AUTHORIZE_PERSON_MENU_ID);
                $userRole->role_id = Role::AUTHORIZE_USER_ADD_MY_PERMISSIONS;
            }

            $userRole->isBlocked = false;
            if ($userRole->role_status == Role::STATUS_DELETED || ($userRole->available_to && $userRole->getOriginal('available_to') < currentDate())) {
                $userRole->isBlocked = true;
            }
        }

        //
        $menuIds = [
            'myApplication' => Menu::getMenuIdByName(Menu::USER_DOCUMENTS_MENU_NAME),
            'report' => Menu::getMenuIdByName(Menu::REPORTS_MENU_NAME),
            'paymentAndObligations' => Menu::getMenuIdByName(Menu::PAYMENT_AND_OBLIGATIONS_MENU_NAME),
        ];

        return view('authorize-person.edit')->with([
            'user' => User::findOrFail($userId),
            'userRoles' => $userRoles,
            'menuIds' => $menuIds,
            'menuModules' => MenuManager::menuModules(),
            'saveMode' => $saveMode
        ]);
    }

    /**
     * Function to store roles
     *
     * @param ProvideRoleRequest $request
     * @param UserRolesManager $userRolesManager
     * @return JsonResponse
     */
    public function update(ProvideRoleRequest $request, UserRolesManager $userRolesManager)
    {
        $userRolesManager->store($request->validated());

        return responseResult('OK', back()->getTargetUrl());
    }

    /**
     * Function to Return roles by menu_id,if menu_id is empty return all roles
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function indexPageSearchMenuAndRole(Request $request)
    {
        $this->validate($request, [
            'menu_id' => 'nullable|integer_with_max|exists:menu,id'
        ]);

        $data['roles'] = $this->roleManager->searchRoleByMenu($request->input('menu_id'));

        return responseResult('OK', '', $data);
    }

    /**
     * Function to get user by ssn
     *
     * @param AuthorizePersonRequest $request
     * @return JsonResponse
     */
    public function getUserBySSN(AuthorizePersonRequest $request)
    {
        $user = User::select('first_name', 'last_name')->where('ssn', $request->get('authorized_ssn'))->first();

        if (!is_null($user)) {
            return responseResult('OK', '', [
                'first_name' => $user->first_name,
                'last_name' => $user->last_name
            ]);
        }

        $errors['authorized_ssn'] = trans('swis.user.in_system.not_found');

        return responseResult('INVALID_DATA', '', '', $errors);
    }

    /**
     * Function to render role table
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws Throwable
     */
    public function renderTable(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer_with_max'
        ]);

        $data['html'] = view('authorize-person.role-table')->with(['i' => $request->get('id'), 'mode' => 'add', 'menuModules' => MenuManager::menuModules()])->render();

        return responseResult('OK', '', $data);
    }
}
