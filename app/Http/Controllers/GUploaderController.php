<?php

namespace App\Http\Controllers;

use App\GUploader;
use File;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Input;

/**
 * Class GUploaderController
 * @package App\Http\Controllers
 */
class GUploaderController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function upload(Request $request)
    {
        $conf = config('guploader');
        $confKey = $request->input('conf');
        if (empty($confKey) || !isset($conf[$confKey])) {
            Log::info('g-uploader-error-confKey-not-isset: ' . __LINE__ . ' ' . print_r(json_encode($request->all()) . '', 1));

            return response()->json([
                'status' => 'INVALID_DATA',
                'message' => trans('core.loading.invalid_data')
            ]);
        }
        $conf = $conf[$confKey];
        $guploader = new GUploader($confKey);

        $files = $request->file('files');
        if (empty($files)) {
            return response()->json([
                'status' => 'INVALID_DATA',
                'message' => trans('core.uploader.errors.file_required')
            ]);
        }

        $errors = '';
        $data = [];
        foreach ($files as $file) {
            $type = $guploader->checkType($file);
            if ($type === false) {
                $errors .= trans('core.uploader.errors.invalid_type');
                continue;
            }
            if (!$guploader->checkSize($file)) {
                $errors .= trans('core.uploader.errors.invalid_size');
                continue;
            }
            $fileData = $guploader->storeTemp($file);

            if (mb_substr($fileData['file_name'], 0, 1) == '.') {
                $errors .= trans('core.uploader.errors.invalid_name');
                continue;
            }

            $data [] = [
//                'thumbnail' => ($type == 'photo') ? url($conf['disk'] . '/' . $fileData['file_path']) : 'http://ucispace.lib.uci.edu/static/images/icons/icon-file-type-document.gif',
                'file_name' => $fileData['tmp_name'],
                'label' => $fileData['file_name']
            ];

            if (empty($errors) && !$conf['multiple']) {
                continue;
            }
        }

        if (!empty($errors)) {
            return response()->json([
                'status' => 'INVALID_DATA',
                'message' => $errors
            ]);
        }

        return response()->json([
            'status' => 'OK',
            'data' => $data
        ]);
    }
}