<?php

namespace App\Http\Middleware;

use Closure;

/**
 * Class ConstructorAuthAccess
 * @package App\Http\Middleware
 */
class ConstructorAuthAccess
{
    public function handle($request, Closure $next)
    {
        $authUser = config('global.constructor_auth_username');
        $authPassword = config('global.constructor_auth_password');

        if ($authPassword) {

            header('Cache-Control: no-cache, must-revalidate, max-age=0');
            $has_supplied_credentials = !(empty($_SERVER['PHP_AUTH_USER']) && empty($_SERVER['PHP_AUTH_PW']));
            $is_not_authenticated = (
                !$has_supplied_credentials ||
                $_SERVER['PHP_AUTH_USER'] != $authUser ||
                $_SERVER['PHP_AUTH_PW'] != $authPassword
            );

            if ($is_not_authenticated) {
                header('HTTP/1.1 401 Authorization Required');
                header('WWW-Authenticate: Basic realm="Access denied"');
                exit;
            }
        }

        return $next($request);
    }
}