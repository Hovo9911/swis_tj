<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Core\Exceptions\LocalNotSetException;
use App\Http\Controllers\Core\Language\ApplicationLanguageManager;
use Closure;
use Exception;
use Illuminate\Http\Request;

/**
 * Class SetAppLanguage
 * @package App\Http\Middleware
 */
class SetAppLanguage
{
    /**
     * @var ApplicationLanguageManager
     */
    private $applicationManager;

    /**
     * SetAppLanguage constructor.
     *
     * @param ApplicationLanguageManager $applicationManager
     */
    public function __construct(ApplicationLanguageManager $applicationManager)
    {
        $this->applicationManager = $applicationManager;
    }

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws Exception
     */
    public function handle($request, Closure $next)
    {
        if ($request->path() === '/') {
            header("HTTP/1.1 301 Moved Permanently");
            header('Location: en/', true, 301);
            exit();
        }

        try {
            $lngCode = $request->segment(1);
            if (strlen($lngCode) === 2) {
                $this->applicationManager->setApplicationLanguageByCode($request->segment(1));
                if (cLng('code') == 'hy') {
                    setlocale(LC_ALL, 'hy_AM.UTF-8');
                } elseif (cLng('code') == 'ru') {
                    setlocale(LC_ALL, 'ru_RU.UTF-8');
                } elseif (cLng('code') == 'en') {
                    setlocale(LC_ALL, 'en_US.UTF-8');
                }
            } else {
                abort(404);
            }
        } catch (LocalNotSetException $e) {
            abort(404);
        }

        return $next($request);
    }
}
