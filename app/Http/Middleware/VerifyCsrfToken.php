<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

/**
 * Class VerifyCsrfToken
 * @package App\Http\Middleware
 */
class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '*/api/v1/payment/add',
        '*/api/v1/payment/status',
        '*/api/v1/payment/check-tin',
        '*/api/v1/alif/add',
        '*/api/v1/fn/*',
        '*/api/v1/subapplication/info',
        '*/api/v1/document_archive/info'
    ];
}
