<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class ReferenceFormAccess
 * @package App\Http\Middleware
 */
class ReferenceFormAccess
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user()->hasReferenceFormModuleAccess()) {
            return abort('404');
        }

        return $next($request);
    }
}
