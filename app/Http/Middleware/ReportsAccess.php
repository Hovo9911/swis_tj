<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class ReportsAccess
 * @package App\Http\Middleware
 */
class ReportsAccess
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!in_array($request->route('code'), Auth::user()->getUserReports()->pluck('code')->toArray()) && $request->method() != 'POST') {
            return abort('404');
        }

        return $next($request);
    }
}
