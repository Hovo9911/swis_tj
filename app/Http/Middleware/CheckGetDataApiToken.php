<?php

namespace App\Http\Middleware;

use App\Models\ApiProvider\ApiProvider;
use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class CheckGetDataApiToken
 * @package App\Http\Middleware
 */
class CheckGetDataApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return JsonResponse|mixed
     */
    public function handle($request, Closure $next)
    {
        $apiProvider = new ApiProvider();

        $provider = $apiProvider->getProvider($request->header('token'));

        if (is_null($provider)) {
            return response()->json([
                'status' => 'ERROR',
                'description' => trans('swis.api.error.invalid_data') //'Invalid token'
            ]);
        }

//        $allowIP = $apiProvider->checkIpAddressByProvider($provider, request()->ip());
//        if (!$allowIP) {
//            return response()->json([
//                'status' => 'PERMISSION_DENIED',
//                'description' => trans('swis.api.error.permission_denied') // 'permission denied'
//            ]);
//        }

        return $next($request);
    }
}
