<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

/**
 * Class Logger
 * @package App\Http\Middleware
 */
class Logger
{
    /**
     * @var $logFileName
     */
    private $logFileName = '';

    /**
     * Logger constructor.
     */
    public function __construct()
    {
        $date = currentDate();
        $this->logFileName = "swis-audit-{$date}.log";
    }

    /**
     * @var array
     */
    protected $unAuthorizeRequests = [
        'loginPost' => 'login',
        'logout' => 'logout',
        'resetPasswordPost' => 'try reset password',
        'set-new-password' => 'set new password'
    ];

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route = $request->route();
        $prefix = $route->getPrefix();
        $authCheck = Auth::check();
        $withPrefix = $this->getIsWithPrefix($prefix);
        $requestAll = $request->all();
        if (isset($requestAll['_token'])) {
            unset($requestAll['_token']);
        }

        if ($authCheck) {
            $log = $request->route()->getAction('log');
            if ($withPrefix) {
                if (!empty($log)) {
                    if (strpos($log['action'], '{') !== false && strpos($log['action'], '}') !== false) {
                        $log['action'] = $route->parameter(str_replace(["{", "}"], "", $log['action']));
                    }
                    if ($route->getAction('moduleName') == 'single-application') {
                        if ($log['action'] == 'create') {
                            Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), $log['action'], '', $route->parameters()), JSON_UNESCAPED_UNICODE));
                        } elseif ($log['action'] == 'addProduct') {
                            Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), $log['action'], $request->header('sNumber'), null, null, true, null, '', $request->all()), JSON_UNESCAPED_UNICODE));
                        } elseif ($log['action'] == 'updateProduct') {
                            $manager = new $log['manager'];
                            $oldData = $manager->getLogData($request->input('id'));
                            $response = $next($request);
                            $newData = $manager->getLogData($request->input('id'));
                            Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), $log['action'], $request->header('sNumber'), $requestAll, null, true, null, $oldData, $newData), JSON_UNESCAPED_UNICODE));
                            return $response;
                        } elseif ($log['action'] == 'update') {
                            $manager = $route->getAction('manager');
                            $manager = new $manager;
                            $oldData = $manager->getLogData($request->header('sNumber'));
                            $response = $next($request);
                            $newData = $manager->getLogData($request->header('sNumber'));
                            Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), $request->input('status_type'), $request->header('sNumber'), $requestAll, null, true, null, $oldData, $newData), JSON_UNESCAPED_UNICODE));
                            return $response;
                        } elseif ($log['action'] == 'clone') {
                            Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), $log['action'], $route->parameter('number'), $requestAll), JSON_UNESCAPED_UNICODE));
                        } elseif ($log['action'] == 'edit') {
                            Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), $route->parameter('viewType'), $route->parameter('number'), $route->parameters()), JSON_UNESCAPED_UNICODE));
                        } else {
                            Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), $log['action'], $request->header('sNumber'), $requestAll), JSON_UNESCAPED_UNICODE));
                        }
                    } else {
                        switch ($log['action']) {
                            case 'setRole':
                                Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), $log['action'], $request->input('user_role'), $requestAll), JSON_UNESCAPED_UNICODE));
                                break;
                            case 'search':
                                $data = $requestAll;
                                if (array_key_exists("f", $data)) {
                                    Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), $log['action'], '', $data['f']), JSON_UNESCAPED_UNICODE));
                                }
                                break;
                            case 'xls':
                                $data = $requestAll;
                                if ($data) {
                                    Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), $log['action'], '', $data), JSON_UNESCAPED_UNICODE));
                                }
                                break;
                            case 'edit':
                                $this->appendEditOrView($route, $log);
                                break;
                            case 'create':
                                if (!empty($route->parameter('id'))) {
                                    Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), $log['action'], '', null, null, true, $route->parameter('id')), JSON_UNESCAPED_UNICODE));
                                } else {
                                    Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), $log['action'], '', null), JSON_UNESCAPED_UNICODE));
                                }
                                break;
                            case 'view':
                                $this->appendEditOrView($route, $log);
                                break;
                            case 'clone':
                                Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), $log['action'], $route->parameter('id'), null), JSON_UNESCAPED_UNICODE));
                                break;
                            case 'export':
                                Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), $log['action'], '', null, null, true, $route->parameter('id')), JSON_UNESCAPED_UNICODE));
                                break;
                            case 'import':
                                Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), $log['action'], '', null, null, true, $route->parameter('id')), JSON_UNESCAPED_UNICODE));
                                break;
                            case 'delete':
                                Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), $log['action'], '', $request->input('deleteItems')), JSON_UNESCAPED_UNICODE));
                                break;
                            case 'addDocument':
                                Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), $log['action'], $route->parameter('id2'), $requestAll, null, true, $route->parameter('id')), JSON_UNESCAPED_UNICODE));
                                break;
                            case 'deleteDocument':
                                Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), $log['action'], $request->input('docId'), null, null, true, $route->parameter('id')), JSON_UNESCAPED_UNICODE));
                                break;
                            case 'receive':
                                Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), $log['action'], $route->parameter('id2'), null, null, true, $route->parameter('id')), JSON_UNESCAPED_UNICODE));
                                break;
                            case 'updateDictionary':
                                $manager = $route->getAction('manager');
                                $manager = new $manager;
                                $oldData = $manager->getLogData($request->input('key'));
                                $response = $next($request);
                                $newData = $manager->getLogData($request->input('key'));
                                Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), 'update', $route->parameter('id'), null, null, true, null, $oldData, $newData), JSON_UNESCAPED_UNICODE));
                                return $response;
                            case 'updateRisk':
                                $manager = $route->getAction('manager');
                                $manager = new $manager;
                                $oldData = $manager->getLogData($request->input('id'));
                                Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), 'update', $route->parameter('id'), null, null, true, null, $oldData, $requestAll), JSON_UNESCAPED_UNICODE));
                                break;
                            case 'updateVersion':
                                $manager = $route->getAction('manager');
                                $manager = new $manager;
                                list($oldData, $code) = $manager->getLogData($route->parameter('id'), $route->parameter('id2'));
                                $response = $next($request);
                                list($newData, $code) = $manager->getLogData($route->parameter('id'), $route->parameter('id2'), $code);
                                Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), 'update', $route->parameter('id2'), null, null, true, $route->parameter('id'), $oldData, $newData), JSON_UNESCAPED_UNICODE));
                                return $response;
                                break;
                            case 'update':
                                $manager = $route->getAction('manager');
                                $manager = new $manager;
                                $id = !is_null($route->parameter('id')) ? $route->parameter('id') : $request->input('id');

                                $oldData = $manager->getLogData($id, $route->parameter('id2'));
                                $response = $next($request);
                                $newData = $manager->getLogData($id, $route->parameter('id2'));

                                if (!empty($route->parameter('id2'))) {
                                    Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), $log['action'], $route->parameter('id2'), null, null, true, $id, $oldData, $newData), JSON_UNESCAPED_UNICODE));
                                } else {
                                    Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), $log['action'], $id, '', null, true, null, $oldData, $newData), JSON_UNESCAPED_UNICODE));
                                }
                                return $response;
                            case 'store':
//                                $newData = $requestAll;
//                                $id = $route->parameter('id');
//
//                                if (!empty($route->parameter('id2'))) {
//                                    Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), $log['action'], $route->parameter('id2'), null, null, true, $id, '', $newData), JSON_UNESCAPED_UNICODE));
//                                } else {
//                                    Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), $log['action'], $id, '',  null, true, null, '', $newData), JSON_UNESCAPED_UNICODE));
//                                }
                                break;

                            case 'open-profile':
                                Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), $log['action']), JSON_UNESCAPED_UNICODE));
                                break;

                            case 'store-profile':
                                $manager = $route->getAction('manager');
                                $manager = new $manager;
                                $id = Auth::id();
                                $data = $request->all();

                                $oldData = $manager->getLogData($id);
                                $response = $next($request);
                                $newData = $manager->getLogData($id);
                                $newData->password_was_changed = (isset($data['old_password']) && isset($data['password']));

                                Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), $log['action'], $id, '', null, true, null, $oldData, $newData), JSON_UNESCAPED_UNICODE));

                                return $response;

                        }
                    }
                }
            } else {
                if ($route->getName() == 'logout' && $request->isMethod('post')) {
                    Storage::append($this->logFileName, json_encode($this->getLogData($route, 'Authentication', 'logout'), JSON_UNESCAPED_UNICODE));
                } else {
                    if (!empty($log) && $request->isMethod('post')) {
                        Storage::append($this->logFileName, json_encode($this->getLogData($route, 'Authentication'), JSON_UNESCAPED_UNICODE));
                    }
                }
            }
        } elseif (!$authCheck && !$withPrefix) {
            if ($request->isMethod('post') && $route->getName()) {
                $username = !empty($request->input('email')) ? $request->input('email') : $request->input('search');

                if ($route->getName() == 'reset-password' || $route->getName() == 'resetPasswordPost' || $route->getName() == 'reset' || $route->getName() == 'set-new-password') {
                    Storage::append($this->logFileName, json_encode($this->getLogData($route, 'Authentication'), JSON_UNESCAPED_UNICODE));
                } else {
                    $credentials = ['username' => $request->input('email'), 'password' => $request->input('password')];
                    Storage::append($this->logFileName, json_encode($this->getLogData($route, 'Authentication', null, '', '', $username, Auth::validate($credentials)), JSON_UNESCAPED_UNICODE));
                }
            }
        }

        return $next($request);
    }

    /**
     * @param $prefix
     * @return string
     */
    private function getIsWithPrefix($prefix)
    {
        return !($prefix == '/{lngCode}');
    }

    /**
     * @param $route
     * @param $module
     * @param null $action
     * @param string $id
     * @param string $data
     * @param $username
     * @param bool $status
     * @param null $parentId
     * @param string $oldData
     * @param string $newData
     * @return array
     */
    private function getLogData($route, $module, $action = null, $id = '', $data = null, $username = null, $status = true, $parentId = null, $oldData = '', $newData = '')
    {
        return [
            'ip' => request()->ip(),
            'date' => date(config('swis.date_time_format')),
            'module' => $module,
            'action' => !is_null($action) ? $action : $this->unAuthorizeRequests[$route->getName()],
            'user_agent' => request()->userAgent(),
            'status' => $status,
            'user_id' => !empty(Auth::id()) ? Auth::id() : null,
            'username' => !empty($username) ? $username : optional(Auth::user())->username,
            'companyTaxId' => !empty(Auth::check()) ? Auth::user()->companyTaxId() : null,
            'row_id' => !empty($id) ? $id : '',
            'parent_id' => !empty($parentId) ? $parentId : '',
            'data' => !empty($data) ? $data : null,
            'oldData' => !empty($oldData) ? $oldData : null,
            'newData' => !empty($newData) ? $newData : null,
        ];
    }

    /**
     * @param $route
     * @param $log
     */
    private function appendEditOrView($route, $log)
    {
        if (!empty($route->parameter('id2'))) {
            Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), $log['action'], $route->parameter('id2'), '', null, true, $route->parameter('id'))));
        } else {
            $id = $route->parameter('id');

            Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), $log['action'], $id, '')));
        }
    }

    /**
     * Function to terminate log
     *
     * @param $request
     * @param $response
     */
    public function terminate($request, $response)
    {
        $route = $request->route();
        $prefix = $route->getPrefix();
        $authCheck = Auth::check();
        $withPrefix = $this->getIsWithPrefix($prefix);
        $requestAll = $request->all();
        if (isset($requestAll['_token'])) {
            unset($requestAll['_token']);
        }

        if ($authCheck) {
            $log = $request->route()->getAction('log');
            if ($withPrefix) {
                if (!empty($log)) {
                    if (strpos($log['action'], '{') !== false && strpos($log['action'], '}') !== false) {
                        $log['action'] = $route->parameter(str_replace(["{", "}"], "", $log['action']));
                    }
                    if ($route->getAction('moduleName') != 'single-application') {
                        if ($log['action'] == 'store') {
                            $newData = $requestAll;
                            $id = $route->parameter('id');
                            $responseData = $response->getData();
                            if (isset($responseData->status) && $responseData->status == 'OK') {
                                if (!empty($route->parameter('id2'))) {
                                    Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), $log['action'], $responseData->data->id ?? $route->parameter('id2'), null, null, true, $id, '', $newData), JSON_UNESCAPED_UNICODE));
                                } else {
                                    Storage::append($this->logFileName, json_encode($this->getLogData($route, $route->getAction('moduleName'), $log['action'], $responseData->data->id ?? $id, null, null, true, null, '', $newData), JSON_UNESCAPED_UNICODE));
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}
