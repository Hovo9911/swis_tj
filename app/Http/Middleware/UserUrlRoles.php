<?php

namespace App\Http\Middleware;

use App\Models\BaseModel;
use App\Models\Menu\MenuManager;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class UserUrlRoles
 * @package App\Http\Middleware
 */
class UserUrlRoles
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        $t1 = microtime(1);
//        DB::enableQueryLog();

        if (!Auth::check()) {
            view()->share('menus', []);

            return $next($request);
        }

        if (is_null(Auth::user()->getCurrentUserRole())) {

            if (!currentRouteNameIs(['roleSet', 'changeRole'])) {
                return redirect(urlWithLng('role/set'));
            }

        } else {

            if (currentRouteNameIs('roleSet')) {
                return redirect(urlWithLng('dashboard'));
            }

            //
            $dashboardUrl = urlWithLng('dashboard');
            $authUserCurrentRoleMenus = Auth::user()->getCurrentRoleMenus();
            $currentUrlMenu = Auth::user()->getCurrentUrlMenu();

            if (!Auth::user()->hasAccessToMenu($authUserCurrentRoleMenus, $currentUrlMenu)) {

                session()->put('hasNoRole', true);

                if ($request->ajax()) {
                    $errors['hasNoRoleAjax'] = true;
                    $errors['url'] = $dashboardUrl;

                    return responseResult('INVALID_DATA', '', '', $errors);
                }

                return redirect($dashboardUrl);
            }

            //
            $needToUpdateCurrentRoleSession = Auth::user()->hasToUpdateCurrentRoleSession($currentUrlMenu);

            if ($needToUpdateCurrentRoleSession) {
                session()->put('needUpdateUserRolesSession', true);

                if ($request->ajax()) {
                    $errors['needRefreshUserRolesSession'] = true;
                    $errors['url'] = $dashboardUrl;

                    return responseResult('INVALID_DATA', '', '', $errors);
                }

                return redirect($dashboardUrl);
            }

            // If user has access only view (no edit,no store,no update)
            if ($request->route()->getAction('checkAccessForUpdate')) {
                $viewType = $request->route('viewType');

                if ((is_null($viewType) || $viewType != BaseModel::VIEW_MODE_VIEW) && !Auth::user()->hasAccessToEditModule()) {

                    if ($request->ajax()) {
                        $errors['hasNoRoleAjax'] = true;
                        $errors['url'] = urlWithLng('404');

                        return responseResult('INVALID_DATA', '', '', $errors);
                    }

                    abort(404);
                }
            }

            //
            if (!$request->isMethod('post')) {
                view()->share('menus', MenuManager::menuTree($authUserCurrentRoleMenus));
            }

        }

        /*$query = DB::getQueryLog();

             echo '<pre>';
             print_r($query);die;*/
//        $t2 = microtime(1);
//        die($t2 - $t1);
        return $next($request);
    }
}
