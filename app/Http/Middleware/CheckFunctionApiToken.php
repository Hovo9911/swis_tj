<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;

/**
 * Class CheckFunctionApiToken
 * @package App\Http\Middleware
 */
class CheckFunctionApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param $request
     * @param Closure $next
     * @return JsonResponse|mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->input('token') !== env("FUNCTION_API_HASH")) {
            return response()->json([
                'status' => 'error',
                'code' => 403,
                'description' => 'Token is not registered',
            ]);
        }

        return $next($request);
    }
}
