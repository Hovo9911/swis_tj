<?php

namespace App\Http\Middleware;

use App\Models\UserSession\UserSession;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class VerifiedUser
 * @package App\Http\Middleware
 */
class VerifiedUser
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {

            if (config('global.auth_verify.email.mode')) {

                if (!Auth::user()->is_email_verified) {
                    return redirect(urlWithLng('email/verify'));
                }
            }

            $session = UserSession::where('session_id', session()->getId())->where('user_id', Auth::user()->id)->first();
            if (!$session) {
                Auth::logout();
                return redirect(urlWithLng('login'));
            }

            /* if (config('global.auth_verify.phone.mode')) {

                 if (!Auth::user()->is_phone_verified) {
                     return redirect(urlWithLng('phone-number/verify'));
                 }
             }*/
        }

        return $next($request);
    }
}
