<?php

namespace App\Http\Middleware;

use App\Models\Pays\PaysManager;
use Closure;
use Illuminate\Http\Request;

/**
 * Class CheckApiParams
 * @package App\Http\Middleware
 */
class CheckApiParams
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $manager = new PaysManager();

        $provider = $manager->getProvider($request->input('token'));

        if (is_null($provider)) {
            return response()->json([
                'status' => 'error',
                'code' => 403,
                'description' => 'Token is not registered',
            ]);
        }

        $allowIP = $manager->checkIpAddressByProvider($provider, request()->ip());
        if (!$allowIP) {
            return response()->json([
                'status' => 'error',
                'code' => 403,
                'description' => 'IP not allowed',
            ]);
        }


        return $next($request);
    }
}
