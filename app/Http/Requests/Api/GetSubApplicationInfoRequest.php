<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Request;

class GetSubApplicationInfoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number' => 'required|string|min:1',
            'type' => 'required|string|min:1',
            'tax_ids' => "required|array|min:1",
            'tax_ids.importer' => "required_without_all:tax_ids.exporter,tax_ids.applicant|string|min:1",
            'tax_ids.exporter' => "required_without_all:tax_ids.applicant,tax_ids.importer|string|min:1",
            'tax_ids.applicant' => "required_without_all:tax_ids.exporter,tax_ids.importer|string|min:1",
        ];
    }
}
