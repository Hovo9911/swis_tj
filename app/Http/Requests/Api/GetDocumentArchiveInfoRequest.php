<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Request;

class GetDocumentArchiveInfoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $accessToken = $this->input('access_token');

        $data = [
            "access_token" => 'nullable|string|min:1',
            'number' => 'nullable|string|min:1',
            'type' => 'nullable|string|min:1',
            "name" => "nullable|string|min:1",
            "description" => "nullable|string|min:1",
            "provision_date" => "nullable|date"
        ];

        if (!isset($accessToken)) {
            $data['tax_ids'] = "required|array|min:1";
            $data['tax_ids.creator'] = "required_without_all:tax_ids.holder|string|min:1";
            $data['tax_ids.holder'] = "required_without_all:tax_ids.creator|string|min:1";
        } else {
            $data['tax_ids'] = "nullable|array|min:1";
        }

        return $data;
    }

}
