<?php

namespace App\Http\Requests\ProfileSettings;

use App\Http\Requests\Request;

/**
 * Class ProfileSettingsRequest
 * @package App\Http\Requests\ProfileSettings
 */
class ProfileSettingsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'address' => 'nullable|string|max:45',
            'city_village' => 'nullable|string|max:45',
            'post_address' => 'nullable|string|max:45',
            'phone_number' => config('global.phone_number.required') ? 'required|' : 'nullable|' . 'phone_number_validator',
            'email' => 'required|email|max:250',
            'lng_id' => 'required|integer_with_max|exists:languages,id',
            'sms_notification' => 'nullable|integer|max:1',
            'email_notification' => 'nullable|integer|max:1',
            'old_password' => 'validate_old_password'
        ];

        if (!empty($this->request->get('old_password'))) {
            $rules['password'] = 'required|confirmed|validate_password|set_different_passwords';
            $rules['password_confirmation'] = 'required|validate_password|set_different_passwords';
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => trans('core.base.field.required'),
            'email.max' => trans('core.base.field.max_characters', ['max' => 250]),
            'email.email' => trans('core.loading.email_valid'),
            'email.*' => trans('core.loading.invalid_data'),

            'old_password.validate_old_password' => trans('core.base.field.current_password_not_correct'),

            'password.required' => trans('core.base.field.required'),
            'password.confirmed' => trans('core.base.password.dont_match'),
            'password.validate_password' => trans('swis.profile_settings.password_regex_incorrect', ['min' => config('global.password_characters.min'), 'max' => config('global.password_characters.max')]),
            'password.set_different_passwords' => trans('core.base.field.need_set_different_password.message'),
            'password.*' => trans('core.loading.invalid_data'),

            'password_confirmation.required' => trans('core.base.field.required'),
            'password_confirmation.confirmed' => trans('core.base.password.dont_match'),
            'password_confirmation.validate_password' => trans('swis.profile_settings.password_regex_incorrect', ['min' => config('global.password_characters.min'), 'max' => config('global.password_characters.max')]),
            'password_confirmation.set_different_passwords' => trans('core.base.field.need_set_different_password.message'),
            'password_confirmation.*' => trans('core.loading.invalid_data'),

            'city_village.max' => trans('core.base.field.max_characters', ['max' => 45]),
            'city_village.*' => trans('core.loading.invalid_data'),

            'address.max' => trans('core.base.field.max_characters', ['max' => 45]),
            'address.*' => trans('core.loading.invalid_data'),

            'post_address.max' => trans('core.base.field.max_characters', ['max' => 45]),
            'post_address.*' => trans('core.loading.invalid_data'),

            'sms_notification.*' => trans('core.loading.invalid_data'),
            'email_notification.*' => trans('core.loading.invalid_data'),
        ];
    }
}
