<?php

namespace App\Http\Requests\CustomsPortal;

use App\Http\Requests\Request;

/**
 * Class CustomsPortalSearchRequest
 * @package App\Http\Requests\CustomsPortal
 */
class CustomsPortalSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.agency' => 'integer_with_max|exists:agency,id',
            'f.subdivision' => 'integer_with_max|exists:reference_agency_subdivisions,id',
            'f.constructor_document' => 'string_with_max|exists:constructor_documents,document_code',
            'f.regular_number' => 'string_with_max',
            'f.importer_value' => 'string_with_max',
            'f.exporter_value' => 'string_with_max',
            'f.applicant_value' => 'string_with_max',
            'f.sub_application_received_at_date_start' => 'date',
            'f.sub_application_received_at_date_end' => 'date',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
