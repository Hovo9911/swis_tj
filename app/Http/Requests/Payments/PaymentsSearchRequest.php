<?php

namespace App\Http\Requests\Payments;

use App\Http\Requests\Request;

/**
 * Class AgencySearchRequest
 * @package App\Http\Requests\Agency
 */
class PaymentsSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.service_provider_name' => 'string_with_max',
            'f.saf_number' => 'string_with_max',
            'f.sub_application_number' => 'string_with_max',
            'f.agency' => 'string_with_max|exists:agency,tax_id',
            'f.constructor_document' => 'integer_with_max|exists:constructor_documents,id',
            'f.obligation_type' => 'integer_with_max|exists:reference_obligation_type,id',
            'f.transaction_date_start' => 'date',
            'f.transaction_date_end' => 'date',
            'f.transaction_username' => 'string_with_max',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
