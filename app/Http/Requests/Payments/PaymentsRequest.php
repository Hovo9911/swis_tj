<?php

namespace App\Http\Requests\Payments;

use App\Http\Requests\Request;

/**
 * Class PaymentsRequest
 * @package App\Http\Requests\Payments
 */
class PaymentsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required|numeric|min:0|max:1000000000',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'amount.required' => trans('core.base.field.required'),
            'amount.*' => trans('core.loading.invalid_data')
        ];
    }

}
