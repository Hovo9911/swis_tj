<?php

namespace App\Http\Requests\NotificationTemplates;

use App\Http\Requests\Request;

/**
 * Class NotificationTemplatesSearchRequest
 * @package App\Http\Requests\NotificationTemplates
 */
class NotificationTemplatesSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.id' => 'integer_with_max',
            'f.name' => 'string_with_max',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
