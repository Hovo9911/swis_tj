<?php

namespace App\Http\Requests\NotificationTemplates;

use App\Http\Requests\Request;

/**
 * Class NotificationTemplatesRequest
 * @package App\Http\Requests\NotificationTemplates
 */
class NotificationTemplatesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|min:3|max:40',
            'show_status' => 'required|integer_with_max',
            'locked' => 'nullable|in:0,1'
        ];

        foreach ($this->request->get('ml') as $key => $ml) {

            $rules['ml.' . $key . '.email_subject'] = 'required|max:255';
            $rules['ml.' . $key . '.email_notification'] = 'required|not_in:<p>&nbsp;</p>';
            $rules['ml.' . $key . '.in_app_notification'] = 'required|max:1000';
            $rules['ml.' . $key . '.show_status'] = 'required|in:1,2';

            if (config('swis.sms_send')) {
                $rules['ml.' . $key . '.sms_notification'] = 'required|max:512';
            }
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        $messages = [
            'name.max' => trans('core.base.field.max_characters', ['max' => 40]),
            'name.min' => trans('core.base.field.min_characters', ['min' => 3]),
            'name.*' => trans('core.loading.invalid_data'),
        ];

        foreach ($this->request->get('ml') as $key => $ml) {
            $messages['ml.' . $key . '.email_notification.not_in'] = trans('core.base.field.required');
            $messages['ml.' . $key . '.in_app_notification.max'] = trans('core.base.field.max_characters', ['max' => 1000]);
            $messages['ml.' . $key . '.sms_notification.max'] = trans('core.base.field.max_characters', ['max' => 512]);
        }

        return $messages;
    }
}
