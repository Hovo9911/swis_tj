<?php

namespace App\Http\Requests\PaymentAndObligation;

use App\Http\Requests\Request;

/**
 * Class PaymentAndObligationSearchRequest
 * @package App\Http\Requests\PaymentAndObligation
 */
class PaymentAndObligationSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.obligation_created_date_start' => 'date',
            'f.obligation_created_date_end' => 'date',
            'f.saf_number' => 'string_with_max',
            'f.sub_application_number' => 'string_with_max',
            'f.applicant_tin' => 'string_with_max',
            'f.applicant' => 'string_with_max',
            'f.tin_importer_exporter' => 'string_with_max',
            'f.importer_exporter' => 'string_with_max',
            'f.agency' => 'string_with_max|exists:agency,tax_id',
            'f.subdivision_id' => 'string_with_max|exists:reference_agency_subdivisions,id',
            'f.document' => 'string_with_max|exists:constructor_documents,document_code',
            'f.obligation_type' => 'string_with_max|exists:reference_obligation_type,code',
            'f.budget_line' => 'integer_with_max',
            'f.pay' => 'string_with_max',
            'f.obligation_pay_date_start' => 'date',
            'f.obligation_pay_date_end' => 'date',
            'f.balance_start' => 'between:0,99.99',
            'f.balance_end' => 'between:0,99.99',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
