<?php

namespace App\Http\Requests\Password;

use App\Http\Requests\Request;

/**
 * Class PasswordRequest
 * @package App\Http\Requests\Password
 */
class PasswordRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|confirmed|validate_password',
            'password_confirmation' => 'required',
            'token' => 'required'
        ];
    }

    /**
     * @return array|mixed
     */
    public function messages()
    {
        return [
            'password.required' => trans('core.base.field.required'),
            'password.validate_password' => trans('swis.profile_settings.password_regex_incorrect', ['min' => config('global.password_characters.min'),'max' => config('global.password_characters.max')]),
            'password.confirmed' => trans('core.base.password.confirmed'),
            'password.*' => trans('core.loading.invalid_data'),
        ];
    }
}
