<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

/**
 * Class Request
 * @package App\Http\Requests
 */
abstract class Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function validationData()
    {
        $data = parent::validationData();
        if ($this->isJson()) {
            $json = json_decode($this->getContent(), 1);
            $data = !is_null($json) ? $json : $data;
        }
        return $data;
    }


    /**
     * failedValidation
     *
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = [];
        foreach ($validator->errors()->getMessages() as $key => $error) {
            $key = str_replace(".", "-", $key);
            $errors[$key] = implode(', ', $error);
        }
        throw new HttpResponseException(response()->json([
            'status' => 'INVALID_DATA',
            'errors' => $errors
        ], 200));
    }


//    /**
//     * Get the validated data from the request.
//     *
//     * @return array
//     */
//    public function validated()
//    {
//        $rules = $this->container->call([$this, 'rules']);
//
//        foreach(collect($rules)->keys() as $key=>$value){
//            if(!str_contains($value, '.') && !isset($_POST[$value])){
//                $this->merge([$value => 'default value']);
//            }
//        }
//
//        $data = $this->only(collect($rules)->keys()->map(function ($rule) {
//            return str_contains($rule, '.') ? explode('.', $rule)[0] : $rule;
//        })->unique()->toArray());
//
//        return $data;
//    }
}