<?php

namespace App\Http\Requests\Reports;

use App\Http\Requests\Request;
use App\Models\Reports\Reports;
use App\Models\SingleApplication\SingleApplication;
use Illuminate\Support\Facades\Auth;

/**
 * Class ReportsRequest
 * @package App\Http\Requests\Reports
 */
class ReportsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = $this->request->all();

        $reportCode = $data['report_code'];

        switch ($reportCode) {
            case Reports::REPORT_SW_ADMIN_REPORT_1_CODE:
                $rules = [
                    "state_border_head" => "nullable|string_with_max",
                    "head_name" => "nullable|string_with_max"
                ];
                break;
            case Reports::REPORT_SW_ADMIN_REPORT_2_CODE:
                $rules = [
                    "regime" => "required|array|in:" . implode(',', SingleApplication::REGIMES),
                    "hs_code" => "nullable|string_with_max",
                    "document_id" => "nullable|array|exists:constructor_documents,id",
                    "document_status" => "nullable|array|exists:constructor_states,id",
                    "sub_division_ids" => "nullable|array|exists:reference_agency_subdivisions,id",
                    "applicant_type_value" => "nullable|string_with_max",
                    "request_start_date_start" => "nullable|date",
                    "request_start_date_end" => "nullable|date"
                ];
                break;
            case Reports::REPORT_SW_ADMIN_REPORT_3_CODE:
                $rules = [
                    "regime" => "required|array|in:" . implode(',', SingleApplication::REGIMES),
                    "hs_code" => "nullable|string_with_max",
                    "document_id" => "nullable|array|exists:constructor_documents,id",
                    "document_status" => "nullable|array|exists:constructor_states,id",
                    "sub_division_ids" => "nullable|array|exists:reference_agency_subdivisions,id",
                    "applicant_type_value" => "nullable|string_with_max",
                    "request_start_date_start" => "nullable|date",
                    "request_start_date_end" => "nullable|date",
                    "registration_start_date_start" => "nullable|date",
                    "registration_start_date_end" => "nullable|date",
                    "registration_number" => "nullable|string_with_max"
                ];
                break;
            case Reports::REPORT_SW_ADMIN_REPORT_4_CODE:
                $rules = [
                    "regime" => "required|array|in:" . implode(',', SingleApplication::REGIMES),
                    "company_payer" => "nullable|string_with_max",
                    "saf_number" => "nullable|string_with_max",
                    "natural_person_payer" => "nullable|string_with_max"
                ];
                break;
            case Reports::REPORT_SW_ADMIN_REPORT_5_CODE:
                $rules = [
                    "document_id" => "required|integer_with_max|exists:constructor_documents,id",
                    "document_status" => "nullable|array|exists:constructor_states,id",
                    "hs_code" => "nullable|string_with_max",
                    "regime" => "nullable|array|in:" . implode(',', SingleApplication::REGIMES),
                    "sub_application_number" => "nullable|string_with_max",
                    "sub_division_ids" => "nullable|array|exists:reference_agency_subdivisions,id"
                ];

                if (Auth::user()->isSwAdmin()) {
                    $rules["agency"] = "required|exists:agency,tax_id";
                }

                break;
            case Reports::REPORT_SW_ADMIN_REPORT_6_CODE:
                $rules = [
                    "regime" => "required|array|in:" . implode(',', SingleApplication::REGIMES),
                    "saf_number" => "nullable|string_with_max",
                    "broker_name_or_tin" => "required|string_with_max"
                ];
                break;
            case Reports::REPORT_SW_ADMIN_REPORT_7_CODE:
                $rules = [
                    "regime" => "nullable|array|in:" . implode(',', SingleApplication::REGIMES),
                    "applicant" => "nullable|string_with_max",
                ];
                break;
            case Reports::REPORT_GLOBAL_REPORT_1_CODE:
                $rules = [
                    "document_id" => "required|integer_with_max|exists:constructor_documents,id",
                    "document_status" => "required|integer_with_max|exists:constructor_states,id",
                    "hs_code" => "nullable|string_with_max",
                    "regime" => "required|array|in:" . implode(',', SingleApplication::REGIMES)
                ];
                break;
            case Reports::REPORT_GLOBAL_REPORT_2_CODE:
                $rules = [
                    "document_id" => "required|integer_with_max|exists:constructor_documents,id",
                    "document_status" => "required|array|exists:constructor_states,id",
                    "hs_code" => "nullable|string_with_max",
                    "regime" => "required|array|in:" . implode(',', SingleApplication::REGIMES),
                    "sub_division_ids" => "nullable|array|exists:reference_agency_subdivisions,id"
                ];
                break;
            case Reports::REPORT_GLOBAL_REPORT_3_CODE:
                $rules = [
                    "document_id" => "required|integer_with_max|exists:constructor_documents,id",
                    "document_status" => "required|array|exists:constructor_states,id",
                    "hs_code" => "nullable|string_with_max",
                    "regime" => "required|array|in:" . implode(',', SingleApplication::REGIMES),
                    "tax_id" => "nullable",
                    "country" => "nullable|integer_with_max|exists:reference_countries,id"
                ];
                break;
            case Reports::REPORT_GLOBAL_REPORT_4_CODE:
                $rules = [
                    "start_date_start" => "nullable|date",
                    "end_date_start" => "nullable|date",
                    "company_name" => "nullable|string_with_max",
                    "user_name" => "nullable|string_with_max",
                    "user_ssn" => "nullable|string_with_max",
                    "transaction_id" => "nullable|string_with_max",
                    "payment_provider_id" => "nullable|integer_with_max",
                    "id" => "nullable|integer_with_max",
                ];
                break;
            case Reports::REPORT_BROKER_REPORT_1_CODE:
                $rules = [
                    "document_id" => "required|integer_with_max",
                    "document_status" => "nullable|integer_with_max|exists:constructor_states,id",
                    "hs_code" => "nullable|string_with_max",
                    "regime" => "required|array|in:" . implode(',', SingleApplication::REGIMES),
                    "tax_id" => "nullable",
                    "phone_number" => "nullable",
                    "email" => "nullable",
                    "country" => "nullable|integer_with_max|exists:reference_countries,id",
                    "sub_division_ids" => "nullable|array|exists:reference_agency_subdivisions,id",
                    "submitting_date_start" => "nullable|date",
                    "submitting_date_end" => "nullable|date",
                    "start_date_start" => "nullable|date",
                    "start_date_end" => "nullable|date"
                ];
                break;
            case Reports::REPORT_BROKER_REPORT_2_CODE:
                $rules = [
                    "document_id" => "required|integer_with_max|exists:constructor_documents,id",
                    "document_status" => "nullable|integer_with_max|exists:constructor_states,id",
                    "hs_code" => "nullable|string_with_max",
                    "regime" => "required|array|in:" . implode(',', SingleApplication::REGIMES),
                    "tax_id" => "nullable",
                    "country" => "nullable|integer_with_max|exists:reference_countries,id",
                    "sub_division_ids" => "nullable|array|exists:reference_agency_subdivisions,id"
                ];
                break;
            case Reports::REPORT_BROKER_REPORT_3_CODE:
                $rules = [
                    "document_id" => "nullable|integer_with_max|exists:constructor_documents,id",
                    "document_status" => "nullable|integer_with_max|exists:constructor_states,id",
                    "regime" => "required|array|in:" . implode(',', SingleApplication::REGIMES),
                    "tax_id" => "nullable",
                    "obligation_type" => "nullable"
                ];
                break;
            case Reports::REPORT_TJST_1_CODE:
                $rules = [
                    "document_id" => "required|integer_with_max",
                    "document_status" => "required|integer_with_max|exists:constructor_states,id",
                    "end_date_start" => "required|date",
                    "end_date_end" => "required|date",
                    "hs_code_count" => "required|integer_with_max",
                    "hs_code" => "nullable|string_with_max",
                    "regime" => "required|in:" . implode(',', SingleApplication::REGIMES)
                ];
                break;
            case Reports::REPORT_TJST_3_CODE:
            case Reports::REPORT_TJST_13_CODE:
                $rules = [
                    "document_id" => "required|integer_with_max|exists:constructor_documents,id",
                    "document_status" => "required|integer_with_max",
                    "end_date_start" => "required|date",
                    "end_date_end" => "required|date",
                    "hs_code" => "nullable|string_with_max"
                ];
                break;
            case Reports::REPORT_TJST_2_CODE:
            case Reports::REPORT_TJST_4_CODE:
            case Reports::REPORT_TJST_6_CODE:
            case Reports::REPORT_TJST_8_CODE:
            case Reports::REPORT_TJST_11_CODE:
            case Reports::REPORT_TJST_14_CODE:
            case Reports::REPORT_TJMOH_2_CODE:
                $rules = [
                    "document_id" => "required|integer_with_max|exists:constructor_documents,id",
                    "document_status" => "required|integer_with_max|exists:constructor_states,id",
                    "end_date_start" => "required|date",
                    "end_date_end" => "required|date",
                    "hs_code" => "required|string_with_max",
                    "regime" => "required|in:" . implode(',', SingleApplication::REGIMES)
                ];
                break;
            case Reports::REPORT_TJST_5_CODE:
            case Reports::REPORT_TJMOH_3_CODE:
                $rules = [
                    "document_id" => "required|integer_with_max|exists:constructor_documents,id",
                    "document_status" => "required|integer_with_max|exists:constructor_states,id",
                    "regime" => "required|in:together," . implode(',', SingleApplication::REGIMES),
                    "year" => "required",
                    "sub_division_ids" => "nullable|array|exists:reference_agency_subdivisions,id",
                    "count_type" => "required|in:quantity,sum"
                ];
                break;
            case Reports::REPORT_TJST_9_CODE:
                $rules = [
                    "document_id" => "required|integer_with_max|exists:constructor_documents,id",
                    "document_status" => "required|integer_with_max|exists:constructor_states,id",
                    "end_date_start" => "required|date",
                    "end_date_end" => "required|date",
                    "hs_code" => "required|string_with_max",
                    "regime" => "required|in:" . implode(',', SingleApplication::REGIMES),
                    "name_of_group" => "nullable"
                ];
                break;
            case Reports::REPORT_TJST_10_CODE:
                $rules = [
                    "document_id" => "required|integer_with_max|exists:constructor_documents,id",
                    "document_status" => "required|integer_with_max",
                    "end_date_start" => "required|date",
                    "end_date_end" => "required|date",
                    "hs_code" => "required|string_with_max",
                    "regime" => "required|in:" . implode(',', SingleApplication::REGIMES)
                ];
                break;
            case Reports::REPORT_TJST_12_CODE:
                $rules = [
                    "document_id" => "required|integer_with_max|exists:constructor_documents,id",
                    "document_status" => "required|integer_with_max|exists:constructor_states,id",
                    "year_1" => "required",
                    "year_2" => "required",
                    "month" => "required"
                ];
                break;
            case Reports::REPORT_TJST_16_CODE:
                $rules = [
                    "document_id" => "required|integer_with_max|exists:constructor_documents,id",
                    "document_status" => "required|integer_with_max|exists:constructor_states,id",
                    "download_type" => "required|string|in:pdf,xls,html"
                ];
                break;
            case Reports::REPORT_TJST_17_CODE:
            case Reports::REPORT_TJST_19_CODE:
            case Reports::REPORT_TJST_21_CODE:
            case Reports::REPORT_TJFSC_1_CODE:
            case Reports::REPORT_TJMOH_4_CODE:
                $rules = [
                    "document_id" => "required|integer_with_max|exists:constructor_documents,id",
                    "document_status" => "required|integer_with_max|exists:constructor_states,id",
                    "hs_code" => "nullable|string_with_max",
                    "regime" => "required|in:" . implode(',', SingleApplication::REGIMES)
                ];
                break;
            case Reports::REPORT_TJST_18_CODE:
                $rules = [
                    "document_id" => "required|integer_with_max|exists:constructor_documents,id",
                    "document_status" => "required|array",
                    "hs_code" => "required|string_with_max",
                    "regime" => "required|in:" . implode(',', SingleApplication::REGIMES)
                ];
                break;
            case Reports::REPORT_TJST_20_CODE:
                $rules = [
                    "document_id" => "required|integer_with_max|exists:constructor_documents,id",
                    "document_status" => "nullable|integer_with_max|exists:constructor_states,id",
                    "end_date_start" => "required|date",
                    "end_date_end" => "required|date",
                    "hs_code" => "nullable|string_with_max",
                    "laboratory" => "required"
                ];
                break;
            case Reports::REPORT_TJFSC_2_CODE:
                $rules = [
                    "expiration_date_start" => "nullable|date",
                    "expiration_date_end" => "nullable|date",
                    "import_name" => "nullable|string_with_max",
                    "user_position" => "nullable|string_with_max",
                    "export_country" => "nullable|integer_with_max|exists:reference_countries,id"
                ];
                break;
            case Reports::REPORT_TJFSC_3_CODE:
                $rules = [
                    "expiration_date_start" => "nullable|date",
                    "expiration_date_end" => "nullable|date",
                    "import_name" => "nullable|string_with_max",
                    "user_position" => "nullable|string_with_max",
                    "export_country" => "nullable|integer_with_max|exists:reference_countries,id",
                    "end_date_start" => "required|date",
                    "end_date_end" => "required|date"
                ];
                break;
            case Reports::REPORT_TJFSC_4_CODE:
                $rules = [
                    "expiration_date_start" => "nullable|date",
                    "expiration_date_end" => "nullable|date",
                    "import_name" => "nullable|string_with_max",
                    "user_position" => "nullable|string_with_max",
                    "export_country" => "nullable|integer_with_max|exists:reference_countries,id",
                    "hs_code" => "nullable|string_with_max",
                    "permission_number" => "nullable|string_with_max"
                ];
                break;
            case Reports::REPORT_TJFSC_5_CODE:
                $rules = [
                    "expiration_date_start" => "nullable|date",
                    "expiration_date_end" => "nullable|date",
                    "import_name" => "nullable|string_with_max",
                    "user_position" => "nullable|string_with_max",
                    "export_country" => "nullable|integer_with_max|exists:reference_countries,id",
                    "hs_code" => "nullable|string_with_max",
                    "export_company" => "nullable|string_with_max",
                    "import_country" => "nullable|integer_with_max|exists:reference_countries,id",
                    "sub_division_id" => "nullable|integer_with_max|exists:reference_agency_subdivisions,id",
                ];
                break;
            case Reports::REPORT_TJFSC_6_CODE:
                $rules = [
                    "expiration_date_start" => "nullable|date",
                    "expiration_date_end" => "nullable|date",
                    "import_name" => "nullable|string_with_max",
                    "user_position" => "nullable|string_with_max",
                    "export_country" => "nullable|integer_with_max|exists:reference_countries,id",
                    "hs_code" => "nullable|string_with_max",
                    "end_date_start" => "nullable|date",
                    "end_date_end" => "nullable|date",
                    "document_id" => "nullable|integer_with_max|exists:constructor_documents,id",
                    "regime" => "required|array|in:" . implode(',', SingleApplication::REGIMES)
                ];
                break;
            case Reports::REPORT_TJFSC_7_CODE:
                $rules = [
                    "expiration_date_start" => "nullable|date",
                    "expiration_date_end" => "nullable|date",
                    "import_name" => "nullable|string_with_max",
                    "user_position" => "nullable|string_with_max",
                    "export_country" => "nullable|integer_with_max|exists:reference_countries,id",
                    "hs_code" => "nullable|string_with_max",
                    "permission_number" => "nullable|string_with_max",
                    "producer_country" => "nullable|integer_with_max|exists:reference_countries,id"
                ];
                break;
            case Reports::REPORT_TJFSC_8_CODE:
                $rules = [
                    "permission_number" => "nullable|string_with_max",
                    "export_country" => "nullable|integer_with_max|exists:reference_countries,id",
                    "importer" => "nullable|string_with_max",
                    "commercial_description" => "nullable|string_with_max",
                ];
                break;
            case Reports::REPORT_TJFSC_9_CODE:
                $rules = [
                    "permission_number" => "nullable|string_with_max",
                    "sub_division_id" => "nullable|integer_with_max|exists:reference_agency_subdivisions,id",
                    "importer" => "nullable|string_with_max",
                    "commercial_description" => "nullable|string_with_max",
                    "producer_country" => "nullable|integer_with_max|exists:reference_countries,id",
                ];
                break;
            case Reports::REPORT_TJCS_1_CODE:
                $rules = [
                    "document_id" => "required|integer_with_max|exists:constructor_documents,id",
                    "document_status" => "required|array|exists:constructor_states,id",
                    "hs_code" => "nullable|string_with_max",
                    "regime" => "required|in:" . implode(',', SingleApplication::REGIMES)
                ];
                break;
            case Reports::REPORT_TJMOH_1_CODE:
                $rules = [
                    "document_id" => "required|integer_with_max|exists:constructor_documents,id",
                    "document_status" => "required|integer_with_max|exists:constructor_states,id",
                    "end_date_start" => "required|date",
                    "end_date_end" => "required|date",
                    "hs_code_count" => "required|integer_with_max",
                    "hs_code" => "nullable|string_with_max",
                    "regime" => "required|in:" . implode(',', SingleApplication::REGIMES)
                ];
                break;
            case Reports::REPORT_TJMOH_5_CODE:
                $rules = [
                    "document_id" => "required|integer_with_max|exists:constructor_documents,id",
                    "document_status" => "required|integer_with_max|exists:constructor_states,id",
                    "hs_code" => "nullable|string_with_max",
                    "sub_division_ids" => "nullable|array|exists:reference_agency_subdivisions,id",
                ];
                break;
            case Reports::REPORT_TJMOH_7_CODE:
                $rules = [
                    "permission_number" => "nullable|string_with_max",
                    "importer" => "nullable|string_with_max",
                    "exporter" => "nullable|string_with_max",
                    "sub_division_ids" => "nullable|array|exists:reference_agency_subdivisions,id",
                    "regime" => "nullable|array|in:" . implode(',', SingleApplication::REGIMES),
                ];
                break;
            case Reports::REPORT_TJMOH_8_CODE:
                $rules = [
                    "saf_number" => "nullable|string_with_max",
                    "import_target" => "nullable|integer_with_max|exists:reference_terget_of_import,id",
                    "medicine_type" => "nullable|integer_with_max|exists:reference_type_of_medicines,id",
                ];
                break;
            case Reports::REPORT_TJ_CCI_TPP:
                $rules = [
                    "document_id" => "required|integer_with_max|exists:constructor_documents,id",
                    "document_status" => "required|integer_with_max|exists:constructor_states,id",
                    "hs_code" => "nullable|string_with_max"
                ];
                break;
            case Reports::REPORT_SUB_APPLICATION_PROCESS_TIME:
                $rules = [
                    "document_id" => "nullable|integer_with_max|exists:constructor_documents,id",
                    "document_status" => "nullable|integer_with_max|exists:constructor_states,id",
                    "hs_code" => "nullable|string_with_max",
                    "regime" => "nullable|array",
                    "regime.*" => "nullable|in:" . implode(',', SingleApplication::REGIMES),
                    "agency" => "required",
                    "sub_application_number" => "nullable"
                ];
                break;
        }


        $rules["download_type"] = "required|string|in:" . implode(',', Reports::DOWNLOAD_TYPES);

        if ($reportCode != Reports::REPORT_TJMOH_3_CODE &&
            $reportCode != Reports::REPORT_TJST_5_CODE &&
            $reportCode != Reports::REPORT_TJST_12_CODE &&
            $reportCode != Reports::REPORT_BROKER_REPORT_1_CODE &&
            $reportCode != Reports::REPORT_GLOBAL_REPORT_4_CODE
        ) {
            $rules["start_date_start"] = "required|date";
            $rules["start_date_end"] = "required|date";
        }

        $rules["state_border_head"] = "nullable|string_with_max";
        $rules["head_name"] = "nullable|string_with_max";

        return $rules;
    }
}
