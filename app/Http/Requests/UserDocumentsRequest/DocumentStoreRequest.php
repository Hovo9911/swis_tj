<?php

namespace App\Http\Requests\UserDocumentsRequest;

use App\Http\Requests\Request;
use App\Models\SingleApplication\SingleApplication;

/**
 * Class UserDocumentsDocumentRequest
 * @package App\Http\Requests\UserDocumentsRequest
 */
class DocumentStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'document_name' => 'required|string|max:250',
            'document_description' => 'nullable|string|max:500',
            'document_number' => 'required|string|max:50',
            'document_type' => 'required|string_with_max|exists:reference_classificator_of_documents,code',
            'document_release_date' => 'required|date|before_or_equal:' . currentDate(),
            'document_file' => 'required|string|max:10000',
            'document_products' => 'nullable|string_with_max',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        SingleApplication::select('id')->where(['regular_number' => request()->header('sNumber')])->firstOrFail();

        return [
            'document_name.required' => trans('core.base.field.required'),
            'document_name.max' => trans('core.base.field.max_characters', ['max' => 250]),
            'document_name.*' => trans('core.loading.invalid_data'),

            'document_description.max' => trans('core.base.field.max_characters', ['max' => 500]),
            'document_description.*' => trans('core.loading.invalid_data'),

            'document_number.required' => trans('core.base.field.required'),
            'document_number.max' => trans('core.base.field.max_characters', ['max' => 50]),
            'document_number.*' => trans('core.loading.invalid_data'),

            'document_type.required' => trans('core.base.field.required'),
            'document_type.*' => trans('core.loading.invalid_data'),

            'document_release_date.required' => trans('core.base.field.required'),
            'document_release_date.before_or_equal' => trans('core.base.field.end_date', ['end' => currentDate()]),
            'document_release_date.*' => trans('core.loading.invalid_data'),

            'document_file.required' => trans('core.base.field.required'),
        ];
    }
}
