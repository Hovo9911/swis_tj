<?php

namespace App\Http\Requests\UserDocumentsRequest;

use App\Http\Requests\Request;

/**
 * Class UserDocumentsDocumentRequest
 * @package App\Http\Requests\UserDocumentsRequest
 */
class ExaminationStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'examinationId' => 'nullable',
            'saf_product_id' => 'required',
            'type_of_good' => 'required_with:saf_product_id',
            'type_of_metal' => 'required_with:saf_product_id',
            'weight' => 'required_with:saf_product_id|numeric|min:0',
            'quantity' => 'required_with:saf_product_id|numeric|min:0',
            'type_of_stone' => 'nullable|array',
            'piece' => 'nullable|integer_with_max|numeric|min:0',
            'carat' => 'nullable|numeric|min:0'
        ];

        if ($this->request->get('type_of_stone')[0]) {
            $rules['piece'] = 'required|integer_with_max|numeric|min:0';
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'saf_product_id.required' => trans('core.base.field.required'),
            'saf_product_id.*' => trans('core.loading.invalid_data'),

            'type_of_good.required_with' => trans('core.base.field.required'),
            'type_of_metal.required_with' => trans('core.base.field.required'),
            'type_of_metal.*' => trans('core.loading.invalid_data'),

            'weight.required_with' => trans('core.base.field.required'),
            'weight.numeric' => trans('core.loading.invalid_data'),
            'weight.min' => trans('core.loading.invalid_data'),
            'weight.*' => trans('core.loading.invalid_data'),

            'quantity.required_with' => trans('core.base.field.required'),
            'quantity.numeric' => trans('core.loading.invalid_data'),
            'quantity.min' => trans('core.loading.invalid_data'),
            'quantity.*' => trans('core.loading.invalid_data'),

            'piece.required' => trans('core.base.field.required'),
            'piece.integer' => trans('core.loading.invalid_data'),
            'piece.min' => trans('core.loading.invalid_data'),
            'piece.numeric' => trans('core.loading.invalid_data'),
            'piece.*' => trans('core.loading.invalid_data'),

            'carat.min' => trans('core.loading.invalid_data'),
            'carat.numeric' => trans('core.loading.invalid_data'),
            'carat.*' => trans('core.loading.invalid_data'),
        ];
    }


}
