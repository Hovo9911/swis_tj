<?php

namespace App\Http\Requests\UserDocumentsRequest;

use App\Http\Requests\Request;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorMapping\ConstructorMapping;
use App\Models\ConstructorMappingToRule\ConstructorMappingToRule;
use App\Models\ConstructorRules\ConstructorRules;
use App\Models\DataModel\DataModel;
use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure;
use App\Models\SingleApplicationProducts\SingleApplicationProducts;
use App\Models\SingleApplicationProductsBatch\SingleApplicationProductsBatch;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use App\Models\UserDocuments\UserDocuments;
use App\Rules\ValidateUniqueMDMFields;
use App\SwisRules\ValidationRules;
use Illuminate\Support\Facades\DB;

/**
 * Class UserDocumentsRequest
 * @package App\Http\Requests\UserDocumentsRequest
 */
class UserDocumentsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validationField = $mandatoryFieldsDataName = [];
        $userDocumentID = $this->input('id');
        $applicationID = $this->input('applicationId');

        $lastXML = SingleApplicationDataStructure::lastXml();
        $safListsControlledFields = SingleApplication::getSafHiddenElements(true);
        $singleApplicationSubApp = SingleApplicationSubApplications::select('saf_sub_applications.*', 'saf.regime')->where('saf_sub_applications.id', $this->input('applicationId'))->leftJoin('saf', 'saf.regular_number', '=', 'saf_sub_applications.saf_number')->first();
        $products = SingleApplicationProducts::select('id')->where('saf_number', $singleApplicationSubApp->saf_number)->whereIn('id', array_keys($singleApplicationSubApp->productsOrderedList()))->get();

        $datasetFromMdm = DocumentsDatasetFromMdm::join('documents_dataset_from_mdm_ml', 'documents_dataset_from_mdm_ml.documents_dataset_from_mdm_id', '=', 'documents_dataset_from_mdm.id')->where('lng_id', cLng('id'))->where('document_id', $userDocumentID)->get();

        $mdmFields = DataModel::join('data_model_excel_ml', 'data_model_excel.id', '=', 'data_model_excel_ml.data_model_id')
            ->where('data_model_excel_ml.lng_id', cLng('id'))
            ->whereIn('id', $datasetFromMdm->pluck('mdm_field_id')->all())
            ->get()->keyBy('id');

        $fields = getUserDocuments($datasetFromMdm, $mdmFields);

        $mapping = ConstructorMapping::where('id', $this->input('mapping_id'))->first();
        if (isset($mapping->mandatoryFieldsMdm)) {
            list ($validationField, $batchValidationField) = $this->generateRulesForSafAndMdmFields($mapping->mandatoryFieldsMdm, $userDocumentID, $validationField, $fields);
        }

        if (isset($mapping->mandatoryFieldsSaf)) {
            foreach ($mapping->mandatoryFieldsSaf as $item) {
                $safItem = $lastXML->xpath("//*[@id='{$item->saf_field_id}']");
                if (!empty($safItem)) {
                    if (getSaFXmlFieldModuleName($item->saf_field_id, $lastXML) == 'default' && !empty((string)$safItem[0]->attributes()->dataName)) {
                        $mandatoryFieldsDataName[] = (string)$safItem[0]->attributes()->name;
                    } else {
                        $mandatoryFieldsDataName[] = !empty((string)$safItem[0]->attributes()->dataName) ? (string)$safItem[0]->attributes()->dataName : (string)$safItem[0]->attributes()->name;
                    }
                }
            }
        }

        // ---------------------------- START VALIDATE BATCHES ---------------------------- //

        $productBatches = SingleApplicationProductsBatch::select('id')->whereIn('saf_product_id', array_keys($singleApplicationSubApp->productsOrderedList()))->get();

        $validationRulesByMdmId = [];
        if (isset($safListsControlledFields['products_batch'])) {
            foreach ($safListsControlledFields['products_batch'] as $field) {
                $batchFields = "batch[_index_][{$field}]";
                $xmlAttributes = $lastXML->xpath("//*[@name='{$batchFields}']");
                if ($xmlAttributes) {
                    $mdmId = (int)$xmlAttributes[0]->attributes()->mdm;

                    if ($mdmId && !array_key_exists($field, $validationRulesByMdmId)) {
                        $validationRulesByMdmId[$field] = $this->mdmValidationRules($mdmId);
                    }
                }
            }
        }

        if (isset($safListsControlledFields['default'])) {
            foreach ($safListsControlledFields['default'] as $field) {
                $xmlAttributes = $lastXML->xpath("//*[@name='{$field}']");

                if ($xmlAttributes) {
                    $mdmId = (int)$xmlAttributes[0]->attributes()->mdm;
                    if ($mdmId && !array_key_exists($field, $validationRulesByMdmId)) {
                        $validationRulesByMdmId[$field] = $this->mdmValidationRules($mdmId);
                    }
                }
            }
        }

        foreach ($productBatches as $batch) {

            if (isset($safListsControlledFields['products_batch'])) {
                foreach ($safListsControlledFields['products_batch'] as $field) {
                    $validationField["product_batches.{$batch->id}.$field"] = in_array($field, $mandatoryFieldsDataName) ? 'required|' : '';
                    if (array_key_exists($field, $validationRulesByMdmId)) {
                        $validationField["product_batches.{$batch->id}.$field"] = $validationField["product_batches.{$batch->id}.$field"] . $validationRulesByMdmId[$field];
                    }
                }
            }

            if (isset($safListsControlledFields['default'])) {
                foreach ($safListsControlledFields['default'] as $field) {
                    $validationField[getSafFieldCorrectName($field)] = in_array($field, $mandatoryFieldsDataName) ? 'required|' : '';

                    if (array_key_exists($field, $validationRulesByMdmId)) {
                        $validationField[getSafFieldCorrectName($field)] = $validationField[getSafFieldCorrectName($field)] . $validationRulesByMdmId[$field];

                        if (strpos($validationField[getSafFieldCorrectName($field)], 'required') !== false) {
                            $validationField[getSafFieldCorrectName($field)] = str_replace('nullable|', '', $validationField[getSafFieldCorrectName($field)]);
                        }
                        if (substr($validationField[getSafFieldCorrectName($field)], 0, 1) === '|') {
                            $validationField[getSafFieldCorrectName($field)] = substr($validationField[getSafFieldCorrectName($field)], 1);
                        }

                    }
                }
            }

            foreach ($batchValidationField as $field => $batchValidation) {
                $validationField["mdmData.{$batch->id}.{$field}"] = 'required';
            }

        }

        // ---------------------------- END VALIDATE BATCHES ---------------------------- //

        // ---------------------------- START VALIDATE NON VISIBLE + MANDATORY  ---------------------------- //
        // non visible field not come with post parameters need to check it manually

        $nonVisibleSafFields = $mapping->nonVisibleFieldsSaf->keyBy('saf_field_id')->toArray();
        foreach ($nonVisibleSafFields as $field) {
            $safItem = $lastXML->xpath("//*[@id='{$field['saf_field_id']}']");
            $name = (string)optional($safItem[0]->attributes())->name;
            if (in_array($name, $mandatoryFieldsDataName)) {
                $name = str_replace('[', '.', str_replace(']', '', str_replace('][', '.', $name)));
                if (isset($validationField[$name])) {
                    unset($validationField[$name]);
                }
            }
        }
        $nonVisibleMdmFields = $mapping->nonVisibleFieldsMdm->keyBy('constructor_data_set_field_id')->toArray();
        foreach ($nonVisibleMdmFields as $fieldId => $field) {
            if (isset($validationField["customData.{$fieldId}"])) {
                unset($validationField["customData.{$fieldId}"]);
            }
        }

        // ---------------------------- END VALIDATE NON VISIBLE + MANDATORY  ---------------------------- //

        $customData = $this->input('customData');
        $rules = array_merge(['state' => 'required|integer'], $validationField);

        // ---------------------------- START VALIDATE UNIQUE FIELDS ---------------------------- //

        $constructorDocument = ConstructorDocument::select('document_code')->where('id', $userDocumentID)->active()->first();
        $uniqueDataSetMdm = DocumentsDatasetFromMdm::select('field_id')->where(['document_id' => $userDocumentID, 'is_unique' => 1])->get();

        foreach ($fields as $field) {
            $validateKey = "customData.{$field['field_id']}";
            if (!empty($products)) {
                if ($field['tab'] == 'products') {
                    if ($field['block'] == 'swis.single_app.products_list') {
                        foreach ($products as $productKey => $product) {
                            $validationField = $field['validation'];
                            if (array_key_exists($validateKey, $rules)) {
                                $validationField = str_replace("nullable", "required", $validationField);

                                if ($productKey + 1 == count($products)) {
                                    unset($rules[$validateKey]);
                                }
                            }

                            $rules["customData.{$field['field_id']}_pr_{$product->id}"] = $validationField;

                            foreach ($uniqueDataSetMdm as $dataSetMdm) {
                                if ($dataSetMdm->field_id == $field['field_id']) {
                                    $productCustomRules = $rules["customData.{$field['field_id']}_pr_{$product->id}"];
                                    $productCustomRules = explode('|', $productCustomRules);
                                    $productCustomRules[] = new ValidateUniqueMDMFields($constructorDocument, $customData, $applicationID, $dataSetMdm);

                                    if (count($productCustomRules) > 0) {
                                        $rules["customData.{$field['field_id']}_pr_{$product->id}"] = $productCustomRules;
                                    }
                                }

                            }

                        }
                    }
                }
            }
        }
        $rules['lastUpdate'] = 'date';

        foreach ($uniqueDataSetMdm as $dataSetMdm) {

            if (isset($rules["customData.{$dataSetMdm->field_id}"])) {
                $customRules = $rules["customData.{$dataSetMdm->field_id}"];
                $customRules = explode('|', $customRules);
                $customRules[] = new ValidateUniqueMDMFields($constructorDocument, $customData, $applicationID, $dataSetMdm);
            } else {
                $customRules = [];
            }

            if (count($customRules) > 0) {
                $rules["customData.{$dataSetMdm->field_id}"] = $customRules;
            }
        }

        // ---------------------------- END VALIDATE UNIQUE FIELDS ---------------------------- //

        // validate free sum obligations
        if (!empty($this->input('free_sum_obligation'))) {
            foreach ($this->input('free_sum_obligation') as $row => $freeSumObligation) {
                $rules["free_sum_obligation.$row.obligation_type"] = 'required|exists:reference_obligation_budget_line,id';
                $rules["free_sum_obligation.$row.budget_line"] = 'required|string_with_max';
                $rules["free_sum_obligation.$row.obligation"] = 'required|numeric|regex:/^[+-]?[0-9]{1,8}(\.[0-9]{1,2})?$/';
            }
        }

        $safPathById = getSafIdNameArray();
        $dataFromFront = $this->all();
        $safData = ['saf' => $dataFromFront['saf'] ?? []];
        $dataFromFront['customData'] = isset($dataFromFront['customData']) ? $dataFromFront['customData'] : [];
        $dataFromFront['customData'] = array_merge($customData ?? [], $dataFromFront['customData'] ?? []);
        $safDataForValidation = getJsonDataAsArray($safData, $dataFromFront['customData'], $singleApplicationSubApp->productListSelect->toArray());

        $availableProductsIds = $singleApplicationSubApp->productList->pluck('id')->all();
        $safProducts = SingleApplicationProducts::select('id')->whereIn('id', $availableProductsIds)->get();
        $productBatches = SingleApplicationProductsBatch::select('id')->whereIn('saf_product_id', array_keys($singleApplicationSubApp->productsOrderedList()))->get();
        $customDataProductAndBatchFields = UserDocuments::getCustomDataProductAndBatchFields($this->input('id'));


        $mappingRules = ConstructorMappingToRule::select('constructor_rules.*')->where('constructor_mapping_id', $this->input('mapping_id'))
            ->join('constructor_rules', 'constructor_rules.id', '=', 'constructor_mapping_to_rule.rule_id')
            ->where('constructor_rules.type', ConstructorRules::VALIDATION_TYPE)
            ->get();

        foreach ($mappingRules as $rule) {
            $validationRules = new ValidationRules($safDataForValidation, $rule->condition, $rule->rule, 'obligation', $singleApplicationSubApp);

            if ($validationRules->checkCondition()) {
                $result = $validationRules->matchRules();
                if (isset($result['function']) && $result['function'] == "makeMandatory") {
                    foreach ($result['result'] as $field) {
                        if (strpos($field, "SAF_ID_") !== false) {
                            $name = $safPathById[$field];
                            if (strpos($name, "batch[_index_]") !== false) {
                                $replacedName = str_replace("batch._index_.", "", str_replace(']', '', str_replace(['][', '['], '.', $name)));
                                foreach ($productBatches as $batch) {
                                    $rules["product_batches.{$batch->id}.{$replacedName}"] = "required";
                                }
                            } else {
                                $rules[str_replace(']', '', str_replace(['][', '['], '.', $name))] = "required";
                            }
                        } else {

                            $name = str_replace("FIELD_ID_", "", $field);
                            if (in_array($name, $customDataProductAndBatchFields['products'])) {
                                foreach ($safProducts as $product) {

                                    $customDataProductValidation = 'required';
                                    if (isset($rules["customData.{$name}_pr_{$product->id}"])) {
                                        $customDataProductValidation = str_replace("nullable", "required", $rules["customData.{$name}_pr_{$product->id}"]);
                                    }

                                    $rules["customData.{$name}_pr_{$product->id}"] = $customDataProductValidation;
                                }
                            } elseif (in_array($name, $customDataProductAndBatchFields['batches'])) {
                                foreach ($productBatches as $batch) {

                                    $mdmBatchValidation = 'required';
                                    if (isset($rules["mdmData-{$batch->id}-{$name}"])) {
                                        $mdmBatchValidation = str_replace("nullable", "required", $rules["mdmData-{$batch->id}-{$name}"]);
                                    }

                                    $rules["mdmData-{$batch->id}-{$name}"] = $mdmBatchValidation;
                                }
                            } else {

                                $customDataValidation = 'required';
                                if (isset($rules["customData.{$name}"])) {
                                    $customDataValidation = str_replace("nullable", "required", $rules["customData.{$name}"]);
                                }

                                $rules["customData.{$name}"] = $customDataValidation;

                            }
                        }
                    }
                } elseif (isset($result['function']) && $result['function'] == "makeLabMandatory") {
                    foreach ($this->input('lab_examination') as $productId => $indicators) {
                        unset($indicators['correspond']);
                        foreach ($indicators as $indicatorId => $values) {
                            $rules["lab_examination.{$productId}.{$indicatorId}.indicator"] = 'required';
                            $rules["lab_examination.{$productId}.{$indicatorId}.correspond_or_not"] = 'required';
                            $rules["lab_examination.{$productId}.{$indicatorId}.found_size"] = "required_without:lab_examination.{$productId}.{$indicatorId}.found_or_not";
                            $rules["lab_examination.{$productId}.{$indicatorId}.found_or_not"] = "required_without:lab_examination.{$productId}.{$indicatorId}.found_size";
                        }
                    }
                }
            }
        }

        return $rules;
    }

    /**
     * Function to generate rules for SAF and MDM fields
     *
     * @param $mandatoryFields
     * @param $userDocumentID
     * @param $validationField
     * @param array $fields
     * @return array
     */
    private function generateRulesForSafAndMdmFields($mandatoryFields, $userDocumentID, $validationField, $fields = [])
    {
        $batchValidationField = [];
        foreach ($mandatoryFields as $field) {
            $constructorDataSetField = DocumentsDatasetFromMdm::select('id', 'xpath')->where('field_id', $field->constructor_data_set_field_id)->where('document_id', $userDocumentID)->first();
            if ($constructorDataSetField && $constructorDataSetField['xpath'] != "tab[@key='products']/block[@name='swis.single_app.products_list']/subBlock[@name='swis.single_app.batches']") {
                $validationField["customData.{$field->constructor_data_set_field_id}"] = 'required';
            }
            if ($constructorDataSetField && $constructorDataSetField['xpath'] == "tab[@key='products']/block[@name='swis.single_app.products_list']/subBlock[@name='swis.single_app.batches']") {
                $batchValidationField[$field->constructor_data_set_field_id] = 'required';
            }
        }

        // for detect field type and replace validation type if it's need
        if (isset($fields) && count($fields) > 0 && $customData = $this->input('customData')) {
            foreach ($fields as $field) {
                if (isset($field['mdm_field_id']) && array_key_exists($field['field_id'], $customData)) {
                    if (isset($validationField["customData.{$field['field_id']}"])) {
                        if ($field['mdm_type'] != DataModel::MDM_ELEMENT_TYPE_MULTIPLE) {
                            $validationField["customData.{$field['field_id']}"] = str_replace("nullable", "required", $field['validation']);
                        } else {
                            $validationField["customData.{$field['field_id']}"] .= '|array|min:1';
                        }
                    } else {
                        $validationField["customData.{$field['field_id']}"] = (strpos($field['validation'], 'nullable|') !== false) ? $field['validation'] : '|nullable' . $field['validation'];
                    }
                }
            }
        }

        return [$validationField, $batchValidationField];
    }

    /**
     * Function to get validation by mdm id
     *
     * @param $mdmId
     * @return string|void
     */
    private function mdmValidationRules($mdmId)
    {
        $dataModelAllData = DataModel::allData();
//        $mdmData = DataModel::where(DB::raw('id::varchar'), $mdmId)->first();

        if (isset($dataModelAllData[$mdmId])) {
            $mdmData = (object)$dataModelAllData[$mdmId];

            return getValidationRulesFromFormat($mdmData->interface_format, $mdmData->pattern, false, $mdmData);
        }
    }

    /**
     * @return array
     */
    public function messages()
    {
        $messages = [];

        $userDocumentID = $this->request->get('id');

        $datasetFromMdm = DocumentsDatasetFromMdm::join('documents_dataset_from_mdm_ml', 'documents_dataset_from_mdm_ml.documents_dataset_from_mdm_id', '=', 'documents_dataset_from_mdm.id')->where('lng_id', cLng('id'))->where('document_id', $userDocumentID)->get();

        $mdmFields = DataModel::join('data_model_excel_ml', 'data_model_excel.id', '=', 'data_model_excel_ml.data_model_id')
            ->where('data_model_excel_ml.lng_id', cLng('id'))
            ->whereIn('id', $datasetFromMdm->pluck('mdm_field_id')->all())
            ->get()->keyBy('id');
        $fields = getUserDocuments($datasetFromMdm, $mdmFields);

        $mapping = ConstructorMapping::where('document_id', $userDocumentID)->where('action', $this->input('action'))->where('end_state', $this->input('state'))->first();
        $globalHiddenFields = UserDocuments::getGlobalHiddenElements($userDocumentID);

        if (isset($mapping->mandatoryFieldsMdm)) {
            foreach ($mapping->mandatoryFieldsMdm as $mdmField) {

                $constructorDataSetField = DocumentsDatasetFromMdm::select('id', 'field_id', 'group')->where('field_id', $mdmField->constructor_data_set_field_id)->where('document_id', $userDocumentID)->first();
                if (!is_null($constructorDataSetField)) {
                    $messages["customData.{$mdmField->constructor_data_set_field_id}.required"] = trans('core.base.hidden-field.required', ['field' => $constructorDataSetField->field_id, 'tab' => $constructorDataSetField->group]);
                }
            }
        }

        foreach ($fields as $field) {
            if (!isset($messages['customData.' . $field['field_id'] . '.required'])) {
                $messages['customData.' . $field['field_id'] . '.required'] = trans('core.base.field.required');
            } else if (isset($globalHiddenFields['mdm']) && !in_array($field['field_id'], $globalHiddenFields['mdm'])) {
                $messages['customData.' . $field['field_id'] . '.required'] = trans('core.base.field.required');
            }

            $messages['customData.' . $field['field_id'] . '.mdm_alpha'] = trans('core.base.invalid_mdm_alpha');
            $messages['customData.' . $field['field_id'] . '.mdm_numeric'] = trans('core.base.invalid_mdm_numeric');
            $messages['customData.' . $field['field_id'] . '.mdm_alphanumeric'] = trans('core.base.invalid_mdm_alphanumeric');
            $messages['customData.' . $field['field_id'] . '.max'] = trans('core.base.field.max_characters', ['max' => ($field['max_value'] > 0) ? $field['max_value'] : 10]);
            $messages['customData.' . $field['field_id'] . '.digits_between'] = trans('core.base.field.digits_between', ['min' => 0, 'max' => 10]);
            $messages['customData.' . $field['field_id'] . '.*'] = trans('core.loading.invalid_data');
        }

        return $messages;
    }
}
