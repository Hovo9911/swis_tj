<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

/**
 * Class UserRequest
 * @package App\Http\Requests\User
 */
class UserStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = $this->request->all();

        $sendFormConfirmation = $this->request->get('send_for_confirmation');

        $rules = [
            'added_manually' => 'nullable|in:1',

            'ssn' => 'required|ssn_validator|unique:users',
            'first_name' => 'required|alpha|max:250',
            'last_name' => 'required|alpha|max:250',
            'username' => 'required|max:50|only_latin|insensitive_unique:users,username',

            'passport' => 'required|passport_validator',
            'passport_issue_date' => 'nullable|date',
            'passport_issued_by' => 'nullable|string_with_max',

            'phone_number' => 'required|phone_number_validator',
            'registration_address' => 'required|max:150',
            'send_for_confirmation' => 'nullable|in:1',
            'lng_id' => 'required|in:' . implode(',', activeLanguages()->pluck('id')->toArray()),
            'additional_information' => 'nullable|string|max:350',
            'is_head' => 'nullable|integer|max:1',
            'show_status' => 'required|in:1,2',
        ];

        if ($data['last_name'] == '-') {
            $rules['last_name'] = 'required|in:-';
        }

        if ($data['first_name'] == '-') {
            $rules['first_name'] = 'required|in:-';
        }

        $passCond = 'nullable|';
        if (!$sendFormConfirmation) {

            if (empty($this->request->get('id'))) {
                $passCond = 'required|';
            }

            $rules['password'] = $passCond . "string|validate_password";
        } else {
            $rules['email'] = 'required|email|max:250';
        }

        $companiesTaxIds = [];
        if (!empty($this->request->get('company'))) {
            foreach ($this->request->get('company') as $key => $val) {
                $rules['company.' . $key . '.tax_id'] = 'nullable|tax_id_validator';

                if (in_array($val['tax_id'], $companiesTaxIds)) {
                    $rules['company.' . $key . '.tax_id'] = 'unique_list';
                }

                $companiesTaxIds[] = $val['tax_id'];
            }
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        $messages = [
            'first_name.required' => trans('core.base.field.required'),
            'first_name.max' => trans('core.base.field.max_characters', ['max' => 250]),
            'first_name.*' => trans('core.loading.invalid_data'),

            'last_name.required' => trans('core.base.field.required'),
            'last_name.max' => trans('core.base.field.max_characters', ['max' => 250]),
            'last_name.*' => trans('core.loading.invalid_data'),

            'ssn.required' => trans('core.base.field.required'),
            'ssn.unique' => trans('core.base.field.unique'),
            'ssn.*' => trans('core.loading.invalid_data'),

            'username.required' => trans('core.base.field.required'),
            'username.max' => trans('core.base.field.max_characters', ['max' => 50]),
            'username.insensitive_unique' => trans('core.base.field.unique'),
            'username.only_latin' => trans('core.base.field.only_latin_characters'),
            'username.*' => trans('core.loading.invalid_data'),

            'password.required' => trans('core.base.field.required'),
            'password.validate_password' => trans('swis.profile_settings.password_regex_incorrect', ['min' => config('global.password_characters.min'), 'max' => config('global.password_characters.max')]),
            'password.*' => trans('core.loading.invalid_data'),

            'email.required' => trans('core.base.field.required'),
            'email.max' => trans('core.base.field.max_characters', ['max' => 250]),
            'email.email' => trans('core.loading.email_valid'),
            'email.*' => trans('core.loading.invalid_data'),

            'registration_address.max' => trans('core.base.field.max_characters', ['max' => 150]),
            'registration_address.*' => trans('core.loading.invalid_data'),

            'phone_number.required' => trans('core.base.field.required'),

            'passport.required' => trans('core.base.field.required'),
            'passport.*' => trans('swis.passport.field.not_valid'),

            'additional_information.max' => trans('core.base.field.max_characters', ['max' => 350]),
            'additional_information.*' => trans('core.loading.invalid_data'),
        ];

        if (!empty($this->request->get('company'))) {
            foreach ($this->request->get('company') as $key => $val) {
                $messages['company.' . $key . '.unique_list'] = trans('core.base.field.unique');
            }
        }

        return $messages;
    }
}
