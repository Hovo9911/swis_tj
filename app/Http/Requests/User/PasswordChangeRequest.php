<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

/**
 * Class PasswordChangeRequest
 * @package App\Http\Requests\User
 */
class PasswordChangeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => 'required|validate_old_password',
            'password' => 'required|confirmed|validate_password',
            'password_confirmation' => 'required|validate_password|set_different_passwords',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'old_password.validate_old_password' => trans('core.base.field.current_password_not_correct'),
            'password.required' => trans('core.base.field.required'),
            'password.confirmed' => trans('core.base.password.confirmed'),
            'password.validate_password' => trans('swis.profile_settings.password_regex_incorrect', ['min' => config('global.password_characters.min'),'max' => config('global.password_characters.max')]),
            'password.*' => trans('core.loading.invalid_data'),
        ];
    }
}