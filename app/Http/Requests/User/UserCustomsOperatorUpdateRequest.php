<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;
use App\Models\User\User;

/**
 * Class UserRequest
 * @package App\Http\Requests\User
 */
class UserCustomsOperatorUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|alpha|max:250',
            'last_name' => 'required|alpha|max:250',
            'phone_number' => 'nullable|phone_number_validator',
            'ref_custom_body' => 'nullable|array',
            'ref_custom_body.*' => 'integer_with_max|exists:reference_custom_body_reference,code',
            'username' => 'required|max:50|only_latin|insensitive_unique:users,username,' . $this->request->get('id'),
            'password' => 'nullable|validate_password',
            'show_status' => 'required|in:1,2',
            'lng_id' => 'required|in:' . implode(',', activeLanguages()->pluck('id')->toArray()),
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'first_name.required' => trans('core.base.field.required'),
            'first_name.max' => trans('core.base.field.max_characters', ['max' => 250]),
            'first_name.*' => trans('core.loading.invalid_data'),

            'last_name.required' => trans('core.base.field.required'),
            'last_name.max' => trans('core.base.field.max_characters', ['max' => 250]),
            'last_name.*' => trans('core.loading.invalid_data'),

            'phone_number.*' => trans('core.loading.invalid_data'),

            'ref_custom_body.*' => trans('core.loading.invalid_data'),

            'username.required' => trans('core.base.field.required'),
            'username.max' => trans('core.base.field.max_characters', ['max' => 50]),
            'username.insensitive_unique' => trans('core.base.field.unique'),
            'username.only_latin' => trans('core.base.field.only_latin_characters'),
            'username.*' => trans('core.loading.invalid_data'),

            'password.required' => trans('core.base.field.required'),
            'password.validate_password' => trans('swis.profile_settings.password_regex_incorrect', ['min' => config('global.password_characters.min'),'max' => config('global.password_characters.max')]),
            'password.*' => trans('core.loading.invalid_data'),
        ];
    }
}
