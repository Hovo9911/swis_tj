<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;
use App\Models\User\User;

/**
 * Class UserSearchRequest
 * @package App\Http\Requests\User
 */
class UserSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.id' => 'integer_with_max',
            'f.passport' => 'string_with_max',
            'f.ssn' => 'string_with_max',
            'f.first_name' => 'string_with_max',
            'f.last_name' => 'string_with_max',
            'f.company_tax_id' => 'string_with_max',
            'f.company_name' => 'string_with_max',
            'f.authorizer_ssn' => 'string_with_max',
            'f.authorizer_first_name' => 'string_with_max',
            'f.authorizer_last_name' => 'string_with_max',
            'f.register_username' => 'string_with_max',
            'f.menu' => 'integer_with_max|exists:menu,id',
            'f.role' => 'integer_with_max|exists:roles,id',
            'f.sub_division_id' => 'integer_with_max|exists:reference_agency_subdivisions,id',
            'f.ref_custom_body' => 'string_with_max|integer_with_max|exists:reference_custom_body_reference,code',
            'f.user_type' => 'string_with_max|in:' . implode(',', User::USER_TYPES),
            'f.is_head' => 'integer|in:1',
            'f,show_status' => 'integer|in:1,2',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
