<?php

namespace App\Http\Requests\Agency;

use App\Http\Requests\Request;
use App\Models\Agency\Agency;
use App\Models\ReferenceTable\ReferenceTable;
use App\Rules\ValidateLunchHours;
use App\Rules\ValidateWorkingHours;
use Carbon\Carbon;

/**
 * Class AgencyStoreRequest
 * @package App\Http\Requests\Agency
 */
class AgencyStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $workingHoursStart = strtotime($this->input('working_hours_start'));
        $workingHoursEnd = strtotime($this->input('working_hours_end'));
        $lunchHoursStart = strtotime($this->input('lunch_hours_start'));
        $lunchHoursEnd = strtotime($this->input('lunch_hours_end'));

        $rules = [
            'tax_id' => 'required|tax_id_validator|unique:agency,tax_id',
            'legal_entity_name' => 'required|max:255',
            'address' => 'nullable|string|max:70',
            'logo' => 'nullable|string|max:50',
            'agency_code' => 'nullable|string|max:35',
            'phone_number' => 'required|phone_number_validator',
            'email' => 'required|email|max:250',
            'website_url' => 'nullable|string|max:2048',
            'subdivision_mark_type' => 'required|in:' . implode(',', Agency::SUBDIVISION_MARK_ALL_TYPES),
            'allow_send_sub_applications_other_agency' => 'nullable|in:1',
            'show_status' => 'required|in:1,2',
            'is_lab' => 'nullable',
            'working_hours_start' => 'required','exists:' . ReferenceTable::REFERENCE_HOURS_INTERVAL . ',hours',
            'working_hours_end' => ['required','exists:' . ReferenceTable::REFERENCE_HOURS_INTERVAL . ',hours', new ValidateWorkingHours($workingHoursStart, $workingHoursEnd)],
            'lunch_hours_start' => ['required','exists:' . ReferenceTable::REFERENCE_HOURS_INTERVAL . ',hours', new ValidateLunchHours($workingHoursStart, $workingHoursEnd, $lunchHoursStart, $lunchHoursEnd)],
            'lunch_hours_end' => ['required','exists:' . ReferenceTable::REFERENCE_HOURS_INTERVAL . ',hours', new ValidateWorkingHours($lunchHoursStart, $lunchHoursEnd)]
        ];

        if ($this->input('is_lab')) {
            $rules['certificate_number'] = 'required|string|max:35|unique:laboratory';
            $rules['laboratory_code'] = 'required|string|max:250';
            $rules['certification_period_validity'] = 'required|after_or_equal:' . Carbon::parse(currentDateTime())->addMinute(-1);
        }

        if ($this->input('indicators')) {
            foreach ($this->input('indicators') as $id => $indicator) {
                $rules["indicators.{$id}.price"] = 'required|numeric';
                $rules["indicators.{$id}.duration"] = 'required|numeric';
            }
        }

        foreach ($this->request->get('ml') as $key => $ml) {
            $rules['ml.' . $key . '.name'] = 'required|max:255';
            $rules['ml.' . $key . '.short_name'] = 'nullable|max:255';
            $rules['ml.' . $key . '.address'] = 'nullable|max:255';
            $rules['ml.' . $key . '.description'] = 'nullable|max:5000';
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        $messages = [
            'tax_id.required' => trans('core.base.field.required'),
            'tax_id.unique' => trans('core.base.field.unique'),
            'tax_id.*' => trans('core.loading.invalid_data'),

            'agency_code.max' => trans('core.base.field.max_characters', ['max' => 35]),
            'agency_code.*' => trans('core.loading.invalid_data'),

            'address.max' => trans('core.base.field.max_characters', ['max' => 70]),
            'address.*' => trans('core.loading.invalid_data'),

            'email.required' => trans('core.base.field.required'),
            'email.max' => trans('core.base.field.max_characters', ['max' => 250]),
            'email.email' => trans('core.loading.email_valid'),
            'email.*' => trans('core.loading.invalid_data'),

            'website_url.max' => trans('core.base.field.max_characters', ['max' => 2048]),
            'website_url.*' => trans('core.loading.invalid_data'),

            'legal_entity_name.required' => trans('core.base.field.required'),
            'legal_entity_name.max' => trans('core.base.field.max_characters', ['max' => 255]),
            'legal_entity_name.*' => trans('core.loading.invalid_data', ['max' => 255]),

            'phone_number.required' => trans('core.base.field.required'),
            'phone_number.*' => trans('core.loading.invalid_data'),
        ];

        foreach ($this->request->get('ml') as $key => $ml) {
            $messages['ml.' . $key . '.name.max'] = trans('core.base.field.max_characters', ['max' => 255]);
            $messages['ml.' . $key . '.short_name.max'] = trans('core.base.field.max_characters', ['max' => 255]);
            $messages['ml.' . $key . '.address.max'] = trans('core.base.field.max_characters', ['max' => 255]);
            $messages['ml.' . $key . '.description.max'] = trans('core.base.field.max_characters', ['max' => 5000]);
        }

        return $messages;
    }
}
