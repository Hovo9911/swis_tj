<?php

namespace App\Http\Requests\Agency;

use App\Http\Requests\Request;

/**
 * Class AgencySearchRequest
 * @package App\Http\Requests\Agency
 */
class AgencySearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.id' => 'integer_with_max',
            'f.name' => 'string_with_max',
            'f.tax_id' => 'string_with_max',
            'f.address' => 'string_with_max',
            'f.agency_code' => 'string_with_max',
            'f.show_status' => 'in:1,2',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
