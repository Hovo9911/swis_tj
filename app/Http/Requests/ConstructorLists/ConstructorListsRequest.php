<?php

namespace App\Http\Requests\ConstructorLists;

use App\Http\Requests\Request;
use App\Models\ConstructorDocument\ConstructorDocument;

/**
 * Class ConstructorListsRequest
 * @package App\Http\Requests\ConstructorListsRequest
 */
class ConstructorListsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $agencyDocumentsIds = ConstructorDocument::getConstructorDocumentForAgency()->pluck('id')->toArray();

        return [
            'document_id' => 'required|integer|exists:constructor_documents,id|in:' . implode(',', $agencyDocumentsIds),
            'list_type' => 'required|integer|min:1|max:3',
            'description' => 'required|string|max:500',
            'show_status' => 'required|integer|in:1,2',
            'paramsSaf' => 'array|nullable',
            'paramsMDM' => 'array|nullable',
            'paramsTabs' => 'array|nullable',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'document_id.required' => trans('core.base.field.required'),
            'document_id.*' => trans('core.loading.invalid_data'),
        ];
    }
}
