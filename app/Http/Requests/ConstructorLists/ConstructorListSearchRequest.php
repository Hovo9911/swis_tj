<?php

namespace App\Http\Requests\ConstructorLists;

use App\Http\Requests\Request;

/**
 * Class ConstructorMappingSearchRequest
 * @package App\Http\Requests\ConstructorMapping
 */
class ConstructorListSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.id' => 'integer_with_max',
            'f.constructor_document_id' => 'integer_with_max|exists:constructor_documents,id',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
