<?php

namespace App\Http\Requests\SingleApplicationExpertise;

use App\Http\Requests\Request;

/**
 * Class SingleApplicationExpertiseRequest
 * @package App\Http\Requests\SingleApplicationExpertise
 */
class SingleApplicationExpertiseRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sample_code' => 'required|string_with_max',
            'saf_number' => 'required|string_with_max',
            'sub_application_id' => 'required|integer',
            'sampling_weight' => 'required|numeric',
            'sampling_method_code' => 'required|string_with_max',
            'sampler_code' => 'required|array',
            'expertising_method_code' => 'required|string_with_max',
            'expertising_indicator_code' => 'nullable|string_with_max',
            'product_group_code' => 'nullable|string_with_max',
            'product_sub_group_code' => 'nullable|string_with_max',
            'total_price' => 'nullable|string_with_max',
            'saf_product_id' => 'required|string_with_max',
            'saf_product_batches_ids' => 'nullable|array',
            'expertising_actions' => 'required',
            'lab_id' => 'required|array'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
