<?php

namespace App\Http\Requests\PaymentProviders;

use App\Http\Requests\Request;

/**
 * Class PaymentProviderSearchRequest
 * @package App\Http\Requests\PaymentProviders
 */
class PaymentProviderSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.id' => 'integer_with_max',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
