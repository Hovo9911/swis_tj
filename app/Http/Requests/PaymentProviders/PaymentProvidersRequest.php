<?php

namespace App\Http\Requests\PaymentProviders;

use App\Http\Requests\Request;

/**
 * Class PaymentProvidersRequest
 * @package App\Http\Requests\PaymentProvidersRequest
 */
class PaymentProvidersRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string_with_max',
            'ip_address' => 'required|string_with_max',
            'token' => 'required|string_with_max|unique:payment_providers,token,' . $this->route('id'),
            'address' => 'required|string_with_max',
            'contacts' => 'required|string_with_max',
            'description' => 'required|string_with_max',
            'show_status' => 'required|integer'
        ];

        if (is_null($this->route('id'))) {
            $rules['token'] = 'required|string_with_max|unique:payment_providers,token';
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'description.required' => trans('core.base.field.required'),
            'ip_address.required' => trans('core.base.field.required'),
            'token.required' => trans('core.base.field.required'),
            'address.required' => trans('core.base.field.required'),
            'contacts.required' => trans('core.base.field.required'),
            'name.required' => trans('core.base.field.required'),
        ];
    }
}
