<?php

namespace App\Http\Requests\ConstructorStates;

use App\Http\Requests\Request;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorStates\ConstructorStates;

/**
 * Class ConstructorStatesRequest
 * @package App\Http\Requests\ConstructorStates
 */
class ConstructorStatesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Function to get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $agencyDocumentsIds = ConstructorDocument::getConstructorDocumentForAgency()->pluck('id')->toArray();

        $rules = [
            'constructor_document_id' => 'required|integer|exists:constructor_documents,id|in:' . implode(',', $agencyDocumentsIds),
            'state_type' => 'required|in:' . implode(',', ConstructorStates::STATE_TYPES),
            'state_approved_status' => 'integer_with_max',
            'show_status' => 'nullable|integer_with_max',
            'is_countable' => 'nullable'
        ];

        foreach ($this->request->get('ml') as $key => $ml) {
            $rules['ml.' . $key . '.description'] = 'nullable|max:500|string';
            $rules['ml.' . $key . '.state_name'] = 'required|max:100|string';
        }

        return $rules;
    }

    /**
     * Function to get the validation messages
     *
     * @return array
     */
    public function messages()
    {
        $messages = [
            'constructor_document_id.required' => trans('core.base.field.required'),
            'constructor_document_id.*' => trans('core.loading.invalid_data'),
            'state_type.required' => trans('core.base.field.required'),
            'state_type.*' => trans('core.loading.invalid_data'),
            'state_approved_status.*' => trans('core.loading.invalid_data')
        ];

        foreach ($this->request->get('ml') as $key => $ml) {
            $messages['ml.' . $key . '.description.max'] = trans('core.base.field.max_characters', ['max' => 500]);
            $messages['ml.' . $key . '.state_name.max'] = trans('core.base.field.max_characters', ['max' => 100]);
        }

        return $messages;
    }
}
