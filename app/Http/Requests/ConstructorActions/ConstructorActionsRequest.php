<?php

namespace App\Http\Requests\ConstructorActions;

use App\Http\Requests\Request;
use App\Models\ConstructorDocument\ConstructorDocument;

/**
 * Class ConstructorActionsRequest
 * @package App\Http\Requests\ConstructorActions
 */
class ConstructorActionsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $agencyDocumentsIds = ConstructorDocument::getConstructorDocumentForAgency()->pluck('id')->toArray();

        $rules = [
            'constructor_document_id' => 'required|integer|exists:constructor_documents,id|in:' . implode(',', $agencyDocumentsIds),
            'special_type' => 'required|integer|min:1|max:4',
            'show_status' => 'integer',
            'templates' => 'array|nullable',
            'print_default' => 'nullable',
            'select_all_products' => 'nullable',
            'order' => 'nullable'
        ];

        foreach ($this->request->get('ml') as $key => $ml) {
            $rules['ml.' . $key . '.description'] = 'nullable|max:500|string';
            $rules['ml.' . $key . '.action_name'] = 'required|max:100|string';
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        $messages = [
            'constructor_document_id.required' => trans('core.base.field.required'),
            'constructor_document_id.*' => trans('core.loading.invalid_data'),
            'special_type.required' => trans('core.base.field.required'),
            'templates.*' => trans('core.loading.invalid_data'),
        ];

        foreach ($this->request->get('ml') as $key => $ml) {
            $messages['ml.' . $key . '.description.max'] = trans('core.base.field.max_characters', ['max' => 500]);
            $messages['ml.' . $key . '.action_name.max'] = trans('core.base.field.max_characters', ['max' => 100]);
        }

        return $messages;
    }
}
