<?php

namespace App\Http\Requests\TransportApplication;

use App\Http\Requests\Request;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\TransportApplication\TransportApplication;

/**
 * Class TransportApplicationRejectReasonRequest
 * @package App\Http\Requests\TransportApplication
 */
class TransportApplicationRejectReasonRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return[
            'reject_reason' => 'required|string|max:512',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'reject_reason.required' => trans('core.base.field.required'),
            'reject_reason.max' => trans('core.base.field.max_characters', ['max' => 512]),
            'reject_reason.*' => trans('core.loading.invalid_data'),
        ];
    }
}