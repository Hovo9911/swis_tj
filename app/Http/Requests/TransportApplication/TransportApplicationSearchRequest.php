<?php

namespace App\Http\Requests\TransportApplication;

use App\Http\Requests\Request;
use App\Models\TransportApplication\TransportApplication;

/**
 * Class TransportApplicationSearchRequest
 * @package App\Http\Requests\TransportApplication
 */
class TransportApplicationSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.id' => 'integer_with_max',
            'f.application_number' => 'string_with_max',
            'f.vehicle_reg_number' => 'string_with_max',
            'f.ref_transport_permit_type' => 'exists:reference_transport_permit_types,id',
            'f.current_status' => 'in:'.implode(',',TransportApplication::TRANSPORT_APPLICATION_STATUSES),
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}