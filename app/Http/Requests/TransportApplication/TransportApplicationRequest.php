<?php

namespace App\Http\Requests\TransportApplication;

use App\Http\Requests\Request;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\TransportApplication\TransportApplication;

/**
 * Class TransportApplicationRequest
 * @package App\Http\Requests\TransportApplication
 */
class TransportApplicationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = $this->request->all();
        $inputStatus = $data['current_status'];
        $currentStatus = null;
        $rules = [];

        $transportApplication = TransportApplication::where('id', $data['id'])->first();
        if (!is_null($transportApplication)) {
            $currentStatus = $transportApplication->current_status;
        }

        // In Rejected
        if ($currentStatus == TransportApplication::TRANSPORT_APPLICATION_STATUS_REJECTED) {
            return [];
        }

        // To Waiting Payment Confirmation
        if ($currentStatus == TransportApplication::TRANSPORT_APPLICATION_STATUS_APPROVED) {
            return [
                'current_status' => 'required|in:' . TransportApplication::TRANSPORT_APPLICATION_STATUS_WAITING_PAYMENT_CONFIRMATION
            ];
        }

        // To Issued
        if ($currentStatus == TransportApplication::TRANSPORT_APPLICATION_STATUS_WAITING_PAYMENT_CONFIRMATION) {
            return [
                'current_status' => 'required|in:' . TransportApplication::TRANSPORT_APPLICATION_STATUS_ISSUED,
                'transport_application_receipt_file' => 'required|max:1000'
            ];
        }

        // To Register // To Approve
        if (is_null($currentStatus) || $currentStatus == TransportApplication::TRANSPORT_APPLICATION_STATUS_REGISTERED) {

            // To Approve
            $toApproveStatus = false;
            if ($currentStatus == TransportApplication::TRANSPORT_APPLICATION_STATUS_REGISTERED && $inputStatus == TransportApplication::TRANSPORT_APPLICATION_STATUS_APPROVED) {
                $toApproveStatus = true;
            }

            $rules = [
                'driver_name' => 'required|string|max:70',
                'ref_transport_carriers_id' => 'required|integer_with_max|exists:reference_transport_permits_carriers,id',
                'vehicle_brand' => 'required|string|max:35',
                'vehicle_reg_number' => 'required|string|max:25',
                'trailer_reg_number' => 'nullable|string|max:25',
                'ref_origin_country' => 'required|integer_with_max|exists:reference_countries,id',
                'ref_destination_country' => 'required|integer_with_max|exists:reference_countries,id',
                'ref_transport_permit_type' => 'required|integer_with_max|exists:reference_transport_permit_types,id',

                'entry_goods_weight' => 'required|numeric|regex:/^[+-]?[0-9]{1,10}(\.[0-9]{1,6})?$/',
                'departure_goods_weight' => 'nullable|numeric|regex:/^[+-]?[0-9]{1,10}(\.[0-9]{1,6})?$/',

                'current_status' => 'required|in:' . implode(',', TransportApplication::TRANSPORT_APPLICATION_STATUSES)
            ];

            if ($data['ref_transport_permit_type']) {
                $refTransportPermitType = getReferenceRows(ReferenceTable::REFERENCE_TRANSPORT_PERMIT_TYPES, $data['ref_transport_permit_type']);

                if (!is_null($refTransportPermitType)) {

                    if ($refTransportPermitType->code != ReferenceTable::REFERENCE_TRANSPORT_PERMIT_TYPE_P3) {
                        $rules['vehicle_carrying_capacity'] = 'required|integer|digits_between:1,11';
                        $rules['trailer_carrying_capacity'] = 'nullable|integer|digits_between:1,11';
                        $rules['vehicle_empty_capacity'] = 'required|integer|digits_between:1,11';
                        $rules['trailer_empty_capacity'] = 'nullable|integer|digits_between:1,11';

                        // To Approve
                        if ($toApproveStatus) {
                            $rules['ref_border_crossing'] = 'required|integer_with_max|exists:reference_border_checkpoints,id';
                            $rules['arrival_date'] = 'required|date|after_or_equal:' . currentDate();
                            $rules['departure_date'] = 'nullable|date|after_or_equal:' . currentDate();
                        }

                    } else {
                        $rules['nature_of_goods'] = 'nullable|string|max:35';
                    }
                }
            }

            // To Approve
            if ($toApproveStatus) {
                $rules['permission_number'] = 'required|string|max:70';
                $rules['ref_issue_country'] = 'required|integer_with_max|exists:reference_countries,id';
                $rules['permit_validity_period_from'] = 'required|date|after_or_equal:' . currentDate();
                $rules['permit_validity_period_to'] = 'required|date|after_or_equal:permit_validity_period_from';
                $rules['period_validity'] = 'nullable|integer_with_max';
            }

        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'driver_name.required' => trans('core.base.field.required'),
            'driver_name.max' => trans('core.base.field.max_characters', ['max' => 70]),
            'driver_name.*' => trans('core.loading.invalid_data'),

            'ref_transport_carriers_id.*' => trans('core.loading.invalid_data'),

            'vehicle_brand.required' => trans('core.base.field.required'),
            'vehicle_brand.max' => trans('core.base.field.max_characters', ['max' => 35]),
            'vehicle_brand.*' => trans('core.loading.invalid_data'),

            'vehicle_reg_number.required' => trans('core.base.field.required'),
            'vehicle_reg_number.max' => trans('core.base.field.max_characters', ['max' => 25]),
            'vehicle_reg_number.*' => trans('core.loading.invalid_data'),

            'trailer_reg_number.max' => trans('core.base.field.max_characters', ['max' => 25]),
            'trailer_reg_number.*' => trans('core.loading.invalid_data'),

            'ref_origin_country.*' => trans('core.loading.invalid_data'),
            'ref_destination_country.*' => trans('core.loading.invalid_data'),
            'ref_transport_permit_type.*' => trans('core.loading.invalid_data'),
            'ref_border_crossing.*' => trans('core.loading.invalid_data'),

            'vehicle_carrying_capacity.required' => trans('core.base.field.required'),
            'vehicle_carrying_capacity.integer' => trans('validation.integer'),
            'vehicle_carrying_capacity.*' => trans('core.loading.invalid_data'),

            'trailer_carrying_capacity.integer' => trans('validation.integer'),
            'trailer_carrying_capacity.*' => trans('core.loading.invalid_data'),

            'vehicle_empty_capacity.required' => trans('core.base.field.required'),
            'vehicle_empty_capacity.integer' => trans('validation.integer'),
            'vehicle_empty_capacity.*' => trans('core.loading.invalid_data'),

            'trailer_empty_capacity.integer' => trans('validation.integers'),
            'trailer_empty_capacity.*' => trans('core.loading.invalid_data'),

            'nature_of_goods.max' => trans('core.base.field.max_characters', ['max' => 35]),
            'nature_of_goods.*' => trans('core.loading.invalid_data'),

            'entry_goods_weight.required' => trans('core.base.field.required'),
            'entry_goods_weight.numeric' => trans('validation.numeric'),
            'entry_goods_weight.*' => trans('core.loading.invalid_data'),

            'departure_goods_weight.numeric' => trans('validation.numeric'),
            'departure_goods_weight.*' => trans('core.loading.invalid_data'),

            'arrival_date.required' => trans('core.base.field.required'),
            'arrival_date.after_or_equal' => trans('core.base.field.start_date_or_equal', ['start' => formattedDate(currentDate())]),
            'arrival_date.*' => trans('core.loading.invalid_data'),

            'departure_date.after_or_equal' => trans('core.base.field.start_date_or_equal', ['start' => formattedDate(currentDate())]),
            'departure_date.*' => trans('core.loading.invalid_data'),

            'permission_number.required' => trans('core.base.field.required'),
            'permission_number.max' => trans('core.base.field.max_characters', ['max' => 70]),
            'permission_number.*' => trans('core.loading.invalid_data'),

            'permit_validity_period_from.required' => trans('core.base.field.required'),
            'permit_validity_period_from.after_or_equal' => trans('core.base.field.start_date_or_equal', ['start' => formattedDate(currentDate())]),
            'permit_validity_period_from.*' => trans('core.loading.invalid_data'),

            'permit_validity_period_to.required' => trans('core.base.field.required'),
            'permit_validity_period_to.after_or_equal' => trans('core.base.field.start_date_or_equal', ['start' => formattedDate($this->request->get('permit_validity_period_from'))]),
            'permit_validity_period_to.*' => trans('core.loading.invalid_data'),

            'period_validity.*' => trans('core.loading.invalid_data'),

            'transport_application_receipt_file.required' => trans('core.base.field.required'),
            'transport_application_receipt_file.*' => trans('core.loading.invalid_data'),
        ];
    }
}