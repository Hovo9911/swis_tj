<?php

namespace App\Http\Requests\TransportApplication;

use App\Http\Requests\Request;

/**
 * Class PaymentAmountRequest
 * @package App\Http\Requests\TransportApplication
 */
class TransportApplicationInvoiceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return[
            'payment_amount' => 'required|numeric|regex:/^[+-]?[0-9]{1,10}(\.[0-9]{1,6})?$/',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'payment_amount.required' => trans('core.base.field.required'),
            'payment_amount.numeric' => trans('validation.numeric'),
            'payment_amount.*' => trans('core.loading.invalid_data'),
        ];
    }
}