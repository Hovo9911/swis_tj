<?php

namespace App\Http\Requests\RegionOffice;

use App\Http\Requests\Request;

/**
 * Class RegionalOfficeSearchRequest
 * @package App\Http\Requests\RegionOffice
 */
class RegionalOfficeSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.id' => 'integer_with_max',
            'f.name' => 'string_with_max',
            'f.office_code' => 'string_with_max',
            'f.region' => 'string_with_max',
            'f.city' => 'string_with_max',
            'f.agency' => 'tax_id_validator|exists:agency,tax_id',
            'f.constructor_document' => 'string_with_max|exists:constructor_documents,document_code',
            'f.show_status' => 'in:1,2',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
