<?php

namespace App\Http\Requests\RegionOffice;

use App\Http\Requests\Request;
use App\Models\RegionalOffice\RegionalOffice;
use Carbon\Carbon;

/**
 * Class RegionOfficeRequest
 * @package App\Http\Requests\RegionOffice
 */
class RegionOfficeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = $this->request->all();

        $accreditationValidDate = Carbon::parse(currentDate());
        if (isset($data['accreditation_date_of_issue']) && $data['accreditation_date_of_issue'] > currentDate()) {
            $accreditationValidDate = $data['accreditation_date_of_issue'];
        }

        $rules = [
            'accreditation_number' => 'nullable|string|max:30',
            'accreditation_date_of_issue' => 'nullable|date',
            'accreditation_valid_until' => 'nullable|date|after_or_equal:' . $accreditationValidDate,
            'region' => 'required|string|max:250',
            'city' => 'required|string|max:250',
            'area' => 'required|string|max:250',
            'street' => 'required|string|max:250',
            'fax' => 'nullable|string|max:250',
            'phone_number' => 'required|phone_number_validator',
            'email' => 'nullable|email|max:250',
            'show_status' => 'required|in:1,2',
            'constructor_document' => 'required|array',
            'constructor_document.*' => 'string_with_max|exists:constructor_documents,document_code',
            'sort' => 'required|integer'
        ];

        if (!is_null($this->input('laboratories'))) {
            foreach ($this->input('laboratories') as $id => $lab) {
                if (isset($lab['enable'])) {
                    $rules["laboratories.{$id}.start_date"] = 'nullable';
                    $rules["laboratories.{$id}.end_date"] = 'nullable';
                }
            }
        }

        if (Auth()->user()->isSwAdmin()) {
            $rules['company_tax_id'] = 'required|tax_id_validator|exists:agency,tax_id';
        }

        if (empty($data['id'])) {
            $rules['office_code'] = 'required|string|max:35|unique:regional_office';
        } else {
            if ($data['show_status'] == RegionalOffice::STATUS_INACTIVE) {
                return [
                    'show_status' => 'required|in:1,2'
                ];
            }
        }

        foreach ($this->request->get('ml') as $key => $ml) {
            $rules['ml.' . $key . '.name'] = 'required|max:255';
            $rules['ml.' . $key . '.short_name'] = 'nullable|max:255';
            $rules['ml.' . $key . '.address'] = 'nullable|max:255';
            $rules['ml.' . $key . '.description'] = 'nullable|max:5000';
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        $data = $this->request->all();

        $accreditationValidDate = Carbon::parse(currentDate());
        if (isset($data['accreditation_date_of_issue']) && $data['accreditation_date_of_issue'] > currentDate()) {
            $accreditationValidDate = $data['accreditation_date_of_issue'];
        }

        $messages = [
            'agency_id.required' => trans('core.base.field.required'),
            'agency_id.*' => trans('core.loading.invalid_data'),

            'phone_number.required' => trans('core.base.field.required'),
            'phone_number.*' => trans('core.loading.invalid_data'),

            'accreditation_number.max' => trans('core.base.field.max_characters', ['max' => 30]),
            'accreditation_number.*' => trans('core.loading.invalid_data'),

            'accreditation_date_of_issue.*' => trans('core.loading.invalid_data'),

            'accreditation_valid_until.after_or_equal' => trans('core.base.field.start_date', ['start' => formattedDate($accreditationValidDate)]),
            'accreditation_valid_until.*' => trans('core.loading.invalid_data'),

            'office_code.required' => trans('core.base.field.required'),
            'office_code.unique' => trans('core.base.field.unique'),
            'office_code.max' => trans('core.base.field.max_characters', ['max' => 35]),
            'office_code.*' => trans('core.loading.invalid_data'),

            'address.max' => trans('core.base.field.max_characters', ['max' => 70]),
            'address.*' => trans('core.loading.invalid_data'),

            'region.required' => trans('core.base.field.required'),
            'region.max' => trans('core.base.field.max_characters', ['max' => 250]),
            'region.*' => trans('core.loading.invalid_data'),

            'city.required' => trans('core.base.field.required'),
            'city.max' => trans('core.base.field.max_characters', ['max' => 250]),
            'city.*' => trans('core.loading.invalid_data'),

            'area.required' => trans('core.base.field.required'),
            'area.max' => trans('core.base.field.max_characters', ['max' => 250]),
            'area.*' => trans('core.loading.invalid_data'),

            'street.required' => trans('core.base.field.required'),
            'street.max' => trans('core.base.field.max_characters', ['max' => 250]),
            'street.*' => trans('core.loading.invalid_data'),

            'email.max' => trans('core.base.field.max_characters', ['max' => 250]),
            'email.email' => trans('core.loading.email_valid'),
            'email.*' => trans('core.loading.invalid_data'),
        ];

        foreach ($this->request->get('ml') as $key => $ml) {
            $messages['ml.' . $key . '.name.max'] = trans('core.base.field.max_characters', ['max' => 255]);
            $messages['ml.' . $key . '.short_name.max'] = trans('core.base.field.max_characters', ['max' => 255]);
            $messages['ml.' . $key . '.address.max'] = trans('core.base.field.max_characters', ['max' => 255]);
            $messages['ml.' . $key . '.description.max'] = trans('core.base.field.max_characters', ['max' => 5000]);
        }

        return $messages;
    }
}
