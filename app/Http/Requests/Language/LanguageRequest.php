<?php

namespace App\Http\Requests\Language;

use App\Http\Requests\Request;

/**
 * Class LanguageRequest
 * @package App\Http\Requests\Language
 */
class LanguageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array|bool
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'code' => 'required|max:3',
            'show_status' => 'required|in:1,2'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => trans('core.base.field.required'),
            'name.max' => trans('core.base.field.max_characters', ['max' => 255]),
            'name.*' => trans('core.loading.invalid_data'),

            'code.required' => trans('core.base.field.required'),
            'code.max' => trans('core.base.field.max_characters', ['max' => 3]),
            'code.*' => trans('core.loading.invalid_data'),
        ];
    }
}
