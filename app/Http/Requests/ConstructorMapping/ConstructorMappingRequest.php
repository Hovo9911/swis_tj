<?php

namespace App\Http\Requests\ConstructorMapping;

use App\Http\Requests\Request;
use App\Models\ConstructorDocument\ConstructorDocument;

/**
 * Class ConstructorMappingRequest
 * @package App\Http\Requests\ConstructorMapping
 */
class ConstructorMappingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $constructorDocumentId = $this->input('document_id');
        $agencyDocumentsIds = ConstructorDocument::getConstructorDocumentForAgency()->pluck('id')->toArray();

        $rules = [
            'document_id' => 'required|integer|exists:constructor_documents,id|in:' . implode(',', $agencyDocumentsIds),
            'start_state' => 'required|integer_with_max|exists:constructor_states,id,constructor_document_id,' . $constructorDocumentId,
            'role' => 'required|integer_with_max|exists:constructor_documents_roles,role_id,document_id,' . $constructorDocumentId,
            'action' => 'required|integer_with_max|exists:constructor_actions,id,constructor_document_id,' . $constructorDocumentId,
            'end_state' => 'required|integer_with_max|exists:constructor_states,id,constructor_document_id,' . $constructorDocumentId,
            'description' => 'string|max:5000',
            'non_visible_list' => 'nullable|exists:constructor_lists,id,document_id,' . $constructorDocumentId,
            'editable_list' => 'nullable|exists:constructor_lists,id,document_id,' . $constructorDocumentId,
            'mandatory_list' => 'nullable|exists:constructor_lists,id,document_id,' . $constructorDocumentId,
            'obligation_exist' => 'nullable',
            'enable_risks' => 'nullable',
            'digital_signature' => 'nullable',
            'applicant' => 'integer|exists:notification_templates,id|nullable',
            'importer_exporter' => 'integer|exists:notification_templates,id|nullable',
            'agency_admin' => 'integer|exists:notification_templates,id|nullable',
            'persons_in_next_state' => 'integer|exists:notification_templates,id|nullable',
            'rules' => 'array|nullable',
            'show_status' => 'required|in:1,2'
        ];

        foreach ($this->request->get('ml') as $key => $ml) {
            $rules['ml.' . $key . '.description'] = 'nullable|max:500|string';
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        $messages = [
            'document_id.required' => trans('core.base.field.required'),
            'document_id.*' => trans('core.loading.invalid_data'),

            'non_visible_list.*' => trans('core.loading.record_not_found'),
            'editable_list.*' => trans('core.loading.record_not_found'),
            'mandatory_list.*' => trans('core.loading.record_not_found'),

            'applicant.*' => trans('core.loading.record_not_found'),
            'importer_exporter.*' => trans('core.loading.record_not_found'),
            'agency_admin.*' => trans('core.loading.record_not_found'),
            'persons_in_next_state.*' => trans('core.loading.record_not_found'),
        ];

        foreach ($this->request->get('ml') as $key => $ml) {
            $messages['ml.' . $key . '.description.max'] = trans('core.base.field.max_characters', ['max' => 500]);
        }

        return $messages;
    }
}
