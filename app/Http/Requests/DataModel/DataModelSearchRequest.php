<?php

namespace App\Http\Requests\DataModel;

use App\Http\Requests\Request;
use App\Models\DataModel\DataModel;

/**
 * Class DataModelSearchRequest
 * @package App\Http\Requests\DataModel
 */
class DataModelSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.id' => 'integer_with_max',
            'f.name' => 'string_with_max',
            'f.wco' => 'string_with_max',
            'f.eaec' => 'integer_with_max',
            'f.reference' => 'string_with_max',
            'f.definition' => 'string_with_max',
            'f.type' => 'in:' . implode(',', DataModel::MDM_ELEMENT_TYPES_ALL)
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
