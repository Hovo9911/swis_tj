<?php

namespace App\Http\Requests\DataModel;

use App\Http\Requests\Request;
use App\Models\DataModel\DataModel;
use Illuminate\Support\Facades\DB;

/**
 * Class DataModelRequest
 * @package App\Http\Requests\DataModel
 */
class DataModelRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'excel_id' => 'nullable|integer_with_max',
            'reference' => 'nullable|string_with_max',
            'wco_id' => 'nullable|string_with_max',
            'wco' => 'nullable|string_with_max',
            'wco_class_name' => 'nullable|string_with_max',
            'eaec_id' => 'nullable|max:512',
            'eaec' => 'nullable|string_with_max',
            'eaeu_min' => 'nullable|integer_with_max',
            'eaeu_max' => 'nullable|integer_with_max',
            'eaeu_total_digits' => 'nullable|integer_with_max',
            'eaeu_fraction_digits' => 'nullable|integer_with_max',
            'eaeu_type' => 'nullable|string_with_max',
            'available_at' => 'required|date',
            'available_to' => 'nullable|date|after:available_at',
            'definition' => 'nullable|string_with_max',
            'type' => 'required|in:' . implode(',', DataModel::MDM_ELEMENT_TYPES_ALL),
            'interface_format' => 'required|string_with_max',
            'pattern' => 'nullable|string_with_max|regex_validator',
            'is_content' => 'nullable'
        ];

        $dataClientMin = false;
        $dataClientMax = false;
        $dataDbMax = false;

        if (!empty($this->request->get('id'))) {
            $dataModel = DataModel::where(DB::raw('id::varchar'), $this->request->get('id'))->firstOrFail();

            $dataModel->client_min ? $dataClientMin = '|max:' . $dataModel->client_min : '';
            $dataModel->client_max ? $dataClientMax = '|min:' . $dataModel->client_max : '';
            $dataModel->db_max ? $dataDbMax = '|min:' . $dataModel->db_max : '';

            if ($dataModel->available_at != $this->request->get('available_at')) {

                if (currentDate() > $dataModel->available_at) {
                    $rules['available_at'] .= '|date_equals:' . $dataModel->available_at;
                }

                $rules['available_at'] .= '|after_or_equal:' . currentDate();
                $rules['available_to'] .= '|after_or_equal:' . currentDate();

            }

            if (!empty($dataModel->available_to) && ($dataModel->available_to != $this->request->get('available_to'))) {

                if (currentDate() > $dataModel->available_to) {
                    $rules['available_to'] .= '|date_equals:' . $dataModel->available_to;
                }
            }

            if ($this->request->get('available_to')) {
                $rules['available_to'] .= '|after_or_equal:' . currentDate();
            }

            $rules['name'] = "required|string_with_max|unique:data_model_excel,name," . $dataModel->id;

        } else {
            $rules['name'] = "required|string_with_max|unique:data_model_excel,name";
            $rules['available_at'] .= '|after_or_equal:' . currentDate();
        }

        foreach ($this->request->get('ml') as $key => $ml) {
            $rules['ml.' . $key . '.description'] = 'max:512';
        }

        $type = $this->request->get('type');

        if ($type == 'an' || $type == 'n' || $type == 'a') {
            $rules['client_min'] = 'required|numeric|integer_with_max' . $dataClientMin;
            $rules['client_max'] = 'required|numeric|integer_with_max' . $dataClientMax;

            $rules['db_min'] = 'nullable|numeric|integer_with_max';
            $rules['db_max'] = 'required|numeric|integer_with_max' . $dataDbMax;

            $rules['decimal_fraction'] = 'nullable|integer_with_max';
        }

        if ($type == 'n') {
            $rules['client_min'] .= '|numeric|integer_with_max' . $dataClientMin;
            $rules['client_max'] .= '|numeric|integer_with_max' . $dataClientMax;

            $rules['db_min'] .= '|numeric|integer_with_max';
            $rules['db_max'] .= '|numeric|integer_with_max' . $dataDbMax;

            $rules['decimal_fraction'] = 'nullable|integer_with_max';
        }

        if ($type == 'd' || $type == 'dt') {
            $rules['client_min'] = 'nullable|date' . $dataClientMin;
            $rules['client_max'] = 'nullable|date' . $dataClientMax;

            $rules['db_min'] = 'nullable|date';
            $rules['db_max'] = 'nullable|date' . $dataDbMax;

            $rules['decimal_fraction'] = 'nullable|integer_with_max';
        }

        if ($type == 'dc') {
            $rules['client_min'] = 'required|numeric|integer_with_max' . $dataClientMin;
            $rules['client_max'] = 'required|numeric|integer_with_max' . $dataClientMax;

            $rules['db_min'] = 'nullable|numeric|integer_with_max';
            $rules['db_max'] = 'required|numeric|integer_with_max' . $dataDbMax;

            $rules['decimal_fraction'] = 'required|integer_with_max';
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        $dataModel = false;
        if (!empty($this->request->get('id'))) {
            $dataModel = DataModel::where(DB::raw('id::varchar'), $this->request->get('id'))->firstOrFail();
        }

        $messages = [
            'excel_id.required' => trans('core.base.field.required'),
            'excel_id.*' => trans('core.loading.invalid_data'),

            'name.required' => trans('core.base.field.required'),
            'name.unique' => trans('core.base.field.unique'),
            'name.*' => trans('core.loading.invalid_data'),

            'wco_id.*' => trans('core.loading.invalid_data'),
            'eaec_id.*' => trans('core.loading.invalid_data'),

            'decimal_fraction.*' => trans('core.loading.invalid_data'),

            'client_min.max' => trans('core.base.field.max_number', ['max' => $dataModel ? $dataModel->client_min : '']),
            'client_min.*' => trans('core.loading.invalid_data'),
            'client_max.min' => trans('core.base.field.min_number', ['min' => $dataModel ? $dataModel->client_max : '']),
            'client_max.*' => trans('core.loading.invalid_data'),

            'db_min.*' => trans('core.loading.invalid_data'),
            'db_max.min' => trans('core.base.field.min_number', ['min' => $dataModel ? $dataModel->db_max : '']),
            'db_max.*' => trans('core.loading.invalid_data'),

            'available_at.required' => trans('core.base.field.required'),
            'available_at.after_or_equal' => trans('core.base.field.start_date', ['start' => currentDate()]),
            'available_at.date_equals' => trans('core.loading.cannot_change_date'),
            'available_at.*' => trans('core.loading.invalid_data'),

            'available_to.after' => trans('core.base.field.start_date', ['start' => date(config('swis.date_format'), strtotime($this->request->get('available_at')))]),
            'available_to.date_equals' => trans('core.loading.cannot_change_date'),
            'available_to.after_or_equal' => trans('swis.data_model.field.invalid_end_date'),
            'available_to.*' => trans('core.loading.invalid_data'),
        ];

        foreach ($this->request->get('ml') as $key => $ml) {
            $messages['ml.' . $key . '.description.max'] = trans('core.base.field.max_characters', ['max' => 512]);
        }

        return $messages;
    }
}
