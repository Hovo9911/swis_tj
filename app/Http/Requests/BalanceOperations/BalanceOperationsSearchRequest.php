<?php


namespace App\Http\Requests\BalanceOperations;


use App\Http\Requests\Request;

class BalanceOperationsSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.balance_search_id' => 'integer_with_max',
            'f.balance_search_ssn' => 'string_with_max',
            'f.balance_search_company' => 'exists:company,id',
            'f.balance_min' => 'between:0,99.99',
            'f.balance_max' => 'between:0,99.99',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}