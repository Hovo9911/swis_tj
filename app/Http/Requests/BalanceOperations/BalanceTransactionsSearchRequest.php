<?php

namespace App\Http\Requests\BalanceOperations;

use App\Http\Requests\Request;

class BalanceTransactionsSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.transactions_search_id' => 'integer_with_max',
            'f.ssn' => 'string_with_max',
            'f.transaction_date_start' => 'date',
            'f.transaction_date_end' => 'date',
            'f.company' => 'exists:company,id',
            'f.sum_min' => 'between:0,99.99',
            'f.sum_max' => 'between:0,99.99',
            'f.username' => 'string_with_max',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}