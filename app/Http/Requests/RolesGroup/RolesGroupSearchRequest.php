<?php

namespace App\Http\Requests\RolesGroup;

use App\Http\Requests\Request;

/**
 * Class RolesGroupSearchRequest
 * @package App\Http\Requests\RolesGroup
 */
class RolesGroupSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.id' => 'integer_with_max',
            'f.name' => 'string_with_max',
            'f.menu' => 'integer_with_max|exists:menu,id',
            'f.created_at_start' => 'date',
            'f.created_at_end' => 'date',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
