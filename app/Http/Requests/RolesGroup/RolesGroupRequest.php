<?php

namespace App\Http\Requests\RolesGroup;

use App\Http\Requests\Request;
use App\Models\Menu\Menu;

/**
 * Class RolesGroupRequest
 * @package App\Http\Requests\RolesGroup
 */
class RolesGroupRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = $this->request->all();

        $rules = [
            'show_status' => 'required|in:1,2',
            'roles' => 'required|array',
            'roles.*' => 'integer_with_max|exists:roles,id',
        ];

        if (empty($data['id'])) {
            $rules['menu_id'] = 'required|integer_with_max|exists:menu,id';
        }

        if (isset($data['menu_id']) && $data['menu_id'] == Menu::getMenuIdByName(Menu::USER_DOCUMENTS_MENU_NAME)) {
            $rules['roles_attributes'] = 'nullable|array';
            $rules['roles_attributes.*'] = 'string_with_max';
        }

        foreach ($this->request->get('ml') as $key => $ml) {
            $rules['ml.' . $key . '.name'] = 'required|max:255';
            $rules['ml.' . $key . '.description'] = 'nullable|max:5000';
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        $messages = [];
        foreach ($this->request->get('ml') as $key => $ml) {
            $messages['ml.' . $key . '.name.required'] = trans('core.base.field.required');
            $messages['ml.' . $key . '.name.max'] = trans('core.base.field.max_characters', ['max' => 255]);
            $messages['ml.' . $key . '.name.*'] = trans('core.loading.invalid_data');
            $messages['ml.' . $key . '.description.max'] = trans('core.base.field.max_characters', ['max' => 5000]);
            $messages['ml.' . $key . '.description.*'] = trans('core.loading.invalid_data');
        }

        return $messages;
    }
}
