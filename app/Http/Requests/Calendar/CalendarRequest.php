<?php

namespace App\Http\Requests\Calendar;

use App\Http\Requests\Request;

/**
 * Class BrokerRequest
 * @package App\Http\Requests\Broker
 */
class CalendarRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required|array',
            'date.*' => 'required|string|after_or_equal:' . date(config('swis.date_format')),
            'year' => 'in:' . now()->year . ',' . ((int)now()->year + 1)
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'tax_id.required' => trans('core.base.field.required'),
            'date.*' => trans('swis.calendar.date.invalid_date')
        ];
    }
}
