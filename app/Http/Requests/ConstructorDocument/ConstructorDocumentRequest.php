<?php

namespace App\Http\Requests\ConstructorDocument;

use App\Http\Requests\Request;
use App\Models\ConstructorDocument\ConstructorDocument;
use Illuminate\Support\Facades\Auth;

/**
 * Class ConstructorDocumentRequest
 * @package App\Http\Requests\ConstructorDocument
 */
class ConstructorDocumentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'start_date' => 'required|date',
            'end_date' => 'nullable|date',
            'version' => 'required|max:11|string',
            'show_status' => 'integer|in:1,2',
            'digital_signature' => 'nullable',
            'document_type_id' => 'required|integer|exists:reference_document_type_reference,id',
            'expertise_type' => 'nullable',
            'is_lab_flow' => 'nullable'
        ];

        if (Auth::user()->isLaboratory() && $this->input('is_lab_flow')) {

            $isAlreadyExistLabFlow = ConstructorDocument::where('company_tax_id', Auth::user()->companyTaxId())
                ->where('is_lab_flow', true)
                ->when($this->input('id'), function ($q) {
                    $q->where('id', '!=', $this->input('id'));
                })
                ->exists();

            $rules['is_lab_flow'] = ($isAlreadyExistLabFlow) ? 'required|unique_list' : '';
        }

        if (config('global.digital_signature')) {
            $rules['digital_signature'] = 'in:0,1';
        }

        if (empty($this->request->get('id'))) {
            $rules['document_code'] = 'required|max:10|string|unique:constructor_documents,document_code';
        }

        foreach ($this->request->get('ml') as $key => $ml) {
            $rules['ml.' . $key . '.description'] = 'nullable|max:500|string';
            $rules['ml.' . $key . '.document_name'] = 'required|max:100|string';
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        $messages = [
            'document_code.required' => trans('core.base.field.required'),
            'document_code.max' => trans('core.base.field.max_characters', ['max' => 10]),
            'document_code.unique' => trans('core.base.field.unique'),
            'document_code.*' => trans('core.loading.invalid_data'),

            'start_date.required' => trans('core.base.field.required'),
            'start_date.*' => trans('core.loading.invalid_data'),
            'end_date.*' => trans('core.loading.invalid_data'),

            'version.required' => trans('core.base.field.required'),
            'version.max' => trans('core.base.field.max_characters', ['max' => 11]),
            'version.*' => trans('core.loading.invalid_data'),

            'document_type_id.required' => trans('core.base.field.required'),
            'document_type_id.*' => trans('core.loading.invalid_data'),

            'is_lab_flow.unique_list' => trans('swis.constructor_documents.lab_flow.is_already_exists')
        ];

        foreach ($this->request->get('ml') as $key => $ml) {
            $messages['ml.' . $key . '.description.max'] = trans('core.base.field.max_characters', ['max' => 500]);
            $messages['ml.' . $key . '.document_name.max'] = trans('core.base.field.max_characters', ['max' => 100]);
        }

        return $messages;
    }

}