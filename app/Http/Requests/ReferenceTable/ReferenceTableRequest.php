<?php

namespace App\Http\Requests\ReferenceTable;

use App\Http\Requests\Request;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\ReferenceTable\ReferenceTableStructure;

/**
 * Class ReferenceTableRequest
 * @package App\Http\Requests\ReferenceTable
 */
class ReferenceTableRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $dataValues = $this->request->all();
        $startRefDate = currentDate();

        if ($this->request->get('id')) {
            $currentRef = ReferenceTable::select('show_status', 'available_at')->where('id', $this->request->get('id'))->firstOrFail();

            if ($currentRef->show_status != ReferenceTable::STATUS_ACTIVE) {

                return [
                    'show_status' => 'required|in:1,2'
                ];
            }

            $startRefDate = $currentRef->available_at;
        }

        $rules = [
            'manually_edit' => 'required|integer_with_max',
            'id' => 'nullable|integer_with_max',
            'api_url' => "nullable|string_with_max",
            'import_format' => 'nullable|array',
            'import_format.*' => 'in:' . implode(',', ReferenceTable::REFERENCE_IMPORT_FORMATS),
            'available_at' => 'required|date|after_or_equal:' . $startRefDate,
            'table_name' => 'required|string|max:50|regex:/^[a-z][a-z0-9\_]*$/',
            'show_status' => 'required|in:1,2'
        ];

        foreach ($this->request->get('ml') as $key => $ml) {
            $rules['ml.' . $key . '.name'] = 'required|max:255';
        }

        $structure = $this->request->get('structure');

        if ($structure) {

            $existFieldNames = ReferenceTable::DEFAULT_STRUCTURE_NAMES;

            foreach ($structure as $key => $arr) {

                $columnType = $arr['type'];

                $rules['structure.' . $key . '.field_name'] = 'required|string|regex:/^[a-z][a-z0-9\_]*$/|max:50';
                $rules['structure.' . $key . '.type'] = 'required|string';
                $rules['structure.' . $key . '.required'] = 'nullable|integer_with_max';
                $rules['structure.' . $key . '.searchable'] = 'nullable|integer_with_max';
                $rules['structure.' . $key . '.search_filter'] = 'nullable|integer_with_max';
                $rules['structure.' . $key . '.sortable'] = 'nullable|integer_with_max';
                $rules['structure.' . $key . '.sort_order'] = 'required|integer_with_max';
                $rules['structure.' . $key . '.has_ml'] = 'nullable|in:1';
                $rules['structure.' . $key . '.description'] = 'nullable|string';
                $rules['structure.' . $key . '.show_status'] = 'required|in:1,2';

                if (!in_array($arr['field_name'], $existFieldNames)) {
                    $existFieldNames[] = $arr['field_name'];
                } else {
                    $rules['structure.' . $key . '.field_name'] = 'unique_list';
                }

                switch ($columnType) {

                    // Number, Text
                    case ReferenceTableStructure::COLUMN_TYPE_NUMBER:
                    case ReferenceTableStructure::COLUMN_TYPE_TEXT:

                        $rules['structure.' . $key . '.min'] = 'required|integer';
                        $rules['structure.' . $key . '.max'] = 'required|integer|min:' . intval($structure[$key]['min']);

                        break;

                    // Decimal
                    case ReferenceTableStructure::COLUMN_TYPE_DECIMAL:

                        $rules['structure.' . $key . '.left'] = 'required|numeric';
                        $rules['structure.' . $key . '.right'] = 'required|numeric';

                        break;

                    // Date, Datetime
                    case ReferenceTableStructure::COLUMN_TYPE_DATETIME:
                    case ReferenceTableStructure::COLUMN_TYPE_DATE:

                        $startStruckDate = $endStruckDate = null;

                        if ($this->request->get('id')) {
                            $refStructures = ReferenceTableStructure::where('reference_table_id', $this->request->get('id'))->get();

                            if ($refStructures->count()) {
                                foreach ($refStructures as $refStructure) {
                                    if ($refStructure->field_name == $arr['field_name']) {
                                        $startStruckDate = $refStructure['parameters']['start'];
                                        $endStruckDate = $refStructure['parameters']['end'];
                                    }
                                }
                            }
                        }

                        $rules['structure.' . $key . '.start'] = 'required|date';
                        $rules['structure.' . $key . '.end'] = 'nullable|date|after_or_equal:' . 'structure.' . $key . '.start';

                        // --
                        if (!is_null($startStruckDate)) {
                            $rules['structure.' . $key . '.start'] .= '|after_or_equal:' . $startStruckDate;
                        }

                        if (!is_null($endStruckDate)) {

                            if ($columnType == ReferenceTableStructure::COLUMN_TYPE_DATETIME) {
                                $endStruckDate = currentDateTime();
                            }
                            $rules['structure.' . $key . '.start'] .= '|before_or_equal:' . $endStruckDate;
                            $rules['structure.' . $key . '.end'] = '|after_or_equal:' . $endStruckDate;
                        }

                        break;

                    // Autocomplete
                    case ReferenceTableStructure::COLUMN_TYPE_AUTOCOMPLETE:

                        $rules['structure.' . $key . '.description'] = 'string_with_max';
                        $rules['structure.' . $key . '.source'] = 'required|integer_with_max|exists:reference_table,id';
                        $rules['structure.' . $key . '.fields'] = 'required|array';
                        $rules['structure.' . $key . '.fields.*'] = 'integer_with_max|exists:reference_table_structures,id';

                        break;

                    // Select
                    case ReferenceTableStructure::COLUMN_TYPE_SELECT:

                        $rules['structure.' . $key . '.source'] = 'required|integer_with_max|exists:reference_table,id';

                        break;
                }

                //
                if ($columnType != ReferenceTableStructure::COLUMN_TYPE_TEXT && isset($arr['has_ml'])) {
                    $rules['structure.' . $key . '.has_ml'] = 'field_type_cant_be_ml';
                }

            }
        }

        // ------
        if (isset($dataValues['structure'])) {

            $existSortNumbers = [1, 2, 3, 4];
            foreach ($dataValues['structure'] as $key => $value) {

                if (in_array($value['sort_order'], $existSortNumbers)) {
                    $rules['structure.' . $key . '.sort_order'] = 'required|sort_order';
                } else {
                    $existSortNumbers[] = $value['sort_order'];
                }
            }
        }

        // ------
        $rules['agencies'] = 'nullable|array';
        $rules['agencies.*.id'] = 'integer_with_max|exists:agency,id';

        if (isset($dataValues['agencies'])) {

            foreach ($dataValues['agencies'] as $agency) {
                if (isset($agency['id'])) {
                    $rules['agencies.' . $agency['id'] . '.access'] = 'required|string|in:' . implode(',', ReferenceTable::REFERENCE_AGENCY_ACCESS_TYPES);
                    $rules['agencies.' . $agency['id'] . '.show_only_self'] = 'nullable|integer|max:1';
                }
            }
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        $structure = $this->request->get('structure');
        $startRefDate = currentDate();

        if ($this->request->get('id')) {
            $currentRef = ReferenceTable::select('show_status', 'available_at')->where('id', $this->request->get('id'))->firstOrFail();

            if ($currentRef->show_status != ReferenceTable::STATUS_ACTIVE) {
                return [];
            }

            $startRefDate = $currentRef->available_at;
        }

        $messages = [
            'api_url.*' => trans('core.loading.invalid_data'),
            'available_at.required' => trans('core.base.field.required'),
            'available_at.after_or_equal' => trans('swis.reference_table.field.start_date', ['start' => $startRefDate]),
            'available_at.*' => trans('core.loading.invalid_data'),
            'table_name.required' => trans('core.base.field.required'),
            'table_name.max' => trans('core.base.field.max_number', ['max' => 50]),
            'table_name.*' => trans('core.loading.invalid_data'),
            'agencies.required' => trans('core.base.field.required'),
            'agencies.*' => trans('core.loading.invalid_data'),
        ];

        if ($structure) {

            foreach ($structure as $key => $arr) {

                $columnType = $arr['type'];

                $messages['structure.' . $key . '.field_name.required'] = trans('core.base.field.required');
                $messages['structure.' . $key . '.field_name.unique_list'] = trans('core.base.field.unique');
                $messages['structure.' . $key . '.field_name.*'] = trans('core.loading.invalid_data');

                $messages['structure.' . $key . '.type.required'] = trans('core.base.field.required');
                $messages['structure.' . $key . '.type.*'] = trans('core.loading.invalid_data');

                $messages['structure.' . $key . '.required.*'] = trans('core.loading.invalid_data');
                $messages['structure.' . $key . '.searchable.*'] = trans('core.loading.invalid_data');
                $messages['structure.' . $key . '.search_filter.*'] = trans('core.loading.invalid_data');
                $messages['structure.' . $key . '.sortable.*'] = trans('core.loading.invalid_data');
                $messages['structure.' . $key . '.description.*'] = trans('core.loading.invalid_data');

                $messages['structure.' . $key . '.has_ml.field_type_cant_be_ml'] = trans('swis.reference.field.cant_be_ml.message');
                $messages['structure.' . $key . '.has_ml.*'] = trans('core.loading.invalid_data');

                $messages['structure.' . $key . '.sort_order.required'] = trans('core.base.field.required');
                $messages['structure.' . $key . '.sort_order.*'] = trans('core.loading.invalid_data');

                $messages['structure.' . $key . '.left.required'] = trans('core.base.field.required');
                $messages['structure.' . $key . '.left.*'] = trans('core.loading.invalid_data');
                $messages['structure.' . $key . '.right.required'] = trans('core.base.field.required');
                $messages['structure.' . $key . '.right.*'] = trans('core.loading.invalid_data');

                switch ($columnType) {

                    // Number, Text
                    case ReferenceTableStructure::COLUMN_TYPE_NUMBER:
                    case ReferenceTableStructure::COLUMN_TYPE_TEXT:

                        $messages['structure.' . $key . '.min.required'] = trans('core.base.field.required');
                        $messages['structure.' . $key . '.min.*'] = trans('core.loading.invalid_data');

                        $minTrans = 'core.base.field.min_characters';
                        if ($columnType == ReferenceTableStructure::COLUMN_TYPE_NUMBER) {
                            $minTrans = 'core.base.field.min_number';
                        }

                        $messages['structure.' . $key . '.max.required'] = trans('core.base.field.required');
                        $messages['structure.' . $key . '.max.min'] = trans($minTrans, ['min' => $structure[$key]['min']]);
                        $messages['structure.' . $key . '.max.*'] = trans('core.loading.invalid_data');

                        break;

                    // Date, Datetime
                    case ReferenceTableStructure::COLUMN_TYPE_DATETIME:
                    case ReferenceTableStructure::COLUMN_TYPE_DATE:

                        $startStruckDate = $endStruckDate = null;

                        if ($this->request->get('id')) {
                            $refStructures = ReferenceTableStructure::where('reference_table_id', $this->request->get('id'))->get();

                            if ($refStructures->count()) {
                                foreach ($refStructures as $refStructure) {

                                    if ($refStructure->field_name == $arr['field_name']) {
                                        $startStruckDate = $refStructure['parameters']['start'];
                                        $endStruckDate = $refStructure['parameters']['end'];
                                    }
                                }
                            }
                        }

                        // Start
                        $messages['structure.' . $key . '.start.required'] = trans('core.base.field.required');

                        if (!is_null($startStruckDate)) {
                            $messages['structure.' . $key . '.start.after_or_equal'] = trans('swis.reference_table.start_date.message', ['start_date' => $startStruckDate]);
                        }

                        // End
                        $messages['structure.' . $key . '.end.required'] = trans('core.base.field.required');

                        if (!is_null($endStruckDate)) {
                            $messages['structure.' . $key . '.end.after_or_equal'] = trans('swis.reference_table.end_date.message', ['start_date' => $endStruckDate]);
                            $messages['structure.' . $key . '.start.before_or_equal'] = trans('swis.reference_table.end_date.message', ['start_date' => $endStruckDate]);
                        } else {
                            $messages['structure.' . $key . '.end.after_or_equal'] = trans('swis.reference_table.end_date.message', ['start_date' => date(config('swis.date_format'), strtotime($structure[$key]['start']))]);
                        }

                        $messages['structure.' . $key . '.end.after'] = trans('core.base.field.start_date', ['start' => date(config('swis.date_format'), strtotime($structure[$key]['start']))]);
                        $messages['structure.' . $key . '.end.*'] = trans('core.loading.invalid_data');
                        $messages['structure.' . $key . '.start.*'] = trans('core.loading.invalid_data');

                        break;

                    // Autocomplete
                    case ReferenceTableStructure::COLUMN_TYPE_AUTOCOMPLETE:

                        $messages['structure.' . $key . '.source.required'] = trans('core.base.field.required');
                        $messages['structure.' . $key . '.source.*'] = trans('core.loading.invalid_data');
                        $messages['structure.' . $key . '.fields.required'] = trans('core.base.field.required');
                        $messages['structure.' . $key . '.fields.*'] = trans('core.loading.invalid_data');

                        break;

                    // Select
                    case ReferenceTableStructure::COLUMN_TYPE_SELECT:

                        $messages['structure.' . $key . '.source.required'] = trans('core.base.field.required');
                        $messages['structure.' . $key . '.source.*'] = trans('core.loading.invalid_data');

                        break;

                }
            }
        }

        foreach ($this->request->get('ml') as $key => $ml) {
            $messages['ml.' . $key . '.name.required'] = trans('core.base.field.required');
            $messages['ml.' . $key . '.name.max'] = trans('core.base.field.max_characters', ['max' => '255']);
            $messages['ml.' . $key . '.name.*'] = trans('core.loading.invalid_data');
        }

        return $messages;
    }

}
