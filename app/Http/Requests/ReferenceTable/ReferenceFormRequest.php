<?php

namespace App\Http\Requests\ReferenceTable;

use App\Http\Requests\Request;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\ReferenceTable\ReferenceTableStructure;
use App\Rules\ValidateDecimalRule;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class ReferenceFormRequest
 * @package App\Http\Requests\ReferenceTable
 */
class ReferenceFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');
        $formId = $this->route('id2');

        $referenceTable = ReferenceTable::where('id', $id)->firstOrFail();
        $referenceFormTable = DB::table('reference_' . $referenceTable->table_name)->where('id', $formId)->first();

        if ($referenceTable->show_status != ReferenceTable::STATUS_ACTIVE) {
            return [];
        }

        // To Active
        if ($this->input('show_status') == ReferenceTable::STATUS_ACTIVE && optional($referenceFormTable)->show_status == ReferenceTable::STATUS_INACTIVE) {
            $rules['id'] = 'integer_with_max';
            $rules['show_status'] = 'in:1,2';
            return $rules;
        }

        $refStructure = $referenceTable->structure;
        $rules = [];

        $rules['id'] = 'integer_with_max';
        $rules['show_status'] = 'required|in:1,2';

        foreach ($refStructure as $structure) {

            $required = ($structure->required && $this->input('show_status') == ReferenceTable::STATUS_ACTIVE) ? 'required' : 'nullable';

            if ($structure->show_status == ReferenceTable::STATUS_INACTIVE) {
                continue;
            }

            if (!$structure->has_ml) {

                $fieldName = $structure->field_name;

                switch ($structure->type) {

                    // Text
                    case ReferenceTableStructure::COLUMN_TYPE_TEXT:

                        $type = ReferenceTableStructure::VALIDATION_TYPES[ReferenceTableStructure::COLUMN_TYPE_TEXT];
                        $min = 'min:' . $structure->parameters['min'];
                        $max = 'max:' . $structure->parameters['max'];
                        $rules[$fieldName] = $required . '|' . $type . '|' . $min . '|' . $max;

                        break;

                    // Number
                    case ReferenceTableStructure::COLUMN_TYPE_NUMBER:

                        $type = ReferenceTableStructure::VALIDATION_TYPES[ReferenceTableStructure::COLUMN_TYPE_NUMBER];
                        $min = 'min:' . $structure->parameters['min'];
                        $max = 'max:' . $structure->parameters['max'];
                        $rules[$fieldName] = $required . '|' . $type . '|' . $min . '|' . $max;

                        break;

                    // Decimal
                    case ReferenceTableStructure::COLUMN_TYPE_DECIMAL:

                        $type = ReferenceTableStructure::VALIDATION_TYPES[ReferenceTableStructure::COLUMN_TYPE_DECIMAL];
                        $rules[$fieldName] = [$required, $type, new ValidateDecimalRule($structure->parameters['left'], $structure->parameters['right'])];

                        break;

                    // Date
                    case ReferenceTableStructure::COLUMN_TYPE_DATE:

                        $rules[$fieldName] = $required . '|' . ReferenceTableStructure::COLUMN_TYPE_DATE;

                        if (!$formId && $fieldName == ReferenceTable::REFERENCE_DEFAULT_FIELD_START_DATE) {
                            $rules[$fieldName] .= '|after_or_equal:' . currentDate();
                        }

                        if ($fieldName == ReferenceTable::REFERENCE_DEFAULT_FIELD_START_DATE && !is_null($referenceFormTable) && Carbon::now() >= $referenceFormTable->{$fieldName}) {
                            if ($this->request->get('start_date') != $referenceFormTable->{$fieldName}) {
                                $rules[$fieldName] = 'date_equals:' . $referenceFormTable->{$fieldName};
                            }
                        }

                        if ($fieldName == ReferenceTable::REFERENCE_DEFAULT_FIELD_END_DATE) {
                            if (!$formId) {
                                $rules[$fieldName] .= '|after:start_date';
//                                $rules[$fieldName] .= '|after_or_equal:' . currentDate();
                            } else {
                                if ($referenceFormTable->{$fieldName} != $this->request->get(ReferenceTable::REFERENCE_DEFAULT_FIELD_END_DATE)) {
                                    $rules[$fieldName] .= '|after:start_date';
                                }
                            }
                        }

                        if (!empty($structure->parameters['start']) && $fieldName != ReferenceTable::REFERENCE_DEFAULT_FIELD_START_DATE && $fieldName != ReferenceTable::REFERENCE_DEFAULT_FIELD_END_DATE) {
                            $rules[$fieldName] .= '|after_or_equal:' . date(config('swis.date_format'), strtotime($structure->parameters['start']));
                        }

                        if (!empty($structure->parameters['end']) && $fieldName != ReferenceTable::REFERENCE_DEFAULT_FIELD_START_DATE && $fieldName != ReferenceTable::REFERENCE_DEFAULT_FIELD_END_DATE) {
                            $rules[$fieldName] .= '|before_or_equal:' . date(config('swis.date_format'), strtotime($structure->parameters['end']));
                        }

                        break;

                    // Datetime
                    case ReferenceTableStructure::COLUMN_TYPE_DATETIME:

                        $rules[$fieldName] = $required . '|' . ReferenceTableStructure::COLUMN_TYPE_DATE;

                        if (!empty($structure->parameters['start'])) {
                            $rules[$fieldName] .= '|after_or_equal:' . date(config('swis.date_time_format'), strtotime($structure->parameters['start']));
                        }

                        if (!empty($structure->parameters['end'])) {
                            $rules[$fieldName] .= '|before_or_equal:' . date(config('swis.date_time_format'), strtotime($structure->parameters['end']));
                        }

                        break;

                    // Select
                    case ReferenceTableStructure::COLUMN_TYPE_SELECT:

                        $inCheck = '';
                        if (!Auth::user()->isSwAdmin() && $structure->parameters['source'] == ReferenceTable::getReferenceAgencyId()) {
                            $refAgencyRow = ReferenceTable::getAuthUserAgencyRefRow();
                            if (!is_null($refAgencyRow)) {
                                $inCheck = "in:" . $refAgencyRow->id . '|';
                            }
                        }

                        $type = ReferenceTableStructure::VALIDATION_TYPES[ReferenceTableStructure::COLUMN_TYPE_SELECT];
                        $sourceReferenceTable = ReferenceTable::where('id', $structure->parameters['source'])->pluck('table_name')->first();

                        $rules[$fieldName] = $required . '|' . $type . '|' . $inCheck . 'exists:reference_' . $sourceReferenceTable . ',id';

                        break;

                    // Autocomplete
                    case ReferenceTableStructure::COLUMN_TYPE_AUTOCOMPLETE:

                        $inCheck = '';
                        if (!Auth::user()->isSwAdmin() && $structure->parameters['source'] == ReferenceTable::getReferenceAgencyId()) {
                            $refAgencyRow = ReferenceTable::getAuthUserAgencyRefRow();
                            if (!is_null($refAgencyRow)) {
                                $inCheck = "in:" . $refAgencyRow->id . '|';
                            }
                        }

                        $sourceReferenceTable = ReferenceTable::where('id', $structure->parameters['source'])->pluck('table_name')->first();

                        $rules[$fieldName] = $required . '|' . $inCheck . 'exists:reference_' . $sourceReferenceTable . ',id';
                        $rules[$fieldName . '_description'] = '|max:5000';

                        break;
                }
            }

            //
            if ($structure->has_ml) {

                foreach ($this->request->get('ml') as $key => $ml) {

                    $type = ReferenceTableStructure::COLUMN_VALIDATION_TYPE_STRING;
                    $min = 'min:' . $structure->parameters['min'];
                    $max = 'max:' . $structure->parameters['max'];

                    $rules['ml.' . $key . '.' . $structure->field_name] = $required . '|' . $type . '|' . $min . '|' . $max;

                }
            }
        }

        $rules['code'] .= '|without_spaces';

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        $id = $this->route('id');
        $formId = $this->route('id2');

        $referenceTable = ReferenceTable::where('id', $id)->firstOrFail();
        $referenceFormTable = DB::table('reference_' . $referenceTable->table_name)->where('id', $formId)->first();

        if ($referenceTable->show_status != ReferenceTable::STATUS_ACTIVE) {
            return [];
        }

        $refStructure = $referenceTable->structure;

        $messages = [];
        foreach ($refStructure as $structure) {

            $fieldName = $structure->field_name;

            if (!$structure->has_ml) {

                $messages [$structure->field_name . '.required'] = trans('core.base.field.required');

                if ($structure->type != ReferenceTableStructure::COLUMN_TYPE_DATE && $structure->type != ReferenceTableStructure::COLUMN_TYPE_DATETIME) {

                    $minTrans = 'core.base.field.min_characters';
                    $maxTrans = 'core.base.field.max_characters';

                    if ($structure->type == ReferenceTableStructure::COLUMN_TYPE_NUMBER) {
                        $minTrans = 'core.base.field.min_number';
                        $maxTrans = 'core.base.field.max_number';
                    }

                    if (!empty($structure->parameters["max"])) {
                        $messages [$structure->field_name . '.max'] = trans($maxTrans, ['max' => $structure->parameters["max"]]);
                    }

                    if (!empty($structure->parameters["min"])) {
                        $messages [$structure->field_name . '.min'] = trans($minTrans, ['min' => $structure->parameters["min"]]);
                    }

                } else {

                    if ($structure->field_name == ReferenceTable::REFERENCE_DEFAULT_FIELD_START_DATE && !is_null($referenceFormTable) && Carbon::now() >= $referenceFormTable->{$structure->field_name}) {
                        if ($this->request->get(ReferenceTable::REFERENCE_DEFAULT_FIELD_START_DATE) != $referenceFormTable->{$structure->field_name}) {
                            $messages[$structure->field_name . '.date_equals'] = trans('core.loading.cannot_change_date');
                        }
                    }

                    if ($fieldName == ReferenceTable::REFERENCE_DEFAULT_FIELD_START_DATE && !$formId) {
                        $startDate = currentDateFront();
                    }

                    if (!empty($structure->parameters['start']) && $fieldName != ReferenceTable::REFERENCE_DEFAULT_FIELD_START_DATE && $fieldName != ReferenceTable::REFERENCE_DEFAULT_FIELD_END_DATE) {
                        $isDateTimeField = false;
                        if ($structure->type == ReferenceTableStructure::COLUMN_TYPE_DATETIME) {
                            $isDateTimeField = true;
                        }

                        $startDate = formattedDate($structure->parameters['start'], $isDateTimeField);
                    }

                    $messages [$structure->field_name . '.after_or_equal'] = trans('swis.reference_table_form.start_date.invalid', ['start' => $startDate ?? '']);
                    $messages [$structure->field_name . '.before_or_equal'] = trans('swis.reference_table_form.end_date.invalid');
                    $messages [$structure->field_name . '.after'] = trans('core.base.field.start_date', ['start' => formattedDate($this->request->get(ReferenceTable::REFERENCE_DEFAULT_FIELD_START_DATE))]);
                }

                $messages [$structure->field_name . '.*'] = trans('core.loading.invalid_data');

            }

            //
            if ($structure->has_ml) {
                foreach ($this->request->get('ml') as $key => $ml) {
                    $messages['ml.' . $key . '.' . $structure->field_name . '.required'] = trans('core.base.field.required');
                    $messages['ml.' . $key . '.' . $structure->field_name . '.max'] = trans('core.base.field.max_characters', ['max' => optional($structure)->parameters["max"]]);
                    $messages['ml.' . $key . '.' . $structure->field_name . '.min'] = trans('core.base.field.min_characters', ['min' => optional($structure)->parameters["min"]]);
                    $messages['ml.' . $key . '.' . $structure->field_name . '.*'] = trans('core.loading.invalid_data');
                }
            }
        }

        return $messages;
    }
}
