<?php

namespace App\Http\Requests\ReferenceTable;

use App\Http\Requests\Request;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\ReferenceTable\ReferenceTableStructure;

/**
 * Class ReferenceFormSearchRequest
 * @package App\Http\Requests\ReferenceTable
 */
class ReferenceFormSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'f.id' => 'integer_with_max',
            'f.show_status' => 'nullable|integer_with_max|in:1,2',
        ];

        $refStructures = ReferenceTableStructure::where(['reference_table_id' => request()->route('id'), 'search_result' => ReferenceTable::TRUE])->get();

        if ($refStructures->count()) {
            foreach ($refStructures as $structure) {

                $fieldName = 'f.' . $structure->field_name;

                switch ($structure->type) {

                    case ReferenceTableStructure::COLUMN_TYPE_NUMBER:

                        $rules[$fieldName . '_start'] = 'integer_with_max';
                        $rules[$fieldName . '_end'] = 'integer_with_max';

                        break;

                    case ReferenceTableStructure::COLUMN_TYPE_DECIMAL:

                        $rules[$fieldName . '_start'] = 'numeric';
                        $rules[$fieldName . '_end'] = 'numeric';

                        break;

                    case ReferenceTableStructure::COLUMN_TYPE_DATETIME:

                        $rules[$fieldName . '_start'] = 'date_format:' . config('swis.date_time_format_front_without_second');
                        $rules[$fieldName . '_end'] = 'date_format:' . config('swis.date_time_format_front_without_second');

                        break;

                    case ReferenceTableStructure::COLUMN_TYPE_DATE:

                        $rules[$fieldName . '_start'] = 'date';
                        $rules[$fieldName . '_end'] = 'date';

                        break;

                    case ReferenceTableStructure::COLUMN_TYPE_AUTOCOMPLETE:

                        $rules[$fieldName] = 'integer_with_max';

                        if (isset($structure->parameters['source'])) {

                            $sourceRef = ReferenceTable::select('table_name')->where('id', $structure->parameters['source'])->first();

                            if (!is_null($sourceRef)) {
                                $rules[$fieldName] .= '|exists:reference_' . $sourceRef->table_name . ',id';
                            }
                        }

                        break;

                    case ReferenceTableStructure::COLUMN_TYPE_SELECT:

                        $rules[$fieldName] = 'string_with_max';

                        if (isset($structure->parameters['source'])) {

                            $sourceRef = ReferenceTable::select('table_name')->where('id', $structure->parameters['source'])->first();

                            if (!is_null($sourceRef)) {
                                $rules[$fieldName] .= '|exists:reference_' . $sourceRef->table_name . '_ml,name';
                            }
                        }

                        break;

                    default:
                        $rules[$fieldName] = 'string_with_max';
                }
            }
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
