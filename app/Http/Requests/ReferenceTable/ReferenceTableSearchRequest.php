<?php

namespace App\Http\Requests\ReferenceTable;

use App\Http\Requests\Request;

/**
 * Class ReferenceTableSearchRequest
 * @package App\Http\Requests\ReferenceTable
 */
class ReferenceTableSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.id' => 'integer_with_max',
            'f.name' => 'string_with_max',
            'f.table_name' => 'string_with_max',
            'f.start_date_start' => 'date',
            'f.start_date_end' => 'date',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
