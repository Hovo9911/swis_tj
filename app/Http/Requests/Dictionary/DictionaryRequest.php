<?php

namespace App\Http\Requests\Dictionary;

use App\Http\Requests\Request;

/**
 * Class DictionaryRequest
 * @package App\Http\Requests\Dictionary
 */
class DictionaryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'key' => 'required|max:255',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'key.required' => trans('core.base.field.required'),
            'key.max' => trans('core.base.field.max_characters', ['max' => 255]),
            'key.*' => trans('core.loading.invalid_data')
        ];
    }
}
