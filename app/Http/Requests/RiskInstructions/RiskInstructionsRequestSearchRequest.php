<?php

namespace App\Http\Requests\RiskInstructions;

use App\Http\Requests\Request;

/**
 * Class RiskInstructionsRequestSearchRequest
 * @package App\Http\Requests\RiskInstructionsRequestSearchRequest
 */
class RiskInstructionsRequestSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.document_id' => 'integer_with_max|exists:constructor_documents,id',
            'f.name' => 'string_with_max',
            'f.code' => 'string_with_max',
            'f.description' => 'string_with_max',
            'f.active_date_from_start' => 'date',
            'f.active_date_from_end' => 'date',
            'f.active_date_to_start' => 'date',
            'f.active_date_to_end' => 'date',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
