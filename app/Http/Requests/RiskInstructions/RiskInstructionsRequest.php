<?php

namespace App\Http\Requests\RiskInstructions;

use App\Http\Requests\Request;
use App\Models\BaseModel;
use App\Models\Instructions\Instructions;
use App\Models\ReferenceTable\ReferenceTable;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

/**
 * Class RiskInstructionsRequest
 * @package App\Http\RiskInstructionsRequest
 */
class RiskInstructionsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $typeRef = ReferenceTable::REFERENCE_RISK_INSTRUCTION_TYPE;

        $activeDateFrom = $now = currentDate();
        $id = !empty($this->input('id')) ? (int)$this->input('id') : null;

        if (!is_null($id)) {
            $riskInstruction = Instructions::select('id', 'code', 'active_date_from')->where('id', $id)->firstOrFail();

            if (!is_null($riskInstruction)) {
                $activeDateFrom = Carbon::parse($riskInstruction->getOriginal('active_date_from'))->format(config('swis.date_format'));
            }
        }

        $rules = [
            'document_id' => 'required|exists:constructor_documents,id',
            'type' => "required|exists:{$typeRef},id",
            'feedback' => "required|array",
            'code' => 'required|string|min:1|max:255|unique_with_agency:instructions,code,' . $id,
            'active_date_from' => 'required|date|after_or_equal:' . $activeDateFrom,
            'active_date_to' => 'nullable|date|after_or_equal:active_date_from',
            'show_status' => 'required',
        ];

        $exists = Instructions::where('code', $this->input('code'))->where('company_tax_id', Auth::user()->companyTaxId())->where('id', '!=', $id)->active()->first();

        if (!is_null($id) && $this->input('show_status') == BaseModel::STATUS_ACTIVE && $exists) {
            $rules['code'] = 'required|string|min:1|max:255|unique_with_agency_return';
        }

        if ($this->input('show_status') == BaseModel::STATUS_ACTIVE) {
            $rules['active_date_to'] = "nullable|date|after_or_equal:{$now}";
        }

        foreach ($this->request->get('ml') as $key => $ml) {
            $rules["ml.{$key}.name"] = 'required|string|min:1|max:255';
            $rules["ml.{$key}.description"] = 'required|max:4000|string';
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        $messages = [
            'document_id.required' => trans('core.base.field.required'),
            'type.required' => trans('core.base.field.required'),
            'feedback.required' => trans('core.base.field.required'),

            'name.required' => trans('core.base.field.required'),
            'name.min' => trans('core.base.field.min_characters', ['min' => 1]),
            'name.max' => trans('core.base.field.max_characters', ['max' => 255]),

            'code.required' => trans('core.base.field.required'),
            'code.min' => trans('core.base.field.min_characters', ['min' => 1]),
            'code.max' => trans('core.base.field.max_characters', ['max' => 255]),

            'active_date_from.required' => trans('core.base.field.required'),
            'active_date_from.after_or_equal' => trans('core.base.field.start_date.after_or_equal'),
            'active_date_from.*' => trans('core.loading.invalid_data'),

            'active_date_to.after_or_equal' => trans('core.base.field.start_date.after_or_equal'),
            'active_date_to.*' => trans('core.loading.invalid_data'),

            'code.unique_with_agency_return' => trans('validation.unique_with_agency')
        ];

        foreach ($this->request->get('ml') as $key => $ml) {
            $messages["ml.{$key}.description.max"] = trans('core.base.field.max_characters', ['max' => 500]);
            $messages["ml.{$key}.description.required"] = trans('core.base.field.required');
        }

        return $messages;
    }
}
