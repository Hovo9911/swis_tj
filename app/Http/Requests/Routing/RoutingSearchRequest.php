<?php

namespace App\Http\Requests\Routing;

use App\Http\Requests\Request;
use App\Models\SingleApplication\SingleApplication;

/**
 * Class RoutingSearchRequest
 * @package App\Http\Requests\Routing
 */
class RoutingSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.id' => 'integer_with_max',
            'f.name' => 'string_with_max',
            'f.rule' => 'string_with_max',
            'f.agency_id' => 'integer_with_max|exists:agency,id',
            'f.constructor_document_id' => 'integer_with_max|exists:constructor_documents,id',
            'f.regime' => 'in:' . implode(',', SingleApplication::REGIMES),
            'f.show_status' => 'in:1,2',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
