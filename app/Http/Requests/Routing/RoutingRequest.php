<?php

namespace App\Http\Requests\Routing;

use App\Http\Requests\Request;
use App\Models\SingleApplication\SingleApplication;
use App\Rules\ValidateRuleSyntax;

/**
 * Class RoutingRequest
 * @package App\Http\Requests\Routing
 */
class RoutingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = $this->request->all();

        $rules = [
            'name' => 'required|string_with_max',
            'agency_id' => 'required|integer_with_max|exists:agency,id',
            'constructor_document_id' => 'required|integer_with_max|exists:constructor_documents,id',
            'regime' => 'required|in:' . implode(',', SingleApplication::REGIMES),
            'show_status' => 'required|in:1,2',

            'ref_classificator_codes' => 'nullable|array',

            'saf_controlled_fields' => 'nullable|array',
            'saf_controlled_fields.*.make_mandatory' => 'in:1',
            'saf_controlled_fields.*.make_visible' => 'in:1',
            'is_manual' => 'nullable|in:1',
        ];

        // If routing is for generated sub applications(not manual)
        if (!isset($data['is_manual'])) {
            $rules['rule'] = ['required', new ValidateRuleSyntax];
        }

        if (isset($data['saf_controlled_fields'])) {

            $safControlledFields = $data['saf_controlled_fields'];

            $safFields = getSafXmlControlledFields();
            $safHiddenFields = [];
            foreach ($safFields as $field) {
                $isHidden = $field->hideOnSaf ? true : false;

                if ($isHidden) {
                    $safHiddenFields[] = $field->id;
                }
            }

            foreach ($safControlledFields as $safFieldId => $safControlledField) {

                if (in_array($safFieldId, $safHiddenFields)) {
                    $rules['saf_controlled_fields.' . $safFieldId . '.make_visible'] = 'required|in:1';
                }
            }
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'rules.required' => trans('core.base.field.required'),
            'rules.*' => trans('core.loading.invalid_data'),

            'regime.required' => trans('core.base.field.required'),
            'regime.*' => trans('core.loading.invalid_data')
        ];
    }

}
