<?php

namespace App\Http\Requests\ConstructorDataSet;

use App\Http\Requests\Request;
use App\Models\ConstructorDocument\ConstructorDocument;

/**
 * Class ConstructorDataSetRequest
 * @package App\Http\Requests\ConstructorDataSetRequest
 */
class ConstructorDataSetRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $agencyDocumentsIds = ConstructorDocument::getConstructorDocumentForAgency()->pluck('id')->toArray();

        $rules = [
            'params' => 'array',
            'params.*' => 'array|distinct',
            'documentID' => 'required|integer|exists:constructor_documents,id|in:' . implode(',', $agencyDocumentsIds),
        ];

        $mdmParams = $this->input('paramsMDM');
        $mdmFields = isset($mdmParams) ? array_keys($mdmParams) : [];
        if (isset($mdmFields) && count($mdmFields) > 0) {
            foreach ($mdmFields as $field) {
                $rules["paramsMDM.{$field}"] = 'required';
                $rules["paramsMDM.{$field}.group"] = 'required|string';
                foreach (activeLanguages() as $lng) {
                    $rules["paramsMDM.{$field}.field_label.{$lng->id}"] = 'required|string';
                    $rules["paramsMDM.{$field}.tooltip.{$lng->id}"] = 'nullable|string';
                }

            }
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'params.array' => trans('core.loading.invalid_data'),
            'documentID.required' => trans('core.base.field.required'),
            'documentID.*' => trans('core.loading.invalid_data'),
        ];
    }
}
