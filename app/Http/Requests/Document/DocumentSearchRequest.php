<?php

namespace App\Http\Requests\Document;

use App\Http\Requests\Request;

/**
 * Class DocumentSearchRequest
 * @package App\Http\Requests\Document
 */
class DocumentSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.id' => 'integer_with_max',
            'f.name' => 'string_with_max',
            'f.access_password' => 'string_with_max',
            'f.document_type' => 'string_with_max|exists:reference_classificator_of_documents,code',
            'f.description' => 'string_with_max',
            'f.document_number' => 'string_with_max',
            'f.document_release_date_start' => 'date',
            'f.document_release_date_end' => 'date',
            'f.document_status' => 'string_with_max|exists:document,document_status',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
