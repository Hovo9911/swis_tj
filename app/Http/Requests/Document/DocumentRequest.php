<?php

namespace App\Http\Requests\Document;

use App\Http\Requests\Request;
use App\Models\BaseModel;
use App\Models\Document\Document;
use App\Models\ReferenceTable\ReferenceTable;
use Illuminate\Support\Facades\Auth;

/**
 * Class DocumentRequest
 * @package App\Http\Requests\Document
 */
class DocumentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = $this->request->all();

        $id = $data['id'];
        $documentInputStatus = $data['document_status'];

        $validateFileFieldToRequired = false;
        $isApproveAgencyAndCreator = false;

        if ($documentInputStatus == Document::DOCUMENT_STATUS_REGISTERED) {
            $validateFileFieldToRequired = true;
        }

        // Update Document
        if ($id) {

            $document = Document::where('id', $id)->firstOrFail();
            $documentCurrentStatus = $document->document_status;

            if ($document->isDocumentOwnerOrCreatorAndApproveAgency() && $documentCurrentStatus == Document::DOCUMENT_STATUS_REGISTERED) {
                $isApproveAgencyAndCreator = true;
            }

            if (!$isApproveAgencyAndCreator) {

                // If Other Agency need to update status
                if (!$document->isDocumentOwnerOrCreator()) {
                    $isAgencyApprove = Document::isAgencyApprove($id);

                    if ($isAgencyApprove) {
                        return [
                            'document_status' => 'required|string_with_max|in:' . implode(',', Document::DOCUMENT_STATUSES)
                        ];
                    }
                }

                // In Confirmed
                if ($documentCurrentStatus == Document::DOCUMENT_STATUS_CONFIRMED) {

                    $rules = [
                        'folders' => 'nullable|array',
                        'folders.*' => 'integer_with_max|exists:folder,id',
                        'notes' => 'nullable|max:512',
                        'regenerate_access_key' => 'nullable|in:1',
                        'document_status' => 'nullable|string_with_max|in:' . implode(',', Document::DOCUMENT_STATUSES),
                        'creator_user' => 'nullable|integer_with_max',
                        'updateFolder' => 'nullable'
                    ];

                    if ($documentInputStatus == Document::DOCUMENT_STATUS_SAVED) {
                        unset($rules['document_status']);
                    }

                    return $rules;
                }

                // If Suspended
                if ($documentCurrentStatus == Document::DOCUMENT_STATUS_SUSPENDED) {
                    return [
                        'notes' => 'nullable|max:512'
                    ];
                }

                // If Rejected
                if ($documentCurrentStatus == Document::DOCUMENT_STATUS_REJECTED) {
                    return [];
                }

                // If file is exist no need check required part
                if (!empty($document->document_file)) {
                    $validateFileFieldToRequired = false;
                }
            }

        }

        $rules = [
            'folders' => 'nullable|array',
            'folders.*' => 'integer_with_max|exists:folder,id',

            'regenerate_access_key' => 'nullable|in:1',

            'document_name' => 'required|string|max:250',
            'document_status' => 'required|string_with_max|in:' . implode(',', Document::DOCUMENT_STATUSES),
            'document_file' => 'nullable|max:10000',
            'document_description' => 'nullable|string|max:500',
            'document_number' => 'required|string|max:50',
            'document_type' => 'required|string_with_max|exists:reference_classificator_of_documents,code',
            'document_type_id' => 'nullable|integer_with_max|exists:reference_classificator_of_documents,id',
            'document_release_date' => 'required|date|before_or_equal:' . currentDate(),

            'approve_agency_tax_id' => 'nullable|tax_id_validator|exists:agency,tax_id',
            'approve_type' => 'required|string|in:' . implode(',', Document::APPROVE_TYPES),
            'subject_for_approvement' => 'nullable|in:1',

            //
            'notes' => 'nullable|string|max:512',
        ];

        // Agency is Owner And Approve
        if ($isApproveAgencyAndCreator) {

            $rules['subject_for_approvement'] = 'required';

            if ($data['approve_agency_tax_id'] != Auth::user()->companyTaxId()) {
                $rules['approve_type'] = 'invalid_data';
            }

            // If Suspended
            if ($document->document_status == Document::DOCUMENT_STATUS_SUSPENDED) {
                return [
                    'notes' => 'nullable|max:512'
                ];
            }

        }

        // Holder
        $checkHolderRules = true;
        if (!Auth::user()->isBroker() && $id) {
            $checkHolderRules = false;
        }

        if ($checkHolderRules) {

            $rules['holder_name'] = 'required|string_with_max';
            $rules['holder_type'] = 'required|integer_with_max|exists:reference_legal_entity_form_reference,id';
            $rules['holder_type_value'] = 'required|string|max:15';
            $rules['holder_registration_number'] = 'nullable|max:250';
            $rules['holder_country'] = 'required|integer_with_max|exists:reference_countries,id';
            $rules['holder_address'] = 'nullable|string|max:200';
            $rules['holder_community'] = 'nullable|string_with_max';
            $rules['holder_phone_number'] = 'required|phone_number_validator';
            $rules['holder_email'] = 'required|string|email|max:250';

            // Passport type
            $holderTypeValueRule = 'required';
            if (isset($data['holder_type'])) {
                if (ReferenceTable::isLegalEntityType($data['holder_type'], BaseModel::ENTITY_TYPE_USER_ID)) {
                    $rules['holder_passport'] = 'required|passport_validator';
                }

                if (ReferenceTable::isLegalEntityType($data['holder_type'], BaseModel::ENTITY_TYPE_FOREIGN_CITIZEN)) {
                    $holderPassportRule = 'required';

                    if (isset($data['holder_type_value']) || isset($data['holder_passport'])) {
                        $holderTypeValueRule = 'nullable';
                        $holderPassportRule = 'nullable';
                    }

                    $rules['holder_passport'] = $holderPassportRule;
                }
            }

            $rules['holder_type_value'] = $holderTypeValueRule . '|string|string|max:15';
        }

        // Validate File Field
        if ($validateFileFieldToRequired) {
            $rules['document_file'] = 'required|max:10000';
        }

        // if Only Saved
        if ($documentInputStatus == Document::DOCUMENT_STATUS_SAVED) {
            unset($rules['document_status']);
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'document_name.required' => trans('core.base.field.required'),
            'document_name.max' => trans('core.base.field.max_characters', ['max' => 250]),
            'document_name.*' => trans('core.loading.invalid_data'),

            'document_description.max' => trans('core.base.field.max_characters', ['max' => 500]),
            'document_description.*' => trans('core.loading.invalid_data'),

            'document_type.required' => trans('core.base.field.required'),
            'document_type.*' => trans('core.loading.invalid_data'),

            'document_number.required' => trans('core.base.field.required'),
            'document_number.max' => trans('core.base.field.max_characters', ['max' => 50]),
            'document_number.*' => trans('core.loading.invalid_data'),

            'document_release_date.required' => trans('core.base.field.required'),
            'document_release_date.before_or_equal' => trans('core.base.field.end_date', ['end' => currentDate()]),
            'document_release_date.*' => trans('core.loading.invalid_data'),

            'holder_name.required' => trans('core.base.field.required'),
            'holder_name.*' => trans('core.loading.invalid_data'),

            'holder_type.required' => trans('core.base.field.required'),
            'holder_type.*' => trans('core.loading.invalid_data'),

            'holder_type_value.required' => trans('core.base.field.required'),
            'holder_type_value.max' => trans('core.base.field.max_characters', ['max' => 15]),
            'holder_type_value.*' => trans('core.loading.invalid_data'),

            'holder_country.required' => trans('core.base.field.required'),
            'holder_country.*' => trans('core.loading.invalid_data'),

            'holder_email.required' => trans('core.base.field.required'),
            'holder_email.max' => trans('core.base.field.max_characters', ['max' => 250]),
            'holder_email.*' => trans('core.loading.invalid_data'),

            'holder_phone_number.phone_number_validator' => trans('core.loading.invalid_data'),
            'holder_phone_number.*' => trans('core.loading.invalid_data'),

            'holder_address.max' => trans('core.base.field.max_characters', ['max' => 200]),
            'holder_address.*' => trans('core.loading.invalid_data'),

            'notes.max' => trans('core.base.field.max_characters', ['max' => 512]),
            'notes.*' => trans('core.loading.invalid_data'),

            'document_show_status.*' => trans('core.loading.invalid_data')
        ];
    }
}
