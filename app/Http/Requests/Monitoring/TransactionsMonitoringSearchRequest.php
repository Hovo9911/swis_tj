<?php


namespace App\Http\Requests\Monitoring;


use App\Http\Requests\Request;

class TransactionsMonitoringSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.id' => 'integer_with_max',
            'f.user_name' => 'string_with_max',
            'f.user_ssn' => 'string_with_max',
            'f.transaction_id' => 'integer_with_max',
            'f.payment_date_date_start' => 'date',
            'f.payment_date_date_end' => 'date',
            'f.company_name' => 'string_with_max',
            'f.payment_provider_id' => 'exists:payment_providers,id',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}