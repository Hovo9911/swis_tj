<?php

namespace App\Http\Requests\AuthorizePerson;

use App\Http\Requests\Request;

/**
 * Class AuthorizePersonSearchRequest
 * @package App\Http\Requests\Agency
 */
class AuthorizePersonSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.id' => 'integer_with_max',
            'f.passport' => 'string_with_max',
            'f.ssn' => 'string_with_max',
            'f.first_name' => 'string_with_max',
            'f.last_name' => 'string_with_max',
            'f.authorizer_ssn' => 'string_with_max',
            'f.authorizer_first_name' => 'string_with_max',
            'f.authorizer_last_name' => 'string_with_max',
            'f.menu' => 'integer_with_max|exists:menu,id',
            'f.role' => 'integer_with_max|exists:roles,id',
            'f.sub_division_id' => 'integer_with_max',
            'f.role_status' => 'integer_with_max',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
