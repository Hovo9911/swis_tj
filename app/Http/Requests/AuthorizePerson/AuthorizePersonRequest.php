<?php

namespace App\Http\Requests\AuthorizePerson;

use App\Http\Requests\Request;

/**
 * Class AuthorizePersonRequest
 * @package App\Http\Requests\AuthorizePerson
 */
class AuthorizePersonRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'authorized_ssn' => "required|ssn_validator",
//            'authorized_passport' => "required|passport_validator",
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'authorized_ssn.required' => trans('core.base.field.required'),
            'authorized_ssn.*' => trans('core.loading.invalid_data'),

            'authorized_passport.required' => trans('core.base.field.required'),
            'authorized_passport.*' => trans('core.loading.invalid_data'),
        ];
    }
}
