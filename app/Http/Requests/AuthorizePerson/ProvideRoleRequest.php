<?php

namespace App\Http\Requests\AuthorizePerson;

use App\Http\Requests\Request;
use App\Models\Menu\Menu;
use App\Models\Role\Role;
use App\Models\UserRoles\UserRoles;
use Illuminate\Support\Facades\Auth;

/**
 * Class ProvideRoleRequest
 * @package App\Http\Requests\AuthorizePerson
 */
class ProvideRoleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'id' => 'required|integer_with_max|exists:users,id',
            'changed_modules' => 'nullable|string_with_max',
        ];

        $data = $this->request->get('data');

        if (isset($data) && count($data)) {
            foreach ($data as $key => $currentRole) {

                $availableAt = $availableTo = currentDate();

                if ($currentRole['id']) {
                    $userRole = UserRoles::select('available_at', 'available_to')->where('id', $currentRole['id'])->first();

                    if ($userRole->role_status == Role::STATUS_DELETED) {
                        continue;
                    }

                    $rules['data.' . $key . '.id'] = 'required|integer_with_max';
                    $rules['data.' . $key . '.show_only_self'] = 'required|in:0,1';
                    $rules['data.' . $key . '.role_status'] = 'required|in:' . implode(',', Role::ROLE_STATUSES);
                    $rules['data.' . $key . '.ip_access'] = 'nullable|string_with_max';

                    if (isset($currentRole['available_at']) && ($userRole->getOriginal('available_at') >= currentDate() && $currentRole['available_at'] < currentDate())) {
                        $rules['data.' . $key . '.available_at'] = 'required|date|after_or_equal:' . $availableAt;
                    }

                    if ($userRole->getOriginal('available_to') != $currentRole['available_to']) {
                        $rules['data.' . $key . '.available_to'] = 'nullable|date|after_or_equal:' . $availableTo;
                    }

                } else {

                    $userCanAuthorizeRolesAndRoleGroups = null;
                    $menuShowOnlySelfPermission = null;
                    if ($currentRole['menu']) {
                        $userCanAuthorizeRolesAndRoleGroups = Auth::user()->authorizeModuleRoles(Menu::getMenuNameById($currentRole['menu']));
                        $menuShowOnlySelfPermission = Menu::getMenuShowOnlySelfPermissions(Menu::getMenuNameById($currentRole['menu']));
                        $menuShowOnlySelfPermission = implode(',', array_values($menuShowOnlySelfPermission));
                    }

                    if (isset($data[$key]['available_at'])) {
                        $availableTo = $data[$key]['available_at'];
                    }

                    // Menu (module)
                    $rules['data.' . $key . '.menu'] = 'required|integer_with_max|in:' . implode(',', Auth::user()->getUserCurrentRoleAvailableModules());

                    // Role
                    if (empty($currentRole['role_group_id']) || (empty($currentRole['role_id']) && empty($currentRole['role_group_id']))) {
                        $rules['data.' . $key . '.role'] = 'required|integer_with_max|exists:roles,id';

                        if ($userCanAuthorizeRolesAndRoleGroups) {
                            $rules['data.' . $key . '.role'] .= '|in:' . implode(',', $userCanAuthorizeRolesAndRoleGroups->roles->pluck('id')->all());
                        }
                    }

                    // RoleGroup
                    $rules['data.' . $key . '.role_group_id'] = 'nullable|integer_with_max|exists:roles_group,id';

                    if (!empty($currentRole['role_group_id']) && $userCanAuthorizeRolesAndRoleGroups) {
                        $rules['data.' . $key . '.role_group_id'] .= '|in:' . implode(',', $userCanAuthorizeRolesAndRoleGroups->roleGroups->pluck('id')->all());
                    }

                    $rules['data.' . $key . '.role_status'] = 'required|in:' . implode(',', Role::ROLE_STATUSES);
                    $rules['data.' . $key . '.show_only_self'] = 'required';

                    if (!is_null($menuShowOnlySelfPermission)) {
                        $rules['data.' . $key . '.show_only_self'] .= '|in:' . $menuShowOnlySelfPermission;
                    }

                    $rules['data.' . $key . '.ip_access'] = 'nullable|string_with_max';

                    $rules['data.' . $key . '.attribute'] = 'nullable|integer_with_max';
                    $rules['data.' . $key . '.attribute_type'] = 'nullable|in:' . implode(',', Role::ROLE_ATTRIBUTES_TYPE_ALL);
                    $rules['data.' . $key . '.available_to'] = 'nullable|date|after_or_equal:' . $availableTo;
                    $rules['data.' . $key . '.available_at'] = 'required|date|after_or_equal:' . $availableAt;

                    $attributeValueValidation = 'nullable';
                    if (isset($currentRole['attribute_field_id']) && !is_null($currentRole['attribute_field_id'])) {
                        $attributeValueValidation = 'required';
                    }

                    // Attribute field and Value Validation
                    $attributeFieldValidation = 'nullable|';
                    $attributeValueInValidation = '';
                    if (Auth::user()->isAuthorizedEmployeeForCompany()) {

                        $canAuthorizeAllAttributesWhere = function ($q) use ($currentRole) {
                            if ($currentRole['role_group_id']) {
                                $q->where('role_group_id', $currentRole['role_group_id']);
                            } else {
                                $q->where('role_id', $currentRole['role']);
                            }

                            $q->whereNull('attribute_field_id');
                        };

                        $canAuthorizeAllAttributes = Auth::user()->getCurrentUserRoles(Menu::USER_DOCUMENTS_MENU_NAME, false, ['id'], $canAuthorizeAllAttributesWhere);

                        $userAuthorizedUserRolesWhere = function ($q) use ($currentRole) {

                            if ($currentRole['role_group_id']) {
                                $q->where('role_group_id', $currentRole['role_group_id']);
                            } else {
                                $q->where('role_id', $currentRole['role']);
                            }

                            $q->whereNotNull('attribute_field_id');
                        };

                        $userAuthorizedUserRoles = Auth::user()->getCurrentUserRoles(Menu::USER_DOCUMENTS_MENU_NAME, false, ['attribute_field_id', 'attribute_type', 'attribute_value'], $userAuthorizedUserRolesWhere);

                        if ($userAuthorizedUserRoles->count()) {

                            $availableAttributeFieldIds = [];
                            $availableAttributeValues = [];
                            foreach ($userAuthorizedUserRoles as $authorizedUserRole) {
                                $availableAttributeFieldIds[] = $authorizedUserRole->attribute_field_id;
                                $availableAttributeValues[] = $authorizedUserRole->attribute_value;
                            }

                            $attributeFieldValidation = 'required|in:' . implode(',', array_unique($availableAttributeFieldIds));
                            $attributeValueInValidation .= '|in:' . implode(',', array_unique($availableAttributeValues));

                            if ($canAuthorizeAllAttributes->count()) {
                                $attributeFieldValidation = str_replace('required', 'nullable', $attributeFieldValidation);
                            }
                        }
                    }

                    $rules['data.' . $key . '.attribute_field_id'] = $attributeFieldValidation . '|string|max:250';
                    $rules['data.' . $key . '.attribute_value'] = $attributeValueValidation . '|integer_with_max' . $attributeValueInValidation;
                    $rules['data.' . $key . '.multiple_attributes'] = 'nullable|array';
                    $rules['data.' . $key . '.multiple_attributes.*'] = 'integer_with_max';

                }
            }
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        $data = $this->request->get('data');

        $messages = [];
        if (isset($data) && count($data)) {

            foreach ($data as $key => $currentRole) {
                $currentDate = $availableTo = currentDate();

                if (isset($data[$key]['available_at'])) {
                    $availableTo = $data[$key]['available_at'];
                }

                $messages ['data.' . $key . '.role.required'] = trans('core.base.field.required');
                $messages ['data.' . $key . '.role.*'] = trans('core.loading.invalid_data');

                $messages ['data.' . $key . '.show_only_self.required'] = trans('core.base.field.required');
                $messages ['data.' . $key . '.show_only_self.*'] = trans('core.loading.invalid_data');

                $messages ['data.' . $key . '.role_status.required'] = trans('core.base.field.required');
                $messages ['data.' . $key . '.role_status.*'] = trans('core.loading.invalid_data');

                $messages ['data.' . $key . '.menu.required'] = trans('core.base.field.required');
                $messages ['data.' . $key . '.menu.*'] = trans('core.loading.invalid_data');

                $messages ['data.' . $key . '.ip_access.*'] = trans('core.loading.invalid_data');

                $messages ['data.' . $key . '.available_at.after_or_equal'] = trans('core.base.field.start_date.after_or_equal', ['start' => $currentDate]);
                $messages ['data.' . $key . '.available_to.after_or_equal'] = trans('core.base.field.start_date', ['start' => $availableTo]);
            }
        }

        return $messages;
    }
}
