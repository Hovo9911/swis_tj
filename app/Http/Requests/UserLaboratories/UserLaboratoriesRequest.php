<?php

namespace App\Http\Requests\UserLaboratories;

use App\Http\Requests\Request;

/**
 * Class UserLaboratoriesRequest
 * @package App\Http\Requests\UserLaboratories
 */
class UserLaboratoriesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $indicators = $this->request->get('data');

        $rules = [];
        $rules['data'] = 'required|array';
        if (!empty($indicators)) {
            foreach ($indicators as $key => $value) {
                $rules['data.' . $key . '.actual_result'] = 'required|string_with_max';
                $rules['data.' . $key . '.adequacy'] = 'required|numeric';
            }
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        $indicators = $this->request->get('data');

        $messages = [];
        if (!empty($indicators)) {
            foreach ($indicators as $key => $value) {
                $messages['data.' . $key . '.actual_result.required'] = trans('core.base.field.required');
                $messages['data.' . $key . '.actual_result.*'] = trans('core.loading.invalid_data');

                $messages['data.' . $key . '.adequacy.required'] = trans('core.base.field.required');
                $messages['data.' . $key . '.adequacy.*'] = trans('core.loading.invalid_data');
            }
        }

        return $messages;
    }
}
