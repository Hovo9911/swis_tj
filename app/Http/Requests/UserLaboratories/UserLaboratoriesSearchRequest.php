<?php

namespace App\Http\Requests\UserLaboratories;

use App\Http\Requests\Request;

/**
 * Class UserLaboratoriesSearchRequest
 * @package App\Http\Requests\UserLaboratories
 */
class UserLaboratoriesSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.sample_code' => 'string_with_max',
            'f.saf_product_id' => 'integer_with_max',
            'f.sampler' => 'string_with_max|exists:reference_lab_samplers,id',
            'f.created_at_date_start' => 'date',
            'f.created_at_date_end' => 'date',
            'f.return_date_date_start' => 'date',
            'f.return_date_date_end' => 'date',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
