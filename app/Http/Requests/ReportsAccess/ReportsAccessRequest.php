<?php

namespace App\Http\Requests\ReportsAccess;

use App\Http\Requests\Request;
use App\Models\ReportsAccess\ReportsAccess;

/**
 * Class ReportsAccessRequest
 * @package App\Http\Requests\ReportsAccessRequest
 */
class ReportsAccessRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'role_type' => 'required|in:' . ReportsAccess::ROLE_TYPE_SW_ADMIN . ',' . ReportsAccess::ROLE_TYPE_AGENCY,
//            'agencies' =>'nullable|array',
            'agencies' => 'required_if:role_type,==,' . ReportsAccess::ROLE_TYPE_AGENCY,
            'jasper_report_info' => 'required',
            'show_status' => 'required|integer'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'role_type.required' => trans('core.base.field.required'),
            'show_status.required' => trans('core.base.field.required'),
            'agencies.required_if' => trans('core.base.field.required'),
        ];
    }
}
