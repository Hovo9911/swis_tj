<?php

namespace App\Http\Requests\ReportsAccess;

use App\Http\Requests\Request;

/**
 * Class ReportsAccessSearchRequest
 * @package App\Http\Requests\ReportsAccess
 */
class ReportsAccessSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.id' => 'integer_with_max',
            'f.type' => 'string_with_max|exists:reports_access,role_type',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
