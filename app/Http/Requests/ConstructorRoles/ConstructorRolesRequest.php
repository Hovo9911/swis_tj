<?php

namespace App\Http\Requests\ConstructorRoles;

use App\Http\Requests\Request;
use App\Models\ConstructorDocument\ConstructorDocument;

/**
 * Class ConstructorRolesRequest
 * @package App\Http\Requests\ConstructorRoles
 */
class ConstructorRolesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $agencyDocumentsIds = ConstructorDocument::getConstructorDocumentForAgency()->pluck('id')->toArray();

        return [
            'attributes' => 'array|nullable',
            'roles' => 'array|nullable',
            'documentID' => 'required|integer|exists:constructor_documents,id|in:' . implode(',', $agencyDocumentsIds),
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
