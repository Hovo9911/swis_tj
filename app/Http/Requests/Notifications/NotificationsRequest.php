<?php

namespace App\Http\Requests\Notifications;

use App\Http\Requests\Request;

/**
 * Class NotificationsRequest
 * @package App\Http\Requests\Routing
 */
class NotificationsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        if (!empty($this->request->get('notification_id'))) {
            $rules = [
                'notification_id' => 'required|integer_with_max'
            ];
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }

}
