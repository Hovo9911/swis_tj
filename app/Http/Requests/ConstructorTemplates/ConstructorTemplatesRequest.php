<?php

namespace App\Http\Requests\ConstructorTemplates;

use App\Http\Requests\Request;

/**
 * Class ConstructorTemplatesRequest
 * @package App\Http\Requests\ConstructorTemplatesRequest
 */
class ConstructorTemplatesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'description' => 'required|string|max:5000',
            'qr_code' => 'nullable|in:0,1',
            'authorization_without_login' => 'nullable|in:0,1',
            'show_status' => 'required|integer'
        ];

        foreach ($this->request->get('ml') as $key => $ml) {
            $rules['ml.' . $key . '.template_name'] = 'required|string|max:255';
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        $messages = [
            'description.required' => trans('core.base.field.required'),
            'name.show_status' => trans('core.base.field.required'),
        ];

        foreach ($this->request->get('ml') as $key => $ml) {
            $messages['ml.' . $key . '.template_name'] = trans('core.base.field.required');
        }

        return $messages;
    }
}
