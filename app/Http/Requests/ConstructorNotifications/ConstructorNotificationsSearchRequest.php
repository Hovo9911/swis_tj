<?php

namespace App\Http\Requests\ConstructorNotifications;

use App\Http\Requests\Request;

/**
 * Class ConstructorNotificationsSearchRequest
 * @package App\Http\Requests\ConstructorNotifications
 */
class ConstructorNotificationsSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.id' => 'integer_with_max',
            'f.name' => 'string_with_max',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
