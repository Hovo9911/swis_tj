<?php

namespace App\Http\Requests\ConstructorRules;

use App\Http\Requests\Request;

/**
 * Class ConstructorRulesSearchRequest
 * @package App\Http\Requests\ConstructorRules
 */
class ConstructorRulesSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.constructor_document_id' => 'integer_with_max|exists:constructor_documents,id',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
