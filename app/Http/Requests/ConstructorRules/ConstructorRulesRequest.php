<?php

namespace App\Http\Requests\ConstructorRules;

use App\Http\Requests\Request;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorRules\ConstructorRules;
use App\Models\ReferenceTable\ReferenceTable;

/**
 * Class ConstructorRulesRequest
 * @package App\Http\Requests\ConstructorRules
 */
class ConstructorRulesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $risk = ConstructorRules::RISK_TYPE;
        $obligation = ConstructorRules::OBLIGATION_TYPE;
        $validation = ConstructorRules::VALIDATION_TYPE;
        $calculation = ConstructorRules::CALCULATION_TYPE;
        $obligationTable = ReferenceTable::REFERENCE_OBLIGATION_BUDGET_LINE;
        $agencyDocumentsIds = ConstructorDocument::getConstructorDocumentForAgency()->pluck('id')->toArray();

        $rules = [
            'name' => 'required|min:1',
            'document_id' => 'required|exists:constructor_documents,id|in:' . implode(',', $agencyDocumentsIds),
            'type' => "required|in:{$obligation},{$validation},{$calculation},{$risk}",
            'condition' => 'required|min:1',
            'rule' => 'required|min:1',
            'obligation_code' => 'nullable'
        ];

        if ($this->input('type') == $obligation) {
            $rules['obligation_code'] = "required|exists:$obligationTable,code";

            if (!is_null($this->input('obligation_code'))) {
                $obligationType = ConstructorRules::getObligationType($this->input('obligation_code'));
                if (!is_null($obligationType) && $obligationType->type != 'rule') {
                    unset($rules['rule']);
                }
            }

            unset($rules['condition']);
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'document_id.required' => trans('core.base.field.required'),
            'document_id.*' => trans('core.loading.invalid_data'),
        ];
    }
}
