<?php

namespace App\Http\Requests\SingleApplication;

use App\Http\Requests\Request;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplication\SingleApplication;

/**
 * Class SingleApplicationDocumentRequest
 * @package App\Http\Requests\SingleApplication
 */
class DocumentStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = $this->request->all();

        $saf = SingleApplication::select('id', 'regular_number', 'regime')->where(['regular_number' => request()->header('sNumber')])->safOwnerOrCreator()->firstOrFail();

        $rules = [
            'document_name' => 'required|string|max:250',
            'document_description' => 'nullable|string|max:500',
            'document_number' => 'required|string|max:50',
            'document_type' => 'required|string_with_max|exists:reference_classificator_of_documents,code',
            'document_release_date' => 'required|date|before_or_equal:' . currentDate(),
            'document_file' => 'required|string|max:10000',
            'document_products' => 'nullable|string|max:2000',
            'document_sub_applications' => 'nullable|string|max:2000',
        ];

        if ($saf->regime == SingleApplication::REGIME_IMPORT || $saf->regime == SingleApplication::REGIME_EXPORT) {
            $rules['saf.sides.' . $saf->regime . '.name'] = 'required|string_with_max';
            $rules['saf.sides.' . $saf->regime . '.type'] = 'required|integer_with_max|exists:reference_legal_entity_form_reference,id';
            $rules['saf.sides.' . $saf->regime . '.type_value'] = 'required|string|max:16';
            $rules['saf.sides.' . $saf->regime . '.country'] = 'required|integer_with_max|exists:reference_countries,id';
            $rules['saf.sides.' . $saf->regime . '.address'] = 'nullable|string|max:200';
            $rules['saf.sides.' . $saf->regime . '.community'] = 'nullable|string_with_max';
            $rules['saf.sides.' . $saf->regime . '.phone_number'] = 'required|phone_number_validator';
            $rules['saf.sides.' . $saf->regime . '.email'] = 'required|string_with_max|email|max:250';

            if (isset($data['saf']['sides'][$saf->regime]['type']) && !ReferenceTable::isLegalEntityType($data['saf']['sides'][$saf->regime]['type'], ReferenceTable::ENTITY_TYPE_TAX_ID)) {
                $rules['saf.sides.' . $saf->regime . '.passport'] = 'required|passport_validator';
            }
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        $saf = SingleApplication::where(['regular_number' => request()->header('sNumber')])->firstOrFail();

        $messages = [
            'document_name.required' => trans('core.base.field.required'),
            'document_name.max' => trans('core.base.field.max_characters', ['max' => 250]),
            'document_name.*' => trans('core.loading.invalid_data'),
            'document_description.max' => trans('core.base.field.max_characters', ['max' => 500]),
            'document_description.*' => trans('core.loading.invalid_data'),
            'document_number.required' => trans('core.base.field.required'),
            'document_number.max' => trans('core.base.field.max_characters', ['max' => 50]),
            'document_number.*' => trans('core.loading.invalid_data'),
            'document_type.required' => trans('core.base.field.required'),
            'document_type.*' => trans('core.loading.invalid_data'),
            'document_release_date.required' => trans('core.base.field.required'),
            'document_release_date.before_or_equal' => trans('core.base.field.end_date', ['end' => currentDate()]),
            'document_release_date.*' => trans('core.loading.invalid_data'),
            'document_file.required' => trans('core.base.field.required'),
        ];

        if ($saf->regime == SingleApplication::REGIME_IMPORT || $saf->regime == SingleApplication::REGIME_EXPORT) {
            $messages['saf.sides.' . $saf->regime . '.name.required'] = trans('core.base.field.required');
            $messages['saf.sides.' . $saf->regime . '.name.*'] = trans('core.loading.invalid_data');

            $messages['saf.sides.' . $saf->regime . '.type.required'] = trans('core.base.field.required');
            $messages['saf.sides.' . $saf->regime . '.type.*'] = trans('core.loading.invalid_data');

            $messages['saf.sides.' . $saf->regime . '.type_value.required'] = trans('core.base.field.required');
            $messages['saf.sides.' . $saf->regime . '.type_value.*'] = trans('core.loading.invalid_data');

            $messages['saf.sides.' . $saf->regime . '.country.required'] = trans('core.base.field.required');
            $messages['saf.sides.' . $saf->regime . '.country.*'] = trans('core.loading.invalid_data');

            $messages['saf.sides.' . $saf->regime . '.address.required'] = trans('core.base.field.required');
            $messages['saf.sides.' . $saf->regime . '.address.max'] = trans('core.base.field.max_characters', ['max' => 200]);
            $messages['saf.sides.' . $saf->regime . '.address.*'] = trans('core.loading.invalid_data');

            $messages['saf.sides.' . $saf->regime . '.community.required'] = trans('core.base.field.required');
            $messages['saf.sides.' . $saf->regime . '.community.*'] = trans('core.loading.invalid_data');

            $messages['saf.sides.' . $saf->regime . '.phone_number.required'] = trans('core.base.field.required');
            $messages['saf.sides.' . $saf->regime . '.phone_number.*'] = trans('core.loading.invalid_data');

            $messages['saf.sides.' . $saf->regime . '.email.required'] = trans('core.base.field.required');
            $messages['saf.sides.' . $saf->regime . '.email.max'] = trans('core.base.field.max_characters', ['max' => 250]);
            $messages['saf.sides.' . $saf->regime . '.email.*'] = trans('core.loading.invalid_data');
        }

        return $messages;
    }
}
