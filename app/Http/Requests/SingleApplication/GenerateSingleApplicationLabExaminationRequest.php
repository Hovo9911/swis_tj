<?php

namespace App\Http\Requests\SingleApplication;

use App\Http\Requests\Request;
use App\Models\Laboratory\Laboratory;

/**
 * Class SingleApplicationLabExaminationRequest
 * @package App\Http\Requests\SingleApplication
 */
class GenerateSingleApplicationLabExaminationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $byProductIndicator = Laboratory::SELECT_INDICATOR_BY_PRODUCT_INDICATOR;
        $bySphere = Laboratory::SELECT_PRODUCT_BY_SPHERE;
        $customSend = Laboratory::SELECT_CUSTOM_SEND_TO_LAB;

        $rules = [
            'type' => "required|integer|in:{$byProductIndicator},{$bySphere},{$customSend}",
            'saf_number' => "required|exists:saf,regular_number",
            'products' => 'required|array',
            'laboratory_id' => 'required|exists:agency,id',
        ];

        $productsDataFromFront = $this->input('products');
        if (isset($productsDataFromFront)) {
            foreach ($productsDataFromFront as $key => $product) {
                $rules["products.{$key}.product_id"] = 'required';
                $rules["products.{$key}.sample_quantity"] = 'nullable|string|max:100';
                $rules["products.{$key}.sample_measurement_unit"] = "nullable";
                $rules["products.{$key}.sample_number"] = 'nullable|string|max:100';
            }
        }

        if ($this->input('type') == $byProductIndicator) {
            $rules['indicators'] = 'required|array';
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        $productsDataFromFront = $this->input('products');

        $messages = [];
        if (isset($productsDataFromFront)) {
            foreach ($productsDataFromFront as $key => $product) {
                $messages["products.{$key}.sample_quantity.max"] = trans('core.base.field.max_characters', ['max' => 100]);
                $messages["products.{$key}.sample_number.max"] = trans('core.base.field.max_characters', ['max' => 100]);
            }
        }

        return $messages;
    }
}
