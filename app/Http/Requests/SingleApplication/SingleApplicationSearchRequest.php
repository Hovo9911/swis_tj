<?php

namespace App\Http\Requests\SingleApplication;

use App\Http\Requests\Request;

/**
 * Class SingleApplicationSearchRequest
 * @package App\Http\Requests\SingleApplication
 */
class SingleApplicationSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.regular_number' => 'string_with_max',
            'f.status_type' => 'string_with_max|exists:saf,status_type',
            'f.importer_value' => 'string_with_max',
            'f.importer_name' => 'string_with_max',
            'f.exporter_value' => 'string_with_max',
            'f.exporter_name' => 'string_with_max',
            'f.applicant_value' => 'string_with_max',
            'f.applicant_name' => 'string_with_max',
            'f.sub_application_sender_user' => 'string_with_max',
            'f.departure' => 'integer_with_max|exists:reference_custom_body_reference,id',
            'f.appointment' => 'integer_with_max|exists:reference_custom_body_reference,id',
            'f.export_country' => 'string_with_max|exists:reference_countries,code',
            'f.import_country' => 'string_with_max|exists:reference_countries,code',
            'f.importer_exporter_phone_number' => 'string|max:20',
            'f.importer_exporter_email' => 'string_with_max',
            'f.created_at_date_start' => 'date',
            'f.created_at_date_end' => 'date',
            'f.constructor_document' => 'string_with_max|exists:constructor_documents,document_code',
            'f.requested_sub_applications' => 'integer|max:1',
            'f.approved_sub_applications' => 'integer|max:1',
            'f.submitted_application' => 'integer|max:1',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
