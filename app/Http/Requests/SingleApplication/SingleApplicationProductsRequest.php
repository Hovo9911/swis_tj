<?php

namespace App\Http\Requests\SingleApplication;

use App\Http\Requests\Request;
use App\Models\DataModel\DataModel;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplication\SingleApplication;
use Carbon\Carbon;

/**
 * Class SingleApplicationRequest
 * @package App\Http\Requests\SingleApplication
 */
class SingleApplicationProductsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = $this->all();
        $safNumber = $data['saf']['general']['regular_number'] ?? request()->header('sNumber');

        $saf = SingleApplication::select('id', 'regime', 'saf_data_structure_id')->where('regular_number', $safNumber)->safOwnerOrCreator()->firstOrFail();

        $xml = $saf->xmlDataStructure();
        $dataModelAllData = DataModel::allData();

        $blockType = 'subForm';
        $xmlBlock = $xml->xpath('/tabs/tab[@key="products"]')[0]->block;
        $rules = [];
        foreach ($xmlBlock as $block) {
            if ($blockType != (string)$block->attributes()['type']) {
                continue;
            }

            // Xml Fieldset
            foreach ($block->fieldset as $fieldset) {
                foreach ($fieldset as $field) {
                    $rule = '';
                    $attr = $field->attributes();
                    $name = (string)$attr['name'];
                    $dataName = (string)$attr['dataName'];

                    $mdm = (string)$attr['mdm'];

//                    $dataModel2 = DataModel::select('id', 'interface_format', 'reference', 'pattern')->where(DB::raw('id::varchar'), $mdm)->first();

                    if (!isset($dataModelAllData[$mdm])) {
                        continue;
                    }

                    $dataModel = (object)$dataModelAllData[$mdm];

                    if (empty($dataModel->interface_format)) {
                        continue;
                    }

                    if ((string)$attr['safControlledField'] && !empty($data['id'])) {
                        $saf = SingleApplication::select('id', 'regular_number')->where('regular_number', $safNumber)->safOwnerOrCreator()->firstOrFail();
                        $safID = (string)$attr['id'];

                        $safControlledField = $saf->getSafProductControlledFields($data['id']);

                        if (!in_array($safID, $safControlledField['visible_fields']) && !in_array($safID, $safControlledField['mandatory_fields'])) {
                            continue;
                        }

                        if (in_array($safID, $safControlledField['mandatory_fields'])) {
                            $attr->addAttribute('required', 'true');
                        }
                    }

                    $required = (string)$attr['required'];
                    $notZero = ((string)$attr['notZero']) ? '|not_in:0' : '';

                    if (is_null($dataModel->reference)) {
                        $pattern = $dataModel->pattern;
                        $match = $this->returnMatch($dataModel->interface_format);

                        if ($dataModel->type != DataModel::MDM_ELEMENT_TYPE_LINE && !empty($match)) {

                            $validationType = '';
                            if (!empty($match[5])) {
                                $validationType = 'double';
                            } elseif ($match[1] == 'a') {
                                $validationType = 'mdm_alpha';
                            } elseif ($match[1] == 'an') {
                                $validationType = 'mdm_alphanumeric';
                            } elseif ($match[1] == 'n') {
                                $validationType = 'mdm_numeric';
                            }

                            $between = (isset($match[2]) && $match[2] == '..') ? true : false;
                            $length = (!empty($match[3])) ? $match[3] : 0;
                            $required = $required == 'true' ? 'required|' : 'nullable|';

                            if ($name == 'quantity' && empty($data['quantity']) && !empty($data['measurement_unit'])) {
                                $required = 'required_with:measurement_unit|';
                            }

                            if ($name == 'quantity_2' && empty($data['quantity_2']) && !empty($data['measurement_unit_2'])) {
                                $required = 'required_with:measurement_unit_2|';
                            }

                            switch ($validationType) {
                                case 'double':
                                    $rule = "{$required}mdm_alphanumeric";
                                    $length++;
                                    if ($between) {
                                        $rule = "{$rule}|max:{$length}";
                                    } elseif ($length > 0) {
                                        $rule = "{$rule}|size:{$length}";
                                    }

                                    $rule = $rule . '|regex:/^[0-9]{1,' . $match[3] . '}(\.[0-9]{1,' . $match[5] . '})?$/';
                                    break;
                                case 'mdm_alpha':
                                    $rule = "{$required}mdm_alpha";
                                    if ($between) {
                                        $rule .= "|max:{$length}";
                                    } elseif ($length > 0) {
                                        $rule .= "|size:{$length}";
                                    }
                                    break;
                                case 'mdm_alphanumeric':
                                    $rule = "{$required}mdm_alphanumeric";
                                    if ($between) {
                                        $rule .= "|max:{$length}";
                                    } elseif ($length > 0) {
                                        $rule .= "|size:{$length}";
                                    }
                                    break;
                                case 'mdm_numeric':
                                    $rule = "{$required}mdm_numeric";
                                    if ($between) {
                                        $rule .= "|digits_between:0,{$length}";
                                    } elseif ($length > 0) {
                                        $rule .= "digits:{$length}";
                                    }
                                    break;
                            }

                            if ($name == 'netto_weight') {
                                $rule .= "|validate_netto_weight:{$data['brutto_weight']},{$data['netto_weight']}";
                            }

                            if (!is_null($pattern)) {
                                $rule = $rule . "|regex:{$pattern}";
                            }
                        }

                    } else {
                        $required = $required == 'true' ? 'required|' : 'nullable|';

                        if ($name == 'measurement_unit') {
                            $required = 'nullable|required_with:quantity|';
                        }

                        if ($name == 'measurement_unit_2') {
                            $required = 'nullable|required_with:quantity_2|';
                        }

                        $rule = $required . "integer_with_max|exists:reference_$dataModel->reference,id";

                        if ($name == 'producer_code') {
                            if (!empty($data['producer_code'])) {
                                $producerCodes = getReferenceRows(ReferenceTable::REFERENCE_REGISTRY_OF_PRODUCERS, $data['producer_code'], false, ['country_of_origin']);
                                if (!is_null($producerCodes) && $producerCodes->country_of_origin != $data['producer_country']) {
                                    $rule .= "|producer_country_with_origin";
                                }
                            }

                            if (!empty($data['producer_registry_country'])) {
                                $rule = str_replace('nullable', 'required', $rule);
                            }
                        }
                    }

                    $rule .= $notZero;
                    if (!empty($dataName)) {
                        $name = str_replace('_index_', '*', $dataName);
                    }
                    if (strpos($name, ']') !== false) {
                        $name = str_replace(['][', '['], '.', $name);
                        $name = str_replace(']', '', $name);
                    }

                    if (!empty($dataName)) {
                        $parentName = explode('.*.', $name)[0];

                        if (request()->input($parentName)) {
                            foreach (request()->input($parentName) as $key => $value) {
                                $multiName = str_replace('*', $key, $name);
                                $rules[$multiName] = $rule;
                            }
                        }
                    } else {
                        $rules[$name] = $rule;
                    }
                }
            }
        }

        $now = Carbon::now();

        // Batch Validation
        if (isset($data['batch'])) {
            $batches = $data['batch'];

            $nettoWeight = $quantity = 0;
            $totalNettoWeight = $data['netto_weight'];
            $totalQuantity = $data['quantity'];
            foreach ($batches as $key => $batch) {
                $nettoWeight += $batch['netto_weight'];
                $quantity += $batch['quantity'];
            }

            $nettoWeight = str_replace(',', '.', $nettoWeight);
            $totalNettoWeight = str_replace(',', '.', $totalNettoWeight);

            $quantity = str_replace(',', '.', $quantity);
            $totalQuantity = str_replace(',', '.', $totalQuantity);

            $nettoWeight = str_replace(',', '.', $nettoWeight);
            $totalNettoWeight = str_replace(',', '.', $totalNettoWeight);

            $quantity = str_replace(',', '.', $quantity);
            $totalQuantity = str_replace(',', '.', $totalQuantity);

            $batchNumbers = [];
            foreach ($batches as $key => $batch) {

                foreach ($batch as $field => $value) {
                    switch ($field) {
                        case "batch_number":
                            if (!in_array($batch['batch_number'], $batchNumbers)) {
                                $batchNumbers[] = $batch['batch_number'];
                            } else {
                                $rules['batch.' . $key . '.batch_number'] = 'nullable|unique_list';//'required|unique_list';
                            }
                            break;

                        case "production_date":
                            if (!$batch['auto_generated']) {
                                $rules["batch.{$key}.{$field}"] = "nullable|date|before_or_equal:{$now}";//"required|date|before_or_equal:{$now}";
                            }
                            break;

                        case "expiration_date":
                            if (!$batch['auto_generated']) {
                                $rules["batch.{$key}.{$field}"] = "nullable|date|validate_batches_expiration_date:{$batch['production_date']},{$batch['expiration_date']}";//"required|date|validate_batches_expiration_date:{$batch['production_date']},{$batch['expiration_date']}";
                            }
                            break;

                        case "netto_weight":
                            $rules["batch.{$key}.{$field}"] = "required|validate_batches_netto_weight:$nettoWeight,$totalNettoWeight|not_in:0";
                            break;

                        case "quantity":
                            $rules["batch.{$key}.{$field}"] = "required|validate_batches_quantity:$quantity,$totalQuantity|not_in:0";
                            break;
                    }
                }
            }
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        $messages = [];
        $data = $this->all();

        $safNumber = $data['saf']['general']['regular_number'] ?? request()->header('sNumber');

        $saf = SingleApplication::select('id', 'regime', 'saf_data_structure_id')->where('regular_number', $safNumber)->safOwnerOrCreator()->firstOrFail();

        $xml = $saf->xmlDataStructure();
        $dataModelAllData = DataModel::allData();

        $modules = SingleApplication::XML_MODULES;
        array_push($modules, SingleApplication::XML_MODULE_GENERAL);

        if (!empty($this->request->get('module'))) {
            $modules = [$this->request->get('module')];
        }

        $blockType = '';
        if (!empty($this->request->get('block_type'))) {
            $blockType = $this->request->get('block_type');
        }

        foreach ($modules as $module) {

            $xmlData = $xml->xpath('/tabs/tab[@key="' . $module . '"]');
            $xmlBlock = $xmlData[0]->block;

            if ($module == 'sides') {
                foreach ($xmlData[0]->typeBlock as $key => $typeBlock) {
                    if ((string)$typeBlock->attributes()->regime == $data['saf']['general']['regime']) {
                        $xmlBlock = $typeBlock;
                    }
                }
            }

            foreach ($xmlBlock as $block) {

                $blockAttr = $block->attributes();

                if (!empty($blockType)) {
                    if ($blockType != (string)$blockAttr['type']) {
                        continue;
                    }
                } else {

                    if (!empty((string)$blockAttr['type'])) {
                        continue;
                    }
                }

                foreach ($block->fieldset as $fieldset) {

                    foreach ($fieldset as $field) {
                        $attr = $field->attributes();
                        $name = (string)$attr['name'];
                        $mdm = (string)$attr['mdm'];

                        // If Module is General get only Saf Controlled Fields
                        if ($module == SingleApplication::XML_MODULE_GENERAL) {
                            if (empty($attr->safControlledField)) {
                                continue;
                            }
                        }

                        if (strpos($name, ']') !== false) {
                            $name = str_replace(']', '', $name);
                            $name = str_replace('[', '.', $name);
                        }

//                        $dataModel = DataModel::where(DB::raw('id::varchar'), $mdm)->first();

                        if (!isset($dataModelAllData[$mdm])) {
                            continue;
                        }

                        $dataModel = (object)$dataModelAllData[$mdm];

                        if (empty($dataModel->interface_format)) {
                            continue;
                        }

                        $format = $dataModel->interface_format;
                        $match = $this->returnMatch($format);
                        $between = (isset($match[2]) && $match[2] == '..') ? true : false;
                        $length = (!empty($match[3])) ? $match[3] : 0;
                        $validationType = (!empty($match[1]) && in_array($match[1], ['a', 'an'])) ? 'string' : '';

                        if ($validationType == 'string') {
                            if ($between) {
                                $messages[$name . '.max'] = trans('core.base.field.max_characters', ['max' => $length]);
                            } elseif ($length > 0) {
                                $messages[$name . '.size'] = trans('core.base.field.size_characters', ['size' => $length]);
                            }
                        }

                        $messages["{$name}.mdm_alpha"] = trans('core.base.invalid_mdm_alpha');
                        $messages["{$name}.mdm_numeric"] = trans('core.base.invalid_mdm_numeric');
                        $messages["{$name}.mdm_alphanumeric"] = trans('core.base.invalid_mdm_alphanumeric');
                        $messages["{$name}.validate_netto_weight"] = trans('swis.single_application.netto_weight');

                        $messages[$name . '.required'] = trans('core.base.field.required');
                        $messages[$name . '.required_with'] = trans('core.base.field.required');
                        $messages[$name . '.producer_country_with_origin'] = trans('swis.saf.product.producer_country_with_origin.not_valid');
                        $messages[$name . '.*'] = trans('core.loading.invalid_data');
                    }

                }
            }

            if ($module == SingleApplication::XML_MODULE_PRODUCTS && isset($data['batch'])) {
                foreach ($data['batch'] as $key => $batch) {
                    $messages['batch.' . $key . '.batch_number.unique_list'] = trans('core.base.field.unique');
                    $messages['batch.' . $key . '.netto_weight.not_in'] = trans('core.base.field.not_zero');
                    $messages['batch.' . $key . '.quantity.not_in'] = trans('core.base.field.not_zero');
                }
            }
        }

        return $messages;
    }

    /**
     * @param $format
     * @return false|int
     */
    private function returnMatch($format)
    {
        /*
         * 1 - type
         * 2 - between
         * 3 - length
         * 5 - floating point
         */
        preg_match('/^([a,n]{1,2})([\.]{0,3})([\d]+)([\,]([\d]+))?$/', $format, $match);

        return $match;
    }

}
