<?php

namespace App\Http\Requests\SingleApplication;

use App\Http\Requests\Request;

/**
 * Class SingleApplicationSearchRequest
 * @package App\Http\Requests\SingleApplication
 */
class SingleApplicationSendMessageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message' => 'required|string|max:5000',
            'subAppId' => 'required|integer_with_max|exists:saf_sub_applications,id'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'message.required' => trans('core.base.field.required'),
            'message.max' => trans('core.base.field.max_characters', ['max' => 5000]),
            'message.*' => trans('core.loading.invalid_data'),
        ];
    }
}
