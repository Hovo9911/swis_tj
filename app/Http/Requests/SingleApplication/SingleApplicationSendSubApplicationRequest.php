<?php

namespace App\Http\Requests\SingleApplication;

use App\Http\Requests\Request;
use App\Models\Agency\Agency;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use Illuminate\Support\Facades\DB;

/**
 * Class SingleApplicationSendSubApplicationRequest
 * @package App\Http\Requests\SingleApplication
 */
class SingleApplicationSendSubApplicationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return string[]
     */
    public function rules()
    {
        $subDivisionCheck = 'nullable';
        $expressControlCheck = 'required';

        $subApplication = SingleApplicationSubApplications::select('id', 'saf_number', 'agency_id', 'routings', 'manual_created', 'reference_classificator_id')->where('id', (int)$this->request->get('id'))->first();
        if (!is_null($subApplication)) {

            $agency = Agency::select('id', 'subdivision_mark_type')->where('id', $subApplication->agency_id)->first();
            if ($agency->subdivision_mark_type == Agency::SUBDIVISION_MARK_TYPE_REQUIRED) {
                $subDivisionCheck = 'required';
            }

            //
            $refExpressControl = DB::table(ReferenceTable::REFERENCE_EXPRESS_CONTROL)->select('id')->where(function ($q) use ($subApplication) {
                $q->where('document', $subApplication->reference_classificator_id)->orWhereNull('document');
            })->where('show_status', ReferenceTable::STATUS_ACTIVE)->first();

            if (is_null($refExpressControl)) {
                $expressControlCheck = 'nullable';
            }
        }

        return [
            'id' => 'required|integer_with_max|exists:saf_sub_applications,id',
            'express_control_id' => $expressControlCheck . '|integer_with_max',
            'agency_subdivision_id' => $subDivisionCheck . '|integer_with_max'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
