<?php

namespace App\Http\Requests\SingleApplication;

use App\Http\Requests\Request;
use App\Rules\ValidateDelimited;

/**
 * Class SingleApplicationDocumentRequest
 * @package App\Http\Requests\SingleApplication
 */
class SingleApplicationSubApplicationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /* $safNumber = request()->header('sNumber');

         SingleApplication::where(['regular_number' => $safNumber])->firstOrFail();

         $constructorDocument = ConstructorDocument::where('id', $this->request->get('sub_application_type'))->first();

         $subDivisionCheck = 'nullable|';

         if (!is_null($constructorDocument)) {
             $agency = Agency::select('subdivision_mark_type')->where('tax_id', $constructorDocument->company_tax_id)->active()->first();

             if ($agency->subdivision_mark_type == Agency::SUBDIVISION_MARK_TYPE_REQUIRED) {
                 $subDivisionCheck = 'required|';
             }
         }

         $agency = Agency::select('subdivision_mark_type')->where('id', $this->request->get('agency_id'))->active()->first();

         if (!is_null($agency)) {

             if ($agency->subdivision_mark_type == Agency::SUBDIVISION_MARK_TYPE_REQUIRED) {
                 $subDivisionCheck = 'required|';
             }
         }*/

        return [
            'sub_application_type' => 'required|integer_with_max',
            'sub_application_products' => ['required', 'string', new ValidateDelimited(ValidateDelimited::VALIDATION_TYPE_SAF_PRODUCTS, request()->header('sNumber'))],
            'agency_id' => 'required|integer_with_max',
            'express_control_id' => 'nullable|integer_with_max',
//            'agency_subdivision_id' => $subDivisionCheck . 'integer'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
