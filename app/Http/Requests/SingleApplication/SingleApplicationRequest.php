<?php

namespace App\Http\Requests\SingleApplication;

use App\Http\Requests\Request;
use App\Models\BaseModel;
use App\Models\DataModel\DataModel;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplication\SingleApplication;

/**
 * Class SingleApplicationRequest
 * @package App\Http\Requests\SingleApplication
 */
class SingleApplicationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = $this->request->all();

        $statusType = $data['status_type'] ?? '';
        $safRegime = $data['saf']['general']['regime'] ?? '';
        $safNumber = $data['saf']['general']['regular_number'] ?? request()->header('sNumber');

        $modules = SingleApplication::XML_MODULES;
        array_push($modules, SingleApplication::XML_MODULE_GENERAL);

        $saf = SingleApplication::select('id', 'regular_number', 'regime', 'saf_data_structure_id')->where(['regular_number' => $safNumber, 'regime' => $safRegime])->safOwnerOrCreator()->firstOrFail();
        $xml = $saf->xmlDataStructure();

        $dataModelAllData = DataModel::allData();
        $hasSubmittedSubApplication = $saf->hasSubmittedSubApplication();

        if (!empty($this->request->get('module'))) {
            $modules = [$this->request->get('module')];
        }

        $blockType = '';
        if (!empty($this->request->get('block_type'))) {
            $blockType = $this->request->get('block_type');
        }

        if (empty($this->request->get('module'))) {
            $rules = [
                'status_type' => 'required|string_with_max'
            ];
        }

        if ($statusType == SingleApplication::VOID_TYPE) {

            $rules = [
                'status_type' => 'required|string_with_max',
                'saf.general.regime' => 'required|string_with_max',
            ];

            return $rules;
        }

        $rules['sub_applications'] = 'nullable|array';
        $rules['saf_controlled_fields'] = 'nullable|array';
//        $rules['batch'] = 'nullable|array';

        foreach ($modules as $module) {

            if ($statusType && $statusType == SingleApplication::DRAFT_TYPE) {
                if ($module == SingleApplication::XML_MODULE_TRANSPORTATION) {
                    continue;
                }
            }

            if ($statusType && ($statusType == SingleApplication::SEND_TYPE || $statusType == SingleApplication::VERIFIED_TYPE) && $hasSubmittedSubApplication) {
                if ($module == SingleApplication::XML_MODULE_TRANSPORTATION || $module == SingleApplication::XML_MODULE_SIDES) {
                    continue;
                }
            }

            $xmlData = $xml->xpath('/tabs/tab[@key="' . $module . '"]');
//            $subBlocks = $xml->xpath('/tabs/tab[@key="' . $module . '"]/block/subBlock[@mainForm]');
            $xmlBlock = $xmlData[0]->block;

            if ($module == SingleApplication::XML_MODULE_SIDES) {
                foreach ($xmlData[0]->typeBlock as $key => $typeBlock) {
                    if ((string)$typeBlock->attributes()->regime == $safRegime) {
                        $xmlBlock = $typeBlock;
                    }
                }
            }

            foreach ($xmlBlock as $block) {

                $blockAttr = $block->attributes();

                if (!empty($blockType)) {
                    if ($blockType != (string)$blockAttr['type']) {
                        continue;
                    }
                } else {

                    if (!empty((string)$blockAttr['type'])) {
                        continue;
                    }
                }

                // Xml Fieldset
                foreach ($block->fieldset as $fieldset) {
                    foreach ($fieldset as $field) {

                        $attr = $field->attributes();
                        $name = (string)$attr['name'];
                        $dataName = (string)$attr['dataName'];

                        // Hide On Saf
                        if ((bool)$attr->hideOnSaf) {
                            continue;
                        }

                        // If Module is General get only Saf Controlled Fields
                        if ($module == SingleApplication::XML_MODULE_GENERAL) {
                            if (empty($attr->safControlledField)) {
                                continue;
                            }
                        }

                        $mdm = (string)$attr['mdm'];

                        if (empty($mdm) || empty($name)) {
                            continue; // @TODO remove
                        }

                        if (!isset($dataModelAllData[$mdm])) {
                            continue;
                        }

                        $dataModel = (object)$dataModelAllData[$mdm];

                        // Products Saf Controlled fields
                        if ($module == SingleApplication::XML_MODULE_PRODUCTS) {

                            if ((string)$attr['safControlledField'] && !empty($data['id'])) {
                                $safControlledField = $saf->getSafProductControlledFields($data['id']);

                                if (!in_array((string)$attr['id'], $safControlledField['visible_fields'])) {
                                    continue;
                                }

                                if (in_array((string)$attr['id'], $safControlledField['mandatory_fields'])) {
                                    $attr->addAttribute('required', 'true');
                                }
                            }
                        }

                        // ----------------------------------------

                        $required = (string)$attr['required'];

                        ((string)$attr['notZero']) ? $notZero = '|not_in:0' : $notZero = '';

                        if (is_null($dataModel->reference)) {

                            $format = $dataModel->interface_format;
                            $pattern = $dataModel->pattern;

                            $match = $this->returnMatch($format);

                            if ($dataModel->type != DataModel::MDM_ELEMENT_TYPE_LINE && is_array($match) && count($match) > 0) {

                                $validationType = '';
                                if ($match[1] == 'a') {
                                    $validationType = 'mdm_alpha';
                                } elseif ($match[1] == 'an') {
                                    $validationType = 'mdm_alphanumeric';
                                } elseif ($match[1] == 'n') {
                                    $validationType = 'mdm_numeric';
                                }

                                $between = (isset($match[2]) && $match[2] == '..') ? true : false;
                                $length = (!empty($match[3])) ? $match[3] : 0;

                                if (!empty($match[5])) {
                                    $validationType = 'double';
                                }

                                if (!empty((string)$attr->regime)) {
                                    if ($required == 'true' && (string)$attr->regime == $safRegime) {
                                        $required = 'required|';
                                    } else {
                                        $required = 'nullable|';
                                    }
                                } else {
                                    $required = $required == 'true' ? 'required|' : 'nullable|';
                                }

                                if ($name == 'quantity_2') {
                                    if (empty($data['measurement_unit_2'])) {
                                        $required = 'nullable|';
                                    }
                                }

                                if ($statusType && $statusType == SingleApplication::DRAFT_TYPE) {
                                    if ($module == SingleApplication::XML_MODULE_SIDES) {

                                        if ((string)$block->attributes()->regime != $safRegime) {
                                            $required = 'nullable|';
                                        }

                                    }
                                }

                                switch ($validationType) {
                                    case 'string':
                                    case 'mdm_alphanumeric':
                                        $rule = $required . 'mdm_alphanumeric';
                                        if ($between) {
                                            $rule .= '|max:' . $length;
                                        } elseif ($length > 0) {
                                            $rule .= '|size:' . $length;
                                        }
                                        break;
                                    case 'integer':
                                        $rule = $required . 'mdm_numeric';
                                        if ($between) {
                                            $rule .= '|digits_between:0,' . $length;
                                        } elseif ($length > 0) {
                                            $rule = $required . 'digits:' . $length;
                                        }
                                        break;
                                    case 'double':
                                        $rule = $required . 'mdm_alphanumeric';
                                        $length++;
                                        if ($between) {
                                            $rule = $rule . '|max:' . $length;
                                        } elseif ($length > 0) {
                                            $rule = $rule . '|size:' . $length;
                                        }

                                        if (arrayKeyExistsRecursive('total_value', $data)) {
                                            $data['saf']['product']['total_value'] = str_replace(',', '.', $data['saf']['product']['total_value']);
                                        }

                                        $rule = $rule . '|regex:/^[+-]?[0-9]{1,' . $match[3] . '}(\.[0-9]{1,' . $match[5] . '})?$/';
                                        break;
                                    case 'mdm_alpha':
                                        $rule = $required . 'mdm_alpha';
                                        if ($between) {
                                            $rule .= '|max:' . $length;
                                        } elseif ($length > 0) {
                                            $rule .= '|size:' . $length;
                                        }
                                        break;
                                    case 'mdm_numeric':
                                        $rule = $required . 'mdm_numeric';
                                        if ($between) {
                                            $rule .= '|digits_between:0,' . $length;
                                        } elseif ($length > 0) {
                                            $rule = $rule . 'digits:' . $length;
                                        }
                                        break;
                                }
                            }

                            if (isset($data['saf']) && isset($data['saf']['sides'])) {

                                $sidesData = $data['saf']['sides'];
                                $sidesTypes = [SingleApplication::REGIME_IMPORT, SingleApplication::REGIME_EXPORT];

                                foreach ($sidesTypes as $sidesType) {

                                    if (isset($sidesData[$sidesType], $sidesData[$sidesType]['type']) && ($name == "saf[sides][$sidesType][passport]" || $name == "saf[sides][$sidesType][type_value]")) {

                                        $passportRule = 'nullable';
                                        $typeValueRule = 'required';

                                        // If Foreign citizen
                                        if (ReferenceTable::isLegalEntityType($sidesData[$sidesType]['type'], BaseModel::ENTITY_TYPE_FOREIGN_CITIZEN)) {

                                            if ($sidesType == $safRegime) {
                                                $passportRule = 'required';
                                            } else {
                                                $typeValueRule = 'nullable';
                                            }

                                        }

                                        // If Natural Person
                                        if (ReferenceTable::isLegalEntityType($sidesData[$sidesType]['type'], BaseModel::ENTITY_TYPE_USER_ID)) {
                                            $passportRule = 'required';
                                        }

                                        // Passport Field
                                        if ($name == "saf[sides][$sidesType][passport]") {
                                            $rule = str_replace(['nullable', 'required'], $passportRule, $rule);
                                        }

                                        // Tax_Id / SSN
                                        if ($name == "saf[sides][$sidesType][type_value]") {
                                            $rule = str_replace(['nullable', 'required'], $typeValueRule, $rule);
                                        }
                                    }
                                }
                            }

                            if (!is_null($pattern)) {
                                $regex = '|regex:' . $pattern;
                                $rule = $rule . $regex;
                            }


                        } else {

                            // -------------------  // Reference data validate // ---------------------- //

                            if (!empty((string)$attr->regime)) {
                                if ($required == 'true' && (string)$attr->regime == $safRegime) {
                                    $required = 'required|';
                                } else {
                                    $required = 'nullable|';
                                }
                            } else {
                                $required = $required == 'true' ? 'required|' : 'nullable|';
                            }

                            if ($statusType && $statusType == SingleApplication::DRAFT_TYPE) {
                                if ($module == SingleApplication::XML_MODULE_SIDES) {

                                    if ((string)$block->attributes()->regime != $safRegime) {
                                        $required = 'nullable|';
                                    }
                                }
                            }

                            if (strpos($name, ']') !== false) {
                                $name = str_replace(['][', '['], '.', $name);
                                $name = str_replace(']', '', $name);
                            }

                            $rule = $required . "integer_with_max|exists:reference_$dataModel->reference,id";

                            if ($name == 'producer_code') {
                                if (!empty($data['producer_code'])) {

                                    $producerCodes = getReferenceRows(ReferenceTable::REFERENCE_REGISTRY_OF_PRODUCERS, $data['producer_code'], false, ['country_of_origin']);

                                    if (!is_null($producerCodes) && $producerCodes->country_of_origin != $data['producer_country']) {
                                        $rule .= '|producer_country_with_origin';
                                    }
                                }

                                if (!empty($data['producer_registry_country'])) {
                                    $rule = str_replace('nullable', 'required', $rule);
                                }
                            }

                        }

                        $rule .= $notZero;

                        if (!empty($dataName)) {
                            $name = str_replace('_index_', '*', $dataName);
                        }

                        if (strpos($name, ']') !== false) {
                            $name = str_replace(['][', '['], '.', $name);
                            $name = str_replace(']', '', $name);
                        }

                        if (!empty($dataName)) {
                            $parentName = explode('.*.', $name)[0];

                            if (request()->input($parentName)) {
                                foreach (request()->input($parentName) as $key => $value) {
                                    $multiName = str_replace('*', $key, $name);
                                    $rules[$multiName] = $rule;
                                }
                            }
                        } else {
                            $rules[$name] = $rule;
                        }
                    }
                }
            }
        }

        // Transit country
        $rules['saf.transportation.transit_country'] = 'array';
        $rules['saf.transportation.transit_country.*'] = 'nullable|integer_with_max|exists:reference_countries,id|distinct';

        // comma problem
        $rules['saf.product.total_value'] = 'nullable|string_with_max';
        $rules['saf.product.total_value_all'] = 'nullable|string_with_max';

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        $data = $this->request->all();

        $safNumber = $data['saf']['general']['regular_number'] ?? request()->header('sNumber');

        $saf = SingleApplication::select('id', 'regular_number', 'regime', 'saf_data_structure_id')->where('regular_number', $safNumber)->safOwnerOrCreator()->firstOrFail();
        $xml = $saf->xmlDataStructure();

        $modules = SingleApplication::XML_MODULES;
        array_push($modules, SingleApplication::XML_MODULE_GENERAL);

        $dataModelAllData = DataModel::allData();

        if (!empty($this->request->get('module'))) {
            $modules = [$this->request->get('module')];
        }

        $blockType = '';
        if (!empty($this->request->get('block_type'))) {
            $blockType = $this->request->get('block_type');
        }

        $messages = [];
        foreach ($modules as $module) {

            $xmlData = $xml->xpath('/tabs/tab[@key="' . $module . '"]');
            $xmlBlock = $xmlData[0]->block;

            if ($module == 'sides') {
                foreach ($xmlData[0]->typeBlock as $key => $typeBlock) {
                    if ((string)$typeBlock->attributes()->regime == $data['saf']['general']['regime']) {
                        $xmlBlock = $typeBlock;
                    }
                }
            }

            foreach ($xmlBlock as $block) {

                $blockAttr = $block->attributes();

                if (!empty($blockType)) {
                    if ($blockType != (string)$blockAttr['type']) {
                        continue;
                    }
                } else {

                    if (!empty((string)$blockAttr['type'])) {
                        continue;
                    }
                }

                foreach ($block->fieldset as $fieldset) {

                    foreach ($fieldset as $field) {
                        $attr = $field->attributes();
                        $name = (string)$attr['name'];
                        $mdm = (string)$attr['mdm'];

                        // If Module is General get only Saf Controlled Fields
                        if ($module == SingleApplication::XML_MODULE_GENERAL) {
                            if (empty($attr->safControlledField)) {
                                continue;
                            }
                        }

                        if (strpos($name, ']') !== false) {
                            $name = str_replace(']', '', $name);
                            $name = str_replace('[', '.', $name);
                        }

                        if (!isset($dataModelAllData[$mdm])) {
                            continue;
                        }

                        $dataModel = (object)$dataModelAllData[$mdm];

                        $format = $dataModel->interface_format;
                        $match = $this->returnMatch($format);

                        if (is_array($match) && count($match) > 0) {

                            $between = (isset($match[2]) && $match[2] == '..') ? true : false;
                            $length = (!empty($match[3])) ? $match[3] : 0;
                            $validationType = (in_array($match[1], ['a', 'an'])) ? 'string' : '';

                            if ($validationType == 'string') {
                                if ($between) {
                                    $messages[$name . '.max'] = trans('core.base.field.max_characters', ['max' => $length]);
                                } elseif ($length > 0) {
                                    $messages[$name . '.size'] = trans('core.base.field.size_characters', ['size' => $length]);
                                }
                            }
                        }

                        $messages["{$name}.mdm_alpha"] = trans('core.base.invalid_mdm_alpha');
                        $messages["{$name}.mdm_numeric"] = trans('core.base.invalid_mdm_numeric');
                        $messages["{$name}.mdm_alphanumeric"] = trans('core.base.invalid_mdm_alphanumeric');

                        $messages[$name . '.required'] = trans('core.base.field.required');
                        $messages[$name . '.producer_country_with_origin'] = trans('swis.saf.product.producer_country_with_origin.not_valid');
                        $messages[$name . '.*'] = trans('core.loading.invalid_data');
                    }

                }
            }

            if ($module == SingleApplication::XML_MODULE_PRODUCTS && isset($data['batch'])) {
                foreach ($data['batch'] as $key => $batch) {
                    $messages['batch.' . $key . '.batch_number.unique_list'] = trans('core.base.field.unique');
                }
            }
        }

        return $messages;
    }

    /**
     * @param $format
     * @return false|int
     */
    private function returnMatch($format)
    {
        /*
         * 1 - type
         * 2 - between
         * 3 - length
         * 5 - floating point
         */
        preg_match('/^([a,n]{1,2})([\.]{0,3})([\d]+)([\,]([\d]+))?$/', $format, $match);

        return $match;
    }

}
