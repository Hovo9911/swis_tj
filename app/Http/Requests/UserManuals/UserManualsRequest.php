<?php

namespace App\Http\Requests\UserManuals;

use App\Http\Requests\Request;
use App\Models\UserManualsRoles\UserManualsRoles;


/**
 * Class UserManualsRequest
 * @package App\Http\Requests\Routing
 */
class UserManualsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Function to get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'type' => 'required',
            'version' => 'required',
            'file' => 'nullable|string_with_max',
            'available_without_auth' => 'required|integer_with_max',
            'url' => 'nullable|string_with_max',
            'roles' => 'nullable|array',
            'roles.*' => 'in: ' . implode(',', UserManualsRoles::USER_MANUAL_ROLE_TYPES),
            'agencies' => 'nullable|array',
            'agencies.*' => 'string_with_max|exists:agency,tax_id',
        ];

        if ($this->request->has('ml')) {
            foreach ($this->request->get('ml') as $key => $ml) {
                $rules['ml.' . $key . '.module'] = 'required|max:255';
                $rules['ml.' . $key . '.description'] = 'max:1024';
            }
        }

        return $rules;
    }

    /**
     * Function to get the validation messages
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }

}
