<?php

namespace App\Http\Requests\UserManuals;

use App\Http\Requests\Request;

/**
 * Class UserManualsSearchRequest
 * @package App\Http\Requests\UserManuals
 */
class UserManualsSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.id' => 'integer_with_max',
            'f.module' => 'string_with_max',
            'f.description' => 'string_with_max',
            'f.type' => 'string_with_max|exists:user_manuals,type',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
