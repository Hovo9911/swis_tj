<?php

namespace App\Http\Requests\Laboratory;

use App\Http\Requests\Request;
use App\Models\Laboratory\Laboratory;
use Carbon\Carbon;

/**
 * Class LaboratoryRequest
 * @package App\Http\Requests\Laboratory
 */
class LaboratoryUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = $this->request->all();

        $rules = [
            'tax_id' => "required|tax_id_validator|exists:agency,tax_id",
            'laboratory_code' => 'required|string|max:250',
            'users' => 'nullable|array',
            'certificate_number' => 'required|string|max:35',
            'certification_period_validity' => 'required|after_or_equal:' . Carbon::parse(currentDateTime())->addMinute(-1),
            'phone_number' => 'required|phone_number_validator',
            'email' => 'required|email|max:250',
            'website_url' => 'nullable|string|max:2048',
            'show_status' => 'required|in:1,2'
        ];

        if (Auth()->user()->isSwAdmin()) {
            $rules['company_tax_id'] = 'required|tax_id_validator|exists:agency,tax_id';
        }

        $currentLab = Laboratory::find($data['id']);

        if (!is_null($currentLab) && $currentLab->laboratory_code != $data['laboratory_code']) {
            $rules['laboratory_code'] .= '|unique:laboratory';
        }

        foreach ($this->request->get('ml') as $key => $ml) {
            $rules['ml.' . $key . '.name'] = 'required|max:255';
            $rules['ml.' . $key . '.head_name'] = 'nullable|string|max:255';
        }

        if ($this->input('indicators')) {
            foreach ($this->input('indicators') as $id => $indicator) {
                $rules["indicators.{$id}.price"] = 'required|numeric';
                $rules["indicators.{$id}.duration"] = 'required|numeric';
            }
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        $messages = [
            'laboratory_code.required' => trans('core.base.field.required'),
            'laboratory_code.max' => trans('core.base.field.max_characters', ['max' => 250]),
            'laboratory_code.unique' => trans('core.base.field.unique'),
            'laboratory_code.*' => trans('core.loading.invalid_data'),

            'certificate_number.required' => trans('core.base.field.required'),
            'certificate_number.max' => trans('core.base.field.max_characters', ['max' => 35]),
            'certificate_number.*' => trans('core.loading.invalid_data'),

            'certification_period_validity.after_or_equal' => trans('core.base.field.start_date', ['start' => currentDateTimeFront()]),
            'certification_period_validity.*' => trans('core.loading.invalid_data'),

            'phone_number.required' => trans('core.base.field.required'),
            'phone_number.*' => trans('core.loading.invalid_data'),

            'email.required' => trans('core.base.field.required'),
            'email.max' => trans('core.base.field.max_characters', ['max' => 250]),
            'email.email' => trans('core.loading.email_valid'),
            'email.*' => trans('core.loading.invalid_data'),

            'website_url.max' => trans('core.base.field.max_characters', ['max' => 2048]),
            'website_url.*' => trans('core.loading.invalid_data'),
        ];

        foreach ($this->request->get('ml') as $key => $ml) {
            $messages['ml.' . $key . '.name.max'] = trans('core.base.field.max_characters', ['max' => 255]);
            $messages['ml.' . $key . '.head_name.max'] = trans('core.base.field.max_characters', ['max' => 255]);
        }

        return $messages;
    }
}
