<?php

namespace App\Http\Requests\Laboratory;

use App\Http\Requests\Request;
use Carbon\Carbon;

/**
 * Class LaboratoryRequest
 * @package App\Http\Requests\Laboratory
 */
class LaboratoryStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'tax_id' => "required|tax_id_validator|exists:agency,tax_id",
            'legal_entity_name' => "required|string|max:255",
            'address' => "nullable|string|max:70",
            'laboratory_code' => "required|string|max:250|unique_lab_code:{$this->input('tax_id')},{$this->input('company_tax_id')}",
            'users' => 'nullable|array',
            'certificate_number' => 'required|string|max:35',
            'certification_period_validity' => 'required|after_or_equal:' . Carbon::parse(currentDateTime())->addMinute(-1),
            'phone_number' => 'required|phone_number_validator',
            'email' => 'required|email|max:250',
            'website_url' => 'nullable|string|max:2048',
            'show_status' => 'required|in:1,2'
        ];

        if (Auth()->user()->isSwAdmin()) {
            $rules['company_tax_id'] = 'required|tax_id_validator|exists:agency,tax_id';
        }

        foreach ($this->request->get('ml') as $key => $ml) {
            $rules['ml.' . $key . '.name'] = 'required|max:255';
            $rules['ml.' . $key . '.head_name'] = 'nullable|string|max:255';
        }

        if ($this->input('indicators')) {
            foreach ($this->input('indicators') as $id => $indicator) {
                $rules["indicators.{$id}.price"] = 'required|numeric';
                $rules["indicators.{$id}.duration"] = 'required|numeric';
            }
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        $messages = [
            'tax_id.required' => trans('core.base.field.required'),
            'tax_id.unique' => trans('core.base.field.unique'),
            'tax_id.*' => trans('core.loading.invalid_data'),

            'laboratory_code.required' => trans('core.base.field.required'),
            'laboratory_code.max' => trans('core.base.field.max_characters', ['max' => 250]),
            'laboratory_code.unique_lab_code' => trans('core.base.field.unique'),
            'laboratory_code.*' => trans('core.loading.invalid_data'),

            'certificate_number.required' => trans('core.base.field.required'),
            'certificate_number.max' => trans('core.base.field.max_characters', ['max' => 35]),
            'certificate_number.*' => trans('core.loading.invalid_data'),

            'certification_period_validity.after_or_equal' => trans('core.base.field.start_date', ['start' => currentDateTimeFront()]),
            'certification_period_validity.*' => trans('core.loading.invalid_data'),

            'address.max' => trans('core.base.field.max_characters', ['max' => 70]),
            'address.*' => trans('core.loading.invalid_data'),

            'phone_number.required' => trans('core.base.field.required'),
            'phone_number.*' => trans('core.loading.invalid_data'),

            'email.required' => trans('core.base.field.required'),
            'email.max' => trans('core.base.field.max_characters', ['max' => 250]),
            'email.email' => trans('core.loading.email_valid'),
            'email.*' => trans('core.loading.invalid_data'),

            'website_url.max' => trans('core.base.field.max_characters', ['max' => 2048]),
            'website_url.*' => trans('core.loading.invalid_data'),

            'legal_entity_name.required' => trans('core.base.field.required'),
            'legal_entity_name.max' => trans('core.base.field.max_characters', ['max' => 255]),
            'legal_entity_name.*' => trans('core.loading.invalid_data'),
        ];

        foreach ($this->request->get('ml') as $key => $ml) {
            $messages['ml.' . $key . '.name.max'] = trans('core.base.field.max_characters', ['max' => 255]);
            $messages['ml.' . $key . '.head_name.max'] = trans('core.base.field.max_characters', ['max' => 255]);
        }

        return $messages;
    }
}
