<?php

namespace App\Http\Requests\Laboratory;

use App\Http\Requests\Request;

/**
 * Class LaboratorySearchRequest
 * @package App\Http\Requests\Laboratory
 */
class LaboratorySearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.id' => 'integer_with_max',
            'f.name' => 'string_with_max',
            'f.agency_tax_id' => 'string_with_max|exists:agency,tax_id',
            'f.tax_id' => 'string_with_max',
            'f.laboratory_code' => 'string_with_max',
            'f.legal_entity_name' => 'string_with_max',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
