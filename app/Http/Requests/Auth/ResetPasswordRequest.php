<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

/**
 * Class ProfileSettingsRequest
 * @package App\Http\Requests\ProfileSettings
 */
class ResetPasswordRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'token' => 'required',
            'password' => 'required|confirmed|validate_password|set_different_passwords',
            'password_confirmation' => 'required|validate_password|set_different_passwords',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [

            'password.required' => trans('core.base.field.required'),
            'password.confirmed' => trans('core.base.password.dont_match'),
            'password.validate_password' => trans('swis.profile_settings.password_regex_incorrect', ['min' => config('global.password_characters.min'), 'max' => config('global.password_characters.max')]),
            'password.set_different_passwords' => trans('core.base.field.need_set_different_password.message'),
            'password.*' => trans('core.loading.invalid_data'),

            'password_confirmation.required' => trans('core.base.field.required'),
            'password_confirmation.validate_password' => trans('swis.profile_settings.password_regex_incorrect', ['min' => config('global.password_characters.min'), 'max' => config('global.password_characters.max')]),
            'password_confirmation.set_different_passwords' => trans('core.base.field.need_set_different_password.message'),
            'password_confirmation.*' => trans('core.loading.invalid_data'),

        ];
    }
}
