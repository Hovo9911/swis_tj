<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

/**
 * Class LoginRequest
 * @package App\Http\Requests\Auth
 */
class LoginRequest extends Request
{
    /**
     * @return array|bool
     */
    public function rules()
    {
        return [
            'username' => 'required|string_with_max',
            'password' => 'required|string_with_max'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'username.required' => trans('core.base.field.required'),
            'username.*' => trans('core.base.field.email_valid'),
            'password.required' => trans('core.base.field.required'),
            'password.*' => trans('core.loading.invalid_data'),
        ];
    }
}