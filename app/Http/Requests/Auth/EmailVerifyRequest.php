<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

/**
 * Class EmailVerifyRequest
 * @package App\Http\Requests\Auth
 */
class EmailVerifyRequest extends Request
{
    /**
     * @return array|bool
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => trans('core.base.field.required'),
            'email.email' => trans('core.loading.email_valid'),
            'email.*' => trans('core.base.field.email_valid'),
        ];
    }


}