<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

/**
 * Class PhoneActivationCodeRequest
 * @package App\Http\Requests\Auth
 */
class PhoneActivationCodeRequest extends Request
{
    /**
     * @return array|bool
     */
    public function rules()
    {
        return [
            'activation_code' => 'required',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'activation_code.required' => trans('core.base.field.required'),
        ];
    }

}