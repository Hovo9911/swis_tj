<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

/**
 * Class PhoneVerifyRequest
 * @package App\Http\Requests\Auth
 */
class PhoneVerifyRequest extends Request
{
    /**
     * @return array|bool
     */
    public function rules()
    {
        return [
            'phone_number' => 'required|phone_number_validator'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'phone_number.required' => trans('core.base.field.required'),
        ];
    }

}