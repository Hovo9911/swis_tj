<?php

namespace App\Http\Requests\Role;

use App\Http\Requests\Request;

/**
 * Class RoleSearchRequest
 * @package App\Http\Requests\Role
 */
class RoleSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.id' => 'integer_with_max',
            'f.name' => 'string_with_max',
            'f.menu' => 'integer_with_max|exists:menu,id',
            'f.created_at_start' => 'date',
            'f.created_at_end' => 'date',
            'f.agency' => 'string_with_max|exists:agency,tax_id',
            'f.create_permission' => 'nullable|in:1',
            'f.update_permission' => 'nullable|in:1',
            'f.delete_permission' => 'nullable|in:1'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
