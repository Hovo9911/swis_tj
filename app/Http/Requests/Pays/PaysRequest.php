<?php

namespace App\Http\Requests\Pays;

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

/**
 * Class PaysRequest
 * @package App\Http\Requests\Pays
 */
class PaysRequest extends Request
{
    /**
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(
            response()->json([
                'status' => 'error',
                'code' => 422,
                'description' => $validator->errors()->all()
            ], 200)
        );
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'token' => 'required|max:120',
            'document_id' => 'required',
            'payment_done_date' => 'required',
            'amount' => 'required|numeric|min:1',
            'beneficiary_name' => 'required',
            'tax_id' => 'required',
            'user_id' => 'nullable',
            'description' => 'nullable'
        ];
    }

    /**
     * Get the validation rules fail messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'amount.min' => trans('core.base.field.min_characters', ['min' => 1.00]),
        ];
    }
}
