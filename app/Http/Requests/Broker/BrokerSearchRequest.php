<?php

namespace App\Http\Requests\Broker;

use App\Http\Requests\Request;

/**
 * Class BrokerSearchRequest
 * @package App\Http\Requests\Broker
 */
class BrokerSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.id' => 'integer_with_max',
            'f.tax_id' => 'string_with_max',
            'f.name' => 'string_with_max',
            'f.legal_entity_name' => 'string_with_max',
            'f.certification_number' => 'string_with_max',
            'f.address' => 'string_with_max',
            'f.show_status' => 'in:1,2',
            'f.certification_period_validity_date_start' => 'date',
            'f.certification_period_validity_date_end' => 'date',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
