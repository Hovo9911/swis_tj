<?php

namespace App\Http\Requests\Broker;

use App\Http\Requests\Request;
use Carbon\Carbon;

/**
 * Class BrokerRequest
 * @package App\Http\Requests\Broker
 */
class BrokerStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'tax_id' => 'required|tax_id_validator|unique:broker,tax_id',
            'legal_entity_name' => 'required|string|max:255',
            'address' => 'nullable|string|max:255',

            'certification_number' => 'nullable|string|max:70',
            'certification_period_validity' => 'required|date|after:' . Carbon::parse(currentDate())->addDays(-1),
            'phone_number' => 'required|phone_number_validator',
            'email' => 'required|email|string|max:250',
            'show_status' => "required|in:1,2",
        ];

        foreach ($this->request->get('ml') as $key => $ml) {
            $rules['ml.' . $key . '.name'] = 'required|max:255';
            $rules['ml.' . $key . '.description'] = 'nullable|string|max:500';
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        $messages = [
            'tax_id.required' => trans('core.base.field.required'),
            'tax_id.unique' => trans('core.base.field.unique'),
            'tax_id.*' => trans('core.loading.invalid_data'),

            'email.required' => trans('core.base.field.required'),
            'email.email' => trans('core.loading.email_valid'),
            'email.max' => trans('core.base.field.max_characters', ['max' => 250]),
            'email.*' => trans('core.loading.invalid_data'),

            'address.max' => trans('core.base.field.max_characters', ['max' => 255]),
            'address.*' => trans('core.loading.invalid_data'),

            'legal_entity_name.required' => trans('core.base.field.required'),
            'legal_entity_name.max' => trans('core.base.field.max_characters', ['max' => 255]),
            'legal_entity_name.*' => trans('core.loading.invalid_data'),

            'certification_period_validity.required' => trans('core.base.field.required'),
            'certification_period_validity.after' => trans('swis.broker.certification_deadline.invalid_date'),
            'certification_number.max' => trans('core.base.field.max_characters', ['max' => 70]),
        ];

        foreach ($this->request->get('ml') as $key => $ml) {
            $messages['ml.' . $key . '.name.max'] = trans('core.base.field.max_characters', ['max' => 255]);
            $messages['ml.' . $key . '.description.max'] = trans('core.base.field.max_characters', ['max' => 500]);
        }

        return $messages;
    }
}
