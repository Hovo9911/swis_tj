<?php

namespace App\Http\Requests\Folder;

use App\Http\Requests\Request;

/**
 * Class FolderDocumentSearchRequest
 * @package App\Http\Requests\Agency
 */
class FolderDocumentSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.document_access_password' => 'integer_with_max',
            'f.document_type' => 'string_with_max|exists:reference_classificator_of_documents,code',
            'f.description' => 'string_with_max',
            'f.document_number' => 'string_with_max',
            'f.document_release_date_start' => 'date',
            'f.document_release_date_end' => 'date',
            'f.search_folder_name' => 'string_with_max',
            'f.search_folder_desc' => 'string_with_max',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
