<?php

namespace App\Http\Requests\Folder;

use App\Http\Requests\Request;

/**
 * Class DocumentStoreRequest
 * @package App\Http\Requests\Folder
 */
class DocumentStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'document_name' => 'required|string|max:250',
            'document_description' => 'nullable|string|max:500',
            'document_number' => 'required|string|max:50',
            'document_type' => 'required|string_with_max|exists:reference_classificator_of_documents,code',
            'document_release_date' => 'required|date|before_or_equal:' . currentDate(),
            'document_file' => 'required|string|max:10000',

            'holder_name' => 'required|string_with_max',
            'holder_type' => 'required|integer_with_max|exists:reference_legal_entity_form_reference,id',
            'holder_type_value' => 'required|string|max:15',
            'holder_country' => 'required|integer_with_max|exists:reference_countries,id',
            'holder_address' => 'nullable|string|max:200',
            'holder_community' => 'nullable|string_with_max',
            'holder_phone_number' => 'required|phone_number_validator',
            'holder_email' => 'required|string|email|max:250',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'document_name.required' => trans('core.base.field.required'),
            'document_name.max' => trans('core.base.field.max_characters', ['max' => 250]),
            'document_name.*' => trans('core.loading.invalid_data'),

            'document_description.max' => trans('core.base.field.max_characters', ['max' => 500]),
            'document_description.*' => trans('core.loading.invalid_data'),

            'document_number.required' => trans('core.base.field.required'),
            'document_number.max' => trans('core.base.field.max_characters', ['max' => 50]),
            'document_number.*' => trans('core.loading.invalid_data'),

            'document_type.required' => trans('core.base.field.required'),
            'document_type.*' => trans('core.loading.invalid_data'),

            'document_release_date.required' => trans('core.base.field.required'),
            'document_release_date.before_or_equal' => trans('core.base.field.end_date', ['end' => currentDate()]),
            'document_release_date.*' => trans('core.loading.invalid_data'),

            'document_file.required' => trans('core.base.field.required'),

            'holder_name.required' => trans('core.base.field.required'),
            'holder_name.*' => trans('core.loading.invalid_data'),

            'holder_type.required' => trans('core.base.field.required'),
            'holder_type.*' => trans('core.loading.invalid_data'),

            'holder_type_value.required' => trans('core.base.field.required'),
            'holder_type_value.*' => trans('core.loading.invalid_data'),

            'holder_country.required' => trans('core.base.field.required'),
            'holder_country.*' => trans('core.loading.invalid_data'),

            'holder_email.required' => trans('core.base.field.required'),
            'holder_email.max' => trans('core.base.field.max_characters', ['max' => 255]),
            'holder_email.*' => trans('core.loading.invalid_data'),

            'holder_address.max' => trans('core.base.field.max_characters', ['max' => 200]),
            'holder_address.*' => trans('core.loading.invalid_data'),
        ];
    }
}
