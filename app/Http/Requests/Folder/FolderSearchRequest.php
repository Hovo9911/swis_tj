<?php

namespace App\Http\Requests\Folder;

use App\Http\Requests\Request;

/**
 * Class FolderSearchRequest
 * @package App\Http\Requests\Folder
 */
class FolderSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.name' => 'string_with_max',
            'f.access_password' => 'string_with_max',
            'f.description' => 'string_with_max',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
