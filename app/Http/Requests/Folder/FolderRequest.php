<?php

namespace App\Http\Requests\Folder;

use App\Http\Requests\Request;
use App\Models\BaseModel;
use App\Models\ReferenceTable\ReferenceTable;
use Illuminate\Support\Facades\Auth;

/**
 * Class FolderRequest
 * @package App\Http\Requests\Folder
 */
class FolderRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = $this->request->all();

        $rules = [
            'regenerate_access_key' => 'nullable|in:1',
            'folder_name' => 'required|max:20',
            'folder_description' => 'required|max:250',
            'notes' => 'nullable|max:512',
            'documents' => 'array'
        ];

        // Holder
        $checkHolderRules = true;
        if (!Auth::user()->isBroker() && $data['folder_id']) {
            $checkHolderRules = false;
        }

        if ($checkHolderRules) {

            $rules['holder_name'] = 'required|string_with_max';
            $rules['holder_type'] = 'required|integer_with_max|exists:reference_legal_entity_form_reference,id';
            $rules['holder_type_value'] = 'required|string|max:15';
            $rules['holder_registration_number'] = 'nullable|max:250';
            $rules['holder_country'] = 'required|integer_with_max|exists:reference_countries,id';
            $rules['holder_address'] = 'nullable|string|max:200';
            $rules['holder_community'] = 'nullable|string_with_max';
            $rules['holder_phone_number'] = 'required|phone_number_validator';
            $rules['holder_email'] = 'required|string|email|max:250';

            // Passport type
            $holderTypeValueRule = 'required';
            if (isset($data['holder_type'])) {
                if (ReferenceTable::isLegalEntityType($data['holder_type'], BaseModel::ENTITY_TYPE_USER_ID)) {
                    $rules['holder_passport'] = 'required|passport_validator';
                }

                if (ReferenceTable::isLegalEntityType($data['holder_type'], BaseModel::ENTITY_TYPE_FOREIGN_CITIZEN)) {
                    $holderPassportRule = 'required';

                    if (isset($data['holder_type_value']) || isset($data['holder_passport'])) {
                        $holderTypeValueRule = 'nullable';
                        $holderPassportRule = 'nullable';
                    }

                    $rules['holder_passport'] = $holderPassportRule;
                }
            }

            $rules['holder_type_value'] = $holderTypeValueRule . '|string|string|max:15';
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'folder_name.required' => trans('core.base.field.required'),
            'folder_name.max' => trans('core.base.field.max_characters', ['max' => 20]),
            'folder_name.*' => trans('core.loading.invalid_data'),

            'folder_description.required' => trans('core.base.field.required'),
            'folder_description.max' => trans('core.base.field.max_characters', ['max' => 250]),
            'folder_description.*' => trans('core.loading.invalid_data'),

            'holder_name.required' => trans('core.base.field.required'),
            'holder_name.*' => trans('core.loading.invalid_data'),

            'holder_type.required' => trans('core.base.field.required'),
            'holder_type.*' => trans('core.loading.invalid_data'),

            'holder_type_value.required' => trans('core.base.field.required'),
            'holder_type_value.max' => trans('core.base.field.max_characters', ['max' => 15]),
            'holder_type_value.*' => trans('core.loading.invalid_data'),

            'holder_country.required' => trans('core.base.field.required'),
            'holder_country.*' => trans('core.loading.invalid_data'),

            'holder_email.required' => trans('core.base.field.required'),
            'holder_email.max' => trans('core.base.field.max_characters', ['max' => 255]),
            'holder_email.*' => trans('core.loading.invalid_data'),

            'holder_address.max' => trans('core.base.field.max_characters', ['max' => 200]),
            'holder_address.*' => trans('core.loading.invalid_data'),

            'holder_phone_number.*' => trans('core.loading.invalid_data'),

            'notes.max' => trans('core.base.field.max_characters', ['max' => 512]),
            'notes.*' => trans('core.loading.invalid_data'),
        ];
    }
}
