<?php

namespace App\Http\Requests\LaboratoryIndicator;

use App\Http\Requests\Request;

/**
 * Class LaboratoryIndicatorRequest
 * @package App\Http\Requests\LaboratoryIndicator
 */
class LaboratoryIndicatorRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'laboratory_id' => "required|integer_with_max|exists:laboratory,id",
            'indicator_id' => 'required|integer_with_max',
            'price' => 'required|numeric|min:0|max:10000000',
            'duration' => 'required|min:0|integer_with_max',
            'date_time' => 'required|string_with_max',
            'show_status' => 'required|in:1,2'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'laboratory_id.required' => trans('core.base.field.required'),
            'laboratory_id.*' => trans('core.loading.invalid_data'),

            'indicator_id.required' => trans('core.base.field.required'),
            'indicator_id.*' => trans('core.loading.invalid_data'),

            'price.required' => trans('core.base.field.required'),
            'price.*' => trans('core.loading.invalid_data'),

            'duration.required' => trans('core.base.field.required'),
            'duration.*' => trans('core.loading.invalid_data'),

            'date_time.required' => trans('core.base.field.required'),
            'date_time.*' => trans('core.loading.invalid_data'),
        ];
    }
}
