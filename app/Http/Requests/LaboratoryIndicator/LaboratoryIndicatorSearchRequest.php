<?php

namespace App\Http\Requests\LaboratoryIndicator;

use App\Http\Requests\Request;

/**
 * Class LaboratoryIndicatorSearchRequest
 * @package App\Http\Requests\LaboratoryIndicator
 */
class LaboratoryIndicatorSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f.id' => 'integer_with_max',
            'f.laboratory_id' => 'integer_with_max|exists:laboratory,id',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
