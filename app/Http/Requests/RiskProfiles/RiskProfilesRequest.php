<?php

namespace App\Http\Requests\RiskProfiles;

use App\Http\Requests\Request;
use App\Models\BaseModel;
use App\Models\RiskProfile\RiskProfile;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

/**
 * Class RiskProfilesRequest
 * @package App\Http\RiskProfilesRequest
 */
class RiskProfilesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $activeDateFrom = currentDate();
        $id = $this->request->get('id') ?? null;

        if(!empty($this->request->get('id'))){
            $riskInstruction = RiskProfile::select('active_date_from')->where('id', $id)->firstOrFail();

            if(!is_null($riskInstruction)){
                $activeDateFrom = Carbon::parse($riskInstruction->getOriginal('active_date_from'))->format(config('swis.date_format'));
            }
        }

        // field name = code
        $rules = [
            'document_id'       => 'required|exists:constructor_documents,id',
            'name'              => "required|string|min:1|max:255|unique_with_agency:risk_profile,name,$id",
            'weight'            => "required|integer|between:0,999999",
            'frequency'         => 'required|integer|between:0,999999',
            'condition'         => 'required|string',
            'active_date_from'  => 'required|date|after_or_equal:' . $activeDateFrom,
            'active_date_to'    => 'nullable|date|after_or_equal:active_date_from',
            'instructions'      => 'required|min:1',
            'show_status'       => 'required',
            'risk_run_type'     => 'required',
        ];

        $exists = RiskProfile::where('name', $this->input('name'))->where('company_tax_id', Auth::user()->companyTaxId())->where('id', '!=', $this->request->get('id'))->active()->exists();

        if (!empty($this->request->get('id')) && $this->input('show_status') == BaseModel::STATUS_ACTIVE && $exists) {
            $rules['name'] = 'required|string|min:1|max:255|unique_with_agency_return';
        }

        foreach ($this->request->get('ml') as $key => $ml) {
            $rules["ml.{$key}.description"] = 'nullable|max:4000|string';
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        $activeDateFrom = currentDate();

        if (!empty($this->request->get('id'))) {
            $id = $this->request->get('id');
            $riskProfile = RiskProfile::select('active_date_from')->where('id', $id)->firstOrFail();

            if (!is_null($riskProfile)) {
                $activeDateFrom = Carbon::parse($riskProfile->getOriginal('active_date_from'))->format(config('swis.date_format'));
            }
        }

        $messages = [
            'document_id.required'  => trans('core.base.field.required'),

            'name.required'         => trans('core.base.field.required'),
            'name.max'              => trans('core.base.field.max_characters',['max' => 255]),
            'name.min'              => trans('core.base.field.min_characters',['min' => 1]),

            'weight.required'       => trans('core.base.field.required'),
            'weight.min'            => trans('core.base.field.min_characters',['min' => 1]),

            'frequency.between'     => trans('core.base.field.between'),
            'frequency.required'    => trans('core.base.field.required'),
            'frequency.min'         => trans('core.base.field.min_characters',['min' => 1]),

            'condition.required'    => trans('core.base.field.required'),

            'active_date_from.required' => trans('core.base.field.required'),
            'active_date_from.after_or_equal' => trans('core.base.field.start_date', ['start' => formattedDate($activeDateFrom)]),

            'active_date_from.*'    => trans('core.loading.invalid_data'),
            'active_date_to.*'      => trans('core.loading.invalid_data'),

            'name.unique_with_agency_return' => trans('validation.unique_with_agency')
        ];

        foreach ($this->request->get('ml') as $key => $ml) {
            $messages["ml.{$key}.description.max"]   = trans('core.base.field.max_characters', ['max' => 500]);
        }

        return $messages;
    }
}
