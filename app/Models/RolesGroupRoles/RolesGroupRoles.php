<?php

namespace App\Models\RolesGroupRoles;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

/**
 * Class RolesGroupRoles
 * @package App\Models\RolesGroupRoles
 */
class RolesGroupRoles extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['roles_group_id', 'role_id'];

    /**
     * @var string
     */
    public $incrementing = 'false';

    /**
     * @var string
     */
    public $table = 'roles_group_roles';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'role_id',
        'roles_group_id',
    ];


}
