<?php

namespace App\Models\OnlineTransactions;

use Illuminate\Support\Facades\DB;

/**
 * Class OnlineTransactionsManager
 * @package App\Models\OnlineTransactions
 */
class OnlineTransactionsManager
{
    /**
     * Function to store data in db
     *
     * @param array $data
     * @return OnlineTransactions
     */
    public function store(array $data)
    {
        $onlineTransactions = new OnlineTransactions($data);

        DB::transaction(function () use ($onlineTransactions) {
            $onlineTransactions->save();
        });

        return $onlineTransactions;
    }
}
