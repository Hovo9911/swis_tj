<?php

namespace App\Models\OnlineTransactions;

use App\Models\BaseModel;
use App\Models\PaymentProviders\PaymentProviders;
use Carbon\Carbon;
use DateTime;

/**
 * Class OnlineTransactions
 * @package App\Models\OnlineTransactions
 */
class OnlineTransactions extends BaseModel
{
    /**
     * @var string
     */
    const STATUS_OK = 'ok';
    const STATUS_PENDING = 'pending';
    const STATUS_FAILED = 'failed';
    const STATUS_NOT_FOUND = 'not_found';

    /**
     *
     */
    const ALIF_CARD = 'alif';

    /**
     * @var string
     */
    public $table = 'online_transactions';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'company_tax_id',
        'type',
        'transaction_id',
        'token',
        'amount',
        'status'
    ];

    /**
     * Function to get updated_at value
     *
     * @param $value
     * @return string
     */
    public function getUpdatedAtAttribute($value)
    {
        return Carbon::parse($value)->format(config('swis.date_time_format_front'));
    }

    /**
     * Function to return pending online transactions
     *
     * @return OnlineTransactions
     */
    public static function getPendingOnlineTransactions()
    {
        $pendingOnlineTransactions = OnlineTransactions::select('id', 'user_id', 'company_tax_id', 'amount', 'token', 'updated_at')
            ->where(['type' => OnlineTransactions::ALIF_CARD, 'status' => OnlineTransactions::STATUS_PENDING]);

        $paymentCheckTime = config('payment.alif.payment_check_time');

        $date = new DateTime;
        $date->modify("-{$paymentCheckTime} minutes");
        $formatted_date = $date->format(config('swis.date_time_format'));

        $pendingOnlineTransactions = $pendingOnlineTransactions->where('updated_at', '<', $formatted_date)->where('created_at','>','2021-03-04');

        return $pendingOnlineTransactions->get();

    }
}
