<?php

namespace App\Models\UserManualsRoles;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

/**
 * Class UserManualsRoles
 * @package App\Models\UserManualsRoles
 */
class UserManualsRoles extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['user_manual_id', 'role_id'];

    /**
     * @var string
     */
    public $incrementing = 'false';

    /**
     * @var string
     */
    const USER_MANUAL_ROLE_TYPE_APPLICANT = 1;
    const USER_MANUAL_ROLE_TYPE_AGENCY = 2;
    const USER_MANUAL_ROLE_TYPE_ADMIN = 3;
    const USER_MANUAL_ROLE_TYPE_NATURAL_PERSON = 4;
    const USER_MANUAL_ROLE_TYPE_CUSTOMS_PORTAL = 5;

    /**
     * @var array
     */
    const USER_MANUAL_ROLE_TYPES = [
        self::USER_MANUAL_ROLE_TYPE_APPLICANT,
        self::USER_MANUAL_ROLE_TYPE_AGENCY,
        self::USER_MANUAL_ROLE_TYPE_ADMIN,
        self::USER_MANUAL_ROLE_TYPE_NATURAL_PERSON,
        self::USER_MANUAL_ROLE_TYPE_CUSTOMS_PORTAL
    ];

    /**
     * @var string
     */
    public $table = 'user_manuals_roles';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'user_manual_id',
        'role_id',
    ];
}
