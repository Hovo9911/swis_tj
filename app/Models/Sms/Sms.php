<?php

namespace App\Models\Sms;

use App\Models\BaseModel;

/**
 * Class Sms
 * @package App\Models\Sms
 */
class Sms extends BaseModel
{
    /**
     * @var string
     */
    const STATUS_PENDING = 'pending';
    const STATUS_SENT = 'sent';
    const STATUS_FAILED = 'failed';

    /**
     * @var string
     */
    public $table = 'sms';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'phone',
        'message',
        'status',
    ];
}
