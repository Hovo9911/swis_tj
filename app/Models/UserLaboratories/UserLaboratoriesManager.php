<?php

namespace App\Models\UserLaboratories;

use App\Models\BaseModel;
use App\Models\SingleApplicationExpertiseIndicators\SingleApplicationExpertiseIndicators;

/**
 * Class UserLaboratoriesManager
 * @package App\Models\UserLaboratories
 */
class UserLaboratoriesManager extends BaseModel
{
    /**
     * Function to Update SAF->Indicators data
     *
     * @param array $data
     */
    public function updateIndicators(array $data)
    {
        $indicatorsData = $data['data'];

        foreach ($indicatorsData as $key => $data) {
            $indicator = SingleApplicationExpertiseIndicators::where('id', $key)->firstOrFail();
            $indicator->update($data);
        }
    }
}
