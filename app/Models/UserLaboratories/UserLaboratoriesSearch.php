<?php

namespace App\Models\UserLaboratories;

use App\Http\Controllers\Core\DataTableSearch;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplicationExpertise\SingleApplicationExpertise;
use App\Models\SingleApplicationExpertiseIndicators\SingleApplicationExpertiseIndicators;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class AgencySearch
 * @package App\Models\Agency
 */
class UserLaboratoriesSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);

        $expertise = $query->get();

        $refSamplers = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_LABORATORY_SAMPLERS);

        foreach ($expertise as $key => $value) {

            $expertiseIndicator = SingleApplicationExpertiseIndicators::select('actual_result', 'adequacy', 'status_send')->where('saf_expertise_id', $value->id)->first();

            if (!is_null($expertiseIndicator)) {
                $value->has_indicators = 0;
                if (!is_null($expertiseIndicator->actual_result) && !is_null($expertiseIndicator->adequacy)) {
                    $value->has_indicators = 1;
                }
                $value->send_indicators = 0;
                if ($expertiseIndicator->status_send) {
                    $value->send_indicators = 1;
                }
            }

            // Sampler
            if (is_array($value->sampler_code)) {
                $refSampler = getReferenceRowsWhereIn(ReferenceTable::REFERENCE_LABORATORY_SAMPLERS, $value->sampler_code);

                $samplers = [];
                if ($refSampler->count()) {
                    $samplers = $refSampler->pluck('name')->all();
                }

                $samplersNames = implode(', ', $samplers);

            } else {

                if (isset($refSamplers[$value->sampler_code])) {
                    $samplersNames = $refSamplers[$value->sampler_code]['name'];
                }
            }

            $value->sampler = $samplersNames ?? '';

        }

        return $expertise;
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $orderByType = 'desc';
        if ($this->orderByType == 'asc') {
            $orderByType = 'asc';
        }

        $query->orderBy($this->orderByCol ?? 'created_at', $orderByType);
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $labId = request()->route('id');

        $query = SingleApplicationExpertise::select('saf_expertise.id', 'saf_expertise.created_at', 'saf_expertise.return_date', 'saf_expertise.sampler_code', 'saf_expertise.sample_code', 'saf_expertise.sampling_weight', 'saf_expertise.saf_product_id', 'saf_sub_applications.agency_id', DB::raw("CONCAT('(', agency.tax_id , ') ', agency_ml.name) AS agency_name"))->where(['saf_expertise.laboratory_id' => $labId, 'saf_expertise.company_tax_id' => Auth::user()->companyTaxId()])
            ->join('saf_sub_applications', 'saf_expertise.sub_application_id', '=', 'saf_sub_applications.id')
            ->join('agency', 'saf_sub_applications.agency_id', '=', 'agency.id')
            ->join('agency_ml', 'agency.id', '=', 'agency_ml.agency_id')
            ->where('agency_ml.lng_id', '=', cLng('id'))
            ->where('send_to_lab', SingleApplicationExpertise::STATUS_ACTIVE);

        if (isset($this->searchData['sample_code'])) {
            $query->where('saf_expertise.sample_code', 'ILIKE', '%' . strtolower(trim($this->searchData['sample_code'])) . '%');
        }

        if (isset($this->searchData['saf_product_id'])) {
            $query->where(DB::raw('saf_expertise.saf_product_id::varchar'), 'ILIKE', '%' . strtolower(trim($this->searchData['saf_product_id'])) . '%');
        }

        if (isset($this->searchData['sampler'])) {
            $query->where(DB::raw('saf_expertise.sampler_code'), 'ILIKE', '%' . strtolower(trim($this->searchData['sampler'])) . '%');
        }

        if (isset($this->searchData['created_at_date_start'])) {
            $query->where('saf_expertise.created_at', '>=', trim($this->searchData['created_at_date_start']));
        }

        if (isset($this->searchData['created_at_date_end'])) {
            $query->where('saf_expertise.created_at', '<=', trim($this->searchData['created_at_date_end']));
        }

        if (isset($this->searchData['return_date_date_start'])) {
            $query->where('saf_expertise.return_date', '>=', trim($this->searchData['return_date_date_start']));
        }

        if (isset($this->searchData['return_date_date_end'])) {
            $query->where('saf_expertise.return_date', '<=', trim($this->searchData['return_date_date_end']));
        }

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}