<?php

namespace App\Models\ConstructorDocument;

use App\Models\Agency\Agency;
use App\Models\BaseModel;
use App\Models\ConstructorActions\ConstructorActions;
use App\Models\ConstructorDocumentsRoles\ConstructorDocumentsRoles;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\Role\Role;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Auth;

/**
 * Class Constructor
 * @package App\Models\Constructor
 */
class ConstructorDocument extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'constructor_documents';

    /**
     * @var bool
     */
    protected $hasAdminInfo = true;

    /**
     * @var int
     */
    const NEED_DIGITAL_SIGNATURE = 1;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'document_code',
        'company_tax_id',
        'document_type_id',
        'start_date',
        'end_date',
        'version',
        'environment',
        'digital_signature',
        'show_status',
        'expertise_type',
        'is_lab_flow'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'created_admin_id',
        'created_admin_ip',
        'updated_at',
        'updated_admin_id',
        'updated_admin_ip',
        'deleted_admin_id',
        'deleted_admin_ip'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'expertise_type' => 'array'
    ];

    /**
     * Function to get active languages
     *
     * @return HasMany
     */
    public function ml()
    {
        return $this->hasMany(ConstructorDocumentMl::class, 'constructor_documents_id', 'id');
    }

    /**
     * Function to get current language
     *
     * @return HasOne
     */
    public function current()
    {
        return $this->hasOne(ConstructorDocumentMl::class, 'constructor_documents_id', 'id')->notDeleted()->where('lng_id', cLng('id'))->first();
    }

    /**
     * Function to get current language
     *
     * @return HasOne
     */
    public function currentMl()
    {
        return $this->hasOne(ConstructorDocumentMl::class, 'constructor_documents_id', 'id')->notDeleted()->where('lng_id', cLng('id'));
    }

    /**
     * Function to relate with agency
     *
     * @return HasOne
     */
    public function agency()
    {
        return $this->hasOne(Agency::class, 'tax_id', 'company_tax_id');
    }

    /**
     * Function to relate with states
     *
     * @return HasMany
     */
    public function states()
    {
        return $this->hasMany(ConstructorStates::class, 'constructor_document_id', 'id')->active();
    }

    /**
     * Function to relate with role
     *
     * @return HasMany
     */
    public function roles()
    {
        return $this->hasMany(ConstructorDocumentsRoles::class, 'document_id', 'id')->whereNotIn('role_id', Role::DEFAULT_ROLES_ALL);
    }

    /**
     * Function to relate with actions
     *
     * @return HasMany
     */
    public function actions()
    {
        return $this->hasMany(ConstructorActions::class, 'constructor_document_id', 'id')->active();
    }

    /**
     * Function to check auth user has role
     *
     * @param $query
     * @return Builder
     */
    public function scopeConstructorDocumentOwner($query)
    {
        $companyTaxId = -1;
        if (Auth::user()->isAgency()) {
            $companyTaxId = Auth()->user()->companyTaxId();
        }

        return $query->where($this->getTable() . '.company_tax_id', $companyTaxId);
    }

    /**
     * Function to return document list for admin and agency
     *
     * @param false $companyTaxId
     * @param array $constructorDocumentIds
     * @return ConstructorDocument
     */
    public static function getConstructorDocumentForAgency($companyTaxId = false, $constructorDocumentIds = [])
    {
        $constructorDocuments = self::select('id', 'document_name', 'document_code')
            ->joinMl()
            ->active();

        if ($companyTaxId) {
            $constructorDocuments->where('constructor_documents.company_tax_id', $companyTaxId);
        } else {
            $constructorDocuments->constructorDocumentOwner();
        }

        if (count($constructorDocumentIds)) {
            $constructorDocuments->whereIn('id', $constructorDocumentIds);
        }

        return $constructorDocuments->orderBy('document_name')->get();
    }

    /**
     * Function to return all document list
     *
     * @param array $ids
     * @param bool $withLab
     * @return ConstructorDocument
     */
    public static function getConstructorDocuments(array $ids = [], $withLab = true)
    {
        $constructorDocs = self::select('id', 'document_name', 'document_code')
            ->joinMl()
            ->orderBy('constructor_documents_ml.document_name')
            ->active();

        if (count($ids)) {
            $constructorDocs->whereIn('id', $ids);
        }

        if (!$withLab) {
            $constructorDocs->where('is_lab_flow', false);
        }

        return $constructorDocs->get();
    }

    /**
     * Function to get all constructor docs
     *
     * @return ConstructorDocument
     */
    public static function getAll()
    {
        return self::select('id', 'document_type_id', 'company_tax_id', 'document_code', 'document_name')
            ->joinMl()
            ->get()
            ->keyBy('id');
    }
}
