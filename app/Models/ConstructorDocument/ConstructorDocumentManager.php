<?php

namespace App\Models\ConstructorDocument;

use App\Models\BaseMlManager;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\ConstructorStates\ConstructorStatesMl;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class ConstructorManager
 * @package App\Models\Constructor
 */
class ConstructorDocumentManager
{
    use BaseMlManager;

    /**
     * Function to Store data to DB
     *
     * @param array $data
     * @return ConstructorDocument
     */
    public function store(array $data)
    {
        $data['user_id'] = Auth::id();
        $data['company_tax_id'] = Auth::user()->companyTaxId();
        $data['digital_signature'] = $data['digital_signature'] ?? 0;
        $stateData = $this->getStateMlArray($data);

        $constructor = new ConstructorDocument($data);
        $constructorMl = new ConstructorDocumentMl();
        $constructorState = new ConstructorStates();
        $constructorStateMl = new ConstructorStatesMl();

        return DB::transaction(function () use ($data, $constructor, $constructorMl, $constructorState, $constructorStateMl, $stateData) {

            // save document to DB
            $constructor->save();
            $this->storeMl($data['ml'], $constructor, $constructorMl);

            // save start state to DB `constructor state`
            $stateData['constructor_document_id'] = $constructor->id;
            $constructorState->fill($stateData);
            $constructorState->save();

            $this->storeMl($stateData['ml'], $constructorState, $constructorStateMl);

            return $constructor;
        });
    }

    /**
     * Function to Update data
     *
     * @param array $data
     * @param int $id
     */
    public function update(array $data, int $id)
    {
        $constructor = ConstructorDocument::where('id', $id)->constructorDocumentOwner()->firstOrFail();
        $constructorMl = new ConstructorDocumentMl();

        $data['digital_signature'] = $data['digital_signature'] ?? 0;
        $data['expertise_type'] = $data['expertise_type'] ?? [];

        DB::transaction(function () use ($data, $constructor, $constructorMl) {
            $constructor->update($data);
            $this->updateMl($data['ml'], 'constructor_documents_id', $constructor, $constructorMl);
        });
    }

    /**
     * Function to get state ml array
     *
     * @param $data
     * @return array
     */
    private function getStateMlArray($data)
    {
        //set values for state
        $stateData['state_id'] = generateID(10);
        $stateData['state_type'] = 'start';
        $stateData['user_id'] = $data['user_id'];
        $languages = activeLanguages();

        foreach ($languages as $lang) {
            $stateData['ml'][$lang->id] = [
                'state_name' => 'submitted',
                'description' => 'Single application form is submitted'
            ];
        }

        return $stateData;
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @param null $id2
     * @return array
     */
    public function getLogData($id, $id2 = null)
    {
        $data = ConstructorDocument::where('id', $id)->first()->toArray();
        $data['mls'] = ConstructorDocumentMl::where('constructor_documents_id', $id)->get()->toArray();

        return $data;
    }

}
