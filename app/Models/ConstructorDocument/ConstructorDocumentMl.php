<?php

namespace App\Models\ConstructorDocument;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

/**
 * Class ConstructorMl
 * @package App\Models\Constructor
 */
class ConstructorDocumentMl extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['constructor_documents_id', 'lng_id'];

    /**
     * @var string
     */
    public $incrementing = 'false';

    /**
     * @var string
     */
    public $table = 'constructor_documents_ml';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'constructor_documents_id',
        'lng_id',
        'document_name',
        'description',
    ];
}
