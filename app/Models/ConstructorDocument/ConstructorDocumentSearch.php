<?php

namespace App\Models\ConstructorDocument;

use App\Http\Controllers\Core\DataTableSearch;
use Illuminate\Support\Facades\DB;

/**
 * Class ConstructorDocumentSearch
 * @package App\Models\ConstructorDocument
 */
class ConstructorDocumentSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        return $query->get();
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($this->orderByCol, $this->orderByType ?? 'desc');
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $query = ConstructorDocument::select(
            'constructor_documents.id',
            'constructor_documents_ml.document_name',
            'constructor_documents.document_code',
            'constructor_documents.start_date',
            'constructor_documents.version',
            'reference_document_type_reference_ml.name as document_type'
        )
            ->joinMl()
            ->leftJoin('reference_document_type_reference', 'constructor_documents.document_type_id', '=', 'reference_document_type_reference.id')
            ->leftJoin('reference_document_type_reference_ml', function ($query) {
                $query->on('reference_document_type_reference.id', '=', 'reference_document_type_reference_ml.reference_document_type_reference_id')
                    ->where('reference_document_type_reference_ml.lng_id', '=', cLng('id'));
            })
            ->notDeleted();

        if (isset($this->searchData['id'])) {
            $query->where(DB::raw('constructor_documents.id::varchar'), $this->searchData['id']);
        }

        $query->constructorDocumentOwner();

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}
