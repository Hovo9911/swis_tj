<?php

namespace App\Models\ReportsAccess;

use App\Models\BaseModel;
use App\Models\ReportsAccessAgencies\ReportsAccessAgencies;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class ReportsAccess
 * @package App\Models\ReportsAccess
 */
class ReportsAccess extends BaseModel
{
    /**
     * @var string
     */
    const ROLE_TYPE_SW_ADMIN = '1';
    const ROLE_TYPE_AGENCY = '2';

    /**
     * @var string
     */
    public $table = 'reports_access';

    /**
     * @var array
     */
    protected $fillable = [
        'role_type',
        'jasper_report_label',
        'jasper_report_uri',
        'show_status'
    ];

    /**
     * Function to relate with ReportAccessAgencies
     *
     * @return HasMany
     */
    public function reportsAccessAgencies()
    {
        return $this->hasMany(ReportsAccessAgencies::class, 'report_id', 'id');
    }
}