<?php

namespace App\Models\ReportsAccess;

use App\Models\Reports\Reports;
use App\Models\ReportsAccessAgencies\ReportsAccessAgencies;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\DB;

/**
 * Class ReportsAccessManager
 * @package App\Models\ReportsAccess
 */
class ReportsAccessManager
{
    /**
     * Function to Store data to DB
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $jasperReportInfo = json_decode($data['jasper_report_info'], true);

        $data['jasper_report_label'] = $jasperReportInfo['label'] ?? '';
        $data['jasper_report_uri'] = $jasperReportInfo['uri'] ?? '';

        $reportsAccess = new ReportsAccess($data);
        $reportsAccessAgencyData = [];

        if (isset($data['role_type']) && isset($data['agencies']) && $data['role_type'] == ReportsAccess::ROLE_TYPE_AGENCY) {
            foreach ($data['agencies'] as $agency) {
                $agencyData['company_tax_id'] = $agency;
                $reportsAccessAgencyData[] = new ReportsAccessAgencies($agencyData);
            }
        }

        DB::transaction(function () use ($data, $reportsAccess, $reportsAccessAgencyData) {
            $reportsAccess->save();

            if (count($reportsAccessAgencyData) > 0) {
                $reportsAccess->reportsAccessAgencies()->saveMany($reportsAccessAgencyData);
            }

        });
    }

    /**
     * Function to Update data
     *
     * @param array $data
     * @param $id
     */
    public function update(array $data, $id)
    {
        $reportsAccess = ReportsAccess::where('id', $id)->firstOrFail();

        $reportsAgenciesData = [];

        if (isset($data['agencies'])) {
            foreach ($data['agencies'] as $agency) {
                $agenciesData['company_tax_id'] = $agency;
                $reportsAgenciesData[] = new ReportsAccessAgencies($agenciesData);
            }
        }

        DB::transaction(function () use ($data, $reportsAccess, $reportsAgenciesData, $id) {
            $reportsAccess->update($data);

            if ($data['role_type'] == ReportsAccess::ROLE_TYPE_SW_ADMIN) {
                ReportsAccessAgencies::where('report_id', $id)->delete();
            }

            if (isset($data['agencies']) && $data['role_type'] != ReportsAccess::ROLE_TYPE_SW_ADMIN) {
                ReportsAccessAgencies::where('report_id', $id)->delete();
                $reportsAccess->reportsAccessAgencies()->saveMany($reportsAgenciesData);
            }
        });
    }

    /**
     * Function to Delete data
     *
     * @param array $deleteItems
     */
    public function delete(array $deleteItems)
    {
        DB::transaction(function () use ($deleteItems) {

            ReportsAccess::whereIn('id', $deleteItems)->update([
                'show_status' => ReportsAccess::STATUS_DELETED
            ]);

            ReportsAccessAgencies::whereIn('report_id', $deleteItems)->delete();
        });
    }

    /**
     * Function to get Jasper reports
     *
     * @return array
     * @throws GuzzleException
     */
    public function getJasperReports()
    {
        try {
            $client = new Client();

            $res = $client->request('GET', config('services.jasper_reports.url'), [
                'auth' => [config('services.jasper_reports.username'), config('services.jasper_reports.password')]
            ]);

            $response = $res->getBody()->getContents();
            $xml = str_replace("windows-1251", "utf-8", $response);
            $result = simplexml_load_string($xml);

            $jasperReport = [];

            $usedReports = ReportsAccess::active()->pluck('jasper_report_uri')->all();

            foreach ($result->resourceLookup as $resource) {
                if (optional($resource)->resourceType == Reports::JASPER_REPORT_UNIT && !(in_array((string)$resource->uri, $usedReports))) {
                    $jasperReport[(string)$resource->uri] = (string)$resource->label;
                }
            }

            return $jasperReport;

        } catch (Exception $e) {
//            echo $e;
            return [];
        }
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @param $id2
     * @return array
     */
    public function getLogData($id, $id2 = null)
    {
        return ReportsAccess::where('id', $id)->first()->toArray();
    }
}
