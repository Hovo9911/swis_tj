<?php

namespace App\Models\ReportsAccess;

use App\Http\Controllers\Core\DataTableSearch;
use App\Models\ReportsAccessRoles\ReportsAccessRoles;
use Illuminate\Support\Facades\DB;

/**
 * Class RegionalOfficerSearch
 * @package App\Models\RegionalOffice
 */
class ReportsAccessSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        return $query->get();
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $orderBy = $this->orderByCol ?? 'id';
        $query->orderBy($orderBy, $this->orderByType ?? 'desc');
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $query = ReportsAccess::active();

        if (isset($this->searchData['id'])) {
            $query->where(DB::raw('id::varchar'), $this->searchData['id']);
        }

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}
