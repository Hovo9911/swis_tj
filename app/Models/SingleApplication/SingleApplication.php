<?php

namespace App\Models\SingleApplication;

use App\Models\BaseModel;
use App\Models\DataModel\DataModel;
use App\Models\Document\Document;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\Routing\Routing;
use App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure;
use App\Models\SingleApplicationObligations\SingleApplicationObligations;
use App\Models\SingleApplicationProducts\SingleApplicationProducts;
use App\Models\SingleApplicationSubApplicationProducts\SingleApplicationSubApplicationProducts;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use App\Models\User\User;
use App\Models\UserDocuments\UserDocuments;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class SingleApplication
 * @package App\Models\SingleApplication
 */
class SingleApplication extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'saf';

    /**
     * @var string (XML Modules)
     */
    const XML_MODULE_GENERAL = 'general';
    const XML_MODULE_PRODUCTS = 'products';
    const XML_MODULE_PRODUCTS_BATCH = 'products_batch';
    const XML_MODULE_SIDES = 'sides';
    const XML_MODULE_TRANSPORTATION = 'transportation';
    const XML_MODULE_SUB_APPLICATIONS = 'subtitles';
    const XML_MODULE_ATTACHED_DOCUMENTS = 'attached_documents';

    /**
     * @var array
     */
    const XML_MODULES = [self::XML_MODULE_GENERAL, self::XML_MODULE_SIDES, self::XML_MODULE_TRANSPORTATION, self::XML_MODULE_PRODUCTS];

    /**
     * @var string (Saf Status Types)
     */
    const DRAFT_TYPE = 'draft';
    const SEND_TYPE = 'send';
    const CREATED_TYPE = 'created';
    const VOID_TYPE = 'void';

    /**
     * @var string (verified only for request not in db)
     */
    const VERIFIED_TYPE = 'verify';

    /**
     * @var string
     */
    const REGIME_IMPORT = 'import';
    const REGIME_EXPORT = 'export';
    const REGIME_TRANSIT = 'transit';

    /**
     * @var array
     */
    const REGIMES = [self::REGIME_IMPORT, self::REGIME_EXPORT, self::REGIME_TRANSIT];

    /**
     * @var string
     */
    const SAF_AGENCY_SUBDIVISION_ID = 'SAF_ID_142';
    const SAF_PLACES_TYPE_ID = 'SAF_ID_97';

    /**
     * @var bool
     */
    protected $hasAdminInfo = true;

    /**
     * @var array
     */
    protected $fillable = [
        'company_tax_id',
        'regime',
        'saf_data_structure_id',
        'regular_number',
        'user_id',
        'creator_user_id',
        'importer_type',
        'importer_value',
        'exporter_type',
        'exporter_value',
        'applicant_type',
        'applicant_value',
        'data',
        'saf_controlled_fields',
        'custom_data',
        'status_type',
        'show_status',
        'need_reform',
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'created_admin_id',
        'created_admin_ip',
        'updated_at',
        'updated_admin_id',
        'updated_admin_ip',
        'deleted_admin_id',
        'deleted_admin_ip'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'data' => 'array',
        'custom_data' => 'array',
        'saf_controlled_fields' => 'array',
    ];

    /**
     * Function to get created_at value
     *
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format(config('swis.date_time_format_front'));
    }

    /**
     * Function to relate with User
     *
     * @return HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * Function to relate with SingleApplicationProducts
     *
     * @return HasMany
     */
    public function productList()
    {
        return $this->hasMany(SingleApplicationProducts::class, 'saf_number', 'regular_number')->orderByAsc();
    }

    /**
     * Function to relate with SingleApplicationSubApplications
     *
     * @return HasMany
     */
    public function subApplications()
    {
        return $this->hasMany(SingleApplicationSubApplications::class, 'saf_number', 'regular_number')->orderByAsc();
    }

    /**
     * Function to relate with SingleApplicationSubApplications
     *
     * @return HasMany
     */
    public function formedSubApplications()
    {
        return $this->hasMany(SingleApplicationSubApplications::class, 'saf_number', 'regular_number')->formedSubApplication()->orderByAsc();
    }

    /**
     * Function to get sub application fixed type obligation(E0_59)
     *
     * @return HasMany
     */
    public function subApplicationsFixedObligations()
    {
        return $this->hasMany(SingleApplicationObligations::class, 'saf_number', 'regular_number')->where(['saf_obligations.obligation_code' => SingleApplicationObligations::SAF_OBLIGATIONS_FIX_CODE_FOR_SEND_APPLICATION_OBLIGATION]);
    }

    /**
     * Function to check for owner/creator
     *
     * @param $query
     * @return Builder
     */
    public function scopeSafOwnerOrCreator($query)
    {
        $userType = Auth::user()->currentUserType();
        $userTypeValue = Auth::user()->currentUserTypeValue();
        $showOthersData = Auth::user()->userShowOthersData();

        $query->where(function ($q) use ($userType, $userTypeValue) {
            $q->where('saf.regime', SingleApplication::REGIME_IMPORT)->where('saf.importer_value', $userTypeValue)->where('saf.importer_type', $userType)
                ->orWhere('saf.regime', SingleApplication::REGIME_EXPORT)->where('saf.exporter_value', $userTypeValue)->where('saf.exporter_type', $userType)
                ->orWhere('saf.applicant_type', $userType)->where('saf.applicant_value', $userTypeValue);
        });

        // User login as company/agency
        if (Auth::user()->companyTaxId()) {

            if (!$showOthersData) {
                $query->where('saf.user_id', Auth::id());
            }

        }

        // User login as Authorized Natural Person
        if (Auth::user()->isAuthorizedNaturalPerson()) {

            if (!$showOthersData) {
                $query->where(['saf.creator_user_id' => Auth::id(), 'company_tax_id' => Document::FALSE]);
            }
        }

        return $query;
    }

    /**
     * Function to check Saf has Submitted SubApplication
     *
     * @return bool
     */
    public function hasSubmittedSubApplication()
    {
        $submittedSubApplications = SingleApplicationSubApplications::where('saf_number', $this->regular_number)->submittedSubApplication()->first();

        if (!is_null($submittedSubApplications)) {
            return true;
        }

        return false;
    }

    /**
     * Function to check SAf has Submitted or Requested SubApplications
     *
     * @return bool
     */
    public function hasSubmittedOrRequestedSubApplication()
    {
        $submittedOrRequestedSubApp = SingleApplicationSubApplications::select('id')->where('saf_number', $this->regular_number)->where(function ($q) {
            $q->submittedOrRequestedSubApplication();
        })->first();

        if (!is_null($submittedOrRequestedSubApp)) {
            return true;
        }

        return false;
    }

    /**
     * Function to get Saf Global Default Data
     *
     * @return array
     */
    public function getSafGlobalDefaultValue()
    {
        $creator = Auth::user()->getCreator();

        return [
            'global' => [
                'regularNumber' => $this->regular_number,
                'safDate' => Carbon::parse($this->getOriginal('created_at'))->format(config('swis.date_format')),
                'type' => $this->regime,
                'typeName' => trans('swis.saf.general.regime.' . $this->regime . '.title'),
            ],
            'user' => [
                'passport' => $creator->ssn ?? '',
                'name' => $creator->name ?? '',
                'address' => $creator->address ?? '',
                'phone_number' => $creator->phone_number ?? '',
                'email' => $creator->email ?? '',
            ]
        ];
    }

    /**
     * Function to get saf xml data for generating fields
     *
     * @return bool|string
     */
    public function xmlDataStructure()
    {
        $xmlDataId = $this->saf_data_structure_id;
        if (is_null($this->saf_data_structure_id)) {
            $saf = SingleApplication::select('saf_data_structure_id')->where(function ($q) {
                $q->where('id', $this->id)->orWhere('regular_number', $this->regular_number);
            })->first();

            $xmlDataId = $saf->saf_data_structure_id;
        }

        $xmlDataStructure = SingleApplicationDataStructure::where('id', $xmlDataId)->first();

        if (!is_null($xmlDataStructure)) {
            return $xmlDataStructure->xml;
        }

        return false;
    }

    /**
     * Function to get products Id Numbers
     *
     * @return array
     */
    public function productsOrderedList()
    {
        $availableProductsIds = $this->productList->pluck('id')->all();
        $availableProductsIdNumbers = [];
        $incNumber = 1;
        for ($i = 0; $i < count($availableProductsIds); $i++) {
            $availableProductsIdNumbers[$availableProductsIds[$i]] = $incNumber;
            $incNumber++;
        }

        return $availableProductsIdNumbers;
    }

    /**
     * Products that not formed sub applications
     *
     * @return array
     */
    public function productsCodesNotFormedSubApplication()
    {
        $safAllProducts = $this->productList->pluck('id')->unique()->all();

        $subApplicationProducts = SingleApplicationSubApplicationProducts::where('saf_number', $this->regular_number)->pluck('product_id')->unique()->all();

        $notFormedProducts = array_diff($safAllProducts, $subApplicationProducts);

        $productCodes = [];
        if (count($notFormedProducts)) {
            $productCodeIds = SingleApplicationProducts::whereIn('id', $notFormedProducts)->pluck('product_code');
            $productCodes = DB::table(ReferenceTable::REFERENCE_HS_CODE_6)->whereIn('id', $productCodeIds)->pluck('code')->all();
        }

        return $productCodes;
    }

    /**
     * Function to get Saf Hidden Elements
     *
     * @param $withModule
     * @return array
     */
    public static function getSafHiddenElements($withModule = false)
    {
        $self = (new self);
        $hiddenFields = [];
        $lastXML = SingleApplicationDataStructure::lastXml();
        $fields = $lastXML->xpath("/tabs/tab/block/fieldset/field[@showInView='true']");
        $fieldset = $lastXML->xpath("/tabs/tab/block/fieldset[@showInView='true']");
        $subBlocks = $lastXML->xpath("/tabs/tab/block/subBlock/fieldset[@showInView='true']");
        $subBlocksFields = $lastXML->xpath("/tabs/tab/block/subBlock/fieldset/field[@showInView='true']");

        foreach ($fields as $field) {
            $hiddenFields = $self->getHiddenFields($withModule, $field->attributes(), $lastXML, $hiddenFields);
        }

        foreach ($fieldset as $item) {
            $hiddenFields = $self->getHiddenFields($withModule, $item->field->attributes(), $lastXML, $hiddenFields);
        }

        foreach ($subBlocks as $subBlock) {
            $hiddenFields = $self->getHiddenFields($withModule, $subBlock->field->attributes(), $lastXML, $hiddenFields);
        }

        foreach ($subBlocksFields as $field) {
            $hiddenFields = $self->getHiddenFields($withModule, $field->attributes(), $lastXML, $hiddenFields);
        }

        return $hiddenFields;
    }

    /**
     * Function to get Saf Hidden Elements array
     *
     * @param $withModule
     * @param $attribute
     * @param $lastXML
     * @param $hiddenFields
     * @return array
     */
    private function getHiddenFields($withModule, $attribute, $lastXML, $hiddenFields)
    {
        if ($withModule) {
            // case of transportation tab multiple fields
            if (getSaFXmlFieldModuleName($attribute->id, $lastXML) == 'default' && !empty((string)$attribute->dataName)) {
                $hiddenFields[getSaFXmlFieldModuleName($attribute->id, $lastXML)][] = (string)$attribute->name;
            } else {
                $hiddenFields[getSaFXmlFieldModuleName($attribute->id, $lastXML)][] = !empty((string)$attribute->dataName) ? (string)$attribute->dataName : (string)$attribute->name;
            }
        } else {
            $hiddenFields[] = !empty((string)$attribute->dataName) ? (string)$attribute->dataName : (string)$attribute->name;
        }

        return $hiddenFields;
    }

    /**
     * Function to get all sfa and mdm data array (key => value)
     *
     * @param $applicationId
     * @return array
     */
    public static function getAllDataByKeyValue($applicationId)
    {
        $subApplication = SingleApplicationSubApplications::select('id', 'data', 'custom_data', 'saf_number')->find($applicationId);
        $subData = $subApplication->data;

        $safProducts = $subApplication->productListSelect->toArray();

        foreach ($safProducts as $item) {
            $subData['saf']['product']['products'][] = $item;
        }
        if (isset($subData['status_type'])) {
            unset($subData['status_type']);
        }
        if (isset($subData['form_is_changed'])) {
            unset($subData['form_is_changed']);
        }

        return getJsonDataAsArray($subData, $subApplication->custom_data, $safProducts);
    }

    /**
     * Function to get all saf controlled fields
     *
     * @param array $routingIds
     * @param bool $onlyProducts
     * @return array
     */
    public function getSafControlledFields($routingIds = [], $onlyProducts = false)
    {
        $visibleFields = [];
        $mandatoryFields = [];

        if ($this->status_type != self::CREATED_TYPE) {
            if (!count($routingIds)) {
                $routingIds = SingleApplicationSubApplications::where('saf_number', $this->regular_number)->whereNotNull('routings')->pluck('routings');

                $uniqueRoutingIds = [];
                foreach ($routingIds as $routingId) {
                    if (is_array($routingId)) {
                        $uniqueRoutingIds = array_merge($uniqueRoutingIds, $routingId);
                    } else {
                        $uniqueRoutingIds[] = $routingId;
                    }
                }
                $routingIds = array_unique($uniqueRoutingIds);
            }

            $safControlledFields = Routing::whereIn('id', $routingIds)->whereNotNull('saf_controlled_fields')->pluck('saf_controlled_fields');

            $safXml = $this->xmlDataStructure();
            foreach ($safControlledFields as $safControlledField) {
                foreach ($safControlledField as $safID => $value) {
                    if ($onlyProducts) {
                        if (!$this->isSafXmlFieldModuleisDocument($safID, $safXml)) {
                            continue;
                        }
                    } else {
                        if ($this->isSafXmlFieldModuleisDocument($safID, $safXml)) {
                            continue;
                        }
                    }

                    if (isset($value['make_visible'])) {
                        if (!in_array($safID, $visibleFields)) {
                            $visibleFields[] = $safID;
                        }
                    }

                    if (isset($value['make_mandatory'])) {
                        if (!in_array($safID, $mandatoryFields)) {
                            $mandatoryFields[] = $safID;
                        }
                    }
                }
            }
        }

        return [
            'visible_fields' => $visibleFields,
            'mandatory_fields' => $mandatoryFields,
        ];

    }

    /**
     * Function to saf product controlled field
     *
     * @param $productIds
     * @return array
     */
    public function getSafProductControlledFields($productIds)
    {
        $productIds = !is_array($productIds) ? (array)$productIds : $productIds;

        $subApplicationIds = SingleApplicationSubApplicationProducts::whereIn('product_id', $productIds)->where(['saf_number' => $this->regular_number])->pluck('subapplication_id');

        if ($subApplicationIds->count()) {

            $subApplications = SingleApplicationSubApplications::select('routing_products')->whereIn('id', $subApplicationIds)->get();

            $routings = [];
            foreach ($subApplications as $subApplication) {
                $routingProducts = $subApplication->routing_products;
                if (!is_null($routingProducts)) {
                    foreach ($routingProducts as $routingId => $products) {
                        if (count(array_intersect($productIds, $products)) !== 0) {
                            $routings[] = $routingId;
                        }
                    }
                }
            }

            if (count($routings)) {
                return $this->getSafControlledFields($routings, true);
            }
        }

        return [
            'visible_fields' => [],
            'mandatory_fields' => [],
        ];
    }

    /**
     * Function to for saf if has updated any column (product,document,document_product)
     *
     * @param $safNumber
     */
    public static function needReForm($safNumber)
    {
        $saf = SingleApplication::where('regular_number', $safNumber)->firstOrFail();

        $safHasNotSubmittedSubApplications = SingleApplicationSubApplications::where(['saf_number' => $safNumber])->where(function ($q) {
            $q->whereNull('current_status')->orWhere('current_status', SingleApplicationSubApplications::STATUS_REQUESTED);
        })->first();

        if (!is_null($safHasNotSubmittedSubApplications)) {
            $saf->update(['need_reform' => SingleApplication::TRUE]);
        }
    }

    /**
     * Function to return side company tax id and user id
     *
     * @param $data
     * @return array
     */
    public static function getSidesUser($data)
    {
        $userId = 0;
        $companyTaxId = $data['type_value'];

        $referenceLegalEntities = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_LEGAL_ENTITY_FORM_TABLE_NAME);

        if (isset($referenceLegalEntities[$data['type']]) && $referenceLegalEntities[$data['type']]['code'] == UserDocuments::ENTITY_TYPE_TAX_ID) {
            $companyTaxId = $data['type_value'];
        } elseif (isset($referenceLegalEntities[$data['type']]) && $referenceLegalEntities[$data['type']]['code'] == UserDocuments::ENTITY_TYPE_USER_ID) {
            $user = User::select('id')->where('ssn', $data['type_value'])->active()->first();
            $userId = !is_null($user) ? $user->id : 0;
            $companyTaxId = 0;
        }

        return [
            'userId' => $userId,
            'companyTaxId' => $companyTaxId,
        ];
    }

    /**
     * Function to get product saf id
     *
     * @param array $result
     * @return array
     */
    public static function getProductSafIds($result = [])
    {
        $lastXml = SingleApplicationDataStructure::lastXml();
        $xmlBlock = $lastXml->xpath('/tabs/tab[@key="products"]')[0]->block;

        foreach ($xmlBlock as $block) {
            foreach ($block->fieldset as $fieldset) {
                foreach ($fieldset as $field) {
                    $result[] = (string)$field->attributes()->id;
                }
            }
        }

        return $result;
    }

    /**
     * Function to check validation of required fields in saf fields and products
     *
     * @param $subApplicationId
     * @param bool $inSaf
     * @param bool $inProducts
     * @return array
     */
    public function getNotValidSafControlledField($subApplicationId, $inSaf = true, $inProducts = false)
    {
        $subApplication = SingleApplicationSubApplications::select('id', 'saf_number', 'routings')->where('id', $subApplicationId)->firstOrFail();
        $saf = SingleApplication::select('id', 'regular_number', 'regime', 'saf_data_structure_id', 'saf_controlled_fields', 'status_type')->where('regular_number', $subApplication->saf_number)->firstOrFail();

        $data = [];
        if ($inSaf) {
            $safStoredControlledFields = [];
            if (!is_null($saf->saf_controlled_fields)) {
                $safStoredControlledFields = prefixKey('', $saf->saf_controlled_fields);
            }

            $safControlledFields = $saf->getSafControlledFields($subApplication->routings);
            $safControlledRequiredFields = $safControlledFields['mandatory_fields'];

            $notSafFieldsNames = [];
            $safXml = $saf->xmlDataStructure();
            if (count($safControlledRequiredFields)) {
                foreach ($safControlledRequiredFields as $safFieldId) {

                    $xmlField = getSafXmlFieldByID($safFieldId, $safXml);

                    if (isset($xmlField[0])) {
                        $mdmFieldType = DataModel::where('id', (string)$xmlField[0]->attributes()->mdm)->pluck('type')->first();
                        $fieldName = getSafFieldCorrectName((string)$xmlField[0]->attributes()->name);


                        if ($mdmFieldType != DataModel::MDM_ELEMENT_TYPE_MULTIPLE) {
                            if (!isset($safStoredControlledFields[$fieldName])) {
                                $notSafFieldsNames[str_replace('.', '-', $fieldName)] = trans('core.base.field.required');
                            }
                        } else {
                            if (!isset($safStoredControlledFields[$fieldName . '.0'])) {
                                $notSafFieldsNames[str_replace('.', '-', $fieldName)] = trans('core.base.field.required');
                            }
                        }

                    }
                }
            }

            if (!$inProducts) {
                return $notSafFieldsNames;
            }

            $data['inSaf'] = $notSafFieldsNames;

        }

        if ($inProducts) {

            $notValidProducts = [];
            $lastXML = SingleApplicationDataStructure::lastXml();
            foreach ($subApplication->productList as $product) {
                $routingSafControlledFields = $saf->getSafProductControlledFields($product->id);

                $mandatoryFields = $routingSafControlledFields['mandatory_fields'];

                if ($mandatoryFields) {

                    $productControlledFieldsValue = $product->saf_controlled_fields;

                    foreach ($mandatoryFields as $mandatoryFieldId) {
                        $safField = getSafXmlFieldByID($mandatoryFieldId, $lastXML);

                        if (isset($safField[0])) {
                            $fieldNameXml = (string)$safField[0]->attributes()->name;

                            if (strpos($fieldNameXml, ']') !== false) {
                                preg_match('/(?<=\[)(.*?)(?=\])/', $fieldNameXml, $matches);
                            } else {
                                $matches[0] = $fieldNameXml;
                            }

                            if (isset($matches[0]) && empty($productControlledFieldsValue[$matches[0]])) {
                                $notValidProducts[] = $product->id;
                                continue 2;
                            }
                        }
                    }
                }
            }

            if (!$inSaf) {
                return $notValidProducts;
            }

            $data['inProducts'] = $notValidProducts;
        }

        return $data;
    }


    /**
     * Function to get from xml what saf controlled fields SAF_IDS
     *
     * @param bool $inProducts
     * @return array
     */
    public function getSafControlledDefaultVisibleFields($inProducts = false)
    {
        $xml = $this->xmlDataStructure();

        if ($inProducts) {
            $xmlFields = $xml->xpath('/tabs/tab/block[@renderTable="productList"]/fieldset/field[@safControlledField="true" and not(@hideOnSaf)]');
        } else {
            $xmlFields = $xml->xpath('/tabs/tab/block[not(@renderTable="productList")]/fieldset/field[@safControlledField="true" and not(@hideOnSaf)]');
        }

        $defaultVisibleSafControlledFields = [];
        foreach ($xmlFields as $field) {
            $defaultVisibleSafControlledFields[] = (string)$field->attributes()->id;
        }

        return $defaultVisibleSafControlledFields;
    }

    /**
     * Function to check field module is Document
     *
     * @param $id
     * @param bool $lastXML
     * @return bool
     */
    public function isSafXmlFieldModuleIsDocument($id, $lastXML = false)
    {
        if (!$lastXML) {
            $lastXML = SingleApplicationDataStructure::lastXml();
        }

        $module = $lastXML->xpath("//field[@id='" . $id . "']/ancestor::tab");
        $block = $lastXML->xpath("//field[@id='" . $id . "']/ancestor::block");

        if (isset($module[0]) && (string)$module[0]->attributes()->key == SingleApplication::XML_MODULE_PRODUCTS && isset($block[0]) && (string)$block[0]->attributes()->type == 'subForm') {
            return true;
        }

        return false;
    }
}
