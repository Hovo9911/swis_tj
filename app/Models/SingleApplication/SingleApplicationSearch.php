<?php

namespace App\Models\SingleApplication;

use App\Http\Controllers\Core\DataTableSearch;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class SingleApplicationSearch
 * @package App\Models\SingleApplication
 */
class SingleApplicationSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);

        $data = [];
        $result = $query->get();
        $countryRef = ReferenceTable::REFERENCE_COUNTRIES;
        $refCountries = getReferenceRowsKeyValue($countryRef);
        $refCountriesByCode = getReferenceRowsKeyValue($countryRef, 'code');

        foreach ($result as $value) {

            $safData = $value->data['saf'];

            $importCountry = null;
            $exportCountry = null;
            $exportCountryName = $importCountryName = $exporter = $importer = '';

            if (!empty($safData['transportation']['import_country'])) {

                if (isset($refCountries[$safData['transportation']['import_country']])) {

                    $importCountry = (object)$refCountries[$safData['transportation']['import_country']];

                    if (!$value->hasSubmittedSubApplication()) {
                        if (isset($refCountriesByCode[$importCountry->code])) {
                            $importCountry = (object)$refCountriesByCode[$importCountry->code];
                        }
                    }

                    if (!is_null($importCountry)) {
                        $importCountryName = '(' . $importCountry->code . ') ' . $importCountry->name;
                    }
                }
            }

            if (!empty($safData['transportation']['export_country'])) {

                if (isset($refCountries[$safData['transportation']['export_country']])) {

                    $exportCountry = (object)$refCountries[$safData['transportation']['export_country']];

                    if (!$value->hasSubmittedSubApplication()) {
                        if (isset($refCountriesByCode[$exportCountry->code])) {
                            $exportCountry = (object)$refCountriesByCode[$exportCountry->code];
                        }
                    }

                    if (!is_null($exportCountry)) {
                        $exportCountryName = '(' . $exportCountry->code . ') ' . $exportCountry->name;
                    }
                }
            }

            if (!empty($safData['sides']['export']) && !empty($safData['sides']['export']['name'])) {
                $exporter = $safData['sides']['export']['name'];
            }

            if (!empty($safData['sides']['import']) && !empty($safData['sides']['import']['name'])) {
                $importer = $safData['sides']['import']['name'];
            }

            $data[] = [
                'regular_number' => $value->regular_number,
                'status_type' => $value->status_type,
                'export_country' => $exportCountryName,
                'import_country' => $importCountryName,
                'created_at' => $value->created_at,
                'exporter' => $exporter,
                'importer' => $importer,
            ];
        }

        return $data;
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $orderByType = 'desc';
        if ($this->orderByType == 'asc') {
            $orderByType = 'asc';
        }

        $orderByField = $this->orderByCol ?? 'id';
        $query->orderBy($orderByField, $orderByType);
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $query = SingleApplication::select('saf.*')->safOwnerOrCreator();

        if (isset($this->searchData['id'])) {
            $query->where(DB::raw('saf.id::varchar'), $this->searchData['id']);
        }

        if (isset($this->searchData['regime'])) {
            $query->where('saf.regime', $this->searchData['regime']);
        }

        if (isset($this->searchData['status_type'])) {
            $query->where('saf.status_type', $this->searchData['status_type']);
        }

        if (isset($this->searchData['regular_number'])) {
            $query->where('saf.regular_number', 'ilike', "%{$this->searchData['regular_number']}%");
        }

        if (isset($this->searchData['created_at_date_start'])) {
            $query->where('saf.created_at', '>=', Carbon::parse($this->searchData['created_at_date_start'])->startOfDay());
        }

        if (isset($this->searchData['created_at_date_end'])) {
            $query->where('saf.created_at', '<=', Carbon::parse($this->searchData['created_at_date_end'])->endOfDay());
        }

        if (isset($this->searchData['importer_value'])) {
            $query->where('saf.importer_value', 'ilike', "%{$this->searchData['importer_value']}%");
        }

        if (isset($this->searchData['importer_name'])) {
            $query->where('saf.data->saf->sides->import->name', 'ilike', "%{$this->searchData['importer_name']}%");
        }

        if (isset($this->searchData['exporter_value'])) {
            $query->where('saf.exporter_value', 'ilike', "%{$this->searchData['exporter_value']}%");
        }

        if (isset($this->searchData['exporter_name'])) {
            $query->where('saf.data->saf->sides->export->name', 'ilike', "%{$this->searchData['exporter_name']}%");
        }

        if (isset($this->searchData['applicant_value'])) {
            $query->where('saf.applicant_value', 'ilike', "%{$this->searchData['applicant_value']}%");
        }

        if (isset($this->searchData['applicant_name'])) {
            $query->where('saf.data->saf->sides->applicant->name', 'ilike', "%{$this->searchData['applicant_name']}%");
        }

        if (isset($this->searchData['importer_exporter_phone_number'])) {
            $query->where(function ($q) {
                $q->where('saf.data->saf->sides->import->phone_number', 'ilike', "%{$this->searchData['importer_exporter_phone_number']}%")
                    ->orWhere('saf.data->saf->sides->export->phone_number', 'ilike', "%{$this->searchData['importer_exporter_phone_number']}%");
            });
        }

        if (isset($this->searchData['importer_exporter_email'])) {
            $query->where(function ($q) {
                $q->where('saf.data->saf->sides->import->email', 'ilike', "%{$this->searchData['importer_exporter_email']}%")
                    ->orWhere('saf.data->saf->sides->export->email', 'ilike', "%{$this->searchData['importer_exporter_email']}%");
            });
        }

        if (isset($this->searchData['departure'])) {

            $departure = $this->searchData['departure'];

            $query->where('saf.data->saf->transportation->departure', $departure);
        }

        if (isset($this->searchData['appointment'])) {

            $appointment = $this->searchData['appointment'];

            $query->where('saf.data->saf->transportation->appointment', $appointment);
        }

        if (isset($this->searchData['export_country'])) {

            $refCountriesIds = DB::table(ReferenceTable::REFERENCE_COUNTRIES)->where(['code' => $this->searchData['export_country']])->pluck('id')->toArray();

            if (count($refCountriesIds)) {
                $query->whereIn('saf.data->saf->transportation->export_country', $refCountriesIds);
            }

        }

        if (isset($this->searchData['import_country'])) {

            $refCountriesIds = DB::table(ReferenceTable::REFERENCE_COUNTRIES)->where(['code' => $this->searchData['import_country']])->pluck('id')->toArray();

            if (count($refCountriesIds)) {
                $query->whereIn('saf.data->saf->transportation->import_country', $refCountriesIds);
            }
        }

        // Join Sub Applications
        if (isset($this->searchData['constructor_document']) || isset($this->searchData['requested_sub_applications']) || isset($this->searchData['approved_sub_applications']) || isset($this->searchData['submitted_application'])) {
            $query->join('saf_sub_applications', 'saf.regular_number', '=', 'saf_sub_applications.saf_number');
        }

        if (isset($this->searchData['constructor_document'])) {
            $query->where('saf_sub_applications.document_code', $this->searchData['constructor_document']);
        }

        if (isset($this->searchData['submitted_application'])) {
            $query->where('saf_sub_applications.current_status', SingleApplicationSubApplications::STATUS_SUBMITTED);
        }

        if (isset($this->searchData['requested_sub_applications'])) {
            $query->where('saf_sub_applications.current_status', SingleApplicationSubApplications::STATUS_REQUESTED);
        }

        if (isset($this->searchData['approved_sub_applications'])) {

            $query->join('saf_sub_application_states', 'saf_sub_applications.id', '=', 'saf_sub_application_states.saf_subapplication_id');

            $query->join('constructor_states', function ($query) {
                $query->on('constructor_states.id', '=', 'saf_sub_application_states.state_id')->where(['constructor_states.state_type' => ConstructorStates::END_STATE_TYPE, 'state_approved_status' => ConstructorStates::END_STATE_APPROVED]);
            });
        }

        if (Auth::user()->isLocalAdmin()) {
            if (isset($this->searchData['sub_application_sender_user'])) {

                $senderUserId = User::select('id')->where('ssn', $this->searchData['sub_application_sender_user'])->pluck('id')->first() ?? 0;

                $senderSubApplications = SingleApplication::select('saf.*')->safOwnerOrCreator()->join('saf_sub_applications', 'saf.regular_number', '=', 'saf_sub_applications.saf_number')
                    ->where('sender_user_id', $senderUserId)
                    ->pluck('id')->unique()->all();

                $query->whereIn('saf.id', $senderSubApplications);
            }
        }

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}
