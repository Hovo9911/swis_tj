<?php

namespace App\Models\SingleApplication;

use App\Models\Document\Document;
use App\Models\ReferenceTable\ReferenceTable;
use Illuminate\Support\Facades\Auth;
use DOMDocument;

/**
 * Class SingleApplicationXmlManager
 * @package App\Models\SingleApplication
 */
class SingleApplicationXmlManager
{
    /**
     * Function to get Saf xml data for export
     *
     * @param $safNumber
     * @return DOMDocument
     */
    public function exportXmlSafData($safNumber)
    {
        // Enable user error handling
        libxml_use_internal_errors(true);

        $xml = new \DOMDocument("1.0", "utf-8");
        $xml->formatOutput = true;
        $xml->version = "1.0";

        // -------------

        $authUser = Auth::user();

        $saf = SingleApplication::where('regular_number', $safNumber)->safOwnerOrCreator()->firstOrFail();
        $safProducts = $saf->productList;
        $safData = $saf->data['saf'];

        $safRegime = $saf->regime;
        if ($saf->regime == SingleApplication::REGIME_EXPORT) {
            $regimeShortName = 'E';
        }

        if ($saf->regime == SingleApplication::REGIME_IMPORT) {
            $regimeShortName = 'I';
        }

        if ($saf->regime == SingleApplication::REGIME_TRANSIT) {
            $regimeShortName = 'T';
        }

        $countriesRef = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_COUNTRIES);

        $countries = [];
        foreach ($countriesRef as $country) {
            $countries[$country['id']] = $country['code'];
        }

        $productsOriginCountry = $safProducts->pluck('producer_country')->unique()->toArray();

        $countryOfOrigin = '';
        $tradingCountry = '';
        if (count($productsOriginCountry) == 1) {
            $countryOfOrigin = $countries[$productsOriginCountry[0]];
            $tradingCountry = $countries[$productsOriginCountry[0]];
        }

        $typeOfTransport = getReferenceRows(ReferenceTable::REFERENCE_TRANSPORT_TYPE, $safData['transportation']['type_of_transport']);

        // Global Info
        $totalGrossMass = $safProducts->sum('brutto_weight');
        $totalNumberOfItems = $safProducts->count();
        $totalNumberOfPackages = $safProducts->sum('number_of_places');
        $containerFlag = 'false';
        $totalAmount = str_replace(',', '.', $safProducts->sum('total_value_national'));
        $currency = config('swis.default_currency');
        $exchangeRate = '1';
        $transportModeAtBorder = $typeOfTransport->code ?? '';

        $departure = getReferenceRows(ReferenceTable::REFERENCE_CUSTOM_BODY_REFERENCE, $safData['transportation']['departure']);
        $appointment = getReferenceRows(ReferenceTable::REFERENCE_CUSTOM_BODY_REFERENCE, $safData['transportation']['appointment']);

        // Exporter
        $exporterTin = $saf->exporter_value;
        $exporterName = $safData['sides']['export']['name'];
        $exporterCity = $safData['sides']['export']['community'] ?? '';
        $exporterStreet = $safData['sides']['export']['address'] ?? '';
        $exporterCountry = $countries[$safData['sides']['export']['country']] ?? '';

        // Importer
        $importerTin = $saf->importer_value;
        $importerName = $safData['sides']['import']['name'];
        $importerCountry = $countries[$safData['sides']['import']['country']] ?? '';

        // Declarant
        $applicantTin = $saf->applicant_value;
        $applicantName = $safData['sides']['applicant']['name'];
        $applicantCity = '';
        $applicantStreet = $safData['sides']['applicant']['address'];
        $applicantCountry = 'TJ';

        // --------------------------

        $schemaElement = $xml->createElement('schema:' . $regimeShortName . '15TJ');
        $schemaElement->setAttribute('xsi:schemaLocation', 'http://cdps.customs.tj/tok/schemas/v1r7 ' . $regimeShortName . '15TJ.xsd ');
        $schemaElement->setAttribute('messageId', '20190319-153512-dc161768-8000-4dd7');
        $schemaElement->setAttribute('sender', '/' . $regimeShortName . '15TJ/Representative/TIN');
        $schemaElement->setAttribute('xmlns:schema', 'http://cdps.customs.tj/tok/schemas/v1r7');
        $schemaElement->setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        $schemaElement->setAttribute('xmlns:msxsl', 'urn:schemas-microsoft-com:xslt');
        $schemaElement->setAttribute('xmlns:myfn', 'urn:custom-code');

        $schema = $xml->appendChild($schemaElement);

        // Global Info
        $schema->appendChild($xml->createElement('LRN'));
        $schema->appendChild($xml->createElement('blankNo', $safNumber));

        if ($safRegime == SingleApplication::REGIME_TRANSIT) {
            $schema->appendChild($xml->createElement('typeOfTransit', 'МТТ'));
        }

        if ($safRegime != SingleApplication::REGIME_TRANSIT) {
            $schema->appendChild($xml->createElement('totalGrossMass', $totalGrossMass));
        }

        $schema->appendChild($xml->createElement('totalNumberOfItems', $totalNumberOfItems));
        $schema->appendChild($xml->createElement('totalNumberOfPackages', $totalNumberOfPackages));

        if ($tradingCountry && $safRegime != SingleApplication::REGIME_TRANSIT) {
            $schema->appendChild($xml->createElement('tradingCountry', $tradingCountry));
        }

        $countryOfDispatchAndExportCode = isset($safData['transportation'], $safData['transportation']['export_country']) ? $countries[$safData['transportation']['export_country']] ?? '' : '';
        $countryOfDestination = isset($safData['transportation'], $safData['transportation']['import_country']) ? $countries[$safData['transportation']['import_country']] ?? '' : '';

        // Export
        if ($safRegime == SingleApplication::REGIME_EXPORT) {

            if ($countryOfOrigin) {
                $schema->appendChild($xml->createElement('countryOfOrigin', $countryOfOrigin));
            }

            if ($countryOfDestination) {
                $schema->appendChild($xml->createElement('countryOfDestination', $countryOfDestination));
            }
        }

        // Import
        if ($safRegime == SingleApplication::REGIME_IMPORT) {

            if ($countryOfDispatchAndExportCode) {
                $schema->appendChild($xml->createElement('countryOfDispatchAndExportCode', $countryOfDispatchAndExportCode));
            }

            if ($countryOfOrigin) {
                $schema->appendChild($xml->createElement('countryOfOrigin', $countryOfOrigin));
            }
        }

        // Transit
        if ($safRegime == SingleApplication::REGIME_TRANSIT) {

            if ($countryOfDispatchAndExportCode) {
                $schema->appendChild($xml->createElement('countryOfDispatchAndExportCode', $countryOfDispatchAndExportCode));
            }

            if ($countryOfOrigin) {
                $schema->appendChild($xml->createElement('countryOfOrigin', $countryOfOrigin));
            }

            if ($countryOfDestination) {
                $schema->appendChild($xml->createElement('countryOfDestination', $countryOfDestination));
            }

            if ($transportModeAtBorder && strlen($transportModeAtBorder) == 2) {
                $schema->appendChild($xml->createElement('transportModeAtBorder', $transportModeAtBorder));
            }

            $schema->appendChild($xml->createElement('officeOfDestination', $appointment->code ?? ''));
        }

        $schema->appendChild($xml->createElement('containerFlag', $containerFlag));

        // currencyAndTotalAmountInvoiced
        $currencyAndTotalAmountInvoiced = $xml->createElement('currencyAndTotalAmountInvoiced');
        $currencyAndTotalAmountInvoiced->appendChild($xml->createElement('value', $totalAmount));
        $currencyAndTotalAmountInvoiced->appendChild($xml->createElement('currency', $currency));

        $schema->appendChild($currencyAndTotalAmountInvoiced);

        // totalTaxesAmount
        if ($safRegime != SingleApplication::REGIME_TRANSIT) {
            $totalTaxesAmount = $xml->createElement('totalTaxesAmount');

            $totalTaxesAmount->appendChild($xml->createElement('value', $totalAmount));
            $totalTaxesAmount->appendChild($xml->createElement('currency', $currency));

            $schema->appendChild($totalTaxesAmount);
        }

        // OfficeOfDeparture
        if ($safRegime == SingleApplication::REGIME_TRANSIT) {
            $OfficeOfDeparture = $xml->createElement('OfficeOfDeparture');
            $OfficeOfDeparture->appendChild($xml->createElement('office', $departure->code ?? ''));
            $schema->appendChild($OfficeOfDeparture);
        }

        // exchangeRate
        if ($safRegime != SingleApplication::REGIME_TRANSIT) {
            $schema->appendChild($xml->createElement('exchangeRate', $exchangeRate));

            // transportModeAtBorder
            if (strlen($transportModeAtBorder) == 2) {
                $schema->appendChild($xml->createElement('transportModeAtBorder', $transportModeAtBorder));
            }
        }

        // Consignee
        if ($safRegime == SingleApplication::REGIME_TRANSIT) {

            $consignee = $xml->createElement('Consignee');
            $consignee->appendChild($xml->createElement('TIN', $importerTin));
            $consignee->appendChild($xml->createElement('name', $importerName));

            $consigneeAddress = $xml->createElement('Address');
            $consigneeAddress->appendChild($xml->createElement('country', $importerCountry));
            $consignee->appendChild($consigneeAddress);

            $schema->appendChild($consignee);
        }

        // Exporter
        $exporter = $xml->createElement('Exporter');

        if (strlen($exporterTin) == 9) {
            $exporter->appendChild($xml->createElement('TIN', $exporterTin));
        }

        $exporter->appendChild($xml->createElement('name', $exporterName));
        $exporterAddress = $xml->createElement('Address');

        if ($exporterCity && $exporterCity != '-'){
            $exporterAddress->appendChild($xml->createElement('city', $exporterCity));
        }

        if ($exporterStreet && $exporterStreet != '-'){
            $exporterAddress->appendChild($xml->createElement('street', $exporterStreet));
        }

        $exporterAddress->appendChild($xml->createElement('country', $exporterCountry));
        $exporter->appendChild($exporterAddress);

        //
        $schema->appendChild($exporter);

        if ($safRegime == SingleApplication::REGIME_TRANSIT) {

            $personResponsibleForFinancialSettlement = $xml->createElement('PersonResponsibleForFinancialSettlement');
            $personResponsibleForFinancialSettlement->appendChild($xml->createElement('TIN', $importerTin));
            $personResponsibleForFinancialSettlement->appendChild($xml->createElement('name', $importerName));

            $personResponsibleForFinancialSettlementAddress = $xml->createElement('Address');
            $personResponsibleForFinancialSettlementAddress->appendChild($xml->createElement('country', $importerCountry));
            $personResponsibleForFinancialSettlement->appendChild($personResponsibleForFinancialSettlementAddress);
            $schema->appendChild($personResponsibleForFinancialSettlement);

        } else {

            // Consignee
            $consignee = $xml->createElement('Consignee');
            $consignee->appendChild($xml->createElement('TIN', $importerTin));
            $consignee->appendChild($xml->createElement('name', $importerName));

            $consigneeAddress = $xml->createElement('Address');
            $consigneeAddress->appendChild($xml->createElement('country', $importerCountry));
            $consignee->appendChild($consigneeAddress);

            $schema->appendChild($consignee);

            if ($safRegime == SingleApplication::REGIME_IMPORT) {

                $personResponsibleForFinancialSettlement = $xml->createElement('PersonResponsibleForFinancialSettlement');
                $personResponsibleForFinancialSettlement->appendChild($xml->createElement('TIN', $importerTin));
                $personResponsibleForFinancialSettlement->appendChild($xml->createElement('name', $importerName));

                $personResponsibleForFinancialSettlementAddress = $xml->createElement('Address');
                $personResponsibleForFinancialSettlementAddress->appendChild($xml->createElement('country', $importerCountry));
                $personResponsibleForFinancialSettlement->appendChild($personResponsibleForFinancialSettlementAddress);
                $schema->appendChild($personResponsibleForFinancialSettlement);
            }

            if ($safRegime == SingleApplication::REGIME_EXPORT) {

                $personResponsibleForFinancialSettlement = $xml->createElement('PersonResponsibleForFinancialSettlement');
                $personResponsibleForFinancialSettlement->appendChild($xml->createElement('TIN', $importerTin));
                $personResponsibleForFinancialSettlement->appendChild($xml->createElement('name', $exporterName));

                $personResponsibleForFinancialSettlementAddress = $xml->createElement('Address');
                $personResponsibleForFinancialSettlementAddress->appendChild($xml->createElement('country', $exporterCountry));
                $personResponsibleForFinancialSettlement->appendChild($personResponsibleForFinancialSettlementAddress);
                $schema->appendChild($personResponsibleForFinancialSettlement);
            }

        }

        // Declarant
        $declarant = $xml->createElement('Declarant');
        $declarant->appendChild($xml->createElement('TIN', $applicantTin));
        $declarant->appendChild($xml->createElement('name', $applicantName));

        $declarantAddress = $xml->createElement('Address');

        if ($applicantCity) {
            $declarantAddress->appendChild($xml->createElement('city', $applicantCity));
        }

        if ($applicantStreet && $applicantStreet != '-') {
            $declarantAddress->appendChild($xml->createElement('street', $applicantStreet));
        }

        $declarantAddress->appendChild($xml->createElement('country', $applicantCountry));
        $declarant->appendChild($declarantAddress);

        $schema->appendChild($declarant);

        // Representative
        $Representative = $xml->createElement('Representative');
        $representative = $xml->createElement('representative');

        $representative->appendChild($xml->createElement('TIN', $applicantTin));
        $representative->appendChild($xml->createElement('name', $applicantName));

        $person = $xml->createElement('Person');
        $person->appendChild($xml->createElement('name', $authUser->first_name));
        $person->appendChild($xml->createElement('familyName', $authUser->last_name));
        $person->appendChild($xml->createElement('fatherName', $authUser->patronymic_name));

        $personalIdentificationDocument = $xml->createElement('PersonalIdentificationDocument');

        $personalIdentificationDocument->appendChild($xml->createElement('type', 'PS'));
        $personalIdentificationDocument->appendChild($xml->createElement('reference', $authUser->passport));
        $personalIdentificationDocument->appendChild($xml->createElement('issueDate', $authUser->passport_issue_date ?? ''));
        $personalIdentificationDocument->appendChild($xml->createElement('authority', $authUser->passport_issued_by ?? ''));

        $person->appendChild($personalIdentificationDocument);
        $representative->appendChild($person);
        $Representative->appendChild($representative);

        $schema->appendChild($Representative);

        // IdentityCrossingBorder
        $identity = '';
        if (count($safData['transportation']['state_number_customs_support'])) {
            $stateNumbers = [];
            foreach ($safData['transportation']['state_number_customs_support'] as $sealNumber) {
                $stateNumbers[] = implode(' / ', array_filter($sealNumber));
            }

            $identity = implode(',', $stateNumbers);
        }

        if ($identity) {
            $identityCrossingBorder = $xml->createElement('IdentityCrossingBorder');
            $identityCrossingBorder->appendChild($xml->createElement('identity', $identity));

            $schema->appendChild($identityCrossingBorder);
        }

        if ($safRegime == SingleApplication::REGIME_EXPORT || $safRegime == SingleApplication::REGIME_IMPORT) {

            $trCode = $departure->code ?? '';
            $trName = $departure->name ?? '';
            if ($safRegime == SingleApplication::REGIME_IMPORT) {
                $trCode = $appointment->code ?? '';
                $trName = $appointment->name ?? '';
            }

            $locationOfGoods = $xml->createElement('LocationOfGoods');

            //
            $locationOfGoods->appendChild($xml->createElement('locationName', $trName));
            $locationOfGoods->appendChild($xml->createElement('office', $trCode));

            $schema->appendChild($locationOfGoods);
        }

        // GoodsItem
        $i = 1;
        foreach ($safProducts as $product) {

            $currencyName = getReferenceRows(ReferenceTable::REFERENCE_CURRENCIES, $product->total_value_code)->name ?? '';

            $goodsItem = $xml->createElement('GoodsItem');
            $goodsItem->appendChild($xml->createElement('goodsDescription', $product->product_description));
            $goodsItem->appendChild($xml->createElement('itemNumber', $i));
            $goodsItem->appendChild($xml->createElement('countryOfOrigin', $countries[$product->producer_country] ?? ''));
            $goodsItem->appendChild($xml->createElement('grossMass', $product->brutto_weight));
            $goodsItem->appendChild($xml->createElement('netMass', $product->netto_weight));

            //
            $itemPrice = $xml->createElement('itemPrice');
            $itemPrice->appendChild($xml->createElement('value', $product->total_value));
            $itemPrice->appendChild($xml->createElement('currency', getReferenceRows(ReferenceTable::REFERENCE_CURRENCIES, $product->total_value_code)->name ?? ''));

            //
            $statisticalValueAmountAndCurrency = $xml->createElement('statisticalValueAmountAndCurrency');
            $statisticalValueAmountAndCurrency->appendChild($xml->createElement('value', $product->total_value));
            $statisticalValueAmountAndCurrency->appendChild($xml->createElement('currency', $currencyName));

            if ($safRegime == SingleApplication::REGIME_IMPORT) {
                $goodsItem->appendChild($itemPrice);
            }

            $goodsItem->appendChild($statisticalValueAmountAndCurrency);

            //
            $commodityCode = $xml->createElement('CommodityCode');
            $commodityCode->appendChild($xml->createElement('combinedNomenclature', getReferenceRows(ReferenceTable::REFERENCE_HS_CODE_6, $product->product_code)->code ?? ''));

            $goodsItem->appendChild($commodityCode);

            //
            $package = $xml->createElement('Package');
            $package->appendChild($xml->createElement('kindOfPackages', getReferenceRows(ReferenceTable::REFERENCE_PACKAGING_REFERENCE, $product->places_type)->code ?? ''));
            $package->appendChild($xml->createElement('numberOfPackages', $product->number_of_places));

            $goodsItem->appendChild($package);

            //
            $customsProcedure = $xml->createElement('CustomsProcedure');
            $customsProcedure->appendChild($xml->createElement('additionalNationalProcedure', '00'));
            $customsProcedure->appendChild($xml->createElement('procedureRequested', '00'));
            $customsProcedure->appendChild($xml->createElement('previousProcedure', '00'));

            $goodsItem->appendChild($customsProcedure);

            //
            if ($safRegime == SingleApplication::REGIME_IMPORT || $safRegime == SingleApplication::REGIME_TRANSIT) {
                $correctionOfValue = $xml->createElement('CorrectionOfValue');

                $customsValueAmount = $xml->createElement('customsValueAmount');
                $customsValueAmount->appendChild($xml->createElement('value', $product->total_value));
                $customsValueAmount->appendChild($xml->createElement('currency', $currencyName));

                $correctionOfValue->appendChild($customsValueAmount);
                $correctionOfValue->appendChild($xml->createElement('customsValueValuationMethod', '1'));

                $goodsItem->appendChild($correctionOfValue);
            }

            //
            $documentIds = $product->safDocuments->pluck('document_id')->toArray();
            if (count($documentIds)) {
                $documents = Document::select('document_type', 'document_release_date', 'document_number')->whereIn('id', $documentIds)->get();

                foreach ($documents as $document) {
                    $producedDocument = $xml->createElement('ProducedDocument');
                    $producedDocument->appendChild($xml->createElement('type', $document->document_type));
                    $producedDocument->appendChild($xml->createElement('reference', $document->document_number));
                    $producedDocument->appendChild($xml->createElement('issueDate', $document->getOriginal('document_release_date')));

                    $goodsItem->appendChild($producedDocument);
                }
            }

            $schema->appendChild($goodsItem);
            $i++;
        }

        // ------------

        if ($safRegime != SingleApplication::REGIME_TRANSIT) {
            $schema->appendChild($xml->createElement('dateOfDeclaration', currentDate()));
        }

        if ($safRegime == SingleApplication::REGIME_EXPORT) {
            $schema->appendChild($xml->createElement('storingFlag', 'false'));
            $schema->appendChild($xml->createElement('officeOfExport', $departure->code ?? ''));
            $schema->appendChild($xml->createElement('officeOfExit', $appointment->code ?? ''));
        }

        if ($safRegime == SingleApplication::REGIME_IMPORT) {
            $schema->appendChild($xml->createElement('officeOfImport', $appointment->code ?? ''));
        }

        return $xml;
    }

}