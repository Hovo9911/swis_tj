<?php

namespace App\Models\SingleApplication;

use App\EntityTypesInfo\LegalEntityForms;
use App\Exceptions\TaxServiceTjCompanyInactiveException;
use App\Exceptions\TaxServiceTjInnNotFoundException;
use App\Exceptions\TaxServiceTjInvalidResponseException;
use App\Models\Agency\Agency;
use App\Models\BaseModel;
use App\Models\Document\Document;
use App\Models\Document\DocumentManager;
use App\Models\LaboratoryIndicator\LaboratoryIndicator;
use App\Models\Notifications\NotificationsManager;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure;
use App\Models\SingleApplicationDocumentProducts\SingleApplicationDocumentProducts;
use App\Models\SingleApplicationDocuments\SingleApplicationDocuments;
use App\Models\SingleApplicationNotes\SingleApplicationNotes;
use App\Models\SingleApplicationProducts\SingleApplicationProducts;
use App\Models\SingleApplicationProducts\SingleApplicationProductsManager;
use App\Models\SingleApplicationProductsBatch\SingleApplicationProductsBatch;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class SingleApplicationManager
 * @package App\Models\SingleApplication
 */
class SingleApplicationManager
{
    /**
     * Function to Store data when first time open saf
     *
     * @param array $data
     * @return SingleApplication
     */
    public function store(array $data)
    {
        $defaultData = [
            'data' => null,
            'custom_data' => null,
        ];

        $data['user_id'] = Auth::user()->getCreatorUser()->id;
        $data['creator_user_id'] = Auth::id();
        $data['company_tax_id'] = Auth::user()->companyTaxId();
        $data['applicant_type'] = Auth::user()->currentUserType();
        $data['applicant_value'] = Auth::user()->currentUserTypeValue();
        $data['show_status'] = SingleApplication::STATUS_ACTIVE;

        $insertData = array_merge($defaultData, $data);

        $saf = new SingleApplication($insertData);

        return DB::transaction(function () use ($saf, $data) {

            $saf->save();

            // Note
            SingleApplicationNotes::storeNotes(['saf_number' => $data['regular_number'], 'note' => 'swis.saf.status_type.' . SingleApplication::CREATED_TYPE], SingleApplicationNotes::NOTE_TRANS_KEY_CREATE_SAF);

            return $saf;
        });
    }

    /**
     * Function to Store data when saf is formed
     *
     * @param array $data
     *
     * @throws TaxServiceTjCompanyInactiveException
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function storeDataJson(array $data)
    {
        $safNumber = $data['saf']['general']['regular_number'];
        $statusType = $data['status_type'];
        $sidesData = [];

        $saf = SingleApplication::where('regular_number', $safNumber)->select('id', 'regular_number', 'regime', 'data', 'status_type', 'saf_controlled_fields', 'created_at')->firstOrFail();

        // Saf Controlled Fields info getting from Routing
        $safControlledFieldsData = [];
        if ($saf->status_type == SingleApplication::CREATED_TYPE) {
            $safControlledFieldsData = $saf->saf_controlled_fields;
        }

        if (!$saf->hasSubmittedSubApplication()) {

            if (isset($data['saf']['sides'])) {

                // Holder (importer/exporter)
                foreach ([SingleApplication::REGIME_IMPORT, SingleApplication::REGIME_EXPORT] as $regime) {

                    if ($saf->status_type != SingleApplication::CREATED_TYPE && $regime == $saf->regime) {

                        $data['saf']['sides'][$regime] = $saf->data['saf']['sides'][$regime];

                        continue;
                    }

                    $checkBrokerPart = true;
                    if ($regime != $saf->regime) {
                        $checkBrokerPart = false;
                    }

                    $legalEntityForms = LegalEntityForms::getInstance();
                    $holderData = $legalEntityForms->holderDataByType($data['saf']['sides'][$regime]['type'], $data['saf']['sides'][$regime]['type_value'], $data['saf']['sides'][$regime]['passport'] ?? null, $checkBrokerPart, '');

                    $data['saf']['sides'][$regime] = array_merge($data['saf']['sides'][$regime], $holderData);
                }

                // Holder (Applicant)
                $authUserCreator = Auth::user()->getCreator();

                $data['saf']['sides']['applicant'] = [
                    'type' => $authUserCreator->type,
                    'type_value' => $authUserCreator->ssn,
                    'name' => $authUserCreator->name,
                    'address' => $authUserCreator->address,
                    'phone_number' => $authUserCreator->phone_number,
                    'email' => $authUserCreator->email,
                ];

                //
                $sidesData = [
                    'importer_type' => $data['saf']['sides']['import']['type'],
                    'importer_value' => $data['saf']['sides']['import']['type_value'],
                    'exporter_type' => $data['saf']['sides']['export']['type'] ?? 0,
                    'exporter_value' => $data['saf']['sides']['export']['type_value'] ?? 0
                ];
            }

            //
            if ($saf->regime == SingleApplication::REGIME_IMPORT) {
                $data['saf']['transportation']['import_country'] = getReferenceRowIdByCode(ReferenceTable::REFERENCE_COUNTRIES, config('swis.country_mode'));
            }

            if ($saf->regime == SingleApplication::REGIME_EXPORT) {
                $data['saf']['transportation']['export_country'] = getReferenceRowIdByCode(ReferenceTable::REFERENCE_COUNTRIES, config('swis.country_mode'));
            }

            // Products totals
            $singleApplicationDocumentsManager = new SingleApplicationProductsManager();
            $safProductsTotalValues = $singleApplicationDocumentsManager->getSafProductsTotalValues($safNumber);

            $data['saf']['product'] = [
                'brutto_weight' => $safProductsTotalValues['brutto_weight'],
                'netto_weight' => $safProductsTotalValues['netto_weight'],
                'product_count' => $safProductsTotalValues['product_count'],
                'total_value' => $safProductsTotalValues['total_value'],
                'total_value_all' => $safProductsTotalValues['total_value_all'],
            ];

            // Saf main data
            $data['saf']['general']['regime'] = $saf->regime;
            $data['saf']['general']['regular_number'] = $saf->regular_number;
            $data['saf']['general']['date'] = Carbon::parse($saf->getOriginal('created_at'))->format('Y-m-d');

            //
            if (isset($data['saf_controlled_fields'])) {
                $safControlledFieldsData['saf_controlled_fields'] = $data['saf_controlled_fields'];

                unset($data['saf_controlled_fields']);
            }
        }

        //
        DB::transaction(function () use ($saf, $safNumber, $statusType, $safControlledFieldsData, $data, $sidesData) {

            // Notes
            $safNote = $data['saf']['notes']['notes'] ?? null;

            if ($safNote) {

                SingleApplicationNotes::storeNotes(['saf_number' => $safNumber, 'note' => $safNote], SingleApplicationNotes::NOTE_TRANS_KEY_SAF_SAVE_NOT);

                unset($data['saf']['notes']);
            }

            if ($statusType == SingleApplication::DRAFT_TYPE) {

                SingleApplicationNotes::storeNotes(['saf_number' => $safNumber, 'note' => 'swis.saf.status_type.' . SingleApplication::CREATED_TYPE], SingleApplicationNotes::NOTE_TRANS_KEY_CREATE_SAF_SAVED);
            }

            if ($statusType == SingleApplication::SEND_TYPE) {
                $note = 'swis.saf.status_type.' . SingleApplication::SEND_TYPE;

                SingleApplicationNotes::storeNotes(['saf_number' => $safNumber, 'note' => $note], SingleApplicationNotes::NOTE_TRANS_KEY_SAF_UPDATE_STATUS_TYPE);
            }

            // If saf has submitted sub application -> no update data
            if(!$saf->hasSubmittedSubApplication()){

                $saf->update(['data' => $data, 'need_reform' => SingleApplication::FALSE, 'saf_controlled_fields' => $safControlledFieldsData]);

                if (count($sidesData)) {
                    $saf->update($sidesData);
                }

                //
                if ($statusType == SingleApplication::SEND_TYPE && $saf->status_type != SingleApplication::SEND_TYPE) {
                    $saf->update(['status_type' => SingleApplication::SEND_TYPE]);
                }
            }

            // check if product has not batch add new
            SingleApplicationProducts::checkAndNewBatch($safNumber);
        });
    }

    /**
     * Function to delete saf
     *
     * @param $safNumber
     */
    public function deleteSaf($safNumber)
    {
        if (!is_null($safNumber)) {
            SingleApplication::select('id', 'regular_number')->where('regular_number', $safNumber)->where('status_type', SingleApplication::CREATED_TYPE)->safOwnerOrCreator()->firstOrFail();

            DB::transaction(function () use ($safNumber) {

                SingleApplication::where('regular_number', $safNumber)->delete();

                $productsIds = SingleApplicationProducts::where('saf_number', $safNumber)->pluck('id')->all();

                if (count($productsIds)) {
                    SingleApplicationProductsBatch::whereIn('saf_product_id', $productsIds)->delete();
                }

                SingleApplicationProducts::where('saf_number', $safNumber)->delete();
                SingleApplicationDocuments::where('saf_number', $safNumber)->delete();
                SingleApplicationDocumentProducts::where('saf_number', $safNumber)->delete();
                SingleApplicationNotes::where('saf_number', $safNumber)->delete();

                Log::info('saf-delete: Saf-number - ' . $safNumber . ', User id: ' . Auth::id());

            });
        }

    }

    /**
     * Function to clone Saf
     *
     * @param $safNumber
     * @return SingleApplication
     */
    public function cloneSaf($safNumber)
    {
        $cloningFromSaf = SingleApplication::where('regular_number', $safNumber)->safOwnerOrCreator()->firstOrFail();

        $regime = $cloningFromSaf->regime;

        DB::beginTransaction();

        // Get Next Saf number
        $regularNumber = $this->returnRegularNumber();

        // Parties , Transportation
        $safDataJson = $cloningFromSaf->data;

        $safDataJson['saf']['general'] = [
            "date" => date('Y-m-d'),
            "regime" => $cloningFromSaf->regime,
            "regular_number" => $regularNumber
        ];

        $authUserCreator = Auth::user()->getCreator();

        $safDataJson['saf']['sides']['applicant'] = [
            "name" => $authUserCreator->name,
            "email" => $authUserCreator->email,
            "address" => $authUserCreator->address,
            "type_value" => $authUserCreator->ssn,
            "phone_number" => $authUserCreator->phone_number,
        ];

        $safData = [
            'regular_number' => $regularNumber,
            'regime' => $regime,
            'saf_data_structure_id' => SingleApplicationDataStructure::lastXml('id'),
            'data' => $safDataJson,
            'saf_controlled_fields' => $cloningFromSaf->saf_controlled_fields ?? [],
            'status_type' => SingleApplication::CREATED_TYPE,
        ];

        $newCreatedSaf = $this->store($safData);

        DB::commit();

        // Store Products
        $cloningSafProducts = SingleApplicationProducts::where('saf_number', $safNumber)->get();

        $newProductIds = [];
        if ($cloningSafProducts->count() > 0) {
            foreach ($cloningSafProducts as $product) {

                $cloningProduct = SingleApplicationProducts::find($product->id);
                // cloning record
                $newProduct = $cloningProduct->replicate();
                $newProduct->saf_number = $newCreatedSaf->regular_number;
                $newProduct->saf_id = $newCreatedSaf->id;
                $newProduct->user_id = Auth::id();
                $newProduct->save();

                // Store Batches
                $newProductIds[$product->id] = $newProduct->id;
                if ($product->batches->count() > 0) {
                    foreach ($product->batches as $batch) {
                        $cloningBatch = SingleApplicationProductsBatch::where(['id' => $batch->id])->first();
                        $newBatch = $cloningBatch->replicate();
                        $newBatch->saf_product_id = $newProduct->id;
                        $newBatch->approved_quantity = null;
                        $newBatch->approved_type = null;
                        $newBatch->rejected_quantity = null;
                        $newBatch->rejection_reason = null;
                        $newBatch->save();
                    }
                }
            }
        }

        // Store Documents
        $cloningSafDocuments = SingleApplicationDocuments::where(['saf_number' => $safNumber, 'added_from_module' => SingleApplicationDocuments::ADDED_FROM_MODULE_SAF])->get();

        if ($cloningSafDocuments->count() > 0) {
            $documentManager = new DocumentManager();

            foreach ($cloningSafDocuments as $document) {

                $cloningDocument = Document::find($document->document_id);
                // cloning record
                $newDocument = $cloningDocument->replicate();
                $newDocument->user_id = Auth::id();
                $newDocument->company_tax_id = Auth::user()->companyTaxId();
                $newDocument->uuid = $documentManager->generateUniqueCode();
                $newDocument->document_access_password = $documentManager->generateUniqueCode();
                $newDocument->save();

                $safDocumentData = [
                    'user_id' => Auth::id(),
                    'company_tax_id' => Auth::user()->companyTaxId(),
                    'document_id' => $newDocument->id,
                    'saf_number' => $newCreatedSaf->regular_number,
                    'saf_id' => $newCreatedSaf->id,
                    'added_from_module' => 'saf'
                ];

                $newSafDocument = SingleApplicationDocuments::create($safDocumentData);

                if ($document->documentProducts->count() > 0) {
                    foreach ($document->documentProducts as $documentProduct) {

                        $newDocumentProduct = [
                            'user_id' => Auth::id(),
                            'company_tax_id' => Auth::user()->companyTaxId(),
                            'saf_number' => $newCreatedSaf->regular_number,
                            'product_id' => $newProductIds[$documentProduct->product_id] ?? 0,
                            'saf_document_id' => $newSafDocument->id,
                        ];

                        SingleApplicationDocumentProducts::create($newDocumentProduct);

                    }

                }

            }

        }

        return $newCreatedSaf;
    }

    /**
     * Function to return next possible regular number
     *
     * @return string
     */
    public function returnRegularNumber()
    {
        $lastSingleApplication = SingleApplication::select('regular_number')->orderBy('id', 'desc')->lockForUpdate()->first();

        $regularNumber = date('Y') . "-";

        if (!is_null($lastSingleApplication)) {
            $lastPrefix = explode("-", $lastSingleApplication->regular_number);
            $incrementFromOne = ($lastPrefix[0] == date('Y'));
            $lastPrefix = end($lastPrefix);

            if ($incrementFromOne) {
                $lastNumber = (int)$lastPrefix + 1;

                $regularNumber .= $lastNumber;
            }else{
                $regularNumber .= "1";
            }
        } else {
            $regularNumber .= "1";
        }

        $existSingleApp = SingleApplication::select('regular_number')->where('regular_number', $regularNumber)->first();

        if (is_null($existSingleApp)) {
            return $regularNumber;
        }

        return $this->returnRegularNumber();
    }

    /**
     * Function to send message yo sub application
     *
     * @param $safNumber
     * @param $subApplicationId
     * @param $message
     */
    public static function sendMessageToSubApplication($safNumber, $subApplicationId, $message)
    {
        DB::transaction(function () use ($safNumber, $subApplicationId, $message) {

            $noteData = [
                'saf_number' => $safNumber,
                'type' => SingleApplicationNotes::NOTE_TYPE_MESSAGE,
                'sub_application_id' => $subApplicationId,
                'note' => $message,
            ];

            SingleApplicationNotes::storeNotes($noteData, SingleApplicationNotes::NOTE_TRANS_KEY_MESSAGE_SEND_FROM_SAF_TO_SUB_APP);
            NotificationsManager::storeSafSubApplicationMessages($noteData);
        });
    }

    /**
     * Function to get Hs Codes for lab expertise
     *
     * @param $validatedData
     * @param null $ids
     * @return SingleApplicationProducts
     */
    public function getHsCodesForLabExpertise($validatedData, $ids = null)
    {
        $hsCodeTable = ReferenceTable::REFERENCE_HS_CODE_6;

        return SingleApplicationProducts::select('saf_products.*', "{$hsCodeTable}.code as hs_code", "{$hsCodeTable}_ml.path_name as hs_code_path_name")
            ->where('saf_number', $validatedData['saf_number'])
            ->join($hsCodeTable, DB::raw("{$hsCodeTable}.id::varchar"), "=", "saf_products.product_code")
            ->join("{$hsCodeTable}_ml", function ($q) use ($hsCodeTable) {
                $q->on("{$hsCodeTable}_ml.{$hsCodeTable}_id", "=", "{$hsCodeTable}.id")->where('lng_id', cLng());
            })
            ->when(isset($ids), function ($q) use ($ids, $hsCodeTable) {
                $q->whereIn("saf_products.id", $ids);
            })
            ->orderBy('saf_products.id')
            ->get();
    }

    /**
     * Function to get laboratory indicators
     *
     * @param $validatedData
     * @return LaboratoryIndicator
     */
    public function getLaboratoryExaminationGetLaboratories($validatedData)
    {
        $labHsIndicator = ReferenceTable::REFERENCE_LAB_HS_INDICATOR;
        $hsCodes = $this->getHsCodesForLabExpertise($validatedData, $validatedData['product_ids'])->pluck('hs_code')->all();

        $subDivision = null;
        if (isset($validatedData['sub_application_id'])) {
            $subApplicationData = SingleApplicationSubApplications::select('id', 'agency_subdivision_id')->find($validatedData['sub_application_id']);
            if (isset($subApplicationData)) {
                $subDivision = getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS, $subApplicationData->agency_subdivision_id);
            }
        }

        return LaboratoryIndicator::select('laboratory_indicator.laboratory_id', "{$labHsIndicator}.sphere")
            ->join($labHsIndicator, "{$labHsIndicator}.id", "=", "laboratory_indicator.indicator_id")
            ->where('laboratory_indicator.company_tax_id', Auth::user()->companyTaxId())
            ->when(isset($hsCodes), function ($q) use ($hsCodes, $labHsIndicator) {
                $q->where(function ($q) use ($hsCodes, $labHsIndicator) {
                    foreach ($hsCodes as $hsCode) {
                        $q->where(function ($query) use ($hsCode, $labHsIndicator) {
                            $query->where("{$labHsIndicator}.hs_start", '<=', $hsCode);
                            $query->where("{$labHsIndicator}.hs_end", '>=', $hsCode);
                        });
                    }
                });
            })
            ->when(isset($subDivision), function ($q) use ($subDivision) {
                $q->join('regional_office_laboratory', 'regional_office_laboratory.agency_laboratory_id', '=', 'laboratory_indicator.laboratory_id')->where('regional_office_laboratory.regional_office_code', $subDivision->code);
            })
            ->get();
    }

    /**
     * Function to get indicators by product hs_code
     *
     * @param $validatedData
     * @return Collection
     */
    public function getIndicatorsByProduct($validatedData)
    {
        $labIndicators = [];
        $hsCodeTable = ReferenceTable::REFERENCE_HS_CODE_6;
        $refTable = ReferenceTable::REFERENCE_LAB_HS_INDICATOR;

        $hsCode = SingleApplicationProducts::select('saf_products.*', "{$hsCodeTable}.code as hs_code", "{$hsCodeTable}_ml.path_name as hs_code_path_name")
            ->join($hsCodeTable, DB::raw("{$hsCodeTable}.id::varchar"), "=", "saf_products.product_code")
            ->join("{$hsCodeTable}_ml", function ($q) use ($hsCodeTable) {
                $q->on("{$hsCodeTable}_ml.{$hsCodeTable}_id", "=", "{$hsCodeTable}.id")->where('lng_id', cLng());
            })
            ->where("saf_products.id", $validatedData['product_id'])
            ->first();


        if (Auth::user()->isLaboratory()) {
            $lab = Agency::select('id')->where('tax_id', Auth::user()->companyTaxId())->first();
            if (!is_null($lab)) {
                $labIndicators = LaboratoryIndicator::select('indicator_id')->where('laboratory_id', $lab->id)->pluck('indicator_id')->all();
            }
        }

        return DB::table($refTable)
            ->select('id', 'code', 'name')
            ->join("{$refTable}_ml", function ($q) use ($refTable) {
                $q->on("{$refTable}.id", "=", "{$refTable}_ml.{$refTable}_id")->where('lng_id', cLng('id'));
            })
            ->when($hsCode, function ($q) use ($hsCode) {
                $q->where('hs_start', '<=', $hsCode->hs_code);
                $q->where('hs_end', '>=', $hsCode->hs_code);
            })
            ->when(!empty($labIndicators), function ($q) use ($labIndicators) {
                $q->whereIn('id', $labIndicators);
            })
            ->where("{$refTable}.show_status", BaseModel::TRUE)
            ->get();
    }

    /**
     * Function to get labs by indicators
     *
     * @param $validatedData
     * @return LaboratoryIndicator
     */
    public function getLabsByIndicators($validatedData)
    {
        $indicators = getReferenceRowsWhereIn(ReferenceTable::REFERENCE_LAB_HS_INDICATOR, $validatedData['indicators'])->pluck('code')->all();

        $subDivision = null;
        if (isset($validatedData['sub_application_id'])) {
            $subApplicationData = SingleApplicationSubApplications::select('id', 'agency_subdivision_id')->find($validatedData['sub_application_id']);
            if (isset($subApplicationData)) {
                $subDivision = getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS, $subApplicationData->agency_subdivision_id);
            }
        }

        return LaboratoryIndicator::select('agency.id', 'agency.tax_id', 'agency_ml.name')
            ->where('company_tax_id', Auth::user()->companyTaxId())
            ->whereIn('indicator_code', $indicators)
            ->join('agency', 'agency.id', '=', 'laboratory_indicator.laboratory_id')
            ->join('agency_ml', function ($q) {
                $q->on('agency_ml.agency_id', '=', 'agency.id')->where('lng_id', cLng('id'));
            })
            ->when(isset($subDivision), function ($q) use ($subDivision) {
                $q->join('regional_office_laboratory', 'regional_office_laboratory.agency_laboratory_id', '=', 'laboratory_indicator.laboratory_id')->where('regional_office_laboratory.regional_office_code', $subDivision->code);
            })
            ->groupBy('agency.id', 'agency.tax_id', 'agency_ml.name')
            ->havingRaw('count(indicator_code) = ?', [count($indicators)])
            ->get();
    }

    /**
     * Function to log data
     *
     * @param $safNumber
     * @return array
     */
    public function getLogData($safNumber)
    {
        $logData = [];
        $saf = SingleApplication::where('regular_number', $safNumber)->first();

        if (!is_null($saf)) {
            $saf = $saf->toArray();
            $subApplications = SingleApplicationSubApplications::where('saf_number', $safNumber)->get()->toArray();
            $logData = $saf;
            $logData['subApplications'] = $subApplications;
        }

        return $logData;
    }

}
