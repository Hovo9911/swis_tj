<?php

namespace App\Models\ApiProvider;


use App\Models\BaseModel;

/**
 * Class ApiProvider
 * @package App\Models\PaymentAndObligation
 */
class ApiProvider extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'api_providers';

    /**
     * @var array
     */
    protected $fillable = [
        'ip_address',
        'token',
    ];

    /**
     * Function to get Provider
     *
     * @param $token
     * @return mixed
     */
    public function getProvider($token)
    {
        return ApiProvider::select('id', 'ip_address')->where('token', $token)->first();
    }

    /**
     * Function to check is the ip address from provider
     *
     * @param $provider
     * @param $ipAddress
     * @return bool
     */
    public function checkIpAddressByProvider($provider, $ipAddress)
    {
        $getIps = explode(',', str_replace(' ', '', $provider->ip_address));

        return in_array($ipAddress, $getIps);
    }
}
