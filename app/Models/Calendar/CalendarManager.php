<?php

namespace App\Models\Calendar;

use App\Models\BaseMlManager;
use App\Models\BaseModel;
use Illuminate\Support\Facades\DB;

/**
 * Class CalendarManager
 * @package App\Models\Calendar
 */
class CalendarManager
{
    use BaseMlManager;

    /**
     * Function to store data
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $year = $data['year'] ?? '';
        $tableStatus = Calendar::where(['year' => $year, 'agency_id' => 0])->first();

        //if table data exist update table,else insert data into DB
        if ($tableStatus) {
            $this->updateData($data, $year);
        } else {
            $this->storeData($data, $year);
        }
    }

    /**
     * Function to store data into DB
     *
     * @param $nonWorkingDays
     * @param $year
     * @param null $workingHours
     * @param null $agencyId
     * @param null $yearStartDate
     * @param null $yearEndDate
     */
    public static function storeData($nonWorkingDays, $year, $workingHours = null, $agencyId = null, $yearStartDate = null, $yearEndDate = null)
    {
        DB::transaction(function () use ($nonWorkingDays, $year, $workingHours, $agencyId, $yearStartDate, $yearEndDate) {

            //Current year
            $now = now();
            $now->year = $year;

            //Start date and end date
            $yearStartDateCarbon = $yearStartDate ?? $now->copy()->startOfYear();
            $yearEndDateCarbon = $yearEndDate ?? $now->copy()->endOfYear();

            $i = 1;
            $dates = [];
            //while year start date less than year end date insert data to DB and add 1 day to start date
            while ($yearStartDateCarbon->lt($yearEndDateCarbon)) {

                $yearStartDate = $yearStartDateCarbon->toDateString();
                $dates[$i]['year'] = $year;
                $dates[$i]['agency_id'] = $agencyId ?? 0;
                $dates[$i]['date_from'] = $yearStartDate . ' ' . ($workingHours['working_hours_start'] ?? config('swis.working_hours_start'));
                $dates[$i]['date_to'] = $yearStartDate . ' ' . ($workingHours['lunch_hours_start'] ?? config('swis.lunch_hours_start'));
                $dates[$i]['show_status'] = in_array($yearStartDate, $nonWorkingDays['date']) ? BaseModel::STATUS_INACTIVE : BaseModel::STATUS_ACTIVE;
                ++$i;
                $dates[$i]['year'] = $year;
                $dates[$i]['agency_id'] = $agencyId ?? 0;
                $dates[$i]['date_from'] = $yearStartDate . ' ' . ($workingHours['lunch_hours_end'] ?? config('swis.lunch_hours_end'));
                $dates[$i]['date_to'] = $yearStartDate . ' ' . ($workingHours['working_hours_end'] ?? config('swis.working_hours_end'));
                $dates[$i]['show_status'] = in_array($yearStartDate, $nonWorkingDays['date']) ? BaseModel::STATUS_INACTIVE : BaseModel::STATUS_ACTIVE;
                ++$i;

                // add 1 day to start date
                $yearStartDateCarbon->addDays(1);
            }

            //Sw Admin added dates into calendar table
            Calendar::insert($dates);

            //When agency have dates in calendar table before SW Admin added non working days,update calendar table for agencies
            (new self)->updateDatesForAgencies($nonWorkingDays, $year);
        });
    }

    /**
     * Function to update data into DB
     *
     * @param $data
     * @param $year
     */
    private function updateData($data, $year)
    {
        DB::transaction(function () use ($data, $year) {

            $now = now()->toDateString();
            Calendar::where('year', $year)->whereDate('date_from', '>', $now)->update(['show_status' => BaseModel::STATUS_ACTIVE]);

            if ($data['date']) {
                foreach ($data['date'] as $date) {
                    Calendar::uniqueFields($year, $date)->update(['show_status' => BaseModel::STATUS_INACTIVE]);
                }
            }
        });
    }

    /**
     * Function to update agency data when Sw Admin added dates
     *
     * @param $nonWorkingDays
     * @param $year
     */
    private function updateDatesForAgencies($nonWorkingDays, $year)
    {
        $now = now()->toDateString();

        Calendar::where('year', $year)
            ->where('agency_id', '!=', 0)
            ->whereDate('date_from', '>', $now)
            ->update(['show_status' => BaseModel::STATUS_ACTIVE]);

        if ($nonWorkingDays['date']) {
            foreach ($nonWorkingDays['date'] as $nonWorkingDays) {
                Calendar::uniqueFields($year, $nonWorkingDays)
                    ->where('agency_id', '!=', 0)
                    ->update(['show_status' => BaseModel::STATUS_INACTIVE]);
            }
        }
    }

    /**
     * Function to create or update agency calendar
     *
     * @param $data
     * @param $agencyId
     */
    public static function createOrUpdateAgencyCalendar($data, $agencyId)
    {
        $year = now()->year;
        $now = now()->toDateString();

        if (isset($data['working_hours_start']) && isset($data['working_hours_end']) && isset($data['lunch_hours_start']) && isset($data['lunch_hours_end'])) {

            $workingHours = [
                'working_hours_start' => $data['working_hours_start'],
                'working_hours_end' => $data['working_hours_end'],
                'lunch_hours_start' => $data['lunch_hours_start'],
                'lunch_hours_end' => $data['lunch_hours_end'],
            ];

            $agencyCalendar = Calendar::where(['year' => $year, 'agency_id' => $agencyId])->first();
            $agencyNonWorkingDays['date'] = [];

            if ($agencyCalendar) {

                //update calendar table by year and agency_id
                $agencyNonWorkingDays['date'] = Calendar::getNonWorkingDays($year);
                Calendar::where(['year' => $year, 'agency_id' => $agencyId])->whereDate('date_from', '>', $now)->delete();
                (new self)->storeData($agencyNonWorkingDays, $year, $workingHours, $agencyId, now()->addDays(1));

            } else {
                //select calendar default values when agency_id=0 and insert into calendar
                $calendars = Calendar::where(['year' => $year, 'agency_id' => 0])->first();

                if ($calendars) {
                    $agencyNonWorkingDays['date'] = Calendar::getNonWorkingDays($year);
                    if (count($agencyNonWorkingDays['date'])) {
                        (new self)->storeData($agencyNonWorkingDays, $year, $workingHours, $agencyId);
                    }
                } else {
                    //when calendar table empty
                    (new self)->storeData($agencyNonWorkingDays, $year, $workingHours, $agencyId);
                }
            }
        } else {
            Calendar::where(['year' => $year, 'agency_id' => $agencyId])->delete();
        }
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @param null $parentId
     * @return array
     */
    public function getLogData($id = null, $parentId = null)
    {
        if (!is_null($id)) {
            $data = Calendar::where('id', $id)->first();
            $logData = $data;
            $logData['mls'] = $data->ml()->get()->toArray();

            return $logData;
        }
        return [];
    }
}
