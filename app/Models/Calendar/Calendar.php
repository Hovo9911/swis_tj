<?php

namespace App\Models\Calendar;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

/**
 * Class Calendar
 * @package App\Models\Calendar
 */
class Calendar extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['year', 'date_from', 'date_to'];

    /**
     * @var string
     */
    public $table = 'calendar';

    /**
     * @var bool
     */
    protected $hasAdminInfo = true;

    /**
     * @var array
     */
    protected $fillable = [
        'agency_id',
        'year',
        'date_from',
        'date_to',
        'show_status'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'created_admin_id',
        'created_admin_ip',
        'updated_at',
        'updated_admin_id',
        'updated_admin_ip',
        'deleted_admin_id',
        'deleted_admin_ip'
    ];

    /**
     * Function to get unique fields
     *
     * @param $query
     * @param $year
     * @param $date
     * @return Builder
     */
    public function scopeUniqueFields($query, $year, $date)
    {
        return $query->where(['year' => $year])
            ->whereDate('date_from', '=', $date)
            ->whereDate('date_to', '=', $date);
    }

    /**
     * Function to get non working days in year for agency
     *
     * @param $year
     * @param $agencyId
     * @return array
     */
    public static function getNonWorkingDays($year, $agencyId = 0)
    {
        return array_unique(self::select(DB::raw('DATE(date_from) AS date'))
            ->where(['year' => $year, 'agency_id' => $agencyId, 'show_status' => BaseModel::STATUS_INACTIVE])
            ->pluck('date')
            ->toArray());
    }

    /**
     * Function to get date_from,date_to from calendar for agency
     *
     * @param $startDate
     * @param $endDate
     * @param $agencyId
     * @return array
     */
    public static function getCalendarDates($startDate, $endDate, $agencyId)
    {
        $agencyWorkingHours = Calendar::select('date_from', 'date_to')
            ->where('date_from', '>=', $startDate)
            ->where('date_to', '<=', $endDate)
            ->where('agency_id', $agencyId)
            ->orderByAsc('date_to')
            ->active()
            ->get();

        if (!$agencyWorkingHours->count()) {
            $agencyWorkingHours = Calendar::select('date_from', 'date_to')
                ->where('date_from', '>=', $startDate)
                ->where('date_to', '<=', $endDate)
                ->where('agency_id', 0)
                ->orderByAsc('date_to')
                ->active()
                ->get();
        }

        return $agencyWorkingHours;
    }

    /**
     * Function to get next working range
     *
     * @param $dateTo
     * @param $agencyId
     * @return mixed
     */
    public static function getNextWorkingRange($dateTo, $agencyId)
    {
        $nextDateTo = Calendar::select('date_from', 'date_to')
            ->where('date_to', '>', $dateTo)
            ->where('agency_id', $agencyId)
            ->orderByAsc('date_to')
            ->active()
            ->first();

        if (is_null($nextDateTo)) {
            $nextDateTo = Calendar::select('date_from', 'date_to')
                ->where('date_to', '>', $dateTo)
                ->where('agency_id', 0)
                ->orderByAsc('date_to')
                ->active()
                ->first();
        }
        return $nextDateTo;
    }

    /**
     * Function to get next date_from
     *
     * @param $date
     * @param $type
     * @param $agencyId
     * @return Calendar
     */
    public static function nextDate($date, $type, $agencyId)
    {
        $nextDateFrom = Calendar::where($type, '>=', $date)
            ->where('agency_id', $agencyId)
            ->orderByAsc($type)
            ->active()
            ->pluck($type)
            ->first();
        if (is_null($nextDateFrom)) {
            $nextDateFrom = Calendar::where($type, '>=', $date)
                ->where('agency_id', 0)
                ->orderByAsc($type)
                ->active()
                ->pluck($type)
                ->first();
        }

        return $nextDateFrom;
    }

    /**
     * Function to get previous date_to
     *
     * @param $date
     * @param $agencyId
     * @return Calendar
     */
    public static function previousDateTo($date, $agencyId)
    {
        $dateTo = Calendar::where('date_to', '<=', $date)
            ->where('agency_id', $agencyId)
            ->orderByDesc('date_to')
            ->active()
            ->pluck('date_to')
            ->first();

        if (is_null($dateTo)) {
            $dateTo = Calendar::where('date_to', '<=', $date)
                ->where('agency_id', 0)
                ->orderByDesc('date_to')
                ->active()
                ->pluck('date_to')
                ->first();
        }

        return $dateTo;
    }

    /**
     * Function to get previous date_from
     *
     * @param $date
     * @param $agencyId
     * @return Calendar
     */
    public static function currentDateFrom($date, $agencyId)
    {
        $dateTo = Calendar::where('date_from', '<=', $date)
            ->where('agency_id', $agencyId)
            ->orderByDesc('date_from')
            ->active()
            ->pluck('date_from')
            ->first();
        if (is_null($dateTo)) {
            $dateTo = Calendar::where('date_from', '<=', $date)
                ->where('agency_id', 0)
                ->orderByDesc('date_from')
                ->active()
                ->pluck('date_from')
                ->first();
        }

        return $dateTo;
    }

    /**
     * Function to get current date_to
     *
     * @param $date
     * @param $agencyId
     * @return Calendar
     */
    public static function currentDateTo($date, $agencyId)
    {
        $dateTo = Calendar::where('date_to', '>=', $date)
            ->where('agency_id', $agencyId)
            ->orderByAsc('date_to')
            ->active()
            ->pluck('date_to')
            ->first();
        if (is_null($dateTo)) {
            $dateTo = Calendar::where('date_to', '>=', $date)
                ->where('agency_id', 0)
                ->orderByAsc('date_to')
                ->active()
                ->pluck('date_to')
                ->first();
        }

        return $dateTo;
    }

    /**
     * Function to check $date is working hour or not
     *
     * @param $date
     * @param $agencyId
     * @return Calendar
     */
    public static function checkWorkingHour($date, $agencyId)
    {
        $checkWorkingHour = Calendar::select('id')
            ->where('date_from', '<=', $date)
            ->where('date_to', '>=', $date)
            ->where('agency_id', $agencyId)
            ->orderByAsc('date_to')
            ->active()
            ->first();

        if (is_null($checkWorkingHour)) {
            $checkWorkingHour = Calendar::select('id')
                ->where('date_from', '<=', $date)
                ->where('date_to', '>=', $date)
                ->where('agency_id', 0)
                ->orderByAsc('date_to')
                ->active()
                ->first();
        }

        return $checkWorkingHour;
    }
}
