<?php

namespace App\Models\SingleApplicationSubApplicationCertificates;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

/**
 * Class SingleApplicationSubApplicationProducts
 * @package App\Models\SingleApplicationSubApplicationProducts
 */
class SingleApplicationSubApplicationCertificates extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['sub_application_id', 'user_id'];

    /**
     * @var string
     */
    public $incrementing = 'false';

    /**
     * @var string
     */
    public $table = 'saf_sub_applications_certificates';

    /**
     * @var array
     */
    protected $fillable = [
        'sub_application_id',
        'user_id',
        'file_name',
        'sub_application_status',
    ];
}
