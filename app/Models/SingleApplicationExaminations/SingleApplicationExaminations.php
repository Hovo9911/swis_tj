<?php

namespace App\Models\SingleApplicationExaminations;

use App\Models\BaseModel;

/**
 * Class SingleApplicationExaminations
 * @package App\Models\SingleApplication
 */
class SingleApplicationExaminations extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'saf_examinations';

    /**
     * @var array
     */
    protected $fillable = [
        'saf_number',
        'saf_subapplication_id',
        'document_id',
        'saf_product_id',
        'type_of_good',
        'type_of_metal',
        'weight',
        'quantity',
        'measurement_unit',
        'type_of_stone',
        'piece',
        'carat'
    ];
}