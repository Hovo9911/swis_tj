<?php

namespace App\Models\SingleApplicationExaminations;

use App\Models\BaseMlManager;
use Illuminate\Support\Facades\DB;

/**
 * Class SingleApplicationExaminationsManager
 * @package App\Models\SingleApplicationExamination
 */
class SingleApplicationExaminationsManager
{
    use BaseMlManager;

    /**
     * Function to Store data to DB
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $singleApplicationExamination = new SingleApplicationExaminations($data);

        DB::transaction(function () use ($data, $singleApplicationExamination) {
            $singleApplicationExamination->save();
        });
    }

}
