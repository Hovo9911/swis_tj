<?php

namespace App\Models\SingleApplicationExpertiseIndicators;

use App\Models\BaseModel;

/**
 * Class SingleApplicationExpertiseIndicators
 * @package App\Models\SingleApplicationExpertiseIndicators
 */
class SingleApplicationExpertiseIndicators extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'saf_expertise_indicators';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'saf_expertise_id',
        'indicator_code',
        'actual_result',
        'adequacy',
        'status_send',
    ];
}
