<?php

namespace App\Models\SingleApplicationSubApplicationStates;

use App\Models\Agency\Agency;
use App\Models\BaseModel;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\SingleApplicationNotes\SingleApplicationNotes;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class SingleApplicationSubApplicationStates
 * @package App\Models\SingleApplicationSubApplicationStates
 */
class SingleApplicationSubApplicationStates extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'saf_sub_application_states';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'creator_user_id',
        'saf_number',
        'saf_id',
        'saf_subapplication_id',
        'mapping_id',
        'state_id',
        'is_current',
    ];

    /**
     * Function to get created_at value
     *
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format(config('swis.date_time_format_front'));
    }

    /**
     * Function to relate with agency
     *
     * @return HasOne
     */
    public function agency()
    {
        return $this->hasOne(Agency::class, 'id', 'agency_id');
    }

    /**
     * Function to relate with constructor_documents_roles_attribute
     *
     * @return HasOne
     */
    public function document()
    {
        return $this->hasOne(ConstructorDocument::class, 'id', 'constructor_document_id');
    }

    /**
     * Function to relate with ConstructorStates
     *
     * @return HasOne
     */
    public function state()
    {
        return $this->hasOne(ConstructorStates::class, 'id', 'state_id');
    }

    /**
     * Function to relate with SingleApplicationSubApplications
     *
     * @return HasOne
     */
    public function subApplication()
    {
        return $this->hasOne(SingleApplicationSubApplications::class, 'id', 'saf_subapplication_id');
    }

    /**
     * Function to Store data to DB
     *
     * @param array $data
     * @param $userDocumentsRequest
     * @param $toSafHolder
     */
    public static function store(array $data, $userDocumentsRequest, $toSafHolder = false)
    {
        self::create($data);

        //--------------------- SAF notes part ---------------------//

        $constructorApprovedState = null;
        if (isset($userDocumentsRequest['action'])) {
            $note = "|ACTION_" . $userDocumentsRequest['action'] . "|" . "|STATE_" . $userDocumentsRequest['state'] . "|";
            $transKey = SingleApplicationNotes::NOTE_TRANS_KEY_SUB_APP_ACTION_COMPLETE_NEW_STATE;

            $constructorApprovedState = ConstructorStates::select('id')->where(['id' => $userDocumentsRequest['state'], 'state_type' => ConstructorStates::END_STATE_TYPE, 'state_approved_status' => ConstructorStates::END_STATE_APPROVED])->first();
        } else {
            $user = User::where('id', $userDocumentsRequest['user_id'])->first();
            $note = $user->name();

            $transKey = SingleApplicationNotes::NOTE_TRANS_KEY_SUB_APP_SUPERSCRIBE_CREATE;
        }

        // SubApplication Note
        SingleApplicationNotes::storeNotes(['saf_number' => $data['saf_number'], 'note' => $note, 'sub_application_id' => $data['saf_subapplication_id'], 'send_from_module' => SingleApplicationNotes::NOTE_SEND_MODULE_APPLICATION, 'to_saf_holder' => $toSafHolder], $transKey);

        // Saf Note
        if (!is_null($constructorApprovedState)) {
            $note = "|STATE_" . $userDocumentsRequest['state'] . "|";
            SingleApplicationNotes::storeNotes(['saf_number' => $data['saf_number'], 'note' => $note, 'sub_application_id' => $data['saf_subapplication_id'], 'send_from_module' => SingleApplicationNotes::NOTE_SEND_MODULE_APPLICATION, 'to_saf_holder' => self::TRUE], SingleApplicationNotes::NOTE_TRANS_KEY_SAF_SUB_APP_ACTION_COMPLETE_NEW_STATE);
        }

        //--------------------- End SAF notes part ---------------------//
    }
}
