<?php

namespace App\Models\SingleApplicationDocuments;

use App\Models\BaseModel;
use App\Models\Document\Document;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationDocumentProducts\SingleApplicationDocumentProducts;
use App\Models\SingleApplicationDocumentSubApplications\SingleApplicationDocumentSubApplications;
use App\Models\SingleApplicationExpertise\SingleApplicationExpertise;
use App\Models\SingleApplicationProducts\SingleApplicationProducts;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class SingleApplicationDocuments
 * @package App\Models\SingleApplicationDocuments
 */
class SingleApplicationDocuments extends BaseModel
{
    /**
     * @var string
     */
    const ADDED_FROM_MODULE_SAF = 'saf';
    const ADDED_FROM_MODULE_APPLICATION = 'application';
    const ADDED_FROM_MODULE_LABORATORY = 'laboratory';

    /**
     * @var string
     */
    public $table = 'saf_documents';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'company_tax_id',
        'user_id',
        'creator_user_id',
        'saf_number',
        'saf_id',
        'document_id',
        'state_id',
        'subapplication_id',
        'expertise_id',
        'added_from_module'
    ];

    /**
     * Function to get created_at value
     *
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format(config('swis.date_time_format_front'));
    }

    /**
     * Function to relate with SingleApplicationDocumentProducts
     *
     * @return HasMany
     */
    public function documentProducts()
    {
        return $this->hasMany(SingleApplicationDocumentProducts::class, 'saf_document_id', 'id');
    }

    /**
     * Function to relate with SingleApplicationDocumentSubApplications
     *
     * @return HasMany
     */
    public function documentSubApplications()
    {
        return $this->hasMany(SingleApplicationDocumentSubApplications::class, 'saf_document_id', 'id');
    }

    /**
     * Function to relate with SingleApplicationDocumentProducts
     *
     * @return HasManyThrough
     */
    public function productList()
    {
        return $this->hasManyThrough(SingleApplicationProducts::class, SingleApplicationDocumentProducts::class, 'saf_document_id', 'id', 'id', 'product_id')->orderByAsc();
    }

    /**
     * Function to relate with Document
     *
     * @return HasOne
     */
    public function document()
    {
        return $this->hasOne(Document::class, 'id', 'document_id');
    }

    /**
     * Function to relate with SingleApplication
     *
     * @return HasOne
     */
    public function saf()
    {
        return $this->hasOne(SingleApplication::class, 'regular_number', 'saf_number');
    }

    /**
     * Function to relate with SingleApplicationSubApplications
     *
     * @return HasOne
     */
    public function subApplication()
    {
        return $this->hasOne(SingleApplicationSubApplications::class, 'id', 'subapplication_id');
    }

    /**
     * Function to relate with User
     *
     * @return HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * Function to relate with SingleApplicationExpertise
     *
     * @return HasOne
     */
    public function expertise()
    {
        return $this->hasOne(SingleApplicationExpertise::class, 'id', 'expertise_id');
    }

    /**
     * Function to check can user delete document
     *
     * @return bool
     */
    public function canDeleteDocument()
    {
        // check document exist in submitted sub application
        $documentProducts = $this->documentProducts->pluck('product_id')->toArray();
        $disabledProducts = SingleApplicationSubApplications::subApplicationDisabledProducts($this->saf_number);
        if (count(array_intersect($disabledProducts, $documentProducts))) {
            return false;
        }

        // check sub application is submitted
        $documentSubApplications = $this->documentSubApplications->pluck('sub_application_id')->toArray();
        $submittedSubApplications = SingleApplicationSubApplications::where('saf_number', $this->saf_number)->submittedSubApplication()->pluck('id')->toArray();
        if (count(array_intersect($submittedSubApplications, $documentSubApplications))) {
            return false;
        }

        return true;
    }

    /**
     * Function to get product Numbers
     *
     * @param $subApplicationProductIds
     * @return string
     */
    public function productNumbers($subApplicationProductIds)
    {
        $documentProductNumbers = [];

        if (!is_null($this->productList)) {

            foreach ($this->productList->pluck('id')->all() as $documentProductId) {

                if ($subApplicationProductIds && isset($subApplicationProductIds[$documentProductId])) {
                    $documentProductNumbers[] = $subApplicationProductIds[$documentProductId];
                }
            }
        }

        return count($documentProductNumbers) ? implode(',', $documentProductNumbers) : "";
    }

    /**
     * Function to get product ids
     *
     * @return string
     */
    public function productIdList()
    {
        $productIds = [];
        if (!is_null($this->productList)) {
            $productIds = $this->productList->pluck('id')->toArray();
        }

        return count($productIds) ? implode(',', $productIds) : "";
    }

    /**
     * Function to get Sub Application types and ids
     *
     * @param $documentSubApplications
     * @param string $type
     * @return string
     */
    public function subApplicationsList($documentSubApplications, $type = 'id')
    {
        $subApplicationIds = $documentSubApplications->pluck('sub_application_id')->toArray();

        $subApplications = [];
        if (count($subApplicationIds)) {
            if ($type == 'code') {
                $subApplications = SingleApplicationSubApplications::join('reference_classificator_of_documents', 'saf_sub_applications.reference_classificator_id', '=', 'reference_classificator_of_documents.id')->whereIn('saf_sub_applications.id', $subApplicationIds)->pluck('reference_classificator_of_documents.code')->toArray();
            }

            if ($type == 'id') {
                $subApplications = $subApplicationIds;
            }
        }

        return count($subApplications) ? implode(',', $subApplications) : "";
    }
}
