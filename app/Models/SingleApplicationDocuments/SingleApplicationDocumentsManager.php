<?php

namespace App\Models\SingleApplicationDocuments;

use App\EntityTypesInfo\LegalEntityForms;
use App\Exceptions\TaxServiceTjCompanyInactiveException;
use App\Exceptions\TaxServiceTjInnNotFoundException;
use App\Exceptions\TaxServiceTjInvalidResponseException;
use App\GUploader;
use App\Models\BaseModel;
use App\Models\Document\Document;
use App\Models\Document\DocumentManager;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationDocumentProducts\SingleApplicationDocumentProducts;
use App\Models\SingleApplicationDocumentSubApplications\SingleApplicationDocumentSubApplications;
use App\Models\SingleApplicationExpertise\SingleApplicationExpertise;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class SingleApplicationDocumentsManager
 * @package App\Models\SingleApplicationDocuments
 */
class SingleApplicationDocumentsManager extends BaseModel
{
    /**
     * @var DocumentManager
     */
    public $documentManager;

    /**
     * SingleApplicationDocumentsManager constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->documentManager = new DocumentManager();
    }

    /**
     * Function to Store data to DB
     *
     * @param array $data
     * @param $safNumber
     */
    public function storeDocumentProducts(array $data, $safNumber)
    {
        if (count($data)) {

            $saf = SingleApplication::select('id')->where('regular_number', $safNumber)->firstOrFail();

            DB::transaction(function () use ($data, $saf, $safNumber) {

                $disabledProducts = SingleApplicationSubApplications::subApplicationDisabledProducts($safNumber);

                foreach ($data as $value) {
                    if (!empty($value['document_id'])) {

                        $addedFromModule = !empty($value['added_from_module']) ? $value['added_from_module'] : SingleApplicationDocuments::ADDED_FROM_MODULE_SAF;

                        $safDocument = SingleApplicationDocuments::where(['saf_number' => $safNumber, 'document_id' => $value['document_id'], 'added_from_module' => $addedFromModule])->first();

                        if (is_null($safDocument)) {

                            $value['user_id'] = Auth::user()->getCreatorUser()->id;
                            $value['creator_user_id'] = Auth::id();
                            $value['company_tax_id'] = Auth::user()->companyTaxId();
                            $value['saf_number'] = $safNumber;
                            $value['saf_id'] = $saf->id;
                            $value['added_from_module'] = $addedFromModule;

                            $safDocument = SingleApplicationDocuments::create($value);
                        }

                        if (!empty($value['products'])) {
                            $products = explode(',', $value['products']);

                            $notDeletedProducts = [];
                            foreach ($products as $pr_id) {
                                $notDeletedProducts[] = (int)$pr_id;

                                // TODO open creator_user_id when all data from is cleared
                                $documentProduct = [
//                                'creator_user_id' => Auth::id(),
                                    'company_tax_id' => Auth::user()->companyTaxId(),
                                    'saf_number' => $safNumber,
                                    'product_id' => (int)$pr_id,
                                    'saf_document_id' => $safDocument->id,
                                ];

                                $documentProductStoreData = [
                                    'user_id' => Auth::user()->getCreatorUser()->id
                                ];

                                $documentProductStoreData = array_merge($documentProduct, $documentProductStoreData);

                                SingleApplicationDocumentProducts::updateOrCreate($documentProduct, $documentProductStoreData);
                            }

                            $notDeletedProducts = array_merge($notDeletedProducts, $disabledProducts);

                            SingleApplicationDocumentProducts::whereNotIn('product_id', $notDeletedProducts)->where('saf_document_id', $safDocument->id)->delete();
                        } else {
                            SingleApplicationDocumentProducts::where('saf_document_id', $safDocument->id)->whereNotIn('product_id', $disabledProducts)->delete();
                        }
                    }
                }
            });
        }
    }

    /**
     * Function to Store Sub applications
     *
     * @param array $data
     * @param $safNumber
     */
    public function storeDocumentSubApplications(array $data, $safNumber)
    {
        if (count($data)) {

            $saf = SingleApplication::select('id', 'regular_number')->where('regular_number', $safNumber)->firstOrFail();
            $allFormedSubApplications = SingleApplicationSubApplications::where('saf_number', $saf->regular_number)->formedSubApplication()->pluck('id')->toArray();

            DB::transaction(function () use ($data, $saf, $allFormedSubApplications, $safNumber) {

                foreach ($data as $value) {
                    if (!empty($value['document_id'])) {

                        $addedFromModule = SingleApplicationDocuments::ADDED_FROM_MODULE_SAF;

                        $safDocument = SingleApplicationDocuments::where(['saf_number' => $safNumber, 'document_id' => $value['document_id'], 'added_from_module' => $addedFromModule])->first();

                        if (is_null($safDocument)) {

                            $value['user_id'] = Auth::user()->getCreatorUser()->id;
                            $value['creator_user_id'] = Auth::id();
                            $value['company_tax_id'] = Auth::user()->companyTaxId();
                            $value['saf_number'] = $safNumber;
                            $value['saf_id'] = $saf->id;
                            $value['added_from_module'] = $addedFromModule;

                            $safDocument = SingleApplicationDocuments::create($value);
                        }

                        if (!empty($value['sub_applications'])) {
                            $subApplications = explode(',', $value['sub_applications']);

                            $notDeletedSubApplications = [];
                            foreach ($subApplications as $subApplicationId) {

                                $subApplication = SingleApplicationSubApplications::select('id', 'agency_id', 'reference_classificator_id')->where(['id' => $subApplicationId, 'saf_number' => $safNumber])->firstOrFail();

                                $notDeletedSubApplications[] = (int)$subApplicationId;

                                $documentSubApplication = [
//                                'creator_user_id' => Auth::id(),
                                    'company_tax_id' => Auth::user()->companyTaxId(),
                                    'saf_number' => $safNumber,
                                    'sub_application_id' => (int)$subApplicationId,
                                    'saf_document_id' => $safDocument->id,
                                    'sub_application_agency_id' => $subApplication->agency_id,
                                    'sub_applicaiton_classificator_id' => $subApplication->reference_classificator_id,
                                ];

                                $documentSubApplicationStoreData = [
                                    'user_id' => Auth::user()->getCreatorUser()->id
                                ];

                                $documentSubApplicationStoreData = array_merge($documentSubApplication, $documentSubApplicationStoreData);

                                SingleApplicationDocumentSubApplications::updateOrCreate($documentSubApplication, $documentSubApplicationStoreData);
                            }

                            $notDeletedSubApplications = array_unique(array_merge($notDeletedSubApplications, $allFormedSubApplications));

                            SingleApplicationDocumentSubApplications::whereNotIn('sub_application_id', $notDeletedSubApplications)->where('saf_document_id', $safDocument->id)->delete();
                        } else {
                            SingleApplicationDocumentSubApplications::where('saf_document_id', $safDocument->id)->whereNotIn('sub_application_id', $allFormedSubApplications)->delete();
                        }
                    }
                }

            });
        }
    }

    /**
     * Function to Store data to DB when creating from SAF
     *
     * @param $dataDoc
     * @param $safNumber
     * @return Document
     *
     * @throws TaxServiceTjCompanyInactiveException
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function storeDocumentsFromSaf($dataDoc, $safNumber)
    {
        $saf = SingleApplication::select('id', 'regime', 'status_type', 'data')->where('regular_number', $safNumber)->first();

        $referenceClassificator = getReferenceRows(ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS, false, $dataDoc['document_type']);
        $authUserCreator = Auth::user()->getCreator();

        if ($saf->regime == SingleApplication::REGIME_EXPORT || $saf->regime == SingleApplication::REGIME_IMPORT) {

            $holderData = $dataDoc['saf']['sides'][$saf->regime];
            if ($saf->status_type == SingleApplication::SEND_TYPE) {
                $holderData = $saf->data['saf']['sides'][$saf->regime];
            } else {
                // Holder Data if Company/Natural Person
                if (isset($holderData['type'])) {

                    $legalEntityForms = LegalEntityForms::getInstance();
                    $holderServiceData = $legalEntityForms->holderDataByType($holderData['type'], $holderData['type_value'], $holderData['passport'] ?? null, true, '');

                    $holderData = array_merge($holderData, $holderServiceData);
                }
            }

            $data['holder_type'] = $holderData['type'];
            $data['holder_type_value'] = $holderData['type_value'];
            $data['holder_name'] = $holderData['name'];
            $data['holder_country'] = $holderData['country'] ?? '-';
            $data['holder_community'] = $holderData['community'] ?? '-';
            $data['holder_address'] = $holderData['address'] ?? '-';
            $data['holder_phone_number'] = $holderData['phone_number'];
            $data['holder_email'] = $holderData['email'];
            $data['holder_passport'] = $holderData['passport'] ?? null;
        }

        if ($saf->regime == SingleApplication::REGIME_TRANSIT) {
            $data['holder_type'] = $authUserCreator->type;
            $data['holder_type_value'] = $authUserCreator->ssn;
            $data['holder_name'] = $authUserCreator->name;
            $data['holder_country'] = $authUserCreator->country;
            $data['holder_community'] = $authUserCreator->community;
            $data['holder_address'] = $authUserCreator->address;
            $data['holder_phone_number'] = $authUserCreator->phone_number;
            $data['holder_email'] = $authUserCreator->email;
            $data['holder_passport'] = $authUserCreator->passport;
        }

        $data['document_type_id'] = optional($referenceClassificator)->id ?? 0;
        $data['document_type'] = optional($referenceClassificator)->code ?? '';
        $data['document_name'] = optional($referenceClassificator)->name ?? '';

        $data['document_description'] = $dataDoc['document_description'];
        $data['document_number'] = $dataDoc['document_number'];
        $data['document_release_date'] = $dataDoc['document_release_date'];
        $data['document_file'] = $dataDoc['document_file'];
        $data['document_status'] = Document::DOCUMENT_STATUS_REGISTERED;
        $data['created_from_module'] = Document::DOCUMENT_CREATE_MODULE_SAF;

        $data['approve_type'] = Document::APPROVE_OTHER;
        $data['document_form'] = Document::DOCUMENT_FORM_PAPER;
        $data['uuid'] = $this->documentManager->generateUniqueCode();
        $data['user_id'] = Auth::user()->getCreatorUser()->id;
        $data['creator_user_id'] = Auth::id();
        $data['company_tax_id'] = Auth::user()->companyTaxId();
        $data['document_access_password'] = $this->documentManager->generateUniqueCode();

        $data['creator_type'] = $authUserCreator->type;
        $data['creator_type_value'] = $authUserCreator->ssn;
        $data['creator_name'] = $authUserCreator->name;
        $data['creator_country'] = $authUserCreator->country;
        $data['creator_address'] = $authUserCreator->address;
        $data['creator_phone_number'] = $authUserCreator->phone_number;
        $data['creator_email'] = $authUserCreator->email;

        $data['show_status'] = Document::STATUS_ACTIVE;
        $data['subject_for_approvement'] = $data['subject_for_approvement'] ?? Document::FALSE;

        $document = new Document($data);

        // Upload file
        if (!empty($data['document_file'])) {
            $gUploader = new GUploader('document_report');
            $document->document_file = $gUploader->storePerm($data['document_file'], $document->generateFilePermPath());
        }


        DB::transaction(function () use ($dataDoc, $document, $safNumber) {

            $document->save();

            $singleAppDocProducts[] = [
                'products' => $dataDoc['document_products'],
                'document_id' => $document->id,
                'added_from_module' => SingleApplicationDocuments::ADDED_FROM_MODULE_SAF
            ];

            $data[] = [
                'document_id' => $document->id,
                'sub_applications' => $dataDoc['document_sub_applications'],
                'added_from_module' => SingleApplicationDocuments::ADDED_FROM_MODULE_SAF
            ];

            $this->storeDocumentSubApplications($data, $safNumber);
            $this->storeDocumentProducts($singleAppDocProducts, $safNumber);

        });

        return $document;
    }

    /**
     * Function to Store data to DB when creating from Sub application
     *
     * @param $dataDoc
     * @param $safNumber
     * @return Document
     */
    public function storeDocumentsFromUserDocument($dataDoc, $safNumber)
    {
        $saf = SingleApplication::select('regime', 'data')->where('regular_number', $safNumber)->first();

        $referenceClassificator = getReferenceRows(ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS, false, $dataDoc['document_type']);
        $authUserCreator = Auth::user()->getCreator();

        if ($saf->regime == SingleApplication::REGIME_EXPORT || $saf->regime == SingleApplication::REGIME_IMPORT) {
            $holderData = $saf->data['saf']['sides'][$saf->regime];

            $referenceLegalEntityValues = ReferenceTable::legalEntityValues();

            if (isset($referenceLegalEntityValues[$holderData['type']])) {
                $holderData['type'] = $referenceLegalEntityValues[$holderData['type']];
            }

            $data['holder_type'] = $holderData['type'];
            $data['holder_type_value'] = $holderData['type_value'];
            $data['holder_name'] = $holderData['name'];
            $data['holder_country'] = $holderData['country'] ?? '-';
            $data['holder_community'] = $holderData['community'] ?? '-';
            $data['holder_address'] = $holderData['address'] ?? '-';
            $data['holder_phone_number'] = $holderData['phone_number'];
            $data['holder_email'] = $holderData['email'];
            $data['holder_passport'] = $holderData['passport'] ?? null;
        }

        if ($saf->regime == SingleApplication::REGIME_TRANSIT) {
            $data['holder_type'] = $authUserCreator->type;
            $data['holder_type_value'] = $authUserCreator->ssn;
            $data['holder_name'] = $authUserCreator->name;
            $data['holder_country'] = $authUserCreator->country;
            $data['holder_community'] = $authUserCreator->community;
            $data['holder_address'] = $authUserCreator->address;
            $data['holder_phone_number'] = $authUserCreator->phone_number;
            $data['holder_email'] = $authUserCreator->email;
            $data['holder_passport'] = $authUserCreator->passport;
        }

        $data['document_type_id'] = optional($referenceClassificator)->id ?? 0;
        $data['document_type'] = optional($referenceClassificator)->code ?? '';
        $data['document_name'] = optional($referenceClassificator)->name ?? '';

        $data['document_description'] = $dataDoc['document_description'];
        $data['document_number'] = $dataDoc['document_number'];
        $data['document_release_date'] = $dataDoc['document_release_date'];
        $data['document_file'] = $dataDoc['document_file'];
        $data['document_status'] = Document::DOCUMENT_STATUS_REGISTERED;
        $data['created_from_module'] = Document::DOCUMENT_CREATE_MODULE_APPLICATION;

        $data['approve_type'] = Document::APPROVE_OTHER;
        $data['document_form'] = Document::DOCUMENT_FORM_PAPER;
        $data['uuid'] = $this->documentManager->generateUniqueCode();
        $data['user_id'] = Auth::user()->getCreatorUser()->id;
        $data['creator_user_id'] = Auth::id();
        $data['company_tax_id'] = Auth::user()->companyTaxId();
        $data['document_access_password'] = $this->documentManager->generateUniqueCode();

        $data['creator_type'] = $authUserCreator->type;
        $data['creator_type_value'] = $authUserCreator->ssn;
        $data['creator_name'] = $authUserCreator->name;
        $data['creator_country'] = $authUserCreator->country;
        $data['creator_address'] = $authUserCreator->address;
        $data['creator_phone_number'] = $authUserCreator->phone_number;
        $data['creator_email'] = $authUserCreator->email;

        $data['show_status'] = Document::STATUS_ACTIVE;
        $data['subject_for_approvement'] = $data['subject_for_approvement'] ?? Document::FALSE;

        $document = new Document($data);

        // Upload file
        if (!empty($data['document_file'])) {
            $gUploader = new GUploader('document_report');
            $document->document_file = $gUploader->storePerm($data['document_file'], $document->generateFilePermPath());
        }

        DB::transaction(function () use ($dataDoc, $document, $safNumber) {

            $document->save();

            $singleAppDocProducts[] = [
                'products' => $dataDoc['document_products'],
                'state_id' => $dataDoc['state_id'] ?? null,
                'subapplication_id' => $dataDoc['subapplication_id'] ?? null,
                'document_id' => $document->id,
                'added_from_module' => SingleApplicationDocuments::ADDED_FROM_MODULE_APPLICATION
            ];

            $this->storeDocumentProducts($singleAppDocProducts, $safNumber);

        });

        return $document;
    }

    /**
     * Function to Store data to DB when creating from User Laboratories
     *
     * @param $dataDoc
     * @param $safNumber
     * @return Document
     */
    public function storeDocumentsFromLaboratory($dataDoc, $safNumber)
    {
        $saf = SingleApplication::select('regime', 'data')->where('regular_number', $safNumber)->first();

        $referenceClassificator = getReferenceRows(ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS, false, $dataDoc['document_type']);
        $authUserCreator = Auth::user()->getCreator();

        if ($saf->regime == SingleApplication::REGIME_EXPORT || $saf->regime == SingleApplication::REGIME_IMPORT) {

            $holderData = $saf->data['saf']['sides'][$saf->regime];

            $referenceLegalEntityValues = ReferenceTable::legalEntityValues();

            if (isset($referenceLegalEntityValues[$holderData['type']])) {
                $holderData['type'] = $referenceLegalEntityValues[$holderData['type']];
            }

            $data['holder_type'] = $holderData['type'];
            $data['holder_type_value'] = $holderData['type_value'];
            $data['holder_name'] = $holderData['name'];
            $data['holder_country'] = $holderData['country'];
            $data['holder_community'] = $holderData['community'] ?? '-';
            $data['holder_address'] = $holderData['address'] ?? '-';
            $data['holder_phone_number'] = $holderData['phone_number'];
            $data['holder_email'] = $holderData['email'];
            $data['holder_passport'] = $holderData['passport'] ?? null;
        }

        if ($saf->regime == SingleApplication::REGIME_TRANSIT) {
            $data['holder_type'] = $authUserCreator->type;
            $data['holder_type_value'] = $authUserCreator->ssn;
            $data['holder_name'] = $authUserCreator->name;
            $data['holder_country'] = $authUserCreator->country;
            $data['holder_community'] = $authUserCreator->community;
            $data['holder_address'] = $authUserCreator->address;
            $data['holder_phone_number'] = $authUserCreator->phone_number;
            $data['holder_email'] = $authUserCreator->email;
            $data['holder_passport'] = $authUserCreator->passport;
        }

        $data['document_type_id'] = optional($referenceClassificator)->id ?? 0;
        $data['document_type'] = optional($referenceClassificator)->code ?? '';
        $data['document_name'] = optional($referenceClassificator)->name ?? '';

        $data['document_description'] = $dataDoc['document_description'];
        $data['document_number'] = $dataDoc['document_number'];
        $data['document_release_date'] = $dataDoc['document_release_date'];
        $data['document_file'] = $dataDoc['document_file'];
        $data['created_from_module'] = Document::DOCUMENT_CREATE_MODULE_LABORATORY;
        $data['document_status'] = Document::DOCUMENT_STATUS_REGISTERED;

        $data['approve_type'] = Document::APPROVE_OTHER;
        $data['document_form'] = Document::DOCUMENT_FORM_PAPER;
        $data['uuid'] = $this->documentManager->generateUniqueCode();
        $data['user_id'] = Auth::user()->getCreatorUser()->id;
        $data['creator_user_id'] = Auth::id();
        $data['document_access_password'] = $this->documentManager->generateUniqueCode();
        $data['company_tax_id'] = Auth::user()->companyTaxId();

        $data['creator_type'] = $authUserCreator->type;
        $data['creator_type_value'] = $authUserCreator->ssn;
        $data['creator_name'] = $authUserCreator->name;
        $data['creator_country'] = $authUserCreator->country;
        $data['creator_address'] = $authUserCreator->address;
        $data['creator_phone_number'] = $authUserCreator->phone_number;
        $data['creator_email'] = $authUserCreator->email;

        $data['show_status'] = Document::STATUS_ACTIVE;
        $data['subject_for_approvement'] = $data['subject_for_approvement'] ?? Document::FALSE;

        $document = new Document($data);

        DB::transaction(function () use ($dataDoc, $document, $safNumber) {

            $document->save();

            $singleAppDocProducts[] = [
                'products' => $dataDoc['document_products'],
                'document_id' => $document->id,
                'expertise_id' => $dataDoc['expertise_id'],
                'added_from_module' => SingleApplicationDocuments::ADDED_FROM_MODULE_LABORATORY
            ];

            $this->storeDocumentProducts($singleAppDocProducts, $safNumber);

        });

        return $document;
    }

    /**
     * Function to delete documents from saf module
     *
     * @param $documentIds
     * @param $safNumber
     */
    public function deleteDocumentsFromSaf($documentIds, $safNumber)
    {
        SingleApplicationDocuments::where(['company_tax_id' => Auth::user()->companyTaxId(), 'saf_number' => $safNumber, 'added_from_module' => SingleApplicationDocuments::ADDED_FROM_MODULE_SAF])
            ->whereIn('document_id', $documentIds)
            ->delete();
    }

    /**
     * Function to delete documents from My application module
     *
     * @param $documentIds
     * @param $safNumber
     * @param $subApplicationId
     */
    public function deleteDocumentsFromMyApplication($documentIds, $safNumber, $subApplicationId)
    {
        $singleAppDocument = SingleApplicationDocuments::where([
            'company_tax_id' => Auth::user()->companyTaxId(),
            'saf_number' => $safNumber,
            'subapplication_id' => $subApplicationId,
            'added_from_module' => SingleApplicationDocuments::ADDED_FROM_MODULE_APPLICATION
        ])->whereIn('document_id', $documentIds);


        if (!Auth::user()->isLocalAdmin()) {
            $singleAppDocument->where('user_id', Auth::id());
        }

        $singleAppDocument->delete();
    }

    /**
     * Function to get documents need to show in SAF
     *
     * @param $safNumber
     * @return SingleApplicationDocuments[]
     */
    public function getSafDocuments($safNumber)
    {
        $attachedDocumentProducts = SingleApplicationDocuments::with(['document', 'documentProducts', 'documentSubApplications', 'productList'])->select('saf_documents.added_from_module', 'saf_documents.document_id', 'saf_documents.id', 'saf_documents.saf_number')->join('document', 'saf_documents.document_id', '=', 'document.id')
            ->leftJoin('saf_document_products', 'saf_documents.id', '=', 'saf_document_products.saf_document_id')
            ->where('saf_documents.saf_number', $safNumber)
            ->groupBy(['saf_documents.document_id', 'saf_documents.id', 'saf_documents.added_from_module'])
            ->where(function ($q) {
                $q->orWhere('added_from_module', SingleApplicationDocuments::ADDED_FROM_MODULE_SAF)->orWhereNotNull('saf_document_products.product_id')->where('added_from_module', '!=', SingleApplicationDocuments::ADDED_FROM_MODULE_LABORATORY);
            })->orderBy('saf_documents.id');

        // if Saf has sub application in Laboratory for expertise  (if laboratory expertise and send back to saf show attached documents) -- START --
        $labExpertiseIds = SingleApplicationExpertise::where('saf_number', $safNumber)
            ->join('saf_expertise_indicators', 'saf_expertise.id', '=', 'saf_expertise_indicators.saf_expertise_id')
            ->where('saf_expertise_indicators.status_send', SingleApplicationExpertise::STATUS_ACTIVE)
            ->pluck('saf_expertise.id')->unique()->all();

        if (count($labExpertiseIds)) {
            $attachedDocumentProducts->orWhere('added_from_module', SingleApplicationDocuments::ADDED_FROM_MODULE_LABORATORY)->whereIn('expertise_id', $labExpertiseIds);
        }
        // if Saf has sub application in Laboratory for expertise -- END --

        return $attachedDocumentProducts->get();
    }

    /**
     * Function to get products of document
     *
     * @param $safNumber
     * @param $documentId
     * @return SingleApplicationDocumentProducts
     */
    public function getSafDocumentProductIds($safNumber, $documentId)
    {
        return SingleApplicationDocumentProducts::where(['saf_document_products.saf_number' => $safNumber, 'document_id' => $documentId])
            ->join('saf_products', 'saf_document_products.product_id', '=', 'saf_products.id')
            ->join('saf_documents', 'saf_documents.id', '=', 'saf_document_products.saf_document_id')
            ->pluck('saf_products.id')
            ->all();
    }
}
