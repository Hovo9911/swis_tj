<?php

namespace App\Models\SingleApplicationObligations;

use App\GUploader;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplicationObligationReceipts\SingleApplicationObligationReceipts;
use App\Models\Transactions\TransactionsManager;
use App\Models\UserDocuments\UserDocumentsNotificationsManager;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class SingleApplicationObligationsManager
 * @package App\Models\SingleApplicationObligations
 */
class SingleApplicationObligationsManager
{
    /**
     * Function to get single window payment
     *
     * @param $safNumber
     * @param $subApplicationId
     * @param $constructorStateId
     */
    public function storeSubApplicationObligationAndTransaction($safNumber, $subApplicationId, $constructorStateId)
    {
        $budgetLine = getReferenceRows(ReferenceTable::REFERENCE_OBLIGATION_BUDGET_LINE, false, SingleApplicationObligations::SAF_OBLIGATIONS_FIX_CODE_FOR_SEND_APPLICATION_OBLIGATION);
        $obligation = new SingleApplicationObligations();

        $userBalance = Auth::user()->balance();
        $sum = SingleApplicationObligations::SAF_OBLIGATIONS_PRICE_FOR_SEND_APPLICATION_OBLIGATION;

        $obligation->user_id = Auth::id();
        $obligation->creator_user_id = Auth::id();
        $obligation->company_tax_id = Auth::user()->companyTaxId();
        $obligation->saf_number = $safNumber;
        $obligation->state_id = $constructorStateId;
        $obligation->subapplication_id = $subApplicationId;
        $obligation->ref_budget_line_id = optional($budgetLine)->id;
        $obligation->obligation_code = SingleApplicationObligations::SAF_OBLIGATIONS_FIX_CODE_FOR_SEND_APPLICATION_OBLIGATION;
        $obligation->payment = $sum;
        $obligation->payment_date = Carbon::now();
        $obligation->obligation = $sum;
        $obligation->pdf_hash_key = str_random(64);

        if(Auth::user()->isAgency()){
            $obligation->is_paid = true;
        }

        DB::transaction(function () use ($safNumber, $sum, $userBalance, $obligation) {

            $userType = SingleApplicationObligations::SAF_OBLIGATIONS_TYPE_NATURAL_PERSON;
            if (Auth::user()->isSwAdmin()) {
                $userType = SingleApplicationObligations::SAF_OBLIGATIONS_TYPE_SW_ADMIN;
            } elseif (Auth::user()->isAgency() || Auth::user()->isCompany()) {
                $userType = SingleApplicationObligations::SAF_OBLIGATIONS_TYPE_AGENCY;
            } elseif (Auth::user()->isBroker()) {
                $userType = SingleApplicationObligations::SAF_OBLIGATIONS_TYPE_BROKER;
            }

            $transactionsManager = new TransactionsManager();
            $transactionsManager->store([
                'payment_id' => null,
                'saf_number' => $safNumber,
                'user_id' => Auth::user()->id,
                'company_tax_id' => Auth::user()->companyTaxId(),
                'type' => 'output',
                'transaction_date' => Carbon::now(),
                'amount' => -$sum,
                'total_balance' => number_format($userBalance - $sum, 2, '.', '')
            ]);

            $obligation->payer_user_type = $userType;
            $obligation->payer_company_tax_id = Auth::user()->companyTaxId();
            $obligation->payer_user_id = Auth::id();
            $obligation->save();
        });
    }

    /**
     * Function to pay saf obligations from saf module
     *
     * @param $safNumber
     * @param $sum
     * @param $obligationIds
     */
    public function paySafObligations($safNumber, $sum, $obligationIds)
    {
        $userBalance = Auth::user()->balance();

        $obligations = SingleApplicationObligations::select('id', 'obligation', 'payment', 'payment_date')->whereIn('id', $obligationIds)->get();

        DB::transaction(function () use ($safNumber, $sum, $userBalance, $obligations) {

            foreach ($obligations as $obligation) {
                $userType = SingleApplicationObligations::SAF_OBLIGATIONS_TYPE_NATURAL_PERSON;
                if (Auth::user()->isSwAdmin()) {
                    $userType = SingleApplicationObligations::SAF_OBLIGATIONS_TYPE_SW_ADMIN;
                } elseif (Auth::user()->isAgency() || Auth::user()->isCompany()) {
                    $userType = SingleApplicationObligations::SAF_OBLIGATIONS_TYPE_AGENCY;
                } elseif (Auth::user()->isBroker()) {
                    $userType = SingleApplicationObligations::SAF_OBLIGATIONS_TYPE_BROKER;
                }

                $transactionsManager = new TransactionsManager();
                $transactionsManager->store([
                    'payment_id' => null,
                    'saf_number' => $safNumber,
                    'user_id' => Auth::user()->id,
                    'company_tax_id' => Auth::user()->companyTaxId(),
                    'type' => 'output',
                    'transaction_date' => Carbon::now(),
                    'amount' => -$sum,
                    'total_balance' => number_format($userBalance - $sum, 2, '.', ''),
                    'obligation_id' => $obligation->id
                ]);

                $obligation->payment = $obligation->obligation;
                $obligation->payment_date = currentDateTime();
                $obligation->payer_user_type = $userType;
                $obligation->payer_company_tax_id = Auth::user()->companyTaxId();
                $obligation->payer_user_id = Auth::id();
                $obligation->is_paid = true;
                $obligation->save();

                $userDocumentsNotificationsManager = new UserDocumentsNotificationsManager();
                $userDocumentsNotificationsManager->storeNotification($obligation);
            }
        });

    }

    /**
     * Function to update or create obligation receipt
     *
     * @param $data
     */
    public function storeObligationReceipts($data)
    {
        $gUploader = new GUploader('obligation_receipt');
        $gUploader->storePerm($data['file_name']);

        SingleApplicationObligationReceipts::updateOrCreate([
            'saf_obligation_id' => $data['saf_obligation_id'],
        ], [
            'user_id' => Auth::id(),
            'saf_obligation_id' => $data['saf_obligation_id'],
            'file_name' => $data['file_name'],
        ]);
    }

    /**
     * Function to delete obligation receipt
     *
     * @param $obligationId
     */
    public function deleteObligationReceipts($obligationId)
    {
        $receipt = SingleApplicationObligationReceipts::where('saf_obligation_id', $obligationId)->first();

        $file = Storage::disk('uploads')->path($receipt->file_name);

        if (file_exists($file)) {
            unlink($file);
        }

        $receipt->delete();
    }
}