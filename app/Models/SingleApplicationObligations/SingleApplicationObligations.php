<?php

namespace App\Models\SingleApplicationObligations;

use App\Facades\Mailer;
use App\Models\BaseModel;
use App\Models\Company\Company;
use App\Models\ConstructorMappingToRule\ConstructorMappingToRule;
use App\Models\ConstructorRules\ConstructorRules;
use App\Models\Document\Document;
use App\Models\Languages\Language;
use App\Models\Notifications\Notifications;
use App\Models\Notifications\NotificationsManager;
use App\Models\NotificationTemplates\NotificationTemplates;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationObligationReceipts\SingleApplicationObligationReceipts;
use App\Models\User\User;
use App\Sms\SmsManager;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class SingleApplicationObligations
 * @package App\Models\SingleApplicationObligations
 */
class SingleApplicationObligations extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'saf_obligations';

    /**
     * @var bool
     */
    private $alreadyGeneratedFreeSum = false;

    /**
     * @var string
     */
    const SAF_OBLIGATIONS_TYPE_NATURAL_PERSON = 'natural_person';
    const SAF_OBLIGATIONS_TYPE_SW_ADMIN = 'sw_admin';
    const SAF_OBLIGATIONS_TYPE_AGENCY = 'agency';
    const SAF_OBLIGATIONS_TYPE_BROKER = 'broker';

    /**
     * @var int
     */
    const SAF_OBLIGATIONS_PRICE_FOR_SEND_APPLICATION_OBLIGATION = 59;

    /**
     * @var string
     */
    const SAF_OBLIGATIONS_FIX_CODE_FOR_SEND_APPLICATION_OBLIGATION = 'ЕО_59';

    /**
     * @var string
     */
    const SAF_OBLIGATIONS_PAYMENT_FOR_SINGLE_WINDOW_SERVICE_CODE = '0000';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'creator_user_id',
        'company_tax_id',
        'saf_number',
        'state_id',
        'saf_id',
        'subapplication_id',
        'type',
        'name',
        'budget_line',
        'obligation_code',
        'ref_budget_line_id',
        'payment',
        'balance',
        'pdf_hash_key',
        'obligation',
        'show_status',
        'is_paid'
    ];

    /**
     * Function to relate with Saf
     *
     * @return HasOne
     */
    public function saf()
    {
        return $this->hasOne(SingleApplication::class, 'regular_number', 'saf_number');
    }

    /**
     * Function to relate with Obligation receipts
     *
     * @return HasOne
     */
    public function obligationReceipt()
    {
        return $this->hasOne(SingleApplicationObligationReceipts::class, 'saf_obligation_id', 'id');
    }

    /**
     * Function to get file basename
     *
     * @return string
     */
    public function receiptFileBaseName()
    {
        if ($this->receiptFilePath()) {
            $file = pathinfo($this->receiptFilePath());

            return $file['basename'];
        }

        return "";
    }

    /**
     * Function to get receipt file path
     *
     * @return string
     */
    public function receiptFilePath()
    {
        if ($this->obligationReceipt->file_name) {
            return url('files/uploads/files/' . $this->obligationReceipt->file_name);
        }

        return "";
    }

    /**
     * Function to check obligation is payed
     *
     * @return bool
     */
    public function isObligationPayed()
    {
        if ($this->obligation == $this->payment) {
            return true;
        }

        return false;
    }

    /**
     * Function to get Sum obligations
     *
     * @param $field
     * @param $companyTaxId
     * @param null $obligationIds
     * @return float
     */
    public static function getObligationsSum($field, $companyTaxId, $obligationIds = null)
    {
        $sum = SingleApplicationObligations::select([
            DB::raw('CAST(saf_obligations.obligation AS DECIMAL(10,2)) as obligation'),
            DB::raw('CAST(saf_obligations.payment AS DECIMAL(10,2)) as payment'),
            DB::raw('CAST(saf_obligations.balance AS DECIMAL(10,2)) as balance'),
        ])
            ->join('saf_sub_applications', 'saf_sub_applications.id', '=', 'saf_obligations.subapplication_id')
            ->join('saf', 'saf.regular_number', '=', 'saf_sub_applications.saf_number')
            ->leftJoin('users', 'users.id', '=', 'saf.creator_user_id')
            ->join('constructor_documents', function ($query) {
                $query->on('constructor_documents.document_code', '=', 'saf_sub_applications.document_code')->where('constructor_documents.show_status', '1');
            })
            ->join('constructor_documents_ml', function ($query) {
                $query->on('constructor_documents_ml.constructor_documents_id', '=', 'constructor_documents.id')->where('constructor_documents_ml.lng_id', cLng('id'));
            })
            ->join('reference_obligation_budget_line', function ($query) {
                $query->on('reference_obligation_budget_line.id', '=', 'saf_obligations.ref_budget_line_id');
//                ->where('reference_obligation_budget_line.show_status', '1')
            })
            ->join('reference_obligation_budget_line_ml', 'reference_obligation_budget_line_ml.reference_obligation_budget_line_id', '=', 'reference_obligation_budget_line.id')
            ->where('reference_obligation_budget_line_ml.lng_id', cLng('id'));

        if (!is_null($obligationIds)) {
            $sum->whereIn('saf_obligations.id', $obligationIds);
        }
        if (Auth::user()) {
            if (!Auth::user()->isNaturalPerson() && !Auth::user()->isAuthorizedNaturalPerson()) {
                if (isset($companyTaxId)) {
                    $sum = $sum->where('saf_obligations.company_tax_id', $companyTaxId);
                }
            } else {
                $sum = $sum->where('saf.creator_user_id', Auth::id());
            }
        }

        return $sum->sum($field);
    }

    /**
     * Function to generate and store obligation into database
     *
     * @param $data
     * @param $singleApplicationSubApp
     * @param $mappingId
     * @param $safData
     * @param $stateId
     * @param array $freeSumData
     * @return array|false|string
     */
    public static function generateObligation($data, $singleApplicationSubApp, $mappingId, $safData, $stateId, $freeSumData)
    {
        $self = (new self);
        $obligationData = $customDataFields = [];
        $rules = ConstructorMappingToRule::select('constructor_rules.*')->where('constructor_mapping_id', $mappingId)
            ->join('constructor_rules', 'constructor_rules.id', '=', 'constructor_mapping_to_rule.rule_id')
            ->get();

        foreach ($rules as $rule) {

            switch ($rule->type) {
                case ConstructorRules::OBLIGATION_TYPE:
                    $obligationData = $self->getObligationTypes($rule->id, $singleApplicationSubApp, $obligationData, $data, $stateId, $freeSumData);
                    break;
                case ConstructorRules::CALCULATION_TYPE:
                    $customDataFields = $self->calculationTypes($rule, $safData, $singleApplicationSubApp, $customDataFields, $stateId);
                    break;
            }

        }

        self::insert($obligationData);

        return $customDataFields;
    }

    /**
     * Function to get obligation type
     *
     * @param $ruleId
     * @param $singleApplicationSubApp
     * @param $obligationData
     * @param $data
     * @param $stateId
     * @param array $freeSumData
     * @return array
     */
    private function getObligationTypes($ruleId, $singleApplicationSubApp, $obligationData, $data, $stateId, $freeSumData)
    {
        $rule = ConstructorRules::select('constructor_rules.type', 'constructor_rules.obligation_code', 'reference_obligation_budget_line.id', 'reference_obligation_budget_line.code', 'reference_obligation_budget_line.budget_line', 'reference_obligation_budget_line.obligation_sum', 'reference_obligation_creation_type.code as obligation_creation_type')
            ->join('reference_obligation_budget_line', function ($query) {
                $query->on('reference_obligation_budget_line.code', '=', 'constructor_rules.obligation_code')->where('reference_obligation_budget_line.show_status', '1');
            })
            ->join('reference_obligation_type', 'reference_obligation_type.id', '=', 'reference_obligation_budget_line.budget_line')
            ->join('reference_obligation_creation_type', 'reference_obligation_creation_type.id', '=', 'reference_obligation_budget_line.obligation_creation_type')
            ->where('constructor_rules.id', $ruleId)
            ->first();

        if ($rule) {
            $exists = SingleApplicationObligations::where(['obligation_code' => $rule->obligation_code, 'saf_number' => $singleApplicationSubApp->saf_number, 'state_id' => $stateId, 'subapplication_id' => $singleApplicationSubApp->id])->exists();
            if ($rule->obligation_creation_type == ConstructorRules::CREATION_TYPE_FREE_SUM_CODE) {
                $exists = false;
            }

            if (!$exists) {
                $type = $rule->obligation_creation_type;
                $isFreeSum = false;
                $price = 0.00;

                switch ($type) {
                    case ConstructorRules::CREATION_TYPE_RULE_CODE:
                        $price = 0.00;
                        $isFreeSum = false;
                        break;
                    case ConstructorRules::CREATION_TYPE_FIXED_SUM_CODE:
                        $price = $rule->obligation_sum;
                        $isFreeSum = false;
                        break;
                    case ConstructorRules::CREATION_TYPE_PERCENTAGE_CODE:
                        $percentage = $rule->obligation_sum;
                        $availableProductsSum = $singleApplicationSubApp->productList->sum('total_value_national');
                        $price = ($availableProductsSum * $percentage) / 100;
                        $isFreeSum = false;
                        break;
                    case ConstructorRules::CREATION_TYPE_FREE_SUM_CODE:
                        $isFreeSum = true;
                        if (!$this->alreadyGeneratedFreeSum) {
                            if ($freeSumData && count($freeSumData) > 0) {
                                foreach ($freeSumData as $freeSum) {
                                    $obligationData[] = [
                                        'user_id' => Auth::id(),
                                        'saf_number' => $data->saf_number,
                                        'state_id' => $stateId,
                                        'company_tax_id' => $data->company_tax_id,
                                        'creator_company_tax_id' => optional($data->saf)->company_tax_id,
                                        'subapplication_id' => $data->id,
                                        'budget_line' => $freeSum['budget_line'],
                                        'free_sum_budget_line_code' => $freeSum['obligation_type'],
                                        'ref_budget_line_id' => $freeSum['obligation_type'],
                                        'is_free_sum' => true,
                                        'obligation_code' => $rule->code,
                                        'obligation' => $freeSum['obligation'],
                                        'payment' => 0,
                                        'created_at' => currentDateTime(),
                                        'pdf_hash_key' => str_random(64)
                                    ];

                                    $this->sendObligationNotification(Auth::id(), $data->company_tax_id, $singleApplicationSubApp->document_number, $singleApplicationSubApp);
                                }
                                $this->alreadyGeneratedFreeSum = true;
                            }
                        }
                        break;
                }

                if (!$isFreeSum) {
                    $obligationData[] = [
                        'user_id' => Auth::id(),
                        'saf_number' => $data->saf_number,
                        'state_id' => $stateId,
                        'company_tax_id' => $data->company_tax_id,
                        'creator_company_tax_id' => optional($data->saf)->company_tax_id,
                        'subapplication_id' => $data->id,
                        'budget_line' => $rule->budget_line,
                        'free_sum_budget_line_code' => null,
                        'ref_budget_line_id' => $rule->id,
                        'is_free_sum' => false,
                        'obligation_code' => $rule->code,
                        'obligation' => $price,
                        'payment' => 0,
                        'created_at' => currentDateTime(),
                        'pdf_hash_key' => str_random(64)
                    ];
                }

                return $obligationData;
            }
        }

        return $obligationData;
    }

    /**
     * Function to get calculation type
     *
     * @param $rule
     * @param $safData
     * @param $subApplication
     * @param $customDataFields
     * @param $stateId
     * @return array
     */
    private function calculationTypes($rule, $safData, $subApplication, $customDataFields, $stateId)
    {
        $results = [];
        $isMatchCondition = checkCondition($safData, $rule->condition, 'obligation', $subApplication, $stateId);

        if ($isMatchCondition) {

            $result = matchRules($safData, $rule->rule, $subApplication, $stateId);

            if ($result) {
                $result = json_decode($result);

                if ($result) {
                    foreach ($result as $key => $item) {
                        $key = str_replace('FIELD_ID_', '', $key);
                        $results[$key] = $item;
                    }
                }
            }
        }

        if (!is_null($subApplication->custom_data)) {
            $subApplication->custom_data = array_merge($subApplication->custom_data, $results);
            $subApplication->save();
        }

        return array_merge($customDataFields, $results);
    }

    /**
     * Function to check is all obligations paid
     *
     * @param $applicationId
     * @return bool
     */
    public static function checkObligationsPays($applicationId)
    {
        $obligationsCount = self::where('subapplication_id', $applicationId)->count();
        $notPaidObligationsCount = self::where('subapplication_id', $applicationId)->whereRaw('obligation != payment')->count();

        return ($notPaidObligationsCount > 0 && $obligationsCount > 0);
    }

    /**
     * Function to get Obligations
     *
     * @param $block
     * @param $saf
     * @param $application
     * @return array
     */
    public static function getObligations($block, $saf, $application)
    {
        $balanceSum = number_format(0, 2, '.', '');
        $columns = [];
        $getColumns = (array)$block->columns;
        $hideColumns = (array)$block->columnsNotShowInView ?? [];
        foreach ($getColumns as $column) {
            if (!in_array($column, $hideColumns)) {
                $columns[] = trans("swis.single_app.{$column}");
            }
        }

        $obligations = [];

        $safObligations = SingleApplicationObligations::select([
            'saf_obligations.id',
            'saf_obligations.obligation',
            'saf_obligations.balance',
            'saf_obligations.is_free_sum',
            'saf_obligations.payment',
            'saf_obligations.payment_date',
            'saf_obligations.payer_user_id',
            'saf_obligations.payer_user_type',
            'saf_obligations.pdf_hash_key',
            DB::raw("CONCAT('(', users.ssn, ') ', users.first_name , ' ', users.last_name) as payer_person"),
            'saf_obligations.payer_company_tax_id',
            'saf_sub_applications.document_number',
            'reference_obligation_budget_line.account_number',
            DB::raw("CONCAT('(', reference_obligation_type.code , ') ', reference_obligation_type_ml.name) AS obligation_type"),
            DB::raw("CONCAT('(', reference_document_type_reference.code , ') ', reference_document_type_reference_ml.name) AS document_name")
        ])
            ->join('saf_sub_applications', 'saf_sub_applications.id', '=', "saf_obligations.subapplication_id")
            ->leftJoin('users', 'users.id', '=', DB::raw("saf_obligations.payer_user_id::integer"))
            ->join('reference_obligation_budget_line', 'reference_obligation_budget_line.id', '=', 'saf_obligations.ref_budget_line_id')
            ->join('reference_obligation_type', 'reference_obligation_type.id', '=', 'reference_obligation_budget_line.budget_line')
            ->join('reference_obligation_type_ml', function ($q) {
                $q->on('reference_obligation_type_ml.reference_obligation_type_id', '=', 'reference_obligation_type.id')->where('reference_obligation_type_ml.lng_id', cLng('id'));
            })
            ->join('reference_document_type_reference', 'reference_document_type_reference.id', '=', 'reference_obligation_budget_line.document')
            ->join('reference_document_type_reference_ml', function ($q) {
                $q->on('reference_document_type_reference_ml.reference_document_type_reference_id', '=', 'reference_document_type_reference.id')->where('reference_document_type_reference_ml.lng_id', cLng('id'));
            })
            ->where('saf_obligations.obligation_code', '!=', SingleApplicationObligations::SAF_OBLIGATIONS_FIX_CODE_FOR_SEND_APPLICATION_OBLIGATION)
            ->where("saf_obligations.subapplication_id", $application->id)
            ->get();

        $obligationIds = [];

        foreach ($safObligations as $obligation) {
            $sumBalance = number_format($obligation->obligation - $obligation->payment, 2, '.', '');
            $id = isset(explode('/', $obligation->document_number)[1]) ? explode('/', $obligation->document_number)[1] : '';
            $obligationIds[] = $obligation->id;
            if (!is_null($obligation->payer_user_type)) {
                if ($obligation->payer_user_type == SingleApplicationObligations::SAF_OBLIGATIONS_TYPE_NATURAL_PERSON) {
                    $payer = "(" . $obligation->payer_company_tax_id . ') ' . User::find($obligation->payer_user_id)->name();
                } elseif ($obligation->payer_user_type == SingleApplicationObligations::SAF_OBLIGATIONS_TYPE_AGENCY || $obligation->payer_user_type == SingleApplicationObligations::SAF_OBLIGATIONS_TYPE_BROKER) {
                    $payer = "(" . $obligation->payer_company_tax_id . ') ' . Company::getCompanyByTaxID($obligation->payer_company_tax_id)->name;
                } else {
                    $payer = "(" . $obligation->payer_company_tax_id . ') ' . trans("swis.saf_obligations.payer.{$obligation->payer_user_type}");
                }
            }

            $obligations[] = [
                'id' => $obligation->id,
                'subApplication' => $id,
                'subApplicationType' => $obligation->code,
                'documentId' => $obligation->document_number,
                'obligationType' => $obligation->obligation_type,
                'subApplicationName' => $obligation->document_name,
                'budgetLine' => $obligation->account_number,
                'obligation_sum' => number_format($obligation->obligation, 2, '.', ''),
                'payments' => number_format($obligation->payment, 2, '.', ''),
                'balance' => $sumBalance,
                'isFreeSum' => (bool)$obligation->is_free_sum,
                'paymentDate' => is_null($obligation->payment_date) ? '' : Carbon::parse($obligation->payment_date)->format(config('swis.date_time_format')),
                'payer' => isset($payer) ? $payer : '',
                'payer_person' => (trim($obligation->payer_person) == '()') ? '' : $obligation->payer_person,
                'pdf_hash_kay' => $obligation->pdf_hash_key,
            ];

            $balanceSum += $sumBalance;
        }

        return [
            'columns' => $columns,
            'obligations' => $obligations,
            'totalSum' => SingleApplicationObligations::getObligationsSum('saf_obligations.obligation', null, $obligationIds),
            'paymentSum' => SingleApplicationObligations::getObligationsSum('saf_obligations.payment', null, $obligationIds),
            'balanceSum' => $balanceSum,
        ];
    }

    /**
     * Function to get obligations for pdf
     *
     * @param $saf
     * @param $application
     * @return array
     */
    public function getObligationsForPDF($saf, $application = null)
    {
        $obligations = [];
        $balanceSum = number_format(0, 2, '.', '');

        $safObligations = SingleApplicationObligations::with(['obligationReceipt'])->select([
            'saf_obligations.id',
            'saf_obligations.saf_number',
            'saf_obligations.obligation',
            'saf_obligations.balance',
            'saf_obligations.payment',
            'saf_obligations.is_free_sum',
            'saf_obligations.payment_date',
            'saf_obligations.payer_user_id',
            'saf_obligations.pdf_hash_key',
            DB::raw("CONCAT('(', users.ssn, ') ', users.first_name , ' ', users.last_name) as payer_person"),
            'saf_obligations.payer_user_type',
            'saf_obligations.payer_company_tax_id',
            'saf_sub_applications.document_number',
            'saf_obligations.obligation_code',
            'reference_obligation_budget_line.account_number',
            DB::raw("CONCAT('(', reference_obligation_type.code , ') ', reference_obligation_type_ml.name) AS obligation_type"),
            DB::raw("CONCAT('(', reference_document_type_reference.code , ') ', reference_document_type_reference_ml.name) AS document_name")
        ])
            ->join('saf_sub_applications', 'saf_sub_applications.id', '=', "saf_obligations.subapplication_id")
            ->leftJoin('users', 'users.id', '=', DB::raw("saf_obligations.payer_user_id::integer"))
            ->join('reference_obligation_budget_line', 'reference_obligation_budget_line.id', '=', 'saf_obligations.ref_budget_line_id')
            ->join('reference_obligation_type', 'reference_obligation_type.id', '=', 'reference_obligation_budget_line.budget_line')
            ->join('reference_obligation_type_ml', function ($q) {
                $q->on('reference_obligation_type_ml.reference_obligation_type_id', '=', 'reference_obligation_type.id')->where('reference_obligation_type_ml.lng_id', cLng('id'));
            })
            ->join('reference_document_type_reference', 'reference_document_type_reference.id', '=', 'reference_obligation_budget_line.document')
            ->join('reference_document_type_reference_ml', function ($q) {
                $q->on('reference_document_type_reference_ml.reference_document_type_reference_id', '=', 'reference_document_type_reference.id')->where('reference_document_type_reference_ml.lng_id', cLng('id'));
            })
            ->where("saf_obligations.saf_number", $saf->regular_number)
            ->when($application, function ($q) use ($application) {
                $q->where("saf_obligations.subapplication_id", $application->id);
            })
            ->get();

        $obligationIds = [];
        foreach ($safObligations as $obligation) {
            if (!is_null($obligation->payer_user_type)) {
                if ($obligation->payer_user_type == SingleApplicationObligations::SAF_OBLIGATIONS_TYPE_NATURAL_PERSON) {
                    $payer = "(" . $obligation->payer_company_tax_id . ') ' . User::find($obligation->payer_user_id)->name();
                } elseif ($obligation->payer_user_type == SingleApplicationObligations::SAF_OBLIGATIONS_TYPE_AGENCY || $obligation->payer_user_type == SingleApplicationObligations::SAF_OBLIGATIONS_TYPE_BROKER) {
                    $payer = "(" . $obligation->payer_company_tax_id . ') ' . optional(Company::getCompanyByTaxID($obligation->payer_company_tax_id))->name;
                } else {
                    $payer = "(" . $obligation->payer_company_tax_id . ') ' . trans("swis.saf_obligations.payer.{$obligation->payer_user_type}");
                }
            }

            $sumBalance = number_format($obligation->obligation - $obligation->payment, 2, '.', '');
            $id = isset(explode('/', $obligation->document_number)[1]) ? explode('/', $obligation->document_number)[1] : '';
            $obligationIds[] = $obligation->id;
            $obligations[] = [
                'id' => $obligation->id,
                'subApplication' => ($obligation->obligation_code == SingleApplicationObligations::SAF_OBLIGATIONS_FIX_CODE_FOR_SEND_APPLICATION_OBLIGATION) ? '-' : $id,
                'subApplicationType' => $obligation->code,
                'documentId' => ($obligation->obligation_code == SingleApplicationObligations::SAF_OBLIGATIONS_FIX_CODE_FOR_SEND_APPLICATION_OBLIGATION) ? $obligation->saf_number : $obligation->document_number,
                'obligationType' => $obligation->obligation_type,
                'subApplicationName' => ($obligation->obligation_code == SingleApplicationObligations::SAF_OBLIGATIONS_FIX_CODE_FOR_SEND_APPLICATION_OBLIGATION) ? trans('swis.saf.obligation.single_application_form.title') : $obligation->document_name,
                'budgetLine' => $obligation->account_number,
                'obligation_sum' => number_format($obligation->obligation, 2, '.', ''),
                'payments' => number_format($obligation->payment, 2, '.', ''),
                'balance' => $sumBalance,
                'isFreeSum' => (bool)$obligation->is_free_sum,
                'paymentDate' => is_null($obligation->payment_date) ? '' : Carbon::parse($obligation->payment_date)->format(config('swis.date_format')),
                'payer' => isset($payer) ? $payer : '',
                'payer_person' => (trim($obligation->payer_person) == '()') ? '' : $obligation->payer_person,
                'pdf_hash_key' => $obligation->pdf_hash_key,
                'obligation_is_payed' => $obligation->isObligationPayed(),
                'obligation_receipt_file_name' => !is_null($obligation->obligationReceipt) ? $obligation->receiptFileBaseName() : '',
                'obligation_receipt_file_path' => !is_null($obligation->obligationReceipt) ? $obligation->receiptFilePath() : '',
            ];

            $balanceSum += $sumBalance;
        }

        return [
            'obligations' => $obligations,
            'totalSum' => SingleApplicationObligations::getObligationsSum('saf_obligations.obligation', null, $obligationIds),
            'paymentSum' => SingleApplicationObligations::getObligationsSum('saf_obligations.payment', null, $obligationIds),
            'balanceSum' => $balanceSum,
        ];
    }

    /**
     * Function to check for owner/creator
     *
     * @param $query
     * @return Builder
     */
    public function scopeSafOwnerOrCreator($query)
    {
        $userType = Auth::user()->currentUserType();
        $userTypeValue = Auth::user()->currentUserTypeValue();
        $showOthersData = Auth::user()->userShowOthersData();

        $query->where(function ($q) use ($userType, $userTypeValue) {
            $q->where('saf.regime', SingleApplication::REGIME_IMPORT)->where('saf.importer_value', $userTypeValue)->where('saf.importer_type', $userType)
                ->orWhere('saf.regime', SingleApplication::REGIME_EXPORT)->where('saf.exporter_value', $userTypeValue)->where('saf.exporter_type', $userType)
                ->orWhere('saf.applicant_type', $userType)->where('saf.applicant_value', $userTypeValue);
        });

        // User login as company/agency
        if (Auth::user()->isAgency() || Auth::user()->isCompany()) {

            if (!$showOthersData) {
                $query->where('saf.user_id', Auth::id());
            }

        }

        // User login as Authorized Natural Person
        if (Auth::user()->isAuthorizedNaturalPerson()) {

            if (!$showOthersData) {
                $query->where(['saf.creator_user_id' => Auth::id(), 'saf.company_tax_id' => Document::FALSE]);
            }
        }

        return $query;

    }

    /**
     * Function to send notifications when manually created obligation
     *
     * @param $userId
     * @param $tin
     * @param $subapplicationId
     * @param $singleApplicationSubApp
     */
    public static function sendObligationNotification($userId, $tin, $subapplicationId, $singleApplicationSubApp)
    {
        $user = User::where('id', $userId)->first();

        $constructorNotificationsAll = NotificationTemplates::where(['name' => NotificationTemplates::OBLIGATION_GENERATING_NOTIFICATION_TEMPLATE])
            ->leftJoin('notification_templates_ml', 'notification_templates.id', '=', 'notification_templates_ml.notification_template_id')
            ->get();

        $constructorNotifications = $constructorNotificationsAll->where('lng_id', cLng('id'))->first();

        $languageCode = Language::where(['id' => $user->lng_id])->pluck('code')->first();

        $url = url('/' . $languageCode . '/single-application/view/' . $singleApplicationSubApp->data['saf']['general']['regime'] . '/' . $singleApplicationSubApp->saf_number . '');
        $urlInApp = Notifications::IN_APP_URL_PREFIX . '/single-application/view/' . $singleApplicationSubApp->data['saf']['general']['regime'] . '/' . $singleApplicationSubApp->saf_number . '';

        $safURL = "<a target='_blank' href='" . $url . "' class='mark-as-read'>" . $subapplicationId . "</a>";
        $safURLInApp = "<a target='_blank' href='" . $urlInApp . "' class='mark-as-read'>" . $subapplicationId . "</a>";

        if ($constructorNotifications && $user) {

            $transKeys = [
                "{{USER_FULLNAME}}" => $user->first_name . " " . $user->last_name,
                "{{SUB_APP_ID}}" => $safURL
            ];

            $inAppTransKeys = [
                "{{USER_FULLNAME}}" => $user->first_name . " " . $user->last_name,
                "{{SUB_APP_ID}}" => $safURLInApp
            ];

            $item['notification_id'] = $constructorNotifications->id;
            $item['user_id'] = $user->id;
            $item['company_tax_id'] = $tin;
            $item['sub_application_id'] = 0;
            $item['email_notification'] = strReplaceAssoc($transKeys, $constructorNotifications->email_notification);
            $item['sms_notification'] = strReplaceAssoc($transKeys, $constructorNotifications->sms_notification);
            $item['read'] = 0;
            $item['show_status'] = Notifications::STATUS_ACTIVE;
            $item['notification_type'] = Notifications::NOTIFICATION_TYPE_CUSTOM_TEMPLATE;

            $inApplicationNotes = [];
            foreach ($constructorNotificationsAll as $constructorNote) {
                $inApplicationNotes[$constructorNote->lng_id] = [
                    'in_app_notification' => Notifications::replaceVariableToValue($inAppTransKeys, $constructorNote->in_app_notification, $constructorNote->lng_id)
                ];
            }

            // Store in app notifications
            NotificationsManager::storeInAppNotifications($item, $inApplicationNotes);

            //Store email into db
            if ($user->email && $user->email_notification) {

                $dataForMail = [
                    'subject' => $constructorNotifications->email_subject,
                    'to' => $user->email,
                    'body' => $item['email_notification'],
                ];

                Mailer::makeEmail($dataForMail);
            }

            //Store SMS into db
            if (config('swis.sms_send') && $user->sms_notification) {

                $sms = new SmsManager();

                $data = [
                    'user_id' => $user->id,
                    'phone' => trim($user->phone_number),
                    'message' => $item['sms_notification'],
                ];

                $sms->storeSms($data);
            }
        }
    }
}
