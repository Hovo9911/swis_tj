<?php

namespace App\Models\SingleApplicationSubApplicationsLaboratory;

use App\Models\SingleApplicationSubApplicationProducts\SingleApplicationSubApplicationProducts;

/**
 * Class SingleApplicationSubApplicationsLaboratoryManager
 * @package App\Models\SingleApplicationSubApplicationsLaboratoryManager
 */
class SingleApplicationSubApplicationsLaboratoryManager
{
    /**
     * Function to store data
     *
     * @param array $data
     * @param $subApplicationId
     */
    public function store(array $data, $subApplicationId)
    {
        foreach ($data as $productId => $indicators) {
            if (count($indicators) === 1) {
                continue;
            }
            if (array_key_exists('correspond', $indicators)) {
                SingleApplicationSubApplicationProducts::where('subapplication_id', $subApplicationId)->where('product_id', $productId)->update(['correspond' => $indicators['correspond']]);
                unset($indicators['correspond']);
            }

            foreach ($indicators as $indicatorId => $values) {
                if (count($values) > 1) {
                    SingleApplicationSubApplicationsLaboratory::updateOrCreate([
                        'sub_application_id' => $subApplicationId,
                        'saf_product_id' => $productId,
                        'indicator_id' => $indicatorId
                    ], [
                        'found_size' => $values['found_size'] ?? '',
                        'found_or_not' => $values['found_or_not'] ?? 2,
                        'correspond_or_not' => $values['correspond_or_not'] ?? 2,
                        'price' => $values['price'] ?? null,
                        'duration' => $values['duration'] ?? null
                    ]);
                }
            }
        }
    }

    /**
     * Function to delete data by id
     *
     * @param $id
     */
    public function delete($id)
    {

    }
}
