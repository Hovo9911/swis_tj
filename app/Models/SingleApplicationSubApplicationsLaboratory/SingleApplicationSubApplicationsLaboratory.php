<?php

namespace App\Models\SingleApplicationSubApplicationsLaboratory;

use App\Models\BaseModel;

/**
 * Class SingleApplicationSubApplicationsLaboratory
 * @package App\Models\SingleApplicationSubApplicationsLaboratory
 */
class SingleApplicationSubApplicationsLaboratory extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'saf_sub_applications_laboratory';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'sub_application_id',
        'saf_product_id',
        'indicator_id',
        'found_size',
        'found_or_not',
        'correspond_or_not',
        'price',
        'duration'
    ];

    /**
     * Function to return total indicator value
     *
     * @param $labExaminations
     * @return int
     */
    public static function totalIndicatorPrice($labExaminations)
    {
        $total = 0;

        foreach ($labExaminations as $labExamination) {
            if ($labExamination->send_lab_company_tax_id) {
                foreach ($labExamination->labIndicators ?? [] as $labIndicator) {
                    $total += $labIndicator->price ?? 0;
                }
            }
        }

        return $total;
    }
}
