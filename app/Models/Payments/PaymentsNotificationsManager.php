<?php

namespace App\Models\Payments;

use App\Models\Notifications\Notifications;
use App\Models\Notifications\NotificationsManager;
use App\Models\NotificationTemplates\NotificationTemplates;
use App\Models\User\User;

/**
 * Class PaymentsNotificationsManager
 * @package App\Models\Payments
 */
class PaymentsNotificationsManager
{
    /**
     * Function to send notifications when payment added
     *
     * @param $userId
     * @param $tin
     * @param $amount
     * @param $userBalance
     */
    public function sendPaymentNotification($userId, $tin, $amount, $userBalance)
    {
        $user = User::where('id', $userId)->first();

        $notificationTemplatesAll = NotificationTemplates::mlByCode(NotificationTemplates::PAYMENT_NOTIFICATION_TEMPLATE);

        if ($notificationTemplatesAll->count()) {
            $notificationTemplates = $notificationTemplatesAll->where('lng_id', cLng('id'))->first();

            if ($notificationTemplates && $user) {

                $transKeys = [
                    "{{USER_FULLNAME}}" => $user->first_name . " " . $user->last_name,
                    "{{AMOUNT}}" => $amount,
                    "{{USER_BALANCE}}" => $userBalance
                ];

                $item['notification_id'] = $notificationTemplates->id;
                $item['user_id'] = $user->id;
                $item['company_tax_id'] = $tin;
                $item['sub_application_id'] = 0;
                $item['email_subject'] = strip_tags(Notifications::replaceVariableToValue($transKeys, $notificationTemplates->email_subject, ($user->lng_id ?? cLng('id'))));
                $item['email_notification'] = Notifications::replaceVariableToValue($transKeys, $notificationTemplates->email_notification, ($user->lng_id ?? cLng('id')));
                $item['sms_notification'] = strip_tags(Notifications::replaceVariableToValue($transKeys, $notificationTemplates->sms_notification, ($user->lng_id ?? cLng('id'))));

                $item['read'] = Notifications::NOTIFICATION_STATUS_UNREAD;
                $item['show_status'] = Notifications::STATUS_ACTIVE;
                $item['notification_type'] = Notifications::NOTIFICATION_TYPE_CUSTOM_TEMPLATE;

                $inApplicationNotes = [];
                foreach ($notificationTemplatesAll as $constructorNote) {
                    $inApplicationNotes[$constructorNote->lng_id] = [
                        'in_app_notification' => Notifications::replaceVariableToValue($transKeys, $constructorNote->in_app_notification, $constructorNote->lng_id)
                    ];
                }

                // Store in app notifications
                NotificationsManager::storeInAppNotifications($item, $inApplicationNotes);

                // Send email and Sms (store DB too)
                NotificationsManager::sendEmailAndSms($user, $item);
            }
        }
    }
}