<?php

namespace App\Models\Laboratory;

use App\Models\BaseModel;
use App\Models\LaboratoryIndicator\LaboratoryIndicator;
use App\Models\LaboratoryUsers\LaboratoryUsers;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplicationExpertise\SingleApplicationExpertise;
use App\Models\SingleApplicationExpertiseIndicators\SingleApplicationExpertiseIndicators;
use App\Models\SingleApplicationProductsBatch\SingleApplicationProductsBatch;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * Class Laboratory
 * @package App\Models\Laboratory
 */
class Laboratory extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'laboratory';

    /**
     * @var string
     */
    const referenceTable = 'reference_loboratories';

    /**
     * @var bool
     */
    protected $hasAdminInfo = true;

    /**
     * @var string (Lab Expertise)
     */
    const SELECT_INDICATOR_BY_PRODUCT_INDICATOR = '1';
    const SELECT_PRODUCT_BY_SPHERE = '2';
    const SELECT_CUSTOM_SEND_TO_LAB = '3';

    /**
     * @var array
     */
    const LAB_TYPES = [
        'by_product_indicator' => self::SELECT_INDICATOR_BY_PRODUCT_INDICATOR,
        'by_sphere' => self::SELECT_PRODUCT_BY_SPHERE,
        'send_custom' => self::SELECT_CUSTOM_SEND_TO_LAB
    ];

    /**
     * @var null
     */
    public static $instance = null;

    /**
     * @var array
     */
    protected $fillable = [
        'company_tax_id',
        'tax_id',
        'user_id',
        'laboratory_code',
        'certificate_number',
        'certification_period_validity',
        'head_name',
        'legal_entity_name',
        'address',
        'phone_number',
        'email',
        'website_url',
        'logo',
        'show_status'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'created_admin_id',
        'created_admin_ip',
        'updated_at',
        'updated_admin_id',
        'updated_admin_ip',
        'deleted_admin_id',
        'deleted_admin_ip'
    ];

    /**
     * Function to format Certification period field
     *
     * @param $value
     * @return string
     */
    public function getCertificationPeriodValidityAttribute($value)
    {
        return Carbon::parse($value)->format(config('swis.date_time_format_front'));
    }

    /**
     * Function to relate with LaboratoryUsers
     *
     * @return HasMany
     */
    public function users()
    {
        return $this->hasMany(LaboratoryUsers::class, 'laboratory_id', 'id');
    }

    /**
     * Function to relate with LaboratoryMl all
     *
     * @return HasMany
     */
    public function ml()
    {
        return $this->hasMany(LaboratoryMl::class, 'laboratory_id', 'id');
    }

    /**
     * Function to relate with LaboratoryMl current
     *
     * @return HasOne
     */
    public function current()
    {
        return $this->hasOne(LaboratoryMl::class, 'laboratory_id', 'id')->notDeleted()->where('lng_id', cLng('id'))->first();
    }

    /**
     * Singleton
     *
     * @return Laboratory|null
     */
    public static function getInstance()
    {
        if (!is_null(self::$instance)) {
            return Self::$instance;
        }

        return (new Self);
    }

    /**
     * Function to get Laboratories Data
     *
     * @param $singleAppProducts
     * @param $safNumber
     * @param $subApplicationId
     * @param bool $edit
     * @param null $expertiseId
     * @return array
     */
    public static function getLaboratoriesData($singleAppProducts, $safNumber, $subApplicationId, $edit = false, $expertiseId = null)
    {
        $currentSubApplication = SingleApplicationSubApplications::select('id', 'agency_subdivision_id')->where('id', $subApplicationId)->first();

        $lab = self::getInstance();
        $laboratoryData = [];
        $laboratoryData['reference']['sampling_methods'] = $lab->getReference(ReferenceTable::REFERENCE_SAMPLING_TECH);
        $laboratoryData['reference']['sampler'] = $lab->getReference('lab_samplers', ['subdivision' => $currentSubApplication->agency_subdivision_id]);
        $laboratoryData['reference']['products'] = $lab->generateProducts($singleAppProducts);
        $laboratoryData['reference']['expertising_method'] = $lab->getReference(ReferenceTable::REFERENCE_TECH_REGUL);
        $laboratoryData['reference']['product_group'] = $lab->getReference(ReferenceTable::REFERENCE_TECH_REG);
        $laboratoryData['reference']['product_sub_group'] = $lab->getReference(ReferenceTable::REFERENCE_TECH_REG_CHAPTERS);
        $laboratoryData['reference']['expertising_indicator'] = $lab->getReference(ReferenceTable::REFERENCE_LABORATORY_EXPERTISE);
        $laboratoryData['list_of_view_all'] = $lab->getListOfView($safNumber, $subApplicationId);
        $laboratoryData['list_of_view'] = $laboratoryData['list_of_view_all']['singleApplicationExpertise'];
        $laboratoryData['has_lab_info'] = $laboratoryData['list_of_view_all']['hasLabInfo'];

        if ($edit) {
            $products = SingleApplicationExpertise::select('saf_product_id', 'saf_product_batches_ids')->find($expertiseId);
            $laboratoryData['selected_products'] = $products->saf_product_id;
            $laboratoryData['selected_products_batches'] = $products->saf_product_batches_ids;
            $laboratoryData['reference']['products_batches'] = SingleApplicationProductsBatch::select('id', 'batch_number')->where('saf_product_id', $products->saf_product_id)->get();
            $laboratoryData['indicators'] = $lab->getIndicatorsBySafExpertiseId($expertiseId);
        }

        return $laboratoryData;
    }

    /**
     * Function to get Indicators By Saf Expertise Id
     *
     * @param $safExpertiseId
     * @return mixed
     */
    private function getIndicatorsBySafExpertiseId($safExpertiseId)
    {
        $measurementRef = ReferenceTable::REFERENCE_MEASUREMENT_UNITS;

        $indicators = SingleApplicationExpertiseIndicators::select([
            'reference_laboratory_expertise.id',
            'reference_laboratory_expertise.code',
            'laboratory_indicator.price as indicator_price',
            'laboratory_indicator.duration',
            'reference_laboratory_expertise_ml.name',
            "{$measurementRef}_ml.name as measurement_unit",
            'laboratory_indicator.duration as laboratory_indicator_duration',
            'laboratory_indicator.date_time as date_time',
            'laboratory_ml.name as lab_name'
        ])
            ->join('saf_expertise', 'saf_expertise.id', '=', 'saf_expertise_indicators.saf_expertise_id')
            ->join('reference_laboratory_expertise', 'reference_laboratory_expertise.code', '=', 'saf_expertise_indicators.laboratory_expertise_code')
            ->join('reference_laboratory_expertise_ml', function ($query) {
                $query->on('reference_laboratory_expertise_ml.reference_laboratory_expertise_id', '=', 'reference_laboratory_expertise.id')->where("reference_laboratory_expertise_ml.lng_id", cLng('id'));
            })
            ->join($measurementRef, "{$measurementRef}.id", '=', "reference_laboratory_expertise.ind_m_unit")
            ->join("{$measurementRef}_ml", function ($query) use ($measurementRef) {
                $query->on("{$measurementRef}.id", '=', "{$measurementRef}_ml.{$measurementRef}_id")->where("{$measurementRef}_ml.lng_id", cLng('id'));
            })
            ->join('laboratory_indicator', function ($query) {
                $query->on('reference_laboratory_expertise.indicators', '=', 'laboratory_indicator.indicator_id')
                    ->on('saf_expertise.laboratory_id', '=', 'laboratory_indicator.laboratory_id')
                    ->where('laboratory_indicator.company_tax_id', Auth::user()->companyTaxId());
            })
            ->join('laboratory', function ($query) {
                $query->on('laboratory.id', '=', 'laboratory_indicator.laboratory_id')->where('laboratory_indicator.company_tax_id', '!=', "0");
            })
            ->join('laboratory_ml', function ($query) {
                $query->on('laboratory_ml.laboratory_id', '=', 'laboratory.id')->where('laboratory_ml.lng_id', cLng('id'));
            })
            ->where('saf_expertise_id', $safExpertiseId)
            ->get();

        return $indicators;
    }

    /**
     * Function to get List Of View
     *
     * @param $safNumber
     * @param bool $subApplicationId
     * @return array
     */
    public function getListOfView($safNumber, $subApplicationId = false)
    {
        $singleApplicationExpertise = SingleApplicationExpertise::select([
            'saf_expertise.id',
            'saf_expertise.sample_code',
            'saf_expertise.letter_code',
            'saf_expertise.saf_product_id',
            'saf_expertise.total_price',
            'saf_expertise_indicators.actual_result',
            'saf_expertise_indicators.adequacy',
            'saf_expertise_indicators.status_send',
            'laboratory_ml.name as lab_name',
            'laboratory.id as lab_id',
            DB::Raw("array_to_string(array_agg(reference_laboratory_expertise_ml.name), ',') as indicators")
        ])
            ->join('saf_expertise_indicators', 'saf_expertise_indicators.saf_expertise_id', '=', 'saf_expertise.id')
            ->join('reference_laboratory_expertise', 'reference_laboratory_expertise.code', '=', 'saf_expertise_indicators.laboratory_expertise_code')
            ->join('reference_laboratory_expertise_ml', function ($query) {
                $query->on('reference_laboratory_expertise_ml.reference_laboratory_expertise_id', '=', 'reference_laboratory_expertise.id')->where('reference_laboratory_expertise_ml.lng_id', cLng('id'));
            })
            ->join('laboratory', 'laboratory.id', '=', 'saf_expertise.laboratory_id')
            ->join('laboratory_ml', function ($query) {
                $query->on('laboratory_ml.laboratory_id', '=', 'laboratory.id')->where('laboratory_ml.lng_id', cLng('id'));
            })
            ->where('saf_number', $safNumber)
            ->where('saf_expertise.company_tax_id', Auth::user()->companyTaxId())
            ->groupBy([
                'saf_expertise.id',
                'saf_expertise.sample_code',
                'saf_expertise.letter_code',
                'saf_expertise.saf_product_id',
                'saf_expertise.total_price',
                'saf_expertise_indicators.actual_result',
                'saf_expertise_indicators.adequacy',
                'saf_expertise_indicators.status_send',
                'lab_name',
                'lab_id'
            ])
            ->orderBy('saf_expertise.id', 'desc');

        if ($subApplicationId) {
            $singleApplicationExpertise->where('saf_expertise.sub_application_id', $subApplicationId);
        }

        $singleApplicationExpertise = $singleApplicationExpertise->get();

        $hasLabInfo = false;

        foreach ($singleApplicationExpertise as $key => &$safExpertise) {
            $labs = LaboratoryIndicator::select('id', 'date_time', 'duration')->where('laboratory_id', $safExpertise->lab_id)->get();

            $allDurations = [];
            foreach ($labs as $lab) {
                $allDurations[$lab->id] = ($lab->date_time == "days") ? $lab->duration * 24 : $lab->duration;
            }
            $labIndicatorId = array_search(max($allDurations), $allDurations);

            $labIndicator = LaboratoryIndicator::select('duration', 'date_time')->find($labIndicatorId);
            $safExpertise->duration = $labIndicator->duration;
            $safExpertise->date_time = $labIndicator->date_time;
            if (!empty($safExpertise->actual_result) && !empty($safExpertise->adequacy)) {

                $adequacyRefTableName = ReferenceTable::REFERENCE_USER_LAB_ADEQUACY;
                $adequacyRefTableMlName = ReferenceTable::REFERENCE_USER_LAB_ADEQUACY . '_ml';
                $adequacyRef = DB::table($adequacyRefTableName)->join($adequacyRefTableMlName, $adequacyRefTableName . '.id', '=', $adequacyRefTableMlName . '.' . $adequacyRefTableName . '_id')
                    ->where($adequacyRefTableMlName . '.lng_id', '=', cLng('id'))
                    ->where($adequacyRefTableName . '.id', $safExpertise->adequacy)
                    ->first();

                if (!is_null($adequacyRef)) {
                    $singleApplicationExpertise[$key]->adequacy = $adequacyRef->name;
                }

                $hasLabInfo = true;
            }
        }

        return [
            'singleApplicationExpertise' => $singleApplicationExpertise,
            'hasLabInfo' => $hasLabInfo
        ];
    }

    /**
     * Function to get reference
     *
     * @param $tableName
     * @param array $where
     * @return array
     */
    public function getReference($tableName, $where = [])
    {
        $data = [];
        $referenceTableName = "reference_{$tableName}";

        if (substr($tableName, 0, 10) === 'reference_') {
            $referenceTableName = $tableName;
        }

        $referenceTableShowStatus = $referenceTableName . '.show_status';
        $referenceTableNameML = "{$referenceTableName}_ml";

        $referenceTable = DB::table($referenceTableName)->select("{$referenceTableName}.id", "{$referenceTableName}.code", "{$referenceTableNameML}.name")
            ->join($referenceTableNameML, "{$referenceTableNameML}.{$referenceTableName}_id", "=", "{$referenceTableName}.id")
            ->where('lng_id', cLng('id'))
            ->where($referenceTableShowStatus, Laboratory::STATUS_ACTIVE);

        if (count($where)) {
            foreach ($where as $column => $value) {
                if (Schema::hasColumn($referenceTableName, $column)) {
                    $referenceTable->where([$column => $value]);
                }
            }
        }

        $referenceTable = $referenceTable->get();

        $onlyRefNames = [ReferenceTable::REFERENCE_LABORATORY_SAMPLERS];
        foreach ($referenceTable as $item) {
            if (in_array($referenceTableName, $onlyRefNames)) {
                $data[] = [
                    'id' => $item->id,
                    'name' => "$item->name",
                ];
            } else {
                $data[] = [
                    'id' => $item->id,
                    'name' => "({$item->code}) $item->name",
                ];
            }
        }

        return $data;
    }

    /**
     * Function to generate Products
     *
     * @param $singleAppProducts
     * @param array $data
     * @return array
     */
    public function generateProducts($singleAppProducts, $data = [])
    {
        foreach ($singleAppProducts as $key => $product) {
            $refTableData = getReferenceRows(ReferenceTable::REFERENCE_HS_CODE_6, $product->product_code, false);
            $data[] = [
                'id' => $product->id,
                'name' => ++$key . " (" . optional($refTableData)->code . ")"
            ];
        }

        return $data;
    }

    /**
     * Function to get Processing Result
     *
     * @param array $data
     * @return array
     */
    public static function getProcessingResult(array $data)
    {
        if (!isset($data['expertising_method_code']) && !isset($data['expertising_indicator_code']) && !isset($data['product_group_code']) && !isset($data['product_sub_group_code'])) {
            return [];
        }
        $measurementRef = ReferenceTable::REFERENCE_MEASUREMENT_UNITS;

        $result = DB::table("reference_laboratory_expertise")
            ->select([
                'reference_laboratory_expertise.*',
                'reference_laboratory_expertise_ml.name',
                "{$measurementRef}_ml.name as measurement_unit",
                'laboratory_indicator.duration as laboratory_indicator_duration',
                'laboratory_indicator.created_at as date_time',
                'laboratory_indicator.price as indicator_price',
                'laboratory_ml.name as lab_name',
                'laboratory.id as lab_id',
                'reference_laboratory_expertise.limit'
            ])
            ->join('reference_laboratory_expertise_ml', function ($query) {
                $query->on('reference_laboratory_expertise_ml.reference_laboratory_expertise_id', '=', 'reference_laboratory_expertise.id')->where("reference_laboratory_expertise_ml.lng_id", cLng('id'));
            })
            ->join($measurementRef, "{$measurementRef}.id", '=', "reference_laboratory_expertise.ind_m_unit")
            ->join("{$measurementRef}_ml", function ($query) use ($measurementRef) {
                $query->on("{$measurementRef}.id", '=', "{$measurementRef}_ml.{$measurementRef}_id")->where("{$measurementRef}_ml.lng_id", cLng('id'));
            })
            ->join('laboratory_indicator', function ($query) {
                $query->on('reference_laboratory_expertise.indicators', '=', 'laboratory_indicator.indicator_id')->where('laboratory_indicator.company_tax_id', '!=', "0");
            })
            ->join('laboratory', function ($query) {
                $query->on('laboratory.id', '=', 'laboratory_indicator.laboratory_id')->where('laboratory.company_tax_id', Auth::user()->companyTaxId());
            })
            ->join('laboratory_ml', function ($query) {
                $query->on('laboratory_ml.laboratory_id', '=', 'laboratory.id')->where('laboratory_ml.lng_id', cLng('id'));
            })
            ->where('laboratory.show_status', self::STATUS_ACTIVE)
            ->where('laboratory_indicator.show_status', self::STATUS_ACTIVE);

        if (isset($data['expertising_method_code']) || isset($data['product_group_code'])) {
            $result->join('reference_tech_regul', 'reference_tech_regul.id', '=', 'reference_laboratory_expertise.technical_regulation');
        }

        if (isset($data['expertising_method_code'])) {
            $result->where('reference_tech_regul.id', $data['expertising_method_code'])
                ->where('reference_tech_regul.show_status', "1");
        }

        if (isset($data['product_group_code'])) {
            $result->join('reference_tech_reg', 'reference_tech_reg.regul_name', '=', 'reference_tech_regul.id')
                ->where('reference_tech_reg.id', $data['product_group_code'])
                ->where('reference_tech_reg.show_status', "1");
        }

        if (isset($data['product_sub_group_code'])) {
            $result->join('reference_tech_reg_chapters', 'reference_tech_reg_chapters.tech_reg', '=', 'reference_tech_reg.id')
                ->where('reference_tech_reg_chapters.id', $data['product_sub_group_code'])
                ->where('reference_tech_reg_chapters.show_status', "1");
        }

        if (isset($data['expertising_indicator_code'])) {
            $result->where('reference_laboratory_expertise.id', $data['expertising_indicator_code']);
        }

        return $result->get();
    }

    /**
     * Function to get Laboratory Id By Indicator
     *
     * @param $indicatorCode
     * @return mixed
     */
    public static function getLaboratoryIdByIndicator($indicatorCode)
    {
        $labExpertise = DB::table("reference_" . ReferenceTable::REFERENCE_LABORATORY_EXPERTISE)->select('id')->where('code', $indicatorCode)->where('show_status', ReferenceTable::STATUS_ACTIVE)->first();

        $laboratory = LaboratoryIndicator::select('laboratory_id')->where('indicator_id', $labExpertise->id)->first();

        return ($laboratory) ? $laboratory->laboratory_id : null;
    }
}
