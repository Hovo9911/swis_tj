<?php

namespace App\Models\Laboratory;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

/**
 * Class LaboratoryMl
 * @package App\Models\Laboratory
 */
class LaboratoryMl extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['laboratory_id', 'lng_id'];

    /**
     * @var string
     */
    public $incrementing = 'false';

    /**
     * @var string
     */
    public $table = 'laboratory_ml';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'laboratory_id',
        'lng_id',
        'name',
        'head_name',
        'description',
        'show_status'
    ];
}
