<?php

namespace App\Models\Laboratory;

use App\Http\Controllers\Core\DataTableSearch;
use App\Models\Agency\Agency;
use Illuminate\Support\Facades\DB;

/**
 * Class LaboratorySearch
 * @package App\Models\Laboratory
 */
class LaboratorySearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**legal_entity_name
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        return $query->get();
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($this->orderByCol, $this->orderByType ?? 'desc');
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $query = Laboratory::select('laboratory.*', 'laboratory_ml.laboratory_id', 'laboratory_ml.lng_id', 'laboratory_ml.name', 'laboratory_ml.head_name', 'laboratory_ml.description', 'agency_ml.name as agency_name')
            ->joinMl()
            ->join('agency', 'agency.tax_id', '=', 'laboratory.company_tax_id')
            ->join('agency_ml', 'agency.id', '=', 'agency_ml.agency_id')->where('agency.show_status', Agency::STATUS_ACTIVE)->where('agency_ml.lng_id', '=', cLng('id'));

        if (isset($this->searchData['id'])) {
            $query->where(DB::raw("laboratory.id::varchar"), $this->searchData['id']);
        }

        if (isset($this->searchData['name'])) {
            $query->where('laboratory_ml.name', 'ILIKE', '%' . strtolower(trim($this->searchData['name'])) . '%');
        }

        if (isset($this->searchData['tax_id'])) {
            $query->where('laboratory.tax_id', 'ILIKE', '%' . strtolower(trim($this->searchData['tax_id'])) . '%');
        }

        if (isset($this->searchData['laboratory_code'])) {
            $query->where('laboratory.laboratory_code', 'ILIKE', '%' . strtolower(trim($this->searchData['laboratory_code'])) . '%');
        }

        if (isset($this->searchData['address'])) {
            $query->where('address', 'ILIKE', '%' . strtolower(trim($this->searchData['address'])) . '%');
        }

        if (isset($this->searchData['agency_tax_id'])) {
            $query->where('agency.tax_id', $this->searchData['agency_tax_id']);
        }

        if (isset($this->searchData['legal_entity_name'])) {
            $query->where('laboratory.legal_entity_name', 'ILIKE', '%' . strtolower(trim($this->searchData['legal_entity_name'])) . '%');
        }

        if (isset($this->searchData['show_status'])) {
            $query->where('laboratory.show_status', $this->searchData['show_status']);
        }

        $query->checkUserHasRole();

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}


