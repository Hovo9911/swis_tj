<?php

namespace App\Models\Laboratory;

use App\Models\Agency\Agency;
use App\Models\Agency\AgencyLabIndicators;
use App\Models\BaseMlManager;
use App\Models\BaseModel;
use App\Models\LaboratoryIndicator\LaboratoryIndicator;
use App\Models\LaboratoryUsers\LaboratoryUsers;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\ReferenceTableForm\ReferenceTableFormManager;
use App\Models\RegionalOffice\RegionalOffice;
use App\Models\RegionalOffice\RegionalOfficeLaboratory;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class LaboratoryManager
 * @package App\Models\Laboratory
 */
class LaboratoryManager
{
    use BaseMlManager;

    /**
     * Function to Store data to DB
     *
     * @param array $data
     * @return Laboratory
     */
    public function store(array $data)
    {
        $data['user_id'] = Auth::id();

        if (!Auth()->user()->isSwAdmin()) {
            $data['company_tax_id'] = Auth::user()->companyTaxId();
        }

        $laboratory = new Laboratory($data);
        $laboratoryMl = new LaboratoryMl();

        // Synchronize Laboratories module with Reference Laboratories
        $activeRefAgencyRow = DB::table(ReferenceTable::REFERENCE_AGENCY)->select('id', 'show_status')->where(['code' => $data['company_tax_id'], 'show_status' => ReferenceTable::STATUS_ACTIVE])->orderBy('id', 'DESC')->first();

        $referenceData = [
            'code' => $data['laboratory_code'],
            'show_status' => $data['show_status'],
            'end_date' => $data['certification_period_validity'],
            'start_date' => currentDate(),
            'agency' => $activeRefAgencyRow->id,
            'company_tax_id' => $data['company_tax_id']
        ];

        foreach ($data['ml'] as $lngId => $ml) {
            $referenceData['ml'][$lngId] = [
                'name' => $ml['name'],
                'long_name' => $data['legal_entity_name']
            ];
        }

        if ($data['show_status'] == ReferenceTable::STATUS_INACTIVE) {
            $referenceData['end_date'] = currentDate();
        }

        return DB::transaction(function () use ($data, $laboratory, $laboratoryMl, $referenceData) {

            $laboratory->save();
            $this->storeMl($data['ml'], $laboratory, $laboratoryMl);

            if (isset($data['indicators'])) {
                foreach ($data['indicators'] as $indicatorId => $indicator) {
                    LaboratoryIndicator::where('id', $indicatorId)->update([
                        'price' => $indicator['price'],
                        'duration' => $indicator['duration']
                    ]);
                }
            }

            if (isset($data['company_tax_id'])) {
                $subDivisions = RegionalOffice::select('id')->where('company_tax_id', $data['company_tax_id'])->active()->get();

                foreach ($subDivisions as $subDivision) {
                    RegionalOfficeLaboratory::create([
                        'regional_office_id' => $subDivision->id,
                        'laboratory_id' => $laboratory->id,
                        'start_date' => null,
                        'end_date' => null
                    ]);
                }
            }

            // ---
            $referenceTableFormManager = new ReferenceTableFormManager();
            $referenceTableFormManager->store($referenceData, ReferenceTable::getReferenceIdByTableName(ReferenceTable::REFERENCE_LABORATORIES_TABLE_NAME));

            //
            if (isset($data['users']) && count($data['users'])) {
                $this->storeLaboratoryUsers($data['users'], $laboratory->id);
            } else {
                LaboratoryUsers::where(['laboratory_id' => $laboratory->id])->delete();
            }

            return $laboratory;
        });
    }

    /**
     * Function to Update data
     *
     * @param array $data
     * @param int $id
     */
    public function update(array $data, int $id)
    {
        $laboratory = Laboratory::where('id', $id)->checkUserHasRole()->firstOrFail();
        $laboratoryMl = new LaboratoryMl();

        if (!empty($data['users'])) {
            $this->storeLaboratoryUsers($data['users'], $laboratory->id);
        } else {
            LaboratoryUsers::where(['laboratory_id' => $laboratory->id])->delete();
        }

        // Synchronize Laboratory module with Reference Laboratory
        $currentReferenceRow = DB::table(ReferenceTable::REFERENCE_LABORATORIES)->select('id', 'show_status')->where(['code' => $laboratory->laboratory_code])->orderBy('id', 'DESC')->first();
        $referenceTableFormManager = new ReferenceTableFormManager();

        if (!is_null($currentReferenceRow)) {

            if (!Auth()->user()->isSwAdmin()) {
                $data['company_tax_id'] = Auth::user()->companyTaxId();
            }

            $referenceData = [
                'start_date' => currentDate(),
                'show_status' => $data['show_status'],
                'end_date' => $data['certification_period_validity'],
            ];

            if ($data['show_status'] == ReferenceTable::STATUS_INACTIVE) {
                $referenceData['end_date'] = currentDate();
                $referenceData['f_id'] = $currentReferenceRow->id;
            } else {

                if (!empty($data['company_tax_id'])) {
                    $activeRefAgencyRow = DB::table(ReferenceTable::REFERENCE_AGENCY)->select('id', 'show_status')->where(['code' => $data['company_tax_id'], 'show_status' => ReferenceTable::STATUS_ACTIVE])->orderBy('id', 'desc')->first();

                    $referenceData['agency'] = $activeRefAgencyRow->id;
                }

                foreach ($data['ml'] as $lngId => $ml) {
                    $referenceData['ml'][$lngId] = [
                        'name' => $ml['name'],
                        'long_name' => $laboratory->legal_entity_name
                    ];
                }
            }
        } else {

            // Synchronize Laboratories module with Reference Laboratories
            $activeRefAgencyRow = DB::table(ReferenceTable::REFERENCE_AGENCY)->select('id', 'show_status')->where(['code' => $laboratory->company_tax_id, 'show_status' => ReferenceTable::STATUS_ACTIVE])->orderBy('id', 'DESC')->first();

            $referenceData = [
                'code' => $data['laboratory_code'],
                'show_status' => $data['show_status'],
                'end_date' => $data['certification_period_validity'],
                'start_date' => currentDate(),
                'agency' => $activeRefAgencyRow->id
            ];

            foreach ($data['ml'] as $lngId => $ml) {
                $referenceData['ml'][$lngId] = [
                    'name' => $ml['name'],
                    'long_name' => $laboratory->legal_entity_name
                ];
            }

            if ($data['show_status'] == ReferenceTable::STATUS_INACTIVE) {
                $referenceData['end_date'] = currentDate();
            }

            $referenceTableFormManager->store($referenceData, ReferenceTable::getReferenceIdByTableName(ReferenceTable::REFERENCE_LABORATORIES_TABLE_NAME));
        }

        DB::transaction(function () use ($data, $laboratory, $laboratoryMl, $currentReferenceRow, $referenceData, $referenceTableFormManager) {
            $laboratory->update($data);
            $this->updateMl($data['ml'], 'laboratory_id', $laboratory, $laboratoryMl);

            if (isset($data['indicators'])) {
                foreach ($data['indicators'] as $indicatorId => $indicator) {
                    LaboratoryIndicator::where('id', $indicatorId)->update([
                        'price' => $indicator['price'],
                        'duration' => $indicator['duration']
                    ]);
                }
            }

            // ---
            if (!is_null($currentReferenceRow)) {
                $referenceTableFormManager->update($referenceData, ReferenceTable::getReferenceIdByTableName(ReferenceTable::REFERENCE_LABORATORIES_TABLE_NAME), $currentReferenceRow->id);
            }
        });
    }

    /**
     * Function to store Laboratory Users to DB
     *
     * @param $users
     * @param $lab_id
     */
    public function storeLaboratoryUsers($users, $lab_id)
    {
        $labUsers = [];

        foreach ($users as $value) {
            $labUsers[] = $value;
            $data = [
                'user_id' => $value,
                'laboratory_id' => $lab_id
            ];

            LaboratoryUsers::updateOrCreate($data);
        }

        LaboratoryUsers::whereNotIn('user_id', $labUsers)->where(['laboratory_id' => $lab_id])->delete();
    }

    /**
     * Function to inactivating laboratories
     */
    public function inactivateLaboratories()
    {
        //If laboratory certification_period_validity < today  inactivate laboratory row
        $laboratories = Laboratory::select('id', 'tax_id')->active()
            ->where('certification_period_validity', '<', currentDateTime())->get();

        foreach ($laboratories as $laboratory) {

            Laboratory::where('id', $laboratory->id)
                ->update(['show_status' => Laboratory::STATUS_INACTIVE]);

            DB::table(ReferenceTable::REFERENCE_LABORATORIES)
                ->where(['code' => $laboratory->tax_id, 'show_status' => ReferenceTable::STATUS_ACTIVE])
                ->update(['end_date' => Carbon::now(), 'show_status' => Laboratory::STATUS_INACTIVE]);
        }
    }

    /**
     * Function to get active laboratories
     *
     * @param $labTaxId
     * @return Agency
     */
    public function getLabs($labTaxId = null)
    {
        $authUser = Auth::user();
        $query = Agency::select(['id', 'tax_id', 'name'])
            ->where('is_lab', true)
            ->joinMl()
            ->active();

        if (!$authUser->isSwAdmin()) {

            $existLabs = Laboratory::where('company_tax_id', $authUser->companyTaxId())->pluck('tax_id')->all();

            $query->where(function ($q) use ($existLabs, $labTaxId) {
                $q->whereNotIn('tax_id', $existLabs)
                    ->orWhere('tax_id', $labTaxId);
            });

        }

        return $query->get();
    }

    /**
     * Function to search available indicator
     *
     * @param $data
     * @return Builder
     */
    public function getIndicatorsSearch($data)
    {
        $refTable = ReferenceTable::REFERENCE_LAB_HS_INDICATOR;
        $refSphereTable = ReferenceTable::REFERENCE_SPHERE;
        $refLabExpertiseIndicatorTable = ReferenceTable::REFERENCE_LAB_EXPERTISE_INDICATOR;
        $laboratory = Agency::select('id')->where('tax_id', $data['lab'])->where('show_status', BaseModel::STATUS_ACTIVE)->first();

        $availableCodes = AgencyLabIndicators::where('agency_id', $laboratory->id ?? null)->whereNotNull('lab_hs_indicator_code')->pluck('lab_hs_indicator_code')->all();
        $exists = LaboratoryIndicator::where('laboratory_id', $laboratory->id)->where('company_tax_id', $data['agency'])->pluck('indicator_code')->all();

        return DB::table($refTable)
            ->select([
                "{$refTable}.id",
                "{$refSphereTable}_ml.name as sphere",
                DB::raw("CONCAT(reference_lab_hs_indicator.hs_start, ' - ', reference_lab_hs_indicator.hs_end) as hs_code"),
                "{$refLabExpertiseIndicatorTable}_ml.name as indicator",
                "{$refLabExpertiseIndicatorTable}.code as indicator_code",
            ])
            ->join("{$refTable}_ml", function ($q) use ($refTable) {
                $q->on("{$refTable}.id", "=", "{$refTable}_ml.{$refTable}_id")->where('lng_id', cLng('id'));
            })
            ->join("{$refSphereTable}", function ($q) use ($refTable, $refSphereTable) {
                $q->on("{$refSphereTable}.id", "=", "{$refTable}.sphere");
            })
            ->join("{$refSphereTable}_ml", function ($q) use ($refTable, $refSphereTable) {
                $q->on("{$refSphereTable}_ml.{$refSphereTable}_id", "=", "{$refSphereTable}.id")->where("{$refSphereTable}_ml.lng_id", cLng('id'));
            })
            ->join("{$refLabExpertiseIndicatorTable}", function ($q) use ($refTable, $refLabExpertiseIndicatorTable) {
                $q->on("{$refLabExpertiseIndicatorTable}.id", "=", "{$refTable}.lab_expertise_indicator")
                    ->where("{$refLabExpertiseIndicatorTable}.show_status", BaseModel::STATUS_ACTIVE);
            })
            ->join("{$refLabExpertiseIndicatorTable}_ml", function ($q) use ($refTable, $refLabExpertiseIndicatorTable) {
                $q->on("{$refLabExpertiseIndicatorTable}_ml.{$refLabExpertiseIndicatorTable}_id", "=", "{$refLabExpertiseIndicatorTable}.id")
                    ->where("{$refLabExpertiseIndicatorTable}_ml.lng_id", cLng('id'));
            })
            ->when($data['sphere'], function ($q) use ($data, $refSphereTable) {
                $sphereIds = DB::table($refSphereTable)->where('code', $data['sphere'])->pluck('id')->all();
                $q->whereIn('sphere', $sphereIds);
            })
            ->when($data['indicator'], function ($q) use ($data, $refLabExpertiseIndicatorTable) {
                $q->where("{$refLabExpertiseIndicatorTable}_ml.name", "%{$data['indicator']}%")->orWhere("{$refLabExpertiseIndicatorTable}.code", $data['indicator']);
            })
            ->when($data['hs_code'], function ($q) use ($data) {
                $q->where('hs_start', '<=', $data['hs_code']);
                $q->where('hs_end', '>=', $data['hs_code']);
            })
            ->whereIn("{$refTable}.code", $availableCodes)
            ->where("{$refTable}.show_status", BaseModel::STATUS_ACTIVE)
            ->whereNotIn("{$refTable}.code", $exists)
            ->get();
    }

    /**
     * Function to store in db Indicators
     *
     * @param $data
     * @param $laboratory
     * @return array
     */
    public function saveIndicators($data, $laboratory)
    {
        if (isset($data['indicators'])) {
            return DB::transaction(function () use ($data, $laboratory) {
                $returnData = [];
                $labIndicators = $laboratory->indicators->keyBy('lab_hs_indicator_code')->toArray();

                $indicators = getReferenceRowsWhereIn(ReferenceTable::REFERENCE_LAB_HS_INDICATOR, $data['indicators'], [], ['id', 'code', 'name'], false)->keyBy('id')->toArray();

                foreach ($data['indicators'] as $indicator) {
                    $indicator = LaboratoryIndicator::create([
                        'company_tax_id' => $data['agency'],
                        'user_id' => Auth::user()->id,
                        'laboratory_id' => $laboratory->id,
                        'indicator_id' => $indicator,
                        'price' => isset($labIndicators[$indicators[$indicator]->code]) ? $labIndicators[$indicators[$indicator]->code]['price'] : null,
                        'duration' => isset($labIndicators[$indicators[$indicator]->code]) ? $labIndicators[$indicators[$indicator]->code]['duration'] : null,
                        'indicator_code' => $indicators[$indicator]->code
                    ]);
                    $returnData[] = $indicator->id;
                }
                return $returnData;
            });
        }

        return [];
    }

    /**
     * Function to return agency indicators
     *
     * @param $agency
     * @param $lab
     * @return LaboratoryIndicator
     */
    public function getIndicators($agency, $lab)
    {
        $refTable = ReferenceTable::REFERENCE_LAB_HS_INDICATOR;
        $refSphereTable = ReferenceTable::REFERENCE_SPHERE;
        $refLabExpertiseIndicatorTable = ReferenceTable::REFERENCE_LAB_EXPERTISE_INDICATOR;

        return LaboratoryIndicator::select([
            "laboratory_indicator.id",
            "{$refSphereTable}_ml.name as sphere",
            DB::raw("CONCAT(reference_lab_hs_indicator.hs_start, ' - ', reference_lab_hs_indicator.hs_end) as hs_code"),
            "{$refLabExpertiseIndicatorTable}_ml.name as indicator",
            "{$refLabExpertiseIndicatorTable}.code as indicator_code",
            "laboratory_indicator.price",
            "laboratory_indicator.duration",
        ])
            ->join("{$refTable}", function ($q) use ($refTable) {
                $q->on("{$refTable}.code", "=", "laboratory_indicator.indicator_code")->where("{$refTable}.show_status", BaseModel::STATUS_ACTIVE)->where("{$refTable}.start_date", '<=', Carbon::now());
            })
            ->join("{$refTable}_ml", function ($q) use ($refTable) {
                $q->on("{$refTable}.id", "=", "{$refTable}_ml.{$refTable}_id")->where("{$refTable}_ml.lng_id", cLng('id'));
            })
            ->join("{$refSphereTable}_ml", function ($q) use ($refTable, $refSphereTable) {
                $q->on("{$refSphereTable}_ml.{$refSphereTable}_id", "=", "{$refTable}.sphere")->where("{$refSphereTable}_ml.lng_id", cLng('id'));
            })
            ->join($refLabExpertiseIndicatorTable, "{$refLabExpertiseIndicatorTable}.id", "=", "{$refTable}.lab_expertise_indicator")
            ->join("{$refLabExpertiseIndicatorTable}_ml", function ($q) use ($refLabExpertiseIndicatorTable) {
                $q->on("{$refLabExpertiseIndicatorTable}_ml.{$refLabExpertiseIndicatorTable}_id", "=", "{$refLabExpertiseIndicatorTable}.id")->where("{$refLabExpertiseIndicatorTable}_ml.lng_id", cLng('id'));
            })
            ->where('laboratory_indicator.company_tax_id', $agency)
            ->where('laboratory_indicator.laboratory_id', $lab->id)
            ->get();
    }

    /**
     * Function to return agency indicators
     *
     * @param $agency
     * @param $lab
     */
    public function removeWrongIndicators($agency, $lab)
    {
        LaboratoryIndicator::select("laboratory_indicator.id")
            ->where('laboratory_indicator.company_tax_id', $agency)
            ->where('laboratory_indicator.laboratory_id', $lab->id)
            ->whereNull('price')
            ->whereNull('duration')
            ->delete();
    }

    /**
     * Function to remove indicator from db
     *
     * @param $data
     */
    public function deleteIndicator($data)
    {
        LaboratoryIndicator::where('id', $data['indicator'])->delete();
    }

    /**
     * Function to get agency available labs
     *
     * @param $data
     * @return Agency
     */
    public function getAgencyAvailableLabs($data)
    {
        $existLabs = Laboratory::where('company_tax_id', $data['agency'])->pluck('tax_id')->all();

        return Agency::select(['id', 'tax_id', 'name'])
            ->where('is_lab', true)
            ->whereNotIn('tax_id', $existLabs)
            ->joinMl()
            ->active()
            ->get();
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @param null $parentId
     * @return array
     */
    public function getLogData($id = null, $parentId = null)
    {
        if (!is_null($id)) {
            $data = Laboratory::where('id', $id)->first();
            $logData = $data;
            $logData['mls'] = $data->ml()->get()->toArray();

            return $logData;
        }

        return [];
    }
}
