<?php

namespace App\Models\SingleApplicationSubApplicationInstructionFeedback;

use App\Models\BaseModel;

/**
 * Class SingleApplicationSubApplicationInstructionFeedback
 * @package App\Models\SingleApplicationSubApplicationInstructionFeedback
 */
class SingleApplicationSubApplicationInstructionFeedback extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'saf_sub_application_instruction_feedback';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'saf_sub_application_instruction_id',
        'instruction_id',
        'instruction_code',
        'feedback_id',
        'note',
        'feedback_code'
    ];

}
