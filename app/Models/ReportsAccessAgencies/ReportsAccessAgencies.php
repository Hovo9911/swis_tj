<?php

namespace App\Models\ReportsAccessAgencies;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

class ReportsAccessAgencies extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['report_id', 'company_tax_id'];

    /**
     * @var string
     */
    public $incrementing = 'false';

    /**
     * @var string
     */
    public $table = 'reports_access_agencies';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'report_id',
        'company_tax_id',
    ];
}
