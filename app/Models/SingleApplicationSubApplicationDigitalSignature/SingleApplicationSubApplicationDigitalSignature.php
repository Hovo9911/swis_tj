<?php

namespace App\Models\SingleApplicationSubApplicationDigitalSignature;

use App\Models\BaseModel;
use Illuminate\Support\Facades\Storage;

/**
 * Class SingleApplicationSubApplicationProducts
 * @package App\Models\SingleApplicationSubApplicationProducts
 */
class SingleApplicationSubApplicationDigitalSignature extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'saf_sub_applications_digital_signature';

    /**
     * @var array
     */
    protected $fillable = [
        'subapplication_id',
        'user_id',
        'company_tax_id',
        'file_path',
        'file_digest',
        'file_digest_hash'
    ];

    /**
     * Function to check digital signature validity
     *
     * @param $where
     * @return array
     */
    public static function checkDigitalSignature($where = [])
    {
        $digitalSignature = SingleApplicationSubApplicationDigitalSignature::select('file_digest_hash', 'file_path')->where($where)->orderByDesc()->first();

        $verificationFileStart = "-----BEGIN CMS-----\n";
        $verificationFileHash = optional($digitalSignature)->file_digest_hash;
        $verificationFileEnd = "\n-----END CMS-----";

        $verificationFileContent = $verificationFileStart . $verificationFileHash . $verificationFileEnd;

        $tmpDirectory = env('DIGITAL_SIGNATURE_FILE_TMP_PATH');
        $validationFileName = 'digitalSignature_' . rand() . '.pem';
        $validationFileFullName = $tmpDirectory . '/' . $validationFileName;

        $tmpFileName = '/tmpDigitalSignature_' . rand() . '.pem';
        $tmpFileFullName = $tmpDirectory . '/' . $tmpFileName;

        Storage::disk('tmp')->put($validationFileName, $verificationFileContent);
        Storage::disk('tmp')->put($tmpFileName, '');

        $cmd = "/usr/local/openssl/usr/local/openssl/bin/openssl cms -verify -binary -in {$validationFileFullName} -inform PEM -CAfile /var/www/shared/content/ca-bundle.pem -crl_check_all -certsout {$tmpFileFullName}";

        exec(escapeshellcmd($cmd), $output, $returnCode);
        // if $returnCode === 0 command executed successful
        // else error
        // if $output == 'disgn of json file' then verified
        //

        join('', $output) . "\n\n";

        $cert = openssl_x509_parse(file_get_contents($tmpFileFullName));

        $cmd = "/usr/local/openssl/usr/local/openssl/bin/openssl asn1parse -i -in {$validationFileFullName}";
        exec(escapeshellcmd($cmd), $output);
        $str = join('', $output);
        $str = substr($str, strpos($str, ':signingTime'));
        $str = substr($str, strpos($str, "UTCTIME"));
        $str = substr($str, 0, strpos($str, 'Z') + 1);
        $str = '20' . substr($str, strpos($str, ':') + 1);

        if ($cert) {
            $data['command'] = $cert;
        }
        $data['signing_time'] = date('d/m/Y H:i:s', strtotime($str));

        // Deleting files
        unlink($validationFileFullName);
        unlink($tmpFileFullName);

        return $data;
    }
}
