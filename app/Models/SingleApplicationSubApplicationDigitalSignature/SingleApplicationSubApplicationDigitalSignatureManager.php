<?php

namespace App\Models\SingleApplicationSubApplicationDigitalSignature;

/**
 * Class SingleApplicationProducts
 * @package App\Models\SingleApplication
 */
class SingleApplicationSubApplicationDigitalSignatureManager
{
    /**
     * Function store single application sub application digital signature
     *
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return (new SingleApplicationSubApplicationDigitalSignature($data))->insertGetId($data);
    }

    /**
     * Function to update data
     *
     * @param array $data
     * @return bool|void
     */
    public function update(array $data)
    {
        SingleApplicationSubApplicationDigitalSignature::where(['id' => $data['id']])
            ->update(['file_digest_hash' => $data['fileDigestHash']]);
    }
}