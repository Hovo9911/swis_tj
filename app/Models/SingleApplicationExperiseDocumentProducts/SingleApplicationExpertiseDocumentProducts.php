<?php

namespace App\Models\SingleApplicationExperiseDocumentProducts;

use App\Models\BaseModel;
use App\Models\Document\Document;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class SingleApplicationExpertiseDocumentProducts
 * @package App\Models\SingleApplicationExperiseDocumentProducts
 */
class SingleApplicationExpertiseDocumentProducts extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'saf_expertise_documents_products';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'company_tax_id',
        'saf_expertise_id',
        'document_id',
        'products',
    ];

    /**
     * Function to relate with Document
     *
     * @return HasOne
     */
    public function document()
    {
        return $this->hasOne(Document::class, 'id', 'document_id');
    }
}
