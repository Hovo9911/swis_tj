<?php

namespace App\Models\Transactions;

use App\Http\Controllers\Core\DataTableSearch;
use App\Models\BaseModel;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class TransactionsSearch.phpp
 *
 * @package App\Models\User
 */
class TransactionsSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        $companyTaxId = Auth::user()->companyTaxId() ? Auth::user()->companyTaxId() : 0;
        $query = Transactions::when($companyTaxId !== 0, function ($q) use ($companyTaxId) {
            $q->where('transactions.company_tax_id', $companyTaxId);
        });
        if (!$companyTaxId && !Auth::user()->isSwAdmin()) {
            $query = $query->Where('transactions.user_id', Auth::id());
        }

        return $query->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        $result = $query->get();

        foreach ($result as &$value) {
            if (is_null($value->payment_id) && $value->type == Transactions::PAYMENT_TYPE_OUTPUT) {
                $value->payment_id = 'SW' . str_pad($value->id, 10, '0', STR_PAD_LEFT);
            }

            if(trim($value->obligation_type) == '()'){
                $value->obligation_type = '';
            }
        }

        return $result;
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $orderByCol = $this->orderByCol;
        switch ($orderByCol):
            case 'document':
                $query->orderBy('constructor_documents_ml.document_name', $this->orderByType ?? 'desc');
                break;

                case 'agency':
                $query->orderBy('agency_ml.name', $this->orderByType ?? 'desc');
                break;

            default:
                $query->orderBy($orderByCol ?? 'transactions.id', $this->orderByType ?? 'desc');

            endswitch;

    }

    /**
     * @param array $filterData
     * @return mixed
     */
    protected function constructQuery($filterData = [])
    {
        if (count($filterData)) {
            $this->searchData = $filterData;
        }

        $companyTaxId = (Auth::user()->companyTaxId()) ? Auth::user()->companyTaxId() : 0;

        $subDivisionRef = ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS;
        $subDivisionRefMl = "{$subDivisionRef}_ml";

        $query = Transactions::select([
            'transactions.id',
            'transactions.type',
            'transactions.payment_id',
            'transactions.saf_number',
            'transactions.amount',
            'transactions.transaction_date',
            'transactions.total_balance',
            'saf_sub_applications.document_number',
            'agency_ml.name as agency',
            'constructor_documents_ml.document_name as document',
            'users.ssn as user_ssn',
            "{$subDivisionRefMl}.name as subdivision",
            DB::raw("CONCAT(payment_providers.name ,' - ', payment_providers.description) AS service_provider_name"),
            DB::raw("company.name  AS company_name"),
            DB::raw("CONCAT('(', reference_obligation_type.code , ') ', reference_obligation_type_ml.name) AS obligation_type"),
            DB::raw("users.first_name || ' ' || users.last_name AS username"),
        ])
            ->leftJoin('company', 'company.tax_id', '=', 'transactions.company_tax_id')
            ->leftJoin('users', 'users.id', '=', 'transactions.user_id')
            ->leftJoin('saf_obligations', 'saf_obligations.id', '=', 'transactions.obligation_id')
            ->leftJoin('saf_sub_applications', 'saf_sub_applications.id', '=', 'saf_obligations.subapplication_id')
            ->leftJoin('agency', function ($q) {
                $q->on('agency.tax_id', '=', 'saf_sub_applications.company_tax_id')->where('agency.show_status', BaseModel::STATUS_ACTIVE);
            })
            ->leftJoin('agency_ml', function ($query) {
                $query->on('agency_ml.agency_id', '=', 'agency.id')->where('agency_ml.lng_id', cLng('id'));
            })
            ->leftJoin('constructor_documents', function ($query) {
                $query->on('constructor_documents.document_code', '=', 'saf_sub_applications.document_code')->where('constructor_documents.show_status', '1');
            })
            ->leftJoin('constructor_documents_ml', function ($query) {
                $query->on('constructor_documents_ml.constructor_documents_id', '=', 'constructor_documents.id')->where('constructor_documents_ml.lng_id', cLng('id'));
            })
            ->leftJoin('reference_obligation_budget_line', 'reference_obligation_budget_line.id', '=', 'saf_obligations.ref_budget_line_id')
            ->leftJoin('reference_obligation_budget_line_ml', function ($q) {
                $q->on('reference_obligation_budget_line_ml.reference_obligation_budget_line_id', '=', 'reference_obligation_budget_line.id')->where('reference_obligation_budget_line_ml.lng_id', cLng('id'));
            })
            ->leftJoin('reference_obligation_type', 'reference_obligation_type.id', '=', 'reference_obligation_budget_line.budget_line')
            ->leftJoin('reference_obligation_type_ml', function ($q) {
                $q->on('reference_obligation_type_ml.reference_obligation_type_id', '=', 'reference_obligation_type.id')->where('reference_obligation_type_ml.lng_id', cLng('id'));
            })
            ->leftJoin("{$subDivisionRef}", "{$subDivisionRef}.id", '=', "saf_sub_applications.agency_subdivision_id")
            ->leftJoin("{$subDivisionRefMl}", function ($q) use ($subDivisionRef, $subDivisionRefMl) {
                $q->on("{$subDivisionRefMl}.{$subDivisionRef}_id", '=', "{$subDivisionRef}.id")->where("{$subDivisionRefMl}.lng_id", cLng('id'));
            })
            ->leftJoin('payments', function ($q) {
                $q->on('payments.id', '=', 'transactions.payment_id');
            })
            ->leftJoin('payment_providers', function ($q) {
                $q->on('payment_providers.id', '=', 'payments.service_provider_id');
            })
            ->when($companyTaxId !== 0, function ($q) use ($companyTaxId) {
                $q->where('transactions.company_tax_id', $companyTaxId);
            });

        if (isset($this->searchData['service_provider_name'])) {
            $query->where(function ($q) {
                $q->where('payment_providers.name', 'ILIKE', "%{$this->searchData['service_provider_name']}%")
                    ->orWhere('payment_providers.description', 'ILIKE', "%{$this->searchData['service_provider_name']}%");
            });
        }

        if (isset($this->searchData['saf_number'])) {
            $query->where('transactions.saf_number', 'ILIKE', '%' . trim($this->searchData['saf_number']) . '%');
        }

        if (isset($this->searchData['sub_application_number'])) {
            $query->where('saf_sub_applications.document_number', 'ILIKE', '%' . trim($this->searchData['sub_application_number']) . '%');
        }

        if (isset($this->searchData['agency'])) {
            $query->where('agency.tax_id', $this->searchData['agency']);
        }

        if (isset($this->searchData['constructor_document'])) {
            $query->where('saf_sub_applications.constructor_document_id', $this->searchData['constructor_document']);
        }

        if (isset($this->searchData['obligation_type'])) {
            $query->where('reference_obligation_type.id', $this->searchData['obligation_type']);
        }

        if (isset($this->searchData['transaction_date_start'])) {
            $query->where('transactions.transaction_date', '>=', Carbon::parse($this->searchData['transaction_date_start'])->startOfDay());
        }

        if (isset($this->searchData['transaction_date_end'])) {
            $query->where('transactions.transaction_date', '<=', Carbon::parse($this->searchData['transaction_date_end'])->endOfDay());
        }

        if (isset($this->searchData['transaction_username'])) {

            $transactionUsername = $this->searchData['transaction_username'];
            $transactionUsersId = User::whereRaw("concat(first_name, ' ', last_name) ILIKE '%$transactionUsername%' ")->pluck('id')->toArray();

            $query->whereIn('users.id', $transactionUsersId);
        }

        if ($companyTaxId === 0 && !Auth::user()->isSwAdmin()) {
            $query = $query->where('transactions.user_id', Auth::id());
        }

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }

    /**
     * Function to call protected ConstructorQuery
     *
     * @param $data
     * @return mixed
     */
    public function callToConstructorQuery($data)
    {
        return $this->constructQuery($data, true);
    }
}