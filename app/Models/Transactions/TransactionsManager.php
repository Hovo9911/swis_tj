<?php

namespace App\Models\Transactions;

/**
 * Class TransactionsManager
 * @package App\Models\Transactions
 */
class TransactionsManager
{
    /**
     * Function to store data to db
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $transaction = new Transactions($data);

        $transaction->save();
    }
}
