<?php

namespace App\Models\Transactions;

use App\Models\BaseModel;
use App\Models\Pays\Pays;
use App\Models\SingleApplication\SingleApplication;
use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Transactions
 * @package App\Models\Transactions
 */
class Transactions extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'transactions';

    /**
     * @var string
     */
    const PAYMENT_TYPE_INPUT = 'input';
    const PAYMENT_TYPE_OUTPUT = 'output';

    /**
     * @var array
     */
    protected $fillable = [
        'payment_id',
        'saf_number',
        'obligation_id',
        'user_id',
        'company_tax_id',
        'type',
        'transaction_date',
        'amount',
        'total_balance',
        'obligation_id',
        'is_refund'
    ];

    /**
     * @param $value
     * @return string
     */
    public function getTransactionDateAttribute($value)
    {
        return Carbon::parse($value)->format(config('swis.date_time_format_front'));
    }

    /**
     * Function to get and all transactions for not registered users and create transactions
     *
     * @param $taxId
     * @param $userId
     */
    public static function createTransactionsForNewUsers($taxId, $userId)
    {
        $transactionsManager = new TransactionsManager();
        $payments = Pays::select('id', 'tax_id', 'payment_done_date', 'amount', 'sended_into_balance')->where('sended_into_balance', 'no')->where('tax_id', $taxId)->get();

        foreach ($payments as $pay) {
            $userBalance = User::getBalance($taxId, $userId);
            $transactionsManager->store([
                'payment_id' => $pay->id,
                'user_id' => $userId,
                'company_tax_id' => $taxId,
                'type' => 'input',
                'transaction_date' => $pay->payment_done_date,
                'amount' => $pay->amount,
                'total_balance' => number_format($userBalance + $pay->amount, 2, '.', '')
            ]);

            $pay->sended_into_balance = 'yes';
            $pay->save();
        }
    }

    /**
     * Function to get all saf
     *
     * @return HasMany
     */
    public function safAll()
    {
        return $this->hasMany(SingleApplication::class, 'company_tax_id', 'company_tax_id');
    }

}
