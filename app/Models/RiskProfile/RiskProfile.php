<?php

namespace App\Models\RiskProfile;

use App\Models\BaseModel;
use App\Models\Instructions\Instructions;
use App\Models\ProductFieldCall\ProductFieldCall;
use App\Models\RiskProfileToInstructions\RiskProfileToInstructions;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationSubApplicationInstructions\SingleApplicationSubApplicationInstructions;
use App\Models\SingleApplicationSubApplicationInstructions\SingleApplicationSubApplicationInstructionsManager;
use App\Models\SingleApplicationSubApplicationInstructionsProfile\SingleApplicationSubApplicationInstructionsProfile;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Auth;

/**
 * Class RiskProfile
 * @package App\Models\InstructionsManager
 */
class RiskProfile extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'risk_profile';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'company_tax_id',
        'document_id',
        'name',
        'condition',
        'weight',
        'frequency',
        'active_date_from',
        'active_date_to',
        'show_status',
        'risk_run_type'
    ];

    /**
     * @var int
     */
    const RISK_RUN_TYPE_WITH_CONDITION = 1;
    const RISK_RUN_TYPE_WITHOUT_CONDITION = 2;

    /**
     * Function to get activate date from
     *
     * @param $value
     * @return string|void
     */
    public function getActiveDateFromAttribute($value)
    {
        if (isset($value)) {
            return Carbon::parse($value)->format(config('swis.date_format_front'));
        }
    }

    /**
     * Function to get activate date to
     *
     * @param $value
     * @return string|void
     */
    public function getActiveDateToAttribute($value)
    {
        if (isset($value)) {
            return Carbon::parse($value)->format(config('swis.date_format_front'));
        }
    }

    /**
     * Function to return active languages
     *
     * @return HasMany
     */
    public function ml()
    {
        return $this->hasMany(RiskProfileMl::class, 'risk_profile_id', 'id');
    }

    /**
     * Function to relate with RiskProfileMl and get active
     *
     * @return HasOne
     */
    public function current()
    {
        return $this->hasOne(RiskProfileMl::class, 'risk_profile_id', 'id')->notDeleted()->where('lng_id', cLng('id'))->first();
    }

    /**
     * Function to relate with Instructions
     *
     * @return HasMany
     */
    public function instructions()
    {
        return $this->hasMany(RiskProfileToInstructions::class, 'risk_profile_id', 'id');
    }

    /**
     * Function to generate Instructions
     *
     * @param $mapping
     * @param $safDataForValidation
     * @param $singleApplicationSubApp
     * @param $stateId
     * @param $documentId
     */
    public static function generateInstructions($mapping, $safDataForValidation, $singleApplicationSubApp, $stateId, $documentId)
    {
        $allInstructions = SingleApplicationSubApplicationInstructions::getInstructions($singleApplicationSubApp);
        $getExistsInstructions = $allInstructions['instructions'];

        $instructions = [];
        $applicationSubApplicationInstructionsManager = new SingleApplicationSubApplicationInstructionsManager();

        if ($mapping->enable_risks) {

            $riskProfiles = RiskProfile::where('company_tax_id', Auth::user()->companyTaxId())->where('document_id', $documentId)->where('show_status', BaseModel::STATUS_ACTIVE)->get();
            foreach ($riskProfiles as $riskProfile) {
                $incrementCount = $riskProfile->increment_count + 1;

                if ($riskProfile->risk_run_type == RiskProfile::RISK_RUN_TYPE_WITH_CONDITION) {

                    $getProductSafIds = SingleApplication::getProductSafIds();

                    preg_match_all('/SAF_ID_[\w]*/ius', $riskProfile->condition, $variables);
                    foreach ($variables[0] as $key => $variable) {
                        if (in_array($variable, $getProductSafIds)) {
                            ProductFieldCall::insert([
                                'sub_application_id' => $singleApplicationSubApp->id,
                                'field_id' => $variable,
                                'rule_condition' => $riskProfile->condition,
                                'profile_id' => $riskProfile->id
                            ]);
                        }
                    }

                    $isMatchCondition = checkCondition($safDataForValidation, $riskProfile->condition, 'obligation', $singleApplicationSubApp, $stateId);

                    if ($riskProfile->frequency == $incrementCount && $isMatchCondition) {
                        $incrementCount = 0;
                        $instructions = (new self)->getInstructions($riskProfile, $instructions);
                    }

                    $riskProfile->increment_count = $incrementCount;
                    $riskProfile->save();
                } else {

                    if ($riskProfile->frequency == $incrementCount) {
                        $incrementCount = 0;
                        $instructions = (new self)->getInstructions($riskProfile, $instructions);
                    }

                    $riskProfile->increment_count = $incrementCount;
                    $riskProfile->save();
                }
            }

            // generated instructions
            $instructionsIds = Instructions::whereIn('code', array_keys($instructions))->pluck('id', 'code')->all();

            $existsInstructions = $getExistsInstructions->pluck('instructions_id', 'instructions_code')->all();

            foreach ($instructionsIds as $instructionCode => $instructionId) {

                if (array_key_exists($instructionCode, $existsInstructions)) {
                    if ($existsInstructions[$instructionCode] == $instructionId) {

                        $getSingleInstruction = Instructions::select('id', 'show_status')->find($instructionId);

                        if ($getSingleInstruction->show_status == BaseModel::STATUS_INACTIVE) {
                            $subApplicationInstruction = SingleApplicationSubApplicationInstructions::where([
                                'instruction_id' => $instructionId,
                                'instruction_code' => $instructionCode,
                                'sub_application_id' => $singleApplicationSubApp->id
                            ])->first();

                            SingleApplicationSubApplicationInstructionsProfile::where('saf_sub_application_instructions_id', $subApplicationInstruction->id)->delete();
                            $subApplicationInstruction->delete();
                        }

                    } else {
                        $wrongInstructions = SingleApplicationSubApplicationInstructions::select('saf_sub_application_instructions.id')
                            ->leftJoin('saf_sub_application_instruction_feedback', 'saf_sub_application_instruction_feedback.saf_sub_application_instruction_id', '=', 'saf_sub_application_instructions.id')
                            ->where('sub_application_id', $singleApplicationSubApp->id)
                            ->whereNull('saf_sub_application_instruction_feedback.feedback_id')
                            ->pluck('id')->all();

                        SingleApplicationSubApplicationInstructions::whereIn('id', $wrongInstructions)->delete();
                    }
                } else {
                    $applicationSubApplicationInstructionsManager->store($singleApplicationSubApp->id, $instructionCode, $instructions[$instructionCode], $instructionsIds);
                }
            }

        }
    }

    /**
     * Function to get Risk Profile Instructions
     *
     * @param $riskProfile
     * @param $instructions
     * @return array
     */
    private function getInstructions($riskProfile, $instructions)
    {
        if ($riskProfile->instructions) {
            foreach ($riskProfile->instructions as $instruction) {
                $instructions[$instruction->instruction_code][] = $riskProfile->id;
            }
        }

        return $instructions;
    }

}
