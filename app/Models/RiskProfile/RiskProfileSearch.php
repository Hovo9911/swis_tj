<?php

namespace App\Models\RiskProfile;

use App\Http\Controllers\Core\DataTableSearch;
use Carbon\Carbon;

/**
 * Class RiskProfileSearch
 * @package App\Models\RiskProfileSearch
 */
class RiskProfileSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        return $query->get();
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($this->orderByCol, $this->orderByType ?? 'desc');
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $query = RiskProfile::select('risk_profile.id', 'risk_profile.name', 'risk_profile.weight', 'risk_profile.frequency', 'constructor_documents_ml.document_name', 'risk_profile.active_date_from', 'risk_profile.show_status', 'risk_profile.active_date_to', 'risk_profile_ml.description')
            ->joinMl()
            ->join('constructor_documents', 'constructor_documents.id', '=', 'risk_profile.document_id')
            ->join('constructor_documents_ml', function ($q) {
                $q->on('constructor_documents_ml.constructor_documents_id', '=', 'constructor_documents.id')->where('constructor_documents_ml.lng_id', '=', cLng('id'));
            })
            ->notDeleted();

        if (isset($this->searchData['document_id'])) {
            $query->where('risk_profile.document_id', $this->searchData['document_id']);
        }

        if (isset($this->searchData['name'])) {
            $query->where('risk_profile.name', 'ILIKE', '%' . strtolower(trim($this->searchData['name'])) . '%');
        }

        if (isset($this->searchData['description'])) {
            $query->where('risk_profile_ml.description', 'ILIKE', '%' . strtolower(trim($this->searchData['description'])) . '%');
        }

        if (isset($this->searchData['active_date_from_start'])) {
            $query->where('risk_profile.active_date_from', '>=', Carbon::parse($this->searchData['active_date_from_start'])->startOfDay());
        }

        if (isset($this->searchData['active_date_from_end'])) {
            $query->where('risk_profile.active_date_from', '<=',Carbon::parse($this->searchData['active_date_from_end'])->endOfDay());
        }

        if (isset($this->searchData['active_date_to_start'])) {
            $query->where('risk_profile.active_date_to', '>=', Carbon::parse($this->searchData['active_date_to_start'])->startOfDay());
        }

        if (isset($this->searchData['active_date_to_end'])) {
            $query->where('risk_profile.active_date_to', '<=', Carbon::parse($this->searchData['active_date_to_end'])->endOfDay());
        }

        $query->checkUserHasRole();

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}
