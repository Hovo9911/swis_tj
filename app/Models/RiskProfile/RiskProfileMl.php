<?php

namespace App\Models\RiskProfile;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

/**
 * Class RiskProfileMl
 * @package App\Models\RiskProfileMl
 */
class RiskProfileMl extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var string
     */
    public $incrementing = 'false';

    /**
     * @var array
     */
    protected $primaryKey = ['risk_profile_id', 'lng_id'];

    /**
     * @var string
     */
    public $table = 'risk_profile_ml';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'risk_profile_id',
        'lng_id',
        'description'
    ];
}
