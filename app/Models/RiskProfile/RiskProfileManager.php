<?php

namespace App\Models\RiskProfile;

use App\Models\BaseMlManager;
use App\Models\BaseModel;
use App\Models\RiskProfileToInstructions\RiskProfileToInstructions;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class RiskProfileManager
 * @package App\Models\InstructionsManager
 */
class RiskProfileManager
{
    use BaseMlManager;

    /**
     * Function to Store data to DB
     *
     * @param array $data
     * @return RiskProfile
     */
    public function store(array $data)
    {
        $data['company_tax_id'] = Auth::user()->companyTaxId();
        $data['user_id'] = Auth::id();
        if ($data['show_status'] == BaseModel::STATUS_INACTIVE) {
            $data['active_date_to'] = Carbon::now()->format(config('swis.date_format'));
        }
        $riskProfile = new RiskProfile($data);
        $riskProfileMl = new RiskProfileMl();

        return DB::transaction(function () use ($riskProfile, $riskProfileMl, $data) {

            $riskProfile->save();
            $this->storeMl($data['ml'], $riskProfile, $riskProfileMl);

            //
            foreach ($data['instructions'] as $instructionCode) {
                RiskProfileToInstructions::insert([
                    'risk_profile_id' => $riskProfile->id,
                    'instruction_code' => $instructionCode
                ]);
            }

            return $riskProfile;
        });
    }

    /**
     * Function to Update data
     *
     * @param array $data
     * @param int $id
     */
    public function update(array $data, int $id)
    {
        $data['company_tax_id'] = Auth::user()->companyTaxId();
        $data['user_id'] = Auth::id();
        $data['active_date_to'] = (isset($data['active_date_to'])) ? $data['active_date_to'] : null;

        $riskProfile = RiskProfile::where('id', $id)->checkUserHasRole()->firstOrFail();
        $newRiskProfile = new RiskProfile($data);
        $riskProfileMl = new RiskProfileMl();

        DB::transaction(function () use ($data, $riskProfile, $riskProfileMl, $newRiskProfile) {

            $riskProfile->show_status = BaseModel::STATUS_INACTIVE;
            $riskProfile->active_date_to = Carbon::now()->format(config('swis.date_format'));
            $riskProfile->save();

            $riskProfileMl->where('risk_profile_id', $riskProfile->id)->update(['show_status' => BaseModel::STATUS_INACTIVE]);

            if ($data['show_status'] != BaseModel::STATUS_INACTIVE) {
                $newRiskProfile->active_date_from = Carbon::now()->format(config('swis.date_format'));
                $newRiskProfile->active_date_to = null;
                $newRiskProfile->save();

                $this->storeMl($data['ml'], $newRiskProfile, $riskProfileMl);

                foreach ($data['instructions'] as $instructionCode) {
                    RiskProfileToInstructions::insert([
                        'risk_profile_id' => $newRiskProfile->id,
                        'instruction_code' => $instructionCode
                    ]);
                }
            }
        });
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @return array
     */
    public function getLogData($id)
    {
        return RiskProfile::where('id', $id)->with(['ml', 'instructions'])->first()->toArray();
    }
}
