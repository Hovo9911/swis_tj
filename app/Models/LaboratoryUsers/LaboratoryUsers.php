<?php

namespace App\Models\LaboratoryUsers;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

/**
 * Class LaboratoryUsers
 * @package App\Models\LaboratoryUsers
 */
class LaboratoryUsers extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['user_id', 'laboratory_id'];

    /**
     * @var string
     */
    public $table = 'laboratory_users';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'laboratory_id',
    ];

}
