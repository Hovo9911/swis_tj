<?php

namespace App\Models\TransportApplicationStates;

use App\Models\BaseModel;
use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\DB;

/**
 * Class TransportApplicationStates
 * @package App\Models\TransportApplicationStates
 */
class TransportApplicationStates extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'transport_application_states';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'transport_application_id',
        'user_id',
        'company_tax_id',
        'from_status',
        'to_status',
    ];

    /**
     * Function to get created_at value
     *
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format(config('swis.date_time_format_front'));
    }

    /**
     * Function to relate with user table
     *
     * @return HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id')->select('id', DB::raw("CONCAT(first_name, ' ', last_name) as name"));
    }
}