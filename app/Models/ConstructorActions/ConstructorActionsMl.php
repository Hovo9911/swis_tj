<?php

namespace App\Models\ConstructorActions;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

/**
 * Class ConstructorActionsMl
 * @package App\Models\ConstructorActions
 */
class ConstructorActionsMl extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['constructor_actions_id', 'lng_id'];

    /**
     * @var string
     */
    public $incrementing = 'false';

    /**
     * @var string
     */
    public $table = 'constructor_actions_ml';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'constructor_actions_id',
        'lng_id',
        'action_name',
        'description',
    ];
}
