<?php

namespace App\Models\ConstructorActions;

use App\Models\BaseMlManager;
use App\Models\ConstructorActionToTemplate\ConstructorActionToTemplate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class ConstructorActionsManager
 * @package App\Models\ConstructorActions
 */
class ConstructorActionsManager
{
    use BaseMlManager;

    /**
     * Function to Store data to DB
     *
     * @param array $data
     * @return ConstructorActions
     */
    public function store(array $data)
    {
        $data['user_id'] = Auth::id();
        $data['action_id'] = generateID(10);
        $data['print_default'] = $data['print_default'] ?? 0;

        $constructorActions = new ConstructorActions($data);
        $constructorActionsMl = new ConstructorActionsMl();

        return DB::transaction(function () use ($data, $constructorActions, $constructorActionsMl) {
            $constructorActions->save();
            $this->storeMl($data['ml'], $constructorActions, $constructorActionsMl);
            $actionToTemplate = (isset($data['templates'])) ? $this->generateActionToTemplateArray($data['templates'], $constructorActions->id) : [];
            ConstructorActionToTemplate::insert($actionToTemplate);

            return $constructorActions;
        });
    }

    /**
     * Function to Update data
     *
     * @param array $data
     * @param int $id
     */
    public function update(array $data, int $id)
    {
        $data['select_all_products'] = $data['select_all_products'] ?? false;
        $data['print_default'] = $data['print_default'] ?? 0;
        $constructorActions = ConstructorActions::where('id', $id)->constructorActionOwner()->firstOrFail();
        $constructorActionsMl = new ConstructorActionsMl();

        DB::transaction(function () use ($data, $constructorActions, $constructorActionsMl) {
            $constructorActions->update($data);
            $this->updateMl($data['ml'], 'constructor_actions_id', $constructorActions, $constructorActionsMl);

            ConstructorActionToTemplate::where('constructor_action_id', $constructorActions->id)->delete();

            $actionToTemplate = (isset($data['templates'])) ? $this->generateActionToTemplateArray($data['templates'], $constructorActions->id) : [];
            ConstructorActionToTemplate::insert($actionToTemplate);
        });
    }

    /**
     * Function to generate Action To Template Array
     *
     * @param $templates
     * @param $actionId
     * @return array
     */
    private function generateActionToTemplateArray($templates, $actionId)
    {
        $insertData = [];

        foreach ($templates as $template) {
            $insertData[] = [
                'constructor_action_id' => $actionId,
                'constructor_template_id' => $template
            ];
        }

        return $insertData;
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @param null $id2
     * @return array
     */
    public function getLogData($id, $id2 = null)
    {
        $data = ConstructorActions::where('id', $id)->first()->toArray();
        $data['mls'] = ConstructorActionsMl::where('constructor_actions_id', $id)->get()->toArray();

        return $data;
    }
}
