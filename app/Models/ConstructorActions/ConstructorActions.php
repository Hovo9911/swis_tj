<?php

namespace App\Models\ConstructorActions;

use App\Models\BaseModel;
use App\Models\ConstructorActionToTemplate\ConstructorActionToTemplate;
use App\Models\ConstructorDocument\ConstructorDocument;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Query\Builder;

/**
 * Class ConstructorActions
 * @package App\Models\ConstructorActions
 */
class ConstructorActions extends BaseModel
{
    /**
     * @var array
     */
    public static $specialTypes = [
        '1' => 'Simple',
        '2' => 'Print',
        '3' => 'Verify',
        '4' => 'Superscribe',
    ];

    /**
     * @var string
     */
    const SUPERSCRIBE_TYPE = '4';
    const PRINT_TYPE = '2';

    /**
     * @var string
     */
    public $table = 'constructor_actions';

    /**
     * @var bool
     */
    protected $hasAdminInfo = true;

    /**
     * @var null
     */
    protected static $constructorActionAllData = null;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'constructor_document_id',
        'special_type',
        'action_id',
        'show_status',
        'select_all_products',
        'order',
        'print_default'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'created_admin_id',
        'created_admin_ip',
        'updated_at',
        'updated_admin_id',
        'updated_admin_ip',
        'deleted_admin_id',
        'deleted_admin_ip'
    ];

    /**
     * Function to return active languages
     *
     * @return HasMany
     */
    public function ml()
    {
        return $this->hasMany(ConstructorActionsMl::class, 'constructor_actions_id', 'id');
    }

    /**
     * Function to return current language
     *
     * @return HasOne
     */
    public function current()
    {
        return $this->hasOne(ConstructorActionsMl::class, 'constructor_actions_id', 'id')->notDeleted()->where('lng_id', cLng('id'))->first();
    }

    /**
     * Function to return current language
     *
     * @return HasOne
     */
    public function currentMl()
    {
        return $this->hasOne(ConstructorActionsMl::class, 'constructor_actions_id', 'id')->notDeleted()->where('lng_id', cLng('id'));
    }

    /**
     * Function to relate with templates
     *
     * @return HasMany
     */
    public function actionTemplates()
    {
        return $this->hasMany(ConstructorActionToTemplate::class, 'constructor_action_id', 'id');
    }

    /**
     * Function to check auth user has role
     *
     * @param $query
     * @return Builder
     */
    public function scopeConstructorActionOwner($query)
    {
        $agencyDocumentIds = ConstructorDocument::getConstructorDocumentForAgency()->pluck('id')->toArray();

        return $query->whereIn($this->getTable() . '.constructor_document_id', $agencyDocumentIds ?? -1);
    }

    /**
     * Function to get all data
     *
     * @return null|ConstructorActions
     */
    public static function allData()
    {
        if (self::$constructorActionAllData === null) {
            self::setAllData();
        }

        return self::$constructorActionAllData;
    }

    /**
     * Function to set constructor_action model all
     *
     * @return void
     */
    public static function setAllData()
    {
        self::$constructorActionAllData = self::select('id', 'action_name')->joinMl()->get()->keyBy('id');
    }

    /**
     * Function to get action name
     *
     * @param $id
     * @return string
     */
    public static function getActionName($id)
    {
        $allData = self::allData();

        $actionName = '';
        if (isset($allData[$id])) {
            $actionName = $allData[$id]->action_name;
        }

        return $actionName;
    }

}
