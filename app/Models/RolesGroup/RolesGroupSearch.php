<?php

namespace App\Models\RolesGroup;

use App\Http\Controllers\Core\DataTableSearch;
use Illuminate\Support\Facades\DB;

/**
 * Class RolesGroupSearch
 * @package App\Models\RolesGroup
 */
class RolesGroupSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        return $query->get();
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($this->orderByCol, $this->orderByType ?? 'desc');
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $query = RolesGroup::select('roles_group.id', 'roles_group.show_status', 'roles_group.created_at', 'roles_group_ml.name', 'roles_group_ml.description', 'dictionary_ml.value as menu_name')
            ->joinMl(['f_k' => 'role_group_id'])
            ->join('menu', 'roles_group.menu_id', '=', 'menu.id')
            ->join('dictionary', 'menu.title', '=', 'dictionary.key')
            ->join('dictionary_ml', 'dictionary.id', '=', 'dictionary_ml.dictionary_id')->where('dictionary_ml.lng_id', '=', cLng('id'));

        if (isset($this->searchData['id'])) {
            $query->where(DB::raw("roles_group.id::varchar"), $this->searchData['id']);
        }

        if (isset($this->searchData['name'])) {
            $query->where('roles_group_ml.name', 'ILIKE', '%' . strtolower(trim($this->searchData['name'])) . '%');
        }

        if (isset($this->searchData['menu'])) {
            $query->where('roles_group.menu_id', $this->searchData['menu']);
        }

        if (isset($this->searchData['created_at_start'])) {
            $query->where('roles_group.created_at', '>=', $this->searchData['created_at_start']);
        }

        if (isset($this->searchData['created_at_end'])) {
            $query->where('roles_group.created_at', '<=', $this->searchData['created_at_end']);
        }

        if (isset($this->searchData['show_status'])) {
            $query->where('roles_group.show_status', $this->searchData['show_status']);
        }

        $query->checkUserHasRole();

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}
