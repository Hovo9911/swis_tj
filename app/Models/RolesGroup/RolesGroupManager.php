<?php

namespace App\Models\RolesGroup;

use App\Models\BaseMlManager;
use App\Models\ConstructorDocumentsRoles\ConstructorDocumentsRoles;
use App\Models\Role\Role;
use App\Models\RolesGroupRoles\RolesGroupRoles;
use App\Models\RolesGroupRolesAttributes\RolesGroupRolesAttributes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class RolesGroupManager
 * @package App\Models\RolesGroup
 */
class RolesGroupManager
{
    use BaseMlManager;

    /**
     * Function to Store data to DB
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $data['user_id'] = Auth::id();
        $data['company_tax_id'] = Auth::user()->companyTaxId();

        $roleGroup = new RolesGroup($data);
        $roleGroupMl = new RolesGroupMl();

        DB::transaction(function () use ($data, $roleGroup, $roleGroupMl) {

            $roleGroup->save();
            $this->storeMl($data['ml'], $roleGroup, $roleGroupMl);

            //
            $this->storeRolesGroup($data['roles'], $roleGroup->id);
            $this->storeRoleGroupAttributes($data['roles'], $data['roles_attributes'] ?? [], $roleGroup->id);
        });
    }

    /**
     * Function to Update data
     *
     * @param array $data
     * @param int $id
     */
    public function update(array $data, int $id)
    {
        $roleGroup = RolesGroup::where('id', $id)->checkUserHasRole()->firstOrFail();
        $roleGroupMl = new RolesGroupMl();

        DB::transaction(function () use ($data, $roleGroup, $roleGroupMl) {

            $roleGroup->update($data);
            $this->updateMl($data['ml'], 'role_group_id', $roleGroup, $roleGroupMl);

            //
            $this->storeRolesGroup($data['roles'], $roleGroup->id);
            $this->storeRoleGroupAttributes($data['roles'], $data['roles_attributes'] ?? [], $roleGroup->id);
        });
    }

    /**
     * Function to store Roles Group
     *
     * @param $roles
     * @param $roleGroupId
     */
    private function storeRolesGroup($roles, $roleGroupId)
    {
        foreach ($roles as $role) {
            $data = [
                'role_id' => $role,
                'roles_group_id' => $roleGroupId
            ];

            RolesGroupRoles::updateOrCreate($data);
        }

        RolesGroupRoles::whereNotIn('role_id', $roles)->where(['roles_group_id' => $roleGroupId])->delete();
    }

    /**
     * Function to store role group attributes
     *
     * @param $roles
     * @param $rolesAttributes
     * @param $roleGroupId
     */
    private function storeRoleGroupAttributes($roles, $rolesAttributes, $roleGroupId)
    {
        if (count($rolesAttributes)) {
            $attributeIds = [];

            foreach ($rolesAttributes as $attribute) {

                // SAF
                if (substr($attribute, '0', '4') == Role::ROLE_ATTRIBUTE_TYPE_SAF . '_') {
                    $attributeId = str_replace(Role::ROLE_ATTRIBUTE_TYPE_SAF . '_', '', $attribute);

                    $safAttrFieldRoles = $this->constructorDocumentRoleByType(Role::ROLE_ATTRIBUTE_TYPE_SAF, $attributeId, $roles);

                    if ($safAttrFieldRoles->count()) {
                        foreach ($safAttrFieldRoles as $safAttrFieldRole) {
                            $attributeIds[] = $safAttrFieldRole->field_id;

                            $allRolesAttributes = [
                                'roles_group_id' => $roleGroupId,
                                'role_id' => $safAttrFieldRole->role_id,
                                'attribute_id' => $safAttrFieldRole->field_id,
                                'attribute_type' => Role::ROLE_ATTRIBUTE_TYPE_SAF,
                            ];

                            RolesGroupRolesAttributes::updateOrCreate($allRolesAttributes);
                        }
                    }
                }

                // MDM
                if (substr($attribute, '0', '4') == Role::ROLE_ATTRIBUTE_TYPE_MDM . '_') {
                    $attributeId = str_replace(Role::ROLE_ATTRIBUTE_TYPE_MDM . '_', '', $attribute);

                    $mdmFieldRoles = $this->constructorDocumentRoleByType(Role::ROLE_ATTRIBUTE_TYPE_MDM, $attributeId, $roles);

                    if ($mdmFieldRoles->count()) {
                        foreach ($mdmFieldRoles as $mdmFieldRole) {
                            $attributeIds[] = $mdmFieldRole->field_id;

                            $allRolesAttributes = [
                                'roles_group_id' => $roleGroupId,
                                'role_id' => $mdmFieldRole->role_id,
                                'attribute_id' => $mdmFieldRole->field_id,
                                'attribute_type' => Role::ROLE_ATTRIBUTE_TYPE_MDM,
                            ];

                            RolesGroupRolesAttributes::updateOrCreate($allRolesAttributes);
                        }
                    }
                }
            }

            RolesGroupRolesAttributes::whereNotIn('attribute_id', $attributeIds)->where(['roles_group_id' => $roleGroupId])->delete();
        } else {
            RolesGroupRolesAttributes::where(['roles_group_id' => $roleGroupId])->delete();
        }
    }

    /**
     * Function to get constructor document roles (mdm,saf)
     *
     * @param $attributeType
     * @param $fieldId
     * @param $roles
     * @return ConstructorDocumentsRoles
     */
    private function constructorDocumentRoleByType($attributeType, $fieldId, $roles)
    {
        return ConstructorDocumentsRoles::select('role_id', 'field_id')
            ->join('constructor_documents_roles_attribute', 'constructor_documents_roles.id', '=', 'constructor_documents_roles_attribute.constructor_documents_roles_id')
            ->join('constructor_documents', 'constructor_documents.id', '=', 'constructor_documents_roles.document_id')
            ->where('type', $attributeType)
            ->where('field_id', $fieldId)
            ->where('company_tax_id', Auth::user()->companyTaxId())->whereIn('role_id', $roles)->get();
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @param null $parentId
     * @return array
     */
    public function getLogData($id = null, $parentId = null)
    {
        if (!is_null($id)) {
            return RolesGroup::where('id', $id)->first();
        }

        return [];
    }
}
