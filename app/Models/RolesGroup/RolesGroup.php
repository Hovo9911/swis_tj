<?php

namespace App\Models\RolesGroup;

use App\Models\BaseModel;
use App\Models\Role\Role;
use App\Models\RolesGroupRolesAttributes\RolesGroupRolesAttributes;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class RolesGroup
 * @package App\Models\RolesGroup
 */
class RolesGroup extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'roles_group';

    /**
     * @var bool
     */
    protected $hasAdminInfo = true;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'user_id',
        'company_tax_id',
        'menu_id',
        'show_status',
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_admin_id',
        'created_admin_ip',
        'updated_at',
        'updated_admin_id',
        'updated_admin_ip',
        'deleted_admin_id',
        'deleted_admin_ip'
    ];

    /**
     * Function to get create_at value
     *
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format(config('swis.date_time_format_front'));
    }

    /**
     * Function to return trans by role name if role is default
     *
     * @param $name
     * @return string
     */
    public function getNameAttribute($name)
    {
        if (!is_null($this->current()) && !is_null($this->current()->name)) {
            return $this->current()->name;
        }

        return $name;
    }

    /**
     * Function to return trans by role description if role is default
     *
     * @param $description
     * @return string
     */
    public function getDescriptionAttribute($description)
    {
        if (!is_null($this->current()) && !is_null($this->current()->description)) {
            return $this->current()->description;
        }

        return $description;
    }

    /**
     * Function to relate with Role
     *
     * @return BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'roles_group_roles')->withPivot(['role_id', 'roles_group_id']);
    }

    /**
     * Function to relate with Role
     *
     * @return HasMany
     */
    public function attributes()
    {
        return $this->hasMany(RolesGroupRolesAttributes::class, 'roles_group_id', 'id');
    }

    /**
     * Function to return ml
     *
     * @return HasMany
     */
    public function ml()
    {
        return $this->hasMany(RolesGroupMl::class, 'role_group_id', 'id');
    }

    /**
     * Function to return current ml
     *
     * @return HasOne
     */
    public function current()
    {
        return $this->hasOne(RolesGroupMl::class, 'role_group_id', 'id')->where('lng_id', cLng('id'))->first();
    }

}
