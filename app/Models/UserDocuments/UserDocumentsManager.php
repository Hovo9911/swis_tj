<?php

namespace App\Models\UserDocuments;

use App\Models\Agency\Agency;
use App\Models\BaseModel;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\DataModel\DataModel;
use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm;
use App\Models\Instructions\Instructions;
use App\Models\Notifications\NotificationsManager;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplicationNotes\SingleApplicationNotes;
use App\Models\SingleApplicationProductsBatch\SingleApplicationProductsBatch;
use App\Models\SingleApplicationSubApplicationInstructionFeedback\SingleApplicationSubApplicationInstructionFeedback;
use App\Models\SingleApplicationSubApplicationInstructions\SingleApplicationSubApplicationInstructions;
use App\Models\SingleApplicationSubApplicationProducts\SingleApplicationSubApplicationProducts;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplicationCompleteData;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use App\Models\SingleApplicationSubApplicationsLaboratory\SingleApplicationSubApplicationsLaboratoryManager;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class ConstructorMapping
 * @package App\Models\ConstructorMapping
 */
class UserDocumentsManager extends Model
{
    /**
     * Function to Update data from my application
     *
     * @param array $data
     * @param $singleApplicationSubApp
     * @param array $oldCustomData
     * @param array $nonVisibleFields
     * @param array $notUpdatedFields
     * @param $state
     * @param $documentId
     * @param $superScribe
     * @param bool $isInstructionTabEditable
     * @return array
     */
    public function updateStoreData(array $data, $singleApplicationSubApp, array $oldCustomData, array $nonVisibleFields, $notUpdatedFields, $state, $documentId, $superScribe, $isInstructionTabEditable = true)
    {
        ini_set('memory_limit', '256M');
        return DB::transaction(function () use ($notUpdatedFields, $oldCustomData, $nonVisibleFields, $singleApplicationSubApp, $state, $data, $documentId, $superScribe, $isInstructionTabEditable) {

            $notUpdatedSortedFields = ['saf' => [], 'mdm' => []];
            foreach ($notUpdatedFields as $field => $value) {
                $type = (strpos($field, "SAF_ID_") !== false) ? 'saf' : 'mdm';
                $notUpdatedSortedFields[$type][$field] = $value;
            }

            // check if tab is editable update instruction id to active (if have not active id don't update)
            if ($isInstructionTabEditable) {
                $getSubApplicationInstructions = SingleApplicationSubApplicationInstructions::where('sub_application_id', $singleApplicationSubApp->id)->get();
                foreach ($getSubApplicationInstructions as $subAppInstruction) {
                    $getActiveInstruction = Instructions::where('code', $subAppInstruction->instruction_code)->where('show_status', BaseModel::STATUS_ACTIVE)->orderByDesc()->first();

                    if (!is_null($getActiveInstruction) && $getActiveInstruction->id != $subAppInstruction->instruction_id) {

                        SingleApplicationSubApplicationInstructionFeedback::where('saf_sub_application_instruction_id', $subAppInstruction->id)->where('instruction_id', $subAppInstruction->instruction_id)->update(['instruction_id' => $getActiveInstruction->id]);

                        $subAppInstruction->instruction_id = $getActiveInstruction->id;
                        $subAppInstruction->save();
                    }
                }
            }

            if (!empty($data['instructions_feedback'])) {
                $subApplicationInstructions = SingleApplicationSubApplicationInstructions::select('id', 'instruction_id', 'instruction_code')->whereIn('id', array_keys($data['instructions_feedback']))->get()->keyBy('id')->toArray();

                foreach ($data['instructions_feedback'] as $subApplicationInstructionId => $feedback) {
                    if(isset($feedback['feedback'])){
                        $getFeedback = getReferenceRows(ReferenceTable::REFERENCE_RISK_FEEDBACK, $feedback['feedback']);

                        SingleApplicationSubApplicationInstructionFeedback::updateOrCreate([
                            'saf_sub_application_instruction_id' => $subApplicationInstructionId,
                            'instruction_id' => $subApplicationInstructions[$subApplicationInstructionId]['instruction_id'],
                        ], [
                            'instruction_code' => $subApplicationInstructions[$subApplicationInstructionId]['instruction_code'],
                            'feedback_id' => $feedback['feedback'] ?? null,
                            'note' => $feedback['note'] ?? null,
                            'feedback_code' => isset($getFeedback) ? $getFeedback->code : null
                        ]);
                    }
                }
            }

            $customData = [];

            if ((!empty($data['customData']) || !empty($data['saf'])) || !empty($notUpdatedFields)) {
                $dataCustomData = !empty($data['customData']) ? $data['customData'] : [];
                $customData = $this->setNonVisibleFieldsValues($oldCustomData, $nonVisibleFields, $dataCustomData);
                $safNote = $data['saf']['notes']['notes'] ?? null;

                //--------------------- SAF notes part ---------------------//

                if ($safNote) {

                    $noteTransKey = SingleApplicationNotes::NOTE_TRANS_KEY_SUB_APP_SAVE_NOT;
                    if (isset($data['saf']['notes']['to_saf_holder'])) {
                        $noteTransKey = SingleApplicationNotes::NOTE_TRANS_KEY_SEND_FROM_SUB_APP_TO_SAF;
                    }

                    $note = $safNote;
                    $toSafHolder = isset($data['saf']['notes']['to_saf_holder']) ? SingleApplicationNotes::TRUE : SingleApplicationNotes::FALSE;

                    $noteData = [
                        'saf_number' => $singleApplicationSubApp->saf_number,
                        'note' => $note,
                        'sub_application_id' => $singleApplicationSubApp->id,
                        'to_saf_holder' => $toSafHolder,
                        'send_from_module' => SingleApplicationNotes::NOTE_SEND_MODULE_APPLICATION
                    ];

                    SingleApplicationNotes::storeNotes($noteData, $noteTransKey);
                    if ($toSafHolder) {
                        NotificationsManager::storeSafSubApplicationMessages($noteData, 'sub-application', $documentId);
                    }

                    unset($data['saf']['notes']);
                }

                //--------------------- SAF notes part END ---------------------//

                $customData = array_merge($customData, $notUpdatedSortedFields['mdm']);
                $this->updateColumnFields($customData, $documentId, $singleApplicationSubApp);

                $subApplicationData = $singleApplicationSubApp->data;

                if ($superScribe) {
                    $safData = json_encode($subApplicationData);
                } else {
                    if (isset($data['saf']['transportation']['state_number_customs_support'], $subApplicationData['saf']['transportation']['state_number_customs_support'])) {
                        $stateNumberCustomsSupport = $data['saf']['transportation']['state_number_customs_support'];
                        unset($data['saf']['transportation']['state_number_customs_support']);
                        $i = 1;
                        foreach ($subApplicationData['saf']['transportation']['state_number_customs_support'] as &$item) {
                            $item['track_container_temperature'] = $stateNumberCustomsSupport[$i]['track_container_temperature'];
                            $i++;
                        }
                    }
                    $data['saf'] = array_replace_recursive($subApplicationData['saf'], $data['saf']);
                    $safData['saf'] = $data['saf'];
                    $safData = json_encode($safData);
                }

                $updatedInfo = ['custom_data' => json_encode($customData), 'data' => $safData];

                if ($state->state_type != ConstructorStates::REQUEST_STATE_TYPE && $singleApplicationSubApp->applicationCurrentState->state->state_type != ConstructorStates::REQUEST_STATE_TYPE && $state->is_countable) {
                    $now = Carbon::now();
                    $updatedInfo['last_action_date'] = $now;
                    if (isset($singleApplicationSubApp->last_action_date)) {
                        $lastActionDate = Carbon::parse($singleApplicationSubApp->last_action_date);
//                        $diff = $now->diffInHours($lastActionDate);
                        $diff = Agency::getWorkingHoursBetweenDates($lastActionDate, $now, $singleApplicationSubApp->agency_id);
                        $updatedInfo['hour_spend'] = (int)$singleApplicationSubApp->hour_spend + $diff;
                    } else {
                        $updatedInfo['hour_spend'] = 0;
                    }
                }

                SingleApplicationSubApplications::where('id', $singleApplicationSubApp->id)->update($updatedInfo);

                // update All data
                $subApplication = SingleApplicationSubApplications::select('id', 'saf_number', 'data', 'custom_data', 'routings', 'routing_products', 'constructor_document_id')->with(['saf:id,saf_controlled_fields,regular_number', 'productList'])->where('id', $singleApplicationSubApp->id)->first();

                $singleApplicationSubApplicationCompleteData = new SingleApplicationSubApplicationCompleteData();
                $singleApplicationSubApplicationCompleteData->getParams();
                $singleApplicationSubApplicationCompleteData->setApplicationVariable($subApplication);

                list($safData, $productData, $productBatches) = $singleApplicationSubApplicationCompleteData->setData();
                $subApplication->all_data = json_encode(['data' => $safData, 'products' => array_values($productData), 'batches' => array_values($productBatches)]);
                $subApplication->save();
            }

            if (!empty($data['batch']) && isset($data['product_batches']) && count($data['product_batches']) > 0) {
                $safListsControlledFields = $this->checkNonVisibleBatchFields(reset($data['product_batches']));

                foreach ($data['batch'] as $productId => $batches) {
                    foreach ($batches as $batch) {
                        if (isset($data['product_batches'])) {

                            $getBatch = SingleApplicationProductsBatch::where('id', $batch['id'])->first();
                            $mdmData = is_null($getBatch->mdm_data) ? [] : $getBatch->mdm_data;
                            $dataFromView = isset($data['mdmData']) ? $data['mdmData'] : [];
                            $getBatch->mdm_data = array_replace_recursive($mdmData, $dataFromView);
                            $getBatch->save();
                            SingleApplicationProductsBatch::where('id', $batch['id'])->update(array_merge($safListsControlledFields, $data['product_batches'][$batch['id']]));
                        }
                    }
                }
            }

            if (isset($data['lab_examination']) && !empty($data['lab_examination'])) {
                $subApplicationLaboratoryManger = new SingleApplicationSubApplicationsLaboratoryManager();
                $subApplicationLaboratoryManger->store($data['lab_examination'], $singleApplicationSubApp->id);
            }

            return $customData;
        });

    }

    /**
     * Function to set Non Visible Fields Values
     *
     * @param $oldCustomData
     * @param $nonVisibleFields
     * @param $customData
     * @return array
     */
    public function setNonVisibleFieldsValues($oldCustomData, $nonVisibleFields, $customData)
    {
        foreach ($nonVisibleFields as $field => $value) {
            if (!array_key_exists($field, $customData) && array_key_exists($field, $oldCustomData)) {
                $customData[$field] = $oldCustomData[$field];
            }
        }

        if (isset($oldCustomData)) {
            foreach ($oldCustomData as $field => $item) {
                if (!array_key_exists($field, $customData)) {
                    $customData[$field] = $item;
                }
            }
        }

        return $customData;
    }

    /**
     * Function to check Non Visible Batch Fields
     *
     * @param $batch
     * @return array
     */
    public function checkNonVisibleBatchFields($batch)
    {
        $returnArray = [];
        $safListsControlledFields = ['approved_netto_weight', 'rejected_netto_weight', 'approved_quantity', 'approved_type', 'rejected_quantity', 'rejection_reason'];
        foreach ($safListsControlledFields as $field) {
            if (in_array($field, $batch)) {
                $returnArray[] = $field;
            }
        }

        return $returnArray;
    }

    /**
     * Function to products Numbers
     *
     * @param $safProductIds
     * @return string
     */
    public function productNumbers($safProductIds)
    {
        $documentProductNumbers = [];

        if (!is_null($this->productList)) {

            foreach ($this->productList->pluck('id')->all() as $documentProductId) {

                if ($safProductIds && isset($safProductIds[$documentProductId])) {
                    $documentProductNumbers[] = $safProductIds[$documentProductId];
                }
            }
        }

        return count($documentProductNumbers) ? implode(',', $documentProductNumbers) : "";
    }

    /**
     * Function to get Columns Fields
     *
     * @return array
     */
    public function getColumnFields()
    {
        return [
            'requestDate' => DataModel::getMdmFieldIDByConstant(DataModel::REQUEST_DATE_MDM_ID),
            'rejectionDate' => DataModel::getMdmFieldIDByConstant(DataModel::REJECTION_DATE_MDM_ID),
            'registrationDate' => DataModel::getMdmFieldIDByConstant(DataModel::REGISTRATION_DATE_MDM_ID),
            'permissionDate' => DataModel::getMdmFieldIDByConstant(DataModel::PERMISSION_DATE_MDM_ID),
            'registrationNumber' => DataModel::getMdmFieldIDByConstant(DataModel::REGISTRATION_NUMBER_MDM_ID),
            'permissionNumber' => DataModel::getMdmFieldIDByConstant(DataModel::PERMISSION_NUMBER_MDM_ID),
            'expirationDate' => DataModel::getMdmFieldIDByConstant(DataModel::EXPIRATION_DATE_MDM_ID),
            'rejectedNumber' => DataModel::getMdmFieldIDByConstant(DataModel::REJECTION_NUMBER_MDM_ID),
            'rejectedReason' => DataModel::getMdmFieldIDByConstant(DataModel::REJECTION_REASON_MDM_ID),
            'permissionDocument' => DataModel::getMdmFieldIDByConstant(DataModel::PERMISSION_DOCUMENT_MDM_ID),
            'totalNettoWeight' => DataModel::getMdmFieldIDByConstant(DataModel::TOTAL_NETTO_WEIGHT_MDM_ID)
        ];
    }

    /**
     * Function to update column fields
     *
     * @param $customData
     * @param $documentId
     * @param $singleApplicationSubApp
     * @return void
     */
    public function updateColumnFields($customData, $documentId, $singleApplicationSubApp)
    {
        extract($this->getColumnFields(), EXTR_OVERWRITE);
        $dataSet = DocumentsDatasetFromMdm::select('mdm_field_id', 'field_id')->where('document_id', $documentId)->whereIn('mdm_field_id', [$requestDate, $rejectionDate, $registrationDate, $permissionDate, $registrationNumber, $permissionNumber, $expirationDate, $rejectedNumber, $rejectedReason, $permissionDocument, $totalNettoWeight])->get()->keyBy('mdm_field_id')->toArray();

        SingleApplicationSubApplications::where('id', $singleApplicationSubApp->id)->update([
            'request_date' => isset($dataSet[$requestDate], $customData[$dataSet[$requestDate]['field_id']]) ? $customData[$dataSet[$requestDate]['field_id']] : $singleApplicationSubApp->request_date,
            'rejection_date' => isset($dataSet[$rejectionDate], $customData[$dataSet[$rejectionDate]['field_id']]) ? $customData[$dataSet[$rejectionDate]['field_id']] : $singleApplicationSubApp->rejection_date,
            'registration_date' => isset($dataSet[$registrationDate], $customData[$dataSet[$registrationDate]['field_id']]) ? $customData[$dataSet[$registrationDate]['field_id']] : $singleApplicationSubApp->registration_date,
            'permission_date' => isset($dataSet[$permissionDate], $customData[$dataSet[$permissionDate]['field_id']]) ? $customData[$dataSet[$permissionDate]['field_id']] : $singleApplicationSubApp->permission_date,
            'registration_number' => isset($dataSet[$registrationNumber], $customData[$dataSet[$registrationNumber]['field_id']]) ? $customData[$dataSet[$registrationNumber]['field_id']] : $singleApplicationSubApp->registration_number,
            'permission_number' => isset($dataSet[$permissionNumber], $customData[$dataSet[$permissionNumber]['field_id']]) ? $customData[$dataSet[$permissionNumber]['field_id']] : $singleApplicationSubApp->permission_number,
            'expiration_date' => isset($dataSet[$expirationDate], $customData[$dataSet[$expirationDate]['field_id']]) ? $customData[$dataSet[$expirationDate]['field_id']] : $singleApplicationSubApp->expiration_date,
            'rejected_number' => isset($dataSet[$rejectedNumber], $customData[$dataSet[$rejectedNumber]['field_id']]) ? $customData[$dataSet[$rejectedNumber]['field_id']] : $singleApplicationSubApp->rejected_number,
            'rejected_reason' => isset($dataSet[$rejectedReason], $customData[$dataSet[$rejectedReason]['field_id']]) ? $customData[$dataSet[$rejectedReason]['field_id']] : $singleApplicationSubApp->rejected_reason,
            'permission_document' => isset($dataSet[$permissionDocument], $customData[$dataSet[$permissionDocument]['field_id']]) ? $customData[$dataSet[$permissionDocument]['field_id']] : $singleApplicationSubApp->permission_document,
            'total_netto_weight' => isset($dataSet[$totalNettoWeight], $customData[$dataSet[$totalNettoWeight]['field_id']]) ? $customData[$dataSet[$totalNettoWeight]['field_id']] : $singleApplicationSubApp->total_netto_weight,
        ]);
    }

    /**
     * Function to get Hs Codes for lab expertise
     *
     * @param $validatedData
     * @param null $ids
     * @return SingleApplicationSubApplicationProducts
     */
    public function getHsCodesForLabExpertise($validatedData, $ids = null)
    {
        $hsCodeTable = ReferenceTable::REFERENCE_HS_CODE_6;

        return SingleApplicationSubApplicationProducts::select('saf_products.*', "{$hsCodeTable}.code as hs_code", "{$hsCodeTable}_ml.path_name as hs_code_path_name")
            ->join('saf_products', 'saf_products.id', '=', 'saf_sub_application_products.product_id')
            ->join($hsCodeTable, DB::raw("{$hsCodeTable}.id::varchar"), "=", "saf_products.product_code")
            ->join("{$hsCodeTable}_ml", function ($q) use ($hsCodeTable) {
                $q->on("{$hsCodeTable}_ml.{$hsCodeTable}_id", "=", "{$hsCodeTable}.id")->where('lng_id', cLng());
            })
            ->when(isset($ids), function ($q) use ($ids, $hsCodeTable) {
                $q->whereIn("saf_products.id", $ids);
            })
            ->where('subapplication_id', $validatedData['sub_application_id'])
            ->orderBy('saf_products.id')
            ->get();
    }
}
