<?php

namespace App\Models\UserDocuments;

use App\Models\Agency\Agency;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\Notifications\Notifications;
use App\Models\Notifications\NotificationsManager;
use App\Models\NotificationTemplates\NotificationTemplates;
use App\Models\SingleApplicationObligations\SingleApplicationObligations;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;

/**
 * Class ReferenceTableNotificationsManager
 * @package App\Models\ReferenceTable
 */
class UserDocumentsNotificationsManager
{
    /**
     * Function to Store Notifications and Send Email and SMS
     *
     * @param $obligation
     */
    public function storeNotification($obligation)
    {

        $obligationInfo = SingleApplicationObligations::where('id', $obligation->id)->first();
        $currentState = $obligationInfo->state_id;

        // Sub Application
        $subApplication = SingleApplicationSubApplications::where(['id' => $obligationInfo->subapplication_id, 'saf_number' => $obligationInfo->saf_number])->firstOrFail();
        $agency = Agency::select('tax_id')->where('id', $subApplication->agency_id)->first();
        $documentId = ConstructorDocument::where(['document_code' => $subApplication->document_code, 'company_tax_id' => $agency->tax_id])->pluck('id')->first();
        $currentStateUsers = ConstructorStates::getUsersByMapping($currentState, $documentId);
        $notificationTemplatesAll = NotificationTemplates::mlByCode(NotificationTemplates::OBLIGATION_PAYED_TEMPLATE);

        if ($notificationTemplatesAll->count()) {
            $constructorNotificationCurrentLng = $notificationTemplatesAll->where('lng_id', cLng('id'))->first();

            $url = url('/' . cLng('code') . '/user-documents/' . $documentId . '/application/' . $subApplication->id . '/edit');
            $urlInApp = Notifications::IN_APP_URL_PREFIX . '/user-documents/' . $documentId . '/application/' . $subApplication->id . '/edit';

            $userDocumentURL = "<a target='_blank' href='" . $url . "' class='mark-as-read'>" . $subApplication->document_number . "</a>";
            $userDocumentURLInApp = "<a target='_blank' href='" . $urlInApp . "' class='mark-as-read'>" . $subApplication->document_number . "</a>";

            foreach ($currentStateUsers as $user) {

                $transKeys = [
                    "{{USER_FULLNAME}}" => $user->first_name . " " . $user->last_name,
                    "{{SUB_APP_ID}}" => $userDocumentURL
                ];

                $inAppTransKeys = [
                    "{{USER_FULLNAME}}" => $user->first_name . " " . $user->last_name,
                    "{{SUB_APP_ID}}" => $userDocumentURLInApp
                ];

                //
                $notification = [
                    'notification_id' => $constructorNotificationCurrentLng->id,
                    'user_id' => $user->id,
                    'company_tax_id' => $agency->tax_id,
                    'notification_type' => Notifications::NOTIFICATION_TYPE_CUSTOM_TEMPLATE,
                    'email_subject' => strip_tags(Notifications::replaceVariableToValue($transKeys, $constructorNotificationCurrentLng->email_subject, ($user->lng_id ?? cLng('id')))),
                    'email_notification' => Notifications::replaceVariableToValue($transKeys, $constructorNotificationCurrentLng->email_notification, ($user->lng_id ?? cLng('id'))),
                    'sms_notification' => strip_tags(Notifications::replaceVariableToValue($transKeys, $constructorNotificationCurrentLng->sms_notification, ($user->lng_id ?? cLng('id')))),
                ];

                //
                $inApplicationNotes = [];
                foreach ($notificationTemplatesAll as $constructorNote) {
                    $inApplicationNotes[$constructorNote->lng_id] = [
                        'in_app_notification' => Notifications::replaceVariableToValue($inAppTransKeys, $constructorNote->in_app_notification, $constructorNote->lng_id)
                    ];
                }

                // Store in app notifications
                NotificationsManager::storeInAppNotifications($notification, $inApplicationNotes);

                // Send Email And SMS
                NotificationsManager::sendEmailAndSms($user, $notification);
            }
        }
    }

    /**
     * Function to get variables trans keys
     *
     * @param $sendArrayParams
     * @param $supApplication
     * @param $obligation
     * @param $transKeysType
     * @return array|void
     */
    public static function getVariablesTransKeys($sendArrayParams, $supApplication, $obligation, $transKeysType)
    {
        switch ($transKeysType) {
            case Notifications::IN_APP_TRANS_KEYS:
                return [
                    "{{USER_FULLNAME}}" => $sendArrayParams['user_fullname'],
                    "{{SAF_ID}}" => $sendArrayParams['saf_url_in_app'],
                    "{{SAF_DATE}}" => $sendArrayParams['saf_date'],
                    "{{SUB_APP_ID}}" => $sendArrayParams['sub_app_url_in_app'],
                    "{{DOC_NAME}}" => $sendArrayParams['doc_name'],
                    "{{CURRENT_STATE}}" => $sendArrayParams['current_state'],
                    "{{EXPERT_FULLNAME}}" => $sendArrayParams['expert_fullname'],
                    "{{NOTES}}" => $sendArrayParams['notes'],
                    "{{IMPORTER_FULLNAME}}" => $sendArrayParams['importer_fullname'],
                    "{{EXPORTER_FULLNAME}}" => $sendArrayParams['exporter_fullname'],
                    "{{APPLICANT_FULLNAME}}" => $sendArrayParams['applicant_fullname'],
                    "{{SENDER_FULLNAME}}" => $sendArrayParams['sender_fullname'],
                    "{{SUPERSCRIBE_EXPERT_FULLNAME}}" => $sendArrayParams['superscribe_expert_fullname'],
                    "{{AGENCY_NAME}}" => $sendArrayParams['agency_name'],
                    "{{OBLIGATION}}" => $obligation['obligationSum'],
                    "{{PAYMENT}}" => $obligation['paymentSum'],
                    "{{BALANCE}}" => $obligation['balanceSum'],
                ];
                break;
            case Notifications::EMAIL_TRANS_KEYS:
                return [
                    "{{USER_FULLNAME}}" => $sendArrayParams['user_fullname'],
                    "{{SAF_ID}}" => $sendArrayParams['saf_url'],
                    "{{SAF_DATE}}" => $sendArrayParams['saf_date'],
                    "{{SUB_APP_ID}}" => $sendArrayParams['sub_app_url'],
                    "{{DOC_NAME}}" => $sendArrayParams['doc_name'],
                    "{{CURRENT_STATE}}" => $sendArrayParams['current_state'],
                    "{{EXPERT_FULLNAME}}" => $sendArrayParams['expert_fullname'],
                    "{{NOTES}}" => $sendArrayParams['notes'],
                    "{{IMPORTER_FULLNAME}}" => $sendArrayParams['importer_fullname'],
                    "{{EXPORTER_FULLNAME}}" => $sendArrayParams['exporter_fullname'],
                    "{{APPLICANT_FULLNAME}}" => $sendArrayParams['applicant_fullname'],
                    "{{SENDER_FULLNAME}}" => $sendArrayParams['sender_fullname'],
                    "{{SUPERSCRIBE_EXPERT_FULLNAME}}" => $sendArrayParams['superscribe_expert_fullname'],
                    "{{AGENCY_NAME}}" => $sendArrayParams['agency_name'],
                    "{{OBLIGATION}}" => $obligation['obligationSum'],
                    "{{PAYMENT}}" => $obligation['paymentSum'],
                    "{{BALANCE}}" => $obligation['balanceSum'],
                ];
                break;
            case Notifications::SMS_TRANS_KEYS:
                return [
                    "{{USER_FULLNAME}}" => $sendArrayParams['user_fullname'],
                    "{{SAF_ID}}" => $supApplication->saf_number,
                    "{{SAF_DATE}}" => $sendArrayParams['saf_date'],
                    "{{SUB_APP_ID}}" => $supApplication->document_number,
                    "{{DOC_NAME}}" => $sendArrayParams['doc_name'],
                    "{{CURRENT_STATE}}" => $sendArrayParams['current_state'],
                    "{{EXPERT_FULLNAME}}" => $sendArrayParams['expert_fullname'],
                    "{{NOTES}}" => $sendArrayParams['notes'],
                    "{{IMPORTER_FULLNAME}}" => $sendArrayParams['importer_fullname'],
                    "{{EXPORTER_FULLNAME}}" => $sendArrayParams['exporter_fullname'],
                    "{{APPLICANT_FULLNAME}}" => $sendArrayParams['applicant_fullname'],
                    "{{SENDER_FULLNAME}}" => $sendArrayParams['sender_fullname'],
                    "{{SUPERSCRIBE_EXPERT_FULLNAME}}" => $sendArrayParams['superscribe_expert_fullname'],
                    "{{AGENCY_NAME}}" => $sendArrayParams['agency_name'],
                    "{{OBLIGATION}}" => $obligation['obligationSum'],
                    "{{PAYMENT}}" => $obligation['paymentSum'],
                    "{{BALANCE}}" => $obligation['balanceSum'],
                ];
                break;
        }
    }
}