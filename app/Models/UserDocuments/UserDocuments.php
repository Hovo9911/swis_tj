<?php

namespace App\Models\UserDocuments;

use App\Facades\Mailer;
use App\Models\Agency\Agency;
use App\Models\BaseModel;
use App\Models\ConstructorActions\ConstructorActions;
use App\Models\ConstructorActionSuperscribes\ConstructorActionSuperscribes;
use App\Models\ConstructorDataSet\ConstructorDataSet;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorDocument\ConstructorDocumentMl;
use App\Models\ConstructorMapping\ConstructorMapping;
use App\Models\ConstructorMapping\ConstructorMappingMl;
use App\Models\ConstructorMappingToRule\ConstructorMappingToRule;
use App\Models\ConstructorRules\ConstructorRules;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\DataModel\DataModel;
use App\Models\DataModel\DataModelManager;
use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm;
use App\Models\DocumentsDatasetFromSaf\DocumentsDatasetFromSaf;
use App\Models\Languages\Language;
use App\Models\Notifications\Notifications;
use App\Models\Notifications\NotificationsManager;
use App\Models\NotificationTemplates\NotificationTemplates;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\RegionalOffice\RegionalOffice;
use App\Models\Role\Role;
use App\Models\Routing\Routing;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure;
use App\Models\SingleApplicationObligations\SingleApplicationObligations;
use App\Models\SingleApplicationProducts\SingleApplicationProducts;
use App\Models\SingleApplicationProductsBatch\SingleApplicationProductsBatch;
use App\Models\SingleApplicationSubApplicationInstructions\SingleApplicationSubApplicationInstructions;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use App\Models\SingleApplicationSubApplicationStates\SingleApplicationSubApplicationStates;
use App\Models\User\User;
use App\Models\UserRoles\UserRoles;
use App\Sms\SmsManager;
use App\SwisRules\ValidationRules;
use Carbon\Carbon;
use Faker\Provider\Base;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class ConstructorMapping
 * @package App\Models\ConstructorMapping
 */
class UserDocuments extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'constructor_mapping';

    /**
     * @var null
     */
    private static $instance = null;

    /**
     * @return UserDocuments|null
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @var bool
     */
    protected $hasAdminInfo = true;

    /**
     * @var array
     */
    protected $fillable = [
        'document_id',
        'start_state',
        'role',
        'action',
        'end_state',
        'show_status'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'created_admin_id',
        'created_admin_ip',
        'updated_at',
        'updated_admin_id',
        'updated_admin_ip',
        'deleted_admin_id',
        'deleted_admin_ip'
    ];

    /**
     * @var string
     */
    const FIELD_LABEL = 'label';
    const FIELD_TOOLTIP = 'tooltip';
    const SSN_FIELD_IN_SEARCH = 'swis.base.label.tax_id_or_ssn';

    /**
     * @var array (type Constants for search fields)
     */
    const SEARCH_FIELD_TYPE = [
        'SAF_ID_5' => 'import',
        'SAF_ID_13' => 'export',
        'SAF_ID_42' => 'applicant'
    ];

    /**
     * @return HasMany
     */
    public function ml()
    {
        return $this->hasMany(ConstructorMappingMl::class, 'constructor_mapping_id', 'id');
    }

    /**
     * @return HasOne
     */
    public function current()
    {
        return $this->hasOne(ConstructorMappingMl::class, 'constructor_mapping_id', 'id')->notDeleted()->where('lng_id', cLng('id'))->first();
    }

    /**
     * @return array
     */
    public static function generateSafFields()
    {
        $safFields = [];
        $lastXML = SingleApplicationDataStructure::lastXml();
        $xmlTabs = $lastXML->xpath('/tabs/tab');

        if ($xmlTabs && count($xmlTabs) > 0) {
            foreach ($xmlTabs as $tab) {
                $tabKey = (string)$tab->attributes()->key;
                if (isset($tab->block, $tab->block->fieldset)) {
                    if (count($tab->block) > 1) {
                        foreach ($tab->block as $block) {
                            if (isset($block->fieldset)) {
                                $safFields = self::setTabField($safFields, $block->fieldset, $tabKey);
                            }

                            if (isset($block->subBlock, $block->subBlock->fieldset)) {
                                $safFields = self::setTabField($safFields, $block->subBlock->fieldset, $tabKey);
                            }
                        }
                    } else {
                        $safFields = self::setTabField($safFields, $tab->block->fieldset, $tabKey);
                    }

                } elseif (isset($tab->typeBlock)) {
                    foreach ($tab->typeBlock as $typeBlock) {
                        if (isset($typeBlock->block)) {
                            foreach ($typeBlock->block as $block) {
                                if (isset($block->fieldset)) {
                                    $safFields = self::setTabField($safFields, $block->fieldset, $tabKey);
                                }
                            }
                        }
                    }
                }
            }
        }

        return $safFields;
    }

    /**
     * Function to return array key = tabID value = fieldId
     *
     * @param $safFields
     * @param $data
     * @param $tabKey
     * @return array
     */
    private static function setTabField($safFields, $data, $tabKey)
    {
        foreach ($data as $field) {
            $attr = (count($field->field) == 1) ? $field->field->attributes() : $field->field[0]->attributes();
            $safFields[$tabKey][] = (string)$attr->id;
        }

        return $safFields;
    }

    /**
     * Function to return hidden tabs
     *
     * @param $documentId
     * @param $lists
     * @param $globalHiddenFields
     * @param $mdmFields
     * @return array
     */
    public static function getHiddensTabs($documentId, $lists, $globalHiddenFields, $mdmFields)
    {
        $hiddenFields = $hiddenTabs = [];
        $tabFields = (new self)->mergeMdmFieldsToTabFields(self::generateSafFields(), $mdmFields);

        if ($tabFields && count($tabFields) > 0 && !empty($lists)) {
            foreach ($tabFields as $tabKey => $tab) {
                foreach ($tab as $field) {

                    if (isset($globalHiddenFields['mdm']) && in_array($field, $globalHiddenFields['mdm'])) {
                        $hiddenFields[$tabKey][] = $field;
                        continue;
                    }
                    if (isset($globalHiddenFields['saf']) && in_array($field, $globalHiddenFields['saf'])) {
                        $hiddenFields[$tabKey][] = $field;
                        continue;
                    }
                    if (array_key_exists($field, $lists) && in_array("non_visible", $lists[$field])) {
                        $hiddenFields[$tabKey][] = $field;
                    }
                }
            }
        }

        if (!empty($hiddenFields)) {
            foreach ($hiddenFields as $tabKey => $hidden) {
                if (count($hidden) == count($tabFields[$tabKey])) {
                    $hiddenTabs[] = $tabKey;
                }
            }
        }

        return $hiddenTabs;
    }

    /**
     * @param $tabFields
     * @param $mdmFields
     * @return array
     */
    private function mergeMdmFieldsToTabFields($tabFields, $mdmFields)
    {
        if (!empty($mdmFields)) {
            foreach ($mdmFields as $field) {
                $tabFields[$field['tab']][] = $field['field_id'];
            }
        }

        return $tabFields;
    }

    /**
     * Function to get global hidden elements from dataSet
     *
     * @param $documentId
     * @return array
     */
    public static function getGlobalHiddenElements($documentId)
    {
        $hiddenElements = [];
        $dataSetMdm = DocumentsDatasetFromMdm::select('field_id', 'document_id', 'mdm_field_id')->where('document_id', $documentId)->where('is_hidden', true)->get();
        $dataSetSaf = DocumentsDatasetFromSaf::select('saf_field_id', 'document_id')->where('document_id', $documentId)->where('is_hidden', true)->get();

        $hiddenElements = self::setGlobalHideElements($hiddenElements, $dataSetMdm, 'mdm');
        $hiddenElements = self::setGlobalHideElements($hiddenElements, $dataSetSaf, 'saf');

        return $hiddenElements;
    }

    /**
     * Function to set global hidden elements from dataSet
     *
     * @param $hiddenElements
     * @param $dataSet
     * @param $type
     * @return array
     */
    private static function setGlobalHideElements($hiddenElements, $dataSet, $type)
    {
        foreach ($dataSet as $item) {
            $hiddenElements[$type][] = ($type == 'mdm') ? $item->field_id : $item->saf_field_id;
        }

        return $hiddenElements;
    }

    // ========================== My Application Search =========================== //

    /**
     * Function to get search fields
     *
     * @param $docId
     * @param $lastXml
     * @return array
     */
    public static function getSearchFields($docId, $lastXml)
    {
        $userDocuments = (new self);
        $getSafSearchFields = $userDocuments->getSafSearchFields($docId, $lastXml);
        $getAllSearchFields = $userDocuments->getMdmSearchFields($docId, $getSafSearchFields);

        return !empty($getAllSearchFields) ? $getAllSearchFields : ['text' => [], 'date' => []];
    }

    /**
     * Function to get search saf fields
     *
     * @param $docId
     * @param $lastXml
     * @return array
     */
    private function getSafSearchFields($docId, $lastXml)
    {
        $returnArray = [];
        $fields = DocumentsDatasetFromSaf::select('id', 'saf_field_id')->where(function ($q) {
            $q->where(['is_search_param' => true, 'is_hidden' => false])->orWhereIn('saf_field_id', ConstructorDataSet::DEFAULT_SEARCH_PARAMETER_FIELDS);
        })->where('document_id', $docId)->with('current')->get();

        foreach ($fields as $field) {
            $xmlField = $lastXml->xpath("//*[@id='{$field->saf_field_id}']");
            $fieldAttr = $xmlField[0]->attributes();
            $currentLabel = optional($field->current)->label;

            if (isset($fieldAttr->title) && $fieldAttr->title == UserDocuments::SSN_FIELD_IN_SEARCH) {
                $currentLabel = !empty($currentLabel) ? $currentLabel : trans((string)$fieldAttr->title . '.' . UserDocuments::SEARCH_FIELD_TYPE[(string)$fieldAttr->id]);
            } else {
                $currentLabel = !empty($currentLabel) ? $currentLabel : trans((string)$fieldAttr->title);
            }

            $mdmFieldType = (isset($fieldAttr->mdm)) ? $this->getMdmFieldById($fieldAttr->mdm, true) : $fieldAttr->type;
            $returnArray = $this->setSearchArray($returnArray, 'saf', (string)$fieldAttr->name, $currentLabel, $mdmFieldType, $field->saf_field_id);
        }

        return $returnArray;
    }

    /**
     * Function to get search mdm fields
     *
     * @param $docId
     * @param $returnArray
     * @return array
     */
    private function getMdmSearchFields($docId, $returnArray)
    {
        $fields = DocumentsDatasetFromMdm::select('mdm_field_id', 'label', 'field_id')
            ->join('documents_dataset_from_mdm_ml', 'documents_dataset_from_mdm.id', '=', 'documents_dataset_from_mdm_ml.documents_dataset_from_mdm_id')
            ->where('lng_id', cLng('id'))
            ->where(['document_id' => $docId, 'is_search_param' => true, 'is_hidden' => false])
            ->get();

        foreach ($fields as $field) {
            $mdmFieldType = $this->getMdmFieldById($field->mdm_field_id, true);
            $returnArray = $this->setSearchArray($returnArray, 'mdm', $field->field_id, $field->label, $mdmFieldType, $field->field_id);
        }

        return $returnArray;
    }

    /**
     * Function to set search array
     *
     * @param $returnArray
     * @param $type
     * @param $name
     * @param $label
     * @param $mdmFieldId
     * @param $fieldId
     * @param string $reference
     * @return array
     */
    private function setSearchArray($returnArray, $type, $name, $label, $mdmFieldId, $fieldId, $reference = '')
    {
        $sort = ($mdmFieldId['type'] == 'datetime' || $mdmFieldId['type'] == 'date') ? 'date' : 'text';

        if ($mdmFieldId['type'] == 'varchar') {
            $mdmFieldId['type'] = 'text';
        }

        $returnArray[$sort][] = [
            'fieldType' => $type,
            'name' => $name,
            'mdm_type' => $mdmFieldId['mdm_type'],
            'type' => ($mdmFieldId['type'] == 'datetime') ? ['type' => 'date'] : $mdmFieldId,
            'title' => $label,
            'field_id' => $fieldId,
        ];

        return $returnArray;
    }

    /**
     * Function to get mdm field by id
     *
     * @param $mdmId
     * @param bool $returnType
     * @return array
     */
    private function getMdmFieldById($mdmId, $returnType = false)
    {
        $dataModelAllData = DataModel::allData();

        $mdmfield = $dataModelAllData[(string)$mdmId];

        if ($mdmfield['reference']) {
            $type = 'autocomplete';
        } else {
            $type = $returnType ? DataModelManager::getDbType($mdmfield['type']) : $mdmfield;
        }

        return [
            'type' => $type,
            'mdm_type' => $mdmfield['type'],
            'reference' => ($type == 'autocomplete') ? $mdmfield['reference'] : ''
        ];
    }

    /**
     * Function to return array with database credentials for where
     *
     * @param $searchData
     * @param $query
     * @param $documentId
     * @param $selectFields
     * @param $fieldPlaces
     * @return Builder
     */
    public static function getJsonFilter($searchData, $query, $documentId, $selectFields, $fieldPlaces)
    {
        $notJsonSearchedField = ['saf[general][sub_application_access_token]'];
        $fieldIdNames = getSafIdNameArray();

        if (!empty($searchData)) {
            foreach ($searchData as $field => $value) {
                $fieldId = $field;
                if (strpos($field, '_start_date_filter') !== false) {
                    $field = str_replace('_start_date_filter', '', $field);
                    if (array_key_exists($field, $fieldIdNames)) {
                        $field = $fieldIdNames[$field];
                    }
                    $field .= "_start_date_filter";
                } elseif (strpos($field, '_end_date_filter') !== false) {
                    $field = str_replace('_end_date_filter', '', $field);
                    if (array_key_exists($field, $fieldIdNames)) {
                        $field = $fieldIdNames[$field];
                    }
                    $field .= "_end_date_filter";
                } elseif (array_key_exists($field, $fieldIdNames)) {
                    $field = $fieldIdNames[$field];
                }

                if ($field == 'search_by_superscribe_field') {
                    continue;
                }

                // Search with Hs Code or Hs Descriptions
                if ($field == 'product_code' || $field == 'product_description') {

                    if (!BaseModel::isJoined($query, 'saf_sub_application_products')) {
                        $query->join('saf_sub_application_products', 'saf_sub_applications.id', '=', 'saf_sub_application_products.subapplication_id')
                            ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id');
                    }

                    if ($field == 'product_code') {
                        $query->where('saf_products.product_code', $value);
                    }

                    if ($field == 'product_description') {

                        $hsCodeRef = ReferenceTable::REFERENCE_HS_CODE_6;
                        $hsCodeRefMl = $hsCodeRef . '_ml';

                        $hsCodeIds = DB::table($hsCodeRef)->join($hsCodeRefMl, $hsCodeRef . '.id', '=', $hsCodeRefMl . '.' . $hsCodeRef . '_id')
                            ->where($hsCodeRefMl . '.lng_id', cLng('id'))
                            ->where(function ($q) use ($value) {

                                $value = str_replace(['...', '…'], '', $value);
                                $q->where('name', 'ILIKE', '%' . trim($value) . '%')
                                    ->orWhere('long_name', 'ILIKE', '%' . $value . '%')
                                    ->orWhere('code', 'ILIKE', '%' . $value . '%');
                            })
                            ->where($hsCodeRef . '.show_status', ReferenceTable::STATUS_ACTIVE)
                            ->pluck('id')->all();

                        $query->whereIn('saf_products.product_code', $hsCodeIds);
                    }

                    continue;
                }

                // Superscribe user
                if ($field == 'superscribe_id') {
//                    join('constructor_action_superscribes', 'saf_sub_applications.id', '=', 'constructor_action_superscribes.saf_subapplication_id')

                    $query->where(['constructor_action_superscribes.constructor_document_id' => $documentId, 'to_user' => $value, 'constructor_action_superscribes.is_current' => ConstructorActionSuperscribes::TRUE]);

                    continue;
                }

                if ($field == 'unsuperscribe_sub_applications') {
                    $query->whereNull('constructor_action_superscribes.id');

                    continue;
                }

                // Saf Importer
                if ($field == 'saf_importer' || $field == 'saf[sides][import][type_value]') {
                    $query->where('importer_value', $value);
                    continue;
                }

                // Saf Exporter
                if ($field == 'saf_exporter' || $field == 'saf[sides][export][type_value]') {
                    $query->where('exporter_value', $value);
                    continue;
                }

                // Saf Applicant
                if ($field == 'saf_applicant' || $field == 'saf[sides][applicant][type_value]') {
                    $query->where('applicant_value', $value);
                    continue;
                }

                // Saf Applicant Name
                if ($field == 'saf_applicant_name' || $field == 'saf[sides][applicant][name]') {

                    $query->where('saf.data->saf->sides->applicant->name', 'ilike', "%{$value}%");

                    continue;
                }

                // Saf Importer Name
                if ($field == 'saf_importer_name' || $field == 'saf[sides][import][name]') {

                    $query->where('saf.data->saf->sides->import->name', 'ilike', "%{$value}%");

                    continue;
                }

                // Saf Exporter Name
                if ($field == 'saf_exporter_name' || $field == 'saf[sides][export][name]') {

                    $query->where('saf.data->saf->sides->export->name', 'ilike', "%{$value}%");

                    continue;
                }

                //
                if (in_array($field, $notJsonSearchedField)) {
                    continue;
                }
                // if for Saf elements and else for Mdm element
                $availableIds = [];
                $isDate = (new self)->checkIsFieldDate($field);
                $isSelectField = (in_array($field, $selectFields)) ? true : false;
                $dataset = DocumentsDatasetFromMdm::select('id', 'mdm_field_id')->where('document_id', $documentId)->where('field_id', $field)->first();
                if (is_null($dataset)) {
                    if (isset(getSafXmlFieldByID($fieldId)[0])) {
                        $mdm = DataModel::select('id', 'type', 'reference')->where(DB::raw('id::varchar'), (int)getSafXmlFieldByID($fieldId)[0]->attributes()->mdm)->first();

                        $isMultipleField = (!is_null($mdm) && !is_null(optional($mdm)->type) && $mdm->type == DataModel::MDM_ELEMENT_TYPE_MULTIPLE) ? true : false;
                        if (!is_null($mdm->reference)) {
                            $refData = DB::table("reference_" . $mdm->reference)->select('code')->where('id', $value)->first();
                            $availableIds = DB::table("reference_" . $mdm->reference)->where('code', $refData->code)->pluck('id')->all();
                        }
                    }

                } else {
                    $isMultipleField = (!is_null($dataset) && !is_null($dataset->mdm) && !is_null(optional($dataset->mdm)->type) && $dataset->mdm->type == DataModel::MDM_ELEMENT_TYPE_MULTIPLE) ? true : false;
                }

                $fieldOriginalName = $field;
                $field = (new self)->getFieldCorrectName($fieldId, $isDate, $fieldPlaces);
                $fieldId = (isset($isDate->isDate) && $isDate->isDate) ? str_replace($isDate->prefix, "", str_replace("]", "", $fieldId)) : str_replace("]", "", $fieldId);

                if (isset($fieldPlaces[$fieldId]) && ($fieldPlaces[$fieldId] == 'products' || $fieldPlaces[$fieldId] == 'batches')) {
                    $query->whereRaw('(' . $field . ')::jsonb @> \'[{"' . $fieldId . '":"' . $value . '"}]\'');
                } else {
                    if ($isDate->isDate) {
                        if (isDateFormat($value)) {
                            $query->where($field, $isDate->simbol, $value);
                        }
                    } else {
                        if (is_array($value)) {
                            if ($isMultipleField) {
                                $field = str_replace($fieldOriginalName, "'{$fieldOriginalName}'", $field);
                                $query->whereRaw("({$field})::jsonb ~@& ARRAY['" . implode("','", $value) . "']");
                            } else {
                                foreach ($value as $item) {
                                    $item = ($isSelectField) ? $item : "%{$item}%";
                                    $query->where($field . '->' . key($value), 'iLike', $item);
                                }
                            }
                        } else {
                            if ($isSelectField) {
                                $query->whereIn($field, $availableIds);
                            } else {
                                $query->where($field, 'iLike', "%{$value}%");
                            }
                        }
                    }
                }

            }
        }

        return $query;
    }

    /**
     * Function to correct saf field name
     *
     * @param $field
     * @param $isDate
     * @param $fieldPlaces
     * @return string
     */
    private function getFieldCorrectName($field, $isDate, $fieldPlaces)
    {
        $field = (isset($isDate->isDate) && $isDate->isDate) ? str_replace($isDate->prefix, "", str_replace("]", "", $field)) : str_replace("]", "", $field);
        if ($fieldPlaces[$field] == 'products' || $fieldPlaces[$field] == 'batches') {
            return "saf_sub_applications.all_data->'{$fieldPlaces[$field]}'";
        } else {
            if ($field == 'SAF_ID_139') {
                return "saf_sub_applications.status_date";
            }

            return "saf_sub_applications.all_data->{$fieldPlaces[$field]}->{$field}";
        }
    }

    /**
     * Function to check is date field and return query builder
     *
     * @param $field
     * @return object
     */
    private function checkIsFieldDate($field)
    {
        $where = ['isDate' => false];
        if (strpos($field, '_start_date_filter') !== false) {
            $where = ['isDate' => true, 'simbol' => '>=', 'prefix' => '_start_date_filter'];
        } elseif (strpos($field, '_end_date_filter') !== false) {
            $where = ['isDate' => true, 'simbol' => '<=', 'prefix' => '_end_date_filter'];
        }

        return (object)$where;
    }

    /**
     * Function to generate custom data array
     *
     * @param $customData
     * @param bool $isPdfGeneration
     * @return array
     */
    public static function generateCustomData($customData, $isPdfGeneration = false)
    {
        if (!empty($customData)) {

            $documentDataSetAll = DocumentsDatasetFromMdm::select('field_id', 'mdm_field_id', 'data_model_excel.reference')
                ->join('data_model_excel', DB::raw('data_model_excel.id::varchar'), '=', 'documents_dataset_from_mdm.mdm_field_id')
                ->get()->keyBy('field_id');

            foreach ($customData as $fieldId => &$value) {
                if (!is_null($value)) {

                    if (empty($documentDataSetAll[$fieldId]->reference)) {
                        continue;
                    }

                    $refValue = getReferenceRows($documentDataSetAll[$fieldId]->reference, $value);

                    if (!is_null($refValue)) {
                        if ($isPdfGeneration) {
                            $value = optional($refValue)->name;
                        } else {
                            $value = [
                                'value' => $value,
                                'text' => optional($refValue)->name
                            ];
                        }
                    }
                }
            }
        }

        return $customData;
    }

    // ========================== My Application Search End =========================== //


    // ========================== My Application Send Notifications Start=========================== //

    /**
     * Function to send users notification about application state
     *
     * @param $mapping
     * @param $saf
     * @param $documentId
     * @param $applicationId
     * @param $userDocumentsRequest
     */
    public static function sendNotifications($mapping, $saf, $documentId, $applicationId, $userDocumentsRequest)
    {
        //TODO xntrvume poxel amboxjhutyamb

        $self = (new self);
        $sendArray = [];
        $recipientsList = $self->recipientsList($mapping);
        $docName = ConstructorDocumentML::select('constructor_documents_id', 'document_name')->where('constructor_documents_id', $documentId)->where('lng_id', cLng('id'))->first();
        $expertFullName = Auth::user()->name();

        $importer = $saf->data['saf']['sides']['import'] ?? '';
        $exporter = $saf->data['saf']['sides']['export'] ?? '';
        $applicant = $saf->data['saf']['sides']['applicant'] ?? '';

        $referenceLegalEntities = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_LEGAL_ENTITY_FORM_TABLE_NAME);

        $senderUserId = SingleApplicationSubApplicationStates::where('saf_subapplication_id', $applicationId)->orderByAsc()->pluck('user_id')->first();
        $superscribeExpertId = ConstructorActionSuperscribes::where('saf_subapplication_id', $applicationId)->orderByDesc()->pluck('to_user')->first();

        $companyTaxId = Auth::user()->companyTaxId();
        $senderUser = Auth::user()->name($senderUserId);
        $superscribeExpertUser = Auth::user()->name($superscribeExpertId);

        if (is_null($saf->user)) {
            return;
        }

        $languageCode = Language::where(['id' => $saf->user->lng_id])->pluck('code')->first();
        $safInfo = SingleApplication::select('regime', 'regular_number')->where('id', $saf->id)->first();
        $supApplication = SingleApplicationSubApplications::where('id', $applicationId)->first();

        $urlForImporterExporterApplicant = url('/' . $languageCode . '/single-application/view/' . $safInfo->regime . '/' . $safInfo->regular_number . '');
        $urlForImporterExporterApplicantInApp = Notifications::IN_APP_URL_PREFIX . '/single-application/view/' . $safInfo->regime . '/' . $safInfo->regular_number . '';

        $urlForAgencies = url('/' . $languageCode . '/user-documents/' . $documentId . '/application/' . $applicationId . '/edit');
        $urlForAgenciesInApp = Notifications::IN_APP_URL_PREFIX . '/user-documents/' . $documentId . '/application/' . $applicationId . '/edit';

        $safUrlForImporterExporterApplicant = "<a target='_blank' href='" . $urlForImporterExporterApplicant . "' class='mark-as-read'>" . $safInfo->regular_number . "</a>";
        $safUrlForImporterExporterApplicantInApp = "<a target='_blank' href='" . $urlForImporterExporterApplicantInApp . "' class='mark-as-read'>" . $safInfo->regular_number . "</a>";

        $safUrlForNextStateUsersAgencies = "<a target='_blank' href='" . $urlForAgencies . "' class='mark-as-read'>" . $safInfo->regular_number . "</a>";
        $safUrlForNextStateUsersAgenciesInApp = "<a target='_blank' href='" . $urlForAgenciesInApp . "' class='mark-as-read'>" . $safInfo->regular_number . "</a>";

        $safSubAppUrlForImporterExporterApplicant = "<a target='_blank' href='" . $urlForImporterExporterApplicant . "' class='mark-as-read'>" . $supApplication->document_number . "</a>";
        $safSubAppUrlForImporterExporterApplicantInApp = "<a target='_blank' href='" . $urlForImporterExporterApplicantInApp . "' class='mark-as-read'>" . $supApplication->document_number . "</a>";

        $safSubAppUrlForNextStateUsersAgencies = "<a target='_blank' href='" . $urlForAgencies . "' class='mark-as-read'>" . $supApplication->document_number . "</a>";
        $safSubAppUrlForNextStateUsersAgenciesInApp = "<a target='_blank' href='" . $urlForAgenciesInApp . "' class='mark-as-read'>" . $supApplication->document_number . "</a>";

        // APPLICANT
        if (in_array(Notifications::NOTIFICATION_TYPE_APPLICANT, $recipientsList)) {
            if ($saf->user->show_status == User::STATUS_ACTIVE) {
                $sendArray[] = [
                    'user_fullname' => $saf->user->name($saf->user->id),
                    'user_id' => $saf->user->id,
                    'company_tax_id' => $saf->company_tax_id,
                    'email' => $saf->user->email,
                    'phone_number' => $saf->user->phone_number,
                    'lng_id' => $saf->user->lng_id,
                    'saf_id' => $saf->id,
                    'saf_url' => $safUrlForImporterExporterApplicant,
                    'saf_url_in_app' => $safUrlForImporterExporterApplicantInApp,
                    'saf_date' => $saf->created_at,
                    'sub_app_id' => $applicationId,
                    'sub_app_url' => $safSubAppUrlForImporterExporterApplicant,
                    'sub_app_url_in_app' => $safSubAppUrlForImporterExporterApplicantInApp,
                    'doc_name' => is_null($docName) ? '' : $docName->document_name,
                    'current_state' => $mapping->end_state,
                    'obligation' => 0.00,
                    'expert_fullname' => $expertFullName,
                    'notes' => isset($userDocumentsRequest['saf']['notes'], $userDocumentsRequest['saf']['notes']['notes']) ? $userDocumentsRequest['saf']['notes']['notes'] : '',
                    'template_id' => $mapping->applicant,
                    'importer_fullname' => $importer['name'],
                    'exporter_fullname' => $exporter['name'],
                    'applicant_fullname' => $applicant['name'],
                    'sender_fullname' => is_null($senderUser) ? '' : $senderUser,
                    'superscribe_expert_fullname' => is_null($superscribeExpertUser) ? '' : $superscribeExpertUser,
                    'agency_name' => $companyTaxId,
                    'notification_type' => Notifications::NOTIFICATION_TYPE_APPLICANT
                ];
            }

            if (!is_null($applicant['type_value'])) {
                $localHeads = UserRoles::where(['role_id' => Role::HEAD_OF_COMPANY, 'company_tax_id' => $applicant['type_value']])->activeRole()->pluck('user_id')->all();
                $localHeadsUsers = User::where('id', '!=', $saf->user->id)->whereIn('id', $localHeads)->active()->get();

                foreach ($localHeadsUsers as $localHeadsUser) {
                    if (!is_null($localHeadsUser)) {
                        $sendArray[] = [
                            'user_fullname' => $localHeadsUser->first_name . ' ' . $localHeadsUser->last_name,
                            'email' => $localHeadsUser->email,
                            'phone_number' => $localHeadsUser->phone_number,
                            'user_id' => $localHeadsUser->id,
                            'company_tax_id' => $applicant['type_value'],
                            'lng_id' => $saf->user->lng_id,
                            'saf_id' => $saf->id,
                            'saf_url' => $safUrlForImporterExporterApplicant,
                            'saf_url_in_app' => $safUrlForImporterExporterApplicantInApp,
                            'saf_date' => $saf->created_at,
                            'sub_app_id' => $applicationId,
                            'sub_app_url' => $safSubAppUrlForImporterExporterApplicant,
                            'sub_app_url_in_app' => $safSubAppUrlForImporterExporterApplicantInApp,
                            'doc_name' => is_null($docName) ? '' : $docName->document_name,
                            'current_state' => $mapping->end_state,
                            'obligation' => 0.00,
                            'expert_fullname' => $expertFullName,
                            'notes' => isset($userDocumentsRequest['saf']['notes'], $userDocumentsRequest['saf']['notes']['notes']) ? $userDocumentsRequest['saf']['notes']['notes'] : '',
                            'template_id' => $mapping->applicant,
                            'importer_fullname' => $importer['name'],
                            'exporter_fullname' => $exporter['name'],
                            'applicant_fullname' => $applicant['name'],
                            'sender_fullname' => is_null($senderUser) ? '' : $senderUser,
                            'superscribe_expert_fullname' => is_null($superscribeExpertUser) ? '' : $superscribeExpertUser,
                            'agency_name' => $companyTaxId,
                            'notification_type' => Notifications::NOTIFICATION_TYPE_APPLICANT
                        ];
                    }
                }
            }

            $subApplicationSederUser = User::where('id', $senderUserId)->active()->first();

            if ($saf->user->id != $senderUserId && !is_null($subApplicationSederUser)) {

                $sendArray[] = [
                    'user_fullname' => $subApplicationSederUser->name($subApplicationSederUser->id),
                    'user_id' => $subApplicationSederUser->id,
                    'company_tax_id' => $supApplication->user_company_tax_id,
                    'email' => $subApplicationSederUser->email,
                    'phone_number' => $subApplicationSederUser->phone_number,
                    'lng_id' => $subApplicationSederUser->lng_id,
                    'saf_id' => $saf->id,
                    'saf_url' => $safUrlForImporterExporterApplicant,
                    'saf_url_in_app' => $safUrlForImporterExporterApplicantInApp,
                    'saf_date' => $saf->created_at,
                    'sub_app_id' => $applicationId,
                    'sub_app_url' => $safSubAppUrlForImporterExporterApplicant,
                    'sub_app_url_in_app' => $safSubAppUrlForImporterExporterApplicantInApp,
                    'doc_name' => is_null($docName) ? '' : $docName->document_name,
                    'current_state' => $mapping->end_state,
                    'obligation' => 0.00,
                    'expert_fullname' => $expertFullName,
                    'notes' => isset($userDocumentsRequest['saf']['notes'], $userDocumentsRequest['saf']['notes']['notes']) ? $userDocumentsRequest['saf']['notes']['notes'] : '',
                    'template_id' => $mapping->applicant,
                    'importer_fullname' => $importer['name'],
                    'exporter_fullname' => $exporter['name'],
                    'applicant_fullname' => $applicant['name'],
                    'sender_fullname' => is_null($senderUser) ? '' : $senderUser,
                    'superscribe_expert_fullname' => is_null($superscribeExpertUser) ? '' : $superscribeExpertUser,
                    'agency_name' => $companyTaxId,
                    'notification_type' => Notifications::NOTIFICATION_TYPE_APPLICANT
                ];

            }
        }

        // IMPORTER EXPORTER
        if (in_array(Notifications::NOTIFICATION_TYPE_IMPORTER_EXPORTER, $recipientsList)) {

            $data = SingleApplication::getSidesUser($importer);
            $currentRegimeData = $importer;

            $sendArray[] = [
                'user_fullname' => $importer['name'],
                'email' => $importer['email'],
                'phone_number' => $importer['phone_number'],
                'user_id' => $data['userId'],
                'company_tax_id' => $data['companyTaxId'],
                'lng_id' => $saf->user->lng_id,
                'saf_id' => $saf->id,
                'saf_url' => $safUrlForImporterExporterApplicant,
                'saf_url_in_app' => $safUrlForImporterExporterApplicantInApp,
                'saf_date' => $saf->created_at,
                'sub_app_id' => $applicationId,
                'sub_app_url' => $safSubAppUrlForImporterExporterApplicant,
                'sub_app_url_in_app' => $safSubAppUrlForImporterExporterApplicantInApp,
                'doc_name' => is_null($docName) ? '' : $docName->document_name,
                'current_state' => $mapping->end_state,
                'obligation' => 0.00,
                'expert_fullname' => $expertFullName,
                'notes' => isset($userDocumentsRequest['saf']['notes'], $userDocumentsRequest['saf']['notes']['notes']) ? $userDocumentsRequest['saf']['notes']['notes'] : '',
                'template_id' => $mapping->importer_exporter,
                'importer_fullname' => $importer['name'],
                'exporter_fullname' => $exporter['name'],
                'applicant_fullname' => $applicant['name'],
                'sender_fullname' => is_null($senderUser) ? '' : $senderUser,
                'superscribe_expert_fullname' => is_null($superscribeExpertUser) ? '' : $superscribeExpertUser,
                'agency_name' => $companyTaxId,
                'notification_type' => Notifications::NOTIFICATION_TYPE_IMPORTER_EXPORTER
            ];

            // SEND IMPORTER COMPANY HEADS START
            if (!is_null($currentRegimeData) && isset($referenceLegalEntities[$currentRegimeData['type']]) && $referenceLegalEntities[$currentRegimeData['type']]['code'] == ReferenceTable::ENTITY_TYPE_TAX_ID) {

                $localHeads = UserRoles::where(['role_id' => Role::HEAD_OF_COMPANY, 'company_tax_id' => $currentRegimeData['type_value']])->activeRole()->pluck('user_id')->all();
                $localHeadsUsers = User::whereIn('id', $localHeads)->active()->get();

                foreach ($localHeadsUsers as $localHeadsUser) {
                    $sendArray[] = [
                        'user_fullname' => $localHeadsUser->first_name . ' ' . $localHeadsUser->last_name,
                        'email' => $localHeadsUser->email,
                        'phone_number' => $localHeadsUser->phone_number,
                        'user_id' => $localHeadsUser->id,
                        'company_tax_id' => $data['companyTaxId'],
                        'lng_id' => $saf->user->lng_id,
                        'saf_id' => $saf->id,
                        'saf_url' => $safUrlForImporterExporterApplicant,
                        'saf_url_in_app' => $safUrlForImporterExporterApplicantInApp,
                        'saf_date' => $saf->created_at,
                        'sub_app_id' => $applicationId,
                        'sub_app_url' => $safSubAppUrlForImporterExporterApplicant,
                        'sub_app_url_in_app' => $safSubAppUrlForImporterExporterApplicantInApp,
                        'doc_name' => is_null($docName) ? '' : $docName->document_name,
                        'current_state' => $mapping->end_state,
                        'obligation' => 0.00,
                        'expert_fullname' => $expertFullName,
                        'notes' => isset($userDocumentsRequest['saf']['notes'], $userDocumentsRequest['saf']['notes']['notes']) ? $userDocumentsRequest['saf']['notes']['notes'] : '',
                        'template_id' => $mapping->importer_exporter,
                        'importer_fullname' => $importer['name'],
                        'exporter_fullname' => $exporter['name'],
                        'applicant_fullname' => $applicant['name'],
                        'sender_fullname' => is_null($senderUser) ? '' : $senderUser,
                        'superscribe_expert_fullname' => is_null($superscribeExpertUser) ? '' : $superscribeExpertUser,
                        'agency_name' => $companyTaxId,
                        'notification_type' => Notifications::NOTIFICATION_TYPE_IMPORTER_EXPORTER
                    ];
                }
            }

            //SEND EXPORTER COMPANY HEADS END
            $data = SingleApplication::getSidesUser($exporter);
            $currentRegimeData = $exporter;
            $sendArray[] = [
                'user_fullname' => $exporter['name'],
                'email' => $exporter['email'],
                'phone_number' => $exporter['phone_number'],
                'user_id' => $data['userId'],
                'company_tax_id' => $data['companyTaxId'],
                'lng_id' => $saf->user->lng_id,
                'saf_id' => $saf->id,
                'saf_url' => $safUrlForImporterExporterApplicant,
                'saf_url_in_app' => $safUrlForImporterExporterApplicantInApp,
                'saf_date' => $saf->created_at,
                'sub_app_id' => $applicationId,
                'sub_app_url' => $safSubAppUrlForImporterExporterApplicant,
                'sub_app_url_in_app' => $safSubAppUrlForImporterExporterApplicantInApp,
                'doc_name' => is_null($docName) ? '' : $docName->document_name,
                'current_state' => $mapping->end_state,
                'obligation' => 0.00,
                'expert_fullname' => $expertFullName,
                'notes' => isset($userDocumentsRequest['saf']['notes'], $userDocumentsRequest['saf']['notes']['notes']) ? $userDocumentsRequest['saf']['notes']['notes'] : '',
                'template_id' => $mapping->importer_exporter,
                'importer_fullname' => $importer['name'],
                'exporter_fullname' => $exporter['name'],
                'applicant_fullname' => $applicant['name'],
                'sender_fullname' => is_null($senderUser) ? '' : $senderUser,
                'superscribe_expert_fullname' => is_null($superscribeExpertUser) ? '' : $superscribeExpertUser,
                'agency_name' => $companyTaxId,
                'notification_type' => Notifications::NOTIFICATION_TYPE_IMPORTER_EXPORTER
            ];

            // SEND EXPORTER COMPANY HEADS START
            if (!is_null($currentRegimeData) && isset($referenceLegalEntities[$currentRegimeData['type']]) && $referenceLegalEntities[$currentRegimeData['type']]['code'] == ReferenceTable::ENTITY_TYPE_TAX_ID) {

                $localHeads = UserRoles::where(['role_id' => Role::HEAD_OF_COMPANY, 'company_tax_id' => $currentRegimeData['type_value']])->activeRole()->pluck('user_id')->all();
                $localHeadsUsers = User::whereIn('id', $localHeads)->active()->get();

                foreach ($localHeadsUsers as $localHeadsUser) {
                    $sendArray[] = [
                        'user_fullname' => $localHeadsUser->first_name . ' ' . $localHeadsUser->last_name,
                        'email' => $localHeadsUser->email,
                        'phone_number' => $localHeadsUser->phone_number,
                        'user_id' => $localHeadsUser->id,
                        'company_tax_id' => $data['companyTaxId'],
                        'lng_id' => $saf->user->lng_id,
                        'saf_id' => $saf->id,
                        'saf_url' => $safUrlForImporterExporterApplicant,
                        'saf_url_in_app' => $safUrlForImporterExporterApplicantInApp,
                        'saf_date' => $saf->created_at,
                        'sub_app_id' => $applicationId,
                        'sub_app_url' => $safSubAppUrlForImporterExporterApplicant,
                        'sub_app_url_in_app' => $safSubAppUrlForImporterExporterApplicantInApp,
                        'doc_name' => is_null($docName) ? '' : $docName->document_name,
                        'current_state' => $mapping->end_state,
                        'obligation' => 0.00,
                        'expert_fullname' => $expertFullName,
                        'notes' => isset($userDocumentsRequest['saf']['notes'], $userDocumentsRequest['saf']['notes']['notes']) ? $userDocumentsRequest['saf']['notes']['notes'] : '',
                        'template_id' => $mapping->importer_exporter,
                        'importer_fullname' => $importer['name'],
                        'exporter_fullname' => $exporter['name'],
                        'applicant_fullname' => $applicant['name'],
                        'sender_fullname' => is_null($senderUser) ? '' : $senderUser,
                        'superscribe_expert_fullname' => is_null($superscribeExpertUser) ? '' : $superscribeExpertUser,
                        'agency_name' => $companyTaxId,
                        'notification_type' => Notifications::NOTIFICATION_TYPE_IMPORTER_EXPORTER
                    ];
                }
            }
        }

        // AGENCY ADMIN
        if (in_array(Notifications::NOTIFICATION_TYPE_AGENCY_ADMIN, $recipientsList)) {

            $subApplication = SingleApplicationSubApplications::where('id', $applicationId)->first();
            $agency = Agency::where('id', $subApplication->agency_id)->first();

            if (!is_null($agency)) {
                $adminIds = UserRoles::where(['company_tax_id' => $agency->tax_id, 'role_id' => Role::HEAD_OF_COMPANY])
                    ->activeRole()
                    ->pluck('user_id')
                    ->unique()
                    ->all();
                $users = User::whereIn('id', $adminIds)->active()->get();
                foreach ($users as $user) {
                    $sendArray[] = [
                        'user_fullname' => $user->name($user->id),
                        'user_id' => $user->id,
                        'company_tax_id' => $agency->tax_id,
                        'email' => $user->email,
                        'phone_number' => $user->phone_number,
                        'lng_id' => $user->lng_id,
                        'saf_id' => $saf->id,
                        'saf_url' => $safUrlForNextStateUsersAgencies,
                        'saf_url_in_app' => $safUrlForNextStateUsersAgenciesInApp,
                        'saf_date' => $saf->created_at,
                        'sub_app_id' => $applicationId,
                        'sub_app_url' => $safSubAppUrlForNextStateUsersAgencies,
                        'sub_app_url_in_app' => $safSubAppUrlForNextStateUsersAgenciesInApp,
                        'doc_name' => is_null($docName) ? '' : $docName->document_name,
                        'current_state' => $mapping->end_state,
                        'obligation' => 0.00,
                        'expert_fullname' => $expertFullName,
                        'notes' => isset($userDocumentsRequest['saf']['notes'], $userDocumentsRequest['saf']['notes']['notes']) ? $userDocumentsRequest['saf']['notes']['notes'] : '',
                        'template_id' => $mapping->agency_admin,
                        'importer_fullname' => $importer['name'],
                        'exporter_fullname' => $exporter['name'],
                        'applicant_fullname' => $applicant['name'],
                        'sender_fullname' => is_null($senderUser) ? '' : $senderUser,
                        'superscribe_expert_fullname' => is_null($superscribeExpertUser) ? '' : $superscribeExpertUser,
                        'agency_name' => $companyTaxId,
                        'notification_type' => Notifications::NOTIFICATION_TYPE_AGENCY_ADMIN
                    ];
                }
            }
        }

        // IN NEXT STATE
        if (in_array(Notifications::NOTIFICATION_TYPE_PERSON_IN_NEXT_STATE, $recipientsList)) {

            $hasSuperscribeAction = ConstructorMapping::where('document_id', $documentId)
                ->join('constructor_actions', 'constructor_mapping.action', '=', 'constructor_actions.id')
                ->active()
                ->where(['start_state' => $mapping->end_state, 'special_type' => ConstructorActions::SUPERSCRIBE_TYPE])
                ->first();

            $usersNextStep = collect();
            if (!is_null($hasSuperscribeAction)) {

                $superscribe = ConstructorActionSuperscribes::where(['saf_subapplication_id' => $applicationId,])->orderByDesc()->active()->first();

                if ($superscribe) {
                    $supApplicationUserId = $superscribe->to_user;
                    $usersNextStep = User::where('id', $supApplicationUserId)->active()->get();
                }
                $notificationType = Notifications::NOTIFICATION_TYPE_PERSON_IN_NEXT_STATE_SUPERSCRIBE;
            } else {
                $usersNextStep = ConstructorStates::getUsersByMapping($mapping->end_state, $documentId)->where('show_status',BaseModel::STATUS_ACTIVE);
                $notificationType = Notifications::NOTIFICATION_TYPE_PERSON_IN_NEXT_STATE;
            }

            if ($usersNextStep->count()) {
                $constructorDocuments = ConstructorDocument::where('id', $documentId)->first();

                foreach ($usersNextStep as $user) {
                    $sendArray[] = [
                        'user_fullname' => $user->name($user->id),
                        'user_id' => $user->id,
                        'company_tax_id' => $constructorDocuments->company_tax_id,
                        'email' => $user->email,
                        'phone_number' => $user->phone_number,
                        'lng_id' => $user->lng_id,
                        'saf_id' => $saf->id,
                        'saf_url' => $safUrlForNextStateUsersAgencies,
                        'saf_url_in_app' => $safUrlForNextStateUsersAgenciesInApp,
                        'saf_date' => $saf->created_at,
                        'sub_app_id' => $applicationId,
                        'sub_app_url' => $safSubAppUrlForNextStateUsersAgencies,
                        'sub_app_url_in_app' => $safSubAppUrlForNextStateUsersAgenciesInApp,
                        'doc_name' => is_null($docName) ? '' : $docName->document_name,
                        'current_state' => $mapping->end_state,
                        'obligation' => 0.00,
                        'expert_fullname' => $expertFullName,
                        'notes' => isset($userDocumentsRequest['saf']['notes'], $userDocumentsRequest['saf']['notes']['notes']) ? $userDocumentsRequest['saf']['notes']['notes'] : '',
                        'template_id' => $mapping->persons_in_next_state,
                        'importer_fullname' => $importer['name'],
                        'exporter_fullname' => $exporter['name'],
                        'applicant_fullname' => $applicant['name'],
                        'sender_fullname' => is_null($senderUser) ? '' : $senderUser,
                        'superscribe_expert_fullname' => is_null($superscribeExpertUser) ? '' : $superscribeExpertUser,
                        'agency_name' => $companyTaxId,
                        'notification_type' => $notificationType
                    ];
                }
            }
        }

        //--------------------- Obligation variables part start ---------------------//

        $obligationIds = SingleApplicationObligations::where('saf_obligations.subapplication_id', $applicationId)
            ->where('saf_obligations.obligation_code', '!=', SingleApplicationObligations::SAF_OBLIGATIONS_FIX_CODE_FOR_SEND_APPLICATION_OBLIGATION)
            ->pluck('id');

        $obligation['obligationSum'] = number_format(SingleApplicationObligations::getObligationsSum('saf_obligations.obligation', null, $obligationIds), '2', '.', '');
        $obligation['paymentSum'] = number_format(SingleApplicationObligations::getObligationsSum('saf_obligations.payment', null, $obligationIds), '2', '.', '');
        $obligation['balanceSum'] = number_format($obligation['obligationSum'] - $obligation['paymentSum'], '2', '.', '');

        //--------------------- End obligation variables part ---------------------//

        $sendEmails = [
            'email' => [],
            'sms' => [],
        ];

        foreach ($sendArray as $sendArrayParams) {

            if (is_null($sendArrayParams['template_id'])) {
                continue;
            }

            $constructorNotificationsAll = NotificationTemplates::where(['id' => $sendArrayParams['template_id']])
                ->leftJoin('notification_templates_ml', 'notification_templates.id', '=', 'notification_templates_ml.notification_template_id')
                ->get();

            $constructorNotifications = $constructorNotificationsAll->where('lng_id', $sendArrayParams['lng_id'])->first();

            $inAppTransKeys = UserDocumentsNotificationsManager::getVariablesTransKeys($sendArrayParams, $supApplication, $obligation, Notifications::IN_APP_TRANS_KEYS);
            $emailTransKeys = UserDocumentsNotificationsManager::getVariablesTransKeys($sendArrayParams, $supApplication, $obligation, Notifications::EMAIL_TRANS_KEYS);
            $smsTransKeys = UserDocumentsNotificationsManager::getVariablesTransKeys($sendArrayParams, $supApplication, $obligation, Notifications::SMS_TRANS_KEYS);

            $item['notification_id'] = $sendArrayParams['template_id'];
            $item['user_id'] = $sendArrayParams['user_id'];
            $item['company_tax_id'] = $sendArrayParams['company_tax_id'] ?? 0;
            $item['sub_application_id'] = $sendArrayParams['sub_app_id'];

            $item['email_subject'] = strip_tags(Notifications::replaceVariableToValue($smsTransKeys, $constructorNotifications->email_subject, $sendArrayParams['lng_id']));
            $item['email_notification'] = Notifications::replaceVariableToValue($emailTransKeys, $constructorNotifications->email_notification, $sendArrayParams['lng_id']);
            $item['sms_notification'] = strip_tags(Notifications::replaceVariableToValue($smsTransKeys, $constructorNotifications->sms_notification, $sendArrayParams['lng_id']));

            $item['read'] = Notifications::NOTIFICATION_STATUS_UNREAD;
            $item['show_status'] = Notifications::STATUS_ACTIVE;
            $item['notification_type'] = $sendArrayParams['notification_type'];

            $inApplicationNotes = [];
            foreach ($constructorNotificationsAll as $constructorNote) {
                $inApplicationNotes[$constructorNote->lng_id] = [
                    'in_app_notification' => Notifications::replaceVariableToValue($inAppTransKeys, $constructorNote->in_app_notification, $constructorNote->lng_id)
                ];
            }

            if( $item['user_id'] != 0) {

                // Store in app notifications
                NotificationsManager::storeInAppNotifications($item, $inApplicationNotes);
            }

            //Store emails into db
            $toSendUser = User::find($sendArrayParams['user_id']);

            $toEmail = null;
            $toSms = null;
            if (!is_null($toSendUser) && $toSendUser->email_notification) {
                $toEmail = $toSendUser->email;
            }
            if (!is_null($toSendUser) && $toSendUser->sms_notification) {
                $toSms = true;
            }

            if (is_null($toSendUser)) {
                $companyEmail = Agency::where('tax_id', $sendArrayParams['company_tax_id'])->first();

                if (!is_null($companyEmail) && !empty($companyEmail->email)) {
                    $toEmail = $companyEmail->email;
                } elseif (is_null($companyEmail)) {
                    $toEmail = $sendArrayParams['email'];
                }
            }

            if ($toEmail) {
                if (empty($sendEmails['email'][$sendArrayParams['notification_type']]) || !in_array($toEmail, $sendEmails['email'][$sendArrayParams['notification_type']])) {

                    $dataForMail = [
                        'subject' => $item['email_subject'],
                        'to' => $toEmail,
                        'body' => $item['email_notification'],
                    ];

                    Mailer::makeEmail($dataForMail);

                    $sendEmails['email'][$sendArrayParams['notification_type']][] = $toEmail;
                }
            }

            //Store SMS into db
            if (config('swis.sms_send') && $sendArrayParams['user_id'] && !is_null($toSendUser) && $toSms) {

                if ($toSendUser->sms_notification) {
                    if (empty($sendEmails['sms'][$sendArrayParams['notification_type']]) || !in_array($sendArrayParams['phone_number'], $sendEmails['sms'][$sendArrayParams['notification_type']])) {
                        $sms = new SmsManager();

                        $data = [
                            'user_id' => $sendArrayParams['user_id'],
                            'phone' => trim($sendArrayParams['phone_number']),
                            'message' => $item['sms_notification'],
                        ];

                        $sms->storeSms($data);

                        $sendEmails['sms'][$sendArrayParams['notification_type']][] = $sendArrayParams['phone_number'];
                    }
                }
            }
        }
    }

    /**
     * Function to get recipient's list
     *
     * @param $mapping
     * @return array
     */
    private function recipientsList($mapping)
    {
        $list = [];
        $recipientsList = ConstructorMapping::getFixRoles();
        foreach ($recipientsList as $item) {
            if (isset($mapping->{$item})) {
                $list[] = $item;
            }
        }

        return $list;
    }

    // ========================== My Application Send Notifications End=========================== //


    // ========================== My Application Refactoring =========================== //

    /**
     * Function to return user document edit page fields with value
     *
     * @param $generateDataParams
     * @return array
     */
    public static function getPageData($generateDataParams)
    {
        // @fixme "separate product part form all inputs"
        $saf = $generateDataParams['saf'];
        $application = $generateDataParams['singleApplicationSubApp'];
        $laboratoryStatesCount = $generateDataParams['laboratoryStatesCount'];
        $availableProductsIds = $generateDataParams['availableProductsIds'];
        $availableProductsIdNumbers = $generateDataParams['availableProductsIdNumbers'];
        $userDocumentFields = $generateDataParams['fields'];
        $globalHiddenFields = $generateDataParams['globalHiddenFields'];
        $lists = $generateDataParams['lists'];
        $mandatoryListForView = $generateDataParams['mandatoryListForView'];
        $hiddenTabs = $generateDataParams['hiddenTabs'];
        $showUserMappingList = $generateDataParams['showUserMappingList'];
        $documentId = $generateDataParams['documentId'];
        $tabLists = $generateDataParams['tabLists'];
        $superscribe = $generateDataParams['superscribe'];
        $stateHasSuperscribeMapping = $generateDataParams['stateHasSuperscribeMapping'];
        $getReferenceWithAgencyColumn = ReferenceTable::getReferenceWithAgencyColumn();
        $userReferenceAgencyIds = ReferenceTable::getUserReferenceAgencyIds();
        $data = [];
        $simpleAppTabs = SingleApplicationDataStructure::lastXml();
        $fieldLabels = self::getInstance()->getSafMLFields($documentId, $simpleAppTabs);
        $safControlFields = self::getInstance()->getSafControlFields($application, $saf, $availableProductsIds);

        foreach ($simpleAppTabs as $simpleAppTab) {
            $key = (string)$simpleAppTab->attributes()->key;

            if ((isset($hiddenTabs) && in_array($key, $hiddenTabs)) || ($key == 'subtitles')) {
                continue;
            }

            if (array_key_exists($key, $tabLists) && in_array("non_visible", $tabLists[$key]) && !in_array("editable", $tabLists[$key])) {
                continue;
            }

            if ($key == 'examination' && Auth::user()->companyTaxId() != Agency::SAS_AGENCY_TAX_ID) {
                continue;
            }

            if (($key != 'laboratories') || ($key == 'laboratories' && $laboratoryStatesCount > 0)) {

                $instructionAndFeedback = SingleApplicationSubApplicationInstructions::getInstructions($application);

                if ($key == 'instructions' && !count($instructionAndFeedback['instructions'])) {
                    continue;
                }
                $data['tabs'][] = [
                    'href' => "#tab-{$key}",
                    'name' => trans('' . $simpleAppTab->attributes()->name),
                    'class' => (!empty($simpleAppTab->attributes()->show) && $simpleAppTab->attributes()->show == 'false') ? ($saf->status_type == SingleApplication::SEND_TYPE) ? '' : 'hide' : '',
                ];
            }
        }

        foreach ($simpleAppTabs as $simpleAppTab) {
            $key = (string)$simpleAppTab->attributes()->key;
            $tabIsEditable = false;
            if ((isset($hiddenTabs) && in_array($key, $hiddenTabs)) || ($key == 'subtitles')) {
                continue;
            }

            if (array_key_exists($key, $tabLists) && in_array("non_visible", $tabLists[$key]) && !in_array("editable", $tabLists[$key])) {
                continue;
            } elseif (array_key_exists($key, $tabLists) && in_array("editable", $tabLists[$key])) {
                if (is_null($stateHasSuperscribeMapping)) {
                    $tabIsEditable = true;
                } elseif (!is_null($superscribe) && $superscribe->to_user == Auth::id()) {
                    $tabIsEditable = true;
                }
            }

            /* ----- whiten for typeBlocks in sides tab ----- */
            if ((string)$key == 'sides') {
                foreach ($simpleAppTab->typeBlock as $typeBlock) {
                    if ((string)$typeBlock->attributes()->regime == $saf->regime) {
                        $simpleAppTab = $typeBlock;
                        $simpleAppTab->addAttribute('key', 'sides');
                    }
                }
            }

            /* ----- whiten for typeBlocks in sides tab content ----- */
            $data['tabContent'][$key]['tabId'] = "tab-{$key}";
            $data['tabContent'][$key]['editable'] = $tabIsEditable;

            foreach ($simpleAppTab->block as $block) {
                if ($block->attributes()->type != 'subForm' && (empty($block->attributes()->draw) || $block->attributes()->draw != 'false')) {

                    if ($block->attributes()->type != 'dataTable') {
                        $safParamsData = [$application, $saf, $simpleAppTab, $userDocumentFields, $safControlFields];
                        $listControlledData = [$globalHiddenFields, $lists, $mandatoryListForView];
                        $productParamsData = [[], false, []];
                        $additionalParams = [false, $getReferenceWithAgencyColumn, $fieldLabels, $userReferenceAgencyIds, $tabIsEditable];

                        $data['tabContent'][$key]['block'][] = [
                            'blockId' => (string)$block->attributes()->idName,
                            'blockName' => trans((string)$block->attributes()->name),
                            'blockType' => (string)$block->attributes()->type,
                            'module' => (string)$block->attributes()->module,
                            'multiple' => (string)$block->attributes()->multiple,
                            'fields' => self::getAllApplicationData($block, $key, $safParamsData, $listControlledData, $productParamsData, $showUserMappingList, [], $additionalParams)
                        ];
                    } else {
                        $blockData = [
                            'blockId' => (string)$block->attributes()->idName,
                            'blockName' => trans((string)$block->attributes()->name),
                            'blockType' => (string)$block->attributes()->type,
                            'module' => (string)$block->attributes()->module,
                            'multiple' => (string)$block->attributes()->multiple,
                            'subForm' => (!empty($block->attributes()->subForm)) ? 'subForm' : '',
                            'renderTr' => (!empty($block->attributes()->renderTr)) ? 'renderTr' : '',
                            'tableClass' => (string)$block->attributes()->tableClass
                        ];

                        switch ((string)$block->attributes()->module) {
                            case 'responsibilities':
                                $blockData['data'] = SingleApplicationObligations::getObligations($block, $saf, $application);
                                break;
                            case 'products':
                                $safParamsData = [$saf, $application, $userDocumentFields, $availableProductsIds, $availableProductsIdNumbers, $safControlFields];
                                $listControlledData = [$globalHiddenFields, $lists, $mandatoryListForView, $showUserMappingList, $tabIsEditable];
                                $blockData['data'] = SingleApplicationProducts::getSafProducts($block, $safParamsData, $listControlledData, $getReferenceWithAgencyColumn, $fieldLabels, $simpleAppTabs, $userReferenceAgencyIds);
                                $data['applicationProductsKeyValue'] = $blockData['data']['applicationProductsKeyValue'];
                                break;
                            case 'examination':
                                $blockData['data'] = [];
                                break;
                            case 'instructions':
                                $blockData['data'] = SingleApplicationSubApplicationInstructions::getInstructions($application, [], $tabIsEditable);
                                break;
                        }

                        $data['tabContent'][$key]['block'][] = $blockData;
                    }
                }
            }

        }

        return $data;
    }

    /**
     * Function to get all application data
     *
     * @param $block
     * @param $tabKey
     * @param $safParamsData
     * @param $listControlledData
     * @param $productParamsData
     * @param bool $showUserMappingList
     * @param array $availableProductsIdNumbers
     * @param $additionalParams
     * @return array
     */
    public static function getAllApplicationData($block, $tabKey, $safParamsData, $listControlledData, $productParamsData, $showUserMappingList, $availableProductsIdNumbers, $additionalParams)
    {
        list($application, $saf, $simpleAppTab, $userDocumentFields, $safControlFields) = $safParamsData;
        list($globalHiddenFields, $lists, $mandatoryListForView) = $listControlledData;
        list($productData, $product, $productsColumns) = $productParamsData;
        list($hasQuantity2, $getReferenceWithAgencyColumn, $fieldLabels, $userReferenceAgencyIds, $tabIsEditable) = $additionalParams;

        $fieldSetWithFields = [];
        $applicationData = $application->data;
        $applicationCustomData = $application->custom_data;
        $dataModelAllData = DataModel::allData();
        $refData = null;

        // Mdm fields
        if (!empty($userDocumentFields)) {
            foreach ($userDocumentFields as $userDocumentField) {
                if ($userDocumentField['xpath'] == "tab[@key='products']/block[@name='swis.single_app.products_list']/subBlock[@name='swis.single_app.batches']") {
                    continue;
                }

                $listControlled = listControlled($userDocumentField['field_id'], $globalHiddenFields, $lists, 'mdm', $mandatoryListForView);
                if ($listControlled['continue']) {
                    continue;
                }
                $requiredField = $listControlled['requiredField'];
                if ($tabIsEditable) {
                    $disabled = '';
                } else {
                    $disabled = $listControlled['disabled'];
                }

                $prName = ($product) ? "_pr_{$productData->id}" : '';

                if (!empty($tabKey)) {
                    $xmlTabKey = $tabKey;
                } elseif (!empty($simpleAppTab)) {
                    $xmlTabKey = $simpleAppTab->attributes()->key;
                }

                if ((!empty($xmlTabKey) && $xmlTabKey == $userDocumentField['tab']) || $xmlTabKey == 'typeBlock') {

                    if (!empty($userDocumentField['block']) && $block->attributes()->name != $userDocumentField['block']) {
                        continue;
                    }

                    switch ($userDocumentField['type']) {
                        case 'varchar':
                            $inputType = 'text';
                            break;
                        case 'integer':
                            $inputType = 'number';
                            break;
                        case 'date':
                            $inputType = 'date';
                            break;
                        case 'datetime':
                            $inputType = 'datetime-local';
                            break;
                        case 'autocomplete':
                            $inputType = 'autocomplete';
                            break;
                        default:
                            $inputType = 'text';
                    }

                    $name = str_replace(']', '', $userDocumentField['field_id']);
                    $name = str_replace('[', '-', $name);
                    $errorName = "form-error-customData-" . str_replace('.', '-', $name) . $prName;
                    $fieldAttributes = $appendMultipleName = '';
                    if ($userDocumentField['mdm_type'] == DataModel::MDM_ELEMENT_TYPE_MULTIPLE) {
                        $fieldAttributes .= 'multiple';
                        $appendMultipleName = '[]';
                    }

                    if ($inputType == 'autocomplete') {

                        // ($disabled === 'readonly') === (Field != editable)
                        $getActives = ($disabled === 'readonly') ? false : true;

                        $options = [];
                        $hasAgencyColumnGetName = (array_key_exists($userDocumentField['reference'], $getReferenceWithAgencyColumn)) ? $getReferenceWithAgencyColumn[$userDocumentField['reference']] : false;

                        $where = function ($query) use ($hasAgencyColumnGetName, $userDocumentField, $userReferenceAgencyIds, $prName, $applicationCustomData) {
                            $refTableName = "reference_{$userDocumentField['reference']}";
                            if ($hasAgencyColumnGetName) {
                                $query->whereIn("{$refTableName}.{$hasAgencyColumnGetName}", $userReferenceAgencyIds);
                            }
                            $query->where("{$refTableName}.show_status", BaseModel::STATUS_ACTIVE);
                            if (isset($applicationCustomData) && array_key_exists($userDocumentField['field_id'] . $prName, $applicationCustomData)) {
                                $savedData = $applicationCustomData[$userDocumentField['field_id'] . $prName];
                                if (is_array($savedData)) {
                                    $validSavedData = [];
                                    foreach ($savedData as $saveData) {
                                        if ((int)$saveData) {
                                            $validSavedData[] = (int)$saveData;
                                        }
                                    }

                                    $query->orWhereIn(DB::raw('id::varchar'), $validSavedData);
                                } else {
                                    $query->orWhere(DB::raw('id::varchar'), $savedData);
                                }
                            }
                        };

                        $refAllData = getReferenceRows($userDocumentField['reference'], false, false, ['id', 'code', 'name'], 'get', 'id', 'desc', $where, !$getActives);

                        if ($refAllData->count() < 30) {
                            foreach ($refAllData as $refData) {
                                if (isset($applicationCustomData) && array_key_exists($userDocumentField['field_id'] . $prName, $applicationCustomData)) {

                                    if (is_array($applicationCustomData[$userDocumentField['field_id'] . $prName])) {
                                        if ($userDocumentField['mdm_type'] == DataModel::MDM_ELEMENT_TYPE_MULTIPLE) {
                                            if (is_array($applicationCustomData[$userDocumentField['field_id'] . $prName]) && count($applicationCustomData[$userDocumentField['field_id'] . $prName]) > 0) {
                                                $options[] = [
                                                    'selected' => in_array($refData->id, $applicationCustomData[$userDocumentField['field_id'] . $prName]) ? 'selected' : '',
                                                    'name' => "({$refData->code}) {$refData->name}",
                                                    'value' => $refData->id
                                                ];
                                            }
                                        } else {
                                            $options[] = [
                                                'selected' => $applicationCustomData[$userDocumentField['field_id'] . $prName]['value'] == $refData->id ? 'selected' : '',
                                                'name' => "({$refData->code}) {$refData->name}",
                                                'value' => $refData->id,
                                            ];
                                        }
                                    } else {
                                        $options[] = [
                                            'selected' => $applicationCustomData[$userDocumentField['field_id'] . $prName] == $refData->id ? 'selected' : '',
                                            'name' => "({$refData->code}) {$refData->name}",
                                            'value' => $refData->id,
                                        ];
                                    }
                                } else {
                                    $options[] = [
                                        'selected' => '',
                                        'name' => "({$refData->code}) {$refData->name}",
                                        'value' => $refData->id,
                                    ];
                                }
                            }

                            $fieldClass = ($refAllData->count() > 10) ? 'select2' : '';
                            $fieldDataUrl = $fieldDataRef = '';
                        } else {

                            if (isset($applicationCustomData) && isset($userDocumentField['field_id']) && array_key_exists($userDocumentField['field_id'] . $prName, $applicationCustomData) && isset($applicationCustomData[$userDocumentField['field_id']])) {

                                if ($userDocumentField['mdm_type'] == DataModel::MDM_ELEMENT_TYPE_MULTIPLE) {
//                                    $mdmReference = optional(DataModel::select('id', 'reference')->where(DB::raw('id::varchar'), $userDocumentField['mdm_field_id'])->first())->reference;

                                    if (isset($dataModelAllData[$userDocumentField['mdm_field_id']])) {

                                        $mdmReference = $dataModelAllData[$userDocumentField['mdm_field_id']]['reference'];

                                        if (is_array($applicationCustomData[$userDocumentField['field_id'] . $prName]) && count($applicationCustomData[$userDocumentField['field_id'] . $prName]) > 0) {
                                            /*if ("reference_{$mdmReference}" == ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS) {
                                                $data = getReferenceRowsWhereIn($mdmReference, [], $applicationCustomData[$userDocumentField['field_id'] . $prName], ['id', 'name', 'code'], $getActives)->pluck('name', 'id');
                                            } else {
                                            }*/

                                            $data = getReferenceRowsWhereIn($mdmReference, $applicationCustomData[$userDocumentField['field_id'] . $prName], [], ['id', 'name', 'code'], $getActives)->pluck('name', 'id');

                                            foreach ($data as $id => $name) {
                                                $options[] = [
                                                    'selected' => 'selected',
                                                    'name' => $name,
                                                    'value' => $id
                                                ];
                                            }
                                        }
                                    }

                                } else {
                                    if (isset($applicationCustomData[$userDocumentField['field_id'] . $prName]['text'])) {
                                        $options[] = [
                                            'selected' => 'selected',
                                            'name' => $applicationCustomData[$userDocumentField['field_id'] . $prName]['text'],
                                            'value' => $applicationCustomData[$userDocumentField['field_id'] . $prName]['value']
                                        ];
                                    } else {

                                        if (isset($dataModelAllData[$userDocumentField['mdm_field_id']])) {

//                                            $mdmReference = optional(DataModel::select('id', 'reference')->where(DB::raw('id::varchar'), $userDocumentField['mdm_field_id'])->first())->reference;
                                            $mdmReference = $dataModelAllData[$userDocumentField['mdm_field_id']]['reference'];

                                            $data = getReferenceRows($mdmReference, $applicationCustomData[$userDocumentField['field_id'] . $prName], [], ['id', 'name', 'code'], 'get', false, false, [], $getActives);
                                            $options[] = [
                                                'selected' => 'selected',
                                                'name' => "({$data->code}) {$data->name}",
                                                'value' => $data->id
                                            ];
                                        }
                                    }

                                }
                            }
                            $fieldClass = 'select2-ajax';
                            $fieldDataUrl = urlWithLng('/reference-table/ref');
                            $fieldDataRef = $userDocumentField['reference_id'];
                        }
                        $disabled = (isset($showUserMappingList) && !$showUserMappingList) ? 'disabled' : $disabled;

                        $fieldSetWithFields[] = [
                            'fieldSetClass' => $requiredField,
                            'fieldSetTitle' => isset($userDocumentField['tooltip']) ? $userDocumentField['tooltip'] : '',
                            'fieldSetTitlePlacement' => 'top',
                            'fieldSetMultipleClass' => '',
                            'fieldSetMultiple' => '',
                            'fieldSetOrigin' => 'mdm',
                            'fieldSetData' => '',
                            'fieldSetLabel' => isset($userDocumentField['label']) ? $userDocumentField['label'] : '',
                            'fieldClass' => $fieldClass,
                            'fieldTitle' => isset($userDocumentField['tooltip']) ? $userDocumentField['tooltip'] : '',
                            'fieldId' => "select2-{$userDocumentField['reference_id']}",
                            'fieldName' => "customData[{$userDocumentField['field_id']}{$prName}]{$appendMultipleName}",
                            'fieldType' => $inputType,
                            'fieldIsContent' => $userDocumentField['is_content'],
                            'fieldAttributes' => $fieldAttributes,
                            'fieldDataUrl' => $fieldDataUrl,
                            'fieldDataRef' => $fieldDataRef,
                            'fieldErrorName' => $errorName,
                            'options' => $options,
                            'fieldDisabled' => $disabled
                        ];
                    } else {
                        $disabled = (isset($showUserMappingList) && !$showUserMappingList) ? 'disabled' : $disabled;

                        $fieldValue = '';
                        if (isset($applicationCustomData[$userDocumentField['field_id'] . $prName])) {

                            $fieldValue = $applicationCustomData[$userDocumentField['field_id'] . $prName];

                            if (isDateFormat($fieldValue)) {
                                $fieldValue = formattedDate($fieldValue);
                            } else {
                                if ($userDocumentField['mdm_type'] == 'dc') {
                                    $fieldValue = str_replace(",", ".", $fieldValue);
                                }
                            }
                        }

                        $fieldSetWithFields[] = [
                            'fieldSetClass' => $requiredField,
                            'fieldSetTitle' => isset($userDocumentField['tooltip']) ? $userDocumentField['tooltip'] : '',
                            'fieldSetTitlePlacement' => 'top',
                            'fieldSetMultipleClass' => '',
                            'fieldSetMultiple' => '',
                            'fieldSetOrigin' => 'mdm',
                            'fieldSetData' => '',
                            'isFieldMdmLine' => $userDocumentField['mdm_line_type'],
                            'fieldSetLabel' => isset($userDocumentField['label']) ? $userDocumentField['label'] : '',
                            'fieldTitle' => isset($userDocumentField['tooltip']) ? $userDocumentField['tooltip'] : '',
                            'fieldName' => "customData[{$userDocumentField['field_id']}{$prName}]{$appendMultipleName}",
                            'fieldType' => $inputType,
                            'fieldIsContent' => $userDocumentField['is_content'],
                            'fieldAttributes' => $fieldAttributes,
                            'fieldValue' => $fieldValue,
                            'fieldDisabled' => $disabled,
                            'fieldErrorName' => $errorName,
                        ];
                    }

                }
            }
        }

        // Saf fields
        $expressControlFieldName = 'saf.general.sub_application_express_control_id';
        $subdivisionFieldName = 'saf.general.sub_application_agency_subdivision_id';
        foreach ($block->fieldset as $fieldSet) {

            $fieldSetFieldName = (string)$fieldSet->field->attributes()->name;

            if ((string)$fieldSet->field->attributes()->safControlledField) {
                if ($product) {
                    if (strpos((string)$fieldSet->field->attributes()->name, ']') !== false) {
                        $fieldSetFieldName = str_replace(']', '', str_replace(['][', '['], '.', (string)$fieldSet->field->attributes()->name));
                    } else {
                        $fieldSetFieldName = 'saf_controlled_fields.' . (string)$fieldSet->field->attributes()->name;
                    }
                    if (isset($safControlFields['routingProductValues'][$productData->id]) && !array_key_exists($fieldSetFieldName, $safControlFields['routingProductValues'][$productData->id])) {
                        continue;
                    }
                } else {
                    if (isset($safControlFields['routingValues'][(string)$fieldSet->field->attributes()->name]) && !array_key_exists((string)$fieldSet->field->attributes()->name, $safControlFields['routingValues'])) {
                        continue;
                    }
                }
            }

            if ($fieldSetFieldName == 'saf[sides][import][passport]' || $fieldSetFieldName == "saf[sides][export][passport]" || $fieldSetFieldName == "saf[sides][applicant][passport]") {
                if (
                    ((isset($applicationData['saf']['sides']['import']) && ReferenceTable::isLegalEntityType($applicationData['saf']['sides']['import']['type'], BaseModel::ENTITY_TYPE_TAX_ID))) ||
                    ((isset($applicationData['saf']['sides']['export']) && ReferenceTable::isLegalEntityType($applicationData['saf']['sides']['export']['type'], BaseModel::ENTITY_TYPE_TAX_ID)))
                ) {
                    continue;
                }
            }

            $listControlled = listControlled((string)$fieldSet->field->attributes()->id, $globalHiddenFields, $lists, 'saf', $mandatoryListForView);

            if ($listControlled['continue']) {
                continue;
            }

            $requiredField = $listControlled['requiredField'];

            if ($tabIsEditable && (bool)$fieldSet->field->attributes()->showInView) {
                $disabled = '';
            } else {
                $disabled = $listControlled['disabled'];
            }

            $fieldData = $className = $select2ClassName = '';
            $fields = [];

            if ($fieldSet->attributes()->multiple) {
                if ((string)$fieldSet->field->attributes()->name == "saf[transportation][transit_country][]") {
                    $fieldData = json_encode($applicationData['saf']['transportation']['transit_country']);
                    $className = "saf_transit_country";
                } elseif ((string)$fieldSet->field->attributes()->name == "saf[transportation][state_number_customs_support][1][state_number]") {
                    if (isset($applicationData['saf']['transportation']['state_number_customs_support'])) {
                        $fieldData = json_encode($applicationData['saf']['transportation']['state_number_customs_support']);
                        $className = "state_number_customs_support";
                    }
                }
            }

            foreach ($fieldSet->field as $field) {

                if ((string)$field->attributes()->id == SingleApplication::SAF_AGENCY_SUBDIVISION_ID) {
                    $select2ClassName = 'select2';
                }
                if (count($fieldSet->field) > 1) {
                    $listControlled = listControlled((string)$field->attributes()->id, $globalHiddenFields, $lists, 'saf', $mandatoryListForView);
                    if ($listControlled['continue']) {
                        continue;
                    }
                    $requiredField = $listControlled['requiredField'];

                    if ($tabIsEditable && (bool)$field->attributes()->showInView) {
                        $disabled = '';
                    } else {
                        $disabled = $listControlled['disabled'];
                    }
                }

                $options = $dataAttribute = [];
                $isContentField = false;

                if ($field->attributes()->mdm && !empty($field->attributes()->mdm)) {
//                    $mdm = DataModel::select('id', 'reference', 'is_content')->where(DB::raw('id::varchar'), $field->attributes()->mdm)->first();
                    if (isset($dataModelAllData[(string)$field->attributes()->mdm])) {
                        $mdm = (object)$dataModelAllData[(string)$field->attributes()->mdm];

                        $isContentField = optional($mdm)->is_content;
                        if (!empty($mdm->reference)) {
                            $hasAgencyColumnGetName = (array_key_exists($mdm->reference, $getReferenceWithAgencyColumn)) ? $getReferenceWithAgencyColumn[$mdm->reference] : false;

                            if ($product && (string)$field->attributes()->name == 'product_code') {
                                $where = function ($query) use ($hasAgencyColumnGetName, $mdm, $productData, $userReferenceAgencyIds) {
                                    $query->where('id', $productData->product_code);
                                    if ($hasAgencyColumnGetName) {
                                        $query->whereIn("reference_{$mdm->reference}.{$hasAgencyColumnGetName}", $userReferenceAgencyIds);
                                    }
                                };
                            } elseif ($field->attributes()->id == SingleApplication::SAF_AGENCY_SUBDIVISION_ID) {
                                $activeRefAgencyRow = getReferenceRows(ReferenceTable::REFERENCE_AGENCY_TABLE_NAME, false, Auth::user()->companyTaxId());
                                if (!is_null($activeRefAgencyRow)) {

                                    $where = function ($query) use ($activeRefAgencyRow, $hasAgencyColumnGetName, $mdm, $userReferenceAgencyIds, $application) {
                                        $query->where('agency', $activeRefAgencyRow->id);

                                        if ($hasAgencyColumnGetName) {

                                            $subDivisions = $subDivisions = RegionalOffice::whereHas('constructorDocuments', function ($q) use ($application) {
                                                $q->where('constructor_document_code', $application->document_code);
                                            })->active()->pluck('office_code')->toArray();

                                            $query->whereIn("reference_{$mdm->reference}.{$hasAgencyColumnGetName}", $userReferenceAgencyIds)->whereIn("reference_{$mdm->reference}.code", $subDivisions);
                                        }
                                    };
                                }
                            } else {
                                $where = function ($query) use ($hasAgencyColumnGetName, $mdm, $userReferenceAgencyIds) {
                                    if ($hasAgencyColumnGetName) {
                                        $query->whereIn("reference_{$mdm->reference}.{$hasAgencyColumnGetName}", $userReferenceAgencyIds);
                                    }
                                };
                            }
                        }
                    }
                }
                $name = (string)$field->attributes()->name;
                if (strpos($name, ']') !== false) {
                    $name = str_replace(['][', '['], '.', $name);
                    $name = str_replace(']', '', $name);
                }

                $applicationDataByPrefix = prefixKey('', $applicationData);
                if ($product) {
                    if ($field->attributes()->safControlledField) {
                        $explodeField = explode('.', $name);
                        $explodeField = end($explodeField);

                        if (!isset($productData->saf_controlled_fields[$explodeField]) && !array_key_exists($explodeField, $productData->toArray())) {
                            continue 2;
                        }

                        $storeValue = '';
                        if (!empty($explodeField) && isset($productData->saf_controlled_fields) && isset($productData->saf_controlled_fields[$explodeField])) {
                            $storeValue = $productData->saf_controlled_fields[$explodeField];
                        }

                    } else {
                        $storeValue = isset($productData->{$name}) ? $productData->{$name} : '';
                    }
                } else {
                    $storeValue = !empty($applicationDataByPrefix[$name]) ? $applicationDataByPrefix[$name] : '';
                }

                $fieldDataUrl = $fieldDataRef = $fieldClass = '';

                switch ($field->attributes()->type) {
                    case 'select':

                        $isFieldDisabled = (isset($showUserMappingList) && !$showUserMappingList) ? 'disabled' : $disabled;

                        $dataAttribute['dataType'] = (string)$field->attributes()->dataType;

                        $selectRefArr = ['id', 'code', 'name'];

                        // Express control
                        if ($name == $expressControlFieldName) {
                            $storeValue = $application->express_control_id ?? '';
                        }

                        // Subdivision
                        if ($name == $subdivisionFieldName) {
                            $storeValue = $application->agency_subdivision_id ?? '';
                            $selectRefArr[] = 'short_name';
                        }

                        // Ref By ID
                        $refData = getReferenceRows($mdm->reference, $storeValue, false, $selectRefArr, 'get', 'id', 'desc', $where ?? []);

                        //
                        if ((isset($showUserMappingList) && $showUserMappingList) && ($name == $expressControlFieldName || $name == $subdivisionFieldName) && !$isFieldDisabled) {

                            // Express control
                            if ($name == $expressControlFieldName) {

                                $referenceClassificatorId = $application->reference_classificator_id;
                                $queryExpressWhere = function ($query) use ($referenceClassificatorId) {
                                    return $query->where("document", $referenceClassificatorId ?? 0)->orWhereNull('document');
                                };

                                $refData = getReferenceRows($mdm->reference, false, false, ['id', 'code', 'name'], 'get', false, false, $queryExpressWhere);
                            }

                            // Subdivision
                            if ($name == $subdivisionFieldName) {
                                $agency = Agency::where('id', $application->agency_id)->first();

                                $refData = collect();
                                if (!is_null($agency)) {
                                    $subDivisions = RegionalOffice::whereHas('constructorDocuments', function ($q) use ($application) {
                                        $q->where('constructor_document_code', $application->document_code);
                                    })->active()->pluck('office_code')->toArray();

                                    if (count($subDivisions)) {
                                        $refAgencySubDivisions = getReferenceRowsWhereIn(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS, [], $subDivisions, ['id', 'name', 'code', 'short_name'], true, 'sort');

                                        $refData = $refAgencySubDivisions;
                                    }
                                }
                            }

                            //
                            if (!empty($refData)) {

                                $lastItemActiveExpressControl = $lastItemActiveSubDivision = null;

                                if ($name == $subdivisionFieldName) {
                                    $lastItemActiveSubDivision = getReferenceItemLastActiveRow($mdm->reference, $application->agency_subdivision_id);
                                }

                                if ($name == $expressControlFieldName) {
                                    $lastItemActiveExpressControl = getReferenceItemLastActiveRow($mdm->reference, $application->express_control_id);
                                }

                                foreach ($refData as $ref) {
                                    $selected = '';

                                    if (!is_null($lastItemActiveSubDivision) && $ref->id == $lastItemActiveSubDivision->id) {
                                        $selected = 'selected';
                                    }

                                    if (!is_null($lastItemActiveExpressControl) && $ref->id == $lastItemActiveExpressControl->id) {
                                        $selected = 'selected';
                                    }

                                    $refShortName = $ref->name;
                                    if (isset($ref->short_name) && !is_null($ref->short_name)) {
                                        $refShortName = $ref->short_name;
                                    }

                                    $options[] = [
                                        'selected' => $selected,
                                        'name' => !empty($field->attributes()->ajax) ? "({$ref->code}) {$refShortName}" : $refShortName,
                                        'value' => $ref->id,
                                    ];
                                }
                            }

                        } else {
                            if (!empty($refData)) {

                                $refShortName = $refData->name ?? '';
                                if (isset($refData->short_name) && !is_null($refData->short_name)) {
                                    $refShortName = $refData->short_name;
                                }

                                $options[] = [
                                    'selected' => 'selected',
                                    'name' => !empty($field->attributes()->ajax) ? "({$refData->code}) {$refShortName}" : $refShortName,
                                    'value' => $refData->id,
                                ];
                            }
                        }


                        break;
                    case 'input':
                        $defaultValue = '';
                        /*if ($field->attributes()->defaultValue) {
                            $defaultValueParts = explode('::', $field->attributes()->defaultValue);

                            switch ($defaultValueParts[0]) {
                                case 'global':
                                    $defaultValue = $defaultValueParts[1];
                                    if ($defaultValueParts[1] == 'regularNumber') {
                                        $defaultValue = $application->saf_number;
                                    } elseif ($defaultValueParts[1] == 'safDate') {
                                        $defaultValue = Carbon::createFromFormat('d/m/Y H:i:s', $saf->created_at)->format(config('swis.date_format'));
                                    } elseif ($defaultValueParts[1] == 'type') {
                                        $defaultValue = $saf->regime;
                                    }
                                    break;
                                case 'user':
                                    $defaultValue = @Auth::user()->getCreator()->{$defaultValueParts[1]} ?? '';
                                    break;
                            }
                        }*/

                        $dataAttribute = [
                            'dataName' => ($field->attributes()->dataName) ? (string)$field->attributes()->dataName : '',
                            'dataType' => ($field->attributes()->dataType) ? (string)$field->attributes()->dataType : ''
                        ];

                        if ($field->attributes()->isPhoneNumber) {
                            $storeValue = isset($storeValue) ? $storeValue : $defaultValue;
                        } else {
                            if (empty($applicationData)) {
                                $storeValue = (!empty($productData->{$name})) ? $productData->{$name} : $defaultValue;
                                $storeValue = isDateFormat($storeValue) ? formattedDate($storeValue) : $storeValue;
                            } else {
                                if ($name == 'saf.general.sub_application_number') {
                                    $storeValue = $application->document_number or '';
                                } elseif ($name == 'saf.general.subapplication_submitting_date') {
                                    $storeValue = (empty($applicationData['saf.general.subapplication_submitting_date.date'])) ? Carbon::parse($application->status_date)->format(config('swis.date_format')) : Carbon::parse($applicationData['saf.general.subapplication_submitting_date.date'])->format(config('swis.date_format'));
                                } elseif ($name == 'saf.general.sub_application_access_token') {
                                    $storeValue = $application->access_token or '';
                                }
                                $storeValue = isDateFormat($storeValue) ? formattedDate($storeValue) : $storeValue;
                            }
                        }
                        if (!empty($field->attributes()->mdm)) {
//                            $dataModel = DataModel::where('id', (string)$field->attributes()->mdm)->first();

                            if (isset($dataModelAllData[(string)$field->attributes()->mdm])) {
                                $storeValue = str_replace(",", ".", $storeValue);
                            }
                        }

                        if ($field->attributes()->safControlledField) {
                            if ($product) {
                                if (isset($safControlFields['routingProductValues'][$productData->id]) && array_key_exists($name, $safControlFields['routingProductValues'][$productData->id])) {
                                    $storeValue = $safControlFields['routingProductValues'][$productData->id][$name];
                                }
                            } else {
                                if (array_key_exists((string)$field->attributes()->name, $safControlFields['routingValues'])) {
                                    $storeValue = $safControlFields['routingValues'][(string)$field->attributes()->name];
                                }
                            }
                        }
                        break;
                    case 'textarea':
                        $storeValue = !empty($productData->{$name}) ? $productData->{$name} : $storeValue;

                        if ($field->attributes()->safControlledField) {
                            if ($product) {
                                if (isset($safControlFields['routingProductValues'][$productData->id]) && array_key_exists($name, $safControlFields['routingProductValues'][$productData->id])) {
                                    $storeValue = $safControlFields['routingProductValues'][$productData->id][$name];
                                }
                            } else {
                                if (array_key_exists((string)$field->attributes()->name, $safControlFields['routingValues'])) {
                                    $storeValue = $safControlFields['routingValues'][(string)$field->attributes()->name];
                                }
                            }
                        }
                        break;
                    case 'autocomplete':

                        $refSelectOptionColumns = (!empty($field->attributes()->refSelectOptionColumns)) ? explode(',', $field->attributes()->refSelectOptionColumns) : null;

                        if ($product) {
                            if (!empty($field->attributes()->ref)) {
                                $refName = "reference_" . env($field->attributes()->ref);
                                $refNameMl = 'reference_' . env($field->attributes()->ref) . '_ml';
                            }

                            if (!empty($field->attributes()->mdm)) {

//                                $dataModel = DataModel::where('id', $field->attributes()->mdm)->first();
                                if (isset($dataModelAllData[(string)$field->attributes()->mdm])) {

                                    $dataModel = $dataModelAllData[(string)$field->attributes()->mdm];

                                    if ($dataModel['reference']) {
                                        $refName = "reference_{$dataModel['reference']}";
                                        $refNameMl = "reference_{$dataModel['reference']}_ml";
                                    }
                                }
                            }

                            if (!empty($refName)) {
                                if ($field->attributes()->safControlledField) {
                                    $explodeField = explode('.', $name);
                                    $explodeField = end($explodeField);

                                    $refValue = !empty($explodeField) ? $productData->saf_controlled_fields[$explodeField] : '';
                                } else {
                                    $refValue = $productData->{$name};
                                }

                                /*$rawRef = DB::table($refName)->select('id', 'code', 'name')
                                    ->join($refNameMl, "{$refName}.id", '=', "{$refNameMl}.{$refName}_id")->where("{$refNameMl}.lng_id", cLng('id'))
                                    ->where(DB::raw('id::varchar'), $refValue)
                                    ->first();*/

                                $rawRef = getReferenceRows($refName, $refValue);

                                if (!is_null($rawRef)) {
                                    $rawOptionVal = "({$rawRef->code}) {$rawRef->name}";

                                    if (!empty($refSelectOptionColumns) && !empty($refSelectOptionColumns[0]) && !empty($refSelectOptionColumns[1])) {
                                        if (!empty($rawRef->{$refSelectOptionColumns[0]}) && !empty($rawRef->{$refSelectOptionColumns[1]})) {
                                            $rawOptionVal = "(" . $rawRef->{$refSelectOptionColumns[0]} . ") " . $rawRef->{$refSelectOptionColumns[1]};
                                        }
                                    }

                                    $options[] = [
                                        'selected' => 'selected',
                                        'name' => (!empty($field->attributes()->ajax)) ? "{$rawRef->code} {$rawOptionVal}" : $rawOptionVal,
                                        'value' => $rawRef->id,
                                    ];

                                }
                            }
                        }

                        $fieldClass = !empty($field->attributes()->ajax) ? 'select2-ajax select2-ajax-saf' : 'select2';
                        if (($saf->regime == 'import' && (string)$field->attributes()->name == 'saf[transportation][import_country]') || ($saf->regime == 'export' && (string)$field->attributes()->name == 'saf[transportation][export_country]')) {
                            $disabled = "disabled";
                        }

                        $storeValue = ((string)$fieldSet->field->attributes()->name == "saf[transportation][transit_country][]") ? false : $storeValue;

                        if (isset($mdm) && optional($mdm)->reference) {
                            $refData = getReferenceRows($mdm->reference, $storeValue, false, '*');
                        }

                        if ((string)$fieldSet->field->attributes()->name == "saf[transportation][transit_country][]") {
                            foreach ($refData as $item) {
                                if (($product && !empty($rawRef) && $rawRef->code == $item->code) || (is_null($item))) {
                                    continue;
                                }
                                if (($saf->regime == 'import' && (string)$field->attributes()->name == 'saf[transportation][export_country]' && env('COUNTRY_MODE') == $item->code) ||
                                    ($saf->regime == 'export' && (string)$field->attributes()->name == 'saf[transportation][import_country]' && env('COUNTRY_MODE') == $item->code)) {
                                    continue;
                                }

                                $selected = '';
                                if (($saf->regime == 'import' && env('COUNTRY_MODE') == $item->code && (string)$field->attributes()->name == 'saf[transportation][import_country]') ||
                                    ($saf->regime == 'export' && env('COUNTRY_MODE') == $item->code && (string)$field->attributes()->name == 'saf[transportation][export_country]')) {
                                    $selected = 'selected';
                                }
                                $selected = ($product) && ($productData->{$name} == $item->id) ? 'selected' : ($storeValue == $item->id) ? 'selected' : $selected;

                                $refOptionVal = "({$item->code}) {$item->name}";
                                if (!empty($refSelectOptionColumns) && !empty($refSelectOptionColumns[0]) && !empty($refSelectOptionColumns[1])) {
                                    if (!empty($item->{$refSelectOptionColumns[0]}) && !empty($item->{$refSelectOptionColumns[1]})) {
                                        $refOptionVal = "(" . $item->{$refSelectOptionColumns[0]} . ") " . $item->{$refSelectOptionColumns[1]};
                                    }
                                }
                                $options[] = [
                                    'selected' => $selected,
                                    'name' => $refOptionVal,
                                    'value' => $item->id,
                                ];
                            }
                        } else {
                            $fieldReferenceSelectColumns = (string)$fieldSet->field->attributes()->refSelectOptionColumns;

                            if (!empty($fieldReferenceSelectColumns)) {
                                $refSelectOptionColumns = explode(',', $fieldReferenceSelectColumns);

                                if (!empty($refSelectOptionColumns[2])) {
                                    $refSelectedCustomName = $refSelectOptionColumns[2];
                                }
                            }

                            if ((is_null($refData)) || ($product && !empty($rawRef) && $rawRef->code == $refData->code)) {
                                continue;
                            }
                            if (($saf->regime == 'import' && (string)$field->attributes()->name == 'saf[transportation][export_country]' && env('COUNTRY_MODE') == $refData->code) ||
                                ($saf->regime == 'export' && (string)$field->attributes()->name == 'saf[transportation][import_country]' && env('COUNTRY_MODE') == $refData->code)) {
                                continue;
                            }

                            $selected = '';
                            if (($saf->regime == 'import' && env('COUNTRY_MODE') == $refData->code && (string)$field->attributes()->name == 'saf[transportation][import_country]') ||
                                ($saf->regime == 'export' && env('COUNTRY_MODE') == $refData->code && (string)$field->attributes()->name == 'saf[transportation][export_country]')) {
                                $selected = 'selected';
                            }
                            $selected = ($product) && ($productData->{$name} == $refData->id) ? 'selected' : ($storeValue == $refData->id) ? 'selected' : $selected;

                            $refOptionVal = "({$refData->code}) {$refData->name}";
                            if (!empty($refSelectOptionColumns) && !empty($refSelectOptionColumns[0]) && !empty($refSelectOptionColumns[1])) {
                                if (!empty($refData->{$refSelectOptionColumns[0]}) && !empty($refData->{$refSelectOptionColumns[1]})) {
                                    $refOptionVal = "(" . $refData->{$refSelectOptionColumns[0]} . ") " . $refData->{$refSelectOptionColumns[1]};
                                }
                            }
                            if (!empty($refSelectedCustomName) && !empty($refData->{$refSelectedCustomName})) {
                                $refOptionVal .= ', ' . $refData->{$refSelectedCustomName};
                            }
                            $options[] = [
                                'selected' => $selected,
                                'name' => $refOptionVal,
                                'value' => $refData->id,
                            ];
                        }

                        break;
                    case 'hidden':
                        $storeValue = (string)$field->attributes()->defaultValue;
                        break;
                    case 'autoincrement':
                        $storeValue = (!empty($availableProductsIdNumbers) && isset($availableProductsIdNumbers[$productData->id])) ? $availableProductsIdNumbers[$productData->id] : @$productData->{$name};
                        break;
                }

                $storeValue = isset($storeValue) ? $storeValue : $defaultValue ?? '';
                $storeValue = ($product && !empty($productData->{$name})) ? $productData->{$name} : $storeValue;
                if ($name == 'product_number') {
                    $storeValue = $productsColumns[0]['name'];
                }

                $name = str_replace(']', '', $name);
                $name = str_replace('[', '-', $name);
                $errorName = str_replace('.', '-', $name);

                if ($fieldSet->attributes()->multiple || count($fieldSet->field) > 1) {
                    $fieldReadOnly = (isset($showUserMappingList) && !$showUserMappingList) ? 'disabled' : $disabled;

                    $fields[] = [
                        'fieldSetMultiple' => (string)$fieldSet->attributes()->multiple,
                        'fieldClass' => $field->attributes()->defaultClass . ' ' . $fieldClass . ' ' . $select2ClassName,
                        'fieldBlockClass' => (count($fieldSet->field) > 1) ? 'col-lg-5 col-md-4' : 'col-md-10',
                        'fieldTitle' => '',
                        'fieldIsContent' => $isContentField,
                        'fieldId' => (string)$field->attributes()->defaultID or '',
                        'fieldName' => ($product) ? "" : (string)$field->attributes()->name,
                        'fieldType' => (string)$field->attributes()->type,
                        'fieldReadOnly' => $fieldReadOnly,
                        'fieldDataUrl' => $fieldDataUrl,
                        'fieldDataRef' => $fieldDataRef ?? '',
                        'fieldDisabled' => $fieldReadOnly,
                        'refType' => (string)$field->attributes()->refType,
                        'fieldValue' => $storeValue,
                        'fieldErrorName' => "form-error-{$errorName}",
                        'fieldInputType' => ($field->attributes()->inputType) ? (string)$field->attributes()->inputType : 'text',
                        'fieldNeedMdm' => (empty($fieldSet->field->attributes()->mdm) && $field->attributes()->type != 'hidden') ? "<b style='color: red'>Need add MDM field id {$fieldSet->field->attributes()->inputType}}</b>" : '',
                        'fieldIsPhoneNumber' => $field->attributes()->isPhoneNumber,
                        'dataAttributes' => array_merge(['dataName' => '', 'dataType' => ''], $dataAttribute),
                        'options' => $options
                    ];
                }

            }

            if (!$fieldSet->attributes()->multiple && count($fieldSet->field) <= 1) {
                $fieldReadOnly = (isset($showUserMappingList) && !$showUserMappingList) ? 'disabled' : $disabled;

                $fields[] = [
                    'fieldClass' => $field->attributes()->defaultClass . ' ' . $fieldClass . ' ' . $select2ClassName,
                    'fieldBlockClass' => (count($fieldSet->field) > 1) ? 'col-lg-5 col-md-4' : 'col-lg-10 col-md-8',
                    'fieldTitle' => '',
                    'fieldIsContent' => $isContentField,
                    'fieldId' => (string)$field->attributes()->defaultID or '',
                    'fieldName' => ($product) ? "" : (string)$field->attributes()->name,
                    'fieldType' => (string)$field->attributes()->type,
                    'fieldReadOnly' => $fieldReadOnly,
                    'fieldDataUrl' => $fieldDataUrl ?? '',
                    'fieldDataRef' => $fieldDataRef ?? '',
                    'fieldDisabled' => $fieldReadOnly,
                    'refType' => (string)$field->attributes()->refType,
                    'fieldValue' => $storeValue ?? '',
                    'fieldErrorName' => "form-error-{$errorName}",
                    'fieldInputType' => ($field->attributes()->inputType) ? (string)$field->attributes()->inputType : 'text',
                    'fieldNeedMdm' => (empty($fieldSet->field->attributes()->mdm) && $field->attributes()->type != 'hidden') ? "<b style='color: red'>Need add MDM field id {$fieldSet->field->attributes()->inputType}}</b>" : '',
                    'fieldIsPhoneNumber' => $field->attributes()->isPhoneNumber,
                    'dataAttributes' => array_merge(['dataName' => '', 'dataType' => ''], $dataAttribute ?? []),
                    'options' => $options
                ];
            }

            $fieldSetWithFields[] = [
                'fieldSetClass' => ($fieldSet->field->attributes()->required) ? 'required' : $requiredField,
                'fieldSetTitle' => (isset($fieldSet->field->attributes()->id, $fieldLabels[(string)$fieldSet->field->attributes()->id]) && !empty($fieldLabels[(string)$fieldSet->field->attributes()->id]['tooltip'])) ? $fieldLabels[(string)$fieldSet->field->attributes()->id]['tooltip'] : $fieldSet->field->attributes()->tooltipText,
                'fieldSetTitlePlacement' => (empty($fieldSet->field->attributes()->tooltipPlacement)) ? 'top' : $fieldSet->field->attributes()->tooltipPlacement,
                'fieldSetLabel' => isset($fieldSet->field->attributes()->id, $fieldLabels[(string)$fieldSet->field->attributes()->id]) && !empty($fieldLabels[(string)$fieldSet->field->attributes()->id]['label']) ? $fieldLabels[(string)$fieldSet->field->attributes()->id]['label'] : trans((string)$fieldSet->field->attributes()->title),
                'fieldSetMultipleClass' => ($fieldSet->attributes()->multiple) ? 'multipleFields ' . $className : '',
                'fieldSetMultiple' => (string)$fieldSet->attributes()->multiple,
                'fieldInFieldSet' => $fields,
                'fieldSetData' => !empty($fieldData) ? $fieldData : '',
                'fieldSetOrigin' => 'saf',
            ];
        }

        return $fieldSetWithFields;
    }

    // ========================== My Application Refactoring End =========================== //


    /**
     * Function to get saf control fields
     *
     * @param $application
     * @param $saf
     * @param $availableProductsIds
     * @return array
     */
    private function getSafControlFields($application, $saf, $availableProductsIds)
    {
        $routingValues = $routingProductValues = [];
        $safControlledFieldsXml = getSafControlFieldIdNameArray();
        $defaultVisibleFieldsInProducts = $saf->getSafControlledDefaultVisibleFields(true);
        $safControlledFields = !is_null($saf->saf_controlled_fields) ? prefixKey('', $saf->saf_controlled_fields) : [];
        $safProducts = SingleApplicationProducts::select('id', 'saf_controlled_fields')->where('saf_number', $saf->regular_number)->whereIn('id', $availableProductsIds)->get()->keyBy('id')->toArray();
        $safProductsAllData = SingleApplicationProducts::where('saf_number', $saf->regular_number)->whereIn('id', $availableProductsIds)->get()->keyBy('id')->toArray();

        foreach ($safProducts as $productId => $safProduct) {
            foreach ($defaultVisibleFieldsInProducts as $defaultVisibleFieldsInProduct) {
                if (isset($safControlledFieldsXml[$defaultVisibleFieldsInProduct]) && $defaultField = $safControlledFieldsXml[$defaultVisibleFieldsInProduct]) {
                    if (!array_key_exists($defaultField, $safProduct['saf_controlled_fields'])) {
                        $safProducts[$productId]['saf_controlled_fields'][$defaultField] = isset($safProductsAllData[$productId][$defaultField]) ? $safProductsAllData[$productId][$defaultField] : '';
                    }
                }
            }
        }

        $routings = is_array($application->routings) ? $application->routings : [];
        $routingProducts = is_array($application->routing_products) ? $application->routing_products : [];

        foreach ($routings as $item) {
            $routing = Routing::select('saf_controlled_fields')->where('id', $item)->first();
            if (!is_null($routing->saf_controlled_fields)) {
                foreach ($routing->saf_controlled_fields as $fieldId => $safControlledField) {
                    if (array_key_exists('make_visible', $safControlledField) && array_key_exists($fieldId, $safControlledFieldsXml)) {
                        $trimFieldName = str_replace("][", ".", $safControlledFieldsXml[$fieldId]);
                        $trimFieldName = str_replace("[", ".", $trimFieldName);
                        $trimFieldName = str_replace("]", "", $trimFieldName);

                        if (array_key_exists($trimFieldName, $safControlledFields)) {
                            $routingValues[$safControlledFieldsXml[$fieldId]] = $safControlledFields[$trimFieldName];
                        }
                    }
                }
            }
        }

        foreach ($routingProducts as $items) {
            foreach ($items as $productId) {
                unset($safProducts[$productId]['id']);
                if (array_key_exists($productId, $safProducts)) {
                    $routingProductValues[$productId] = prefixKey('', $safProducts[$productId]);
                }
            }
        }

        return ['routingValues' => $routingValues, 'routingProductValues' => $routingProductValues];
    }

    /**
     * Function to get saf ml fields
     *
     * @param $documentId
     * @param $simpleAppTabs
     * @return array
     */
    private function getSafMLFields($documentId, $simpleAppTabs)
    {
        $fieldMls = [];
        $fieldValues = DocumentsDatasetFromSaf::select('id', 'saf_field_id')->with('current')->where('document_id', $documentId)->get();

        foreach (getSafIdNameArray() as $fieldId => $path) {
            $fieldMls[$fieldId] = [
                'label' => null,
                'tooltip' => null
            ];
        }

        foreach ($fieldValues as $item) {
            $fieldMls[$item->saf_field_id]['label'] = optional($item->current)->label;
            $fieldMls[$item->saf_field_id]['tooltip'] = optional($item->current)->tooltip;
        }
        return $fieldMls;
    }

    /**
     * @param $userDocumentsRequest
     * @param $safDataForValidation
     * @param $singleApplicationSubApp
     * @param $mapping
     * @return array
     */
    public static function runValidationRules($userDocumentsRequest, $safDataForValidation, $singleApplicationSubApp, $mapping)
    {
        $errors = [];
        $safPathById = getSafIdNameArray();

        $mappingRules = ConstructorMappingToRule::select('constructor_rules.*')->where('constructor_mapping_id', $userDocumentsRequest->input('mapping_id'))
            ->join('constructor_rules', 'constructor_rules.id', '=', 'constructor_mapping_to_rule.rule_id')
            ->where('constructor_rules.type', ConstructorRules::VALIDATION_TYPE)
            ->get();
        $hasError = false;

        $availableProductsIds = $singleApplicationSubApp->productList->pluck('id')->all();
        $safProducts = SingleApplicationProducts::select('id')->whereIn('id', $availableProductsIds)->get();
        $productBatches = SingleApplicationProductsBatch::select('id')->whereIn('saf_product_id', array_keys($singleApplicationSubApp->productsOrderedList()))->get();
        $customDataProductAndBatchFields = self::getCustomDataProductAndBatchFields($userDocumentsRequest->input('id'));

        foreach ($mappingRules as $rule) {
            $validationRules = new ValidationRules($safDataForValidation, $rule->condition, $rule->rule, 'obligation', $singleApplicationSubApp);

            if ($validationRules->checkCondition()) {
                $result = $validationRules->matchRules();

                if ($result['function'] == "showError") {
                    if (!empty($result['result']['fields'])) {
                        $field = $result['result']['fields'][0];

                        if (strpos($field, "SAF_ID_") !== false) {
                            $hasError = true;
                            $name = str_replace(']', '', str_replace(['][', '['], '.', $safPathById[$field]));
                            if (strpos($name, "batch._index_.") !== false) {
                                $name = str_replace("batch._index_.", "", $name);
                                foreach ($productBatches as $batch) {
                                    $errors['rule_show_error']["product_batches-{$batch->id}-{$name}"] = $errors['custom_errors']["product_batches-{$batch->id}-{$name}"] = $result['result']['message'];
                                }
                            } else {
                                $errors['rule_show_error'][$name] = $errors['custom_errors'][$name] = $result['result']['message'];
                            }
                        } elseif (strpos($field, "FIELD_ID_") !== false) {
                            $hasError = true;
                            $name = str_replace("FIELD_ID_", "", $field);
                            if (in_array($name, $customDataProductAndBatchFields['products'])) {
                                foreach ($safProducts as $product) {
                                    $errors['rule_show_error']["customData-{$name}_pr_{$product->id}"] = $errors['custom_errors']["customData-{$name}_pr_{$product->id}"] = $result['result']['message'];
                                }
                            } elseif (in_array($name, $customDataProductAndBatchFields['batches'])) {
                                foreach ($productBatches as $batch) {
                                    $errors['rule_show_error']["mdmData-{$batch->id}-{$name}"] = $errors['custom_errors']["mdmData-{$batch->id}-{$name}"] = $result['result']['message'];
                                }
                            } else {
                                $errors['rule_show_error']["customData-{$name}"] = $errors['custom_errors']["customData-{$name}"] = $result['result']['message'];
                            }
                        }

                    } else {
                        $hasError = true;
                        $errors['rule_show_error'][] = $errors['custom_errors'][] = $result['result']['message'];
                    }
                } elseif (($result['function'] == "showWarning" || $result['function'] == "showWarning2") && $userDocumentsRequest->input('is_confirmed') == 'false') {
                    $ruleShowWarning = ($result['function'] == "showWarning") ? 'rule_show_warning' : 'rule_show_warning2';
                    if (!empty($result['result']['fields'])) {
                        $field = $result['result']['fields'][0];
                        if (strpos($field, "SAF_ID_") !== false) {
                            $hasError = true;

                            $name = str_replace(']', '', str_replace(['][', '['], '.', $safPathById[$field]));
                            if (strpos($name, "batch._index_.") !== false) {
                                $name = str_replace("batch._index_.", "", $name);
                                foreach ($productBatches as $batch) {
                                    $errors[$ruleShowWarning]["product_batches-{$batch->id}-{$name}"] = $errors['custom_errors']["product_batches-{$batch->id}-{$name}"] = $result['result']['message'];
                                }
                            } else {
                                $errors[$ruleShowWarning][$name] = $errors['custom_errors'][$name] = $result['result']['message'];
                            }
                        } elseif (strpos($field, "FIELD_ID_") !== false) {
                            $hasError = true;
                            $name = str_replace("FIELD_ID_", "", $field);
                            if (in_array($name, $customDataProductAndBatchFields['products'])) {
                                $errors[$ruleShowWarning]["customData-{$name}_pr_{$safProducts[0]->id}"] = $result['result']['message'];
                                foreach ($safProducts as $product) {
                                    $errors['custom_errors']["customData-{$name}_pr_{$product->id}"] = $result['result']['message'];
                                }
                            } elseif (in_array($name, $customDataProductAndBatchFields['batches'])) {
                                foreach ($productBatches as $batch) {
                                    $errors[$ruleShowWarning]["mdmData-{$batch->id}-{$name}"] = $errors['custom_errors']["mdmData-{$batch->id}-{$name}"] = $result['result']['message'];
                                }
                            } else {
                                $errors[$ruleShowWarning]["customData-{$name}"] = $errors['custom_errors']["customData-{$name}"] = $result['result']['message'];
                            }
                        }
                    } else {
                        $hasError = true;
                        $errors[$ruleShowWarning][] = $errors['custom_errors'][] = $result['result']['message'];
                    }
                    $errors['confirmButton'] = "#mappingButton{$mapping->id}-{$userDocumentsRequest->input('action')}-{$mapping->end_state}";
                }

            }
        }
//        dd($errors);
        return [$hasError, $errors];
    }

    /**
     * Function to get saf control field for pdf
     *
     * @param $application
     * @param $saf
     * @param $availableProductsIds
     * @return array
     */
    public static function getSafControlFieldsForPdf($application, $saf, $availableProductsIds)
    {
        return self::getInstance()->getSafControlFields($application, $saf, $availableProductsIds);
    }

    /**
     * Function to get product and batch custom data
     *
     * @param $documentId
     * @return array
     */
    public static function getCustomDataProductAndBatchFields($documentId)
    {
        $data['products'] = DocumentsDatasetFromMdm::select('field_id')->where('document_id', $documentId)->where('xpath', "tab[@key='products']/block[@name='swis.single_app.products_list']")->pluck('field_id')->all();
        $data['batches'] = DocumentsDatasetFromMdm::select('field_id')->where('document_id', $documentId)->where('xpath', "tab[@key='products']/block[@name='swis.single_app.products_list']/subBlock[@name='swis.single_app.batches']")->pluck('field_id')->all();

        return $data;
    }

    /**
     * Function to return Document Mapping
     *
     * @param $documentId
     * @param $roles
     * @param $documentCurrentState
     * @param bool $subApplicationId
     * @return ConstructorMapping
     */
    public static function getDocumentMappings($documentId, $roles, $documentCurrentState, $subApplicationId = false)
    {
        if (!Auth::user()->isLocalAdmin() && $subApplicationId) {

            $usedRoles = [];
            foreach ($roles as $role) {
                $roleIsUsed = SingleApplicationSubApplications::select('saf_sub_applications.id')->myApplicationRoles([$role])->where('saf_sub_applications.id', $subApplicationId)->first();
                if (!is_null($roleIsUsed)) {
                    $usedRoles[] = $role;
                }
            }

            $roles = $usedRoles;
        }

        return ConstructorMapping::select('constructor_mapping.*', 'constructor_actions.special_type as action_type')
            ->join('constructor_actions', 'constructor_mapping.action', '=', 'constructor_actions.id')
            ->where('start_state', $documentCurrentState->state_id)
            ->where('document_id', $documentId)
            ->whereIn('role', $roles)
            ->orderBy('constructor_actions.order')
            ->active()
            ->with(['rAction' => function ($q) {
                $q->with(['currentMl', 'actionTemplates.template.currentMl']);
            }, 'nonVisibleFieldsSaf', 'nonVisibleFieldsMdm', 'nonVisibleTabs', 'editableTabs', 'editableFieldsSaf', 'editableFieldsMdm', 'mandatoryFieldsSaf', 'mandatoryFieldsMdm'])
            ->get();
    }
}
