<?php

namespace App\Models\UserDocuments;

use App\Http\Controllers\Core\DataTableSearch;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\DataModel\DataModel;
use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm;
use App\Models\DocumentsDatasetFromSaf\DocumentsDatasetFromSaf;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use Illuminate\Support\Facades\DB;

/**
 * Class ConstructorMappingSearch
 * @package App\Models\ConstructorMapping
 */
class UserDocumentsSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->rowsCountAll();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->rowsCountAll();
    }

    /**
     * @return int
     */
    private function rowsCountAll()
    {
        $query = $this->constructQuery();
        $result = DB::select("SELECT count(*) as cnt FROM ({$query->toSql()}) as temp", $query->getBindings());

        return $result[0]->cnt;
    }

    /**
     * @return array
     */
    public function search()
    {
        //@fixme
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);

        $data = $query->get();
        if ($data->isEmpty()) {
            return [];
        }

        $dataSetSaf = DocumentsDatasetFromSaf::where('document_id', request()->route('id'))->get();
        $searchResultFieldsSaf = $dataSetSaf->filter(function ($value, $key) {
            return $value->show_in_search_results == 1;
        });

        $fieldNames = [];
        foreach ($searchResultFieldsSaf as $item) {
            $name = str_replace(['][', '['], '_', $item->xml_field_name);
            $fieldNames[] = [
                'field' => str_replace(']', '', $name),
                'fieldId' => $item->saf_field_id
            ];
        }

        $dataSetMdm = DocumentsDatasetFromMdm::select('id', 'field_id', 'mdm_field_id')
            ->where(['show_in_search_results' => true, 'document_id' => request()->route('id')])
            ->with('current')
            ->get();

        foreach ($dataSetMdm as $field) {
            $fieldNames[] = [
                'field' => $field->field_id
            ];
        }

        return $this->generateResult($data, $fieldNames);
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $this->orderByCol = $this->orderByCol == 'state' ? 'created_at' : $this->orderByCol;
        $orderByType = $this->orderByType ?? 'desc';

        if ($this->orderByCol != 'created_at') {
            $name = $this->orderByCol;

            $name = str_replace("][", "->", $name);
            $name = str_replace("[", "->", $name);
            $name = str_replace("]", "", $name);

            // check if field is saf or mdm
            if (strpos($name, 'saf->') !== false) {
                $query->orderBy("saf_sub_applications.data->{$name}", $orderByType);
            } else {
                $query->orderBy("saf_sub_applications.custom_data->{$name}", $orderByType);
            }

        }

        $query->orderBy('saf_sub_applications.created_at', 'desc');
    }

    /**
     * @param array $data
     * @return mixed
     */
    protected function constructQuery($data = [])
    {
        $documentId = request()->route('id');
        $searchData = !empty($this->searchData) ? $this->searchData : [];
        if (!empty($data)) {
            $searchData = $data;
        }

        $selectFields = $this->getSafAutoCompleteFieldsIdNameArray();
        $filterData = !empty($data) ? $data : $searchData;
        if (isset($filterData['state_id'])) {
            unset($filterData['state_id']);
        }

        $query = SingleApplicationSubApplications::select([
            'saf.id',
            'saf.company_tax_id',
            'saf.regime',
            'saf.regular_number',
            'saf.user_id',
            'saf.creator_user_id',
            'saf.status_type',
            'saf.show_status',
            'saf.created_at',
            'saf_sub_applications.express_control_id',
            'saf_sub_applications.access_token',
            'saf_sub_applications.status_date',
            'saf_sub_applications.is_edited',
            'saf_sub_applications.document_number',
            'saf_sub_applications.id as saf_sub_application_id',
            'constructor_action_superscribes.to_user as superscribeUserId',
            DB::raw("array_to_string(array_agg(constructor_mapping.action), ',') as action_ids"),
            DB::raw("COALESCE(saf_sub_applications.data, saf.data) as data"),
            DB::raw("saf_sub_applications.custom_data as custom_data")
        ])
            ->join('saf', function ($join) {
                $join->on('saf.regular_number', '=', 'saf_sub_applications.saf_number');
            })
            ->leftJoin('constructor_action_superscribes', function ($join) {
                $join->on('constructor_action_superscribes.saf_subapplication_id', '=', 'saf_sub_applications.id')->where('constructor_action_superscribes.is_current', '1');
            })
            ->groupBy('saf.id',
                'saf.company_tax_id',
                'saf.regime',
                'saf.regular_number',
                'saf.user_id',
                'saf.creator_user_id',
                'saf.status_type',
                'saf.show_status',
                'saf.created_at',
                'constructor_action_superscribes.to_user',
                'saf_sub_applications.id',
                'saf_sub_applications.document_number',
                'saf_sub_applications.express_control_id',
                'saf_sub_applications.access_token',
                'saf_sub_applications.status_date',
                'saf_sub_applications.is_edited'
            )
            ->with(['productListInSearch'])
            ->with(['currentState.state.currentMl'])
            ->with(['superscribeUser.user']);

        $fieldPlaces = [];
        $fields = DocumentsDatasetFromMdm::select('id', 'field_id', 'xpath')->where('document_id', $documentId)->get();
        foreach ($fields as $field) {
            $fieldPlace = 'data';

            if ($field->xpath == "tab[@key='products']/block[@name='swis.single_app.products_list']") {
                $fieldPlace = 'products';
            } elseif ($field->xpath == "tab[@key='products']/block[@name='swis.single_app.products_list']/subBlock[@name='swis.single_app.batches']") {
                $fieldPlace = 'batches';
            }

            $fieldPlaces[$field->field_id] = $fieldPlace;
        }

        foreach (getSafIdNameArray() as $field => $item) {
            $fieldPlaces[$field] = 'data';
        }

        foreach (getSafProductsIdNameArray() as $field => $item) {
            $fieldPlaces[$field] = 'products';
        }

        foreach (getSafProductsBatchesIdNameArray() as $field => $item) {
            $fieldPlaces[$field] = 'batches';
        }

        $query = UserDocuments::getJsonFilter($filterData, $query, $documentId, $selectFields, $fieldPlaces);

        // ----------------------

        if (isset($searchData['id'])) {
            $query->where('id', $searchData['id']);
        }

        if (isset($searchData['state_id'])) {
            $query->where('saf_sub_application_states.state_id', $searchData['state_id'])->where('saf_sub_application_states.is_current', '1');
        }

        if (isset($searchData['documentID'])) {
            $query->where('document_id', $searchData['documentID']);
        }

        if (isset($searchData['saf[general][sub_application_access_token]'])) {
            $query->where('saf_sub_applications.access_token', $searchData['saf[general][sub_application_access_token]']);
        }

        $query->myApplicationRoles();

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }

    /**
     * Function to call protected ConstructorQuery
     *
     * @param $data
     * @return mixed
     */
    public function callToConstructorQuery($data)
    {
        return $this->constructQuery($data);
    }

    /**
     * Function to generate and return result
     *
     * @param $data
     * @param $fieldNames
     * @param null $searchData
     * @return array
     */
    public function generateResult($data, $fieldNames, $searchData = null)
    {
        $result = [];
        $dataModelAllData = DataModel::allData();
        $documentsDatasetFromMdmAllData = DocumentsDatasetFromMdm::with(['mdm'])->select('id', 'field_id', 'mdm_field_id')->get()->keyBy('field_id')->toArray();
        $refExpressControlAllData = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_EXPRESS_CONTROL);
        $xmlFields = SingleApplicationDataStructure::getXmlIdMdm();

        foreach ($data as $key => $value) {

            $row = [];
            $safData['saf'] = isset($value->data['saf']) ? $value->data['saf'] : $value->data;

            $filledData = prefixKey('', $safData, '_');
            $filledCustomData = $value->custom_data;
            $expressControlId = (isset($value->express_control_id)) ? $value->express_control_id : '';
            if ($expressControlId) {
                if (isset($refExpressControlAllData[$expressControlId])) {
                    $row['expressControlCode'] = $refExpressControlAllData[$expressControlId]['code'];
                }
            }

            foreach ($fieldNames as $field) {
                $searchValue = '';
                $mdmType = '';

                if (isset($filledData[$field['field']])) {

                    $searchValue = $filledData[$field['field']];

//                    $xmlAttr = $xmlFields->xpath("//*[@id='{$field['fieldId']}']");
                    $mdmId = $xmlFields[$field['fieldId']] ?? null;

                    if ($mdmId) {
                        $mdmType = '';
                        if (isset($dataModelAllData[$mdmId])) {

                            $getReference = (object)$dataModelAllData[$mdmId];

                            $mdmType = $getReference->type ?? '';
                            if (!is_null($getReference->reference)) {
                                $newValue = getReferenceRows($getReference->reference, $searchValue, false);

                                if (!is_null($newValue)) {
                                    $searchValue = isDateFormat($newValue->name) ? formattedDate($newValue->name) : $newValue->name;
                                }
                            }
                        }
                    }

                    if ($field['field'] == 'saf_general_sub_application_express_control_id') {
                        if (is_null($searchData)) {
                            if (isset($row['expressControlCode']) && $row['expressControlCode'] == ReferenceTable::REFERENCE_EXPRESS_CONTROL_TYPE_ACCELERATED) {
                                $searchValue = '<p><i class="fas fa-circle" style="color: red;"></i> ' . $searchValue . '</p>';
                            } elseif (isset($row['expressControlCode']) && $row['expressControlCode'] == ReferenceTable::REFERENCE_EXPRESS_CONTROL_TYPE_PERISHABLES) {

                            } else {
                                $searchValue = '<p><i class="fas fa-circle" style="color: lightgrey;"></i> ' . $searchValue . '</p>';
                            }
                        }
                    }
                } elseif (isset($filledCustomData[$field['field']])) {
                    $searchValue = $filledCustomData[$field['field']];

                    if (isset($documentsDatasetFromMdmAllData[$field['field']]) && !is_null($documentsDatasetFromMdmAllData[$field['field']]['mdm'])) {

                        $fieldMdm = $documentsDatasetFromMdmAllData[$field['field']]['mdm'];
                        $mdmType = $fieldMdm['type'] ?? '';

                        if ($fieldMdm['reference']) {
                            if (is_array($searchValue)) {
                                if (!isset($searchValue['value'])) {
                                    $refData = getReferenceRowsWhereIn($fieldMdm['reference'], $searchValue);
                                    $searchValue = "";
                                    foreach ($refData as $item) {
                                        $searchValue .= "({$item->code}) $item->name <br /><br />";
                                    }
                                } else {
                                    // to remove
                                    $searchValue = $searchValue['value'];
                                    if (is_array($searchValue)) {
                                        $searchValue = $searchValue['value'];
                                    }
                                }
                            }
                            $newValue = getReferenceRows($fieldMdm['reference'], $searchValue, false);
                            if (!is_null($newValue)) {
                                if ($mdmType == 'd' || $mdmType == 'dt') {
                                    $searchValue = isDateFormat($newValue->name) ? formattedDate($newValue->name) : $newValue->name;
                                } else {
                                    $searchValue = $newValue->name;
                                }
                            } elseif ($searchValue === '0') {
                                $searchValue = '';
                            }
                        }
                    }
                } else {

                    if ($field['field'] == 'saf_general_sub_application_access_token') {
                        $searchValue = $value->access_token;
                    }

                    if ($field['field'] == 'saf_general_subapplication_submitting_date') {
                        $searchValue = isDateFormat($value->status_date) ? formattedDate($value->status_date) : $value->status_date;
                    }
                }
                if (!empty($mdmType) && ($mdmType == 'd' || $mdmType == 'dt')) {
                    $row[$field['field']] = isDateFormat($searchValue) ? formattedDate($searchValue) : $searchValue;
                } else {
                    $row[$field['field']] = $searchValue;
                }

            }

            $row['id'] = $value->saf_sub_application_id;
            $row['state'] = !empty($value->currentState->state) ? $value->currentState->state->currentMl->state_name : '';
            $row['submittedDate'] = !empty($value->status_date) ? isDateFormat($value->status_date) ? formattedDate($value->status_date) : $value->status_date : '';
            $row['submittedDateOriginal'] = !empty($value->status_date) ? $value->status_date : '';
            $row['is_edited'] = $value->is_edited;

            $state = optional($value->currentState)->state;
            if (!is_null($state) && (($state->state_type == ConstructorStates::END_STATE_TYPE && $state->state_approved_status == ConstructorStates::END_STATE_NOT_APPROVED) || $state->state_type == ConstructorStates::CANCELED_STATE_TYPE)) {
                $row['currentStatus'] = 'rejected';
            } elseif (!is_null($state) && $state->state_type == ConstructorStates::END_STATE_TYPE && $state->state_approved_status == ConstructorStates::END_STATE_APPROVED) {
                $row['currentStatus'] = 'success';
            }

            if (!is_null($searchData)) {
                $this->searchData = $searchData;
            }
            if (isset($this->searchData, $this->searchData['search_by_superscribe_field'])) {
                $row['superscribe_field'] = '';
                if ($superscribeUser = $value->superscribeUser) {
                    if ($user = $superscribeUser->user) {
                        $row['superscribe_field'] = $user->name();
                    }
                }
            }

            $result[] = $row;
        }

        return $result;
    }

    /**
     * Function to get saf autocomplete fields is name
     *
     * @return array
     */
    private function getSafAutoCompleteFieldsIdNameArray()
    {
        $returnArray = [];
        $lastXML = SingleApplicationDataStructure::lastXml();
        $elements = $lastXML->xpath("//field");

        foreach ($elements as $element) {
            $name = (string)$element->attributes()->name;
            $type = (string)$element->attributes()->type;
            if ($name && ($type == 'select' || $type == 'autocomplete')) {
                $returnArray[(string)$element->attributes()->id] = $name;
            }
        }

        return $returnArray;
    }
}
