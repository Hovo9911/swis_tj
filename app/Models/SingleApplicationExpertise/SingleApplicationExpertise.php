<?php

namespace App\Models\SingleApplicationExpertise;

use App\Models\BaseModel;
use App\Models\Laboratory\Laboratory;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Auth;

/**
 * Class SingleApplicationExpertise
 * @package App\Models\SingleApplicationExpertise
 */
class SingleApplicationExpertise extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'saf_expertise';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'company_tax_id',
        'letter_code',
        'saf_number',
        'sub_application_id',
        'laboratory_id',
        'sample_code',
        'sampling_method_code',
        'sampling_weight',
        'sampler_code',
        'saf_product_id',
        'saf_product_batches_ids',
        'expertising_method_code',
        'product_group_code',
        'expertising_indicator_code',
        'product_sub_group_code',
        'send_to_lab',
        'total_price',
        'received_date',
        'return_date',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'saf_product_batches_ids' => 'array',
        'sampler_code' => 'array',
    ];

    /**
     * Function to get received date value
     *
     * @param $value
     * @return string|void
     */
    public function getReceivedDateAttribute($value)
    {
        if ($value) {
            return Carbon::parse($value)->format(config('swis.date_format_front'));
        }
    }

    /**
     * Function to get return date value
     *
     * @param $value
     * @return string|void
     */
    public function getReturnDateAttribute($value)
    {
        if ($value) {
            return Carbon::parse($value)->format(config('swis.date_time_format_front'));
        }
    }

    /**
     * Function to ger created_at value
     *
     * @param $value
     * @return string|void
     */
    public function getCreatedAtAttribute($value)
    {
        if ($value) {
            return Carbon::parse($value)->format(config('swis.date_time_format_front'));
        }
    }

    /**
     * Function to relate with Laboratory
     *
     * @return HasOne
     */
    public function laboratory()
    {
        return $this->hasOne(Laboratory::class, 'id', 'laboratory_id');
    }

    /**
     * Function to check for owner/creator
     *
     * @param $query
     * @return Builder
     */
    public function scopeSafExpertiseOwner($query)
    {
        $query->where('saf_expertise.company_tax_id', Auth::user()->companyTaxId());

        return $query;
    }
}
