<?php

namespace App\Models\SingleApplicationExpertise;

use App\Models\Laboratory\Laboratory;
use App\Models\SingleApplicationExpertiseIndicators\SingleApplicationExpertiseIndicators;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class SingleApplicationExpertiseManager
 * @package App\Models\SingleApplicationExpertise
 */
class SingleApplicationExpertiseManager
{
    /**
     * Function to Store data to DB
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $labId = array_unique($data['lab_id'])[0];

        $data = $this->convertToInsertData($data, $labId);

        $singleApplicationExpertise = new SingleApplicationExpertise($data);

        DB::transaction(function () use ($data, $singleApplicationExpertise) {
            $singleApplicationExpertise->save();
            $indicators = $this->generateExpertiseIndicator($data['expertising_actions'], $singleApplicationExpertise->id);

            foreach ($indicators as $indicator) {
                SingleApplicationExpertiseIndicators::insert($indicator);
            }
        });
    }

    /**
     * Function to Update data
     *
     * @param array $data
     * @param $id
     */
    public function update(array $data, $id)
    {
        $labId = (!empty($data['expertising_actions'])) ? Laboratory::getLaboratoryIdByIndicator(key($data['expertising_actions'])) : 0;
        $data = $this->convertToInsertData($data, $labId, 'edit');
        $singleApplicationExpertise = SingleApplicationExpertise::where('id', $id)->firstOrFail();

        DB::transaction(function () use ($data, $singleApplicationExpertise) {
            $singleApplicationExpertise->update($data);
            SingleApplicationExpertiseIndicators::where('saf_expertise_id', $singleApplicationExpertise->id)->delete();
            $indicators = $this->generateExpertiseIndicator($data['expertising_actions'], $singleApplicationExpertise->id);

            foreach ($indicators as $indicator) {
                SingleApplicationExpertiseIndicators::insert($indicator);
            }
        });
    }

    /**
     * Function to convert To Insert Data
     *
     * @param array $data
     * @param $labId
     * @param string $saveMode
     * @return array
     */
    private function convertToInsertData(array $data, $labId, $saveMode = 'add')
    {
        if ($saveMode == 'add') {
            $data['letter_code'] = "L-M" . uniqid();
            $data['laboratory_id'] = $labId;
            $data['company_tax_id'] = Auth::user()->companyTaxId();
            $data['user_id'] = Auth::id();
        }

        return $data;
    }

    /**
     * Function to generate Expertise Indicator
     *
     * @param $data
     * @param $safExpertiseId
     * @return array
     */
    private function generateExpertiseIndicator($data, $safExpertiseId)
    {
        $indicators = [];

        if (!empty($data)) {
            foreach ($data as $key => $item) {
                $indicators[] = [
                    'saf_expertise_id' => $safExpertiseId,
                    'company_tax_id' => Auth::user()->companyTaxId(),
                    'user_id' => Auth::id(),
                    'laboratory_expertise_code' => $key,
                ];
            }
        }

        return $indicators;
    }
}
