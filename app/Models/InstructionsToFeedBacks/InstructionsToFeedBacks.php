<?php

namespace App\Models\InstructionsToFeedBacks;

use App\Models\BaseModel;

/**
 * Class InstructionsToFeedBacks
 * @package App\Models\InstructionsToFeedBacks
 */
class InstructionsToFeedBacks extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'instructions_to_feedback';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'instructions_id',
        'feedback_id',
        'code'
    ];
}
