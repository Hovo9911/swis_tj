<?php

namespace App\Models\ConstructorMapping;

use App\Models\BaseMlManager;
use App\Models\ConstructorMappingToRule\ConstructorMappingToRule;
use App\Models\ConstructorMappingToRule\ConstructorMappingToRuleManager;
use Illuminate\Support\Facades\DB;

/**
 * Class ConstructorMappingManager
 * @package App\Models\ConstructorMapping
 */
class ConstructorMappingManager
{
    use BaseMlManager;

    /**
     * Function to Store data to DB
     *
     * @param array $data
     * @return ConstructorMapping
     */
    public function store(array $data)
    {
        $data['obligation_exist'] = isset($data['obligation_exist']) ? ConstructorMapping::TRUE : ConstructorMapping::FALSE;
        $data['enable_risks'] = isset($data['enable_risks']) ? ConstructorMapping::TRUE : ConstructorMapping::FALSE;
        $data['digital_signature'] = isset($data['digital_signature']) ? ConstructorMapping::TRUE : ConstructorMapping::FALSE;

        $constructorMapping = new ConstructorMapping($data);
        $constructorMappingMl = new ConstructorMappingMl();
        $constructorMappingToRuleManager = new ConstructorMappingToRuleManager();

        return DB::transaction(function () use ($data, $constructorMapping, $constructorMappingMl, $constructorMappingToRuleManager) {

            $constructorMapping->save();
            $this->storeMl($data['ml'], $constructorMapping, $constructorMappingMl);

            if (!empty($data['rules'])) {
                $constructorMappingToRuleManager->store($data['rules'], $constructorMapping->id);
            }

            return $constructorMapping;
        });
    }

    /**
     * Function to Update data
     *
     * @param array $data
     * @param int $id
     */
    public function update(array $data, int $id)
    {
        $constructorMapping = ConstructorMapping::where('id', $id)->constructorMappingOwner()->firstOrFail();
        $constructorMappingMl = new ConstructorMappingMl();
        $constructorMappingToRuleManager = new ConstructorMappingToRuleManager();

        $data['obligation_exist'] = isset($data['obligation_exist']) ? ConstructorMapping::TRUE : ConstructorMapping::FALSE;
        $data['enable_risks'] = isset($data['enable_risks']) ? ConstructorMapping::TRUE : ConstructorMapping::FALSE;
        $data['digital_signature'] = isset($data['digital_signature']) ? ConstructorMapping::TRUE : ConstructorMapping::FALSE;

        DB::transaction(function () use ($data, $constructorMapping, $constructorMappingMl, $constructorMappingToRuleManager) {
            $constructorMapping->update($data);
            $this->updateMl($data['ml'], 'constructor_mapping_id', $constructorMapping, $constructorMappingMl);

            if (empty($data['rules'])) {
                $data['rules'] = [];
            }

            $constructorMappingToRuleManager->update($data['rules'], $constructorMapping->id);
        });
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @param $id2
     * @return array
     */
    public function getLogData($id, $id2 = null)
    {
        $data = ConstructorMapping::where('id', $id)->first()->toArray();
        $data['mls'] = ConstructorMappingMl::where('constructor_mapping_id', $id)->get()->toArray();
        $data['rules'] = ConstructorMappingToRule::where('constructor_mapping_id', $id)->get()->toArray();

        return $data;
    }

}
