<?php

namespace App\Models\ConstructorMapping;

use App\Models\BaseModel;
use App\Models\ConstructorActions\ConstructorActions;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorLists\ConstructorLists;
use App\Models\ConstructorListToDataSetFromMdm\ConstructorListToDataSetFromMdm;
use App\Models\ConstructorListToDataSetFromSaf\ConstructorListToDataSetFromSaf;
use App\Models\ConstructorListToTabs\ConstructorListToTabs;
use App\Models\ConstructorRules\ConstructorRules;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\Notifications\Notifications;
use App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Query\Builder;

/**
 * Class ConstructorMapping
 * @package App\Models\ConstructorMapping
 */
class ConstructorMapping extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'constructor_mapping';

    /**
     * @var bool
     */
    protected $hasAdminInfo = true;

    /**
     * @var array
     */
    protected $fillable = [
        'document_id',
        'start_state',
        'role',
        'action',
        'end_state',
        'show_status',
        'non_visible_list',
        'editable_list',
        'mandatory_list',
        'obligation_exist',
        'enable_risks',
        'applicant',
        'importer_exporter',
        'agency_admin',
        'persons_in_next_state',
        'digital_signature'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'created_admin_id',
        'created_admin_ip',
        'updated_at',
        'updated_admin_id',
        'updated_admin_ip',
        'deleted_admin_id',
        'deleted_admin_ip'
    ];

    /**
     * @var int
     */
    const NEED_DIGITAL_SIGNATURE = 1;

    /**
     * @return array
     */
    public static function getFixRoles()
    {
        return [
            Notifications::NOTIFICATION_TYPE_APPLICANT,
            Notifications::NOTIFICATION_TYPE_IMPORTER_EXPORTER,
            Notifications::NOTIFICATION_TYPE_AGENCY_ADMIN,
            Notifications::NOTIFICATION_TYPE_PERSON_IN_NEXT_STATE,
        ];
    }

    /**
     * Function to relate with ConstructorMappingMl
     *
     * @return HasMany
     */
    public function ml()
    {
        return $this->hasMany(ConstructorMappingMl::class, 'constructor_mapping_id', 'id');
    }

    /**
     * Function to relate with ConstructorMappingMl
     *
     * @return HasOne
     */
    public function current()
    {
        return $this->hasOne(ConstructorMappingMl::class, 'constructor_mapping_id', 'id')->notDeleted()->where('lng_id', cLng('id'))->first();
    }

    /**
     * Function to relate with ConstructorStates
     *
     * @return HasOne
     */
    public function startState()
    {
        return $this->hasOne(ConstructorStates::class, 'id', 'start_state');
    }

    /**
     * Function to relate with ConstructorStates
     *
     * @return HasMany
     */
    public function endStates()
    {
        return $this->hasMany(ConstructorStates::class, 'id', 'end_state');
    }

    /**
     * Function to relate with ConstructorStates
     *
     * @return HasOne
     */
    public function endState()
    {
        return $this->hasOne(ConstructorStates::class, 'id', 'end_state');
    }

    /**
     * Function to relate with ConstructorActions
     *
     * @return HasOne
     */
    public function rAction()
    {
        return $this->hasOne(ConstructorActions::class, 'id', 'action');
    }

    /**
     * Function to relate with ConstructorLists
     *
     * @return HasOne
     */
    public function getNonVisibleList()
    {
        return $this->hasOne(ConstructorLists::class, 'id', 'non_visible_list');
    }

    /**
     * Function to relate with ConstructorLists
     *
     * @return HasOne
     */
    public function getEditableList()
    {
        return $this->hasOne(ConstructorLists::class, 'id', 'editable_list');
    }

    /**
     * Function to relate with ConstructorLists
     *
     * @return HasOne
     */
    public function getMandatoryList()
    {
        return $this->hasOne(ConstructorLists::class, 'id', 'mandatory_list');
    }

    /**
     * Function to relate with ConstructorListToDataSetFromSaf
     *
     * @return HasMany
     */
    public function nonVisibleFieldsSaf()
    {
        return $this->hasMany(ConstructorListToDataSetFromSaf::class, 'constructor_list_id', 'non_visible_list');
    }

    /**
     * Function to relate with ConstructorListToDataSetFromMdm
     *
     * @return HasMany
     */
    public function nonVisibleFieldsMdm()
    {
        return $this->hasMany(ConstructorListToDataSetFromMdm::class, 'constructor_list_id', 'non_visible_list');
    }

    /**
     * Function to relate with ConstructorListToTabs
     *
     * @return HasMany
     */
    public function nonVisibleTabs()
    {
        return $this->hasMany(ConstructorListToTabs::class, 'constructor_list_id', 'non_visible_list');
    }

    /**
     * Function to relate with ConstructorListToDataSetFromSaf
     *
     * @return HasMany
     */
    public function editableFieldsSaf()
    {
        return $this->hasMany(ConstructorListToDataSetFromSaf::class, 'constructor_list_id', 'editable_list');
    }

    /**
     * Function to relate with ConstructorListToDataSetFromMdm
     *
     * @return HasMany
     */
    public function editableFieldsMdm()
    {
        return $this->hasMany(ConstructorListToDataSetFromMdm::class, 'constructor_list_id', 'editable_list');
    }

    /**
     * Function to relate with ConstructorListToTabs
     *
     * @return HasMany
     */
    public function editableTabs()
    {
        return $this->hasMany(ConstructorListToTabs::class, 'constructor_list_id', 'editable_list');
    }

    /**
     * Function to relate with ConstructorListToDataSetFromSaf
     *
     * @return HasMany
     */
    public function mandatoryFieldsSaf()
    {
        return $this->hasMany(ConstructorListToDataSetFromSaf::class, 'constructor_list_id', 'mandatory_list');
    }

    /**
     * Function to relate with ConstructorListToDataSetFromMdm
     *
     * @return HasMany
     */
    public function mandatoryFieldsMdm()
    {
        return $this->hasMany(ConstructorListToDataSetFromMdm::class, 'constructor_list_id', 'mandatory_list');
    }

    /**
     * Function to check auth user has role
     *
     * @param $query
     * @return Builder
     */
    public function scopeConstructorMappingOwner($query)
    {
        $agencyDocumentIds = ConstructorDocument::getConstructorDocumentForAgency()->pluck('id')->toArray();

        return $query->whereIn($this->getTable() . '.document_id', $agencyDocumentIds ?? -1);
    }

    /**
     * Function to get Mappings With Lists
     *
     * @param $mappings
     * @param bool $onlyNoneVisible
     * @return array
     */
    public static function getMappingsWithLists($mappings, $onlyNoneVisible = false)
    {
        $mappingWithList = ConstructorLists::LIST_TYPES;

        foreach ($mappings as $mapping) {
            if (isset($mapping->non_visible_list)) {
                $mappingWithList['non_visible'][] = $mapping;
            }
            if (!$onlyNoneVisible) {
                if (isset($mapping->editable_list)) {
                    $mappingWithList['editable'][] = $mapping;
                }
                if (isset($mapping->mandatory_list)) {
                    $mappingWithList['mandatory'][] = $mapping;
                }
            }
        }

        return $mappingWithList;
    }

    /**
     * Function to get tab lists
     *
     * @param $mappings
     * @return array
     */
    public static function getTabLists($mappings)
    {
        $mappingWithList = ['non_visible' => [], 'editable' => []];
        foreach ($mappings as $mapping) {
            if (isset($mapping->non_visible_list)) {
                $mappingWithList['non_visible'][] = $mapping;
            }

            if (isset($mapping->editable_list)) {
                $mappingWithList['editable'][] = $mapping;
            }
        }

        return $mappingWithList;
    }

    /**
     * Function to check is one of mapping has a attached free sum rule
     *
     * @param $documentId
     * @param $roles
     * @param $documentCurrentState
     * @return bool
     */
    public static function isHaveMultipleObligation($documentId, $roles, $documentCurrentState)
    {
        $mappings = ConstructorMapping::select('reference_obligation_creation_type.obligation_creation_type')
            ->join('constructor_actions', 'constructor_mapping.action', '=', 'constructor_actions.id')
            ->join('constructor_mapping_to_rule', 'constructor_mapping_to_rule.constructor_mapping_id', '=', 'constructor_mapping.id')
            ->join('constructor_rules', 'constructor_rules.id', '=', 'constructor_mapping_to_rule.rule_id')
            ->join('reference_obligation_budget_line', 'reference_obligation_budget_line.code', '=', 'constructor_rules.obligation_code')
            ->join('reference_obligation_creation_type', 'reference_obligation_creation_type.id', '=', 'reference_obligation_budget_line.obligation_creation_type')
            ->where('reference_obligation_creation_type.obligation_creation_type', ConstructorRules::CREATION_TYPE_FREE_SUM)
            ->where('constructor_mapping.document_id', $documentId)
            ->where('constructor_mapping.start_state', $documentCurrentState->state_id)
            ->whereIn('constructor_mapping.role', $roles)
            ->orderBy('constructor_mapping.action')
            ->active()
            ->count();

        return ($mappings > 0);
    }

    /**
     * Function to get Only Editable data
     *
     * @param $mapping
     * @param $dataFromFront
     * @return array
     */
    public static function getOnlyEditableData($mapping, $dataFromFront)
    {
        $lastXML = SingleApplicationDataStructure::lastXml();
        $notDeletedItems = $getSafIdNameArray = [];
        $safData = prefixKey('', ['saf' => $dataFromFront['saf'] ?? []]);
        $tempCustomData = $dataFromFront['customData'];
        if (isset($mapping->editable_list)) {

            foreach ($lastXML->xpath("//field") as $element) {
                if ($name = (string)$element->attributes()->name) {
                    $getSafIdNameArray[(string)$element->attributes()->id] = (!empty((string)$element->attributes()->dataName)) ? (string)$element->attributes()->dataName : $name;
                }
            }

            $editableFieldsSaf = isset($mapping->editableFieldsSaf) ? $mapping->editableFieldsSaf->pluck('saf_field_id')->all() : [];

            foreach ($editableFieldsSaf as $item) {
                if (isset($getSafIdNameArray[$item]) && $safFieldName = $getSafIdNameArray[$item]) {
                    if ($safFieldName == "saf[transportation][state_number_customs_support][_index_][track_container_temperature]") {
                        if (isset($dataFromFront['saf']['transportation']['state_number_customs_support'])) {
                            foreach ($dataFromFront['saf']['transportation']['state_number_customs_support'] as $key => $stateNumberCustomsSupport) {
                                $fieldName = str_replace("][", ".", str_replace('_index_', $key++, $safFieldName));
                                $fieldName = str_replace("[", ".", $fieldName);
                                $fieldName = str_replace("]", "", $fieldName);
                                $notDeletedItems[$fieldName] = array_key_exists($fieldName, $safData) ? $safData[$fieldName] : null;
                            }
                        }
                    } else {
                        $fieldName = str_replace("][", ".", $getSafIdNameArray[$item]);
                        $fieldName = str_replace("[", ".", $fieldName);
                        $fieldName = str_replace("]", "", $fieldName);
                        $notDeletedItems[$fieldName] = array_key_exists($fieldName, $safData) ? $safData[$fieldName] : null;
                    }
                }
            }

            $editableFieldsMdm = isset($mapping->editableFieldsMdm) ? $mapping->editableFieldsMdm->pluck('constructor_data_set_field_id')->all() : [];
            foreach ($tempCustomData as $key => $item) {
                if (strpos($key, "_pr_") !== false) {
                    $key = str_replace(substr($key, strpos($key, "_pr_")), "", $key);
                }
                if (!in_array($key, $editableFieldsMdm)) {
                    unset($tempCustomData[$key]);
                }
            }
        }

        //      //custom_data    //data
        return [$tempCustomData, self::getArrayFromSafNameWithPoints($notDeletedItems)];
    }

    /**
     * Function to get array from saf
     *
     * @param $safData
     * @param array $finalArray
     * @return array
     */
    public static function getArrayFromSafNameWithPoints($safData, $finalArray = [])
    {
        foreach ($safData as $key => $value) {
            $keys = explode('.', $key);
            $dataArray = self::dataArray($keys, $value);
            $finalArray = array_merge_recursive($finalArray, $dataArray);
        }

        return $finalArray;
    }

    /**
     * Function to data Array
     *
     * @param $array
     * @param $val
     * @return array
     */
    public static function dataArray($array, $val)
    {
        $value = $val;
        $resultArray = $value;
        for ($i = count($array) - 1; $i >= 0; $i--) {
            $resultArray = array($array[$i] => $resultArray);
        }

        return $resultArray;
    }

}
