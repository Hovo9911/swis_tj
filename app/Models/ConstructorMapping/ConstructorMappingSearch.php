<?php

namespace App\Models\ConstructorMapping;

use App\Http\Controllers\Core\DataTableSearch;
use Illuminate\Support\Facades\DB;

/**
 * Class ConstructorMappingSearch
 * @package App\Models\ConstructorMapping
 */
class ConstructorMappingSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        $mappings = $query->get();

        foreach ($mappings as &$item) {
            $item->nonVisibleListToolTip = isset($item->getNonVisibleList) ? $item->getNonVisibleList->description : '';
            $item->editableListToolTip = isset($item->getEditableList) ? $item->getEditableList->description : '';
            $item->mandatoryListToolTip = isset($item->getMandatoryList) ? $item->getMandatoryList->description : '';
        }

        return $mappings;
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($this->orderByCol, $this->orderByType ?? 'desc');
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $query = ConstructorMapping::select(
            'constructor_mapping.id',
            'constructor_states_ml.state_name as start_state',
            'constructor_actions_ml.action_name as action',
            'role_ml.name as role',
            'constructor_mapping.non_visible_list',
            'constructor_mapping.editable_list',
            'constructor_mapping.mandatory_list',
            'constructor_mapping.show_status',
            't1.state_name as end_state')
            ->joinMl()
            ->join('constructor_states_ml', 'constructor_mapping.start_state', '=', 'constructor_states_ml.constructor_states_id')
            ->join('constructor_actions_ml', 'constructor_mapping.action', '=', 'constructor_actions_ml.constructor_actions_id')
            ->join('roles', 'constructor_mapping.role', '=', 'roles.id')
            ->join('role_ml', 'roles.id', '=', 'role_ml.role_id')->where('role_ml.lng_id', '=', cLng('id'))
            ->join('constructor_states_ml as t1', 'constructor_mapping.end_state', '=', 't1.constructor_states_id')
            ->where('constructor_states_ml.lng_id', '=', cLng('id'))
            ->where('constructor_actions_ml.lng_id', '=', cLng('id'))
            ->where('t1.lng_id', '=', cLng('id'))
            ->where('constructor_mapping.document_id', $this->searchData['constructor_document_id'])
            ->constructorMappingOwner()
            ->notDeleted();

        if (isset($this->searchData['id'])) {
            $query->where(DB::raw('constructor_mapping.id::varchar'), $this->searchData['id']);
        }

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}
