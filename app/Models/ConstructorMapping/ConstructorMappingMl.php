<?php

namespace App\Models\ConstructorMapping;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

/**
 * Class ConstructorMappingMl
 * @package App\Models\ConstructorMapping
 */
class ConstructorMappingMl extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['constructor_mapping_id', 'lng_id'];

    /**
     * @var string
     */
    public $incrementing = 'false';

    /**
     * @var string
     */
    public $table = 'constructor_mapping_ml';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'constructor_mapping_id',
        'lng_id',
        'description',
        'show_status'
    ];
}
