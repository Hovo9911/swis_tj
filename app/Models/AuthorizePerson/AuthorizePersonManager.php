<?php

namespace App\Models\AuthorizePerson;

use App\Models\Role\Role;
use App\Models\UserRoles\UserRoles;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

/**
 * Class AuthorizePersonManager
 * @package App\Models\AuthorizePerson
 */
class AuthorizePersonManager
{
    /**
     * Function to inactivating  user roles
     */
    public function inactivateUserRoles()
    {
        UserRoles::where('role_status', Role::STATUS_ACTIVE)->where('available_to', '<', currentDate())
            ->update(['role_status' => Role::STATUS_DELETED]);
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @param null $parentId
     * @return array
     */
    public function getLogData($id = null, $parentId = null)
    {
        if (!is_null($id)) {
            $userRoles = UserRoles::where(['user_id' => $id])->active();

            if (!Auth::user()->isSwAdmin()) {
                $userRoles->whereNotIn('role_id', [Role::SW_ADMIN]);

                if (Auth::user()->isLocalAdmin()) {
                    $userRoles->where('user_roles.company_tax_id', Auth::user()->companyTaxId());
                } else {
                    $userRoles->where(['user_roles.authorized_by_id' => Auth::id()])->where('company_tax_id', '=', '0')->whereNotIn('user_roles.role_id', Role::DEFAULT_ROLES_ALL);
                }

            } else {

                if (Auth::user()->isAuthorizedSwAdmin() && !Auth::user()->userShowOthersData()) {
                    $userRoles->where(['user_roles.authorized_by_id' => Auth::id()])
                        ->where('company_tax_id', '=', '0')
                        ->whereNotIn('user_roles.role_id', [Role::NATURAL_PERSON]);
                }

                $userRoles->where('role_id', Role::SW_ADMIN);
            }

            return $userRoles->get()->toArray();
        }

        return [];
    }
}
