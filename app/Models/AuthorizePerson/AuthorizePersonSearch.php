<?php

namespace App\Models\AuthorizePerson;

use App\Http\Controllers\Core\DataTableSearch;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\Role\Role;
use App\Models\SingleApplication\SingleApplication;
use App\Models\User\User;
use App\Models\UserRoles\UserRoles;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class AuthorizePersonSearch
 * @package App\Models\AuthorizePerson
 */
class AuthorizePersonSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->rowsCountAll();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->rowsCountAll();
    }

    /**
     * @return int
     */
    private function rowsCountAll()
    {
        $query = $this->constructQuery();
        $result = DB::select("SELECT count(*) as cnt FROM ({$query->toSql()}) as temp", $query->getBindings());

        return $result[0]->cnt;
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        $users = $query->get();

        foreach ($users as &$user) {
            $user->available_to = !is_null($user->available_to) ? formattedDate($user->available_to) : '';
        }

        return $users;
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($this->orderByCol ?? 'first_role_created_at', $this->orderByType ?? 'desc');
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $companyTaxId = Auth::user()->companyTaxId();
        $roleCondition = '';
        if (Auth::user()->isSwAdmin()) {
            $roleCondition = " and role_type = '" . Role::ROLE_TYPE_SW_ADMIN . "'";
        }

        $query = User::select('users.id')->addSelect('users.ssn', 'users.passport', DB::raw("CONCAT(users.first_name,' ',users.last_name) as name"))
            ->selectRaw("(
                SELECT created_at from user_roles
                    where users.id = user_roles.user_id and updated_at IS NOT NULL and company_tax_id='$companyTaxId'  $roleCondition
                    ORDER BY id ASC LIMIT 1
            ) as first_role_created_at"
            )->selectRaw("(
                SELECT created_at from user_roles
                    where users.id = user_roles.user_id and company_tax_id='$companyTaxId' $roleCondition
                    ORDER BY id ASC LIMIT 1
            ) as registered_at")
            ->selectRaw("(
                SELECT count(role_status) from user_roles
                    where users.id = user_roles.user_id AND
                    role_status = '1' AND company_tax_id='$companyTaxId' and role_status != 'null' $roleCondition
                    GROUP BY role_status LIMIT 1
            ) as role_status")
            ->selectRaw("(
                SELECT available_to from user_roles
                    where users.id = user_roles.user_id AND
                    role_status = '1' AND company_tax_id='$companyTaxId' AND  available_to IS NOT NULL $roleCondition
                    ORDER BY available_to DESC LIMIT 1
            ) as available_to")
            ->selectRaw("(
                SELECT roleUser.first_name || ' ' || roleUser.last_name from user_roles
                    left join users as roleUser on roleUser.id = user_roles.authorized_by_id
                    where users.id = user_roles.user_id 
                    AND company_tax_id='$companyTaxId' $roleCondition
                    ORDER BY user_roles.id ASC LIMIT 1
            ) as first_role_created_username");

        $query->join('user_roles', 'users.id', '=', 'user_roles.user_id')
            ->where('user_roles.show_status', '!=', Role::STATUS_DELETED)
            ->groupBy('users.id');

        if (isset($this->searchData['id'])) {
            $query->where(DB::raw("users.id::varchar"), $this->searchData['id']);
        }

        if (isset($this->searchData['last_name'])) {
            $query->where('users.last_name', 'ILIKE', '%' . strtolower(trim($this->searchData['last_name'])) . '%');
        }

        if (isset($this->searchData['first_name'])) {
            $query->where('first_name', 'ILIKE', '%' . strtolower(trim($this->searchData['first_name'])) . '%');
        }

        if (isset($this->searchData['ssn'])) {
            $query->where('users.ssn', $this->searchData['ssn']);
        }

        if (isset($this->searchData['passport'])) {
            $query->where('users.passport', $this->searchData['passport']);
        }

        if (isset($this->searchData['role_status'])) {

            if ($this->searchData['role_status'] == Role::STATUS_ACTIVE) {

                $query->where('user_roles.role_status', Role::STATUS_ACTIVE);

            } else {

                $userRoles = UserRoles::select('user_id')
                    ->selectRaw("(SELECT count(role_status) from user_roles as u_roles
                    where user_roles.user_id = u_roles.user_id AND
                    role_status != '0' AND role_status != '2' AND company_tax_id='$companyTaxId' $roleCondition
                    GROUP BY role_status LIMIT 1
                )  as role_status_not_deleted")
                    ->groupBy('user_id')
                    ->get();

                $query->whereIn('user_roles.user_id', $userRoles->where('role_status_not_deleted', null)->pluck('user_id')->all());
            }
        }

        if (isset($this->searchData['menu'])) {
            if (isset($this->searchData['role']) && $this->searchData['role'] != Role::AUTHORIZE_USER_ADD_MY_PERMISSIONS) {
                $query->where('user_roles.menu_id', $this->searchData['menu']);
            }
        }

        /* if (isset($this->searchData['agency'])) {
             $query->where('user_roles.company_tax_id', $this->searchData['agency']);
         }*/

        if (isset($this->searchData['role'])) {

            if ($this->searchData['role'] == Role::AUTHORIZE_USER_ADD_MY_PERMISSIONS) {
                $query->where('user_roles.role_id', Role::HEAD_OF_COMPANY)->whereNotNull('authorized_by_id');
            } else {
                $query->where('user_roles.role_id', $this->searchData['role']);
            }

        }

        if (isset($this->searchData['authorizer_ssn'])) {

            $userId = User::where('ssn', $this->searchData['authorizer_ssn'])->pluck('id')->first();

            $query->where('user_roles.authorized_by_id', $userId);
        }

        if (isset($this->searchData['authorizer_first_name'])) {

            $authorizerUsersId = User::where('first_name', 'ILIKE', '%' . strtolower(trim($this->searchData['authorizer_first_name'])) . '%')->pluck('id')->all();

            if (count($authorizerUsersId)) {
                $query->whereIn('user_roles.authorized_by_id', $authorizerUsersId);
            } else {
                $query->where('user_roles.id', 0);
            }
        }

        if (isset($this->searchData['authorizer_last_name'])) {

            $authorizerUsersId = User::where('last_name', 'ILIKE', '%' . strtolower(trim($this->searchData['authorizer_last_name'])) . '%')->pluck('id')->all();

            if (count($authorizerUsersId)) {
                $query->whereIn('user_roles.authorized_by_id', $authorizerUsersId);
            } else {
                $query->where('user_roles.id', 0);
            }
        }

        if (Auth::user()->isNaturalPerson() && Auth::user()->isSwAdmin()) {
            $query->where('users.id', '!=', Auth::id());
        }

        if (isset($this->searchData['sub_division_id'])) {

            $subDivisionCode = optional(getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS, $this->searchData['sub_division_id'], false, ['code']))->code;

            $subDivisionsIds = !is_null($subDivisionCode) ? DB::table(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS)->where('code', $subDivisionCode)->pluck('id')->all() : [];

            $query->where(['user_roles.attribute_field_id' => SingleApplication::SAF_AGENCY_SUBDIVISION_ID]);
            $query->whereIn('user_roles.attribute_value', $subDivisionsIds);
        }

        //
        $query->userRolesByType();
        $query->whereNotIn('users.ssn', User::SW_ADMIN_SSN);

        //
        if (Auth::user()->isTransportMinistryAgencyEmployee()) {
            $query->where('users.id', '!=', Auth::id());
        }

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}
