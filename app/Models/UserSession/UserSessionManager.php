<?php

namespace App\Models\UserSession;

use Illuminate\Support\Facades\DB;

/**
 * Class UserSessionManager
 * @package App\Models\UserSession
 */
class UserSessionManager
{
    /**
     * Function to Store data to DB
     *
     * @param int $userId
     * @param string $sessionId
     */
    public static function store(int $userId, string $sessionId)
    {
        DB::transaction(function () use ($userId, $sessionId) {
            UserSession::create([
                'user_id' => $userId,
                'session_id' => $sessionId,
            ]);
        });
    }

    /**
     * Function to Delete data
     *
     * @param int $userId
     * @param string $sessionId
     */
    public static function delete(int $userId, string $sessionId)
    {
        DB::transaction(function () use ($userId, $sessionId) {
            UserSession::where('user_id', $userId)->where('session_id', $sessionId)->delete();
        });
    }

    /**
     * Function to Delete data
     *
     * @param int $userId
     * @param string $sessionId
     */
    public static function deleteAllWithoutMySession(int $userId, string $sessionId)
    {
        DB::transaction(function () use ($userId, $sessionId) {
            UserSession::where('user_id', $userId)->where('session_id', '!=', $sessionId)->delete();
        });
    }
}
