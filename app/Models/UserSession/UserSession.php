<?php

namespace App\Models\UserSession;

use App\Models\BaseModel;

/**
 * Class UserSession
 * @package App\Models\UserSession
 */
class UserSession extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'user_session';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'session_id',
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
