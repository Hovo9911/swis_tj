<?php

namespace App\Models\ConstructorActionSuperscribes;

use App\Models\BaseModel;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class ConstructorActionSuperscribes
 * @package App\Models\ConstructorActionSuperscribes
 */
class ConstructorActionSuperscribes extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'constructor_action_superscribes';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'company_tax_id',
        'saf_subapplication_id',
        'constructor_document_id',
        'from_user',
        'to_user',
        'start_state_id',
        'end_state_id',
        'is_current',
        'show_status'
    ];

    /**
     * Function to relate with user table
     *
     * @return HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'to_user');
    }
}
