<?php

namespace App\Models\ConstructorDataSet;

use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm;
use App\Models\DocumentsDatasetFromSaf\DocumentsDatasetFromSaf;

/**
 * Class ConstructorDataSetManager
 * @package App\Models\ConstructorDataSet
 */
class ConstructorDataSetManager
{
    /**
     * Function to get logged data
     *
     * @param $id
     * @param null $id2
     * @return array
     */
    public function getLogData($id, $id2 = null)
    {
        $data['saf'] = DocumentsDatasetFromSaf::where('document_id', $id)->with(['ml'])->get()->toArray();
        $data['mdm'] = DocumentsDatasetFromMdm::where('document_id', $id)->with(['ml'])->get()->toArray();

        return $data;
    }
}