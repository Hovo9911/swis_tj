<?php

namespace App\Models\ConstructorDataSet;

use App\Models\BaseModel;
use App\Models\DataModel\DataModel;
use App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure;

/**
 * Class ConstructorDataSet
 * @package App\Models\ConstructorDataSet
 */
class ConstructorDataSet extends BaseModel
{
    /**
     * @var array (By Default search param)
     */
    const DEFAULT_SEARCH_PARAMETER_FIELDS = ['SAF_ID_13', 'SAF_ID_20', 'SAF_ID_35', 'SAF_ID_42', 'SAF_ID_57', 'SAF_ID_21', 'SAF_ID_43'];

    /**
     * Function to get Group Paths
     *
     * @return array
     */
    public static function getGroupPaths()
    {
        $groupPaths = [];
        $simpleAppTabs = SingleApplicationDataStructure::lastXml();
        $tabs = $simpleAppTabs->xpath('/tabs/tab');

        if ($tabs && count($tabs) > 0) {
            foreach ($tabs as $tab) {
                $tabKey = (string)$tab->attributes()->key;
                if (isset($tab->block, $tab->block)) {
                    if (count($tab->block) > 1) {
                        foreach ($tab->block as $block) {
                            $blockName = (string)$block->attributes()->name;
                            if (isset($block->fieldset)) {
                                $groupPaths['groupPath']["{$tabKey}/{$blockName}"] = trans($tabKey) . '/' . trans($blockName);
                                $groupPaths['groupXPath']["{$tabKey}/{$blockName}"] = "tab[@key='{$tabKey}']/block[@name='{$blockName}']";
                            }
                            if (isset($block->subBlock)) {
                                $subBlockName = (string)$block->subBlock->attributes()->name;
                                $groupPaths['groupPath']["{$tabKey}/{$blockName}/{$subBlockName}"] = trans($tabKey) . '/' . trans($blockName) . '/' . trans($subBlockName);
                                $groupPaths['groupXPath']["{$tabKey}/{$blockName}/{$subBlockName}"] = "tab[@key='{$tabKey}']/block[@name='{$blockName}']/subBlock[@name='{$subBlockName}']";
                            }
                        }
                    } else {
                        foreach ($tab->block as $block) {
                            $blockName = (string)$block->attributes()->name;
                            $groupPathName = empty($tab->block->attributes()) ? trans($tabKey) : trans($tabKey) . '/' . trans($blockName);
                            $groupPathNameSave = empty($tab->block->attributes()) ? $tabKey : $tabKey . '/' . $blockName;
                            $groupPaths['groupPath'][$groupPathNameSave] = $groupPathName;
                            $groupPaths['groupXPath'][$groupPathNameSave] = empty($tab->block->attributes()) ? "tab[@key='{$tabKey}']" : "tab[@key='{$tabKey}']/block[@name='{$blockName}']";
                        }
                    }
                } elseif (isset($tab->typeBlock)) {
                    $typeBlock = $tab->typeBlock[0];
                    if (isset($typeBlock->block)) {
                        foreach ($typeBlock->block as $block) {
                            $blockName = $block->attributes()->name;
                            $typeBlockName = (string)$typeBlock->attributes()->name;
                            $groupPaths['groupPath']["{$tabKey}/{$typeBlockName}/{$blockName}"] = trans($tabKey) . '/' . trans($blockName);
                            $groupPaths['groupXPath']["{$tabKey}/{$typeBlockName}/{$blockName}"] = "tab[@key='{$tabKey}']/typeBlock[@name='{$typeBlockName}']/block[@name='{$blockName}']";
                        }
                    }
                } elseif ((string)$tab->attributes()->key == 'print_forms') {
                    $groupPaths['groupPath'][$tabKey] = trans((string)$tab->attributes()->name);
                    $groupPaths['groupXPath'][$tabKey] = "tab[@key='{$tabKey}']";
                }
            }
        }

        return $groupPaths;
    }

    /**
     * Function to get array with key (field name) and value (field path)
     *
     * @return array
     */
    public static function getGroupNamePath()
    {
        $simpleAppTabs = SingleApplicationDataStructure::lastXml();
        $tabs = $simpleAppTabs->xpath('/tabs/tab');
        $groupPaths = $safFields = [];

        if ($tabs && count($tabs) > 0) {
            foreach ($tabs as $tab) {
                if (isset($tab->block, $tab->block)) {
                    if (count($tab->block) > 1) {
                        foreach ($tab->block as $block) {
                            if (isset($block->fieldset)) {
                                foreach ($block->fieldset as $fieldSet) {
                                    $safFields = ConstructorDataSet::returnSafFields($tab, $block, $fieldSet, $safFields, 'block');
                                }
                            }
                            if (isset($block->subBlock, $block->subBlock->fieldset)) {
                                foreach ($block->subBlock->fieldset as $fieldSet) {
                                    $safFields = ConstructorDataSet::returnSafFields($tab, $block, $fieldSet, $safFields, 'subBlock');
                                }
                            }
                        }
                    } else {
                        foreach ($tab->block->fieldset as $fieldSet) {
                            $safFields = ConstructorDataSet::returnSafFields($tab, $tab->block, $fieldSet, $safFields, 'tabBlock');
                        }
                    }
                } elseif (isset($tab->typeBlock)) {
                    foreach ($tab->typeBlock as $typeBlock) {
                        if (isset($typeBlock->block)) {
                            foreach ($typeBlock->block as $block) {
                                if (isset($block->fieldset)) {
                                    foreach ($block->fieldset as $fieldSet) {
                                        $safFields = ConstructorDataSet::returnSafFields($tab, $block, $fieldSet, $safFields, 'typeBlock', $typeBlock);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        foreach ($safFields as $field) {
            $groupPaths[$field['xml_field_name']] = $field['tab'];
        }

        return $groupPaths;
    }

    /**
     * Function to set $SafField array value
     *
     * @param $attr
     * @param $fieldSet
     * @param $show_attr
     * @param $tab
     * @param $tabPathForSave
     * @param $isField
     * @param $mdm
     * @return array
     */
    public static function setSafFieldsArray($attr, $fieldSet, $show_attr, $tab, $tabPathForSave, $isField, $mdm)
    {
        if (!is_null($mdm)) {
            $desc = $mdm->definition;
        } elseif ($isField) {
            $desc = $fieldSet->attributes()->tooltipText ? trans((string)$fieldSet->attributes()->tooltipText) : '';
        } else {
            $desc = $fieldSet->field->attributes()->tooltipText ? trans((string)$fieldSet->field->attributes()->tooltipText) : '';
        }

        return [
            'name' => trans((string)$attr->title),
            'xml_field_name' => (string)$attr->name,
            'title_trans_key' => (string)$attr->title,
            'mdm_id' => (int)$attr->mdm,
            'description' => $desc,
            'tab' => $tab,
            'path' => $tabPathForSave,
            'show_attr' => $show_attr
        ];
    }

    /**
     * Function to return Saf Fields
     *
     * @param $tab
     * @param $block
     * @param $fieldSet
     * @param $safFields
     * @param $step
     * @param $typeBlock
     * @param bool $isField
     * @return mixed
     */
    public static function returnSafFields($tab, $block, $fieldSet, $safFields, $step, $typeBlock = null, $isField = false)
    {
        if (isset($fieldSet->field) && count($fieldSet->field) > 1) {
            foreach ($fieldSet->field as $field) {
                $safFields = self::returnSafFields($tab, $block, $field, $safFields, 'block', null, true);
            }
        } else {
            $data = self::returnAttributes($tab, $block, $fieldSet, $step, $typeBlock, $isField);
            $safFields[(string)$data['attr']->id] = self::setSafFieldsArray($data['attr'], $fieldSet, $data['showAttr'], $data['tabPath'], $data['tabPathForSave'], $isField, $data['mdm']);
        }

        return $safFields;
    }

    /**
     * Function to generate and return group path
     *
     * @param $tab
     * @param $block
     * @param $fieldSet
     * @param $step
     * @param $typeBlock
     * @param $isField
     * @return array
     */
    public static function returnAttributes($tab, $block, $fieldSet, $step, $typeBlock, $isField = false)
    {
        $mdm = null;
        $attr = (new self)->returnAttr($isField, $fieldSet);
        $showAttr = false;

        if ((int)$attr->mdm > 0) {
            $dataModelALLData = DataModel::allData();
            if (isset($dataModelALLData[(string)$attr->mdm])) {
                $mdm = (object)$dataModelALLData[(string)$attr->mdm];
                $showAttr = ($mdm && !is_null($mdm->reference)) ? true : false;
            }
        }

        switch ($step) {
            case 'block':
            case 'typeBlock':
                $tabPath = trans((string)$tab->attributes()->name) . ' / ' . trans((string)$block->attributes()->name);
                $tabPathForSave = (string)$tab->attributes()->name . ' / ' . (string)$block->attributes()->name;
                break;
            case 'subBlock':
                $tabPath = trans((string)$tab->attributes()->name) . ' / ' . trans((string)$block->attributes()->name) . ' / ' . trans((string)$block->subBlock->attributes()->name);
                $tabPathForSave = (string)$tab->attributes()->name . ' / ' . (string)$block->attributes()->name . ' / ' . (string)$block->subBlock->attributes()->name;
                break;
            case 'tabBlock':
                $tabPath = empty($tab->block->attributes()) ? trans((string)$tab->attributes()->name) : trans((string)$tab->attributes()->name) . ' / ' . trans((string)$tab->block->attributes()->name);
                $tabPathForSave = empty($tab->block->attributes()) ? (string)$tab->attributes()->name : (string)$tab->attributes()->name . ' / ' . (string)$tab->block->attributes()->name;
                break;
        }

        return [
            'mdm' => $mdm,
            'attr' => $attr,
            'showAttr' => $showAttr,
            'tabPath' => $tabPath,
            'tabPathForSave' => $tabPathForSave
        ];
    }

    /**
     * Function to attributes array
     *
     * @param $isField
     * @param $fieldSet
     * @return mixed
     */
    private function returnAttr($isField, $fieldSet)
    {
        if ($isField) {
            return $fieldSet->attributes();
        } elseif (count($fieldSet->field) == 1) {
            return $fieldSet->field->attributes();
        } else {
            return $fieldSet->field[0]->attributes();
        }
    }
}
