<?php

namespace App\Models;

use App\Models\Role\Role;
use App\Models\UserRoles\UserRoles;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Support\Facades\Auth;

/**
 * Class BaseModel
 * @package App\Models
 */
class BaseModel extends EloquentModel
{
    /**
     * @var string
     */
    const STATUS_ACTIVE = '1';
    const STATUS_INACTIVE = '2';
    const STATUS_DELETED = '0';

    /**
     * @var string
     */
    const LANGUAGE_CODE_EN = '1';
    const LANGUAGE_CODE_RU = '133';
    const LANGUAGE_CODE_TJ = '10';

    /**
     * @var array
     */
    const ROLE_STATUSES = [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_DELETED];

    /**
     * @var string
     */
    const TRUE = '1';
    const FALSE = '0';

    /**
     * @var string
     */
    protected $defaultColumns = '*';
    protected $hasAdminInfo = false;

    /**
     * @var string
     */
    const ENTITY_TYPE_TAX_ID = 'tax_id';
    const ENTITY_TYPE_USER_ID = 'id_card';
    const ENTITY_TYPE_FOREIGN_CITIZEN = 'f_citizen';

    /**
     * @var string
     */
    const VIEW_MODE_VIEW = 'view';
    const VIEW_MODE_EDIT = 'edit';
    const VIEW_MODE_ADD = 'add';
    const VIEW_MODE_CREATE = 'create';

    /**
     * @param $query
     * @param null $columns
     * @return mixed
     */
    public function scopeBase($query, $columns = null)
    {
        if ($columns === null) {
            $columns = $this->defaultColumns;
        }

        return $query->get($columns);
    }

    /**
     * @param $query
     * @param string $field
     * @return mixed
     */
    public function scopeOrderByAsc($query, $field = 'id')
    {
        return $query->orderBy($field, 'asc');
    }

    /**
     * @param $query
     * @param string $field
     * @return mixed
     */
    public function scopeOrderByDesc($query, $field = 'id')
    {
        return $query->orderBy($field, 'desc');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeJoinMl($query)
    {
        $params = func_get_args();
        $table = $this->getTable();
        $tableMl = isset($params[1]['t_ml']) ? $params[1]['t_ml'] : $table . '_ml';

        return $query->join($tableMl, function ($query) use ($table, $params, $tableMl) {
            $foreignKey = isset($params[1]['f_k']) ? $params[1]['f_k'] : $table . '_id';
            $query->on($tableMl . '.' . $foreignKey, '=', $table . '.id')->where($tableMl . '.lng_id', '=', cLng('id'));

            /*$status = isset($params[1]['status']) ? $params[1]['status'] : true;
            $active = isset($params[1]['active']) ? $params[1]['active'] : true;
            if ($status) {
                if ($active) {
                    $query->where('ml.show_status', '=', self::STATUS_ACTIVE);
                } else {
                    $query->where('ml.show_status', '!=', self::STATUS_DELETED);
                }
            }*/
        });
    }

    /**
     * Function to check table is already joined
     *
     * @param $query
     * @param $table
     * @return bool
     */
    public static function isJoined($query, $table)
    {
        $joins = $query->getQuery()->joins;
        if ($joins == null) {
            return false;
        }

        foreach ($joins as $join) {
            if ($join->table == $table) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeCLng($query)
    {
        return $query->where($this->getTable() . '.lng_id', cLng('id'));
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where($this->getTable() . '.show_status', self::STATUS_ACTIVE);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeInactive($query)
    {
        return $query->where($this->getTable() . '.show_status', self::STATUS_INACTIVE);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeNotDeleted($query)
    {
        return $query->where($this->getTable() . '.show_status', '!=', self::STATUS_DELETED);
    }

    /**
     * Function to check user has role for get table row
     *
     * @param $query
     * @param string $column
     * @param string $column2
     * @return mixed
     */
    public function scopeCheckUserHasRole($query, $column = 'company_tax_id', $column2 = 'user_id')
    {
        $userShowOthersData = Auth::user()->userShowOthersData();

        // Sw Admin
        if (Auth::user()->isSwAdmin() && Auth()->user()->isAuthorizedSwAdmin()) {
            if (!$userShowOthersData) {
                return $query->where($this->getTable() . '.' . $column2, Auth::id());
            }
        }

        // Company
        if (Auth::user()->isCompanyType()) {

            if (Auth::user()->isLocalAdmin() || $userShowOthersData) {
                return $query->where($this->getTable() . '.' . $column, Auth::user()->companyTaxId());
            } else {

                $column = 'user_id';

                $query->where($this->getTable() . '.' . $column, Auth::id())->where($this->getTable() . '.company_tax_id', Auth::user()->companyTaxId());

                return $query;
            }
        }

        // Natural Person
        if (Auth::user()->isNaturalPerson()) {

            if (Auth::user()->isAuthorizedNaturalPerson()) {
                if (!$userShowOthersData) {
                    $column2 = 'creator_user_id';

                    return $query->where($this->getTable() . '.' . $column, '0')->where($this->getTable() . '.' . $column2, Auth::id());
                }
            }

            return $query->where($this->getTable() . '.' . $column, '0')->where($this->getTable() . '.' . $column2, Auth::user()->getCreatorUser()->id);
        }

        return $query;
    }

    /**
     * Function to get user roles of Auth user type
     *
     * @param $query
     * @return mixed
     */
    public function scopeUserRolesByType($query)
    {
        // Sw Admin
        if (Auth::user()->isSwAdmin()) {

            $query->where('role_type', Role::ROLE_TYPE_SW_ADMIN)->where('user_id', '!=', Auth::id());

            if (Auth::user()->isAuthorizedSwAdmin() && !Auth::user()->userShowOthersData()) {
                $query->where(['user_roles.authorized_by_id' => Auth::id()]);
            }
        }

        // Company
        if (Auth::user()->isCompanyType()) {

            $query->where('role_type', Role::ROLE_TYPE_COMPANY)->where('company_tax_id', Auth::user()->companyTaxId());

            if (Auth::user()->isLocalAdmin()) {
                $query->where(function ($q){
                    $q->whereIn('authorized_by_id_type', [UserRoles::AUTHORIZED_BY_USER_TYPE_COMPANY_LOCAL_ADMIN, UserRoles::AUTHORIZED_BY_USER_TYPE_COMPANY_EMPLOYEE])
                        ->orWhereNull('authorized_by_id_type')->whereNotNull('user_roles.authorized_by_admin_id');
                });
            }

            if (Auth::user()->isAuthorizedEmployeeForCompany()) {
                $query->where(['user_roles.authorized_by_id' => Auth::id()])
                    ->where('user_roles.authorized_by_id_type', UserRoles::AUTHORIZED_BY_USER_TYPE_COMPANY_EMPLOYEE);
            }
        }

        // Natural Person
        if (Auth::user()->isNaturalPerson()) {
            $query->where('role_type', Role::ROLE_TYPE_NATURAL_PERSON)->where(['user_roles.authorized_by_id' => Auth::id()]);
        }

        return $query;
    }

    /**
     * Function to check company_tax_id for users
     *
     * @param $query
     * @return mixed
     */
    public function scopeCheckUserCompanyTaxId($query)
    {
        return $query->where($this->getTable() . '.company_tax_id', Auth::user()->companyTaxId());
    }

    /**
     * @param $query
     * @param string $mode
     * @return mixed
     */
    public function scopeOrdered($query, $mode = 'ASC')
    {
        $table = $this->getTable();
        return $query->orderBy($table . '.sort_order', $mode)->orderBy($table . '.id', 'DESC');
    }

    /**
     * @param $query
     * @param $field
     * @param $value
     * @return mixed
     */
    public function scopeLike($query, $field, $value)
    {
        return $query->where($this->getTable() . '.' . $field, 'ILIKE', "%{$value}%");
    }

    /**
     * @param $query
     * @param $field
     * @param $value
     * @return mixed
     */
    public function scopeOrLike($query, $field, $value)
    {
        return $query->orWhere($this->getTable() . '.' . $field, 'ILIKE', "%{$value}%");
    }

    /**
     * @param $query
     * @param null $perPage
     * @param null $columns
     * @return mixed
     */
    public function scopePaginateBase($query, $perPage = null, $columns = null)
    {
        $paginator = $query->getQuery()->getConnection()->getPaginator();

        $total = $query->getQuery()->getPaginationCount();

        // Once we have the paginator we need to set the limit and offset values for
        // the query so we can get the properly paginated items. Once we have an
        // array of items we can create the paginator instances for the items.
        $page = $paginator->getCurrentPage($total);

        $query->getQuery()->forPage($page, $perPage);

        if ($columns === null) {
            $columns = $this->defaultColumns == null ? '*' : $this->defaultColumns;
        }

        return $paginator->make($query->get($columns)->all(), $total, $perPage);

        $perPage = $perPage ?: 10;

        $paginator = $query->getQuery()->getConnection()->getPaginator();

        $total = $query->getQuery()->getPaginationCount();

        // Once we have the paginator we need to set the limit and offset values for
        // the query so we can get the properly paginated items. Once we have an
        // array of items we can create the paginator instances for the items.
        $page = $paginator->getCurrentPage($total);

        $query->getQuery()->forPage($page, $perPage);


        return $paginator->make($this->get($columns)->all(), $total, $perPage);
    }

    /**
     * Boot
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if ($model->hasAdminInfo === true) {
                $model->created_admin_id = Auth::check() ? Auth::user()->id : 0;
                $model->created_admin_ip = request()->ip();
            }
        });

        static::updating(function ($model) {
            if ($model->hasAdminInfo === true) {

                if ($model->show_status == '1') {
                    $model->updated_admin_id = Auth::check() ? Auth::user()->id : $model->updated_admin_id;
                    $model->updated_admin_ip = request()->ip();
                } elseif ($model->show_status == '0') {
                    $model->deleted_admin_id = Auth::check() ? Auth::user()->id : $model->deleted_admin_id;
                    $model->updated_admin_ip = request()->ip();
                }
            }
        });
    }

    /**
     * @param $query
     * @param array $value
     * @return mixed
     */
    public function scopeExclude($query, $value = array())
    {
        return $query->select(array_diff($this->columns, (array)$value));
    }
}