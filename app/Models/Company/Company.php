<?php

namespace App\Models\Company;

use App\Models\BaseModel;
use App\Models\Transactions\Transactions;
use App\Models\UserRoles\UserRoles;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\DB;

/**
 * Class Company
 * @package App\Models\Company
 */
class Company extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'company';

    /**
     * @var array
     */
    protected $fillable = [
        'tax_id',
        'name',
        'name_en',
        'name_ru',
        'city_town',
        'company_type',
        'executive_ssn',
        'street1',
        'house',
        'apt',
        'street2',
        'city_town',
        'postcode',
        'community',
        'phone_number',
        'email',
        'address',
        'legal_address',
    ];

    /**
     * Function to when get name check isset short_name if isset get short_name
     *
     * @param $name
     * @return mixed
     */
    /*  public function getNameAttribute($name)
      {
          if (isset($this->attributes['tax_id'])) {
              $agency = Agency::where('tax_id', $this->attributes['tax_id'])->joinMl()->active()->first();
              if (!is_null($agency)) {
                  return $agency->shortName();
              }
          }

          return $name;
      }*/


    /**
     * Function to relate with user role
     *
     * @return HasOne
     */
    public function userRole()
    {
        $now = currentDate();
        return $this->hasOne(UserRoles::class, 'company_id')->where('user_roles.role_status', Company::STATUS_ACTIVE)
            ->where(function ($q) use ($now) {
                $q->orWhereDate('available_to', '>=', $now)->orWhereNull('available_to');
            })
            ->where(function ($q) use ($now) {
                $q->orWhereDate('available_at', '<=', $now)->orWhereNull('available_at');
            });
    }

    /**
     * Function to create Company or Update Company Data
     *
     * @param array $data
     * @return Company
     */
    public static function store(array $data)
    {
        return Company::updateOrCreate(['tax_id' => $data['tax_id']], $data);
    }

    /**
     * Function to get company balance
     *
     * @param $taxId
     * @return string
     */
    public function balance($taxId)
    {
        return optional(Transactions::select('id', 'total_balance')->where('company_tax_id', $taxId)->orderByDesc()->first())->total_balance;
    }

    /**
     * Function to get Company info
     *
     * @param $companyTaxId
     * @return Company
     */
    public static function getCompanyByTaxID($companyTaxId)
    {
        return Company::where('tax_id', $companyTaxId)->first();
    }

    /**
     * Function to get company name if its now is agency get agency name
     *
     * @param $companyTaxId
     * @return Company
     */
    public static function getAuthorizedCompanyInfo($companyTaxId)
    {
        return Company::select(DB::raw("COALESCE(agency_ml.name, company.name) as company_name"), 'company.tax_id')->where('company.tax_id', $companyTaxId)
            ->leftJoin('agency', function ($q) {
                $q->on('agency.tax_id', '=', 'company.tax_id')->where('agency.show_status', UserRoles::STATUS_ACTIVE);
            })
            ->leftJoin('agency_ml', function ($q) {
                $q->on('agency_ml.agency_id', '=', 'agency.id')->where('agency_ml.lng_id', cLng('id'));
            })
            ->first();
    }
}
