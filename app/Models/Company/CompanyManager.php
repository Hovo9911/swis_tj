<?php

namespace App\Models\Company;

/**
 * Class AgencyManager
 * @package App\Models\Agency
 */
class CompanyManager
{
    /**
     * Function to create Company or Update Company Data
     *
     * @param array $companyInfo
     */
    public function updateInfoOrCreate(array $companyInfo)
    {
        $companyData = [
            'tax_id' => $companyInfo['tax_id'],
            'name' => $companyInfo['name'],
            'address' => $companyInfo['address'] ?? null,
            'legal_address' => $companyInfo['address'] ?? null,
            'community' => $companyInfo['community'] ?? '-',
        ];

        Company::updateOrCreate(['tax_id' => $companyInfo['tax_id']], $companyData);
    }
}
