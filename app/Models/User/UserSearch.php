<?php

namespace App\Models\User;

use App\Http\Controllers\Core\DataTableSearch;
use App\Models\Agency\Agency;
use App\Models\Broker\Broker;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\Role\Role;
use App\Models\SingleApplication\SingleApplication;
use App\Models\UserRoles\UserRoles;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class UserSearch
 * @package App\Models\User
 */
class UserSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->rowsCountAll();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->rowsCountAll();
    }

    /**
     * @return int
     */
    private function rowsCountAll()
    {
        $query = $this->constructQuery();
        $result = DB::select("SELECT count(*) as cnt FROM ({$query->toSql()}) as temp", $query->getBindings());

        return $result[0]->cnt;
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        return $query->get();
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($this->orderByCol, $this->orderByType ?? 'desc');
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $query = User::select('users.id', 'users.ssn', 'users.first_name', 'users.last_name', 'users.username', 'users.passport', 'users.email', 'users.phone_number', 'users.show_status', 'users.is_customs_operator', DB::raw("registered_user.first_name || ' ' || registered_user.last_name AS registered_username"), 'users.registered_at')->distinct()
            ->join('user_roles', 'users.id', '=', 'user_roles.user_id')
            ->leftJoin('users as registered_user', 'registered_user.id', '=', 'users.registered_by_id')
            ->notDeleted();

        if (isset($this->searchData['id'])) {
            $query->where(DB::raw('users.id::varchar'), $this->searchData['id']);
        }

        if (isset($this->searchData['ssn'])) {
            $query->where('users.ssn', 'ILIKE', '%' . strtolower(trim($this->searchData['ssn'])) . '%');
        }

        if (isset($this->searchData['first_name'])) {
            $query->where('users.first_name', 'ILIKE', '%' . strtolower(trim($this->searchData['first_name'])) . '%');
        }

        if (isset($this->searchData['last_name'])) {
            $query->where('users.last_name', 'ILIKE', '%' . strtolower(trim($this->searchData['last_name'])) . '%');
        }

        if (isset($this->searchData['username'])) {
            $query->where('users.username', 'ILIKE', '%' . strtolower(trim($this->searchData['username'])) . '%');
        }

        if (isset($this->searchData['passport'])) {
            $query->where('users.passport', 'ILIKE', '%' . strtolower(trim($this->searchData['passport'])) . '%');
        }

        if (isset($this->searchData['registered_username'])) {
            $register_username = $this->searchData['registered_username'];
            $query->whereRaw("concat(registered_user.first_name, ' ', registered_user.last_name) ILIKE '%$register_username%' ");
        }

        if (isset($this->searchData['show_status'])) {
            $query->where('users.show_status', $this->searchData['show_status']);
        }

        if (isset($this->searchData['ref_custom_body'])) {
            $value = $this->searchData['ref_custom_body'];
            $query->whereRaw('(users.ref_custom_body)::jsonb @> \'"' . $value . '"\'');
        }

        if (isset($this->searchData['company_tax_id']) || isset($this->searchData['company_name'])) {

            if (!empty($this->searchData['company_tax_id'])) {
                $query->where('user_roles.company_tax_id', $this->searchData['company_tax_id']);
            }

            if (!empty($this->searchData['company_name'])) {

                $query->join('company', 'company.id', '=', 'user_roles.company_id');

                $query->where('company.name', 'ILIKE', '%' . strtolower(trim($this->searchData['company_name'])) . '%');
            }

            $query->where('user_roles.role_status', User::STATUS_ACTIVE);
        }

        if (isset($this->searchData['sub_division_id'])) {

            $subDivisionCode = optional(getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS, $this->searchData['sub_division_id'], false, ['code']))->code;

            $subDivisionsIds = !is_null($subDivisionCode) ? DB::table(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS)->where('code', $subDivisionCode)->pluck('id')->all() : [];

            $query->where(['user_roles.attribute_field_id' => SingleApplication::SAF_AGENCY_SUBDIVISION_ID]);
            $query->whereIn('user_roles.attribute_value', $subDivisionsIds);

        }

        if (isset($this->searchData['authorizer_ssn'])) {

            $userId = User::where('ssn', $this->searchData['authorizer_ssn'])->pluck('id')->first();

            $query->where('user_roles.authorized_by_id', $userId);
        }

        if (isset($this->searchData['authorizer_first_name'])) {

            $authorizerUsersId = User::where('first_name', 'ILIKE', '%' . strtolower(trim($this->searchData['authorizer_first_name'])) . '%')->pluck('id')->all();

            if (count($authorizerUsersId)) {
                $query->whereIn('user_roles.authorized_by_id', $authorizerUsersId);
            } else {
                $query->where('user_roles.id', 0);
            }
        }

        if (isset($this->searchData['authorizer_last_name'])) {

            $authorizerUsersId = User::where('last_name', 'ILIKE', '%' . strtolower(trim($this->searchData['authorizer_last_name'])) . '%')->pluck('id')->all();

            if (count($authorizerUsersId)) {
                $query->whereIn('user_roles.authorized_by_id', $authorizerUsersId);
            } else {
                $query->where('user_roles.id', 0);
            }
        }

        if (isset($this->searchData['menu'])) {
            $query->where('user_roles.menu_id', $this->searchData['menu']);
        }

        if (isset($this->searchData['role'])) {
            $query->where('user_roles.role_id', $this->searchData['role']);
        }

        if (isset($this->searchData['registered_at_start'])) {
            $query->where('users.registered_at', '>=', Carbon::parse($this->searchData['registered_at_start'])->startOfDay());
        }

        if (isset($this->searchData['registered_at_end'])) {
            $query->where('users.registered_at', '<=', Carbon::parse($this->searchData['registered_at_end'])->endOfDay());
        }

        if (isset($this->searchData['is_head'])) {
            $query->where("user_roles.role_id", Role::HEAD_OF_COMPANY);
        }

        if (isset($this->searchData['user_type'])) {

            switch ($this->searchData['user_type']) {

                case User::USER_TYPE_SW_ADMIN:
                    $query->where('user_roles.role_id', Role::SW_ADMIN);
                    break;

                case User::USER_TYPE_BROKER:
                    $brokersTaxId = Broker::active()->pluck('tax_id')->all();
                    $query->whereIn('user_roles.company_tax_id', $brokersTaxId);
                    break;

                case User::USER_TYPE_AGENCY:
                    $agenciesTaxId = Agency::active()->pluck('tax_id')->all();
                    $query->whereIn('user_roles.company_tax_id', $agenciesTaxId);
                    break;

                case User::USER_TYPE_COMPANY:
                    $agenciesTaxId = Agency::active()->pluck('tax_id')->all();
                    $brokersTaxId = Broker::active()->pluck('tax_id')->all();
                    $agenciesAndBrokersTaxId = array_unique(array_merge($agenciesTaxId, $brokersTaxId));

                    $query->whereNotIn('user_roles.company_tax_id', $agenciesAndBrokersTaxId)->whereNotNull('user_roles.company_id')->where('user_roles.role_id', '!=', Role::HEAD_OF_COMPANY);
                    break;

                case User::USER_TYPE_COMPANY_HEADS:
                    $query->whereNotNull('user_roles.company_id')->where('user_roles.role_id', Role::HEAD_OF_COMPANY);
                    break;

                case User::USER_TYPE_NATURAL_PERSON:
                    $onlyNaturalPersons = UserRoles::where('role_id', '!=', Role::NATURAL_PERSON)->groupBy('user_id')->pluck('user_id')->all();

                    $query->whereNotIn('user_id', $onlyNaturalPersons);
                    break;

                case User::USER_TYPE_CUSTOMS_OPERATOR:
                    $query->where('users.is_customs_operator', User::TRUE);
                    break;
            }

        }

        if (Auth::user()->isAuthorizedSwAdmin() && !Auth::user()->userShowOthersData()) {
            $query->where(['user_roles.authorized_by_id' => Auth::id()])
                ->where('company_tax_id', '=', '0')
                ->whereNotIn('user_roles.role_id', [Role::NATURAL_PERSON]);
        }

        $query->where(function ($q) {
            $q->whereNotIn('users.ssn', User::SW_ADMIN_SSN)->orWhere('users.is_customs_operator', '1');
        });

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}
