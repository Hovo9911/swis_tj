<?php

namespace App\Models\User;

use App\Facades\Mailer;
use App\Models\Broker\Broker;
use App\Models\Company\Company;
use App\Models\Laboratory\Laboratory;
use App\Models\Languages\Language;
use App\Models\Menu\Menu;
use App\Models\Notifications\NotificationsManager;
use App\Models\Role\Role;
use App\Models\Transactions\Transactions;
use App\Models\UserCompanies\UserCompanies;
use App\Models\UserPasswords\UserPasswords;
use App\Models\UserRoles\UserRoles;
use App\Models\UserRoles\UserRolesManager;
use App\Models\UserSession\UserSessionManager;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class UserManager
 * @package App\Models\User
 */
class UserManager
{
    /**
     * Function to store data to DB
     *
     * @param array $data
     * @return User
     */
    public function store(array $data)
    {
        return DB::transaction(function () use ($data) {

            // User
            $user = $this->createNewUser($data);

            // Companies
            if (isset($data['company'])) {
                $this->setUserCompanies($data['company'], $user->id);
            }

            // Email
            if (isset($data['send_for_confirmation'])) {
                $this->sendRegisterEmail($user, $data['lng_id']);
            }

            return $user;
        });
    }

    /**
     * Function to store Customs operator
     *
     * @param array $data
     * @return User
     */
    public function storeCustomsOperator(array $data)
    {
        $data['registered_at'] = Carbon::now();
        $data['registered_by_id'] = Auth::id();
        $data['is_email_verified'] = User::TRUE;
        $data['is_phone_verified'] = User::TRUE;
        $data['is_customs_operator'] = User::TRUE;

        $user = new User($data);

        DB::transaction(function () use ($data, $user) {
            $user->save();

            User::where('id', $user->id)->update(['password' => bcrypt($data['password'])]);
            UserRolesManager::addCustomsOperatorRole($user->id);
        });

        return $user;
    }

    /**
     * Function to update data
     *
     * @param array $data
     * @param int $id
     */
    public function update(array $data, int $id)
    {
        $user = User::where('id', $id)->firstOrFail();

        DB::transaction(function () use ($user, $data) {

            if (count($data['company'])) {
                $this->setUserCompanies($data['company'], $user->id);
            }

            if (isset($data['email']) && $data['email'] != $user->email) {
                $data['is_email_verified'] = User::FALSE;
                $this->changeEmail($data['email'], $user);
            }

            if (isset($data['show_status']) && $user->show_status != $data['show_status']) {
                $data['status_changed_by_user_id'] = Auth::id();
            }

            if (isset($data['password'])) {
                User::where('id', $user->id)->update(['password' => bcrypt($data['password'])]);
                unset($data['password']);
            }

            $user->update($data);

            $this->sendNotification($user);
        });
    }

    /**
     * Function to update customs operator
     *
     * @param array $data
     * @param int $id
     */
    public function updateCustomsOperator(array $data, int $id)
    {
        $user = User::where('id', $id)->firstOrFail();

        if (!isset($data['ref_custom_body'])) {
            $data['ref_custom_body'] = [];
        }

        if (isset($data['show_status']) && $user->show_status != $data['show_status']) {
            $data['status_changed_by_user_id'] = Auth::id();
        }

        DB::transaction(function () use ($user, $data) {

            if (isset($data['password'])) {
                User::where('id', $user->id)->update(['password' => bcrypt($data['password'])]);
            }

            $user->update($data);
        });
    }

    /**
     * Function to store user profile settings (if user change email send new mail verification)
     *
     * @param array $data
     */
    public function storeProfileSettings(array $data)
    {
        $user = Auth::user();
        $data['email_notification'] = $data['email_notification'] ?? 0;

        //
        if (!isset($data['sms_notification']) && config('swis.sms_send')) {
            $data['sms_notification'] = 0;
        }

        //
        DB::transaction(function () use ($user, $data) {

            if ($user->email != $data['email']) {
                $this->profileSettingsChangeEmail($data['email']);
                $data['is_email_verified'] = User::FALSE;
            }

            //
            if (isset($data['password'])) {
                $this->profileSettingsChangePassword($user, $data['email'], $data['password']);
            }

            $user->update($data);

            $this->sendNotification($user);
        });
    }

    /**
     * Function to add new user and check his transactions
     *
     * @param $userData
     * @return User
     */
    private function createNewUser($userData)
    {
        $userCreateData = [
            'first_name' => $userData['first_name'],
            'last_name' => $userData['last_name'],
            'patronymic_name' => $userData['patronymic_name'] ?? null,
            'ssn' => $userData['ssn'],
            'genus' => $userData['genus'] ?? null,
            'email' => $userData['email'] ?? null,
            'address' => $userData['address'] ?? null,
            'birth_date' => $userData['birth_date'] ?? null,
            'lng_id' => $userData['lng_id'] ?? '1',

            'english_first_name' => $userData['english_first_name'] ?? null,
            'english_last_name' => $userData['english_last_name'] ?? null,
            'english_patronymic_name' => $userData['english_patronymic_name'] ?? null,

            'community' => $userData['community'] ?? '-',
            'username' => $userData['username'] ?? null,
            'phone_number' => isset($userData['phone_number']) ? trim($userData['phone_number']) : null,
            'login' => '-',

            'passport_type' => $userData['passport_type'] ?? 0,
            'passport_issued_by' => $userData['passport_issued_by'] ?? null,
            'passport' => $userData['passport'] ?? null,
            'passport_issue_date' => $userData['passport_issue_date'] ?? null,
            'passport_validity_date' => $userData['passport_validity_date'] ?? null,
            'other_passports' => $userData['other_passports'] ?? null,

            'is_dead' => $userData['is_dead'] ?? null,
            'death_date' => $userData['death_date'] ?? null,

            'allowed_from_ips' => '',
            'lastlogin_at' => Carbon::now(),
            'lastlogin_ip' => request()->ip(),

            'registration_address' => isset($userData['registration_address']) ? $userData['registration_address'] : '-',
            'is_email_verified' => User::TRUE,
            'is_phone_verified' => User::FALSE,
            'sms_notification' => User::FALSE,
            'email_notification' => User::TRUE,

            'registered_at' => Carbon::now(),
            'registered_by_id' => Auth::id(),
            'status_changed_by_user_id' => 0,
            'added_manually' => $userData['added_manually'] ?? 0,
        ];

        $user = User::create($userCreateData);

        // Default Role
        UserRolesManager::addDefaultRole($user->id);

        if (isset($userData['password'])) {
            User::where('id', $user->id)->update(['password' => bcrypt($userData['password'])]);
        }

        //
        Transactions::createTransactionsForNewUsers($user->ssn, $user->id);

        return $user;
    }

    /**
     * Function to set user companies (add or delete companies, SW ADMIN CREATES) TJ
     *
     * @param $companies
     * @param $user_id
     */
    private function setUserCompanies($companies, $user_id)
    {
        $taxIds = [];

        foreach ($companies as $value) {

            $tax_id = $value['tax_id'];

            if (!is_null($tax_id)) {

                $company = Company::where('tax_id', $tax_id)->first();

                if (!is_null($company)) {

                    $taxIds[] = $tax_id;

                    $headOfCompany = UserRoles::where(['company_tax_id' => $tax_id, 'role_id' => Role::HEAD_OF_COMPANY, 'role_status' => Role::STATUS_ACTIVE])->whereNull('authorized_by_id')->first();

                    if (!is_null($headOfCompany) && $headOfCompany->user_id == $user_id) {
                        continue;
                    }

                    UserCompanies::updateOrCreate([
                        'user_id' => $user_id,
                        'company_id' => $company->id], [
                        'user_id' => $user_id,
                        'company_id' => $company->id
                    ]);

                    if (isset($value['is_head'])) {

                        // close all roles
                        if (!is_null($headOfCompany)) {
                            UserRoles::where(['company_tax_id' => $tax_id, 'user_id' => $headOfCompany->user_id])->update(['role_status' => Role::STATUS_INACTIVE]);
                        }

                        UserRoles::create([
                            'user_id' => $user_id,
                            'role_id' => Role::HEAD_OF_COMPANY,
                            'company_id' => $company->id,
                            'company_tax_id' => $tax_id,
                            'broker_id' => Broker::where(['tax_id' => $tax_id])->pluck('id')->first(),
                            'laboratory_id' => Laboratory::where(['tax_id' => $tax_id])->pluck('id')->first(),
                            'user_status' => Role::STATUS_ACTIVE,
                            'show_only_self' => User::FALSE,
                            'is_local_admin' => User::FALSE,
                            'role_status' => Role::STATUS_ACTIVE,
                            'available_at' => currentDate(),
                            'authorized_by_admin_id' => Auth::id(),
                            'role_type' => Role::ROLE_TYPE_COMPANY
                        ]);

                    } else {

                        $userHasCompanyRoles = UserRoles::select('id')->where(['user_id' => $user_id, 'company_id' => $company->id])->activeRole()->first();

                        if (is_null($userHasCompanyRoles)) {

                            $crudRoleMenu = [
                                Menu::getMenuIdByName(Menu::SINGLE_APPLICATION_MENU_NAME) => Role::CRUD_ALL_FUNCTIONALITY,
                                Menu::getMenuIdByName(Menu::DOCUMENT_CLOUD_MENU_NAME) => Role::CRUD_ALL_FUNCTIONALITY,
                            ];

                            foreach ($crudRoleMenu as $menu_id => $role) {
                                UserRoles::create(
                                    [
                                        'user_id' => $user_id,
                                        'role_id' => $role,
                                        'menu_id' => $menu_id,
                                        'company_id' => $company->id,
                                        'company_tax_id' => $tax_id,
                                        'user_status' => Role::STATUS_ACTIVE,
                                        'show_only_self' => User::TRUE,
                                        'is_local_admin' => User::FALSE,
                                        'role_status' => Role::STATUS_ACTIVE,
                                        'available_at' => currentDate(),
//                                    :( 'authorized_by_id' => !is_null($headOfCompany) ? $headOfCompany->user_id : null,
                                        'authorized_by_id' => Auth::id(),
                                        'authorized_by_admin_id' => Auth::id(),
                                        'role_type' => Role::ROLE_TYPE_COMPANY
                                    ]);
                            }

                            //
                            $notificationData = [
                                'authorized_user' => $user_id,
                                'company_tax_id' => $tax_id,
                                'modules' => array_keys($crudRoleMenu)
                            ];

                            $notificationsManager = new NotificationsManager();
                            $notificationsManager->userAuthorizationNotification($notificationData);
                        }
                    }
                }
            }
        }

        UserRoles::whereNotIn('company_tax_id', $taxIds)->activeRole()->whereNotNull('company_tax_id')->where('company_tax_id', '!=', User::FALSE)->where('user_id', $user_id)->update(['role_status' => Role::STATUS_INACTIVE]);
    }

    /**
     * Function to email send again
     *
     * @param $lngId
     * @param $userId
     * @return void
     */
    public function emailSendAgain($lngId, $userId)
    {
        $user = User::select('email', 'first_name', 'last_name', 'is_email_verified', 'password', 'id')->where('id', $userId)->first();

        if ($user->email) {

            $token = uniqid() . '' . str_random(12) . time();

            if (!is_null($user->password)) {

                UserVerify::create([
                    'user_id' => $user->id,
                    'type' => User::VERIFY_TYPE_EMAIL,
                    'token' => $token
                ]);

                $userName = $user->name();
                $mail = [
                    'to' => $user->email,
                    'body' => view('mailer.verification-code', compact('token', 'userName')),
                ];

            } else {

                UserVerify::create([
                    'user_id' => $user->id,
                    'type' => User::VERIFY_TYPE_PASSWORD,
                    'token' => $token
                ]);

                $lngCode = 'en';
                if ($lngId) {
                    $lngCode = Language::where('id', $lngId)->pluck('code')->first();
                }

                $userName = $user->name();

                $mail = [
                    'to' => $user->email,
                    'userName' => $user->name(),
                    'body' => view('mailer.send-password', compact('token', 'lngCode', 'userName')),
                ];
            }

            Mailer::send($mail);
        }
    }

    /**
     * Function to send register email to user
     *
     * @param $user
     * @param $lngId
     */
    private function sendRegisterEmail($user, $lngId)
    {
        $token = uniqid() . '' . str_random(12) . '' . time();

        UserVerify::create([
            'user_id' => $user->id,
            'type' => User::VERIFY_TYPE_PASSWORD,
            'token' => $token
        ]);

        $languageCode = Language::where('id', $lngId)->pluck('code')->first();

        $lngCode = $languageCode ?: 'en';
        $userName = $user->name();

        $data = [
            'subject' => trans("swis.user.register.password_type.subject"),
            'to' => $user->email,
            'body' => view('mailer.send-password', compact('token', 'lngCode', 'userName')),
        ];

        Mailer::send($data);
    }

    /**
     * Function to if user change current email send new email message
     *
     * @param $newEmail
     * @param $user
     */
    private function changeEmail($newEmail, $user)
    {
        $token = uniqid() . '' . str_random(12) . '' . time();
        $userName = $user->name();

        if (is_null($user->password)) {

            UserVerify::create([
                'user_id' => $user->id,
                'type' => User::VERIFY_TYPE_PASSWORD,
                'token' => $token
            ]);

            $lngCode = 'en';

            $mail = [
                'subject' => trans("swis.user.register.password_type.subject"),
                'to' => $newEmail,
                'body' => view('mailer.send-password', compact('token', 'lngCode', 'userName')),
            ];

        } else {

            $oldUser = true;

            UserVerify::create([
                'user_id' => $user->id,
                'type' => User::VERIFY_TYPE_EMAIL,
                'token' => $token
            ]);

            $mail = [
                'subject' => trans('swis.verify.mail.send_activation_code.subject'),
                'to' => $newEmail,
                'body' => view('mailer.verification-code', compact('token', 'userName', 'oldUser')),
            ];
        }

        Mailer::send($mail);
    }

    /**
     * Function to change email from profile settings
     *
     * @param $email
     */
    private function profileSettingsChangeEmail($email)
    {
        $token = uniqid() . '' . str_random(12) . time();
        $userName = Auth::user()->name();
        $oldUser = true;

        UserVerify::create([
            'user_id' => Auth::id(),
            'type' => 'email',
            'token' => $token
        ]);

        $mail = [
            'subject' => trans('swis.verify.mail.send_activation_code.subject'),
            'to' => $email,
            'body' => view('mailer.verification-code', compact('token', 'userName', 'oldUser')),
        ];

        Mailer::send($mail);
    }

    /**
     * Function to change password form profile settings
     *
     * @param $user
     * @param $email
     * @param $newPassword
     */
    private function profileSettingsChangePassword($user, $email, $newPassword)
    {
        DB::transaction(function () use ($user, $email, $newPassword) {

            $newPassword = bcrypt($newPassword);

            UserSessionManager::deleteAllWithoutMySession(Auth::user()->id, session()->getId());
            User::where('id', Auth::id())->update(['password' => $newPassword, 'password_changed_at' => now()]);
            UserPasswords::create(['user_id' => Auth::id(), 'password' => $newPassword]);

            // Notify user about password changing
            $notificationsManager = new NotificationsManager();
            $notificationsManager->passwordChangeNotification($user, $email);
        });
    }

    /**
     * Function to change password
     *
     * @param $newPassword
     */
    public function changePassword($newPassword)
    {
        DB::transaction(function () use ($newPassword) {

            $newPassword = bcrypt($newPassword);

            User::where('id', Auth::id())->update([
                'password' => $newPassword,
                'password_need_change' => User::FALSE,
                'password_changed_at' => now()
            ]);

            UserPasswords::create(['user_id' => Auth::id(), 'password' => $newPassword]);

        });
    }

    /**
     * Function to send notification
     *
     * @param $user
     */
    private function sendNotification($user)
    {
        if (count($user->getChanges())) {
            NotificationsManager::userInfoChangeNotification($user, $user->getChanges());
        }
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @param null $parentId
     * @return array
     */
    public function getLogData($id = null, $parentId = null)
    {
        if (!is_null($id)) {
            return User::where('id', $id)->first();
        }

        return [];
    }

}
