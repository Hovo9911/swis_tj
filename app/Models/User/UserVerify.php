<?php

namespace App\Models\User;

use App\Models\BaseModel;

/**
 * Class UserVerify
 * @package App\Models
 */
class UserVerify extends BaseModel
{
    /**
     * @var string
     */
    const VERIFY_TYPE_EMAIL = 'email';
    const VERIFY_TYPE_PHONE = 'phone';

    /**
     * @var string
     */
    public $table = 'users_verify';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id', 'type', 'token'
    ];
}
