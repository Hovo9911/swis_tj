<?php

namespace App\Models\User;

use App\Models\Agency\Agency;
use App\Models\BaseModel;
use App\Models\Broker\Broker;
use App\Models\Company\Company;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorDocumentsRoles\ConstructorDocumentsRoles;
use App\Models\ConstructorMapping\ConstructorMapping;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\Laboratory\Laboratory;
use App\Models\Menu\Menu;
use App\Models\Menu\MenuManager;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\ReferenceTable\ReferenceTableAgencies;
use App\Models\RegionalOffice\RegionalOffice;
use App\Models\RegionalOfficeConstructorDocuments\RegionalOfficeConstructorDocuments;
use App\Models\Reports\Reports;
use App\Models\Role\Role;
use App\Models\RoleMenus\RoleMenus;
use App\Models\RolesGroup\RolesGroup;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use App\Models\SingleApplicationSubApplicationStates\SingleApplicationSubApplicationStates;
use App\Models\Transactions\Transactions;
use App\Models\UserRoles\UserRoles;
use App\Models\UserRolesMenus\UserRolesMenus;
use App\Models\UserRolesMenus\UserRolesMenusManager;
use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Session\SessionManager;
use Illuminate\Session\Store;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/**
 * Class User
 * @package App
 */
class User extends BaseModel implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Notifiable;
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'patronymic_name',
        'english_first_name',
        'english_last_name',
        'english_patronymic_name',
        'genus',
        'birth_date',
        'community',
        'ssn',
        'email',
        'username',
        'passport_type',
        'passport',
        'passport_issue_date',
        'passport_validity_date',
        'passport_issued_by',
        'other_passports',
        'phone_number',
        'address',
        'city_village',
        'is_dead',
        'death_date',
        'registration_address',
        'post_address',
        'login',
        'allowed_from_ips',
        'registered_at',
        'registered_by_id',
        'status_changed_by_user_id',
        'lastlogin_at',
        'lastlogin_ip',
        'is_email_verified',
        'is_phone_verified',
        'email_notification',
        'sms_notification',
        'activated',
        'lng_id',
        'show_status',
        'last_activity',
        'is_customs_operator',
        'added_manually',
        'ref_custom_body',
        'additional_information'
    ];

    /**
     * @var array
     */
    const SW_ADMIN_SSN = [
        '2610460120',
        '7111450065',
    ];

    /**
     * @var string (User Types (used in user index page search part))
     */
    const USER_TYPE_SW_ADMIN = 'sw_admin';
    const USER_TYPE_BROKER = 'broker';
    const USER_TYPE_AGENCY = 'agency';
    const USER_TYPE_COMPANY = 'company';
    const USER_TYPE_COMPANY_HEADS = 'company_heads';
    const USER_TYPE_NATURAL_PERSON = 'natural_person';
    const USER_TYPE_CUSTOMS_OPERATOR = 'customs_operator';

    /**
     * @var array
     */
    const USER_TYPES = [
        self::USER_TYPE_SW_ADMIN,
        self::USER_TYPE_BROKER,
        self::USER_TYPE_AGENCY,
        self::USER_TYPE_COMPANY,
        self::USER_TYPE_COMPANY_HEADS,
        self::USER_TYPE_NATURAL_PERSON,
        self::USER_TYPE_CUSTOMS_OPERATOR,
    ];

    /**
     * @var string (User Types for global notifications)
     */
    const USER_TYPE_ALL_USERS = 'all_users';
    const SAF_SIDES = 'saf_sides';

    /**
     * @var array
     */
    const USER_TYPES_FOR_GLOBAL_NOTIFICATIONS = [
        self::USER_TYPE_ALL_USERS,
        self::SAF_SIDES,
        self::USER_TYPE_BROKER,
        self::USER_TYPE_AGENCY
    ];

    /**
     * @var string (User Types for notify about sub application expiration date)
     */
    const SAF_APPLICANT = 'saf_applicant';
    const SUB_APPLICATION_CURRENT_STATE_USERS = 'sub_application_current_state_users';
    const AGENCY_HEADS = 'agency_heads';

    /**
     * @var array
     */
    const USER_TYPES_FOR_NOTIFY_ABOUT_SUB_APPLICATION_EXP_DATE = [
        self::SAF_APPLICANT,
        self::SUB_APPLICATION_CURRENT_STATE_USERS,
        self::AGENCY_HEADS,
        self::USER_TYPE_SW_ADMIN
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    /**
     * @var array
     */
    const NO_NEED_CHECK_ACCESS_ROUTE_NAMES = [
        'changeRole',
        'userTypeInfo',
        'isBroker',
        'roleSet',
        'refData',
        'searchDocuments',
        'deleteFreeSumObligation'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'other_passports' => 'array',
        'ref_custom_body' => 'array',
    ];

    /**
     * @var string
     */
    const TYPE_COMPANY = 'company';
    const TYPE_USER = 'user';

    /**
     * @var string
     */
    const VERIFY_TYPE_EMAIL = 'email';
    const VERIFY_TYPE_PASSWORD = 'password';

    /**
     * @var string
     */
    private $authType;

    /**
     * Function to get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->authType;
    }

    /**
     * Function to set type
     *
     * @param $value
     */
    public function setType($value)
    {
        $this->authType = $value;
    }

    /**
     * Function to format Certification period field
     *
     * @param $value
     * @return string
     */
    public function getRegisteredAtAttribute($value)
    {
        return Carbon::parse($value)->format(config('swis.date_time_format_front'));
    }

    /**
     * Function to return user name (first name + last name)
     *
     * @param bool $user_id
     * @return string
     */
    public function name($user_id = false)
    {
        $user = $this;
        $userName = '';

        if ($user_id) {
            $user = User::where('id', $user_id)->first();

            if (is_null($user)) {
                return $userName;
            }
        }

        if ($user->first_name != '-') {
            $userName .= $user->first_name . ' ';
        }

        if ($user->last_name != '-') {
            $userName .= $user->last_name;
        }

        return $userName;
    }

    /**
     * Function to get user all roles
     *
     * @return HasMany
     */
    public function userRoles()
    {
        return $this->hasMany(UserRoles::class, 'user_id', 'id');
    }

    /**
     * Function to get user all roles
     *
     * @return object
     */
    public function roles()
    {
        $defaultRoles = $this->defaultRoles();
        $customRoles = $this->customRoles();

        $userAllCompanyIds = array_merge($defaultRoles->company->pluck('company_id')->toArray(), $customRoles->company->pluck('company_id')->toArray());

        $companyNames = [];
        $agencyNames = [];
        if (count($userAllCompanyIds)) {
            $companyNames = Company::select('id', 'name', 'tax_id')->whereIn('id', $userAllCompanyIds)->get()->keyBy('id');
            $agencyNames = Agency::select('id', DB::raw('COALESCE(short_name, name) as name'), 'tax_id')->joinMl()->whereIn('tax_id', $companyNames->pluck('tax_id'))->active()->get()->keyBy('tax_id')->toArray();

            $companyNames = $companyNames->toArray();
        }

        return (object)[
            'default' => $defaultRoles,
            'custom' => $customRoles,
            'companyNames' => $companyNames,
            'agencyNames' => $agencyNames,
        ];
    }

    /**
     * Function to get user roles(default)
     *
     * @return object
     */
    public function defaultRoles()
    {
        $defaultRolesSWAdmin = $this->userRolesDefault(['user_roles.role_type' => Role::ROLE_TYPE_SW_ADMIN], ['role_type']);
        $defaultRolesNaturalPersons = $this->userRolesDefault(['user_roles.role_id' => Role::NATURAL_PERSON, 'user_roles.role_type' => Role::ROLE_TYPE_NATURAL_PERSON], ['role_id']);
        $defaultRolesCompany = $this->userRolesDefault(['user_roles.role_id' => Role::HEAD_OF_COMPANY, 'user_roles.role_type' => Role::ROLE_TYPE_COMPANY], ['role_id', 'company_id']);
        $defaultRolesCustomsPortal = $this->userRolesDefault(['user_roles.role_type' => Role::ROLE_TYPE_CUSTOMS_OPERATOR], ['role_type']);

        return (object)[
            'sw_admin' => $defaultRolesSWAdmin,
            'natural_persons' => $defaultRolesNaturalPersons,
            'company' => $defaultRolesCompany,
            'customs_portal' => $defaultRolesCustomsPortal,
        ];
    }

    /**
     * Function to get user roles(custom)
     *
     * @return object
     */
    public function customRoles()
    {
        $customRolesCompany = $this->userRolesCustom(['user_roles.role_type' => Role::ROLE_TYPE_COMPANY], ['company_id']);
        $customRolesNaturalPersons = $this->userRolesCustom(['user_roles.role_type' => Role::ROLE_TYPE_NATURAL_PERSON], ['role_type', 'authorized_by_id']);

        return (object)[
            'natural_persons' => $customRolesNaturalPersons,
            'company' => $customRolesCompany
        ];
    }

    /**
     * Function to get users default roles
     *
     * @param $where
     * @param array $select
     * @return UserRoles
     */
    public function userRolesDefault($where, $select = [])
    {
        $userRoles = UserRoles::select($select)
            ->join('roles', 'user_roles.role_id', '=', 'roles.id')
            ->where('level', Role::DEFAULT_ROLE_LEVEL)
            ->where('user_roles.user_id', Auth::id())
            ->activeRole()
            ->groupBy($select);

        $userRoles = $userRoles->where($where)->get();

        return $userRoles;
    }

    /**
     * Function to get users custom roles
     *
     * @param bool $where
     * @param array $select
     * @return UserRoles
     */
    public function userRolesCustom($where = false, $select = [])
    {
        $userRoles = UserRoles::select($select)
//            ->join('roles', 'user_roles.role_id', '=', 'roles.id')
            ->whereNotIn('role_id', Role::DEFAULT_ROLES_ALL)
            ->where('user_roles.user_id', Auth::id())
            ->activeRole()
            ->whereNotNull('authorized_by_id')
            ->groupBy($select);

        if ($where) {
            $userRoles->where($where);
        }

        $userRoles = $userRoles->get();

        return $userRoles;
    }

    /**
     * Function to set Current Role
     *
     * @param $roleData
     */
    public function setCurrentRole($roleData)
    {
        $data = [];
        if ($roleData->role == Role::ROLE_LEVEL_TYPE_DEFAULT || $roleData->role == Role::ROLE_LEVEL_TYPE_CUSTOM) {

            $data = (object)[
                'user_role_ids' => $roleData->user_role_ids ?? User::FALSE,
                'role_id' => $roleData->role_id ?? User::FALSE,
                'role' => $roleData->role,
                'role_type' => $roleData->role_type,
                'company_tax_id' => $roleData->company_tax_id ?? User::FALSE,
                'company_id' => $roleData->company_id ?? User::FALSE,
                'agency_id' => $roleData->agency_id ?? User::FALSE,
                'is_lab' => $roleData->is_lab ?? User::FALSE,
                'authorized_by_id' => $roleData->authorized_by_id ?? User::FALSE,
            ];

            // 
            $userRolesMenusManager = new UserRolesMenusManager();
            $userRolesMenusManager->setDefaultStateUserRoleMenus();

        }

        session()->put('UserCurrentRole', $data);
    }

    /**
     * Function to check if user is authorized from company or agency , or has default local admin , get company tax id
     *
     * @return string
     */
    public function companyTaxId()
    {
        $userCurrentRole = $this->getCurrentUserRole();

        if (!is_null($userCurrentRole)) {
            return $userCurrentRole->company_tax_id;
        }

        return User::FALSE;
    }

    /**
     * Function to get user companies
     *
     * @return UserRoles
     */
    public function companies()
    {
        return UserRoles::select('company_id', 'company.name', 'company.tax_id', 'company.address', DB::raw('COALESCE(agency_ml.short_name, agency_ml.name) as agency_name'))
            ->join('roles', 'user_roles.role_id', '=', 'roles.id')
            ->join('company', 'user_roles.company_id', '=', 'company.id')
            ->leftJoin('agency', function ($q) {
                $q->on('agency.tax_id', '=', 'company.tax_id')->where('agency.show_status', UserRoles::STATUS_ACTIVE);
            })
            ->leftJoin('agency_ml', function ($q) {
                $q->on('agency_ml.agency_id', '=', 'agency.id')->where('agency_ml.lng_id', cLng('id'));
            })
            ->where('user_roles.user_id', $this->id)
            ->activeRole()
            ->groupBy('company_id', 'company.name', 'company.tax_id', 'company.address', DB::raw('COALESCE(agency_ml.short_name, agency_ml.name)'))//TODO check this line
            ->get();
    }

    /**
     * Function to Get active Users of Company
     *
     * @param $taxId
     * @param bool $onlyMainHeads
     * @return User
     */
    public static function getCompanyHeads($taxId, $onlyMainHeads = false)
    {
        $userIds = UserRoles::where('company_tax_id', $taxId)
            ->active()
            ->activeRole()
            ->where('role_id', Role::HEAD_OF_COMPANY);

        if ($onlyMainHeads) {
            $userIds->whereNull('authorized_by_id');
        }

        $userIds = $userIds->pluck('user_id')->all();

        return User::select('id', 'first_name', 'last_name', 'ssn')->whereIn('id', $userIds)->get();
    }

    /**
     * Function to get companyEmployees
     *
     * @param null $companyTaxId
     * @return User
     */
    public static function getCompanyEmployees($companyTaxId = null)
    {
        if (!$companyTaxId) {
            $companyTaxId = Auth::user()->companyTaxId();
        }

        $usersRoles = UserRoles::where(['company_tax_id' => $companyTaxId])->where('role_id', '!=', Role::HEAD_OF_COMPANY)->whereNotNull('authorized_by_id')->activeRole()->pluck('user_id');

        return User::whereIn('id', $usersRoles)->active()->orderByDesc()->get();
    }

    /**
     * Function to get company tax id
     *
     * @param $taxId
     * @return string
     */
    public function getCompanyTaxIdType($taxId)
    {
        $company = Company::where('tax_id', $taxId)->exists();
        if (!$company) {
            $user = User::where('ssn', $taxId)->exists();
            if (!$user) {
                return static::TYPE_USER;
            }
        }

        return static::TYPE_COMPANY;
    }

    /**
     * Function to check user can see other users data
     *
     * @return bool
     */
    public function userShowOthersData()
    {
        if ($this->isLocalAdmin()) {
            return true;
        }

        $urlAllUserRoles = $this->getUrlAllUserRoles();
        $currentUserRole = $this->getCurrentUserRole();

        if ($urlAllUserRoles->count()) {

            if ($currentUserRole->role == Role::ROLE_LEVEL_TYPE_CUSTOM || $currentUserRole->role == Role::ROLE_LEVEL_TYPE_DEFAULT) {

                if ($urlAllUserRoles->where('show_only_self', User::FALSE)->count()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Function to get current user login type , user login as company/agency or natural person
     *
     * @return bool
     */
    public function currentUserType()
    {
        $refLegalEntities = ReferenceTable::legalEntityValues();

        if ($this->companyTaxId()) {
            return $refLegalEntities[BaseModel::ENTITY_TYPE_TAX_ID];
        }

        if ($this->isNaturalPerson() || $this->isAuthorizedNaturalPerson()) {
            return $refLegalEntities[BaseModel::ENTITY_TYPE_USER_ID];
        }

        return false;
    }

    /**
     * Function to get current user type value
     *
     * @return bool
     */
    public function currentUserTypeValue()
    {
        if ($this->companyTaxId()) {
            return $this->companyTaxId();
        }

        if ($this->isNaturalPerson() || $this->isAuthorizedNaturalPerson()) {
            return $this->getCreator()->ssn;
        }

        return false;
    }

    /**
     * Function to check Auth user role type is Company
     *
     * @return bool
     */
    public function isCompanyType()
    {
        if ($this->companyTaxId()) {
            return true;
        }

        return false;
    }

    /**
     * Function to check Auth user current role is Company
     *
     * @return bool
     */
    public function isCompany()
    {
        if ($this->companyTaxId() && !$this->getCurrentUserRole()->agency_id) {
            return true;
        }

        return false;
    }

    /**
     * Function to check Auth current role is Agency
     *
     * @return bool
     */
    public function isAgency()
    {
        if ($this->companyTaxId() && $this->getCurrentUserRole()->agency_id) {
            return true;
        }

        return false;
    }

    /**
     * Function to check Auth current role is Laboratory
     *
     * @return bool
     */
    public function isLaboratory()
    {
        if ($this->companyTaxId() && $this->getCurrentUserRole()->is_lab) {
            return true;
        }

        return false;
    }

    /**
     * Function to check Auth user current role is SW ADMIN
     *
     * @return bool
     */
    public function isSwAdmin()
    {
        if (!is_null($this->getCurrentUserRole()) && $this->getCurrentUserRole()->role_id == Role::SW_ADMIN) {
            return true;
        }

        return false;
    }

    /**
     * Function to check Auth user current role is authorized SW ADMIN
     *
     * @return bool
     */
    public function isAuthorizedSwAdmin()
    {
        if ($this->getCurrentUserRole()->role_id == Role::SW_ADMIN && !in_array(Auth::user()->ssn, User::SW_ADMIN_SSN)) {
            return true;
        }

        return false;
    }

    /**
     * Function to check is local admin
     *
     * @return bool
     */
    public function isLocalAdmin()
    {
        if (!is_null($this->getCurrentUserRole()) && $this->getCurrentUserRole()->role_type == Role::ROLE_TYPE_COMPANY && $this->getCurrentUserRole()->role_id == Role::HEAD_OF_COMPANY) {
            return true;
        }

        return false;
    }

    /**
     * Function to check is authorized local admin
     *
     * @return bool
     */
    public function isAuthorizedLocalAdmin()
    {
        if (!is_null($this->getCurrentUserRole()) && $this->getCurrentUserRole()->role_type == Role::ROLE_TYPE_COMPANY && $this->getCurrentUserRole()->role_id == Role::HEAD_OF_COMPANY && $this->getCurrentUserRole()->authorized_by_id) {
            return true;
        }

        return false;
    }

    /**
     * Function to check is authorized local admin
     *
     * @return bool
     */
    public function isAuthorizedEmployeeForCompany()
    {
        if (!is_null($this->getCurrentUserRole()) && $this->getCurrentUserRole()->role == Role::ROLE_LEVEL_TYPE_CUSTOM && $this->getCurrentUserRole()->role_type == Role::ROLE_TYPE_COMPANY && $this->getCurrentUserRole()->role_id == Role::FALSE) {
            return true;
        }

        return false;
    }

    /**
     * Function to check is natural person
     *
     * @return bool
     */
    public function isNaturalPerson()
    {
        if (!is_null($this->getCurrentUserRole()) && $this->getCurrentUserRole()->role_type == Role::ROLE_TYPE_NATURAL_PERSON && $this->getCurrentUserRole()->role_id == Role::NATURAL_PERSON) {
            return true;
        }

        return false;
    }

    /**
     * Function to check is authorized natural person
     *
     * @return bool
     */
    public function isAuthorizedNaturalPerson()
    {
        if (!is_null($this->getCurrentUserRole()) && $this->getCurrentUserRole()->role_id == Role::NATURAL_PERSON && $this->getCurrentUserRole()->authorized_by_id) {
            return true;
        }

        return false;
    }

    /**
     * Function to check is authorized natural person
     *
     * @return bool
     */
    public function isCustomsOperator()
    {
        if (!is_null($this->getCurrentUserRole()) && $this->getCurrentUserRole()->role_id == Role::CUSTOMS_OPERATOR && $this->getCurrentUserRole()->role_type == Role::ROLE_TYPE_CUSTOMS_OPERATOR) {
            return true;
        }

        return false;
    }

    /**
     * Function to check if company is broker or not (check if in broker table exist that tin and date is valid)
     *
     * @return bool
     */
    public function isBroker()
    {
        if ($this->isAgency() || $this->isOnlyBroker()) {
            return true;
        }

        return false;
    }

    /**
     * Function to check current user company is only broker
     *
     * @return bool
     */
    public function isOnlyBroker()
    {
        if ($this->isCompany()) {

            $broker = Broker::where('tax_id', $this->companyTaxId())->active()->first();
            if (!is_null($broker)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Function to check current user is only company
     *
     * @return bool
     */
    public function isOnlyCompany()
    {
        if ($this->isCompanyType() && !$this->isBroker()) {
            return true;
        }

        return false;
    }

    /**
     * Function to check user is original Head of company
     *
     * @param $companyTaxId
     * @return bool
     */
    public function isMainLocalAdminOfCompany($companyTaxId)
    {
        $userRoles = UserRoles::select('company_id', 'authorized_by_id')
            ->where('user_roles.user_id', $this->id)
            ->where('company_tax_id', $companyTaxId)
            ->activeRole()
            ->groupBy('company_id', 'authorized_by_id')
            ->whereNull('authorized_by_id')
            ->first();

        if (!is_null($userRoles)) {
            return true;
        }

        return false;
    }

    /**
     * Function to check auth user is Employee of Transport Ministry Agency
     *
     * @return bool
     */
    public function isTransportMinistryAgencyEmployee()
    {
        if ($this->isAgency() && $this->companyTaxId() == Agency::TRANSPORT_MINISTRY_TAX_ID) {
            return true;
        }

        return false;
    }

    /**
     * Function to get that roles(modules) user can authorize
     *
     * @param $menuName
     * @return object
     */
    public function authorizeModuleRoles($menuName)
    {
        $roles = collect();
        $roleGroups = collect();

        $preDefinedRoles = [
            Role::CRUD_ALL_FUNCTIONALITY,
        ];

        if (!in_array($menuName, Menu::MENUS_ONLY_HAS_ONLY_ALL_FUNCTIONALITY_ROLE)) {
            $preDefinedRoles[] = Role::CRUD_ONLY_READ;
        }

        if ($menuName == Menu::USER_DOCUMENTS_MENU_NAME) {
            $preDefinedRoles = [];
        }

        // Company(Agency) Head
        if ($this->isLocalAdmin()) {

            if ($menuName == Menu::AUTHORIZE_PERSON_MENU_NAME) {

                $preDefinedRoles = [];
                if ($this->hasAccessToAuthorizeFromMyName()) {
                    $preDefinedRoles = [
                        Role::AUTHORIZE_USER_ADD_MY_PERMISSIONS,
                        Role::ALLOW_AUTHORIZE_USER_ADD_MY_PERMISSIONS,
                    ];
                }
            }

            $roles = Role::select('roles.id', 'roles.name', 'level')->leftJoin('menu', 'roles.menu_id', '=', 'menu.id')->active()
                ->where(['company_tax_id' => $this->companyTaxId(), 'menu.name' => $menuName])
                ->orWhereIn('roles.id', $preDefinedRoles)
                ->orderByDesc('roles.id');

            $roleGroups = RolesGroup::select('roles_group.id', 'roles_group.name')->distinct()->where('roles_group.company_tax_id', $this->companyTaxId())
                ->join('menu', 'roles_group.menu_id', '=', 'menu.id')
                ->join('roles_group_roles', 'roles_group.id', '=', 'roles_group_roles.roles_group_id')
                ->join('roles', 'roles_group_roles.role_id', '=', 'roles.id')
                ->where('menu.name', $menuName)
                ->where('roles.show_status', Role::STATUS_ACTIVE)
                ->orderByDesc('roles_group.id')
                ->active()
                ->get();

            $roles = $roles->get();
        }

        // Company(Agency) Authorized Employee
        if ($this->isAuthorizedEmployeeForCompany()) {

            $rolesAndRoleGroups = $this->getUserRolesAndRolesGroups($menuName);

            $roleIds = $rolesAndRoleGroups['roles'];
            if ($menuName == Menu::AUTHORIZE_PERSON_MENU_NAME) {
                $roleIds[] = Role::ALLOW_AUTHORIZE_USER_ADD_MY_PERMISSIONS;
            }

            $roles = Role::select('id', 'name', 'level')
                ->active()
                ->whereIn('id', $roleIds);

            $roles = $roles->get();

            //
            $roleGroups = RolesGroup::select('id', 'name')->active()->whereIn('id', $rolesAndRoleGroups['roleGroups'])->get();
        }

        // Natural Person
        if ($this->isNaturalPerson()) {

            if ($menuName == Menu::AUTHORIZE_PERSON_MENU_NAME && !$this->isAuthorizedNaturalPerson()) {
                $preDefinedRoles = [
                    Role::AUTHORIZE_USER_ADD_MY_PERMISSIONS,
                ];
            }

            $roles = Role::select('id', 'name', 'level')->whereIn('id', $preDefinedRoles)->get();
        }

        // SW Admin
        if ($this->isSwAdmin()) {
            $roles = Role::select('id', 'name', 'level')->whereIn('id', $preDefinedRoles)->get();
        }

        $allRoles = [
            'roles' => $roles,
            'roleGroups' => $roleGroups,
        ];

        return (object)$allRoles;
    }

    /**
     * Function to get user current
     *
     * @return SessionManager|Store|mixed
     */
    public function getCurrentUserRole()
    {
        return session('UserCurrentRole');
    }

    /**
     * Function to remove User current session of roles
     */
    public function removeCurrentUserRole()
    {
        session()->forget('UserCurrentRole');
    }

    /**
     * Function to get menu all user roles
     *
     * @param bool $menuName
     * @return Collection
     */
    public function getUrlAllUserRoles($menuName = false)
    {
        if (!$menuName) {
            $currentMenu = $this->getCurrentUrlMenu();
        } else {
            $currentMenu = Menu::select('id', 'parent_id')->where('name', $menuName)->first();
        }

        if ($currentMenu) {

            $currentMenuUserRoles = UserRoles::select('show_only_self', 'role_id')->whereIn('id', $this->getCurrentUserRole()->user_role_ids);

            if (is_null($currentMenu->parent_id)) {
                $currentMenuUserRoles->where('menu_id', $currentMenu->id);
            } else {
                $currentMenuUserRoles->where('menu_id', $currentMenu->parent_id);
            }

            $currentMenuUserRoles = $currentMenuUserRoles->get();

            return $currentMenuUserRoles;
        }

        return collect();
    }

    /**
     * Function to get current user role all roles
     *
     * @param bool $menuName
     * @return Role[]
     */
    public function getCurrentRoles($menuName = false)
    {
        $allRoles = [];
        if ($this->isLocalAdmin()) {

            if ($menuName == Menu::USER_DOCUMENTS_MENU_NAME) {
                $allRoles = $this->getUserCreatedRoles(Menu::USER_DOCUMENTS_MENU_NAME);
            }

        } else {

            $userRolesIds = $this->getCurrentUserRole()->user_role_ids;

            if (count($userRolesIds)) {

                $userRoles = UserRoles::select('user_roles.id', 'user_roles.role_id', 'role_group_id', 'roles_group_roles.role_id as role_group_role_id')->whereIn('user_roles.id', $userRolesIds)
                    ->leftJoin('roles_group_roles', 'user_roles.role_group_id', '=', 'roles_group_roles.roles_group_id')
                    ->activeRole();

                if ($menuName) {

                    $userRoles->join('menu', 'user_roles.menu_id', '=', 'menu.id')->where('menu.name', $menuName);
                }

                $allRoles = array_filter(array_unique(array_merge($userRoles->pluck('role_id')->unique()->all(), $userRoles->pluck('role_group_role_id')->unique()->all())));

                return Role::whereIn('id', $allRoles)->active()->pluck('id')->all();
            }

        }

        return $allRoles;
    }

    /**
     * Function to get current company_tax_id user roles
     *
     * @param $menuName
     * @param bool $defaultRoles
     * @param array $addSelect
     * @param array $where
     * @return mixed
     */
    public function getCurrentUserRoles($menuName, $defaultRoles = true, $addSelect = [], $where = [])
    {
        $userRoles = UserRoles::select('id', 'role_id', 'menu_id', 'show_only_self', 'authorized_by_id')
            ->where('menu_id', Menu::getMenuIdByName($menuName))
            ->where('user_id', Auth::id())
            ->where('company_tax_id', $this->companyTaxId())
            ->activeRole();

        if (count($addSelect)) {
            $userRoles->addSelect($addSelect);
        }

        if (!$defaultRoles) {
            $userRoles->whereNotIn('role_id', Role::DEFAULT_ROLES_ALL);
        }

        if ($where) {
            $userRoles->where($where);
        }

        return $userRoles->get();
    }

    /**
     * Function to get user logged role menus
     *
     * @return array
     */
    public function getCurrentRoleMenus()
    {
        $currentUserRole = $this->getCurrentUserRole();

        // Fixed Menus for current type users
        if (Auth::user()->isTransportMinistryAgencyEmployee() && Auth::user()->isLocalAdmin()) {
            return Menu::whereIn('name', Menu::MENUS_FOR_TRANSPORT_APPLICATION)->pluck('id')->all();
        }

        //
        if (!is_null($currentUserRole) && in_array($currentUserRole->role_id, Role::USER_DEFAULT_TYPES_ROLES) && !$this->isAuthorizedSwAdmin()) {

            $menuIds = RoleMenus::where('role_id', $currentUserRole->role_id)->join('menu', 'role_menus.menu_id', '=', 'menu.id');

            if ($this->isCompany()) {
                $menuIds->where('role_menus.role_type', Role::ROLE_TYPE_COMPANY);
            }

            if ($this->isAuthorizedNaturalPerson()) {
                $menuIds->where('menu.name', '!=', Menu::AUTHORIZE_PERSON_MENU_NAME);
            }

            // now need to close this type (check in future)
            if ($this->isAgency()) {
                $menuIds->whereNotIn('menu.name', [Menu::PAYMENTS_MENU_NAME, Menu::SINGLE_APPLICATION_MENU_NAME]);
            }

            $menuIds = $menuIds->pluck('menu_id')->unique()->all();

            return $menuIds;

        } else {

            $menuIds = UserRoles::whereIn('user_roles.id', $currentUserRole->user_role_ids)->whereNotNull('user_roles.menu_id');

            if ($this->isCompany()) {
                $menuIds->join('role_menus', 'user_roles.menu_id', '=', 'role_menus.menu_id')->where('role_menus.role_type', Role::ROLE_TYPE_COMPANY);
            }

            // now need to close this type (check in future)
            if ($this->isAgency()) {
                $menuIds->join('menu', 'user_roles.menu_id', '=', 'menu.id')->whereNotIn('menu.name', [Menu::PAYMENTS_MENU_NAME, Menu::SINGLE_APPLICATION_MENU_NAME]);
            }

            $menuIds = $menuIds->pluck('user_roles.menu_id')->unique()->all();

            if ($this->hasAccessToAuthorizeFromMyName()) {
                $menuIds[] = Menu::AUTHORIZE_PERSON_MENU_ID;
            }

            return $menuIds;
        }
    }

    /**
     * Function to get User roles and roles groups
     *
     * @param $menuName
     * @return array
     */
    public function getUserRolesAndRolesGroups($menuName)
    {
        $userRoles = UserRoles::select('role_id', 'role_group_id')
            ->join('menu', 'user_roles.menu_id', '=', 'menu.id')
            ->where('menu.name', $menuName)
            ->whereIn('user_roles.id', $this->getCurrentUserRole()->user_role_ids)->whereNotIn('role_id', Role::DEFAULT_ROLES_ALL)
            ->get();

        $roleIds = $roleGroupIds = [];
        foreach ($userRoles as $userRole) {
            if ($userRole->role_id) {
                $roleIds[] = $userRole->role_id;
            }

            if ($userRole->role_group_id) {
                $roleGroupIds[] = $userRole->role_group_id;
            }
        }

        return [
            'roles' => $roleIds,
            'roleGroups' => $roleGroupIds
        ];
    }

    /**
     * Function to get current route url and check menu has module or not
     *
     * @return bool|Menu
     */
    public function getCurrentUrlMenu()
    {
        if (!in_array(Route::currentRouteName(), self::NO_NEED_CHECK_ACCESS_ROUTE_NAMES)) {

            $url = Route::getCurrentRoute()->uri;

            if (!empty($url)) {

                $action = explode('/', $url)[1];

                $menu = Menu::select('id', 'parent_id', 'name')->where('href', $action)->first();

                if (!is_null($menu)) {
                    return $menu;
                }
            }
        }

        return false;
    }

    /**
     * Function to get user roles(with role_group_roles) created roles
     *
     * @param null $menuName
     * @return array
     */
    public function getUserCreatedRoles($menuName = null)
    {
        $userCurrentRole = $this->getCurrentUserRole();
        $allRoles = [];

        if ($userCurrentRole->role_type == Role::ROLE_TYPE_COMPANY) {

            $roles = Role::select('roles.id')->join('menu', 'roles.menu_id', '=', 'menu.id')->where('company_tax_id', $this->companyTaxId())->active();

            if ($menuName) {
                $roles->where('menu.name', $menuName);
            }

            $roleGroups = RolesGroup::select('role_id')
                ->join('menu', 'roles_group.menu_id', '=', 'menu.id')
                ->join('roles_group_roles', 'roles_group.id', '=', 'roles_group_roles.roles_group_id')
                ->join('roles', 'roles_group_roles.role_id', '=', 'roles.id')
                ->where('roles_group.company_tax_id', $this->companyTaxId())
                ->where('roles_group.show_status', Role::STATUS_ACTIVE)
                ->where('roles.show_status', Role::STATUS_ACTIVE);

            if ($menuName) {
                $roleGroups->where('menu.name', $menuName);
            }

            $allRoles = array_filter(array_unique(array_merge($roles->pluck('roles.id')->unique()->all(), $roleGroups->pluck('role_id')->unique()->all())));
        }

        return $allRoles;
    }

    /**
     * Function to get creator user
     *
     * @return AuthenticatableContract|null
     */
    public function getCreator()
    {
        // If user login as Agency
        if ($this->isAgency()) {

            $companyTaxID = $this->companyTaxId();

            $agency = Agency::where('tax_id', $companyTaxID)->first();

            if (!is_null($agency)) {

                $agency->name = !empty($agency->current()->name) ? mb_substr($agency->current()->name, 0, 70) : '';

                $agency->ssn = $companyTaxID;
                $agency->type = (string)getReferenceRowIdByCode(ReferenceTable::REFERENCE_LEGAL_ENTITY_FORM, User::ENTITY_TYPE_TAX_ID);
                $agency->phone_number = trim(Auth::user()->phone_number);
                $agency->email = Auth::user()->email;
                $agency->community = '-';
                $agency->passport = null;
                $agency->address = !empty($agency->address) ? $agency->address : '-';
                $agency->country = getReferenceRowIdByCode(ReferenceTable::REFERENCE_COUNTRIES, config('swis.country_mode'));

                return $agency;
            }

        }

        // If user login as company
        if ($this->isCompany()) {

            $companyTaxID = $this->companyTaxId();
            $company = Company::where('tax_id', $companyTaxID)->first();

            if (!is_null($company)) {
                $company->ssn = $companyTaxID;
                $company->name = mb_substr($company->name, 0, 70);
                $company->community = !empty($company->community) ? mb_substr($company->community, 0, 35) : '-';
                $company->type = (string)getReferenceRowIdByCode(ReferenceTable::REFERENCE_LEGAL_ENTITY_FORM, User::ENTITY_TYPE_TAX_ID);
                $company->address = !empty($company->address) ? $company->address : '-';
                $company->phone_number = trim(Auth::user()->phone_number);
                $company->email = Auth::user()->email;
                $company->passport = null;
            }

            $company->country = getReferenceRowIdByCode(ReferenceTable::REFERENCE_COUNTRIES, config('swis.country_mode'));

            return $company;
        }

        // If user login as authorized NATURAL PERSON
        if ($this->isAuthorizedNaturalPerson()) {
            $user = User::find($this->getCurrentUserRole()->authorized_by_id);
        } else {
            // If user login from his natural person role
            $user = Auth::user();
        }

        $user->name = $user->name($user->id);
        $user->address = !empty($user->registration_address) ? $user->registration_address : '-';
        $user->passport = !empty($user->passport) ? $user->passport : '-';
        $user->community = !empty($user->community) ? mb_substr($user->community, 0, 35) : '-';
        $user->type = (string)getReferenceRowIdByCode(ReferenceTable::REFERENCE_LEGAL_ENTITY_FORM, User::ENTITY_TYPE_USER_ID);

        $user->country = getReferenceRowIdByCode(ReferenceTable::REFERENCE_COUNTRIES, config('swis.country_mode'));

        return $user;
    }

    /**
     * Function to get current info creator id , AUTH user or authorized as NATURAL PERSON
     *
     * @return AuthenticatableContract|null
     */
    public function getCreatorUser()
    {
        $user = Auth::user();

        if ($this->isAuthorizedNaturalPerson()) {
            $authorizedUser = User::find($this->getCurrentUserRole()->authorized_by_id);

            if (!is_null($authorizedUser)) {
                $user = $authorizedUser;
            }
        }

        return $user;
    }

    /**
     * Function to get user documents
     *
     * @return Collection
     */
    public function getUserDocuments()
    {
        $constructorDocuments = collect();

        if (!is_null($this->getCurrentUserRole()) && $this->getCurrentUserRole()->role_type == Role::ROLE_TYPE_COMPANY) {

            $roles = $this->getCurrentRoles(Menu::USER_DOCUMENTS_MENU_NAME);

            if (!count($roles)) {
                return $constructorDocuments;
            }

            $constructorDocumentIds = ConstructorDocumentsRoles::whereIn('role_id', $roles)->pluck('document_id')->unique()->toArray();

            $documentIds = ConstructorMapping::whereIn('role', $roles)->whereIn('document_id', $constructorDocumentIds)->pluck('document_id')->unique()->toArray();

            $constructorDocuments = ConstructorDocument::select('id', 'constructor_documents_ml.document_name')
                ->join('constructor_documents_ml', 'constructor_documents.id', '=', 'constructor_documents_ml.constructor_documents_id')->where('constructor_documents_ml.lng_id', '=', cLng('id'))
                ->where('company_tax_id', Auth::user()->companyTaxId())
                ->whereIn('id', $documentIds)->active()->get();
        }

        return $constructorDocuments;
    }

    /**
     * Function to check user has access laboratories module
     *
     * @return Collection
     */
    public function getUserLaboratories()
    {
        $agencyLaboratories = collect();
        if (!is_null($this->getCurrentUserRole()) && $this->getCurrentUserRole()->role_type == Role::ROLE_TYPE_COMPANY) {

            $agencyLaboratories = Laboratory::select('id', 'laboratory_ml.name')->where('company_tax_id', $this->companyTaxId())
                ->join('laboratory_ml', 'laboratory.id', '=', 'laboratory_ml.laboratory_id')->where('laboratory_ml.lng_id', '=', cLng('id'))
                ->where('certification_period_validity', '>=', currentDateTime())->active();


            if (!$this->isLocalAdmin()) {
                $agencyLaboratories->join('laboratory_users', 'laboratory.id', '=', 'laboratory_users.laboratory_id')
                    ->where('laboratory_users.user_id', Auth::id());
            }

            $agencyLaboratories = $agencyLaboratories->get();
        }

        return $agencyLaboratories;
    }

    /**
     * Function to get user agent reference table
     *
     * @return Collection
     */
    public function getUserAgencyReferenceTables()
    {
        $referenceTables = collect();

        if (!is_null($this->getCurrentUserRole()) && $this->getCurrentUserRole()->role_type == Role::ROLE_TYPE_COMPANY) {

            if (!$this->isLocalAdmin()) {

                $roleHasModuleReferenceTable = $this->getUrlAllUserRoles(Menu::REFERENCE_TABLE_FORM_MENU_NAME);

                if (!$roleHasModuleReferenceTable->count()) {
                    return $referenceTables;
                }
            }

            $referenceIds = ReferenceTableAgencies::where('agency_id', $this->getCurrentUserRole()->agency_id)->pluck('reference_table_id')->unique()->all();

            $referenceTables = ReferenceTable::select('reference_table.*', 'reference_table_ml.name as ml_name')
                ->joinMl()
                ->whereIn('id', $referenceIds)
                ->orderBy('ml_name');

            // Fix where for Transport Ministry
            if ($this->isTransportMinistryAgencyEmployee() && $this->isAuthorizedEmployeeForCompany()) {
                $referenceTables->whereNotIn('table_name', [ReferenceTable::REFERENCE_TRANSPORT_BORDER_CHECKPOINTS_TABLE_NAME, ReferenceTable::REFERENCE_TRANSPORT_PERMIT_TYPES_TABLE_NAME]);
            }

            $referenceTables = $referenceTables->get();
        }

        return $referenceTables;
    }

    /**
     * Function to get auth user reports if auth user has and show in left menu
     *
     * @return Reports|Collection
     */
    public function getUserReports()
    {
        $reports = collect();

        if ($this->hasReportModuleAccess()) {

            // Company/Broker/Agency
            if ($this->companyTaxId()) {

                $userHasFullAccessForReports = true;
                if (!$this->isLocalAdmin()) {
                    $userHasFullAccessForReports = UserRoles::select('id')->where(['user_id' => Auth::id(), 'menu_id' => Menu::getMenuIdByName(Menu::REPORTS_MENU_NAME)])->activeRole()->whereNull('multiple_attributes')->first();
                }

                if ($userHasFullAccessForReports) {
                    $reports = Reports::active();

                    if ($this->isAgency()) {
                        $reports->whereIn('type', Reports::REPORT_TYPES_FOR_AGENCIES)->orWhere(function ($q) {
                            $q->where(['company_tax_id' => $this->companyTaxId(), 'type' => Reports::REPORT_TYPE_AGENCY_WITH_TAX_ID]);
                        });
                    }

                    if ($this->isOnlyBroker()) {
                        $reports->whereIn('type', Reports::REPORT_TYPES_FOR_ONLY_BROKERS);
                    }

                    if ($this->isOnlyCompany()) {
                        $reports->where('id', -1);
                    }

                } else {
                    $userAllAccessForReports = UserRoles::where(['user_id' => Auth::id(), 'menu_id' => Menu::getMenuIdByName(Menu::REPORTS_MENU_NAME)])->activeRole()->whereNotNull('multiple_attributes')->pluck('multiple_attributes')->toArray();

                    $userAccessReports = [];
                    foreach ($userAllAccessForReports as $accessForReport) {
                        $userAccessReports = array_merge($userAccessReports, $accessForReport);
                    }

                    $reports = Reports::whereIn('id', $userAccessReports);
                }

                $reports = $reports->orderBy('id')->get();

            } else {

                // Sw Admin
                $reports = Reports::whereIn('type', Reports::REPORT_TYPES_FOR_SW_ADMIN)->orWhere('type', Reports::REPORT_TYPE_ALL)->orderBy('id')->get();
            }

        }

        return $reports;
    }

    /**
     * Function to get user available for search subdivision (payment and obligation module)
     *
     * @return array|Collection|null
     */
    public function getPaymentObligationsAvailableRefSubdivisions()
    {
        $subDivisions = collect();

        if ($this->isLocalAdmin()) {
            $subDivisions = RegionalOffice::getRefSubdivisions();
        } else {
            $paymentObligationUserRoles = $this->getCurrentUserRoles(Menu::PAYMENT_AND_OBLIGATIONS_MENU_NAME, false)->pluck('id')->all();

            if (count($paymentObligationUserRoles)) {
                //
                $subDivisionIds = UserRoles::whereIn('id', $paymentObligationUserRoles)->where(['attribute_field_id' => Role::ROLE_ATTRIBUTES_PAYMENT_AND_OBLIGATION_SUBDIVISION])->pluck('attribute_value')->all();

                //
                $userRolePaymentObligationDocument = UserRoles::whereIn('id', $paymentObligationUserRoles)->where(['attribute_field_id' => Role::ROLE_ATTRIBUTES_PAYMENT_AND_OBLIGATION_DOCUMENT])->pluck('attribute_value')->all();

                if (count($userRolePaymentObligationDocument)) {

                    $constructorDocumentCodes = ConstructorDocument::whereIn('id', $userRolePaymentObligationDocument)->pluck('document_code')->all();

                    $subdivisionsFromConstructorDocument = RegionalOfficeConstructorDocuments::whereIn('constructor_document_code', $constructorDocumentCodes)
                        ->join('regional_office', 'regional_office_constructor_documents.regional_office_id', '=', 'regional_office.id')->where('regional_office.show_status', RegionalOffice::STATUS_ACTIVE)
                        ->pluck('office_code')->all();

                    $subdivisionsFromConstructorDocumentRef = getReferenceRowsWhereIn(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS, [], $subdivisionsFromConstructorDocument, ['id']);

                    $subDivisionIds = array_merge($subdivisionsFromConstructorDocumentRef->pluck('id')->all(), $subDivisionIds);
                }

                //
                if (count($subDivisionIds)) {
                    return getReferenceRowsWhereIn(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS, $subDivisionIds, [], ['id', 'name', 'short_name']);
                }
            }
        }

        return $subDivisions;
    }

    /**
     * Function to get user current role available modules
     *
     * @return array
     */
    public function getUserCurrentRoleAvailableModules()
    {
        $availableModules = MenuManager::menuModules();

        return $availableModules->pluck('id')->toArray();
    }

    /**
     * Function to show user first name
     *
     * @param $userId
     * @return string
     */
    public static function getUserNameByID($userId)
    {
        $user = User::where('id', $userId)->first();

        $name = '';
        if (!is_null($user)) {
            $name = (new self)->name($user->id);
        }

        return $name;
    }

    /**
     * Function to get crud permissions
     *
     * @param $column
     * @param $menu
     * @return bool
     */
    public function userCrudPermissions($column, $menu = null)
    {
        if (($this->isSwAdmin() && !$this->isAuthorizedSwAdmin()) || $this->isLocalAdmin()) {
            return true;
        }

        $menuRoles = $this->getUrlAllUserRoles($menu);

        if ($menuRoles->count()) {
            $roles = Role::select('id')->where($column, User::TRUE)->whereIn('id', $menuRoles->pluck('role_id')->all())->first();

            if (is_null($roles)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Function to check User Create Permission
     *
     * @return bool
     */
    public function canCreate()
    {
        return $this->userCrudPermissions('create_permission');
    }

    /**
     * Function to check User Read Permission
     *
     * @param null $menu
     * @return bool
     */
    public function canRead($menu = null)
    {
        return $this->userCrudPermissions('read_permission', $menu);
    }

    /**
     * Function to check User Update Permission
     *
     * @return bool
     */
    public function canUpdate()
    {
        return $this->userCrudPermissions('update_permission');
    }

    /**
     * Function to check User Delete Permission
     *
     * @return bool
     */
    public function canDelete()
    {
        return $this->userCrudPermissions('delete_permission');
    }

    /**
     * Function get user or company balance (if user login a company show company balance)
     *
     * @return string
     */
    public function balance()
    {
        $companyTaxId = $this->companyTaxId();

        return self::getBalance($companyTaxId);
    }

    /**
     * Function to return user balance with number format
     *
     * @param $userId
     * @param $companyTaxId
     * @return float
     */
    public static function getBalance($companyTaxId, $userId = null)
    {
        $userBalance = null;
        if (is_null($userId)) {

            if (Auth::check() && Auth::user()->isNaturalPerson()) {
                $userBalance = Transactions::select('transactions.id', 'transactions.total_balance')->where('transactions.user_id', Auth::id())
                    ->leftJoin('company', 'company.tax_id', '=', 'transactions.company_tax_id')
                    ->whereNull('company.tax_id')
                    ->orderby('transactions.id', 'desc')
                    ->first();
            } elseif (!empty(Auth::user()) && is_null(Auth::user()->getCurrentUserRole())) {
                $userBalance = null;
            } else {
                $userBalance = Transactions::select('id', 'total_balance')->where('company_tax_id', $companyTaxId)->orderByDesc()->first();
            }

        } else {

            switch ((new self)->getCompanyTaxIdType($companyTaxId)) {
                case static::TYPE_COMPANY:
                    $userBalance = Transactions::select('id', 'total_balance')->where('company_tax_id', $companyTaxId)->orderByDesc()->first();
                    break;
                case static::TYPE_USER:
                    $userBalance = Transactions::select('id', 'total_balance')->where('user_id', $userId)->where('company_tax_id', $companyTaxId)->orderByDesc()->first();
                    break;
            }
        }

        $userBalance = is_null($userBalance) ? 0 : $userBalance->total_balance;

        return number_format($userBalance, 2, '.', '');
    }

    /**
     * Function to check user has access to menu
     *
     * @param bool $currentRoleMenus
     * @param bool|Collection $currentUrlMenu
     * @return bool
     */
    public function hasAccessToMenu($currentRoleMenus = false, $currentUrlMenu = false)
    {
        // for authorize person part check
        if (request()->route()->getAction('noNeedCheckAccess')) {
            return true;
        }

        if ($currentUrlMenu) {
            $needCheckMenuId = $currentUrlMenu->id;

            if (!is_null($currentUrlMenu->parent_id)) {
                $needCheckMenuId = $currentUrlMenu->parent_id;
            }

            if (!$currentRoleMenus) {
                $currentRoleMenus = $this->getCurrentRoleMenus();
            }

            if (!in_array($needCheckMenuId, $currentRoleMenus)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Function to check need to update user current session
     *
     * @param bool|Collection $currentUrlMenu
     * @return bool
     */
    public function hasToUpdateCurrentRoleSession($currentUrlMenu)
    {
        if ($currentUrlMenu) {

            $needCheckMenuId = $currentUrlMenu->id;

            if (!is_null($currentUrlMenu->parent_id)) {
                $needCheckMenuId = $currentUrlMenu->parent_id;
            }

            if (!$this->isSwAdmin()) {

                //
                $userRoleMenu = UserRolesMenus::where(['user_id' => Auth::id(), 'need_update' => User::TRUE, 'company_tax_id' => $this->companyTaxId()])
                    ->where(function ($q) use ($needCheckMenuId) {
                        $q->where('type', UserRolesMenus::ROLE_TYPE_USER_ROLE)->where('menu_id', $needCheckMenuId)
                            ->orWhereIn('type', [UserRolesMenus::ROLE_TYPE_BROKER, UserRolesMenus::ROLE_TYPE_AGENCY])->whereNull('menu_id');
                    })->first();

                if (!is_null($userRoleMenu)) {
                    return true;
                }

            } else {
                // Sw Admin roles
                $userRoleMenu = UserRolesMenus::where(['user_id' => Auth::id(), 'menu_id' => $needCheckMenuId, 'need_update' => User::TRUE, 'type' => UserRolesMenus::ROLE_TYPE_SW_ADMIN])->where('company_tax_id', self::FALSE)->first();

                if (!is_null($userRoleMenu)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Function to check user has access to payment module if yes show in left menu and top menu
     *
     * @return bool
     */
    public function hasPaymentModuleAccess()
    {
        if ($this->companyTaxId()) {

            if (!$this->isLocalAdmin() && !in_array(Menu::getMenuIdByName(Menu::PAYMENTS_MENU_NAME), $this->getCurrentRoleMenus())) {
                return false;
            }
        }

        return true;
    }

    /**
     * Function to check user has access to Documents archive module
     *
     * @return bool
     */
    public function hasDocumentArchiveModuleAccess()
    {
        if ($this->companyTaxId()) {

            if (!$this->isLocalAdmin() && !in_array(Menu::getMenuIdByName(Menu::DOCUMENT_CLOUD_MENU_NAME), $this->getCurrentRoleMenus())) {
                return false;
            }
        }

        return true;
    }

    /**
     * Function to check user has access to Reports module
     *
     * @return bool
     */
    public function hasReportModuleAccess()
    {
        if ($this->isTransportMinistryAgencyEmployee()) {
            return false;
        }

        if ($this->isLocalAdmin() || ($this->isSwAdmin() && !$this->isAuthorizedSwAdmin())) {
            return true;
        } else {

            if (in_array(Menu::getMenuIdByName(Menu::REPORTS_MENU_NAME), $this->getCurrentRoleMenus())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Function to check user has access to Reference Form module
     *
     * @return bool
     */
    public function hasReferenceFormModuleAccess()
    {
        if ($this->isAgency()) {
            $referenceId = request()->route('id') ?? 0;

            if (in_array($referenceId, $this->getUserAgencyReferenceTables()->pluck('id')->toArray())) {
                return true;
            }

            return false;
        }

        if ($this->isSwAdmin()) {

            if (!$this->isAuthorizedSwAdmin()) {
                return true;
            }

            if (in_array(Menu::REFERENCE_TABLE_FORM_MENU_ID, $this->getCurrentRoleMenus())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Function to check user allow to add permission from his name (authorize as local admin)
     *
     * @return bool
     */
    public function hasAccessToAuthorizeFromMyName()
    {
        if ($this->isLocalAdmin() && !$this->isAuthorizedLocalAdmin()) {
            return true;
        }

        $userRole = UserRoles::where(['company_tax_id' => $this->companyTaxId(), 'user_id' => Auth::id(), 'role_id' => Role::ALLOW_AUTHORIZE_USER_ADD_MY_PERMISSIONS])->activeRole()->first();

        if (!is_null($userRole)) {
            return true;
        }

        return false;
    }

    /**
     * Function to check user has access to edit current module or only view
     *
     * @return bool
     */
    public function hasAccessToEditModule()
    {
        if (!$this->canUpdate() && !$this->canCreate()) {
            return false;
        }

        return true;
    }

    /**
     * Function to check need user for change password
     *
     * @return bool
     */
    public function hasToChangePassword()
    {
        if (Auth::user()->password_changed_at) {
            $passwordChangedAt = Carbon::parse(Auth::user()->password_changed_at);

            if ($passwordChangedAt->diffInDays(now()) > config('global.password_expiration')) {
                Auth::user()->password_need_change = User::TRUE;
                Auth::user()->save();
            }
        }

        return false;
    }

    /**
     * Function to get user types for global notifications
     *
     * @param $userType
     * @param $agencies
     * @param null $subApplicationId
     * @return array
     */
    public static function getUsersGroupForNotifications($userType, $agencies, $subApplicationId = null)
    {
        $usersGroup = [];

        switch ($userType) {

            case User::USER_TYPE_ALL_USERS:

                $companiesIds = Company::pluck('tax_id')->all();

                $usersGroup =
                    UserRoles::select('user_id', 'company_tax_id', 'first_name', 'last_name', 'email', 'phone_number', 'email_notification', 'sms_notification', 'lng_id')
                        ->join('users', function($join){
                            $join->on('users.id', '=', 'user_roles.user_id');
                            $join->where('users.show_status', User::STATUS_ACTIVE);
                        })
                        ->whereIn('company_tax_id', $companiesIds)
                        ->where('users.show_status', User::STATUS_ACTIVE)
                        ->orWhere('role_id', '=', Role::NATURAL_PERSON)
                        ->activeRole()
                        ->groupBy('user_id', 'company_tax_id', 'first_name', 'last_name', 'email', 'phone_number', 'email_notification', 'sms_notification', 'lng_id')
                        ->get();

                break;

            case User::SAF_APPLICANT:

                $referenceLegalEntityValues = $referenceLegalEntityValues = getReferenceRows(ReferenceTable::REFERENCE_LEGAL_ENTITY_FORM, false, false, [], 'get', false, false, [], false, BaseModel::LANGUAGE_CODE_EN)->toArray();

                $legalEntityValues = [];
                if (count($referenceLegalEntityValues)) {
                    foreach ($referenceLegalEntityValues as $value) {
                        $legalEntityValues[$value->code] = $value->id;
                    }
                }

                $safNumber = SingleApplicationSubApplications::where('id', $subApplicationId)->pluck('saf_number')->first();
                $safSideValues = SingleApplication::select('applicant_type', 'applicant_value')->where('regular_number', $safNumber)->first();

                $naturalPersonType = $legalEntityValues[User::ENTITY_TYPE_USER_ID];
                $naturalPersonForeignCitizenType = $legalEntityValues[User::ENTITY_TYPE_FOREIGN_CITIZEN];

                $companyType = $legalEntityValues[User::ENTITY_TYPE_TAX_ID];

                $usersSSN = [];
                $companyTaxIds = [];

                // Natural Person / Foreign Natural Person
                if ($safSideValues->applicant_type == $naturalPersonType || $safSideValues->applicant_type == $naturalPersonForeignCitizenType) {
                    $usersSSN[] = $safSideValues->applicant_value;
                }

                // Company
                if ($safSideValues->applicant_type == $companyType) {
                    $companyTaxIds[] = $safSideValues->applicant_value;

                }

                $users = User::select('id', 'email', 'phone_number', 'lng_id', 'first_name', 'last_name', 'email_notification', 'sms_notification', DB::raw("CONCAT('0') as company_tax_id"))
                    ->whereIn('ssn', array_unique($usersSSN))
                    ->active()
                    ->get();

                $userWithCompany = UserRoles::select('user_id as id', 'email', 'phone_number', 'lng_id', 'users.first_name', 'users.last_name', 'users.email_notification', 'users.sms_notification', 'company_tax_id')
                    ->where('role_id', Role::HEAD_OF_COMPANY)
                    ->whereIn('company_tax_id', array_unique($companyTaxIds))
                    ->join('users', function($join){
                        $join->on('users.id', '=', 'user_roles.user_id');
                        $join->where('users.show_status', User::STATUS_ACTIVE);
                    })
                    ->activeRole()
                    ->groupBy('company_tax_id', 'user_id', 'email', 'phone_number', 'lng_id', 'users.first_name', 'users.last_name', 'users.email_notification', 'users.sms_notification')
                    ->get();

                $usersGroup = $users->toBase()->merge($userWithCompany->toBase());

                break;

            case User::SAF_SIDES:

                $legalEntityValues = ReferenceTable::legalEntityValues();

                $safSideValues = SingleApplication::select('importer_type', 'importer_value', 'exporter_type', 'exporter_value', 'applicant_type', 'applicant_value')->get();

                $naturalPersonType = $legalEntityValues[User::ENTITY_TYPE_USER_ID];
                $naturalPersonForeignCitizenType = $legalEntityValues[User::ENTITY_TYPE_FOREIGN_CITIZEN];

                $companyType = $legalEntityValues[User::ENTITY_TYPE_TAX_ID];

                $usersSSN = [];
                $companyTaxIds = [];

                foreach ($safSideValues as $saf) {

                    // Natural Person / Foreign Natural Person
                    if ($saf->importer_type == $naturalPersonType || $saf->importer_type == $naturalPersonForeignCitizenType) {
                        $usersSSN[] = $saf->importer_value;
                    }

                    if ($saf->exporter_type == $naturalPersonType || $saf->exporter_type == $naturalPersonForeignCitizenType) {
                        $usersSSN[] = $saf->exporter_value;
                    }

                    if ($saf->applicant_type == $naturalPersonType || $saf->applicant_type == $naturalPersonForeignCitizenType) {
                        $usersSSN[] = $saf->applicant_value;
                    }

                    // Company
                    if ($saf->importer_type == $companyType) {
                        $companyTaxIds[] = $saf->importer_value;
                    }

                    if ($saf->exporter_type == $companyType) {
                        $companyTaxIds[] = $saf->exporter_value;
                    }

                    if ($saf->applicant_type == $companyType) {
                        $companyTaxIds[] = $saf->applicant_value;
                    }

                }

                $users = User::select('id', 'email', 'phone_number', 'lng_id', 'first_name', 'last_name', 'email_notification', 'sms_notification', DB::raw("CONCAT('0') as company_tax_id"))->whereIn('ssn', array_unique($usersSSN))->active()->get();

                $userWithCompany = UserRoles::select('user_id as id', 'email', 'phone_number', 'lng_id', 'users.first_name', 'users.last_name', 'company_tax_id')->whereIn('company_tax_id', array_unique($companyTaxIds))
                    ->join('users', function($join){
                        $join->on('users.id', '=', 'user_roles.user_id');
                        $join->where('users.show_status', User::STATUS_ACTIVE);
                    })
                    ->activeRole()
                    ->groupBy('company_tax_id', 'user_id', 'email', 'phone_number', 'lng_id', 'users.first_name', 'users.last_name')->get();

                $usersGroup = $users->toBase()->merge($userWithCompany->toBase());

                break;

            case User::USER_TYPE_BROKER:

                $brokerCompaniesIds = Broker::join('company', 'company.tax_id', '=', 'broker.tax_id')->active()->pluck('company.id')->all();

                $usersGroup = User::select('users.id', 'users.ssn', 'users.first_name', 'users.last_name', 'users.username', 'users.passport', 'users.email', 'users.phone_number', 'users.lng_id', 'user_roles.company_tax_id', 'users.email_notification', 'users.sms_notification')
                    ->distinct()
                    ->join('user_roles', 'users.id', '=', 'user_roles.user_id')
                    ->where('user_roles.role_status', Role::STATUS_ACTIVE)
                    ->whereIn('user_roles.company_id', $brokerCompaniesIds)
                    ->active()
                    ->get();

                break;

            case User::USER_TYPE_AGENCY:

                $query = User::select('users.id', 'users.ssn', 'users.first_name', 'users.last_name', 'users.username', 'users.passport', 'users.email', 'users.phone_number', 'users.lng_id', 'users.email_notification', 'users.sms_notification', 'user_roles.company_tax_id')
                    ->distinct()
                    ->join('user_roles', 'users.id', '=', 'user_roles.user_id');

                if (count($agencies) > 0) {
                    $query->whereIn('user_roles.company_tax_id', $agencies);
                } else {
                    $activeAgencies = Agency::active()->pluck('tax_id')->all();
                    $query->whereIn('user_roles.company_tax_id', $activeAgencies);
                }

                $usersGroup = $query->active()->get();

                break;

            case User::SUB_APPLICATION_CURRENT_STATE_USERS:

                $subApplication = SingleApplicationSubApplications::select('document_code', 'agency_id')->where('id', $subApplicationId)->first();

                if (!is_null($subApplication)) {
                    $subApplicationDocumentCode = optional($subApplication)->document_code;
                    $agencyId = optional($subApplication)->agency_id;

                    $agencyTaxId = Agency::where('id', $agencyId)->pluck('tax_id')->first();

                    if (!is_null($subApplicationDocumentCode)) {
                        $currentState = SingleApplicationSubApplicationStates::where(['saf_subapplication_id' => $subApplicationId, 'is_current' => '1'])
                            ->pluck('state_id')
                            ->first();
                        $documentId = ConstructorDocument::where(['document_code' => $subApplicationDocumentCode])->pluck('id')->first();

                        if (!is_null($documentId)) {
                            $usersGroup = ConstructorStates::getUsersByMapping($currentState, $documentId);
                            foreach ($usersGroup as $user) {
                                $user->company_tax_id = $agencyTaxId;
                            }
                        }
                    }
                }

                break;

            case User::AGENCY_HEADS:

                $agencyId = SingleApplicationSubApplications::where(['id' => $subApplicationId])->pluck('agency_id')->first();

                if (!is_null($agencyId)) {
                    $agency = Agency::where('id', $agencyId)->first();

                    if (!is_null($agency)) {
                        $adminIds = UserRoles::where(['company_tax_id' => $agency->tax_id, 'role_id' => Role::HEAD_OF_COMPANY])->activeRole()->pluck('user_id')->unique()->all();

                        $usersGroup = User::select('users.id', 'users.ssn', 'users.first_name', 'users.last_name', 'users.username', 'users.passport', 'users.email', 'users.phone_number', 'users.lng_id', 'users.email_notification', 'users.sms_notification', 'user_roles.company_tax_id')
                            ->whereIn('users.id', $adminIds)
                            ->where('company_tax_id', $agency->tax_id)
                            ->distinct()
                            ->join('user_roles', 'users.id', '=', 'user_roles.user_id')
                            ->active()
                            ->get();
                    }
                }

                break;

            case User::USER_TYPE_SW_ADMIN:

                $usersGroup = User::getSwAdmins();

                break;
        }

        return $usersGroup;
    }

    /**
     * Function to get Sw_Admins
     *
     * @return User
     */
    public static function getSwAdmins()
    {
        $swAdminUserIds = UserRoles::activeRole()->where('role_id', Role::SW_ADMIN)->pluck('user_id')->unique();

        return User::whereIn('id', $swAdminUserIds)->active()->get();
    }

    /**
     * Function to get logged data
     *
     * @param null $action
     * @return array
     */
    public function getLogData($action = null)
    {
        $logData['data'] = '';
        if ($action == 'update' || $action == 'edit' || $action == 'view') {
            $data = Offence::where('offence_reference_number', request()->uuid)->first();
            $logData['id'] = $data->id;
            $logData['uuid'] = $data->offence_reference_number;
        }

        if ($action == 'update') {
            $logData['data'] = Offence::where('offence_reference_number', request()->uuid)->first();
        } elseif ($action == 'edit' || $action == 'view') {
            $logData['data'] = request()->getUri();
        }

        return $logData;
    }
}
