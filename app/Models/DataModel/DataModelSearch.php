<?php

namespace App\Models\DataModel;

use App\Http\Controllers\Core\DataTableSearch;
use Illuminate\Support\Facades\DB;

/**
 * Class DataModelSearch
 * @package App\Models\DataModel
 */
class DataModelSearch extends DataTableSearch
{
    /**
     * Function to search rows count
     *
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * Function to get data_model_excel table not deleted rows count
     *
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * Function to search
     *
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        return $query->get();
    }

    /**
     * Function to construct order for search
     *
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($this->orderByCol, $this->orderByType ?? 'desc');
    }

    /**
     * Function to construct search query
     *
     * @return mixed
     */
    protected function constructQuery()
    {
        $query = DataModel::select('data_model_excel.*')
            ->joinMl(['f_k' => 'data_model_id'])
            ->notDeleted();

        if (isset($this->searchData['id'])) {
            $query->where(DB::raw('data_model_excel.id::varchar'), $this->searchData['id']);
        }

        if (isset($this->searchData['name'])) {
            $query->where('name', 'ILIKE', '%' . strtolower(trim($this->searchData['name'])) . '%');
        }

        if (isset($this->searchData['description'])) {
            $query->where('data_model_ml.description', 'ILIKE', '%' . strtolower(trim($this->searchData['description'])) . '%');
        }

        if (isset($this->searchData['wco'])) {
            $query->where('wco', $this->searchData['wco']);
        }

        if (isset($this->searchData['eaec'])) {
            $query->where('eaec', $this->searchData['eaec']);
        }

        if (isset($this->searchData['reference_table'])) {
            $query->where('reference', $this->searchData['reference_table']);
        }

        if (isset($this->searchData['definition'])) {
            $query->where('definition', 'ILIKE', '%' . strtolower(trim($this->searchData['definition'])) . '%');
        }

        if (isset($this->searchData['type'])) {
            $query->where('type', $this->searchData['type']);
        }

        return $query;
    }

    /**
     * Function to construct search limit
     *
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}
