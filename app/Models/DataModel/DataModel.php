<?php

namespace App\Models\DataModel;

use App\Models\BaseModel;
use App\Models\DataModelConstants\DataModelConstants;
use App\Models\ReferenceTable\ReferenceTable;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\DB;

/**
 * Class DataModel
 * @package App\Models\DataModel
 */
class DataModel extends BaseModel
{
    /**
     * @var string
     */
    const REGISTRATION_NUMBER_MDM_ID = 'REGISTRATION_NUMBER_MDM_ID';
    const REGISTRATION_DATE_MDM_ID = 'REGISTRATION_DATE_MDM_ID';
    const PERMISSION_NUMBER_MDM_ID = 'PERMISSION_NUMBER_MDM_ID';
    const PERMISSION_DATE_MDM_ID = 'PERMISSION_DATE_MDM_ID';
    const EXPIRATION_DATE_MDM_ID = 'EXPIRATION_DATE_MDM_ID';
    const REQUEST_DATE_MDM_ID = 'REQUEST_DATE_MDM_ID';
    const REJECTION_DATE_MDM_ID = 'REJECTION_DATE_MDM_ID';
    const REJECTION_NUMBER_MDM_ID = 'REJECTION_NUMBER_MDM_ID';
    const REJECTION_REASON_MDM_ID = 'REJECTION_REASON_MDM_ID';
    const PERMISSION_DOCUMENT_MDM_ID = 'PERMISSION_DOCUMENT_MDM_ID';
    const TOTAL_NETTO_WEIGHT_MDM_ID = 'TOTAL_NETTO_WEIGHT_MDM_ID';

    /**
     * @var string
     */
    const MDM_ELEMENT_TYPE_ALFA = 'a';
    const MDM_ELEMENT_TYPE_ALFA_NUMERIC = 'an';
    const MDM_ELEMENT_TYPE_NUMERIC = 'n';
    const MDM_ELEMENT_TYPE_DATE = 'd';
    const MDM_ELEMENT_TYPE_DATE_TIME = 'dt';
    const MDM_ELEMENT_TYPE_DECIMAL = 'dc';
    const MDM_ELEMENT_TYPE_LINE = 'line';
    const MDM_ELEMENT_TYPE_MULTIPLE = 'm';

    /**
     * @var array
     */
    const MDM_ELEMENT_TYPES_ALL = [
        self::MDM_ELEMENT_TYPE_ALFA,
        self::MDM_ELEMENT_TYPE_ALFA_NUMERIC,
        self::MDM_ELEMENT_TYPE_NUMERIC,
        self::MDM_ELEMENT_TYPE_DATE,
        self::MDM_ELEMENT_TYPE_DATE_TIME,
        self::MDM_ELEMENT_TYPE_DECIMAL,
        self::MDM_ELEMENT_TYPE_LINE,
        self::MDM_ELEMENT_TYPE_MULTIPLE
    ];

    /**
     * @var string
     */
    public $table = 'data_model_excel';

    /**
     * @var string
     */
    const STANDARD_TYPE_WCO = 'wco';
    const STANDARD_TYPE_EAEU = 'eaeu';

    /**
     * @var array
     */
    const STANDARDS_ALL = [
        self::STANDARD_TYPE_WCO,
        self::STANDARD_TYPE_EAEU,
    ];

    /**
     * @var bool
     */
    protected $hasAdminInfo = true;

    /**
     * @var null
     */
    protected static $dataModelAllData = null;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'excel_id',
        'reference',
        'name',
        'definition',
        'type',
        'db_type',
        'wco_id',
        'wco_class_name',
        'wco',
        'eaec_id',
        'eaec',
        'eaeu_min',
        'eaeu_max',
        'eaeu_type',
        'eaeu_total_digits',
        'eaeu_fraction_digits',
        'decimal_fraction',
        'found_type',
        'client_min',
        'client_max',
        'pattern',
        'db_min',
        'db_max',
        'interface_format',
        'available_at',
        'available_to',
        'show_status',
        'is_content'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'created_admin_id',
        'created_admin_ip',
        'updated_at',
        'updated_admin_id',
        'updated_admin_ip',
        'deleted_admin_id',
        'deleted_admin_ip'
    ];

    /**
     * Function to relate an get all languages with DataModelMl
     *
     * @return HasMany
     */
    public function ml()
    {
        return $this->hasMany(DataModelMl::class, 'data_model_id', 'id');
    }

    /**
     * Function to relate an get current languages with DataModelMl
     *
     * @return HasOne
     */
    public function current()
    {
        return $this->hasOne(DataModelMl::class, 'data_model_id', 'id')->notDeleted()->where('lng_id', cLng('id'))->first();
    }

    /**
     * Function to get data model all data
     *
     * @return DataModel
     */
    public static function allData()
    {
        if (self::$dataModelAllData === null) {
            self::setAllData();
        }

        return self::$dataModelAllData;
    }

    /**
     * Function to set data model all
     *
     * @return void
     */
    public static function setAllData()
    {
        self::$dataModelAllData = DataModel::select('id', 'reference', 'interface_format', 'database_format', 'pattern', 'type','is_content','definition')->get()->keyBy('id')->toArray();
    }

    /**
     * Function to get field type form fileds array
     *
     * @param $fields
     * @return array
     */
    public static function getMdmFieldsType($fields)
    {
        $dataModelManager = new DataModelManager();

        $dataModelAllData = DataModel::allData();
        $referenceTableAllData = ReferenceTable::select('id','table_name')->active()->get()->keyBy('table_name');

        if ($fields && count($fields) > 0) {
            foreach ($fields as &$field) {

//                $mdmField = self::select('type', 'reference', 'is_content')->where('id', $field['mdm_field_id'])->firstOrFail();
                if(isset($dataModelAllData[$field['mdm_field_id']])){
                    $mdmField = (object)$dataModelAllData[$field['mdm_field_id']];

                    $field['type'] = (!empty($mdmField->reference)) ? 'autocomplete' : $dataModelManager->dbType($mdmField->type);
                    $field['mdm_line_type'] = ($mdmField->type == DataModel::MDM_ELEMENT_TYPE_LINE);
                    $field['mdm_type'] = $mdmField->type;
                    $field['is_content'] = $mdmField->is_content;

                    if (!empty($mdmField->reference)) {
//                    $refId = ReferenceTable::select('id')->where('table_name', $mdmField->reference)->active()->first();
                        $field['reference'] = $mdmField->reference;

                        $refTableId = null;
                        if(isset($referenceTableAllData[$mdmField->reference])){
                            $refTableId = $referenceTableAllData[$mdmField->reference]['id'];
                        }

                        $field['reference_id'] = $refTableId;
                    }
                }
            }
        }

        return $fields;
    }

    /**
     * Function to get description
     *
     * @param $dataModelId
     * @return string
     */
    public function getDescription($dataModelId)
    {
        $dataModelDescription = self::select('data_model_excel_ml.lng_id as language', 'data_model_excel_ml.description')->where('data_model_id', $dataModelId)->join('data_model_excel_ml', 'data_model_excel_ml.data_model_id', '=', DB::raw('data_model_excel.id'))->get();

        return ($dataModelDescription) ? $dataModelDescription : '';
    }

    /**
     * Function to get mdm field id by constant
     *
     * @param $constant
     * @return DataModelConstants|string
     */
    public static function getMdmFieldIDByConstant($constant)
    {
        $dataModel = DataModelConstants::select('mdm_id')->where('constant', $constant)->pluck('mdm_id')->first();

        return $dataModel ? $dataModel : '';
    }
}
