<?php

namespace App\Models\DataModel;

use App\Models\BaseMlManager;
use App\Models\DataModelConstants\DataModelConstants;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class DataModelManager
 * @package App\Models\DataModel
 */
class DataModelManager
{
    use BaseMlManager;

    /**
     * Function to Store to DB
     *
     * @param array $data
     * @return DataModel
     */
    public function store(array $data)
    {
        $data['db_type'] = $this->dbType($data['type']);
        $data['user_id'] = Auth::id();
        $data['show_status'] = DataModel::STATUS_ACTIVE;
        $data['is_content'] = (($data['type'] == 'a' || $data['type'] == 'an') && isset($data['is_content'])) ? 1 : 0;

        $dataModel = new DataModel($data);
        $dataModelMl = new DataModelMl();

        return DB::transaction(function () use ($data, $dataModel, $dataModelMl) {
            $dataModel->save();
            $this->storeMl($data['ml'], $dataModel, $dataModelMl);

            return $dataModel;
        });
    }

    /**
     * Function to Update data
     *
     * @param array $data
     * @param int $id
     */
    public function update(array $data, int $id)
    {
        $data['db_type'] = $this->dbType($data['type']);
        $data['is_content'] = (($data['type'] == 'a' || $data['type'] == 'an') && isset($data['is_content'])) ? 1 : 0;

        $dataModel = DataModel::where(DB::raw('id::varchar'), $id)->firstOrFail();
        $dataModelMl = new DataModelMl();

        DB::transaction(function () use ($data, $dataModel, $dataModelMl) {

            $today = Carbon::today()->toDateString();

            ($data['available_to'] != '' && $data['available_to'] < $today) ? $data['show_status'] = DataModel::STATUS_INACTIVE : '';

            $dataModel->update($data);
            $this->updateMl($data['ml'], 'data_model_id', $dataModel, $dataModelMl);
        });
    }

    /**
     * Function to search
     *
     * @param $searchValue
     * @return array
     */
    public function searchInDataModel($searchValue)
    {
        $constantMdmIds = DataModelConstants::pluck('mdm_id')->all();

        $results = DataModel::leftJoin('data_model_excel_ml', function ($query) use ($searchValue) {
            return $query->on("data_model_excel.id", "=", "data_model_excel_ml.data_model_id")->where("lng_id", cLng("id"));
        })->where(function ($query) use ($searchValue) {
            $query->orWhere(DB::raw("id::varchar"), "=", $searchValue)
                ->orWhere('definition', "ILIKE", "%{$searchValue}%")
                ->orWhere("name", "ILIKE", "%{$searchValue}%")
                ->orWhere("type", "ILIKE", "%{$searchValue}%")
                ->orWhere("wco_id", "ILIKE", "%{$searchValue}%")
                ->orWhere("eaec_id", "ILIKE", "%{$searchValue}%");
        })
            ->whereNotIn('id', $constantMdmIds)
            ->get();

        $values = [];
        foreach ($results as $result) {
            $values['items'][] = [
                'id' => $result->id,
                'name' => "({$result->id}) {$result->name}"
            ];
        }

        return $values;
    }

    /**
     * Function to inactivating data model rows
     */
    public function inactivateDataModelRows()
    {
        //If data model row active and start_date > today or end_date < today inactivate data model row
        DataModel::where('show_status', DataModel::STATUS_ACTIVE)
            ->where(function ($q) {
                $q->where('available_at', '>', currentDate())
                    ->orWhere('available_to', '<', currentDate());
            })
            ->update(['show_status' => DataModel::STATUS_INACTIVE]);
    }

    /**
     * Function to get field info and type by WCO and EAEU standards
     *
     * @param $type
     * @param $searchVal
     * @param $eaeuType
     * @return array
     */
    public function getFieldTypeAndAllInfo($type, $searchVal, $eaeuType)
    {
        $result = [];
        if ($type == DataModel::STANDARD_TYPE_WCO) {
            $result = DB::connection('wco')->table('wco_dataset')->select(DB::raw("CONCAT(wco_id, ' - ', format ,' - ' , definition) AS name"), 'wco_id', 'class_name', 'id', 'format')
                ->where(function ($q) use ($searchVal) {
                    $q->where(DB::raw('wco_id::varchar'), 'ILIKE', $searchVal . '%')
                        ->orWhere('class_name', 'ILIKE', '%' . $searchVal . '%');
                })
                ->first();
        }

        $eec_values = [];
        if ($type == DataModel::STANDARD_TYPE_EAEU) {

            switch ($eaeuType) {
                case 'complexData':
                    $tableName = 'ComplexDataFinal';
                    break;
                case 'ComplexDataSequences':
                    $tableName = 'ComplexDataSequencesFinal';
                    break;
                default:
                    $tableName = 'SimpleDataFinal';

            }

            $result = DB::table($tableName)->select(DB::raw("CONCAT(id, ' - ', documentation) AS name"), 'id', 'documentation')
                ->where(function ($q) use ($searchVal) {
                    $q->where(DB::raw('id::varchar'), $searchVal);
                })->first();

            if ($eaeuType == 'ComplexDataSequences' && !is_null($result)) {

                $eec = DB::table('ComplexDataSequencesFinal')->where('id', $result->id)->first();

                $ref = explode(':', $eec->ref);
                if (!empty($ref[1])) {

                    $element = DB::table('SimpleDataFinal')->where(['folder_name' => $eec->folder_name])->where(['name' => $ref[1]])->first();

                    $result->name = $result->id . ' - ' . $result->documentation . ' -> ' . $element->id . ' - ' . $element->documentation;

                    $type = explode(':', $element->type);

                    $simpleDataRow = DB::table('SimpleDataFinal')->where(['folder_name' => $eec->folder_name])->where(['name' => $type[1]])->first();

                    $columnType = NULL;
                    if (!empty($simpleDataRow->restriction)) {

                        $rest = DB::table('SimpleDataFinal')->where(['folder_name' => $eec->folder_name])->where(['name' => explode(':', $simpleDataRow->restriction)[1]])->first();
                        $columnType = $rest->restriction ?? '';
                    }

                    //
                    $eType = $this->EeauType($columnType);

                    $eec_values = [
                        'type' => $eType['cType'],
                        'db_type' => $eType['dbType'],
                        'min' => empty($simpleDataRow->minLength) ? 0 : $simpleDataRow->minLength,
                        'max' => empty($simpleDataRow->maxLength) ? 0 : $simpleDataRow->maxLength,
                        'total_digits' => empty($simpleDataRow->totalDigits) ? 0 : $simpleDataRow->totalDigits,
                        'fraction_digits' => empty($simpleDataRow->fractionDigits) ? 0 : $simpleDataRow->fractionDigits
                    ];

                } else {
                    $eec_values = [
                        'type' => '',
                        'db_type' => '',
                        'min' => empty($eec->minLength) ? 0 : $eec->minLength,
                        'max' => empty($eec->maxLength) ? 0 : $eec->maxLength,
                        'total_digits' => empty($eec->totalDigits) ? 0 : $eec->totalDigits,
                        'fraction_digits' => empty($eec->fractionDigits) ? 0 : $eec->fractionDigits
                    ];
                }

            } elseif ($eaeuType == 'SimpleData' && !is_null($result)) {

                $simpleDataRow = DB::table('SimpleDataFinal')->where('id', $result->id)->first();

                $eType = $this->EeauType($columnType = NULL);

                $eec_values = [
                    'type' => $eType['cType'],
                    'db_type' => $eType['dbType'],
                    'min' => empty($simpleDataRow->minLength) ? 0 : $simpleDataRow->minLength,
                    'max' => empty($simpleDataRow->maxLength) ? 0 : $simpleDataRow->maxLength,
                    'total_digits' => empty($simpleDataRow->totalDigits) ? 0 : $simpleDataRow->totalDigits,
                    'fraction_digits' => empty($simpleDataRow->fractionDigits) ? 0 : $simpleDataRow->fractionDigits
                ];

            }

        }

        //
        $resultFormat = $result->format ?? '';
        if ($type == DataModel::STANDARD_TYPE_WCO) {

            $values = [];
            if (!is_null($result) && !is_null($result->format)) {

                preg_match('/^([a,n]{1,2})([\.]{0,3})([\d]+)([\,]([\d]+))?$/', $resultFormat, $matches, PREG_OFFSET_CAPTURE);

                if ($resultFormat != 'N/A') {

                    $values['type'] = $matches[1][0];
                    $values['max'] = $matches[3][0];

                    if ($matches[2][0] == '..') {

                        if (empty($matches[5])) {
                            $values['min'] = 1;
                        } else {

                            $values['max'] = $matches[3][0];
                            $values['min'] = $matches[5][0];
                            $values['decimal'] = true;
                        }

                    } else {
                        $values['min'] = $matches[3][0];
                    }
                }
            }
        }

        //
        $data['options'] = [(array)$result ?? ''];
        if (!empty($result)) {

            if ($type == DataModel::STANDARD_TYPE_WCO) {
                $classInfo = DB::connection('wco')
                    ->table('wco_dataset')
                    ->select('wco_dataset.class_name as class_name', 'wco_data_model.wco_id as wco_id')
                    ->leftJoin('wco_data_model', 'wco_dataset.class_name', '=', 'wco_data_model.class_name')
                    ->where('wco_dataset.wco_id', $result->wco_id)
                    ->get()
                    ->toArray();

                $classInfo = array_map(function ($value) {
                    return (array)$value;
                }, $classInfo);

                $classNamesAndCodes = [];
                $classNames = [];
                foreach ($classInfo as $classItem) {
                    $classNamesAndCodes[] = $classItem['wco_id'] . ' - ' . $classItem['class_name'];
                    $classNames[] = $classItem['class_name'];
                }

                $data['options'][0]['classNamesAndCodes'] = $classNamesAndCodes;
                $data['options'][0]['classNames'] = $classNames;
                $data['options'][0]['values'] = json_encode($values, true);
            }

            if ($type == DataModel::STANDARD_TYPE_EAEU) {
                $data['options'][0]['values'] = json_encode($eec_values, true);
            }
        }

        return $data;

    }

    /**
     * Function to get db type
     *
     * @param $type
     * @return string
     */
    public function dbType($type)
    {
        $columnType = '';
        switch ($type) {

            case DataModel::MDM_ELEMENT_TYPE_ALFA:
            case DataModel::MDM_ELEMENT_TYPE_ALFA_NUMERIC:
                $columnType = 'varchar';
                break;

            case DataModel::MDM_ELEMENT_TYPE_NUMERIC:
                $columnType = 'integer';
                break;

            case DataModel::MDM_ELEMENT_TYPE_DATE:
                $columnType = 'date';
                break;

            case DataModel::MDM_ELEMENT_TYPE_DATE_TIME:
                $columnType = 'datetime';
                break;
            case DataModel::MDM_ELEMENT_TYPE_DECIMAL:
                $columnType = 'decimal';
                break;
        }

        return $columnType;
    }

    /**
     * Function to get static ref for dbType
     *
     * @param $type
     * @return string
     */
    public static function getDbType($type)
    {
        return (new self)->dbType($type);
    }

    /**
     * Function to return Eeau Type
     *
     * @param $columnType
     * @return array
     */
    public function EeauType($columnType)
    {
        switch ($columnType) {
            case 'xs:token':
            case 'xs:string':
            case 'xs:duration':
            case 'xs:base64Binary':
                $cType = 'an';
                $dbType = 'varchar';
                break;

            case 'xs:positiveInteger':
            case 'xs:nonNegativeInteger':
                $cType = 'n';
                $dbType = 'integer';
                break;

            case 'xs:date':
                $cType = 'd';
                $dbType = 'date';
                break;

            case 'xs:decimal':
                $cType = 'dc';
                $dbType = 'decimal';
                break;

            default :
                $cType = '';
                $dbType = '';
                break;
        }

        return [
            'cType' => $cType,
            'dbType' => $dbType
        ];
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @return array
     */
    public function getLogData($id)
    {
        return DataModel::where('id', $id)->first()->toArray();
    }
}
