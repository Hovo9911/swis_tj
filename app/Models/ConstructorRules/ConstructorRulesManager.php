<?php

namespace App\Models\ConstructorRules;

/**
 * Class ConstructorRulesManager
 * @package App\Models\ConstructorRules
 */
class ConstructorRulesManager
{
    /**
     * Function to save data in db
     *
     * @param array $data
     * @return bool
     */
    public function store(array $data)
    {
        $constructorRules = new ConstructorRules($data);

        return $constructorRules->save();
    }

    /**
     * Function to update data in db
     *
     * @param array $data
     * @param int $id
     */
    public function update(array $data, int $id)
    {
        $constructorRule = ConstructorRules::where('id', $id)->constructorRulesOwner()->first();

        $constructorRule->update($data);
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @param $id2
     * @return array
     */
    public function getLogData($id, $id2)
    {
        return ConstructorRules::where('id', $id2)->first()->toArray();
    }
}