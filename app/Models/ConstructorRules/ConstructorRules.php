<?php

namespace App\Models\ConstructorRules;

use App\Models\BaseModel;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorMappingToRule\ConstructorMappingToRule;
use App\Models\ReferenceTable\ReferenceTable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class ConstructorRules
 * @package App\Models\ConstructorRules
 */
class ConstructorRules extends BaseModel
{
    /**
     * @var string
     */
    const OBLIGATION_TYPE = 'obligation';
    const VALIDATION_TYPE = 'validation';
    const CALCULATION_TYPE = 'calculation';
    const RISK_TYPE = 'risk';

    /**
     * @var array
     */
    const ALL_RULE_TYPES = [ConstructorRules::OBLIGATION_TYPE, ConstructorRules::VALIDATION_TYPE, ConstructorRules::CALCULATION_TYPE, ConstructorRules::RISK_TYPE];

    /**
     * @var string
     */
    const CREATION_TYPE_FIXED_SUM = 'fixed_sum';
    const CREATION_TYPE_PERCENTAGE = 'percentage';
    const CREATION_TYPE_RULE = 'rule';
    const CREATION_TYPE_FREE_SUM = 'free sum';
    const CREATION_TYPE_FIXED_SUM_CODE = 1;
    const CREATION_TYPE_PERCENTAGE_CODE = 2;
    const CREATION_TYPE_RULE_CODE = 3;
    const CREATION_TYPE_FREE_SUM_CODE = 4;

    /**
     * @var string
     */
    protected $table = 'constructor_rules';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'document_id',
        'type',
        'obligation_code',
        'condition',
        'rule'
    ];

    /**
     * Function to check auth user has role
     *
     * @param $query
     * @return mixed
     */
    public function scopeConstructorRulesOwner($query)
    {
        $agencyDocumentIds = ConstructorDocument::getConstructorDocumentForAgency()->pluck('id')->toArray();

        return $query->whereIn($this->getTable() . '.document_id', $agencyDocumentIds ?? -1);
    }

    /**
     * Function to get type of rules
     *
     * @return array
     */
    public static function getTypes()
    {
        return [
            'obligation' => trans('swis.constructor_rules.' . self::OBLIGATION_TYPE),
            'validation' => trans('swis.constructor_rules.' . self::VALIDATION_TYPE),
            'calculation' => trans('swis.constructor_rules.' . self::CALCULATION_TYPE),
        ];
    }

    /**
     * Function to get obligations from reference tables
     *
     * @return array
     */
    public static function getObligations()
    {
        $obligationBudgetLineTable = ReferenceTable::REFERENCE_OBLIGATION_BUDGET_LINE;
        $obligationBudgetLineMlTable = ReferenceTable::REFERENCE_OBLIGATION_BUDGET_LINE_ML;
        $obligationCreationTypeTable = ReferenceTable::REFERENCE_OBLIGATION_CREATION_TYPE;
        $agency = getReferenceRows(ReferenceTable::REFERENCE_AGENCY_TABLE_NAME, false, Auth::user()->companyTaxId());

        return DB::table($obligationBudgetLineTable)
            ->select("{$obligationBudgetLineTable}.code", "{$obligationBudgetLineMlTable}.name", "{$obligationCreationTypeTable}.obligation_creation_type as type")
            ->join($obligationBudgetLineMlTable, function ($query) use ($obligationBudgetLineMlTable, $obligationBudgetLineTable) {
                $query->on("{$obligationBudgetLineMlTable}.{$obligationBudgetLineTable}_id", '=', "{$obligationBudgetLineTable}.id")
                    ->where("{$obligationBudgetLineMlTable}.lng_id", cLng('id'));
            })
            ->join($obligationCreationTypeTable, "{$obligationCreationTypeTable}.id", '=', "{$obligationBudgetLineTable}.obligation_creation_type")
            ->where("{$obligationBudgetLineTable}.show_status", '1')
            ->when(!is_null($agency), function ($q) use ($agency, $obligationBudgetLineTable) {
                $q->where("{$obligationBudgetLineTable}.agency", $agency->id);
            })
            ->get();
    }

    /**
     * Function to get obligation type by id
     *
     * @param $id
     * @return mixed
     */
    public static function getObligationType($id)
    {
        $obligationBudgetLineTable = ReferenceTable::REFERENCE_OBLIGATION_BUDGET_LINE;
        $obligationCreationTypeTable = ReferenceTable::REFERENCE_OBLIGATION_CREATION_TYPE;

        return DB::table($obligationBudgetLineTable)
            ->select("{$obligationBudgetLineTable}.id", "{$obligationCreationTypeTable}.obligation_creation_type as type")
            ->join($obligationCreationTypeTable, "{$obligationCreationTypeTable}.id", '=', "{$obligationBudgetLineTable}.obligation_creation_type")
            ->where("{$obligationBudgetLineTable}.code", $id)
            ->first();
    }

    /**
     * Function to get rules for document
     *
     * @param $documentId
     * @param null $mappingId
     * @return array
     */
    public static function getRules($documentId, $mappingId = null)
    {
        $mappingRules = [];
        $rules = self::select('id', 'type', 'name')->where('document_id', $documentId)->orderByDesc()->get();

        foreach ($rules as $rule) {
            $mappingRules[$rule->type][] = [
                'id' => $rule->id,
                'name' => $rule->name,
            ];
        }

        if (!is_null($mappingId)) {
            $checked = ConstructorMappingToRule::where('constructor_mapping_id', $mappingId)->pluck('rule_id')->all();
            foreach ($mappingRules as &$group) {
                foreach ($group as &$rule) {
                    $rule['checked'] = (in_array($rule['id'], $checked)) ? true : false;
                }
            }
        }

        return $mappingRules;
    }

    /**
     * Function to get obligation type
     *
     * @param $code
     * @return mixed
     */
    public static function obligationType($code)
    {
        $obligationType = DB::table(ReferenceTable::REFERENCE_OBLIGATION_TYPE)
            ->select('reference_obligation_budget_line.id', 'reference_obligation_creation_type.obligation_creation_type')
            ->where('reference_obligation_budget_line.code', $code)
            ->join('reference_obligation_creation_type', 'reference_obligation_creation_type.id', '=', 'reference_obligation_budget_line.obligation_creation_type')
            ->first();

        return $obligationType->obligation_creation_type;
    }

}
