<?php

namespace App\Models\RegionalOffice;

use App\Http\Controllers\Core\DataTableSearch;
use App\Models\Agency\Agency;
use Illuminate\Support\Facades\DB;

/**
 * Class RegionalOfficerSearch
 * @package App\Models\RegionalOffice
 */
class RegionalOfficerSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->rowsCountAll();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->rowsCountAll();
    }

    /**
     * @return int
     */
    private function rowsCountAll()
    {
        $query = $this->constructQuery();
        $result = DB::select("SELECT count(*) as cnt FROM ({$query->toSql()}) as temp", $query->getBindings());

        return $result[0]->cnt;
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);

        $results = $query->get();

        foreach ($results as &$result) {
            $constructorDocumentNames = [];
            if (!is_null($result->constructorDocuments)) {
                foreach ($result->constructorDocuments as $constructorDocument) {
                    $constructorDocumentNames[] = $constructorDocument->current()->document_name;
                }
            }

            $result->constructor_document_name = implode(', ', $constructorDocumentNames);
        }

        return $results;
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $orderByType = 'desc';
        if ($this->orderByType == 'asc') {
            $orderByType = 'asc';
        }
        $query->orderBy($this->orderByCol, $orderByType);
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $query = RegionalOffice::select('regional_office_ml.name', 'regional_office.show_status', 'regional_office.office_code', 'regional_office_ml.address', 'regional_office.email', 'regional_office.id', 'agency_ml.name as agency_name')
            ->joinMl()
            ->join('agency', 'regional_office.company_tax_id', '=', 'agency.tax_id')
            ->join('agency_ml', 'agency.id', '=', 'agency_ml.agency_id')
            ->join('regional_office_constructor_documents', 'regional_office.id', '=', 'regional_office_constructor_documents.regional_office_id')
            ->where('agency_ml.lng_id', '=', cLng('id'))
            ->where('agency.show_status', '=', Agency::STATUS_ACTIVE)
            ->groupBy('regional_office_ml.regional_office_id', 'regional_office_ml.lng_id', 'regional_office_ml.address', 'regional_office_ml.name', 'regional_office.id', 'agency_ml.name')
            ->notDeleted();

        if (isset($this->searchData['id'])) {
            $query->where(DB::raw("regional_office.id::varchar"), $this->searchData['id']);
        }

        if (isset($this->searchData['office_code'])) {
            $query->where('regional_office.office_code', 'ILIKE', '%' . strtolower(trim($this->searchData['office_code'])) . '%');
        }

        if (isset($this->searchData['address'])) {
            $query->where('regional_office.address', 'ILIKE', '%' . strtolower(trim($this->searchData['address'])) . '%');
        }

        if (isset($this->searchData['region'])) {
            $query->where('regional_office.region', 'ILIKE', '%' . strtolower(trim($this->searchData['region'])) . '%');
        }

        if (isset($this->searchData['city'])) {
            $query->where('regional_office.city', 'ILIKE', '%' . strtolower(trim($this->searchData['city'])) . '%');
        }

        if (isset($this->searchData['show_status'])) {
            $query->where('regional_office.show_status', $this->searchData['show_status']);
        }

        if (isset($this->searchData['name'])) {
            $query->where('regional_office_ml.name', 'ILIKE', '%' . strtolower(trim($this->searchData['name'])) . '%');
        }

        if (isset($this->searchData['agency'])) {
            $query->where('regional_office.company_tax_id', $this->searchData['agency']);
        }

        if (isset($this->searchData['constructor_document'])) {
            $query->where(DB::raw("regional_office_constructor_documents.constructor_document_code"), $this->searchData['constructor_document']);
        }

        $query->checkUserHasRole();

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}
