<?php

namespace App\Models\RegionalOffice;

use App\Models\BaseModel;

/**
 * Class RegionalOfficeLaboratory
 * @package App\Models\RegionalOffice
 */
class RegionalOfficeLaboratory extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'regional_office_laboratory';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'regional_office_id',
        'laboratory_id',
        'start_date',
        'end_date',
        'regional_office_code',
        'agency_laboratory_id'
    ];

}