<?php

namespace App\Models\RegionalOffice;

use App\Models\BaseModel;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\RegionalOfficeConstructorDocuments\RegionalOfficeConstructorDocuments;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

/**
 * Class RegionalOffice
 * @package App\Models\RegionalOffice
 */
class RegionalOffice extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'regional_office';

    /**
     * @var bool
     */
    protected $hasAdminInfo = true;

    /**
     * @var array
     */
    protected $casts = [
        'constructor_document' => 'array'
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'company_tax_id',
        'user_id',
        'office_code',
        'accreditation_number',
        'accreditation_date_of_issue',
        'accreditation_valid_until',
        'address',
        'phone_number',
        'email',
        'show_status',
        'region',
        'city',
        'area',
        'street',
        'fax',
        'sort',
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'created_admin_id',
        'created_admin_ip',
        'updated_at',
        'updated_admin_id',
        'updated_admin_ip',
        'deleted_admin_id',
        'deleted_admin_ip'
    ];

    /**
     * Function to relate with RegionalOfficeMl
     *
     * @return HasMany
     */
    public function ml()
    {
        return $this->hasMany(RegionalOfficeMl::class, 'regional_office_id', 'id');
    }

    /**
     * Function to relate with RegionalOfficeMl current
     *
     * @return HasOne
     */
    public function current()
    {
        return $this->hasOne(RegionalOfficeMl::class, 'regional_office_id', 'id')->notDeleted()->where('lng_id', cLng('id'))->first();
    }

    /**
     * Function to relate with RegionalOfficeMl current
     *
     * @return HasManyThrough
     */
    public function constructorDocuments()
    {
        return $this->hasManyThrough(ConstructorDocument::class, RegionalOfficeConstructorDocuments::class, 'regional_office_id', 'document_code', 'id', 'constructor_document_code')->where('constructor_documents.show_status', RegionalOffice::STATUS_ACTIVE)->orderByAsc();
    }

    /**
     * Function to relate with RegionalOfficeLaboratory
     *
     * @return HasMany
     */
    public function laboratories()
    {
        return $this->hasMany(RegionalOfficeLaboratory::class, 'regional_office_id', 'id');
    }

    /**
     * Function to get agency ref subdivisions
     *
     * @return array|Collection|null
     */
    public static function getRefSubdivisions()
    {
        if (!Auth::user()->isSwAdmin()) {
            return getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS, false, false, ['id', 'name', 'short_name'], 'get', false, false, ['company_tax_id' => Auth::user()->companyTaxId()]);
        } else {
            return getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS);
        }
    }

    /**
     * Function to get Subdivisions by constructor document
     *
     * @param $documentCode
     * @return Collection
     */
    public static function getRefSubdivisionsByConstructorDocument($documentCode)
    {
        $subDivisions = RegionalOffice::whereHas('constructorDocuments', function ($q) use ($documentCode) {
            $q->where('constructor_document_code', $documentCode);
        })->active()->pluck('office_code')->toArray();

        if (count($subDivisions)) {
            return $refAgencySubDivisions = getReferenceRowsWhereIn(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS, [], $subDivisions, ['id', 'name', 'code', 'short_name'], true, 'sort');
        }

        return collect();
    }
}
