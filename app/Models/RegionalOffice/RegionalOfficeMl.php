<?php

namespace App\Models\RegionalOffice;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

/**
 * Class RegionalOfficeMl
 * @package App\Models\RegionalOffice
 */
class RegionalOfficeMl extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['regional_office_id', 'lng_id'];

    /**
     * @var string
     */
    public $incrementing = 'false';

    /**
     * @var string
     */
    public $table = 'regional_office_ml';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'regional_office_id',
        'lng_id',
        'name',
        'short_name',
        'address',
        'supervisor',
        'department_head',
        'description',
        'show_status'
    ];
}
