<?php

namespace App\Models\RegionalOffice;

use App\Models\BaseMlManager;
use App\Models\BaseModel;
use App\Models\Laboratory\Laboratory;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\ReferenceTableForm\ReferenceTableFormManager;
use App\Models\RegionalOfficeConstructorDocuments\RegionalOfficeConstructorDocuments;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class RegionalOfficeManager
 * @package App\Models\RegionalOffice
 */
class RegionalOfficeManager
{
    use BaseMlManager;

    /**
     * Function to store Validated Data
     *
     * @param array $data
     * @return RegionalOffice
     */
    public function store(array $data)
    {
        $data['user_id'] = Auth::id();
        if (!Auth()->user()->isSwAdmin()) {
            $data['company_tax_id'] = Auth::user()->companyTaxId();
        }

        $regionalOffice = new RegionalOffice($data);
        $regionalOfficeMl = new RegionalOfficeMl();

        // Synchronize Subdivisions module with Reference Subdivisions
        $activeRefAgencyRow = DB::table(ReferenceTable::REFERENCE_AGENCY)->select('id', 'show_status')->where(['code' => $data['company_tax_id'], 'show_status' => ReferenceTable::STATUS_ACTIVE])->orderBy('id', 'DESC')->first();

        $referenceData = [];
        if (!is_null($activeRefAgencyRow)) {
            $referenceData = [
                'code' => $data['office_code'],
                'show_status' => $data['show_status'],
                'start_date' => currentDate(),
                'agency' => $activeRefAgencyRow->id,
                'company_tax_id' => $data['company_tax_id'],
                'sort' => $data['sort']
            ];

            foreach ($data['ml'] as $lngId => $ml) {
                $referenceData['ml'][$lngId] = [
                    'name' => $ml['name'],
                    'short_name' => $ml['short_name'],
                ];
            }

            if ($data['show_status'] == ReferenceTable::STATUS_INACTIVE) {
                $referenceData['end_date'] = currentDate();
            }
        }

        return DB::transaction(function () use ($data, $regionalOffice, $regionalOfficeMl, $referenceData, $activeRefAgencyRow) {

            $regionalOffice->save();
            $this->storeMl($data['ml'], $regionalOffice, $regionalOfficeMl);

            // ---
            $this->storeRegionalOfficeLaboratories($data, $regionalOffice);

            // ---
            $this->storeConstructorDocuments($regionalOffice, $data['constructor_document']);

            // ----
            if (!is_null($activeRefAgencyRow)) {
                $referenceTableFormManager = new ReferenceTableFormManager();
                $referenceTableFormManager->store($referenceData, ReferenceTable::getReferenceIdByTableName(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME));
            }

            return $regionalOffice;
        });
    }

    /**
     * Function to Update data
     *
     * @param array $data
     * @param int $id
     */
    public function update(array $data, int $id)
    {
        $regionalOffice = RegionalOffice::where('id', $id)->checkUserHasRole()->firstOrFail();
        $regionalOfficeMl = new RegionalOfficeMl();

        // Synchronize Subdivisions module with Reference Subdivisions
        $currentReferenceRow = DB::table(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS)->select('id', 'show_status')->where(['code' => $regionalOffice->office_code])->orderBy('id', 'DESC')->first();

        $referenceData = [];
        if (!is_null($currentReferenceRow)) {

            if (!Auth()->user()->isSwAdmin()) {
                $data['company_tax_id'] = Auth::user()->companyTaxId();
            }

            $referenceData = [
                'start_date' => currentDate(),
                'show_status' => $data['show_status'],
                'company_tax_id' => $data['company_tax_id'],
                'sort' => $data['sort']
            ];

            if ($data['show_status'] == ReferenceTable::STATUS_INACTIVE) {
                $referenceData['end_date'] = currentDate();
                $referenceData['f_id'] = $currentReferenceRow->id;
            } else {

                if (!empty($data['company_tax_id'])) {
                    $activeRefAgencyRow = DB::table(ReferenceTable::REFERENCE_AGENCY)->select('id', 'show_status')->where(['code' => $data['company_tax_id'], 'show_status' => ReferenceTable::STATUS_ACTIVE])->orderBy('id', 'desc')->first();
                    $referenceData['agency'] = $activeRefAgencyRow->id;
                }

                foreach ($data['ml'] as $lngId => $ml) {
                    $referenceData['ml'][$lngId] = [
                        'name' => $ml['name'],
                        'short_name' => $ml['short_name'],
                    ];
                }
            }
        }

        DB::transaction(function () use ($data, $regionalOffice, $regionalOfficeMl, $currentReferenceRow, $referenceData) {

            if ($data['show_status'] == ReferenceTable::STATUS_INACTIVE || $regionalOffice->show_status == ReferenceTable::STATUS_INACTIVE) {
                $regionalOffice->update(['show_status' => $data['show_status']]);
            } else {

                $regionalOffice->update($data);
                $this->updateMl($data['ml'], 'regional_office_id', $regionalOffice, $regionalOfficeMl);

                // ---
                RegionalOfficeLaboratory::where('regional_office_id', $regionalOffice->id)->delete();

                $this->storeRegionalOfficeLaboratories($data, $regionalOffice);

                // ---
                $this->updateConstructorDocuments($regionalOffice, $data['constructor_document']);
            }

            // ----
            if (!is_null($currentReferenceRow)) {
                $referenceTableFormManager = new ReferenceTableFormManager();
                $referenceTableFormManager->update($referenceData, ReferenceTable::getReferenceIdByTableName(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME), $currentReferenceRow->id);
            }
        });
    }

    /**
     * Function to store laboratories
     *
     * @param $data
     * @param $regionalOffice
     * @return void
     */
    private function storeRegionalOfficeLaboratories($data, $regionalOffice)
    {
        if (isset($data['laboratories'])) {

            $laboratories = Laboratory::select('laboratory.id', 'agency.id as agency_id')
                ->join('agency', function ($q) {
                    $q->on('agency.tax_id', '=', 'laboratory.tax_id')->where('agency.show_status', BaseModel::STATUS_ACTIVE);
                })
                ->whereIn('laboratory.id', array_keys($data['laboratories'] ?? []))
                ->pluck('agency_id', 'id')
                ->all();

            foreach ($data['laboratories'] as $labId => $laboratory) {
                if (isset($laboratory['enable'])) {
                    RegionalOfficeLaboratory::create([
                        'regional_office_id' => $regionalOffice->id,
                        'laboratory_id' => $labId,
                        'start_date' => $laboratory['start_date'] ?? null,
                        'end_date' => $laboratory['end_date'] ?? null,
                        'regional_office_code' => $regionalOffice->office_code,
                        'agency_laboratory_id' => $laboratories[$labId] ?? null
                    ]);
                }
            }
        }
    }

    /**
     * Function to save Constructor Documents Codes
     *
     * @param $regionalOffice
     * @param $documentCodes
     * @return void
     */
    private function storeConstructorDocuments($regionalOffice, $documentCodes)
    {
        if (count($documentCodes)) {

            $constructorDocuments = [];
            foreach ($documentCodes as $code) {
                $constructorDocuments[] = [
                    'constructor_document_code' => $code,
                    'regional_office_id' => $regionalOffice->id,
                ];
            }

            RegionalOfficeConstructorDocuments::insert($constructorDocuments);
        }
    }

    /**
     * Function to save Constructor Documents Codes
     *
     * @param $regionalOffice
     * @param $documentCodes
     * @return void
     */
    private function updateConstructorDocuments($regionalOffice, $documentCodes)
    {
        $notDeletedCodes = [];
        foreach ($documentCodes as $code) {

            $notDeletedCodes[] = $code;
            $regConstructorData = [
                'regional_office_id' => $regionalOffice->id,
                'constructor_document_code' => $code,
            ];

            RegionalOfficeConstructorDocuments::updateOrCreate($regConstructorData, $regConstructorData);
        }

        RegionalOfficeConstructorDocuments::where('regional_office_id', $regionalOffice->id)->whereNotIn('constructor_document_code', $notDeletedCodes)->delete();
    }

    /**
     * Function to inactivating Regional offices
     *
     * @return void
     */
    public function inactivateRegionalOffice()
    {
        // If regional office accreditation_valid_until < today  inactivate regional row
        $regionalOffices = RegionalOffice::select('id', 'office_code')->active()
            ->where('accreditation_valid_until', '<', currentDateTime())->whereNotNull('accreditation_valid_until')->get();

        foreach ($regionalOffices as $regionalOffice) {

            RegionalOffice::where('id', $regionalOffice->id)->update(['show_status' => RegionalOffice::STATUS_INACTIVE]);

            DB::table(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS)
                ->where(['code' => $regionalOffice->office_code, 'show_status' => RegionalOffice::STATUS_ACTIVE])
                ->update(['end_date' => Carbon::now(), 'show_status' => RegionalOffice::STATUS_INACTIVE]);
        }
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @param null $parentId
     * @return array
     */
    public function getLogData($id = null, $parentId = null)
    {
        if (!is_null($id)) {
            $data = RegionalOffice::where('id', $id)->first();
            $logData = $data;
            $logData['mls'] = $data->ml()->get()->toArray();

            return $logData;
        }

        return [];
    }
}
