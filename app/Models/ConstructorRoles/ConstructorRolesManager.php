<?php

namespace App\Models\ConstructorRoles;

use App\Models\ConstructorDocumentsRoles\ConstructorDocumentsRoles;
use App\Models\ConstructorDocumentsRolesAttribute\ConstructorDocumentsRolesAttribute;
use App\Models\Role\Role;
use Illuminate\Support\Facades\DB;

/**
 * Class ConstructorRolesManager
 * @package App\Models\ConstructorRoles
 */
class ConstructorRolesManager 
{
    /**
     * Function to Store data to DB
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $documentRolesInsertArray = $this->getDocumentRolesInsertArray($data);
        
        DB::transaction(function () use ($data, $documentRolesInsertArray) {
            ConstructorDocumentsRoles::firstOrCreate([
                'document_id' => $data['documentID'],
                'role_id'     => Role::HEAD_OF_COMPANY,
            ]);

            $constructorDocumentsRoles = ConstructorDocumentsRoles::where('document_id', $data['documentID'])->get();
            foreach ($constructorDocumentsRoles as $item) {
                ConstructorDocumentsRolesAttribute::where('constructor_documents_roles_id', $item->id)->delete();
            }
            ConstructorDocumentsRoles::where('document_id', $data['documentID'])->where('role_id', '!=', Role::HEAD_OF_COMPANY)->delete();

            foreach ($documentRolesInsertArray as $item) {
                $constructorDocumentRoles = new ConstructorDocumentsRoles($item);
                $constructorDocumentRoles->save();

                $documentRolesAttributeInsertArray = $this->getDocumentRolesAttributeInsertArray($constructorDocumentRoles, $data);
                foreach ($documentRolesAttributeInsertArray as $insertArray) {
                    $constructorDocumentsRolesAttribute = new ConstructorDocumentsRolesAttribute($insertArray);
                    $constructorDocumentsRolesAttribute->save();
                }
            }
        });
    }

    /**
     * Function to get inserted array for constructor_documents_roles
     *
     * @param $data
     * @return array
     */
    private function getDocumentRolesInsertArray($data)
    {
        $insertData = [];
        if (isset($data['roles']) && is_array($data['roles'])) {
            foreach ($data['roles'] as $roleID) {
                $insertData[] = [
                    'document_id' => $data['documentID'],
                    'role_id'     => $roleID,
                ];
            }
        }

        return $insertData;
    }

    /**
     * Function to get inserted array for constructor_documents_roles_attribute
     *
     * @param $constructorDocumentRoles
     * @param $data
     * @return array
     */
    private function getDocumentRolesAttributeInsertArray($constructorDocumentRoles, $data)
    {
        $insertData = [];
        if (isset($data['attributes'], $data['attributes'][$constructorDocumentRoles->role_id]) && is_array($data['attributes'][$constructorDocumentRoles->role_id])) {
            foreach ($data['attributes'][$constructorDocumentRoles->role_id] as $type => $attribute) {
                if (is_array($attribute)) {
                    foreach ($attribute as $attrID => $value) {
                        $insertData[] = [
                            'constructor_documents_roles_id'    => $constructorDocumentRoles->id,
                            'type'                              => $type,
                            'field_id'                          => $attrID,
                        ];
                    }
                }
            }
        }

        return $insertData;
    }
}
