<?php

namespace App\Models\ConstructorRoles;

use App\Models\BaseModel;
use App\Models\ConstructorDocument\ConstructorDocument;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class ConstructorRoles
 * @package App\Models\ConstructorRoles
 */
class ConstructorRoles extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'constructor_roles';

    /**
     * @var bool
     */
    protected $hasAdminInfo = true;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'constructor_document_id',
        'role_id',
        'show_status'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'created_admin_id',
        'created_admin_ip',
        'updated_at',
        'updated_admin_id',
        'updated_admin_ip',
        'deleted_admin_id',
        'deleted_admin_ip'
    ];

    /**
     * Function to relate with ConstructorRolesMl
     *
     * @return HasMany
     */
    public function ml()
    {
        return $this->hasMany(ConstructorRolesMl::class, 'constructor_roles_id', 'id');
    }

    /**
     * Function to relate with ConstructorRolesMl
     *
     * @return mixed
     */
    public function current()
    {
        return $this->hasOne(ConstructorRolesMl::class, 'constructor_roles_id', 'id')->notDeleted()->where('lng_id', cLng('id'))->first();
    }

    /**
     * Function to check auth user has role
     *
     * @param $query
     * @return mixed
     */
    public function scopeConstructorRolesOwner($query)
    {
        $agencyDocumentIds = ConstructorDocument::getConstructorDocumentForAgency()->pluck('id')->toArray();

        return $query->whereIn($this->getTable() . '.constructor_document_id', $agencyDocumentIds ?? -1);
    }
}
