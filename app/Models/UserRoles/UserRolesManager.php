<?php

namespace App\Models\UserRoles;

use App\Models\BaseModel;
use App\Models\Menu\Menu;
use App\Models\Notifications\NotificationsManager;
use App\Models\Role\Role;
use App\Models\UserCompanies\UserCompanies;
use App\Models\UserRolesMenus\UserRolesMenus;
use App\Models\UserRolesMenus\UserRolesMenusManager;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class UserRolesManager
 * @package App\Models\UserRoles
 */
class UserRolesManager extends BaseModel
{
    /**
     * Function to add default role to new registration user
     *
     * @param int $user_id
     * @param array $params
     */
    public static function addDefaultRole(int $user_id, $params = [])
    {
        $defaultData = [
            'user_id' => $user_id,
            'role_id' => Role::NATURAL_PERSON,
            'user_status' => Role::STATUS_ACTIVE,
            'is_local_admin' => '0',
            'show_only_self' => '0',
            'show_status' => Role::STATUS_ACTIVE,
            'authorized_by_id' => null,
            'role_type' => Role::ROLE_TYPE_NATURAL_PERSON
        ];

        $data = array_merge($defaultData, $params);

        UserRoles::create($data);
    }

    /**
     * Function to add default role to new registration user
     *
     * @param $user_id
     * @param $params
     */
    public static function addCustomsOperatorRole($user_id, $params = [])
    {
        $customsRoleData = [
            'user_id' => $user_id,
            'role_id' => Role::CUSTOMS_OPERATOR,
            'user_status' => Role::STATUS_ACTIVE,
            'menu_id' => Menu::getMenuIdByName(Menu::CUSTOMS_PORTAL_MENU_NAME),
            'is_local_admin' => '0',
            'show_only_self' => '0',
            'show_status' => Role::STATUS_ACTIVE,
            'authorized_by_id' => Auth::id(),
            'role_type' => Role::ROLE_TYPE_CUSTOMS_OPERATOR
        ];

        $data = array_merge($customsRoleData, $params);

        UserRoles::create($data);
    }

    /**
     * Function to Store roles data to DB
     *
     * @param array $data
     */
    public function store(array $data)
    {
        if (isset($data['data']) && count($data['data'])) {

            if (Auth::user()->isSwAdmin()) {
                $this->authorizeSwAdminUsersRoles($data);
            } else {
                $this->authorizeUsersRoles($data);
            }
        }
    }

    /**
     * Function to authorize users roles
     *
     * @param $data
     */
    private function authorizeUsersRoles($data)
    {
        DB::transaction(function () use ($data) {

            $user_id = $data['id'];

            $changed_modules = $notificationMenus = [];
            if (!is_null($data['changed_modules'])) {
                $changed_modules = explode(',', $data['changed_modules']);
            }

            $company_tax_id = Auth::user()->companyTaxId();
            $company_id = Auth::user()->getCurrentUserRole()->company_id;

            if ($company_id) {
                $user_company = [
                    'user_id' => $user_id,
                    'company_id' => $company_id
                ];

                UserCompanies::updateOrCreate($user_company, $user_company);
            }

            foreach ($data['data'] as $value) {

                $userRole = UserRoles::select('id', 'role_status', 'available_at')->where('id', $value['id'])->first();

                if (!is_null($userRole) && $userRole->role_status != Role::STATUS_DELETED) {

                    $updateData = [
                        'role_status' => $value['role_status'],
                        'available_to' => $value['available_to'] ?? null,
                        'show_only_self' => $value['show_only_self'],
                        'ip_access' => $value['ip_access'],
                    ];

                    if ($userRole->getOriginal('available_at') >= currentDate()) {
                        $updateData['available_at'] = $value['available_at'];
                    }

                    if($value['role_status'] == Role::STATUS_DELETED){
                        $updateData['available_to'] = currentDate();
                    }

                    $userRole->update($updateData);

                } else {

                    if($value['role_status'] == Role::STATUS_DELETED){
                        $value['available_to'] = currentDate();
                    }

                    // Add My Permission
                    if ($value['role'] == Role::AUTHORIZE_USER_ADD_MY_PERMISSIONS) {
                        $value['menu'] = null;
                        $value['show_only_self'] = UserRoles::FALSE;
                        $value['role'] = Role::HEAD_OF_COMPANY;

                        // Natural Person
                        if (Auth::user()->isNaturalPerson()) {
                            $value['role'] = Role::NATURAL_PERSON;
                            $company_id = '0';
                        }
                    }

                    if (!is_null($value['menu']) && $value['menu']) {
                        array_push($changed_modules, $value['menu']);
                    }

                    $authorizedByUserType = null;
                    if (Auth::user()->isLocalAdmin()) {
                        $authorizedByUserType = UserRoles::AUTHORIZED_BY_USER_TYPE_COMPANY_LOCAL_ADMIN;
                    }

                    if (Auth::user()->isAuthorizedEmployeeForCompany()) {
                        $authorizedByUserType = UserRoles::AUTHORIZED_BY_USER_TYPE_COMPANY_EMPLOYEE;
                    }

                    $insertData = [
                        'menu_id' => $value['menu'],
                        'user_id' => $user_id,
                        'role_id' => isset($value['role']) ? $value['role'] : 0,
                        'role_group_id' => isset($value['role_group_id']) ? $value['role_group_id'] : 0,
                        'attribute_id' => isset($value['attribute']) ? $value['attribute'] : null,
                        'attribute_field_id' => isset($value['attribute_field_id']) ? $value['attribute_field_id'] : null,
                        'attribute_type' => isset($value['attribute_type']) ? $value['attribute_type'] : null,
                        'attribute_value' => isset($value['attribute_value']) ? $value['attribute_value'] : null,
                        'show_only_self' => $value['show_only_self'],
                        'company_tax_id' => $company_tax_id,
                        'company_id' => $company_id,
                        'authorized_by_id' => Auth::id(),
                        'authorized_by_id_type' => $authorizedByUserType,
                        'role_status' => $value['role_status'],
                        'ip_access' => $value['ip_access'],
                        'available_at' => $value['available_at'],
                        'available_to' => isset($value['available_to']) ? $value['available_to'] : null,
                        'role_type' => Auth::user()->getCurrentUserRole()->role_type,
                        'show_status' => Role::STATUS_ACTIVE,
                    ];

                    if (Menu::getMenuIdByName(Menu::REPORTS_MENU_NAME) == $value['menu']) {
                        $insertData['multiple_attributes'] = $value['multiple_attributes'] ?? null;
                    }

                    //
                    UserRoles::create($insertData);

                    if (!is_null($value['menu'])) {
                        $notificationMenus[] = $value['menu'];
                    }
                }
            }

            //
            $userRolesMenusManager = new UserRolesMenusManager();
            $userRolesMenusManager->updateUserRoleMenus(UserRolesMenus::ROLE_TYPE_USER_ROLE, $company_tax_id, $user_id, $changed_modules);

            if (in_array(Menu::getMenuIdByName(Menu::AUTHORIZE_PERSON_MENU_NAME), $changed_modules)) {
                $userRolesMenusManager->updateUserRoleMenus(UserRolesMenus::ROLE_TYPE_AGENCY, $company_tax_id, [$user_id]);
            }

            if (count($notificationMenus)) {

                $notificationData = [
                    'authorized_user' => $user_id,
                    'company_tax_id' => $company_tax_id,
                    'modules' => array_unique($notificationMenus)
                ];

                //
                $notificationsManager = new NotificationsManager();
                $notificationsManager->userAuthorizationFromAuthorizeModuleNotification($notificationData);
            }

        });
    }

    /**
     * Function to Store SW Admin roles data to DB
     *
     * @param $data
     */
    private function authorizeSwAdminUsersRoles($data)
    {
        DB::transaction(function () use ($data) {

            $user_id = $data['id'];

            $changed_modules = [];
            if (!is_null($data['changed_modules'])) {
                $changed_modules = explode(',', $data['changed_modules']);
            }

            foreach ($data['data'] as $value) {

                $userRole = UserRoles::select('id', 'role_status', 'available_at')->where('id', $value['id'])->first();

                if (!is_null($userRole) && $userRole->role_status != Role::STATUS_DELETED) {

                    $updateData = [
                        'role_status' => $value['role_status'],
                        'available_to' => $value['available_to'] ?? null,
                        'show_only_self' => $value['show_only_self'],
                        'ip_access' => $value['ip_access'],
                    ];

                    if ($userRole->getOriginal('available_at') >= currentDate()) {
                        $updateData['available_at'] = $value['available_at'];
                    }

                    if($value['role_status'] == Role::STATUS_DELETED){
                        $updateData['available_to'] = currentDate();
                    }

                    $userRole->update($updateData);

                } else {

                    if($value['role_status'] == Role::STATUS_DELETED){
                        $value['available_to'] = currentDate();
                    }

                    if (!is_null($value['menu']) && $value['menu']) {
                        array_push($changed_modules, $value['menu']);
                    }

                    $insertData = [
                        'menu_id' => !empty($value['menu']) ? $value['menu'] : 0,
                        'user_id' => $user_id,
                        'role_id' => $value['role'],
                        'show_only_self' => $value['show_only_self'],
                        'authorized_by_id' => Auth::id(),
                        'role_status' => $value['role_status'],
                        'ip_access' => $value['ip_access'],
                        'available_at' => $value['available_at'],
                        'available_to' => !empty($value['available_to']) ? $value['available_to'] : null,
                        'role_type' => Role::ROLE_TYPE_SW_ADMIN,
                        'show_status' => Role::STATUS_ACTIVE
                    ];

                    UserRoles::create($insertData);
                }
            }

            //
            $userRolesMenusManager = new UserRolesMenusManager();
            $userRolesMenusManager->updateUserRoleMenus(UserRolesMenus::ROLE_TYPE_SW_ADMIN, self::FALSE, $user_id, $changed_modules);

        });
    }
}
