<?php

namespace App\Models\UserRoles;

use App\Models\Agency\Agency;
use App\Models\BaseModel;
use App\Models\Company\Company;
use App\Models\Menu\Menu;
use App\Models\Role\Role;
use App\Models\RolesGroup\RolesGroup;
use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class UserRoles
 * @package App\Models\UserRoles
 */
class UserRoles extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'user_roles';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'menu_id',
        'role_id',
        'role_group_id',
        'company_id',
        'company_tax_id',
        'broker_id',
        'laboratory_id',
        'attribute_id',
        'attribute_field_id',
        'attribute_type',
        'attribute_value',
        'multiple_attributes',
        'available_at',
        'available_to',
        'show_only_self',
        'role_status',
        'show_status',
        'is_local_admin',
        'authorized_by_id',
        'authorized_by_admin_id',
        'origin',
        'ip_access',
        'authorized_by_id_type',
        'role_type'
    ];

    /**
     * @var string
     */
    const AUTHORIZED_BY_USER_TYPE_COMPANY_LOCAL_ADMIN = 'local_admin';
    const AUTHORIZED_BY_USER_TYPE_COMPANY_EMPLOYEE = 'employee';

    /**
     * @var array
     */
    protected $casts = [
        'multiple_attributes' => 'array',
    ];

    /**
     * Function to get available to value
     *
     * @param $value
     * @return string|void
     */
    public function getAvailableToAttribute($value)
    {
        if ($value) {
            return Carbon::parse($value)->format(config('swis.date_format_front'));
        }
    }

    /**
     * Function to get available at value
     *
     * @param $value
     * @return string|void
     */
    public function getAvailableAtAttribute($value)
    {
        if ($value) {
            return Carbon::parse($value)->format(config('swis.date_format_front'));
        }
    }

    /**
     * Function to relate with Role
     *
     * @return HasOne
     */
    public function role()
    {
        return $this->hasOne(Role::class, 'id', 'role_id');
    }

    /**
     * Function to relate with RolesGroup
     *
     * @return HasOne
     */
    public function roleGroup()
    {
        return $this->hasOne(RolesGroup::class, 'id', 'role_group_id');
    }

    /**
     * Function to relate with Company
     *
     * @return HasOne
     */
    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    /**
     * Function to relate with Agency
     *
     * @return HasOne
     */
    public function agency()
    {
        return $this->hasOne(Agency::class, 'tax_id', 'company_tax_id')->active();
    }

    /**
     * Function to relate with Menu
     *
     * @return HasOne
     */
    public function menu()
    {
        return $this->hasOne(Menu::class, 'id', 'menu_id');
    }

    /**
     * Function to relate with User
     *
     * @return HasOne
     */
    public function authorizer()
    {
        return $this->hasOne(User::class, 'id', 'authorized_by_id');
    }

    /**
     * Function to scope Custom Roles
     *
     * @param $query
     */
    public function scopeActiveRole($query)
    {
        $now = currentDate();
        $query->where('user_roles.role_status', Role::STATUS_ACTIVE)
            ->where(function ($q) use ($now) {
                $q->orWhereDate('user_roles.available_at', '<=', $now)->orWhereNull('user_roles.available_at');
            })
            ->where(function ($q) use ($now) {
                $q->orWhereDate('user_roles.available_to', '>=', $now)->orWhereNull('user_roles.available_to');
            });
    }

}