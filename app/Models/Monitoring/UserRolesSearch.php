<?php

namespace App\Models\Monitoring;

use App\Http\Controllers\Core\DataTableSearch;
use App\Models\Agency\Agency;
use App\Models\Role\Role;
use App\Models\UserRoles\UserRoles;
use Illuminate\Support\Facades\DB;

/**
 * Class UserSearch
 * @package App\Models\User
 */
class UserRolesSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        $data = $query->get();

        foreach ($data as &$userRole) {
            $userRole->module = $userRole->module ? trans($userRole->module) : '';
            $userRole->show_only_self = $userRole->show_only_self ? trans("swis.role.show_only_self.only_self") : trans("swis.role.show_only_self.others_too");

            $userRole->subdivision = '';
            if (is_null($userRole->company_name) && $userRole->role_id == Role::SW_ADMIN) {
                $userRole->company_name = Agency::SW_ADMIN_AGENCY_NAME . ' (' . Agency::SW_ADMIN_AGENCY_TAX_ID . ')';
                $userRole->subdivision = Agency::SW_ADMIN_AGENCY_SUBDIVISION;
            }

            $roleName = '';
            if ($userRole->role_id) {
                $roleName = $userRole->role->name;

                if ($userRole->role_id == Role::HEAD_OF_COMPANY) {
                    $company = $userRole->company;
                    $roleName .= ' (' . optional($company)->name . ')';
                }
            }

            if ($userRole->role_group_id) {
                $roleGroupRoles = [];
                if (!is_null($userRole->roleGroup) && !is_null($userRole->roleGroup->roles)) {
                    foreach ($userRole->roleGroup->roles as $role) {
                        $roleGroupRoles[] = $role->name;
                    }
                    $roleName = implode(', ', $roleGroupRoles);
                }
            }

            $userRole->role_name = $roleName;

        }

        return $data;
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($this->orderByCol, $this->orderByType ?? 'desc');
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $query = UserRoles::select('user_roles.id', 'user_roles.role_id', 'user_roles.role_group_id', 'user_roles.company_id',
            'user_roles.show_only_self', 'user_roles.available_at', 'user_roles.available_to', 'user_roles.role_status',
            'menu.title as module',
            'company.name as company_name',
            DB::raw("users.first_name || ' ' || users.last_name || ' (' || users.ssn || ')' AS username"),
            DB::raw("authorized_users.first_name || ' ' || authorized_users.last_name || ' (' || authorized_users.ssn || ')' as authorized_user"))
            ->join('users', 'user_roles.user_id', '=', 'users.id')
            ->leftJoin('users as authorized_users', 'user_roles.authorized_by_id', '=', 'authorized_users.id')
            ->leftJoin('menu', 'user_roles.menu_id', '=', 'menu.id')
            ->leftJoin('company', 'user_roles.company_tax_id', '=', 'company.tax_id')
            ->whereNotNull('user_roles.updated_at')
            ->where('role_id', '!=', Role::NATURAL_PERSON)
            ->orderBy('user_roles.updated_at', 'DESC');

        if (isset($this->searchData['id'])) {
            $query->where(DB::raw("user_roles.id::varchar"), $this->searchData['id']);
        }

        if (isset($this->searchData['ssn'])) {
            $query->where('users.ssn', 'ILIKE', '%' . strtolower(trim($this->searchData['ssn'])) . '%');
        }

        if (isset($this->searchData['first_name'])) {
            $query->where('users.first_name', 'ILIKE', '%' . strtolower(trim($this->searchData['first_name'])) . '%');
        }

        if (isset($this->searchData['last_name'])) {
            $query->where('users.last_name', 'ILIKE', '%' . strtolower(trim($this->searchData['last_name'])) . '%');
        }

        if (isset($this->searchData['authorized_user'])) {
            $authorizedUserName = $this->searchData['authorized_user'];
            $query->whereRaw("concat(authorized_users.first_name, ' ', authorized_users.last_name) ILIKE '%$authorizedUserName%' ");
        }

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}
