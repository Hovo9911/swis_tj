<?php

namespace App\Models\Monitoring;

use App\Http\Controllers\Core\DataTableSearch;
use App\Models\Agency\Agency;
use App\Models\BaseModel;
use App\Models\Transactions\Transactions;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * Class TransactionsSearch.phpp
 *
 * @package App\Models\User
 */
class TransactionsMonitoringSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        return $query->get();
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($this->orderByCol, $this->orderByType ?? 'desc');
    }

    /**
     * @param array $filterData
     * @return mixed
     */
    protected function constructQuery($filterData = [])
    {
        $data = count($filterData) ? $filterData : $this->searchData;

        $query = Transactions::select([
            'payments.id as id',
            DB::raw("concat(users.first_name, ' ', users.last_name) as user_name"),
            'users.ssn as user_ssn',
            'payments.document_id as transaction_id',
            'payments.payment_done_date as payment_date',
            'payments.amount',
            'payment_providers.name as provider',
            'company.tax_id as company_tax_id',
            DB::raw("COALESCE(agency_ml.name, company.name) as company_name"),
        ])
            ->leftJoin('users', 'users.id', '=', 'transactions.user_id')
            ->join('payments', 'payments.id', '=', 'transactions.payment_id')
            ->join('payment_providers', 'payment_providers.id', '=', 'payments.service_provider_id')
            ->leftJoin('company', 'company.tax_id', '=', 'transactions.company_tax_id')
            ->leftJoin('agency', function ($query) {
                $query->on('agency.tax_id', '=', 'transactions.company_tax_id')->where('agency.show_status', BaseModel::STATUS_ACTIVE);
            })
            ->leftJoin('agency_ml', function ($q) {
                $q->on('agency_ml.agency_id', '=', 'agency.id')->where('agency_ml.lng_id', cLng('id'));
            });

        // NEED for now remove agencies ,in future maybe open
        $agenciesTaxIds = Agency::active()->pluck('tax_id')->all();
        $query->whereNotIn('transactions.company_tax_id', $agenciesTaxIds);

        if (isset($data['id'])) {
            $query->where('payments.id', $data['id']);
        }

        if (isset($data['user_name'])) {
            $query->where(function ($q) use ($data) {
                foreach (explode(" ", $data['user_name']) ?? [] as $value) {
                    $q->orWhere('users.first_name', "ILIKE", "{$value}%")->orWhere('users.last_name', "ILIKE", "%{$value}");
                }
            });
        }

        if (isset($data['user_ssn'])) {
            $query->where('users.ssn', $data['user_ssn']);
        }

        if (isset($data['transaction_id'])) {
            $query->where('payments.document_id', $data['transaction_id']);
        }

        if (isset($data['payment_provider_id'])) {
            $query->where('payment_providers.id', $data['payment_provider_id']);
        }

        if (isset($data['payment_date_date_start'])) {
            $query->where(DB::raw('payments.payment_done_date::date'), '>=', Carbon::parse($data['payment_date_date_start'])->startOfDay());
        }

        if (isset($data['payment_date_date_end'])) {
            $query->where(DB::raw('payments.payment_done_date::date'), '<=',  Carbon::parse($data['payment_date_date_end'])->endOfDay());
        }

        if (isset($data['company_name'])) {
            $query->where(function ($q) use ($data) {
                $companyName = str_replace('"', "", $data['company_name']);
                $q->orWhere(DB::raw("REPLACE(agency_ml.name, '\"', '')"), 'ILIKE', "%{$companyName}%")->orWhere(DB::raw("REPLACE(company.name, '\"', '')"), 'ILIKE', "%{$companyName}%");
            });
        }

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }

    /**
     * Function to call protected ConstructorQuery
     *
     * @param $data
     * @return mixed
     */
    public function callToConstructorQuery($data)
    {
        return $this->constructQuery($data);
    }
}