<?php

namespace App\Models\Monitoring;

use App\Http\Controllers\Core\DataTableSearch;
use App\Models\User\User;
use Illuminate\Support\Facades\DB;

/**
 * Class UserSearch
 * @package App\Models\User
 */
class UserSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        return $query->get();
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($this->orderByCol, $this->orderByType ?? 'desc');
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $query = User::select('users.id', 'users.ssn', 'users.first_name', 'users.last_name', 'users.username', 'users.passport', 'users.email', 'users.phone_number', 'users.show_status', 'users.registered_at', DB::raw("status_changed_user.first_name || ' ' || status_changed_user.last_name AS status_changed_by_user"), DB::raw("authorized_users.first_name || ' ' || authorized_users.last_name AS authorized_user"))
            ->leftJoin('users as authorized_users', 'authorized_users.id', '=', 'users.registered_by_id')
            ->leftJoin('users as status_changed_user', 'status_changed_user.id', '=', 'users.status_changed_by_user_id')
            ->whereNotIn('users.ssn', User::SW_ADMIN_SSN);

        if (isset($this->searchData['id'])) {
            $query->where(DB::raw("users.id::varchar"), $this->searchData['id']);
        }

        if (isset($this->searchData['ssn'])) {
            $query->where('users.ssn', 'ILIKE', '%' . strtolower(trim($this->searchData['ssn'])) . '%');
        }

        if (isset($this->searchData['first_name'])) {
            $query->where('users.first_name', 'ILIKE', '%' . strtolower(trim($this->searchData['first_name'])) . '%');
        }

        if (isset($this->searchData['last_name'])) {
            $query->where('users.last_name', 'ILIKE', '%' . strtolower(trim($this->searchData['last_name'])) . '%');
        }

        if (isset($this->searchData['passport'])) {
            $query->where('users.passport', 'ILIKE', '%' . strtolower(trim($this->searchData['passport'])) . '%');
        }

        if (isset($this->searchData['registered_at_date_start'])) {
            $query->where('users.registered_at', '>=', trim($this->searchData['registered_at_date_start']));
        }

        if (isset($this->searchData['registered_at_date_end'])) {
            $query->where('users.registered_at', '<=', trim($this->searchData['registered_at_date_end']));
        }

        if (isset($this->searchData['authorized_user'])) {

            $authorizedName = trim($this->searchData['authorized_user']);
            $authorizedUserIds = User::whereRaw("concat(first_name, ' ', last_name) ILIKE '%$authorizedName%' ")->pluck('id')->toArray();

            $query->whereIn('users.registered_by_id', $authorizedUserIds);
        }

        if (isset($this->searchData['status_changed_by_user'])) {
            $statusChangedUserName = trim($this->searchData['status_changed_by_user']);
            $authorizedUserIds = User::whereRaw("concat(first_name, ' ', last_name) ILIKE '%$statusChangedUserName%' ")->pluck('id')->toArray();

            $query->whereIn('users.status_changed_by_user_id', $authorizedUserIds);
        }

        if (isset($this->searchData['show_status']) && $this->searchData['show_status'] != 'all') {
            $query->where('users.show_status', $this->searchData['show_status']);
        }

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}
