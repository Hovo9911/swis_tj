<?php

namespace App\Models\Monitoring;

use App\Http\Controllers\Core\DataTableSearch;
use App\Models\BaseModel;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\Transactions\Transactions;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class TransactionsSearch.phpp
 *
 * @package App\Models\User
 */
class TransactionsSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        $result = $query->get();

        foreach ($result as &$value) {
            if (is_null($value->payment_id) && $value->type == Transactions::PAYMENT_TYPE_OUTPUT) {
                $value->payment_id = 'SW' . str_pad($value->increment_id, 10, '0', STR_PAD_LEFT);
            }

            if(trim($value->obligation_type) == '()'){
                $value->obligation_type = '';
            }
        }

        return $result;
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($this->orderByCol, $this->orderByType ?? 'desc');
    }

    /**
     * @param array $filterData
     * @param bool $xlsExport
     * @return mixed
     */
    protected function constructQuery($filterData = [], $xlsExport = false)
    {
        if (count($filterData)) {
            $this->searchData = $filterData;
        }

        $companyTaxId = (Auth::user()->companyTaxId()) ? Auth::user()->companyTaxId() : 0;
        $subDivisionRef = ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS;
        $subDivisionRefMl = "{$subDivisionRef}_ml";
        $query = Transactions::select([
            'transactions.id',
            DB::raw("users.first_name || ' ' || users.last_name AS username"),
            'users.ssn as user_ssn',
            DB::raw("company.name AS company_name"),
            'transactions.payment_id',
            'transactions.transaction_date',
            'transactions.saf_number',
            'saf_sub_applications.document_number',
            'agency_ml.name as agency',
            "{$subDivisionRefMl}.name as subdivision",
            'constructor_documents_ml.document_name as document',
            DB::raw("CONCAT('(', reference_obligation_type.code , ') ', reference_obligation_type_ml.name) AS obligation_type"),
            'transactions.amount',
            'transactions.type',
            DB::raw('ROW_NUMBER() OVER() as "increment_id"'),
        ])
            ->leftJoin('company', 'company.tax_id', '=', 'transactions.company_tax_id')
            ->leftJoin('users', 'users.id', '=', 'transactions.user_id')
            ->leftJoin('saf_obligations', 'saf_obligations.id', '=', 'transactions.obligation_id')
            ->leftJoin('saf_sub_applications', 'saf_sub_applications.id', '=', 'saf_obligations.subapplication_id')
            ->leftJoin('agency', function ($q) {
                $q->on('agency.tax_id', '=', 'saf_sub_applications.company_tax_id')->where('agency.show_status', BaseModel::STATUS_ACTIVE);
            })
            ->leftJoin('agency_ml', function ($query) {
                $query->on('agency_ml.agency_id', '=', 'agency.id')->where('agency_ml.lng_id', cLng('id'));
            })
            ->leftJoin('constructor_documents', function ($query) {
                $query->on('constructor_documents.document_code', '=', 'saf_sub_applications.document_code')->where('constructor_documents.show_status', '1');
            })
            ->leftJoin('constructor_documents_ml', function ($query) {
                $query->on('constructor_documents_ml.constructor_documents_id', '=', 'constructor_documents.id')->where('constructor_documents_ml.lng_id', cLng('id'));
            })
            ->leftJoin('reference_obligation_budget_line', 'reference_obligation_budget_line.id', '=', 'saf_obligations.ref_budget_line_id')
            ->leftJoin('reference_obligation_budget_line_ml', function ($q) {
                $q->on('reference_obligation_budget_line_ml.reference_obligation_budget_line_id', '=', 'reference_obligation_budget_line.id')->where('reference_obligation_budget_line_ml.lng_id', cLng('id'));
            })
            ->leftJoin('reference_obligation_type', 'reference_obligation_type.id', '=', 'reference_obligation_budget_line.budget_line')
            ->leftJoin('reference_obligation_type_ml', function ($q) {
                $q->on('reference_obligation_type_ml.reference_obligation_type_id', '=', 'reference_obligation_type.id')->where('reference_obligation_type_ml.lng_id', cLng('id'));
            })
            ->leftJoin("{$subDivisionRef}", "{$subDivisionRef}.id", '=', "saf_sub_applications.agency_subdivision_id")
            ->leftJoin("{$subDivisionRefMl}", function ($q) use ($subDivisionRef, $subDivisionRefMl) {
                $q->on("{$subDivisionRefMl}.{$subDivisionRef}_id", '=', "{$subDivisionRef}.id")->where("{$subDivisionRefMl}.lng_id", cLng('id'));
            })
            ->leftJoin('payments', function ($q) {
                $q->on('payments.id', '=', 'transactions.payment_id');
            })
            ->leftJoin('payment_providers', function ($q) {
                $q->on('payment_providers.id', '=', 'payments.service_provider_id');
            })
            ->when($companyTaxId !== 0, function ($q) use ($companyTaxId) {
                $q->where('transactions.company_tax_id', $companyTaxId);
            });

        if (!$xlsExport) {
            $query->addSelect(
                'transactions.total_balance',
                DB::raw("CONCAT(payment_providers.name ,' - ', payment_providers.description) AS service_provider_name"));
        }

        if ($companyTaxId === 0 && !Auth::user()->isSwAdmin()) {
            $query = $query->Where('transactions.user_id', Auth::id());
        }

        if (isset($this->searchData['id'])) {
            $query->where(DB::raw("transactions.id::varchar"), $this->searchData['id']);
        }

        if (isset($this->searchData['username'])) {
            $username = $this->searchData['username'];
            $query->whereRaw("concat(users.first_name, ' ', users.last_name) ILIKE '%$username%' ");
        }

        if (isset($this->searchData['user_ssn'])) {
            $query->where('users.ssn', 'ILIKE', '%' . strtolower(trim($this->searchData['user_ssn'])) . '%');
        }

        if (isset($this->searchData['company_name'])) {
            $companyName = str_replace('"', '', $this->searchData['company_name']);
            $query->where(DB::raw("REPLACE(company.name, '\"', '')"), 'ILIKE', "%{$companyName}%");
        }

        if (isset($this->searchData['transaction_id'])) {
            $query->where(DB::raw("transactions.payment_id::varchar"), 'ILIKE', '%' . strtolower(trim($this->searchData['transaction_id'])) . '%');
        }

        if (isset($this->searchData['updated_at_date_start'])) {
            $query->where('transactions.transaction_date', '>=', trim($this->searchData['updated_at_date_start']));
        }

        if (isset($this->searchData['updated_at_date_end'])) {
            $query->where('transactions.transaction_date', '<=', trim($this->searchData['updated_at_date_end']));
        }

        if (isset($this->searchData['amount'])) {
            $query->where('transactions.amount', $this->searchData['amount']);
        }

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }

    /**
     * Function to call protected ConstructorQuery
     *
     * @param $data
     * @return mixed
     */
    public function callToConstructorQuery($data)
    {
        return $this->constructQuery($data, true);
    }
}