<?php

namespace App\Models\Monitoring;

use App\Http\Controllers\Core\DataTableSearch;
use App\Models\Agency\Agency;
use App\Models\Company\Company;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use App\Models\User\User;
use Illuminate\Support\Facades\DB;

/**
 * Class ConstructorMappingSearch
 * @package App\Models\ConstructorMapping
 */
class UserDocumentsSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->rowsCountAll();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->rowsCountAll();
    }

    /**
     * @return int
     */
    private function rowsCountAll()
    {
        $query = $this->constructQuery();
        $result = DB::select("SELECT count(*) as cnt FROM ({$query->toSql()}) as temp", $query->getBindings());

        return $result[0]->cnt;
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        $data = $query->get();

        $result = [];
        $referenceExpressControlAll = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_EXPRESS_CONTROL);
        $referenceSubDivisionAll = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS);

        //
        $users = User::select('id', 'ssn', DB::raw("users.first_name || ' ' || users.last_name AS username"))->get()->keyBy('id')->toArray();
        $usersKeyBySSN = User::select('id', 'ssn', DB::raw("users.first_name || ' ' || users.last_name AS username"))->get()->keyBy('ssn')->toArray();

        //
        $states = ConstructorStates::select('id', 'constructor_states_ml.state_name')->join('constructor_states_ml', 'constructor_states.id', '=', 'constructor_states_ml.constructor_states_id')->where('constructor_states_ml.lng_id', '=', cLng('id'))->get()->keyBy('id')->toArray();

        //
        $agencies = Agency::select('agency.id', 'agency_ml.name', 'tax_id')->join('agency_ml', 'agency.id', '=', 'agency_ml.agency_id')->where('agency_ml.lng_id', '=', cLng('id'))->get()->keyBy('tax_id')->toArray();

        //
        $companies = Company::select('name', 'tax_id')->get()->keyBy('tax_id')->toArray();

        foreach ($data as $key => $value) {

            $safData = $value->data;
            $safSidesData = $safData['saf']['sides'] ?? [];

            $applicantName = $applicantUserName = '';
            if ($safSidesData && isset($safSidesData['applicant'])) {
                $applicantTin = $safSidesData['applicant']['type_value'] ?? '';

                if ($applicantTin) {

                    $applicantName = '(' . $applicantTin . ') ';

                    if (isset($usersKeyBySSN[$applicantTin])) {
                        $applicantName .= $usersKeyBySSN[$applicantTin]['username'];
                    } else {
                        if (isset($companies[$applicantTin])) {
                            $applicantName .= $companies[$applicantTin]['name'];
                        }
                    }
                }
            }

            $importer = '';
            if(isset($safSidesData['import']['type_value'])){
                $importer .= '('.$safSidesData['import']['type_value'].') ';
            }

            if(isset($safSidesData['import']['name'])){
                $importer .= $safSidesData['import']['name'];
            }

            $exporter = '';
            if(isset($safSidesData['export']['type_value'])){
                $exporter .= '('.$safSidesData['export']['type_value'].') ';
            }

            if(isset($safSidesData['export']['name'])){
                $exporter .= $safSidesData['export']['name'];
            }

            $value->state = isset($states[$value->state_id]) ? $states[$value->state_id]['state_name'] : '';

            $state = optional($value->currentState)->state;
            if (!is_null($state) && ($state->state_type == ConstructorStates::END_STATE_TYPE && $state->state_approved_status == ConstructorStates::END_STATE_NOT_APPROVED) || $state->state_type == ConstructorStates::CANCELED_STATE_TYPE) {
                $value->currentStatus = 'rejected';
            } elseif (!is_null($state) && $state->state_type == ConstructorStates::END_STATE_TYPE && $state->state_approved_status == ConstructorStates::END_STATE_APPROVED) {
                $value->currentStatus = 'success';
            }

            $expressControl = '';
            if ($value->sub_application_express_control && isset($referenceExpressControlAll[$value->sub_application_express_control])) {
                $expressControl = $referenceExpressControlAll[$value->sub_application_express_control]['name'];
            }

            $subDivision = '';
            if ($value->sub_application_agency_subdivision && isset($referenceSubDivisionAll[$value->sub_application_agency_subdivision])) {
                $subDivision = $referenceSubDivisionAll[$value->sub_application_agency_subdivision]['name'];
            }

            $subApplicationAgencyName = '';
            if ($value->sub_application_tax_id) {
                if (isset($agencies[$value->sub_application_tax_id])) {
                    $subApplicationAgencyName = '(' . $agencies[$value->sub_application_tax_id]['tax_id'] . ') ' . $agencies[$value->sub_application_tax_id]['name'];
                }
            }

            if ($value->user_id) {
                if (isset($users[$value->user_id])) {
                    $applicantUserName = '(' . $users[$value->user_id]['ssn'] . ') ' . $users[$value->user_id]['username'];
                }
            }

            if ($value->sub_application_send_date) {
                $value->sub_application_send_date = formattedDate($value->sub_application_send_date);
            }

            $value->sub_application_express_control = $expressControl;
            $value->sub_application_agency_subdivision = $subDivision;
            $value->sub_application_agency = $subApplicationAgencyName;
            $value->sub_application_registration_date = formattedDate($value->sub_application_registration_date);
            $value->sub_application_permission_date = formattedDate($value->sub_application_permission_date);
            $value->sub_application_expiration_date = formattedDate($value->sub_application_expiration_date);
            $value->saf_applicant_tin = $applicantName;
            $value->saf_applicant_user = $applicantUserName;
            $value->saf_importer = $importer;
            $value->saf_exporter = $exporter;
            $value->saf_status_type = trans('swis.' . $value->saf_status_type . '_title');

            unset($value->custom_data);
            unset($value->saf_data);

            $result[] = $value;
        }

        return $result;
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $orderByType = $this->orderByType ?? 'desc';

        if ($this->orderByCol == 'state') {
            $this->orderByCol = 'saf_sub_applications.created_at';
        } elseif ($this->orderByCol == 'saf_sub_application_id') {
            $this->orderByCol = 'saf_sub_applications.id';
        }

        $this->orderByCol = $this->orderByCol ?: 'saf_sub_applications.id';

        $query->orderBy($this->orderByCol, $orderByType);
    }

    /**
     * @param array $filterData
     * @return mixed
     */
    protected function constructQuery($filterData = [])
    {
        $searchData = !empty($this->searchData) ? $this->searchData : [];

        if (count($filterData)) {
            $searchData = $filterData;
        }

        $query = SingleApplicationSubApplications::select([
            'saf.id',
        ]);

        $query->addSelect('saf.regime as saf_status_type',
            'saf.regular_number as saf_number',
            'saf.applicant_value as saf_applicant_tin',
            'saf.user_id',
            'saf_sub_applications.agency_id as sub_application_agency',
            'saf_sub_applications.document_code',
            'saf_sub_applications.express_control_id as sub_application_express_control',
            'saf_sub_applications.document_number as sub_application_number',
            'saf_sub_applications.access_token',
            'saf_sub_applications.status_date as sub_application_send_date',
            'saf_sub_applications.agency_subdivision_id as sub_application_agency_subdivision',
            'saf_sub_applications.company_tax_id as sub_application_tax_id',
            'saf_sub_applications.id as saf_sub_application_id',
            'saf_sub_applications.registration_number as sub_application_registration_number',
            'saf_sub_applications.registration_date as sub_application_registration_date',
            'saf_sub_applications.permission_number as sub_application_permission_number',
            'saf_sub_applications.permission_date as sub_application_permission_date',
            'saf_sub_applications.expiration_date as sub_application_expiration_date',
            'saf_sub_application_states.state_id as state_id',
            DB::raw("CONCAT('(', reference_classificator_of_documents.code , ') ', reference_classificator_of_documents_ml.name) AS document_type"),
            DB::raw("regexp_replace(COALESCE(saf_sub_applications.data, saf.data)::text, '\\u0000', '', 'g')::json as data"),
            DB::raw("regexp_replace(COALESCE(saf_sub_applications.custom_data, saf.custom_data)::text, '\\u0000', '', 'g')::json as custom_data"));

        $query->join('saf', 'saf.regular_number', '=', 'saf_sub_applications.saf_number')
            ->leftJoin('users', 'users.id', '=', 'saf.creator_user_id')
            ->leftJoin('reference_agency_subdivisions', 'reference_agency_subdivisions.id', '=', 'saf_sub_applications.agency_subdivision_id')
            ->leftJoin('reference_agency_subdivisions_ml', function ($q) {
                $q->on('reference_agency_subdivisions_ml.reference_agency_subdivisions_id', '=', 'reference_agency_subdivisions.id')->where('reference_agency_subdivisions_ml.lng_id', cLng('id'));
            })
            ->join('saf_sub_application_states', function ($join) {
                $join->on('saf_sub_application_states.saf_subapplication_id', '=', 'saf_sub_applications.id')->where('saf_sub_application_states.is_current', '1');
            })
            ->join('reference_classificator_of_documents', 'reference_classificator_of_documents.id', '=', 'saf_sub_applications.reference_classificator_id')
            ->join('reference_classificator_of_documents_ml', function ($query) {
                $query->on('reference_classificator_of_documents_ml.reference_classificator_of_documents_id', '=', 'reference_classificator_of_documents.id')->where("reference_classificator_of_documents_ml.lng_id", cLng('id'));
            })->groupBy(
                'saf.id',
                'saf_sub_applications.id',
                'saf.regime',
                'saf.regular_number',
                'saf.user_id',
                'saf_sub_applications.agency_id',
                'saf_sub_applications.document_code',
                'saf_sub_applications.express_control_id',
                'saf_sub_applications.document_number',
                'saf_sub_applications.access_token',
                'saf_sub_applications.status_date',
                'saf_sub_applications.agency_subdivision_id',
                'saf_sub_applications.company_tax_id',
                'saf_sub_applications.registration_number',
                'saf_sub_applications.registration_date',
                'saf_sub_applications.permission_number',
                'saf_sub_applications.permission_date',
                'saf_sub_applications.expiration_date',
                'saf_sub_application_states.state_id',
                'reference_classificator_of_documents_ml.name',
                'reference_classificator_of_documents.code');

        if (isset($searchData['id'])) {
            $query->where(DB::raw("saf_sub_applications.id::varchar"), $searchData['id']);
        }

        if (isset($searchData['state_id'])) {
            $query->where('saf_sub_application_states.state_id', $searchData['state_id'])->where('saf_sub_application_states.is_current', '1');
        }

        if (isset($searchData['saf_regime'])) {
            $query->where('saf.regime', $searchData['saf_regime']);
        }

        if (isset($searchData['saf_number'])) {
            $query->where('saf.regular_number', $searchData['saf_number']);
        }

        if (isset($searchData['sub_app_number'])) {
            $query->where('saf_sub_applications.document_number', $searchData['sub_app_number']);
        }

        if (isset($searchData['sub_application_express_control'])) {
            $query->where('saf_sub_applications.express_control_id', $searchData['sub_application_express_control']);
        }

        if (isset($searchData['sub_application_agency'])) {
            $query->where('saf_sub_applications.company_tax_id', $searchData['sub_application_agency']);
        }

        if (isset($searchData['subdivision'])) {
            $query->where('saf_sub_applications.agency_subdivision_id', $searchData['subdivision']);
        }

        if (isset($searchData['saf_applicant_user'])) {
            $searchData['saf_applicant_user'] = explode(' ', $searchData['saf_applicant_user']);
            $query->where(function ($q) use ($searchData) {
                if (isset($searchData['saf_applicant_user'][0])) {
                    $q->orWhere('users.first_name', 'ILIKE', '%' . strtolower(trim($searchData['saf_applicant_user'][0])) . '%');
                    $q->orWhere('users.last_name', 'ILIKE', '%' . strtolower(trim($searchData['saf_applicant_user'][0])) . '%');
                    $q->orWhere('users.ssn', 'ILIKE', '%' . trim($searchData['saf_applicant_user'][0]) . '%');
                }

                if (isset($searchData['saf_applicant_user'][1])) {
                    $q->orWhere('users.first_name', 'ILIKE', '%' . strtolower(trim($searchData['saf_applicant_user'][1])) . '%');
                    $q->orWhere('users.last_name', 'ILIKE', '%' . strtolower(trim($searchData['saf_applicant_user'][1])) . '%');
                    $q->orWhere('users.ssn', 'ILIKE', '%' . trim($searchData['saf_applicant_user'][1]) . '%');
                }
            });
        }

        if (isset($searchData['documentID'])) {
            $query->where('document_id', $searchData['documentID']);
        }

        if (isset($searchData['registration_number'])) {
            $query->where('saf_sub_applications.registration_number', $searchData['registration_number']);
        }

        if (isset($searchData['saf_applicant_tin_or_name'])) {
            $safApplicantTinOrName = $searchData['saf_applicant_tin_or_name'];

            $query->where(function ($q) use ($safApplicantTinOrName) {
                $q->where('saf.data->saf->sides->applicant->name', 'ilike', "%{$safApplicantTinOrName}%")
                    ->orWhere('saf.applicant_value', $safApplicantTinOrName);
            });
        }

        if (isset($searchData['saf_importer_tin_or_name'])) {
            $safImporterTinOrName = $searchData['saf_importer_tin_or_name'];

            $query->where(function ($q) use ($safImporterTinOrName) {
                $q->where('saf.data->saf->sides->import->name', 'ilike', "%{$safImporterTinOrName}%")
                    ->orWhere('saf.importer_value', $safImporterTinOrName);
            });
        }

        if (isset($searchData['saf_exporter_tin_or_name'])) {
            $safExporterTinOrName = $searchData['saf_exporter_tin_or_name'];

            $query->where(function ($q) use ($safExporterTinOrName) {
                $q->where('saf.data->saf->sides->export->name', 'ilike', "%{$safExporterTinOrName}%")
                    ->orWhere('saf.exporter_value', $safExporterTinOrName);
            });
        }

        //Дата представления
        if (isset($searchData['submitting_date_start_date_filter']) && isDateFormat($searchData['submitting_date_start_date_filter'])) {
            $query->where('saf_sub_applications.status_date', '>=', $searchData['submitting_date_start_date_filter']);
        }

        if (isset($searchData['submitting_date_end_date_filter']) && isDateFormat($searchData['submitting_date_end_date_filter'])) {
            $query->where('saf_sub_applications.status_date', '<=', $searchData['submitting_date_end_date_filter']);
        }

        //Дата регистрации
        if (isset($searchData['registration_date_start_date_filter']) && isDateFormat($searchData['registration_date_start_date_filter'])) {
            $query->where('saf_sub_applications.registration_date', '>=', $searchData['registration_date_start_date_filter']);
        }

        if (isset($searchData['registration_date_end_date_filter']) && isDateFormat($searchData['registration_date_end_date_filter'])) {
            $query->where('saf_sub_applications.registration_date', '<=', $searchData['registration_date_end_date_filter']);
        }

        //Дата разрешения
        if (isset($searchData['permission_date_start_date_filter']) && isDateFormat($searchData['permission_date_start_date_filter'])) {
            $query->where('saf_sub_applications.permission_date', '>=', $searchData['permission_date_start_date_filter']);
        }

        if (isset($searchData['permission_date_end_date_filter']) && isDateFormat($searchData['permission_date_end_date_filter'])) {
            $query->where('saf_sub_applications.permission_date', '<=', $searchData['permission_date_end_date_filter']);
        }

        //Срок действия
        if (isset($searchData['expiration_date_start_date_filter']) && isDateFormat($searchData['expiration_date_start_date_filter'])) {
            $query->where('saf_sub_applications.expiration_date', '>=', $searchData['expiration_date_start_date_filter']);
        }

        if (isset($searchData['expiration_date_end_date_filter']) && isDateFormat($searchData['expiration_date_end_date_filter'])) {
            $query->where('saf_sub_applications.expiration_date', '<=', $searchData['expiration_date_end_date_filter']);
        }

        if (isset($searchData['documentCode'])) {
            $query->where('document_code', $searchData['documentCode']);
        }

        if (isset($searchData['saf[general][sub_application_access_token]'])) {
            $query->where('saf_sub_applications.access_token', $searchData['saf[general][sub_application_access_token]']);
        }

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }

    /**
     * Function to call protected ConstructorQuery
     *
     * @param $data
     * @return mixed
     */
    public function callToConstructorQuery($data)
    {
        return $this->constructQuery($data);
    }
}
