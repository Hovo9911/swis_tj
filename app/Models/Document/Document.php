<?php

namespace App\Models\Document;

use App\Models\BaseModel;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\Folder\Folder;
use App\Models\ReferenceTable\ReferenceTable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Auth;

/**
 * Class Document
 * @package App\Models\Document
 */
class Document extends BaseModel
{
    /**
     * @var string
     */
    const DOCUMENT_STATUS_NULL = '';
    const DOCUMENT_STATUS_CREATED = 'created';
    const DOCUMENT_STATUS_REGISTERED = 'registered';
    const DOCUMENT_STATUS_CONFIRMED = 'confirmed';
    const DOCUMENT_STATUS_REJECTED = 'rejected';
    const DOCUMENT_STATUS_SUSPENDED = 'suspended';
    const DOCUMENT_STATUS_SAVED = 'save';

    /**
     * @var string
     */
    const DOCUMENT_CODE_FOR_PDF_TEMPLATE = '122237';
    const DOCUMENT_CODE_FOR_TJ_SWT_756_CCI_V1_PDF_TEMPLATE = '4021';
    const DOCUMENT_CODE_FOR_TJ_SWT_756_CCI_FORM_A_PDF_TEMPLATE = '4021';
    const DOCUMENT_CODE_FOR_TJ_SWT_756_CCI_V2_PDF_TEMPLATE = '4021';
    const DOCUMENT_CODE_FOR_KOOC = '0139';
    const DOCUMENT_CODE_90009 = '90009';
    const DOCUMENT_CODE_6012 = '6012';

    /**
     * @var array
     */
    const DOCUMENT_STATUSES = [
        self::DOCUMENT_STATUS_CREATED,
        self::DOCUMENT_STATUS_REGISTERED,
        self::DOCUMENT_STATUS_CONFIRMED,
        self::DOCUMENT_STATUS_REJECTED,
        self::DOCUMENT_STATUS_SUSPENDED,
        self::DOCUMENT_STATUS_SAVED,
    ];

    /**
     * @var string (Prefix form documents -> document_access_password & uuid)
     */
    const DOCUMENT_TYPE_PREFIX_DOCUMENT = 'D';
    const DOCUMENT_TYPE_PREFIX_ELECTRONIC_DOCUMENT = 'E';

    /**
     * @var string (From which module document is created)
     */
    const DOCUMENT_CREATE_MODULE_DOCUMENT = 'document';
    const DOCUMENT_CREATE_MODULE_FOLDER = 'folder';
    const DOCUMENT_CREATE_MODULE_SAF = 'saf';
    const DOCUMENT_CREATE_MODULE_APPLICATION = 'application';
    const DOCUMENT_CREATE_MODULE_LABORATORY = 'laboratory';

    /**
     * @var string
     */
    const DOCUMENT_FORM_PAPER = 'paper';
    const DOCUMENT_FORM_ELECTRONIC = 'electronic';

    /**
     * @var string
     */
    const APPROVE_OTHER = 'other';
    const APPROVE_AGENCY = 'agency';

    /**
     * @var array
     */
    const APPROVE_TYPES = [self::APPROVE_OTHER, self::APPROVE_AGENCY];

    /**
     * @var string
     */
    public $table = 'document';

    /**
     * @var bool
     */
    protected $hasAdminInfo = true;

    /**
     * @var null|array
     */
    public static $referenceClassificatorsData = null;

    /**
     * @var array
     */
    protected $fillable = [
        'document_form',
        'user_id',
        'creator_user_id',
        'company_tax_id',
        'uuid',
        'document_access_password',
        'document_type',
        'document_type_id',
        'document_status',
        'document_name',
        'document_description',
        'document_number',
        'document_file',
        'document_release_date',
        'approve_type',
        'approve_agency_tax_id',
        'subject_for_approvement',
        'confirmation_date',
        'confirming',
        'holder_type',
        'holder_type_value',
        'holder_passport',
        'holder_name',
        'holder_country',
        'holder_community',
        'holder_address',
        'holder_phone_number',
        'holder_email',
        'creator_type',
        'creator_type_value',
        'creator_name',
        'creator_country',
        'creator_address',
        'creator_phone_number',
        'creator_email',
        'created_from_module',
        'sub_application_id',
        'state_id',
        'show_status',
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'created_admin_id',
        'created_admin_ip',
        'updated_at',
        'updated_admin_id',
        'updated_admin_ip',
        'deleted_admin_id',
        'deleted_admin_ip'
    ];

    /**
     * Function get release data value
     *
     * @param $value
     * @return string|void
     */
    public function getDocumentReleaseDateAttribute($value)
    {
        if ($value) {
            return Carbon::parse($value)->format(config('swis.date_format_front'));
        }
    }

    /**
     * Function to get created_at value
     *
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format(config('swis.date_time_format_front'));
    }

    /**
     * Function to get document name
     *
     * @param $value
     * @return string
     */
    public function getDocumentNameAttribute($value)
    {
        if (isset($this->attributes['document_type_id']) && $this->attributes['document_type_id']) {

            if (self::$referenceClassificatorsData == null) {
                $refClassification = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS);
                self::$referenceClassificatorsData = $refClassification;
            }

            if (isset(self::$referenceClassificatorsData[$this->attributes['document_type_id']])) {
                return self::$referenceClassificatorsData[$this->attributes['document_type_id']]['name'];
            }
        }

        return $this->attributes['document_name'] ?? '';
    }

    /**
     * Function to get document description
     *
     * @param $value
     * @return string
     */
    public function getDocumentDescriptionAttribute($value)
    {
        if (isset($this->attributes['document_type_id']) && $this->attributes['document_type_id'] && isset($this->attributes['document_form']) && $this->attributes['document_form'] == Document::DOCUMENT_FORM_ELECTRONIC) {

            if (self::$referenceClassificatorsData == null) {
                $refClassification = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS);
                self::$referenceClassificatorsData = $refClassification;
            }

            if (isset(self::$referenceClassificatorsData[$this->attributes['document_type_id']])) {
                return self::$referenceClassificatorsData[$this->attributes['document_type_id']]['name'];
            }
        }

        return $this->attributes['document_description'] ?? '';
    }

    /**
     * Function to relate with Folder
     *
     * @return BelongsToMany
     */
    public function folders()
    {
        return $this->belongsToMany(Folder::class, 'folder_documents')->withPivot(['document_id']);
    }

    /**
     * Function to get only Electronic Documents
     *
     * @param $query
     * @return Builder
     */
    public function scopeElectronicDocument($query)
    {
        return $query->where('document_form', self::DOCUMENT_FORM_ELECTRONIC);
    }

    /**
     * @param $query
     * @return Builder
     */
    public function scopePaperDocument($query)
    {
        return $query->where('document_form', self::DOCUMENT_FORM_PAPER);
    }

    /**
     * Function to check for owner/creator
     *
     * @param $query
     * @return Builder
     */
    public function scopeDocumentOwnerOrCreator($query)
    {
        $userType = Auth::user()->currentUserType();
        $userTypeValue = Auth::user()->currentUserTypeValue();
        $showOthersData = Auth::user()->userShowOthersData();

        $query->where(function ($q) use ($userType, $userTypeValue) {
            $q->where('document.holder_type_value', $userTypeValue)->where('holder_type', $userType)
                ->orWhere('document.creator_type_value', $userTypeValue)->where('creator_type', $userType);
        });

        // User login as company/agency
        if (Auth::user()->companyTaxId()) {

            if (!$showOthersData) {
                $query->where('document.user_id', Auth::id());
            }
        }

        // User login as Authorized Natural Person
        if (Auth::user()->isAuthorizedNaturalPerson()) {

            if (!$showOthersData) {
                $query->where(['document.creator_user_id' => Auth::id(), 'company_tax_id' => Document::FALSE]);
            }
        }

        return $query;
    }

    /**
     * Function to get file basename
     *
     * @return string
     */
    public function fileBaseName()
    {
        if ($this->document_form == self::DOCUMENT_FORM_PAPER && $this->filePath()) {
            $file = pathinfo($this->filePath());

            return $file['basename'];
        }

        return "";
    }

    /**
     * Function to get file path
     *
     * @return string
     */
    public function filePath()
    {
        if ($this->document_form == self::DOCUMENT_FORM_ELECTRONIC) {

            $pdfHashKey = Document::leftJoin('saf_sub_applications', 'saf_sub_applications.id', '=', 'document.sub_application_id')
                ->where('document.id', '=', $this->id)
                ->pluck('pdf_hash_key')
                ->first();

            return urlWithLng("application/$this->sub_application_id/" . $pdfHashKey);
        }

        if ($this->document_file) {
            return url('files/uploads/files/' . $this->document_file);
        }

        return "";
    }

    /**
     * Function to get electronic doc state name
     *
     * @return string
     */
    public function getElectronicDocumentStateName()
    {
        $electronicDocumentStateName = '';
        if ($this->document_type_id && $this->document_form == Document::DOCUMENT_FORM_ELECTRONIC) {
            $electronicDocumentState = ConstructorStates::select('id')->with('currentMl')->where('id', $this->state_id)->first();
            if (!is_null($electronicDocumentState)) {
                $electronicDocumentStateName = $electronicDocumentState->currentMl->state_name;
            }
        }

        return $electronicDocumentStateName;
    }

    /**
     * Function to generate what buttons need to show in current status
     *
     * @param $status
     * @return string
     */
    public static function generateDocumentStatusButtons($status)
    {
        $renderButtons = '';
        switch ($status) {

            // Null
            case self::DOCUMENT_STATUS_NULL:
                $renderButtons = self::renderButtons([
                    self::DOCUMENT_STATUS_CREATED,
                    self::DOCUMENT_STATUS_REGISTERED,
                ]);
                break;

            // Created
            case self::DOCUMENT_STATUS_CREATED:
                $renderButtons = self::renderButtons([
                    self::DOCUMENT_STATUS_REGISTERED,
                    self::DOCUMENT_STATUS_SUSPENDED,
                    self::DOCUMENT_STATUS_SAVED,
                ]);
                break;

            // Registered -- // Confirmed
            case self::DOCUMENT_STATUS_REGISTERED:
            case self::DOCUMENT_STATUS_CONFIRMED:
                $renderButtons = self::renderButtons([
                    self::DOCUMENT_STATUS_SUSPENDED,
                    self::DOCUMENT_STATUS_SAVED,
                ]);
                break;

            // Rejected
            case self::DOCUMENT_STATUS_REJECTED:
                $renderButtons = self::renderButtons([
                    self::DOCUMENT_STATUS_SAVED,
                ]);
                break;
        }

        return $renderButtons;
    }

    /**
     *  Function to generate button for agency that need to confirm or reject , if approved agency not current user agency
     *
     * @param $document
     * @return string
     */
    public static function generateButtonsForConfirmingAgency($document)
    {
        $renderButtons = '';

        switch ($document->document_status) {

            // Created
            case self::DOCUMENT_STATUS_CREATED:

                // If Agency is Owner/Creator And Approve
                $renderButtons = self::renderButtons([
                    self::DOCUMENT_STATUS_REGISTERED,
                    self::DOCUMENT_STATUS_SUSPENDED,
                    self::DOCUMENT_STATUS_SAVED,
                ]);

                break;

            // Registered
            case self::DOCUMENT_STATUS_REGISTERED:
                if ($document->approve_agency_tax_id == Auth::user()->companyTaxId() && $document->isDocumentOwnerOrCreator()) {
                    $renderButtons = self::renderButtons([
                        self::DOCUMENT_STATUS_CONFIRMED,
                        self::DOCUMENT_STATUS_REJECTED,
                        self::DOCUMENT_STATUS_SUSPENDED,
                        self::DOCUMENT_STATUS_SAVED,
                    ]);
                } else {
                    $renderButtons = self::renderButtons([
                        self::DOCUMENT_STATUS_CONFIRMED,
                        self::DOCUMENT_STATUS_REJECTED,
                    ]);
                }
                break;

            // Confirmed
            case self::DOCUMENT_STATUS_CONFIRMED:
                if ($document->approve_agency_tax_id == Auth::user()->companyTaxId() && $document->isDocumentOwnerOrCreator()) {
                    $renderButtons = self::renderButtons([
                        self::DOCUMENT_STATUS_SUSPENDED,
                        self::DOCUMENT_STATUS_SAVED,
                        self::DOCUMENT_STATUS_REJECTED,
                    ]);
                } else {
                    $renderButtons = self::renderButtons([
                        self::DOCUMENT_STATUS_REJECTED,
                    ]);
                }
                break;

            // Rejected
            case self::DOCUMENT_STATUS_REJECTED:
                $renderButtons = self::renderButtons([
                    self::DOCUMENT_STATUS_SAVED,
                ]);
                break;
        }

        return $renderButtons;
    }

    /**
     * Function to generate buttons(html)
     *
     * @param $statuses
     * @return string
     */
    private static function renderButtons($statuses)
    {
        $html = '';
        foreach ($statuses as $status) {
            $html .= ' <button  data-status="' . $status . '" class="btn btn-primary save-button mr-10  btn-' . $status . '">' . trans('swis.document.status.' . $status . '.button') . '</button>';
        }

        return $html;
    }

    /**
     * Function to check if auth user company is that agency need to approve
     *
     * @param $documentId
     * @return bool
     */
    public static function isAgencyApprove($documentId)
    {
        if (Auth::user()->isAgency()) {
            $document = Document::find($documentId);

            if ($document->subject_for_approvement == Document::TRUE &&
                $document->approve_type == Document::APPROVE_AGENCY &&
                $document->approve_agency_tax_id == Auth::user()->companyTaxId()
//                && $document->company_tax_id != Auth::user()->companyTaxId()
            ) {
                return true;
            }

        }

        return false;
    }

    /**
     * Function to generate perm path of file
     *
     * @return string
     */
    public function generateFilePermPath()
    {
        $firstDir = substr($this->uuid, 0, 4);
        $secondDir = substr($this->uuid, 4, 4);
        $thirdDir = substr($this->uuid, 8);

        return "$firstDir/$secondDir/" . $thirdDir;
    }

    /**
     * Function to check is Document owner/creator
     *
     * @return bool
     */
    public function isDocumentOwnerOrCreator()
    {
        $document = $this;

        $legalEntities = ReferenceTable::legalEntityValues();

        $holderType = array_search($document->holder_type, $legalEntities);
        $creatorType = array_search($document->creator_type, $legalEntities);

        $isHolder = false;
        $isCreator = false;

        $showOthersData = Auth::user()->userShowOthersData();

        // User login as company/agency
        if (Auth::user()->companyTaxId()) {

            // Holder
            if ($holderType == BaseModel::ENTITY_TYPE_TAX_ID) {

                if ($document->holder_type_value == Auth::user()->companyTaxId()) {
                    $isHolder = true;
                }

                if (!$showOthersData && $document->holder_type_value != Auth::user()->companyTaxId()) {
                    if ($document->user_id != Auth::id()) {
                        $isHolder = false;
                    }
                }
            }

            // Creator
            if ($creatorType == BaseModel::ENTITY_TYPE_TAX_ID) {
                if ($document->creator_type_value == Auth::user()->companyTaxId()) {
                    $isCreator = true;
                }

                if (!$showOthersData && $document->holder_type_value != Auth::user()->companyTaxId()) {
                    if ($document->user_id != Auth::id()) {
                        $isCreator = false;
                    }
                }
            }

        }

        // User login as Natural Person
        if (Auth::user()->isNaturalPerson()) {

            if ($holderType == BaseModel::ENTITY_TYPE_USER_ID) {
                if ($document->holder_type_value == Auth::user()->ssn) {
                    $isHolder = true;
                }
            }

            if ($creatorType == BaseModel::ENTITY_TYPE_USER_ID) {
                if ($document->creator_type_value == Auth::user()->ssn) {
                    $isCreator = true;
                }
            }

        }

        if ($isCreator || $isHolder) {
            return true;
        }

        return false;
    }

    /**
     * Function to check is Document owner/creator And Agency is Approve
     *
     * @return bool
     */
    public function isDocumentOwnerOrCreatorAndApproveAgency()
    {
        if ($this->isDocumentOwnerOrCreator() && Document::isAgencyApprove($this->id)) {
            return true;
        }

        return false;
    }

    /**
     * Function to check is user current agency need to approve document
     *
     * @param $query
     * @return Builder
     */
    public function scopeAgencyNeedToApproveDocument($query)
    {
        if (!Auth::user()->userShowOthersData()) {
            return $query;
        }

        if (Auth::user()->isAgency()) {
            return $query->where(['approve_type' => Document::APPROVE_AGENCY, 'approve_agency_tax_id' => Auth::user()->companyTaxId(), 'subject_for_approvement' => Document::TRUE])
                ->whereIn('document_status', [Document::DOCUMENT_STATUS_REGISTERED, Document::DOCUMENT_STATUS_CONFIRMED, Document::DOCUMENT_STATUS_REJECTED, Document::DOCUMENT_STATUS_CREATED])
                ->where('document.show_status', Document::STATUS_ACTIVE);
        }
    }

    /**
     * Function to get in saf attached docs too
     *
     * @param $query
     * @param $module
     * @return Builder
     */
    public function scopeSafAttachedDocument($query, $module)
    {
        if (!Auth::user()->userShowOthersData()) {
            return $query;
        }

        $userType = Auth::user()->currentUserType();
        $userTypeValue = Auth::user()->currentUserTypeValue();

        return $query->when($module == Document::DOCUMENT_CREATE_MODULE_SAF, function ($q) use ($module) {
            return $q->whereIn('document_status', [Document::DOCUMENT_STATUS_REGISTERED, Document::DOCUMENT_STATUS_CONFIRMED]);
        })->where('holder_type_value', $userTypeValue)->where('holder_type', $userType);
    }

    /**
     * Function to check document access password changed or not
     *
     * @param $documentUUID
     * @return bool
     */
    public static function isDocumentAccessPasswordChanged($documentUUID)
    {
        $activeDocumentAccessPassword = Document::where(['uuid' => $documentUUID, 'show_status' => DOCUMENT::STATUS_ACTIVE])->pluck('document_access_password')->first();
        $inactiveDocumentAccessPassword = Document::where(['uuid' => $documentUUID, 'show_status' => DOCUMENT::STATUS_INACTIVE])->orderByDesc()->pluck('document_access_password')->first();

        if ($inactiveDocumentAccessPassword && $activeDocumentAccessPassword) {
            if ($activeDocumentAccessPassword != $inactiveDocumentAccessPassword) {
                return true;
            }
        }

        return false;
    }

    /**
     * Function to get document pdf hash key
     *
     * @return Document
     */
    public function documentPDFHashKey()
    {
        return Document::leftJoin('saf_sub_applications', 'saf_sub_applications.id', '=', 'document.sub_application_id')
            ->where('document.id', '=', $this->id)
            ->pluck('pdf_hash_key')
            ->first();
    }

}
