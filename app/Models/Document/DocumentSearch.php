<?php

namespace App\Models\Document;

use App\Http\Controllers\Core\DataTableSearch;
use App\Models\Folder\Folder;
use App\Models\FolderDocuments\FolderDocuments;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationDocuments\SingleApplicationDocuments;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use Illuminate\Support\Facades\DB;

/**
 * Class DocumentSearch
 * @package App\Models\Document
 */
class DocumentSearch extends DataTableSearch
{
    /**
     * @var null
     */
    private $folderId;

    /**
     * DocumentSearch constructor.
     * @param null $folderId
     */
    public function __construct($folderId = null)
    {
        $this->folderId = $folderId;
    }

    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        $result = $query->get();

        if (isset($this->searchData, $this->searchData['document_access_password']) && $result->count() > 0) {

            $canEdit = true;
            $document = Document::select('id')->where('id', $result[0]->id)->documentOwnerOrCreator()->first();
            if (is_null($document)) {
                $canEdit = false;
            }

            $result[0]->canEdit = $canEdit;
            $result[0]->searched_by_access_password = true;
        }

        foreach ($result as &$item) {
            $item->document_file = $item->filePath();
        }

        return $result;
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy('document.' . $this->orderByCol, $this->orderByType ?? 'desc');
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $selectStr = ['document.id', 'document.document_type_id', 'document.document_name', 'document.uuid', 'document.document_status', 'document.document_access_password', 'document.document_description', 'document.document_form', 'document.document_type', 'document.document_number', 'document.document_file', 'document.document_release_date', 'document.show_status', 'sub_application_id', 'saf_sub_applications.pdf_hash_key'];
        $query = Document::select($selectStr)->distinct()->leftJoin('saf_sub_applications', 'saf_sub_applications.id', '=', 'document.sub_application_id');

        $module = isset($this->searchData['moduleForDocuments']) ? $this->searchData['moduleForDocuments'] : null;
        $myDocs = (isset($this->folderId)) ? FolderDocuments::where('folder_id', $this->folderId)->get()->pluck('document_id')->all() : [];

        $query->whereNotIn(DB::raw("document.id::varchar"), $myDocs);
        if (empty($this->searchData['docIds'])) {

            if (isset($this->searchData['id'])) {
                $query->where(DB::raw("document.id::varchar"), $this->searchData['id']);
            }

            if (isset($this->searchData['document_name'])) {
                $query->where('document_name', 'ILIKE', '%' . strtolower(trim($this->searchData['document_name'])) . '%');
            }

            if (isset($this->searchData['document_access_password'])) {

                $query->where('document_access_password', trim($this->searchData['document_access_password']));
                $result = $query->first();

                if ($query->count()) {
                    if ($result->document_status == Document::DOCUMENT_STATUS_SUSPENDED) {
                        $query->documentOwnerOrCreator();
                    }
                }
            }

            if (isset($this->searchData['document_type'])) {
                $query->where('document_type', 'ILIKE', '%' . strtolower(trim($this->searchData['document_type'])) . '%');
            }

            if (isset($this->searchData['document_number'])) {
                $query->where('document.document_number', 'ILIKE', '%' . strtolower(trim($this->searchData['document_number'])) . '%');
            }

            if (isset($this->searchData['document_description'])) {
                $query->where('document_description', 'ILIKE', '%' . strtolower(trim($this->searchData['document_description'])) . '%');
            }

            if (isset($this->searchData['document_release_date_start'])) {
                $query->where('document_release_date', '>=', $this->searchData['document_release_date_start']);
            }

            if (isset($this->searchData['document_release_date_end'])) {
                $query->where('document_release_date', '<=', $this->searchData['document_release_date_end']);
            }

            if (isset($this->searchData['document_status'])) {
                $query->Where('document_status', $this->searchData['document_status']);
            }

            if (!empty($this->searchData['search_folder_name'])) {

                $folder = Folder::where('folder_name', 'ILIKE', '%' . strtolower($this->searchData['search_folder_name']) . '%')->folderOwnerOrCreator()->pluck('id')->all();

                $documentIDs = FolderDocuments::whereIn('folder_id', $folder)->pluck('document_id')->all();

                if (!empty($documentIDs)) {
                    $query->whereIn('document.id', $documentIDs);
                } else {
                    $query->where('document.id', '-1');
                }
            }

            if (!empty($this->searchData['search_folder_desc'])) {

                $folder = Folder::where('folder_description', 'ILIKE', '%' . strtolower($this->searchData['search_folder_desc']) . '%')->folderOwnerOrCreator()->pluck('id')->all();

                $documentIDs = FolderDocuments::whereIn('folder_id', $folder)->pluck('document_id')->all();

                if (!empty($documentIDs)) {
                    $query->whereIn('document.id', $documentIDs);
                } else {
                    $query->where('document.id', '-1');
                }
            }

            // When empty ACCESS PASSWORD field only get documents when user is owner or creator
            if (!isset($this->searchData['document_access_password'])) {
                $query->documentOwnerOrCreator();
            }

        } else {

            if (!empty($this->searchData['docIds'])) {
                $query->whereIn('document.id', $this->searchData['docIds']);
            } else {
                $query->where('document.id', '-1');
            }

        }

        if (empty($this->searchData)) {

            // Get documents From SAF attached
            $query->orWhere(function ($q) use ($module) {
                $q->safAttachedDocument($module);
            });

            // When user login agency need to approve documents
            $query->orWhere(function ($q) use ($module) {
                $q->agencyNeedToApproveDocument();
            });

        } else {

            $query->where('document_status', '!=', Document::DOCUMENT_STATUS_REJECTED);
        }

        if ($module == Document::DOCUMENT_CREATE_MODULE_SAF || $module == Document::DOCUMENT_CREATE_MODULE_LABORATORY || $module == Document::DOCUMENT_CREATE_MODULE_APPLICATION) {
            $query->whereIn('document_status', [Document::DOCUMENT_STATUS_REGISTERED, Document::DOCUMENT_STATUS_CONFIRMED]);
        }

        // Saf
        if ($module == Document::DOCUMENT_CREATE_MODULE_SAF) {
            $saf = SingleApplication::select('id', 'regular_number')->where('regular_number', request()->header('sNumber'))->first();

            if (!is_null($saf)) {
                $attachedDocumentsId = SingleApplicationDocuments::where('saf_number', $saf->regular_number)->pluck('document_id')->all();
                $query->whereNotIn('document.id', $attachedDocumentsId);
            }
        }

        // My application
        if ($module == Document::DOCUMENT_CREATE_MODULE_APPLICATION && isset($this->searchData['sub_application_id']) && $this->searchData['sub_application_id']) {
            $subApplication = SingleApplicationSubApplications::select('id', 'saf_number')->where('id', $this->searchData['sub_application_id'])->first();

            if (!is_null($subApplication)) {
                $attachedDocumentProducts = SingleApplicationSubApplications::getAttachedDocumentProducts($subApplication, $subApplication->productList->pluck('id')->all())->pluck('document_id')->all();
                $query->whereNotIn('document.id', $attachedDocumentProducts);
            }
        }

        // Folder
        if ($module == Document::DOCUMENT_CREATE_MODULE_FOLDER && isset($this->searchData['folder_id']) && $this->searchData['folder_id']) {
            $folder = Folder::select('id')->where('id', $this->searchData['folder_id'])->first();
            if (!is_null($folder)) {
                $attachedDocumentsId = $folder->documents->pluck('id')->all();
                $query->whereNotIn('document.id', $attachedDocumentsId);
            }
        }

        $query->active();

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }

}
