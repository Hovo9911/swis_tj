<?php

namespace App\Models\Document;

use App\EntityTypesInfo\LegalEntityForms;
use App\Exceptions\TaxServiceTjCompanyInactiveException;
use App\Exceptions\TaxServiceTjInnNotFoundException;
use App\Exceptions\TaxServiceTjInvalidResponseException;
use App\GUploader;
use App\Models\BaseModel;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\DocumentsNotes\DocumentsNotes;
use App\Models\Folder\Folder;
use App\Models\FolderDocuments\FolderDocuments;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationDocuments\SingleApplicationDocuments;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class DocumentManager
 * @package App\Models\Document
 */
class DocumentManager
{
    /**
     * Function to Store data to DB
     *
     * @param array $data
     * @return Document
     *
     * @throws TaxServiceTjCompanyInactiveException
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function store(array $data)
    {
        $data['uuid'] = $this->generateUniqueCode();
        $data['user_id'] = Auth::user()->getCreatorUser()->id;
        $data['creator_user_id'] = Auth::id();
        $data['document_form'] = Document::DOCUMENT_FORM_PAPER;
        $data['show_status'] = Document::STATUS_ACTIVE;
        $data['subject_for_approvement'] = $data['subject_for_approvement'] ?? Document::FALSE;
        $data['document_access_password'] = $this->generateUniqueCode();
        $data['company_tax_id'] = Auth::user()->companyTaxId();

        // Holder Data if Company/Natural Person
        if (isset($data['holder_type'])) {
            $legalEntityForms = LegalEntityForms::getInstance();
            $holderData = $legalEntityForms->holderDataByType($data['holder_type'], $data['holder_type_value'], $data['holder_passport'] ?? null);

            $data = array_merge($data, $holderData);
        }

        // Creator
        $authUserCreator = Auth::user()->getCreator();

        $data['creator_type'] = $authUserCreator->type;
        $data['creator_type_value'] = $authUserCreator->ssn;
        $data['creator_name'] = $authUserCreator->name;
        $data['creator_country'] = $authUserCreator->country;
        $data['creator_community'] = $authUserCreator->community;
        $data['creator_address'] = $authUserCreator->address;
        $data['creator_phone_number'] = $authUserCreator->phone_number;
        $data['creator_email'] = $authUserCreator->email;

        $document = new Document($data);

        // Upload file
        if (isset($data['document_file'])) {
            $gUploader = new GUploader('document_report');
            $document->document_file = $gUploader->storePerm($data['document_file'], $document->generateFilePermPath());
        }

        return DB::transaction(function () use ($data, $document) {
            $document->save();

            if (isset($data['folders'])) {
                $this->storeFolder($data['folders'], $document->id);
            }

            if ($data) {
                DocumentsNotes::storeNotes([
                    'document_uuid' => $document->uuid,
                    'trans_key' => isset($data['document_status']) ? DocumentsNotes::DOCUMENT_NOTE_TRANS_KEY_STATUSES["DOCUMENT_NOTE_TRANS_KEY_" . strtoupper($data['document_status'])] : ''
                ]);

                if (isset($data['notes'])) {
                    DocumentsNotes::storeNotes(['document_uuid' => $document->uuid, 'trans_key' => DocumentsNotes::DOCUMENT_NOTE_TRANS_KEY_SAVE_NOTE, 'note' => $data['notes']]);
                }
            }

            return $document;
        });
    }

    /**
     * Function to Update data
     *
     * @param array $data
     * @param int $id
     *
     * @throws TaxServiceTjCompanyInactiveException
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function update(array $data, int $id)
    {
        $document = Document::where('id', $id)->firstOrFail();

        $folders = Folder::folderOwnerOrCreator()->groupBy('folder.id')->active()->pluck('id');
        $folderIds = $folders->toArray();

        //
        $data['subject_for_approvement'] = !isset($data['subject_for_approvement']) ?: Document::FALSE;
        if (isset($data['document_status']) && ($data['document_status'] == Document::DOCUMENT_STATUS_REJECTED || $data['document_status'] == Document::DOCUMENT_STATUS_CONFIRMED)) {
            $data['subject_for_approvement'] = $document->subject_for_approvement;
        }

        if (isset($data['approve_type']) && $data['approve_type'] == Document::APPROVE_OTHER) {
            $data['approve_agency_tax_id'] = null;
        }

        // Upload Document file
        if (!empty($data['document_file'])) {
            $gUploader = new GUploader('document_report');
            $data['document_file'] = $gUploader->storePerm($data['document_file'], $document->generateFilePermPath());
        }

        // Access Key
        if (isset($data['regenerate_access_key'])) {
            $data['document_access_password'] = $this->generateUniqueCode();
        }

        // Holder Data if Company/Natural Person
        if (isset($data['holder_type'])) {
            $legalEntityForms = LegalEntityForms::getInstance();
            $holderData = $legalEntityForms->holderDataByType($data['holder_type'], $data['holder_type_value'], $data['holder_passport'] ?? null);

            $data = array_merge($data, $holderData);
        }

        DB::transaction(function () use ($data, $document, $folderIds) {

            $document->show_status = Document::STATUS_INACTIVE;
            $document->save();

            $oldData = $document->toArray();
            $oldData['document_release_date'] = $document->getOriginal('document_release_date');

            $oldDocumentId = !empty($oldData) ? $oldData['id'] : null;
            $newDocument = new Document(array_merge($oldData, $data));
            $newDocument->show_status = Document::STATUS_ACTIVE;
            $newDocument->version = $document->version + 1;
            $newDocument->save();

            // @fixme
            if (!(isset($data['document_access_password'])) && !($document->isDocumentAccessPasswordChanged($document->uuid) && !($document->isDocumentOwnerOrCreator()))) {

                FolderDocuments::where('document_id', $oldDocumentId)->whereNotIN('folder_id', $folderIds)->update(['document_id' => $newDocument->id]);
            }

            empty($data['folders']) ? $folders = $folderIds : $folders = array_diff($folderIds, $data['folders']);

            if (!empty($folders)) {
                FolderDocuments::where('document_id', $document->id)->whereIn('folder_id', $folders)->delete();
            }

            if (isset($data['folders']) && count($data['folders']) > 0) {
                foreach ($data['folders'] as $folder) {
                    FolderDocuments::insert([
                        'folder_id' => $folder,
                        'document_id' => $newDocument->id,
                        'user_id' => Auth::id()
                    ]);
                }
            }

            if ($data) {
                DocumentsNotes::storeNotes([
                    'document_uuid' => $document->uuid,
                    'trans_key' => isset($data['document_status']) ? DocumentsNotes::DOCUMENT_NOTE_TRANS_KEY_STATUSES["DOCUMENT_NOTE_TRANS_KEY_" . strtoupper($data['document_status'])] : DocumentsNotes::DOCUMENT_NOTE_TRANS_KEY_SAVE,
                    'version' => $newDocument->version,
                    'old_data' => $oldData,
                    'new_data' => $newDocument->toArray(),
                ]);

                if (isset($data['notes'])) {
                    DocumentsNotes::storeNotes(['document_uuid' => $document->uuid, 'trans_key' => DocumentsNotes::DOCUMENT_NOTE_TRANS_KEY_SAVE_NOTE, 'note' => $data['notes'], 'version' => $newDocument->version]);
                }
            }

        });
    }

    /**
     * Function to store electronic document
     *
     * @param $applicationId
     * @param $stateId
     * @param $subApplicationAccessToken
     */
    public function storeElectronicDocument($applicationId, $stateId, $subApplicationAccessToken)
    {
        $subApplication = SingleApplicationSubApplications::find($applicationId);

        $referenceClassificator = getReferenceRows(ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS, $subApplication->reference_classificator_id);
        $authUserCreator = Auth::user()->getCreator();

        $saf = SingleApplication::select('regime', 'data')->where('regular_number', $subApplication->saf_number)->first();

        if ($saf->regime == SingleApplication::REGIME_EXPORT || $saf->regime == SingleApplication::REGIME_IMPORT) {

            $holderData = $saf->data['saf']['sides'][$saf->regime];

            $data['holder_type'] = $holderData['type'];
            $data['holder_type_value'] = $holderData['type_value'];
            $data['holder_name'] = $holderData['name'];
            $data['holder_country'] = $holderData['country'] ?? '-';
            $data['holder_community'] = $holderData['community'] ?? '-';
            $data['holder_address'] = $holderData['address'] ?? '-';
            $data['holder_phone_number'] = $holderData['phone_number'];
            $data['holder_email'] = $holderData['email'];

            if (!ReferenceTable::isLegalEntityType($holderData['type'], BaseModel::ENTITY_TYPE_TAX_ID)) {
                $data['holder_passport'] = $holderData['passport'] ?? '-';
            }
        }

        if ($saf->regime == SingleApplication::REGIME_TRANSIT) {
            $data['holder_type'] = $authUserCreator->type;
            $data['holder_type_value'] = $authUserCreator->ssn;
            $data['holder_name'] = $authUserCreator->name;
            $data['holder_country'] = $authUserCreator->country;
            $data['holder_community'] = $authUserCreator->community;
            $data['holder_address'] = $authUserCreator->address;
            $data['holder_phone_number'] = $authUserCreator->phone_number;
            $data['holder_email'] = $authUserCreator->email;
            $data['holder_passport'] = $authUserCreator->passport;
        }

        $data['sub_application_id'] = $applicationId;
        $data['state_id'] = $stateId;
        $data['document_type_id'] = optional($referenceClassificator)->id ?? 0;
        $data['document_type'] = optional($referenceClassificator)->code ?? '';
        $data['document_name'] = optional($referenceClassificator)->name ?? '';
        $data['document_description'] = optional($referenceClassificator)->name ?? '';
        $data['document_number'] = $subApplication->document_number;
        $data['document_release_date'] = currentDate();
        $data['document_file'] = '';
        $data['created_from_module'] = Document::DOCUMENT_CREATE_MODULE_APPLICATION;
        $data['document_status'] = Document::DOCUMENT_STATUS_CONFIRMED;

        $data['approve_type'] = Document::APPROVE_AGENCY;
        $data['approve_agency_tax_id'] = Auth::user()->companyTaxId();
        $data['document_form'] = Document::DOCUMENT_FORM_ELECTRONIC;
        $data['user_id'] = Auth::user()->getCreatorUser()->id;
        $data['creator_user_id'] = Auth::id();
        $data['document_access_password'] = $subApplicationAccessToken ?? '';

        $data['company_tax_id'] = Auth::user()->companyTaxId();
        $data['creator_type'] = $authUserCreator->type;
        $data['creator_type_value'] = $authUserCreator->ssn;
        $data['creator_name'] = $authUserCreator->name;
        $data['creator_country'] = $authUserCreator->country;
        $data['creator_address'] = $authUserCreator->address;
        $data['creator_phone_number'] = $authUserCreator->phone_number;
        $data['creator_email'] = $authUserCreator->email;
        $data['show_status'] = Document::STATUS_ACTIVE;
        $data['subject_for_approvement'] = Document::TRUE;

        $documentElectronic = Document::where('sub_application_id', $applicationId)->orderByDesc()->select('id')->first();

        DB::transaction(function () use ($data, $documentElectronic) {

            if (!is_null($documentElectronic)) {
                $this->update($data, $documentElectronic->id);
            } else {

                $data['uuid'] = $this->generateUniqueCode(Document::DOCUMENT_TYPE_PREFIX_ELECTRONIC_DOCUMENT);

                $document = new Document($data);
                $document->save();
            }

        });
    }

    /**
     * Function to Delete data
     *
     * @param array $deleteItems
     */
    public function delete(array $deleteItems)
    {
        DB::transaction(function () use ($deleteItems) {
            foreach ($deleteItems as $item) {

                $document = Document::where('id', $item)->documentOwnerOrCreator()->first();

                if ($document->document_status == Document::DOCUMENT_STATUS_CREATED) {
                    Document::where('id', $item)->update([
                        'show_status' => Document::STATUS_DELETED,
                        'deleted_admin_id' => Auth::user()->id,
                        'deleted_admin_ip' => request()->ip()
                    ]);
                }
            }
        });
    }

    /**
     * Function to generate new unique code
     *
     * @param string $prefix
     * @return string
     */
    public function generateUniqueCode($prefix = Document::DOCUMENT_TYPE_PREFIX_DOCUMENT)
    {
        $generateID =  uniqid() . uniqid();
        $generateID[0] = $prefix;

        $document = Document::where(['document_access_password' => $generateID])->orWhere(['uuid' => $generateID])->first();

        if (is_null($document)) {
            return $generateID;
        }

        return $this->generateUniqueCode();
    }

    /**
     * Function to Store folder to DB
     *
     * @param $folders
     * @param $documentId
     */
    private function storeFolder($folders, $documentId)
    {
        $notUsedFolders = [];

        foreach ($folders as $folderId) {
            $notUsedFolders[] = $folderId;

            $folderDocData = [
                'folder_id' => $folderId,
                'document_id' => $documentId,
            ];

            $folderDocDataUpdate = array_merge(['user_id' => Auth::id()], $folderDocData);

            FolderDocuments::updateOrCreate($folderDocData, $folderDocDataUpdate);

        }

        FolderDocuments::whereNotIn('folder_id', $notUsedFolders)->where(['document_id' => $documentId])->delete();
    }

    /**
     * Function to get attached doc history
     *
     * @param $uuid
     * @return array
     */
    public function getAttachedDocHistory($uuid)
    {
        $documentsIds = Document::where('uuid', $uuid)->pluck('id')->all();

        $attached = [];
        $attachedSafDocs = SingleApplicationDocuments::with(['document','user','saf','expertise','subApplication'])->whereIn('document_id', $documentsIds)->get();
        $attachedFolderDocs = FolderDocuments::with(['document','user','folder'])->whereIn('document_id', $documentsIds)->get();

        foreach ($attachedSafDocs as $doc) {

            if ($doc->added_from_module == SingleApplicationDocuments::ADDED_FROM_MODULE_SAF) {
                $link = urlWithLng("/single-application/edit/{$doc->saf->regime}/{$doc->saf_number}");
                $attachedFrom = $doc->saf_number;
                $icon = 'fas fa-file-alt documents-file-icon';
            } elseif ($doc->added_from_module == SingleApplicationDocuments::ADDED_FROM_MODULE_APPLICATION) {
                $docId = ConstructorDocument::where('document_code', $doc->subApplication->document_code)->pluck('id')->first();
                $link = urlWithLng("/user-documents/{$docId}/application/{$doc->subapplication_id}/edit");
                $attachedFrom = $doc->subApplication->document_number;
                $icon = 'fas fa-file-signature';
            } elseif ($doc->added_from_module == SingleApplicationDocuments::ADDED_FROM_MODULE_LABORATORY) {
                $link = '#';
                $attachedFrom = $doc->expertise->laboratory->current()->name;
                $icon = 'fas fa-flask';
            }

            $userFirstName = $userLastName = '';

            if ($doc->user) {
                $userFirstName = $doc->user->first_name;
                $userLastName = $doc->user->last_name;
            }

            $attached[] = [
                'attached_from' => $attachedFrom ?? '',
                'version' => isset($doc->document) ? $doc->document->version : 1,
                'attached_link' => isset($link) ? $link : '#',
                'attached_date' => $doc->created_at,
                'user' => "{$userFirstName} {$userLastName}",
                'icon' => $icon ?? ''
            ];

        }

        foreach ($attachedFolderDocs as $doc) {
            $attached[] = [
                'attached_from' => isset($doc->folder) ? $doc->folder->folder_name : '',
                'version' => isset($doc->document) ? $doc->document->version : 1,
                'attached_link' => '#',
                'attached_date' => $doc->created_at,
                'user' => "{$doc->user->first_name} {$doc->user->last_name}",
                'icon' => isset($icon) ? $icon : ''
            ];
        }

        return $attached;
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @param $id2
     * @param $uuId
     * @return array
     */
    public function getLogData($id, $id2 = null, $uuId = null)
    {
        if (!is_null($uuId)) {
            return [Document::where('uuid', $uuId)->where('show_status', '1')->first()->toArray(), $uuId];
        } else {
            $data = Document::where('id', $id)->first()->toArray();
            return [$data, $data['uuid']];
        }
    }
}
