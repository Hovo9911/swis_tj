<?php

namespace App\Models\SingleApplicationSubApplicationInstructionsProfile;

use App\Models\BaseModel;
use App\Models\RiskProfile\RiskProfile;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class SingleApplicationSubApplicationInstructionsProfile
 * @package App\Models\SingleApplicationSubApplicationInstructionsProfile
 */
class SingleApplicationSubApplicationInstructionsProfile extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'saf_sub_application_instructions_profile';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var null
     */
    protected $primaryKey = null;

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'saf_sub_application_instructions_id',
        'profile_id',
    ];

    /**
     * Function to relate with RiskProfile
     *
     * @return HasOne
     */
    public function profile()
    {
        return $this->hasOne(RiskProfile::class, 'id', 'profile_id');
    }
}
