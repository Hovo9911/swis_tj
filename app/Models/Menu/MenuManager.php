<?php

namespace App\Models\Menu;

use App\Models\BaseModel;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class MenuManager
 * @package App\Models\Menu
 */
class MenuManager extends BaseModel
{
    /**
     * Function to get auth user current role menus and return menu tree
     *
     * @param bool|array $currentRoleMenuIds
     * @return array
     */
    public static function menuTree($currentRoleMenuIds = false)
    {
        if (!$currentRoleMenuIds) {
            $currentRoleMenuIds = Auth::user()->getCurrentRoleMenus();
        }

        $data = [];
        if (!is_null($currentRoleMenuIds)) {

            $menus = DB::table('menu')->whereNotIn('name', Menu::NOT_SHOW_IN_LEFT_MENU);

            if (!Auth::user()->isSwAdmin()) {
                $menus->where('name', '!=', Menu::REFERENCE_TABLE_FORM_MENU_NAME);

                // Remove Ref menu
                if (($key = array_search(Menu::REFERENCE_TABLE_FORM_MENU_ID, $currentRoleMenuIds)) !== false) {
                    unset($currentRoleMenuIds[$key]);
                }
            }

            $menus = $menus->whereIn('id', $currentRoleMenuIds)->orWhereIn('parent_id', $currentRoleMenuIds)->where('show_status', BaseModel::STATUS_ACTIVE)->get();

            // Menu Tree creating
            if ($menus->count() > 0) {

                $menus = $menus->sortBy('parent_id');

                foreach ($menus as $menu) {

                    if (is_null($menu->parent_id)) {
                        $data[$menu->id] = $menu;
                    } else {
                        $data[$menu->parent_id]->sub[] = $menu;
                    }
                }
            }

            $data = collect($data)->sortBy('sort_order');

            foreach ($data as &$item) {
                if (!empty($item->sub) && !empty($item->name) && $item->name == Menu::REFERENCE_TABLE_FORM_MENU_NAME) {
                    foreach ($item->sub as &$sub) {
                        $sub->titleForView = ucfirst(trans($sub->title));
                    }
                }
            }

            foreach ($data as $item) {
                if (!empty($item->sub) && !empty($item->name)) {
                    if ($item->name == Menu::REFERENCE_TABLE_FORM_MENU_NAME) {
                        $item->sub = collect($item->sub)->sortBy('titleForView');
                    }

                    if ($item->name == Menu::SINGLE_APPLICATION_MENU_NAME) {
                        $item->sub = collect($item->sub)->sortBy('sort_order');
                    }
                }
            }
        }

        return $data;
    }

    /**
     * Function to get Menu Modules of current User
     *
     * @param bool $currentRoleMenuIds
     * @return Menu|Collection
     */
    public static function menuModules($currentRoleMenuIds = false)
    {
        $menus = collect();

        if (!$currentRoleMenuIds) {
            $currentRoleMenuIds = Auth::user()->getCurrentRoleMenus();
        }

        if (!is_null($currentRoleMenuIds)) {
            $menus = Menu::whereIn('id', $currentRoleMenuIds)->orWhereIn('parent_id', $currentRoleMenuIds)->isModule()->orderBy('sort_order')->get();
        }

        return $menus;
    }

}
