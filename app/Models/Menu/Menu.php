<?php

namespace App\Models\Menu;

use App\Models\BaseModel;
use App\Models\RoleMenus\RoleMenus;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Query\Builder;

/**
 * Class Menu
 * @package App\Models\Menu
 */
class Menu extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'menu';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'title',
        'icon',
        'href',
        'sort_order',
        'parent_id',
        'show_status',
        'is_module'
    ];

    /**
     * @var string
     */
    const ADMIN_MENU_NAME = 'admin_menu';
    const DICTIONARY_MENU_NAME = 'dictionary_menu';
    const LANGUAGES_MENU_NAME = 'languages_menu';

    const AGENCY_MENU_NAME = 'agency_menu';
    const LABORATORY_MENU_NAME = 'laboratory_menu';
    const REGIONAL_OFFICE_MENU_NAME = 'regional_office_menu';
    const BROKER_MENU_NAME = 'broker_menu';

    const USER_MANAGEMENT_MENU_NAME = 'user_management_role';
    const USER_MENU_NAME = 'user_menu';
    const ROLE_MENU_NAME = 'role_menu';
    const ROLE_GROUP_MENU_NAME = 'role_group_menu';

    const AUTHORIZE_PERSON_MENU_NAME = 'authorize_person_menu';
    const DATA_MODEL_MENU_NAME = 'data_model_menu';

    const REFERENCE_TABLE_MENU_NAME = 'reference_table_menu';
    const REFERENCE_TABLE_FORM_MENU_NAME = 'reference_table_form_menu';

    const DOCUMENT_CLOUD_MENU_NAME = 'document_cloud_menu';
    const DOCUMENT_CLOUD_FOLDER_MENU_NAME = 'document_folder_menu';
    const DOCUMENT_CLOUD_DOCUMENT_MENU_NAME = 'document_document_menu';

    const SINGLE_APPLICATION_MENU_NAME = 'single_application_menu';
    const SINGLE_APPLICATION_IMPORT_MENU_NAME = 'single_application_import_menu';
    const SINGLE_APPLICATION_EXPORT_MENU_NAME = 'single_application_export_menu';
    const SINGLE_APPLICATION_TRANSIT_MENU_NAME = 'single_application_transit_menu';

    const ROUTING_MENU_NAME = 'routing_menu';

    const CONSTRUCTOR_MENU_NAME = 'constructor_menu';
    const CONSTRUCTOR_DOCUMENTS_MENU_NAME = 'constructor_documents_menu';
    const CONSTRUCTOR_STATES_MENU_NAME = 'constructor_states_menu';
    const CONSTRUCTOR_ACTIONS_MENU_NAME = 'constructor_actions_menu';
    const CONSTRUCTOR_DATA_SET_MENU_NAME = 'constructor_data_set_menu';
    const CONSTRUCTOR_ROLE_MENU_NAME = 'constructor_role_menu';
    const CONSTRUCTOR_MAPPING_NAME = 'constructor_mapping_menu';
    const CONSTRUCTOR_LISTS_NAME = 'constructor_lists_menu';
    const CONSTRUCTOR_TEMPLATES_NAME = 'constructor_templates_menu';

    const USER_DOCUMENTS_MENU_NAME = 'user_documents_menu';

    const PAYMENTS_MENU_NAME = 'payments_menu';
    const PAYMENT_AND_OBLIGATIONS_MENU_NAME = 'payment_and_obligation';

    const REPORTS_MENU_NAME = 'reports';
    const JASPER_REPORTS_MENU_NAME = 'jasper_reports';

    const CUSTOMS_PORTAL_MENU_NAME = 'customs_portal';
    const LABORATORY_INDICATOR_MENU = 'laboratory_indicator_menu';
    const BALANCE_OPERATIONS_MENU = 'balance_operations';

    const MONITORING_MENU_NAME = 'monitoring';

    const TRANSPORT_MINISTRY_MENU_NAME = 'transport_application_menu';

    const CALENDAR_MENU_NAME = 'calendar';

    /**
     * @var int
     */
    const AUTHORIZE_PERSON_MENU_ID = 10;
    const REFERENCE_TABLE_FORM_MENU_ID = 15;

    /**
     * @var array
     */
    const NOT_SHOW_IN_LEFT_MENU = [self::USER_DOCUMENTS_MENU_NAME, self::REPORTS_MENU_NAME];

    /**
     * @var array
     */
    const MENUS_FOR_TRANSPORT_APPLICATION = [
        self::REFERENCE_TABLE_FORM_MENU_NAME,
        self::TRANSPORT_MINISTRY_MENU_NAME,
        self::AUTHORIZE_PERSON_MENU_NAME
    ];

    /**
     * @var array
     */
    const SHOW_ONLY_SELF_ALL_MENU_NAMES = [
        self::REPORTS_MENU_NAME,
        self::AUTHORIZE_PERSON_MENU_NAME,
        self::MONITORING_MENU_NAME,
        self::BALANCE_OPERATIONS_MENU,
        self::PAYMENT_AND_OBLIGATIONS_MENU_NAME,
        self::JASPER_REPORTS_MENU_NAME,
        self::CALENDAR_MENU_NAME,
    ];

    /**
     * @var array
     */
    const MENUS_ONLY_HAS_ONLY_ALL_FUNCTIONALITY_ROLE = [
        self::REPORTS_MENU_NAME,
        self::MONITORING_MENU_NAME,
        self::BALANCE_OPERATIONS_MENU,
        self::BALANCE_OPERATIONS_MENU,
        self::JASPER_REPORTS_MENU_NAME,
        self::CALENDAR_MENU_NAME,
    ];

    /**
     * Function to relate with RoleMenus
     *
     * @return BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(RoleMenus::class, 'id', 'menu_id');
    }

    /**
     * Function scope to get only menu modules
     *
     * @param $query
     * @return Builder
     */
    public function scopeIsModule($query)
    {
        return $query->where('is_module', Menu::TRUE);
    }

    /**
     * Function to get Menu Id By Name
     *
     * @param $menuName
     * @return integer|null
     */
    public static function getMenuIdByName($menuName)
    {
        if (!empty($menuName)) {

            $menu = Menu::select('id')->where('name', $menuName)->first();

            if (!is_null($menu)) {
                return $menu->id;
            }
        }

        return null;
    }

    /**
     * Function to get Menu name By Id
     *
     * @param $menuId
     * @return integer|null
     */
    public static function getMenuNameById($menuId)
    {
        $menu = Menu::select('name')->where('id', $menuId)->first();

        if (!is_null($menu)) {
            return $menu->name;
        }

        return null;
    }

    /**
     * Function to get show_only_self types by menu
     *
     * @param $menuName
     * @return array
     */
    public static function getMenuShowOnlySelfPermissions($menuName)
    {
        $data = [
            trans('swis.authorize_person.accessibility.others_too') => Menu::FALSE
        ];

        if (!in_array($menuName, self::SHOW_ONLY_SELF_ALL_MENU_NAMES)) {
            $data[trans('swis.authorize_person.accessibility.only_self')] = Menu::TRUE;
        }

        return array_reverse($data);
    }

}