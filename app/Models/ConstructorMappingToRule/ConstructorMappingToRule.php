<?php

namespace App\Models\ConstructorMappingToRule;

use App\Models\BaseModel;
use App\Models\ConstructorRules\ConstructorRules;

/**
 * Class ConstructorMappingToRule
 * @package App\Models\ConstructorMappingToRule
 */
class ConstructorMappingToRule extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'constructor_mapping_to_rule';

    /**
     * @var array
     */
    protected $fillable = [
        'constructor_mapping_id',
        'rule_id'
    ];

    /**
     * Function to relate with ConstructorRules
     */
    public function rule()
    {
        return $this->hasOne(ConstructorRules::class, 'id', 'rule_id');
    }
}
