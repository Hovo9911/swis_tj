<?php

namespace App\Models\ConstructorMappingToRule;

use Illuminate\Support\Facades\DB;

/**
 * Class ConstructorMappingToRuleManager
 * @package App\Models\ConstructorMappingToRule
 */
class ConstructorMappingToRuleManager
{
    /**
     * Function to store data in db
     *
     * @param array $data
     * @param $mappingId
     */
    public function store(array $data, $mappingId)
    {
        $data = $this->getInsertionArray($data, $mappingId);

        $constructorMappingToRule = new ConstructorMappingToRule();

        DB::transaction(function () use ($constructorMappingToRule, $data) {
            $constructorMappingToRule->insert($data);
        });
    }

    /**
     * Function to update data in db
     *
     * @param array $data
     * @param $mappingId
     */
    public function update(array $data, $mappingId)
    {
        $data = $this->getInsertionArray($data, $mappingId);

        $constructorMappingToRule = new ConstructorMappingToRule();

        DB::transaction(function () use ($constructorMappingToRule, $data, $mappingId) {
            $constructorMappingToRule->where('constructor_mapping_id', $mappingId)->delete();
            $constructorMappingToRule->insert($data);
        });
    }

    /**
     * Function to generate insertion array
     *
     * @param $data
     * @param $mappingId
     * @return array
     */
    private function getInsertionArray($data, $mappingId)
    {
        $insertionArray = [];
        foreach ($data as $item) {
            $insertionArray[] = [
                'constructor_mapping_id' => $mappingId,
                'rule_id' => $item
            ];
        }

        return $insertionArray;
    }
}
