<?php

namespace App\Models\UserManuals;

use App\Http\Controllers\Core\DataTableSearch;
use App\Models\UserManualsRoles\UserManualsRoles;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class UserManualsSearch
 * @package App\Models\UserManuals
 */
class UserManualsSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        $result = $query->get();

        foreach ($result as &$item){
            $item->filePath = $item->filePath();
            $item->fileName = $item->fileBaseName();
        }

        return $result;
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($orderBy ?? 'id', $this->orderByType ?? 'desc');
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $query = UserManuals::select('user_manuals.*', 'user_manuals_ml.module', 'user_manuals_ml.description', 'users.first_name', 'users.last_name', 'users.ssn')
            ->joinMl(['f_k' => 'user_manual_id'])
            ->leftJoin('users', 'users.id', '=', 'user_manuals.creator_user_id')
            ->when(Auth::guest(), function ($q) {
                $q->where('available_without_auth', '1');
            })
            ->active();

        if (!Auth::guest()) {
            if (Auth::user()->isAgency()) {

                $query->leftJoin('user_manuals_roles', 'user_manuals.id', '=', 'user_manuals_roles.user_manual_id');
                $query->leftJoin('user_manuals_agencies', 'user_manuals.id', '=', 'user_manuals_agencies.user_manual_id');
                $query->where('user_manuals_roles.role_id', UserManualsRoles::USER_MANUAL_ROLE_TYPE_AGENCY);
                $query->where(function ($q) {
                    $q->orWhere('user_manuals_agencies.company_tax_id', Auth::user()->companyTaxId())
                        ->orWhereNull('user_manuals_agencies.user_manual_id');
                });
            }

            if (Auth::user()->isOnlyBroker() || Auth::user()->isOnlyCompany()) {
                $query->leftJoin('user_manuals_roles', 'user_manuals.id', '=', 'user_manuals_roles.user_manual_id');
                $query->where('user_manuals_roles.role_id', UserManualsRoles::USER_MANUAL_ROLE_TYPE_APPLICANT);
            }

            if (Auth::user()->isNaturalPerson()) {
                $query->leftJoin('user_manuals_roles', 'user_manuals.id', '=', 'user_manuals_roles.user_manual_id');
                $query->where('user_manuals_roles.role_id', UserManualsRoles::USER_MANUAL_ROLE_TYPE_NATURAL_PERSON);
            }

            if (Auth::user()->isCustomsOperator()) {
                $query->leftJoin('user_manuals_roles', 'user_manuals.id', '=', 'user_manuals_roles.user_manual_id');
                $query->where('user_manuals_roles.role_id', UserManualsRoles::USER_MANUAL_ROLE_TYPE_CUSTOMS_PORTAL);
            }

        }

        if (isset($this->searchData['id'])) {
            $query->where(DB::raw('user_manuals.id::varchar'), $this->searchData['id']);
        }

        if (isset($this->searchData['module'])) {
            $query->where('user_manuals_ml.module', 'ILIKE', "%{$this->searchData['module']}%");
        }

        if (isset($this->searchData['description'])) {
            $query->where('user_manuals_ml.description', 'ILIKE', "%{$this->searchData['description']}%");
        }

        if (isset($this->searchData['type'])) {
            $query->where('user_manuals.type', $this->searchData['type']);
        }

        if (isset($this->searchData['permission'])) {
            $query->leftJoin('user_manuals_roles', 'user_manuals.id', '=', 'user_manuals_roles.user_manual_id');
            $query->where('user_manuals_roles.role_id', $this->searchData['permission']);
        }

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}
