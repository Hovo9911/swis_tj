<?php

namespace App\Models\UserManuals;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

/**
 * Class UserManualsMl
 * @package App\Models\UserManuals
 */
class UserManualsMl extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['user_manual_id', 'lng_id'];

    /**
     * @var string
     */
    public $incrementing = 'false';

    /**
     * @var string
     */
    public $table = 'user_manuals_ml';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'user_manual_id',
        'lng_id',
        'module',
        'description',
        'show_status'
    ];
}
