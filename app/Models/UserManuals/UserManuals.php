<?php

namespace App\Models\UserManuals;

use App\Models\BaseModel;
use App\Models\UserManualsAgencies\UserManualsAgencies;
use App\Models\UserManualsRoles\UserManualsRoles;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class UserManuals
 * @package App\Models\UserManuals
 */
class UserManuals extends BaseModel
{
    /**
     * @var string
     */
    const USER_MANUAL_TYPES = ['user_manuals', 'videos', 'technical_specifications', 'etc'];
    const FILE_PATH = 'user-manuals';
    const PAGE_TYPE_USER_MANUALS = 'user-manuals';
    const PAGE_TYPE_HELP = 'help';

    /**
     * @var string
     */
    public $table = 'user_manuals';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'module_name',
        'description',
        'type',
        'version',
        'version_date',
        'file',
        'permission',
        'url',
        'available_without_auth',
        'creator_user_id',
        'edited_user_id',
        'show_status'
    ];

    /**
     * Function to get version date by custom date format
     *
     * @param $value
     * @return string
     */
    public function getVersionDateAttribute($value)
    {
        return Carbon::parse($value)->format(config('swis.date_format_front'));
    }

    /**
     * Function to get updated_at by custom date format
     *
     * @param $value
     * @return string
     */
    public function getUpdatedAtAttribute($value)
    {
        return Carbon::parse($value)->format(config('swis.date_format_front'));
    }

    /**
     * Function to relate with UserManualsMl
     *
     * @return HasMany
     */
    public function ml()
    {
        return $this->hasMany(UserManualsMl::class, 'user_manual_id', 'id');
    }

    /**
     * Function to relate with UserManualsRoles
     *
     * @return HasMany
     */
    public function userManualsRoles()
    {
        return $this->hasMany(UserManualsRoles::class, 'user_manual_id', 'id');
    }

    /**
     * Function to relate with UserManualsAgencies
     *
     * @return HasMany
     */
    public function userManualsAgencies()
    {
        return $this->hasMany(UserManualsAgencies::class, 'user_manual_id', 'id');
    }

    /**
     * Function to get file basename
     *
     * @return string
     */
    public function fileBaseName()
    {
        if ($this->filePath()) {
            $file = pathinfo($this->filePath());

            return $file['basename'];
        }

        return "";
    }

    /**
     * Function to get file path
     *
     * @return string
     */
    public function filePath()
    {
        if ($this->file) {
            return url('files/uploads/user-manuals/' . $this->file);
        }

        return "";
    }
}
