<?php

namespace App\Models\UserManuals;

use App\GUploader;
use App\Models\BaseMlManager;
use App\Models\UserManualsAgencies\UserManualsAgencies;
use App\Models\UserManualsRoles\UserManualsRoles;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class UserManualsManager
 * @package App\Models\UserManuals
 */
class UserManualsManager
{
    use BaseMlManager;

    /**
     * Function to store Validated Data
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $data['version_date'] = Carbon::now()->format(config('swis.date_format'));
        $data['creator_user_id'] = Auth::user()->id;

        $userManuals = new UserManuals($data);
        $userManualsMl = new UserManualsMl();

        if (!empty($data['file'])) {
            $gUploader = new GUploader('user_manuals');
            $gUploader->storePerm($data['file']);
        }

        // Roles, Agencies
        $userManualRoleData = $userManualAgencyData = [];

        if (isset($data['roles'])) {
            foreach ($data['roles'] as $role) {
                $roleData['role_id'] = $role;
                $userManualRoleData[] = new UserManualsRoles($roleData);
            }

            if (in_array(UserManualsRoles::USER_MANUAL_ROLE_TYPE_AGENCY, $data['roles']) && isset($data['agencies'])) {
                foreach ($data['agencies'] as $agency) {
                    $agencyData['company_tax_id'] = $agency;
                    $userManualAgencyData[] = new UserManualsAgencies($agencyData);
                }
            }
        }

        DB::transaction(function () use ($data, $userManuals, $userManualsMl, $userManualRoleData, $userManualAgencyData) {
            $userManuals->save();
            $this->storeMl($data['ml'], $userManuals, $userManualsMl);

            // Roles
            if (count($userManualRoleData) > 0) {
                $userManuals->userManualsRoles()->saveMany($userManualRoleData);
            }

            // Agencies
            if (count($userManualAgencyData) > 0) {
                $userManuals->userManualsAgencies()->saveMany($userManualAgencyData);
            }

        });
    }

    /**
     * Function to update data
     *
     * @param array $data
     * @param int $id
     */
    public function update(array $data, int $id)
    {
        $userManuals = UserManuals::where('id', $id)->firstOrFail();
        $userManualsMl = new UserManualsMl();

        $data['edited_user_id'] = Auth::user()->id;

        if (!empty($data['file'])) {
            $gUploader = new GUploader('user_manuals');
            $gUploader->storePerm($data['file']);
        }

        // Roles
        $userManualRoleData = $userManualAgenciesData = [];

        if (isset($data['roles'])) {
            foreach ($data['roles'] as $role) {
                $roleData['role_id'] = $role;
                $userManualRoleData[] = new UserManualsRoles($roleData);
            }
        }

        // Agencies
        if (isset($data['agencies'])) {
            foreach ($data['agencies'] as $agency) {
                $agenciesData['company_tax_id'] = $agency;
                $userManualAgenciesData[] = new UserManualsAgencies($agenciesData);
            }
        }

        DB::transaction(function () use ($data, $userManuals, $userManualsMl, $userManualRoleData, $userManualAgenciesData, $id) {

            $userManuals->update($data);
            $this->updateMl($data['ml'], 'user_manual_id', $userManuals, $userManualsMl);

            // Roles
            if (isset($data['roles'])) {

                UserManualsRoles::where('user_manual_id', $id)->delete();
                $userManuals->userManualsRoles()->saveMany($userManualRoleData);

                if (!(in_array(UserManualsRoles::USER_MANUAL_ROLE_TYPE_AGENCY, $data['roles']))) {
                    UserManualsAgencies::where('user_manual_id', $id)->delete();
                }

                // Agencies
                if (isset($data['agencies']) && in_array(UserManualsRoles::USER_MANUAL_ROLE_TYPE_AGENCY, $data['roles']) or in_array(UserManualsRoles::USER_MANUAL_ROLE_TYPE_AGENCY, $data['roles'])) {

                    UserManualsAgencies::where('user_manual_id', $id)->delete();
                    $userManuals->userManualsAgencies()->saveMany($userManualAgenciesData);
                }

            }
        });
    }

    /**
     * Function to Delete data
     *
     * @param array $deleteItems
     */
    public function delete(array $deleteItems)
    {
        DB::transaction(function () use ($deleteItems) {

            UserManualsRoles::whereIn('user_manual_id', $deleteItems)->delete();
            UserManualsAgencies::whereIn('user_manual_id', $deleteItems)->delete();

            UserManuals::whereIn('id', $deleteItems)->update([
                'show_status' => UserManuals::STATUS_DELETED
            ]);

        });
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @param $id2
     * @return array
     */
    public function getLogData($id, $id2 = null)
    {
        return UserManuals::where('id', $id)->with('ml')->first()->toArray();
    }
}