<?php

namespace App\Models\LaboratoryIndicator;

use App\Models\BaseModel;

/**
 * Class LaboratoryIndicator
 * @package App\Models\LaboratoryIndicator
 */
class LaboratoryIndicator extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'laboratory_indicator';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'company_tax_id',
        'user_id',
        'laboratory_id',
        'indicator_id',
        'price',
        'duration',
        'show_status',
        'indicator_code'
    ];

}
