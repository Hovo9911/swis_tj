<?php

namespace App\Models\LaboratoryIndicator;

use Illuminate\Support\Facades\Auth;

/**
 * Class LaboratoryIndicatorManager
 * @package App\Models\LaboratoryIndicator
 */
class LaboratoryIndicatorManager
{
    /**
     * Function to Store data to DB
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $data['user_id'] = Auth::id();

        if (!Auth()->user()->isSwAdmin()) {
            $data['company_tax_id'] = Auth::user()->companyTaxId();
        }

        $laboratoryIndicator = new LaboratoryIndicator($data);
        $laboratoryIndicator->save();
    }

    /**
     * Function to Update data
     *
     * @param array $data
     * @param $id
     */
    public function update(array $data, $id)
    {
        $laboratoryIndicator = LaboratoryIndicator::where('id', $id)->checkUserHasRole()->firstOrFail();
        $laboratoryIndicator->update($data);
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @param null $parentId
     * @return array
     */
    public function getLogData($id = null, $parentId = null)
    {
        if (!is_null($id)) {
            return LaboratoryIndicator::where('id', $id)->first();
        }

        return [];
    }
}
