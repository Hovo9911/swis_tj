<?php

namespace App\Models\LaboratoryIndicator;

use App\Http\Controllers\Core\DataTableSearch;
use Illuminate\Support\Facades\DB;

/**
 * Class LaboratoryIndicatorSearch
 * @package App\Models\LaboratoryIndicator
 */
class LaboratoryIndicatorSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        return $query->get();
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($this->orderByCol, $this->orderByType ?? 'desc');
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $rTable = 'reference_lab_expertise_indicator';
        $rTableMl = 'reference_lab_expertise_indicator_ml';

        $query = LaboratoryIndicator::select('laboratory_indicator.*', DB::raw("CONCAT({$rTable}.code, ' , ', {$rTableMl}.name) as indicator"))
            ->join($rTable, 'laboratory_indicator.indicator_id', '=', $rTable . '.id')
            ->join($rTableMl, $rTable . '.id', '=', $rTableMl . '.' . $rTable . '_id')
            ->where('lng_id', cLng('id'))
            ->where('laboratory_id', $this->searchData['laboratory_id'] ?? 0)
            ->notDeleted();

        if (isset($this->searchData['id'])) {
            $query->where(DB::raw('laboratory_indicator.id::varchar'), $this->searchData['id']);
        }

        if (isset($this->searchData['laboratory_id'])) {
            $query->where('laboratory_id', $this->searchData['laboratory_id']);
        }

        $query->checkUserHasRole();

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}
