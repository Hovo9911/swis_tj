<?php

namespace App\Models\SingleApplicationSubApplicationInstructions;

use App\Models\SingleApplicationSubApplicationInstructionsProfile\SingleApplicationSubApplicationInstructionsProfile;
use Illuminate\Support\Facades\DB;

/**
 * Class SingleApplicationSubApplicationInstructionsManager
 * @package App\Models\SingleApplicationSubApplicationInstructionsManager
 */
class SingleApplicationSubApplicationInstructionsManager
{
    /**
     * Function to store single application sub application instructions
     *
     * @param $applicationId
     * @param $instruction
     * @param $profiles
     * @param $instructionsIds
     */
    public function store($applicationId, $instruction, $profiles, $instructionsIds)
    {
        DB::transaction(function () use ($applicationId, $instruction, $profiles, $instructionsIds) {

            $subApplicationInstruction = SingleApplicationSubApplicationInstructions::create([
                'sub_application_id' => $applicationId,
                'instruction_code' => $instruction,
                'instruction_id' => $instructionsIds[$instruction]
            ]);

            foreach ($profiles as $profile) {
                SingleApplicationSubApplicationInstructionsProfile::create([
                    'saf_sub_application_instructions_id' => $subApplicationInstruction->id,
                    'profile_id' => $profile,
                ]);
            }

        });
    }
}
