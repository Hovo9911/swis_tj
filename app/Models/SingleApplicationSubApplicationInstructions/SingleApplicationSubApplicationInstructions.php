<?php

namespace App\Models\SingleApplicationSubApplicationInstructions;

use App\Models\BaseModel;
use App\Models\Instructions\Instructions;
use App\Models\InstructionsToFeedBacks\InstructionsToFeedBacks;
use App\Models\SingleApplicationSubApplicationInstructionsProfile\SingleApplicationSubApplicationInstructionsProfile;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Auth;

/**
 * Class SingleApplicationSubApplicationInstructions
 * @package App\Models\SingleApplicationSubApplicationInstructions
 */
class SingleApplicationSubApplicationInstructions extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'saf_sub_application_instructions';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'sub_application_id',
        'instruction_code',
        'instruction_id',
    ];

    /**
     * Function to relate with SingleApplicationSubApplicationInstructionsProfile
     *
     * @return HasMany
     */
    public function subApplicationInstructionsProfile()
    {
        return $this->hasMany(SingleApplicationSubApplicationInstructionsProfile::class, 'saf_sub_application_instructions_id', 'id');
    }

    /**
     * Function to relate with SingleApplicationSubApplicationInstructionsProfile
     *
     * @return HasMany
     */
    public function instructionFeedback()
    {
        return $this->hasMany(InstructionsToFeedBacks::class, 'instructions_id', 'instructions_id');
    }

    /**
     * Function to get Instructions
     *
     * @param $application
     * @param array $select
     * @param $tabIsEditable
     * @return array
     */
    public static function getInstructions($application, $select = [], $tabIsEditable = false)
    {
        if (empty($select)) {
            $select = [
                'instructions.id as instructions_id',
                'saf_sub_application_instruction_feedback.feedback_id as feedback',
                'saf_sub_application_instruction_feedback.note as feedback_note',
                'instructions.code as instructions_code',
                'instructions_ml.description',
                'saf_sub_application_instructions.sub_application_id',
                'saf_sub_application_instructions.id',
                'saf_sub_application_instruction_feedback.feedback_code',
            ];
        }

        $feedback = [];

        $generatedInstructions = self::select($select)
            ->join('instructions', function ($q) use ($tabIsEditable) {
                if (!$tabIsEditable) {
                    $q->on('instructions.id', '=', 'saf_sub_application_instructions.instruction_id');
                } else {
                    $q->on('instructions.code', '=', 'saf_sub_application_instructions.instruction_code')->where('company_tax_id', Auth::user()->companyTaxId())->where('show_status', BaseModel::STATUS_ACTIVE);
                }
            })
            ->join('instructions_ml', function ($q) {
                $q->on('instructions_ml.instruction_id', '=', 'instructions.id')->where('lng_id', cLng('id'));
            })
            ->leftJoin('saf_sub_application_instruction_feedback', 'saf_sub_application_instruction_feedback.saf_sub_application_instruction_id', '=', 'saf_sub_application_instructions.id')
            ->where('sub_application_id', $application->id)
            ->orderby('saf_sub_application_instructions.id')
            ->get();

        $getFeedback = Instructions::getFeedback(!$tabIsEditable);

        foreach ($generatedInstructions as $instruction) {
            if ($tabIsEditable) {

                $instructionFeedback = [];
                $activeIndicatorRow = Instructions::where('code', $instruction->instructions_code)->orderByDesc()->active()->first();
                if ($activeIndicatorRow) {
                    $instructionFeedback = InstructionsToFeedBacks::where('instructions_id', $activeIndicatorRow->id)->get();
                }

                foreach ($instructionFeedback as $instructionItem) {
                    $feedback[$instruction->instructions_id][] = [
                        'name' => $getFeedback[$instructionItem->code]->name,
                        'value' => $getFeedback[$instructionItem->code]->id,
                        'selected' => ($instruction->feedback_code == $instructionItem->code) ? 'selected' : '',
                    ];
                }
            } else {
                if (isset($getFeedback[$instruction->feedback])) {
                    $feedback[$instruction->instructions_id][] = [
                        'name' => $getFeedback[$instruction->feedback]->name,
                        'value' => $getFeedback[$instruction->feedback]->id,
                        'selected' => 'selected',
                    ];
                }
            }

        }

        return ['instructions' => $generatedInstructions, 'feedback' => $feedback];
    }

}
