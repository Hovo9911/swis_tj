<?php

namespace App\Models\RegionalOfficeConstructorDocuments;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

/**
 * Class RegionalOffice
 * @package App\Models\RegionalOffice
 */
class RegionalOfficeConstructorDocuments extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['regional_office_id', 'lng_id'];

    /**
     * @var string
     */
    public $incrementing = 'false';

    /**
     * @var string
     */
    public $table = 'regional_office_constructor_documents';

    /**
     * @var array
     */
    protected $fillable = [
        'regional_office_id',
        'constructor_document_code',
    ];
}
