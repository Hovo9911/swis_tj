<?php

namespace App\Models\Broker;

use App\Models\BaseModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Broker
 * @package App\Models\Broker
 */
class Broker extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'broker';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'tax_id',
        'show_status',
        'certification_number',
        'certification_period_validity',
        'legal_entity_name',
        'address',
        'phone_number',
        'email',
    ];

    /**
     * Function to get certificate period validity
     *
     * @param $value
     * @return string|void
     */
    public function getCertificationPeriodValidityAttribute($value)
    {
        if ($value) {
            return Carbon::parse($value)->format(config('swis.date_format_front'));
        }
    }

    /**
     * Function to return active languages
     *
     * @return HasMany
     */
    public function ml()
    {
        return $this->hasMany(BrokerMl::class, 'broker_id', 'id');
    }

    /**
     * Function to return current language
     *
     * @return HasOne
     */
    public function current()
    {
        return $this->hasOne(BrokerMl::class, 'broker_id', 'id')->notDeleted()->where('lng_id', cLng('id'))->first();
    }

}
