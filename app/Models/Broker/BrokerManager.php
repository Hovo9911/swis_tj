<?php

namespace App\Models\Broker;

use App\Models\BaseMlManager;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\ReferenceTableForm\ReferenceTableFormManager;
use App\Models\UserRolesMenus\UserRolesMenus;
use App\Models\UserRolesMenus\UserRolesMenusManager;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class BrokerManager
 * @package App\Models\Broker
 */
class BrokerManager
{
    use BaseMlManager;

    /**
     * Function to Store data to DB
     *
     * @param array $data
     * @return Broker
     */
    public function store(array $data)
    {
        $data['user_id'] = Auth::id();

        $format = config('swis.date_format') . ' 23:59:59';
        $data['certification_period_validity'] = Carbon::parse($data['certification_period_validity'])->format($format);

        $broker = new Broker($data);
        $brokerMl = new BrokerMl();

        // Synchronize Brokers module with Reference Brokers
        $referenceData = [
            'code' => $data['tax_id'],
            'show_status' => $data['show_status'],
            'start_date' => currentDate(),
        ];

        foreach ($data['ml'] as $lngId => $ml) {
            $referenceData['ml'][$lngId] = [
                'name' => $ml['name'],
                'long_name' => $data['legal_entity_name']
            ];
        }

        if ($data['show_status'] == ReferenceTable::STATUS_INACTIVE) {
            $referenceData['end_date'] = currentDate();
        }

        return DB::transaction(function () use ($data, $broker, $brokerMl, $referenceData) {

            $broker->save();
            $this->storeMl($data['ml'], $broker, $brokerMl);

            // ---
            $referenceTableFormManager = new ReferenceTableFormManager();
            $referenceTableFormManager->store($referenceData, ReferenceTable::getReferenceIdByTableName(ReferenceTable::REFERENCE_BROKERS_TABLE_NAME));

            // ---
            $userRolesMenusManager = new UserRolesMenusManager();
            $userRolesMenusManager->updateUserRoleMenus(UserRolesMenus::ROLE_TYPE_BROKER, $broker->tax_id);

            return $broker;
        });
    }

    /**
     * Function to Update data
     *
     * @param array $data
     * @param int $id
     */
    public function update(array $data, int $id)
    {
        $broker = Broker::where('id', $id)->checkUserHasRole()->firstOrFail();
        $brokerMl = new BrokerMl();

        $format = config('swis.date_format') . ' 23:59:59';
        $data['certification_period_validity'] = Carbon::parse($data['certification_period_validity'])->format($format);

        // Synchronize Brokers module with Reference Brokers
        $currentReferenceRow = DB::table(ReferenceTable::REFERENCE_BROKERS)->select('id', 'show_status')->where(['code' => $broker->tax_id])->orderBy('id', 'DESC')->first();

        $referenceData = [];
        if (!is_null($currentReferenceRow)) {

            $referenceData = [
                'start_date' => currentDate(),
                'show_status' => $data['show_status'],
            ];

            if ($data['show_status'] == ReferenceTable::STATUS_INACTIVE) {
                $referenceData['end_date'] = currentDate();
                $referenceData['f_id'] = $currentReferenceRow->id;
            } else {

                $referenceData['code'] = $broker->tax_id;
                foreach ($data['ml'] as $lngId => $ml) {
                    $referenceData['ml'][$lngId] = [
                        'name' => $ml['name'],
                        'long_name' => $data['legal_entity_name']
                    ];
                }
            }
        }

        DB::transaction(function () use ($data, $broker, $brokerMl, $currentReferenceRow, $referenceData) {

            $currentShowStatusBroker = $broker->show_status;

            $broker->update($data);
            $this->updateMl($data['ml'], 'broker_id', $broker, $brokerMl);

            // ---
            if (!is_null($currentReferenceRow)) {
                $referenceTableFormManager = new ReferenceTableFormManager();
                $referenceTableFormManager->update($referenceData, ReferenceTable::getReferenceIdByTableName(ReferenceTable::REFERENCE_BROKERS_TABLE_NAME), $currentReferenceRow->id);
            }

            // ---
            if ($data['show_status'] != $currentShowStatusBroker) {
                $userRolesMenusManager = new UserRolesMenusManager();
                $userRolesMenusManager->updateUserRoleMenus(UserRolesMenus::ROLE_TYPE_BROKER, $broker->tax_id);
            }
        });

    }

    /**
     * Function to inactivating brokers
     */
    public function inactivateBrokers()
    {
        //If broker certification_period_validity < today  inactivate broker row
        $brokers = Broker::select('id', 'tax_id')->active()
            ->where('certification_period_validity', '<', currentDateTime())->get();

        foreach ($brokers as $broker) {

            Broker::where('id', $broker->id)
                ->update(['show_status' => Broker::STATUS_INACTIVE]);

            DB::table(ReferenceTable::REFERENCE_BROKERS)
                ->where(['code' => $broker->tax_id, 'show_status' => ReferenceTable::STATUS_ACTIVE])
                ->update(['end_date' => Carbon::now(), 'show_status' => Broker::STATUS_INACTIVE]);
        }
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @param null $parentId
     * @return array
     */
    public function getLogData($id = null, $parentId = null)
    {
        if (!is_null($id)) {
            $data = Broker::where('id', $id)->first();
            $logData = $data;
            $logData['mls'] = $data->ml()->get()->toArray();

            return $logData;
        }

        return [];
    }
}
