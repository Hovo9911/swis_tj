<?php

namespace App\Models\Broker;

use App\Http\Controllers\Core\DataTableSearch;
use Illuminate\Support\Facades\DB;

/**
 * Class BrokerSearch
 * @package App\Models\Broker
 */
class BrokerSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        return $query->get();
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($this->orderByCol, $this->orderByType ?? 'desc');
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $query = Broker::select('broker_ml.broker_id', 'broker_ml.lng_id', 'broker_ml.name', 'broker_ml.description', 'broker.*')->joinMl()->notDeleted();

        if (isset($this->searchData['id'])) {
            $query->where(DB::raw("broker.id::varchar"), $this->searchData['id']);
        }

        if (isset($this->searchData['tax_id'])) {
            $query->where('tax_id', 'ILIKE', '%' . strtolower(trim($this->searchData['tax_id'])) . '%');
        }

        if (isset($this->searchData['legal_entity_name'])) {
            $query->where('legal_entity_name', 'ILIKE', '%' . strtolower(trim($this->searchData['legal_entity_name'])) . '%');
        }

        if (isset($this->searchData['name'])) {
            $query->where('name', 'ILIKE', '%' . strtolower(trim($this->searchData['name'])) . '%');
        }

        if (isset($this->searchData['certification_number'])) {
            $query->where('broker.certification_number', 'ILIKE', '%' . strtolower(trim($this->searchData['certification_number'])) . '%');
        }

        if (isset($this->searchData['address'])) {
            $query->where('broker.address', 'ILIKE', '%' . strtolower(trim($this->searchData['address'])) . '%');
        }

        if (isset($this->searchData['show_status'])) {
            $query->where('broker.show_status', $this->searchData['show_status']);
        }

        if (isset($this->searchData['certification_period_validity_date_start'])) {
            $query->where('certification_period_validity', '>=', $this->searchData['certification_period_validity_date_start']);
        }

        if (isset($this->searchData['certification_period_validity_date_end'])) {
            $query->where('certification_period_validity', '<=', $this->searchData['certification_period_validity_date_end']);
        }

        $query->checkUserHasRole();

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}
