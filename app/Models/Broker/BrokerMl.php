<?php

namespace App\Models\Broker;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

/**
 * Class BrokerMl
 * @package App\Models\Broker
 */
class BrokerMl extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['broker_id', 'lng_id'];

    /**
     * @var string
     */
    public $incrementing = 'false';

    /**
     * @var string
     */
    public $table = 'broker_ml';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'broker_id',
        'lng_id',
        'name',
        'description',
        'show_status'
    ];
}
