<?php

namespace App\Models\DataModelConstants;

use App\Models\BaseModel;

/**
 * Class DataModel
 * @package App\Models\DataModelConstants
 */
class DataModelConstants extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'data_model_constants';

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'constant',
        'mdm_id'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}