<?php

namespace App\Models\Notifications;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

/**
 * Class AgencyMl
 * @package App\Models\Agency
 */
class NotificationsMl extends BaseModel
{

    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['notification_id', 'lng_id'];

    /**
     * @var string
     */
    public $incrementing = 'false';

    /**
     * @var string
     */
    public $table = 'notifications_ml';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'notification_id',
        'lng_id',
        'in_app_notification',
        'show_status'
    ];

    /**
     * Function to change |SITE_URL| to current url of site
     *
     * @param $value
     * @return string
     */
    public function getInAppNotificationAttribute($value)
    {
        return str_replace(Notifications::IN_APP_URL_PREFIX, urlWithLng('/'), $value);
    }

}
