<?php

namespace App\Models\Notifications;

use App\Http\Controllers\Core\DataTableSearch;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class NotificationsSearch
 * @package App\Models\Document
 */
class NotificationsSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        $result = $query->get();

        foreach ($result as &$value) {
            $value->in_app_notification = optional($value->current())->in_app_notification;
        }

        return $result;
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($this->orderByCol, $this->orderByType ?? 'desc');
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $query = Notifications::select(['id', 'created_at', 'notifications_ml.in_app_notification', 'read'])
            ->joinMl(['f_k' => 'notification_id'])
            ->where([
                'user_id' => Auth::id(),
                'company_tax_id' => Auth::user()->companyTaxId()
            ]);

        if (Auth::user()->isSwAdmin()) {
            $query->where('notification_type', Notifications::NOTIFICATION_TYPE_SW_ADMIN);
        } else {
            if (Auth::user()->isLocalAdmin()) {
                $query->where('notification_type', '!=', Notifications::NOTIFICATION_TYPE_PERSON_IN_NEXT_STATE);
            } else {
                $query->where('notification_type', '!=', Notifications::NOTIFICATION_TYPE_AGENCY_ADMIN);
            }

            $query->where('notification_type', '!=', Notifications::NOTIFICATION_TYPE_SW_ADMIN);
        }

        $query->orderBy('created_at', 'DESC')->active();

        if (isset($this->searchData['id'])) {
            $query->where(DB::raw("notifications.id::varchar"), $this->searchData['id']);
        }

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }

}
