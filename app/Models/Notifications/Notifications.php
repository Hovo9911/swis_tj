<?php

namespace App\Models\Notifications;

use App\Http\Controllers\Core\Dictionary\DictionaryManager;
use App\Models\Agency\Agency;
use App\Models\BaseModel;
use App\Models\Company\Company;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\Languages\Language;
use App\Models\Menu\Menu;
use App\Models\ReferenceTable\ReferenceTable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Auth;

/**
 * Class Notifications
 * @package App\Models\Notifications
 */
class Notifications extends BaseModel
{
    /**
     * @var string
     */
    const NOTIFICATION_TYPE_APPLICANT = 'applicant';
    const NOTIFICATION_TYPE_IMPORTER_EXPORTER = 'importer_exporter';
    const NOTIFICATION_TYPE_AGENCY_ADMIN = 'agency_admin';
    const NOTIFICATION_TYPE_PERSON_IN_NEXT_STATE = 'persons_in_next_state';
    const NOTIFICATION_TYPE_PERSON_IN_NEXT_STATE_SUPERSCRIBE = 'persons_in_next_state_superscribe';
    const NOTIFICATION_TYPE_CUSTOM_TEMPLATE = 'custom_template';
    const NOTIFICATION_TYPE_GLOBAL_TEMPLATE = 'global_template';
    const NOTIFICATION_TYPE_GLOBAL_BROKER_TEMPLATE = 'global_broker_template';
    const NOTIFICATION_TYPE_SW_ADMIN = 'sw_admin';

    /**
     * @var string
     */
    const IN_APP_TRANS_KEYS = 'in_app_trans_keys';
    const EMAIL_TRANS_KEYS = 'email_trans_keys';
    const SMS_TRANS_KEYS = 'sms_trans_keys';

    /**
     * @var int
     */
    const NOTIFICATION_STATUS_READ = 1;
    const NOTIFICATION_STATUS_UNREAD = 0;

    /**
     * @var string (In Notification message change Const SITE_URL to current site url)
     */
    const IN_APP_URL_PREFIX = '|SITE_URL|';

    /**
     * @var string
     */
    const CURRENT_STATE = '{{CURRENT_STATE}}';
    const AGENCY_NAME = '{{AGENCY_NAME}}';
    const AUTHORIZED_COMPANY = '{{AUTHORIZED_COMPANY}}';
    const AUTHORIZED_SYSTEMS = '{{AUTHORIZED_SYSTEMS}}';
    const REFERENCE_TABLE = '{{REFERENCE_TABLE}}';
    const REFERENCE_TABLE_FORM_CHANGED_ID = '{{REFERENCE_TABLE_FORM_CHANGED_ID}}';
    const USER_CHANGED_INFO = '{{USER_CHANGED_INFO}}';

    /**
     * @var string
     */
    public $table = 'notifications';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'notification_id',
        'user_id',
        'company_tax_id',
        'sub_application_id',
        'email_notification',
        'in_app_notification',
        'sms_notification',
        'read',
        'notification_type',
        'is_broker',
        'show_status'
    ];

    /**
     * Function to get created_at value
     *
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format(config('swis.date_time_format_front'));
    }

    /**
     * Function to change |SITE_URL| to current url of site
     *
     * @param $value
     * @return string
     */
    public function getInAppNotificationAttribute($value)
    {
        return str_replace(Notifications::IN_APP_URL_PREFIX, urlWithLng('/'), $value);
    }

    /**
     * Function to return ml
     *
     * @return HasMany
     */
    public function ml()
    {
        return $this->hasMany(NotificationsMl::class, 'notification_id', 'id');
    }

    /**
     * Function to return current ml
     *
     * @return HasOne
     */
    public function current()
    {
        return $this->hasOne(NotificationsMl::class, 'notification_id', 'id')->notDeleted()->where('lng_id', cLng('id'))->first();
    }

    /**
     * Function to get notifications by read status
     *
     * @param $query
     * @param $readStatus
     * @return Builder
     */
    public function scopeReadStatus($query, $readStatus)
    {
        return $query->where('read', $readStatus);
    }

    /**
     * Function to get new notifications
     *
     * @return string
     */
    public static function newestNotifications()
    {
        return (Auth::check()) ? (new self())->getNotifications()->orderByDesc()->active()->limit(3)->get() : '';
    }

    /**
     * Function to get unread notifications count in APP
     *
     * @return int
     */
    public static function unreadInAppNotificationsCount()
    {
        return (Auth::check()) ? (new self())->getNotifications(true)->readStatus(Notifications::NOTIFICATION_STATUS_UNREAD)->count() : 0;
    }

    /**
     * Function to replace notification variable to value
     *
     * @param array $replace
     * @param $string
     * @param $lngId
     * @return string|string[]
     */
    public static function replaceVariableToValue(array $replace, $string, $lngId)
    {
        // CURRENT_STATE
        if (array_key_exists(self::CURRENT_STATE, $replace)) {

            $replace[self::CURRENT_STATE] = ConstructorStates::where('id', $replace[self::CURRENT_STATE])
                ->leftJoin('constructor_states_ml', 'constructor_states.id', '=', 'constructor_states_ml.constructor_states_id')
                ->where('lng_id', $lngId)
                ->pluck('state_name')
                ->first();
        }

        // AGENCY_NAME
        if (array_key_exists(self::AGENCY_NAME, $replace)) {

            $agencyName = Agency::leftJoin('agency_ml', 'agency.id', '=', 'agency_ml.agency_id')
                ->where(['tax_id' => $replace[self::AGENCY_NAME], 'lng_id' => $lngId])
                ->pluck('agency_ml.name')
                ->first();

            if (is_null($agencyName)) {
                $agencyName = Company::where('tax_id', $replace[self::AGENCY_NAME])->pluck('name')->first();
            }

            $replace[self::AGENCY_NAME] = $agencyName ?? '';
        }

        // AUTHORIZED_COMPANY
        if (array_key_exists(self::AUTHORIZED_COMPANY, $replace)) {

            $company = Company::select('name')->where('tax_id', $replace[self::AUTHORIZED_COMPANY])->first();
            $replace[self::AUTHORIZED_COMPANY] = $company->name ?? '';
        }

        // AUTHORIZED_SYSTEMS
        if (array_key_exists(self::AUTHORIZED_SYSTEMS, $replace)) {

            if (is_array($replace[self::AUTHORIZED_SYSTEMS])) {
                $authorizedSystems = $replace[self::AUTHORIZED_SYSTEMS];
            } else {
                $authorizedSystems = explode(',', $replace[self::AUTHORIZED_SYSTEMS]);
            }

            $dictionaryManager = new DictionaryManager();

            $moduleNames = [];
            if (count($authorizedSystems)) {
                foreach ($authorizedSystems as $authorizedSystem) {
                    if ($authorizedSystem) {
                        $moduleNames[] = $dictionaryManager->getTransByLngCode((Menu::where('id', $authorizedSystem)->pluck('title')->first()), $lngId);
                    }
                }
            }

            $replace[self::AUTHORIZED_SYSTEMS] = implode(', ', $moduleNames);
        }

        // REFERENCE_TABLE
        if (array_key_exists(self::REFERENCE_TABLE, $replace)) {

            $referenceTableString = explode('_', $replace[self::REFERENCE_TABLE]);

            $referenceTable = ReferenceTable::where('id', $referenceTableString[0])
                ->leftJoin('reference_table_ml', 'reference_table.id', '=', 'reference_table_ml.reference_table_id')
                ->where('lng_id', $lngId)
                ->first();

            $referenceTableMlName = $referenceTable->name;

            if (isset($referenceTableString[1])) {

                $changedRefRowId = $replace[self::REFERENCE_TABLE_FORM_CHANGED_ID];

                $refTableChangedCode = '';
                if (array_key_exists(self::REFERENCE_TABLE_FORM_CHANGED_ID, $replace)) {
                    $dictionaryManager = new DictionaryManager();

                    $changedCodeTrans = $dictionaryManager->getTransByLngCode("swis.reference.notification.changed.row.code", $lngId);

                    $changedRefRow = getReferenceRows($referenceTable->table_name, $changedRefRowId);
                    $refTableChangedCode = ' - ' . $changedCodeTrans . $changedRefRow->code;
                }

                $replace[self::REFERENCE_TABLE] = "<a target='_blank' href='" . urlWithLng('reference-table-form/' . $referenceTableString[0] . '/' . $changedRefRowId . '/edit') . "' class='mark-as-read'>" . $referenceTableMlName . $refTableChangedCode . "</a>";
            } else {
                $replace[self::REFERENCE_TABLE] = "<a target='_blank' href='" . urlWithLng('reference-table/' . $referenceTableString[0] . '/edit') . "' class='mark-as-read'>" . $referenceTableMlName . "</a>";
            }
        }

        // USER_CHANGED_INFO
        if (array_key_exists(self::USER_CHANGED_INFO, $replace)) {
            $dictionaryManager = new DictionaryManager();
            $number = 1;
            $userInfo = '<br>';
            foreach ($replace[self::USER_CHANGED_INFO] as $key => $value) {

                if ($key == 'lng_id') {
                    $value = Language::where('id', $value)->pluck('name')->first();
                }

                if ($key == 'email_notification' || $key == 'sms_notification') {
                    $value = $dictionaryManager->getTransByLngCode("swis.user_info.status.{$value}", $lngId);
                }

                $userInfo .= ($number++) . ". " . $dictionaryManager->getTransByLngCode("swis.user_info.{$key}", $lngId) . " " . $value . "<br>";
            }

            $replace[self::USER_CHANGED_INFO] = $userInfo;
        }

        $replace = self::contentVariablesWithBold($replace);

        return str_replace(array_keys($replace), array_values($replace), $string);
    }

    /**
     * Function to add <strong> tag to values
     *
     * @param $contentVariables
     * @return array
     */
    public static function contentVariablesWithBold($contentVariables)
    {
        foreach ($contentVariables as $key => $value) {
            $contentVariables[$key] = '<strong>' . $value . '</strong>';
        }

        return $contentVariables;
    }

    /**
     * Function to return user notifications
     *
     * @param bool $onlyCount
     * @return Notifications
     */
    private function getNotifications($onlyCount = false)
    {
        $notifications = Notifications::select('id');

        if(!$onlyCount){
            $notifications->addSelect('notifications_ml.in_app_notification', 'read', 'created_at');
        }

        $notifications->joinMl(['f_k' => 'notification_id'])
            ->where([
                'user_id' => Auth::user()->id,
                'company_tax_id' => Auth::user()->companyTaxId()
            ]);

        if (Auth::user()->isSwAdmin()) {
            $notifications->where('notification_type', Notifications::NOTIFICATION_TYPE_SW_ADMIN);
        } else {
            if (Auth::user()->isLocalAdmin()) {
                $notifications->where('notification_type', '!=', Notifications::NOTIFICATION_TYPE_PERSON_IN_NEXT_STATE);
            } else {
                $notifications->where('notification_type', '!=', Notifications::NOTIFICATION_TYPE_AGENCY_ADMIN);
            }

            $notifications->where('notification_type', '!=', Notifications::NOTIFICATION_TYPE_SW_ADMIN);
        }

        return $notifications;
    }

    /**
     * Function to return dashboard notifications
     *
     * @return Notifications
     */
    public static function dashboardNotifications()
    {
        return (new self())->getNotifications()->orderByDesc('created_at')->active()->limit(10)->get();
    }
}
