<?php

namespace App\Models\Notifications;

use App\Facades\Mailer;
use App\Models\Company\Company;
use App\Models\ConstructorActionSuperscribes\ConstructorActionSuperscribes;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\NotificationTemplates\NotificationTemplates;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationNotes\SingleApplicationNotes;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use App\Models\SingleApplicationSubApplicationStates\SingleApplicationSubApplicationStates;
use App\Models\User\User;
use App\Models\UserRoles\UserRoles;
use App\Sms\SmsManager;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

/**
 * Class NotificationsManager
 * @package App\Models\Notifications
 */
class NotificationsManager
{
    /**
     * Function to update data
     *
     * @param array $data
     */
    public function update($data)
    {
        if (isset($data['notification_id'])) {
            Notifications::where([
                'id' => $data['notification_id'],
                'user_id' => Auth::user()->id,
                'company_tax_id' => Auth::user()->companyTaxId()
            ])->update(['read' => Notifications::NOTIFICATION_STATUS_READ]);
        }
    }

    /**
     * Function to mark as read all unread notifications
     */
    public function markAsReadAllUnreadNotifications()
    {
        Notifications::where([
            'read' => Notifications::NOTIFICATION_STATUS_UNREAD,
            'user_id' => Auth::user()->id,
            'company_tax_id' => Auth::user()->companyTaxId()
        ])->update(['read' => Notifications::NOTIFICATION_STATUS_READ]);
    }

    /**
     * Function to store in app notifications
     *
     * @param $notification
     * @param $notificationMl
     */
    public static function storeInAppNotifications($notification, $notificationMl)
    {
        if (config('swis.in_app_notifications')) {
            $storeNotification = new Notifications($notification);
            $storeNotification->save();

            self::storeInAppNotificationsMl($storeNotification, $notificationMl);
        }
    }

    /**
     * Function to store in app notifications
     *
     * @param $model
     * @param $mlsData
     */
    public static function storeInAppNotificationsMl($model, $mlsData)
    {
        if (count($mlsData)) {
            $ml = [];
            foreach ($mlsData as $lngId => $mlData) {
                $mlData['lng_id'] = $lngId;
                $mlData['show_status'] = Notifications::STATUS_ACTIVE;
                $ml[] = new NotificationsMl($mlData);
            }
            $model->ml()->saveMany($ml);
        }
    }

    /**
     * Function to send notification when user send message from saf to sub application
     *
     * @param $noteData
     * @param string $noteFrom
     * @param null $documentId
     */
    public static function storeSafSubApplicationMessages($noteData, $noteFrom = 'saf', $documentId = null)
    {
        $subApplicationId = $noteData['sub_application_id'];

        $subApplication = SingleApplicationSubApplications::select('document_number', 'agency_id', 'company_tax_id')->find($subApplicationId);
        $saf = SingleApplication::select('id', 'regular_number', 'data', 'user_id', 'applicant_value')->where('regular_number', $noteData['saf_number'])->first();

        if ($noteFrom == SingleApplicationNotes::NOTE_SEND_MODULE_SAF) {

            $notificationTemplatesAll = NotificationTemplates::mlByCode(NotificationTemplates::SEND_MESSAGE_FROM_SAF);

            if ($notificationTemplatesAll->count()) {

                $notificationTemplates = $notificationTemplatesAll->where('lng_id', cLng('id'))->first();

                $superscribe = ConstructorActionSuperscribes::where([
                    'saf_subapplication_id' => $subApplicationId,
                ])->orderByDesc()->active()->first();

                $supApplicationUserId = null;
                if (!is_null($superscribe)) {
                    $supApplicationUserId = $superscribe->to_user;
                } else {
                    $supApplicationCurrentState = SingleApplicationSubApplicationStates::where('saf_subapplication_id', $subApplicationId)->orderByDesc()->firstOrFail();

                    if (!is_null($supApplicationCurrentState)) {
                        if ($supApplicationCurrentState->state->state_type != ConstructorStates::REQUEST_STATE_TYPE) {
                            $supApplicationUserId = $supApplicationCurrentState->user_id;
                        } else {
                            $supApplicationPreviousState = SingleApplicationSubApplicationStates::select('id', 'user_id')->where('id', '<', $supApplicationCurrentState->id)->orderByDesc()->first();
                            if (!is_null($supApplicationPreviousState)) {
                                $supApplicationUserId = $supApplicationPreviousState->user_id;
                            }
                        }
                    }
                }

                if (!is_null($supApplicationUserId)) {

                    $inApplicationNotes = [];
                    $supApplicationUser = User::select('id', 'first_name', 'last_name', 'email', 'phone_number', 'email_notification', 'sms_notification')->where('id', $supApplicationUserId)->first();

                    if (!is_null($supApplicationUser)) {

                        $transKeys = [
                            "{{USER_FULLNAME}}" => $supApplicationUser->name(),
                            "{{SUB_APP_ID}}" => $subApplication->document_number,
                            "{{NOTES}}" => $noteData['note'],
                        ];

                        $item['notification_id'] = $notificationTemplates->id;
                        $item['user_id'] = $supApplicationUser->id;
                        $item['company_tax_id'] = $subApplication->company_tax_id;
                        $item['sub_application_id'] = $subApplicationId ?? 0;
                        $item['email_subject'] = strip_tags(Notifications::replaceVariableToValue($transKeys, $notificationTemplates->email_subject, $supApplicationUser->lng_id));
                        $item['email_notification'] = Notifications::replaceVariableToValue($transKeys, $notificationTemplates->email_notification, $supApplicationUser->lng_id);
                        $item['sms_notification'] = strip_tags(Notifications::replaceVariableToValue($transKeys, $notificationTemplates->sms_notification, $supApplicationUser->lng_id));

                        $item['read'] = Notifications::NOTIFICATION_STATUS_UNREAD;
                        $item['show_status'] = Notifications::STATUS_ACTIVE;
                        $item['notification_type'] = Notifications::NOTIFICATION_TYPE_CUSTOM_TEMPLATE;

                        foreach ($notificationTemplatesAll as $constructorNote) {
                            $inApplicationNotes[$constructorNote->lng_id] = [
                                'in_app_notification' => Notifications::replaceVariableToValue($transKeys, $constructorNote->in_app_notification, $constructorNote->lng_id)
                            ];
                        }

                        // Store in app notifications
                        self::storeInAppNotifications($item, $inApplicationNotes);

                        // Store email and sms
                        self::sendEmailAndSms($supApplicationUser, $item);
                    }
                }
            }

        } else {

            $notificationTemplatesAll = NotificationTemplates::mlByCode(NotificationTemplates::SEND_MESSAGE_FROM_SUB_APPLICATION);

            if ($notificationTemplatesAll->count()) {
                $notificationTemplates = $notificationTemplatesAll->where('lng_id', cLng('id'))->first();

                $notificationToSendUsers = [];

                $subApplicationSendUser = SingleApplicationSubApplicationStates::where('saf_subapplication_id', $subApplicationId)
                    ->orderByAsc()
                    ->pluck('user_id')
                    ->first();

                /* ---------------- Applicant ---------------- */

                // User
                $notificationToSendUsers[$saf->user_id . '_' . $saf->applicant_value] = [
                    'user_id' => $saf->user_id,
                    'company_tax_id' => $saf->applicant_value,
                ];

                // Company MAIN Heads
                $applicantCompanyHeads = User::getCompanyHeads($saf->applicant_value, true)->pluck('id')->toArray();

                foreach ($applicantCompanyHeads as $key => $user) {
                    $notificationToSendUsers[$user . '_' . $saf->applicant_value] = [
                        'user_id' => $user,
                        'company_tax_id' => $saf->applicant_value,
                    ];
                }

                // Sup Application state changed user
                if ($saf->user->id != $subApplicationSendUser) {
                    $notificationToSendUsers[$saf->user->id . '_' . $subApplication->user_company_tax_id] = [
                        'user_id' => $saf->user->id,
                        'company_tax_id' => $subApplication->user_company_tax_id,
                    ];
                }


                /* ---------------- Importer ---------------- */

                $safImportData = $saf->data['saf']['sides']['import'];
                $importer = SingleApplication::getSidesUser($safImportData);

                // User
                if ($importer['userId']) {
                    $notificationToSendUsers[$importer['userId']] = [
                        'user_id' => $importer['userId'],
                        'company_tax_id' => '0',
                    ];
                }

                // Company MAIN Heads
                if ($importer['companyTaxId']) {
                    $importerCompanyHeads = User::getCompanyHeads($importer['companyTaxId'], true)->pluck('id')->toArray();

                    foreach ($importerCompanyHeads as $key => $user) {
                        $notificationToSendUsers[$user . '_' . $importer['companyTaxId']] = [
                            'user_id' => $user,
                            'company_tax_id' => $importer['companyTaxId'],
                        ];
                    }
                }


                /* ---------------- Exporter ---------------- */

                $safExportData = $saf->data['saf']['sides']['export'];
                $exporter = SingleApplication::getSidesUser($safExportData);

                // User
                if ($exporter['userId']) {
                    $notificationToSendUsers[$exporter['userId']] = [
                        'user_id' => $exporter['userId'],
                        'company_tax_id' => '0',
                    ];
                }

                // Company MAIN Heads
                if ($exporter['companyTaxId']) {
                    $exporterCompanyHeads = User::getCompanyHeads($exporter['companyTaxId'], true)->pluck('id')->toArray();

                    foreach ($exporterCompanyHeads as $key => $user) {
                        $notificationToSendUsers[$user . '_' . $exporter['companyTaxId']] = [
                            'user_id' => $user,
                            'company_tax_id' => $exporter['companyTaxId'],
                        ];
                    }
                }


                /* ---------------- Agency Admin ---------------- */

//                $agency = Agency::select('tax_id')->where('id', $subApplication->agency_id)->first();
//
//                if (!is_null($agency)) {
//                    $agencyAdmins = User::getCompanyHeads($agency->tax_id, true)->pluck('id')->toArray();
//
//                    // Company MAIN Heads
//                    foreach ($agencyAdmins as $key => $user) {
//                        $notificationToSendUsers[$user . '_' . $agency->tax_id] = [
//                            'user_id' => $user,
//                            'company_tax_id' => $agency->tax_id,
//                        ];
//                    }
//                }

                $inApplicationNotes = [];
                if (count($notificationToSendUsers)) {

                    $urlInApp = Notifications::IN_APP_URL_PREFIX . '/user-documents/' . $documentId . '/application/' . $subApplicationId . '/edit';

                    $subApplicationURL = "<a target='_blank' href='" . $urlInApp . "' class='mark-as-read'>" . $subApplication->document_number . "</a>";

                    foreach ($notificationToSendUsers as $notificationToSendUser) {

                        $user = User::select('id', 'first_name', 'last_name', 'email', 'phone_number', 'email_notification', 'sms_notification')->where('id', $notificationToSendUser['user_id'])->first();

                        if (!is_null($user)) {

                            $transKeys = [
                                "{{USER_FULLNAME}}" => $user->name(),
                                "{{SUB_APP_ID}}" => $subApplication->document_number,
                                "{{NOTES}}" => $noteData['note'],
                                "{{EXPERT_FULLNAME}}" => Auth::user()->name(),
                            ];

                            $inAppTransKeys = [
                                "{{USER_FULLNAME}}" => $user->name(),
                                "{{SUB_APP_ID}}" => $subApplicationURL,
                                "{{NOTES}}" => $noteData['note'],
                                "{{EXPERT_FULLNAME}}" => Auth::user()->name(),
                            ];

                            $item['notification_id'] = $notificationTemplates->id;
                            $item['user_id'] = $user->id;
                            $item['company_tax_id'] = $notificationToSendUser['company_tax_id'] ?? 0;
                            $item['sub_application_id'] = $subApplicationId ?? 0;
                            $item['email_subject'] = strip_tags(Notifications::replaceVariableToValue($transKeys, $notificationTemplates->email_subject, $user->lng_id));
                            $item['email_notification'] = Notifications::replaceVariableToValue($transKeys, $notificationTemplates->email_notification, $user->lng_id);
                            $item['sms_notification'] = strip_tags(Notifications::replaceVariableToValue($transKeys, $notificationTemplates->sms_notification, $user->lng_id));

                            $item['read'] = Notifications::NOTIFICATION_STATUS_UNREAD;
                            $item['show_status'] = Notifications::STATUS_ACTIVE;
                            $item['notification_type'] = Notifications::NOTIFICATION_TYPE_CUSTOM_TEMPLATE;

                            foreach ($notificationTemplatesAll as $constructorNote) {
                                $inApplicationNotes[$constructorNote->lng_id] = [
                                    'in_app_notification' => Notifications::replaceVariableToValue($inAppTransKeys, $constructorNote->in_app_notification, $constructorNote->lng_id)
                                ];
                            }

                            // Store in app notifications
                            self::storeInAppNotifications($item, $inApplicationNotes);

                            // Store email and sms
                            self::sendEmailAndSms($user, $item);
                        }
                    }
                }
            }
        }
    }

    /**
     * Function to send notifications when sub application will send
     *
     * @param $users
     * @param $safNumber
     * @param $subApplicationNumber
     * @param $subApplicationId
     * @param $constructorDocumentId
     */
    public function sendSubApplicationTemplateNotification($users, $safNumber, $subApplicationNumber, $subApplicationId, $constructorDocumentId)
    {
        $notificationTemplatesAll = NotificationTemplates::mlByCode(NotificationTemplates::SUB_APPLICATION_NOTIFICATION_TEMPLATE_NAME);

        if ($notificationTemplatesAll->count()) {
            $notificationTemplates = $notificationTemplatesAll->where('lng_id', cLng('id'))->first();

            $url = url('/' . cLng('code') . '/user-documents/' . $constructorDocumentId . '/application/' . $subApplicationId . '/edit');
            $urlInApp = Notifications::IN_APP_URL_PREFIX . '/user-documents/' . $constructorDocumentId . '/application/' . $subApplicationId . '/edit';

            $safUrl = "<a target='_blank' href='" . $url . "' class='mark-as-read'>" . $safNumber . "</a>";
            $safUrlForNextStateUsers = "<a target='_blank' href='" . $url . "' class='mark-as-read'>" . $subApplicationNumber . "</a>";

            $safUrlInApp = "<a target='_blank' href='" . $urlInApp . "' class='mark-as-read'>" . $safNumber . "</a>";
            $safUrlForNextStateUsersInApp = "<a target='_blank' href='" . $urlInApp . "' class='mark-as-read'>" . $subApplicationNumber . "</a>";

            if ($notificationTemplates) {
                $constructorDocuments = ConstructorDocument::select('company_tax_id')->where('id', $constructorDocumentId)->first();

                foreach ($users as $user) {

                    $transKeys = [
                        "{{USER_FULLNAME}}" => $user->first_name . " " . $user->last_name,
                        "{{SAF_ID}}" => $safUrl,
                        "{{SUB_APP_ID}}" => $safUrlForNextStateUsers
                    ];

                    $inAppTransKeys = [
                        "{{USER_FULLNAME}}" => $user->first_name . " " . $user->last_name,
                        "{{SAF_ID}}" => $safUrlInApp,
                        "{{SUB_APP_ID}}" => $safUrlForNextStateUsersInApp
                    ];

                    $smsTransKeys = [
                        "{{USER_FULLNAME}}" => $user->first_name . " " . $user->last_name,
                        "{{SAF_ID}}" => $safNumber,
                        "{{SUB_APP_ID}}" => $subApplicationNumber
                    ];

                    $item['notification_id'] = $notificationTemplates->id;
                    $item['user_id'] = $user->id;
                    $item['company_tax_id'] = $constructorDocuments->company_tax_id;
                    $item['sub_application_id'] = $subApplicationId;
                    $item['email_subject'] = strip_tags(Notifications::replaceVariableToValue($smsTransKeys, $notificationTemplates->email_subject, $user->lng_id));
                    $item['email_notification'] = Notifications::replaceVariableToValue($transKeys, $notificationTemplates->email_notification, $user->lng_id);
                    $item['sms_notification'] = strip_tags(Notifications::replaceVariableToValue($smsTransKeys, $notificationTemplates->sms_notification, $user->lng_id));

                    $item['read'] = Notifications::NOTIFICATION_STATUS_UNREAD;
                    $item['show_status'] = Notifications::STATUS_ACTIVE;
                    $item['notification_type'] = Notifications::NOTIFICATION_TYPE_CUSTOM_TEMPLATE;

                    //
                    $inApplicationNotes = [];
                    foreach ($notificationTemplatesAll as $constructorNote) {
                        $inApplicationNotes[$constructorNote->lng_id] = [
                            'in_app_notification' => Notifications::replaceVariableToValue($inAppTransKeys, $constructorNote->in_app_notification, $constructorNote->lng_id)
                        ];
                    }

                    // Store in app notifications
                    self::storeInAppNotifications($item, $inApplicationNotes);

                    // Send email and Sms (store DB too)
                    self::sendEmailAndSms($user, $item);
                }
            }
        }
    }

    /**
     * Function to send notifications when authorize users from profile settings page
     *
     * @param $authorizedData
     */
    public function userAuthorizationNotification($authorizedData)
    {
        $company = Company::select('id', 'tax_id', 'name')->where('tax_id', $authorizedData['company_tax_id'])->first();

        if (!is_null($company)) {
            $companyMainHeadUserID = UserRoles::where('company_tax_id', $authorizedData['company_tax_id'])->whereNull('authorized_by_id')->pluck('user_id')->first();

            // Agency Main Head
            $companyMainHead = User::where('id', $companyMainHeadUserID)->first();

            // Authorized user
            $authorizedUser = User::where('id', $authorizedData['authorized_user'])->first();

            //
            $constructorNotificationsAll = NotificationTemplates::mlByCode(NotificationTemplates::NEW_AUTHORIZATION_FOR_EMPLOYEE_FROM_AP);
            if ($constructorNotificationsAll->count()) {
                $constructorNotification = $constructorNotificationsAll->where('lng_id', cLng('id'))->first();

                if ($constructorNotification && $companyMainHead) {

                    $transKeys = [
                        "{{USER_FULLNAME}}" => $companyMainHead->name(),
                        "{{AUTHORIZED_USER}}" => $authorizedUser->name(),
                        "{{AUTHORIZED_COMPANY}}" => $company->tax_id ?? '',
                        "{{EXPERT_FULLNAME}}" => Auth::user()->name(),
                        "{{AUTHORIZED_SYSTEMS}}" => $authorizedData['modules']
                    ];

                    $item['notification_id'] = $constructorNotification->id;
                    $item['user_id'] = $companyMainHead->id;
                    $item['company_tax_id'] = $authorizedData['company_tax_id'];
                    $item['sub_application_id'] = 0;
                    $item['email_subject'] = strip_tags(Notifications::replaceVariableToValue($transKeys, $constructorNotification->email_subject, $companyMainHead->lng_id));
                    $item['email_notification'] = Notifications::replaceVariableToValue($transKeys, $constructorNotification->email_notification, $companyMainHead->lng_id);
                    $item['sms_notification'] = strip_tags(Notifications::replaceVariableToValue($transKeys, $constructorNotification->sms_notification, $companyMainHead->lng_id));

                    $item['read'] = 0;
                    $item['show_status'] = Notifications::STATUS_ACTIVE;
                    $item['notification_type'] = Notifications::NOTIFICATION_TYPE_CUSTOM_TEMPLATE;

                    // in Application
                    $inApplicationNotes = [];
                    foreach ($constructorNotificationsAll as $constructorNote) {
                        $inApplicationNotes[$constructorNote->lng_id] = [
                            'in_app_notification' => Notifications::replaceVariableToValue($transKeys, $constructorNote->in_app_notification, $constructorNote->lng_id)
                        ];
                    }

                    // Store in app notifications
                    self::storeInAppNotifications($item, $inApplicationNotes);

                    // Send email and Sms (store DB too)
                    self::sendEmailAndSms($companyMainHead, $item);
                }
            }
        }
    }

    /**
     * Function to send notifications when authorize users from Authorized person module
     *
     * @param $authorizedData
     */
    public function userAuthorizationFromAuthorizeModuleNotification($authorizedData)
    {
        $company = Company::select('id', 'tax_id', 'name')->where('tax_id', $authorizedData['company_tax_id'])->first();

        if (!is_null($company)) {

            // Authorized user
            $authorizedUser = User::where('id', $authorizedData['authorized_user'])->first();

            //
            $constructorNotificationsAll = NotificationTemplates::mlByCode(NotificationTemplates::NEW_AUTHORIZATION_FOR_EMPLOYEE_FROM_AP);

            if ($constructorNotificationsAll->count()) {
                $constructorNotification = $constructorNotificationsAll->where('lng_id', cLng('id'))->first();

                if ($constructorNotification) {

                    $transKeys = [
                        "{{USER_FULLNAME}}" => $authorizedUser->name(),
                        "{{AUTHORIZED_COMPANY}}" => $company->tax_id ?? '',
                        "{{EXPERT_FULLNAME}}" => Auth::user()->name(),
                        "{{AUTHORIZED_SYSTEMS}}" => $authorizedData['modules']
                    ];

                    $item['notification_id'] = $constructorNotification->id;
                    $item['user_id'] = $authorizedUser->id;
                    $item['company_tax_id'] = $authorizedData['company_tax_id'];
                    $item['sub_application_id'] = 0;
                    $item['email_subject'] = strip_tags(Notifications::replaceVariableToValue($transKeys, $constructorNotification->email_subject, $authorizedUser->lng_id));
                    $item['email_notification'] = Notifications::replaceVariableToValue($transKeys, $constructorNotification->email_notification, $authorizedUser->lng_id);
                    $item['sms_notification'] = strip_tags(Notifications::replaceVariableToValue($transKeys, $constructorNotification->sms_notification, $authorizedUser->lng_id));

                    $item['read'] = 0;
                    $item['show_status'] = Notifications::STATUS_ACTIVE;
                    $item['notification_type'] = Notifications::NOTIFICATION_TYPE_CUSTOM_TEMPLATE;

                    // in Application
                    $inApplicationNotes = [];
                    foreach ($constructorNotificationsAll as $constructorNote) {
                        $inApplicationNotes[$constructorNote->lng_id] = [
                            'in_app_notification' => Notifications::replaceVariableToValue($transKeys, $constructorNote->in_app_notification, $constructorNote->lng_id)
                        ];
                    }

                    // Store in app notifications
                    self::storeInAppNotifications($item, $inApplicationNotes);

                    // Send email and Sms (store DB too)
                    self::sendEmailAndSms($authorizedUser, $item);
                }
            }
        }
    }

    /**
     * Function to send notification about password change
     *
     * @param $user
     * @param $email
     */
    public function passwordChangeNotification($user, $email)
    {
        $today = now()->format(config('swis.date_time_format'));
        $ip = request()->ip();

        $mail = [
            'subject' => trans('swis.user.profile_settings.password.changed.subject'),
            'to' => $email,
            'body' => view('mailer.password-changed', compact('today', 'ip')),
        ];

        Mailer::send($mail);

        // Notify User
        $constructorNotificationsAll = NotificationTemplates::mlByCode(NotificationTemplates::PASSWORD_CHANGED_NOTIFICATION_TEMPLATE_NAME);
        if ($constructorNotificationsAll->count()) {

            $constructorNotifications = $constructorNotificationsAll->where('lng_id', cLng('id'))->first();

            if ($constructorNotifications) {

                $inAppTransKeys = [
                    "{{USER_FULLNAME}}" => $user->first_name . " " . $user->last_name,
                    "{{IP}}" => $ip,
                    "{{date}}" => $today
                ];

                foreach ($user->companies() as $key => $value) {

                    $item['notification_id'] = $constructorNotifications->id;
                    $item['user_id'] = $user->id;
                    $item['company_tax_id'] = $value->tax_id;
                    $item['sub_application_id'] = 0;
                    $item['email_notification'] = '';
                    $item['sms_notification'] = '';
                    $item['read'] = 0;
                    $item['show_status'] = Notifications::STATUS_ACTIVE;
                    $item['notification_type'] = Notifications::NOTIFICATION_TYPE_CUSTOM_TEMPLATE;

                    $inApplicationNotes = [];
                    foreach ($constructorNotificationsAll as $constructorNote) {
                        $inApplicationNotes[$constructorNote->lng_id] = [
                            'in_app_notification' => Notifications::replaceVariableToValue($inAppTransKeys, $constructorNote->in_app_notification, $constructorNote->lng_id)
                        ];
                    }

                    // Store in app notifications
                    self::storeInAppNotifications($item, $inApplicationNotes);
                }
            }
        }
    }

    /**
     * Function to send notification when user info changed
     *
     * @param $user
     * @param $userChangedInfo
     */
    public static function userInfoChangeNotification($user, $userChangedInfo)
    {
        $notificationTemplatesAll = NotificationTemplates::mlByCode(NotificationTemplates::USER_INFO_CHANGED);

        if ($notificationTemplatesAll->count() && $user) {

            $notificationTemplates = $notificationTemplatesAll->where('lng_id', cLng('id'))->first();
            $userProfileUrl = "<a target='_blank' href='" . urlWithLng('/profile-settings') . "' class='mark-as-read'>" . urlWithLng('/profile-settings') . "</a>";

            if (isset($userChangedInfo['is_email_verified'])) {
                unset($userChangedInfo['is_email_verified']);
            }

            $transKeys = [
                "{{USER_FULLNAME}}" => $user->name(),
                "{{USER_CHANGED_INFO}}" => $userChangedInfo,
                "{{USER_PROFILE_URL}}" => $userProfileUrl
            ];

            $item = [];

            $item['notification_id'] = $notificationTemplates->id;
            $item['user_id'] = $user->id;
            $item['company_tax_id'] = 0;
            $item['sub_application_id'] = 0;
            $item['email_subject'] = strip_tags(Notifications::replaceVariableToValue($transKeys, $notificationTemplates->email_subject, $user->lng_id));
            $item['email_notification'] = Notifications::replaceVariableToValue($transKeys, $notificationTemplates->email_notification, $user->lng_id);
            $item['sms_notification'] = strip_tags(Notifications::replaceVariableToValue($transKeys, $notificationTemplates->sms_notification, $user->lng_id));

            $item['read'] = Notifications::NOTIFICATION_STATUS_UNREAD;
            $item['show_status'] = Notifications::STATUS_ACTIVE;
            $item['notification_type'] = Notifications::NOTIFICATION_TYPE_CUSTOM_TEMPLATE;

            // Send email and Sms (store DB too)
            self::sendEmailAndSms($user, $item);
        }
    }

    /**
     * Function to send Email And Sms
     *
     * @param $user
     * @param $notification
     */
    public static function sendEmailAndSms($user, $notification)
    {

        if (isset($notification['email_notification']) && $user->email && $user->email_notification) {

            $dataForMail = [
                'subject' => $notification['email_subject'],
                'to' => $user->email,
                'body' => $notification['email_notification'],
            ];

            (new self)->storeEmail($dataForMail);
        }

        if (isset($notification['sms_notification']) && config('swis.sms_send') && $user->sms_notification && !is_null($user->phone_number)) {

            $dataForSms = [
                'user_id' => $user->id,
                'phone' => trim($user->phone_number),
                'message' => $notification['sms_notification'],
            ];

            (new self)->storeSms($dataForSms);
        }
    }

    /**
     * Function to send group users Email And Sms
     *
     * @param $notificationsData
     */
    public static function sendUsersGroupEmailAndSms($notificationsData)
    {
        $notificationsData = uniqueMultiDimensionArray($notificationsData, 'user_id');

        foreach ($notificationsData as $notificationData) {

            $userEmail = isset($notificationData['user_email']) ? $notificationData['user_email'] : null;
            $userPhoneNumber = isset($notificationData['user_phone_number']) ? trim($notificationData['user_phone_number']) : null;
            $emailBodyNotification = isset($notificationData['email_notification']) ? $notificationData['email_notification'] : null;
            $smsNotification = isset($notificationData['sms_notification']) ? $notificationData['sms_notification'] : null;

            if ($notificationData['user_email_status'] && !is_null($userEmail) && !is_null($emailBodyNotification)) {

                $dataForMail = [
                    'subject' => $notificationData['email_subject'],
                    'to' => $userEmail,
                    'body' => $emailBodyNotification,
                ];

                (new self)->storeEmail($dataForMail);
            }

            if (config('swis.sms_send') && $notificationData['user_sms_status'] && !is_null($smsNotification) && !is_null($userPhoneNumber)) {

                $dataForSms = [
                    'user_id' => $notificationData['user_id'],
                    'phone' => $userPhoneNumber,
                    'message' => $smsNotification,
                ];

                (new self)->storeSms($dataForSms);
            }
        }
    }

    /**
     * Function to store sms
     *
     * @param $dataForSms
     */
    private function storeSms($dataForSms)
    {
        if (App::environment('production')) {
            (new SmsManager())->storeSms($dataForSms);
        }
    }

    /**
     * Function to store email
     *
     * @param $dataForMail
     */
    private function storeEmail($dataForMail)
    {
        if (App::environment('production')) {
            Mailer::makeEmail($dataForMail);
        }
    }
}