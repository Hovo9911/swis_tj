<?php

namespace App\Models\ConstructorListToDataSetFromSaf;

use App\Models\BaseModel;
use App\Models\ConstructorLists\ConstructorLists;
use App\Models\DocumentsDatasetFromSaf\DocumentsDatasetFromSaf;
use App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure;

/**
 * Class ConstructorListToDataSetFromSaf
 * @package App\Models\ConstructorListToDataSetFromSaf
 */
class ConstructorListToDataSetFromSaf extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'constructor_list_to_data_set_from_saf';

    /**
     * @var bool
     */
    protected $hasAdminInfo = true;

    /**
     * @var array
     */
    protected $fillable = [
        'constructor_list_id',
        'saf_field_id'
    ];

    /**
     * Function to generate Saf Fields
     *
     * @param $type
     * @param bool $showInView
     * @return array
     */
    public static function generateSafFields($type, $showInView = false)
    {
        $safFields = [];
        $lastXML = SingleApplicationDataStructure::lastXml();
        $xmlTabs = $lastXML->xpath('/tabs/tab');
        $self = (new self);

        if ($xmlTabs && count($xmlTabs) > 0) {
            foreach ($xmlTabs as $tab) {
                if (isset($tab->block, $tab->block->fieldset)) {
                    if (count($tab->block) > 1) {
                        foreach ($tab->block as $block) {
                            if (isset($block->fieldset)) {
                                foreach ($block->fieldset as $filedSet) {
                                    $safFields = $self->returnSafFields($showInView, $tab, $block, $filedSet, $safFields, 'block');
                                }
                            }

                            if (isset($block->subBlock, $block->subBlock->fieldset)) {
                                foreach ($block->subBlock->fieldset as $filedSet) {
                                    $safFields = $self->returnSafFields($showInView, $tab, $block, $filedSet, $safFields, 'subBlock');
                                }
                            }
                        }
                    } else {
                        foreach ($tab->block->fieldset as $filedSet) {
                            $safFields = $self->returnSafFields($showInView, $tab, $tab->block, $filedSet, $safFields, 'tabBlock');
                        }
                    }

                } elseif (isset($tab->typeBlock)) {
                    foreach ($tab->typeBlock as $typeBlock) {
                        if (isset($typeBlock->block)) {
                            foreach ($typeBlock->block as $block) {
                                if (isset($block->fieldset)) {
                                    foreach ($block->fieldset as $filedSet) {
                                        $safFields = $self->returnSafFields($showInView, $tab, $block, $filedSet, $safFields, 'typeBlock', $typeBlock);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if ($showInView && isset($type) && $type == ConstructorLists::EDITABLE) {
            foreach ($safFields as $key => $item) {
                if ($key == 'SAF_ID_152' || $key == 'SAF_ID_146') {
                    unset($safFields[$key]);
                }
            }
        }

        return $safFields;
    }

    /**
     * Function to return  Saf Fields
     *
     * @param $showInView
     * @param $tab
     * @param $block
     * @param $filedSet
     * @param $safFields
     * @param $step
     * @param $typeBlock
     * @param bool $isField
     * @return array
     */
    private function returnSafFields($showInView, $tab, $block, $filedSet, $safFields, $step, $typeBlock = null, $isField = false)
    {
        if ($showInView) {
            if (isset($filedSet->attributes()->showInView) && $filedSet->attributes()->showInView) {
                $attr = $this->returnAttr($isField, $filedSet);
                $path = $this->returnStepPath($tab, $block, $step, $typeBlock);
                $safFields[(string)$attr->id] = self::setSafFieldsArray($attr, $filedSet, $path['tabPath'], $path['tabPathForSave'], $isField);
            } else {
                if (isset($filedSet->field) && count($filedSet->field) > 0) {
                    foreach ($filedSet->field as $field) {
                        $safFields = $this->returnSafFields($showInView, $tab, $block, $field, $safFields, 'block', null, true);
                    }
                }
            }
        } else {
            if (isset($filedSet->field) && count($filedSet->field) > 1) {
                foreach ($filedSet->field as $field) {
                    $safFields = $this->returnSafFields($showInView, $tab, $block, $field, $safFields, 'block', null, true);
                }
            } else {
                $attr = $this->returnAttr($isField, $filedSet);
                $path = $this->returnStepPath($tab, $block, $step, $typeBlock);
                $safFields[(string)$attr->id] = self::setSafFieldsArray($attr, $filedSet, $path['tabPath'], $path['tabPathForSave'], $isField);
            }
        }

        return $safFields;
    }

    /**
     * Function to attributes array
     *
     * @param $isField
     * @param $filedSet
     * @return array
     */
    private function returnAttr($isField, $filedSet)
    {
        if ($isField) {
            return $filedSet->attributes();
        } elseif (count($filedSet->field) == 1) {
            return $filedSet->field->attributes();
        } else {
            return $filedSet->field[0]->attributes();
        }
    }

    /**
     * Function to generate and return group path
     *
     * @param $tab
     * @param $block
     * @param $typeBlock
     * @param $step
     * @return array|void
     */
    private function returnStepPath($tab, $block, $step, $typeBlock)
    {
        switch ($step) {
            case 'block':
            case 'typeBlock':
                return [
                    'tabPath' => trans((string)$tab->attributes()->name) . ' / ' . trans((string)$block->attributes()->name),
                    'tabPathForSave' => (string)$tab->attributes()->name . ' / ' . (string)$block->attributes()->name
                ];

                break;
            case 'subBlock':
                return [
                    'tabPath' => trans((string)$tab->attributes()->name) . ' / ' . trans((string)$block->attributes()->name) . ' / ' . trans((string)$block->subBlock->attributes()->name),
                    'tabPathForSave' => (string)$tab->attributes()->name . ' / ' . (string)$block->attributes()->name . ' / ' . (string)$block->subBlock->attributes()->name
                ];

                break;
            case 'tabBlock':
                return [
                    'tabPath' => empty($tab->block->attributes()) ? trans((string)$tab->attributes()->name) : trans((string)$tab->attributes()->name) . ' / ' . trans((string)$tab->block->attributes()->name),
                    'tabPathForSave' => empty($tab->block->attributes()) ? (string)$tab->attributes()->name : (string)$tab->attributes()->name . ' / ' . (string)$tab->block->attributes()->name
                ];

                break;
        }
    }

    /**
     * Function to get Field from dataSet
     *
     * @param $documentID
     * @param $isHidden
     * @return DocumentsDatasetFromSaf
     */
    public static function getDataSetField($documentID, $isHidden)
    {
        return DocumentsDatasetFromSaf::select('id', 'saf_field_id')
            ->when($documentID, function ($query) use ($documentID) {
                $query->where('document_id', $documentID);
            })
            ->where('is_hidden', $isHidden)
            ->get();
    }

    /**
     * Function to remove Hidden Fields
     *
     * @param $safFields
     * @param $documentID
     * @return array
     */
    public static function removeHiddenFields($safFields, $documentID)
    {
        $hiddenFields = [];
        $getHiddenFields = self::getDataSetField($documentID, true);
        foreach ($getHiddenFields as $item) {
            $hiddenFields[] = $item->saf_field_id;
        }

        if ($safFields && count($safFields) > 0) {
            foreach ($safFields as $fieldID => $item) {
                if (in_array($fieldID, $hiddenFields)) {
                    unset($safFields[$fieldID]);
                }
            }
        }

        return $safFields;
    }

    /**
     * Function to set $SafField array value
     *
     * @param $attr
     * @param $filedSet
     * @param $tab
     * @param $tabPathForSave
     * @param $isField
     * @return array
     */
    public static function setSafFieldsArray($attr, $filedSet, $tab, $tabPathForSave, $isField = false)
    {
        if ($isField) {
            $desc = $filedSet->attributes()->tooltipText ? (string)$filedSet->attributes()->tooltipText : '';
        } else {
            $desc = $filedSet->field->attributes()->tooltipText ? (string)$filedSet->field->attributes()->tooltipText : '';
        }

        return [
            'name' => trans((string)$attr->title),
            'xml_field_name' => (string)$attr->name,
            'title_trans_key' => (string)$attr->title,
            'mdm_id' => (int)$attr->mdm,
            'description' => $desc,
            'tab' => $tab,
            'path' => $tabPathForSave,
        ];
    }
}
