<?php

namespace App\Models\ProductFieldCall;

use App\Models\BaseModel;

/**
 * Class ProductFieldCall
 * @package App\Models\ProductFieldCall
 */
class ProductFieldCall extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'product_field_call';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'sub_application_id',
        'field_id',
        'rule_condition',
        'profile_id'
    ];
}
