<?php

namespace App\Models\SingleApplicationSubApplicationProducts;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

/**
 * Class SingleApplicationSubApplicationProducts
 * @package App\Models\SingleApplicationSubApplicationProducts
 */
class SingleApplicationSubApplicationProducts extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var int
     */
    const CORRESPOND_YES = 1;
    const CORRESPOND_NO = 2;
    const CORRESPOND_NOT_ANSWERED = 3;

    /**
     * @var array
     */
    protected $primaryKey = ['subapplication_id', 'product_id'];

    /**
     * @var string
     */
    public $table = 'saf_sub_application_products';

    /**
     * @var array
     */
    protected $fillable = [
        'company_tax_id',
        'user_id',
        'subapplication_id',
        'product_id',
        'saf_number',
        'sample_quantity',
        'sample_measurement_unit',
        'sample_number',
        'correspond'
    ];
}
