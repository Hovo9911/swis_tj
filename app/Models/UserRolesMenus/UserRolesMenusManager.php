<?php

namespace App\Models\UserRolesMenus;

use App\Models\Menu\Menu;
use App\Models\User\User;
use App\Models\UserRoles\UserRoles;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class UserRolesMenusManager
 * @package App\Models\UserRolesMenus
 */
class UserRolesMenusManager
{
    /**
     * Function to update user role menus
     *
     * @param $type
     * @param $companyTaxId
     * @param array $userId
     * @param array $changedModules
     */
    public function updateUserRoleMenus($type, $companyTaxId = 0, $userId = [], $changedModules = [])
    {
        switch ($type) {
            case UserRolesMenus::ROLE_TYPE_USER_ROLE:
                $this->typeUserRole($userId, $companyTaxId, $changedModules);

                break;

            case UserRolesMenus::ROLE_TYPE_AGENCY:
            case UserRolesMenus::ROLE_TYPE_BROKER:
                $this->typeAgencyOrBroker($companyTaxId, $type, $userId);

                break;
            case UserRolesMenus::ROLE_TYPE_SW_ADMIN:
                $this->typeSwAdminRole($userId, $changedModules);

                break;
        }
    }

    /**
     * Function to update info with type User Roles
     *
     * @param $userId
     * @param $companyTaxId
     * @param $changedModules
     */
    private function typeUserRole($userId, $companyTaxId, $changedModules)
    {
        if (count($changedModules)) {

            $changedModules[] = Menu::AUTHORIZE_PERSON_MENU_ID;

            $changedModules = array_unique($changedModules);

            DB::transaction(function () use ($changedModules, $userId, $companyTaxId) {
                foreach ($changedModules as $menuId) {

                    if (in_array($menuId, Auth::user()->getUserCurrentRoleAvailableModules())) {

                        $checkMenuIdRoles = [
                            'user_id' => $userId,
                            'company_tax_id' => $companyTaxId,
                            'menu_id' => $menuId,
                            'type' => UserRolesMenus::ROLE_TYPE_USER_ROLE
                        ];

                        $storeMenuIdRoles = array_merge($checkMenuIdRoles, [
                            'need_update' => User::TRUE,
                        ]);

                        UserRolesMenus::updateOrCreate($checkMenuIdRoles, $storeMenuIdRoles);
                    }
                }
            });
        }
    }

    /**
     * Function to update info with type Agency or Broker
     *
     * @param $companyTaxId
     * @param $type
     * @param array $userIds
     */
    private function typeAgencyOrBroker($companyTaxId, $type, $userIds = [])
    {
        if (!count($userIds)) {
            $userIds = UserRoles::where('company_tax_id', $companyTaxId)
                ->activeRole()
                ->join('user_session', 'user_roles.user_id', '=', 'user_session.user_id')
                ->pluck('user_roles.user_id')->unique()->all();
        }

        if (count($userIds)) {

            DB::transaction(function () use ($userIds, $type, $companyTaxId) {

                foreach ($userIds as $userId) {

                    $checkMenuIdRoles = [
                        'user_id' => $userId,
                        'company_tax_id' => $companyTaxId,
                        'type' => $type,
                    ];

                    $storeMenuIdRoles = array_merge($checkMenuIdRoles, [
                        'need_update' => User::TRUE,
                    ]);

                    UserRolesMenus::updateOrCreate($checkMenuIdRoles, $storeMenuIdRoles);
                }
            });
        }
    }

    /**
     * Function to update info with type Sw admin Roles
     *
     * @param $userId
     * @param $changedModules
     */
    public function typeSwAdminRole($userId, $changedModules)
    {
        if (count($changedModules)) {

            $changedModules = array_unique($changedModules);

            DB::transaction(function () use ($changedModules, $userId) {
                foreach ($changedModules as $menuId) {

                    if (in_array($menuId, Auth::user()->getUserCurrentRoleAvailableModules())) {

                        $checkMenuIdRoles = [
                            'user_id' => $userId,
                            'company_tax_id' => '0',
                            'menu_id' => $menuId,
                            'type' => UserRolesMenus::ROLE_TYPE_SW_ADMIN
                        ];

                        $storeMenuIdRoles = array_merge($checkMenuIdRoles, [
                            'need_update' => User::TRUE,
                        ]);

                        UserRolesMenus::updateOrCreate($checkMenuIdRoles, $storeMenuIdRoles);
                    }
                }
            });
        }
    }

    /**
     * Function to delete user role menus
     *
     * @return void
     */
    public function setDefaultStateUserRoleMenus()
    {
        DB::transaction(function () {
            UserRolesMenus::where(['user_id' => Auth::id(), 'need_update' => User::TRUE])->update(['need_update' => User::FALSE]);
        });
    }

    /**
     * Function to remove user roles menus weekly
     *
     * @return void
     */
    public function removeUserRoleMenus()
    {
        UserRolesMenus::where('created_at', '<=', Carbon::now()->subDays(7))->where('need_update', User::FALSE)->delete();
    }
}