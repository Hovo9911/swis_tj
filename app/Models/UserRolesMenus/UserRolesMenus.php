<?php

namespace App\Models\UserRolesMenus;

use App\Models\BaseModel;

/**
 * Class UserRolesMenus
 * @package App\Models\UserRolesMenus
 */
class UserRolesMenus extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'user_role_menus';

    /**
     * @var string
     */
    const ROLE_TYPE_USER_ROLE = 'user_role';
    const ROLE_TYPE_AGENCY = 'agency';
    const ROLE_TYPE_BROKER = 'broker';
    const ROLE_TYPE_SW_ADMIN = 'sw_admin_roles';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'company_tax_id',
        'menu_id',
        'need_update',
        'type'
    ];

}