<?php

namespace App\Models\Application;

use App\Models\Languages\Language;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Application
 * @package App\Models\Application
 */
class Application extends Model
{
    /**
     * @var string
     */
    public $table = 'applications';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * @return BelongsToMany
     */
    public function languages()
    {
        return $this->belongsToMany(Language::class, 'application_language', 'application_id', 'lng_id');
    }

    /**
     * @return BelongsToMany
     */
    public function notDeletedLanguages()
    {
        return $this->languages()->where('application_language.show_status', '!=', 0);
    }
}
