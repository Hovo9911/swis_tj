<?php

namespace App\Models\Application;

use App\Models\Languages\Language;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class ApplicationLanguages
 * @package App\Models\Application
 */
class ApplicationLanguages extends Model
{
    /**
     * Constants
     */
    const IS_DEFAULT = '1';
    const IS_NOT_DEFAULT = '0';

    /**
     * @var string
     */
    protected $primaryKey = 'lng_id';

    /**
     * @var string
     */
    public $table = 'application_language';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'application_id',
        'lng_id',
        'is_default',
        'show_status'
    ];

    /**
     * @var array
     */
    protected $attribute = [
        'is_default' => '0'
    ];

    /**
     * Function to relate with language
     *
     * @return HasOne
     */
    public function language()
    {
        return $this->hasOne(Language::class, 'id', 'lng_id');
    }

    /**
     * Function to relate with application
     *
     * @return HasOne
     */
    public function application()
    {
        return $this->hasOne(Application::class, 'id', 'sub_application_id');
    }
}
