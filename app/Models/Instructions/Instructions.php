<?php

namespace App\Models\Instructions;

use App\Models\BaseModel;
use App\Models\InstructionsToFeedBacks\InstructionsToFeedBacks;
use App\Models\ReferenceTable\ReferenceTable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Instructions
 * @package App\Models\Instructions
 */
class Instructions extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'instructions';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'company_tax_id',
        'document_id',
        'type',
        'name',
        'code',
        'active_date_from',
        'active_date_to',
        'show_status',
        'type_code'
    ];

    /**
     * Function to get activate date value
     *
     * @param $value
     * @return string|void
     */
    public function getActiveDateFromAttribute($value)
    {
        if (isset($value)) {
            return Carbon::parse($value)->format(config('swis.date_format_front'));
        }
    }

    /**
     * Function to get active date to
     *
     * @param $value
     * @return string|void
     */
    public function getActiveDateToAttribute($value)
    {
        if (isset($value)) {
            return Carbon::parse($value)->format(config('swis.date_format_front'));
        }
    }

    /**
     * Function to return active languages
     *
     * @return HasMany
     */
    public function ml()
    {
        return $this->hasMany(InstructionsMl::class, 'instruction_id', 'id');
    }

    /**
     * Function to relate with InstructionsMl and get active
     *
     * @return HasOne
     */
    public function current()
    {
        return $this->hasOne(InstructionsMl::class, 'instruction_id', 'id')->notDeleted()->where('lng_id', cLng('id'))->first();
    }

    /**
     * Function to relate with ConstructorStatesMl and get active
     *
     * @return HasMany
     */
    public function feedbacks()
    {
        return $this->hasMany(InstructionsToFeedBacks::class, 'instructions_id', 'id');
    }

    /**
     * Function to get feedbacks
     *
     * @param bool $getAll
     * @return array
     */
    public static function getFeedback($getAll = false)
    {
        if ($getAll) {
            $feedback = getReferenceRows(ReferenceTable::REFERENCE_RISK_FEEDBACK, false, false, ['id', 'code', 'name'], 'get', false, false, [], true);
        } else {
            $feedback = getReferenceRows(ReferenceTable::REFERENCE_RISK_FEEDBACK);
        }

        $result = [];
        foreach ($feedback as $item) {
            if ($getAll) {
                $result[$item->id] = $item;
            } else {
                $result[$item->code] = $item;
            }
        }

        return $result;
    }
}
