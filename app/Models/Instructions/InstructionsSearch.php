<?php

namespace App\Models\Instructions;

use App\Http\Controllers\Core\DataTableSearch;
use Carbon\Carbon;

/**
 * Class InstructionsSearch
 * @package App\Models\InstructionsSearch
 */
class InstructionsSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);

        return $query->get();
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($this->orderByCol, $this->orderByType ?? 'desc');
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $query = Instructions::select('instructions.id', 'instructions.code', 'instructions_ml.name', 'constructor_documents_ml.document_name', 'instructions.show_status', 'instructions.active_date_from', 'instructions.active_date_to', 'instructions_ml.description')
            ->joinMl(['f_k' => 'instruction_id'])
            ->join('constructor_documents', 'constructor_documents.id', '=', 'instructions.document_id')
            ->join('constructor_documents_ml', function ($q) {
                $q->on('constructor_documents_ml.constructor_documents_id', '=', 'constructor_documents.id')->where('constructor_documents_ml.lng_id', '=', cLng('id'));
            })
            ->notDeleted();

        if (isset($this->searchData['document_id'])) {
            $query->where('instructions.document_id', $this->searchData['document_id']);
        }

        if (isset($this->searchData['name'])) {
            $query->where('instructions_ml.name', 'ILIKE', '%' . strtolower(trim($this->searchData['name'])) . '%');
        }

        if (isset($this->searchData['code'])) {
            $query->where('instructions.code', 'ILIKE', '%' . strtolower(trim($this->searchData['code'])) . '%');
        }

        if (isset($this->searchData['description'])) {
            $query->where('instructions_ml.description', 'ILIKE', '%' . strtolower(trim($this->searchData['description'])) . '%');
        }

        if (isset($this->searchData['active_date_from_start'])) {
            $query->where('instructions.active_date_from', '>=', Carbon::parse($this->searchData['active_date_from_start'])->startOfDay());
        }

        if (isset($this->searchData['active_date_from_end'])) {
            $query->where('instructions.active_date_from', '<=',Carbon::parse($this->searchData['active_date_from_end'])->endOfDay());
        }

        if (isset($this->searchData['active_date_to_start'])) {
            $query->where('instructions.active_date_to', '>=', Carbon::parse($this->searchData['active_date_to_start'])->startOfDay());
        }

        if (isset($this->searchData['active_date_to_end'])) {
            $query->where('instructions.active_date_to', '<=', Carbon::parse($this->searchData['active_date_to_end'])->endOfDay());
        }

        $query->checkUserHasRole();

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}
