<?php

namespace App\Models\Instructions;

use App\Models\BaseMlManager;
use App\Models\BaseModel;
use App\Models\InstructionsToFeedBacks\InstructionsToFeedBacks;
use App\Models\ReferenceTable\ReferenceTable;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class InstructionsManager
 * @package App\Models\InstructionsManager
 */
class InstructionsManager
{
    use BaseMlManager;

    /**
     * Function to Store data to DB
     *
     * @param array $data
     * @return Instructions
     */
    public function store(array $data)
    {
        $data['company_tax_id'] = Auth::user()->companyTaxId();
        $data['user_id'] = Auth::id();
        $data['type_code'] = optional(getReferenceRows(ReferenceTable::REFERENCE_RISK_INSTRUCTION_TYPE, $data['type']))->code;

        $instructions = new Instructions($data);
        $instructionsMl = new InstructionsMl();

        return DB::transaction(function () use ($instructions, $instructionsMl, $data) {
            // save document to DB
            $instructions->save();
            $this->storeMl($data['ml'], $instructions, $instructionsMl);

            $feedbacks = getReferenceRowsWhereIn(ReferenceTable::REFERENCE_RISK_FEEDBACK, $data['feedback'])->keyBy('id')->toArray();

            foreach ($data['feedback'] as $feedback) {
                InstructionsToFeedBacks::insert([
                    'instructions_id' => $instructions->id,
                    'feedback_id' => $feedback,
                    'code' => $feedbacks[$feedback]->code
                ]);
            }

            return $instructions;
        });
    }

    /**
     * Function to Update data
     *
     * @param array $data
     * @param $id
     */
    public function update(array $data, $id)
    {
        $instructions = Instructions::where('id', $id)->checkUserHasRole()->firstOrFail();
        $instructionsMl = new InstructionsMl();

        $data['company_tax_id'] = Auth::user()->companyTaxId();
        $data['user_id'] = Auth::id();
        $data['type_code'] = optional(getReferenceRows(ReferenceTable::REFERENCE_RISK_INSTRUCTION_TYPE, $data['type']))->code;
        $newInstructions = new Instructions($data);

        DB::transaction(function () use ($data, $instructions, $instructionsMl, $newInstructions) {
            // update document to DB
            $instructions->show_status = BaseModel::STATUS_INACTIVE;
            $instructions->active_date_to = Carbon::now()->format(config('swis.date_format'));
            $instructions->save();

            $instructionsMl->where('instruction_id', $instructions->id)->update(['show_status' => BaseModel::STATUS_INACTIVE]);

            if ($data['show_status'] != BaseModel::STATUS_INACTIVE) {
                $newInstructions->active_date_from = Carbon::now()->format(config('swis.date_format'));
                $newInstructions->active_date_to = null;
                $newInstructions->save();

                $this->storeMl($data['ml'], $newInstructions, $instructionsMl);

                $feedbacks = getReferenceRowsWhereIn(ReferenceTable::REFERENCE_RISK_FEEDBACK, $data['feedback'])->keyBy('id')->toArray();

                foreach ($data['feedback'] as $feedback) {
                    InstructionsToFeedBacks::insert([
                        'instructions_id' => $newInstructions->id,
                        'feedback_id' => $feedback,
                        'code' => $feedbacks[$feedback]->code
                    ]);
                }
            }
        });
    }

    /**
     * Function to inactivating  reference table rows
     */
    public function inactivateTableRows()
    {
        //If reference row active and start_date > today or end_date < today inactivate ref. row
        Instructions::where('show_status', BaseModel::STATUS_ACTIVE)
            ->where(function ($q) {
                $q->where('active_date_from', '>', currentDate())
                    ->orWhere('active_date_to', '<', currentDate());
            })
            ->update(['show_status' => BaseModel::STATUS_INACTIVE]);
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @param null $id2
     * @param null $code
     * @return array|void
     */
    public function getLogData($id, $id2 = null, $code = null)
    {
        if (!is_null($code)) {
            $data = Instructions::where('code', $code)->where('show_status', '1')->with(['ml', 'feedbacks'])->orderByDesc()->first();
        } else {
            $data = Instructions::where('id', $id)->with(['ml', 'feedbacks'])->first();
        }

        if (!is_null($data)) {
            $data = $data->toArray();

            return [$data, $data['code']];
        }
    }
}
