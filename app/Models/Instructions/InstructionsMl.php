<?php

namespace App\Models\Instructions;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

/**
 * Class InstructionsMl
 * @package App\Models\InstructionsMl
 */
class InstructionsMl extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['instruction_id', 'lng_id'];

    /**
     * @var string
     */
    public $incrementing = 'false';

    /**
     * @var string
     */
    public $table = 'instructions_ml';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'instruction_id',
        'lng_id',
        'description',
        'name'
    ];
}
