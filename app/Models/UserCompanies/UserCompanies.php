<?php

namespace App\Models\UserCompanies;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

/**
 * Class UserCompany
 * @package App\Models\UserCompany
 */
class UserCompanies extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['user_id', 'company_id'];

    /**
     * @var string
     */
    public $incrementing = 'false';

    /**
     * @var string
     */
    public $table = 'user_companies';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'company_id',
        'exec_position'
    ];

}
