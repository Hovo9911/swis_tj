<?php

namespace App\Models\SingleApplicationDocumentSubApplications;

use App\Models\BaseModel;
use App\Models\Document\Document;
use App\Models\Traits\HasCompositePrimaryKey;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class SingleApplicationDocumentProducts
 * @package App\Models\SingleApplicationDocumentProducts
 */
class SingleApplicationDocumentSubApplications extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['saf_number', 'sub_application_id'];

    /**
     * @var string
     */
    public $table = 'saf_document_sub_applications';

    /**
     * @var array
     */
    protected $fillable = [
        'company_tax_id',
        'user_id',
        'creator_user_id',
        'saf_document_id',
        'sub_application_id',
        'sub_application_agency_id',
        'sub_applicaiton_classificator_id',
        'saf_number',
    ];

    /**
     * Function to relate with Document
     *
     * @return HasOne
     */
    public function document()
    {
        return $this->hasOne(Document::class, 'id', 'saf_document_id');
    }


}