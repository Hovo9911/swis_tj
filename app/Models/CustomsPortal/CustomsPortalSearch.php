<?php

namespace App\Models\CustomsPortal;

use App\Http\Controllers\Core\DataTableSearch;
use App\Models\BaseModel;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplication\SingleApplication;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

/**
 * Class AgencySearch
 * @package App\Models\Agency
 */
class CustomsPortalSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        $data = $query->get();

        foreach ($data as &$item) {
            $safData = $item->data;
            if (isset($safData['saf']['sides'], $safData['saf']['sides']['applicant'])) {
                $item->applicant_value = '(' . $safData['saf']['sides']['applicant']['type_value'] . ') ' . $safData['saf']['sides']['applicant']['name'];
            }

            $item->importer_exporter = $item->company_tax_id;
            if ($item->regime == SingleApplication::REGIME_IMPORT) {
                $item->importer_exporter = $item->importer_value;
            } elseif ($item->regime == SingleApplication::REGIME_EXPORT) {
                $item->importer_exporter = $item->exporter_value;
            }

            //
            $item->approve_reject_number = '';

            $isApproved = ConstructorStates::checkEndStateStatus($item->state_id, ConstructorStates::END_STATE_APPROVED);
            if ($isApproved) {
                $item->approve_reject_number = $item->permission_number;
            }

            $isRejected = ConstructorStates::checkEndStateStatus($item->state_id, ConstructorStates::END_STATE_NOT_APPROVED);
            if ($isRejected) {
                $item->approve_reject_number = $item->rejected_number;
            }

            $item->saf_info_link = urlWithLng('saf-info/' . $item->regular_number . '?importer=' . $item->importer_value . '&&exporter=' . $item->exporter_value);
            $item->pdf_hash_key = urlWithLng('application/' . $item->sub_application_id . '/' . $item->pdf_hash_key);
        }

        return $data;
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $orderByType = 'desc';
        if ($this->orderByType == 'asc') {
            $orderByType = 'asc';
        }

        $query->orderBy($this->orderByCol ?? 'saf_sub_applications.created_at', $orderByType);
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $query = SingleApplication::select(
            'saf.regular_number',
            'saf.applicant_value',
            'saf.data',
            'saf.importer_value',
            'saf.exporter_value',
            'saf.company_tax_id',
            'saf.regime',
            'saf_sub_applications.document_number as sub_application_number',
            'saf_sub_applications.id as sub_application_id',
            'reference_classificator_of_documents.code as sub_application_type',
            'reference_classificator_of_documents_ml.name as sub_application_name',
            'saf_sub_application_states.created_at as sub_application_status_date',
            'saf_sub_application_states.state_id',
            'saf_sub_applications.pdf_hash_key',
            'constructor_states_ml.state_name as status',
            'saf_sub_applications.permission_number',
            'saf_sub_applications.rejected_number'
        )
            ->join('saf_sub_applications', 'saf.regular_number', '=', 'saf_sub_applications.saf_number')
            ->join('reference_classificator_of_documents', 'saf_sub_applications.reference_classificator_id', '=', 'reference_classificator_of_documents.id')
            ->join('reference_classificator_of_documents_ml', function ($query) {
                $query->on('reference_classificator_of_documents_ml.reference_classificator_of_documents_id', '=', 'reference_classificator_of_documents.id')->where("reference_classificator_of_documents_ml.lng_id", cLng('id'));
            })
            ->join('saf_sub_application_states', function ($q) {
                $q->on('saf_sub_applications.id', '=', 'saf_sub_application_states.saf_subapplication_id')->where('saf_sub_application_states.is_current', BaseModel::TRUE);
            })
            ->join('constructor_states', function ($q) {
                $q->on('constructor_states.id', '=', 'saf_sub_application_states.state_id');
//                ->where('state_type', ConstructorStates::END_STATE_TYPE)
            })
            ->join('constructor_states_ml', function ($q) {
                $q->on('constructor_states.id', '=', 'constructor_states_ml.constructor_states_id')->where('constructor_states_ml.lng_id', cLng('id'));
            });

        if (Auth::user()->ref_custom_body) {

            $refCustomBodies = getReferenceRowsWhereIn(ReferenceTable::REFERENCE_CUSTOM_BODY_REFERENCE, [], Auth::user()->ref_custom_body, ['id'], false);

            if ($refCustomBodies->count()) {
                $refCustomBodyIds = $refCustomBodies->pluck('id')->all();
                $query->where(function ($q) use ($refCustomBodyIds) {
                    $q->whereIn("saf.data->saf->transportation->departure", $refCustomBodyIds)->orWhereIn('saf.data->saf->transportation->appointment', $refCustomBodyIds);
                });
            }
        }

        if (!count($this->searchData)) {
            $query->where('saf.id', -1);
        }

        if (isset($this->searchData['agency'])) {
            $query->where('saf_sub_applications.agency_id', $this->searchData['agency']);
        }

        if (isset($this->searchData['subdivision'])) {
            $query->where('saf_sub_applications.agency_subdivision_id', $this->searchData['subdivision']);
        }

        if (isset($this->searchData['constructor_document'])) {
            $query->where('saf_sub_applications.document_code', $this->searchData['constructor_document']);
        }

        if (isset($this->searchData['regular_number'])) {
            $query->where('saf.regular_number', 'ILIKE', '%' . strtolower(trim($this->searchData['regular_number'])) . '%');
        }

        if (isset($this->searchData['importer_value'])) {
            $query->where('saf.importer_value', 'ILIKE', '%' . strtolower(trim($this->searchData['importer_value'])) . '%');
        }

        if (isset($this->searchData['approve_reject_number'])) {
            $query->where(function ($q) {
                $q->where('saf_sub_applications.permission_number', 'ILIKE', '%' . strtolower(trim($this->searchData['approve_reject_number'])) . '%')
                    ->orWhere('saf_sub_applications.rejected_number', 'ILIKE', '%' . strtolower(trim($this->searchData['approve_reject_number'])) . '%');
            });
        }

        if (isset($this->searchData['exporter_value'])) {
            $query->where('saf.exporter_value', 'ILIKE', '%' . strtolower(trim($this->searchData['exporter_value'])) . '%');
        }

        if (isset($this->searchData['applicant_value'])) {
            $query->where('saf.applicant_value', 'ILIKE', '%' . strtolower(trim($this->searchData['applicant_value'])) . '%');
        }

        if (isset($this->searchData['sub_application_received_at_date_start'])) {
            $query->where('saf_sub_application_states.created_at', '>=', Carbon::parse($this->searchData['sub_application_received_at_date_start'])->startOfDay());
        }

        if (isset($this->searchData['sub_application_received_at_date_end'])) {
            $query->where('saf_sub_application_states.created_at', '<=', Carbon::parse($this->searchData['sub_application_received_at_date_end'])->endOfDay());
        }

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}