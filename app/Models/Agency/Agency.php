<?php

namespace App\Models\Agency;

use App\Models\BaseModel;
use App\Models\Calendar\Calendar;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\RegionalOffice\RegionalOffice;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Auth;

/**
 * Class Agency
 * @package App\Models\Agency
 */
class Agency extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'agency';

    /**
     * @var string
     */
    const IMG_PATH = 'agency';

    /**
     * @var string
     */
    const SUBDIVISION_MARK_TYPE_REQUIRED = 'required';
    const SUBDIVISION_MARK_TYPE_NOT_REQUIRED = 'not_required';
    const SUBDIVISION_MARK_TYPE_OPTIONAL = 'optional';

    /**
     * @var array
     */
    const SUBDIVISION_MARK_ALL_TYPES = [self::SUBDIVISION_MARK_TYPE_REQUIRED, self::SUBDIVISION_MARK_TYPE_NOT_REQUIRED, self::SUBDIVISION_MARK_TYPE_OPTIONAL];

    /**
     * @var string
     */
    const SW_ADMIN_AGENCY_NAME = 'КОРХОНАИ ВОХИДИ ДАВЛАТИИ "МАРКАЗИ РАВЗАНАИ ЯГОНА" ДАР НАЗДИ ХАДАМОТИ ГУМРУКИ НАЗДИ ХУКУМАТИ ҶУМХУРИИ ТОҶИКИСТОН ИНН';
    const SW_ADMIN_AGENCY_TAX_ID = '020034727';
    const SAS_AGENCY_TAX_ID = '010009232'; //Государственный пробирный надзор РТ при Мин. фин Республики Таджикиста

    /**
     * @var string
     */
    const SW_ADMIN_AGENCY_SUBDIVISION = 'КОРХОНАИ ВОХИДИ ДАВЛАТИИ "МАРКАЗИ РАВЗАНАИ ЯГОНА" ДАР НАЗДИ ХАДАМОТИ ГУМРУКИ НАЗДИ ХУКУМАТИ ҶУМХУРИИ ТОҶИКИСТОН ИНН';

    /**
     * @var bool
     */
    protected $hasAdminInfo = true;

    /**
     * @var string
     */
    const TRANSPORT_MINISTRY_TAX_ID = '020011876';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'tax_id',
        'agency_code',
        'phone_number',
        'legal_entity_name',
        'address',
        'email',
        'website_url',
        'logo',
        'subdivision_mark_type',
        'allow_send_sub_applications_other_agency',
        'show_status',
        'is_lab',
        'certificate_number',
        'laboratory_code',
        'certification_period_validity',
        'working_hours_start',
        'working_hours_end',
        'lunch_hours_start',
        'lunch_hours_end'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'created_admin_id',
        'created_admin_ip',
        'updated_at',
        'updated_admin_id',
        'updated_admin_ip',
        'deleted_admin_id',
        'deleted_admin_ip'
    ];

    /**
     * Function to return ml
     *
     * @return HasMany
     */
    public function ml()
    {
        return $this->hasMany(AgencyMl::class, 'agency_id', 'id');
    }

    /**
     * Function to return current ml
     *
     * @return HasOne
     */
    public function current()
    {
        return $this->hasOne(AgencyMl::class, 'agency_id', 'id')->notDeleted()->where('lng_id', cLng('id'))->first();
    }

    /**
     * Function to return current ml
     *
     * @return HasOne
     */
    public function currentMl()
    {
        return $this->hasOne(AgencyMl::class, 'agency_id', 'id')->notDeleted()->where('lng_id', cLng('id'));
    }

    /**
     * Function to relate with indicators
     *
     * @return HasMany
     */
    public function indicators()
    {
        return $this->hasMany(AgencyLabIndicators::class, 'agency_id', 'id');
    }

    /**
     * Function to check Auth user is Agency Owner or not
     *
     * @param $query
     * @return Builder
     */
    public function scopeAgencyOwner($query)
    {
        if (!Auth::user()->isSwAdmin()) {
            return $query->where($this->getTable() . '.tax_id', Auth()->user()->companyTaxId());
        }

        return $query;
    }

    /**
     * Function to check Agency is Laboratory type
     *
     * @param $query
     * @return Builder
     */
    public function scopeIsLab($query)
    {
        return $query->where($this->getTable() . '.is_lab', Agency::TRUE);
    }

    /**
     * Function to check Agency is not Laboratory type
     *
     * @param $query
     * @return Builder
     */
    public function scopeNotLab($query)
    {
        return $query->where($this->getTable() . '.is_lab', Agency::FALSE);
    }

    /**
     * Function to when get name check isset short_name if isset get short_name
     *
     * @return string
     */
    public function shortName()
    {
        if (!is_null($this->short_name)) {
            return $this->short_name;
        }

        return $this->name ?? '';
    }

    /**
     * Function to get agencies by select
     *
     * @param array $select
     * @param bool $labData
     * @param bool $isOwner
     * @return Agency
     */
    public static function allData($select = [], $labData = false, $isOwner = false)
    {
        $select = !count($select) ? ['id', 'tax_id', 'name','short_name','is_lab'] : $select;

        $agencies = Agency::select($select)->joinMl();

        if($labData != 'all'){
            if ($labData) {
                $agencies->isLab();
            } else {
                $agencies->notLab();
            }
        }

        if ($isOwner) {
            $agencies->agencyOwner();
        }

        return $agencies->active()->orderByDesc('id')->get();
    }

    /**
     * Function to get Agency info for Rules
     *
     * @return string
     */
    public static function getAgencyInfo()
    {
        $activeLanguages = activeLanguagesIdCode();
        $agency = Agency::select(["id", "tax_id as ssn", "legal_entity_name as leg_name", "agency.address as leg_address", "phone_number", "email", "website_url as website", "name as name_ml", "agency_ml.address as address_ml", "description as description_ml"])
            ->joinMl()
            ->where("tax_id", Auth::user()->companyTaxId())
            ->active()
            ->with('ml')->first()->toArray();

        foreach ($agency['ml'] as $item) {
            $agency["name_ml_" . $activeLanguages[$item['lng_id']]] = $item['name'];
            $agency["description_ml_" . $activeLanguages[$item['lng_id']]] = $item['description'];
            $agency["address_ml_" . $activeLanguages[$item['lng_id']]] = $item['address'];
        }
        unset($agency['ml']);

        return json_encode($agency);
    }

    /**
     * Function to get agency hours difference between dates
     *
     * @param $startDate
     * @param $endDate
     * @param int $agencyId
     * @return bool
     */
    public static function getWorkingHoursBetweenDates($startDate, $endDate, $agencyId = 0)
    {
        $startDateStatus = Calendar::checkWorkingHour($startDate, $agencyId);
        $endDateStatus = Calendar::checkWorkingHour($endDate, $agencyId);
        $hours = 0;

        //If startDate,endDate non working dates
        if (is_null($startDateStatus) && is_null($endDateStatus)) {
            $agencyWorkingHours = Calendar::getCalendarDates($startDate, $endDate, $agencyId);
            $hours = getMinutesDifferenceBetweenDates($agencyWorkingHours);
        }

        //If startDate non working date and endDate working date
        if (is_null($startDateStatus) && !is_null($endDateStatus)) {

            $startDate = Calendar::nextDate($startDate, 'date_from', $agencyId);
            $currentEndDate = Calendar::currentDateTo($endDate, $agencyId);
            $agencyWorkingHours = Calendar::getCalendarDates($startDate, $currentEndDate, $agencyId);
            $hours = getMinutesDifferenceBetweenDates($agencyWorkingHours);

            $endDatesDiff = datesDifferenceInMinutes($endDate, $currentEndDate);

            $hours = $hours - $endDatesDiff;
        }

        // If startDate working date and endDate non working date
        if (!is_null($startDateStatus) && is_null($endDateStatus)) {

            $currentStartDate = Calendar::currentDateFrom($startDate, $agencyId);
            $endDate = Calendar::previousDateTo($endDate, $agencyId);
            $agencyWorkingHours = Calendar::getCalendarDates($currentStartDate, $endDate, $agencyId);
            $hours = getMinutesDifferenceBetweenDates($agencyWorkingHours);

            $startDatesDiff = datesDifferenceInMinutes($currentStartDate, $startDate);

            $hours = $hours - $startDatesDiff;
        }

        // If startDate,endDate working dates
        if (!is_null($startDateStatus) && !is_null($endDateStatus)) {

            $currentStartDate = Calendar::currentDateFrom($startDate, $agencyId);
            $currentEndDate = Calendar::currentDateTo($endDate, $agencyId);
            $agencyWorkingHours = Calendar::getCalendarDates($currentStartDate, $currentEndDate, $agencyId);
            $hours = getMinutesDifferenceBetweenDates($agencyWorkingHours);

            $startDatesDiff = datesDifferenceInMinutes($currentStartDate, $startDate);
            $endDatesDiff = datesDifferenceInMinutes($endDate, $currentEndDate);

            $hours = $hours - $startDatesDiff - $endDatesDiff;
        }

        return $hours;
    }

    /**
     * Function to get notification sending time
     *
     * @param $now
     * @param $dateTo
     * @param $controlPeriodMinutes
     * @param $agencyId
     * @return false|string
     */
    public static function getNotificationDate($now, $dateTo, $controlPeriodMinutes, $agencyId)
    {
        $checkWorkingHour = Calendar::checkWorkingHour($now, $agencyId);

        //when date not working hour
        if (is_null($checkWorkingHour)) {
            $nextDate = Calendar::getNextWorkingRange($now, $agencyId);
            $now = $nextDate['date_from'];
        }

        $minutesLeft = datesDifferenceInMinutes($now, $dateTo);

        if ($minutesLeft < $controlPeriodMinutes) {
            $controlPeriodMinutes = $controlPeriodMinutes - $minutesLeft;
            $nextDate = Calendar::getNextWorkingRange($dateTo, $agencyId);

            $nextDateFrom = $nextDate['date_from'];
            $nextDateTo = $nextDate['date_to'];

            return self::getNotificationDate($nextDateFrom, $nextDateTo, $controlPeriodMinutes, $agencyId);
        }

        return date("Y-m-d H:i:s", strtoTime("$dateTo - " . abs($controlPeriodMinutes - $minutesLeft) . " minutes"));
    }

    /**
     * Function to get subdivisions
     *
     * @param $taxId
     * @return array
     */
    public static function getSubdivisions($taxId)
    {
        $regionalOfficeCodes = RegionalOffice::where('company_tax_id', $taxId)->active()->pluck('office_code')->all();

        $subDivisions = [];
        if (count($regionalOfficeCodes)) {
            $refSubDivisions = getReferenceRowsWhereIn(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS, [], $regionalOfficeCodes);

            if ($refSubDivisions->count()) {
                foreach ($refSubDivisions as $item) {
                    $subDivisions[] = [
                        'id' => $item->id,
                        'name' => !empty($item->short_name) ? $item->short_name : $item->name
                    ];
                }
            }
        }

        return $subDivisions;
    }
}
