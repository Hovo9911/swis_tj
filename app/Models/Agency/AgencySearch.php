<?php

namespace App\Models\Agency;

use App\Http\Controllers\Core\DataTableSearch;
use Illuminate\Support\Facades\DB;

/**
 * Class AgencySearch
 * @package App\Models\Agency
 */
class AgencySearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        $result = $query->get();
        foreach ($result as $item) {
            $item->is_lab = ($item->is_lab) ? trans('swis.agency.is_laboratory.yes') : trans('swis.agency.is_laboratory.no');
        }

        return $result;
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($this->orderByCol, $this->orderByType ?? 'desc');
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $query = Agency::select('agency_ml.agency_id', 'agency_ml.lng_id', 'agency_ml.name', 'agency_ml.description', 'agency.*')->joinMl()->notDeleted();

        if (isset($this->searchData['id'])) {
            $query->where(DB::raw("agency.id::varchar"), $this->searchData['id']);
        }

        if (isset($this->searchData['tax_id'])) {
            $query->where('tax_id', 'ILIKE', '%' . strtolower(trim($this->searchData['tax_id'])) . '%');
        }

        if (isset($this->searchData['agency_code'])) {
            $query->where('agency_code', 'ILIKE', '%' . strtolower(trim($this->searchData['agency_code'])) . '%');
        }

        if (isset($this->searchData['name'])) {
            $query->where('agency_ml.name', 'ILIKE', '%' . strtolower(trim($this->searchData['name'])) . '%');
        }

        if (isset($this->searchData['address'])) {
            $query->where('agency.address', 'ILIKE', '%' . strtolower(trim($this->searchData['address'])) . '%');
        }

        if (isset($this->searchData['show_status'])) {
            $query->where('agency.show_status', $this->searchData['show_status']);
        }

        if (isset($this->searchData['is_laboratory'])) {
            $query->where('agency.is_lab', $this->searchData['is_laboratory']);
        }

        $query->checkUserHasRole();

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}