<?php

namespace App\Models\Agency;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

/**
 * Class AgencyMl
 * @package App\Models\Agency
 */
class AgencyMl extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['agency_id', 'lng_id'];

    /**
     * @var string
     */
    public $incrementing = 'false';

    /**
     * @var string
     */
    public $table = 'agency_ml';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'agency_id',
        'lng_id',
        'name',
        'short_name',
        'address',
        'description',
        'show_status'
    ];

}
