<?php


namespace App\Models\Agency;

use App\Models\BaseModel;

/**
 * Class AgencyLabIndicators
 * @package App\Models\Agency
 */
class AgencyLabIndicators extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'agency_lab_indicators';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'agency_id',
        'lab_hs_indicator_id',
        'price',
        'duration',
        'lab_hs_indicator_code'
    ];
}