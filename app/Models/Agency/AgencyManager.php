<?php

namespace App\Models\Agency;

use App\GUploader;
use App\Models\BaseMlManager;
use App\Models\BaseModel;
use App\Models\Calendar\CalendarManager;
use App\Models\LaboratoryIndicator\LaboratoryIndicator;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\ReferenceTableForm\ReferenceTableFormManager;
use App\Models\UserRolesMenus\UserRolesMenus;
use App\Models\UserRolesMenus\UserRolesMenusManager;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class AgencyManager
 * @package App\Models\Agency
 */
class AgencyManager
{
    use BaseMlManager;

    /**
     * Function to store validated data
     *
     * @param array $data
     * @return Agency
     */
    public function store(array $data)
    {
        $data['user_id'] = Auth::id();
        $data['is_lab'] = $data['is_lab'] ?? false;

        $agency = new Agency($data);
        $agencyMl = new AgencyMl();

        if (isset($data['logo'])) {
            $gUploader = new GUploader(Agency::IMG_PATH);
            $gUploader->storePerm($data['logo']);
        }

        // Synchronize Agency module with Reference Agency
        $referenceData = [
            'code' => $data['tax_id'],
            'show_status' => $data['show_status'],
            'start_date' => currentDate(),
        ];

        foreach ($data['ml'] as $lngId => $ml) {
            $referenceData['ml'][$lngId] = [
                'name' => $ml['name'],
                'short_name' => $ml['short_name'],
                'long_name' => $data['legal_entity_name']
            ];
        }

        if ($data['show_status'] == ReferenceTable::STATUS_INACTIVE) {
            $referenceData['end_date'] = currentDate();
        }

        return DB::transaction(function () use ($data, $agency, $agencyMl, $referenceData) {

            $agency->save();
            $this->storeMl($data['ml'], $agency, $agencyMl);

            // ---
            if (isset($data['indicators'])) {
                foreach ($data['indicators'] as $indicatorId => $indicator) {
                    AgencyLabIndicators::where('id', $indicatorId)->update([
                        'agency_id' => $agency->id,
                        'price' => $indicator['price'],
                        'duration' => $indicator['duration']
                    ]);
                }
            }

            // ---
            $referenceTableFormManager = new ReferenceTableFormManager();
            $referenceTableFormManager->store($referenceData, ReferenceTable::getReferenceIdByTableName(ReferenceTable::REFERENCE_AGENCY_TABLE_NAME));

            // ---
            $userRolesMenusManager = new UserRolesMenusManager();
            $userRolesMenusManager->updateUserRoleMenus(UserRolesMenus::ROLE_TYPE_AGENCY, $agency->tax_id);

            // Store or update agency working and lunch hours in calendar table
            CalendarManager::createOrUpdateAgencyCalendar($data, $agency->id);

            return $agency;
        });
    }

    /**
     * Function to Update data
     *
     * @param array $data
     * @param int $id
     */
    public function update(array $data, int $id)
    {
        $agency = Agency::where('id', $id)->checkUserHasRole()->firstOrFail();
        $agencyMl = new AgencyMl();

        //
        if (isset($data['logo'])) {

            if ($data['logo'] != $agency->logo) {
                $gUploader = new GUploader(Agency::IMG_PATH);
                $gUploader->storePerm($data['logo']);
            }

        } else {
            if($agency->logo){
                deleteFile(Storage::disk('image')->path(Agency::IMG_PATH . '/' . $agency->logo));
            }
        }

        // Synchronize Agency module with Reference Agency
        $currentReferenceRow = DB::table(ReferenceTable::REFERENCE_AGENCY)->select('id', 'show_status')->where(['code' => $agency->tax_id])->orderBy('id', 'DESC')->first();

        // Not Used For now
        $referenceData = [];
        if (!is_null($currentReferenceRow)) {

            $referenceData = [
                'start_date' => currentDate(),
                'show_status' => $data['show_status'],
            ];

            if ($data['show_status'] == ReferenceTable::STATUS_INACTIVE) {
                $referenceData['end_date'] = currentDate();
                $referenceData['f_id'] = $currentReferenceRow->id;
            } else {

                $referenceData['code'] = $agency->tax_id;
                foreach ($data['ml'] as $lngId => $ml) {
                    $referenceData['ml'][$lngId] = [
                        'name' => $ml['name'],
                        'short_name' => $ml['short_name'],
                        'long_name' => $agency->legal_entity_name
                    ];
                }
            }
        }

        DB::transaction(function () use ($id, $data, $agency, $agencyMl, $currentReferenceRow, $referenceData) {

            $data['allow_send_sub_applications_other_agency'] = $data['allow_send_sub_applications_other_agency'] ?? 0;
            $data['is_lab'] = $data['is_lab'] ?? false;

            $currentShowStatusAgency = $agency->show_status;
            $currentLabStatus = $agency->is_lab;

            $agency->update($data);
            $this->updateMl($data['ml'], 'agency_id', $agency, $agencyMl);

            // ---
            if (!is_null($currentReferenceRow)) {

                // in future change now need this type
                foreach ($data['ml'] as $lngId => $ml) {

                    DB::table(ReferenceTable::REFERENCE_AGENCY . '_ml')
                        ->where('lng_id', $lngId)
                        ->where('reference_agencies_reference_id', $currentReferenceRow->id)
                        ->update([
                            'name' => $ml['name'],
                            'short_name' => $ml['short_name'],
                            'long_name' => $agency->legal_entity_name
                        ]);
                }

//                 $referenceTableFormManager->update($referenceData, ReferenceTable::getReferenceIdByTableName(ReferenceTable::REFERENCE_AGENCY_TABLE_NAME),$currentReferenceRow->id);
            }

            // ---
            if ($data['show_status'] != $currentShowStatusAgency || $data['is_lab'] != $currentLabStatus) {
                $userRolesMenusManager = new UserRolesMenusManager();
                $userRolesMenusManager->updateUserRoleMenus(UserRolesMenus::ROLE_TYPE_AGENCY, $agency->tax_id);
            }

            // ---
            if (isset($data['indicators'])) {
                foreach ($data['indicators'] as $indicatorId => $indicator) {
                    AgencyLabIndicators::where('agency_id', $id)->where('id', $indicatorId)->update([
                        'price' => $indicator['price'],
                        'duration' => $indicator['duration']
                    ]);
                }
            }

            // Store or update agency working and lunch hours in calendar table
            CalendarManager::createOrUpdateAgencyCalendar($data, $id);
        });
    }

    /**
     * Function to get indicator search
     *
     * @param $data
     * @return Collection
     */
    public function getIndicatorsSearch($data)
    {
        $refTable = ReferenceTable::REFERENCE_LAB_HS_INDICATOR;
        $refSphereTable = ReferenceTable::REFERENCE_SPHERE;
        $refLabExpertiseIndicatorTable = ReferenceTable::REFERENCE_LAB_EXPERTISE_INDICATOR;
        $exists = ($data['agency']) ? AgencyLabIndicators::where('agency_id', $data['agency'])->pluck('lab_hs_indicator_id')->all() : [];

        return DB::table($refTable)
            ->select([
                "{$refTable}.id",
                "{$refSphereTable}_ml.name as sphere",
                DB::raw("CONCAT(reference_lab_hs_indicator.hs_start, ' - ', reference_lab_hs_indicator.hs_end) as hs_code"),
                "{$refLabExpertiseIndicatorTable}_ml.name as indicator",
                "{$refLabExpertiseIndicatorTable}.code as indicator_code",
            ])
            ->join("{$refTable}_ml", function ($q) use ($refTable) {
                $q->on("{$refTable}.id", "=", "{$refTable}_ml.{$refTable}_id")->where('lng_id', cLng('id'));
            })
            ->join("{$refSphereTable}", function ($q) use ($refTable, $refSphereTable) {
                $q->on("{$refSphereTable}.id", "=", "{$refTable}.sphere");
            })
            ->join("{$refSphereTable}_ml", function ($q) use ($refTable, $refSphereTable) {
                $q->on("{$refSphereTable}_ml.{$refSphereTable}_id", "=", "{$refSphereTable}.id")->where("{$refSphereTable}_ml.lng_id", cLng('id'));
            })
            ->join("{$refLabExpertiseIndicatorTable}", function ($q) use ($refTable, $refLabExpertiseIndicatorTable) {
                $q->on("{$refLabExpertiseIndicatorTable}.id", "=", "{$refTable}.lab_expertise_indicator")
                    ->where("{$refLabExpertiseIndicatorTable}.show_status", BaseModel::STATUS_ACTIVE);
            })
            ->join("{$refLabExpertiseIndicatorTable}_ml", function ($q) use ($refTable, $refLabExpertiseIndicatorTable) {
                $q->on("{$refLabExpertiseIndicatorTable}_ml.{$refLabExpertiseIndicatorTable}_id", "=", "{$refLabExpertiseIndicatorTable}.id")
                    ->where("{$refLabExpertiseIndicatorTable}_ml.lng_id", cLng('id'));
            })
            ->when($data['sphere'], function ($q) use ($data, $refSphereTable) {
                $sphereIds = DB::table($refSphereTable)->where("{$refSphereTable}.code", $data['sphere'])->pluck('id')->all();
                $q->whereIn('sphere', $sphereIds);
            })
            ->when($data['indicator'], function ($q) use ($data, $refLabExpertiseIndicatorTable) {
                $q->where("{$refLabExpertiseIndicatorTable}_ml.name", "ILIKE", "%{$data['indicator']}%")->orWhere("{$refLabExpertiseIndicatorTable}.code", $data['indicator']);
            })
            ->when($data['hs_code'], function ($q) use ($data) {
                $q->where('hs_start', '<=', $data['hs_code']);
                $q->where('hs_end', '>=', $data['hs_code']);
            })
            ->whereNotIn("{$refTable}.id", $exists)
            ->where("{$refTable}.show_status", BaseModel::STATUS_ACTIVE)
            ->get();
    }

    /**
     * Function to store in db Indicators
     *
     * @param $data
     * @return array
     */
    public function saveIndicators($data)
    {
        return DB::transaction(function () use ($data) {

            $returnData = [];
            if (isset($data['indicators'])) {
                $indicators = getReferenceRowsWhereIn(ReferenceTable::REFERENCE_LAB_HS_INDICATOR, $data['indicators'])->keyBy('id')->toArray();

                foreach ($data['indicators'] as $indicator) {
                    if (isset($indicators[$indicator])) {

                        $indicator = AgencyLabIndicators::create([
                            'agency_id' => $data['agency'],
                            'lab_hs_indicator_id' => $indicator,
                            'lab_hs_indicator_code' => $indicators[$indicator]->code,
                        ]);

                        $returnData[] = $indicator->id;
                    }
                }
            }

            return $returnData;
        });
    }

    /**
     * Function to return agency indicators
     *
     * @param $agencyId
     * @param array $insertedFields
     * @return AgencyLabIndicators
     */
    public function getIndicators($agencyId, $insertedFields = [])
    {
        $refTable = ReferenceTable::REFERENCE_LAB_HS_INDICATOR;
        $refSphereTable = ReferenceTable::REFERENCE_SPHERE;
        $refLabExpertiseIndicatorTable = ReferenceTable::REFERENCE_LAB_EXPERTISE_INDICATOR;

        return AgencyLabIndicators::select([
            "agency_lab_indicators.id",
            "{$refSphereTable}_ml.name as sphere",
            DB::raw("CONCAT(reference_lab_hs_indicator.hs_start, ' - ', reference_lab_hs_indicator.hs_end) as hs_code"),
            "{$refLabExpertiseIndicatorTable}_ml.name as indicator",
            "{$refLabExpertiseIndicatorTable}.code as indicator_code",
            "agency_lab_indicators.price",
            "agency_lab_indicators.duration",
        ])
            ->join("{$refTable}", function ($q) use ($refTable) {
                $q->on("{$refTable}.code", "=", "agency_lab_indicators.lab_hs_indicator_code")->where("{$refTable}.show_status", BaseModel::STATUS_ACTIVE)->where("{$refTable}.start_date", '<=', Carbon::now());
            })
            ->join("{$refTable}_ml", function ($q) use ($refTable) {
                $q->on("{$refTable}.id", "=", "{$refTable}_ml.{$refTable}_id")->where("{$refTable}_ml.lng_id", cLng('id'));
            })
            ->join("{$refSphereTable}_ml", function ($q) use ($refTable, $refSphereTable) {
                $q->on("{$refSphereTable}_ml.{$refSphereTable}_id", "=", "{$refTable}.sphere")->where("{$refSphereTable}_ml.lng_id", cLng('id'));
            })
            ->join($refLabExpertiseIndicatorTable, "{$refLabExpertiseIndicatorTable}.id", "=", "{$refTable}.lab_expertise_indicator")
            ->join("{$refLabExpertiseIndicatorTable}_ml", function ($q) use ($refLabExpertiseIndicatorTable) {
                $q->on("{$refLabExpertiseIndicatorTable}_ml.{$refLabExpertiseIndicatorTable}_id", "=", "{$refLabExpertiseIndicatorTable}.id")->where("{$refLabExpertiseIndicatorTable}_ml.lng_id", cLng('id'));
            })
            ->when($insertedFields, function ($q) use ($agencyId, $insertedFields) {
                $q->where('agency_id', $agencyId)->orWhereIn('agency_lab_indicators.id', $insertedFields);
            }, function ($q) use ($agencyId) {
                $q->where('agency_id', $agencyId);
            })
            ->get();
    }

    /**
     * Function to remove only indicators that have not price and duration
     *
     * @param $agencyId
     */
    public function deleteWrongIndicators($agencyId)
    {
        AgencyLabIndicators::select("agency_lab_indicators.id")
            ->where('agency_id', $agencyId)
            ->whereNull('price')
            ->whereNull('duration')
            ->delete();
    }

    /**
     * Function to remove indicator from db
     *
     * @param $data
     */
    public function deleteIndicator($data)
    {
        $agencyIndicator = AgencyLabIndicators::select('id', 'lab_hs_indicator_id')->where('id', $data['indicator'])->first();

        AgencyLabIndicators::where('id', $data['indicator'])->delete();
        LaboratoryIndicator::where('laboratory_id', $data['agencyId'])->where('indicator_id', $agencyIndicator->lab_hs_indicator_id)->delete();
    }

    /**
     * Function to delete image
     *
     * @param $path
     * @param $imageId
     */
    private function deleteImage($path, $imageId)
    {
        if ($imageId && file_exists($path . '/' . $imageId)) {
            unlink($path . '/' . $imageId);
        }
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @param null $parentId
     * @return array
     */
    public function getLogData($id = null, $parentId = null)
    {
        if (!is_null($id)) {
            $agency = Agency::where('id', $id)->checkUserHasRole('tax_id')->first();
            $logData = $agency;
            if ($agency) {
                $logData['mls'] = $agency->ml()->get()->toArray();
            }

            return $logData;
        }
        return [];
    }
}
