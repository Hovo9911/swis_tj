<?php

namespace App\Models\RiskProfileToInstructions;

use App\Models\BaseModel;
use App\Models\Instructions\Instructions;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class RiskProfileToInstructions
 * @package App\Models\RiskProfileToInstructions
 */
class RiskProfileToInstructions extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'risk_profile_to_instructions';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'risk_profile_id',
        'instruction_code'
    ];

    /**
     * Function to relate with Instructions
     *
     * @return HasMany
     */
    public function instruction()
    {
        return $this->hasMany(Instructions::class, 'id', 'instruction_id');
    }
}
