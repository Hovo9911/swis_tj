<?php

namespace App\Models\Folder;

use App\Http\Controllers\Core\DataTableSearch;
use App\Models\Document\Document;
use App\Models\FolderDocuments\FolderDocuments;
use Illuminate\Support\Facades\DB;

/**
 * Class FolderSearch
 * @package App\Models\Folder
 */
class FolderSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->rowsCountAll();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->rowsCountAll();
    }

    /**
     * @return int
     */
    private function rowsCountAll()
    {
        $query = $this->constructQuery();
        $result = DB::select("SELECT count(*) as cnt FROM ({$query->toSql()}) as temp", $query->getBindings());

        return $result[0]->cnt;
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        $result = $query->get();

        if (isset($this->searchData, $this->searchData['access_password']) && $result->count() > 0) {

            $folderAccess = Folder::select('id')->where('id', $result[0]->id)->folderOwnerOrCreator()->first();

            if (is_null($folderAccess)) {
                $result[0]->canEdit = false;
            }

            $result[0]->searched_by_access_password = true;
        }

        foreach ($result as $key => $folder) {

            $folderDocumentIds = FolderDocuments::where('folder_id', $folder->id)->pluck('document_id');

            $documentsCount = Document::whereIn('id', $folderDocumentIds)->active()->count();

            /* foreach($folder->documents as $document){

                 if (!($document->isDocumentOwnerOrCreator()) and $document->document_status == Document::DOCUMENT_STATUS_SUSPENDED) {
                     $folder->documents->forget($key);
                 }
             }*/

            $result[$key]['folder_documents_count'] = $documentsCount;
        }

        return $result;
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($this->orderByCol, $this->orderByType ?? 'desc');
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $query = Folder::select('folder.id', 'folder_access_password', 'folder_name', 'folder_description', 'folder.show_status', DB::raw('ROW_NUMBER() OVER() as "incrementId"'))
            ->leftJoin('folder_documents', 'folder.id', '=', 'folder_documents.folder_id')
            ->groupBy('folder.id')
            ->notDeleted();

        if (isset($this->searchData['id'])) {
            $query->where(DB::raw("folder.id::varchar"), $this->searchData['id']);
        }

        if (isset($this->searchData['description'])) {
            $query->where('folder_description', 'ILIKE', '%' . strtolower(trim($this->searchData['description'])) . '%');
        }

        if (isset($this->searchData['name'])) {
            $query->where('folder_name', 'ILIKE', '%' . strtolower(trim($this->searchData['name'])) . '%');
        }

        if (isset($this->searchData['access_password'])) {
            $query->where('folder_access_password', 'ILIKE', '%' . strtolower(trim($this->searchData['access_password'])) . '%');
        } else {
            $query->folderOwnerOrCreator();
        }

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}
