<?php

namespace App\Models\Folder;

use App\Models\BaseModel;
use App\Models\Document\Document;
use App\Models\ReferenceTable\ReferenceTable;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Auth;

/**
 * Class Folder
 * @package App\Models\Folder
 */
class Folder extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'folder';

    /**
     * @var bool
     */
    protected $hasAdminInfo = true;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'creator_user_id',
        'company_tax_id',
        'folder_access_password',
        'folder_name',
        'folder_description',
        'holder_type',
        'holder_type_value',
        'holder_passport',
        'holder_name',
        'holder_country',
        'holder_community',
        'holder_address',
        'holder_phone_number',
        'holder_email',
        'creator_type',
        'creator_type_value',
        'creator_name',
        'creator_country',
        'creator_community',
        'creator_address',
        'creator_phone_number',
        'creator_email',
        'notes',
        'show_status',
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'created_admin_id',
        'created_admin_ip',
        'updated_at',
        'updated_admin_id',
        'updated_admin_ip',
        'deleted_admin_id',
        'deleted_admin_ip'
    ];

    /**
     * Function to relate with Document
     *
     * @return BelongsToMany
     */
    public function documents()
    {
        return $this->belongsToMany(Document::class, 'folder_documents')->active()->withPivot(['folder_id', 'user_id']);
    }

    /**
     * Function to relate with inactive Document
     *
     * @return BelongsToMany
     */
    public function inactiveDocuments()
    {
        return $this->belongsToMany(Document::class, 'folder_documents')->inactive()->withPivot(['folder_id', 'user_id']);
    }

    /**
     * Function to check for Folder owner/creator
     *
     * @param $query
     * @return Builder
     */
    public function scopeFolderOwnerOrCreator($query)
    {
        $userType = Auth::user()->currentUserType();
        $userTypeValue = Auth::user()->currentUserTypeValue();
        $showOthersData = Auth::user()->userShowOthersData();

        $query->where(function ($q) use ($userType, $userTypeValue) {
            $q->where('folder.holder_type_value', $userTypeValue)->where('holder_type', $userType)
                ->orWhere('folder.creator_type_value', $userTypeValue)->where('creator_type', $userType);
        });

        // User login as company/agency
        if (Auth::user()->companyTaxId()) {

            if (!$showOthersData) {
                $query->where('folder.user_id', Auth::id());
            }
        }

        // User login as Authorized Natural Person
        if (Auth::user()->isAuthorizedNaturalPerson()) {

            if (!$showOthersData) {
                $query->where(['folder.creator_user_id' => Auth::id(), 'company_tax_id' => Folder::FALSE]);
            }
        }

        return $query;
    }

    /**
     * Function to check is Folder owner/creator
     *
     * @return bool
     */
    public function isFolderOwnerOrCreator()
    {
        $document = $this;

        $legalEntities = ReferenceTable::legalEntityValues();

        $holderType = array_search($document->holder_type, $legalEntities);
        $creatorType = array_search($document->creator_type, $legalEntities);

        $isHolder = false;
        $isCreator = false;

        $showOthersData = Auth::user()->userShowOthersData();

        // User login as company/agency
        if ((Auth::user()->companyTaxId()) && $holderType == BaseModel::ENTITY_TYPE_TAX_ID) {

            if ($document->holder_type_value == Auth::user()->companyTaxId()) {
                $isHolder = true;
            }

            if (!$showOthersData) {
                if ($document->user_id != Auth::id()) {
                    $isHolder = false;
                }
            }
        }

        // User login as Natural Person
        if (Auth::user()->isNaturalPerson() && $holderType == BaseModel::ENTITY_TYPE_USER_ID) {

            if ($document->holder_type_value == Auth::user()->ssn) {
                $isHolder = true;
            }
        }

        // ---------------------// --------------------- //

        // User login as company/agency
        if ((Auth::user()->isAgency() || Auth::user()->isCompany()) && $creatorType == BaseModel::ENTITY_TYPE_TAX_ID) {

            if ($document->creator_type_value == Auth::user()->companyTaxId()) {
                $isCreator = true;
            }

            if (!$showOthersData) {
                if ($document->user_id != Auth::id()) {
                    $isCreator = false;
                }
            }
        }

        // User login as Natural Person
        if (Auth::user()->isNaturalPerson() && $creatorType == BaseModel::ENTITY_TYPE_USER_ID) {

            if ($document->creator_type_value == Auth::user()->ssn) {
                $isCreator = true;
            }
        }

        if ($isCreator || $isHolder) {
            return true;
        }

        return false;
    }
}
