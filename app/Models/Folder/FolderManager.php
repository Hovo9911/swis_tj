<?php

namespace App\Models\Folder;

use App\EntityTypesInfo\LegalEntityForms;
use App\Exceptions\TaxServiceTjCompanyInactiveException;
use App\Exceptions\TaxServiceTjInnNotFoundException;
use App\Exceptions\TaxServiceTjInvalidResponseException;
use App\Models\FolderDocuments\FolderDocuments;
use App\Models\FolderNotes\FolderNotes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class FolderManager
 * @package App\Models\Folder
 */
class FolderManager
{
    /**
     * Function to Store data to DB
     *
     * @param array $data
     * @return Folder
     *
     * @throws TaxServiceTjCompanyInactiveException
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function store(array $data)
    {
        $data['user_id'] = Auth::user()->getCreatorUser()->id;
        $data['creator_user_id'] = Auth::id();
        $data['company_tax_id'] = Auth::user()->companyTaxId();
        $data['folder_access_password'] = generateID();

        // Holder Data if Company/Natural Person
        if (isset($data['holder_type'])) {
            $legalEntityForms = LegalEntityForms::getInstance();

            $holderData = $legalEntityForms->holderDataByType($data['holder_type'], $data['holder_type_value'], $data['holder_passport'] ?? null);

            $data = array_merge($data, $holderData);
        }

        // Creator
        $authUserCreator = Auth::user()->getCreator();

        $data['creator_type'] = $authUserCreator->type;
        $data['creator_type_value'] = $authUserCreator->ssn;
        $data['creator_name'] = $authUserCreator->name;
        $data['creator_country'] = $authUserCreator->country;
        $data['creator_community'] = $authUserCreator->community;
        $data['creator_address'] = $authUserCreator->address;
        $data['creator_phone_number'] = $authUserCreator->phone_number;
        $data['creator_email'] = $authUserCreator->email;
        $data['show_status'] = Folder::STATUS_ACTIVE;

        //
        $folder = new Folder($data);

        return DB::transaction(function () use ($data, $folder) {

            $folder->save();

            //
            if (isset($data['documents'])) {
                $this->storeDocument($data['documents'], $folder->id);
            }

            FolderNotes::storeNotes([
                'folder_id' => $folder->id,
                'trans_key' => FolderNotes::FOLDER_NOTE_TRANS_KEY_CREATE
            ]);

            if (isset($data['notes'])) {
                FolderNotes::storeNotes(['folder_id' => $folder->id, 'trans_key' => FolderNotes::FOLDER_NOTE_TRANS_KEY_SAVE_NOTE, 'note' => $data['notes']]);
            }

            return $folder;
        });
    }

    /**
     * Function to Update data
     *
     * @param array $data
     * @param int $id
     *
     * @throws TaxServiceTjCompanyInactiveException
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function update(array $data, int $id)
    {
        $folder = Folder::where('id', $id)->folderOwnerOrCreator()->firstOrFail();

        if (isset($data['regenerate_access_key'])) {
            $data['folder_access_password'] = generateID();
        }

        // Holder Data if Company/Natural Person
        if (isset($data['holder_type'])) {
            $legalEntityForms = LegalEntityForms::getInstance();
            $holderData = $legalEntityForms->holderDataByType($data['holder_type'], $data['holder_type_value'], $data['holder_passport'] ?? null);

            $data = array_merge($data, $holderData);
        }

        DB::transaction(function () use ($data, $folder) {

            $oldData = $folder->toArray();
            $folder->update($data);

            //
            if (isset($data['documents'])) {
                $this->storeDocument($data['documents'], $folder->id);
            }

            if ($data) {
                $lastVersion = FolderNotes::select('version')->where('folder_id', $folder->id)->max('version');

                FolderNotes::storeNotes([
                    'folder_id' => $folder->id,
                    'trans_key' => FolderNotes::FOLDER_NOTE_TRANS_KEY_SAVE,
                    'version' => $lastVersion + 1,
                    'old_data' => $oldData,
                    'new_data' => $folder->toArray(),
                ]);

                if (isset($data['notes'])) {
                    FolderNotes::storeNotes(['folder_id' => $folder->id, 'trans_key' => FolderNotes::FOLDER_NOTE_TRANS_KEY_SAVE_NOTE, 'note' => $data['notes'], 'version' => $lastVersion + 1]);
                }
            }
        });
    }

    /**
     * Function to Delete data
     *
     * @param array $deleteItems
     */
    public function delete(array $deleteItems)
    {
        DB::transaction(function () use ($deleteItems) {

            Folder::whereIn('id', $deleteItems)->folderOwnerOrCreator()->update([
                'show_status' => Folder::STATUS_DELETED,
                'deleted_admin_id' => Auth::user()->id,
                'deleted_admin_ip' => request()->ip()
            ]);

        });
    }

    /**
     * Function to Store document to DB
     *
     * @param $documents
     * @param $folderId
     */
    public function storeDocument($documents, $folderId)
    {
        DB::transaction(function () use ($documents, $folderId) {

            $docs = [];
            foreach ($documents as $value) {
                if ($value) {
                    $docs[] = $value;

                    $folderDocData = [
                        'document_id' => $value,
                        'folder_id' => $folderId,
                    ];

                    $folderDocDataUpdate = array_merge(['user_id' => Auth::id()], $folderDocData);

                    FolderDocuments::updateOrCreate($folderDocData, $folderDocDataUpdate);
                }
            }

            FolderDocuments::whereNotIn('document_id', $docs)->where(['folder_id' => $folderId, 'user_id' => Auth::id()])->delete();
        });
    }

    /**
     * Function to update folder documents
     *
     * @param $folderId
     * @param $newDocIds
     */
    public function updateFolderDocuments(int $folderId, array $newDocIds)
    {
        if ($folderId) {
            DB::transaction(function () use ($folderId, $newDocIds) {

                foreach ($newDocIds as $document) {

                    $folderDocData = [
                        'document_id' => $document,
                        'folder_id' => $folderId,
                    ];

                    $folderDocDataUpdate = array_merge(['user_id' => Auth::id()], $folderDocData);

                    FolderDocuments::updateOrCreate($folderDocData, $folderDocDataUpdate);
                }
            });
        }
    }

    /**
     * Function to delete folder documents
     *
     * @param $documentIds
     * @param $folderId
     */
    public function deleteFolderDocuments($documentIds, $folderId)
    {
        FolderDocuments::where(['user_id' => Auth::id(), 'folder_id' => $folderId])->whereIn('document_id', $documentIds)->delete();
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @param null $id2
     * @return array
     */
    public function getLogData($id, $id2 = null)
    {
        $folder = Folder::where('id', $id)->first()->toArray();
        $folder['documents'] = FolderDocuments::where('folder_id', $folder['id'])->get()->toArray();

        return $folder;
    }
}
