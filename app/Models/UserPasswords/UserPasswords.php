<?php

namespace App\Models\UserPasswords;

use App\Models\BaseModel;

/**
 * Class UserPasswords
 * @package App\Models\UserPasswords
 */
class UserPasswords extends BaseModel
{
    /**
     * @var string[]
     */
    protected $fillable = [
        'user_id',
        'password',
    ];
}