<?php

namespace App\Models\UserPasswords;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

/**
 * Class UserPasswordsManager
 * @package App\Models\UserPasswords
 */
class UserPasswordsManager
{
    /**
     * Function to check user new password same with old passwords or not
     *
     * @param $newPassword
     * @return bool
     */
    public static function checkUserNewPassIsDifferentFromOlds($newPassword)
    {
        $userPasswords = UserPasswords::where('user_id', Auth::id())->pluck('password')->all();

        if (!count($userPasswords)) {
            return true;
        }

        foreach ($userPasswords as $userPassword) {

            if (Hash::check($newPassword,$userPassword)) {
                return false;
            }
        }

        return true;
    }
}