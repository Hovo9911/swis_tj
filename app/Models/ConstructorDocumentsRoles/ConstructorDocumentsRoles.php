<?php

namespace App\Models\ConstructorDocumentsRoles;

use App\Models\BaseModel;
use App\Models\ConstructorDocumentsRolesAttribute\ConstructorDocumentsRolesAttribute;
use App\Models\Role\Role;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class ConstructorDocumentsRoles
 * @package App\Models\ConstructorDocumentsRoles
 */
class ConstructorDocumentsRoles extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'constructor_documents_roles';

    /**
     * @var array
     */
    protected $fillable = [
        'document_id',
        'role_id'
    ];

    /**
     * Function to relate with attributes
     *
     * @return HasMany
     */
    public function attributes()
    {
        return $this->hasMany(ConstructorDocumentsRolesAttribute::class, 'constructor_documents_roles_id', 'id');
    }

    /**
     * Function to relate with role
     *
     * @return HasOne
     */
    public function role()
    {
        return $this->hasOne(Role::class, 'id', 'role_id');
    }
}
