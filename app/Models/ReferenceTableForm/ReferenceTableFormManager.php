<?php

namespace App\Models\ReferenceTableForm;

use App\Jobs\UpdateHsCodesRecursive;
use App\Models\BaseModel;
use App\Models\Document\Document;
use App\Models\NotesOfReference\NotesOfReference;
use App\Models\NotesOfReference\NotesOfReferenceManager;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\ReferenceTable\ReferenceTableStructure;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

/**
 * Class ReferenceTableFormManager
 * @package App\Models\ReferenceTableForm
 */
class ReferenceTableFormManager
{
    /**
     * Function to store data
     *
     * @param array $data
     * @param int $referenceTableId
     * @return Collection
     */
    public function store(array $data, int $referenceTableId)
    {
        $refTable = ReferenceTable::where('id', $referenceTableId)->firstOrFail();

        // table name
        $referenceTableFormName = 'reference_' . $refTable->table_name;
        $referenceTableFormMlName = 'reference_' . $refTable->table_name . '_ml';

        // Ml Data
        $mlsData = $data['ml'];
        unset($data['ml']);

        //
        $isImport = $data['is_imported'] ?? false;
        unset($data['is_imported']);

        if ($data['show_status'] == ReferenceTable::STATUS_INACTIVE) {
            $data['end_date'] = Carbon::now();
        }

        $startDate = Carbon::parse($data['start_date'])->format(config('swis.date_format'));
        if ($startDate > currentDate()) {
            $data['show_status'] = ReferenceTable::STATUS_INACTIVE;
        }

        $data['created_admin_id'] = Auth::check() ? Auth::user()->id : 0;
        $data['created_admin_ip'] = request()->ip();
        $data['company_tax_id'] = $data['company_tax_id'] ?? Auth::user()->companyTaxId();
        $data['user_id'] = Auth::id();

        //
        $refCreatedTable = new BaseModel();
        $refCreatedTable->setTable($referenceTableFormName);
        $refCreatedTable->fillable(array_keys($data));
        $refCreatedTable->fill($data);

        $previousRowInfo = null;
        return DB::transaction(function () use ($mlsData, $refCreatedTable, $referenceTableFormName, $referenceTableFormMlName, $referenceTableId, $previousRowInfo, $refTable, $data, $isImport) {

            // Inactivate previous rows when the codes same
            if ($refTable->show_status == ReferenceTable::STATUS_ACTIVE) {

                $codeCheckUnique = DB::table('reference_' . $refTable->table_name)->select('id', 'show_status')->where('code', $data['code'])->where('show_status', '=', ReferenceTable::STATUS_ACTIVE)->get();

                if ($codeCheckUnique->count()) {
                    foreach ($codeCheckUnique as $codeCheck) {
                        $previousRowInfo = $this->inactivatePreviousRowInfo($codeCheck, $refTable->table_name, $endDate = $data['start_date']);
                    }
                }
            }

            // Main Ref Save
            $refCreatedTable->save();

            // Ml
            foreach ($mlsData as $lngId => $mlData) {

                $mlData['lng_id'] = $lngId;
                $mlData[$referenceTableFormName . '_id'] = $refCreatedTable->id;

                DB::table($referenceTableFormMlName)->insert($mlData);
            }

            // Hs Code 6 Ref
            if ($referenceTableFormName == ReferenceTable::REFERENCE_HS_CODE_6) {
                if (strlen($data['code']) == 10) {
                    $this->updateHsCode6($refCreatedTable->id);
                } elseif (strlen($data['code']) < 10) {
                    $hsCodes = DB::table($referenceTableFormName)->select('id', 'code')->where('code', 'like', "{$data['code']}%")->get();
                    foreach ($hsCodes as $item) {
                        $this->updateHsCode6($item, false);
                    }
                }
            }

            if (!$isImport) {

                // Notes
                $notesOfReferenceManager = new NotesOfReferenceManager();
                $notesOfReferenceManager->store(NotesOfReference::ACTION_TYPE_CREATE, $referenceTableId, $refCreatedTable->id, $refCreatedTable->code);

                if (!is_null($previousRowInfo)) {

                    $oldData = $this->generateNotesData($referenceTableFormName, $previousRowInfo[0]);
                    $newData = $this->generateNotesData($referenceTableFormName, $refCreatedTable->id);

                    $notesOfReferenceManager->store('edit', $referenceTableId, $refCreatedTable->id, $refCreatedTable->code, $oldData, $newData);
                }

                // Send Notification to SW Admin
                $this->sendNotification($refTable, $refCreatedTable->id);

            }

            return $refCreatedTable;
        });
    }

    /**
     * Function to Update data
     *
     * @param array $data
     * @param int $referenceFormId
     * @param int $referenceTableId
     */
    public function update(array $data, int $referenceTableId, int $referenceFormId)
    {
        $inputShowStatus = $data['show_status'];

        $referenceTable = ReferenceTable::where('id', $referenceTableId)->firstOrFail();

        $referenceTableFormName = "reference_{$referenceTable->table_name}";
        $referenceTableFormMlName = "reference_{$referenceTable->table_name}_ml";

        $currentFormRow = DB::table($referenceTableFormName)->where('id', $referenceFormId)->first();

        // Data not changed
        if ($currentFormRow->show_status == ReferenceTable::STATUS_INACTIVE && $inputShowStatus == ReferenceTable::STATUS_INACTIVE) {
            return;
        }

        // To Inactive
        if ($currentFormRow->show_status == ReferenceTable::STATUS_ACTIVE && $inputShowStatus == ReferenceTable::STATUS_INACTIVE) {

            $this->updateReferenceRowStatus($referenceTable, $referenceFormId, ReferenceTable::STATUS_INACTIVE);

            return;
        }

        // To Active
        if ($currentFormRow->show_status == ReferenceTable::STATUS_INACTIVE && $inputShowStatus == ReferenceTable::STATUS_ACTIVE) {

            $this->updateReferenceRowStatus($referenceTable, $referenceFormId, ReferenceTable::STATUS_ACTIVE);

            return;
        }

        $data['code'] = $currentFormRow->code ?? $data['code'];
        $mlData = $data['ml'] ?? [];
        $oldData = $this->generateNotesData($referenceTableFormName, $referenceFormId);

        unset($data['ml']);
        unset($data['is_imported']);

        DB::transaction(function () use ($mlData, $data, $referenceFormId, $referenceTableFormName, $referenceTableFormMlName, $referenceTableId, $oldData, $currentFormRow, $referenceTable, $inputShowStatus) {

            // Inactivate previous rows when the codes same
            if ($referenceTable->show_status == ReferenceTable::STATUS_ACTIVE || $inputShowStatus == ReferenceTable::STATUS_ACTIVE) {

                $codeCheckUnique = DB::table('reference_' . $referenceTable->table_name)->select('id', 'show_status', 'end_date')->where('code', $data['code'])->where('show_status', '=', ReferenceTable::STATUS_ACTIVE)->get();

                if ($codeCheckUnique->count()) {
                    foreach ($codeCheckUnique as $codeCheck) {
                        $this->inactivatePreviousRowInfo($codeCheck, $referenceTable->table_name, $endDate = $codeCheck->end_date);
                    }
                }
            }

            // -------

            if (!isset($data['company_tax_id'])) {
                $data['company_tax_id'] = Auth::user()->companyTaxId();
            }

            $data['start_date'] = Carbon::now();
            $data['user_id'] = Auth::id();
            $data['created_at'] = Carbon::now();
            $data['updated_at'] = Carbon::now();
            $data['created_admin_id'] = Auth::check() ? Auth::user()->id : 0;
            $data['created_admin_ip'] = request()->ip();

            $newFormRow = DB::table($referenceTableFormName)->insertGetId($data);

            $newFormMl = [];
            foreach ($mlData as $lngId => $ml) {
                $ml['lng_id'] = $lngId;
                $ml['show_status'] = $data['show_status'];
                $ml[$referenceTableFormName . '_id'] = $newFormRow;
                $newFormMl[] = $ml;
            }

            if (count($newFormMl)) {
                DB::table($referenceTableFormMlName)->insert($newFormMl);
            }

            if ($referenceTableFormName == ReferenceTable::REFERENCE_HS_CODE_6) {
                if (strlen($data['code']) == 10) {
                    $this->updateHsCode6($newFormRow);
                } elseif (strlen($data['code']) < 10) {
                    $hsCodes = DB::table($referenceTableFormName)->select('id', 'code')->where('code', 'like', "{$data['code']}%")->get();
                    foreach ($hsCodes as $item) {
                        $this->updateHsCode6($item, false);
                    }
                }
            }

            // Notes
            $newData = $this->generateNotesData($referenceTableFormName, $newFormRow);
            $notesOfReferenceManager = new NotesOfReferenceManager();
            $notesOfReferenceManager->store(NotesOfReference::ACTION_TYPE_EDIT, $referenceTableId, $referenceFormId, $data['code'], $oldData, $newData);

            // Send Notification to SW_Admin
            $this->sendNotification($referenceTable, $newFormRow);

        });

    }

    /**
     * Function to every day update exchange rate
     *
     * @return void
     */
    public function updateExchangeRatesRefTable()
    {
        try {
            $client = new Client([
                'verify' => false
            ]);

            $today = currentDate();
            $request = $client->get("http://nbt.tj/ru/kurs/export_xml.php?date=$today&export=xmlout");
            $response = $request->getBody()->getContents();

            $xml = str_replace("windows-1251", "utf-8", $response);

            $result = simplexml_load_string($xml);
        } catch (Exception $e) {
            Bugsnag::notifyError('ErrorType', 'Currency Request Failed, Exception message is: ' . $e->getMessage());
        }

        if (!empty($result->Valute)) {

            DB::transaction(function () use ($result) {

                foreach ($result->Valute as $rate) {

                    $currencyId = (string)$rate->attributes()->ID;

                    $refCurrenciesTable = ReferenceTable::REFERENCE_CURRENCIES;
                    $refCurrenciesMlTable = $refCurrenciesTable . '_ml';

                    $currentCurrency = DB::table($refCurrenciesTable)->select('id', 'code', 'name')
                        ->join($refCurrenciesMlTable, "{$refCurrenciesMlTable}.{$refCurrenciesTable}_id", "=", "{$refCurrenciesTable}.id")
                        ->where('lng_id', 1)
                        ->where('code', $currencyId)
                        ->where($refCurrenciesTable . '.show_status', ReferenceTable::STATUS_ACTIVE)
                        ->first();

                    if (!is_null($currentCurrency)) {
                        $exchangeRateRefTableName = ReferenceTable::REFERENCE_EXCHANGE_RATES;
                        $exchangeRateRefTableMlName = ReferenceTable::REFERENCE_EXCHANGE_RATES . '_ml';

                        // Inactive current rows
                        $currentActiveExchangeRates = DB::table($exchangeRateRefTableName)->where(['code' => $currencyId, 'show_status' => ReferenceTable::STATUS_ACTIVE])->pluck('id')->toArray();
                        if (count($currentActiveExchangeRates)) {
                            DB::table($exchangeRateRefTableName)->whereIn('id', $currentActiveExchangeRates)->update(['show_status' => ReferenceTable::STATUS_INACTIVE, 'end_date' => Carbon::now()]);
                            DB::table($exchangeRateRefTableMlName)->whereIn($exchangeRateRefTableName . '_id', $currentActiveExchangeRates)->update(['show_status' => ReferenceTable::STATUS_INACTIVE]);
                        }

                        $exchangeRateData = [
                            'company_tax_id' => '0',
                            'user_id' => 1,
                            'code' => $currencyId,
                            'currency_code' => $currentCurrency->id,
                            'currency_code_description' => $currentCurrency->name,
                            'rate' => (string)$rate->Value,
                            'number_of_units' => (string)$rate->Nominal,
                            'start_date' => Carbon::now(),
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                            'show_status' => ReferenceTable::STATUS_ACTIVE
                        ];

                        $newCurrencyId = DB::table($exchangeRateRefTableName)->insertGetId($exchangeRateData);

                        $newFormMl = [];
                        foreach (activeLanguages() as $language) {

                            $newFormMl[] = [
                                $exchangeRateRefTableName . '_id' => $newCurrencyId,
                                'lng_id' => $language->id,
                                'name' => (string)$rate->CharCode,
                            ];
                        }

                        if (count($newFormMl)) {
                            DB::table($exchangeRateRefTableMlName)->insert($newFormMl);
                        }
                    }
                }
            });
        }
    }

    /**
     * Function to update reference row only show_status,updated_admin_id,updated_admin_ip
     *
     * @param $referenceTable
     * @param $referenceRowId
     * @param $status
     */
    private function updateReferenceRowStatus($referenceTable, $referenceRowId, $status)
    {
        $referenceTableName = $referenceTable->table_name;
        $updateData['updated_admin_id'] = Auth::id();
        $updateData['updated_admin_ip'] = request()->ip();
        $updateData['show_status'] = $status;

        // To Active
        if ($updateData['show_status'] == ReferenceTable::STATUS_ACTIVE) {
            $updateData['end_date'] = null;
        }

        DB::transaction(function () use ($referenceTableName, $referenceRowId, $updateData, $status) {
            DB::table('reference_' . $referenceTableName)->where('id', $referenceRowId)->update($updateData);
            DB::table('reference_' . $referenceTableName . '_ml')->where('reference_' . $referenceTableName . '_id', $referenceRowId)->update(['show_status' => $status]);
        });

        //
        $updatedRef = getReferenceRows($referenceTableName, $referenceRowId);

        // Send Notification to SW_Admin
        $this->sendNotification($referenceTable, $updatedRef->id);
    }

    /**
     * Function to Update HsCode6
     *
     * @param $hdCodeData
     * @param bool $single
     */
    private function updateHsCode6($hdCodeData, $single = true)
    {
        dispatch(new UpdateHsCodesRecursive($hdCodeData, $single));
    }

    /**
     * Function to get product hs codes for saf
     *
     * @param $searchValue
     * @return array
     */
    public function getProductHsCodes($searchValue)
    {
        $referenceTable = ReferenceTable::REFERENCE_HS_CODE_6;
        $referenceTableMl = $referenceTable . '_ml';

        $selectType = DB::raw("CONCAT(code, ' , ', name) AS name");

        $results = DB::table($referenceTable)->select($selectType, 'id', 'code', 'long_name', 'path_name', 'unit_of_measurement_2', 'unit_of_measurement_1')
            ->join($referenceTableMl, "{$referenceTable}.id", '=', "{$referenceTableMl}.{$referenceTable}_id")
            ->where($referenceTableMl . '.lng_id', cLng('id'))
            ->whereRaw('LENGTH(code) = 10')
            ->where(function ($q) use ($searchValue) {
                $q->where('name', 'ILIKE', '%' . $searchValue . '%')
                    ->orWhere('code', 'ILIKE', '%' . $searchValue . '%');
            })
            ->whereRaw('LENGTH(code) = 10')
            ->where("{$referenceTable}.show_status", ReferenceTable::STATUS_ACTIVE);

        $results = $results->limit(1000)->get();

        $referenceData = [];

        $referenceUnitMeasurementData = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT);
        $referenceUnitMeasurementDataByCode = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, 'code', true);

        foreach ($results as $result) {

            $quantity_name_1 = '';
            $quantity_value_1 = 0;
            $quantity_code_1 = '';

            $quantity_name_2 = '';
            $quantity_value_2 = 0;

            if (!is_null($result->unit_of_measurement_2)) {

                if (isset($referenceUnitMeasurementData[$result->unit_of_measurement_2])) {

                    $referenceUnitMeasurement_2 = $referenceUnitMeasurementData[$result->unit_of_measurement_2];

                    if (isset($referenceUnitMeasurementDataByCode[$referenceUnitMeasurement_2['code']])) {
                        $referenceUnitMeasurement_2_code = $referenceUnitMeasurementDataByCode[$referenceUnitMeasurement_2['code']];

                        $quantity_value_2 = $referenceUnitMeasurement_2_code['id'];
                        $quantity_name_2 = '(' . $referenceUnitMeasurement_2_code['code'] . ')' . ' ' . $referenceUnitMeasurement_2_code['name'];
                    }
                }
            }

            if (!is_null($result->unit_of_measurement_1)) {

                if (isset($referenceUnitMeasurementData[$result->unit_of_measurement_1])) {

                    $referenceUnitMeasurement_1 = $referenceUnitMeasurementData[$result->unit_of_measurement_1];

                    if (isset($referenceUnitMeasurementDataByCode[$referenceUnitMeasurement_1['code']])) {

                        $referenceUnitMeasurement_1_code = $referenceUnitMeasurementDataByCode[$referenceUnitMeasurement_1['code']];

                        $quantity_value_1 = $referenceUnitMeasurement_1_code['id'];
                        $quantity_code_1 = $referenceUnitMeasurement_1_code['code'];
                        $quantity_name_1 = '(' . $referenceUnitMeasurement_1_code['code'] . ')' . ' ' . $referenceUnitMeasurement_1_code['name'];
                    }
                }
            }

            $referenceData[] = [
                'name' => trim($result->path_name) ?? $result->name,
                'code' => $result->code,
                'long_name' => trim($result->path_name) ?? $result->long_name,
                'quantity_value_2' => $quantity_value_2,
                'quantity_name_2' => $quantity_name_2,
                'quantity_value_1' => $quantity_value_1,
                'quantity_name_1' => $quantity_name_1,
                'quantity_code_1' => $quantity_code_1,
                'id' => $result->id,
            ];
        }

        return $referenceData;
    }

    /**
     * Function to return old and new data
     *
     * @param $referenceTableFormName
     * @param $referenceFormId
     * @return array
     */
    public function generateNotesData($referenceTableFormName, $referenceFormId)
    {
        $technicalFields = ['created_admin_id', 'created_admin_ip', 'updated_admin_id', 'updated_admin_ip', 'deleted_admin_id', 'deleted_admin_ip', 'created_at', 'updated_at', 'show_status'];

        $data = DB::table($referenceTableFormName)->where('id', $referenceFormId)->first();
        $ml = $this->getRefTableML($referenceTableFormName, $referenceFormId);

        return $this->removeTechnicalFields($technicalFields, $data, $ml);
    }

    /**
     * Function to return ref table row ml fields
     *
     * @param $referenceTableFormName
     * @param $referenceFormId
     * @return array
     */
    private function getRefTableML($referenceTableFormName, $referenceFormId)
    {
        $activeLanguages = [];
        foreach (activeLanguages() as $lng) {
            $activeLanguages[$lng->id] = $lng->name;
        }

        $mlFields = [];
        $technicalFields = ["{$referenceTableFormName}_id", "lng_id", "show_status"];
        $data = DB::table("{$referenceTableFormName}_ml")->where("{$referenceTableFormName}_id", $referenceFormId)->get()->toArray();

        foreach ($data as $item) {
            foreach ($item as $field => $value) {
                if (!in_array($field, $technicalFields)) {
                    $mlFields[$activeLanguages[$item->lng_id]][$field] = $value;
                }
            }
        }

        return $mlFields;
    }

    /**
     * Function to return array without technical fields
     *
     * @param $technicalFields
     * @param $data
     * @param $ml
     * @return array
     */
    private function removeTechnicalFields($technicalFields, $data, $ml)
    {
        $fields = [];
        foreach ($data as $field => $value) {
            if (!in_array($field, $technicalFields)) {
                $fields[$field] = $value;
            }
        }

        $fields['ml'] = $ml;

        return $fields;
    }

    /**
     * Function to inactivating previous row info
     *
     * @param $previousRowInfo
     * @param $referenceTableName
     * @param bool $endDate
     * @return array
     */
    public function inactivatePreviousRowInfo($previousRowInfo, $referenceTableName, $endDate = false)
    {
        $referenceTableFormName = 'reference_' . $referenceTableName;
        $referenceTableFormMlName = 'reference_' . $referenceTableName . '_ml';

        $data['updated_at'] = currentDateTime();
        $data['updated_admin_id'] = Auth::id();
        $data['updated_admin_ip'] = request()->ip();

        if ($previousRowInfo->show_status == ReferenceTable::STATUS_ACTIVE && !is_null(DB::table($referenceTableFormName)->where('id', $previousRowInfo->id)->where('show_status', '!=', ReferenceTable::STATUS_INACTIVE)->first())) {
            $data['end_date'] = Carbon::now();
            $data['show_status'] = ReferenceTable::STATUS_INACTIVE;
        }

        DB::transaction(function () use ($referenceTableFormName, $referenceTableFormMlName, $previousRowInfo, $data) {
            DB::table($referenceTableFormName)->where('id', $previousRowInfo->id)->update($data);
            DB::table($referenceTableFormMlName)->where($referenceTableFormName . '_id', $previousRowInfo->id)->update(['show_status' => ReferenceTable::STATUS_INACTIVE]);
        });

        return array($previousRowInfo->id, $data);
    }

    /**
     * Function to inactivating  reference table rows
     *
     * @return void
     */
    public function inactivateReferenceTableRows()
    {
        $referenceTables = ReferenceTable::select('table_name')->where('show_status', ReferenceTable::STATUS_ACTIVE)->get();

        foreach ($referenceTables as $referenceTable) {

            $referenceTableName = "reference_{$referenceTable->table_name}";

            // Check whether reference_ table has start_date,end date column
            if (Schema::hasTable($referenceTableName)) {

                // If reference row active and start_date > today or end_date < today inactivate ref. row
                $needInactivateReferenceRows = DB::table($referenceTableName)
                    ->where('show_status', ReferenceTable::STATUS_ACTIVE)
                    ->where(function ($q) {
                        $q->where('start_date', '>', currentDate())
                            ->orWhere('end_date', '<', currentDate());
                    })->pluck('id')->all();

                if (count($needInactivateReferenceRows)) {

                    DB::transaction(function () use ($referenceTableName, $needInactivateReferenceRows) {

                        DB::table($referenceTableName)->whereIn('id', $needInactivateReferenceRows)
                            ->update(['show_status' => ReferenceTable::STATUS_INACTIVE, 'updated_at' => currentDateTime()]);

                        DB::table($referenceTableName . '_ml')
                            ->whereIn($referenceTableName . '_id', $needInactivateReferenceRows)->update(['show_status' => ReferenceTable::STATUS_INACTIVE]);

                        Log::info("Reference-inactive-rows: $referenceTableName, Rows id: " . print_r(json_encode($needInactivateReferenceRows) . "", 1));

                    });
                }
            }
        }
    }

    /**
     * Function to get classificators with agencies for document archive
     *
     * @param $searchVal
     * @return array
     */
    public function getDocumentClassificatorsWithAgencies($searchVal)
    {
        $rTableClassification = ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS;
        $rTableClassificationMl = $rTableClassification . '_ml';

        $refClassificatorDocuments = $result = DB::table($rTableClassification)->select($rTableClassification . '.id', $rTableClassification . '.code', $rTableClassificationMl . '.name')
            ->join($rTableClassificationMl, $rTableClassification . '.id', '=', $rTableClassificationMl . '.' . $rTableClassification . '_id')
            ->where(function ($q) use ($searchVal, $rTableClassification, $rTableClassificationMl) {
                $q->where($rTableClassificationMl . '.name', 'ILIKE', '%' . $searchVal . '%')->orWhere($rTableClassification . '.code', 'ILIKE', '%' . $searchVal . '%');
            })
            ->where($rTableClassificationMl . '.lng_id', cLng('id'))
            ->where($rTableClassification . '.show_status', Document::STATUS_ACTIVE)
            ->get();

        $result = [];
        if ($refClassificatorDocuments->count()) {

            foreach ($refClassificatorDocuments as $refClassificatorDocument) {
                $refDocumentTypeAgencies = DB::table('reference_document_type_reference')->select('reference_agencies_reference.code as agency_code', 'reference_agencies_reference_ml.name as agency_name')
                    ->join('reference_agencies_reference', 'reference_document_type_reference.according_agency', '=', 'reference_agencies_reference.id')
                    ->join('reference_agencies_reference_ml', 'reference_agencies_reference.id', '=', 'reference_agencies_reference_ml.reference_agencies_reference_id')
                    ->where('reference_document_type_reference.document_type', $refClassificatorDocument->id)
                    ->where('reference_agencies_reference_ml.lng_id', cLng('id'))
                    ->where('reference_document_type_reference.show_status', Document::STATUS_ACTIVE)
                    ->where('reference_agencies_reference.show_status', Document::STATUS_ACTIVE)
                    ->get();

                $result[] = [
                    'id' => $refClassificatorDocument->id,
                    'name' => $refClassificatorDocument->name,
                    'code' => $refClassificatorDocument->code,
                    'agencies' => $refDocumentTypeAgencies->toArray()
                ];
            }
        }

        return $result;
    }

    /**
     * Function to get reference form data
     *
     * @param $mainRefId
     * @param $searchValue
     * @param $resultValueId
     * @param $refType
     * @param $codeName
     * @return array|mixed
     */
    public function getReferenceFormDataValues($mainRefId, $searchValue, $resultValueId, $refType, $codeName)
    {
        $referenceTable = ReferenceTable::where('id', $mainRefId)->firstOrFail();

        $exceptionList = [
            ReferenceTable::REFERENCE_HS_CODE_6
        ];

        $rTable = 'reference_' . $referenceTable->table_name;
        $rTableMl = 'reference_' . $referenceTable->table_name . '_ml';

        // If user search Classificator Documents
        if ($rTable == ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS) {
            return $this->getClassificatorDocumentTypes($searchValue, $refType, $resultValueId);
        }

        $selectType = $rTableMl . '.name';
        if ($rTable == ReferenceTable::REFERENCE_HS_CODE_6 || $codeName) {
            $selectType = DB::raw("CONCAT('(',code, ') ', name) AS name");
        }

        $results = DB::table($rTable)->select($selectType, $rTable . '.*')->join($rTableMl, $rTable . '.id', '=', $rTableMl . '.' . $rTable . '_id')
            ->where($rTableMl . '.lng_id', cLng('id'))
            ->where(function ($q) use ($searchValue, $rTable) {
                $q->where('name', 'ILIKE', '%' . $searchValue . '%')
                    ->orWhere('code', 'ILIKE', '%' . $searchValue . '%')
                    ->when(($rTable == ReferenceTable::REFERENCE_COUNTRIES), function ($q) use ($searchValue) {
                        $q->orWhere("country_id", $searchValue);
                    });
            })->where($rTable . '.show_status', ReferenceTable::STATUS_ACTIVE)
            ->get();

        $data = [];
        if ($refType != ReferenceTableStructure::COLUMN_TYPE_AUTOCOMPLETE && $results->count()) {

            $dataRes = [];
            foreach ($results as $key => $result) {

                $dataRes[$key]['name'] = $result->name;
                $dataRes[$key]['long_name'] = !empty($result->long_name) ? $result->long_name : '';
                $dataRes[$key]['desc'] = $result->name;
                $dataRes[$key]['id'] = empty($resultValueId) ? $result->code : $result->id;
                $dataRes[$key]['code'] = $result->code;

                if (in_array($rTable, $exceptionList)) {
                    $dataRes[$key]['showOnlyCode'] = true;
                }

            }

            $data['items'] = $dataRes;
        } else {
            $data['items'] = $results;
        }

        return $data;
    }

    /**
     * Function to get classifcator document type
     *
     * @param $searchValue
     * @param bool $refType
     * @param bool $resultValueId
     * @return array
     */
    public function getClassificatorDocumentTypes($searchValue, $refType = false, $resultValueId = false)
    {
        $where = function ($query) use ($searchValue) {
            return $query->orWhere("name", "ILIKE", "%{$searchValue}%")
                ->orWhere("code", "ILIKE", "%{$searchValue}%");
        };

        $results = getReferenceRows(ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS, false, false, ['id', 'name', 'code', 'long_name'], 'get', false, false, $where);

        $items = [];
        foreach ($results as $result) {
            $name = "({$result->code}) {$result->name}";

            if (!empty($refType) && $refType == ReferenceTableStructure::COLUMN_TYPE_AUTOCOMPLETE) {
                $name = $result->name;
            }

            $items[] = [
                'id' => $resultValueId ? $result->id : $result->code,
                'name' => $name,
                'code' => $result->code,
                'long_name' => $result->long_name,
            ];
        }

        $data['items'] = $items;

        return $data;
    }

    /**
     * Function to get document type reference by document type id
     *
     * @param $documentTypeId
     * @return Collection
     */
    public function getDocumentTypeReference($documentTypeId)
    {
        return DB::table('reference_document_type_reference')->select('reference_agencies_reference.code as agency_code', 'reference_agencies_reference_ml.name as agency_name')
            ->join('reference_agencies_reference', 'reference_document_type_reference.according_agency', '=', 'reference_agencies_reference.id')
            ->join('reference_agencies_reference_ml', 'reference_agencies_reference.id', '=', 'reference_agencies_reference_ml.reference_agencies_reference_id')
            ->where('reference_document_type_reference.document_type', $documentTypeId)
            ->where('reference_agencies_reference_ml.lng_id', cLng('id'))->where('reference_document_type_reference.show_status', ReferenceTable::STATUS_ACTIVE)->where('reference_agencies_reference.show_status', ReferenceTable::STATUS_ACTIVE)
            ->get();
    }

    /**
     * Function to get data from autocomplete inputs
     *
     * @param $searchQuery
     * @param $referenceID
     * @return array
     */
    public function getRefDataByAutocomplete($searchQuery, $referenceID)
    {
        $referenceTable = ReferenceTable::where('id', $referenceID)->firstOrFail();

        $refTableName = 'reference_' . $referenceTable->table_name;
        $refTableNameMl = 'reference_' . $referenceTable->table_name . '_ml';

        // Check table exist
        if (!Schema::hasTable($refTableName)) {

            $data['items'] = [];

            return responseResult('OK', '', $data);
        }

        $refFields = ReferenceTableStructure::where('reference_table_id', $referenceID)->where('has_ml', ReferenceTable::FALSE)->pluck('field_name')->all();
        $refFieldsMl = ReferenceTableStructure::where('reference_table_id', $referenceID)->where('has_ml', ReferenceTable::TRUE)->where('field_name', '!=', ReferenceTable::REFERENCE_DEFAULT_FIELD_NAME)->pluck('field_name')->all();

        $query = DB::table($refTableName)->select(DB::raw("CONCAT('(',code, ') ', name) AS name"), $refTableName . '.id', DB::raw("{$referenceTable->show_status} as reference_table_status"))
            ->join($refTableNameMl, $refTableName . '.id', '=', $refTableNameMl . '.' . $refTableName . '_id')
            ->where('lng_id', cLng('id'))
            ->where("{$refTableName}.show_status", ReferenceTable::STATUS_ACTIVE)
            ->limit(25);

        array_walk($refFields, function (&$value, $key) use ($refTableName, $query) {
            $query->addSelect($refTableName . '.' . $value);
        });

        array_walk($refFieldsMl, function (&$value, $key) use ($refTableNameMl, $query) {
            $query->addSelect($refTableNameMl . '.' . $value);
        });

        if (!Auth::user()->isSwAdmin()) {
            $hasAgencyColumnGetName = ReferenceTable::hasAgencyColumnGetName($referenceID);

            if ($hasAgencyColumnGetName) {
                $query->whereIn("reference_$referenceTable->table_name" . '.' . $hasAgencyColumnGetName, ReferenceTable::getUserReferenceAgencyIds());
            }
        }

        $query->where(function ($q) use ($query, $searchQuery) {
            $q->where('name', 'ILIKE', '%' . $searchQuery . '%')
                ->orWhere('code', 'ILIKE', '%' . $searchQuery . '%');
        });

        return $query->get()->toArray();
    }

    /**
     * Function to send notification to SW_Admin
     *
     * @param $referenceTable
     * @param $changedRowId
     */
    private function sendNotification($referenceTable, $changedRowId)
    {
        $referenceTableFormNotificationsManager = new ReferenceTableFormNotificationsManager();
        $referenceTableFormNotificationsManager->storeNotification($referenceTable, $changedRowId);
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @param null $parentId
     * @param null $code
     * @return array
     */
    public function getLogData($id, $parentId, $code = null)
    {
        if (!is_null($parentId)) {
            $referenceTable = ReferenceTable::find($id);
            $tableName = "reference_" . $referenceTable->table_name;
            if (!is_null($code)) {

                $referenceTableRow = DB::table($tableName)->where('code', $code)->orderBy('id', 'desc')->first();
                $referenceTableRowMl = DB::table($tableName . "_ml")->where($tableName . "_id", $referenceTableRow->id)->get()->toArray();
                $referenceTableRow->ml = $referenceTableRowMl;
            } else {
                $referenceTableRow = DB::table($tableName)->where('id', $parentId)->first();
                $referenceTableRowMl = DB::table($tableName . "_ml")->where($tableName . "_id", $parentId)->get()->toArray();
                $referenceTableRow->ml = $referenceTableRowMl;
            }

            return [$referenceTableRow, $referenceTableRow->code];
        }

        return [];
    }
}
