<?php

namespace App\Models\ReferenceTableForm;

use App\Exports\ReferenceTableExport;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\ReferenceTable\ReferenceTableStructure;

/**
 * Class ReferenceTableFormExportManager
 * @package App\Models\ReferenceTableForm
 */
class ReferenceTableFormExportManager
{
    /**
     * Function to get export ref data
     *
     * @param $referenceID
     * @return ReferenceTableExport
     */
    public function export($referenceID)
    {
        $referenceTable = ReferenceTable::where('id', $referenceID)->first();

        $getColumns = ReferenceTableStructure::select('field_name', 'sort_order', 'type', 'parameters', 'has_ml')
            ->where('reference_table_id', $referenceID)
            ->where('show_status', 1)
            ->get();

        $getResult = $this->getExportFields($getColumns, activeLanguages());

        $languages = $getResult['languages'];
        $exportFields = $getResult['exportFields'];

        $referenceTableExport = new ReferenceTableExport();
        return $referenceTableExport->getData($exportFields, $languages, $referenceTable, activeLanguages());
    }

    /**
     * Function to get export fields
     *
     * @param $getColumns
     * @param $activeLangs
     * @return array
     */
    private function getExportFields($getColumns, $activeLangs)
    {
        $exportFields = [];
        $staticFields = [
            'id' => [
                'type' => 'int',
                'parameters' => []
            ]
        ];

        $languages = [];
        foreach ($getColumns as $column) {
            $typeParameterArray = [
                'type' => $column->type,
                'parameters' => $column->parameters
            ];
            if ($column->has_ml) {
                foreach ($activeLangs as $language) {
                    $languages[$language->id][$column->field_name] = $column->field_name . '_' . $language->code;
                    $exportFields[$column->field_name . '_' . $language->code] = $typeParameterArray;
                }
            } else {
                $exportFields[$column->field_name] = $typeParameterArray;
            }

            if ($column->type === 'select') {
                foreach ($activeLangs as $language) {
                    $exportFields[$column->field_name . '_label_' . $language->code] = array_merge($typeParameterArray, ['relation_column' => $column->field_name]);
                }
            }
        }

        $exportFields = array_merge($staticFields, $exportFields);

        return [
            'exportFields' => $exportFields,
            'languages' => $languages
        ];
    }
}