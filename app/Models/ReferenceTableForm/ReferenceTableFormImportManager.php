<?php

namespace App\Models\ReferenceTableForm;

use App\Models\ReferenceTable\ReferenceTable;
use App\Models\ReferenceTable\ReferenceTableStructure;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

// NOT USED
class ReferenceTableFormImportManager
{
    public function import($refId, $fileName)
    {
        $activeLangs = activeLanguages();
        $getColumns = ReferenceTableStructure::select('field_name', 'sort_order', 'type', 'parameters', 'has_ml')
            ->where('reference_table_id', $refId)
            ->where('show_status', 1)
            ->get();

        $getResult = $this->getExportFields($getColumns, $activeLangs);

        $exportFields = $getResult['exportFields'];

        try {
            DB::statement(DB::raw('SET LOCAL lock_timeout = 120'));

            DB::transaction(function () use ($fileName, $exportFields, $refId) {

                if (($handle = fopen(env('FILE_UPLOADER_TMP_PATH') . "/uploads/temp/{$fileName}", "r")) !== FALSE) {
//                    fprintf($request->input('uploded_file'), chr(0xEF) . chr(0xBB) . chr(0xBF));
                    $i = 0;
                    $row = [];
                    $firstRow = [];
                    while (($data = fgetcsv($handle, 0, ',')) !== FALSE) {

                        $i++;

                        if ($i == 1) {
                            $data[0] = "id";
                        }

                        if ($i == 1 && !empty(array_diff(array_keys($exportFields), $data))) {
                            return ['success' => false, 'errors' => trans('swis.reference_table_form.invalid_columns') . implode(',', $data)];
                        } elseif ($i == 1) {
                            $firstRow = $data;
                            continue;
                        }

                        $row[] = array_combine($firstRow, $data);

                        if ($i % 100 == 0) {
                            //Get relation fields labels
                            $incorrectFields = $this->getIncorrectFields($exportFields, $row);

                            //Insert rows
                            $result = $this->removeAndAddImportedTableData($refId, $row, $incorrectFields);
                            $row = [];
                        }
                    }


                    //Get relation fields labels
                    $incorrectFields = $this->getIncorrectFields($exportFields, $row);

                    if (!empty($row)) {
                        $result = $this->removeAndAddImportedTableData($refId, $row, $incorrectFields);
                    }
                    fclose($handle);

                    return $result;
                }
            });

            DB::statement(DB::raw('SET LOCAL lock_timeout = 50'));
        } catch (Throwable $e) {

            return responseResult('INVALID_DATA', urlWithLng("/reference-table-form/{$refId}/"), [], ['uploded_file' => trans('core.loading.invalid_data')]);
        }
    }

    /**
     * Function to return array of relation labels fileds
     *
     * @param $exportFields
     * @param $importData
     * @return array
     */
    private function getIncorrectFields($exportFields, $importData)
    {
        $incorrectFields = [];
        foreach ($exportFields as $key => $field) {
            if ($field['type'] === 'select' && isset($field['relation_column'])) {
                $incorrectFields[] = $key;
            }
        }

        return $incorrectFields;
    }

    /**
     * Function to remove and add imported table data
     *
     * @param $refId
     * @param $importData
     * @param $incorrectFields
     * @return array
     */
    private function removeAndAddImportedTableData($refId, $importData, $incorrectFields)
    {
        $languagesFields = [];
        $referenceTable = ReferenceTable::where('id', $refId)->firstOrFail();
        $referenceTableStructure = ReferenceTableStructure::where('reference_table_id', $refId)->where('has_ml', '1')->get();

        foreach ($referenceTableStructure as $field) {
            $languagesFields[$field->field_name] = "{$field->field_name}_";
        }

        $languages = [];
        $activeLangs = activeLanguages();

        foreach ($activeLangs as $lang) {
            $languages["_{$lang->code}"] = [
                'id' => $lang->id,
                'code' => $lang->code,
            ];
        }

        $now = Carbon::now();
        $rTable = 'reference_' . $referenceTable->table_name;
        $notCorrectItems = $this->getNotCorrectFields($importData, $rTable, $now);

        if (!empty($notCorrectItems)) {
            return [
                'success' => false,
                'errors' => $notCorrectItems
            ];
        }

        foreach ($importData as &$data) {

            $data['start_date'] = is_object($data['start_date']) ? $data['start_date']->format(config('swis.date_time_format')) : Carbon::parse($data['start_date'])->format('Y-m-d h:i:s');

//            unset($data['id']);
            $insertData = [];

//            DB::table($rTable)->where('code', $data['code'])->update(['show_status' => 2]);

            $data = array_filter($data, function ($key) use ($incorrectFields) {
                return (!in_array($key, $incorrectFields));
            }, ARRAY_FILTER_USE_KEY);

            if (isset($data['code']) && $data['code']) {

                $previousRowInfo = false;

                // ========================== Inactivate previous rows when the codes same    start =========================== //

                $codeCheckUnique = DB::table($rTable)->where('code', $data['code'])->where('show_status', '=', ReferenceTable::STATUS_ACTIVE)->get();

                if (!is_null($codeCheckUnique)) {
                    foreach ($codeCheckUnique as $codeCheck) {
                        $previousRowInfo = $this->manager->inactivatePreviousRowInfo($codeCheck, $referenceTable->table_name);
                    }
                }

                // ========================== Inactivate previous row when the codes same     end =========================== //

                $lastItem = DB::table($rTable)->where('code', $data['code'])->orderBy('start_date', 'desc')->first();

                if (!is_null($lastItem)) {
                    DB::table($rTable)->where('id', $lastItem->id)->update(['end_date' => $now]);
                }

                $insertData = $this->generateInsertArray($data, $languages, $languagesFields);
                $insertData['id'] = $refId;
                $this->manager->store($insertData);

                if ($previousRowInfo) {

                    $oldData = $this->manager->generateNotesData($rTable, $previousRowInfo[0]);

                    $refCreatedTable = DB::table($rTable)->select('id', 'code')->where('code', $data['code'])->where('show_status', ReferenceTable::STATUS_ACTIVE)->first();

                    if (!is_null($refCreatedTable)) {
                        $newData = $this->manager->generateNotesData($rTable, $refCreatedTable->id);

                        $this->notesManager->store('edit', $refId, $refCreatedTable->id, $refCreatedTable->code, $oldData, $newData);
                    }

                }
            }
        }
        return [
            'success' => true,
            'errors' => ""
        ];
    }

    /**
     * Function to get Not Correct Fields
     *
     * @param $importData
     * @param $rTable
     * @param $now
     * @return string
     */
    private function getNotCorrectFields($importData, $rTable, $now)
    {
        $notCorrectItems = '';
        foreach ($importData as $data) {
            $lastItem = DB::table($rTable)
                ->select('id', 'end_date')
                ->where('code', $data['code'])
                ->where(function ($query) use ($now) {
                    $query->orWhere('end_date', '>', $now)->orWhereNull('end_date');
                })
                ->first();

            $startDate = is_object($data['start_date']) ? $data['start_date']->format('Y-m-d 20:i:s') : Carbon::parse($data['start_date'])->format('Y-m-d 20:i:s');

            if (isset($data['start_date'])) {
                if ($startDate < $now) {
                    $notCorrectItems .= "`{$data['code']}` Code start date less then now <br />";
                } elseif (!is_null($lastItem)) {
                    if ($data['start_date'] <= $lastItem->end_date) {
                        $notCorrectItems = "`{$data['code']}` start date less then end date of {$lastItem->id}, {$lastItem->end_date} <br />";
                    }
                    DB::table($rTable)->where('id', $lastItem->id)->update(['end_date' => $now]);
                }
            }
        }

        return $notCorrectItems;
    }

    /**
     * Function to generate insert array
     *
     * @param $data
     * @param $languages
     * @param $languagesFields
     * @return array
     */
    private function generateInsertArray($data, $languages, $languagesFields)
    {
        foreach ($data as $field => $value) {
            $islangField = false;
            foreach ($languagesFields as $key => $langField) {
                if (strpos($field, $langField) !== false) {
                    $prefix = substr($field, -3);
                    if (array_key_exists($prefix, $languages)) {
                        if (!isset($insertData['ml'][$languages[$prefix]['id']][$key])) {
                            $insertData['ml'][$languages[$prefix]['id']][$key] = ($value === "NULL" || $value === "") ? '' : $value;
                        }
                    }
                    $islangField = true;

                    if (array_key_exists($field, $insertData)) {
                        unset($insertData[$field]);
                    }
                } else {
                    if (!$islangField) {
                        $insertData[$field] = ($value === "NULL" || $value === "") ? null : $value;
                    }
                }
            }
        }

        $insertData['show_status'] = '1';

        return $insertData;
    }

    private function getExportFields($getColumns, $activeLangs)
    {
        $exportFields = [];
        $staticFields = [
            'id' => [
                'type' => 'int',
                'parameters' => []
            ]
        ];

        $languages = [];
        foreach ($getColumns as $column) {
            $typeParameterArray = [
                'type' => $column->type,
                'parameters' => $column->parameters
            ];
            if ($column->has_ml) {
                foreach ($activeLangs as $language) {
                    $languages[$language->id][$column->field_name] = $column->field_name . '_' . $language->code;
                    $exportFields[$column->field_name . '_' . $language->code] = $typeParameterArray;
                }
            } else {
                $exportFields[$column->field_name] = $typeParameterArray;
            }

            if ($column->type === 'select') {
                foreach ($activeLangs as $language) {
                    $exportFields[$column->field_name . '_label_' . $language->code] = array_merge($typeParameterArray, ['relation_column' => $column->field_name]);
                }
            }
        }

        $exportFields = array_merge($staticFields, $exportFields);

        return [
            'exportFields' => $exportFields,
            'languages' => $languages
        ];
    }
}