<?php

namespace App\Models\ReferenceTableForm;

use App\Http\Controllers\Core\DataTableSearch;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\ReferenceTable\ReferenceTableAgencies;
use App\Models\ReferenceTable\ReferenceTableStructure;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Schema;

/**
 * Class ReferenceTableFormSearch
 * @package App\Models\ReferenceTableForm
 */
class ReferenceTableFormSearch extends DataTableSearch
{
    /**
     * @var mixed
     */
    private $referenceTable;

    /**
     * List of ref tables where relation must be build with code
     *
     * @return mixed
     */
    protected $exceptionList = [48];

    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        $data = $query->get();

        foreach ($this->refTable->structure as $structure) {
            foreach ($data as $key => $value) {

                if (($structure->type == ReferenceTableStructure::COLUMN_TYPE_DATE || $structure->type == ReferenceTableStructure::COLUMN_TYPE_DATETIME) && !empty($value->{$structure->field_name})) {
                    $value->{$structure->field_name} = formattedDate($value->{$structure->field_name}, $structure->type == ReferenceTableStructure::COLUMN_TYPE_DATETIME);
                }

                //
                if ($structure->type == ReferenceTableStructure::COLUMN_TYPE_AUTOCOMPLETE && trim($value->{$structure->field_name}) == '()') {
                    $value->{$structure->field_name} = '';
                }
            }
        }

        return $data;
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $referenceTableName = "reference_{$this->refTable->table_name}";

        $orderByType = ($this->orderByType == 'asc') ? 'asc' : 'desc';

        if ($this->orderByCol == 'show_status') {
            $this->orderByCol = $referenceTableName . '.show_status';
        }

        $query->orderBy($this->orderByCol, $orderByType);
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $this->refTable = ReferenceTable::select('id', 'table_name')->where('id', Route::current()->id)->firstOrFail();

        $referenceTableName = "reference_{$this->refTable->table_name}";
        $referenceTableMlName = "reference_{$this->refTable->table_name}_ml";

        $query = DB::table($referenceTableName)->select("{$referenceTableName}.*", "{$referenceTableMlName}.*", "{$referenceTableName}.show_status as show_status")
            ->leftJoin($referenceTableMlName, function ($q) use ($referenceTableName, $referenceTableMlName) {
                $q->on("{$referenceTableName}.id", '=', "{$referenceTableMlName}.{$referenceTableName}_id")->where("{$referenceTableMlName}.lng_id", '=', cLng('id'));
            })->when(isset($this->searchData['id']), function ($q) use ($referenceTableName) {
                $q->where(DB::raw($referenceTableName . ".id::varchar"), $this->searchData['id']);
            })->when(!empty($this->searchData['show_status']) && $this->searchData['show_status'] != 'all', function ($q) use ($referenceTableName) {
                $q->where($referenceTableName . '.show_status', $this->searchData['show_status']);
            }, function ($q) use ($referenceTableName) {
                $q->where("{$referenceTableName}.show_status", '!=', ReferenceTable::STATUS_DELETED);
            })->when($referenceTableName == ReferenceTable::REFERENCE_AGENCY && !Auth::user()->isSwAdmin(), function ($q) use ($referenceTableName) {
                $q->where("{$referenceTableName}.code", Auth::user()->companyTaxId());
            });

        foreach ($this->refTable->structureSearchField as $structure) {

            switch ($structure->type) {

                // Autocomplete, Select
                case ReferenceTableStructure::COLUMN_TYPE_SELECT:
                case ReferenceTableStructure::COLUMN_TYPE_AUTOCOMPLETE:

                    if (isset($structure->parameters['source'])) {

                        $relatedRefTable = ReferenceTable::select('id', 'table_name')->find($structure->parameters['source']);

                        $refRelTableName = $relatedRefTable->table_name;
                        $refRelTableNameAlias = "{$relatedRefTable->table_name}_{$structure->field_name}";
                        $refRelTableNameMLAlias = "{$relatedRefTable->table_name}_{$structure->field_name}_ml";

                        // Autocomplete
                        if ($structure->type == ReferenceTableStructure::COLUMN_TYPE_AUTOCOMPLETE) {

                            if (isset($structure->parameters['fields'])) {

                                $relatedRefFields = ReferenceTableStructure::select('has_ml', 'field_name')->whereIn('id', $structure->parameters['fields'])->whereNotIn('field_name', [ReferenceTable::REFERENCE_DEFAULT_FIELD_CODE])->get();

                                if ($relatedRefFields->count()) {

                                    $concat = "CONCAT('(', {$refRelTableNameAlias}.code, ') ' ,";
                                    $countOfStructure = $relatedRefFields->count() - 1;

                                    foreach ($relatedRefFields as $key => $relatedRefField) {

                                        $prefix = ($countOfStructure == $key) ? '' : ", ', ',";
                                        $tmpRelTableName = ($relatedRefField->has_ml) ? $refRelTableNameMLAlias : $refRelTableNameAlias;

                                        $concat .= "{$tmpRelTableName}.{$relatedRefField->field_name}{$prefix}";
                                    }

                                    $concat .= ") as {$structure->field_name}";

                                    //
                                    $query->addSelect(DB::raw($concat));
                                }
                            }

                            // Search By autocomplete fields
                            if (isset($this->searchData[$structure->field_name])) {

                                $searchAutocomplete = [];
                                if (!empty($structure->parameters['fields'])) {
                                    $sourceRef = getReferenceAllRowsById($relatedRefTable->table_name, $this->searchData[$structure->field_name]);

                                    if ($sourceRef->count()) {
                                        $searchAutocomplete = $sourceRef->pluck('id')->all();
                                    }
                                }

                                if (!count($searchAutocomplete)) {
                                    $searchAutocomplete[] = $this->searchData[$structure->field_name];
                                }

                                $query->whereIn("{$referenceTableName}.{$structure->field_name}", $searchAutocomplete);
                            }

                        }

                        // Select
                        if ($structure->type == ReferenceTableStructure::COLUMN_TYPE_SELECT) {

                            $query->addSelect(DB::raw("CONCAT('(', {$refRelTableNameAlias}.code, ') ', {$refRelTableNameMLAlias}.name) as {$structure->field_name}"));

                            if (isset($this->searchData[$structure->field_name])) {
                                $relatedRefTableBySelect = ReferenceTable::where('id', $structure->parameters['source'])->pluck('table_name')->first();

                                if (!is_null($relatedRefTableBySelect)) {
                                    $searchBySelect = DB::table("reference_{$relatedRefTableBySelect}_ml")->where("name", 'ILIKE', '%' . strtolower(trim($this->searchData[$structure->field_name])) . '%')->pluck("reference_{$relatedRefTableBySelect}_id")->unique()->all();

                                    $query->whereIn($referenceTableName . '.' . $structure->field_name, $searchBySelect);
                                }
                            }
                        }

                        $query->leftJoin("reference_{$refRelTableName} as {$refRelTableNameAlias}", function ($query) use ($refRelTableNameAlias, $referenceTableName, $structure) {
                            $query->on(DB::raw("{$refRelTableNameAlias}.id::varchar"), "=", DB::raw("{$referenceTableName}.{$structure->field_name}::varchar"));
                        });

                        $query->leftJoin("reference_{$refRelTableName}_ml as {$refRelTableNameMLAlias}", function ($query) use ($refRelTableNameAlias, $refRelTableName, $refRelTableNameMLAlias) {
                            $query->on(DB::raw("{$refRelTableNameAlias}.id::varchar"), "=", DB::raw("{$refRelTableNameMLAlias}.reference_{$refRelTableName}_id::varchar"))->where("{$refRelTableNameMLAlias}.lng_id", '=', cLng('id'));
                        });

                        $this->orderByCol = ($this->orderByCol == $structure->field_name) ? "{$referenceTableName}.{$structure->field_name}" : $this->orderByCol;
                    }

                    break;

                // Date, Datetime
                case ReferenceTableStructure::COLUMN_TYPE_DATE:
                case ReferenceTableStructure::COLUMN_TYPE_DATETIME:

                    $startDate = $this->searchData["{$structure->field_name}_start"] ?? '';
                    $endDate = $this->searchData["{$structure->field_name}_end"] ?? '';

                    if ($startDate) {

                        if ($structure->type == ReferenceTableStructure::COLUMN_TYPE_DATETIME) {
                            $startDate = Carbon::createFromFormat(config('swis.date_time_format_front_without_second'), $startDate);
                        }

                        $query->where("{$referenceTableName}.{$structure->field_name}", '>=', $startDate);
                    }

                    if ($endDate) {

                        if ($structure->type == ReferenceTableStructure::COLUMN_TYPE_DATETIME) {
                            $endDate = Carbon::createFromFormat(config('swis.date_time_format_front_without_second'), $endDate);
                        }

                        $query->where("{$referenceTableName}.{$structure->field_name}", '<=', $endDate);
                    }

                    break;

                // Decimal, Number
                case ReferenceTableStructure::COLUMN_TYPE_DECIMAL:
                case ReferenceTableStructure::COLUMN_TYPE_NUMBER:

                    if (isset($this->searchData["{$structure->field_name}_start"])) {
                        $query->where(DB::raw("{$referenceTableName}.{$structure->field_name}::float"), '>=', $this->searchData["{$structure->field_name}_start"]);
                    }

                    if (isset($this->searchData["{$structure->field_name}_end"])) {
                        $query->where(DB::raw("{$referenceTableName}.{$structure->field_name}::float"), '<=', $this->searchData["{$structure->field_name}_end"]);
                    }

                    break;

                // Text
                default:

                    if (isset($this->searchData[$structure->field_name])) {

                        $tmpTableName = $structure->has_ml ? $referenceTableMlName : $referenceTableName;

                        $query->where(DB::raw("{$tmpTableName}.{$structure->field_name}::varchar"), 'ILIKE', '%' . strtolower(trim($this->searchData[$structure->field_name])) . '%');
                    }
            }
        }

        // ------
        if (Auth::user()->isAgency() && Schema::hasColumns($referenceTableName, ['company_tax_id', 'user_id'])) {

            $refAgency = ReferenceTableAgencies::select('show_only_self')->where(['reference_table_id' => $this->refTable->id, 'agency_id' => Auth::user()->getCurrentUserRole()->agency_id])->first();

            if (!is_null($refAgency) && $refAgency->show_only_self) {
                $query->where("{$referenceTableName}.company_tax_id", '=', Auth::user()->companyTaxId());
            }

            if (!Auth::user()->userShowOthersData()) {
                $query->where("{$referenceTableName}.user_id", Auth::id());
            }
        }

        // ------
        if (!Auth::user()->isSwAdmin()) {
            $hasAgencyColumnGetName = ReferenceTable::hasAgencyColumnGetName($this->refTable->id);

            if ($hasAgencyColumnGetName) {
                $query->whereIn("{$referenceTableName}." . $hasAgencyColumnGetName, ReferenceTable::getUserReferenceAgencyIds());
            }
        }

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}
