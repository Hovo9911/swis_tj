<?php

namespace App\Models\ConstructorStates;

use App\Models\BaseModel;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorMapping\ConstructorMapping;
use App\Models\Menu\Menu;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplication\SingleApplication;
use App\Models\User\User;
use App\Models\UserRoles\UserRoles;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Auth;

/**
 * Class ConstructorStates
 * @package App\Models\ConstructorStates
 */
class ConstructorStates extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'constructor_states';

    /**
     * @var string
     */
    const REQUEST_STATE_TYPE = 'request';
    const LABORATORY_STATE_TYPE = 'laboratory';
    const END_STATE_TYPE = 'end';
    const END_STATE_SIMPLE = 'simple';
    const CANCELED_STATE_TYPE = 'canceled';
    const START_STATE_TYPE = 'start';

    /**
     * @var array
     */
    const STATE_TYPES = [
        self::  START_STATE_TYPE,
        self::  END_STATE_TYPE,
        self::  END_STATE_SIMPLE,
        self::  REQUEST_STATE_TYPE,
        self::  CANCELED_STATE_TYPE,
    ];

    /**
     * @var int
     */
    const END_STATE_APPROVED = 1;
    const END_STATE_NOT_APPROVED = 0;

    /**
     * @var bool
     */
    protected $hasAdminInfo = true;

    /**
     * @var null
     */
    protected static $constructorStateAllData = null;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'constructor_document_id',
        'state_id',
        'state_type',
        'state_approved_status',
        'show_status',
        'is_countable'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'created_admin_id',
        'created_admin_ip',
        'updated_at',
        'updated_admin_id',
        'updated_admin_ip',
        'deleted_admin_id',
        'deleted_admin_ip'
    ];

    /**
     * Function to relate with ConstructorStatesMl all
     *
     * @return HasMany
     */
    public function ml()
    {
        return $this->hasMany(ConstructorStatesMl::class, 'constructor_states_id', 'id');
    }

    /**
     * Function to relate with ConstructorStatesMl and get active
     *
     * @return HasOne
     */
    public function current()
    {
        return $this->hasOne(ConstructorStatesMl::class, 'constructor_states_id', 'id')->notDeleted()->where('lng_id', cLng('id'))->first();
    }

    /**
     * Function to relate with ConstructorStatesMl and get active
     *
     * @return HasOne
     */
    public function currentMl()
    {
        return $this->hasOne(ConstructorStatesMl::class, 'constructor_states_id', 'id')->notDeleted()->where('lng_id', cLng('id'));
    }

    /**
     * Function to check auth user has role
     *
     * @param $query
     * @return mixed
     */
    public function scopeConstructorStatesOwner($query)
    {
        $agencyDocumentIds = ConstructorDocument::getConstructorDocumentForAgency()->pluck('id')->toArray();

        return $query->whereIn($this->getTable() . '.constructor_document_id', $agencyDocumentIds ?? -1);
    }

    /**
     * Function to get end approved states
     *
     * @param $query
     * @return mixed
     */
    public function scopeEndApprovedStates($query)
    {
        return $query->where($this->getTable() . '.state_type', self::END_STATE_TYPE)->where($this->getTable() . '.state_approved_status', self::END_STATE_APPROVED);
    }

    /**
     * Function to get all data
     *
     * @return null|ConstructorStates
     */
    public static function allData()
    {
        if (self::$constructorStateAllData === null) {
            self::setAllData();
        }

        return self::$constructorStateAllData;
    }

    /**
     * Function to set constructor_action model all
     *
     * @return void
     */
    public static function setAllData()
    {
        self::$constructorStateAllData = self::select('id', 'state_name')->joinMl()->get()->keyBy('id');
    }

    /**
     * Function to get Users By State Roles
     *
     * @param bool $startState
     * @param $documentId
     * @param bool $getHeads
     * @return mixed
     */
    public static function getUsersByStateRoles($startState, $documentId, $getHeads = true)
    {
        $needRoles = ConstructorMapping::where(['document_id' => $documentId])->active();

        if ($startState) {
            $needRoles->where(['start_state' => $startState]);
        }

        $needRoles = $needRoles->pluck('role')->unique()->all();

        $userIds = UserRoles::activeRole()
            ->where('company_tax_id', Auth::user()->companyTaxId())
            ->whereIn('role_id', $needRoles)
            ->pluck('user_id')->unique()->all();

        $userIdsFromRoleGroups = UserRoles::activeRole()
            ->join('roles_group_roles', 'user_roles.role_group_id', '=', 'roles_group_roles.roles_group_id')
            ->where('user_roles.company_tax_id', Auth::user()->companyTaxId())
            ->whereIn('roles_group_roles.role_id', $needRoles)
            ->whereNotNull('user_roles.role_group_id')
            ->pluck('user_id')
            ->unique()
            ->all();

        $userAllIds = array_merge($userIds, $userIdsFromRoleGroups);

        if ($getHeads) {
            $companyHeadOfCompanies = User::getCompanyHeads(Auth::user()->companyTaxId());

            $userHeadIds = $companyHeadOfCompanies->pluck('id')->all();
            $userAllIds = array_unique(array_merge($userAllIds, $userHeadIds));
        }

        return User::select('id', 'first_name', 'last_name')->whereIn('id', $userAllIds)->orderBy('first_name', 'asc')->orderBy('last_name', 'asc')->get();
    }

    /**
     * Function to get users in next state and check attributes
     *
     * @param $endState
     * @param $documentId
     * @param $subDivisionId
     * @return mixed
     */
    public static function getUsersByStateRolesAndAttributes($endState, $documentId, $subDivisionId)
    {
        $companyTaxId = Auth::user()->companyTaxId();
        $userDocumentsMenu = Menu::getMenuIdByName(Menu::USER_DOCUMENTS_MENU_NAME);
        $users = self::getUsersByStateRoles($endState, $documentId, true);
        $subDivision = getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME, $subDivisionId, false, ['code', 'reference_agency_subdivisions.show_status']);

        if (isset($subDivisionId)) {
            $subDivisionIds = getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME, false, false, ['id'], 'get', false, false, ['code' => $subDivision->code], true)->pluck('id')->all();

            $getUsersWithAttribute = UserRoles::select('id', 'user_id')
                ->where('menu_id', $userDocumentsMenu)
                ->where('company_tax_id', $companyTaxId)
                ->where('attribute_field_id', SingleApplication::SAF_AGENCY_SUBDIVISION_ID)
                ->whereIn('attribute_value', $subDivisionIds)
                ->activeRole()->get()->pluck('user_id')->all();
            $getUsersWithAttribute = array_unique($getUsersWithAttribute);

            $getUsersWithAllPermissions = UserRoles::select('id', 'user_id')
                ->where('menu_id', $userDocumentsMenu)
                ->where('company_tax_id', $companyTaxId)
                ->whereNull('attribute_field_id')
                ->whereNull('attribute_value')
                ->activeRole()->get()->pluck('user_id')->all();
            $getUsersWithAllPermissions = array_unique($getUsersWithAllPermissions);

            $applicantCompanyHeads = User::getCompanyHeads($companyTaxId, false)->pluck('id')->toArray();

            $users = $users->filter(function ($user, $key) use ($getUsersWithAttribute, $getUsersWithAllPermissions, $applicantCompanyHeads) {
                return in_array($user->id, $getUsersWithAttribute) || in_array($user->id, $getUsersWithAllPermissions) || in_array($user->id, $applicantCompanyHeads);
            });
        }

        return $users;
    }

    /**
     * Function to get users with mapping role
     *
     * @param $startState
     * @param $documentId
     * @return mixed
     */
    public static function getUsersByMapping($startState, $documentId)
    {
        $needRoles = ConstructorMapping::where(['document_id' => $documentId, 'start_state' => $startState])->active()->pluck('role')->unique()->all();

        $userIds = UserRoles::whereIn('role_id', $needRoles)
            ->activeRole()
            ->pluck('user_id')->unique()->all();

        $userIdsFromRoleGroups = UserRoles::activeRole()
            ->join('roles_group_roles', 'user_roles.role_group_id', '=', 'roles_group_roles.roles_group_id')
            ->whereIn('roles_group_roles.role_id', $needRoles)
            ->whereNotNull('user_roles.role_group_id')
            ->pluck('user_id')
            ->unique()
            ->all();

        $userAllIds = array_merge($userIds, $userIdsFromRoleGroups);

        return User::whereIn('id', $userAllIds)->orderBy('first_name', 'asc')->orderBy('last_name', 'asc')->get();
    }

    /**
     * Function to get State Name
     *
     * @param $id
     * @return string
     */
    public static function getStateName($id)
    {
        $allData = self::allData();

        $stateName = '';
        if(isset($allData[$id])){
            $stateName =  $allData[$id]->state_name;
        }

        return $stateName;
    }

    /**
     * Function to check state status
     *
     * @param $stateId
     * @param int $type
     * @return bool|null
     */
    public static function checkEndStateStatus($stateId, $type = self::END_STATE_APPROVED)
    {
        if (!is_null($stateId)) {
            $constructorStateInfo = ConstructorStates::select('state_type', 'state_approved_status')->where('id', $stateId)->first();
            if (!is_null($constructorStateInfo)) {

                if ($type == self::END_STATE_APPROVED && ($constructorStateInfo->state_type == ConstructorStates::END_STATE_TYPE && $constructorStateInfo->state_approved_status == ConstructorStates::END_STATE_APPROVED)) {
                    return true;
                }

                if ($type == self::END_STATE_NOT_APPROVED && (($constructorStateInfo->state_type == ConstructorStates::END_STATE_TYPE && $constructorStateInfo->state_approved_status == ConstructorStates::END_STATE_NOT_APPROVED) || $constructorStateInfo->state_type == ConstructorStates::CANCELED_STATE_TYPE)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Function to get end approved states ids by constructor document id
     *
     * @param $constructorDocumentId
     * @return array
     */
    public static function getEndApprovedStateIds($constructorDocumentId)
    {
        return self::where('constructor_document_id', $constructorDocumentId)->endApprovedStates()->pluck('id')->all();
    }
}
