<?php

namespace App\Models\ConstructorStates;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

/**
 * Class ConstructorStatesMl
 * @package App\Models\ConstructorStates
 */
class ConstructorStatesMl extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['constructor_states_id', 'lng_id'];

    /**
     * @var string
     */
    public $incrementing = 'false';

    /**
     * @var string
     */
    public $table = 'constructor_states_ml';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'constructor_states_id',
        'lng_id',
        'state_name',
        'description',
    ];
}
