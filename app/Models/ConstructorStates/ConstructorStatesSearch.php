<?php

namespace App\Models\ConstructorStates;

use App\Http\Controllers\Core\DataTableSearch;
use Illuminate\Support\Facades\DB;

/**
 * Class ConstructorStatesSearch
 * @package App\Models\ConstructorStates
 */
class ConstructorStatesSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        return $query->get();
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($this->orderByCol, $this->orderByType ?? 'desc');
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $query = ConstructorStates::select('*')
            ->joinMl()
            ->where('constructor_document_id', $this->searchData['constructor_document_id'])
            ->notDeleted()
            ->constructorStatesOwner();

        if (isset($this->searchData['id'])) {
            $query->where(DB::raw("constructor_states.id::varchar"), $this->searchData['id']);
        }

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}
