<?php

namespace App\Models\ConstructorStates;

use App\Models\BaseMlManager;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class ConstructorStatesManager
 * @package App\Models\ConstructorStates
 */
class ConstructorStatesManager
{
    use BaseMlManager;

    /**
     * Function to Store data to DB
     *
     * @param array $data
     * @return ConstructorStates
     */
    public function store(array $data)
    {
        $data['user_id'] = Auth::id();
        $data['state_id'] = generateID(10);
        $data['is_countable'] = $data['is_countable'] ?? false;
        $data['state_approved_status'] = $data['state_approved_status'] ?? 0;
        $data['state_type'] != ConstructorStates::END_STATE_TYPE ? $data['state_approved_status'] = 0 : '';

        $constructorStates = new ConstructorStates($data);
        $constructorStatesMl = new ConstructorStatesMl();

        return DB::transaction(function () use ($data, $constructorStates, $constructorStatesMl) {
            $constructorStates->save();
            $this->storeMl($data['ml'], $constructorStates, $constructorStatesMl);

            return $constructorStates;
        });
    }

    /**
     * Function to Update data
     *
     * @param array $data
     * @param int $id
     */
    public function update(array $data, int $id)
    {
        $data['is_countable'] = $data['is_countable'] ?? false;
        $data['state_approved_status'] = $data['state_approved_status'] ?? 0;
        $data['state_type'] != ConstructorStates::END_STATE_TYPE ? $data['state_approved_status'] = 0 : '';

        $constructorStates = ConstructorStates::where('id', $id)->constructorStatesOwner()->firstOrFail();
        $constructorStatesMl = new ConstructorStatesMl();

        DB::transaction(function () use ($data, $constructorStates, $constructorStatesMl) {
            $constructorStates->update($data);
            $this->updateMl($data['ml'], 'constructor_states_id', $constructorStates, $constructorStatesMl);
        });
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @param $id2
     * @return array
     */
    public function getLogData($id, $id2 = null)
    {
        $data = ConstructorStates::where('id', $id)->first()->toArray();
        $data['mls'] = ConstructorStatesMl::where('constructor_states_id', $id)->get()->toArray();

        return $data;
    }
}
