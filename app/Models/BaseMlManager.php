<?php

namespace App\Models;

/**
 * Class BaseModel
 * @package App\Models
 */
trait BaseMlManager
{
    /**
     * Function to store ml data
     *
     * @param $mlsData
     * @param $model
     * @param $modelMl
     */
    protected function storeMl($mlsData, $model, $modelMl)
    {
        $ml = [];
        foreach ($mlsData as $lngId => $mlData) {
            $mlData['lng_id'] = $lngId;
            $mlData['show_status'] = '1';
            $ml[] = new $modelMl($mlData);
        }
        $model->ml()->saveMany($ml);
    }

    /**
     * Function to update ml data
     *
     * @param $mlsData
     * @param $foreign_key
     * @param $model
     * @param $modelMl
     */
    protected function updateMl($mlsData, $foreign_key, $model, $modelMl)
    {
        if(isset($mlsData[1]) && isset($mlsData[1]['show_status'])){
            $modelMl->where($foreign_key, $model->id)->update(['show_status' => $model::STATUS_INACTIVE]);
        }

        $newMl = [];
        foreach ($mlsData as $lngId => $mlData) {
            $ml = $modelMl::where($foreign_key, $model->id)->where('lng_id', $lngId)->first();
            if ($ml == null) {
                $mlData['lng_id'] = $lngId;
                $newMl[] = new $modelMl($mlData);
            } else {
                $modelMl::where($foreign_key, $model->id)->where('lng_id', $lngId)->update($mlData);
            }
        }

        if (!empty($newMl)) {
            $model->ml()->saveMany($newMl);
        }
    }
}