<?php

namespace App\Models\TransportApplicationInvoices;

use App\Models\BaseModel;

/**
 * Class TransportApplicationInvoices
 * @package App\Models\TransportApplicationInvoices
 */
class TransportApplicationInvoices extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'transport_application_invoices';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'transport_application_id',
        'user_id',
        'company_tax_id',
        'tracking_number',
        'payment_amount',
        'transaction_number',
    ];

    /**
     * Function to get payment amount
     *
     * @param $value
     * @return float|int
     */
    public function getPaymentAmountAttribute($value)
    {
        if ($value) {
            return round($value, 2);
        }

        return $value;
    }
}