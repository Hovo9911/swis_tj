<?php

namespace App\Models\DocumentsNotes;

use App\Models\BaseModel;
use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

/**
 * Class DocumentsNotes
 * @package App\Models\DocumentsNotes
 */
class DocumentsNotes extends BaseModel
{
    /**
     * @var string
     */
    const DOCUMENT_NOTE_TRANS_KEY_SAVE_NOTE = 'swis.document.notes.save_note';
    const DOCUMENT_NOTE_TRANS_KEY_SAVE = 'swis.document.notes.save';

    /**
     * @var array
     */
    const DOCUMENT_NOTE_TRANS_KEY_STATUSES = [
        'DOCUMENT_NOTE_TRANS_KEY_CREATED' => 'swis.document.notes.created',
        'DOCUMENT_NOTE_TRANS_KEY_REGISTERED' => 'swis.document.notes.registered',
        'DOCUMENT_NOTE_TRANS_KEY_CONFIRMED' => 'swis.document.notes.confirmed',
        'DOCUMENT_NOTE_TRANS_KEY_REJECTED' => 'swis.document.notes.rejected',
        'DOCUMENT_NOTE_TRANS_KEY_SUSPENDED' => 'swis.document.notes.suspended'
    ];

    /**
     * @var string (In trans Variables list)
     */
    const NOTE_VARIABLE_NOTE = 'NOTE';
    const NOTE_VARIABLE_USER_NAME = 'USER_NAME';
    const NOTE_VARIABLE_STATUS_NAME = 'ACTION_NAME';

    /**
     * @var string
     */
    public $table = 'documents_notes';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'version',
        'user_id',
        'document_uuid',
        'trans_key',
        'note',
        'old_data',
        'new_data'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'old_data' => 'array',
        'new_data' => 'array'
    ];

    /**
     * Funciton to get created_at value
     *
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format(config('swis.date_time_format_front'));
    }

    /**
     * Function to store documents notes
     *
     * @param array $data
     */
    public static function storeNotes(array $data)
    {
        $defaultData = [
            'version' => 1,
            'user_id' => Auth::id(),
            'trans_key' => '',
            'note' => null,
            'old_data' => null,
            'new_data' => null,
        ];

        $insertData = array_merge($defaultData, $data);

        self::create($insertData);
    }

    /**
     * Function to get notes
     *
     * @param $uuId
     * @return DocumentsNotes
     */
    public static function getNotes($uuId)
    {
        $notes = self::where('document_uuid', $uuId)
            ->orderByDesc()
            ->get();

        return (new self)->generateWithTrans($notes);
    }

    /**
     * Function to generate notes array by trans key
     *
     * @param $notes
     * @return DocumentsNotes
     */
    public function generateWithTrans($notes)
    {
        foreach ($notes as &$note) {
            $note->note = self::getVariablesValues($note, $note->trans_key);
        }

        return $notes;
    }

    /**
     * Function to change variable name with value
     *
     * @param $note
     * @param $trans
     * @return array
     */
    private static function getVariablesValues($note, $trans)
    {
        $transValue = trans($trans);
        $dataVariables = getStringsBetween($transValue, '{', '}');
        $noteCreatedUserName = User::getUserNameByID($note->user_id);

        $dataVariablesValues = [
            self::NOTE_VARIABLE_USER_NAME => $noteCreatedUserName
        ];

        foreach ($dataVariables as $dataVariable) {
            switch ($dataVariable) {
                case self::NOTE_VARIABLE_NOTE:
                    $dataVariablesValues[self::NOTE_VARIABLE_NOTE] = $note->note ?? '-';
                    break;
            }
        }

        foreach ($dataVariablesValues as $variableKey => $variablesValue) {
            $variableKeyWith = "{{$variableKey}}";
            if (strpos($transValue, $variableKeyWith) !== false) {

                if ($variableKey == self::NOTE_VARIABLE_NOTE) {
                    $variablesValue = "<b>$variablesValue</b>";
                }

                $transValue = str_replace($variableKeyWith, trans($variablesValue), $transValue);
            }
        }

        return $transValue;
    }
}
