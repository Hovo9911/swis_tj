<?php

namespace App\Models\RolesGroupRolesAttributes;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

/**
 * Class RolesGroupRoles
 * @package App\Models\RolesGroupRoles
 */
class RolesGroupRolesAttributes extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['roles_group_id', 'role_id'];

    /**
     * @var string
     */
    public $incrementing = 'false';

    /**
     * @var string
     */
    public $table = 'roles_group_roles_attributes';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'roles_group_id',
        'role_id',
        'attribute_id',
        'attribute_type',
    ];


}
