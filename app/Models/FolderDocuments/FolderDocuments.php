<?php

namespace App\Models\FolderDocuments;

use App\Models\BaseModel;
use App\Models\Document\Document;
use App\Models\Folder\Folder;
use App\Models\Traits\HasCompositePrimaryKey;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class FolderDocuments
 * @package App\Models\FolderDocuments
 */
class FolderDocuments extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['folder_id', 'document_id'];

    /**
     * @var string
     */
    public $incrementing = 'false';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    public $table = 'folder_documents';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'folder_id',
        'document_id',
        'user_id'
    ];

    /**
     * Function to relate with Folder
     *
     * @return HasOne
     */
    public function folder()
    {
        return $this->hasOne(Folder::class, 'id', 'folder_id');
    }

    /**
     * Function to relate with User
     *
     * @return HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * Function to relate with Document
     *
     * @return HasOne
     */
    public function document()
    {
        return $this->hasOne(Document::class, 'id', 'document_id');
    }
}
