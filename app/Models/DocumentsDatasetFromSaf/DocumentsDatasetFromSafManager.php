<?php

namespace App\Models\DocumentsDatasetFromSaf;

use App\Models\BaseMlManager;
use Illuminate\Support\Facades\DB;

/**
 * Class DocumentsDatasetFromSafManager
 * @package App\Models\DocumentsDatasetFromSaf
 */
class DocumentsDatasetFromSafManager
{
    use BaseMlManager;

    /**
     * Function to save data
     *
     * @param array $data
     */
    public function store(array $data)
    {
        DB::transaction(function () use ($data) {

            $documentsDatasetFromSafML = new DocumentsDatasetFromSafML();
            $documentDatasetFromSafDeleteIds = DocumentsDatasetFromSaf::where('document_id', $data['documentID'])->pluck('id')->all();

            DocumentsDatasetFromSaf::where('document_id', $data['documentID'])->delete();
            DocumentsDatasetFromSafML::whereIn('documents_dataset_from_saf_Id',$documentDatasetFromSafDeleteIds)->delete();

            $insertData = $this->getInsertField($data);

            if (isset($insertData['data'])) {
                foreach ($insertData['data'] as $item) {
                    $documentsDatasetFromSaf = new DocumentsDatasetFromSaf($item);
                    $documentsDatasetFromSaf->save();

                    $this->storeMl($insertData['ml'][$item['saf_field_id']], $documentsDatasetFromSaf, $documentsDatasetFromSafML);
                }
            }
        });
    }

    /**
     * Function to get field for insert
     *
     * @param $data
     * @return array
     */
    protected function getInsertField($data)
    {
        $insertData = [];

        if (isset($data['params']) && count($data['params']) > 0) {
            foreach ($data['params'] as $fieldID => $field) {
                if ($this->checkField($field)) continue;

                $insertData['data'][] = [
                    'document_id' => $data['documentID'],
                    'saf_field_id' => $fieldID,
                    'group' => $field['group'],
                    'xml_field_name' => isset($field['xml_field_name']) ? $field['xml_field_name'] : '',
                    'title_trans_key' => isset($field['title_trans_key']) ? $field['title_trans_key'] : '',
                    'is_attribute' => isset($field['is_attribute']),
                    'is_hidden' => isset($field['hide_on_system']),
                    'is_search_param' => isset($field['search_param']),
                    'show_in_search_results' => isset($field['include_search_result'])
                ];

                foreach (activeLanguages() as $lang) {
                    $insertData['ml'][$fieldID][$lang->id] = [
                        'label' => isset($field['field_label']) ? $field['field_label'][$lang->id] : '',
                        'tooltip' => isset($field['tooltip']) ? $field['tooltip'][$lang->id] : ''
                    ];
                }

            }

        }

        return $insertData;
    }

    /**
     * Function to check fields
     *
     * @param $field
     * @return bool
     */
    private function checkField($field)
    {
        $xml_field_name = array_key_exists('xml_field_name', $field);
        $title_trans_key = array_key_exists('title_trans_key', $field);
        $group = array_key_exists('group', $field);

        return (count($field) == 3 && $xml_field_name && $title_trans_key && $group);
    }

//    public function getLogData($action = null)
//    {
//        $logData['data'] = '';
//        if ($action == 'update' || $action == 'edit' || $action == 'view') {
//            $data = Offence::where('offence_reference_number', request()->uuid)->first();
//            $logData['id'] = $data->id;
//            $logData['uuid'] = $data->offence_reference_number;
//        }
//
//        if ($action == 'update') {
//            $logData['data'] = Offence::where('offence_reference_number', request()->uuid)->first();
//        } elseif ($action == 'edit' || $action == 'view') {
//            $logData['data'] = request()->getUri();
//        }
//
//        return $logData;
//    }
}