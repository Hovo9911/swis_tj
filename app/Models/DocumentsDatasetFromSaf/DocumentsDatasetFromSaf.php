<?php

namespace App\Models\DocumentsDatasetFromSaf;

use App\Models\BaseModel;
use App\Models\ConstructorDocument\ConstructorDocument;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Query\Builder;

/**
 * Class DocumentsDatasetFromSaf
 * @package App\Models\DocumentsDatasetFromSaf
 */
class DocumentsDatasetFromSaf extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'documents_dataset_from_saf';

    /**
     * @var array
     */
    protected $fillable = [
        'document_id',
        'saf_field_id',
        'xml_field_name',
        'title_trans_key',
        'is_attribute',
        'is_hidden',
        'is_search_param',
        'is_unique',
        'show_in_search_results',
    ];

    /**
     * Function to relate with DocumentsDatasetFromSafML all
     *
     * @return HasMany
     */
    public function ml()
    {
        return $this->hasMany(DocumentsDatasetFromSafML::class, 'documents_dataset_from_saf_Id', 'id');
    }

    /**
     * Function to relate with DocumentsDatasetFromSafML current
     *
     * @return HasOne
     */
    public function current()
    {
        return $this->hasOne(DocumentsDatasetFromSafML::class, 'documents_dataset_from_saf_Id', 'id')->where('lng_id', cLng('id'));
    }

    /**
     * Function to check auth user has role
     *
     * @param $query
     * @return Builder
     */
    public function scopeConstructorDataSetSafOwner($query)
    {
        $agencyDocumentIds = ConstructorDocument::getConstructorDocumentForAgency()->pluck('id')->toArray();

        return $query->whereIn($this->getTable() . '.document_id', $agencyDocumentIds ?? -1);
    }
}
