<?php

namespace App\Models\DocumentsDatasetFromSaf;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

/**
 * Class DocumentsDatasetFromSafML
 * @package App\Models\DocumentsDatasetFromMdm
 */
class DocumentsDatasetFromSafML extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $primaryKey = ['documents_dataset_from_saf_Id', 'lng_id'];

    /**
     * @var string
     */
    public $table = 'documents_dataset_from_saf_ml';

    /**
     * @var array
     */
    protected $fillable = [
        'documents_dataset_from_saf_Id',
        'lng_id',
        'label',
        'tooltip'
    ];
}
