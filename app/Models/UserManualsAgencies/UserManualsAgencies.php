<?php

namespace App\Models\UserManualsAgencies;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

class UserManualsAgencies extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['user_manual_id', 'company_tax_id'];

    /**
     * @var string
     */
    public $incrementing = 'false';

    /**
     * @var string
     */
    public $table = 'user_manuals_agencies';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'user_manual_id',
        'company_tax_id',
    ];
}
