<?php

namespace App\Models\RoleMenus;

use App\Models\BaseModel;
use App\Models\Menu\Menu;
use App\Models\Role\Role;
use App\Models\Traits\HasCompositePrimaryKey;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class RoleMenus
 * @package App\Models\RoleMenus
 */
class RoleMenus extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['role_id', 'menu_id'];

    /**
     * @var string
     */
    public $incrementing = 'false';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    public $table = 'role_menus';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'role_id',
        'menu_id',
    ];

    /**
     * Function to Menu
     *
     * @return BelongsTo
     */
    public function menu()
    {
        return $this->belongsTo(Menu::class, 'menu_id', 'id');
    }

    /**
     * Function to Role
     *
     * @return BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id', 'id')->whereNotIn('id', [Role::SW_ADMIN, Role::NATURAL_PERSON, Role::HEAD_OF_COMPANY]);
    }
}
