<?php

namespace App\Models\PaymentProviders;

use App\Http\Controllers\Core\DataTableSearch;

/**
 * Class PaymentProvidersSearch
 * @package App\Models\PaymentProviders
 */
class PaymentProvidersSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        return $query->get();
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($this->orderByCol, $this->orderByType ?? 'desc');
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $query = PaymentProviders::select('id', 'name', 'description', 'address', 'show_status')->active();

        if (isset($this->searchData['id'])) {
            $query->where('payment_providers.id', $this->searchData['id']);
        }

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}
