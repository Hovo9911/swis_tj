<?php

namespace App\Models\PaymentProviders;

use Illuminate\Support\Facades\DB;

/**
 * Class PaymentProvidersManager
 * @package App\Models\PaymentProviders
 */
class PaymentProvidersManager
{
    /**
     * Function to Store data to DB
     *
     * @param array $data
     * @return PaymentProviders
     */
    public function store(array $data)
    {
        $paymentProviders = new PaymentProviders($data);

        return DB::transaction(function () use ($data, $paymentProviders) {
            $paymentProviders->save();
            return $paymentProviders;
        });
    }

    /**
     * Function to Update data
     *
     * @param array $data
     * @param $id
     */
    public function update(array $data, $id)
    {
        $paymentProviders = PaymentProviders::where('id', $id)->firstOrFail();

        DB::transaction(function () use ($data, $paymentProviders) {
            $paymentProviders->update($data);
        });
    }

    /**
     * Function to Delete data
     *
     * @param array $deleteItems
     */
    public function delete(array $deleteItems)
    {
        DB::transaction(function () use ($deleteItems) {

            PaymentProviders::whereIn('id', $deleteItems)->update([
                'show_status' => PaymentProviders::STATUS_DELETED
            ]);

        });
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @param $id2
     * @return array
     */
    public function getLogData($id, $id2 = null)
    {
        return PaymentProviders::where('id', $id)->first()->toArray();
    }
}
