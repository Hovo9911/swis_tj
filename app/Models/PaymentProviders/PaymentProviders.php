<?php

namespace App\Models\PaymentProviders;

use App\Models\BaseModel;

/**
 * Class PaymentProviders
 * @package App\Models\PaymentProviders
 */
class PaymentProviders extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'payment_providers';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'address',
        'contacts',
        'ip_address',
        'token',
        'show_status'
    ];
}