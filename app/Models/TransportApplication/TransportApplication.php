<?php

namespace App\Models\TransportApplication;

use App\Models\BaseModel;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\TransportApplicationInvoices\TransportApplicationInvoices;
use App\Models\TransportApplicationStates\TransportApplicationStates;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class TransportApplication
 * @package App\Models\TransportApplication
 */
class TransportApplication extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'transport_application';

    /**
     * @var bool
     */
    protected $hasAdminInfo = true;

    /**
     * @var string
     */
    const TRANSPORT_APPLICATION_STATUS_CREATED = null;
    const TRANSPORT_APPLICATION_STATUS_REGISTERED = 'registered';
    const TRANSPORT_APPLICATION_STATUS_REJECTED = 'rejected';
    const TRANSPORT_APPLICATION_STATUS_APPROVED = 'approved';
    const TRANSPORT_APPLICATION_STATUS_WAITING_PAYMENT_CONFIRMATION = 'waiting_payment_confirmation';
    const TRANSPORT_APPLICATION_STATUS_ISSUED = 'issued';

    /**
     * @var array
     */
    const TRANSPORT_APPLICATION_STATUSES = [
        self::TRANSPORT_APPLICATION_STATUS_REGISTERED,
        self::TRANSPORT_APPLICATION_STATUS_REJECTED,
        self::TRANSPORT_APPLICATION_STATUS_APPROVED,
        self::TRANSPORT_APPLICATION_STATUS_WAITING_PAYMENT_CONFIRMATION,
        self::TRANSPORT_APPLICATION_STATUS_ISSUED,
    ];

    /**
     * @var string
     */
    const TEMPLATE_FOLDER = 'TRANSPORT_APPLICATION';
    const PERMIT_DOCS_FOLDER = 'PERMIT_DOCS';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'company_tax_id',
        'application_number',
        'driver_name',
        'ref_transport_carriers_id',
        'vehicle_brand',
        'vehicle_reg_number',
        'trailer_reg_number',
        'ref_origin_country',
        'ref_destination_country',
        'ref_transport_permit_type',
        'vehicle_carrying_capacity',
        'trailer_carrying_capacity',
        'vehicle_empty_capacity',
        'trailer_empty_capacity',
        'place_of_unloading',
        'entry_goods_weight',
        'departure_goods_weight',
        'nature_of_goods',
        'reject_reason',
        'permission_number',
        'ref_issue_country',
        'permit_validity_period_from',
        'permit_validity_period_to',
        'period_validity',
        'ref_border_crossing',
        'arrival_date',
        'departure_date',
        'transport_application_receipt_file',
        'current_status',
        'created_year'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'created_admin_id',
        'created_admin_ip',
        'updated_at',
        'updated_admin_id',
        'updated_admin_ip',
        'deleted_admin_id',
        'deleted_admin_ip'
    ];

    /**
     * Function to get created_at value
     *
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format(config('swis.date_time_format_front'));
    }

    /**
     * Function to get ref_transport_carriers_id value (code,name)
     *
     * @return string
     */
    public function getRefTransportCarriersAttribute()
    {
        if ($this->ref_transport_carriers_id) {
            $refCarriers = getReferenceRows(ReferenceTable::REFERENCE_TRANSPORT_PERMIT_CARRIERS, $this->ref_transport_carriers_id);

            if (!is_null($refCarriers)) {
                $value = '(' . $refCarriers->code . ')' . ' ' . $refCarriers->name;
            }
        }

        return $value ?? '';
    }

    /**
     * Function to get entry goods weight
     *
     * @param $value
     * @return float|int
     */
    public function getEntryGoodsWeightAttribute($value)
    {
        if ($value) {
            return round($value, 2);
        }

        return $value;
    }

    /**
     * Function to get departure goods weight
     *
     * @param $value
     * @return float|int
     */
    public function getDepartureGoodsWeightAttribute($value)
    {
        if ($value) {
            return round($value, 2);
        }

        return $value;
    }

    /**
     * Function to get file basename
     *
     * @return string
     */
    public function invoiceFileBaseName()
    {
        if ($this->filePath()) {
            $file = pathinfo($this->filePath());

            return $file['basename'];
        }

        return "";
    }

    /**
     * Function to get file path
     *
     * @return string
     */
    public function filePath()
    {
        if ($this->transport_application_receipt_file) {
            return url('files/uploads/files/' . $this->transport_application_receipt_file);
        }

        return "";
    }

    /**
     * Function to return invoice
     *
     * @return HasOne
     */
    public function invoice()
    {
        return $this->hasOne(TransportApplicationInvoices::class, 'transport_application_id', 'id');
    }

    /**
     * Function to return states
     *
     * @return HasMany
     */
    public function states()
    {
        return $this->hasMany(TransportApplicationStates::class, 'transport_application_id', 'id')->with(['user']);
    }

    /**
     * Function to by status type get buttons
     *
     * @param $status
     * @return string
     */
    public static function generateStatusButtons($status)
    {
        $renderButtons = '';
        switch ($status) {

            // Created
            case self::TRANSPORT_APPLICATION_STATUS_CREATED:
                $renderButtons = self::renderButtons([
                    self::TRANSPORT_APPLICATION_STATUS_REGISTERED
                ]);
                break;

            // Registered
            case self::TRANSPORT_APPLICATION_STATUS_REGISTERED:
                $renderButtons = self::renderButtons([
                    self::TRANSPORT_APPLICATION_STATUS_APPROVED,
                    self::TRANSPORT_APPLICATION_STATUS_REJECTED
                ]);
                break;

            // Approve
            case self::TRANSPORT_APPLICATION_STATUS_APPROVED:
                $renderButtons = self::renderButtons([
                    self::TRANSPORT_APPLICATION_STATUS_WAITING_PAYMENT_CONFIRMATION,
                ]);
                break;

            // Waiting Payment
            case self::TRANSPORT_APPLICATION_STATUS_WAITING_PAYMENT_CONFIRMATION:
                $renderButtons = self::renderButtons([
                    self::TRANSPORT_APPLICATION_STATUS_ISSUED,
                ]);
                break;

        }

        return $renderButtons;
    }

    /**
     * Function to get buttons html
     *
     * @param $statuses
     * @return string
     */
    private static function renderButtons($statuses)
    {
        $html = '';
        $allBtnTypes = self::renderBtnTypes();
        foreach ($statuses as $status) {

            $btnType = 'btn-primary';
            if (isset($allBtnTypes[$status])) {
                $btnType = $allBtnTypes[$status];
            }

            $html .= ' <button  data-status="' . $status . '" class="btn ' . $btnType . ' save-button mr-10  btn-' . $status . '">' . trans('swis.transport_application.status.' . $status . '.button') . '</button>';
        }

        return $html;
    }

    /**
     * Function to get render status buttons colors and types
     *
     * @return string[]
     */
    private static function renderBtnTypes()
    {
        return [
            self::TRANSPORT_APPLICATION_STATUS_APPROVED => 'btn-success',
            self::TRANSPORT_APPLICATION_STATUS_REJECTED => 'btn-danger'
        ];
    }

}