<?php

namespace App\Models\TransportApplication;

use App\Http\Controllers\Core\DataTableSearch;
use Illuminate\Support\Facades\DB;

/**
 * Class TransportApplicationSearch
 * @package App\Models\TransportApplication
 */
class TransportApplicationSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);

        return $query->get();
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($this->orderByCol, $this->orderByType ?? 'desc');
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $query = TransportApplication::select('transport_application.id', 'application_number', 'vehicle_reg_number', 'trailer_reg_number', 'current_status', 'reference_transport_permit_types_ml.short_name as permit_type', DB::raw("CONCAT(users.first_name,' ',users.last_name) as created_user_name"))
            ->leftJoin('users', 'users.id', '=', 'transport_application.user_id')
            ->join('reference_transport_permit_types_ml', function ($query) {
                $query->on('reference_transport_permit_types_ml.reference_transport_permit_types_id', '=', 'transport_application.ref_transport_permit_type')->where("reference_transport_permit_types_ml.lng_id", cLng('id'));
            });

        if (isset($this->searchData['id'])) {
            $query->where('transport_application.id', $this->searchData['id']);
        }

        if (isset($this->searchData['application_number'])) {
            $query->where('transport_application.application_number', 'ILIKE', '%' . strtolower(trim($this->searchData['application_number'])) . '%');
        }

        if (isset($this->searchData['vehicle_reg_number'])) {
            $query->where('transport_application.vehicle_reg_number', 'ILIKE', '%' . strtolower(trim($this->searchData['vehicle_reg_number'])) . '%');
        }

        if (isset($this->searchData['ref_transport_permit_type'])) {
            $query->where('transport_application.ref_transport_permit_type', $this->searchData['ref_transport_permit_type']);
        }

        if (isset($this->searchData['current_status'])) {
            $query->where('transport_application.current_status', $this->searchData['current_status']);
        }

        $query->checkUserHasRole();

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}