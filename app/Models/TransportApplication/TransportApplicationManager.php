<?php

namespace App\Models\TransportApplication;

use App\Models\TransportApplicationInvoices\TransportApplicationInvoices;
use App\Models\TransportApplicationStates\TransportApplicationStates;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class TransportApplicationManager
 * @package App\Models\TransportApplication
 */
class TransportApplicationManager
{
    /**
     * Function to store validated data
     * @param array $data
     *
     * @return int
     */
    public function store(array $data)
    {
        DB::beginTransaction();

        $data['user_id'] = Auth::id();
        $data['company_tax_id'] = Auth::user()->companyTaxId();
        $data['application_number'] = $this->returnApplicationNumber();
        $data['created_year'] = date('Y');
        $data['current_status'] = TransportApplication::TRANSPORT_APPLICATION_STATUS_REGISTERED;

        $transportApplication = new TransportApplication($data);
        $transportApplication->save();

        $this->storeState($transportApplication->id, '', TransportApplication::TRANSPORT_APPLICATION_STATUS_REGISTERED);

        DB::commit();

        return $transportApplication->id;
    }

    /**
     * Function to update data
     *
     * @param array $data
     * @param int $id
     */
    public function update(array $data, int $id)
    {
        $transportApplication = TransportApplication::select('id', 'current_status')->where('id', $id)->checkUserHasRole()->firstOrFail();

        if (isset($data['period_validity'])) {
            $periodFrom = Carbon::parse($data['permit_validity_period_from']);
            $periodTo = Carbon::parse($data['permit_validity_period_to']);

            $data['period_validity'] = $periodTo->diffInDays($periodFrom) + 1;
        }

        DB::transaction(function () use ($id, $data, $transportApplication) {

            $this->storeState($transportApplication->id, $transportApplication->current_status, $data['current_status']);

            //
            $transportApplication->update($data);
        });
    }

    /**
     * Function to reject application
     *
     * @param array $data
     * @param int $id
     */
    public function reject(array $data, int $id)
    {
        $transportApplication = TransportApplication::select('id', 'current_status')->where('id', $id)->checkUserHasRole()->firstOrFail();

        if ($transportApplication->current_status == TransportApplication::TRANSPORT_APPLICATION_STATUS_REGISTERED) {

            $data['current_status'] = TransportApplication::TRANSPORT_APPLICATION_STATUS_REJECTED;

            DB::transaction(function () use ($data, $transportApplication) {

                $this->storeState($transportApplication->id, $transportApplication->current_status, TransportApplication::TRANSPORT_APPLICATION_STATUS_REJECTED);

                //
                $transportApplication->update($data);

            });
        }
    }

    /**
     * Function to store invoice
     *
     * @param array $data
     * @param int $id
     */
    public function storeInvoice(array $data, int $id)
    {
        $transportApplication = TransportApplication::select('id', 'current_status')->where('id', $id)->checkUserHasRole()->firstOrFail();

        if ($transportApplication->current_status == TransportApplication::TRANSPORT_APPLICATION_STATUS_APPROVED) {

            DB::transaction(function () use ($data, $transportApplication) {

                $defaultData = [
                    'transport_application_id' => $transportApplication->id,
                    'user_id' => Auth::id(),
                    'company_tax_id' => Auth::user()->companyTaxId(),
                    'tracking_number' => generateID(2) . time(),
                ];

                $data = array_merge($defaultData, $data);

                $transportApplicationInvoice = new TransportApplicationInvoices($data);
                $transportApplicationInvoice->save();

                //
                $this->storeState($transportApplication->id, $transportApplication->current_status, TransportApplication::TRANSPORT_APPLICATION_STATUS_WAITING_PAYMENT_CONFIRMATION);

                // Update Status
                $transportApplication->update(['current_status' => TransportApplication::TRANSPORT_APPLICATION_STATUS_WAITING_PAYMENT_CONFIRMATION]);

            });
        }
    }

    /**
     * Function to store Transport application states
     *
     * @param string $fromState
     * @param string $toState
     * @param int $transportApplicationId
     */
    private function storeState(int $transportApplicationId, string $fromState, string $toState)
    {
        $data = [
            'transport_application_id' => $transportApplicationId,
            'user_id' => Auth::id(),
            'company_tax_id' => Auth::user()->companyTaxId(),
            'from_status' => $fromState ?: null,
            'to_status' => $toState
        ];

        $transportApplicationState = new TransportApplicationStates($data);
        $transportApplicationState->save();
    }

    /**
     * Function to return next possible regular number
     *
     * @return string
     */
    private function returnApplicationNumber()
    {
        $transportApplicationsCountOfCurrentYear = TransportApplication::where('created_year', date('Y'))->count();

        $applicationNumber = str_pad($transportApplicationsCountOfCurrentYear + 1, 7, "0", STR_PAD_LEFT);

        $existTransportApplication = TransportApplication::select('application_number')->where('application_number', $applicationNumber)->where('created_year', date('Y'))->first();
        if (is_null($existTransportApplication)) {
            return $applicationNumber;
        }

        return $this->returnApplicationNumber();
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @param null $id2
     * @return array
     */
    public function getLogData($id, $id2 = null)
    {
        return TransportApplication::where('id', $id)->first()->toArray();
    }
}