<?php

namespace App\Models\ConstructorListToDataSetFromMdm;

use App\Models\BaseModel;
use App\Models\ConstructorLists\ConstructorLists;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class ConstructorListToDataSetFromMdm
 * @package App\Models\ConstructorListToDataSetFromMdm
 */
class ConstructorListToDataSetFromMdm extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'constructor_list_to_data_set_from_mdm';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'constructor_list_id',
        'constructor_data_set_field_id',
    ];

    /**
     * Function to relate with constructor lists
     *
     * @return HasOne
     */
    public function relatedList()
    {
        return $this->hasOne(ConstructorLists::class, 'id', 'constructor_list_id');
    }
}
