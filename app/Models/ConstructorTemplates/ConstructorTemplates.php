<?php

namespace App\Models\ConstructorTemplates;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Auth;

/**
 * Class ConstructorTemplates
 * @package App\Models\ConstructorTemplates
 */
class ConstructorTemplates extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'constructor_templates';

    /**
     * @var string (pdf_type column values)
     */
    const PDF_TYPE_TCPDF = 'tcpdf';
    const PDF_TYPE_MPDF = 'mpdf';

    /**
     * @var string
     */
    const LANGUAGE_CODE_EN = '1';
    const LANGUAGE_CODE_RU = '133';
    const LANGUAGE_CODE_TJ = '10';

    /**
     * @var string
     */
    const DEFAULT_PDF = 'default';

    const TAJIKSTANDART_TEMPLATE_CODE = 'TAJIKSTANDART';                                                   //Print Form code for Tajikstandart
    const TJMOH_TEMPLATE_CODE = 'TjMoH';                                                                   //Print Form code for Conformity Certificate of TJ Ministry of Health
    const TJCS_TEMPLATE_CODE = 'TjCS';                                                                     //Print Form code for Conformity Certificate of TJ Communication Service
    const TJ_SWT_756_CCI_V1 = 'TJ_SWT_756_CCI_V1';                                                         //https://app.asana.com/0/782353902251221/1147629688839114
    const TJ_SWT_756_CCI_V2 = 'TJ_SWT_756_CCI_V2';                                                         //https://app.asana.com/0/803906675136235/1147955694571266
    const TJ_SWT_756_CCI_FORM_A = 'TJ_SWT_756_CCI_FORM_A';                                                 //https://app.asana.com/0/803906675136235/1147740621004884
    const TJ_SWT_756_CCI_FORM_A_WITHOUT_REG_NUMBER = 'TJ_SWT_756_CCI_FORM_A_WITHOUT_REG_NUMBER';           //https://app.asana.com/0/782353902251221/1199220524704720/f
    const FSC_TEMPLATE_CODE = 'FSC';                                                                       //Print Form code for Agency TIN = 020049945 , Agency Legal Name =КУМИТАИ БЕХАТАРИИ ОЗУКАВОРИИ НАЗДИ ХУКУМАТИ ЧУМХУРИИ ТОЧИКИСТОН
    const MOC_TEMPLATE_CODE = 'MOC';                                                                       //Print Form code for Agency TIN = 010009673 , Agency Legal Name =Вазорати фарханги Чумхуриии Точикистон
    const SEC_TEMPLATE_CODE = 'SEC';                                                                       //Print Form code for Agency TIN = 020004117 , Agency Legal Name =Хадамоти назорати давлатии тандурустӣ ва ҳифзи иҷтимоии аҳолӣ,Sanitary-epidemiological conclusion
    const FSC1_TEMPLATE_CODE = 'FSC_1';                                                                    //Print Form code for Agency TIN = 020049945 , Agency Legal Name =КУМИТАИ БЕХАТАРИИ ОЗУКАВОРИИ НАЗДИ ХУКУМАТИ ЧУМХУРИИ ТОЧИКИСТОН, Document id = 24
    const FSCVET_TEMPLATE_CODE = 'FSC_VET';                                                                //Print Form code for Agency TIN = 020049945 , Agency Legal Name =КУМИТАИ БЕХАТАРИИ ОЗУКАВОРИИ НАЗДИ ХУКУМАТИ ЧУМХУРИИ ТОЧИКИСТОН, Document id = 25
    const FSCVET2_TEMPLATE_CODE = 'FSC_VET_2';                                                             //Print Form code for Agency TIN = 020049945 , Agency Legal Name =КУМИТАИ БЕХАТАРИИ ОЗУКАВОРИИ НАЗДИ ХУКУМАТИ ЧУМХУРИИ ТОЧИКИСТОН, Document id = 25
    const FSCVET3_TEMPLATE_CODE = 'FSC_VET_3';                                                             //Print Form code for Agency TIN = 020049945 , Agency Legal Name =КУМИТАИ БЕХАТАРИИ ОЗУКАВОРИИ НАЗДИ ХУКУМАТИ ЧУМХУРИИ ТОЧИКИСТОН, Document id = 25 ,task https://app.asana.com/0/782353902251221/1136885251908028/f
    const FSCVET4_TEMPLATE_CODE = 'FSC_VET_4';                                                             //Print Form code for Agency TIN = 020049945 , Agency Legal Name =КУМИТАИ БЕХАТАРИИ ОЗУКАВОРИИ НАЗДИ ХУКУМАТИ ЧУМХУРИИ ТОЧИКИСТОН, Document id = 25 ,task https://app.asana.com/0/782353902251221/1136885251908029/f
    const FSCVET5_TEMPLATE_CODE = 'FSC_VET_5';                                                             //Print Form code for Agency TIN = 020049945 , Agency Legal Name =КУМИТАИ БЕХАТАРИИ ОЗУКАВОРИИ НАЗДИ ХУКУМАТИ ЧУМХУРИИ ТОЧИКИСТОН, Document id = 25 ,task https://app.asana.com/0/782353902251221/1136885251908030/f
    const FSC_IMP_VET_TEMPLATE_CODE = 'FSC_IMP_VET';                                                       //Print Form code for Agency TIN = 020049945 , Agency Legal Name =КУМИТАИ БЕХАТАРИИ ОЗУКАВОРИИ НАЗДИ ХУКУМАТИ ЧУМХУРИИ ТОЧИКИСТОН, Document id = 37
    const FSC_IMP_QUA_TEMPLATE_CODE = 'FSC_IMP_QUA';                                                       //Print Form code for Agency TIN = 020049945 , Agency Legal Name =КУМИТАИ БЕХАТАРИИ ОЗУКАВОРИИ НАЗДИ ХУКУМАТИ ЧУМХУРИИ ТОЧИКИСТОН, Document id = 38
    const FSC_IMP_QUA2_TEMPLATE_CODE = 'FSC_IMP_QUA_2';                                                    //Print Form code for Agency TIN = 020049945 , Agency Legal Name =КУМИТАИ БЕХАТАРИИ ОЗУКАВОРИИ НАЗДИ ХУКУМАТИ ЧУМХУРИИ ТОЧИКИСТОН, Document id = 38
    const FSC_VET_PHARM_TEMPLATE_CODE = 'FSC_VET_PHARM';                                                   //Print Form code for Agency TIN = 020049945 , Agency Legal Name =КУМИТАИ БЕХАТАРИИ ОЗУКАВОРИИ НАЗДИ ХУКУМАТИ ЧУМХУРИИ ТОЧИКИСТОН, Document id = 30
    const SASS_TEMPLATE_CODE = 'SASS_1';                                                                   //Print Form code for Agency TIN = 010009232 , Agency Legal Name =Государственный пробирный надзор РТ при Мин. фин Республики Таджикистан, Document id = 29 , task https://app.asana.com/0/782353902251221/1142922707574402/f
    const SASS_EXAMINATION = 'SASS_EXAMINATION';                                                           //https://app.asana.com/0/782353902251221/1146588008003430/f
    const SASS_EXAMINATION_TYPE_ITEM = 'SASS_EXAMINATION_TYPE_ITEM';                                       //https://app.asana.com/0/878213809858668/1199682885839943/f
    const KOOC = 'KOOC';                                                                                   //https://app.asana.com/0/782353902251221/1155091262823993
    const TJ_LICENSE_NRSL_SWT_902 = 'TJ_LICENSE_NRSL_SWT_902';                                             //https://app.asana.com/0/803906675136235/1151032525984281
    const TJ_SWT_872 = 'TJ_SWT_872';                                                                       //https://app.asana.com/0/803906675136235/1152404077450405
    const MOC_TJ_TEMPLATE_CODE = 'MOC_TJ';                                                                 //https://app.asana.com/0/782353902251221/1159385577564347
    const FSC_ACT_1_TEMPLATE_CODE = 'FSC_ACT_1';                                                           //https://app.asana.com/0/782353902251221/1170422876121417/f
    const TJMOH_PERMITION_RU = 'TJMOH_PERMITION_RU';                                                       //https://app.asana.com/0/782353902251221/1170960072807433/f
    const TJMOH_PERMITION_EN = 'TJMOH_PERMITION_EN';                                                       //https://app.asana.com/0/782353902251221/1170983989235107/f
    const TJMOH_RESOLUTION = 'TJMOH_RESOLUTION';                                                           //https://app.asana.com/0/782353902251221/1170678014570401/f
    const TJMOH_RESOLUTION_1 = 'TJMOH_RESOLUTION_1';                                                       //https://app.asana.com/0/782353902251221/1178253971157435/f
    const FSC_2 = 'FSC_2';                                                                                 //https://app.asana.com/0/803906675136235/1172191291952352
    const FSC_TJ = 'FSC_TJ';                                                                               //https://app.asana.com/0/803906675136235/1174241973793461/f

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'company_tax_id',
        'description',
        'qr_code',
        'authorization_without_login',
        'show_status'
    ];

    /**
     * Function to return active languages
     *
     * @return HasMany
     */
    public function ml()
    {
        return $this->hasMany(ConstructorTemplatesMl::class, 'constructor_templates_id', 'id');
    }

    /**
     * Function to relate with ConstructorStatesMl and get active
     *
     * @return HasOne
     */
    public function current()
    {
        return $this->hasOne(ConstructorTemplatesMl::class, 'constructor_templates_id', 'id')->notDeleted()->where('lng_id', cLng('id'))->first();
    }

    /**
     * Function to relate with ConstructorStatesMl
     *
     * @return HasOne
     */
    public function currentMl()
    {
        return $this->hasOne(ConstructorTemplatesMl::class, 'constructor_templates_id', 'id')->notDeleted()->where('lng_id', cLng('id'));
    }

    /**
     * Function to check auth user has role
     *
     * @param $query
     * @return Builder
     */
    public function scopeConstructorTemplateOwner($query)
    {
        $companyTaxId = -1;
        if (Auth::user()->isAgency()) {
            $companyTaxId = Auth()->user()->companyTaxId();
        }

        return $query->where($this->getTable() . '.company_tax_id', $companyTaxId);
    }

}
