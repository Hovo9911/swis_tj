<?php

namespace App\Models\ConstructorTemplates;

use App\Models\BaseMlManager;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class ConstructorTemplatesManager
 * @package App\Models\ConstructorTemplates
 */
class ConstructorTemplatesManager 
{
    use BaseMlManager;

    /**
     * Function to Store data to DB
     *
     * @param array $data
     * @return ConstructorTemplates
     */
    public function store(array $data)
    {
        $data['company_tax_id'] = Auth::user()->companyTaxId();
        $data['qr_code'] = isset($data['qr_code']) ? ConstructorTemplates::TRUE : ConstructorTemplates::FALSE;

        if ($data['qr_code'] == ConstructorTemplates::FALSE) {
            $data['authorization_without_login'] = ConstructorTemplates::FALSE;
        } else {
            $data['authorization_without_login'] = isset($data['authorization_without_login']) ? ConstructorTemplates::TRUE : ConstructorTemplates::FALSE;
        }

        $constructorTemplates = new ConstructorTemplates($data);
        $constructorTemplatesMl = new ConstructorTemplatesMl();

        return DB::transaction(function () use ($data, $constructorTemplates,$constructorTemplatesMl) {
            $constructorTemplates->save();
            $this->storeMl($data['ml'], $constructorTemplates, $constructorTemplatesMl);

            return $constructorTemplates;
        });
    }

    /**
     * Function to Update data
     *
     * @param array $data
     * @param int $id
     */
    public function update(array $data, int $id)
    {
        $constructorTemplates = ConstructorTemplates::where('id', $id)->constructorTemplateOwner()->firstOrFail();
        $constructorTemplatesMl = new ConstructorTemplatesMl();

        $data['qr_code'] = isset($data['qr_code']) ? ConstructorTemplates::TRUE : ConstructorTemplates::FALSE;

        if ($data['qr_code'] == ConstructorTemplates::FALSE) {
            $data['authorization_without_login'] = ConstructorTemplates::FALSE;
        } else {
            $data['authorization_without_login'] = isset($data['authorization_without_login']) ? ConstructorTemplates::TRUE : ConstructorTemplates::FALSE;
        }

        DB::transaction(function () use ($data, $constructorTemplates,$constructorTemplatesMl) {
            $constructorTemplates->update($data);
            $this->updateMl($data['ml'], 'constructor_templates_id', $constructorTemplates, $constructorTemplatesMl);
        });
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @param $id2
     * @return array
     */
    public function getLogData($id, $id2 = null)
    {
        $data = ConstructorTemplates::where('id', $id)->first()->toArray();
        $data['mls'] = ConstructorTemplatesMl::where('constructor_templates_id', $id)->get()->toArray();

        return $data;
    }
}
