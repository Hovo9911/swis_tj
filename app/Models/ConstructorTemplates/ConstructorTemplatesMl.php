<?php

namespace App\Models\ConstructorTemplates;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

/**
 * Class ConstructorActionsMl
 * @package App\Models\ConstructorActions
 */
class ConstructorTemplatesMl extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['constructor_templates_id', 'lng_id'];

    /**
     * @var string
     */
    public $incrementing = 'false';

    /**
     * @var string
     */
    public $table = 'constructor_templates_ml';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'constructor_templates_id',
        'lng_id',
        'template_name'
    ];
}
