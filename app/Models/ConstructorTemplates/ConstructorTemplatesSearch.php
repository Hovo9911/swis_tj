<?php

namespace App\Models\ConstructorTemplates;

use App\Http\Controllers\Core\DataTableSearch;
use Illuminate\Support\Facades\DB;

/**
 * Class ConstructorTemplatesSearch
 * @package App\Models\ConstructorTemplates
 */
class ConstructorTemplatesSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        return $query->get();
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($this->orderByCol, $this->orderByType ?? 'desc');
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $query = ConstructorTemplates::joinMl()
            ->select('constructor_templates.id', 'constructor_templates.code', 'constructor_templates.description', 'constructor_templates.show_status', 'constructor_templates_ml.template_name')
            ->notDeleted()
            ->constructorTemplateOwner();

        if (isset($this->searchData['id'])) {
            $query->where(DB::raw('constructor_templates.id::varchar'), $this->searchData['id']);
        }

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}
