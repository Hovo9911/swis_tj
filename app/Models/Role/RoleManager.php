<?php

namespace App\Models\Role;

use App\Models\Agency\Agency;
use App\Models\BaseMlManager;
use App\Models\Company\Company;
use App\Models\ConstructorDataSet\ConstructorDataSet;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorDocumentsRoles\ConstructorDocumentsRoles;
use App\Models\DataModel\DataModel;
use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm;
use App\Models\DocumentsDatasetFromSaf\DocumentsDatasetFromSaf;
use App\Models\Menu\Menu;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\RegionalOffice\RegionalOffice;
use App\Models\RolesGroupRolesAttributes\RolesGroupRolesAttributes;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure;
use App\Models\User\User;
use App\Models\UserRoles\UserRoles;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class RoleManager
 * @package App\Models\Role
 */
class RoleManager
{
    use BaseMlManager;

    /**
     * Function to Store data to DB
     *
     * @param array $data
     * @return Role
     */
    public function store(array $data)
    {
        $data['level'] = Role::CUSTOM_ROLE_LEVEL;
        $data['user_id'] = Auth::id();
        $data['create_permission'] = $data['create_permission'] ?? '0';
        $data['read_permission'] = $data['read_permission'] ?? '0';
        $data['update_permission'] = $data['update_permission'] ?? '0';
        $data['delete_permission'] = $data['delete_permission'] ?? '0';
        $data['company_tax_id'] = Auth::user()->companyTaxId();

        //
        $role = new Role($data);
        $roleMl = new RoleMl();

        return DB::transaction(function () use ($role, $roleMl, $data) {

            $role->save();
            $this->storeMl($data['ml'], $role, $roleMl);

            return $role;
        });
    }

    /**
     * Function to Update data
     *
     * @param array $data
     * @param int $id
     */
    public function update(array $data, int $id)
    {
        $role = Role::where('id', $id)->checkUserHasRole()->firstOrFail();
        $roleMl = new RoleMl();

        $data['create_permission'] = $data['create_permission'] ?? '0';
        $data['read_permission'] = $data['read_permission'] ?? '0';
        $data['update_permission'] = $data['update_permission'] ?? '0';
        $data['delete_permission'] = $data['delete_permission'] ?? '0';

        DB::transaction(function () use ($role, $roleMl, $data) {

            $role->update($data);
            $this->updateMl($data['ml'], 'role_id', $role, $roleMl);

        });
    }

    /**
     * Function to check user has role and get all user current role info data
     *
     * @param $role
     * @param $roleType
     * @param $userRoleValue
     * @return array
     */
    public function currentRoleData($role, $roleType, $userRoleValue)
    {
        $currentRoleData = [];

        // Default Roles
        if ($role == Role::ROLE_LEVEL_TYPE_DEFAULT) {

            // SW Admins roles (Authorized as Sw admin too)
            if ($roleType == Role::ROLE_TYPE_SW_ADMIN) {

                $userRoles = UserRoles::where(['user_id' => Auth::id(), 'role_type' => Role::ROLE_TYPE_SW_ADMIN])->activeRole();

                if (!in_array(Auth::user()->ssn, User::SW_ADMIN_SSN)) {
                    $userRoles->whereNotNull('authorized_by_id');
                } else {
                    $userRoles->whereNull('authorized_by_id');
                }

                $userRoles = $userRoles->pluck('id')->all();

                if (count($userRoles)) {
                    $currentRoleData = [
                        'user_role_ids' => $userRoles,
                        'role_id' => Role::SW_ADMIN,
                        'role' => Role::ROLE_LEVEL_TYPE_DEFAULT,
                        'role_type' => Role::ROLE_TYPE_SW_ADMIN,
                        'authorized_by_id' => $userRoleValue
                    ];
                }
            }

            // Natural Persons roles (Authorized as Natural person too)
            if ($roleType == Role::ROLE_TYPE_NATURAL_PERSON) {
                $userRoles = UserRoles::where(['user_id' => Auth::id(), 'role_type' => Role::ROLE_TYPE_NATURAL_PERSON])->activeRole();

                if ($userRoleValue) {
                    $userRoles->where('authorized_by_id', $userRoleValue);
                } else {
                    $userRoles->whereNull('authorized_by_id');
                }

                $userRoles = $userRoles->pluck('id')->all();

                if (count($userRoles)) {
                    $currentRoleData = [
                        'user_role_ids' => $userRoles,
                        'role_id' => Role::NATURAL_PERSON,
                        'role' => Role::ROLE_LEVEL_TYPE_DEFAULT,
                        'role_type' => Role::ROLE_TYPE_NATURAL_PERSON,
                        'authorized_by_id' => $userRoleValue
                    ];
                }
            }

            // Head of Company(Agency) roles (Authorized as Head of company too)
            if ($roleType == Role::ROLE_TYPE_COMPANY) {
                $userRoles = UserRoles::select('id', 'authorized_by_id')->where(['user_id' => Auth::id(), 'role_id' => Role::HEAD_OF_COMPANY, 'company_id' => $userRoleValue, 'role_type' => Role::ROLE_TYPE_COMPANY])->activeRole()->get();

                $company = Company::select('tax_id')->where('id', $userRoleValue)->first();
                $agency = Agency::select('id', 'is_lab')->where('tax_id', $company->tax_id)->active()->first();

                if (count($userRoles)) {
                    $currentRoleData = [
                        'user_role_ids' => $userRoles->pluck('id')->all(),
                        'role_id' => Role::HEAD_OF_COMPANY,
                        'role' => Role::ROLE_LEVEL_TYPE_DEFAULT,
                        'role_type' => Role::ROLE_TYPE_COMPANY,
                        'company_tax_id' => $company->tax_id,
                        'company_id' => $userRoleValue,
                        'agency_id' => optional($agency)->id,
                        'is_lab' => optional($agency)->is_lab,
                        'authorized_by_id' => $userRoles[0]->authorized_by_id
                    ];
                }
            }

            // Customs Portal Role
            if ($roleType == Role::ROLE_TYPE_CUSTOMS_OPERATOR) {
                $userRoles = UserRoles::select('id')->where(['user_id' => Auth::id(), 'role_type' => Role::ROLE_TYPE_CUSTOMS_OPERATOR])->activeRole()->pluck('id')->all();

                if (count($userRoles)) {
                    $currentRoleData = [
                        'user_role_ids' => $userRoles,
                        'role_id' => Role::CUSTOMS_OPERATOR,
                        'role' => Role::ROLE_LEVEL_TYPE_DEFAULT,
                        'role_type' => Role::ROLE_TYPE_CUSTOMS_OPERATOR,
                    ];
                }
            }
        }

        // Custom Roles
        if ($role == Role::ROLE_LEVEL_TYPE_CUSTOM) {

            // Authorized From company
            if ($roleType == Role::ROLE_TYPE_COMPANY) {
                $userRoles = UserRoles::where(['user_id' => Auth::id(), 'company_id' => $userRoleValue, 'role_type' => Role::ROLE_TYPE_COMPANY])->activeRole()->whereNotNull('authorized_by_id')->pluck('id')->all();

                if (count($userRoles)) {
                    $company = Company::select('tax_id')->where('id', $userRoleValue)->first();
                    $agency = Agency::select('id', 'is_lab')->where('tax_id', $company->tax_id)->active()->first();

                    $currentRoleData = [
                        'user_role_ids' => $userRoles,
                        'role' => Role::ROLE_LEVEL_TYPE_CUSTOM,
                        'role_type' => Role::ROLE_TYPE_COMPANY,
                        'company_id' => $userRoleValue,
                        'agency_id' => optional($agency)->id,
                        'is_lab' => optional($agency)->is_lab,
                        'company_tax_id' => $company->tax_id
                    ];
                }
            }

            // Natural Persons roles (Authorized as Natural person too)
            if ($roleType == Role::ROLE_TYPE_NATURAL_PERSON) {
                $userRoles = UserRoles::where(['user_id' => Auth::id(), 'role_id' => Role::AUTHORIZE_USER_ADD_MY_PERMISSIONS])->activeRole()->where('authorized_by_id', $userRoleValue)->pluck('id')->all();

                if (count($userRoles)) {
                    $currentRoleData = [
                        'user_role_ids' => $userRoles,
                        'role_id' => Role::NATURAL_PERSON,
                        'role' => Role::ROLE_LEVEL_TYPE_CUSTOM,
                        'role_type' => Role::ROLE_TYPE_NATURAL_PERSON,
                        'authorized_by_id' => $userRoleValue
                    ];
                }
            }
        }

        return $currentRoleData;
    }

    /**
     * Function to get Attributes
     *
     * @param $roleIds
     * @param $isRoleGroup
     * @return array
     */
    public function getAttributes($roleIds, $isRoleGroup = false)
    {
        $authorizedSafAttributes = $authorizedMDMAttributes = [];
        $safRoleIds = $mdmRoleIds = $roleIds = (array)$roleIds;

        //
        if (Auth::user()->isAuthorizedEmployeeForCompany()) {

            $where = function ($q) use ($isRoleGroup, $roleIds) {
                $roleCheckKey = $isRoleGroup ? 'role_group_id' : 'role_id';
                $q->whereIn($roleCheckKey, $roleIds)->whereNotNull('attribute_field_id');
            };

            $userAuthorizedUserRoles = Auth::user()->getCurrentUserRoles(Menu::USER_DOCUMENTS_MENU_NAME, false, ['attribute_field_id', 'attribute_type', 'attribute_value'], $where);

            foreach ($userAuthorizedUserRoles as $userRole) {

                if ($userRole->attribute_type == Role::ROLE_ATTRIBUTE_TYPE_SAF) {
                    $authorizedSafAttributes[] = $userRole->attribute_field_id;
                }

                if ($userRole->attribute_type == Role::ROLE_ATTRIBUTE_TYPE_MDM) {
                    $authorizedMDMAttributes[] = $userRole->attribute_field_id;
                }
            }
        }

        // SAF
        if ($isRoleGroup) {
            $safRoleIds = RolesGroupRolesAttributes::whereIn('roles_group_id', $safRoleIds)->where('attribute_type', Role::ROLE_ATTRIBUTE_TYPE_SAF)->pluck('role_id')->all();
        }

        $safAttrFieldIds = $this->constructorDocumentRoleByType($safRoleIds, Role::ROLE_ATTRIBUTE_TYPE_SAF, $authorizedSafAttributes);

        // MDM
        if ($isRoleGroup) {
            $mdmRoleIds = RolesGroupRolesAttributes::whereIn('roles_group_id', $mdmRoleIds)->where('attribute_type', Role::ROLE_ATTRIBUTE_TYPE_MDM)->pluck('role_id')->all();
        }

        $mdmAttrFieldIds = $this->constructorDocumentRoleByType($mdmRoleIds, Role::ROLE_ATTRIBUTE_TYPE_MDM, [], $authorizedMDMAttributes);

        //
        $dataSetMdm = DocumentsDatasetFromMdm::select('documents_dataset_from_mdm.*', 'documents_dataset_from_mdm_ml.label')
            ->leftJoin('documents_dataset_from_mdm_ml', 'documents_dataset_from_mdm_ml.documents_dataset_from_mdm_id', '=', 'documents_dataset_from_mdm.id')
            ->where('lng_id', cLng('id'))
            ->where('is_attribute', true)
            ->whereIn('field_id', $mdmAttrFieldIds)
            ->get();

        $getXmlAttrParams = ConstructorDataSet::getGroupNamePath();

        $datasetFromSaf = DocumentsDatasetFromSaf::whereIn('saf_field_id', $safAttrFieldIds)->where('is_attribute', true)->get();

        $attributes = [];
        if ($datasetFromSaf->count() > 0) {
            foreach ($datasetFromSaf as $safAttr) {
                $attributes['saf'][$safAttr->saf_field_id] = trans($safAttr->title_trans_key) . ' ( ' . $getXmlAttrParams[$safAttr->xml_field_name] . ' )';
            }
        }

        if ($dataSetMdm->count() > 0) {
            foreach ($dataSetMdm as $mdmAttr) {
                $attributes['mdm'][$mdmAttr->field_id] = $mdmAttr->label . ' (' . trans($mdmAttr->group) . ')';
            }
        }

        return $attributes;
    }

    /**
     * Function to get attribute values
     *
     * @param $attribute
     * @param $attributeType
     * @param $roleId
     * @param bool $isRoleGroup
     * @return array
     */
    public function getAttributeValues($attribute, $attributeType, $roleId, $isRoleGroup = false)
    {
        $mdmId = null;
        $attributeValues = $where = $authorizedSafAttributesValues = $authorizedMDMAttributesValues = [];

        if (Auth::user()->isAuthorizedEmployeeForCompany()) {

            $roleCheckKey = $isRoleGroup ? 'role_group_id' : 'role_id';

            $userAuthorizedUserRoles = Auth::user()->getCurrentUserRoles(Menu::USER_DOCUMENTS_MENU_NAME, false, ['attribute_field_id', 'attribute_type', 'attribute_value'], [$roleCheckKey => $roleId, 'attribute_field_id' => $attribute]);

            foreach ($userAuthorizedUserRoles as $userRole) {

                if ($attributeType == Role::ROLE_ATTRIBUTE_TYPE_SAF) {
                    $authorizedSafAttributesValues[] = $userRole->attribute_value;
                }

                if ($attributeType == Role::ROLE_ATTRIBUTE_TYPE_MDM) {
                    $authorizedMDMAttributesValues[] = $userRole->attribute_value;
                }
            }
        }


        // SAF
        if ($attributeType == Role::ROLE_ATTRIBUTE_TYPE_SAF) {
            $lastXml = SingleApplicationDataStructure::lastXml();
            $xmlAttr = $lastXml->xpath("//*[@id='{$attribute}']");

            if (count($authorizedSafAttributesValues)) {

                $where = function ($q) use ($authorizedSafAttributesValues) {
                    $q->whereIn('id', $authorizedSafAttributesValues);
                };

            } else {

                // Subdivision
                if ((string)$xmlAttr[0]->attributes()->id == SingleApplication::SAF_AGENCY_SUBDIVISION_ID) {

                    $constructorDocumentRole = ConstructorDocumentsRoles::select('document_id')
                        ->join('constructor_documents_roles_attribute', 'constructor_documents_roles.id', '=', 'constructor_documents_roles_attribute.constructor_documents_roles_id')
                        ->where('type', Role::ROLE_ATTRIBUTE_TYPE_SAF);

                    if (!$isRoleGroup) {
                        $constructorDocumentRole->where('role_id', $roleId);
                    } else {
                        $roleIds = RolesGroupRolesAttributes::where('roles_group_id', $roleId)->where('attribute_type', Role::ROLE_ATTRIBUTE_TYPE_SAF)->pluck('role_id')->all();

                        $constructorDocumentRole->whereIn('role_id', $roleIds);
                    }

                    $constructorDocumentRole = $constructorDocumentRole->first();

                    if (!is_null($constructorDocumentRole)) {

                        $constructorDocument = ConstructorDocument::select('id', 'document_type_id', 'company_tax_id', 'document_code')->where(['id' => $constructorDocumentRole->document_id, 'company_tax_id' => Auth::user()->companyTaxId()])->first();

                        if (!is_null($constructorDocument)) {

                            $availableTaxIds = RegionalOffice::select('office_code')
                                ->join('regional_office_constructor_documents', function ($q) use ($constructorDocument) {
                                    $q->on('regional_office_constructor_documents.regional_office_id', '=', 'regional_office.id')->where('constructor_document_code', $constructorDocument->document_code);
                                })->pluck('office_code')->all();

                            $activeRefAgencyRow = getReferenceRows(ReferenceTable::REFERENCE_AGENCY_TABLE_NAME, false, Auth::user()->companyTaxId());

                            if (!is_null($activeRefAgencyRow)) {
                                $where = function ($q) use ($availableTaxIds, $activeRefAgencyRow) {
                                    $q->whereIn('code', $availableTaxIds)->where('agency', optional($activeRefAgencyRow)->id);
                                };
                            }

                        }
                    }
                }
            }

            $mdmId = (int)$xmlAttr[0]->attributes()->mdm;
        }

        // MDM
        if ($attributeType == Role::ROLE_ATTRIBUTE_TYPE_MDM) {
            $mdmId = optional(DocumentsDatasetFromMdm::select('mdm_field_id')->where('field_id', $attribute)->first())->mdm_field_id;

            if (count($authorizedMDMAttributesValues)) {
                $where = function ($q) use ($authorizedMDMAttributesValues) {
                    $q->whereIn('id', $authorizedMDMAttributesValues);
                };
            }
        }

        // PAYMENT AND OBLIGATION
        if ($attributeType == Role::ROLE_ATTRIBUTE_TYPE_PAYMENT_AND_OBLIGATION) {
            $activeRefAgencyRow = getReferenceRows(ReferenceTable::REFERENCE_AGENCY_TABLE_NAME, false, Auth::user()->companyTaxId());

            $result = [];
            switch ($attribute) {

                case Role::ROLE_ATTRIBUTES_PAYMENT_AND_OBLIGATION_SUBDIVISION:
                    $codes = RegionalOffice::select('id', 'office_code as code', 'name')->joinMl()->where('company_tax_id', $activeRefAgencyRow->code)->active()->pluck('code')->all();
                    $result = getReferenceRowsWhereIn(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS, [], $codes, ['id', 'code', 'name']);
                    break;

                case Role::ROLE_ATTRIBUTES_PAYMENT_AND_OBLIGATION_DOCUMENT:
                    $result = ConstructorDocument::select('id', 'document_code as code', 'document_name as name')->joinML()->where('company_tax_id', $activeRefAgencyRow->code)->active()->get();
                    break;
            }

            foreach ($result as $value) {
                $attributeValues[$attributeType][$value->id] = $value->code . ', ' . $value->name;
            }

            return $attributeValues;
        }

        //
        $dataModel = DataModel::where(DB::raw('id::varchar'), $mdmId)->first();

        if (!is_null($dataModel) && !empty($dataModel->reference)) {

            $result = getReferenceRows($dataModel->reference, false, false, '*', 'get', 'id', 'desc', $where);

            if (count($result) > 0) {
                foreach ($result as $value) {
                    $attributeValues[$attributeType][$value->id] = $value->code . ', ' . $value->name;
                }
            }
        }

        return $attributeValues;
    }

    /**
     * Function to get constructor document roles (mdm,saf)
     *
     * @param $roleIds
     * @param $attributeType
     * @param $authorizedSafAttributes
     * @param $authorizedMDMAttributes
     * @return ConstructorDocumentsRoles
     */
    private function constructorDocumentRoleByType($roleIds, $attributeType, $authorizedSafAttributes = [], $authorizedMDMAttributes = [])
    {
        $constructorDocumentRoles = ConstructorDocumentsRoles::whereIn('role_id', $roleIds)
            ->join('constructor_documents', 'constructor_documents.id', '=', 'constructor_documents_roles.document_id')
            ->join('constructor_documents_roles_attribute', 'constructor_documents_roles.id', '=', 'constructor_documents_roles_attribute.constructor_documents_roles_id')
            ->where('type', $attributeType)
            ->where('company_tax_id', Auth::user()->companyTaxId());

        if (count($authorizedMDMAttributes)) {
            $constructorDocumentRoles->whereIn('field_id', $authorizedMDMAttributes);
        }

        if (count($authorizedSafAttributes)) {
            $constructorDocumentRoles->whereIn('field_id', $authorizedSafAttributes);
        }

        return $constructorDocumentRoles->pluck('field_id');
    }

    /**
     * Function to search by menu and role
     *
     * @param $menuId
     * @return Role
     */
    public function searchRoleByMenu($menuId)
    {
        if (Auth::user()->companyTaxId()) {
            $roles = Role::select('id', 'name')->where(['company_tax_id' => Auth::user()->companyTaxId()]);
        } else {
            $roles = Role::select('id', 'name', 'menu_id')->where(['user_id' => Auth::id()]);
        }

        if ($menuId) {
            $roles->where(['menu_id' => $menuId]);

            if ($menuId != Menu::getMenuIdByName(Menu::USER_DOCUMENTS_MENU_NAME)) {

                if ($menuId == Menu::getMenuIdByName(Menu::AUTHORIZE_PERSON_MENU_NAME)) {
                    $roles->orWhereIn('id', [Role::AUTHORIZE_USER_ADD_MY_PERMISSIONS, Role::ALLOW_AUTHORIZE_USER_ADD_MY_PERMISSIONS]);
                } else {
                    $roles->orWhereIn('id', [Role::CRUD_ALL_FUNCTIONALITY]);

                    if (!in_array(Menu::getMenuNameById($menuId), Menu::MENUS_ONLY_HAS_ONLY_ALL_FUNCTIONALITY_ROLE)) {
                        $roles->orWhereIn('id', [Role::CRUD_ONLY_READ]);
                    }
                }
            }

        } else {
            $roles->orWhereIn('id', [Role::CRUD_ALL_FUNCTIONALITY, Role::CRUD_ONLY_READ, Role::AUTHORIZE_USER_ADD_MY_PERMISSIONS, Role::ALLOW_AUTHORIZE_USER_ADD_MY_PERMISSIONS]);
        }

        return $roles->active()->get()->unique();
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @param null $parentId
     * @return array
     */
    public function getLogData($id = null, $parentId = null)
    {
        if (!is_null($id)) {
            return Role::where('id', $id)->first();
        }

        return [];
    }
}
