<?php

namespace App\Models\Role;

use App\Models\BaseModel;
use App\Models\ConstructorDataSet\ConstructorDataSet;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\DataModel\DataModel;
use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm;
use App\Models\DocumentsDatasetFromSaf\DocumentsDatasetFromSaf;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Query\Builder;

/**
 * Class Role
 * @package App\Models\Role
 */
class Role extends BaseModel
{
    /**
     * @var string (Role Types)
     */
    const ROLE_TYPE_COMPANY = 'company';
    const ROLE_TYPE_NATURAL_PERSON = 'natural_person';
    const ROLE_TYPE_SW_ADMIN = 'sw_admin';
    const ROLE_TYPE_CUSTOMS_OPERATOR = 'customs_operator';

    /**
     * @var array
     */
    const ROLE_TYPES_ALL = [self::ROLE_TYPE_NATURAL_PERSON, self::ROLE_TYPE_COMPANY, self::ROLE_TYPE_SW_ADMIN, self::ROLE_TYPE_CUSTOMS_OPERATOR];

    /**
     * @var int (Default Roles List)
     */
    const SW_ADMIN = 1;
    const NATURAL_PERSON = 2;
    const HEAD_OF_COMPANY = 3;
    const AUTHORIZE_USER_ADD_MY_PERMISSIONS = 5;
    const CRUD_ALL_FUNCTIONALITY = 7;
    const CRUD_ONLY_READ = 8;
    const ALLOW_AUTHORIZE_USER_ADD_MY_PERMISSIONS = 9;
    const CUSTOMS_OPERATOR = 10;

    /**
     * @var array
     */
    const DEFAULT_ROLES_ALL = [self::SW_ADMIN, self::NATURAL_PERSON, self::HEAD_OF_COMPANY, self::ALLOW_AUTHORIZE_USER_ADD_MY_PERMISSIONS, self::CUSTOMS_OPERATOR];

    /**
     * @var array
     */
    const USER_DEFAULT_TYPES_ROLES = [self::SW_ADMIN, self::NATURAL_PERSON, self::HEAD_OF_COMPANY, self::CUSTOMS_OPERATOR];

    /**
     * @var string
     */
    const DEFAULT_ROLE_LEVEL = '1';
    const CUSTOM_ROLE_LEVEL = '2';

    /**
     * @var string
     */
    const ROLE_LEVEL_TYPE_DEFAULT = 'default';
    const ROLE_LEVEL_TYPE_CUSTOM = 'custom';

    /**
     * @var array
     */
    const ROLE_LEVEL_TYPES_ALL = [self::ROLE_LEVEL_TYPE_DEFAULT, self::ROLE_LEVEL_TYPE_CUSTOM];

    /**
     * @var string
     */
    const ROLE_ATTRIBUTES_PAYMENT_AND_OBLIGATION_SUBDIVISION = 'subdivision';
    const ROLE_ATTRIBUTES_PAYMENT_AND_OBLIGATION_DOCUMENT = 'document';

    /**
     * @var string
     */
    const ROLE_ATTRIBUTE_TYPE_SAF = 'saf';
    const ROLE_ATTRIBUTE_TYPE_MDM = 'mdm';
    const ROLE_ATTRIBUTE_TYPE_PAYMENT_AND_OBLIGATION = 'paymentAndObligation';

    /**
     * @var array
     */
    const ROLE_ATTRIBUTES_TYPE_ALL = [self::ROLE_ATTRIBUTE_TYPE_SAF, self::ROLE_ATTRIBUTE_TYPE_MDM, self::ROLE_ATTRIBUTE_TYPE_PAYMENT_AND_OBLIGATION];

    /**
     * @var string
     */
    public $table = 'roles';

    /**
     * @var bool
     */
    protected $hasAdminInfo = true;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'user_id',
        'company_tax_id',
        'menu_id',
        'show_status',
        'level',
        'create_permission',
        'read_permission',
        'update_permission',
        'delete_permission',
        'created_at'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_admin_id',
        'created_admin_ip',
        'updated_at',
        'updated_admin_id',
        'updated_admin_ip',
        'deleted_admin_id',
        'deleted_admin_ip'
    ];

    /**
     * Function to get created_at value
     *
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format(config('swis.date_time_format_front'));
    }

    /**
     * Function to return trans by role name if role is default
     *
     * @param $name
     * @return string
     */
    public function getNameAttribute($name)
    {
        if ($this->level == self::DEFAULT_ROLE_LEVEL) {
            $name = str_replace(' ', '_', strtolower($name));

            return trans('swis.roles.default.' . $name . '.name');
        }

        if (!is_null($this->current) && !is_null($this->current->name)) {
            return $this->current->name;
        }

        return $name;
    }

    /**
     * Function to return trans by role description if role is default
     *
     * @param $description
     * @return string
     */
    public function getDescriptionAttribute($description)
    {
        if ($this->level == self::DEFAULT_ROLE_LEVEL) {
            $name = str_replace(' ', '_', strtolower($this->getOriginal('name')));

            return trans('swis.roles.default.' . $name . '.description');
        }

        if (!is_null($this->current) && !is_null($this->current->description)) {
            return $this->current->description;
        }

        return $description;
    }

    /**
     * Function to return trans name for JOIN
     *
     * @return string
     */
    public function nameTrans()
    {
        return $this->name;
    }

    /**
     * Function to return trans name for JOIN
     *
     * @return string
     */
    public function descriptionTrans()
    {
        return $this->description;
    }

    /**
     * Function to scope Custom Roles
     *
     * @param $query
     * @return Builder
     */
    public function scopeCustomRoles($query)
    {
        return $query->where('level', '!=', self::DEFAULT_ROLE_LEVEL);
    }

    /**
     * Function to return ml
     *
     * @return HasMany
     */
    public function ml()
    {
        return $this->hasMany(RoleMl::class, 'role_id', 'id');
    }

    /**
     * Function to return current ml
     *
     * @return mixed
     */
    public function current()
    {
        return $this->hasOne(RoleMl::class, 'role_id', 'id')->notDeleted()->where('lng_id', cLng('id'));
    }

    /**
     * Function to get Role attribute values by attribute type(if user is employee show authorized roles only)
     *
     * @param $attributeFieldId
     * @param $attributeType
     * @param $attributeValue
     * @return array
     */
    public static function getRoleAttributeValue($attributeFieldId, $attributeType, $attributeValue)
    {
        $roleAttributes = [];
        $mdmId = null;

        // SAF
        if ($attributeType == self::ROLE_ATTRIBUTE_TYPE_SAF) {
            $getPathsByName = ConstructorDataSet::getGroupNamePath();
            $datasetFromSaf = DocumentsDatasetFromSaf::select('title_trans_key', 'xml_field_name')->where('saf_field_id', $attributeFieldId)->where('title_trans_key', '!=', '')->where('is_attribute', true)->first();
            $roleAttributes['attribute'] = trans($datasetFromSaf->title_trans_key) . " ({$getPathsByName[$datasetFromSaf->xml_field_name]})";

            $lastXml = SingleApplicationDataStructure::lastXml();
            $xmlAttr = $lastXml->xpath("//*[@id='{$attributeFieldId}']");
            $mdmId = (int)$xmlAttr[0]->attributes()->mdm;
        }

        // MDM
        if ($attributeType == self::ROLE_ATTRIBUTE_TYPE_MDM) {
            $dataSetMdm = DocumentsDatasetFromMdm::select('documents_dataset_from_mdm.*', 'documents_dataset_from_mdm_ml.label')
                ->leftJoin('documents_dataset_from_mdm_ml', 'documents_dataset_from_mdm_ml.documents_dataset_from_mdm_id', '=', 'documents_dataset_from_mdm.id')
                ->where('lng_id', cLng('id'))
                ->where('is_attribute', true)
                ->where('field_id', $attributeFieldId)
                ->first();

            $roleAttributes['attribute'] = !is_null($dataSetMdm) ? $dataSetMdm->label . ' (' . $dataSetMdm->group . ')' : '';

            $mdmId = optional(DocumentsDatasetFromMdm::select('mdm_field_id')->where('field_id', $attributeFieldId)->first())->mdm_field_id;
        }

        // PAYMENT AND OBLIGATION
        if ($attributeType == self::ROLE_ATTRIBUTE_TYPE_PAYMENT_AND_OBLIGATION) {
            switch ($attributeFieldId) {
                case Role::ROLE_ATTRIBUTES_PAYMENT_AND_OBLIGATION_SUBDIVISION:

                    $result = getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME, $attributeValue, false, ['id', 'code', 'name']);
                    if (!is_null($result)) {
                        $roleAttributes['attribute_value'] = $result->code . ', ' . $result->name;
                        $roleAttributes['attribute'] = trans('swis.role.attributes.subdivision');
                    }
                    break;
                case Role::ROLE_ATTRIBUTES_PAYMENT_AND_OBLIGATION_DOCUMENT:

                    $result = ConstructorDocument::select('id', 'document_code as code', 'document_name as name')->joinML()->where('id', $attributeValue)->first();
                    if (!is_null($result)) {
                        $roleAttributes['attribute_value'] = $result->code . ', ' . $result->name;
                        $roleAttributes['attribute'] = trans('swis.role.attributes.document');
                    }
                    break;
            }

            return $roleAttributes;
        }

        //
        if (!empty($attributeValue)) {
            $dataModelAllData = DataModel::allData();

            if (isset($dataModelAllData[$mdmId]) && $dataModelAllData[$mdmId]['reference']) {

                $rowRef = getReferenceRows($dataModelAllData[$mdmId]['reference'], $attributeValue);

                if (!is_null($rowRef)) {
                    $roleAttributes['attribute_value'] = $rowRef->code . ', ' . $rowRef->name;
                }
            }
        }

        return $roleAttributes;
    }
}
