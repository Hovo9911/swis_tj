<?php

namespace App\Models\Role;

use App\Http\Controllers\Core\DataTableSearch;
use Illuminate\Support\Facades\DB;

/**
 * Class RoleSearch
 * @package App\Models\Role
 */
class RoleSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);

        return $query->get();
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($this->orderByCol, $this->orderByType ?? 'desc');
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $query = Role::select('id', 'description', 'created_at', 'roles.show_status', 'role_ml.name as name')->customRoles()->joinMl(['t_ml' => 'role_ml', 'f_k' => 'role_id']);

        if (isset($this->searchData['id'])) {
            $query->where(DB::raw('id::varchar'), $this->searchData['id']);
        }

        if (isset($this->searchData['name'])) {
            $query->where('role_ml.name', 'ILIKE', '%' . strtolower(trim($this->searchData['name'])) . '%');
        }

        if (isset($this->searchData['created_at_start'])) {
            $query->where('roles.created_at', '>=', $this->searchData['created_at_start']);
        }

        if (isset($this->searchData['created_at_end'])) {
            $query->where('roles.created_at', '<=', $this->searchData['created_at_end']);
        }

        if (isset($this->searchData['create_permission'])) {
            $query->where('create_permission', $this->searchData['create_permission']);
        }

        if (isset($this->searchData['update_permission'])) {
            $query->where('update_permission', $this->searchData['update_permission']);
        }

        if (isset($this->searchData['delete_permission'])) {
            $query->where('delete_permission', $this->searchData['delete_permission']);
        }

        if (isset($this->searchData['menu'])) {
            $query->where('menu_id', $this->searchData['menu']);
        }

        if (isset($this->searchData['agency'])) {
            $query->where('company_tax_id', $this->searchData['agency']);
        }

        if (isset($this->searchData['show_status'])) {
            $query->where('roles.show_status', $this->searchData['show_status']);
        }

        $query->checkUserHasRole();

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}
