<?php

namespace App\Models\NotificationTemplates;

use App\Models\BaseMlManager;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorDocument\ConstructorDocumentMl;
use App\Models\Notifications\Notifications;
use App\Models\Notifications\NotificationsManager;
use App\Models\NotificationTemplatesSentAt\NotificationTemplatesSentAt;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplication\SingleApplication;
use App\Models\User\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class NotificationTemplatesManager
 * @package App\Models\NotificationTemplates
 */
class NotificationTemplatesManager
{
    use BaseMlManager;

    /**
     * Function to store Validated Data
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $data['user_id'] = Auth::id();
        $data['company_tax_id'] = Auth::user()->companyTaxId();

        $notificationTemplate = new NotificationTemplates($data);
        $notificationTemplateMl = new NotificationTemplatesMl();

        DB::transaction(function () use ($data, $notificationTemplate, $notificationTemplateMl) {
            $notificationTemplate->save();
            $this->storeMl($data['ml'], $notificationTemplate, $notificationTemplateMl);
        });
    }

    /**
     * Function to update data
     *
     * @param array $data
     * @param $id
     */
    public function update(array $data, $id)
    {
        $notificationTemplate = NotificationTemplates::where('id', $id)->notificationTemplateOwner()->firstOrFail();
        $notificationTemplateMl = new NotificationTemplatesMl();

        DB::transaction(function () use ($data, $notificationTemplate, $notificationTemplateMl) {
            $notificationTemplate->update($data);
            $this->updateMl($data['ml'], 'notification_template_id', $notificationTemplate, $notificationTemplateMl);
        });
    }

    /**
     * Function to Delete data
     *
     * @param array $deleteItems
     */
    public function delete(array $deleteItems)
    {
        DB::transaction(function () use ($deleteItems) {

            NotificationTemplates::where('id', $deleteItems)->update([
                'show_status' => NotificationTemplates::STATUS_DELETED
            ]);

            NotificationTemplatesMl::where('notification_template_id', $deleteItems)->update([
                'show_status' => NotificationTemplatesMl::STATUS_DELETED
            ]);

        });
    }

    /**
     * Function to send global notification by user groups
     *
     * @param $usersGroup
     * @param $userType
     * @param $notificationTemplateId
     * @param null $subApplication
     */
    public function sendGlobalNotifications($usersGroup, $userType, $notificationTemplateId, $subApplication = null)
    {
        $notificationTemplate = NotificationTemplates::where('id', $notificationTemplateId)
            ->leftJoin('notification_templates_ml', 'notification_templates.id', '=', 'notification_templates_ml.notification_template_id')
            ->get();

        $notificationsData = [];

        if (!is_null($subApplication)) {
            $saf = SingleApplication::select('id', 'regime')->where('regular_number', $subApplication->saf_number)->first();

            //Sub Application URL
            $subApplicationUrlInApp = $this->getSubApplicationUrlByUserType($userType, $subApplication);

            //SAF URL
            $safUrlInApp = $this->getSafUrlByUserType($userType, $saf, $subApplication);

            $documentTypeId = ConstructorDocument::where('id', $subApplication->document_id)->pluck('document_type_id')->first();
            //Notify hours age
            $notifyHoursAgoRef = DB::table(ReferenceTable::REFERENCE_DOCUMENT_TYPE_REFERENCE)
                ->where(['id' => $documentTypeId])
                ->pluck('notify_hours_ago')
                ->first();

            $notifyHoursAgo = $notifyHoursAgoRef ?? 0;
        }

        foreach ($usersGroup as $user) {

            $transKeys = ["{{USER_FULLNAME}}" => $user->first_name . " " . $user->last_name];

            if (!is_null($subApplication)) {
                $docNameRef = ConstructorDocumentML::select('constructor_documents_id', 'document_name')
                    ->where('constructor_documents_id', $subApplication->document_id)
                    ->where('lng_id', ($user->lng_id ?? cLng('id')))
                    ->first();

                $docName = optional($docNameRef)->document_name ?? '';

                $transKeys = [
                    "{{DOCUMENT_NAME}}" => $docName ?? '',
                    "{{SUB_APPLICATION_ID}}" => $subApplicationUrlInApp ?? '',
                    "{{SAF}}" => $safUrlInApp ?? '',
                    "{{HOURS}}" => $notifyHoursAgo ?? '',
                ];

                $smsTransKeys = [
                    "{{DOCUMENT_NAME}}" => $docName ?? '',
                    "{{SUB_APPLICATION_ID}}" => $subApplication->document_number ?? '',
                    "{{SAF}}" => $saf->regular_number ?? '',
                    "{{HOURS}}" => $notifyHoursAgo ?? '',
                ];
            }

            $inApplicationNotification = $emailNotification = $smsNotification = $emailSubjectNotification = [];
            foreach ($notificationTemplate as $notificationTemplateByLngId) {
                $inApplicationNotification[$notificationTemplateByLngId->lng_id] = [
                    'in_app_notification' => Notifications::replaceVariableToValue($transKeys, $notificationTemplateByLngId->in_app_notification, $notificationTemplateByLngId->lng_id)
                ];

                $emailSubjectNotification[$notificationTemplateByLngId->lng_id] = strip_tags(Notifications::replaceVariableToValue(($smsTransKeys ?? $transKeys), $notificationTemplateByLngId->email_subject, ($user->lng_id ?? cLng('id'))));
                $emailNotification[$notificationTemplateByLngId->lng_id] = Notifications::replaceVariableToValue($transKeys, $notificationTemplateByLngId->email_notification, ($user->lng_id ?? cLng('id')));
                $smsNotification[$notificationTemplateByLngId->lng_id] = strip_tags(Notifications::replaceVariableToValue(($smsTransKeys ?? $transKeys), $notificationTemplateByLngId->sms_notification, ($user->lng_id ?? cLng('id'))));
            }

            $item['notification_id'] = $notificationTemplateId;
            $item['user_id'] = ($userType != User::USER_TYPE_ALL_USERS) ? $user['id'] : $user['user_id'];
            $item['company_tax_id'] = $user['company_tax_id'] ?? 0;
            $item['email_subject'] = $emailSubjectNotification[$user->lng_id];
            $item['email_notification'] = $emailNotification[$user->lng_id];
            $item['sms_notification'] = $smsNotification[$user->lng_id];
            $item['read'] = Notifications::NOTIFICATION_STATUS_UNREAD;
            $item['show_status'] = Notifications::STATUS_ACTIVE;
            $item['notification_type'] = $this->notificationType($userType);

            // Store in app notifications
            NotificationsManager::storeInAppNotifications($item, $inApplicationNotification);

            $item['user_phone_number'] = trim($user['phone_number']);
            $item['user_email'] = $user['email'];
            $item['user_email_status'] = $user['email_notification'];
            $item['user_sms_status'] = $user['sms_notification'];
            $notificationsData[] = $item;
        }

        // Store email and sms
        NotificationsManager::sendUsersGroupEmailAndSms($notificationsData);
    }

    /**
     * Function to save notification sent at time
     *
     * @param $notificationTemplateId
     */
    public function globalNotificationSentAt($notificationTemplateId)
    {
        NotificationTemplatesSentAt::create([
            'user_id' => Auth::id(),
            'notification_templates_id' => $notificationTemplateId,
            'sent_at' => currentDateTime()
        ]);
    }

    /**
     * Function to get notification type by user_type
     *
     * @param $userType
     * @return string
     */
    private function notificationType($userType)
    {
        switch ($userType) {
            case User::USER_TYPE_BROKER:
                return Notifications::NOTIFICATION_TYPE_GLOBAL_BROKER_TEMPLATE;
                break;
            case User::USER_TYPE_SW_ADMIN:
                return Notifications::NOTIFICATION_TYPE_SW_ADMIN;
                break;
            case User::SUB_APPLICATION_CURRENT_STATE_USERS:
                return Notifications::NOTIFICATION_TYPE_PERSON_IN_NEXT_STATE;
                break;
            case User::AGENCY_HEADS:
                return Notifications::NOTIFICATION_TYPE_AGENCY_ADMIN;
                break;
            case User::SAF_APPLICANT:
                return Notifications::NOTIFICATION_TYPE_APPLICANT;
                break;
            default:
                return Notifications::NOTIFICATION_TYPE_GLOBAL_TEMPLATE;
                break;
        }
    }

    /**
     * Function to return saf url in app by user_type
     *
     * @param $userType
     * @param $subApplication
     * @param $saf
     * @return string
     */
    private function getSafUrlByUserType($userType, $saf, $subApplication)
    {
        switch ($userType) {

            case User::SAF_APPLICANT:
                //SAF URL
                $safUrl = Notifications::IN_APP_URL_PREFIX . '/single-application/view/' . $saf->regime . '/' . $subApplication->saf_number . '';
                $safUrlInApp = "<a target='_blank' href='" . $safUrl . "' class='mark-as-read'>" . $subApplication->document_number . "</a>";
                break;
            default:
                $safUrlInApp = $subApplication->document_number;
                break;
        }

        return $safUrlInApp;
    }

    /**
     * Function to return sub application url in app by user_type
     *
     * @param $userType
     * @param $subApplication
     * @return string
     */
    private function getSubApplicationUrlByUserType($userType, $subApplication)
    {
        switch ($userType) {
            case User::SUB_APPLICATION_CURRENT_STATE_USERS:
            case User::AGENCY_HEADS:
                //SAF URL
                $prefix = Notifications::IN_APP_URL_PREFIX . '/user-documents/' . $subApplication->document_id . '/application/' . $subApplication->id . '/edit';
                $subApplicationUrlInApp = "<a target='_blank' href='" . $prefix . "' class='mark-as-read'>" . $subApplication->document_number . "</a>";
                break;
            default:
                $subApplicationUrlInApp = $subApplication->document_number;
                break;
        }

        return $subApplicationUrlInApp;
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @param null $id2
     * @return array
     */
    public function getLogData($id, $id2 = null)
    {
        $data = NotificationTemplates::where('id', $id)->first()->toArray();
        $data['mls'] = NotificationTemplatesMl::where('notification_template_id', $id)->get()->toArray();

        return $data;
    }
}
