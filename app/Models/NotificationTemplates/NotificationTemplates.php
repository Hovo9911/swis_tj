<?php

namespace App\Models\NotificationTemplates;

use App\Models\BaseModel;
use App\Models\NotificationTemplatesSentAt\NotificationTemplatesSentAt;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Auth;

/**
 * Class NotificationTemplates
 * @package App\Models\NotificationTemplates
 */
class NotificationTemplates extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'notification_templates';

    /**
     * @var string
     */
    const SEND_MESSAGE_FROM_SAF = 'send_message_from_saf';
    const SEND_MESSAGE_FROM_SUB_APPLICATION = 'send_message_from_application';

    /**
     * @var string
     */
    const DOCUMENT_CONFIRMER = 'document_confirmer';
    const DOCUMENT_NOTIFICATION_IN_CONFIRM_STATUS = 'document_in_confirm_status';
    const DOCUMENT_NOTIFICATION_IN_REFUSE_STATUS = 'document_in_refuse_status';
    const DOCUMENT_NOTIFICATION_IN_REJECT_STATUS = 'document_in_reject_status';

    /**
     * @var string
     */
    const SUB_APPLICATION_NOTIFICATION_TEMPLATE_NAME = 'sub_application_send_notifications';
    const PAYMENT_NOTIFICATION_TEMPLATE = 'payment_send_notification';
    const OBLIGATION_GENERATING_NOTIFICATION_TEMPLATE = 'obligation_generating_send_notification';
    const PASSWORD_CHANGED_NOTIFICATION_TEMPLATE_NAME = 'password_changed_send_notification';
    const REFERENCE_TABLE_CHANGES_TEMPLATE = 'reference_table_changes_notification';
    const OBLIGATION_PAYED_TEMPLATE = 'obligation_payed_notification';
    const BALANCE_MINUS_TEMPLATE = 'balance_minus_notification';
    const USER_INFO_CHANGED = 'user_info_changed';

    /**
     * @var string (Notification templates to notify sub application expiration date)
     */
    const SUB_APPLICATION_EXPIRATION_DATE_TEMPLATE_FOR_SAF_APPLICANT = 'sub_application_expiration_date_template_for_saf_applicant';
    const SUB_APPLICATION_EXPIRATION_DATE_TEMPLATE_FOR_STATE_USERS = 'sub_application_expiration_date_template_for_state_users';
    const SUB_APPLICATION_EXPIRATION_DATE_TEMPLATE_FOR_AGENCY_HEADS = 'sub_application_expiration_date_template_for_agency_heads';
    const SUB_APPLICATION_EXPIRATION_DATE_TEMPLATE_FOR_SW_ADMINS = 'sub_application_expiration_date_template_for_sw_admins';

    /**
     * @var string(New authorization from profile settings page)
     */
    const NEW_AUTHORIZATION_FOR_EMPLOYEE_FROM_PS = 'new_authorization_for_employee';

    /**
     * @var string (new authorization from Authorized person module)
     */
    const NEW_AUTHORIZATION_FOR_EMPLOYEE_FROM_AP = 'new_authorization_for_employee_from_ap';

    /**
     * @var bool
     */
    protected $hasAdminInfo = true;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'company_tax_id',
        'read',
        'locked',
        'show_status'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'created_admin_id',
        'created_admin_ip',
        'updated_at',
        'updated_admin_id',
        'updated_admin_ip',
        'deleted_admin_id',
        'deleted_admin_ip'
    ];

    /**
     * Function to relate with NotificationTemplatesMl
     *
     * @return HasMany
     */
    public function ml()
    {
        return $this->hasMany(NotificationTemplatesMl::class, 'notification_template_id', 'id');
    }

    /**
     * Function to get last sent time
     *
     * @return HasOne
     */
    public function lastSent()
    {
        return $this->hasOne(NotificationTemplatesSentAt::class, 'notification_templates_id', 'id')->orderByDesc('id');
    }

    /**
     * Function to check auth user has role
     *
     * @param $query
     * @return Builder
     */
    public function scopeNotificationTemplateOwner($query)
    {
        $companyTaxId = -1;
        if (Auth::user()->isAgency()) {
            $companyTaxId = Auth()->user()->companyTaxId();
        } elseif (Auth::user()->isSwAdmin()) {
            $companyTaxId = 0;
        }

        return $query->where($this->getTable() . '.company_tax_id', $companyTaxId);
    }

    /**
     * Function by code get notification templates
     *
     * @param $code
     * @return NotificationTemplates
     */
    public static function mlByCode($code)
    {
        return NotificationTemplates::where('code', $code)
            ->leftJoin('notification_templates_ml', 'notification_templates.id', '=', 'notification_templates_ml.notification_template_id')
            ->get();
    }

    /**
     * Function to return notification id by code
     *
     * @param $notificationCode
     * @return integer
     */
    public static function getNotificationIdByCode($notificationCode)
    {
        return (new self)->where('code', $notificationCode)->pluck('id')->first();
    }
}
