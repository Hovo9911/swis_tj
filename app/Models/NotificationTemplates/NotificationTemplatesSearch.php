<?php

namespace App\Models\NotificationTemplates;

use App\Http\Controllers\Core\DataTableSearch;
use Illuminate\Support\Facades\DB;

/**
 * Class NotificationTemplatesSearch
 * @package App\Models\NotificationTemplates
 */
class NotificationTemplatesSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);

        $data = $query->get();

        foreach ($data as &$value){
            $value->sent_at = $value->lastSent->sent_at ?? '';
        }

        return $data;
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($this->orderByCol, $this->orderByType ?? 'desc');
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $query = NotificationTemplates::with(['lastSent'])->select('*')->joinMl(['f_k' => 'notification_template_id'])->notDeleted();

        if (isset($this->searchData['id'])) {
            $query->where(DB::raw("notification_templates.id::varchar"), $this->searchData['id']);
        }

        if (isset($this->searchData['name'])) {
            $query->where('notification_templates.name', $this->searchData['name']);
        }

        $query->checkUserHasRole();
        $query->notificationTemplateOwner();

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}
