<?php

namespace App\Models\NotificationTemplates;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

/**
 * Class NotificationTemplatesMl
 * @package App\Models\NotificationTemplates
 */
class NotificationTemplatesMl extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['notification_template_id', 'lng_id'];

    /**
     * @var string
     */
    public $incrementing = 'false';

    /**
     * @var string
     */
    public $table = 'notification_templates_ml';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'notification_template_id',
        'lng_id',
        'sub_application_id',
        'email_subject',
        'email_notification',
        'in_app_notification',
        'sms_notification',
        'show_status'
    ];
}
