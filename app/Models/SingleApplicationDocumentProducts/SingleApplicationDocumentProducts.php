<?php

namespace App\Models\SingleApplicationDocumentProducts;

use App\Models\BaseModel;
use App\Models\Document\Document;
use App\Models\Traits\HasCompositePrimaryKey;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Query\Builder;

/**
 * Class SingleApplicationDocumentProducts
 * @package App\Models\SingleApplicationDocumentProducts
 */
class SingleApplicationDocumentProducts extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['saf_number', 'product_id'];

    /**
     * @var string
     */
    public $table = 'saf_document_products';

    /**
     * @var array
     */
    protected $fillable = [
        'company_tax_id',
        'user_id',
        'creator_user_id',
        'saf_document_id',
        'product_id',
        'saf_number',
    ];

    /**
     * Function to relate with Document
     * @return HasOne
     */
    public function document()
    {
        return $this->hasOne(Document::class, 'id', 'saf_document_id');
    }

    /**
     * Function to relate with Products
     *
     * @param $query
     * @param $safNumber
     * @param $safDocumentId
     * @return Builder
     */
    public function scopeProducts($query, $safNumber, $safDocumentId)
    {
        return $query->where(['saf_number' => $safNumber, 'saf_document_id' => $safDocumentId]);
    }

}
