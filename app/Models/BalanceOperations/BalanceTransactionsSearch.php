<?php

namespace App\Models\BalanceOperations;

use App\Http\Controllers\Core\DataTableSearch;
use App\Models\Agency\Agency;
use App\Models\Transactions\Transactions;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * Class BalanceTransactionsSearch
 * @package App\Models\BalanceOperations
 */
class BalanceTransactionsSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->rowsCountAll();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->rowsCountAll();
    }

    /**
     * @return int
     */
    private function rowsCountAll()
    {
        $query = $this->constructQuery();
        $result = DB::select("SELECT count(*) as cnt FROM ({$query->toSql()}) as temp", $query->getBindings());

        return $result[0]->cnt;
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        $data = $query->get();

        foreach ($data as $item) {
            $item->payment_date = formattedDate(Carbon::parse($item->payment_date)->format(config('swis.date_time_format')), true);
        }

        return $data;
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($this->orderByCol, $this->orderByType ?? 'desc');
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $data = $this->searchData;

        $query = Transactions::select([
            'transactions.id',
            DB::raw("concat(users.first_name, ' ', users.last_name) as username"),
            'users.ssn as user_ssn',
            'transactions.transaction_date as payment_date',
            'transactions.amount as sum',
            'company.tax_id as ssn',
            DB::raw("COALESCE(agency_ml.name, company.name) as company_name"),
        ])
            ->leftJoin('users', 'users.id', '=', 'transactions.user_id')
            ->leftJoin('company', 'company.tax_id', '=', 'transactions.company_tax_id')
            ->leftJoin('agency', 'agency.tax_id', '=', 'transactions.company_tax_id')
            ->leftJoin('agency_ml', function ($q) {
                $q->on('agency_ml.agency_id', '=', 'agency.id')->where('agency_ml.lng_id', cLng('id'));
            })
            ->when(isset($data['transactions_search_id']), function ($q) use ($data) {
                $q->where('transactions.id', $data['transactions_search_id']);
            })
            ->when(isset($data['ssn']), function ($q) use ($data) {
                $q->where('transactions.company_tax_id', $data['ssn']);
            })
            ->when(isset($data['company']), function ($q) use ($data) {
                $q->where('company.id', $data['company']);
            })
            ->when(isset($data['transaction_date_start']), function ($q) use ($data) {
                $q->where(DB::raw('transactions.transaction_date::date'), '>=', $data['transaction_date_start']);
            })
            ->when(isset($data['transaction_date_end']), function ($q) use ($data) {
                $q->where(DB::raw('transactions.transaction_date::date'), '<=', $data['transaction_date_end']);
            })
            ->when(isset($data['sum_min']), function ($q) use ($data) {
                $q->where(DB::raw("ABS(transactions.amount)"), '>=', $data['sum_min']);
            })
            ->when(isset($data['sum_max']), function ($q) use ($data) {
                $q->where(DB::raw("ABS(transactions.amount)"), '<=', $data['sum_max']);
            })
            ->when(isset($data['username']), function ($q) use ($data) {
                $q->orWhere('users.first_name', "ILIKE", "%{$data['username']}%")->orWhere('users.last_name', "ILIKE", "%{$data['username']}%");
            })
            ->where('is_refund', true);

        // NEED for now remove agencies ,in future maybe open
        $agenciesTaxIds = Agency::active()->pluck('tax_id')->all();
        $query->whereNotIn('transactions.company_tax_id', $agenciesTaxIds);

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}
