<?php

namespace App\Models\BalanceOperations;

use App\Http\Controllers\Core\DataTableSearch;
use App\Models\Agency\Agency;
use App\Models\Company\Company;
use Illuminate\Support\Facades\DB;

/**
 * Class BalanceOperationsSearch
 * @package App\Models\BalanceOperations
 */
class BalanceOperationsSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->rowsCountAll();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->rowsCountAll();
    }

    /**
     * @return int
     */
    private function rowsCountAll()
    {
        $query = $this->constructQuery();
        $result = DB::select("SELECT count(*) as cnt FROM ({$query->toSql()}) as temp", $query->getBindings());

        return $result[0]->cnt;
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        $data = $query->get();

        $result = [];
        foreach ($data as $key => $item) {
            $getBalance = $item->balance($item->ssn);
            if (isset($this->searchData['balance_min']) && isset($this->searchData['balance_max']) && ($getBalance < $this->searchData['balance_min'] || $getBalance > $this->searchData['balance_max'])) {
                continue;
            } elseif (isset($this->searchData['balance_min']) && !isset($this->searchData['balance_max']) && $getBalance < $this->searchData['balance_min']) {
                continue;
            } elseif (!isset($this->searchData['balance_min']) && isset($this->searchData['balance_max']) && $getBalance > $this->searchData['balance_max']) {
                continue;
            }
            $item->balance = $getBalance;
            $result[] = $item;
        }

        return $result;
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($this->orderByCol, $this->orderByType ?? 'desc');
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $agencies = Agency::active()->pluck('tax_id')->toArray();

        $data = $this->searchData;
        return Company::select('company.id', 'company.name as company_name', 'company.tax_id as ssn')
            ->join('transactions', 'transactions.company_tax_id', '=', 'company.tax_id')
            ->whereNotIn('tax_id', $agencies)
            ->when(isset($data['balance_search_id']), function ($q) use ($data) {
                $q->where('company.id', $data['balance_search_id']);
            })
            ->when(isset($data['balance_search_ssn']), function ($q) use ($data) {
                $q->where('company.tax_id', 'ILIKE', "%{$data['balance_search_ssn']}%");
            })
            ->when(isset($data['balance_search_company']), function ($q) use ($data) {
                $q->where('company.id', $data['balance_search_company']);
            })
            ->groupBy('company.id', 'company.name', 'company.tax_id');
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}
