<?php

namespace App\Models\BalanceOperations;

use App\Models\Company\Company;
use App\Models\Notifications\Notifications;
use App\Models\Notifications\NotificationsManager;
use App\Models\NotificationTemplates\NotificationTemplates;
use App\Models\User\User;
use App\Models\UserRoles\UserRoles;
use Illuminate\Support\Facades\Auth;

/**
 * Class BalanceOperationsNotificationsManager
 * @package App\Models\BalanceOperations
 */
class BalanceOperationsNotificationsManager
{
    /**
     * Function to Store Notifications and Send Email and SMS
     *
     * @param $companyTaxId
     * @param $amount
     */
    public function storeNotification($companyTaxId, $amount)
    {

        $users = UserRoles::select('user_id as id', 'company_tax_id', 'first_name', 'last_name', 'email', 'phone_number', 'email_notification', 'sms_notification', 'lng_id')
            ->join('users', 'users.id', '=', 'user_roles.user_id')
            ->where('company_tax_id', $companyTaxId)
            ->where('users.show_status', User::STATUS_ACTIVE)
            ->activeRole()
            ->groupBy('user_id', 'company_tax_id', 'first_name', 'last_name', 'email', 'phone_number', 'email_notification', 'sms_notification', 'lng_id')
            ->get();

        $notificationTemplatesAll = NotificationTemplates::mlByCode(NotificationTemplates::BALANCE_MINUS_TEMPLATE);

        if ($notificationTemplatesAll->count()) {
            $constructorNotificationCurrentLng = $notificationTemplatesAll->where('lng_id', cLng('id'))->first();
            $company = Company::getCompanyByTaxID($companyTaxId);

            foreach ($users as $user) {

                $contentVariables = [
                    "{{USER_FULLNAME}}" => $user->first_name . " " . $user->last_name,
                    "{{AMOUNT}}" => $amount,
                    "{{COMPANY_NAME}}" => optional($company)->name,
                    "{{ACCOUNTANT_USER_FULLNAME}}" => Auth::user()->name() . " (" . Auth::user()->ssn . ")"
                ];

                //
                $notification = [
                    'notification_id' => $constructorNotificationCurrentLng->id,
                    'user_id' => $user->id,
                    'company_tax_id' => $companyTaxId,
                    'notification_type' => Notifications::NOTIFICATION_TYPE_CUSTOM_TEMPLATE,
                    'email_subject' => strip_tags(Notifications::replaceVariableToValue($contentVariables, $constructorNotificationCurrentLng->email_subject, ($user->lng_id ?? cLng('id')))),
                    'email_notification' => Notifications::replaceVariableToValue($contentVariables, $constructorNotificationCurrentLng->email_notification, ($user->lng_id ?? cLng('id'))),
                    'sms_notification' => strip_tags(Notifications::replaceVariableToValue($contentVariables, $constructorNotificationCurrentLng->sms_notification, ($user->lng_id ?? cLng('id')))),
                ];

                //
                $inApplicationNotes = [];
                foreach ($notificationTemplatesAll as $constructorNote) {
                    $inApplicationNotes[$constructorNote->lng_id] = [
                        'in_app_notification' => Notifications::replaceVariableToValue($contentVariables, $constructorNote->in_app_notification, $constructorNote->lng_id)
                    ];
                }

                // Store in app notifications
                NotificationsManager::storeInAppNotifications($notification, $inApplicationNotes);

                // Send Email And SMS
                NotificationsManager::sendEmailAndSms($user, $notification);
            }
        }
    }
}