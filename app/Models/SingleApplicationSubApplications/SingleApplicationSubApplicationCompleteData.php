<?php


namespace App\Models\SingleApplicationSubApplications;

use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\DataModel\DataModel;
use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm;
use App\Models\Routing\Routing;
use App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure;

/**
 * Class SingleApplicationSubApplicationCompleteData
 * @package App\Models\SingleApplicationSubApplications
 */
class SingleApplicationSubApplicationCompleteData
{
    private $saf;
    private $subApplication;
    private $products;
    private $subAppProductsIds;
    private $lngId;
    private $refColumn;
    private $mdmData;
    private $safIdNameArray;
    private $xml;
    private $controlIdNameArray;
    private $inLineParameters = false;

    /**
     * SingleApplicationSubApplicationCompleteData constructor.
     *
     * @param $subApplication
     * @param $lngId
     * @param string $refColumn
     */
    public function setApplicationVariable($subApplication, $lngId = null, $refColumn = 'name')
    {
        $this->saf = $subApplication->saf;
        $this->subApplication = $subApplication;
        $this->products = $subApplication->productList;
        $this->subAppProductsIds = $subApplication->productList->pluck('id')->all();
        $this->lngId = $lngId ?? cLng('id');
        $this->refColumn = $refColumn;
    }

    /**
     * Function to set data
     *
     * @return array
     */
    public function setData()
    {
        $returnMainData = $returnProductsData = $returnProductBatchesData = $routingValues = $routingProductValues = [];
        $getSafIdNameArray = $this->safIdNameArray;
        $applicationDataByPrefix = prefixKey('', $this->subApplication->data);
        $safControlledFields = !is_null($this->saf->saf_controlled_fields) ? prefixKey('', $this->saf->saf_controlled_fields) : [];
        $safProducts = $this->products->keyBy('id')->toArray();
        $subApplicationRoutings = is_array($this->subApplication->routings) ? $this->subApplication->routings : [];
        $routingProducts = is_array($this->subApplication->routing_products) ? $this->subApplication->routing_products : [];

        foreach ($subApplicationRoutings as $item) {
            $routing = $this->routings[$item] ?? [];
            if (!is_null($routing['saf_controlled_fields'])) {
                foreach ($routing['saf_controlled_fields'] as $fieldId => $safControlledField) {
                    if (array_key_exists('make_visible', $safControlledField) && array_key_exists($fieldId, $this->safControlIdNameArray)) {
                        $trimFieldName = str_replace("][", ".", $this->safControlIdNameArray[$fieldId]);
                        $trimFieldName = str_replace("[", ".", $trimFieldName);
                        $trimFieldName = str_replace("]", "", $trimFieldName);

                        if (array_key_exists($trimFieldName, $safControlledFields)) {
                            $routingValues[$this->safControlIdNameArray[$fieldId]] = $safControlledFields[$trimFieldName];
                        }
                    }
                }
            }
        }

        foreach ($routingProducts as $items) {
            foreach ($items as $productId) {
                unset($safProducts[$productId]['id']);
                unset($safProducts[$productId]['batches']);
                if (isset($safProducts[$productId]) && !is_null($safProducts[$productId])) {
                    $routingProductValues[$productId] = prefixKey('', $safProducts[$productId]);
                }
            }
        }

        foreach ($getSafIdNameArray as $fieldId => $fieldName) {
            if (!in_array($fieldId, $this->safControlIdNameArray)) {
                $fieldName = str_replace('[', '.', str_replace(']', '', str_replace('][', '.', $fieldName)));
                $fieldValue = isset($applicationDataByPrefix[$fieldName]) ? $applicationDataByPrefix[$fieldName] : '';
            } else {
                $fieldValue = isset($routingValues[$fieldName]) ? $routingValues[$fieldName] : '';
            }

            $mdm = (int)$this->xml->xpath("//*[@id='{$fieldId}']")[0]->attributes()->mdm;
            if (isset($this->references, $this->references[$mdm]) && $reference = $this->mdmData[$mdm]['reference']) {
                $fieldValue = optional($this->references[$reference][$mdm][$fieldValue])->{$this->refColumn};
            }
            $returnMainData[$fieldId] = $fieldValue;
        }

        if ($this->subApplication->constructor_document_id && isset($this->mdmFields[$this->subApplication->constructor_document_id]['data'])) {
            foreach ($this->mdmFields[$this->subApplication->constructor_document_id]['data'] as $field => $value) {
                $returnMainData[$field] = $this->subApplication->custom_data[$field] ?? "";
            }
        }

        foreach ($this->products as $product) {
            foreach (getSafProductsIdNameArray() as $fieldId => $fieldName) {
                $returnProductsData[$product->id]["product_id"] = $product->id;
                $fieldName = str_replace('[', '.', str_replace(']', '', str_replace('][', '.', $fieldName)));
                if (isset($routingProductValues[$product->id]) && in_array($fieldName, $routingProductValues[$product->id])) {
                    $returnProductsData[$product->id][$fieldId] = $routingProductValues[$product->id][$fieldName] ?? "";
                } else {
                    $returnProductsData[$product->id][$fieldId] = $product->{$fieldName};
                }
            }

            if ($this->subApplication->constructor_document_id && isset($this->mdmFields[$this->subApplication->constructor_document_id]['products'])) {//21798
                foreach ($this->mdmFields[$this->subApplication->constructor_document_id]['products'] as $field => $value) {
                    $returnProductsData[$product->id][$field] = $this->subApplication->custom_data["{$field}_pr_{$product->id}"] ?? "";
                }
            }


            foreach ($product->batches as $batch) {

                $returnProductBatchesData[$batch->id]["product_id"] = $product->id;
                $returnProductBatchesData[$batch->id]["batch_id"] = $batch->id;
                foreach (getSafProductsBatchesIdNameArray() as $fieldId => $fieldName) {
                    $returnProductBatchesData[$batch->id][$fieldId] = $batch[$fieldName] ?? "";
                }

                if ($this->subApplication->constructor_document_id && isset($this->mdmFields[$this->subApplication->constructor_document_id]['batches'])) {//21798
                    foreach ($this->mdmFields[$this->subApplication->constructor_document_id]['batches'] as $field => $value) {
                        $returnProductBatchesData[$batch->id][$field] = $batch->mdm_data[$batch->id][$field] ?? "";
                    }
                }
            }
        }

        return [$returnMainData, $returnProductsData, $returnProductBatchesData];
    }

    /**
     * Function to set inLineParameters true
     */
    public function getParamsInline()
    {
        $this->inLineParameters = true;
    }

    /**
     * Function to set params
     */
    public function getParams()
    {
        $dataModel = DataModel::allData();
        $safIdNameArray = getSafIdNameArray();
        $getSafControlIdNameArray = array_keys(getSafControlFieldIdNameArray());
        $routings = Routing::select('id', 'saf_controlled_fields')->get()->keyBy('id')->toArray();

        $references = [];
        $lastXML = SingleApplicationDataStructure::lastXml();
        foreach ($safIdNameArray as $fieldId => $fieldName) {
            $mdm = (int)$lastXML->xpath("//*[@id='{$fieldId}']")[0]->attributes()->mdm;
            if (!empty($mdm) && array_key_exists($mdm, $dataModel) && !empty($dataModel[$mdm]['reference']) && $reference = $dataModel[$mdm]['reference']) {
                $references[$reference] = getReferenceRows($reference, false, false, ['id', 'code', 'name'], 'get', false, false, [], false)->keyBy('id')->toArray();
            }
        }

        $mdmFields = [];
        $documents = ConstructorDocument::where('show_status', '1')->get();
        foreach ($documents as $document) {
            $fields = DocumentsDatasetFromMdm::select('id', 'field_id', 'xpath')->where('document_id', $document->id)->get();
            foreach ($fields as $field) {
                if ($field->xpath == "tab[@key='products']/block[@name='swis.single_app.products_list']") {
                    $mdmFields[$document->id]['products'][$field->field_id] = null;
                } elseif ($field->xpath == "tab[@key='products']/block[@name='swis.single_app.products_list']/subBlock[@name='swis.single_app.batches']") {
                    $mdmFields[$document->id]['batches'][$field->field_id] = null;
                } else {
                    $mdmFields[$document->id]['data'][$field->field_id] = null;
                }
            }
        }

        $this->mdmData = $dataModel;
        $this->safIdNameArray = $safIdNameArray;
        $this->xml = SingleApplicationDataStructure::lastXml();
        $this->safControlIdNameArray = $getSafControlIdNameArray;
        $this->references = $references;
        $this->routings = $routings;
        $this->mdmFields = $mdmFields;
    }
}