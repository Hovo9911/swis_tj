<?php

namespace App\Models\SingleApplicationSubApplications;

use App\Models\Agency\Agency;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\Notifications\NotificationsManager;
use App\Models\NotifySubApplicationExpirationDate\NotifySubApplicationExpirationDateManager;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\Routing\Routing;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationDocuments\SingleApplicationDocuments;
use App\Models\SingleApplicationDocumentSubApplications\SingleApplicationDocumentSubApplications;
use App\Models\SingleApplicationNotes\SingleApplicationNotes;
use App\Models\SingleApplicationObligations\SingleApplicationObligations;
use App\Models\SingleApplicationProducts\SingleApplicationProductsManager;
use App\Models\SingleApplicationSubApplicationProducts\SingleApplicationSubApplicationProducts;
use App\Models\SingleApplicationSubApplicationStates\SingleApplicationSubApplicationStates;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class SingleApplicationSubApplicationsManager
 * @package App\Models\SingleApplicationSubApplications
 */
class SingleApplicationSubApplicationsManager
{
    /**
     * Function to Store data to DB
     *
     * @param array $data
     * @return int|void
     */
    public function store(array $data)
    {
        return DB::transaction(function () use ($data) {

            $singleAppSubApplications = new  SingleApplicationSubApplications();

            foreach ($data as $value) {

                $value['user_id'] = Auth::id();
                $value['user_company_tax_id'] = Auth::user()->companyTaxId();
                $value['is_edited'] = SingleApplicationSubApplications::FALSE;

                $whereCredentials = [
                    'saf_sub_applications.saf_number' => $value['saf_number'],
                    'document_code' => $value['document_code'] ?? null,
                    'current_status' => SingleApplicationSubApplications::STATUS_SUBMITTED,
                ];

                if (isset($value['is_lab'])) {
                    $whereCredentials['is_lab'] = SingleApplicationSubApplications::TRUE;
                }

                if (isset($value['is_lab'])) {
                    $selectedProductIds = array_map(function ($value) {
                        return (int)$value['product_id'];
                    }, $value['products']);

                    $subAppExist = $singleAppSubApplications
                        ->select('saf_sub_applications.*')
                        ->where([
                            'saf_sub_applications.saf_number' => $value['saf_number'],
                            'document_code' => $value['document_code'] ?? null,
                            'current_status' => SingleApplicationSubApplications::STATUS_SUBMITTED,
                            'is_lab' => SingleApplicationSubApplications::TRUE
                        ])
                        ->notManualSubApplication()
                        ->whereIn('saf_sub_application_products.product_id', $selectedProductIds)
                        ->join('saf_sub_application_products', 'saf_sub_applications.id', '=', 'saf_sub_application_products.subapplication_id')
                        ->first();
                } else {
                    $subAppExist = $singleAppSubApplications
                        ->select('saf_sub_applications.*')
                        ->where([
                            'saf_sub_applications.saf_number' => $value['saf_number'],
                            'document_code' => $value['document_code'] ?? null,
                            'current_status' => SingleApplicationSubApplications::STATUS_SUBMITTED,
                        ])
                        ->notManualSubApplication()
                        ->whereIn('saf_sub_application_products.product_id', $value['products'])
                        ->join('saf_sub_application_products', 'saf_sub_applications.id', '=', 'saf_sub_application_products.subapplication_id')
                        ->first();
                }

                if (!is_null($subAppExist) && !isset($value['is_lab'])) {
                    continue;
                }

                $updateOrCreateCredentials = [
                    'manual_created' => SingleApplication::FALSE,
                    'saf_number' => $value['saf_number'],
                    'user_id' => Auth::id(),
                    'document_code' => $value['document_code'] ?? null,
                    'current_status' => $value['current_status'],
                    'pdf_hash_key' => str_random(64)
                ];

                if (isset($value['is_lab'])) {
                    $updateOrCreateCredentials['is_lab'] = SingleApplicationSubApplications::TRUE;
                }

                $singleAppSubApplication = $singleAppSubApplications->updateOrCreate($updateOrCreateCredentials, $value);

                // Update Document sub applications ,if new sub application equal old in that case update sub_application id in document sub_applications
                $submittedSubApplications = SingleApplicationSubApplications::where('saf_number', $value['saf_number'])->submittedSubApplication()->pluck('id')->toArray();

                SingleApplicationDocumentSubApplications::where([
                    'company_tax_id' => Auth::user()->companyTaxId(),
                    'saf_number' => $value['saf_number'],
                    'sub_application_agency_id' => $value['agency_id'],
                    'sub_applicaiton_classificator_id' => $value['reference_classificator_id'] ?? null
                ])
                    ->whereNotIn('sub_application_id', $submittedSubApplications)
                    ->update(['sub_application_id' => $singleAppSubApplication->id]);

                // Sub Application Products Part
                $products = $value['products'];
                $deletedProducts = [];
                foreach ($products as $key => $product) {
                    if (is_array($product)) {
                        $deletedProducts[] = $product['product_id'];
                        $subApplicationProducts = [
                            'company_tax_id' => Auth::user()->companyTaxId(),
                            'user_id' => Auth::id(),
                            'subapplication_id' => $singleAppSubApplication->id,
                            'product_id' => $product['product_id'],
                            'saf_number' => $value['saf_number'],
                            'sample_quantity' => $product['sample_quantity'] ?? null,
                            'sample_measurement_unit' => $product['sample_measurement_unit'] ?? null,
                            'sample_number' => $product['sample_number'] ?? null
                        ];
                    } else {
                        $deletedProducts[] = $product;
                        $subApplicationProducts = [
                            'company_tax_id' => Auth::user()->companyTaxId(),
                            'user_id' => Auth::id(),
                            'subapplication_id' => $singleAppSubApplication->id,
                            'product_id' => $product,
                            'saf_number' => $value['saf_number']
                        ];
                    }

                    SingleApplicationSubApplicationProducts::updateOrCreate($subApplicationProducts, $subApplicationProducts);
                }

                SingleApplicationSubApplicationProducts::whereNotIn('product_id', $deletedProducts)->where(['subapplication_id' => $singleAppSubApplication->id])->delete();

                if (count($data) == 1) {
                    return $singleAppSubApplication->id;
                }
            }
        });
    }

    /**
     * Function to store manual Sub application
     *
     * @param $subApplication
     * @param array $products
     * @return SingleApplicationSubApplications
     */
    public function storeManual($subApplication, $products = [])
    {
        $subApplication['is_edited'] = SingleApplicationSubApplications::FALSE;

        return DB::transaction(function () use ($subApplication, $products) {

            //
            $createdSubApplication = SingleApplicationSubApplications::create($subApplication);

            $subApplicationProducts = [];
            if (count($products)) {
                foreach ($products as $key => $pr_id) {
                    $subApplicationProducts [] = [
                        'company_tax_id' => Auth::user()->companyTaxId(),
                        'user_id' => Auth::id(),
                        'subapplication_id' => $createdSubApplication->id,
                        'product_id' => $pr_id,
                        'saf_number' => $subApplication['saf_number'],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ];

                }
            }

            if (count($subApplicationProducts)) {
                SingleApplicationSubApplicationProducts::insert($subApplicationProducts);
            }

            return $createdSubApplication->id;
        });
    }

    /**
     * Function to delete Manual SubApplication
     *
     * @param $subApplicationId
     * @param $safNumber
     */
    public function deleteManualSubApplication($subApplicationId, $safNumber)
    {
        DB::transaction(function () use ($subApplicationId, $safNumber) {
            SingleApplicationSubApplicationProducts::where('subapplication_id', $subApplicationId)->where('saf_number', $safNumber)->delete();
            SingleApplicationSubApplications::where('id', $subApplicationId)->delete();
        });
    }

    /**
     * Function to send subApplication
     *
     * @param $subApplication
     * @param $saf
     * @param $agencySubdivisionId
     * @param $expressControlId
     * @return void
     */
    public function sendSubApplication($subApplication, $saf, $agencySubdivisionId, $expressControlId)
    {
        DB::transaction(function () use ($subApplication, $saf, $agencySubdivisionId, $expressControlId) {

            $subApplicationId = $subApplication->id;
            $safNumber = $saf->regular_number;

            // Update products national currency
            $subApplicationProducts = $subApplication->productList->pluck('id')->all();
            if (count($subApplicationProducts)) {
                $productManager = new SingleApplicationProductsManager();
                $productManager->updateProductNationalAmount($subApplicationProducts, $safNumber);
            }

            //
            $agency = Agency::select('tax_id')->where('id', $subApplication->agency_id)->first();

            $documentCode = $subApplication->document_code;
            $constructorDocument = ConstructorDocument::select('id')->where(['document_code' => $documentCode, 'company_tax_id' => $agency->tax_id])->first();

            $constructorState = ConstructorStates::select('id')->where(['constructor_document_id' => $constructorDocument->id, 'state_type' => ConstructorStates::START_STATE_TYPE])->active()->first();

            if (!is_null($constructorState) && $subApplication->current_status == SingleApplicationSubApplications::STATUS_FORM || $subApplication->current_status == SingleApplicationSubApplications::STATUS_REQUESTED) {

                // $currentSafSubApplicationsCount = SingleApplicationSubApplications::where('saf_number', $safNumber)->submittedSubApplication()->count();

                // First Pay for Obligation
                /*if (config('swis.country_mode_2') && $currentSafSubApplicationsCount < 1) {

                    $userBalance = Auth::user()->balance();
                    $sum = SingleApplicationObligations::SAF_OBLIGATIONS_PRICE_FOR_SEND_APPLICATION_OBLIGATION;
                    if (is_null($confirmed)) {
                        if ($userBalance < $sum) {

                            $errors['message'] = trans('swis.saf.sub_application.send_application_not_enough');
                            $errors['valid_message'] = true;

                            return responseResult('INVALID_DATA', '', '', $errors);

                        } else {
                            $html = view('single-application.obligations.pay-sub-application-modal')->with([
                                'subApplicationId' => $subApplicationId,
                                'expressControlId' => $expressControlId,
                                'agencySubdivisionId' => $agencySubdivisionId,
                                'sum' => $sum,
                            ])->render();

                            return responseResult('OK', '', ['needConfirm' => true, 'html' => $html]);
                        }
                    } else {
                        $this->singleApplicationObligationsManager->storeSubApplicationObligationAndTransaction($safNumber,$subApplicationId,$constructorState->id);
                    }

                }*/

                // Add SubApplication next possible number
                if ($subApplication->current_status == SingleApplicationSubApplications::STATUS_FORM) {
                    $subApplicationNumber = $this->returnUniqueDocumentNumber($safNumber);
                } else {
                    $subApplicationNumber = $subApplication->document_number;
                }

                // Notification
                $usersNextStep = ConstructorStates::getUsersByMapping($constructorState->id, $constructorDocument->id);

                $notificationsManager = new NotificationsManager();
                $notificationsManager->sendSubApplicationTemplateNotification($usersNextStep, $safNumber, $subApplicationNumber, $subApplicationId, $constructorDocument->id);

                // Save sub application data on data json for my application
                $safData = $saf->data;

                $subApplicationProducts = SingleApplicationSubApplicationProducts::select('saf_products.*')
                    ->join('saf_products', 'saf_products.id', '=', 'saf_sub_application_products.product_id')
                    ->where('saf_sub_application_products.subapplication_id', $subApplicationId)
                    ->get();

                $safData['saf']['product']['brutto_weight'] = $subApplicationProducts->sum('brutto_weight');
                $safData['saf']['product']['netto_weight'] = $subApplicationProducts->sum('netto_weight');
                $safData['saf']['product']['product_count'] = count($subApplicationProducts);
                $safData['saf']['product']['total_value'] = $subApplicationProducts->sum('total_value_national');
                $safData['saf']['product']['total_value_all'] = $subApplicationProducts->sum('total_value');

                $safData['saf']['general']['subapplication_submitting_date'] = currentDateTime()->format(config('swis.date_format'));
                $safData['saf']['general']['sub_application_number'] = $subApplicationNumber;
                $safData['saf']['general']['sub_application_agency_subdivision_id'] = $agencySubdivisionId;
                $safData['saf']['general']['sub_application_express_control_id'] = $expressControlId;

                $updatedData = ['data' => $safData];
                if ($subApplication->current_status != SingleApplicationSubApplications::STATUS_REQUESTED) {
                    $updatedData['status_date'] = currentDate();
                }

                $subApplicationUpdateData = [
                    'current_status' => SingleApplicationSubApplications::STATUS_SUBMITTED,
                    'express_control_id' => $expressControlId,
                    'agency_subdivision_id' => $agencySubdivisionId,
                    'sender_user_id' => Auth::id()
                ];

                if ($subApplication->current_status == SingleApplicationSubApplications::STATUS_FORM) {
                    $subApplicationUpdateData = array_merge($subApplicationUpdateData, ['document_number' => $subApplicationNumber]);
                }

                if ($subApplication->current_status == SingleApplicationSubApplications::STATUS_FORM || $subApplication->current_status == SingleApplicationSubApplications::STATUS_REQUESTED) {
                    $subApplicationUpdateData = array_merge($subApplicationUpdateData, $updatedData);
                }

                // Update SubApplication
                $subApplication->update($subApplicationUpdateData);

                //Notify about sub application expiration date
                $notifiedData = [
                    'subapplication_id' => $subApplication->id,
                    'agency_id' => $subApplication->agency_id,
                    'document_id' => $constructorDocument->id
                ];

                $notifySubApplicationExpirationDateManager = new NotifySubApplicationExpirationDateManager();
                $notifySubApplicationExpirationDateManager->store($notifiedData);

                // Sub Application States
                $subAppStatesData = [];
                if ($subApplication->current_status != SingleApplicationSubApplications::STATUS_REQUESTED) {
                    $subAppStatesData['status_date'] = currentDateTime();
                }

                $subAppStatesDataCheck = [
                    'user_id' => Auth::id(),
                    'saf_number' => $safNumber,
                    'saf_id' => $saf->id,
                    'saf_subapplication_id' => $subApplicationId,
                    'state_id' => $constructorState->id,
                    'is_current' => '1'
                ];

                $subAppStatesData = array_merge($subAppStatesData, $subAppStatesDataCheck);

                SingleApplicationSubApplicationStates::where(['saf_number' => $safNumber, 'saf_subapplication_id' => $subApplicationId])->update(['is_current' => '0']);

                SingleApplicationSubApplicationStates::updateOrCreate($subAppStatesDataCheck, $subAppStatesData);

                // update All data
                $subApplication = SingleApplicationSubApplications::select('id', 'saf_number', 'data', 'custom_data', 'routings', 'routing_products', 'constructor_document_id')->with(['saf:id,saf_controlled_fields,regular_number', 'productList'])->where('id', $subApplicationId)->first();

                $singleApplicationSubApplicationCompleteData = new SingleApplicationSubApplicationCompleteData();
                $singleApplicationSubApplicationCompleteData->getParams();
                $singleApplicationSubApplicationCompleteData->setApplicationVariable($subApplication);

                list($safData, $productData, $productBatches) = $singleApplicationSubApplicationCompleteData->setData();

                $subApplication->all_data = json_encode(['data' => $safData, 'products' => array_values($productData), 'batches' => array_values($productBatches)]);
                $subApplication->save();

                // Notes
                $subApplicationNotes = [
                    'saf_number' => $safNumber,
                    'sub_application_id' => $subApplicationId,
                    'user_id' => Auth::id(),
                ];

                SingleApplicationNotes::storeNotes($subApplicationNotes, SingleApplicationNotes::NOTE_TRANS_KEY_CREATED_SUB_APPLICATION);
            }
        });
    }

    /**
     * Function to check sub application routings what documents is attached in saf or not
     *
     * @param $safNumber
     * @param $routingIDs
     * @param bool $subApplicationsId
     * @param array $safFieldsData
     * @return array
     */
    public function checkSubApplicationRoutingsDocuments($safNumber, $routingIDs, $subApplicationsId = false, $safFieldsData = [])
    {
        $routings = Routing::select('constructor_document_id', 'ref_classificator_codes')->whereIn('id', $routingIDs)->whereNotNull('ref_classificator_codes')->get();

        $requiredDocs = [];
        $requiredDocsWithOr = [];

        if (count($routings)) {

            $safDocumentsCodes = SingleApplicationDocuments::where('saf_documents.saf_number', $safNumber)
                ->join('document', 'saf_documents.document_id', '=', 'document.id');

            if ($subApplicationsId) {
                $subApplication = SingleApplicationSubApplications::where(['id' => $subApplicationsId, 'saf_number' => $safNumber])->first();

                $subApplicationProducts = $subApplication->productList->pluck('id')->all();

                $safDocumentsCodes->leftJoin('saf_document_products', 'saf_documents.id', '=', 'saf_document_products.saf_document_id')
                    ->whereIn('saf_document_products.product_id', $subApplicationProducts);

                // if in Saf Sub Application attached to doc
                $documentSubApplications = SingleApplicationDocumentSubApplications::where('sub_application_id', $subApplication->id)->pluck('saf_document_id')->all();

                if (count($documentSubApplications)) {
                    $safDocumentsCodes->orWhereIn('saf_documents.id', $documentSubApplications);
                }

            }

            $safDocumentsCodes = $safDocumentsCodes->pluck('document.document_type')->all();

            $allActiveClassificatorDocs = DB::table(ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS)->where('show_status', ReferenceTable::STATUS_ACTIVE)->pluck('code')->all();

            $saf = SingleApplication::select('data')->where('regular_number', $safNumber)->first();

            $transPortDocumentCode = '';
            if (isset($saf->data['saf']['transportation'], $saf->data['saf']['transportation']['type_of_transport'])) {

                $transportTypeId = $saf->data['saf']['transportation']['type_of_transport'] ?? 0;
                if (isset($safFieldsData['SAF_ID_75'])) {
                    $transportTypeId = $safFieldsData['SAF_ID_75'];
                }

                if ($transportTypeId) {
                    $transPortTypeCode = DB::table(ReferenceTable::REFERENCE_TRANSPORT_TYPE)->where('id', $transportTypeId)->where('show_status', ReferenceTable::STATUS_ACTIVE)->pluck('code')->first();

                    if (!is_null($transPortTypeCode)) {
                        $transPortDocumentCode = DB::table(ReferenceTable::REFERENCE_DOCUMENT_DEPENDING_TRANSPORT)->where('transport_type_code', $transPortTypeCode)->where('show_status', ReferenceTable::STATUS_ACTIVE)->pluck('document_code')->first();
                    }
                }
            }

            foreach ($routings as $routing) {
                foreach ($routing->ref_classificator_codes as $refClassificationDocCodes) {

                    $waybillCodeKey = array_search(ReferenceTable::REFERENCE_CLASSIFICATOR_WAYBILL_CODE, $refClassificationDocCodes, true);
                    if ($waybillCodeKey !== false) {
                        if ($transPortDocumentCode) {
                            $refClassificationDocCodes[$waybillCodeKey] = $transPortDocumentCode;
                        } else {
                            unset($refClassificationDocCodes[$waybillCodeKey]);
                        }
                    }

                    if (count($refClassificationDocCodes) == 1) {
                        if (in_array($refClassificationDocCodes[0], $allActiveClassificatorDocs, true) && !in_array($refClassificationDocCodes[0], $safDocumentsCodes, true)) {
                            $requiredDocs[] = $refClassificationDocCodes[0];
                        }
                    } else {

                        $activeClassDocs = array_intersect($refClassificationDocCodes, $allActiveClassificatorDocs);
                        if (count($activeClassDocs)) {
                            $checkExistDocumentInSaf = array_intersect($activeClassDocs, $safDocumentsCodes);

                            if (!count($checkExistDocumentInSaf)) {
                                $requiredDocsWithOr[] = $activeClassDocs;
                            }
                        }
                    }
                }
            }
        }

        $referenceClassificators = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS, 'code', true);

        $requiredDocs = array_unique($requiredDocs);
        if (count($requiredDocs)) {
            foreach ($requiredDocs as $key => $requiredDoc) {
                if (isset($referenceClassificators[$requiredDoc])) {
                    $requiredDocs[$key] = '<li>(' . $referenceClassificators[$requiredDoc]['code'] . ') ' . '' . $referenceClassificators[$requiredDoc]['name'] . '</li>';
                }
            }
        }

        if (count($requiredDocsWithOr)) {
            foreach ($requiredDocsWithOr as $mainKey => $values) {
                foreach ($values as $key => $requiredDoc) {
                    if (isset($referenceClassificators[$requiredDoc])) {
                        $requiredDocsWithOr[$mainKey][$key] = '<li>(' . $referenceClassificators[$requiredDoc]['code'] . ') ' . '' . $referenceClassificators[$requiredDoc]['name'] . '</li>';
                    }
                }
            }
        }

        return [
            'requiredDocs' => $requiredDocs,
            'requiredDocsWithOr' => $requiredDocsWithOr
        ];
    }

    /**
     * Function to return next possible document_number
     *
     * @param $safNumber
     * @param int $count
     * @return string
     */
    public function returnUniqueDocumentNumber($safNumber, $count = 1)
    {
        $submittedSubApplicationsCount = SingleApplicationSubApplications::where('saf_number', $safNumber)->submittedOrRequestedSubApplication()->count();

        if ($submittedSubApplicationsCount > $count) {
            $count = $submittedSubApplicationsCount + 1;
        }

        $subApplicationNumber = $safNumber . '/' . $count;

        $existSingleApp = SingleApplicationSubApplications::where(['saf_number' => $safNumber, 'document_number' => $subApplicationNumber])->exists();

        if (!$existSingleApp) {
            return $subApplicationNumber;
        }

        $count++;

        return $this->returnUniqueDocumentNumber($safNumber, $count);
    }

    /**
     * Function to get SAF data SubApplication by subApplication Id
     *
     * @param $subApplicationId
     * @return array
     */
    public function getSAFDataForSubApplication($subApplicationId)
    {
        $subApplication = SingleApplicationSubApplications::where('id', $subApplicationId)->first();
        $safNumber = optional($subApplication)->saf_number;
        $saf = SingleApplication::where('regular_number', $safNumber)->firstOrFail();

        $subApplicationProducts = SingleApplicationSubApplicationProducts::select('saf_products.*')
            ->join('saf_products', 'saf_products.id', '=', 'saf_sub_application_products.product_id')
            ->where('saf_sub_application_products.subapplication_id', $subApplicationId)
            ->get();

        $subApplicationDocuments = $subApplication->subApplicationDocuments();
        $subApplicationObligations = SingleApplicationObligations::where(['subapplication_id' => $subApplicationId])->get();

        return [
            'saf' => $saf,
            'subApplicationId' => $subApplicationId,
            'subApplicationProducts' => $subApplicationProducts,
            'subApplicationDocuments' => $subApplicationDocuments,
            'subApplicationObligations' => $subApplicationObligations
        ];
    }

    /**
     * Function to update lab application
     *
     * @param array $validatedData
     * @param array $subApplication
     * @param $labApplicationId
     */
    public function labApplicationUpdate(array $validatedData, array $subApplication, $labApplicationId)
    {
        DB::transaction(function () use ($validatedData, $subApplication, $labApplicationId) {
            $singleAppSubApplication = SingleApplicationSubApplications::find($labApplicationId);

            $subApplication['user_id'] = Auth::id();
            $subApplication['user_company_tax_id'] = Auth::user()->companyTaxId();
            $subApplication['is_edited'] = SingleApplicationSubApplications::TRUE;


            $singleAppSubApplication->update($subApplication);

            // Sub Application Products Part
            $products = $validatedData['products'];
            $deletedProducts = [];
            foreach ($products as $key => $product) {
                $deletedProducts[] = $product['product_id'];
                $subApplicationProducts = [
                    'company_tax_id' => Auth::user()->companyTaxId(),
                    'user_id' => Auth::id(),
                    'subapplication_id' => $singleAppSubApplication->id,
                    'product_id' => $product['product_id'],
                    'saf_number' => $subApplication['saf_number'],
                    'sample_quantity' => $product['sample_quantity'] ?? null,
                    'sample_measurement_unit' => $product['sample_measurement_unit'] ?? null,
                    'sample_number' => $product['sample_number'] ?? null
                ];

                SingleApplicationSubApplicationProducts::updateOrCreate([
                    'company_tax_id' => Auth::user()->companyTaxId(),
                    'user_id' => Auth::id(),
                    'subapplication_id' => $singleAppSubApplication->id,
                    'product_id' => $product['product_id'],
                    'saf_number' => $subApplication['saf_number']
                ], $subApplicationProducts);
            }

            SingleApplicationSubApplicationProducts::whereNotIn('product_id', $deletedProducts)->where(['subapplication_id' => $singleAppSubApplication->id])->delete();

        });
    }

    /**
     * Function to get manual routings available agebncies and constructor documents
     *
     * @param $safRegime
     * @return array
     */
    public function manualRoutingsAvailableAgenciesAndConstructorDocuments($safRegime)
    {
        $manualRoutings = Routing::select('constructor_document_id', 'agency_id')->where('regime', $safRegime)->manualRouting()->active()->get();

        $constructorDocuments = ConstructorDocument::select('id', 'company_tax_id', DB::raw('constructor_documents_ml.document_name as name'))
            ->joinMl()
            ->whereIn('id', $manualRoutings->pluck('constructor_document_id')->all())
            ->active()->orderByDesc()
            ->get();

        $agencies = Agency::select(DB::raw('COALESCE(short_name, name) as name'), 'id')
            ->joinMl()
            ->whereIn('id', $manualRoutings->pluck('agency_id'))
            ->whereIn('tax_id', $constructorDocuments->pluck('company_tax_id')->unique()->all())
            ->active()->orderBy('name')
            ->get();

        return [
            'agencies' => $agencies,
            'constructorDocuments' => $constructorDocuments
        ];
    }

    /**
     * Function to get Agencies by constructor type
     *
     * @param $dataSubApplicationType
     * @param $safRegime
     * @return array
     */
    public function getAgenciesByConstructorType($dataSubApplicationType, $safRegime)
    {
        $subApplicationName = '';
        $agencies = $agenciesIds = [];
        if (!is_null($dataSubApplicationType)) {

            $availableAgencies = Routing::where(['regime' => $safRegime, 'constructor_document_id' => $dataSubApplicationType])->manualRouting()->active()->pluck('agency_id')->unique()->all();
            $constructorDocument = ConstructorDocument::select('company_tax_id', 'document_type_id')->where('id', $dataSubApplicationType)->active()->first();

            $agency = Agency::select('id', 'tax_id', DB::raw('COALESCE(short_name, name) as name'))->joinMl()->where('tax_id', $constructorDocument->company_tax_id)->whereIn('id', $availableAgencies)->active()->first();

            if (!is_null($agency)) {
                $agenciesIds[] = $agency->id;
                $agencies[] = [
                    'id' => $agency->id,
                    'name' => $agency->name,
                ];
            }

            $referenceDocumentType = DB::table(ReferenceTable::REFERENCE_DOCUMENT_TYPE_REFERENCE)->where('id', $constructorDocument->document_type_id)->first();

            if (!is_null($referenceDocumentType)) {
                $referenceClassificator = getReferenceRows(ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS, $referenceDocumentType->document_type);

                if (!is_null($referenceClassificator)) {
                    $subApplicationName = $referenceClassificator->name;
                }
            }
        }

        return [
            'agenciesIds' => $agenciesIds,
            'agencies' => $agencies,
            'subApplicationName' => $subApplicationName,
        ];
    }

    /**
     * Function to get constructor document by agency
     *
     * @param $agencyId
     * @param $dataSubApplicationType
     * @param $safRegime
     * @return array
     */
    public function getConstructorDocumentsByAgency($agencyId, $dataSubApplicationType, $safRegime)
    {
        $subApplicationTypes = [];

        if (!is_null($agencyId)) {

            $agency = Agency::select('id', 'tax_id')->active()->find($agencyId);

            if (!is_null($agency)) {

                $availableConstructorDocuments = Routing::where(['regime' => $safRegime, 'agency_id' => $agency->id])->manualRouting()->active()->pluck('constructor_document_id')->unique()->all();

                //
                if (is_null($dataSubApplicationType)) {
                    $subApplicationTypes = ConstructorDocument::getConstructorDocumentForAgency($agency->tax_id, $availableConstructorDocuments)->toArray();
                }
            }
        }

        return $subApplicationTypes;
    }
}
