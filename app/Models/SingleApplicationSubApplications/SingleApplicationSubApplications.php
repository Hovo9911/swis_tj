<?php

namespace App\Models\SingleApplicationSubApplications;

use App\Models\Agency\Agency;
use App\Models\BaseModel;
use App\Models\ConstructorActions\ConstructorActions;
use App\Models\ConstructorActionSuperscribes\ConstructorActionSuperscribes;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorDocumentsRolesAttribute\ConstructorDocumentsRolesAttribute;
use App\Models\ConstructorLists\ConstructorLists;
use App\Models\ConstructorMapping\ConstructorMapping;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\DataModel\DataModel;
use App\Models\Document\Document;
use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm;
use App\Models\Menu\Menu;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\RegionalOffice\RegionalOffice;
use App\Models\Role\Role;
use App\Models\RolesGroupRolesAttributes\RolesGroupRolesAttributes;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure;
use App\Models\SingleApplicationDocumentProducts\SingleApplicationDocumentProducts;
use App\Models\SingleApplicationDocuments\SingleApplicationDocuments;
use App\Models\SingleApplicationDocumentSubApplications\SingleApplicationDocumentSubApplications;
use App\Models\SingleApplicationExpertise\SingleApplicationExpertise;
use App\Models\SingleApplicationProducts\SingleApplicationProducts;
use App\Models\SingleApplicationSubApplicationProducts\SingleApplicationSubApplicationProducts;
use App\Models\SingleApplicationSubApplicationsLaboratory\SingleApplicationSubApplicationsLaboratory;
use App\Models\SingleApplicationSubApplicationStates\SingleApplicationSubApplicationStates;
use App\Models\User\User;
use App\Models\UserDocuments\UserDocuments;
use App\Models\UserRoles\UserRoles;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class SingleApplicationSubApplications
 * @package App\Models\SingleApplicationSubApplications
 */
class SingleApplicationSubApplications extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'saf_sub_applications';

    /**
     * @var string
     */
    const FILE_PATH = 'sub_applications';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'creator_user_id',
        'user_company_tax_id',
        'saf_number',
        'sub_application_number',
        'saf_id',
        'company_tax_id',
        'reference_classificator_id',
        'reference_document_type_code',
        'agency_id',
        'agency_subdivision_id',
        'subtitle_type',
        'document_code',
        'constructor_document_id',
        'routings',
        'routing_products',
        'name',
//        'products',
        'status_date',
        'data',
        'custom_data',
        'current_status',
        'document_number',
        'status_data',
        'express_control_id',
        'manual_created',
        'pdf_hash_key',
        'sender_user_id',
        'all_data',
        'is_edited',
        'edited_user_id',
        'is_lab',
        'lab_type',
        'indicators',
        'from_subapplication_id_for_labs',
        'send_lab_company_tax_id'
    ];

    /**
     * @var string
     */
    const STATUS_FORM = null;
    const STATUS_REGISTERED = 'registered';
    const STATUS_PRESENTED = 'presented';
    const STATUS_SUBMITTED = 'submitted';
    const STATUS_REQUESTED = 'requested';
    const STATUS_RECALLED = 'recalled';
    const STATUS_CANCELED = 'canceled';

    /**
     * @var string
     */
    const ACTION_SEND = 'send';
    const ACTION_RECALL = 'recall';

    /**
     * @var array
     */
    const PRODUCT_SELECT_LIST = [
        'saf_products.id',
        'product_number',
        'product_code',
        'commercial_description',
        'product_description',
        'producer_country',
        'total_value',
        'total_value_code',
        'total_value_national',
        'quantity',
        'measurement_unit'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'data' => 'array',
        'custom_data' => 'array',
        'routings' => 'array',
        'routing_products' => 'array',
        'indicators' => 'array',
        'tmp_data' => 'array',
    ];

    /**
     * Function to relate with saf
     *
     * @return HasOne
     */
    public function saf()
    {
        return $this->hasOne(SingleApplication::class, 'regular_number', 'saf_number');
    }

    /**
     * Function to relate with constructor_documents_roles_attribute
     *
     * @return HasMany
     */
    public function attributes()
    {
        return $this->hasMany(ConstructorDocumentsRolesAttribute::class, 'constructor_documents_roles_id', 'constructor_documents_roles');
    }

    /**
     * Function to relate with SingleApplicationSubApplicationStates
     *
     * @return HasOne
     */
    public function applicationCurrentState()
    {
        return $this->hasOne(SingleApplicationSubApplicationStates::class, 'saf_subapplication_id', 'id')->where('is_current', '1');
    }

    /**
     * Function to relate with SingleApplicationSubApplicationStates
     *
     * @return HasOne
     */
    public function currentState()
    {
        return $this->hasOne(SingleApplicationSubApplicationStates::class, 'saf_subapplication_id', 'saf_sub_application_id')->where('is_current', '1');
    }

    /**
     * Function to relate with SingleApplicationSubApplicationProducts
     *
     * @return HasManyThrough
     */
    public function productList()
    {
        return $this->hasManyThrough(SingleApplicationProducts::class, SingleApplicationSubApplicationProducts::class, 'subapplication_id', 'id', 'id', 'product_id')->with('batches')->orderByAsc();
    }

    /**
     * Function to relate with SingleApplicationSubApplicationProducts
     *
     * @return HasManyThrough
     */
    public function productListInSearch()
    {
        return $this->hasManyThrough(SingleApplicationProducts::class, SingleApplicationSubApplicationProducts::class, 'subapplication_id', 'id', 'saf_sub_application_id', 'product_id')->with('batches')->orderByAsc();
    }

    /**
     * Function to relate with superScribe table
     *
     * @return HasOne
     */
    public function superscribeUser()
    {
        return $this->hasOne(ConstructorActionSuperscribes::class, 'saf_subapplication_id', 'saf_sub_application_id')->where('is_current', '1');
    }

    /**
     * Function to relate with agency
     *
     * @return HasOne
     */
    public function agency()
    {
        return $this->hasOne(Agency::class, 'id', 'agency_id');
    }

    /**
     * Function to relate with constructor_documents_roles_attribute
     *
     * @return HasOne
     */
    public function document()
    {
        return $this->hasOne(ConstructorDocument::class, 'id', 'constructor_document_id');
    }

    /**
     * Function to relate with SingleApplicationSubApplicationsLaboratory
     *
     * @return HasMany
     */
    public function labIndicators()
    {
        return $this->hasMany(SingleApplicationSubApplicationsLaboratory::class, 'sub_application_id', 'id');
    }

    /**
     * Function to relate with SingleApplicationSubApplicationsLaboratory
     *
     * @return HasOne
     */
    public function userInfo()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * Function to relate with superScribe table
     *
     * @return HasOne
     */
    public function constructorMappingByRoleId()
    {
        return $this->hasOne(ConstructorActionSuperscribes::class, 'saf_subapplication_id', 'saf_sub_application_id')->where('is_current', '1');
    }

    /**
     * Function to get only formatted sub applications (not manual created)
     *
     * @param $query
     * @return Builder
     */
    public function scopeNotManualSubApplication($query)
    {
        return $query->where('manual_created', SingleApplicationSubApplications::FALSE);
    }

    /**
     * Function to get only Manual Created sub applications (not manual created)
     *
     * @param $query
     * @return Builder
     */
    public function scopeManualSubApplication($query)
    {
        return $query->where('manual_created', SingleApplicationSubApplications::TRUE);
    }

    /**
     * Function to get only Manual Created sub applications (not manual created)
     *
     * @param $query
     * @return Builder
     */
    public function scopeNotFormedSubApplication($query)
    {
        return $query->whereNull('current_status');
    }

    /**
     * Function to get only Manual Created sub applications (not manual created)
     *
     * @param $query
     * @return Builder
     */
    public function scopeFormedSubApplication($query)
    {
        return $query->whereNotNull('current_status');
    }

    /**
     * Function to get Submitted Sub applications
     *
     * @param $query
     * @return Builder
     */
    public function scopeSubmittedSubApplication($query)
    {
        return $query->where('current_status', SingleApplicationSubApplications::STATUS_SUBMITTED);
    }

    /**
     * Function to get Submitted or Requested
     *
     * @param $query
     * @return Builder
     */
    public function scopeSubmittedOrRequestedSubApplication($query)
    {
        return $query->where(['current_status' => SingleApplicationSubApplications::STATUS_SUBMITTED])->orWhere(['current_status' => SingleApplicationSubApplications::STATUS_REQUESTED]);
    }

    /**
     * Function to get Submitted or Requested
     *
     * @param $query
     * @return Builder
     */
    public function scopeRequestedSubApplication($query)
    {
        return $query->where(['current_status' => SingleApplicationSubApplications::STATUS_REQUESTED]);
    }

    /**
     * Function to get Submitted or Requested
     *
     * @param $query
     * @return Builder
     */
    public function scopeNotFormedOrRequestedSubApplication($query)
    {
        return $query->whereNull('current_status')->orWhere(['current_status' => SingleApplicationSubApplications::STATUS_REQUESTED]);
    }

    /**
     * Function to get Lab type sub applications
     *
     * @param $query
     * @return Builder
     */
    public function scopeLabType($query)
    {
        return $query->where('is_lab', SingleApplicationSubApplications::TRUE);
    }

    /**
     * Function to get Not Lab type sub applications
     *
     * @param $query
     * @return Builder
     */
    public function scopeNotLabType($query)
    {
        return $query->where('is_lab', SingleApplicationSubApplications::FALSE);
    }

    /**
     * Function to check for accessing of application
     *
     * @param $query
     * @param array $roleIds
     * @return Builder
     */
    public function scopeMyApplicationRoles($query, $roleIds = [])
    {
        $constructorDocumentId = request()->route('id');

        if (Auth::user()->isLaboratory()) {
            $query->where('saf_sub_applications.is_lab', true)->where('saf_sub_applications.send_lab_company_tax_id', Auth::user()->companyTaxId());
        } else {
            $query->where('saf_sub_applications.is_lab', false)->where('saf_sub_applications.company_tax_id', Auth::user()->companyTaxId());
        }

        $query->join('saf_sub_application_states', function ($join) {
            $join->on('saf_sub_application_states.saf_subapplication_id', '=', 'saf_sub_applications.id')->where('saf_sub_application_states.is_current', '1');
        });

        $query->join('constructor_mapping', function ($join) use ($constructorDocumentId) {
            $join->on('saf_sub_application_states.state_id', '=', 'constructor_mapping.start_state')
                ->where('constructor_mapping.document_id', $constructorDocumentId)
                ->where('constructor_mapping.show_status', BaseModel::STATUS_ACTIVE);
        });

        $query->where('saf_sub_applications.constructor_document_id', $constructorDocumentId);

        if (!Auth::user()->isLocalAdmin()) {

            if (!count($roleIds)) {
                $roleIds = Auth::user()->getCurrentRoles(Menu::USER_DOCUMENTS_MENU_NAME);
            }

            //
            $userRoles = UserRoles::select('attribute_field_id', 'attribute_type', 'attribute_value', 'role_id')
                ->whereIn('role_id', $roleIds)
                ->where('company_tax_id', Auth::user()->companyTaxId())
                ->where('user_id', Auth::id())
                ->activeRole()
                ->get()
                ->toArray();

            //
            $rolesWithAllPermission = UserRoles::whereIn('role_id', $roleIds)
                ->where('company_tax_id', Auth::user()->companyTaxId())
                ->where('user_id', Auth::id())
                ->activeRole()
                ->whereNull('attribute_field_id')
                ->get()
                ->pluck('role_id')->unique()->all();

            //
            $userRolesGroup = UserRoles::select('attribute_field_id', 'attribute_type', 'attribute_value', 'role_group_id')
                ->where(function ($q) {
                    $q->whereNotNull('role_group_id')->where('role_group_id', '!=', 0);
                })->where('company_tax_id', Auth::user()->companyTaxId())->where('user_id', Auth::id())
                ->whereNotNull('attribute_field_id')
                ->activeRole()
                ->get();

            //
            $roles = [];
            foreach ($userRoles as $userRole) {
                if (!is_null($userRole['attribute_field_id']) && !in_array($userRole['role_id'], $rolesWithAllPermission)) {
                    $roles[$userRole['role_id']][$userRole['attribute_field_id']][] = $userRole;
                }
            }

            foreach ($userRolesGroup as $userRole) {
                $rolesGroupAttributes = RolesGroupRolesAttributes::where(['roles_group_id' => $userRole->role_group_id, 'attribute_id' => $userRole->attribute_field_id, 'attribute_type' => $userRole->attribute_type])->get();
                foreach ($rolesGroupAttributes as $rolesGroupAttribute) {
                    if (!in_array($rolesGroupAttribute->role_id, $rolesWithAllPermission)) {
                        $roles[$rolesGroupAttribute->role_id][$rolesGroupAttribute->attribute_id][] = array_merge($userRole->toArray(), ['role_id' => $rolesGroupAttribute->role_id]);
                    }
                }
            }

            $query->where(function ($q) use ($roles, $rolesWithAllPermission) {

                foreach ($roles as $item) {

                    $q->orWhere(function ($q) use ($roles, $item) {

                        foreach ($item as $roleGroup) {

                            $q->where(function ($q) use ($roleGroup) {

                                foreach ($roleGroup as $role) {

                                    if ($role['attribute_type'] == Role::ROLE_ATTRIBUTE_TYPE_SAF) {

                                        $xmlElement = getSafFieldById(SingleApplicationDataStructure::lastXml(), $role['attribute_field_id'])[0]->attributes();

                                        $name = str_replace("][", "->", (string)$xmlElement->name);
                                        $name = str_replace("[", "->", $name);
                                        $name = str_replace("]", "", $name);

                                        $attrValue = (string)$role['attribute_value'];

                                        // If Saf Regime -- we save code not Ref ID
                                        if ($role['attribute_field_id'] == 'SAF_ID_1') {

                                            $attrValue = SingleApplication::REGIME_IMPORT;
                                            if ($role['attribute_value'] == 2) {
                                                $attrValue = SingleApplication::REGIME_EXPORT;
                                            } elseif ($role['attribute_value'] == 3) {
                                                $attrValue = SingleApplication::REGIME_TRANSIT;
                                            }
                                        }

                                        $mdm = DataModel::find($xmlElement->mdm);
                                        if (isset($mdm->reference)) {

                                            $getRefRow = getReferenceRows($mdm->reference, $attrValue, false, ['id', 'code']);

                                            if (!is_null($getRefRow)) {

                                                $refAvailableIds = getReferenceRows($mdm->reference, false, false, ['id'], 'get', false, false, ['code' => $getRefRow->code], true)->pluck('id')->all();

                                                // Fixes Where Statements
                                                switch ($name) {
                                                    case 'saf->general->sub_application_agency_subdivision_id':
                                                        $q->orWhereIn("saf_sub_applications.agency_subdivision_id", $refAvailableIds);
                                                        break;

                                                    case 'saf->general->sub_application_express_control_id':
                                                        $q->orWhereIn("saf_sub_applications.express_control_id", $refAvailableIds);
                                                        break;

                                                    default:
                                                        $q->orWhereIn("saf_sub_applications.data->{$name}", $refAvailableIds);
                                                }

                                            } else {
                                                $q->orWhere("saf_sub_applications.data->{$name}", $attrValue);
                                            }

                                        } else {
                                            $q->orWhere("saf_sub_applications.data->{$name}", $attrValue);
                                        }

                                    } else {
                                        $name = $role['attribute_field_id'];
                                        $q->orWhere("saf_sub_applications.custom_data->{$name}", (string)$role['attribute_value']);
                                    }

                                    $q->where("constructor_mapping.role", $role['role_id']);
                                }

                            });
                        }
                    });
                }

                if (count($rolesWithAllPermission)) {
                    $q->orWhereIn('constructor_mapping.role', $rolesWithAllPermission);
                }
            });

            //
            $showOnlySelfSubApplications = Auth::user()->getCurrentUserRoles(Menu::USER_DOCUMENTS_MENU_NAME, false)->where('show_only_self', UserDocuments::FALSE)->first();

            if (is_null($showOnlySelfSubApplications)) {

                if (!BaseModel::isJoined($query, 'constructor_action_superscribes')) {
                    $query->leftJoin('constructor_action_superscribes', function ($join) {
                        $join->on('constructor_action_superscribes.saf_subapplication_id', '=', 'saf_sub_applications.id')->where('constructor_action_superscribes.is_current', self::TRUE);
                    });
                }

                $query->where('constructor_action_superscribes.to_user', Auth::id());
            }
        }

        return $query;
    }

    /**
     * Function to get only Manual Created sub applications
     *
     * @return bool
     */
    public function isManualSubApplication()
    {
        if ($this->manual_created == SingleApplicationSubApplications::TRUE) {
            return true;
        }

        return false;
    }

    /**
     * Function to get only Generated sub applications (not manual created)
     *
     * @return bool
     */
    public function isGeneratedSubApplication()
    {
        if ($this->manual_created == SingleApplicationSubApplications::FALSE) {
            return true;
        }

        return false;
    }

    /**
     * Function to relate with SingleApplicationSubApplicationProducts
     *
     * @return HasMany
     */
    public function productListSelect()
    {
        return $this->hasMany(SingleApplicationSubApplicationProducts::class, 'subapplication_id', 'id')
            ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
            ->select(self::PRODUCT_SELECT_LIST);
    }

    /**
     * Function to relate with SingleApplicationSubApplicationProducts
     *
     * @param $conditions
     * @param bool $selectAll
     * @return HasMany
     */
    public function productListApi($conditions, $selectAll = false)
    {
        $query = $this->hasMany(SingleApplicationSubApplicationProducts::class, 'subapplication_id', 'id')
            ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
            ->join('saf_sub_applications', 'saf_sub_application_products.subapplication_id', '=', 'saf_sub_applications.id');

        if (isset($conditions['refs'])) {
            foreach ($conditions['refs'] as $field => $ref) {
                $refName = "reference_{$ref}";
                $refNameMl = "{$refName}_ml";
                $query->join($refName, DB::raw("{$refName}.id::varchar"), '=', "saf_products.{$field}")
                    ->join($refNameMl, function ($q) use ($refNameMl, $refName) {
                        $q->on("{$refNameMl}.{$refName}_id", "=", "{$refName}.id")->where("{$refNameMl}.lng_id", cLng('id'));
                    });
            }
        }

        if (!$selectAll) {
            $query->select(array_merge(self::PRODUCT_SELECT_LIST, ['saf_sub_applications.tmp_data', 'saf_sub_applications.custom_data']));
        }

        foreach ($conditions['conditions'] as $condition) {
            list ($field, $operator, $value) = $condition;

            if (isset($conditions['refs'][$field])) {
                $refName = "reference_{$conditions['refs'][$field]}";
                $refNameMl = "{$refName}_ml";
                $query->where(function ($q) use ($refName, $refNameMl, $operator, $value) {
                    if (is_array($value) && count($value) > 0) {
                        $q->where(function ($orWhere) use ($refName, $refNameMl, $value, $operator) {
                            foreach ($value as $item) {
                                $values = self::getOperatorAndValue($operator, $item);
                                $orWhere->orWhere("{$refName}.code", $values['operator'], $values['value'])->orWhere("{$refNameMl}.name", $values['operator'], $values['value']);
                            }
                        });
                    } else {
                        $values = self::getOperatorAndValue($operator, $value);
                        $q->where("{$refName}.code", $values['operator'], $values['value'])->orWhere("{$refNameMl}.name", $values['operator'], $values['value']);
                    }
                });
            } elseif (!isset($conditions['mdmRefs'][$field]) && strpos($field, 'FIELD_ID_') === false) {
                if (is_array($value) && count($value) > 0) {
                    $query->where(function ($q) use ($field, $operator, $value) {
                        foreach ($value as $item) {
                            $values = self::getOperatorAndValue($operator, $item);
                            $q->orWhere(DB::raw("{$field}::varchar"), $values['operator'], $values['value']);
                        }
                    });
                } else {
                    $values = self::getOperatorAndValue($operator, $value);
                    $typeCast = (is_numeric($values['value'])) ? '::float' : '::varchar';
                    $query->where(DB::raw($field . $typeCast), $values['operator'], $values['value']);
                }
            }
        }

        return $query;
    }

    /**
     * Function to get product batches
     *
     * @param $conditions
     * @return HasMany
     */
    public function productBatches($conditions)
    {
        $query = $this->hasMany(SingleApplicationSubApplicationProducts::class, 'subapplication_id', 'id')
            ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
            ->join('saf_products_batch', "saf_products_batch.saf_product_id", "=", "saf_products.id");

        if (isset($conditions['refs'])) {
            foreach ($conditions['refs'] as $field => $ref) {
                $refName = "reference_{$ref}";
                $refNameMl = "{$refName}_ml";
                $query->join($refName, DB::raw("{$refName}.id::varchar"), '=', "saf_products.{$field}")
                    ->join($refNameMl, function ($q) use ($refNameMl, $refName) {
                        $q->on("{$refNameMl}.{$refName}_id", "=", "{$refName}.id")->where("{$refNameMl}.lng_id", cLng('id'));
                    });
            }
        }

        if (isset($conditions['conditions'])) {
            foreach ($conditions['conditions'] as $condition) {
                list ($field, $operator, $value, $originalFieldName) = $condition;

                if (isset($conditions['refs'][$field])) {
                    $refName = "reference_{$conditions['refs'][$field]}";
                    $refNameMl = "{$refName}_ml";
                    $query->where(function ($q) use ($refName, $refNameMl, $operator, $value) {
                        if (is_array($value) && count($value) > 0) {
                            $q->where(function ($orWhere) use ($refName, $refNameMl, $value, $operator) {
                                foreach ($value as $item) {
                                    $values = self::getOperatorAndValue($operator, $item);
                                    $orWhere->orWhere("{$refName}.code", $values['operator'], $values['value'])->orWhere("{$refNameMl}.name", $values['operator'], $values['value']);
                                }
                            });
                        } else {
                            $values = self::getOperatorAndValue($operator, $value);
                            $q->where("{$refName}.code", $operator, $value)->orWhere("{$refNameMl}.name", $values['operator'], $values['value']);
                        }
                    });
                } else {
                    $mdmFieldId = optional(DocumentsDatasetFromMdm::select('id', 'field_id', 'mdm_field_id')->where('field_id', str_replace('$FIELD_ID_', "", $originalFieldName))->first())->mdm_field_id;
                    $mdmFieldReference = optional(DataModel::select('reference')->find($mdmFieldId))->reference;

                    if (!is_null($mdmFieldId) && !is_null($mdmFieldReference)) {


                        if (is_array($value) && count($value) > 0) {
                            $referenceIds = [];
                            foreach ($value as $item) {
                                $ref = DB::table("reference_{$mdmFieldReference}")->whereIn(DB::raw("id::varchar"), $value)->orWhere("code", $value)->first();
                                if (!is_null(optional($ref)->id)) {
                                    $referenceIds[] = $ref->id;
                                }
                            }

                            $query->where(function ($q) use ($field, $referenceIds, $query, $operator) {
                                foreach ($referenceIds as $referenceId) {
                                    $q->orWhere($field, $operator, $referenceId);
                                }
                            });

                        } else {
                            $values = self::getOperatorAndValue($operator, $value);
                            $referenceIds = DB::table("reference_{$mdmFieldReference}")->where(DB::raw("id::varchar"), $values['value'])->orWhere("code", $values['value'])->pluck('id')->all();

                            $query->where(function ($q) use ($field, $referenceIds, $query, $values) {
                                foreach ($referenceIds as $referenceId) {
                                    $q->orWhere($field, $values['operator'], $referenceId);
                                }
                            });
                        }

                    } else {
                        if (is_array($value) && count($value) > 0) {
                            $query->where(function ($q) use ($field, $operator, $value) {
                                foreach ($value as $item) {
                                    $values = self::getOperatorAndValue($operator, $item);
                                    $q->orWhere(DB::raw("{$field}::varchar"), $values['operator'], $values['value']);
                                }
                            });
                        } else {
                            $values = self::getOperatorAndValue($operator, $value);
                            $query->where($field, $values['operator'], $values['value']);
                        }
                    }
                }
            }
        }

        return $query;
    }

    /**
     * Function to get operations
     *
     * @param $operator
     * @param $value
     * @return array
     */
    public static function getOperatorAndValue($operator, $value)
    {
        switch ($operator) {
            case "startWith":
                $operator = 'ILIKE';
                $value = "{$value}%";
                break;
            case "endWith":
                $operator = 'ILIKE';
                $value = "%{$value}";
                break;
            case "contain":
                $operator = 'ILIKE';
                $value = "%{$value}%";
                break;
        }

        return ['operator' => $operator, 'value' => $value];
    }

    /**
     * Function to get Sub Application Disabled Products
     *
     * @param $saf_number
     * @param bool $byId
     * @param bool $submittedOrRequested
     * @param bool $constructorDocumentId
     * @return array
     */
    public static function subApplicationDisabledProducts($saf_number, $byId = false, $submittedOrRequested = false, $constructorDocumentId = false)
    {
        $subApplicationsProducts = SingleApplicationSubApplications::where(['saf_sub_applications.saf_number' => $saf_number])
            ->join('saf_sub_application_products', 'saf_sub_applications.id', '=', 'saf_sub_application_products.subapplication_id')
            ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id');

        if ($submittedOrRequested) {
            $subApplicationsProducts->where(function ($q) {
                $q->submittedOrRequestedSubApplication();
            });
        } else {
            $subApplicationsProducts->where(function ($q) {
                $q->submittedSubApplication();
            });
        }

        if ($byId) {
            $subApplicationsProducts->where('saf_sub_applications.id', $byId);
        }

        if ($constructorDocumentId) {
            $constructorDocument = ConstructorDocument::select('document_code')->where('id', $constructorDocumentId)->first();

            $subApplicationsProducts->where('saf_sub_applications.document_code', $constructorDocument->document_code);
        }

        return $subApplicationsProducts->pluck('saf_sub_application_products.product_id')->unique()->all();
    }


    /**
     * Function to get products Id Numbers
     *
     * @return array
     */
    public function productsOrderedList()
    {
        $availableProductsIds = $this->productList->pluck('id')->all();
        $availableProductsIdNumbers = [];
        $incNumber = 1;
        for ($i = 0; $i < count($availableProductsIds); $i++) {
            $availableProductsIdNumbers[$availableProductsIds[$i]] = $incNumber;
            $incNumber++;
        }

        return $availableProductsIdNumbers;
    }

    /**
     * Function to get products Numbers
     *
     * @param $safProductIds
     * @return string
     */
    public function productNumbers($safProductIds)
    {
        $productNumbers = [];

        if (!is_null($this->productList)) {
            foreach ($this->productList->pluck('id')->all() as $product) {

                if ($safProductIds && isset($safProductIds[$product])) {
                    $productNumbers[] = $safProductIds[$product];
                }
            }
        }

        return count($productNumbers) ? implode(',', $productNumbers) : "";
    }

    /**
     * Function to get End Approved certificates with date filters
     *
     * @param $regime
     * @param $typeValue
     * @param $start
     * @param $end
     * @param $returnMethod
     * @return Collection
     */
    public function getEndApprovedCertificatesWithDuringDate($regime, $typeValue, $start, $end, $returnMethod)
    {
        return $this->join('saf', function ($join) use ($regime, $typeValue) {
            $join->on('saf.regular_number', '=', 'saf_sub_applications.saf_number');
            if ($regime == 'import') {
                $join->where('saf.importer_value', $typeValue);
            } elseif ($regime == 'export') {
                $join->where('saf.exporter_value', $typeValue);
            }
        })
            ->join('saf_sub_application_states', function ($join) {
                $join->on('saf_sub_application_states.saf_subapplication_id', '=', 'saf_sub_applications.id')->where('is_current', SingleApplication::TRUE);
            })
            ->join('constructor_states', function ($join) {
                $join->on('constructor_states.id', '=', 'saf_sub_application_states.state_id')->where(['state_approved_status' => true, 'state_type' => ConstructorStates::END_STATE_TYPE]);
            })
            ->where('saf_sub_applications.created_at', '>=', $start)
            ->where('saf_sub_applications.created_at', '<=', $end)
            ->{$returnMethod}();
    }

    /**
     * Function to get Sub Application documents
     *
     * @return Collection
     */
    public function subApplicationDocuments()
    {
        $documents = collect();

        $documentIds = SingleApplicationDocuments::leftJoin('saf_document_products', 'saf_documents.id', '=', 'saf_document_products.saf_document_id')
            ->where('saf_documents.saf_number', $this->saf_number)
            ->whereIn('saf_document_products.product_id', $this->productList->pluck('id'))->pluck('saf_documents.document_id')->toArray();

        if (count($documentIds)) {
            $documents = Document::select('id', 'document_form', 'document_access_password', 'document_type', 'document_name', 'document_description', 'document_number', 'document_release_date', 'document_file')
                ->whereIn('id', $documentIds)->get();
        }

        return $documents;
    }

    /**
     * Function to get attached documents
     *
     * @param $subApplication
     * @param $availableProductsIds
     * @param null $documentCode
     * @return SingleApplicationDocumentProducts
     */
    public static function getAttachedDocumentProducts($subApplication, $availableProductsIds, $documentCode = null)
    {
        $attachedDocumentProducts = SingleApplicationDocuments::select('saf_documents.document_id', 'saf_documents.id', 'saf_documents.added_from_module')
            ->join('document', 'saf_documents.document_id', '=', 'document.id')
            ->leftJoin('saf_document_products', 'saf_documents.id', '=', 'saf_document_products.saf_document_id')
            ->where('saf_documents.saf_number', $subApplication->saf_number)
            ->where(function ($q) use ($availableProductsIds, $subApplication) {
                $q->where('added_from_module', SingleApplicationDocuments::ADDED_FROM_MODULE_APPLICATION)->where('saf_documents.subapplication_id', $subApplication->id)
                    ->orWhere('added_from_module', SingleApplicationDocuments::ADDED_FROM_MODULE_SAF)->whereIn('saf_document_products.product_id', $availableProductsIds);
            })
            ->groupBy('saf_documents.document_id', 'saf_documents.id', 'saf_documents.added_from_module')
            ->orderBy('saf_documents.id');

        // if Sub Application has sub application in Laboratory for expertise  (if laboratory expertise and send back to saf show attached documents) -- START --
        $labApplicationId = SingleApplicationSubApplications::where('from_subapplication_id_for_labs', $subApplication->id)->labType()->pluck('id')->first();
        if ($labApplicationId) {
            $attachedDocumentProducts->orWhere(function ($q) use ($labApplicationId) {
                $q->where(['added_from_module' => SingleApplicationDocuments::ADDED_FROM_MODULE_APPLICATION, 'saf_documents.subapplication_id' => $labApplicationId]);
            });
        }

        // if in Saf Sub Application attached to doc
        $documentSubApplications = SingleApplicationDocumentSubApplications::where('sub_application_id', $subApplication->id)->pluck('saf_document_id')->all();

        if (count($documentSubApplications)) {
            $attachedDocumentProducts->orWhereIn('saf_documents.id', $documentSubApplications);
        }

        if($documentCode){
            $attachedDocumentProducts->where('document.document_type', $documentCode);
        }

        // if Sub Application has sub application in Laboratory for expertise  (if laboratory expertise and send back to saf show attached documents) -- END --
        return $attachedDocumentProducts->get();
    }

    /**
     * Function to get attached documents for pdf
     *
     * @param $subApplication
     * @param $availableProductsIds
     * @return SingleApplicationDocumentProducts
     */
    public static function getAttachedDocumentProductsForPDF($subApplication, $availableProductsIds)
    {
        $attachedDocumentProducts = SingleApplicationDocuments::select('saf_documents.document_id', 'saf_documents.id', 'saf_documents.added_from_module')
            ->join('document', 'saf_documents.document_id', '=', 'document.id')
            ->leftJoin('saf_document_products', 'saf_documents.id', '=', 'saf_document_products.saf_document_id')
            ->where('saf_documents.saf_number', $subApplication->saf_number)
            ->groupBy('saf_documents.document_id', 'saf_documents.id', 'saf_documents.added_from_module')
            ->whereIn('saf_document_products.product_id', $availableProductsIds)
            ->orderBy('saf_documents.id');

        // if Sub Application has sub application in Laboratory for expertise  (if laboratory expertise and send back to saf show attached documents) -- START --
        $labExpertiseIds = SingleApplicationExpertise::where('sub_application_id', $subApplication->id)
            ->join('saf_expertise_indicators', 'saf_expertise.id', '=', 'saf_expertise_indicators.saf_expertise_id')
            ->where('saf_expertise_indicators.status_send', SingleApplicationExpertise::STATUS_ACTIVE)
            ->pluck('saf_expertise.id')->unique()->all();

        if (count($labExpertiseIds)) {
            $attachedDocumentProducts->orWhere('added_from_module', SingleApplicationDocuments::ADDED_FROM_MODULE_LABORATORY)->whereIn('expertise_id', $labExpertiseIds)->whereIn('saf_document_products.product_id', $availableProductsIds);
        }

        // if in Saf Sub Application attached to doc
        $documentSubApplications = SingleApplicationDocumentSubApplications::where('sub_application_id', $subApplication->id)->pluck('saf_document_id')->all();

        if (count($documentSubApplications)) {
            $attachedDocumentProducts->orWhereIn('saf_documents.id', $documentSubApplications);
        }

        // if Sub Application has sub application in Laboratory for expertise  (if laboratory expertise and send back to saf show attached documents) -- END --
        return $attachedDocumentProducts->get();
    }

    /**
     * Function to return sub division info
     *
     * @param $referenceId
     * @return false|string
     */
    public static function getSubDivisionInfo($referenceId)
    {
        $returnData = [];
        $activeLanguages = activeLanguagesIdCode();
        $refName = ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS;

        $ref = DB::table($refName)->select('id', 'code')->where('id', $referenceId)->first();
        if (!is_null($ref)) {
            $data = RegionalOffice::where('office_code', $ref->code)->active()->with('ml')->first();

            if (!is_null($data)) {
                $returnData['phone_number'] = $data->phone_number ?? '';
                $returnData['accreditation_number'] = $data->accreditation_number ?? '';
                $returnData['accreditation_start_date'] = isset($data->accreditation_date_of_issue) ? Carbon::parse($data->accreditation_date_of_issue)->format('d.m.Y') : '';
                $returnData['accreditation_end_date'] = isset($data->accreditation_valid_until) ? Carbon::parse($data->accreditation_valid_until)->format('d.m.Y') : '';
                foreach ($data->ml as $item) {
                    $returnData["department_head_ml_" . $activeLanguages[$item->lng_id]] = $item->department_head ?? '';
                    $returnData["head_ml_" . $activeLanguages[$item->lng_id]] = $item->supervisor ?? '';
                    $returnData["address_ml_" . $activeLanguages[$item->lng_id]] = $item->address ?? '';
                    $returnData["name_ml_" . $activeLanguages[$item->lng_id]] = $item->name ?? '';
                }
            }
        }

        return json_encode($returnData);
    }

    /**
     * Function to get editable tabs
     *
     * @param null $tab
     * @return array
     */
    public function getTabEditable($tab = null)
    {
        $data = [];
        $roles = Auth::user()->getCurrentRoles(Menu::USER_DOCUMENTS_MENU_NAME);
        $documentCurrentState = SingleApplicationSubApplicationStates::where('saf_subapplication_id', $this->id)->orderByDesc()->firstOrFail();
        $documentMappings = UserDocuments::getDocumentMappings($this->constructor_document_id, $roles, $documentCurrentState, $this->id);
        $tabLists = ConstructorLists::margeTabLists($documentMappings, ConstructorMapping::getTabLists($documentMappings));
        $superscribe = ConstructorActionSuperscribes::where('saf_subapplication_id', $this->id)->orderByDesc()->active()->first();
        $stateHasSuperscribeMapping = ConstructorMapping::select('action')
            ->join('constructor_actions', 'constructor_mapping.action', '=', 'constructor_actions.id')
            ->where(['document_id' => $this->constructor_document_id, 'start_state' => $documentCurrentState->state_id, 'special_type' => ConstructorActions::SUPERSCRIBE_TYPE])
            ->active()
            ->first();

        $simpleAppTabs = SingleApplicationDataStructure::lastXml();
        foreach ($simpleAppTabs as $simpleAppTab) {
            $key = (string)$simpleAppTab->attributes()->key;
            $tabIsEditable = false;
            if ((isset($hiddenTabs) && in_array($key, $hiddenTabs)) || ($key == 'subtitles')) {
                continue;
            }

            if (array_key_exists($key, $tabLists) && in_array("non_visible", $tabLists[$key]) && !in_array("editable", $tabLists[$key])) {
                continue;
            } elseif (array_key_exists($key, $tabLists) && in_array("editable", $tabLists[$key])) {
                if (is_null($stateHasSuperscribeMapping)) {
                    $tabIsEditable = true;
                } elseif (!is_null($superscribe) && $superscribe->to_user == Auth::id()) {
                    $tabIsEditable = true;
                }
            }

            $data["tab-{$key}"] = $tabIsEditable;
        }

        if ($tab) {
            return $data["tab-{$tab}"];
        }

        return $data;
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @param $id2
     * @return array
     */
    public function getLogData($id, $id2)
    {
        return SingleApplicationSubApplications::where('id', $id2)->first()->toArray();
    }
}
