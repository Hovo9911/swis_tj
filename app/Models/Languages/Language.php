<?php

namespace App\Models\Languages;

use App\Models\Application\Application;
use App\Models\Application\ApplicationLanguages;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Language
 * @package App\Models\Languages
 */
class Language extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'languages';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'code',
        'show_status'
    ];

    /**
     * Function to relate with Application
     *
     * @return BelongsToMany
     */
    public function applications()
    {
        return $this->belongsToMany(Application::class, 'application_language', 'lng_id', 'application_id');
    }

    /**
     * Static Function to get Active language
     *
     * @return array
     */
    public static function getActiveLng()
    {
        $languages = [];
        $getLanguages = ApplicationLanguages::select('lng_id')->where('show_status', '1')->groupBy('lng_id')->get();

        foreach ($getLanguages as $language) {
            $languages[] = $language->lng_id;
        }

        return $languages;
    }
}
