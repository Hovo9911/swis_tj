<?php

namespace App\Models\Languages;

/**
 * Class Language
 * @package App\Models\Languages
 */
class LanguageManager
{
    /**
     * Function to Store data to DB
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $languages = new Language($data);
        $languages->save();
    }

    /**
     * Function to Update data
     *
     * @param array $data
     * @param $id
     */
    public function update(array $data, $id)
    {
        $language = Language::where(['id' => $id])->firstOrFail();

        $language->update($data);
    }

    /**
     * Function to Delete data
     *
     * @param array $deleteItems
     */
    public function delete(array $deleteItems)
    {
        Language::whereIn('id', $deleteItems)->update([
            'show_status' => Language::STATUS_DELETED,
        ]);
    }
}
