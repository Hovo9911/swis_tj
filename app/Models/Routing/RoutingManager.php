<?php

namespace App\Models\Routing;

use App\Models\Agency\Agency;
use Illuminate\Support\Facades\Auth;

/**
 * Class ConstructorRoutingManager
 * @package App\Models\ConstructorRouting
 */
class RoutingManager
{
    /**
     * Function to Store data to DB
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $data['user_id'] = Auth::id();
        $data['company_tax_id'] = Agency::where('id', $data['agency_id'])->pluck('tax_id')->first();

        $routing = new Routing($data);
        $routing->save();
    }

    /**
     * Function to Update data
     *
     * @param array $data
     * @param int $id
     */
    public function update(array $data, int $id)
    {
        $routing = Routing::where('id', $id)->checkUserHasRole()->firstOrFail();
        $data['company_tax_id'] = Agency::where('id', $data['agency_id'])->pluck('tax_id')->first();

        $data['ref_classificator_codes'] = $data['ref_classificator_codes'] ?? null;
        $data['rule'] = $data['rule'] ?? null;
        $data['saf_controlled_fields'] = $data['saf_controlled_fields'] ?? null;
        $data['is_manual'] = $data['is_manual'] ?? 0;

        $routing->update($data);
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @param null $id2
     * @return array
     */
    public function getLogData($id, $id2 = null)
    {
        return Routing::where('id', $id)->first()->toArray();
    }
}
