<?php

namespace App\Models\Routing;

use App\Models\BaseModel;
use App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure;
use Illuminate\Database\Query\Builder;

/**
 * Class ConstructorRouting
 * @package App\Models\ConstructorRouting
 */
class Routing extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'routing';

    /**
     * @var bool
     */
    protected $hasAdminInfo = true;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'rule',
        'company_tax_id',
        'agency_id',
        'constructor_document_id',
        'ref_classificator_ids',
        'ref_classificator_codes',
        'saf_controlled_fields',
        'regime',
        'is_manual',
        'show_status'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'created_admin_id',
        'created_admin_ip',
        'updated_at',
        'updated_admin_id',
        'updated_admin_ip',
        'deleted_admin_id',
        'deleted_admin_ip'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'ref_classificator_ids' => 'array',
        'ref_classificator_codes' => 'array',
        'saf_controlled_fields' => 'array',
    ];

    /**
     * Function to get only formatted sub applications (not manual created)
     *
     * @param $query
     * @return Builder
     */
    public function scopeManualRouting($query)
    {
        return $query->where('is_manual', Routing::TRUE);
    }

    /**
     * Function to get only formatted sub applications (not manual created)
     *
     * @param $query
     * @return Builder
     */
    public function scopeNotManualRouting($query)
    {
        return $query->where('is_manual', Routing::FALSE);
    }

    /**
     * Function to get active Routings with active Constructor Documents
     *
     * @param $regime
     * @return Routing
     */
    public static function activeRoutings($regime)
    {
        return Routing::select('routing.*')->where('regime', $regime)->join('constructor_documents', 'routing.constructor_document_id', '=', 'constructor_documents.id')->where('constructor_documents.show_status', Routing::STATUS_ACTIVE)->notManualRouting()->active()->get();
    }

    /**
     * Function to get Saf Xml field path
     *
     * @param $field
     * @return string
     */
    public static function getSafXmlPathByFieldId($field)
    {
        $lastXML = SingleApplicationDataStructure::lastXml();

        $parentField = $lastXML->xpath("//field[@id='" . $field . "']/ancestor::tab");

        $path = '';
        if (isset($parentField[0])) {
            $blockName = '';
            if (isset($parentField[0]->block)) {
                $blockName = $parentField[0]->block->attributes()->name;
            } elseif (isset($parentField[0]->subBlock)) {
                $blockName = $parentField[0]->subBlock->attributes()->name;
            } elseif (isset($parentField[0]->typeBlock)) {
                $blockName = $parentField[0]->typeBlock->attributes()->name;
            } elseif (isset($parentField[0]->tabBlock)) {
                $blockName = $parentField[0]->tabBlock->attributes()->name;
            }

            if ($blockName) {
                $blockName = ' / ' . trans((string)$blockName);
            }

            $path = trans((string)$parentField[0]->attributes()->name) . $blockName;
        }

        return $path;
    }
}
