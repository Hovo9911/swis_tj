<?php

namespace App\Models\Routing;

use App\Http\Controllers\Core\DataTableSearch;
use Illuminate\Support\Facades\DB;

/**
 * Class RoutingSearch
 * @package App\Models\Routing
 */
class RoutingSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        return $query->get();
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($this->orderByCol, $this->orderByType ?? 'desc');
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $query = Routing::select('*')->checkUserHasRole()->notDeleted();

        if (isset($this->searchData['id'])) {
            $query->where(DB::raw('id::varchar'), $this->searchData['id']);
        }

        if (isset($this->searchData['name'])) {
            $query->where('name', 'ILIKE', '%' . strtolower(trim($this->searchData['name'])) . '%');
        }

        if (isset($this->searchData['rule'])) {
            $query->where('rule', 'ILIKE', '%' . strtolower(trim($this->searchData['rule'])) . '%');
        }

        if (isset($this->searchData['agency_id'])) {
            $query->where('agency_id', $this->searchData['agency_id']);
        }

        if (isset($this->searchData['constructor_document_id'])) {
            $query->where('constructor_document_id', $this->searchData['constructor_document_id']);
        }

        if (isset($this->searchData['ref_classificator_codes'])) {
            $query->where(function ($q) {
                foreach ((array)$this->searchData['ref_classificator_codes'] as $item) {
                    $q->orWhere(DB::raw('ref_classificator_codes::varchar'), 'ILIKE', "%{$item}%");
                }
            });
        }

        if (isset($this->searchData['regime'])) {
            $query->where('regime', $this->searchData['regime']);
        }

        if (isset($this->searchData['show_status'])) {
            $query->where('show_status', $this->searchData['show_status']);
        }

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}
