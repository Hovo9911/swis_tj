<?php

namespace App\Models\UserAuthFail;

use App\Models\BaseModel;

/**
 * Class UserAuthFail
 * @package App\Models\UserAuthFail
 */
class UserAuthFail extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'user_fail_auth';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'username',
        'hash',
        'last_increment_date',
        'count',
        'is_blocked',
        'is_email_send'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var integer
     */
    const BLOCKED = 1;
    const NOT_BLOCKED = 0;

    /**
     * Function to check user blocked
     *
     * @param $username
     * @param $request
     * @return UserAuthFail
     */
    public static function isBlocked($request, $username)
    {
        return (new self)->where([
            'username' => $username,
            'is_blocked' => UserAuthFail::BLOCKED,
            'hash' => sha1($request->input('email') . $request->header('User-Agent') . $request->ip())
        ])->first();
    }

    /**
     * Function to get blocked users when blockTime >= updated_at
     *
     * @param $blockTime
     * @return UserAuthFail
     */
    public static function getBlockedUsers($blockTime)
    {
        return (new self)->select('id', 'username', 'updated_at')
            ->where('is_blocked', UserAuthFail::BLOCKED)
            ->where('updated_at', '<=', $blockTime)
            ->get();
    }
}
