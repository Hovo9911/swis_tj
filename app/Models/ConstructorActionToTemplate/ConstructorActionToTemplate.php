<?php

namespace App\Models\ConstructorActionToTemplate;

use App\Models\BaseModel;
use App\Models\ConstructorTemplates\ConstructorTemplates;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class ConstructorActionToTemplate
 * @package App\Models\ConstructorActionToTemplate
 */
class ConstructorActionToTemplate extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'constructor_action_to_template';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'constructor_action_id',
        'constructor_template_id'
    ];

    /**
     * Function to relate with template
     *
     * @return HasOne
     */
    public function template()
    {
        return $this->hasOne(ConstructorTemplates::class, 'id', 'constructor_template_id')->where('show_status', '1');
    }

    /**
     * Function to get and generate already checked element for view
     *
     * @param $id
     * @return array
     */
    public static function getAlreadyChecked($id)
    {
        $returnArray = [];
        $data = self::where('constructor_action_id', $id)->get();

        foreach ($data as $check) {
            $returnArray[] = $check->constructor_template_id;
        }

        return $returnArray;
    }

}
