<?php

namespace App\Models\Reports;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Reports
 * @package App\Models\Reports
 */
class Reports extends BaseModel
{
    /**
     * @var string (Reports unique codes)
     */
    const REPORTS_TJST_CODE = 'TjSt';

    const REPORT_TJST_1_CODE = self::REPORTS_TJST_CODE . '_1';
    const REPORT_TJST_2_CODE = self::REPORTS_TJST_CODE . '_2';
    const REPORT_TJST_3_CODE = self::REPORTS_TJST_CODE . '_3';
    const REPORT_TJST_4_CODE = self::REPORTS_TJST_CODE . '_4';
    const REPORT_TJST_5_CODE = self::REPORTS_TJST_CODE . '_5';
    const REPORT_TJST_6_CODE = self::REPORTS_TJST_CODE . '_6';
    const REPORT_TJST_7_CODE = self::REPORTS_TJST_CODE . '_7';
    const REPORT_TJST_8_CODE = self::REPORTS_TJST_CODE . '_8';
    const REPORT_TJST_9_CODE = self::REPORTS_TJST_CODE . '_9';
    const REPORT_TJST_10_CODE = self::REPORTS_TJST_CODE . '_10';
    const REPORT_TJST_11_CODE = self::REPORTS_TJST_CODE . '_11';
    const REPORT_TJST_12_CODE = self::REPORTS_TJST_CODE . '_12';
    const REPORT_TJST_13_CODE = self::REPORTS_TJST_CODE . '_13';
    const REPORT_TJST_14_CODE = self::REPORTS_TJST_CODE . '_14';
    const REPORT_TJST_15_CODE = self::REPORTS_TJST_CODE . '_15';
    const REPORT_TJST_16_CODE = self::REPORTS_TJST_CODE . '_16';
    const REPORT_TJST_17_CODE = self::REPORTS_TJST_CODE . '_17';
    const REPORT_TJST_18_CODE = self::REPORTS_TJST_CODE . '_18';
    const REPORT_TJST_19_CODE = self::REPORTS_TJST_CODE . '_19';
    const REPORT_TJST_20_CODE = self::REPORTS_TJST_CODE . '_20';
    const REPORT_TJST_21_CODE = self::REPORTS_TJST_CODE . '_21';

    // --------------------

    const REPORT_TJMOH_CODE = 'TjMoH';

    const REPORT_TJMOH_1_CODE = self::REPORT_TJMOH_CODE . '_1';
    const REPORT_TJMOH_2_CODE = self::REPORT_TJMOH_CODE . '_2';
    const REPORT_TJMOH_3_CODE = self::REPORT_TJMOH_CODE . '_3';
    const REPORT_TJMOH_4_CODE = self::REPORT_TJMOH_CODE . '_4';
    const REPORT_TJMOH_5_CODE = self::REPORT_TJMOH_CODE . '_5';
    const REPORT_TJMOH_6_CODE = self::REPORT_TJMOH_CODE . '_6';
    const REPORT_TJMOH_7_CODE = self::REPORT_TJMOH_CODE . '_7';
    const REPORT_TJMOH_8_CODE = self::REPORT_TJMOH_CODE . '_8';

    // --------------------

    const REPORT_TJFSC_CODE = 'TjFSC';

    const REPORT_TJFSC_1_CODE = self::REPORT_TJFSC_CODE . '_1';
    const REPORT_TJFSC_2_CODE = self::REPORT_TJFSC_CODE . '_2';
    const REPORT_TJFSC_3_CODE = self::REPORT_TJFSC_CODE . '_3';
    const REPORT_TJFSC_4_CODE = self::REPORT_TJFSC_CODE . '_4';
    const REPORT_TJFSC_5_CODE = self::REPORT_TJFSC_CODE . '_5';
    const REPORT_TJFSC_6_CODE = self::REPORT_TJFSC_CODE . '_6';
    const REPORT_TJFSC_7_CODE = self::REPORT_TJFSC_CODE . '_7';
    const REPORT_TJFSC_8_CODE = self::REPORT_TJFSC_CODE . '_8';
    const REPORT_TJFSC_9_CODE = self::REPORT_TJFSC_CODE . '_9';

    // --------------------

    const REPORT_GLOBAL_REPORT_CODE = 'GlobalReport';

    const REPORT_GLOBAL_REPORT_1_CODE = self::REPORT_GLOBAL_REPORT_CODE . '_1';
    const REPORT_GLOBAL_REPORT_2_CODE = self::REPORT_GLOBAL_REPORT_CODE . '_2';
    const REPORT_GLOBAL_REPORT_3_CODE = self::REPORT_GLOBAL_REPORT_CODE . '_3';
    const REPORT_GLOBAL_REPORT_4_CODE = self::REPORT_GLOBAL_REPORT_CODE . '_4';

    // -------------------

    const REPORT_SW_ADMIN_CODE = 'SwAdminReport';

    const REPORT_SW_ADMIN_REPORT_1_CODE = self::REPORT_SW_ADMIN_CODE . '-1';
    const REPORT_SW_ADMIN_REPORT_2_CODE = self::REPORT_SW_ADMIN_CODE . '-2';
    const REPORT_SW_ADMIN_REPORT_3_CODE = self::REPORT_SW_ADMIN_CODE . '-3';
    const REPORT_SW_ADMIN_REPORT_4_CODE = self::REPORT_SW_ADMIN_CODE . '-4';
    const REPORT_SW_ADMIN_REPORT_5_CODE = self::REPORT_SW_ADMIN_CODE . '-5';
    const REPORT_SW_ADMIN_REPORT_6_CODE = self::REPORT_SW_ADMIN_CODE . '-6';
    const REPORT_SW_ADMIN_REPORT_7_CODE = self::REPORT_SW_ADMIN_CODE . '-7';

    // ------------------

    const REPORT_BROKER_CODE = 'BrokerReport';

    const REPORT_BROKER_REPORT_1_CODE = self::REPORT_BROKER_CODE . '_1';
    const REPORT_BROKER_REPORT_2_CODE = self::REPORT_BROKER_CODE . '_2';
    const REPORT_BROKER_REPORT_3_CODE = self::REPORT_BROKER_CODE . '_3';

    // ------------------

    const REPORT_TJ_CCI_TPP_CODE = 'CCI_TPP';

    const REPORT_TJ_CCI_TPP = self::REPORT_TJ_CCI_TPP_CODE . '_1';

    // ------------------

    const REPORT_TJCS_CODE = 'TjCs';

    const REPORT_TJCS_1_CODE = self::REPORT_TJCS_CODE . '_1';

    // ------------------

    const REPORT_SUB_APPLICATION_PROCESS_TIME = 'Sub_application_process_time';

    /**
     * @var string (Reports download types)
     */
    const DOWNLOAD_TYPE_PDF = 'pdf';
    const DOWNLOAD_TYPE_XLS = 'xls';
    const DOWNLOAD_TYPE_HTML = 'html';

    /**
     * @var array
     */
    const DOWNLOAD_TYPES = [
        self::DOWNLOAD_TYPE_PDF,
        self::DOWNLOAD_TYPE_XLS,
        self::DOWNLOAD_TYPE_HTML,
    ];

    /**
     * @var string
     */
    const REPORT_TYPE_AGENCY_WITH_TAX_ID = 1;
    const REPORT_TYPE_AGENCY_ALL = 2;
    const REPORT_TYPE_SW_ADMIN = 3;
    const REPORT_TYPE_ONLY_BROKER = 4;
    const REPORT_TYPE_AGENCY_ALL_AND_SW_ADMIN = 5;
    const REPORT_TYPE_ALL = 6;
    const REPORT_TYPE_BROKER_ALL_AND_SW_ADMIN = 7;

    /**
     * @var string
     */
    const IMPORT_QUARANTINE_PERMISSION_CODE = '6033';
    const VETERINARY_RESOLUTION_CODE = '6012';
    const PHYTOSANITARY_CERTIFICATE_CODE = '01207';
    const VETERINARY_CERTIFICATE_CODE = 'Veterinary';

    /**
     * @var string
     */
    const JASPER_REPORT_UNIT = 'reportUnit';

    /**
     * @var array (Agency report types)
     */
    const REPORT_TYPES_FOR_AGENCIES = [
        self::REPORT_TYPE_AGENCY_ALL,
        self::REPORT_TYPE_AGENCY_ALL_AND_SW_ADMIN,
    ];

    /**
     * @var array (Sw admin report types)
     */
    const REPORT_TYPES_FOR_SW_ADMIN = [
        self::REPORT_TYPE_SW_ADMIN,
        self::REPORT_TYPE_BROKER_ALL_AND_SW_ADMIN,
        self::REPORT_TYPE_AGENCY_ALL_AND_SW_ADMIN,
    ];

    /**
     * @var array (Broker report types)
     */
    const REPORT_TYPES_FOR_ONLY_BROKERS = [
        self::REPORT_TYPE_ONLY_BROKER,
        self::REPORT_TYPE_BROKER_ALL_AND_SW_ADMIN,
    ];

    /**
     * @var int
     */
    const TJ_MOH_DOCUMENT_ID = 42;

    /**
     * @var string
     */
    public $table = 'reports';

    /**
     * Function to return ml
     *
     * @return HasMany
     */
    public function ml()
    {
        return $this->hasMany(ReportsMl::class, 'report_id', 'id');
    }

    /**
     * Function to return current ml
     *
     * @return HasOne
     */
    public function current()
    {
        return $this->hasOne(ReportsMl::class, 'report_id', 'id')->notDeleted()->where('lng_id', cLng('id'))->first();
    }
}
