<?php

namespace App\Models\Reports;

use App\Models\ConstructorStates\ConstructorStates;
use App\Models\DataModel\DataModel;
use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplicationObligations\SingleApplicationObligations;
use App\Models\SingleApplicationProducts\SingleApplicationProducts;
use App\Models\SingleApplicationProductsBatch\SingleApplicationProductsBatch;
use App\Models\SingleApplicationSubApplicationProducts\SingleApplicationSubApplicationProducts;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class ReportManager
 * @package App\Models\Reports
 */
class ReportManager
{
    /**
     * @var string
     */
    const OBLIGATION_TYPE = '7777';
    const LAB_MDM_FIELD_ID = '412';

    /**
     * Function to get sub applications with search criteria
     *
     * @param null $documentState
     * @param $constructorDocumentId
     * @param null $regime
     * @param null $startDate
     * @param null $endDate
     * @param array $where
     * @param array $whereIn
     * @param bool $getResult
     * @return SingleApplicationSubApplications
     */
    public function getSubApplicationIds($documentState = null, $constructorDocumentId = null, $regime = null, $startDate = null, $endDate = null, $where = [], $whereIn = [], $getResult = true)
    {
        $dateFieldName = $subApplicationSubmissionDate = null;

        //
        $subApplications = SingleApplicationSubApplications::select('saf_sub_applications.*', 'saf_sub_application_states.state_id')
            ->join('saf', 'saf.regular_number', '=', 'saf_sub_applications.saf_number')
            ->join('saf_sub_application_states', function ($q) {
                $q->on('saf_sub_application_states.saf_subapplication_id', '=', 'saf_sub_applications.id')->where('is_current', '1');
            });

            // is Approved
        if (ConstructorStates::checkEndStateStatus($documentState, ConstructorStates::END_STATE_APPROVED)) {
            $dateFieldName = 'permission_date';

            // is Rejected
        } elseif (ConstructorStates::checkEndStateStatus($documentState, ConstructorStates::END_STATE_NOT_APPROVED)) {
            $dateFieldName = 'rejection_date';

            // is Submitted
        } else {
            $subApplicationSubmissionDate = true;
        }

        //
        if (!Auth::user()->isSwAdmin()) {
            if (Auth::user()->isOnlyBroker()) {
                $subApplications->where('saf_sub_applications.user_company_tax_id', Auth::user()->companyTaxId());
            } else {
                $subApplications->where('saf_sub_applications.company_tax_id', Auth::user()->companyTaxId());
            }
        }

        //
        if ($constructorDocumentId) {
            if (is_array($constructorDocumentId)) {
                $subApplications->whereIn('constructor_document_id', $constructorDocumentId);
            } else {
                $subApplications->where('constructor_document_id', $constructorDocumentId);
            }
        }

        //
        if (!is_null($documentState)) {

            if (is_array($documentState)) {
                $subApplications->whereIn('saf_sub_application_states.state_id', $documentState);
            } else {
                $subApplications->where('saf_sub_application_states.state_id', $documentState);
            }

        }

        //
        if (!is_null($regime)) {
            if (is_array($regime)) {
                $subApplications->whereIn('saf.regime', $regime);
            } else {
                $subApplications->where('saf.regime', $regime);
            }
        }

        //
        if (!is_null($subApplicationSubmissionDate)) {

            if ($startDate) {
                $subApplications->where("saf_sub_applications.status_date", '>=', $startDate);
            }

            if ($endDate) {
                $subApplications->where("saf_sub_applications.status_date", '<=', $endDate);
            }
        }

        if (!is_null($startDate) && !is_null($dateFieldName)) {
            $subApplications->where("saf_sub_applications.{$dateFieldName}", '>=', $startDate);
        }

        if (!is_null($endDate) && !is_null($dateFieldName)) {
            $subApplications->where("saf_sub_applications.{$dateFieldName}", '<=', $endDate);
        }

        //
        if (count($whereIn)) {

            foreach ($whereIn as $column => $values) {

                if ($column == 'product_code') {
                    $subApplications->leftJoin('saf_sub_application_products', 'saf_sub_application_products.subapplication_id', '=', 'saf_sub_applications.id')
                        ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id');
                }

                $subApplications->whereIn($column, (array)$values);
            }
        }

        //
        if (!empty($where)) {
            $subApplications->where($where);
        }

        $subApplications->orderByDesc('saf_sub_applications.id');

        if (!$getResult) {
            return $subApplications;
        }

        return $subApplications->get()->unique();
    }

    /**
     * Function to get sub application all data by filters
     *
     * @param $subApplicationIds
     * @param $filteredHsCodes
     * @return array
     */
    public function getSubApplicationAllData($subApplicationIds, $filteredHsCodes)
    {
        $subApplicationsProductsIds = SingleApplicationSubApplicationProducts::whereIn('subapplication_id', $subApplicationIds)->pluck('product_id')->all();

        $reportRow = [];
        if (count($filteredHsCodes)) {
            foreach ($filteredHsCodes as $key => $hsCode) {

                $refHsCodeId = $this->refHsDataIds($hsCode);

                if (count($refHsCodeId)) {
                    $hsCodeProductsIds = SingleApplicationProducts::whereIn('id', $subApplicationsProductsIds)
                        ->whereIn('product_code', $refHsCodeId)
                        ->pluck('id')
                        ->all();

                    $approvedQty = SingleApplicationProductsBatch::selectRaw('SUM(saf_products_batch.approved_quantity::numeric) as approved_quantity ,measurement_unit')->whereIn('saf_product_id', $hsCodeProductsIds)->groupBy('measurement_unit')->get();

                    foreach ($approvedQty as $value) {
                        $reportRow[$hsCode][$value['measurement_unit']]['approvedQty'] = isset($reportRow[$hsCode][$value['measurement_unit']]) ? $reportRow[$hsCode][$value['measurement_unit']]['approved_quantity'] : 0;
                        $reportRow[$hsCode][$value['measurement_unit']]['approvedQty'] = +$value['approved_quantity'];
                    }
                }
            }
        }

        return $reportRow;
    }

    /**
     * Function to get sub applications(obligations) with search criteria
     *
     * @param $documentState
     * @param $documentId
     * @param $startDate
     * @param $endDate
     * @param null $subDivisions
     * @param null $hsCode
     * @return array
     */
    public function getSubApplicationsWithObligations($documentState, $documentId, $startDate, $endDate, $subDivisions = null, $hsCode = null)
    {
        $constructorStateInfo = ConstructorStates::where('id', $documentState)->first() ?? '';

        $dataModel = new DataModel();

        if (($constructorStateInfo->state_type == ConstructorStates::END_STATE_TYPE and $constructorStateInfo->state_approved_status == ConstructorStates::END_STATE_NOT_APPROVED) or $constructorStateInfo->state_type == ConstructorStates::CANCELED_STATE_TYPE) {

            //Rejection date
            $documentDatasetMdm = DocumentsDatasetFromMdm::select('id', 'field_id')->where('document_id', $documentId)->where('mdm_field_id', $dataModel->getMdmFieldIDByConstant(DataModel::REJECTION_DATE_MDM_ID))->first();

        } else {

            //Permission date
            $documentDatasetMdm = DocumentsDatasetFromMdm::select('id', 'field_id')->where('document_id', $documentId)->where('mdm_field_id', $dataModel->getMdmFieldIDByConstant(DataModel::PERMISSION_DATE_MDM_ID))->first();
        }

        $mdmFieldId = $documentDatasetMdm->field_id;

        $subDivisionsTableName = ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS;

        $subApplications = DB::table($subDivisionsTableName)
            ->select([
                'saf.regime',
                "{$subDivisionsTableName}.id as agency_subdivision_id",
                DB::raw("string_agg(DISTINCT saf_sub_applications.id::character, ',') as sub_application_ids"),
                DB::raw("COUNT(saf_sub_applications.id) as certificates_count"),
            ])
            ->join('saf_sub_applications', 'saf_sub_applications.agency_subdivision_id', '=', "{$subDivisionsTableName}.id")
            ->join('saf', 'saf.regular_number', '=', 'saf_sub_applications.saf_number')
            ->join('saf_sub_application_states', function ($q) {
                $q->on('saf_sub_application_states.saf_subapplication_id', '=', 'saf_sub_applications.id')->where('is_current', '1');
            });
        if (!is_null($hsCode)) {
            $subApplications->join('saf_sub_application_products', 'saf_sub_applications.id', '=', 'saf_sub_application_products.subapplication_id')
                ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
                ->whereIn('saf_products.product_code', $hsCode);
        }
        $subApplications->where('saf_sub_applications.company_tax_id', '=', Auth::user()->companyTaxId())
            ->where('saf_sub_application_states.state_id', $documentState)
            ->where("saf_sub_applications.custom_data->{$mdmFieldId}", '>=', $startDate)
            ->where("saf_sub_applications.custom_data->{$mdmFieldId}", '<=', $endDate);


        if (!is_null($subDivisions[0])) {
            $subApplications->whereIn('agency_subdivision_id', $subDivisions[0]);
        }

        $subApplications = $subApplications->whereNotNull('saf_sub_applications.agency_subdivision_id')
            ->groupBy("{$subDivisionsTableName}.id", 'saf.regime')
            ->get();

        foreach ($subApplications as $subApplication) {

            $sumObligation = $this->sumObligations($subApplication->sub_application_ids);

            $subApplication->certificates_sum = $sumObligation;
        }

        return $subApplications;
    }

    /**
     * Function to get agency active subdivisions
     *
     * @return Collection
     */
    public function getAllSubdivisions()
    {
        return DB::table(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS)
            ->where('company_tax_id', Auth::user()->companyTaxId())
            ->pluck('id')
            ->all();
    }

    /**
     * Function to get obligations sum
     *
     * @param $subApplicationsId
     * @return integer
     */
    public function sumObligations($subApplicationsId)
    {
        $subApplicationsId = explode(',', $subApplicationsId);

        return SingleApplicationObligations::whereIn('subapplication_id', $subApplicationsId)
            ->whereRaw('saf_obligations.obligation = saf_obligations.payment')
            ->join('reference_obligation_budget_line', function ($query) {
                $query->on('reference_obligation_budget_line.id', '=', 'saf_obligations.ref_budget_line_id');
            })
            ->join('reference_obligation_type', function ($query) {
                $query->on('reference_obligation_type.id', '=', 'reference_obligation_budget_line.budget_line')->where('reference_obligation_type.code', self::OBLIGATION_TYPE);
            })->sum('saf_obligations.obligation');
    }

    /**
     * Function to get Hs code ids
     *
     * @param $searchCode
     * @return array
     */
    public function refHsDataIds($searchCode)
    {
        return DB::table(ReferenceTable::REFERENCE_HS_CODE_6)->whereRaw('LENGTH(code) = 10')->where('code', 'ILIKE', $searchCode . '%')->pluck('id')->all();
    }
}