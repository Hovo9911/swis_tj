<?php

namespace App\Models\Reports;

use App\Models\Reports\Managers\ReportManager_Broker;
use App\Models\Reports\Managers\ReportManager_CCI_TPP;
use App\Models\Reports\Managers\ReportManager_Global;
use App\Models\Reports\Managers\ReportManager_SubApplication;
use App\Models\Reports\Managers\ReportManager_SwAdmin;
use App\Models\Reports\Managers\ReportManager_TjCs;
use App\Models\Reports\Managers\ReportManager_TjFSC;
use App\Models\Reports\Managers\ReportManager_TjMoH;
use App\Models\Reports\Managers\ReportManager_TjSt;
use Exception;

/**
 * Class ReportManagerFactory
 * @package App\Models\Reports
 */
class ReportManagerFactory
{
    /**
     * Function to init Manager
     *
     * @param $reportCode
     * @return ReportManager_Broker|ReportManager_CCI_TPP|ReportManager_Global|ReportManager_SubApplication|ReportManager_SwAdmin|ReportManager_TjCs|ReportManager_TjFSC|ReportManager_TjMoH|ReportManager_TjSt
     *
     * @throws Exception
     */
    public function initializeManager($reportCode){

        // Broker
        if(substr($reportCode, 0, strlen(Reports::REPORT_BROKER_CODE)) === Reports::REPORT_BROKER_CODE){
            return new ReportManager_Broker();
        }

        // CCI_TPP
        if(substr($reportCode, 0, strlen(Reports::REPORT_TJ_CCI_TPP_CODE)) === Reports::REPORT_TJ_CCI_TPP_CODE){
            return new ReportManager_CCI_TPP();
        }

        // Global
        if(substr($reportCode, 0, strlen(Reports::REPORT_GLOBAL_REPORT_CODE)) === Reports::REPORT_GLOBAL_REPORT_CODE){
            return new ReportManager_Global();
        }

        // SubApplication
        if(substr($reportCode, 0, strlen(Reports::REPORT_SUB_APPLICATION_PROCESS_TIME)) === Reports::REPORT_SUB_APPLICATION_PROCESS_TIME){
            return new ReportManager_SubApplication();
        }

        // SwAdmin
        if(substr($reportCode, 0, strlen(Reports::REPORT_SW_ADMIN_CODE)) === Reports::REPORT_SW_ADMIN_CODE){
            return new ReportManager_SwAdmin();
        }

        // TjCs
        if(substr($reportCode, 0, strlen(Reports::REPORT_TJCS_CODE)) === Reports::REPORT_TJCS_CODE){
            return new ReportManager_TjCs();
        }

        // TjFsc
        if(substr($reportCode, 0, strlen(Reports::REPORT_TJFSC_CODE)) === Reports::REPORT_TJFSC_CODE){
            return new ReportManager_TjFSC();
        }

        // TjMoh
        if(substr($reportCode, 0, strlen(Reports::REPORT_TJMOH_CODE)) === Reports::REPORT_TJMOH_CODE){
            return new ReportManager_TjMoH();
        }

        // TjSt
        if(substr($reportCode, 0, strlen(Reports::REPORTS_TJST_CODE)) === Reports::REPORTS_TJST_CODE){
            return new ReportManager_TjSt();
        }

        throw new Exception('Unexpected report type');
    }
}