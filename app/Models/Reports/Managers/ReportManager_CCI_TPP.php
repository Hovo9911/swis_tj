<?php

namespace App\Models\Reports\Managers;

use App\Models\Reports\ReportManager;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;

// ПАЛАТАИ САВДО ВА САНОАТИ ҶУМҲУРИИ ТОҶИКИСТОН

/**
 * Class ReportManager_CCI_TPP
 * @package App\Models\Reports\Managers
 */
class ReportManager_CCI_TPP extends ReportManager implements ReportManagerInterface
{
    /**
     * Function to get report data
     *
     * @param string $reportCode
     * @param array $inputData
     * @return array
     */
    public function getData(string $reportCode, array $inputData)
    {
        return $this->$reportCode($inputData);
    }

    /**
     * Function to Report CCI_TPP_1
     *
     * @param $data
     * @return array
     */
    private function CCI_TPP_1($data)
    {
        // Hs Code
        $subAppStart = $this->getSubApplicationIds($data['document_status'], $data['document_id'], null, $data['start_date_start'], $data['start_date_end']);

        $subApplications = SingleApplicationSubApplications::
        select('saf_sub_applications.*', 'saf_sub_application_states.state_id', 'saf.data as safData',
            'saf_products.product_code as productCode',
            'saf_products.product_description as productDescription',
            'saf_products.brutto_weight as productBruttoWeight',
            'saf_products.netto_weight as productNettoWeight'
        )
            ->join('saf_sub_application_states', function ($q) {
                $q->on('saf_sub_application_states.saf_subapplication_id', '=', 'saf_sub_applications.id')->where('is_current', '1');
            })
            ->join('saf_sub_application_products', 'saf_sub_applications.id', '=', 'saf_sub_application_products.subapplication_id')
            ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
            ->join('saf', 'saf.regular_number', '=', 'saf_sub_applications.saf_number')
            ->whereIn('saf_sub_applications.id', $subAppStart->pluck('id')->all());

        if (isset($data['hs_code'])) {
            $refHsCodeId = $this->refHsDataIds($data['hs_code']);
            $subApplications->whereIn('product_code', $refHsCodeId);
        }

        $subApplications = $subApplications->get();

        return $subApplications;
    }
}