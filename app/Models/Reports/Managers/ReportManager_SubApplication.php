<?php

namespace App\Models\Reports\Managers;

use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\Reports\ReportManager;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationSubApplicationStates\SingleApplicationSubApplicationStates;
use Carbon\Carbon;

/**
 * Class ReportManager_SubApplication
 * @package App\Models\Reports\Managers
 */
class ReportManager_SubApplication extends ReportManager implements ReportManagerInterface
{
    /**
     * Function to get report data
     *
     * @param string $reportCode
     * @param array $inputData
     * @return array
     */
    public function getData(string $reportCode, array $inputData)
    {
        return $this->ReportSubApplicationProcessTime($inputData);
    }

    /**
     * Function to ReportSubApplicationProcessTime
     *
     * @param $data
     * @return array
     */
    private function ReportSubApplicationProcessTime($data)
    {
        $returnData = [];
        $documentId = isset($data['document_id']) ? $data['document_id'] : ConstructorDocument::getConstructorDocumentForAgency($data['agency'])->pluck('id')->all();
        $regime = $data['regime'] ?? SingleApplication::REGIMES;
        $startDateStart = $data['start_date_start'];
        $startDateEnd = $data['start_date_end'];
        $subApplicationNumber = $data['sub_application_number'];

        $subApplications = SingleApplicationSubApplicationStates::select([
            'saf_sub_applications.saf_number',
            'saf_sub_applications.agency_id',
            'saf_sub_applications.constructor_document_id',
            'saf_sub_applications.document_number',
            'saf_sub_applications.all_data->data->SAF_ID_1 as regime',
            'saf_sub_application_states.state_id',
            'saf_sub_application_states.is_current',
            'saf_sub_application_states.created_at',
        ])
            ->join('saf_sub_applications', 'saf_sub_applications.id', '=', 'saf_sub_application_states.saf_subapplication_id')
            ->join('saf', 'saf.regular_number', '=', 'saf_sub_applications.saf_number')
            ->when($documentId, function ($q) use ($documentId) {
                (is_array($documentId)) ? $q->whereIn('constructor_document_id', $documentId) : $q->where('constructor_document_id', $documentId);
            })
            ->when($regime, function ($q) use ($regime) {
                (is_array($regime)) ? $q->whereIn('saf.regime', $regime) : $q->where('saf.regime', $regime);
            })
            ->when($startDateStart, function ($q) use ($startDateStart) {
                $q->where("saf_sub_applications.status_date", '>=', $startDateStart);
            })
            ->when($startDateEnd, function ($q) use ($startDateEnd) {
                $q->where("saf_sub_applications.status_date", '<=', $startDateEnd);
            })
            ->when($subApplicationNumber, function ($q) use ($subApplicationNumber) {
                $q->where("saf_sub_applications.document_number", 'ilike', "%{$subApplicationNumber}%");
            })
            ->where('saf_sub_applications.company_tax_id', $data['agency'])
            ->orderBy('saf_subapplication_id', 'asc')
            ->orderBy('saf_sub_application_states.id', 'asc')
            ->with(['agency.currentMl','state.currentMl','document.currentMl'])
            ->get();


        foreach ($subApplications as $key => $subApplication) {
            $start = new Carbon(Carbon::createFromFormat('d/m/Y H:i:s', $subApplication->created_at));
            $end = new Carbon(Carbon::createFromFormat('d/m/Y H:i:s', (isset($subApplications[$key + 1]) ? $subApplications[$key + 1]->created_at : $subApplication->created_at)));
            $diff = $end->diff($start);
            $returnData[] = [
                'saf_number' => $subApplication->saf_number,
                'agency' =>$subApplication->agency->currentMl->name ?? '',
                'document' => $subApplication->document->currentMl->document_name ?? '',
                'regime' => $subApplication->regime,
                'saf_sub_application_number' => $subApplication->document_number,
                'saf_sub_application_status' => $subApplication->state->currentMl->state_name ?? '',
                'saf_sub_application_start_date' => formattedDate($subApplication->created_at, false, true),
                'saf_sub_application_end_date' => ($subApplication->is_current) ? '-' : $subApplications[$key + 1]->created_at ?? '-',
                'spend_time' => ($subApplication->is_current) ? '-' : "D {$diff->d}, H {$diff->h}, M {$diff->m}, S {$diff->s}"
            ];
        }

        return $returnData;
    }
}