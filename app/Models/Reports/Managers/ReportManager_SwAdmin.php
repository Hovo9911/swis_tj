<?php

namespace App\Models\Reports\Managers;

use App\Models\Agency\Agency;
use App\Models\Broker\Broker;
use App\Models\Company\Company;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\DataModel\DataModel;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\Reports\ReportManager;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationObligations\SingleApplicationObligations;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use App\Models\Transactions\Transactions;
use App\Models\User\User;
use App\Models\UserRoles\UserRoles;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class ReportManager_SwAdmin
 * @package App\Models\Reports\Managers
 */
class ReportManager_SwAdmin extends ReportManager implements ReportManagerInterface
{
    /**
     * Function to get report data
     *
     * @param string $reportCode
     * @param array $inputData
     * @return array
     */
    public function getData(string $reportCode, array $inputData)
    {
        $reportCode = str_replace('-', '_', $reportCode);
        return $this->$reportCode($inputData);
    }

    /**
     * Function to Sw Admin Report 1
     *
     * @param $data
     * @return array
     */
    private function SwAdminReport_1($data)
    {
        $users = User::select('users.id', 'users.ssn', 'users.first_name', 'users.last_name', 'users.username', 'users.passport', 'users.email', 'users.phone_number', 'users.show_status', 'users.registered_at', DB::raw("authorized_users.first_name || ' ' || authorized_users.last_name AS authorized_user"))
            ->leftJoin('users as authorized_users', 'authorized_users.id', '=', 'users.registered_by_id')
            ->whereNotIn('users.ssn', User::SW_ADMIN_SSN)
            ->where('users.registered_at', '>=', $data['start_date_start'])
            ->where('users.registered_at', '<=', $data['start_date_end']);

        if (!Auth::user()->isSwAdmin()) {
            $userIds = UserRoles::where('company_tax_id', Auth::user()->companyTaxId())->activeRole()->pluck('user_id')->all();
            $users->whereIn('users.id', $userIds);
        }

        $users = $users->get();

        return $users;
    }

    /**
     * Function to Sw Admin Report 2
     *
     * @param $data
     * @return array
     */
    private function SwAdminReport_2($data)
    {
        $subApplicationsProducts = $subAppStart = [];

        $requestDateMdmId = DataModel::getMdmFieldIDByConstant(DataModel::REQUEST_DATE_MDM_ID);

        $where = $whereIn = [];
        if (isset($data['applicant_type_value'])) {
            $where['saf.applicant_value'] = $data['applicant_type_value'];
        }

        if (isset($data['request_start_date_start'])) {
            $where[] = ['rejection_date', '>=', $data['request_start_date_start']];
        }

        if (isset($data['request_start_date_end'])) {
            $where[] = ['rejection_date', '<=', $data['request_start_date_end']];
        }

        // Hs Code
        if (isset($data['hs_code'])) {
            $refHsCodeId = $this->refHsDataIds($data['hs_code']);
            $whereIn['product_code'] = $refHsCodeId;
        }

        // Subdivisions
        if (isset($data['sub_division_ids'])) {
            $whereIn['agency_subdivision_id'] = $data['sub_division_ids'];
        }

        // Get SubApplications
        if (isset($data['document_status'])) {
            $subAppStartArr = [];
            foreach ($data['document_status'] as $key => $documentStatus) {
                $subApp = $this->getSubApplicationIds($documentStatus, $data['document_id'][0], $data['regime'], $data['start_date_start'], $data['start_date_end'], $where, $whereIn);

                $subAppStartArr = array_merge($subAppStartArr, $subApp->pluck('id')->all());
            }

            $itemCollect = [];
            foreach ($subAppStartArr as $item) {
                $itemCollect[]['id'] = $item;
            }

            $subAppStart = collect($itemCollect);

        } else {
            $subAppStart = $this->getSubApplicationIds(null, $data['document_id'] ?? [], $data['regime'], $data['start_date_start'], $data['start_date_end'], $where, $whereIn);
        }

        if ($subAppStart->count()) {

            $refSubDivisionsTableNameMl = ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS . '_ml';
            $refClassificatorTableMl = ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS . '_ml';

            $subApplicationsProducts = SingleApplicationSubApplications::select(
                DB::raw("CONCAT(users.first_name,' ',users.last_name) as superscribe_name"),
                'saf_sub_applications.agency_id',
                'saf_sub_applications.document_number',
                'agency_ml.name as agency_name',
                $refSubDivisionsTableNameMl . '.name as subdivision_name',
                $refClassificatorTableMl . '.name as ref_classificator_name',
                'constructor_states_ml.state_name',
                'documents_dataset_from_mdm.field_id as request_date_field_id',
                'agency_subdivision_id',
                'reference_classificator_id',
                'saf.regime',
                'saf.data as saf_data',
                'saf_sub_application_states.state_id',
                'saf_sub_applications.custom_data as sub_application_data',
                'saf_products.product_code',
                'saf_products.commercial_description',
                'saf_sub_applications.id as sub_application_id'
            )
                //
                ->join('agency_ml', function ($query) {
                    $query->on('saf_sub_applications.agency_id', '=', 'agency_ml.agency_id')->where('lng_id', cLng('id'));
                })
                //
                ->join($refSubDivisionsTableNameMl, function ($query) use ($refSubDivisionsTableNameMl) {
                    $query->on('saf_sub_applications.agency_subdivision_id', '=', $refSubDivisionsTableNameMl . '.reference_agency_subdivisions_id')->where($refSubDivisionsTableNameMl . '.lng_id', cLng('id'));
                })
                //
                ->join($refClassificatorTableMl, function ($query) use ($refClassificatorTableMl) {
                    $query->on('saf_sub_applications.reference_classificator_id', '=', $refClassificatorTableMl . '.reference_classificator_of_documents_id')->where($refClassificatorTableMl . '.lng_id', cLng('id'));
                })
                //
                ->join('saf_sub_application_states', function ($q) {
                    $q->on('saf_sub_application_states.saf_subapplication_id', '=', 'saf_sub_applications.id')->where('is_current', '1');
                })
                //
                ->join('constructor_states_ml', function ($join) {
                    $join->on('constructor_states_ml.constructor_states_id', '=', 'saf_sub_application_states.state_id')->where('constructor_states_ml.lng_id', cLng('id'));
                })
                //
                ->join('documents_dataset_from_mdm', function ($join) use ($requestDateMdmId) {
                    $join->on('documents_dataset_from_mdm.document_id', '=', 'saf_sub_applications.constructor_document_id')->where('documents_dataset_from_mdm.mdm_field_id', $requestDateMdmId);
                })
                //
                ->leftJoin('constructor_action_superscribes', function ($join) use ($requestDateMdmId) {
                    $join->on('constructor_action_superscribes.saf_subapplication_id', '=', 'saf_sub_applications.id')->where('constructor_action_superscribes.is_current', '1');
                })
                //
                ->leftJoin('users', function ($join) use ($requestDateMdmId) {
                    $join->on('users.id', '=', 'constructor_action_superscribes.to_user');
                })
                ->join('saf_sub_application_products', 'saf_sub_applications.id', '=', 'saf_sub_application_products.subapplication_id')
                ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
                ->join('saf', 'saf.regular_number', '=', 'saf_sub_applications.saf_number')
                ->whereIn('saf_sub_applications.id', $subAppStart->pluck('id')->all());


            $subApplicationsProducts = $subApplicationsProducts->orderByDesc('saf_sub_applications.id')->get();
        }

        return $subApplicationsProducts;
    }

    /**
     * Function to Sw Admin Report 3
     *
     * @param $data
     * @return array
     */
    private function SwAdminReport_3($data)
    {
        $subApplicationsProducts = $subAppStart = [];

        $requestDateMdmId = DataModel::getMdmFieldIDByConstant(DataModel::REQUEST_DATE_MDM_ID);

        $where = $whereIn = [];
        if (isset($data['applicant_type_value'])) {
            $where['saf.applicant_value'] = $data['applicant_type_value'];
        }

        // reject date
        if (isset($data['request_start_date_start'])) {
            $where[] = ['request_date', '>=', $data['request_start_date_start']];
        }

        if (isset($data['request_start_date_end'])) {
            $where[] = ['request_date', '<=', $data['request_start_date_end']];
        }

        // registration date
        if (isset($data['registration_start_date_start'])) {
            $where[] = ['registration_date', '>=', $data['registration_start_date_start']];
        }

        if (isset($data['registration_start_date_end'])) {
            $where[] = ['registration_date', '<=', $data['registration_start_date_end']];
        }

        //
        if (isset($data['registration_number'])) {
            $where[] = ['registration_number', $data['registration_number']];
        }


        // Hs Code
        if (isset($data['hs_code'])) {
            $refHsCodeId = $this->refHsDataIds($data['hs_code']);
            $whereIn['product_code'] = $refHsCodeId;
        }

        // Subdivisions
        if (isset($data['sub_division_ids'])) {
            $whereIn['agency_subdivision_id'] = $data['sub_division_ids'];
        }

        // Get SubApplications
        if (isset($data['document_status'])) {
            $subAppStartArr = [];
            foreach ($data['document_status'] as $key => $documentStatus) {
                $subApp = $this->getSubApplicationIds($documentStatus, $data['document_id'][0], $data['regime'], $data['start_date_start'], $data['start_date_end'], $where, $whereIn);

                $subAppStartArr = array_merge($subAppStartArr, $subApp->pluck('id')->all());
            }

            $itemCollect = [];
            foreach ($subAppStartArr as $item) {
                $itemCollect[]['id'] = $item;
            }

            $subAppStart = collect($itemCollect);

        } else {
            $subAppStart = $this->getSubApplicationIds(null, $data['document_id'] ?? [], $data['regime'], $data['start_date_start'], $data['start_date_end'], $where, $whereIn);
        }

        if ($subAppStart->count()) {

            $refSubDivisionsTableNameMl = ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS . '_ml';
            $refClassificatorTableMl = ReferenceTable::REFERENCE_CLASSIFICATOR_OF_DOCUMENTS . '_ml';

            $subApplicationsProducts = SingleApplicationSubApplications::select(
                DB::raw("CONCAT(users.first_name,' ',users.last_name) as superscribe_name"),
                'saf_sub_applications.agency_id',
                'saf_sub_applications.document_number',
                'agency_ml.name as agency_name',
                $refSubDivisionsTableNameMl . '.name as subdivision_name',
                $refClassificatorTableMl . '.name as ref_classificator_name',
                'constructor_states_ml.state_name',
                'saf_sub_applications.approve_reject_number',
                'saf_sub_applications.request_date',
                'saf_sub_applications.registration_date',
                'saf_sub_applications.registration_number',
                'saf_sub_applications.rejected_reason',
                'saf_sub_applications.expiration_date',
                DB::raw('COALESCE(saf_sub_applications.rejection_date, saf_sub_applications.permission_date) as rejection_or_permission_date'),
                'saf_sub_applications.permission_date',
                'saf_sub_applications.rejected_number',
                'saf_sub_applications.permission_number',
                'documents_dataset_from_mdm.field_id as request_date_field_id',
                'agency_subdivision_id',
                'reference_classificator_id',
                'saf.regime',
                'saf.data as saf_data',
                'saf_sub_application_states.state_id',
                'saf_sub_applications.id as sub_application_id'
            )
                //
                ->join('agency_ml', function ($query) {
                    $query->on('saf_sub_applications.agency_id', '=', 'agency_ml.agency_id')->where('lng_id', cLng('id'));
                })
                //
                ->join($refSubDivisionsTableNameMl, function ($query) use ($refSubDivisionsTableNameMl) {
                    $query->on('saf_sub_applications.agency_subdivision_id', '=', $refSubDivisionsTableNameMl . '.reference_agency_subdivisions_id')->where($refSubDivisionsTableNameMl . '.lng_id', cLng('id'));
                })
                //
                ->join($refClassificatorTableMl, function ($query) use ($refClassificatorTableMl) {
                    $query->on('saf_sub_applications.reference_classificator_id', '=', $refClassificatorTableMl . '.reference_classificator_of_documents_id')->where($refClassificatorTableMl . '.lng_id', cLng('id'));
                })
                //
                ->join('saf_sub_application_states', function ($q) {
                    $q->on('saf_sub_application_states.saf_subapplication_id', '=', 'saf_sub_applications.id')->where('is_current', '1');
                })
                //
                ->join('constructor_states_ml', function ($join) {
                    $join->on('constructor_states_ml.constructor_states_id', '=', 'saf_sub_application_states.state_id')->where('constructor_states_ml.lng_id', cLng('id'));
                })
                //
                ->join('documents_dataset_from_mdm', function ($join) use ($requestDateMdmId) {
                    $join->on('documents_dataset_from_mdm.document_id', '=', 'saf_sub_applications.constructor_document_id')->where('documents_dataset_from_mdm.mdm_field_id', $requestDateMdmId);
                })
                //
                ->leftJoin('constructor_action_superscribes', function ($join) use ($requestDateMdmId) {
                    $join->on('constructor_action_superscribes.saf_subapplication_id', '=', 'saf_sub_applications.id')->where('constructor_action_superscribes.is_current', '1');
                })
                //
                ->leftJoin('users', function ($join) use ($requestDateMdmId) {
                    $join->on('users.id', '=', 'constructor_action_superscribes.to_user');
                })
                ->join('saf', 'saf.regular_number', '=', 'saf_sub_applications.saf_number')
                ->whereIn('saf_sub_applications.id', $subAppStart->pluck('id')->all());


            $subApplicationsProducts = $subApplicationsProducts->orderByDesc('saf_sub_applications.id')->get();
        }

        return $subApplicationsProducts;
    }

    /**
     * Function to Sw Admin Report 4
     *
     * @param $data
     * @return array
     */
    private function SwAdminReport_4($data)
    {
        $query = SingleApplicationObligations::select(
            'saf_obligations.id',
            'saf_obligations.payer_user_type',
            'saf_obligations.payer_company_tax_id',
            'saf_obligations.payer_user_id',
            'saf.regime',
            'saf.regular_number',
            'saf_obligations.payment_date as obligation_pay_date',
            'saf_obligations.obligation',
            DB::raw("CONCAT('(', payer_user.ssn , ') ', payer_user.first_name, ' ', payer_user.last_name) AS obligation_payer_person")
        )
            ->join('saf_sub_applications', 'saf_sub_applications.id', '=', 'saf_obligations.subapplication_id')
            ->join('saf', 'saf.regular_number', '=', 'saf_obligations.saf_number')
            ->leftJoin('users as creator_user', 'creator_user.id', '=', 'saf.creator_user_id')
            ->leftJoin('users as payer_user', 'payer_user.id', '=', DB::raw("saf_obligations.payer_user_id::integer"))
            ->leftJoin('users', 'users.id', '=', 'saf_obligations.creator_user_id')
            ->join('company', 'company.tax_id', '=', 'saf_obligations.payer_company_tax_id')
            ->join('reference_obligation_budget_line', 'reference_obligation_budget_line.id', '=', 'saf_obligations.ref_budget_line_id')
            ->join('reference_obligation_budget_line_ml', function ($q) {
                $q->on('reference_obligation_budget_line_ml.reference_obligation_budget_line_id', '=', 'reference_obligation_budget_line.id')->where('reference_obligation_budget_line_ml.lng_id', cLng('id'));
            })
            ->join('reference_obligation_type', 'reference_obligation_type.id', '=', 'reference_obligation_budget_line.budget_line')
            ->join('reference_obligation_type_ml', function ($q) {
                $q->on('reference_obligation_type_ml.reference_obligation_type_id', '=', 'reference_obligation_type.id')->where('reference_obligation_type_ml.lng_id', cLng('id'));
            })
            ->where('reference_obligation_type.code', SingleApplicationObligations::SAF_OBLIGATIONS_PAYMENT_FOR_SINGLE_WINDOW_SERVICE_CODE)
            ->whereIn('saf.regime', $data['regime'])
            ->where('saf_obligations.payment_date', '>=', "{$data['start_date_start']} 00:00:00")
            ->where('saf_obligations.payment_date', '<=', "{$data['start_date_end']} 23:59:59");

        if (!Auth::user()->isSwAdmin()) {
            $query->where(function ($q) {
                $q->safOwnerOrCreator()->orWhere('saf_sub_applications.company_tax_id', Auth::user()->companyTaxId());
            });
        }

        // Start search by filters
        if (isset($data['saf_number'])) {
            $query->where('saf.regular_number', 'ILIKE', "%{$data['saf_number']}%");
        }

        if (isset($data['natural_person_payer'])) {
            $query->where(function ($q) use ($data) {
                $q->where('payer_user.ssn', 'ILIKE', "%{$data['natural_person_payer']}%")
                    ->orWhereRaw("concat(payer_user.first_name, ' ', payer_user.last_name) like '%{$data['natural_person_payer']}%' ");
            });
        }

        if (isset($data['company_payer'])) {
            $query->where(function ($q) use ($data) {
                $q->where('company.tax_id', 'ILIKE', "%{$data['company_payer']}%")
                    ->orWhere('company.name', 'ILIKE', "%{$data['company_payer']}%");
            });
        }


        // get
        $safObligations = $query->orderByDesc('saf_obligations.id')->get();

        //
        $allSum = 0;
        foreach ($safObligations as &$safObligation) {

            $safObligation->obligation_company_payer = '';
            if (!is_null($safObligation->payer_user_type)) {
                if ($safObligation->payer_user_type == SingleApplicationObligations::SAF_OBLIGATIONS_TYPE_NATURAL_PERSON) {
                    $safObligation->obligation_company_payer = "(" . $safObligation->payer_company_tax_id . ') ' . User::find($safObligation->payer_user_id)->name();
                } elseif ($safObligation->payer_user_type == SingleApplicationObligations::SAF_OBLIGATIONS_TYPE_AGENCY || $safObligation->payer_user_type == SingleApplicationObligations::SAF_OBLIGATIONS_TYPE_BROKER) {
                    $safObligation->obligation_company_payer = "(" . $safObligation->payer_company_tax_id . ') ' . optional(Company::getCompanyByTaxID($safObligation->payer_company_tax_id))->name;
                } else {
                    $safObligation->obligation_company_payer = "(" . $safObligation->payer_company_tax_id . ') ' . trans("swis.saf_obligations.payer.{$safObligation->payer_user_type}");
                }
            }

            $allSum += $safObligation->obligation;
            $safObligation->obligation_pay_date = formattedDate(date(config('swis.date_time_format'), strtotime($safObligation->obligation_pay_date)), true);
        }

        return [
            'allSum' => (string)$allSum,
            'obligations' => $safObligations
        ];

    }

    /**
     * Function to Sw Admin Report 5
     *
     * @param $data
     * @return array
     */
    private function SwAdminReport_5($data)
    {
        if (Auth::user()->isSwAdmin()) {
            $companyTaxId = $data['agency'];
        } else {
            $companyTaxId = Auth::user()->companyTaxId();
        }

        $documentStatuses = $data['document_status'] ?? [];

        $subApplications = SingleApplicationSubApplications::select('agency_subdivision_id',
            DB::raw("COUNT(saf_sub_application_states.state_id) as state_count"),
            DB::raw("array_to_string(array_agg(saf_sub_application_states.state_id), ',') as state_ids")
        )
            ->join('saf_sub_application_states', function ($q) {
                $q->on('saf_sub_application_states.saf_subapplication_id', '=', 'saf_sub_applications.id')->where('is_current', '1');
            })
            ->where("saf_sub_applications.constructor_document_id", $data['document_id'])
            ->where('saf_sub_applications.company_tax_id', $companyTaxId)
            ->where("saf_sub_applications.status_date", '>=', $data['start_date_start'])
            ->where("saf_sub_applications.status_date", '<=', $data['start_date_end'])
            ->whereNotNull('agency_subdivision_id');

        if (isset($data['regime'])) {
            $subApplications->join('saf', 'saf.regular_number', '=', 'saf_sub_applications.saf_number')->whereIn('saf.regime', $data['regime']);
        }

        if (count($documentStatuses)) {
            $subApplications->whereIn('saf_sub_application_states.state_id', $documentStatuses);
        }

        if (isset($data['sub_division_ids'])) {
            $subApplications->whereIn('agency_subdivision_id', $data['sub_division_ids']);
        }

        $subApplications = $subApplications->groupBy('agency_subdivision_id')->get();

        //
        $constructorStates = ConstructorStates::select('id', 'state_name')->where(['constructor_document_id' => $data['document_id']])
            ->joinMl();

        if ($documentStatuses) {
            $constructorStates->whereIn('id', $documentStatuses);
        }

        $constructorStates = $constructorStates->orderByAsc()->get();
        $refSubdivisionAll = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS);

        //
        $subDivisionsInfo = [];
        foreach ($subApplications as $subApplication) {
            $states = explode(',', $subApplication->state_ids);

            foreach ($states as $state) {

                if (count($documentStatuses) && !in_array($state, $documentStatuses)) {
                    continue;
                }

                if (isset($refSubdivisionAll[$subApplication->agency_subdivision_id])) {

                    $currentRefCode = $refSubdivisionAll[$subApplication->agency_subdivision_id]['code'];

                    if (isset($subDivisionsInfo[$currentRefCode][$state])) {
                        $subDivisionsInfo[$currentRefCode][$state] += 1;
                    } else {
                        $subDivisionsInfo[$currentRefCode][$state] = 1;
                    }
                }
            }
        }

        //
        $constructorDoc = ConstructorDocument::select('id')->where('id', $data['document_id'])->first();

        $agency = Agency::where('tax_id', $companyTaxId)->first();

        return [
            'agency_name' => $agency->current()->name,
            'constructor_doc_name' => $constructorDoc->current()->document_name,
            'sub_division_info' => $subDivisionsInfo,
            'constructor_states' => $constructorStates
        ];
    }

    /**
     * Function to Sw Admin Report 6
     *
     * @param $data
     * @return array
     */
    private function SwAdminReport_6($data)
    {
        $brokerNameOrTin = $data['broker_name_or_tin'];

        $safData = SingleApplication::with(['formedSubApplications'])->select('saf.id', 'regime', 'regular_number', 'saf.data')
            ->join('saf_sub_applications', 'saf_sub_applications.saf_number', '=', 'saf.regular_number')
            ->whereNotNull('saf_sub_applications.current_status')
            ->where("saf_sub_applications.status_date", '>=', $data['start_date_start'])
            ->where("saf_sub_applications.status_date", '<=', $data['start_date_end'])
            ->whereIn('regime', $data['regime'])
            ->where(function ($q) use ($brokerNameOrTin) {
                $q->where('saf.applicant_value', 'ilike', "%{$brokerNameOrTin}%")->orWhere('saf.data->saf->sides->applicant->name', 'ilike', "%{$brokerNameOrTin}%");
            })
            ->orderByDesc('saf.id')
            ->get();

        $result = [];
        $brokersTaxId = [];
        foreach ($safData as $key => $saf) {

            $firstSendSubApplication = $saf->formedSubApplications[0];

            if (!is_null($firstSendSubApplication)) {
                $sendUser = User::select('id', 'first_name', 'last_name')->where('id', $firstSendSubApplication->sender_user_id)->first();
                $safSidesData = $saf->data['saf']['sides'];

                $sideRegimeType = SingleApplication::REGIME_IMPORT;
                if ($saf->regime == SingleApplication::REGIME_EXPORT) {
                    $sideRegimeType = SingleApplication::REGIME_EXPORT;
                } elseif ($saf->regime == SingleApplication::REGIME_TRANSIT) {
                    $sideRegimeType = 'applicant';
                }

                $sideData = '(' . $safSidesData[$sideRegimeType]['type_value'] . ') ' . $safSidesData[$sideRegimeType]['name'];

                $result['data'][] = [
                    's_n' => $key + 1,
                    'saf_regime' => $saf->regime,
                    'saf_number' => $saf->regular_number,
                    'sub_application_status_date' => formattedDate($firstSendSubApplication->status_date),
                    'sub_application_send_user_info' => !is_null($sendUser) ? $sendUser->name() : '',
                    'side_data' => $sideData,
                ];

                $brokersTaxId[] = $safSidesData['applicant']['type_value'];
            }
        }

        if (isset($brokersTaxId)) {
            $brokerNames = Broker::joinMl()->whereIn('tax_id', array_unique($brokersTaxId))->active()->pluck('name')->all();

            $result['saf_brokers_name'] = implode(', ', $brokerNames);
        }

        return $result;
    }

    /**
     * Function to Sw Admin Report 7
     *
     * @param $data
     * @return array
     */
    private function SwAdminReport_7($data)
    {
        $fixCodeForSendApplications = SingleApplicationObligations::SAF_OBLIGATIONS_FIX_CODE_FOR_SEND_APPLICATION_OBLIGATION;
        $regimeCondition = '';
        if (isset($data['regime'])) {
            $regimes = "'" . implode("','", $data['regime']) . "'";
            $regimeCondition = ' and regime in (' . $regimes . ')';
        }

        $applicantCondition = '';
        if (isset($data['applicant'])) {
            $applicantCondition = " and (saf.data->'saf'->'sides'->'import'->>'name' ilike '" . $data['applicant'] . "' or saf.applicant_value ilike '" . $data['applicant'] . "')";
        }

        $transactions = Transactions::select([
            DB::raw("SUM(amount) as transaction_amount"),
            DB::raw("company.name as company_name"),
            'transactions.company_tax_id',
        ])
            ->selectRaw("(
                SELECT COUNT(company_tax_id) from saf
                    where transactions.company_tax_id = saf.company_tax_id $regimeCondition $applicantCondition
                    GROUP BY company_tax_id LIMIT 1
            ) as saf_count")
            ->selectRaw("(
                SELECT SUM(saf_obligations.payment) from saf_obligations
                    join saf on saf.regular_number = saf_obligations.saf_number
                    where transactions.company_tax_id = saf_obligations.company_tax_id $regimeCondition 
                    and saf_obligations.obligation_code = '$fixCodeForSendApplications' $applicantCondition
                    GROUP BY saf_obligations.company_tax_id LIMIT 1
            ) as obligations_sum")
            ->join('company', 'company.tax_id', '=', 'transactions.company_tax_id')
            ->where('transactions.type', Transactions::PAYMENT_TYPE_INPUT)
            ->where("transaction_date", '>=', Carbon::parse($data['start_date_start'])->startOfDay())
            ->where("transaction_date", '<=', Carbon::parse($data['start_date_end'])->endOfDay())
            ->groupBy('transactions.company_tax_id', 'company.name')
            ->get();

        return [
            'data' => $transactions
        ];

    }
}
