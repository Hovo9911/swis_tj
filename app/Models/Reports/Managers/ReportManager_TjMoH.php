<?php

namespace App\Models\Reports\Managers;

use App\Models\ConstructorStates\ConstructorStates;
use App\Models\DataModel\DataModel;
use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\Reports\ReportManager;
use App\Models\Reports\Reports;
use App\Models\SingleApplicationProducts\SingleApplicationProducts;
use App\Models\SingleApplicationSubApplicationProducts\SingleApplicationSubApplicationProducts;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

//Хадамоти назорати давлатии тандурустӣ ва ҳифзи иҷтимоии аҳолӣ agency tax_id = 020004117 reports part

/**
 * Class ReportManager_TjMoH
 * @package App\Models\Reports\Managers
 */
class ReportManager_TjMoH extends ReportManager implements ReportManagerInterface
{
    /**
     * Function to get report data
     *
     * @param string $reportCode
     * @param array $inputData
     * @return array
     */
    public function getData(string $reportCode, array $inputData)
    {
        return $this->$reportCode($inputData);
    }

    /**
     * Function to generate TjMoH Report 1
     *
     * @param $data
     * @return array
     */
    private function TjMoH_1($data)
    {
        $hs_code_count = $data['hs_code_count'];

        $hsCodeReferenceTableName = ReferenceTable::REFERENCE_HS_CODE_6;

        // Get SubApplications
        $subAppStart = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['start_date_start'], $data['start_date_end']);
        $subAppEnd = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['end_date_start'], $data['end_date_end']);

        // Get SubApplication Products IDS
        $subApplicationsStartProductsIds = SingleApplicationSubApplicationProducts::whereIn('subapplication_id', $subAppStart->pluck('id')->all())->pluck('product_id');
        $subApplicationsEndProductsIds = SingleApplicationSubApplicationProducts::whereIn('subapplication_id', $subAppEnd->pluck('id')->all())->pluck('product_id');

        // By Product ids get product HS codes
        $startHsCodes = SingleApplicationProducts::whereIn('saf_products.id', $subApplicationsStartProductsIds)
            ->leftJoin($hsCodeReferenceTableName, 'saf_products.product_code', '=', DB::raw("{$hsCodeReferenceTableName}.id::varchar"))
            ->whereNotNull('saf_products.product_code')
            ->whereNotNull($hsCodeReferenceTableName . '.code')
            ->pluck($hsCodeReferenceTableName . '.code')
            ->unique()
            ->all();


        $endHsCodes = SingleApplicationProducts::whereIn('saf_products.id', $subApplicationsEndProductsIds)
            ->leftJoin($hsCodeReferenceTableName, 'saf_products.product_code', '=', DB::raw("{$hsCodeReferenceTableName}.id::varchar"))
            ->whereNotNull('saf_products.product_code')
            ->whereNotNull($hsCodeReferenceTableName . '.code')
            ->pluck($hsCodeReferenceTableName . '.code')
            ->unique()
            ->all();
        // All subApplication products HS CODES
        $allHsCodes = array_unique(array_merge($startHsCodes, $endHsCodes));

        $filteredHsCodes = [];
        foreach ($allHsCodes as $hsCode) {
            $filteredHsCodes[] = substr($hsCode, 0, $hs_code_count);
        }
        $filteredHsCodes = array_unique($filteredHsCodes);

        // Get Start date all report data
        $subAppStartReportData = $this->getSubApplicationAllData($subAppStart->pluck('id'), $filteredHsCodes);
        // Get End date all report data
        $subAppEndReportData = $this->getSubApplicationAllData($subAppEnd->pluck('id'), $filteredHsCodes);

        $hsCodeReferenceTableName = ReferenceTable::REFERENCE_HS_CODE_6;
        $hsCodeReferenceMlTableName = $hsCodeReferenceTableName . '_ml';

        $sumAllReportData = [];
        foreach ($filteredHsCodes as $key => $hsCode) {
            $hsCodeName = substr($hsCode, 0, $hs_code_count);

            $refRow = DB::table($hsCodeReferenceTableName)
                ->join($hsCodeReferenceMlTableName, $hsCodeReferenceTableName . '.id', '=', $hsCodeReferenceMlTableName . '.' . $hsCodeReferenceTableName . '_id')
                ->where('code', $hsCode)->first();

            if (!is_null($refRow)) {
                $hsCodeName = optional($refRow)->path_name . ' (' . $hsCode . ')';
            }

            $dataCheck = null;

            if (isset($subAppStartReportData[$hsCode])) {
                $dataCheck = $subAppStartReportData[$hsCode];
            } elseif (isset($subAppEndReportData[$hsCode])) {
                $dataCheck = $subAppEndReportData[$hsCode];
            }

            if (!is_null($dataCheck)) {

                foreach ($dataCheck as $type => $val) {

                    $sumAllReportData[$hsCode]['types'][$type] = [
                        'approvedQty_A' => isset($subAppStartReportData[$hsCode][$type]) ? $subAppStartReportData[$hsCode][$type]['approvedQty'] : 0,
                        'approvedQty_B' => isset($subAppEndReportData[$hsCode][$type]) ? $subAppEndReportData[$hsCode][$type]['approvedQty'] : 0,
                    ];

                    $sumAllReportData[$hsCode]['hsCodeName'] = $hsCodeName;

                }
            }
        }

        return $sumAllReportData;
    }

    /**
     * Function to generate TjMoh Report 2
     *
     * @param $data
     * @return array
     */
    private function TjMoH_2($data)
    {
        $hsCode = $data['hs_code'];
        $refHsCodeId = $this->refHsDataIds($hsCode);

        $allReportData = [];

        if (count($refHsCodeId)) {

            // Get SubApplications
            $subAppStart = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['start_date_start'], $data['start_date_end']);
            $subAppEnd = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['end_date_start'], $data['end_date_end']);

            // Get SubApplication producer countries with group by approved data
            $subApplicationsStartCountriesApprovedQty = SingleApplicationSubApplicationProducts::selectRaw('SUM(saf_products_batch.approved_quantity::numeric) as approved_quantity , producer_country,saf_products_batch.measurement_unit')
                ->whereIn('subapplication_id', $subAppStart->pluck('id')->all())
                ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
                ->join('saf_products_batch', 'saf_products.id', '=', 'saf_products_batch.saf_product_id')
//                ->where('saf_products.product_code', 'ILIKE', $hsCode . '%')
                ->whereIn('saf_products.product_code', $refHsCodeId)
                ->groupBy('producer_country', 'saf_products_batch.measurement_unit')
                ->orderBy('producer_country', 'asc')
                ->orderBy('approved_quantity', 'desc')
                ->get();

            $subApplicationsEndCountriesApprovedQty = SingleApplicationSubApplicationProducts::selectRaw('SUM(saf_products_batch.approved_quantity::numeric) as approved_quantity , producer_country,saf_products_batch.measurement_unit')
                ->whereIn('subapplication_id', $subAppEnd->pluck('id')->all())
                ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
                ->join('saf_products_batch', 'saf_products.id', '=', 'saf_products_batch.saf_product_id')
//                ->where('saf_products.product_code', 'ILIKE', $hsCode . '%')
                ->whereIn('saf_products.product_code', $refHsCodeId)
                ->groupBy('producer_country', 'saf_products_batch.measurement_unit')
                ->orderBy('producer_country', 'asc')
                ->orderBy('approved_quantity', 'desc')
                ->get();

            $startCountriesData = [];
            foreach ($subApplicationsStartCountriesApprovedQty as $value) {
                $startCountriesData[$value->producer_country][$value->measurement_unit] = $value->approved_quantity;
            }

            $endCountriesData = [];
            foreach ($subApplicationsEndCountriesApprovedQty as $value) {
                $endCountriesData[$value->producer_country][$value->measurement_unit] = $value->approved_quantity;
            }

            $countriesReferenceTableName = ReferenceTable::REFERENCE_COUNTRIES;
            $countriesReferenceMlTableName = ReferenceTable::REFERENCE_COUNTRIES . '_ml';

            $countries = DB::table($countriesReferenceTableName)
                ->join($countriesReferenceMlTableName, $countriesReferenceTableName . '.id', '=', $countriesReferenceMlTableName . '.' . $countriesReferenceTableName . '_id')
                ->where($countriesReferenceMlTableName . '.lng_id', '=', cLng('id'))
                ->get();

            foreach ($countries as $key => $country) {
                $dataCheck = null;

                if (isset($startCountriesData[$country->id])) {
                    $dataCheck = $startCountriesData[$country->id];
                } elseif (isset($endCountriesData[$country->id])) {
                    $dataCheck = $endCountriesData[$country->id];
                }

                if (!is_null($dataCheck)) {
                    foreach ($dataCheck as $type => $val) {
                        $allReportData[$key]['types'][$type]['approvedQty_A'] = (isset($startCountriesData[$country->id]) && isset($startCountriesData[$country->id][$type])) ? $startCountriesData[$country->id][$type] : 0;
                        $allReportData[$key]['types'][$type]['approvedQty_B'] = (isset($endCountriesData[$country->id]) && isset($endCountriesData[$country->id][$type])) ? $endCountriesData[$country->id][$type] : 0;
                    }

                    $allReportData[$key]['country_name'] = $country->name;
                }

            }
        }

        return $allReportData;
    }

    /**
     * Function to generate TjMoH Report 3
     *
     * @param $data
     * @return array
     */
    private function TjMoH_3($data)
    {
        $months = array();
        for ($x = 1; $x <= 12; $x++) {
            $months[date('m', mktime(0, 0, 0, $x, 1))] = date('F', mktime(0, 0, 0, $x, 1));
        }

        $byYear = $data['year'];
        $documentStatus = $data['document_status'];
        $documentId = $data['document_id'];
        $inputSubDivisions = $data['sub_division_ids'] ?? $this->getAllSubdivisions();

        $subDivisions = $inputSubDivisions;

        if (!count($subDivisions)) {

            $allSubDivisions = getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS);

            $subDivisions = $allSubDivisions->pluck('id')->all();

        }

        $documentDatasetMdm = DocumentsDatasetFromMdm::select('id', 'field_id')->where('document_id', $documentId)->where('mdm_field_id', 355)->first();

        $mdmFieldId = !is_null($documentDatasetMdm) ? $documentDatasetMdm->field_id : null;

        $startDate = $byYear . '-01-01';
        $endDate = $byYear . '-12-31';

        $subApplications = SingleApplicationSubApplications::select([
            'saf_sub_applications.id',
            'saf_sub_applications.agency_subdivision_id',
            'saf_sub_applications.custom_data',
            DB::raw('(SELECT SUM(saf_obligations.payment) from saf_obligations where saf_obligations.subapplication_id = saf_sub_applications.id ) as certificates_sum')
        ])
            ->join('saf', 'saf.regular_number', '=', 'saf_sub_applications.saf_number')
            ->join('saf_sub_application_states', 'saf_sub_application_states.saf_subapplication_id', '=', 'saf_sub_applications.id')
            ->where('saf_sub_applications.company_tax_id', '=', Auth::user()->companyTaxId())
            ->where('is_current', '1')
            ->whereNotNull('agency_subdivision_id')
            ->where('saf_sub_application_states.state_id', $documentStatus)
            ->where("saf_sub_applications.custom_data->{$mdmFieldId}", '>=', $startDate)
            ->where("saf_sub_applications.custom_data->{$mdmFieldId}", '<=', $endDate);


        if ($data['regime'] != 'together') {
            $subApplications->where('saf.regime', $data['regime']);
        }

        $subApplications->whereIn('agency_subdivision_id', $subDivisions);

        $subApplications = $subApplications->get();

        $subApplicationsAllMonth = [];

        foreach ($subApplications as $subApplication) {
            $approvedDate = $subApplication->custom_data[$mdmFieldId];

            $approvedMonth = date('m', strtotime($approvedDate));

            $monthCertificatesQuantity = isset($subApplicationsAllMonth[$subApplication->agency_subdivision_id][$approvedMonth]) ? $subApplicationsAllMonth[$subApplication->agency_subdivision_id][$approvedMonth]['certificates_quantity'] : 0;
            $monthCertificatesSum = isset($subApplicationsAllMonth[$subApplication->agency_subdivision_id][$approvedMonth]) ? $subApplicationsAllMonth[$subApplication->agency_subdivision_id][$approvedMonth]['certificates_sum'] : 0;

            $currentMonthSum = $monthCertificatesSum + $subApplication['certificates_sum'];
            $currentMonthQuantity = $monthCertificatesQuantity + 1;

            $subApplicationsAllMonth[$subApplication->agency_subdivision_id][$approvedMonth] = [
                'certificates_quantity' => $currentMonthQuantity,
                'certificates_sum' => $currentMonthSum
            ];

            $subApplicationsAllMonth[$subApplication->agency_subdivision_id]['certificates_all_quantity'] = isset($subApplicationsAllMonth[$subApplication->agency_subdivision_id]['certificates_all_quantity']) ? $subApplicationsAllMonth[$subApplication->agency_subdivision_id]['certificates_all_quantity'] + 1 : 1;
            $subApplicationsAllMonth[$subApplication->agency_subdivision_id]['certificates_all_sum'] = isset($subApplicationsAllMonth[$subApplication->agency_subdivision_id]['certificates_all_sum']) ? $subApplicationsAllMonth[$subApplication->agency_subdivision_id]['certificates_all_sum'] + $subApplication['certificates_sum'] : $subApplication['certificates_sum'];
        }

        foreach ($subDivisions as $subDivisionId) {

            if (!isset($subApplicationsAllMonth[$subDivisionId])) {
                $subApplicationsAllMonth[$subDivisionId] = [
                    'certificates_quantity' => '-',
                    'certificates_sum' => '-',
                    'certificates_all_quantity' => 0,
                    'certificates_all_sum' => 0,
                ];
            }
        }

        return $subApplicationsAllMonth;
    }

    /**
     * Function to generate TjMoH Report 4
     *
     * @param $data
     * @return array
     */
    private function TjMoH_4($data)
    {
        $hsCode = $data['hs_code'];

        $refHsCodeId = $this->refHsDataIds($hsCode);

        $allReportData = [];

        $dataModel = new DataModel();

        if (count($refHsCodeId)) {

            // Get SubApplications
            $subApplications = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['start_date_start'], $data['start_date_end']);

            $country = ReferenceTable::REFERENCE_COUNTRIES;
            $countryMl = ReferenceTable::REFERENCE_COUNTRIES . "_ml";

            $subApplicationsData = SingleApplicationSubApplicationProducts::selectRaw(
                "product_code,
                saf_products_batch.id,
                saf_products_batch.batch_number,
                saf_products_batch.production_date,
                saf_products_batch.expiration_date,
                saf_products_batch.approved_type,             
                saf_products.producer_name,
                saf_sub_application_products.saf_number,
                saf.data,
                saf_sub_applications.custom_data,
                {$countryMl}.name as producer_country")
                ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
                ->join('saf_products_batch', 'saf_products.id', '=', 'saf_products_batch.saf_product_id')
                ->leftJoin('saf', 'saf.regular_number', '=', 'saf_sub_application_products.saf_number')
                ->leftJoin('saf_sub_applications', 'saf_sub_applications.id', '=', 'saf_sub_application_products.subapplication_id')
                ->join($country, DB::raw("{$country}.id::varchar"), '=', 'saf_products.producer_country')
                ->join($countryMl, function ($join) use ($country, $countryMl) {
                    $join->on("{$countryMl}.{$country}_id", '=', "{$country}.id")->where("{$countryMl}.lng_id", cLng('id'));
                })
                ->whereIn('subapplication_id', $subApplications->pluck('id')->all())
                ->whereIn('saf_products.product_code', $refHsCodeId)
                ->orderBy('product_code', 'asc')
                ->get();


            if (count($subApplicationsData) > 0) {
                foreach ($subApplicationsData as $subApplication) {

                    $safInfo = json_decode($subApplication['data']);

                    $subApplicationCustomData = (array)(json_decode($subApplication['custom_data']));

                    $productCode = optional(getReferenceRows(ReferenceTable::REFERENCE_HS_CODE_6, $subApplication['product_code'], false, ['code']))->code;

                    $transportType = $safInfo->saf->transportation->type_of_transport ?? '';

                    if ($transportType) {
                        $transportType = optional(getReferenceRows(ReferenceTable::REFERENCE_TRANSPORT_TYPE, $transportType))->name;
                    }

                    $approvedType = optional(getReferenceRows(ReferenceTable::REFERENCE_TYPE_OF_PERMISSION, $subApplication['approved_type']))->name;

                    $nettoWeightDatasetMdm = DocumentsDatasetFromMdm::select('field_id')->where('document_id', $data['document_id'])->where('mdm_field_id', $dataModel->getMdmFieldIDByConstant(DataModel::TOTAL_NETTO_WEIGHT_MDM_ID))->first();

                    $permissionNumberDatasetMdm = DocumentsDatasetFromMdm::select('field_id')->where('document_id', $data['document_id'])->where('mdm_field_id', $dataModel->getMdmFieldIDByConstant(DataModel::PERMISSION_NUMBER_MDM_ID))->first();

                    $nettoWeight = (!is_null($nettoWeightDatasetMdm)) ? $subApplication['id'] . '-' . $nettoWeightDatasetMdm->field_id : '';
                    $permissionNumber = (!is_null($permissionNumberDatasetMdm)) ? $permissionNumberDatasetMdm->field_id : '';

                    $nettoWeight = ($nettoWeight && array_key_exists($nettoWeight, $subApplicationCustomData)) ? $subApplicationCustomData[$nettoWeight] : '-';
                    $permissionNumber = ($permissionNumber && array_key_exists($permissionNumber, $subApplicationCustomData)) ? $subApplicationCustomData[$permissionNumber] : '-';


                    $allReportData[] = [
                        'product_code' => $productCode,
                        'product_batch_number' => $subApplication['batch_number'],
                        'product_producer_country' => $subApplication['producer_country'],
                        'product_producer_name' => $subApplication['producer_name'],
                        'product_netto_weight' => $nettoWeight,
                        'product_batch_production_date' => formattedDate($subApplication['production_date'], false, true),
                        'product_batch_expiration_date' => formatteddate($subApplication['expiration_date'], false, true),
                        'product_transport_type' => $transportType,
                        'product_permission_number' => $permissionNumber,
                        'product_batch_approved_type' => $approvedType
                    ];
                }
            }

        }

        return $allReportData;
    }

    /**
     * Function to generate TjMoH Report 5
     *
     * @param $data
     * @return array
     */
    private function TjMoH_5($data)
    {
        $documentStatus = $data['document_status'];

        $startDate = $data['start_date_start'];
        $startDateEnd = $data['start_date_end'];

        $agencyActiveSubdivisions[] = isset($data['sub_division_ids']) ? $data['sub_division_ids'] : null;

        $hsCode = $data['hs_code'] ?? '';

        $refHsCodeId = ($hsCode) ? $this->refHsDataIds($hsCode) : null;

        $subApplicationsFirstPeriod = $this->getSubApplicationsWithObligations($documentStatus, $data['document_id'], $startDate, $startDateEnd, $agencyActiveSubdivisions, $refHsCodeId);

        $reportsFirstPeriod = [];
        foreach ($subApplicationsFirstPeriod as $subApplication) {
            $reportsFirstPeriod[$subApplication->agency_subdivision_id][$subApplication->regime] = [
                'certificates_sum' => $subApplication->certificates_sum,
                'certificates_count' => $subApplication->certificates_count,
            ];
        }

        $subApplicationsFirstPeriod->pluck('agency_subdivision_id')->all();

        return [
            'reportData' => $reportsFirstPeriod,
            'allActiveSubDivisions' => $agencyActiveSubdivisions
        ];
    }

    /**
     * Function to generate TjMoH Report 7
     *
     * @param $data
     * @return array
     */
    private function TjMoH_7($data)
    {
        $subApplications = SingleApplicationSubApplications::select('saf.regular_number', 'saf_sub_applications.custom_data as custom_data', 'saf_sub_applications.status_date', 'saf_sub_applications.permission_number', 'saf.data as saf_data',
            DB::raw("array_to_string(array_agg(saf_products.producer_country), ',') as producer_countries")
        )
            ->join('saf', 'saf.regular_number', '=', 'saf_sub_applications.saf_number')
            ->join('saf_sub_application_products', 'saf_sub_application_products.subapplication_id', '=', 'saf_sub_applications.id')
            ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
            ->where("saf_sub_applications.constructor_document_id", Reports::TJ_MOH_DOCUMENT_ID)
            ->where('saf_sub_applications.company_tax_id', Auth::user()->companyTaxId())
            ->where("saf_sub_applications.permission_date", '>=', $data['start_date_start'])
            ->where("saf_sub_applications.permission_date", '<=', $data['start_date_end']);

        if (isset($data['importer'])) {
            $subApplications->where(function ($q) use ($data) {
                $q->where('saf.data->saf->sides->import->name', 'ilike', "%{$data['importer']}%")
                    ->orWhere('saf.importer_value', 'ilike', "%{$data['importer']}%");
            });
        }

        if (isset($data['exporter'])) {

            $subApplications->where(function ($q) use ($data) {
                $q->where('saf.data->saf->sides->export->name', 'ilike', "%{$data['exporter']}%")
                    ->orWhere('saf.exporter_value', 'ilike', "%{$data['exporter']}%");
            });
        }

        if (isset($data['permission_number'])) {
            $subApplications->where('saf_sub_applications.permission_number', 'ILIKE', '%' . $data['permission_number'] . '%');
        }

        if (isset($data['regime'])) {
            $subApplications->whereIn('saf.regime', $data['regime']);
        }

        $subApplications = $subApplications->orderByDesc('saf_sub_applications.id')->groupBy('saf.id', 'saf_sub_applications.id')->get();
        $refCountries = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_COUNTRIES);

        $allReportData = [];
        if ($subApplications->count()) {
            foreach ($subApplications as $key => $subApplication) {

                $safData = json_decode($subApplication->saf_data)->saf;
                $customData = getFieldsValueByMDMID(Reports::TJ_MOH_DOCUMENT_ID, $subApplication->custom_data);

                $importerTypeValue = $safData->sides->import->type_value ? '(' . $safData->sides->import->type_value . ') ' : '';
                $exporterTypeValue = $safData->sides->export->type_value ? '(' . $safData->sides->export->type_value . ') ' : '';

                $producerCountries = explode(',', $subApplication->producer_countries);

                $producerCountriesNames = [];
                foreach ($producerCountries as $producerCountry) {
                    if (isset($refCountries[$producerCountry]) && !isset($producerCountriesNames[$producerCountry])) {
                        $producerCountriesNames[$producerCountry] = $refCountries[$producerCountry]['name'];
                    }
                }

                $currency = $customData['288'] ? '(' . $customData['288'] . ') ' : '';

                $allReportData[] = [
                    's_n' => $key + 1,
                    'submitting_date' => formattedDate($subApplication->status_date),
                    'importer' => $importerTypeValue . $safData->sides->import->name,
                    'exporter' => $exporterTypeValue . $safData->sides->export->name,
                    'producer_countries' => implode(", ", $producerCountriesNames),
                    'purpose_of_import' => $customData['484'] ?? '',
                    'type_of_medicines' => $customData['483'] ?? '',
                    'brutto_weight' => $safData->product->brutto_weight,
                    'total' => $currency . $subApplication->custom_data['6141_21'] ?? '',
                    'approved_date' => $customData['355'] ?? '',
                    'permission_number' => $customData['365'] ?? '',
                ];
            }
        }

        return $allReportData;
    }

    /**
     * Function to generate TjMoH Report 8
     *
     * @param $data
     * @return array
     */
    private function TJMoH_8($data)
    {
        $where = function ($query) use ($data) {
            $query->when($data['saf_number'], function ($query) use ($data) {
                $query->where('saf.regular_number', 'ilike', "%{$data['saf_number']}%");
            });

            $query->when($data['import_target'], function ($query) use ($data) {
                $query->where('saf_sub_applications.custom_data->6141_14', $data['import_target']);
            });

            $query->when($data['medicine_type'], function ($query) use ($data) {
                $query->where('saf_sub_applications.custom_data->6141_13', $data['medicine_type']);
            });
        };

        $endApprovedStates = ConstructorStates::getEndApprovedStateIds(Reports::TJ_MOH_DOCUMENT_ID);

        $subApplications = $this->getSubApplicationIds($endApprovedStates, Reports::TJ_MOH_DOCUMENT_ID, null, $data['start_date_start'], $data['start_date_end'], $where,[],false);
        $subApplications->with('productList');

        $subApplications = $subApplications->get();

        $data = [];
        foreach ($subApplications as $subApplication) {
            $customData = $subApplication->custom_data;

            $importTarget = getReferenceRows(ReferenceTable::REFERENCE_TARGET_OF_IMPORT, $customData['6141_14'] ?? 0);
            $medicineType = getReferenceRows(ReferenceTable::REFERENCE_TYPE_OF_MEDICINE, $customData['6141_13'] ?? 0);

            $data[] = [
                'saf_number' => $subApplication->saf_number,
                'import_target' => $importTarget ? "{$importTarget->code} " . $importTarget->name : '',
                'medicine_type' => $medicineType ? "{$medicineType->code} " . $medicineType->name : '',
                'total_amount' => is_numeric($customData['6141_26']) ? $customData['6141_26'] : 0,
                'products_amount' =>  is_numeric($customData['6141_24']) ? $customData['6141_24'] : 0,
                'currency' => $customData['6141_21'],
            ];
        }

        return $data;
    }
}