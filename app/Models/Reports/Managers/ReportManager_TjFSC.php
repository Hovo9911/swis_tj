<?php

namespace App\Models\Reports\Managers;

use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\DataModel\DataModel;
use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\Reports\ReportManager;
use App\Models\Reports\Reports;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationProducts\SingleApplicationProducts;
use App\Models\SingleApplicationSubApplicationProducts\SingleApplicationSubApplicationProducts;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

//КУМИТАИ БЕХАТАРИИ ОЗУКАВОРИИ НАЗДИ ХУКУМАТИ ЧУМХУРИИ ТОЧИКИСТОН agency tax_id = 020049945 reports part

/**
 * Class ReportManager_TjFSC
 * @package App\Models\Reports\Managers
 */
class ReportManager_TjFSC extends ReportManager implements ReportManagerInterface
{
    /**
     * Function to get report data
     *
     * @param string $reportCode
     * @param array $inputData
     * @return array
     */
    public function getData(string $reportCode, array $inputData)
    {
        return $this->$reportCode($inputData);
    }

    /**
     * Function to generate TjFSC Report 1
     *
     * @param $data
     * @return array
     */
    private function TjFSC_1($data)
    {
        $hsCode = $data['hs_code'] ?? null;

        $refHsCodeIds = [];
        if (isset($hsCode)) {
            $refHsCodeIds = $this->refHsDataIds($hsCode);
        }

        $allReportData = [];

        $dataModel = new DataModel();

        // Get SubApplications
        $subApplications = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['start_date_start'], $data['start_date_end']);

        foreach ($subApplications->all() as $subApplication) {

            $subApplicationsProducts = SingleApplicationSubApplicationProducts::select('product_code', 'commercial_description', 'saf_products.saf_number')
                ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
                ->join('saf_products_batch', 'saf_products.id', '=', 'saf_products_batch.saf_product_id')
                ->where('saf_sub_application_products.subapplication_id', $subApplication->id);

            if ($hsCode) {
                $subApplicationsProducts->whereIn('saf_products.product_code', $refHsCodeIds);
            }

            $subApplicationsProducts = $subApplicationsProducts->groupBy('product_code', 'commercial_description', 'saf_products.saf_number')->get()->toArray();

            if (count($subApplicationsProducts) > 0) {
                foreach ($subApplicationsProducts as $subApplicationsProduct) {

                    $saf = SingleApplication::where('regular_number', $subApplicationsProduct['saf_number'])->where('regime', $data['regime'])->pluck('data')->unique()->first();

                    if ($data['regime'] == SingleApplication::REGIME_IMPORT) {

                        $safTransportationCountry = $saf['saf']['transportation']['export_country'] ?? '';
                        $safSideInfo = $saf['saf']['sides']['import']['name'] ?? '';

                    } elseif ($data['regime'] == SingleApplication::REGIME_EXPORT) {

                        $safTransportationCountry = $saf['saf']['transportation']['import_country'] ?? '';
                        $safSideInfo = $saf['saf']['sides']['export']['name'] ?? '';

                    } else {
                        $safTransportationCountry = '';
                        $safSideInfo = '';
                    }

                    $constructorStateInfo = ConstructorStates::where('id', $data['document_status'])->first() ?? '';

                    if (($constructorStateInfo->state_type == ConstructorStates::END_STATE_TYPE and $constructorStateInfo->state_approved_status == ConstructorStates::END_STATE_NOT_APPROVED) or $constructorStateInfo->state_type == ConstructorStates::CANCELED_STATE_TYPE) {

                        //Rejection date
                        $documentDatasetMdm = DocumentsDatasetFromMdm::select('id', 'field_id')->where('document_id', $data['document_id'])->where('mdm_field_id', $dataModel->getMdmFieldIDByConstant(DataModel::REJECTION_DATE_MDM_ID))->first();
                        $expDateDatasetMdm = '-';

                    } else {

                        //Permission date
                        $documentDatasetMdm = DocumentsDatasetFromMdm::select('id', 'field_id')->where('document_id', $data['document_id'])->where('mdm_field_id', $dataModel->getMdmFieldIDByConstant(DataModel::PERMISSION_DATE_MDM_ID))->first();
                        $expDateDatasetMdm = DocumentsDatasetFromMdm::select('id', 'field_id')->where('document_id', $data['document_id'])->where('mdm_field_id', $dataModel->getMdmFieldIDByConstant(DataModel::EXPIRATION_DATE_MDM_ID))->first();
                    }


                    $mdmDateId = !is_null($documentDatasetMdm) ? $documentDatasetMdm->field_id : null;
                    $mdmExpDateId = !is_null($expDateDatasetMdm) ? ($expDateDatasetMdm != '-') ? $expDateDatasetMdm->field_id : '-' : '';

                    $subApplicationPermissionOrRejectionDate = (!is_null($mdmDateId) && array_key_exists($mdmDateId, $subApplication->custom_data ?? [])) ? $subApplication->custom_data[$mdmDateId] : '-';
                    $subApplicationExpirationDate = ($mdmExpDateId != '-' && array_key_exists($mdmExpDateId, $subApplication->custom_data ?? [])) ? $subApplication->custom_data[$mdmExpDateId] : '-';

                    if (!empty($safTransportationCountry)) {
                        $safTransportationCountry = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safTransportationCountry))->name;
                    }

                    $products = [];

                    if (!empty($subApplicationsProducts)) {
                        foreach ($subApplicationsProducts as $subProducts) {
                            $products[] = $subProducts['commercial_description'];
                        }
                    }

                    $allReportData[$subApplication->document_number] = [
                        'permission_date' => formattedDate($subApplicationPermissionOrRejectionDate, false, true),
                        'expiration_date' => formattedDate($subApplicationExpirationDate, false, true),
                        'transportation_country' => $safTransportationCountry,
                        'saf_side_name' => $safSideInfo,
                        'products' => implode(',', $products),
//                            'sub_app_id' => $subApplication->document_number
                    ];
                }
            }

        }

        return $allReportData;
    }

    /**
     * Function to generate FSC-Phyto
     *
     * @param $data
     * @return SingleApplicationSubApplications
     */
    private function TjFSC_2($data)
    {
        $subApplications = SingleApplicationSubApplications::select('saf_sub_applications.id', 'permission_date', 'permission_number', 'expiration_date',
            DB::raw("regexp_replace(COALESCE(saf.data)::text, '\\u0000', '', 'g')::json as saf_data"),
            DB::raw("array_to_string(array_agg(saf_products.commercial_description), ', ') as product_names")
        )
            ->join('saf', 'saf.regular_number', '=', 'saf_sub_applications.saf_number')
            ->leftJoin('saf_sub_application_products', 'saf_sub_application_products.subapplication_id', '=', 'saf_sub_applications.id')
            ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
            //            Импортное карантинное разрешение -> ID-38 , code-6033
            ->where("saf_sub_applications.document_code", Reports::IMPORT_QUARANTINE_PERMISSION_CODE)
            ->where("saf_sub_applications.permission_date", '>=', $data['start_date_start'])
            ->where("saf_sub_applications.permission_date", '<=', $data['start_date_end'])
            ->where('saf_sub_applications.company_tax_id', Auth::user()->companyTaxId());

        if (isset($data['expiration_date_start'])) {
            $subApplications->where("saf_sub_applications.expiration_date", '>=', $data['expiration_date_start']);
        }

        if (isset($data['expiration_date_end'])) {
            $subApplications->where("saf_sub_applications.expiration_date", '<=', $data['expiration_date_end']);
        }

        if (isset($data['export_country'])) {
            $subApplications->where('saf.data->saf->transportation->export_country', $data['export_country']);
        }

        if (isset($data['import_name'])) {
            $subApplications->where(function ($q) use ($data) {
                $q->where('saf.data->saf->sides->import->name', 'ilike', "%{$data['import_name']}%")
                    ->orWhere('saf.importer_value', 'ilike', "%{$data['import_name']}%");
            });
        }

        $subApplications->groupBy('saf_sub_applications.id', 'saf.id');

        $refCountries = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_COUNTRIES);

        $subApplications = $subApplications->orderByAsc('saf_sub_applications.id')->get();

        foreach ($subApplications as &$subApplication) {
            $safData = json_decode($subApplication->saf_data);

            $subApplication->export_country = isset($safData->saf->transportation->export_country, $refCountries[$safData->saf->transportation->export_country]) ? $refCountries[$safData->saf->transportation->export_country]['name'] : '';
            $subApplication->import_name = $safData->saf->sides->import->name ?? '';
        }

        return $subApplications;

    }

    /**
     * Function to generate FSC-Phyto №2
     *
     * @param $data
     * @return array
     */
    private function TjFSC_3($data)
    {
        $subApplicationProducts = SingleApplicationSubApplications::join('saf_sub_application_products', 'saf_sub_application_products.subapplication_id', '=', 'saf_sub_applications.id')
            ->join('saf', 'saf.regular_number', '=', 'saf_sub_applications.saf_number')
            ->where("saf_sub_applications.document_code", Reports::IMPORT_QUARANTINE_PERMISSION_CODE)
            ->where('saf_sub_applications.company_tax_id', Auth::user()->companyTaxId());

        $firsPeriodSubApplicationsProducts = $subApplicationProducts;
        $secondSubApplicationsProducts = $subApplicationProducts;

        //
        if (isset($data['export_country'])) {
            $secondSubApplicationsProducts->where('saf.data->saf->transportation->export_country', $data['export_country']);
            $firsPeriodSubApplicationsProducts->where('saf.data->saf->transportation->export_country', $data['export_country']);
        }

        //
        $firsPeriodSubApplicationsProducts = $firsPeriodSubApplicationsProducts->where("saf_sub_applications.permission_date", '>=', $data['start_date_start'])
            ->where("saf_sub_applications.permission_date", '<=', $data['start_date_end'])
            ->orderByDesc('saf_sub_applications.id')->pluck('product_id')->unique()->all();

        if ($data['end_date_start'] || $data['end_date_end']) {

            if ($data['end_date_start']) {
                $secondSubApplicationsProducts->where("saf_sub_applications.permission_date", '>=', $data['end_date_start']);
            }

            if ($data['end_date_end']) {
                $secondSubApplicationsProducts->where("saf_sub_applications.permission_date", '<=', $data['end_date_end']);
            }

            $secondSubApplicationsProducts = $secondSubApplicationsProducts->orderByDesc('saf_sub_applications.id')->pluck('product_id')->unique()->all();
        } else {
            $secondSubApplicationsProducts = [];
        }

        //
        $firstPeriodSubApplicationProducts = SingleApplicationProducts::select('saf_products.product_code',
            DB::raw("SUM(CAST(saf_products.netto_weight AS DECIMAL)) as netto_weight"),
            DB::raw("array_to_string(array_agg(saf_products.saf_number), ',') as saf_numbers")
        )
            ->whereIn('id', $firsPeriodSubApplicationsProducts)
            ->groupBy('saf_products.product_code')
            ->get()->keyBy('product_code')->toArray();

        $secondPeriodSubApplicationProducts = SingleApplicationProducts::select('saf_products.product_code',
            DB::raw("SUM(CAST(saf_products.netto_weight AS DECIMAL)) as netto_weight"),
            DB::raw("array_to_string(array_agg(saf_products.saf_number), ',') as saf_numbers")
        )
            ->whereIn('id', $secondSubApplicationsProducts)
            ->groupBy('saf_products.product_code')
            ->get()->keyBy('product_code')->toArray();


        //
        $hsCodes = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_HS_CODE_6);
        $refCountries = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_COUNTRIES);

        $result = [];
        foreach ($firstPeriodSubApplicationProducts as $productCode => $firstPeriodProduct) {

            $productName = isset($hsCodes[$productCode]) ? '(' . $hsCodes[$productCode]['code'] . ')' . $hsCodes[$productCode]['path_name'] : '';
            $C = $firstPeriodProduct['netto_weight'] / 1000;
            $D = isset($secondPeriodSubApplicationProducts[$productCode]) ? $secondPeriodSubApplicationProducts[$productCode]['netto_weight'] / 1000 : 0;
            $E = $D - $C;

            $saf = SingleApplication::select('data')->whereIn('regular_number', explode(',', $firstPeriodProduct['saf_numbers']))->get();

            $exportCountries = [];
            foreach ($saf as $value) {
                if (isset($value->data['saf']['transportation']) && !isset($exportCountries[$value->data['saf']['transportation']['export_country']]) && isset($refCountries[$value->data['saf']['transportation']['export_country']])) {
                    $countryId = $value->data['saf']['transportation']['export_country'];
                    $exportCountries[$countryId] = $refCountries[$countryId]['name'];
                }
            }

            $result[] = [
                'B' => $productName,
                'C' => $C,
                'D' => $D,
                'E' => $E,
                'G' => implode(', ', $exportCountries)
            ];
        }

        return $result;
    }

    /**
     * Function to generate КПБ- Report_ Отчет для Ветеринарного разрешение (1)
     *
     * @param $data
     * @return array
     */
    private function TjFSC_4($data)
    {
        $subApplications = SingleApplicationSubApplications::select('saf.regular_number', 'saf_sub_applications.permission_number', 'saf.data as saf_data',
            DB::raw("SUM(CAST(saf_products.netto_weight AS DECIMAL)) as netto_weight"),
            DB::raw("array_to_string(array_agg(saf_products.product_code), ',') as product_codes"),
            DB::raw("array_to_string(array_agg(saf_products.producer_country), ',') as producer_countries"),
            DB::raw("array_to_string(array_agg(saf_products.commercial_description), ',') as commercial_description")
        )
            ->join('saf', 'saf.regular_number', '=', 'saf_sub_applications.saf_number')
            ->join('saf_sub_application_products', 'saf_sub_application_products.subapplication_id', '=', 'saf_sub_applications.id')
            ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
            ->where("saf_sub_applications.document_code", Reports::IMPORT_QUARANTINE_PERMISSION_CODE)
            ->where('saf_sub_applications.company_tax_id', Auth::user()->companyTaxId())
            ->where('saf.regime', SingleApplication::REGIME_IMPORT)
            ->where("saf_sub_applications.permission_date", '>=', $data['start_date_start'])
            ->where("saf_sub_applications.permission_date", '<=', $data['start_date_end']);

        if (isset($data['export_country'])) {
            $subApplications->where('saf.data->saf->transportation->export_country', $data['export_country']);
        }

        if (isset($data['permission_number'])) {
            $subApplications->where('saf_sub_applications.permission_number', 'ILIKE', '%' . $data['permission_number'] . '%');
        }

        if (isset($data['hs_code'])) {
            $refHsCodeId = $this->refHsDataIds($data['hs_code']);
            $subApplications->whereIn('saf_products.product_code', $refHsCodeId);
        }

        $subApplications = $subApplications->orderByDesc('saf_sub_applications.id')->groupBy('saf.id', 'saf_sub_applications.id')->get();

        //
        $hsCodes = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_HS_CODE_6);
        $refCountries = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_COUNTRIES);
        $transportTypes = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_TRANSPORT_TYPE);
        $customBodies = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_CUSTOM_BODY_REFERENCE);

        //
        $result = [];
        foreach ($subApplications as $subApplication) {
            $safData = json_decode($subApplication->saf_data, true);
            $productCodes = explode(',', $subApplication->product_codes);
            $producerCountries = explode(',', $subApplication->producer_countries);

            //
            $productNames = [];
            foreach ($productCodes as $productCode) {
                if (isset($hsCodes[$productCode]) && !isset($productNames[$productCode])) {
                    $productNames[$productCode] = '(' . $hsCodes[$productCode]['code'] . ')' . $hsCodes[$productCode]['path_name'];
                }
            }

            //
            $producerCountriesNames = [];
            foreach ($producerCountries as $producerCountry) {
                if (isset($refCountries[$producerCountry]) && !isset($producerCountriesNames[$producerCountry])) {
                    $producerCountriesNames[$producerCountry] = $refCountries[$producerCountry]['name'];
                }
            }

            //
            $typeOfTransport = $safData['saf']['transportation']['type_of_transport'];
            $transportAppointment = $safData['saf']['transportation']['appointment'];

            $result[] = [
                'B' => $safData['saf']['sides']['import']['name'],
                'C' => $subApplication->permission_number,
                'D' => implode("\n", $productNames),
                'E' => $subApplication->commercial_description ?? '',
                'F' => trans('swis.report.unit_of_measurement.tons.label'),
                'G' => $subApplication->netto_weight / 1000,
                'H' => implode(", ", $producerCountriesNames),
                'I' => isset($transportTypes[$typeOfTransport]) ? $transportTypes[$typeOfTransport]['name'] : '',
                'J' => isset($customBodies[$transportAppointment]) ? $customBodies[$transportAppointment]['name'] : ''
            ];
        }

        return $result;
    }

    /**
     * Function to generate КПБ- Отчет КПБ- Фитосанитарный сертификат при экспорте (по периоду-по товарам
     *
     * @param $data
     * @return array
     */
    private function TjFSC_5($data)
    {
        $subApplications = SingleApplicationSubApplications::select('saf.regular_number', 'saf.data as saf_data', 'saf_sub_applications.agency_subdivision_id', 'saf_products.product_code',
            DB::raw("saf_products.total_value_national::decimal as saf_products_national_total"),
            DB::raw("CAST(saf_products.netto_weight AS DECIMAL) as netto_weight")
        )
            ->join('saf', 'saf.regular_number', '=', 'saf_sub_applications.saf_number')
            ->join('saf_sub_application_products', 'saf_sub_application_products.subapplication_id', '=', 'saf_sub_applications.id')
            ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
            ->where('saf_sub_applications.company_tax_id', Auth::user()->companyTaxId())
            ->where('saf.regime', SingleApplication::REGIME_EXPORT)
            ->where("saf_sub_applications.permission_date", '>=', $data['start_date_start'])
            ->where("saf_sub_applications.permission_date", '<=', $data['start_date_end'])
            ->where("saf_sub_applications.document_code", Reports::PHYTOSANITARY_CERTIFICATE_CODE);

        if (isset($data['import_country'])) {
            $subApplications->where('saf.data->saf->transportation->import_country', $data['import_country']);
        }

        if (isset($data['export_company'])) {
            $subApplications->where(function ($q) use ($data) {
                $q->where('saf.data->saf->sides->export->name', 'ilike', "%{$data['export_company']}%")->orWhere('saf.exporter_value', 'ilike', "%{$data['export_company']}%");
            });
        }

        if (isset($data['sub_division_id'])) {
            $subApplications->where('saf_sub_applications.agency_subdivision_id', $data['sub_division_id']);
        }

        if (isset($data['hs_code'])) {
            $refHsCodeId = $this->refHsDataIds($data['hs_code']);
            $subApplications->whereIn('saf_products.product_code', $refHsCodeId);
        }

        $subApplications = $subApplications->orderByDesc('product_code')->orderByDesc('saf_sub_applications.id')->get();

        //
        $hsCodes = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_HS_CODE_6);
        $refCountries = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_COUNTRIES);
        $refSubDivisions = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS);

        $result = [];
        foreach ($subApplications as $subApplication) {

            $safData = json_decode($subApplication->saf_data, true);

            $transportImportCountry = $safData['saf']['transportation']['import_country'];
            $subDivisionId = $subApplication->agency_subdivision_id;
            $productsNationalTotalValue = $subApplication->saf_products_national_total;
            $productCode = $subApplication->product_code;

            //
            $productName = isset($hsCodes[$productCode]) ? '(' . $hsCodes[$productCode]['code'] . ')' . $hsCodes[$productCode]['path_name'] : '';

            //
            $result[] = [
                'B' => $safData['saf']['sides']['export']['name'],
                'C' => $productName,
                'D' => trans('swis.report.unit_of_measurement.tons.label'),
                'E' => $subApplication->netto_weight / 1000,
                'F' => $productsNationalTotalValue,
                'G' => isset($refCountries[$transportImportCountry]) ? $refCountries[$transportImportCountry]['name'] : '',
                'H' => isset($refSubDivisions[$subDivisionId]) ? $refSubDivisions[$subDivisionId]['name'] : '',
            ];
        }


        return $result;
    }

    /**
     * Function to generate КПБ - Report Вет.Отчет о привозимых товаров (по периоду)
     *
     * @param $data
     * @return array
     */
    private function TjFSC_6($data)
    {
        $subApplicationProducts = SingleApplicationSubApplications::join('saf_sub_application_products', 'saf_sub_application_products.subapplication_id', '=', 'saf_sub_applications.id')
            ->join('saf', 'saf.regular_number', '=', 'saf_sub_applications.saf_number')
            ->where('saf_sub_applications.company_tax_id', Auth::user()->companyTaxId())
            ->whereIn('saf.regime', $data['regime']);

        if (isset($data['document_id'])) {
            $subApplicationProducts->where('constructor_document_id', $data['document_id']);
        }

        if (isset($data['hs_code'])) {
            $refHsCodeId = $this->refHsDataIds($data['hs_code']);
            $subApplicationProducts->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')->whereIn('saf_products.product_code', $refHsCodeId);
        }

        $firstPeriodSubApplicationsProducts = $subApplicationProducts;
        $secondPeriodSubApplicationsProducts = $subApplicationProducts;

        //
        $firstPeriodProductsId = $firstPeriodSubApplicationsProducts->where("saf_sub_applications.permission_date", '>=', $data['start_date_start'])
            ->where("saf_sub_applications.permission_date", '<=', $data['start_date_end'])->orderByDesc('saf_sub_applications.id')->pluck('product_id')->unique()->all();

        if (isset($data['end_date_start']) && $data['end_date_end']) {
            $secondPeriodProductsId = $secondPeriodSubApplicationsProducts->where("saf_sub_applications.permission_date", '>=', $data['end_date_start'])
                ->where("saf_sub_applications.permission_date", '<=', $data['end_date_end'])->orderByDesc('saf_sub_applications.id')->pluck('product_id')->unique()->all();
        }

        //
        $firstPeriodSubApplicationProducts = SingleApplicationProducts::select('saf_products.product_code',
            DB::raw("SUM(CAST(saf_products.netto_weight AS DECIMAL)) as netto_weight"),
            DB::raw("array_to_string(array_agg(saf_products.producer_country), ',') as producer_countries")
        )
            ->whereIn('id', $firstPeriodProductsId)
            ->groupBy('saf_products.product_code')
            ->get()->keyBy('product_code')->toArray();

        $secondPeriodSubApplicationProducts = SingleApplicationProducts::select('saf_products.product_code',
            DB::raw("SUM(CAST(saf_products.netto_weight AS DECIMAL)) as netto_weight"),
            DB::raw("array_to_string(array_agg(saf_products.producer_country), ',') as producer_countries")
        )
            ->whereIn('id', ($secondPeriodProductsId ?? []))
            ->groupBy('saf_products.product_code')
            ->get()->keyBy('product_code')->toArray();

        //
        $hsCodes = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_HS_CODE_6);
        $refCountries = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_COUNTRIES);

        //
        $result = [];
        foreach ($firstPeriodSubApplicationProducts as $productCode => $firstPeriodSubApplication) {

            $producerCountries = explode(',', $firstPeriodSubApplication['producer_countries']);

            //
            $producerCountriesNames = [];
            foreach ($producerCountries as $producerCountry) {
                if (isset($refCountries[$producerCountry]) && !isset($producerCountriesNames[$producerCountry])) {
                    $producerCountriesNames[$producerCountry] = $refCountries[$producerCountry]['name'];
                }
            }

            //
            $productName = isset($hsCodes[$productCode]) ? '(' . $hsCodes[$productCode]['code'] . ')' . $hsCodes[$productCode]['path_name'] : '';

            $D = $firstPeriodSubApplication['netto_weight'] / 1000;
            $E = isset($secondPeriodSubApplicationProducts[$productCode]) ? $secondPeriodSubApplicationProducts[$productCode]['netto_weight'] / 1000 : 0;
            $F = $E - $D;

            //
            $result[] = [
                'B' => $productName,
                'C' => trans('swis.report.unit_of_measurement.tons.label'),
                'D' => $D,
                'E' => $E,
                'F' => $F,
                'G' => implode(", ", $producerCountriesNames),
            ];

        }

        return $result;
    }

    /**
     * Function to generate TjFSC_7
     *
     * @param $data
     * @return array
     */
    private function TjFSC_7($data)
    {
        $subApplications = SingleApplicationSubApplications::select('saf.regular_number', 'saf_sub_applications.permission_number', 'saf.data as saf_data',
            DB::raw("SUM(CAST(saf_products.netto_weight AS DECIMAL)) as netto_weight"),
            DB::raw("array_to_string(array_agg(saf_products.product_code), ',') as product_codes"),
            DB::raw("array_to_string(array_agg(saf_products.producer_country), ',') as producer_countries")
        )
            ->join('saf', 'saf.regular_number', '=', 'saf_sub_applications.saf_number')
            ->join('saf_sub_application_products', 'saf_sub_application_products.subapplication_id', '=', 'saf_sub_applications.id')
            ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
            ->where("saf_sub_applications.document_code", Reports::VETERINARY_RESOLUTION_CODE)
            ->where('saf_sub_applications.company_tax_id', Auth::user()->companyTaxId())
            ->where('saf.regime', SingleApplication::REGIME_IMPORT)
            ->where("saf_sub_applications.permission_date", '>=', $data['start_date_start'])
            ->where("saf_sub_applications.permission_date", '<=', $data['start_date_end']);

        if (isset($data['export_country'])) {
            $subApplications->where('saf.data->saf->transportation->export_country', $data['export_country']);
        }

        if (isset($data['permission_number'])) {
            $subApplications->where('saf_sub_applications.permission_number', 'ILIKE', '%' . $data['permission_number'] . '%');
        }

        if (isset($data['hs_code'])) {
            $refHsCodeId = $this->refHsDataIds($data['hs_code']);
            $subApplications->whereIn('saf_products.product_code', $refHsCodeId);
        }

        if (isset($data['producer_country'])) {
            $subApplications->where('saf_products.producer_country', $data['producer_country']);
        }

        $subApplications = $subApplications->orderByDesc('saf_sub_applications.id')->groupBy('saf.id', 'saf_sub_applications.id')->get();

        //
        $hsCodes = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_HS_CODE_6);
        $refCountries = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_COUNTRIES);
        $transportTypes = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_TRANSPORT_TYPE);
        $customBodies = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_CUSTOM_BODY_REFERENCE);

        //
        $result = [];
        foreach ($subApplications as $subApplication) {
            $safData = json_decode($subApplication->saf_data, true);
            $productCodes = explode(',', $subApplication->product_codes);
            $producerCountries = explode(',', $subApplication->producer_countries);

            //
            $productNames = [];
            foreach ($productCodes as $productCode) {
                if (isset($hsCodes[$productCode]) && !isset($productNames[$productCode])) {
                    $productNames[$productCode] = '(' . $hsCodes[$productCode]['code'] . ')' . $hsCodes[$productCode]['path_name'];
                }
            }

            //
            $producerCountriesNames = [];
            foreach ($producerCountries as $producerCountry) {
                if (isset($refCountries[$producerCountry]) && !isset($producerCountriesNames[$producerCountry])) {
                    $producerCountriesNames[$producerCountry] = $refCountries[$producerCountry]['name'];
                }
            }

            //
            $typeOfTransport = $safData['saf']['transportation']['type_of_transport'];
            $transportAppointment = $safData['saf']['transportation']['appointment'];

            $result[] = [
                'B' => $safData['saf']['sides']['import']['name'],
                'C' => $subApplication->permission_number,
                'D' => implode("\n", $productNames),
                'E' => trans('swis.report.unit_of_measurement.tons.label'),
                'F' => $subApplication->netto_weight / 1000,
                'G' => implode(", ", $producerCountriesNames),
                'H' => isset($transportTypes[$typeOfTransport]) ? $transportTypes[$typeOfTransport]['name'] : '',
                'I' => isset($customBodies[$transportAppointment]) ? $customBodies[$transportAppointment]['name'] : ''
            ];
        }

        return $result;
    }

    /**
     * Function to generate TjFSC_8
     *
     * @param $data
     * @return array
     */
    private function TjFSC_8($data)
    {
        $subApplications = SingleApplicationSubApplications::select('permission_date', 'expiration_date', 'permission_number', 'saf.data as saf_data', 'commercial_description')
            ->join('saf', 'saf.regular_number', '=', 'saf_sub_applications.saf_number')
            ->join('saf_sub_application_products', 'saf_sub_application_products.subapplication_id', '=', 'saf_sub_applications.id')
            ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
            ->join('saf_sub_application_states', function ($q) {
                $q->on('saf_sub_application_states.saf_subapplication_id', '=', 'saf_sub_applications.id')->where('is_current', '1');
            })
            ->join('constructor_states', function ($join) {
                $join->on('constructor_states.id', '=', 'saf_sub_application_states.state_id')->where(['state_approved_status' => ConstructorStates::END_STATE_APPROVED, 'state_type' => ConstructorStates::END_STATE_TYPE]);
            })
            ->where("saf_sub_applications.document_code", Reports::IMPORT_QUARANTINE_PERMISSION_CODE)
            ->where('saf_sub_applications.company_tax_id', Auth::user()->companyTaxId())
            ->where("saf_sub_applications.permission_date", '>=', $data['start_date_start'])
            ->where("saf_sub_applications.permission_date", '<=', $data['start_date_end']);

        if (isset($data['export_country'])) {
            $subApplications->where('saf.data->saf->transportation->export_country', $data['export_country']);
        }

        if (isset($data['permission_number'])) {
            $subApplications->where('saf_sub_applications.permission_number', 'ILIKE', '%' . $data['permission_number'] . '%');
        }

        if (isset($data['commercial_description'])) {
            $subApplications->where('saf_products.commercial_description', 'ILIKE', '%' . $data['commercial_description'] . '%');
        }

        if (isset($data['importer'])) {
            $subApplications->where('saf.data->saf->sides->import->name', 'ILIKE', "%{$data['importer']}%")
                ->orWhere('saf.importer_value', 'ILIKE', "%{$data['importer']}%");
        }

        $subApplications = $subApplications->orderByDesc('saf_sub_applications.id')->groupBy('saf.id', 'saf_sub_applications.id', 'saf_products.id')->get();

        $result = [];
        foreach ($subApplications as $subApplication) {
            $safData = json_decode($subApplication->saf_data, true);
            $importer = '(' . $safData['saf']['sides']['import']['type_value'] . ') ' . $safData['saf']['sides']['import']['name'];

            $exportCountry = getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, $safData['saf']['transportation']['export_country']);

            $result[] = [
                'permission_date' => $subApplication->permission_date,
                'expiration_date' => $subApplication->expiration_date,
                'permission_number' => $subApplication->permission_number,
                'export_country' => "($exportCountry->code) " . $exportCountry->name,
                'importer' => $importer,
                'product_commercial_description' => $subApplication->commercial_description
            ];
        }

        return $result;
    }

    /**
     * Function to generate TjFSC_8
     *
     * @param $data
     * @return array
     */
    private function TjFSC_9($data)
    {
        $constructorDocument = ConstructorDocument::select('id')->where('document_code', Reports::VETERINARY_CERTIFICATE_CODE)->first();
        $endApproveStates = ConstructorStates::getEndApprovedStateIds($constructorDocument->id);

        // Get SubApplications
        $subAppStart = $this->getSubApplicationIds($endApproveStates, $constructorDocument->id, null, $data['start_date_start'], $data['start_date_end']);

        $subApplicationsWithProducts = SingleApplicationSubApplicationProducts::
        select('saf_products.producer_country',
            'saf_products.commercial_description',
            'saf_products.commercial_description',
            'saf_products.measurement_unit',
            'permission_date',
            'saf.data as saf_data',
            'saf_sub_applications.custom_data',
            'saf_sub_applications.agency_subdivision_id',
            DB::raw('SUM(saf_products_batch.approved_quantity::numeric) as approved_quantity'),
            DB::raw('SUM(saf_obligations.obligation) as obligation_sum')
        )
            ->whereIn('saf_sub_application_products.subapplication_id', $subAppStart->pluck('id')->all())
            ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
            ->join('saf_products_batch', 'saf_products.id', '=', 'saf_products_batch.saf_product_id')
            ->join('saf', 'saf.regular_number', '=', 'saf_products.saf_number')
            ->join('saf_sub_applications', 'saf_sub_applications.saf_number', '=', 'saf.regular_number')
            ->join('saf_obligations', 'saf_obligations.saf_number', '=', 'saf.regular_number')
            ->join('saf_sub_application_states', function ($q) {
                $q->on('saf_sub_application_states.saf_subapplication_id', '=', 'saf_sub_applications.id')->where('is_current', '1');
            })
            ->join('constructor_states', function ($join) {
                $join->on('constructor_states.id', '=', 'saf_sub_application_states.state_id')->where(['state_approved_status' => ConstructorStates::END_STATE_APPROVED, 'state_type' => ConstructorStates::END_STATE_TYPE]);
            })
            ->whereNotNull('saf_products_batch.approved_quantity');

        if (isset($data['sub_division_id'])) {
            $subApplicationsWithProducts->where('saf_sub_applications.agency_subdivision_id', $data['sub_division_id']);
        }

        if (isset($data['importer'])) {
            $subApplicationsWithProducts->where('saf.data->saf->sides->import->name', 'ILIKE', "%{$data['importer']}%")
                ->orWhere('saf.importer_value', 'ILIKE', "%{$data['importer']}%");
        }

        if (isset($data['commercial_description'])) {
            $subApplicationsWithProducts->where('saf_products.commercial_description', 'ILIKE', '%' . $data['commercial_description'] . '%');
        }

        if (isset($data['producer_country'])) {
            $subApplicationsWithProducts->where('saf_products.producer_country', $data['producer_country']);
        }

        $subApplicationsWithProducts = $subApplicationsWithProducts->orderByDesc('saf_sub_applications.id')->groupBy('saf_products.id', 'saf.id', 'saf_sub_applications.id')->get();

        $refMeasurementUnitAll = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT_TABLE_NAME);
        $refSubDivisionsAll = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS);
        $refCountriesAll = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_COUNTRIES);

        $result = [];
        foreach ($subApplicationsWithProducts as $subApplicationWithProduct) {
            $safData = json_decode($subApplicationWithProduct->saf_data, true);
            $customData = json_decode($subApplicationWithProduct->custom_data, true);
            $importer = '(' . $safData['saf']['sides']['import']['type_value'] . ') ' . $safData['saf']['sides']['import']['name'];

            $refMeasurementUnit = $refMeasurementUnitAll[$subApplicationWithProduct->measurement_unit] ?? [];
            $refSubdivision = $refSubDivisionsAll[$subApplicationWithProduct->agency_subdivision_id] ?? [];
            $refCountry = $refCountriesAll[$subApplicationWithProduct->producer_country] ?? [];

            $result[] = [
                'permission_date' => $subApplicationWithProduct->permission_date,
                'permission_number' => $customData ? $customData['Veterinary_63'] ?? '' : '',
                'importer' => $importer,
                'product_commercial_description' => $subApplicationWithProduct->commercial_description,
                'product_measurement_unit' => $refMeasurementUnit ? '(' . $refMeasurementUnit['code'] . ') ' . $refMeasurementUnit['name'] : '',
                'product_approved_quantity' => $subApplicationWithProduct->approved_quantity,
                'obligation_sum' => $subApplicationWithProduct->obligation_sum,
                'product_residue' => $customData ? $customData['Veterinary_64'] ?? '' : '',
                'product_producer_country' => $refCountry ? '(' . $refCountry['code'] . ') ' . $refCountry['name'] : '',
                'subdivision' => $refSubdivision ? '(' . $refSubdivision["code"] . ') ' . $refSubdivision['name'] : ''
            ];
        }

        return $result;
    }

}