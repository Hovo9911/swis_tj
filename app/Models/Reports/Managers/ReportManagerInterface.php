<?php

namespace App\Models\Reports\Managers;

interface ReportManagerInterface
{
    public function getData(string $reportCode, array $inputData);
}