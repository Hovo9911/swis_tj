<?php

namespace App\Models\Reports\Managers;

use App\Models\DataModel\DataModel;
use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\Reports\ReportManager;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationObligations\SingleApplicationObligations;
use App\Models\SingleApplicationProducts\SingleApplicationProducts;
use App\Models\SingleApplicationSubApplicationProducts\SingleApplicationSubApplicationProducts;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

// Tajikstandart agency tax_id = 030000230 reports part

/**
 * Class ReportManager_TjSt
 * @package App\Models\Reports\Managers
 */
class ReportManager_TjSt extends ReportManager implements ReportManagerInterface
{
    /**
     * Function to get report data
     *
     * @param string $reportCode
     * @param array $inputData
     * @return array
     */
    public function getData(string $reportCode, array $inputData)
    {
        return $this->$reportCode($inputData);
    }

    /**
     * Function to generate Report 1
     *
     * @param $data
     * @return array
     */
    private function TjSt_1($data)
    {
        $hs_code_count = $data['hs_code_count'];

        $hsCodeReferenceTableName = ReferenceTable::REFERENCE_HS_CODE_6;

        // Get SubApplications
        $subAppStart = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['start_date_start'], $data['start_date_end']);
        $subAppEnd = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['end_date_start'], $data['end_date_end']);

        // Get SubApplication Products IDS
        $subApplicationsStartProductsIds = SingleApplicationSubApplicationProducts::whereIn('subapplication_id', $subAppStart->pluck('id')->all())->pluck('product_id');
        $subApplicationsEndProductsIds = SingleApplicationSubApplicationProducts::whereIn('subapplication_id', $subAppEnd->pluck('id')->all())->pluck('product_id');

        // By Product ids get product HS codes
        $startHsCodes = SingleApplicationProducts::whereIn('saf_products.id', $subApplicationsStartProductsIds)
            ->leftJoin($hsCodeReferenceTableName, 'saf_products.product_code', '=', DB::raw("{$hsCodeReferenceTableName}.id::varchar"))
            ->whereNotNull('saf_products.product_code')
            ->whereNotNull($hsCodeReferenceTableName . '.code')
            ->pluck($hsCodeReferenceTableName . '.code')
            ->unique()
            ->all();


        $endHsCodes = SingleApplicationProducts::whereIn('saf_products.id', $subApplicationsEndProductsIds)
            ->leftJoin($hsCodeReferenceTableName, 'saf_products.product_code', '=', DB::raw("{$hsCodeReferenceTableName}.id::varchar"))
            ->whereNotNull('saf_products.product_code')
            ->whereNotNull($hsCodeReferenceTableName . '.code')
            ->pluck($hsCodeReferenceTableName . '.code')
            ->unique()
            ->all();

        // All subApplication products HS CODES
        $allHsCodes = array_unique(array_merge($startHsCodes, $endHsCodes));

        $filteredHsCodes = [];
        foreach ($allHsCodes as $hsCode) {
            $filteredHsCodes[] = substr($hsCode, 0, $hs_code_count);
        }
        $filteredHsCodes = array_unique($filteredHsCodes);

        // Get Start date all report data
        $subAppStartReportData = $this->getSubApplicationAllData($subAppStart->pluck('id'), $filteredHsCodes);
        // Get End date all report data
        $subAppEndReportData = $this->getSubApplicationAllData($subAppEnd->pluck('id'), $filteredHsCodes);

        $hsCodeReferenceTableName = ReferenceTable::REFERENCE_HS_CODE_6;
        $hsCodeReferenceMlTableName = $hsCodeReferenceTableName . '_ml';

        $sumAllReportData = [];
        foreach ($filteredHsCodes as $key => $hsCode) {
            $hsCodeName = substr($hsCode, 0, $hs_code_count);

            $refRow = DB::table($hsCodeReferenceTableName)
                ->join($hsCodeReferenceMlTableName, $hsCodeReferenceTableName . '.id', '=', $hsCodeReferenceMlTableName . '.' . $hsCodeReferenceTableName . '_id')
                ->where('code', $hsCode)->first();

            if (!is_null($refRow)) {
                $hsCodeName = optional($refRow)->path_name . ' (' . $hsCode . ')';
            }

            $dataCheck = null;

            if (isset($subAppStartReportData[$hsCode])) {
                $dataCheck = $subAppStartReportData[$hsCode];
            } elseif (isset($subAppEndReportData[$hsCode])) {
                $dataCheck = $subAppEndReportData[$hsCode];
            }

            if (!is_null($dataCheck)) {

                foreach ($dataCheck as $type => $val) {

                    $sumAllReportData[$hsCode]['types'][$type] = [
                        'approvedQty_A' => isset($subAppStartReportData[$hsCode][$type]) ? $subAppStartReportData[$hsCode][$type]['approvedQty'] : 0,
                        'approvedQty_B' => isset($subAppEndReportData[$hsCode][$type]) ? $subAppEndReportData[$hsCode][$type]['approvedQty'] : 0,
                    ];

                    $sumAllReportData[$hsCode]['hsCodeName'] = $hsCodeName;

                }
            }
        }

        return $sumAllReportData;
    }

    /**
     * Function to Report 2
     *
     * @param $data
     * @return array
     */
    private function TjSt_2($data)
    {
        $hsCode = $data['hs_code'];
        $allReportData = [];

        $refHsCodeId = $this->refHsDataIds($hsCode);

        if (count($refHsCodeId)) {

            // Get SubApplications
            $subAppStart = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['start_date_start'], $data['start_date_end']);
            $subAppEnd = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['end_date_start'], $data['end_date_end']);

            // Get SubApplication producer countries with group by approved data
            $subApplicationsStartCountriesApprovedQty = SingleApplicationSubApplicationProducts::selectRaw('SUM(saf_products_batch.approved_quantity::numeric) as approved_quantity , producer_country,saf_products_batch.measurement_unit')
                ->whereIn('subapplication_id', $subAppStart->pluck('id')->all())
                ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
                ->join('saf_products_batch', 'saf_products.id', '=', 'saf_products_batch.saf_product_id')
                ->whereIn('saf_products.product_code', $refHsCodeId)
                ->groupBy('producer_country', 'saf_products_batch.measurement_unit')
                ->orderBy('producer_country', 'asc')
                ->orderBy('approved_quantity', 'desc')
                ->get();

            $subApplicationsEndCountriesApprovedQty = SingleApplicationSubApplicationProducts::selectRaw('SUM(saf_products_batch.approved_quantity::numeric) as approved_quantity , producer_country,saf_products_batch.measurement_unit')
                ->whereIn('subapplication_id', $subAppEnd->pluck('id')->all())
                ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
                ->join('saf_products_batch', 'saf_products.id', '=', 'saf_products_batch.saf_product_id')
                ->whereIn('saf_products.product_code', $refHsCodeId)
                ->groupBy('producer_country', 'saf_products_batch.measurement_unit')
                ->orderBy('producer_country', 'asc')
                ->orderBy('approved_quantity', 'desc')
                ->get();

            $startCountriesData = [];
            foreach ($subApplicationsStartCountriesApprovedQty as $value) {
                $startCountriesData[$value->producer_country][$value->measurement_unit] = $value->approved_quantity;
            }

            $endCountriesData = [];
            foreach ($subApplicationsEndCountriesApprovedQty as $value) {
                $endCountriesData[$value->producer_country][$value->measurement_unit] = $value->approved_quantity;
            }

            $countriesReferenceTableName = ReferenceTable::REFERENCE_COUNTRIES;
            $countriesReferenceMlTableName = ReferenceTable::REFERENCE_COUNTRIES . '_ml';

            $countries = DB::table($countriesReferenceTableName)
                ->join($countriesReferenceMlTableName, $countriesReferenceTableName . '.id', '=', $countriesReferenceMlTableName . '.' . $countriesReferenceTableName . '_id')
                ->where($countriesReferenceMlTableName . '.lng_id', '=', cLng('id'))
                ->get();


            foreach ($countries as $key => $country) {
                $dataCheck = null;

                if (isset($startCountriesData[$country->id])) {
                    $dataCheck = $startCountriesData[$country->id];
                } elseif (isset($endCountriesData[$country->id])) {
                    $dataCheck = $endCountriesData[$country->id];
                }

                if (!is_null($dataCheck)) {
                    foreach ($dataCheck as $type => $val) {
                        $allReportData[$key]['types'][$type]['approvedQty_A'] = (isset($startCountriesData[$country->id]) && isset($startCountriesData[$country->id][$type])) ? $startCountriesData[$country->id][$type] : 0;
                        $allReportData[$key]['types'][$type]['approvedQty_B'] = (isset($endCountriesData[$country->id]) && isset($endCountriesData[$country->id][$type])) ? $endCountriesData[$country->id][$type] : 0;
                    }

                    $allReportData[$key]['country_name'] = $country->name;
                }

            }
        }

        return $allReportData;
    }

    /**
     * Function Report 3
     *
     * @param $data
     * @return array
     */
    private function TjSt_3($data)
    {
        // Get SubApplications
        $subAppStartImport = $this->getSubApplicationIds($data['document_status'], $data['document_id'], SingleApplication::REGIME_IMPORT, $data['start_date_start'], $data['start_date_end']);
        $subAppStartExport = $this->getSubApplicationIds($data['document_status'], $data['document_id'], SingleApplication::REGIME_IMPORT, $data['end_date_start'], $data['end_date_end']);

        $subAppEndImport = $this->getSubApplicationIds($data['document_status'], $data['document_id'], SingleApplication::REGIME_EXPORT, $data['start_date_start'], $data['start_date_end']);
        $subAppEndExport = $this->getSubApplicationIds($data['document_status'], $data['document_id'], SingleApplication::REGIME_EXPORT, $data['end_date_start'], $data['end_date_end']);

        return [
            SingleApplication::REGIME_IMPORT => [
                'C' => $subAppStartImport->count(),
                'D' => $subAppStartExport->count(),
            ],
            SingleApplication::REGIME_EXPORT => [
                'C' => $subAppEndImport->count(),
                'D' => $subAppEndExport->count(),
            ]
        ];
    }

    /**
     * Function Report 4
     *
     * @param $data
     * @return array
     */
    private function TjSt_4($data)
    {
        $hsCode = $data['hs_code'];
        $refHsCodeId = $this->refHsDataIds($hsCode);

        $allReportData = [];

        if (count($refHsCodeId)) {

            // Get SubApplications
            $subAppStart = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['start_date_start'], $data['start_date_end']);
            $subAppEnd = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['end_date_start'], $data['end_date_end']);

            $subApplicationsStartCountriesApprovedQty = SingleApplicationSubApplicationProducts::selectRaw('SUM(saf_products_batch.approved_quantity::numeric) as approved_quantity ,product_code ,saf_products.measurement_unit')
                ->whereIn('subapplication_id', $subAppStart->pluck('id')->all())
                ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
                ->join('saf_products_batch', 'saf_products.id', '=', 'saf_products_batch.saf_product_id')
                ->whereIn('saf_products.product_code', $refHsCodeId)
                ->groupBy('product_code', 'saf_products.measurement_unit')
                ->orderBy('product_code', 'asc')
                ->orderBy('saf_products.measurement_unit', 'desc')
                ->whereNotNull('saf_products_batch.approved_quantity')
                ->get();


            $subApplicationsEndCountriesApprovedQty = SingleApplicationSubApplicationProducts::selectRaw('SUM(saf_products_batch.approved_quantity::numeric) as approved_quantity ,product_code ,saf_products.measurement_unit')
                ->whereIn('subapplication_id', $subAppEnd->pluck('id')->all())
                ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
                ->join('saf_products_batch', 'saf_products.id', '=', 'saf_products_batch.saf_product_id')
                ->whereIn('saf_products.product_code', $refHsCodeId)
                ->groupBy('product_code', 'saf_products.measurement_unit')
                ->orderBy('product_code', 'asc')
                ->orderBy('saf_products.measurement_unit', 'desc')
                ->whereNotNull('saf_products_batch.approved_quantity')
                ->get();

            $startHsCodes = $subApplicationsStartCountriesApprovedQty->pluck('product_code')->unique()->all();
            $endHsCodes = $subApplicationsEndCountriesApprovedQty->pluck('product_code')->unique()->all();

            $allSearchedHsCodes = array_unique(array_merge($startHsCodes, $endHsCodes));

            $startHsCodesData = [];
            foreach ($subApplicationsStartCountriesApprovedQty as $value) {
                $startHsCodesData[$value->product_code][$value->measurement_unit] = $value->approved_quantity;
            }

            $endHsCodesData = [];
            foreach ($subApplicationsEndCountriesApprovedQty as $value) {
                $endHsCodesData[$value->product_code][$value->measurement_unit] = $value->approved_quantity;
            }

            foreach ($allSearchedHsCodes as $hsCode) {

                if (!isset($allReportData[$hsCode])) {

                    if (isset($startHsCodesData[$hsCode])) {

                        $approvedQty_D = $endHsCodesData[$hsCode] ?? false;
                        foreach ($startHsCodesData[$hsCode] as $key => $approvedQty) {

                            $measurement = getReferenceRows(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $key);

                            $allReportData[$hsCode][$key] = [
                                'measurement_name' => $measurement->name ?? '-',
                                'approvedQty_B' => $approvedQty,
                                'approvedQty_D' => ($approvedQty_D && isset($approvedQty_D[$key])) ? $approvedQty_D[$key] : 0
                            ];
                        }

                    } elseif (isset($endHsCodesData[$hsCode])) {

                        $approvedQty_B = $startHsCodesData[$hsCode] ?? false;
                        foreach ($endHsCodesData[$hsCode] as $key => $approvedQty) {

                            $measurement = getReferenceRows(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $key);

                            $allReportData[$hsCode][$key] = [
                                'measurement_name' => $measurement->name ?? '-',
                                'approvedQty_B' => ($approvedQty_B && isset($approvedQty_B[$key])) ? $approvedQty_B[$key] : 0,
                                'approvedQty_D' => $approvedQty
                            ];
                        }

                    }

                }
            }
        }

        return $allReportData;

    }

    /**
     * Function Report 5
     *
     * @param $data
     * @return array
     */
    private function TjSt_5($data)
    {
        $months = [];
        for ($x = 1; $x <= 12; $x++) {
            $months[date('m', mktime(0, 0, 0, $x, 1))] = date('F', mktime(0, 0, 0, $x, 1));
        }

        $byYear = $data['year'];
        $documentStatus = $data['document_status'];
        $documentId = $data['document_id'];
        $inputSubDivisions = $data['sub_division_ids'] ?? [];

        $subDivisions = $inputSubDivisions;

        if (!count($subDivisions)) {
            $subDivisions = $this->getAllSubdivisions();
        }

        $dataModel = new DataModel();

        $documentDatasetMdm = DocumentsDatasetFromMdm::select('id', 'field_id')->where('document_id', $documentId)->where('mdm_field_id', $dataModel->getMdmFieldIDByConstant(DataModel::PERMISSION_DATE_MDM_ID))->first();

        $mdmFieldId = !is_null($documentDatasetMdm) ? $documentDatasetMdm->field_id : null;

        $startDate = $byYear . '-01-01';
        $endDate = $byYear . '-12-31';

        $subApplications = SingleApplicationSubApplications::select([
            'saf_sub_applications.id',
            'saf_sub_applications.agency_subdivision_id',
            'saf_sub_applications.custom_data',
            DB::raw('(SELECT SUM(saf_obligations.payment) from saf_obligations where saf_obligations.subapplication_id = saf_sub_applications.id ) as certificates_sum')
        ])
            ->join('saf', 'saf.regular_number', '=', 'saf_sub_applications.saf_number')
            ->join('saf_sub_application_states', 'saf_sub_application_states.saf_subapplication_id', '=', 'saf_sub_applications.id')
            ->where('saf_sub_applications.company_tax_id', '=', Auth::user()->companyTaxId())
            ->where('is_current', '1')
            ->whereNotNull('agency_subdivision_id')
            ->where('saf_sub_application_states.state_id', $documentStatus)
            ->where("saf_sub_applications.custom_data->{$mdmFieldId}", '>=', $startDate)
            ->where("saf_sub_applications.custom_data->{$mdmFieldId}", '<=', $endDate);


        if ($data['regime'] != 'together') {
            $subApplications->where('saf.regime', $data['regime']);
        }

        $subApplications->whereIn('agency_subdivision_id', $subDivisions);

        $subApplications = $subApplications->get();

        $subApplicationsAllMonth = [];

        foreach ($subApplications as $subApplication) {
            $approvedDate = $subApplication->custom_data[$mdmFieldId];

            $approvedMonth = date('m', strtotime($approvedDate));

            $monthCertificatesQuantity = isset($subApplicationsAllMonth[$subApplication->agency_subdivision_id][$approvedMonth]) ? $subApplicationsAllMonth[$subApplication->agency_subdivision_id][$approvedMonth]['certificates_quantity'] : 0;
            $monthCertificatesSum = isset($subApplicationsAllMonth[$subApplication->agency_subdivision_id][$approvedMonth]) ? $subApplicationsAllMonth[$subApplication->agency_subdivision_id][$approvedMonth]['certificates_sum'] : 0;

            $currentMonthSum = $monthCertificatesSum + $subApplication['certificates_sum'];
            $currentMonthQuantity = $monthCertificatesQuantity + 1;

            $subApplicationsAllMonth[$subApplication->agency_subdivision_id][$approvedMonth] = [
                'certificates_quantity' => $currentMonthQuantity,
                'certificates_sum' => $currentMonthSum
            ];

            $subApplicationsAllMonth[$subApplication->agency_subdivision_id]['certificates_all_quantity'] = isset($subApplicationsAllMonth[$subApplication->agency_subdivision_id]['certificates_all_quantity']) ? $subApplicationsAllMonth[$subApplication->agency_subdivision_id]['certificates_all_quantity'] + 1 : 1;
            $subApplicationsAllMonth[$subApplication->agency_subdivision_id]['certificates_all_sum'] = isset($subApplicationsAllMonth[$subApplication->agency_subdivision_id]['certificates_all_sum']) ? $subApplicationsAllMonth[$subApplication->agency_subdivision_id]['certificates_all_sum'] + $subApplication['certificates_sum'] : $subApplication['certificates_sum'];
        }

        foreach ($subDivisions as $subDivisionId) {

            if (!isset($subApplicationsAllMonth[$subDivisionId])) {
                $subApplicationsAllMonth[$subDivisionId] = [
                    'certificates_quantity' => '-',
                    'certificates_sum' => '-',
                    'certificates_all_quantity' => 0,
                    'certificates_all_sum' => 0,
                ];
            }
        }

        return $subApplicationsAllMonth;
    }

    /**
     * Function to Report 6
     *
     * @param $data
     * @return array
     */
    private function TjSt_6($data)
    {
        $hsCode = $data['hs_code'];

        $refHsCodeId = $this->refHsDataIds($hsCode);

        $allReportData = [];

        if (count($refHsCodeId)) {

            // Get SubApplications
            $subAppStart = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['start_date_start'], $data['start_date_end']);
            $subAppEnd = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['end_date_start'], $data['end_date_end']);

            $subApplicationsStartCountriesApprovedQty = SingleApplicationSubApplicationProducts::selectRaw('SUM(saf_products_batch.approved_quantity::numeric) as approved_quantity ,product_code ,saf_products.measurement_unit,saf_products.saf_number')
                ->whereIn('subapplication_id', $subAppStart->pluck('id')->all())
                ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
                ->join('saf_products_batch', 'saf_products.id', '=', 'saf_products_batch.saf_product_id')
                ->whereIn('saf_products.product_code', $refHsCodeId)
//                ->where('saf_products.product_code', 'ILIKE', $hsCode . '%')
                ->groupBy('product_code', 'saf_products.measurement_unit', 'saf_products.saf_number')
                ->orderBy('product_code', 'asc')
                ->orderBy('saf_products.measurement_unit', 'desc')
                ->whereNotNull('saf_products_batch.approved_quantity')
                ->get();

            $subApplicationsEndCountriesApprovedQty = SingleApplicationSubApplicationProducts::selectRaw('SUM(saf_products_batch.approved_quantity::numeric) as approved_quantity ,product_code ,saf_products.measurement_unit,saf_products.saf_number')
                ->whereIn('subapplication_id', $subAppEnd->pluck('id')->all())
                ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
                ->join('saf_products_batch', 'saf_products.id', '=', 'saf_products_batch.saf_product_id')
                ->whereIn('saf_products.product_code', $refHsCodeId)
//                ->where('saf_products.product_code', 'ILIKE', $hsCode . '%')
                ->groupBy('product_code', 'saf_products.measurement_unit', 'saf_products.saf_number')
                ->orderBy('product_code', 'asc')
                ->orderBy('saf_products.measurement_unit', 'desc')
                ->whereNotNull('saf_products_batch.approved_quantity')
                ->get();

            $startHsCodes = $subApplicationsStartCountriesApprovedQty->pluck('product_code')->unique()->all();

            $endHsCodes = $subApplicationsEndCountriesApprovedQty->pluck('product_code')->unique()->all();

            $allSearchedHsCodes = array_unique(array_merge($startHsCodes, $endHsCodes));

            $startHsCodesData = [];

            foreach ($subApplicationsStartCountriesApprovedQty as $value) {
                $startHsCodesData[$value->product_code][$value->measurement_unit] = $value->approved_quantity;
            }

            $endHsCodesData = [];
            foreach ($subApplicationsEndCountriesApprovedQty as $value) {
                $endHsCodesData[$value->product_code][$value->measurement_unit] = $value->approved_quantity;
            }

            foreach ($allSearchedHsCodes as $hsCode) {

                $safProducts = SingleApplicationProducts::where('product_code', $hsCode)->pluck('saf_number')->unique()->all();

                $safInfo = SingleApplication::whereIn('regular_number', $safProducts)->where('regime', $data['regime'])->pluck('data')->unique()->all();

                $safTransportationCountries = [];

                if ($data['regime'] == SingleApplication::REGIME_IMPORT) {
                    foreach ($safInfo as $saf) {
                        $safTransportationCountries[] = $saf['saf']['transportation']['export_country'] ?? '';
                    }
                } elseif ($data['regime'] == SingleApplication::REGIME_EXPORT) {
                    foreach ($safInfo as $saf) {
                        $safTransportationCountries[] = $saf['saf']['transportation']['import_country'] ?? '';
                    }
                } else {
                    $safTransportationCountries[] = '';
                }

                $safTransportationCountries = array_unique($safTransportationCountries);
                $safTransportationCountriesList = [];

                foreach ($safTransportationCountries as $safTransportationCountry) {
                    if (!empty($safTransportationCountry)) {

                        $safTransportationCountriesList[] = optional(getReferenceRows(ReferenceTable::REFERENCE_COUNTRIES, (int)$safTransportationCountry))->name;
                    }
                }

                $safTransportationCountriesInfo = implode(',', $safTransportationCountriesList);

                if (!isset($allReportData[$hsCode])) {

                    if (isset($startHsCodesData[$hsCode])) {

                        $approvedQty_E = $endHsCodesData[$hsCode] ?? false;
                        foreach ($startHsCodesData[$hsCode] as $key => $approvedQty) {

                            $measurement = getReferenceRows(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $key);

                            $allReportData[$hsCode][$key] = [
                                'measurement_name' => $measurement->name ?? '-',
                                'approvedQty_D' => $approvedQty,
                                'approvedQty_E' => ($approvedQty_E && isset($approvedQty_E[$key])) ? $approvedQty_E[$key] : 0,
                                'countries_list' => $safTransportationCountriesInfo
                            ];
                        }

                    } elseif (isset($endHsCodesData[$hsCode])) {

                        $approvedQty_D = $startHsCodesData[$hsCode] ?? false;
                        foreach ($endHsCodesData[$hsCode] as $key => $approvedQty) {

                            $measurement = getReferenceRows(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $key);

                            $allReportData[$hsCode][$key] = [
                                'measurement_name' => $measurement->name ?? '-',
                                'approvedQty_D' => ($approvedQty_D && isset($approvedQty_D[$key])) ? $approvedQty_D[$key] : 0,
                                'approvedQty_E' => $approvedQty,
                                'countries_list' => $safTransportationCountriesInfo
                            ];
                        }

                    }

                }
            }
        }

        return $allReportData;
    }

    /**
     *  Function Report 7
     *
     * @param $data
     * @param array $allReportData
     * @return array
     */
    private function TjSt_7($data, $allReportData = [])
    {
        $refHsCodeId = $this->refHsDataIds($data['hs_code']);

        if (count($refHsCodeId)) {
            $subAppStart = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['start_date_start'], $data['start_date_end']);

            foreach ($subAppStart as $key => $subApp) {
                $typeValue = (isset($subApp->data)) ? $subApp->data['saf']['sides'][$data['regime']]['type_value'] : '';

                $country = ReferenceTable::REFERENCE_COUNTRIES;
                $countryMl = ReferenceTable::REFERENCE_COUNTRIES . "_ml";
                $hsCode6 = ReferenceTable::REFERENCE_HS_CODE_6;
                $hsCode6Ml = ReferenceTable::REFERENCE_HS_CODE_6 . "_ml";

                $products = SingleApplicationSubApplicationProducts::selectRaw("SUM(saf_products_batch.approved_quantity::numeric) / 1000 as approved_quantity, {$hsCode6Ml}.name as hs_code_name, {$countryMl}.name")
                    ->join('saf_products', 'saf_products.id', '=', 'saf_sub_application_products.product_id')
                    ->join('saf_products_batch', 'saf_products.id', '=', 'saf_products_batch.saf_product_id')
                    ->join('saf_sub_application_states', function ($join) {
                        $join->on('saf_sub_application_states.saf_subapplication_id', '=', 'saf_sub_application_products.subapplication_id')->where('is_current', '1');
                    })
                    ->join('constructor_states', function ($join) {
                        $join->on('constructor_states.id', '=', 'saf_sub_application_states.state_id')->where('state_approved_status', true);
                    })
                    ->join($hsCode6, DB::raw("{$hsCode6}.id::varchar"), '=', 'saf_products.product_code')
                    ->join($hsCode6Ml, function ($join) use ($hsCode6, $hsCode6Ml) {
                        $join->on("{$hsCode6}.id", '=', "{$hsCode6Ml}.{$hsCode6}_id")->where("{$hsCode6Ml}.lng_id", cLng('id'));
                    })
                    ->join($country, DB::raw("{$country}.id::varchar"), '=', 'saf_products.producer_country')
                    ->join($countryMl, function ($join) use ($country, $countryMl) {
                        $join->on("{$countryMl}.{$country}_id", '=', "{$country}.id")->where("{$countryMl}.lng_id", cLng('id'));
                    })
                    ->where('subapplication_id', $subApp->id)
                    ->whereIn('saf_products.product_code', $refHsCodeId)
                    ->where('saf_sub_application_products.created_at', '>=', $data['start_date_start'])
                    ->where('saf_sub_application_products.created_at', '<=', $data['start_date_end'])
                    ->where('saf_products_batch.approved_quantity', '>', 0)
                    ->groupBy("{$hsCode6Ml}.name", "{$countryMl}.name")
                    ->get()->toArray();

                $allReportData[] = [
                    'incrementNumber' => ++$key,
                    'enterpriseNames' => (isset($subApp->data)) ? $subApp->data['saf']['sides'][$data['regime']]['name'] : '',
                    'certificateCount' => $subApp->getEndApprovedCertificatesWithDuringDate($data['regime'], $typeValue, $data['start_date_start'], $data['start_date_end'], 'count'),
                    'products' => $products,
                ];
            }

            $totalCertificateCount = $totalWeight = 0;


            foreach ($allReportData as $item) {
                $totalCertificateCount += $item['certificateCount'];
                foreach ($item['products'] as $product) {
                    $totalWeight += $product['approved_quantity'];
                }
            }

            $allReportData['total'] = [
                'certificateCount' => $totalCertificateCount,
                'approved_quantity' => $totalWeight
            ];
        }

        return $allReportData;

    }

    /**
     * Function to Report 8
     *
     * @param $data
     * @return array
     */
    private function TjSt_8($data)
    {
        $hsCode = $data['hs_code'];

        $refHsCodeId = $this->refHsDataIds($hsCode);

        $allReportData = [];

        if (count($refHsCodeId)) {

            // Get SubApplications
            $subAppStart = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['start_date_start'], $data['start_date_end']);
            $subAppEnd = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['end_date_start'], $data['end_date_end']);

            $subApplicationsStartCountriesApprovedQty = SingleApplicationSubApplicationProducts::selectRaw('SUM(saf_products_batch.approved_quantity::numeric) as approved_quantity ,product_code ,saf_products.measurement_unit')
                ->whereIn('subapplication_id', $subAppStart->pluck('id')->all())
                ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
                ->join('saf_products_batch', 'saf_products.id', '=', 'saf_products_batch.saf_product_id')
                ->whereIn('saf_products.product_code', $refHsCodeId)
//                ->where('saf_products.product_code', 'ILIKE', $hsCode . '%')
                ->groupBy('product_code', 'saf_products.measurement_unit')
                ->orderBy('product_code', 'asc')
                ->orderBy('saf_products.measurement_unit', 'desc')
                ->whereNotNull('saf_products_batch.approved_quantity')
                ->get();


            $subApplicationsEndCountriesApprovedQty = SingleApplicationSubApplicationProducts::selectRaw('SUM(saf_products_batch.approved_quantity::numeric) as approved_quantity ,product_code ,saf_products.measurement_unit')
                ->whereIn('subapplication_id', $subAppEnd->pluck('id')->all())
                ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
                ->join('saf_products_batch', 'saf_products.id', '=', 'saf_products_batch.saf_product_id')
                ->whereIn('saf_products.product_code', $refHsCodeId)
//                ->where('saf_products.product_code', 'ILIKE', $hsCode . '%')
                ->groupBy('product_code', 'saf_products.measurement_unit')
                ->orderBy('product_code', 'asc')
                ->orderBy('saf_products.measurement_unit', 'desc')
                ->whereNotNull('saf_products_batch.approved_quantity')
                ->get();

            $startHsCodes = $subApplicationsStartCountriesApprovedQty->pluck('product_code')->unique()->all();
            $endHsCodes = $subApplicationsEndCountriesApprovedQty->pluck('product_code')->unique()->all();

            $allSearchedHsCodes = array_unique(array_merge($startHsCodes, $endHsCodes));

            $startHsCodesData = [];
            foreach ($subApplicationsStartCountriesApprovedQty as $value) {
                $startHsCodesData[$value->product_code][$value->measurement_unit] = $value->approved_quantity;
            }

            $endHsCodesData = [];
            foreach ($subApplicationsEndCountriesApprovedQty as $value) {
                $endHsCodesData[$value->product_code][$value->measurement_unit] = $value->approved_quantity;
            }


            foreach ($allSearchedHsCodes as $hsCode) {
                if (!isset($allReportData[$hsCode])) {

                    if (isset($startHsCodesData[$hsCode])) {

                        $approvedQty_D = $endHsCodesData[$hsCode] ?? false;
                        foreach ($startHsCodesData[$hsCode] as $key => $approvedQty) {

                            $measurement = getReferenceRows(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $key);

                            $allReportData[$hsCode][$key] = [
                                'measurement_name' => optional($measurement)->name ?? '-',
                                'approvedQty_C' => $approvedQty,
                                'approvedQty_D' => ($approvedQty_D && isset($approvedQty_D[$key])) ? $approvedQty_D[$key] : 0
                            ];
                        }

                    } elseif (isset($endHsCodesData[$hsCode])) {

                        $approvedQty_C = $startHsCodesData[$hsCode] ?? false;
                        foreach ($endHsCodesData[$hsCode] as $key => $approvedQty) {

                            $measurement = getReferenceRows(ReferenceTable::REFERENCE_UNIT_OF_MEASUREMENT, $key);

                            $allReportData[$hsCode][$key] = [
                                'measurement_name' => optional($measurement)->name ?? '-',
                                'approvedQty_C' => ($approvedQty_C && isset($approvedQty_C[$key])) ? $approvedQty_C[$key] : 0,
                                'approvedQty_D' => $approvedQty
                            ];
                        }
                    }
                }
            }
        }

        return $allReportData;
    }

    /**
     * Function Report 9
     *
     * @param $data
     * @return array
     */
    private function TjSt_9($data)
    {
        $hsCode = $data['hs_code'];

        $refHsCodeId = $this->refHsDataIds($hsCode);

        $allReportData = $allReportDataTotals = [];

        if (count($refHsCodeId)) {

            // Get SubApplications
            $subAppStart = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['start_date_start'], $data['start_date_end']);
            $subAppEnd = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['end_date_start'], $data['end_date_end']);

            $subApplicationsStartApprovedQty = SingleApplicationSubApplicationProducts::
            selectRaw('SUM(saf_products_batch.approved_quantity::numeric) as approved_quantity ,product_code ,saf_products.measurement_unit')
                ->whereIn('subapplication_id', $subAppStart->pluck('id')->all())
                ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
                ->join('saf_products_batch', 'saf_products.id', '=', 'saf_products_batch.saf_product_id')
                ->whereIn('saf_products.product_code', $refHsCodeId)
                ->groupBy('saf_products.measurement_unit', 'product_code')
                ->orderBy('product_code', 'asc')
                ->orderBy('saf_products.measurement_unit', 'desc')
                ->whereNotNull('saf_products_batch.approved_quantity')
                ->get();

            $subApplicationsEndApprovedQty = SingleApplicationSubApplicationProducts::
            selectRaw('SUM(saf_products_batch.approved_quantity::numeric) as approved_quantity ,product_code ,saf_products.measurement_unit')
                ->whereIn('subapplication_id', $subAppEnd->pluck('id')->all())
                ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
                ->join('saf_products_batch', 'saf_products.id', '=', 'saf_products_batch.saf_product_id')
                ->whereIn('saf_products.product_code', $refHsCodeId)
                ->groupBy('saf_products.measurement_unit', 'product_code')
                ->orderBy('product_code', 'asc')
                ->orderBy('saf_products.measurement_unit', 'desc')
                ->whereNotNull('saf_products_batch.approved_quantity')
                ->get();

            if (count($subApplicationsStartApprovedQty) > 0) {
                foreach ($subApplicationsStartApprovedQty as $item) {

                    if (isset($allReportData[$item['measurement_unit']][$item['product_code']][$item['producer_country']])) {
                        $approvedQuantity = $allReportData[$item['measurement_unit']][$item['product_code']]['approved_quantity'];

                        $allReportData[$item['measurement_unit']][$item['product_code']] = [
                            'measurement_unit' => $item['measurement_unit'],
                            'approved_quantity' => float($approvedQuantity + $item['approved_quantity']),
                            'approved_quantity_2' => '-'
                        ];
                    } else {
                        $allReportData[$item['measurement_unit']][$item['product_code']] = [
                            'measurement_unit' => $item['measurement_unit'],
                            'approved_quantity' => (float)$item['approved_quantity'],
                            'approved_quantity_2' => '-'
                        ];
                    }
                }
            }
            if (count($subApplicationsEndApprovedQty) > 0) {
                foreach ($subApplicationsEndApprovedQty as $item) {

                    if (isset($allReportData[$item['measurement_unit']][$item['product_code']])) {
                        $approvedQuantity2 = (float)$allReportData[$item['measurement_unit']][$item['product_code']]['approved_quantity_2'];
                        $allReportData[$item['measurement_unit']][$item['product_code']]['approved_quantity_2'] = $approvedQuantity2 + $item['approved_quantity'];
                    } else {

                        $allReportData[$item['measurement_unit']][$item['product_code']] = [
                            'measurement_unit' => '-',
                            'approved_quantity' => '-',
                            'approved_quantity_2' => 0
                        ];
                        $allReportData[$item['measurement_unit']][$item['product_code']]['approved_quantity_2'] += $item['approved_quantity'];
                    }
                }
            }
        }

        return $allReportData;

    }

    /**
     * Function to  Report 10
     *
     * @param $data
     * @return array
     */
    private function TjSt_10($data)
    {
        $hsCode = $data['hs_code'];

        $refHsCodeId = $this->refHsDataIds($hsCode);

        $allReportData = [];

        $country = ReferenceTable::REFERENCE_COUNTRIES;
        $countryMl = ReferenceTable::REFERENCE_COUNTRIES . "_ml";

        if (count($refHsCodeId)) {

            // Get SubApplications
            $subAppStart = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['start_date_start'], $data['start_date_end']);
            $subAppEnd = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['end_date_start'], $data['end_date_end']);

            $subApplicationsStartCountriesApprovedQty = SingleApplicationSubApplicationProducts::selectRaw(
                "SUM(saf_products_batch.approved_quantity::numeric) as approved_quantity ,
                product_code ,
                saf_products.measurement_unit, 
                {$countryMl}.name as producer_country")
                ->whereIn('subapplication_id', $subAppStart->pluck('id')->all())
                ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
                ->join('saf_products_batch', 'saf_products.id', '=', 'saf_products_batch.saf_product_id')
                ->join($country, DB::raw("{$country}.id::varchar"), '=', 'saf_products.producer_country')
                ->join($countryMl, function ($join) use ($country, $countryMl) {
                    $join->on("{$countryMl}.{$country}_id", '=', "{$country}.id")->where("{$countryMl}.lng_id", cLng('id'));
                })
                ->whereIn('saf_products.product_code', $refHsCodeId)
//                ->where('saf_products.product_code', 'ILIKE', $hsCode . '%')
                ->groupBy('product_code', 'saf_products.measurement_unit', "{$countryMl}.name")
                ->orderBy('product_code', 'asc')
                ->orderBy('saf_products.measurement_unit', 'desc')
                ->whereNotNull('saf_products_batch.approved_quantity')
                ->get()->toArray();

            $subApplicationsEndCountriesApprovedQty = SingleApplicationSubApplicationProducts::selectRaw("SUM(saf_products_batch.approved_quantity::numeric) as approved_quantity ,product_code ,saf_products.measurement_unit,{$countryMl}.name  as producer_country")
                ->whereIn('subapplication_id', $subAppEnd->pluck('id')->all())
                ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
                ->join('saf_products_batch', 'saf_products.id', '=', 'saf_products_batch.saf_product_id')
                ->join($country, DB::raw("{$country}.id::varchar"), '=', 'saf_products.producer_country')
                ->join($countryMl, function ($join) use ($country, $countryMl) {
                    $join->on("{$countryMl}.{$country}_id", '=', "{$country}.id")->where("{$countryMl}.lng_id", cLng('id'));
                })
                ->whereIn('saf_products.product_code', $refHsCodeId)
//                ->where('saf_products.product_code', 'ILIKE', $hsCode . '%')
                ->groupBy('product_code', 'saf_products.measurement_unit', "{$countryMl}.name")
                ->orderBy('product_code', 'asc')
                ->orderBy('saf_products.measurement_unit', 'desc')
                ->whereNotNull('saf_products_batch.approved_quantity')
                ->get()->toArray();

            if (count($subApplicationsStartCountriesApprovedQty) > 0) {
                foreach ($subApplicationsStartCountriesApprovedQty as $item) {

                    if (isset($allReportData[$item['product_code']][$item['measurement_unit']][$item['producer_country']])) {
                        $approvedQuantity = $allReportData[$item['product_code']][$item['measurement_unit']][$item['producer_country']]['approved_quantity'];

                        $allReportData[$item['product_code']][$item['measurement_unit']][$item['producer_country']] = [
                            'measurement_unit' => $item['measurement_unit'],
                            'approved_quantity' => $approvedQuantity + $item['approved_quantity'],
                            'approved_quantity_2' => '-'
                        ];
                    } else {
                        $allReportData[$item['product_code']][$item['measurement_unit']][$item['producer_country']] = [
                            'measurement_unit' => $item['measurement_unit'],
                            'approved_quantity' => $item['approved_quantity'],
                            'approved_quantity_2' => '-'
                        ];
                    }
                }
            }
            if (count($subApplicationsEndCountriesApprovedQty) > 0) {
                foreach ($subApplicationsEndCountriesApprovedQty as $item) {

                    if (isset($allReportData[$item['product_code']][$item['measurement_unit']][$item['producer_country']])) {
                        $approvedQuantity2 = (float)$allReportData[$item['product_code']][$item['measurement_unit']][$item['producer_country']]['approved_quantity_2'];
                        $allReportData[$item['product_code']][$item['measurement_unit']][$item['producer_country']]['approved_quantity_2'] = $approvedQuantity2 + $item['approved_quantity'];
                    } else {

                        $allReportData[$item['product_code']][$item['measurement_unit']][$item['producer_country']] = [
                            'measurement_unit' => '-',
                            'approved_quantity' => '-',
                            'approved_quantity_2' => 0
                        ];
                        $allReportData[$item['product_code']][$item['measurement_unit']][$item['producer_country']]['approved_quantity_2'] += $item['approved_quantity'];
                    }
                }
            }
        }

        return $allReportData;
    }

    /**
     * Function to generate Report 11
     *
     * @param $data
     * @return array
     */
    private function TjSt_11($data)
    {
        $hsCode = $data['hs_code'];

        $refHsCodeId = $this->refHsDataIds($hsCode);

        $allReportData = $allReportDataTotals = [];

        if (count($refHsCodeId)) {

            // Get SubApplications
            $subAppStart = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['start_date_start'], $data['start_date_end']);
            $subAppEnd = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['end_date_start'], $data['end_date_end']);

            $subApplicationsFirstPeriod = SingleApplicationSubApplicationProducts::selectRaw('SUM(saf_products_batch.approved_quantity::numeric) as approved_quantity,producer_country ,product_code ,saf_products.measurement_unit')
                ->whereIn('subapplication_id', $subAppStart->pluck('id')->all())
                ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
                ->join('saf_products_batch', 'saf_products.id', '=', 'saf_products_batch.saf_product_id')
                ->whereIn('saf_products.product_code', $refHsCodeId)
                ->groupBy('product_code', 'saf_products.measurement_unit', 'saf_products.producer_country')
                ->orderBy('product_code', 'asc')
                ->orderBy('saf_products.measurement_unit', 'desc')
                ->whereNotNull('saf_products_batch.approved_quantity')
                ->get();


            $subApplicationsSecondPeriod = SingleApplicationSubApplicationProducts::selectRaw('SUM(saf_products_batch.approved_quantity::numeric) as approved_quantity,producer_country ,product_code ,saf_products.measurement_unit')
                ->whereIn('subapplication_id', $subAppEnd->pluck('id')->all())
                ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
                ->join('saf_products_batch', 'saf_products.id', '=', 'saf_products_batch.saf_product_id')
                ->whereIn('saf_products.product_code', $refHsCodeId)
                ->groupBy('product_code', 'saf_products.measurement_unit', 'saf_products.producer_country')
                ->orderBy('product_code', 'asc')
                ->orderBy('saf_products.measurement_unit', 'desc')
                ->whereNotNull('saf_products_batch.approved_quantity')
                ->get();

            $startHsCodes = $subApplicationsFirstPeriod->pluck('product_code')->unique()->all();
            $endHsCodes = $subApplicationsSecondPeriod->pluck('product_code')->unique()->all();

            $allSearchedHsCodes = array_unique(array_merge($startHsCodes, $endHsCodes));

            $startHsCodesData = [];
            foreach ($subApplicationsFirstPeriod as $value) {
                $startHsCodesData[$value->product_code][$value->measurement_unit][$value->producer_country] = $value->approved_quantity;
            }

            $endHsCodesData = [];
            foreach ($subApplicationsSecondPeriod as $value) {
                $endHsCodesData[$value->product_code][$value->measurement_unit][$value->producer_country] = $value->approved_quantity;
            }


            foreach ($allSearchedHsCodes as $hsCode) {

                if (!isset($allReportData[$hsCode])) {

                    if (isset($startHsCodesData[$hsCode])) {

                        foreach ($startHsCodesData[$hsCode] as $productType => $values) {

                            $approvedQty_D2 = isset($endHsCodesData[$hsCode]) && isset($endHsCodesData[$hsCode][$productType]) ? $endHsCodesData[$hsCode][$productType] : false;

                            foreach ($values as $countryId => $approvedVal) {

                                $approve_D2 = ($approvedQty_D2 && isset($approvedQty_D2[$countryId])) ? $approvedQty_D2[$countryId] : 0;

                                $allReportData[$hsCode][$productType][$countryId] = [
                                    'approvedQty_D1' => $approvedVal,
                                    'approvedQty_D2' => $approve_D2
                                ];

                                $allReportDataTotals[$hsCode][$productType]['total_D1'] = isset($allReportDataTotals[$hsCode][$productType]['total_D1']) ? $allReportDataTotals[$hsCode][$productType]['total_D1'] + $approvedVal : $approvedVal;
                                $allReportDataTotals[$hsCode][$productType]['total_D2'] = isset($allReportDataTotals[$hsCode][$productType]['total_D2']) ? $allReportDataTotals[$hsCode][$productType]['total_D2'] + $approve_D2 : $approve_D2;
                            }
                        }

                    } elseif (isset($endHsCodesData[$hsCode])) {

                        foreach ($endHsCodesData[$hsCode] as $productType => $values) {

                            $approvedQty_D1 = isset($startHsCodesData[$hsCode]) && isset($startHsCodesData[$hsCode][$productType]) ? $startHsCodesData[$hsCode][$productType] : false;

                            foreach ($values as $countryId => $approvedVal) {

                                $approve_D1 = ($approvedQty_D1 && isset($approvedQty_D1[$countryId])) ? $approvedQty_D1[$countryId] : 0;

                                $allReportData[$hsCode][$productType][$countryId] = [
                                    'approvedQty_D2' => $approvedVal,
                                    'approvedQty_D1' => $approve_D1
                                ];

                                $allReportDataTotals[$hsCode][$productType]['total_D1'] = isset($allReportDataTotals[$hsCode][$productType]['total_D1']) ? $allReportDataTotals[$hsCode][$productType]['total_D1'] + $approve_D1 : $approve_D1;
                                $allReportDataTotals[$hsCode][$productType]['total_D2'] = isset($allReportDataTotals[$hsCode][$productType]['total_D2']) ? $allReportDataTotals[$hsCode][$productType]['total_D2'] + $approvedVal : $approvedVal;

                            }
                        }
                    }
                }
            }
        }

        return [
            'data' => $allReportData,
            'totals' => $allReportDataTotals,
        ];
    }

    /**
     * Function to generate Report 12
     *
     * @param $data
     * @return array
     */
    private function TjSt_12($data)
    {
        $year_1 = $data['year_1'];
        $year_2 = $data['year_2'];
        $month = $data['month'];

        $documentStatus = $data['document_status'];
        $documentId = $data['document_id'];

        $startDate_1 = $year_1 . '-' . $month . '-01';
        $endDate_1 = $year_1 . '-' . $month . '-31';

        $startDate_2 = $year_2 . '-' . $month . '-01';
        $endDate_2 = $year_2 . '-' . $month . '-31';

        $prevM = Carbon::parse($startDate_1);

        $previous_month_year = $year_2;
        $previous_month = date('m', strtotime($prevM->subMonth()));

        if ($month == '01') {
            $previous_month_year = $year_2 - 1;
            $previous_month = "12";
        }

        $startDate_2_previous_month = $previous_month_year . '-' . $previous_month . '-01';
        $endDate_2_previous_month = $previous_month_year . '-' . $previous_month . '-31';

        // YEAR 1
        $subApplicationsYear_1 = $this->getSubApplicationsWithObligations($documentStatus, $documentId, $startDate_1, $endDate_1);

        // Year 2
        $subApplicationsYear_2 = $this->getSubApplicationsWithObligations($documentStatus, $documentId, $startDate_2, $endDate_2);

        // Previous Month
        $subApplicationsYear_2_previousMonth = $this->getSubApplicationsWithObligations($documentStatus, $documentId, $startDate_2_previous_month, $endDate_2_previous_month);


        // SUM && COUNT
        $reportYear_1 = [
            SingleApplication::REGIME_IMPORT => [
                'certificates_sum' => $subApplicationsYear_1->where('regime', SingleApplication::REGIME_IMPORT)->sum('certificates_sum'),
                'certificates_count' => $subApplicationsYear_1->where('regime', SingleApplication::REGIME_IMPORT)->sum('certificates_count'),
            ],
            SingleApplication::REGIME_EXPORT => [
                'certificates_sum' => $subApplicationsYear_1->where('regime', SingleApplication::REGIME_EXPORT)->sum('certificates_sum'),
                'certificates_count' => $subApplicationsYear_1->where('regime', SingleApplication::REGIME_EXPORT)->sum('certificates_count'),
            ],
            'certificates_all_sum' => $subApplicationsYear_1->sum('certificates_sum'),
            'certificates_all_count' => $subApplicationsYear_1->sum('certificates_count')
        ];

        $reportYear_2 = [
            SingleApplication::REGIME_IMPORT => [
                'certificates_sum' => $subApplicationsYear_2->where('regime', SingleApplication::REGIME_IMPORT)->sum('certificates_sum'),
                'certificates_count' => $subApplicationsYear_2->where('regime', SingleApplication::REGIME_IMPORT)->sum('certificates_count'),
            ],
            SingleApplication::REGIME_EXPORT => [
                'certificates_sum' => $subApplicationsYear_2->where('regime', SingleApplication::REGIME_EXPORT)->sum('certificates_sum'),
                'certificates_count' => $subApplicationsYear_2->where('regime', SingleApplication::REGIME_EXPORT)->sum('certificates_count'),
            ],
            'certificates_all_sum' => $subApplicationsYear_2->sum('certificates_sum'),
            'certificates_all_count' => $subApplicationsYear_2->sum('certificates_count'),
        ];

        // Year 2 previous month info
        $reportPreviousMonthYear_2 = [
            SingleApplication::REGIME_IMPORT => [
                'certificates_sum' => $subApplicationsYear_2_previousMonth->where('regime', SingleApplication::REGIME_IMPORT)->sum('certificates_sum'),
                'certificates_count' => $subApplicationsYear_2_previousMonth->where('regime', SingleApplication::REGIME_IMPORT)->sum('certificates_count'),
            ],
            SingleApplication::REGIME_EXPORT => [
                'certificates_sum' => $subApplicationsYear_2_previousMonth->where('regime', SingleApplication::REGIME_EXPORT)->sum('certificates_sum'),
                'certificates_count' => $subApplicationsYear_2_previousMonth->where('regime', SingleApplication::REGIME_EXPORT)->sum('certificates_count'),
            ],
            'certificates_all_sum' => $subApplicationsYear_2_previousMonth->sum('certificates_sum'),
            'certificates_all_count' => $subApplicationsYear_2_previousMonth->sum('certificates_count'),
            'month' => $previous_month,
            'year' => $previous_month_year,
        ];

        return [
            'year_1' => $reportYear_1,
            'year_2' => $reportYear_2,
            'year_2_previous_month' => $reportPreviousMonthYear_2
        ];
    }

    /**
     * Function to generate Report 13
     *
     * @param $data
     * @return array
     */
    private function TjSt_13($data)
    {
        $documentStatus = $data['document_status'];

        $startDate = $data['start_date_start'];
        $startDateEnd = $data['start_date_end'];

        $endDate = $data['end_date_start'];
        $endDateEnd = $data['end_date_end'];

        $subApplicationsFirstPeriod = $this->getSubApplicationsWithObligations($documentStatus, $data['document_id'], $startDate, $startDateEnd);

        $subApplicationsSecondPeriod = $this->getSubApplicationsWithObligations($documentStatus, $data['document_id'], $endDate, $endDateEnd);

        $agencyActiveSubdivisions = $this->getAllSubdivisions();

        $reportsFirstPeriod = [];
        foreach ($subApplicationsFirstPeriod as $subApplication) {
            $reportsFirstPeriod[$subApplication->agency_subdivision_id][$subApplication->regime] = [
                'certificates_sum' => $subApplication->certificates_sum,
                'certificates_count' => $subApplication->certificates_count,
            ];
        }

        $reportsSecondPeriod = [];
        foreach ($subApplicationsSecondPeriod as $subApplication) {
            $reportsSecondPeriod[$subApplication->agency_subdivision_id][$subApplication->regime] = [
                'certificates_sum' => $subApplication->certificates_sum,
                'certificates_count' => $subApplication->certificates_count,
            ];
        }


        $reportsFirstPeriodSubDivisions = $subApplicationsFirstPeriod->pluck('agency_subdivision_id')->all();
        $reportsSecondPeriodSubDivisions = $subApplicationsSecondPeriod->pluck('agency_subdivision_id')->all();

        $reportsAllSubDivisions = array_unique(array_merge($reportsFirstPeriodSubDivisions, $reportsSecondPeriodSubDivisions));

        return [
            'firstPeriod' => $reportsFirstPeriod,
            'secondPeriod' => $reportsSecondPeriod,
            'allSubDivisions' => $reportsAllSubDivisions,
            'allActiveSubDivisions' => $agencyActiveSubdivisions
        ];
    }

    /**
     * Function to generate Report 14
     *
     * @param $data
     * @return array
     */
    private function TjSt_14($data)
    {
        $hsCode = $data['hs_code'];

        $refHsCodeId = $this->refHsDataIds($hsCode);

        $allReportData = [];

        $country = ReferenceTable::REFERENCE_COUNTRIES;
        $countryMl = ReferenceTable::REFERENCE_COUNTRIES . "_ml";

        if (count($refHsCodeId)) {

            // Get SubApplications
            $subAppStart = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['start_date_start'], $data['start_date_end']);
            $subAppEnd = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['end_date_start'], $data['end_date_end']);

            $subApplicationsStartCountriesApprovedQty = SingleApplicationSubApplicationProducts::selectRaw(
                "SUM(saf_products_batch.approved_quantity::numeric) as approved_quantity ,
                product_code ,
                saf_products.measurement_unit, 
                {$countryMl}.name as producer_country")
                ->whereIn('subapplication_id', $subAppStart->pluck('id')->all())
                ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
                ->join('saf_products_batch', 'saf_products.id', '=', 'saf_products_batch.saf_product_id')
                ->join($country, DB::raw("{$country}.id::varchar"), '=', 'saf_products.producer_country')
                ->join($countryMl, function ($join) use ($country, $countryMl) {
                    $join->on("{$countryMl}.{$country}_id", '=', "{$country}.id")->where("{$countryMl}.lng_id", cLng('id'));
                })
                ->whereIn('saf_products.product_code', $refHsCodeId)
//                ->where('saf_products.product_code', 'ILIKE', $hsCode . '%')
                ->groupBy('product_code', 'saf_products.measurement_unit', "{$countryMl}.name")
                ->orderBy('product_code', 'asc')
                ->orderBy('saf_products.measurement_unit', 'desc')
                ->whereNotNull('saf_products_batch.approved_quantity')
                ->get()->toArray();

            $subApplicationsEndCountriesApprovedQty = SingleApplicationSubApplicationProducts::selectRaw("SUM(saf_products_batch.approved_quantity::numeric) as approved_quantity ,product_code ,saf_products.measurement_unit,{$countryMl}.name  as producer_country")
                ->whereIn('subapplication_id', $subAppEnd->pluck('id')->all())
                ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
                ->join('saf_products_batch', 'saf_products.id', '=', 'saf_products_batch.saf_product_id')
                ->join($country, DB::raw("{$country}.id::varchar"), '=', 'saf_products.producer_country')
                ->join($countryMl, function ($join) use ($country, $countryMl) {
                    $join->on("{$countryMl}.{$country}_id", '=', "{$country}.id")->where("{$countryMl}.lng_id", cLng('id'));
                })
                ->whereIn('saf_products.product_code', $refHsCodeId)
//                ->where('saf_products.product_code', 'ILIKE', $hsCode . '%')
                ->groupBy('product_code', 'saf_products.measurement_unit', "{$countryMl}.name")
                ->orderBy('product_code', 'asc')
                ->orderBy('saf_products.measurement_unit', 'desc')
                ->whereNotNull('saf_products_batch.approved_quantity')
                ->get()->toArray();

            if (count($subApplicationsStartCountriesApprovedQty) > 0) {
                foreach ($subApplicationsStartCountriesApprovedQty as $item) {

                    if (isset($allReportData[$item['product_code']][$item['measurement_unit']][$item['producer_country']])) {
                        $approvedQuantity = $allReportData[$item['product_code']][$item['measurement_unit']][$item['producer_country']]['approved_quantity'];

                        $allReportData[$item['product_code']][$item['measurement_unit']][$item['producer_country']] = [
                            'measurement_unit' => $item['measurement_unit'],
                            'approved_quantity' => $approvedQuantity + $item['approved_quantity'],
                            'approved_quantity_2' => '-'
                        ];
                    } else {
                        $allReportData[$item['product_code']][$item['measurement_unit']][$item['producer_country']] = [
                            'measurement_unit' => $item['measurement_unit'],
                            'approved_quantity' => $item['approved_quantity'],
                            'approved_quantity_2' => '-'
                        ];
                    }
                }
            }
            if (count($subApplicationsEndCountriesApprovedQty) > 0) {
                foreach ($subApplicationsEndCountriesApprovedQty as $item) {

                    if (isset($allReportData[$item['product_code']][$item['measurement_unit']][$item['producer_country']])) {
                        $approvedQuantity2 = (float)$allReportData[$item['product_code']][$item['measurement_unit']][$item['producer_country']]['approved_quantity_2'];
                        $allReportData[$item['product_code']][$item['measurement_unit']][$item['producer_country']]['approved_quantity_2'] = $approvedQuantity2 + $item['approved_quantity'];
                    } else {

                        $allReportData[$item['product_code']][$item['measurement_unit']][$item['producer_country']] = [
                            'measurement_unit' => '-',
                            'approved_quantity' => '-',
                            'approved_quantity_2' => 0
                        ];
                        $allReportData[$item['product_code']][$item['measurement_unit']][$item['producer_country']]['approved_quantity_2'] += $item['approved_quantity'];
                    }
                }
            }


        }

        return $allReportData;

    }

    /**
     * Function to generate Report 16
     *
     * @param $data
     * @return array
     */
    private function TjSt_16($data)
    {
        $documentStatus = $data['document_status'];
        $documentId = $data['document_id'];

        $startDate = $data['start_date_start'];
        $endDate = $data['start_date_end'];

        $subApplications = $this->getSubApplicationsWithObligations($documentStatus, $documentId, $startDate, $endDate);

        $reportAllData = [];
        $existSubApplications = [];
        foreach ($subApplications as $subApplication) {
            $existSubApplications[] = $subApplication->agency_subdivision_id;
            $reportAllData[$subApplication->agency_subdivision_id][$subApplication->regime] = [
                'certificates_sum' => $subApplication->certificates_sum,
                'certificates_count' => $subApplication->certificates_count,
            ];
        }

        $allSubDivisions = $this->getAllSubdivisions();

        foreach ($allSubDivisions as $subDivisionId) {

            if (!in_array($subDivisionId, $existSubApplications)) {
                $reportAllData[$subDivisionId] = [
                    'import' => [
                        'certificates_sum' => 0,
                        'certificates_count' => 0
                    ],

                    'export' => [
                        'certificates_sum' => 0,
                        'certificates_count' => 0
                    ],

                ];
            }
        }

        return $reportAllData;
    }

    /**
     * Function to generate TjST Report 17
     *
     * @param $data
     * @return array
     */
    private function TjSt_17($data)
    {
        $allReportData = [];

        $hsCode = $data['hs_code'];

        $refHsCodeId = $this->refHsDataIds($hsCode);

        if (count($refHsCodeId)) {

            // Get SubApplications
            $subAppStart = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['start_date_start'], $data['start_date_end']);

            $subApplicationsData = SingleApplicationSubApplicationProducts::selectRaw(
                "product_code,
                saf_sub_application_products.saf_number")
                ->whereIn('subapplication_id', $subAppStart->pluck('id')->all())
                ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
                ->join('saf_products_batch', 'saf_products.id', '=', 'saf_products_batch.saf_product_id')
                ->leftJoin('saf', 'saf.regular_number', '=', 'saf_sub_application_products.saf_number')
                ->whereIn('saf_products.product_code', $refHsCodeId)
                ->groupBy('product_code', 'saf_sub_application_products.saf_number')
                ->orderBy('product_code', 'asc')
                ->get()->toArray();

            if (count($subApplicationsData) > 0) {
                foreach ($subApplicationsData as $item) {

                    $saf = SingleApplication::where('regular_number', $item['saf_number'])->pluck('data')->first();

                    if ($data['regime'] == SingleApplication::REGIME_IMPORT) {
                        $safSideInfo = $saf['saf']['sides']['import']['name'];
                    } elseif ($data['regime'] == SingleApplication::REGIME_EXPORT) {
                        $safSideInfo = $saf['saf']['sides']['export']['name'];
                    } else {
                        $safSideInfo = '';
                    }

                    $allReportData[$item['product_code']]['columnB'] = $safSideInfo;
                }
            }
        }

        return $allReportData;
    }

    /**
     * Function to generate TjST Report 18
     *
     * @param $data
     * @return array
     */
    private function TjSt_18($data)
    {
        $allReportData = $subAppStart = $typeValue = [];

        $hsCode = $data['hs_code'];

        $hsCodeReferenceTableName = ReferenceTable::REFERENCE_HS_CODE_6;

        $refHsCodeId = DB::table($hsCodeReferenceTableName)->where('code', 'ILIKE', '%' . $hsCode . '%')->pluck('id')->all();

        if (count($refHsCodeId)) {

            // Get SubApplications
            foreach ($data['document_status'] as $documentStatus) {
                $subAppStart[] = $this->getSubApplicationIds($documentStatus, $data['document_id'], $data['regime'], $data['start_date_start'], $data['start_date_end']);
            }

            if (count($subAppStart) > 0) {

                foreach ($subAppStart as $items) {

                    $country = ReferenceTable::REFERENCE_COUNTRIES;
                    $countryMl = ReferenceTable::REFERENCE_COUNTRIES . "_ml";

                    $subApplicationsProducts = SingleApplicationSubApplicationProducts::select('saf_products_batch.id as saf_product_id', 'product_code', 'commercial_description', '' . $countryMl . '.name as producer_country')
                        ->whereIn('saf_sub_application_products.subapplication_id', $items->pluck('id')->all())
                        ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
                        ->join('saf_products_batch', 'saf_products.id', '=', 'saf_products_batch.saf_product_id')
                        ->join($country, (DB::raw("{$country}.id::varchar")), '=', 'saf_products.producer_country')
                        ->join($countryMl, function ($join) use ($country, $countryMl) {
                            $join->on("{$countryMl}.{$country}_id", '=', "{$country}.id")->where("{$countryMl}.lng_id", cLng('id'));
                        })
                        ->leftJoin($hsCodeReferenceTableName, 'saf_products.product_code', '=', (DB::raw("{$hsCodeReferenceTableName}.id::varchar")))
                        ->whereIn('saf_products.product_code', $refHsCodeId)
                        ->groupBy('saf_products_batch.id', 'product_code', 'commercial_description', "{$countryMl}.name")
                        ->get()->toArray();


                    foreach ($items as $subApp) {

                        $typeValue[] = (isset($subApp->data)) ? $subApp->data['saf']['sides'][$data['regime']]['type_value'] : '';

                        $safData = $subApp->data;

                        $subAppCustomData = $subApp->custom_data;

                        if ($data['regime'] == SingleApplication::REGIME_IMPORT) {
                            $safSideInfo = $safData['saf']['sides']['import']['name'];
                            $safSideTypeValue = $safData['saf']['sides']['import']['type_value'];
                        } elseif ($data['regime'] == SingleApplication::REGIME_EXPORT) {
                            $safSideInfo = $safData['saf']['sides']['export']['name'];
                            $safSideTypeValue = $safData['saf']['sides']['export']['type_value'];
                        } else {
                            $safSideInfo = $safSideTypeValue = '';
                        }

                        $products = [];

                        if (!empty($subApplicationsProducts)) {
                            foreach ($subApplicationsProducts as $key => $subApplicationsProduct) {

                                $productBatchApprovedQuantityByKg = 0;

                                $productBatchId = $subApplicationsProduct['saf_product_id'] ?? '';

                                if ($productBatchId) {

                                    $productBatchApprovedQuantityByKg = isset($subAppCustomData[$productBatchId . '-1645_28']) ? $productBatchApprovedQuantityByKg + (int)$subAppCustomData[$productBatchId . '-1645_28'] : $productBatchApprovedQuantityByKg;
                                    $productBatchApprovedQuantityByKg = ($productBatchApprovedQuantityByKg > 0) ? ($productBatchApprovedQuantityByKg / 1000) : 0;
                                }

                                if ($productBatchApprovedQuantityByKg > 0) {

                                    $hsCodeRef = getReferenceRows(ReferenceTable::REFERENCE_HS_CODE_6, $subApplicationsProduct['product_code'], false, ['code']);

                                    $productInfo = optional($hsCodeRef)->code . ' - ' . $subApplicationsProduct['commercial_description'];
                                    $products[$subApplicationsProduct['product_code']]['product_info'] = $productInfo;
                                    $products[$subApplicationsProduct['product_code']]['quantity'] = (isset($products[$subApplicationsProduct['product_code']]['quantity'])) ? $productBatchApprovedQuantityByKg + $products[$subApplicationsProduct['product_code']]['quantity'] : $productBatchApprovedQuantityByKg;
                                    $products[$subApplicationsProduct['product_code']]['producer_country'][] = $subApplicationsProduct['producer_country'];

                                }
                            }
                        }

                        if ($safSideInfo && $products) {

                            $allReportData[$safSideInfo] = [
                                'columnB' => $safSideInfo,
                                'saf_side_type_value' => $safSideTypeValue,
                                'products' => $products
                            ];

                        }
                    }
                }
            }
        }

        $allReportData['type_value'] = $typeValue;

        return $allReportData;
    }

    /**
     * Function to generate TjST Report 19
     *
     * @param $data
     * @return array
     */
    private function TjSt_19($data)
    {
        $subApplications = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['start_date_start'], $data['start_date_end']);

        $allRefHsCodeData = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_HS_CODE_6);
        $allRefCountriesData = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_COUNTRIES);

        $result = [];
        foreach ($subApplications as $subApplication) {

            $safData = $subApplication->data['saf'];
            $allData = json_decode($subApplication->all_data);

            $safSideInfo = '';
            if ($data['regime'] == SingleApplication::REGIME_IMPORT) {
                $safSideInfo = $safData['sides']['import']['name'] ?? '';
            } elseif ($data['regime'] == SingleApplication::REGIME_EXPORT) {
                $safSideInfo = $safData['sides']['export']['name'] ?? '';
            }

            $products = [];
            $producerCountries = [];
            $totalNationalValue = 0;
            foreach ($allData->products as $product) {
                $products[] = '(' . $allRefHsCodeData[$product->SAF_ID_89]['code'] . ') ' . $product->SAF_ID_91;

                if (isset($product->SAF_ID_92, $allRefCountriesData[$product->SAF_ID_92])) {
                    $producerCountries[] = $allRefCountriesData[$product->SAF_ID_92]['name'];
                }

                $totalNationalValue += $product->SAF_ID_136;
            }

            $result[] = [
                'permission_date' => formattedDate($subApplication->permission_date, false, true),
                'expiration_date' => formattedDate($subApplication->expiration_date, false, true),
                'permission_number' => $subApplication->permission_number,
                'saf_side_name' => $safSideInfo,
                'products' => implode(', ', $products),
                'producer_countries' => implode(', ', array_unique($producerCountries)),
                'total_netto_weight' => $allData->data->SAF_ID_82,
                'total_national_value' => $totalNationalValue,
            ];
        }

        return $result;
    }

    /**
     * Function to generate TjST report 20
     *
     * @param $data
     * @return array
     */
    private function TjSt_20($data)
    {
        $labId = $data['laboratory'];

        $fieldId = DocumentsDatasetFromMdm::where('mdm_field_id', self::LAB_MDM_FIELD_ID)->pluck('field_id')->first();

        $where["saf_sub_applications.custom_data->{$fieldId}"] = $labId;

        $subAppStart = $this->getSubApplicationIds(null, $data['document_id'], null, $data['start_date_start'], $data['start_date_end'], $where);
        $subAppEnd = $this->getSubApplicationIds(null, $data['document_id'], null, $data['end_date_start'], $data['end_date_end'], $where);

        $lab = getReferenceRows(ReferenceTable::REFERENCE_LABORATORIES, $labId);
        $labName = '';
        if (!is_null($lab)) {
            $labName = $lab->name;
        }

        $hsCode = $data['hs_code'];

        $hsCodeReferenceTableName = ReferenceTable::REFERENCE_HS_CODE_6;

        $refHsCodeId = DB::table($hsCodeReferenceTableName)->where('code', 'ILIKE', $hsCode . '%')->pluck('id')->all();

        $subApplicationsStart = SingleApplicationSubApplicationProducts::select(
            DB::raw("SUM(CAST(saf_products.netto_weight AS DECIMAL)) as netto_weight"),
            'product_code',
            'saf.regime',
            DB::raw("COUNT(saf_products) as filter_count")
        )
            ->whereIn('subapplication_id', $subAppStart->pluck('id')->all())
            ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
            ->join('saf', 'saf.regular_number', '=', 'saf_products.saf_number')
            ->whereIn((DB::raw('product_code::integer')), $refHsCodeId)
            ->groupBy('product_code', 'saf.regime')
            ->get();

        $subApplicationsEnd = SingleApplicationSubApplicationProducts::select(
            DB::raw("SUM(CAST(saf_products.netto_weight AS DECIMAL)) as netto_weight"),
            'product_code',
            'saf.regime',
            DB::raw("COUNT(saf_products) as filter_count")
        )
            ->whereIn('subapplication_id', $subAppEnd->pluck('id')->all())
            ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
            ->join('saf', 'saf.regular_number', '=', 'saf_products.saf_number')
            ->whereIn((DB::raw('product_code::integer')), $refHsCodeId)
            ->groupBy('product_code', 'saf.regime')
            ->get();

        $startSubApp = [];
        if ($subApplicationsStart->count()) {
            foreach ($subApplicationsStart as $subApplication) {
                $startSubApp[$subApplication->product_code][$subApplication->regime] = [
                    'filter_count' => $subApplication->filter_count,
                    'netto_weight' => $subApplication->netto_weight / 1000
                ];
            }
        }

        $endSubApp = [
            'import' => [
                'sum_filter_count' => $subApplicationsEnd->where('regime', SingleApplication::REGIME_IMPORT)->sum('filter_count'),
                'sum_netto_weight' => $subApplicationsEnd->where('regime', SingleApplication::REGIME_IMPORT)->sum('netto_weight') / 1000,
            ],
            'export' => [
                'sum_filter_count' => $subApplicationsEnd->where('regime', SingleApplication::REGIME_EXPORT)->sum('filter_count'),
                'sum_netto_weight' => $subApplicationsEnd->where('regime', SingleApplication::REGIME_EXPORT)->sum('netto_weight') / 1000,
            ],
        ];

        return [
            'startSubApp' => $startSubApp,
            'endSubApp' => $endSubApp,
            'labName' => $labName
        ];
    }

    /**
     * Function to generate TjST Report 21
     *
     * @param $data
     * @return array
     */
    private function TjSt_21($data)
    {
        $whereIn = [];
        if (isset($data['hs_code'])) {
            $refHsCodeId = $this->refHsDataIds($data['hs_code']);
            $whereIn['product_code'] = $refHsCodeId;
        }

        $subApplications = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['start_date_start'], $data['start_date_end'], [], $whereIn);

        $allRefHsCodeData = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_HS_CODE_6);
        $allRefCountriesData = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_COUNTRIES);

        $obligations = SingleApplicationObligations::selectRaw('SUM(saf_obligations.obligation) as obligation_sum,saf_obligations.subapplication_id')->whereIn('subapplication_id', $subApplications->pluck('id'))
            ->whereRaw('saf_obligations.obligation = saf_obligations.payment')
            ->join('reference_obligation_budget_line', function ($query) {
                $query->on('reference_obligation_budget_line.id', '=', 'saf_obligations.ref_budget_line_id');
            })
            ->join('reference_obligation_type', function ($query) {
                $query->on('reference_obligation_type.id', '=', 'reference_obligation_budget_line.budget_line')->where('reference_obligation_type.code', self::OBLIGATION_TYPE);
            })
            ->groupBy('subapplication_id')
            ->get()
            ->keyBy('subapplication_id')
            ->toArray();

        $result = [];
        foreach ($subApplications as $subApplication) {

            $safData = $subApplication->data['saf'];
            $customData = $subApplication->custom_data;
            $allData = json_decode($subApplication->all_data);

            $safSideInfo = '';
            if ($data['regime'] == SingleApplication::REGIME_IMPORT) {
                $safSideInfo = $safData['sides']['import']['name'] ?? '';
            } elseif ($data['regime'] == SingleApplication::REGIME_EXPORT) {
                $safSideInfo = $safData['sides']['export']['name'] ?? '';
            }

            $products = [];
            $producerCountries = [];
            $totalNationalValue = 0;
            foreach ($allData->products as $product) {
                $products[] = '(' . $allRefHsCodeData[$product->SAF_ID_89]['code'] . ') ' . $product->SAF_ID_91;

                if (isset($product->SAF_ID_92, $allRefCountriesData[$product->SAF_ID_92])) {
                    $producerCountries[] = $allRefCountriesData[$product->SAF_ID_92]['name'];
                }

                $totalNationalValue += $product->SAF_ID_136;
            }

            $obligationSum = 0;
            if(isset($obligations[$subApplication->id])){
                $obligationSum = $obligations[$subApplication->id]['obligation_sum'];
            }

            $result[] = [
                'permission_date' => formattedDate($subApplication->permission_date, false, true),
                'expiration_date' => formattedDate($subApplication->expiration_date, false, true),
                'permission_number' => $subApplication->permission_number,
                'saf_side_name' => $safSideInfo,
                'products' => implode(', ', $products),
                'producer_countries' => implode(', ', array_unique($producerCountries)),
                'based' => $customData['1645_24'] ?? '',
                'total_netto_weight' => $allData->data->SAF_ID_82,
                'total_national_value' => $totalNationalValue,
                'obligations_sum' => $obligationSum
            ];
        }

        return $result;
    }
}