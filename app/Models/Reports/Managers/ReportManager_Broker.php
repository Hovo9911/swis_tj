<?php

namespace App\Models\Reports\Managers;

use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\DataModel\DataModel;
use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\Reports\ReportManager;
use App\Models\Reports\Reports;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationObligations\SingleApplicationObligations;
use App\Models\SingleApplicationSubApplicationProducts\SingleApplicationSubApplicationProducts;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use App\Models\SingleApplicationSubApplicationStates\SingleApplicationSubApplicationStates;
use App\Models\User\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class ReportManager_Broker
 * @package App\Models\Reports\Managers
 */
class ReportManager_Broker extends ReportManager implements ReportManagerInterface
{
    /**
     * Function to get report data
     *
     * @param string $reportCode
     * @param array $inputData
     * @return array
     */
    public function getData(string $reportCode, array $inputData)
    {
        return $this->$reportCode($inputData);
    }

    /**
     * Function to get Broker Report 1
     *
     * @param $data
     * @return array
     */
    private function BrokerReport_1($data)
    {
        $allReportData = $subAppStart = $whereIn = [];

        $subDivisions = $data['sub_division_ids'] ?? null;

        $subDivisonsReferenceTableName = ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS;
        $subDivisonsReferenceMlTableName = ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS . '_ml';
        $document = ConstructorDocument::select('company_tax_id', 'document_code', 'constructor_documents_ml.document_name')->join('constructor_documents_ml', 'constructor_documents_ml.constructor_documents_id', '=', 'constructor_documents.id')->where('lng_id', cLng('id'))->where('id', $data['document_id'])->first();

        // Hs Code
        if (isset($data['hs_code'])) {
            $refHsCodeId = $this->refHsDataIds($data['hs_code']);
            $whereIn['product_code'] = $refHsCodeId;
        }

        // Subdivisions
        if ($subDivisions) {
            $whereIn['agency_subdivision_id'] = $subDivisions;
        }

        $dataModel = new DataModel();

        // Get SubApplications
        $where = function ($query) use ($data) {
            $query->when($data['tax_id'], function ($query1) use ($data) {
                $query1->where(function ($query2) use ($data) {
                    $query2->where('saf.regime', SingleApplication::REGIME_IMPORT)->where('saf.importer_value', $data['tax_id'])
                        ->orWhere('saf.regime', SingleApplication::REGIME_EXPORT)->where('saf.exporter_value', $data['tax_id']);
                });
            });
            $query->when($data['phone_number'], function ($query1) use ($data) {
                $query1->where(function ($query2) use ($data) {
                    $query2->where('saf.regime', SingleApplication::REGIME_IMPORT)->where('saf.data->saf->sides->import->phone_number', $data['phone_number'])
                        ->orWhere('saf.regime', SingleApplication::REGIME_EXPORT)->where('saf.data->saf->sides->export->phone_number', $data['phone_number']);
                });
            });
            $query->when($data['email'], function ($query1) use ($data) {
                $query1->where(function ($query2) use ($data) {
                    $query2->where('saf.regime', SingleApplication::REGIME_IMPORT)->where('saf.data->saf->sides->import->email', $data['email'])
                        ->orWhere('saf.regime', SingleApplication::REGIME_EXPORT)->where('saf.data->saf->sides->export->email', $data['email']);
                });
            });

        };

        if (!isset($data['document_status'])) {
            $allEndStates = ConstructorStates::select('id')->where(['constructor_document_id' => $data['document_id'], 'state_type' => ConstructorStates::END_STATE_TYPE])->active()->get();
            foreach ($allEndStates as $documentStatus) {
                $subApplicationIds = $this->getSubApplicationIds($documentStatus->id, $data['document_id'], $data['regime'], $data['start_date_start'], $data['start_date_end'], $where, $whereIn, false);

                $subApplicationIds->where('saf.applicant_value', Auth::user()->companyTaxId());

                //Дата представления
                if (isset($data['submitting_date_start']) && isDateFormat($data['submitting_date_start'])) {
                    $subApplicationIds->where('saf_sub_applications.status_date', '>=', $data['submitting_date_start']);
                }

                if (isset($data['submitting_date_end']) && isDateFormat($data['submitting_date_end'])) {
                    $subApplicationIds->where('saf_sub_applications.status_date', '<=', $data['submitting_date_end']);
                }

                if (!empty($this->data['obligation_type'])) {
                    $subApplicationIds->join('saf_obligations', 'saf_obligations.subapplication_id', '=', 'saf_sub_applications.id')
                        ->join('reference_obligation_budget_line', 'reference_obligation_budget_line.id', '=', 'saf_obligations.ref_budget_line_id')
                        ->join('reference_obligation_type', function ($q) {
                            $q->on('reference_obligation_type.id', '=', 'reference_obligation_budget_line.budget_line')->whereIn('reference_obligation_type.code', $this->data['obligation_type']);
                        });
                }

                $subAppStart[] = $subApplicationIds->get()->unique();
            }

        } else {
            $subApplicationIds = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['start_date_start'], $data['start_date_end'], $where, $whereIn, false);

            //Дата представления
            if (isset($data['submitting_date_start']) && isDateFormat($data['submitting_date_start'])) {
                $subApplicationIds->where('saf_sub_applications.status_date', '>=', $data['submitting_date_start']);
            }

            if (isset($data['submitting_date_end']) && isDateFormat($data['submitting_date_end'])) {
                $subApplicationIds->where('saf_sub_applications.status_date', '<=', $data['submitting_date_end']);
            }

            if (!empty($this->data['obligation_type'])) {
                $subApplicationIds->join('saf_obligations', 'saf_obligations.subapplication_id', '=', 'saf_sub_applications.id')
                    ->join('reference_obligation_budget_line', 'reference_obligation_budget_line.id', '=', 'saf_obligations.ref_budget_line_id')
                    ->join('reference_obligation_type', function ($q) {
                        $q->on('reference_obligation_type.id', '=', 'reference_obligation_budget_line.budget_line')->whereIn('reference_obligation_type.code', $this->data['obligation_type']);
                    });
            }

            $subAppStart[] = $subApplicationIds->get()->unique();
        }

        if (count($subAppStart) > 0) {
            foreach ($subAppStart as $items) {

                foreach ($items as $item) {
                    $stateName = '';
                    $currentState = null;

                    $subAppState = SingleApplicationSubApplicationStates::where(['saf_number' => $item['saf_number'], 'saf_subapplication_id' => $item['id'], 'is_current' => '1'])->first();
                    if (!is_null($subAppState)) {
                        $currentState = ConstructorStates::where('id', $subAppState->state_id)->first();
                        $stateName = optional($currentState)->current()->state_name;
                    }

                    $safInfo = SingleApplication::select('data', 'regime')
                        ->where('regular_number', $item['saf_number'])
                        ->whereIn('regime', $data['regime'])->first()->toArray();

                    $subApplicationNumber = $item['document_number'] ?? '';

                    $subApplicationCustomData = $item['custom_data'];
                    $subApplicationRequestDate = $item['data']['saf']['general']['subapplication_submitting_date'] ?? '-';

                    $subApplicationSubmittingDate = $item->status_date;
                    $subApplicationPermissionOrRejectionDate = $subApplicationPermissionOrRejectionNumber = $subApplicationPermissionOrRejectionReason = '-';

                    if (!is_null($subApplicationCustomData) && !is_null($currentState)) {

                        $requestDateDatasetMdm = DocumentsDatasetFromMdm::select('field_id')->where('document_id', $data['document_id'])->where('mdm_field_id', $dataModel->getMdmFieldIDByConstant(DataModel::REQUEST_DATE_MDM_ID))->first();
                        $mdmRequestDateFieldId = !is_null($requestDateDatasetMdm) ? $requestDateDatasetMdm->field_id : null;
                        $subApplicationRequestDate = (!is_null($mdmRequestDateFieldId) && array_key_exists($mdmRequestDateFieldId, $subApplicationCustomData)) ? $subApplicationCustomData[$mdmRequestDateFieldId] : '-';
                        $permissionOrRejectionReasonDatasetMdm = null;

                        if (($currentState->state_type == ConstructorStates::END_STATE_TYPE && $currentState->state_approved_status == ConstructorStates::END_STATE_NOT_APPROVED) || $currentState->state_type == ConstructorStates::CANCELED_STATE_TYPE) {

                            $permissionOrRejectionDateDatasetMdm = DocumentsDatasetFromMdm::select('id', 'field_id')->where('document_id', $data['document_id'])->where('mdm_field_id', $dataModel->getMdmFieldIDByConstant(DataModel::REJECTION_DATE_MDM_ID))->first(); //Rejection date
                            $permissionOrRejectionNumberDatasetMdm = DocumentsDatasetFromMdm::select('id', 'field_id')->where('document_id', $data['document_id'])->where('mdm_field_id', $dataModel->getMdmFieldIDByConstant(DataModel::REJECTION_NUMBER_MDM_ID))->first(); //Rejection number
                            $permissionOrRejectionReasonDatasetMdm = DocumentsDatasetFromMdm::select('id', 'field_id')->where('document_id', $data['document_id'])->where('mdm_field_id', $dataModel->getMdmFieldIDByConstant(DataModel::REJECTION_REASON_MDM_ID))->first(); //Rejection reason
                            $mdmPermissionOrRejectionReasonFieldId = !is_null($permissionOrRejectionReasonDatasetMdm) ? $permissionOrRejectionReasonDatasetMdm->field_id : null;
                            $subApplicationPermissionOrRejectionReason = (!is_null($mdmPermissionOrRejectionReasonFieldId) && array_key_exists($mdmPermissionOrRejectionReasonFieldId, $subApplicationCustomData)) ? $subApplicationCustomData[$mdmPermissionOrRejectionReasonFieldId] : '-';
                            $subApplicationPermissionOrRejectionReason = optional(getReferenceRows(ReferenceTable::REFERENCE_REJECTION_REASON, $subApplicationPermissionOrRejectionReason))->name;
                        } else {

                            $permissionOrRejectionDateDatasetMdm = DocumentsDatasetFromMdm::select('id', 'field_id')->where('document_id', $data['document_id'])->where('mdm_field_id', $dataModel->getMdmFieldIDByConstant(DataModel::PERMISSION_DATE_MDM_ID))->first();//Permission date
                            $permissionOrRejectionNumberDatasetMdm = DocumentsDatasetFromMdm::select('id', 'field_id')->where('document_id', $data['document_id'])->where('mdm_field_id', $dataModel->getMdmFieldIDByConstant(DataModel::PERMISSION_NUMBER_MDM_ID))->first(); //Permission number
                            $permissionOrRejectionReasonDatasetMdm = DocumentsDatasetFromMdm::select('id', 'field_id')->where('document_id', $data['document_id'])->where('mdm_field_id', $dataModel->getMdmFieldIDByConstant(DataModel::PERMISSION_DOCUMENT_MDM_ID))->first(); //Permission document
                            $mdmPermissionOrRejectionReasonFieldId = !is_null($permissionOrRejectionReasonDatasetMdm) ? $permissionOrRejectionReasonDatasetMdm->field_id : null;
                            $subApplicationPermissionOrRejectionReason = (!is_null($mdmPermissionOrRejectionReasonFieldId) && array_key_exists($mdmPermissionOrRejectionReasonFieldId, $subApplicationCustomData)) ? $subApplicationCustomData[$mdmPermissionOrRejectionReasonFieldId] : '-';
//                            $subApplicationPermissionOrRejectionReason = optional(getReferenceRows(ReferenceTable::REFERENCE_TYPE_OF_PERMISSION, $subApplicationPermissionOrRejectionReason))->name;
                        }

                        $mdmPermissionOrRejectionDateFieldId = !is_null($permissionOrRejectionDateDatasetMdm) ? $permissionOrRejectionDateDatasetMdm->field_id : null;
                        $mdmPermissionOrRejectionNumberFieldId = !is_null($permissionOrRejectionNumberDatasetMdm) ? $permissionOrRejectionNumberDatasetMdm->field_id : null;


                        $subApplicationPermissionOrRejectionDate = (!is_null($mdmPermissionOrRejectionDateFieldId) && array_key_exists($mdmPermissionOrRejectionDateFieldId, $subApplicationCustomData)) ? $subApplicationCustomData[$mdmPermissionOrRejectionDateFieldId] : '-';
                        $subApplicationPermissionOrRejectionNumber = (!is_null($mdmPermissionOrRejectionNumberFieldId) && array_key_exists($mdmPermissionOrRejectionNumberFieldId, $subApplicationCustomData)) ? $subApplicationCustomData[$mdmPermissionOrRejectionNumberFieldId] : '-';

                    }

                    if ($safInfo['regime'] == SingleApplication::REGIME_IMPORT) {
                        $safSideInfo = $safInfo['data']['saf']['sides']['import']['name'] ?? '-';
                        $safSideTypeValue = $safInfo['data']['saf']['sides']['import']['type_value'] ?? '-';
                    } elseif ($safInfo['regime'] == SingleApplication::REGIME_EXPORT) {
                        $safSideInfo = $safInfo['data']['saf']['sides']['export']['name'] ?? '-';
                        $safSideTypeValue = $safInfo['data']['saf']['sides']['export']['type_value'] ?? '-';
                    } else {
                        $safSideInfo = '-';
                        $safSideTypeValue = '-';
                    }

                    $user = User::find($item->user_id);
                    $safSideApplicantUserName = $user->name();
                    $safSideApplicantUserSSN = $user->ssn;

                    $subDivision = DB::table($subDivisonsReferenceTableName)
                        ->select('id', "{$subDivisonsReferenceMlTableName}.name", "{$subDivisonsReferenceTableName}.show_status", "{$subDivisonsReferenceTableName}.code")
                        ->join($subDivisonsReferenceMlTableName, $subDivisonsReferenceTableName . '.id', '=', $subDivisonsReferenceMlTableName . '.' . $subDivisonsReferenceTableName . '_id')
                        ->where($subDivisonsReferenceMlTableName . '.lng_id', '=', cLng('id'))
                        ->where('code', $document->company_tax_id)
                        ->first();

                    if (!is_null($subDivision) && $subDivision->show_status != ReferenceTable::STATUS_ACTIVE) {
                        $subDivision = DB::table($subDivisonsReferenceTableName)
                            ->select('id', "{$subDivisonsReferenceMlTableName}.name", "{$subDivisonsReferenceTableName}.show_status", "{$subDivisonsReferenceTableName}.code")
                            ->join($subDivisonsReferenceMlTableName, $subDivisonsReferenceTableName . '.id', '=', $subDivisonsReferenceMlTableName . '.' . $subDivisonsReferenceTableName . '_id')
                            ->where($subDivisonsReferenceMlTableName . '.lng_id', '=', cLng('id'))
                            ->where('code', $subDivision->code)
                            ->where("{$subDivisonsReferenceTableName}.show_status", ReferenceTable::STATUS_ACTIVE)
                            ->first();
                    }

                    if (!is_null($subDivisions)) {
                        $subDivisionNames = '';
                        foreach (getReferenceRowsWhereIn(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS, $subDivisions) as $subDivisionItem) {
                            $subDivisionNames .= $subDivisionItem->name . ',<br />';
                        }
                    } else {
                        $subDivisionNames = optional(getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS, $item['agency_subdivision_id']))->name ?? '-';
                    }

                    $allReportData[] = [
                        'saf_side_info' => $safSideInfo,
                        'saf_side_type_value' => "{$safSideTypeValue}, {$safSideInfo}",
                        'subdivision' => $subDivision->name ?? '',
                        'document' => $document->document_name ?? '',
                        'saf_applicant_type_value' => "{$safSideApplicantUserSSN}, {$safSideApplicantUserName}",
                        'saf_sub_application_number' => $subApplicationNumber,
                        'saf_sub_application_request_date' => formattedDate($subApplicationRequestDate, false, true),
                        'saf_sub_application_submitting_date' => formattedDate($subApplicationSubmittingDate, false, true),
                        'saf_sub_application_permission_or_rejection_date' => !empty($subApplicationPermissionOrRejectionDate) ? formattedDate($subApplicationPermissionOrRejectionDate, false, true) : '-',
                        'saf_sub_application_permission_or_rejection_number' => !empty($subApplicationPermissionOrRejectionNumber) ? $subApplicationPermissionOrRejectionNumber : '-',
                        'saf_sub_application_permission_or_rejection_reason' => !empty($subApplicationPermissionOrRejectionReason) ? $subApplicationPermissionOrRejectionReason : '-',
                        'saf_regime' => $safInfo['regime'],
                        'state_name' => !empty($stateName) ? $stateName : '-',
                        'subDivision_names' => $subDivisionNames
                    ];
                }

            }
        }

        return $allReportData;
    }

    /**
     * Function to get Broker Report 2
     *
     * @param $data
     * @return array
     */
    private function BrokerReport_2($data)
    {
        $allReportData = $subAppStart = $whereIn = [];

        $hsCodeReferenceTableName = ReferenceTable::REFERENCE_HS_CODE_6;
        $hsCodeReferenceTableNameMl = "{$hsCodeReferenceTableName}_ml";

        $document = ConstructorDocument::select('company_tax_id', 'document_code', 'constructor_documents_ml.document_name')->join('constructor_documents_ml', 'constructor_documents_ml.constructor_documents_id', '=', 'constructor_documents.id')->where('lng_id', cLng('id'))->where('id', $data['document_id'])->first();

        $refHsCodeId = [];
        if (isset($data['hs_code'])) {
            $refHsCodeId = $this->refHsDataIds($data['hs_code']);
            $whereIn['product_code'] = $refHsCodeId;
        }

        $allEndStates = ConstructorStates::where(['constructor_document_id' => $data['document_id'], 'state_type' => ConstructorStates::END_STATE_TYPE])->active()->get();
        if (isset($data['document_status'])) {
            $allEndStates = ConstructorStates::where('id', $data['document_status'])->active()->get();
        }
        $subAppStart = [];
        $dataModel = new DataModel();
        foreach ($allEndStates as $documentStatus) {
            $documentDatasetMdm = $subApplicationSubmissionDate = null;
            if (!is_null($documentStatus) && (($documentStatus->state_type == ConstructorStates::END_STATE_TYPE and $documentStatus->state_approved_status == ConstructorStates::END_STATE_NOT_APPROVED) or $documentStatus->state_type == ConstructorStates::CANCELED_STATE_TYPE)) {
                $documentDatasetMdm = DocumentsDatasetFromMdm::select('id', 'field_id')->where('document_id', $data['document_id'])->where('mdm_field_id', $dataModel->getMdmFieldIDByConstant(DataModel::REJECTION_DATE_MDM_ID))->first();
            } elseif (optional($documentStatus)->state_type == ConstructorStates::END_STATE_TYPE and optional($documentStatus)->state_approved_status == ConstructorStates::END_STATE_APPROVED) {
                $documentDatasetMdm = DocumentsDatasetFromMdm::select('id', 'field_id')->where('document_id', $data['document_id'])->where('mdm_field_id', $dataModel->getMdmFieldIDByConstant(DataModel::PERMISSION_DATE_MDM_ID))->first();
            } else {
                $subApplicationSubmissionDate = true;
            }

            $mdmFieldId = !is_null($documentDatasetMdm) ? $documentDatasetMdm->field_id : null;

            $subApplicationIds = SingleApplicationSubApplications::select('saf_sub_applications.id')
                ->join('saf', 'saf.regular_number', '=', 'saf_sub_applications.saf_number')
                ->join('saf_sub_application_states', 'saf_sub_application_states.saf_subapplication_id', '=', 'saf_sub_applications.id');

            if (count($whereIn)) {
                $subApplicationIds->leftJoin('saf_sub_application_products', 'saf_sub_application_products.subapplication_id', '=', 'saf_sub_applications.id')
                    ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
                    ->whereIn('saf_products.product_code', $whereIn['product_code']);
            }

            $subApplicationIds->where('saf.applicant_value', Auth::user()->companyTaxId());
            $subApplicationIds->where('saf_sub_application_states.state_id', $documentStatus->id);
            $subApplicationIds->when($data['country'], function ($q) use ($data) {
                $q->where('saf.regime', SingleApplication::REGIME_IMPORT)->where("saf_sub_applications.data->saf->sides->export->country", $data['country'])
                    ->orWhere('saf.regime', SingleApplication::REGIME_EXPORT)->where("saf_sub_applications.data->saf->sides->import->country", $data['country']);
            });

            if (is_array($data['regime'])) {
                $subApplicationIds->whereIn('saf.regime', $data['regime']);
            } else {
                $subApplicationIds->where('saf.regime', $data['regime']);
            }

            if (!is_null($subApplicationSubmissionDate)) {
                $subApplicationIds->where("saf_sub_applications.data->saf->general->subapplication_submitting_date", '>=', $data['start_date_start']);
                $subApplicationIds->where("saf_sub_applications.data->saf->general->subapplication_submitting_date", '<=', $data['start_date_end']);
            }

            if (!is_null($mdmFieldId)) {
                $subApplicationIds->where("saf_sub_applications.custom_data->{$mdmFieldId}", '>=', $data['start_date_start']);
                $subApplicationIds->where("saf_sub_applications.custom_data->{$mdmFieldId}", '<=', $data['start_date_end']);
            }

            foreach ($subApplicationIds->get() as $item) {
                $subAppStart[] = $item;
            }
        }

        if (count($subAppStart) > 0) {
            $subApplicationsProducts = SingleApplicationSubApplicationProducts::select('saf_products.producer_country', 'saf_sub_application_states.id as saf_sub_application_states_id', 'saf_sub_applications.id', 'saf_sub_applications.document_number', 'saf_sub_applications.custom_data', 'saf.regime', 'saf.data', "{$hsCodeReferenceTableName}.code", "{$hsCodeReferenceTableNameMl}.name", DB::raw("CONCAT(users.first_name , ' ', users.last_name) AS full_name"), 'users.ssn as user_ssn')
                ->join('saf_sub_applications', 'saf_sub_applications.id', '=', 'saf_sub_application_products.subapplication_id')
                ->join('users', 'users.id', '=', 'saf_sub_applications.user_id')
                ->leftJoin('saf_sub_application_states', function ($q) {
                    $q->on('saf_sub_application_states.saf_subapplication_id', '=', 'saf_sub_applications.id')->where('is_current', '1');
                })
                ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
                ->join('saf', 'saf.regular_number', '=', 'saf_sub_applications.saf_number')
                ->join($hsCodeReferenceTableName, DB::raw("{$hsCodeReferenceTableName}.id::varchar"), "=", "saf_products.product_code")
                ->join($hsCodeReferenceTableNameMl, "{$hsCodeReferenceTableNameMl}.{$hsCodeReferenceTableName}_id", "=", "{$hsCodeReferenceTableName}.id")
                ->whereIn('subapplication_id', collect($subAppStart)->pluck('id')->all())
                ->when(!empty($refHsCodeId), function ($q) use ($refHsCodeId) {
                    $q->whereIn('saf_products.product_code', $refHsCodeId);
                })
                ->orderBy('subapplication_id', 'asc')
                ->get();

            $exportCountry = getReferenceRows('countries', false, false, ['id', 'code', 'name'])->keyBy('id')->toArray();
            $subDivisions = getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME, false, false, ['id', 'code', 'name']);

            foreach ($subApplicationsProducts as $item) {
                $subApplicationNumber = $item['document_number'] ?? '';
                $subApplicationCustomData = $item['custom_data'];
                $safInfo = $item;
                $safInfoData = json_decode($item->data, true);

                if (!is_null($subApplicationCustomData) && !is_null($item->saf_sub_application_states_id)) {
                    $permissionOrRejectionReasonDatasetMdm = null;
                }

                if ($safInfo->regime == SingleApplication::REGIME_IMPORT) {
                    $safSideInfo = $safInfoData['saf']['sides']['import']['name'] ?? '-';
                    $safSideTypeValue = $safInfoData['saf']['sides']['import']['type_value'] ?? '-';
                } elseif ($safInfo->regime == SingleApplication::REGIME_EXPORT) {
                    $safSideInfo = $safInfoData['saf']['sides']['export']['name'] ?? '-';
                    $safSideTypeValue = $safInfoData['saf']['sides']['export']['type_value'] ?? '-';
                } else {
                    $safSideInfo = '-';
                    $safSideTypeValue = '-';
                }

                $exportCountryId = $item->producer_country;
                $exportImportCountry = ($safInfo['regime'] == SingleApplication::REGIME_EXPORT) ? $safInfoData['saf']['sides']['import']['country'] ?? 0 : $safInfoData['saf']['sides']['export']['country'] ?? 0;

                $exportCountryName = $exportCountry[$exportCountryId]->name ?? '';
                $exportImportCountryName = $exportCountry[$exportImportCountry]->name ?? '';

                $allReportData[] = [
                    'product_hs_code' => $item->code,
                    'product_hs_code_description' => $item->name,
                    'export_country' => $exportCountryName,
                    'saf_applicant_user_name' => $item->user_ssn . ', ' . $item->full_name,
                    'saf_side_type_value' => "{$safSideTypeValue}, {$safSideInfo}",
                    'saf_side_info' => $safSideInfo,
                    'subdivision' => $subDivisions->where('code', $document->company_tax_id)->first()->name ?? '',
                    'document' => $document->document_name ?? '',
                    'export_import_country' => $exportImportCountryName ?? '',
                    'saf_regime' => $safInfo->regime,
                    'saf_sub_application_number' => $subApplicationNumber,
                ];
            }
        }

        return $allReportData;
    }

    /**
     * Function to get Broker Report 3
     *
     * @param $data
     * @return array
     */
    private function BrokerReport_3($data)
    {
        $subApplications = SingleApplicationSubApplications::with(['document.currentMl', 'applicationCurrentState.state.currentMl', 'agency', 'userInfo'])
            ->select('saf_sub_applications.*', 'saf.regime', 'saf.regular_number', 'saf.data as safData')
            ->join('saf', 'saf.regular_number', '=', 'saf_sub_applications.saf_number')
            ->where("saf_sub_applications.status_date", '>=', $data['start_date_start'])
            ->where("saf_sub_applications.status_date", '<=', $data['start_date_end'])
            ->whereIn('saf.regime', $data['regime'])
            ->where('saf_sub_applications.user_company_tax_id', Auth::user()->companyTaxId())
            ->formedSubApplication()
            ->orderByDesc('saf_sub_applications.id');

        if (isset($data['obligation_type'])) {
            $subApplications->join('saf_obligations', 'saf_obligations.subapplication_id', '=', 'saf_sub_applications.id')
                ->join('reference_obligation_budget_line', 'reference_obligation_budget_line.id', '=', 'saf_obligations.ref_budget_line_id')
                ->join('reference_obligation_type', function ($q) use ($data) {
                    $q->on('reference_obligation_type.id', '=', 'reference_obligation_budget_line.budget_line')->whereIn('reference_obligation_type.code', $data['obligation_type']);
                });
        }

        if (isset($data['document_id'])) {
            $subApplications->where('saf_sub_applications.constructor_document_id', $data['document_id']);
        }

        if (isset($data['document_status'])) {
            $subApplications->join('saf_sub_application_states', function ($q) use ($data) {
                $q->on('saf_sub_application_states.saf_subapplication_id', '=', 'saf_sub_applications.id')->where(['saf_sub_application_states.state_id' => $data['document_status'], 'is_current' => '1']);
            });
        }

        if (isset($data['tax_id'])) {
            $subApplications->where('saf.regime', SingleApplication::REGIME_IMPORT)->where('saf.importer_value', $data['tax_id'])
                ->orWhere('saf.regime', SingleApplication::REGIME_EXPORT)->where('saf.exporter_value', $data['tax_id']);
        }

        $subApplications = $subApplications->get();
        $reportCode = Reports::REPORT_BROKER_REPORT_3_CODE;

        $result = [];
        foreach ($subApplications as $subApplication) {

            // Obligation
            $obligationType = trans("swis.{$reportCode}.total");
            if (!empty($data['obligation_type'])) {
                $obligationType = '';
                foreach (getReferenceRowsWhereIn(ReferenceTable::REFERENCE_OBLIGATION_TYPE, [], $data['obligation_type'], ['id', 'code', 'name']) as $obligation) {
                    $obligationType .= $obligation->name . "<br />";
                }
            }

            $sumObligation = SingleApplicationObligations::select('id', 'obligation')
                ->where('subapplication_id', $subApplication->id)
                ->join('reference_obligation_budget_line', 'reference_obligation_budget_line.id', '=', 'saf_obligations.ref_budget_line_id')
                ->join('reference_obligation_type', function ($q) use ($data) {
                    $q->on('reference_obligation_type.id', '=', 'reference_obligation_budget_line.budget_line')
//                        ->where('reference_obligation_budget_line.code', '!=', SingleApplicationObligations::SAF_OBLIGATIONS_FIX_CODE_FOR_SEND_APPLICATION_OBLIGATION)
                        ->when(isset($data['obligation_type']), function ($q1) use ($data) {
                            $q1->whereIn('reference_obligation_type.code', $data['obligation_type']);
                        });
                })->sum('saf_obligations.obligation');

            // Saf Sides
            $safSidesData = json_decode($subApplication->safData, true)['saf']['sides'];

            $safSideInfo = '-';
            $safSideTypeValue = '-';
            $safSideApplicantTypeValue = '-';
            if ($subApplication->regime == SingleApplication::REGIME_IMPORT) {
                $safSideInfo = $safSidesData['import']['name'] ?? '-';
                $safSideTypeValue = $safSidesData['import']['type_value'] ?? '-';
                $safSideApplicantTypeValue = $safSidesData['applicant']['type_value'] ?? '-';
            } elseif ($subApplication->regime == SingleApplication::REGIME_EXPORT) {
                $safSideInfo = $safSidesData['export']['name'] ?? '-';
                $safSideTypeValue = $safSidesData['export']['type_value'] ?? '-';
                $safSideApplicantTypeValue = $safSidesData['applicant']['type_value'] ?? '-';
            }

            //
            $safSideApplicantUserName = $subApplication->userInfo->name() ?? '';

            //
            $subDivision = getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS, $subApplication->agency_subdivision_id);

            $approvedOrRejectNumber = '-';
            $approvedOrRejectDate = '-';
            if ($subApplication->applicationCurrentState->state->state_approved_status == ConstructorStates::END_STATE_APPROVED) {
                $approvedOrRejectNumber = $subApplication->permission_number;
                $approvedOrRejectDate = $subApplication->permission_date;
            } elseif ($subApplication->applicationCurrentState->state->state_approved_status == ConstructorStates::END_STATE_NOT_APPROVED) {
                $approvedOrRejectNumber = $subApplication->rejected_number;
                $approvedOrRejectDate = $subApplication->rejection_date;
            }

            $result[] = [
                'saf_regime' => $subApplication->regime,
                'saf_sub_application_number' => $subApplication->document_number,
                'agency' => $subApplication->agency->current()->name,
                'sub_division' => $subDivision->name ?? '',
                'document' => $subApplication->document->currentMl->document_name ?? '',
                'saf_applicant_type_value' => "{$safSideApplicantTypeValue}, {$safSideApplicantUserName}",
                'saf_side_type_value' => "{$safSideTypeValue}, {$safSideInfo}",
                'saf_sub_application_submitting_date' => formattedDate($subApplication->status_date, false, true),
                'saf_sub_application_permission_or_rejection_number' => $approvedOrRejectNumber,
                'state_name' => $subApplication->applicationCurrentState->state->currentMl->state_name ?? '',
                'saf_sub_application_permission_or_rejection_date' => formattedDate($approvedOrRejectDate, false, true),
                'obligation_type' => $obligationType,
                'obligation_sum' => $sumObligation
            ];
        }

        return $result;
    }
}