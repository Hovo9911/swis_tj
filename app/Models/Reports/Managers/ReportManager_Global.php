<?php

namespace App\Models\Reports\Managers;

use App\Models\Agency\Agency;
use App\Models\BaseModel;
use App\Models\ConstructorActionSuperscribes\ConstructorActionSuperscribes;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\Reports\ReportManager;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationObligations\SingleApplicationObligations;
use App\Models\SingleApplicationSubApplicationProducts\SingleApplicationSubApplicationProducts;
use App\Models\Transactions\Transactions;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class ReportManager_Global
 * @package App\Models\Reports\Managers
 */
class ReportManager_Global extends ReportManager implements ReportManagerInterface
{
    /**
     * Function to get report data
     *
     * @param string $reportCode
     * @param array $inputData
     * @return array
     */
    public function getData(string $reportCode, array $inputData)
    {
        return $this->$reportCode($inputData);
    }

    /**
     * Function to generate Global report 1
     *
     * @param $data
     * @return array
     */
    private function GlobalReport_1($data)
    {
        $allReportData = [];

        $country = ReferenceTable::REFERENCE_COUNTRIES;
        $countryMl = $country . "_ml";

        $allRefHsCodeData = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_HS_CODE_6);

        $hsCode = $data['hs_code'] ?? null;

        $refHsCodeIds = [];
        if (isset($hsCode)) {
            $refHsCodeIds = $this->refHsDataIds($hsCode);
        }

        // Get SubApplications
        $subApplications = $this->getSubApplicationIds($data['document_status'], $data['document_id'], $data['regime'], $data['start_date_start'], $data['start_date_end']);

        if ($subApplications->count()) {

            $subApplicationsProducts = SingleApplicationSubApplicationProducts::select('product_code', 'saf_products.saf_number', 'saf_products.product_description', "{$countryMl}.name as producer_country", 'saf.regime as saf_regime', 'saf.data')
                ->whereIn('subapplication_id', $subApplications->pluck('id')->all())
                ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
                ->join('saf', 'saf.regular_number', '=', 'saf_products.saf_number')
                ->join($country, DB::raw("{$country}.id::varchar"), '=', 'saf_products.producer_country')
                ->join($countryMl, function ($join) use ($country, $countryMl) {
                    $join->on("{$countryMl}.{$country}_id", '=', "{$country}.id")->where("{$countryMl}.lng_id", cLng('id'));
                });

            if ($hsCode) {
                $subApplicationsProducts->whereIn('saf_products.product_code', $refHsCodeIds);
            }

            $subApplicationsProducts = $subApplicationsProducts->groupBy('product_code', 'saf_products.saf_number', 'saf_products.product_description', "{$countryMl}.name", 'saf.id')
                ->orderByDesc('saf_number')
                ->get();

            foreach ($subApplicationsProducts as $subApplicationsProduct) {

                $safRegime = $subApplicationsProduct->saf_regime;
                $sidesInfo = json_decode($subApplicationsProduct->data, true)['saf']['sides'];

                if ($safRegime == SingleApplication::REGIME_IMPORT) {
                    $safSideInfo = $sidesInfo['import']['name'] ?? '';
                    $safSideTypeValue = $sidesInfo['import']['type_value'] ?? '';
                } elseif ($safRegime == SingleApplication::REGIME_EXPORT) {
                    $safSideInfo = $sidesInfo['export']['name'] ?? '';
                    $safSideTypeValue = $sidesInfo['export']['type_value'] ?? '';
                } else {
                    $safSideInfo = '';
                    $safSideTypeValue = '';
                }

                $allReportData[] = [
                    'product' => '(' . $allRefHsCodeData[$subApplicationsProduct['product_code']]['code'] . ') ' . $subApplicationsProduct['product_description'],
                    'saf_side_info' => $safSideInfo,
                    'saf_side_type_value' => $safSideTypeValue,
                    'saf_regime' => $safRegime,
                    'producer_country' => $subApplicationsProduct['producer_country']
                ];
            }
        }

        return $allReportData;
    }

    /**
     * Function to generate Global report 2
     *
     * @param $data
     * @return array
     */
    private function GlobalReport_2($data)
    {
        $allReportData = $subAppStart = $whereIn = [];

        $hsCodeReferenceTableName = ReferenceTable::REFERENCE_HS_CODE_6;

        // Hs Code
        if (isset($data['hs_code'])) {
            $refHsCodeId = DB::table($hsCodeReferenceTableName)->where('code', 'ILIKE', '%' . $data['hs_code'] . '%')->pluck('id')->all();
            $whereIn['product_code'] = $refHsCodeId;
        }

        // Subdivisions
        if (isset($data['sub_division_ids'])) {
            $whereIn['agency_subdivision_id'] = $data['sub_division_ids'];
        }

        // Get SubApplications
        foreach ($data['document_status'] as $documentStatus) {
            $subAppStart[] = $this->getSubApplicationIds($documentStatus, $data['document_id'], $data['regime'], $data['start_date_start'], $data['start_date_end'], [], $whereIn);
        }

        $allRefSubDivisions = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS);

        if (count($subAppStart) > 0) {
            foreach ($subAppStart as $items) {

                $allSuperscribes = ConstructorActionSuperscribes::with(['user'])->whereIn('saf_subapplication_id',$items->pluck('id')->all())
                    ->orderByDesc('id')
                    ->get()->keyBy('saf_subapplication_id');

                $allConstructorStates = ConstructorStates::with('currentMl')->whereIn('id', $items->pluck('state_id')->unique()->all())->get()->keyBy('id');

                foreach ($items as $subApplication) {

                    $stateName = '-';
                    if (isset($allConstructorStates[$subApplication->state_id])) {
                        $stateName = $allConstructorStates[$subApplication->state_id]->currentMl->state_name;
                    }

                    $safRegime = $subApplication['data']['saf']['general']['regime'] ?? '';
                    $sidesInfo = $subApplication['data']['saf']['sides'] ?? '';

                    if ($safRegime == SingleApplication::REGIME_IMPORT) {
                        $safSideInfo = $sidesInfo['import']['name'] ?? '-';
                        $safSideApplicantTypeValue = $sidesInfo['import']['type_value'] ?? '-';
                    } elseif ($safRegime == SingleApplication::REGIME_EXPORT) {
                        $safSideInfo = $sidesInfo['export']['name'] ?? '-';
                        $safSideApplicantTypeValue = $sidesInfo['export']['type_value'] ?? '-';
                    } else {
                        $safSideInfo = '-';
                        $safSideApplicantTypeValue = '-';
                    }

                    $superscribeExpertUserName = '-';
                    if(isset($allSuperscribes[$subApplication->id])){
                        $superscribeExpertUserName = $allSuperscribes[$subApplication->id]->user->name();
                    }

                    $permissionOrRejectionDate = '-';
                    if(!empty($subApplication->permission_date)){
                        $permissionOrRejectionDate = formattedDate($subApplication->permission_date);
                    }

                    if(!empty($subApplication->rejection_date)){
                        $permissionOrRejectionDate = formattedDate($subApplication->rejection_date);
                    }

                    $subDivision = $allRefSubDivisions[$subApplication['agency_subdivision_id']] ?? '';

                    $allReportData[$subDivision['code'] ?? '-'][] = [
                        'saf_side_info' => $safSideInfo,
                        'saf_applicant_type_value' => $safSideApplicantTypeValue,
                        'saf_sub_application_request_date' => formattedDate($subApplication->request_date),
                        'saf_sub_application_registration_date' => formattedDate($subApplication->registration_date),
                        'saf_sub_application_submission_date' => formattedDate($subApplication->status_date),
                        'saf_sub_application_permission_or_rejection_date' => formattedDate($permissionOrRejectionDate),
                        'saf_sub_application_permission_or_rejection_number' => $subApplication->permission_number ?? $subApplication->rejected_number,
                        'saf_regime' => $safRegime,
                        'state_name' => $stateName,
                        'superscribe' => $superscribeExpertUserName,
                        'subdivision' => $subDivision['name'] ?? '-'
                    ];

                }
            }
        }

        return $allReportData;
    }

    /**
     * Function to generate Global report 3
     *
     * @param $data
     * @return array
     */
    private function GlobalReport_3($data)
    {
        $allReportData = $subAppStart = $whereIn = [];

        $allRefCountriesData = getReferenceRowsKeyValue(ReferenceTable::REFERENCE_COUNTRIES);

        // Hs Code
        if (isset($data['hs_code'])) {
            $refHsCodeId = $this->refHsDataIds($data['hs_code']);
            $whereIn['product_code'] = $refHsCodeId;
        }

        // Get SubApplications
        $where = function ($query) use ($data) {
            $query->when($data['tax_id'], function ($q) use ($data) {
                $q->where('saf.regime', SingleApplication::REGIME_IMPORT)->where('saf.importer_value', $data['tax_id'])
                    ->orWhere('saf.regime', SingleApplication::REGIME_EXPORT)->where('saf.exporter_value', $data['tax_id']);
            });
            $query->when($data['country'], function ($q) use ($data) {
                $q->where('saf.regime', SingleApplication::REGIME_IMPORT)->where('saf.data->saf->transportation->export_country', $data['country'])
                    ->orWhere('saf.regime', SingleApplication::REGIME_EXPORT)->where('saf.data->saf->transportation->import_country', $data['country']);
            });
        };

        foreach ($data['document_status'] as $documentStatus) {
            $subAppStart[] = $this->getSubApplicationIds($documentStatus, $data['document_id'], $data['regime'], $data['start_date_start'], $data['start_date_end'], $where, $whereIn);
        }

        if (count($subAppStart) > 0) {

            foreach ($subAppStart as $items) {

                //
                $allSuperscribes = ConstructorActionSuperscribes::with(['user'])->whereIn('saf_subapplication_id',$items->pluck('id')->all())
                    ->orderByDesc('id')
                    ->get()->keyBy('saf_subapplication_id');

                //
                $allConstructorStates = ConstructorStates::with('currentMl')->whereIn('id', $items->pluck('state_id')->unique()->all())->get()->keyBy('id');

                //
                $allObligations = SingleApplicationObligations::selectRaw('saf_obligations.obligation,subapplication_id')->whereIn('subapplication_id',$items->pluck('id')->all())
                    ->whereRaw('saf_obligations.obligation = saf_obligations.payment')
                    ->join('reference_obligation_budget_line', function ($query) {
                        $query->on('reference_obligation_budget_line.id', '=', 'saf_obligations.ref_budget_line_id');
                    })
                    ->join('reference_obligation_type', function ($query) {
                        $query->on('reference_obligation_type.id', '=', 'reference_obligation_budget_line.budget_line')->where('reference_obligation_type.code', self::OBLIGATION_TYPE);
                    })
                    ->groupBy('subapplication_id','saf_obligations.obligation')
                    ->get()->keyBy('subapplication_id');

                foreach ($items as $subApplication) {

                    $stateName = '-';
                    if (isset($allConstructorStates[$subApplication->state_id])) {
                        $stateName = $allConstructorStates[$subApplication->state_id]->currentMl->state_name;
                    }

                    $safRegime = $subApplication['data']['saf']['general']['regime'] ?? '';
                    $safInfo = $subApplication['data']['saf'] ?? [];

                    $safSideInfo = '-';
                    $safSideTypeValue = '-';
                    $safTransportationCountry = '-';
                    $safSideApplicantName = '-';
                    $safSideApplicantTypeValue = '-';

                    if ($safRegime == SingleApplication::REGIME_IMPORT || $safRegime == SingleApplication::REGIME_EXPORT) {
                        $safSideInfo = $safInfo['sides'][$safRegime]['name'] ?? '-';
                        $safSideTypeValue = $safInfo['sides'][$safRegime]['type_value'] ?? '-';
                        $safTransportationCountry = $allRefCountriesData[ $safInfo['transportation']['export_country']]['name'] ?? '-';
                        $safSideApplicantName = $safInfo['sides']['applicant']['name'] ?? '-';
                        $safSideApplicantTypeValue = $safInfo['sides']['applicant']['type_value'] ?? '-';
                    }

                    $superscribeExpertUserName = '-';
                    if(isset($allSuperscribes[$subApplication->id])){
                        $superscribeExpertUserName = $allSuperscribes[$subApplication->id]->user->name();
                    }

                    $permissionOrRejectionDate = '-';
                    if(!empty($subApplication->permission_date)){
                        $permissionOrRejectionDate = formattedDate($subApplication->permission_date);
                    }

                    if(!empty($subApplication->rejection_date)){
                        $permissionOrRejectionDate = formattedDate($subApplication->rejection_date);
                    }

                    //
                    $obligationSum = 0;
                    if(isset($allObligations[$subApplication->id])){
                        $obligationSum = $allObligations[$subApplication->id]->obligation;
                    }

                    $allReportData[] = [
                        'saf_side_info' => $safSideInfo,
                        'saf_side_type_value' => $safSideTypeValue,
                        'saf_transportation_country' => $safTransportationCountry,
                        'saf_applicant_name' => $safSideApplicantName,
                        'saf_applicant_type_value' => $safSideApplicantTypeValue,
                        'saf_sub_application_number' => $subApplication->document_number,
                        'saf_sub_application_request_date' => formattedDate($subApplication->status_date),
                        'saf_sub_application_registration_date' => formattedDate($subApplication->registration_date),
                        'saf_sub_application_permission_or_rejection_date' => $permissionOrRejectionDate,
                        'obligations_sum' => $obligationSum,
                        'saf_sub_application_permission_or_rejection_number' => $subApplication->permission_number ?? $subApplication->rejected_number,
                        'saf_sub_application_rejection_reason' => $subApplication->rejected_reason,
                        'saf_regime' => $safRegime,
                        'state_name' => $stateName,
                        'superscribe' => $superscribeExpertUserName,
                    ];

                }
            }
        }

        return $allReportData;
    }

    /**
     * Function to generate Global report 4
     *
     * @param $data
     * @return array
     */
    private function GlobalReport_4($data)
    {
        $query = Transactions::select([
            'payments.id as id',
            DB::raw("concat(users.first_name, ' ', users.last_name) as user_name"),
            'users.ssn as user_ssn',
            'payments.document_id as transaction_id',
            'payments.payment_done_date as payment_date',
            'payments.amount',
            'payment_providers.name as provider',
            DB::raw("COALESCE(agency_ml.name, company.name) as company_name"),
        ])
            ->leftJoin('users', 'users.id', '=', 'transactions.user_id')
            ->join('payments', 'payments.id', '=', 'transactions.payment_id')
            ->join('payment_providers', 'payment_providers.id', '=', 'payments.service_provider_id')
            ->leftJoin('company', 'company.tax_id', '=', 'transactions.company_tax_id')
            ->leftJoin('agency', function ($query) {
                $query->on('agency.tax_id', '=', 'transactions.company_tax_id')->where('agency.show_status', BaseModel::STATUS_ACTIVE);
            })
            ->leftJoin('agency_ml', function ($q) {
                $q->on('agency_ml.agency_id', '=', 'agency.id')->where('agency_ml.lng_id', cLng('id'));
            })
            ->orderbyDesc('transactions.id');


        // NEED for now remove agencies ,in future maybe open
        $agenciesTaxIds = Agency::active()->pluck('tax_id')->all();
        $query->whereNotIn('transactions.company_tax_id', $agenciesTaxIds);

        if (!Auth::user()->isSwAdmin()) {
            $query->where('transactions.company_tax_id', Auth::user()->companyTaxId());
        }

        if (isset($data['start_date_start'])) {
            $query->where('payments.payment_done_date', '>=', Carbon::parse($data['start_date_start'])->startOfDay());
        }

        if (isset($data['start_date_end'])) {
            $query->where('payments.payment_done_date', '>=', Carbon::parse($data['start_date_end'])->endOfDay());
        }

        if (isset($data['id'])) {
            $query->where('payments.id', $data['id']);
        }

        if (isset($data['user_name'])) {
            $query->where(function ($q) use ($data) {
                foreach (explode(" ", $data['user_name']) ?? [] as $value) {
                    $q->orWhere('users.first_name', "ILIKE", "{$value}%")->orWhere('users.last_name', "ILIKE", "%{$value}");
                }
            });
        }

        if (isset($data['user_ssn'])) {
            $query->where('users.ssn', $data['user_ssn']);
        }

        if (isset($data['transaction_id'])) {
            $query->where('payments.document_id', $data['transaction_id']);
        }

        if (isset($data['payment_provider_id'])) {
            $query->where('payment_providers.id', $data['payment_provider_id']);
        }

        if (isset($data['company_name'])) {
            $query->where(function ($q) use ($data) {
                $companyName = str_replace('"', "", $data['company_name']);
                $q->orWhere(DB::raw("REPLACE(agency_ml.name, '\"', '')"), 'ILIKE', "%{$companyName}%")->orWhere(DB::raw("REPLACE(company.name, '\"', '')"), 'ILIKE', "%{$companyName}%");
            });
        }

        return $query->get()->toArray();

    }
}