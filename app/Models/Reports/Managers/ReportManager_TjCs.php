<?php

namespace App\Models\Reports\Managers;

use App\Models\ConstructorStates\ConstructorStates;
use App\Models\DataModel\DataModel;
use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\Reports\ReportManager;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationSubApplicationProducts\SingleApplicationSubApplicationProducts;
use Illuminate\Support\Facades\DB;

//ХАДАМОТИ АЛОКАИ НАЗДИ ҲУКУМАТИ ҶУМҲУРИИ ТОҶИКИСТОН  agency tax_id = 020004915 reports part

/**
 * Class ReportManager_TjCs
 * @package App\Models\Reports\Managers
 */
class ReportManager_TjCs extends ReportManager implements ReportManagerInterface
{
    /**
     * Function to get report data
     *
     * @param string $reportCode
     * @param array $inputData
     * @return array
     */
    public function getData(string $reportCode, array $inputData)
    {
        return $this->$reportCode($inputData);
    }

    /**
     * Function to generate Report 1
     *
     * @param $data
     * @return array
     */
    private function TjCs_1($data)
    {
        $allReportData = $subAppStart = [];

        $hsCodeReferenceTableName = ReferenceTable::REFERENCE_HS_CODE_6;

        // Get SubApplications
        foreach ($data['document_status'] as $documentStatus) {
            $subAppStart[] = $this->getSubApplicationIds($documentStatus, $data['document_id'], $data['regime'], $data['start_date_start'], $data['start_date_end']);
        }

        $dataModel = new DataModel();

        $constructorStateInfos = ConstructorStates::whereIn('id', $data['document_status'])->get() ?? '';

        if (count($subAppStart) > 0) {
            foreach ($subAppStart as $items) {
                foreach ($items as $item) {
                    foreach ($constructorStateInfos as $constructorStateInfo) {

                        $saf = SingleApplication::select('data')->where('regular_number', $item->saf_number)->firstOrFail();

                        $safData = $saf->data;
                        $subAppCustomData = $item->custom_data;

                        $subApplicationNumber = $item->document_number;
                        $subApplicationSubmittingDate = $item->status_date;

                        if ($data['regime'] == SingleApplication::REGIME_IMPORT) {
                            $safSideInfo = $safData['saf']['sides']['import']['name'];
                        } elseif ($data['regime'] == SingleApplication::REGIME_EXPORT) {
                            $safSideInfo = $safData['saf']['sides']['export']['name'];
                        } else {
                            $safSideInfo = '';
                        }

                        $subApplicationsProducts = SingleApplicationSubApplicationProducts::select('product_code', 'saf_products.commercial_description', 'total_value_national')
                            ->where('saf_sub_application_products.subapplication_id', $item->id)
                            ->join('saf_products', 'saf_sub_application_products.product_id', '=', 'saf_products.id')
                            ->leftJoin($hsCodeReferenceTableName, 'saf_products.product_code', '=', DB::raw("{$hsCodeReferenceTableName}.id::varchar"))
                            ->get()->toArray();

                        $totalValueNational = 0;
                        $products = '';

                        if (!empty($subApplicationsProducts)) {
                            foreach ($subApplicationsProducts as $subApplicationsProduct) {

                                $hsCodeRef = getReferenceRows(ReferenceTable::REFERENCE_HS_CODE_6, $subApplicationsProduct['product_code'], false, ['code']);
                                $products = $products . optional($hsCodeRef)->code . ' - ' . $subApplicationsProduct['commercial_description'] . '<br>';
                                $totalValueNational = $totalValueNational + $subApplicationsProduct['total_value_national'];
                            }
                        }

                        if (($constructorStateInfo->state_type == ConstructorStates::END_STATE_TYPE and $constructorStateInfo->state_approved_status == ConstructorStates::END_STATE_NOT_APPROVED) or $constructorStateInfo->state_type == ConstructorStates::CANCELED_STATE_TYPE) {

                            //Rejection date
                            $documentDatasetMdm = DocumentsDatasetFromMdm::select('id', 'field_id')->where('document_id', $data['document_id'])->where('mdm_field_id', $dataModel->getMdmFieldIDByConstant(DataModel::REJECTION_NUMBER_MDM_ID))->first();

                        } else {

                            //Permission date
                            $documentDatasetMdm = DocumentsDatasetFromMdm::select('id', 'field_id')->where('document_id', $data['document_id'])->where('mdm_field_id', $dataModel->getMdmFieldIDByConstant(DataModel::PERMISSION_NUMBER_MDM_ID))->first();
                        }

                        $mdmFieldId = !is_null($documentDatasetMdm) ? $documentDatasetMdm->field_id : '';

                        if ($subApplicationNumber) {
                            $allReportData[$subApplicationNumber] = [
                                'columnB' => $subApplicationNumber . ' - ' . $subApplicationSubmittingDate,
                                'columnC' => $safSideInfo,
                                'columnD' => $products,
                                'columnE' => $subAppCustomData[$mdmFieldId] ?? '',
                                'columnF' => $totalValueNational
                            ];
                        }
                    }
                }
            }
        }

        return $allReportData;
    }
}