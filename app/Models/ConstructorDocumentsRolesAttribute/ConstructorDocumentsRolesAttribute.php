<?php

namespace App\Models\ConstructorDocumentsRolesAttribute;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

/**
 * Class ConstructorDocumentsRolesAttribute
 * @package App\Models\ConstructorDocumentsRolesAttribute
 */
class ConstructorDocumentsRolesAttribute extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var string
     */
    public $table = 'constructor_documents_roles_attribute';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $primaryKey = ['constructor_documents_roles_id', 'attribute_id'];

    /**
     * @var array
     */
    protected $fillable = [
        'constructor_documents_roles_id',
        'type',
        'field_id'
    ];
}
