<?php

namespace App\Models\SingleApplicationNotes;

use App\Models\BaseModel;
use App\Models\ConstructorActions\ConstructorActions;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\SingleApplicationObligations\SingleApplicationObligations;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Contracts\Translation\Translator;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class SingleApplicationNotes
 *
 * @package App\Models\SingleApplicationNotes
 */
class SingleApplicationNotes extends BaseModel
{
    /**
     * @var string
     */
    const NOTE_TYPE_NOTIFICATION = 'notification';
    const NOTE_TYPE_MESSAGE = 'message';

    /**
     * @var array
     */
    const NOTE_TYPES = [self::NOTE_TYPE_NOTIFICATION, self::NOTE_TYPE_MESSAGE];

    /**
     * @var string
     */
    const NOTE_SEND_MODULE_SAF = 'saf';
    const NOTE_SEND_MODULE_APPLICATION = 'application';

    /**
     * @var array
     */
    const NOTE_SEND_MODULES = [self::NOTE_SEND_MODULE_SAF, self::NOTE_SEND_MODULE_APPLICATION];

    /**
     * @var int
     */
    const NOTE_TO_SAF_HOLDER = 1;

    /**
     * @var string (NOTES TRANS KEYS)
     */
    // SAF
    const NOTE_TRANS_KEY_CREATE_SAF = 'swis.saf.notes.saf_created';
    const NOTE_TRANS_KEY_CREATE_SAF_SAVED = 'swis.saf.notes.saf_created.saved';
    const NOTE_TRANS_KEY_DRAFT_SAF = 'swis.saf.notes.draft_saf.key';
    const NOTE_TRANS_KEY_VOID_SAF = 'swis.saf.notes.void_saf.key';
    const NOTE_TRANS_KEY_SEND_SAF = 'swis.saf.notes.send_saf.key';
    const NOTE_TRANS_PAY_OBLIGATION_TYPE = 'swis.saf.notes.pay.obligation';
    const NOTE_TRANS_KEY_SAF_UPDATE_STATUS_TYPE = 'swis.saf.notes.saf_updates_status_type.key';
    const NOTE_TRANS_KEY_CREATED_SUB_APPLICATION = 'swis.saf.notes.sub_application_created.key';
    const NOTE_TRANS_KEY_MESSAGE_SEND_FROM_SAF_TO_SUB_APP = 'swis.saf.notes.message_send_from_saf_to_sub_app.key';
    const NOTE_TRANS_KEY_MESSAGE_SEND_FROM_SAF_TO_SUB_APP_INFO = 'swis.saf.notes.message_send_from_saf_to_sub_app_info.key';
    const NOTE_TRANS_KEY_SAF_SAVE_NOT = 'swis.saf.notes.saf_save_note.key';
    const NOTE_TRANS_KEY_SAF_SUB_APP_ACTION_COMPLETE_NEW_STATE = 'swis.saf.notes.sub_application_action_complete_new_state.to_holder.key';
    const NOTE_TRANS_KEY_SIGNATURE_BTN = 'swis.saf.digital-signature.check';

    /**
     * @var string (User Documents(My application))
     */
    const NOTE_TRANS_KEY_SUB_APP_SAVE_NOT = 'swis.saf.notes.sub_app_save_note.key';
    const NOTE_TRANS_KEY_SEND_FROM_SUB_APP_TO_SAF_INFO = 'swis.saf.notes.sent_from_sub_application_info.key';
    const NOTE_TRANS_KEY_SEND_FROM_SUB_APP_TO_SAF = 'swis.saf.notes.sent_from_sub_application.key';
    const NOTE_TRANS_KEY_SUB_APP_SUPERSCRIBE_CREATE = 'swis.saf.notes.sub_application_superscribe_create.key';
    const NOTE_TRANS_KEY_SUB_APP_SUPERSCRIBE_DELETE = 'swis.saf.notes.sub_application_superscribe_delete.key';
    const NOTE_TRANS_KEY_SUB_APP_ACTION_COMPLETE_NEW_STATE = 'swis.saf.notes.sub_application_action_complete_new_state.key';
    const NOTE_TRANS_KEY_SEND_FROM_SUB_APP_TO_SHOW_IN_SUB = 'swis.saf.notes.sent_from_sub_application_info_for_sub_application.key';
    const NOTE_TRANS_KEY_REQUEST_FROM_SUB_APP_TO_SAF = 'swis.saf.notes.request_send_from_sub_application_to_saf.key';

    /**
     * @var string (In trans Variables list)
     */
    const NOTE_VARIABLE_USER_NAME = 'USER_NAME';
    const NOTE_VARIABLE_SUB_APPLICATION_NUMBER = 'SUB_APPLICATION_NUMBER';
    const NOTE_VARIABLE_NOTE = 'NOTE';
    const NOTE_VARIABLE_ACTION_NAME = 'ACTION_NAME';
    const NOTE_VARIABLE_STATE_NAME = 'STATE_NAME';
    const NOTE_VARIABLE_OBLIGATION_TYPE = 'OBLIGATION_TYPE';
    const NOTE_VARIABLE_OBLIGATION_SUM = 'OBLIGATION_SUM';
    const NOTE_VARIABLE_SUB_APPLICATION_ID = 'SUBAPPLICATION_ID';
    const NOTE_VARIABLE_SIGNATURE_KEY = 'SIGNATURE_KEY';

    /**
     * @var array
     */
    const HIDE_NOTES_IN_SUB_APPLICATION = [
        self::NOTE_TRANS_KEY_SAF_SUB_APP_ACTION_COMPLETE_NEW_STATE
    ];

    /**
     * @var string
     */
    public $table = 'saf_notes';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'creator_user_id',
        'saf_number',
        'trans_key',
        'note',
        'to_saf_holder',
        'sub_application_id',
        'type',
        'send_from_module',
        'obligation_id',
    ];

    /**
     * Function to get create_at value
     *
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format(config('swis.date_time_format_front'));
    }

    /**
     * Function to relate with SingleApplicationSubApplications
     *
     * @return HasOne
     */
    public function subApplication()
    {
        return $this->hasOne(SingleApplicationSubApplications::class, 'id', 'sub_application_id');
    }

    /**
     * Function to Store data to DB
     *
     * @param $data
     */
    public static function store(array $data)
    {
        $defaultData = [
            'sub_application_id' => null,
            'user_id' => Auth::id(),
            'type' => SingleApplicationNotes::NOTE_TYPE_NOTIFICATION,
            'to_saf_holder' => SingleApplicationNotes::FALSE,
            'send_from_module' => SingleApplicationNotes::NOTE_SEND_MODULE_SAF,
        ];

        $insertData = array_merge($defaultData, $data);

        self::create($insertData);
    }

    /**
     * Function to store Saf Notes
     *
     * @param array $data
     * @param null $transKey
     */
    public static function storeNotes(array $data, $transKey = null)
    {
        $defaultData = [
            'sub_application_id' => null,
            'user_id' => Auth::user()->getCreatorUser()->id,
            'creator_user_id' => Auth::id(),
            'type' => SingleApplicationNotes::NOTE_TYPE_NOTIFICATION,
            'to_saf_holder' => SingleApplicationNotes::FALSE,
            'send_from_module' => SingleApplicationNotes::NOTE_SEND_MODULE_SAF,
            'trans_key' => $transKey
        ];

        $insertData = array_merge($defaultData, $data);

        self::create($insertData);
    }

    /**
     * Function to get Saf Notes
     *
     * @param $regularNumber
     * @return mixed
     */
    public static function getSafNotes($regularNumber)
    {
        $notes = self::where('saf_number', $regularNumber)
            ->where(function ($query) {
                $query->orWhere('send_from_module', SingleApplicationNotes::NOTE_SEND_MODULE_SAF)->orWhere('to_saf_holder', true);
            })
            ->orderByDesc()
            ->get();

        return (new self)->generateWithTrans($notes);
    }

    /**
     * Function to get Application Notes
     *
     * @param $subApplicationId
     * @return mixed
     */
    public static function getApplicationNotes($subApplicationId)
    {
        $notes = self::where('sub_application_id', $subApplicationId)
            ->orderByDesc()
            ->whereNotIn('trans_key', self::HIDE_NOTES_IN_SUB_APPLICATION)
            ->get();

        return (new self)->generateWithTrans($notes, true);
    }

    /**
     * Function to get Sub Application Notes for PDF
     *
     * @param $subApplicationId
     * @return SingleApplicationNotes
     */
    public static function getSubApplicationNotesForPdf($subApplicationId)
    {
        $notes = self::where(['sub_application_id' => $subApplicationId, 'send_from_module' => self::NOTE_SEND_MODULE_APPLICATION])
            ->where(function ($q) {
                $q->where('trans_key', '!=', self::NOTE_TRANS_KEY_SUB_APP_SAVE_NOT)
                    ->orWhere('trans_key', '=', self::NOTE_TRANS_KEY_SUB_APP_SAVE_NOT)
                    ->where('to_saf_holder', '=', self::NOTE_TO_SAF_HOLDER);
            })
            ->orderByDesc()
            ->get();

        return (new self)->generateWithTrans($notes, true);
    }

    /**
     * Function to generate With Trans
     *
     * @param $notes
     * @param $isApplication
     * @return SingleApplicationNotes
     */
    private function generateWithTrans($notes, $isApplication = false)
    {
        $noteCreatedUsers = [];
        $subApplicationsList = [];
        foreach ($notes as &$note) {

            if (!isset($noteCreatedUsers[$note->user_id])) {
                $noteCreatedUserName = User::getUserNameByID($note->user_id);
                $noteCreatedUsers[$note->user_id] = $noteCreatedUserName;
            } else {
                $noteCreatedUserName = $noteCreatedUsers[$note->user_id];
            }

            $subApplicationDocumentNumber = '';
            if (!isset($subApplicationsList[$note->sub_application_id])) {

                if (!is_null($note->sub_application_id)) {
                    $subApplication = SingleApplicationSubApplications::select('document_number')->where('id', $note->sub_application_id)->first();

                    if (!is_null($subApplication)) {
                        $subApplicationDocumentNumber = $subApplication->document_number;

                        $subApplicationsList[$note->sub_application_id] = $subApplication->document_number;
                    }
                }
            } else {
                $subApplicationDocumentNumber = $subApplicationsList[$note->sub_application_id];
            }

            $generatedNote = self::getVariablesValues($note, $note->trans_key, $noteCreatedUserName, $subApplicationDocumentNumber);

            // if Notes send to saf user from sub application, user will see the note
            if ($note->to_saf_holder) {
                $note->note_from_sub_application = self::getVariablesValues($note, self::NOTE_TRANS_KEY_SEND_FROM_SUB_APP_TO_SAF_INFO, $noteCreatedUserName, $subApplicationDocumentNumber);
                // for sub application user will see his sent sub application note
                if ($isApplication && $note->trans_key != SingleApplicationNotes::NOTE_TRANS_KEY_SUB_APP_ACTION_COMPLETE_NEW_STATE) {
                    $note->sent_to_saf_holder = self::getVariablesValues($note, self::NOTE_TRANS_KEY_SEND_FROM_SUB_APP_TO_SHOW_IN_SUB, $noteCreatedUserName, $subApplicationDocumentNumber);
                    $generatedNote = self::getVariablesValues($note, self::NOTE_TRANS_KEY_SUB_APP_SAVE_NOT, $noteCreatedUserName, $subApplicationDocumentNumber);
                }
            }

            // when user send message to application
            if ($note->type == SingleApplicationNotes::NOTE_TYPE_MESSAGE && $note->send_from_module == SingleApplicationNotes::NOTE_SEND_MODULE_SAF) {
                $note->message_from_saf = self::getVariablesValues($note, self::NOTE_TRANS_KEY_MESSAGE_SEND_FROM_SAF_TO_SUB_APP_INFO, $noteCreatedUserName, $subApplicationDocumentNumber);
            }

            $note->note = $generatedNote;
        }

        return $notes;
    }

    /**
     * Function to change variable name with value
     *
     * @param $note
     * @param $trans
     * @param string $noteCreatedUserName
     * @param string $subApplicationDocumentNumber
     * @return array|Translator|mixed|string|null
     */
    private static function getVariablesValues($note, $trans, $noteCreatedUserName = '', $subApplicationDocumentNumber = '')
    {
        if ($note->trans_key) {
            $transValue = trans($trans);
            $dataVariables = getStringsBetween($transValue, '{', '}');

            $dataVariablesValues = [
                self::NOTE_VARIABLE_USER_NAME => $noteCreatedUserName
            ];

            foreach ($dataVariables as $dataVariable) {
                switch ($dataVariable) {

                    case self::NOTE_VARIABLE_SUB_APPLICATION_NUMBER:
                        $dataVariablesValues[self::NOTE_VARIABLE_SUB_APPLICATION_NUMBER] = $subApplicationDocumentNumber ?? '-';
                        break;

                    case self::NOTE_VARIABLE_NOTE:

                        $noteMessage = $note->note ?? '-';
                        if ($trans == self::NOTE_TRANS_KEY_SAF_SAVE_NOT ||
                            $trans == self::NOTE_TRANS_KEY_SUB_APP_SAVE_NOT ||
                            ($trans == self::NOTE_TRANS_KEY_MESSAGE_SEND_FROM_SAF_TO_SUB_APP && $note->type == self::NOTE_TYPE_MESSAGE) ||
                            ($trans == self::NOTE_TRANS_KEY_SEND_FROM_SUB_APP_TO_SAF && $note->type == self::NOTE_TYPE_NOTIFICATION)
                        ) {
                            $noteMessage = "<b>$noteMessage</b>";
                        }

                        $dataVariablesValues[self::NOTE_VARIABLE_NOTE] = $noteMessage;
                        break;

                    case self::NOTE_VARIABLE_ACTION_NAME:

                        $dataLineVariables = getStringsBetween($note->note, '|', '|');

                        foreach ($dataLineVariables as $dataLineVariable) {
                            $lineVariable = explode('_', $dataLineVariable);

                            if (isset($lineVariable[0]) && $lineVariable[0] == 'ACTION') {

                                $actionName = ConstructorActions::getActionName($lineVariable[1]);

                                $dataVariablesValues[self::NOTE_VARIABLE_ACTION_NAME] = $actionName;
                            }
                        }
                        break;

                    case self::NOTE_VARIABLE_STATE_NAME:

                        $dataLineVariables = getStringsBetween($note->note, '|', '|');

                        foreach ($dataLineVariables as $dataLineVariable) {
                            $lineVariable = explode('_', $dataLineVariable);

                            if (isset($lineVariable[0]) && $lineVariable[0] == 'STATE') {

                                $stateName = ConstructorStates::getStateName($lineVariable[1]);

                                $dataVariablesValues[self::NOTE_VARIABLE_STATE_NAME] = $stateName;
                            }
                        }
                        break;

                    case self::NOTE_VARIABLE_OBLIGATION_TYPE:
                        $obligation = (new self)->getObligationData($note->obligation_id);
                        $dataVariablesValues[self::NOTE_VARIABLE_OBLIGATION_TYPE] = $obligation->obligation_type ?? '';
                        break;

                    case self::NOTE_VARIABLE_OBLIGATION_SUM:
                        $obligation = (new self)->getObligationData($note->obligation_id);
                        $dataVariablesValues[self::NOTE_VARIABLE_OBLIGATION_SUM] = $obligation->obligation ?? '';
                        break;

                    case self::NOTE_VARIABLE_SUB_APPLICATION_ID:
                        $dataLineVariable = getStringsBetween($note->note, '|', '|');
                        $lineVariable = explode('_', $dataLineVariable[0]);

                        if (isset($lineVariable[0]) && $lineVariable[0] == 'SIGNATURE') {
                            $dataVariablesValues[self::NOTE_VARIABLE_SUB_APPLICATION_ID] = $lineVariable[1];
                        }
                        break;

                    case self::NOTE_VARIABLE_SIGNATURE_KEY:
                        $dataVariablesValues[self::NOTE_VARIABLE_SIGNATURE_KEY] = trans('swis.saf.notes.digital_signature.check');
                        break;
                }
            }

            foreach ($dataVariablesValues as $variableKey => $variablesValue) {
                $variableKey = '{' . $variableKey . '}';
                if (strpos($transValue, $variableKey) !== false) {

                    $transValue = str_replace($variableKey, trans($variablesValue), $transValue);
                }
            }
            return $transValue;
        }

        return null;
    }

    /**
     * Function to get obligations data
     *
     * @param $obligationId
     * @return SingleApplicationObligations
     */
    private function getObligationData($obligationId)
    {
        return SingleApplicationObligations::select('saf_obligations.obligation', DB::raw("CONCAT('(', reference_obligation_type.code , ') ', reference_obligation_type_ml.name) AS obligation_type"))
            ->join('reference_obligation_budget_line', 'reference_obligation_budget_line.id', '=', 'saf_obligations.ref_budget_line_id')
            ->leftJoin('reference_obligation_type', 'reference_obligation_type.id', '=', 'reference_obligation_budget_line.budget_line')
            ->leftJoin('reference_obligation_type_ml', 'reference_obligation_type_ml.reference_obligation_type_id', '=', 'reference_obligation_type.id')
            ->where('reference_obligation_type_ml.lng_id', cLng('id'))
            ->where('saf_obligations.id', $obligationId)
            ->first();
    }
}
