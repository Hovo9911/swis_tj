<?php

namespace App\Models\NotesOfReference;

use App\Models\BaseModel;

/**
 * Class NotesOfReference
 * @package App\Models\NotesOfReference
 */
class NotesOfReference extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'notes_of_reference';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'type',
        'version',
        'reference_table_id',
        'reference_row_id',
        'reference_row_code',
        'note',
        'old_data',
        'new_data'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'old_data' => 'array',
        'new_data' => 'array'
    ];

    /**
     * @var string
     */
    const ACTION_TYPE_CREATE = 'created';
    const ACTION_TYPE_EDIT = 'edit';

    /**
     * Function to get notes and replace text constants
     *
     * @param $refId
     * @param null $rowId
     * @param null $rowCode
     * @return array
     */
    public static function getNotes($refId, $rowId = null, $rowCode = null)
    {
        $notes = NotesOfReference::select('id', 'version', 'note', 'old_data', 'new_data', 'created_at')
            ->where('reference_table_id', $refId);

        if ($rowCode) {
            $notes->where('reference_row_code', $rowCode);
        } else {
            $notes->where('reference_row_id', $rowId);
        }

        $notes = $notes->orderByDesc()->get();

        return self::generateWithTrans($notes);
    }

    /**
     * Function to generate With Trans
     *
     * @param $notes
     * @return array
     */
    private static function generateWithTrans($notes)
    {
        $transKeys = [
            "TRANS_REFERENCE_NOTES_VIEW" => trans('swis.reference_table.notes.view'),
            "TRANS_REFERENCE_NOTES_EDIT" => trans('swis.reference_table.notes.edited'),
            "TRANS_REFERENCE_NOTES_SHOW_DIFF" => trans('swis.reference_table.notes.show_diff'),
            "TRANS_REFERENCE_NOTES_IMPORTED" => trans('swis.reference_table.notes.imported'),
            "TRANS_REFERENCE_NOTES_CREATED" => trans('swis.reference_table.notes.created'),
            "TRANS_REFERENCE_NOTES_VERSION" => trans('swis.reference_table.notes.version'),
        ];

        foreach ($notes as &$note) {
            $note->note = strReplaceAssoc($transKeys, $note->note);
        }

        return $notes;
    }
}
