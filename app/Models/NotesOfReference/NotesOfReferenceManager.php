<?php

namespace App\Models\NotesOfReference;

use Illuminate\Support\Facades\Auth;

/**
 * Class NotesOfReferenceManager
 * @package App\Models\NotesOfReference
 */
class NotesOfReferenceManager
{
    /**
     * @var string (type of types)
     */
    const REFERENCE_TABLE = 'reference_table';
    const REFERENCE_TABLE_FORMATION = 'reference_table_formation';

    /**
     * @var array
     */
    const ALLOW_ACTIONS = [
        'view',
        'edit',
        'import',
        'created'
    ];

    /**
     * Function to Store data to DB
     *
     * @param $action
     * @param $referenceTableId
     * @param null $referenceTableFormId
     * @param null $refTableCode
     * @param null $oldData
     * @param null $newData
     * @return false|void
     */
    public function store($action, $referenceTableId, $referenceTableFormId = null, $refTableCode = null, $oldData = null, $newData = null)
    {
        if (!in_array($action, self::ALLOW_ACTIONS)) {
            return false;
        }

        $notesOfReferenceLast = NotesOfReference::select('id', 'version')
            ->where('reference_table_id', $referenceTableId)
            ->orderByDesc('version');

        if ($refTableCode) {
            $notesOfReferenceLast->where('reference_row_code', $refTableCode);
        }

        $notesOfReferenceLast = $notesOfReferenceLast->first();

        $incrementedLastId = !is_null($notesOfReferenceLast) ? ++$notesOfReferenceLast->version : 1;

        $data = [
            'type' => self::REFERENCE_TABLE,
            'version' => $incrementedLastId,
            'reference_table_id' => $referenceTableId,
            'reference_row_id' => $referenceTableFormId,
            'reference_row_code' => $refTableCode,
            'note' => $this->getNoteFromType($action, $incrementedLastId),
            'old_data' => $oldData,
            'new_data' => $newData
        ];

        $notesOfReference = new NotesOfReference($data);
        $notesOfReference->save();
    }

    /**
     * Function to generate note text by action
     *
     * @param $action
     * @param $incrementedLastId
     * @return string
     */
    private function getNoteFromType($action, $incrementedLastId)
    {
        $name = Auth::user()->name();
        switch ($action) {
            case 'view':
                return "<b class='text-info'>" . $name . '</b> TRANS_REFERENCE_NOTES_VIEW';
            case 'edit':
                return "<b class='text-info'>" . $name . '</b> TRANS_REFERENCE_NOTES_EDIT <small class="text-muted show-diff collapseWithIcon" data-toggle="collapse" data-target="#notes-' . $incrementedLastId . '"> TRANS_REFERENCE_NOTES_SHOW_DIFF ' . $incrementedLastId . ' <i class="fa fa-chevron-down"></i> </small>';
            case 'created':
                return 'TRANS_REFERENCE_NOTES_CREATED ' . "<b class='text-info'>" . $name . '</b>';
            default:
                return "";
        }
    }

}
