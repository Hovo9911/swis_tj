<?php


namespace App\Models\SingleApplicationObligationReceipts;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

/**
 * Class SingleApplicationObligationReceipts
 * @package App\Models\SingleApplicationObligationReceipts
 */
class SingleApplicationObligationReceipts extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['saf_obligation_id', 'user_id'];

    /**
     * @var string
     */
    public $incrementing = 'false';

    /**
     * @var string
     */
    public $table = 'saf_obligation_receipts';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'saf_obligation_id',
        'file_name',
    ];
}