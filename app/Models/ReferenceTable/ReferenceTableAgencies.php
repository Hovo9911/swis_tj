<?php

namespace App\Models\ReferenceTable;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

/**
 * Class ReferenceTableAgencies
 * @package App\Models\ReferenceTableExport
 */
class ReferenceTableAgencies extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['reference_table_id', 'agency_id'];

    /**
     * @var string
     */
    public $incrementing = 'false';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    public $table = 'reference_table_agencies';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'reference_table_id',
        'agency_id',
        'access',
        'show_only_self'
    ];
}
