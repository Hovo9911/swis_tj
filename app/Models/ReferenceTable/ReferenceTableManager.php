<?php

namespace App\Models\ReferenceTable;

use App\Http\Controllers\Core\Dictionary\Dictionary;
use App\Http\Controllers\Core\Dictionary\DictionaryManager;
use App\Http\Controllers\Core\Dictionary\DictionaryMl;
use App\Models\BaseMlManager;
use App\Models\Languages\Language;
use App\Models\Menu\Menu;
use App\Models\NotesOfReference\NotesOfReferenceManager;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * Class ReferenceTableManager
 * @package App\Models\ReferenceTableExport
 */
class ReferenceTableManager
{
    use BaseMlManager;

    /**
     * Function to store data to DB
     *
     * @param array $data
     * @return ReferenceTable
     */
    public function store(array $data)
    {
        $referenceTableData = [
            'manually_edit' => $data['manually_edit'],
            'available_at' => $data['available_at'],
            'table_name' => $data['table_name'],
            'user_id' => Auth::id(),
            'import_format' => $data['import_format'] ?? null,
            'api_url' => $data['api_url'] ?? null,
            'show_status' => $data['show_status']
        ];

        $referenceTable = new ReferenceTable($referenceTableData);
        $referenceTableMl = new ReferenceTableMl();

        return DB::transaction(function () use ($data, $referenceTable, $referenceTableMl) {

            $referenceTable->save();
            $this->storeMl($data['ml'], $referenceTable, $referenceTableMl);

            if (isset($data['agencies'])) {
                $this->storeReferenceTableAgencies($data['agencies'], $referenceTable);
            }

            // Reference Table Structure part
            $referenceInputDataStructure = $data['structure'] ?? [];
            $defaultWithUserCreatedStructures = array_merge($this->defaultStructures(), $referenceInputDataStructure);

            $refStructure = $this->storeReferenceTableStructure($defaultWithUserCreatedStructures, $referenceTable);
            $this->createReferenceTableDbStructure($referenceTable, $refStructure, $data['ml']);

            // Notes
            $notesOfReferenceManager = new NotesOfReferenceManager();
            $notesOfReferenceManager->store('created', $referenceTable->id, null, null, null, null);

            // Send Notification to SW_Admin
            $this->sendNotification($referenceTable);

            // Create in Menu
            $this->createMenuTrans($data['ml'], $referenceTable);

            return $referenceTable;
        });
    }

    /**
     * Function to Update data
     *
     * @param array $data
     * @param int $id
     * @return null|void
     */
    public function update(array $data, int $id)
    {
        $referenceTable = ReferenceTable::where('id', $id)->checkUserHasRole()->firstOrFail();

        // Inactive
        if ($referenceTable->show_status == ReferenceTable::STATUS_INACTIVE && $data['show_status'] == ReferenceTable::STATUS_INACTIVE) {
            return null;
        }

        // To Inactive
        if ($referenceTable->show_status == ReferenceTable::STATUS_ACTIVE && $data['show_status'] == ReferenceTable::STATUS_INACTIVE) {

            // Send Notification to SW_Admin
            $this->sendNotification($referenceTable);

            // Update menu status
            $this->updateMenuStatus($referenceTable->id, ReferenceTable::STATUS_INACTIVE);

            return $referenceTable->update(['show_status' => ReferenceTable::STATUS_INACTIVE]);
        }

        // Is Inactive
        if ($referenceTable->show_status == ReferenceTable::STATUS_INACTIVE && $data['show_status'] == ReferenceTable::STATUS_ACTIVE) {

            // Send Notification to SW_Admin
            $this->sendNotification($referenceTable);

            // Update menu status
            $this->updateMenuStatus($referenceTable->id, ReferenceTable::STATUS_ACTIVE);

            return $referenceTable->update(['show_status' => ReferenceTable::STATUS_ACTIVE]);
        }

        $referenceTableData = [
            'user_id' => Auth::id(),
            'table_name' => (!empty($referenceTable->table_name)) ? $referenceTable->table_name : $data['table_name'],
            'manually_edit' => $data['manually_edit'],
            'available_at' => $data['available_at'],
            'import_format' => $data['import_format'] ?? null,
            'api_url' => $data['api_url'] ?? null,
            'show_status' => $data['show_status']
        ];

        // Ml
        $referenceTableMl = new ReferenceTableMl();

        //
        $oldData = $this->generateNotesData($referenceTable);

        DB::transaction(function () use ($data, $oldData, $id, $referenceTable, $referenceTableMl, $referenceTableData) {

            $referenceTable->update($referenceTableData);
            $this->updateMl($data['ml'], 'reference_table_id', $referenceTable, $referenceTableMl);

            // Structures
            $referenceInputDataStructure = $data['structure'] ?? [];
            $newStructures = $this->updateReferenceTableStructure($referenceInputDataStructure, $referenceTable);

            // If has new structures update
            if ($newStructures->count()) {
                $this->createdReferenceTableAddNewColumn('reference_' . $referenceTable->table_name, $newStructures);
            }

            $this->updateReferenceTableAgencies($data, $referenceTable);

            // Send Notification to SW Admin
            $this->sendNotification($referenceTable);

            // Notes
            $notesOfReferenceManager = new NotesOfReferenceManager();
            $newData = $this->generateNotesData(ReferenceTable::where('id', $id)->firstOrFail());
            $notesOfReferenceManager->store('edit', $referenceTable->id, null, null, $oldData, $newData);

            // Update in menu
            $this->updateMenuTrans($data['ml'], $referenceTable->table_name);

        });
    }

    /**
     * Function to store Reference Table Agencies
     *
     * @param $agencies
     * @param $referenceTable
     */
    private function storeReferenceTableAgencies($agencies, $referenceTable)
    {
        $data = [];
        foreach ($agencies as $agency) {
            if (!empty($agency['id'])) {
                $referenceAgency = [
                    'agency_id' => $agency['id'],
                    'access' => $agency['access'],
                    'show_only_self' => $agency['show_only_self'] ?? '0',
                ];

                $data[] = new ReferenceTableAgencies($referenceAgency);
            }
        }

        $referenceTable->agencies()->saveMany($data);
    }

    /**
     * Function to update Reference Table Agencies
     *
     * @param $allData
     * @param $referenceTable
     */
    private function updateReferenceTableAgencies($allData, $referenceTable)
    {
        if (isset($allData['agencies'])) {

            $data = $allAgencies = [];
            $agencies = $allData['agencies'];
            foreach ($agencies as $agency) {
                if (!empty($agency['id'])) {

                    $allAgencies[] = $agency['id'];
                    $showOnlySelf = $agency['show_only_self'] ?? '0';

                    $refAgency = ReferenceTableAgencies::select('id')->where(['agency_id' => $agency['id'], 'reference_table_id' => $referenceTable->id])->first();
                    if (!is_null($refAgency)) {
                        ReferenceTableAgencies::where(['agency_id' => $agency['id'], 'reference_table_id' => $referenceTable->id])->update(['access' => $agency['access'], 'show_only_self' => $showOnlySelf]);
                    } else {

                        $referenceAgency = [
                            'agency_id' => $agency['id'],
                            'access' => $agency['access'],
                            'show_only_self' => $showOnlySelf,
                        ];

                        $data[] = new ReferenceTableAgencies($referenceAgency);
                    }
                }
            }

            if (count($allAgencies)) {
                ReferenceTableAgencies::where(['reference_table_id' => $referenceTable->id])->whereNotIn('agency_id', $allAgencies)->delete();
            }

            $referenceTable->agencies()->saveMany($data);
        } else {
            ReferenceTableAgencies::where(['reference_table_id' => $referenceTable->id])->delete();
        }
    }

    /**
     * Function to store Reference Table Structure
     *
     * @param $structures
     * @param $referenceTable
     * @return ReferenceTableStructure
     */
    private function storeReferenceTableStructure($structures, $referenceTable)
    {
        $data = [];
        foreach ($structures as $structure) {

            $structureType = $structure['type'];

            // Number, Text
            if ($structureType == ReferenceTableStructure::COLUMN_TYPE_NUMBER || $structureType == ReferenceTableStructure::COLUMN_TYPE_TEXT) {
                $structure['parameters'] = ['min' => $structure['min'], 'max' => $structure['max']];
            }

            // Decimal
            if ($structureType == ReferenceTableStructure::COLUMN_TYPE_DECIMAL) {
                $structure['parameters'] = ['left' => $structure['left'], 'right' => $structure['right']];
            }

            // Date, Datetime
            if ($structureType == ReferenceTableStructure::COLUMN_TYPE_DATE || $structureType == ReferenceTableStructure::COLUMN_TYPE_DATETIME) {
                $structure['parameters'] = ['start' => $structure['start'], 'end' => $structure['end']];
            }

            // Autocomplete
            if ($structureType == ReferenceTableStructure::COLUMN_TYPE_AUTOCOMPLETE) {
                $structure['parameters'] = ['description' => $structure['description'] ?? '', 'source' => $structure['source']];

                if (!empty($structure['fields'])) {
                    $structure['parameters']['fields'] = $structure['fields'];
                }
            }

            // Select
            if ($structureType == ReferenceTableStructure::COLUMN_TYPE_SELECT) {
                $structure['parameters'] = ['source' => $structure['source']];
            }

            $data[] = new ReferenceTableStructure($structure);

        }

        return $referenceTable->structure()->saveMany($data);
    }

    /**
     * Function to update Reference Table Structure
     *
     * @param $structures
     * @param $referenceTableFormation
     * @return ReferenceTableStructure|Collection
     */
    private function updateReferenceTableStructure($structures, $referenceTableFormation)
    {
        $data = collect();
        foreach ($structures as $structure) {

            $structureType = $structure['type'];

            $structure['required'] = $structure['required'] ?? 0;
            $structure['searchable'] = $structure['searchable'] ?? 0;
            $structure['search_filter'] = $structure['search_filter'] ?? 0;
            $structure['search_result'] = $structure['search_result'] ?? 0;
            $structure['sortable'] = $structure['sortable'] ?? 0;

            // Number, Text
            if ($structureType == ReferenceTableStructure::COLUMN_TYPE_NUMBER || $structureType == ReferenceTableStructure::COLUMN_TYPE_TEXT) {
                $structure['parameters'] = ['min' => $structure['min'], 'max' => $structure['max']];
            }

            // Decimal
            if ($structureType == ReferenceTableStructure::COLUMN_TYPE_DECIMAL) {
                $structure['parameters'] = ['left' => $structure['left'], 'right' => $structure['right']];
            }

            // Date, Datetime
            if ($structureType == ReferenceTableStructure::COLUMN_TYPE_DATE || $structureType == ReferenceTableStructure::COLUMN_TYPE_DATETIME) {
                $structure['parameters'] = ['start' => $structure['start'], 'end' => $structure['end']];
            }

            // Autocomplete
            if ($structureType == ReferenceTableStructure::COLUMN_TYPE_AUTOCOMPLETE) {
                $structure['parameters'] = ['description' => $structure['description'] ?? '', 'source' => $structure['source']];

                if (!empty($structure['fields'])) {
                    $structure['parameters']['fields'] = $structure['fields'];
                }
            }

            // Select
            if ($structureType == ReferenceTableStructure::COLUMN_TYPE_SELECT) {
                $structure['parameters'] = ['source' => $structure['source']];
            }

            if (empty($structure['id'])) {

                $structure['has_ml'] = $structure['has_ml'] ?? 0;

                $data[] = new ReferenceTableStructure($structure);

            } else {

                $refTableStructure = new ReferenceTableStructure($structure);
                $refStructure = ReferenceTableStructure::find($structure['id']);

                if ($structureType == ReferenceTableStructure::COLUMN_TYPE_TEXT || $structureType == ReferenceTableStructure::COLUMN_TYPE_NUMBER) {

                    $currentFieldMaxSize = $refStructure->parameters['max'];
                    $inputFieldMaxSize = $structure['max'];

                    if ($inputFieldMaxSize > $currentFieldMaxSize) {

                        $refTable = 'reference_' . $referenceTableFormation->table_name;
                        $refTableMl = 'reference_' . $referenceTableFormation->table_name . '_ml';
                        $fieldName = $refTableStructure->field_name;

                        $changingRefTableName = $structure['has_ml'] ? $refTableMl : $refTable;

                        Schema::table($changingRefTableName, function ($table) use ($inputFieldMaxSize, $fieldName) {
                            $table->string($fieldName, $inputFieldMaxSize)->change();
                        });
                    }


                    if ($refStructure->field_name != $structure['field_name']) {

                        /*    $fromName = $createdRef->field_name;
                            $toName = $structure['field_name'];*/
                    }
                }

                $refStructure->update($refTableStructure->toArray());
            }

        }

        return $referenceTableFormation->structure()->saveMany($data);
    }

    /**
     * Function to create ref item in menu
     *
     * @param $mlsData
     * @param $referenceTable
     */
    private function createMenuTrans($mlsData, $referenceTable)
    {
        $dictionaryKey = 'swis.reference.title.' . $referenceTable->table_name;

        $dictionary = new Dictionary([
            'key' => $dictionaryKey,
            'application_type' => config('app.type')
        ]);

        $dicData = $updatedLngCodes = [];
        foreach ($mlsData as $lngId => $ml) {
            $lngCode = Language::where('id', $lngId)->pluck('code')->first();

            $updatedLngCodes[] = $lngCode;
            $mlData = [
                'dictionary_id' => $dictionary->id,
                'lng_id' => $lngId,
                'value' => $ml['name']
            ];

            $dicData[] = new DictionaryMl($mlData);
        }

        $dictionary->save();
        $dictionary->ml()->saveMany($dicData);

        //
        if (count($updatedLngCodes)) {
            $dictionaryManager = new DictionaryManager();

            foreach ($updatedLngCodes as $locale) {
                $trans = $dictionaryManager->getTransFromDB(config('app.type'), $locale);
                $dictionaryManager->updateTransJsonFile($trans, $locale);
            }
        }

        Menu::create([
            'title' => $dictionaryKey,
            'sort_order' => 0,
            'href' => 'reference-table-form/' . $referenceTable->id,
            'parent_id' => Menu::getMenuIdByName(Menu::REFERENCE_TABLE_FORM_MENU_NAME),
            'show_status' => $referenceTable->show_status ?: ReferenceTable::STATUS_ACTIVE
        ]);
    }

    /**
     * Function to update Menu ref item title
     *
     * @param $mlsData
     * @param $referenceTableName
     */
    private function updateMenuTrans($mlsData, $referenceTableName)
    {
        $dictionaryKey = 'swis.reference.title.' . $referenceTableName;
        $dictionary = Dictionary::where('key', $dictionaryKey)->first();

        if (!is_null($dictionary)) {

            $updatedLngCodes = [];
            foreach ($mlsData as $lngId => $mlData) {
                $lngCode = Language::where('id', $lngId)->pluck('code')->first();
                $updatedLngCodes[] = $lngCode;

                DictionaryMl::where('dictionary_id', $dictionary->id)->where('lng_id', $lngId)->update(['value' => $mlData['name']]);
            }

            //
            if (count($updatedLngCodes)) {
                $dictionaryManager = new DictionaryManager();

                foreach ($updatedLngCodes as $locale) {
                    $trans = $dictionaryManager->getTransFromDB(config('app.type'), $locale);
                    $dictionaryManager->updateTransJsonFile($trans, $locale);
                }
            }
        }
    }

    /**
     * Function to update in menu ref href (show or hide)
     *
     * @param $referenceId
     * @param $refShowStatus
     */
    private function updateMenuStatus($referenceId, $refShowStatus)
    {
        Menu::where(['href' => 'reference-table-form/' . $referenceId])->update(['show_status' => $refShowStatus]);
    }

    /**
     * Function to return old and new data
     *
     * @param $referenceTable
     * @param array $newAgencies
     * @return array
     */
    private function generateNotesData($referenceTable, $newAgencies = [])
    {
        $technicalFields = ['created_admin_id', 'created_admin_ip', 'updated_admin_id', 'updated_admin_ip', 'deleted_admin_id', 'deleted_admin_ip', 'created_at', 'updated_at', 'show_status'];
        $ml = $referenceTable->ml->sortBy('lng_id')->toArray();
        $structure = $referenceTable->structure->toArray();
        $agencies = $referenceTable->agencies->toArray();

        return $this->removeTechnicalFields($technicalFields, $referenceTable, $ml, $structure, $agencies);
    }

    /**
     * Function to send notification to SW_Admin
     *
     * @param $referenceTable
     */
    private function sendNotification($referenceTable)
    {
        $referenceTableNotificationsManager = new ReferenceTableNotificationsManager();
        $referenceTableNotificationsManager->storeNotification($referenceTable);
    }

    /**
     * Function to return array without technical fields
     *
     * @param $technicalFields
     * @param $data
     * @param $ml
     * @param $structure
     * @param $agencies
     * @return array
     */
    private function removeTechnicalFields($technicalFields, $data, $ml, $structure, $agencies)
    {
        $fields = [];
        foreach ($data as $field => $value) {
            if (!in_array($field, $technicalFields)) {
                $fields[$field] = $value;
            }
        }

        $fields['ml'] = $ml;
        $fields['structure'] = $structure;
        $fields['agencies'] = $agencies;

        return $fields;
    }

    /**
     * Function to Create Reference Table Db Structure
     *
     * @param $referenceTable
     * @param $refStructure
     * @param $mls
     */
    private function createReferenceTableDbStructure($referenceTable, $refStructure, $mls = [])
    {
        $refTableName = $referenceTable->table_name;

        Schema::create('reference_' . $refTableName, function (Blueprint $table) use ($refStructure, $refTableName) {

            $table->increments('id');
            $table->string('company_tax_id')->default('0');
            $table->unsignedInteger('user_id')->nullable();

            foreach ($refStructure as $structure):
                if (!$structure->has_ml) {

                    switch ($structure->type):

                        // Number  // Select
                        case ReferenceTableStructure::COLUMN_TYPE_NUMBER:
                        case ReferenceTableStructure::COLUMN_TYPE_SELECT:
                            $table->unsignedInteger($structure->field_name)->nullable();
                            break;

                        // Decimal
                        case ReferenceTableStructure::COLUMN_TYPE_DECIMAL:
                            $table->string($structure->field_name)->nullable();
                            break;

                        // Date
                        case ReferenceTableStructure::COLUMN_TYPE_DATE:
                            $table->date($structure->field_name)->nullable();
                            break;

                        // Datetime
                        case ReferenceTableStructure::COLUMN_TYPE_DATETIME:
                            $table->dateTime($structure->field_name)->nullable();
                            break;

                        // Text
                        case ReferenceTableStructure::COLUMN_TYPE_TEXT:
                            $table->string($structure->field_name, $structure->parameters['max'])->nullable();
                            break;

                        // Autocomplete
                        case ReferenceTableStructure::COLUMN_TYPE_AUTOCOMPLETE:
                            $table->text($structure->field_name . '_description')->nullable();
                            $table->unsignedInteger($structure->field_name)->nullable();
                            break;

                    endswitch;

                }

            endforeach;

            $table->integer('created_admin_id')->unsigned()->nullable();
            $table->string('created_admin_ip')->default('');
            $table->integer('updated_admin_id')->unsigned()->nullable();
            $table->string('updated_admin_ip')->default('');
            $table->integer('deleted_admin_id')->unsigned()->nullable();
            $table->string('deleted_admin_ip')->default('');
            $table->timestamps();
            $table->enum('show_status', ['1', '2', '0'])->default(1);

        });

        // ML DB SCHEMA
        Schema::create('reference_' . $refTableName . '_ml', function (Blueprint $table) use ($refStructure, $refTableName) {

            $table->integer('reference_' . $refTableName . '_id');
            $table->unsignedSmallInteger('lng_id');
            foreach ($refStructure as $structure):

                if ($structure->has_ml) {
                    switch ($structure->type):

                        // Number   // Select  // Autocomplete
                        case ReferenceTableStructure::COLUMN_TYPE_NUMBER:
                        case ReferenceTableStructure::COLUMN_TYPE_SELECT:
                        case ReferenceTableStructure::COLUMN_TYPE_AUTOCOMPLETE:
                            $table->unsignedInteger($structure->field_name)->nullable();
                            break;

                        // Decimal
                        case ReferenceTableStructure::COLUMN_TYPE_DECIMAL:
//                            $table->decimal($structure->field_name, ($structure->parameters['left'] + $structure->parameters['right']), $structure->parameters['right'])->nullable();
                            $table->string($structure->field_name)->nullable();
                            break;

                        // Date
                        case ReferenceTableStructure::COLUMN_TYPE_DATE:
                            $table->date($structure->field_name)->nullable();
                            break;

                        // Datetime
                        case ReferenceTableStructure::COLUMN_TYPE_DATETIME:
                            $table->dateTime($structure->field_name)->nullable();
                            break;

                        // Text
                        case ReferenceTableStructure::COLUMN_TYPE_TEXT:
                            $table->string($structure->field_name, $structure->parameters['max'])->nullable();
                            break;

                    endswitch;

                }

            endforeach;

            $table->enum('show_status', ['1', '2', '0'])->default(1);

        });

    }

    /**
     * Function to Created Reference Table Add New Column
     *
     * @param $tableName
     * @param $refStructure
     */
    private function createdReferenceTableAddNewColumn($tableName, $refStructure)
    {
        foreach ($refStructure as $structure):

            if (!$structure->has_ml) {
                Schema::table($tableName, function ($table) use ($refStructure, $structure) {
                    switch ($structure->type):

                        // Number  // Select
                        case ReferenceTableStructure::COLUMN_TYPE_NUMBER:
                        case ReferenceTableStructure::COLUMN_TYPE_SELECT:
                            $table->unsignedInteger($structure->field_name)->nullable();
                            break;

                        // Decimal
                        case ReferenceTableStructure::COLUMN_TYPE_DECIMAL:
                            $table->string($structure->field_name)->nullable();
                            break;

                        // Date
                        case ReferenceTableStructure::COLUMN_TYPE_DATE:
                            $table->date($structure->field_name)->nullable();
                            break;

                        // Datetime
                        case ReferenceTableStructure::COLUMN_TYPE_DATETIME:
                            $table->dateTime($structure->field_name)->nullable();
                            break;

                        // Text
                        case ReferenceTableStructure::COLUMN_TYPE_TEXT:
                            $table->string($structure->field_name, $structure->parameters['max'])->nullable();
                            break;

                        // Autocomplete
                        case ReferenceTableStructure::COLUMN_TYPE_AUTOCOMPLETE:
                            $table->unsignedInteger($structure->field_name)->nullable();
                            $table->text($structure->field_name . '_description')->nullable();
                            break;

                    endswitch;

                });
            }

            // Ml
            if ($structure->has_ml) {
                Schema::table($tableName . '_ml', function ($table) use ($refStructure, $structure) {

                    switch ($structure->type):

                        // Number   // Select   // Autocomplete
                        case ReferenceTableStructure::COLUMN_TYPE_NUMBER:
                        case ReferenceTableStructure::COLUMN_TYPE_SELECT:
                        case ReferenceTableStructure::COLUMN_TYPE_AUTOCOMPLETE:
                            $table->unsignedInteger($structure->field_name)->nullable();
                            break;

                        // Decimal
                        case ReferenceTableStructure::COLUMN_TYPE_DECIMAL:
                            $table->string($structure->field_name)->nullable();
                            break;

                        // Date
                        case ReferenceTableStructure::COLUMN_TYPE_DATE:
                            $table->date($structure->field_name)->nullable();
                            break;

                        // Datetime
                        case ReferenceTableStructure::COLUMN_TYPE_DATETIME:
                            $table->dateTime($structure->field_name)->nullable();
                            break;

                        // Text
                        case ReferenceTableStructure::COLUMN_TYPE_TEXT:
                            $table->string($structure->field_name, $structure->parameters['max'])->nullable();
                            break;

                    endswitch;

                });

            }

        endforeach;

    }

    /**
     * Function to store default structure data
     *
     * @return array
     */
    public function defaultStructures()
    {
        return [
            [
                'field_name' => 'code',
                'type' => ReferenceTableStructure::COLUMN_TYPE_TEXT,
                'min' => '1',
                'max' => '50',
                'required' => ReferenceTable::TRUE,
                'searchable' => ReferenceTable::TRUE,
                'search_filter' => ReferenceTable::TRUE,
                'search_result' => ReferenceTable::TRUE,
                'sort_order' => '1',
                'sortable' => ReferenceTable::TRUE,
                'has_ml' => ReferenceTable::FALSE,
                'show_status' => ReferenceTable::STATUS_ACTIVE
            ],
            [
                'field_name' => 'name',
                'type' => ReferenceTableStructure::COLUMN_TYPE_TEXT,
                'min' => '1',
                'max' => '255',
                'required' => ReferenceTable::TRUE,
                'searchable' => ReferenceTable::TRUE,
                'search_filter' => ReferenceTable::TRUE,
                'search_result' => ReferenceTable::TRUE,
                'sort_order' => '2',
                'sortable' => ReferenceTable::TRUE,
                'has_ml' => ReferenceTable::TRUE,
                'show_status' => ReferenceTable::STATUS_ACTIVE
            ],
            [
                'field_name' => 'start_date',
                'type' => ReferenceTableStructure::COLUMN_TYPE_DATE,
                'start' => '',
                'end' => '',
                'required' => ReferenceTable::TRUE,
                'searchable' => ReferenceTable::TRUE,
                'search_filter' => ReferenceTable::TRUE,
                'search_result' => ReferenceTable::TRUE,
                'sort_order' => '3',
                'sortable' => ReferenceTable::TRUE,
                'has_ml' => ReferenceTable::FALSE,
                'show_status' => ReferenceTable::STATUS_ACTIVE
            ],
            [
                'field_name' => 'end_date',
                'type' => ReferenceTableStructure::COLUMN_TYPE_DATE,
                'start' => '',
                'end' => '',
                'required' => ReferenceTable::FALSE,
                'searchable' => ReferenceTable::TRUE,
                'search_filter' => ReferenceTable::TRUE,
                'search_result' => ReferenceTable::TRUE,
                'sort_order' => '4',
                'sortable' => ReferenceTable::TRUE,
                'has_ml' => ReferenceTable::FALSE,
                'show_status' => ReferenceTable::STATUS_ACTIVE
            ],
        ];
    }

    /**
     * Function to search ref by name
     *
     * @param $searchString
     * @param null $all
     * @return array
     */
    public function searchReferenceTables($searchString, $all = null)
    {
        $results = ReferenceTable::select('reference_table.table_name', 'reference_table_ml.name')
            ->join('reference_table_ml', 'reference_table.id', '=', 'reference_table_ml.reference_table_id')
            ->where('reference_table_ml.lng_id', '=', cLng('id'))
            ->where(function ($q) use ($searchString) {
                $q->where('table_name', 'ILIKE', $searchString . '%')->orWhere('name', 'ILIKE', '%' . $searchString . '%');
            });
        if (is_null($all)) {
            $results->where('reference_table.show_status', ReferenceTable::STATUS_ACTIVE);
        }
        $results = $results->get();

        $values = [];
        if ($results->count()) {
            foreach ($results as $result) {
                $values[] = [
                    'id' => $result->table_name,
                    'name' => $result->name
                ];
            }
        }

        return $values;
    }

    /**
     * Function to get all ref without current
     *
     * @param $id
     * @return ReferenceTable
     */
    public function allDataWithoutCurrent($id)
    {
        return ReferenceTable::joinMl()->notDeleted()->where('id', '!=', $id)->checkUserHasRole()->orderBy('reference_table_ml.name')->get();
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @param null $parentId
     * @return array
     */
    public function getLogData($id = null, $parentId = null)
    {
        if (!is_null($id)) {
            $referenceTable = ReferenceTable::where('id', $id)->first()->toArray();
            $structure = ReferenceTableStructure::where('reference_table_id', $referenceTable['id'])->get()->toArray();

            return $referenceTable['structure'] = $structure;
        }

        return [];
    }
}
