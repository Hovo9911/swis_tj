<?php

namespace App\Models\ReferenceTable;

use App\Http\Controllers\Core\DataTableSearch;
use Illuminate\Support\Facades\DB;

/**
 * Class ReferenceTableSearch
 * @package App\Models\ReferenceTableExport
 */
class ReferenceTableSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);

        $data = $query->get();
        foreach ($data as &$item){
            $item->available_at = formattedDate($item->available_at);
        }

        return $data;
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $orderByType = 'desc';
        if ($this->orderByType == 'asc') {
            $orderByType = 'asc';
        }

        $tablePrefix = 'reference_table';
        if ($this->orderByCol == 'name') {
            $tablePrefix = 'reference_table_ml';
        }

        $query->orderBy($tablePrefix . '.' . $this->orderByCol, $orderByType);
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $query = ReferenceTable::select('*', 'reference_table.show_status as show_status')->joinMl()->notDeleted();

        if (isset($this->searchData['id'])) {
            $query->where(DB::raw("reference_table.id::varchar"), $this->searchData['id']);
        }

        if (isset($this->searchData['name'])) {
            $query->where('reference_table_ml.name', 'ILIKE', '%' . strtolower(trim($this->searchData['name'])) . '%');
        }

        if (isset($this->searchData['table_name'])) {
            $query->where('reference_table.table_name', 'ILIKE', '%' . strtolower(trim(preg_replace('/reference_/', '', $this->searchData['table_name'])) . '%'));
        }

        if (isset($this->searchData['start_date_start'])) {
            $query->where('reference_table.available_at', '>=', $this->searchData['start_date_start']);
        }

        if (isset($this->searchData['start_date_end'])) {
            $query->where('reference_table.available_at', '<=', $this->searchData['start_date_end']);
        }

        if (isset($this->searchData['show_status']) && $this->searchData['show_status'] != 'all') {
            $query->where('reference_table.show_status', $this->searchData['show_status']);
        }

        $query->checkUserHasRole();

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}
