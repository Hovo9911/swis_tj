<?php

namespace App\Models\ReferenceTable;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

/**
 * Class ReferenceTableMl
 * @package App\Models\ReferenceTableExport
 */
class ReferenceTableMl extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var array
     */
    protected $primaryKey = ['reference_table_id', 'lng_id'];

    /**
     * @var string
     */
    public $incrementing = 'false';

    /**
     * @var string
     */
    public $table = 'reference_table_ml';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'reference_table_id',
        'lng_id',
        'name',
        'show_status'
    ];
}
