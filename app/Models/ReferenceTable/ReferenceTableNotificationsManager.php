<?php

namespace App\Models\ReferenceTable;

use App\Models\Notifications\Notifications;
use App\Models\Notifications\NotificationsManager;
use App\Models\NotificationTemplates\NotificationTemplates;
use App\Models\User\User;
use Illuminate\Support\Facades\DB;

/**
 * Class ReferenceTableNotificationsManager
 * @package App\Models\ReferenceTable
 */
class ReferenceTableNotificationsManager
{
    /**
     * Function to Store Notifications and Send Email and SMS
     *
     * @param $referenceTable
     */
    public function storeNotification($referenceTable)
    {
        DB::transaction(function () use ($referenceTable) {

            $notificationTemplatesAll = NotificationTemplates::mlByCode(NotificationTemplates::REFERENCE_TABLE_CHANGES_TEMPLATE);

            if ($notificationTemplatesAll->count()) {
                $constructorNotificationCurrentLng = $notificationTemplatesAll->where('lng_id', cLng('id'))->first();

                foreach (User::getSwAdmins() as $user) {

                    $contentVariables = [
                        "{{USER_FULLNAME}}" => $user->first_name . " " . $user->last_name,
                        "{{REFERENCE_TABLE}}" => $referenceTable->id
                    ];

                    //
                    $notification = [
                        'notification_id' => $constructorNotificationCurrentLng->id,
                        'user_id' => $user->id,
                        'company_tax_id' => 0,
                        'notification_type' => Notifications::NOTIFICATION_TYPE_SW_ADMIN,
                        'email_subject' => strip_tags(Notifications::replaceVariableToValue($contentVariables, $constructorNotificationCurrentLng->email_subject, ($user->lng_id ?? cLng('id')))),
                        'email_notification' => Notifications::replaceVariableToValue($contentVariables, $constructorNotificationCurrentLng->email_notification, ($user->lng_id ?? cLng('id'))),
                        'sms_notification' => strip_tags(Notifications::replaceVariableToValue($contentVariables, $constructorNotificationCurrentLng->sms_notification, ($user->lng_id ?? cLng('id')))),
                    ];

                    //
                    $inApplicationNotes = [];
                    foreach ($notificationTemplatesAll as $constructorNote) {
                        $inApplicationNotes[$constructorNote->lng_id] = [
                            'in_app_notification' => Notifications::replaceVariableToValue($contentVariables, $constructorNote->in_app_notification, $constructorNote->lng_id)
                        ];
                    }

                    // Store in app notifications
                    NotificationsManager::storeInAppNotifications($notification, $inApplicationNotes);

                    // Send Email And SMS
                    NotificationsManager::sendEmailAndSms($user, $notification);
                }
            }

        });
    }
}