<?php

namespace App\Models\ReferenceTable;

use App\Models\BaseModel;

/**
 * Class ReferenceTableStructure
 * @package App\Models\ReferenceTableExport
 */
class ReferenceTableStructure extends BaseModel
{
    /**
     * @var string (Reference table structure column types)
     */
    const COLUMN_TYPE_TEXT = 'text';
    const COLUMN_TYPE_NUMBER = 'number';
    const COLUMN_TYPE_DECIMAL = 'decimal';
    const COLUMN_TYPE_DATE = 'date';
    const COLUMN_TYPE_DATETIME = 'datetime';
    const COLUMN_TYPE_SELECT = 'select';
    const COLUMN_TYPE_AUTOCOMPLETE = 'autocomplete';

    /**
     * @var array
     */
    const COLUMN_TYPES = [
        self::COLUMN_TYPE_TEXT,
        self::COLUMN_TYPE_NUMBER,
        self::COLUMN_TYPE_DECIMAL,
        self::COLUMN_TYPE_DATE,
        self::COLUMN_TYPE_DATETIME,
        self::COLUMN_TYPE_SELECT,
        self::COLUMN_TYPE_AUTOCOMPLETE,
    ];

    /**
     * @var string (Reference validation structure type)
     */
    const COLUMN_VALIDATION_TYPE_STRING = 'string';
    const COLUMN_VALIDATION_TYPE_INTEGER = 'integer';
    const COLUMN_VALIDATION_TYPE_NUMERIC = 'numeric';

    /**
     * @var array
     */
    const VALIDATION_TYPES = [
        self::COLUMN_TYPE_TEXT => self::COLUMN_VALIDATION_TYPE_STRING,
        self::COLUMN_TYPE_NUMBER => self::COLUMN_VALIDATION_TYPE_INTEGER,
        self::COLUMN_TYPE_DECIMAL => self::COLUMN_VALIDATION_TYPE_NUMERIC,
        self::COLUMN_TYPE_SELECT => self::COLUMN_VALIDATION_TYPE_INTEGER
    ];

    /**
     * @var string
     */
    public $table = 'reference_table_structures';

    /**
     * @var bool
     */
    protected $hasAdminInfo = true;

    /**
     * @var array
     */
    protected $fillable = [
        'reference_agencies_id',
        'code',
        'name',
        'field_name',
        'type',
        'parameters',
        'required',
        'searchable',
        'search_filter',
        'search_result',
        'sort_order',
        'sortable',
        'has_ml',
        'show_status'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'created_admin_id',
        'created_admin_ip',
        'updated_at',
        'updated_admin_id',
        'updated_admin_ip',
        'deleted_admin_id',
        'deleted_admin_ip'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'parameters' => 'array'
    ];

    /**
     * Function to relate wit reference_table table
     */
    public function referenceTable()
    {
        return $this->hasOne(ReferenceTable::class, 'id', 'reference_table_id');
    }
}
