<?php

namespace App\Models\ReferenceTable;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class ReferenceTableExport
 * @package App\Models\ReferenceTableExport
 */
class ReferenceTable extends BaseModel
{
    /**
     * @var string
     */
    const REFERENCE_TABLE_PREFIX = 'reference_';

    /**
     * @var string
     */
    const REF_KILOGRAM_CODE = "166";

    /**
     * @var string (Reference default field names)
     */
    const REFERENCE_DEFAULT_FIELD_CODE = 'code';
    const REFERENCE_DEFAULT_FIELD_NAME = 'name';
    const REFERENCE_DEFAULT_FIELD_START_DATE = 'start_date';
    const REFERENCE_DEFAULT_FIELD_END_DATE = 'end_date';

    /**
     * @var array (Reference default field names array)
     */
    const DEFAULT_STRUCTURE_NAMES = [
        self::REFERENCE_DEFAULT_FIELD_CODE,
        self::REFERENCE_DEFAULT_FIELD_NAME,
        self::REFERENCE_DEFAULT_FIELD_START_DATE,
        self::REFERENCE_DEFAULT_FIELD_END_DATE
    ];

    /**
     * @var string (Reference Table Names)
     */
    const REFERENCE_AGENCY_TABLE_NAME = 'agencies_reference';
    const REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME = 'agency_subdivisions';
    const REFERENCE_BROKERS_TABLE_NAME = 'brokers_reference';
    const REFERENCE_LABORATORIES_TABLE_NAME = 'loboratories';
    const REFERENCE_LEGAL_ENTITY_FORM_TABLE_NAME = 'legal_entity_form_reference';
    const REFERENCE_COUNTRIES_TABLE_NAME = 'countries';
    const REFERENCE_SPHERE_TABLE_NAME = 'sphere';
    const REFERENCE_LAB_HS_INDICATOR_TABLE_NAME = 'lab_hs_indicator';
    const REFERENCE_HS_CODE_6_TABLE_NAME = 'hs_code';
    const REFERENCE_TRANSPORT_BORDER_CHECKPOINTS_TABLE_NAME = 'border_checkpoints';
    const REFERENCE_TRANSPORT_PERMIT_CARRIERS_TABLE_NAME = 'transport_permits_carriers';
    const REFERENCE_TRANSPORT_PERMIT_TYPES_TABLE_NAME = 'transport_permit_types';
    const REFERENCE_PACKAGING_REFERENCE_TABLE_NAME = 'packaging_reference';
    const REFERENCE_CURRENCIES_TABLE_NAME = 'currencies';
    const REFERENCE_UNIT_OF_MEASUREMENT_TABLE_NAME = 'unit_of_measurement_reference';

    /**
     * @var string
     */
    const REFERENCE_OBLIGATION_BUDGET_LINE = 'reference_obligation_budget_line';
    const REFERENCE_OBLIGATION_BUDGET_LINE_ML = 'reference_obligation_budget_line_ml';
    const REFERENCE_OBLIGATION_CREATION_TYPE = 'reference_obligation_creation_type';
    const REFERENCE_DOCUMENT_TYPE_REFERENCE = 'reference_document_type_reference';
    const REFERENCE_EXCHANGE_RATES = 'reference_exchange_rates';
    const REFERENCE_CURRENCIES = 'reference_' . self::REFERENCE_CURRENCIES_TABLE_NAME;
    const REFERENCE_COUNTRIES = 'reference_' . self::REFERENCE_COUNTRIES_TABLE_NAME;
    const REFERENCE_LEGAL_ENTITY_FORM = 'reference_' . self::REFERENCE_LEGAL_ENTITY_FORM_TABLE_NAME;
    const REFERENCE_UNIT_OF_MEASUREMENT = 'reference_' . self::REFERENCE_UNIT_OF_MEASUREMENT_TABLE_NAME;
    const REFERENCE_HS_CODE_6 = 'reference_hs_code';
//    const REFERENCE_HS_CODE = 'reference_hs_code';
    const REFERENCE_CLASSIFICATOR_OF_DOCUMENTS = 'reference_classificator_of_documents';
    const REFERENCE_AGENCY = 'reference_' . self::REFERENCE_AGENCY_TABLE_NAME;
    const REFERENCE_AGENCY_SUBDIVISIONS = 'reference_' . self::REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME;
    const REFERENCE_SAMPLING_TECH = 'reference_sampling_tech_reg';
    const REFERENCE_TECH_REGUL = 'reference_tech_regul';
    const REFERENCE_TECH_REG = 'reference_tech_reg';
    const REFERENCE_TECH_REG_CHAPTERS = 'tech_reg_chapters';
    const REFERENCE_LABORATORY_EXPERTISE = 'laboratory_expertise';
    const REFERENCE_LABORATORY_SAMPLERS = 'reference_lab_samplers';
    const REFERENCE_MEASUREMENT_UNITS = 'reference_measurement_units';
    const REFERENCE_USER_LAB_ADEQUACY = 'reference_lab_ans';
    const REFERENCE_EXPRESS_CONTROL = 'reference_express_control';
    const REFERENCE_BROKERS = 'reference_' . self::REFERENCE_BROKERS_TABLE_NAME;
    const REFERENCE_LABORATORIES = 'reference_' . self::REFERENCE_LABORATORIES_TABLE_NAME;
    const REFERENCE_REGISTRY_OF_PRODUCERS = 'reference_registry_of_producers';
    const REFERENCE_REJECTION_REASON = 'reference_rejection_reason_referrence';
    const REFERENCE_RISK_INSTRUCTION_TYPE = 'reference_risk_instruction_type';
    const REFERENCE_RISK_FEEDBACK = 'reference_risk_feedback';
    const REFERENCE_TRANSPORT_TYPE = 'reference_transport_reference';
    const REFERENCE_TYPE_OF_PERMISSION = 'reference_type_of_permission';
    const REFERENCE_OBLIGATION_TYPE = 'reference_obligation_type';
    const REFERENCE_LAB_EXPERTISE_INDICATOR = 'reference_lab_expertise_indicator';
    const REFERENCE_PACKAGING_REFERENCE = 'reference_' . self::REFERENCE_PACKAGING_REFERENCE_TABLE_NAME;
    const REFERENCE_CUSTOM_BODY_REFERENCE = 'custom_body_reference';
    const REFERENCE_TYPES_OF_PRODUCTS = 'reference_types_of_products';
    const REFERENCE_TYPE_OF_METAL = 'reference_type_of_metal';
    const REFERENCE_TYPE_OF_STONES = 'reference_type_of_stones';
    const REFERENCE_VALUE_OF_STONES = 'reference_value_of_stones';
    const REFERENCE_EU_COUNTRIES = 'reference_eu_countries';
    const REFERENCE_CITES_TARGET = 'reference_cites_target';
    const REFERENCE_CITES_SOURCE = 'reference_cites_source';
    const REFERENCE_CITES_APPENDIX = 'reference_cites_appendix';
    const REFERENCE_DOCUMENT_DEPENDING_TRANSPORT = 'reference_document_depending_transport';
    const REFERENCE_TYPE_OF_NARCOTIC_DRUGS = 'reference_type_of_narcotic_drugs';
    const REFERENCE_TARGET_OF_IMPORT = 'reference_terget_of_import';
    const REFERENCE_SPHERE = 'reference_' . self::REFERENCE_SPHERE_TABLE_NAME;
    const REFERENCE_LAB_HS_INDICATOR = 'reference_' . self::REFERENCE_LAB_HS_INDICATOR_TABLE_NAME;
    const REFERENCE_LAB_QUALITY_INDICATOR = 'reference_lab_quality_indicator';
    const REFERENCE_HOURS_INTERVAL = 'reference_hours_interval';
    const REFERENCE_TRANSPORT_PERMIT_TYPES = 'reference_' . self::REFERENCE_TRANSPORT_PERMIT_TYPES_TABLE_NAME;
    const REFERENCE_TRANSPORT_PERMIT_CARRIERS = 'reference_' . self::REFERENCE_TRANSPORT_PERMIT_CARRIERS_TABLE_NAME;
    const REFERENCE_TRANSPORT_BORDER_CHECKPOINTS = 'reference_' . self::REFERENCE_TRANSPORT_BORDER_CHECKPOINTS_TABLE_NAME;
    const REFERENCE_BOOLEAN_VALUES = 'reference_bulian_values';
    const REFERENCE_MOBILE_OPERATORS_CODES = 'reference_mobile_operators_codes';
    const REFERENCE_TYPE_OF_MEDICINE = 'reference_type_of_medicines';

    /**
     * @var array (Reference tables not editable only view)
     */
    const NONE_EDITABLE_REFERENCE_TABLE_NAMES = [
        self::REFERENCE_AGENCY_TABLE_NAME,
        self::REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME,
        self::REFERENCE_BROKERS_TABLE_NAME,
        self::REFERENCE_LABORATORIES_TABLE_NAME,
        self::REFERENCE_LEGAL_ENTITY_FORM_TABLE_NAME
    ];

    /**
     * @var string (Reference metal type codes)
     */
    const REFERENCE_METAL_TYPE_PLATINUM = 'Pt';
    const REFERENCE_METAL_TYPE_GOLD = 'Au';
    const REFERENCE_METAL_TYPE_PALLADIUM = 'Pd';
    const REFERENCE_METAL_TYPE_30_PERCENT_SILVER = '30';
    const REFERENCE_METAL_TYPE_SILVER = 'Ag';
    const REFERENCE_METAL_TYPE_BIJOUTERIE = 'Bj';

    /**
     * @var string (REFERENCE_BOOLEAN_VALUES ref table codes)
     */
    const REFERENCE_BOOLEAN_CODE_FALSE = 0;
    const REFERENCE_BOOLEAN_CODE_TRUE = 1;

    /**
     * @var string (Reference target of import code)
     */
    const REFERENCE_TARGET_OF_IMPORT_CODE_HG = 'HG';

    /**
     * @var string
     */
    const REFERENCE_AGENCY_ACCESS_TYPE_READ = 'read';
    const REFERENCE_AGENCY_ACCESS_TYPE_EDIT = 'edit';

    /**
     * @var array
     */
    const REFERENCE_AGENCY_ACCESS_TYPES = [
        self::REFERENCE_AGENCY_ACCESS_TYPE_READ,
        self::REFERENCE_AGENCY_ACCESS_TYPE_EDIT,
    ];

    /**
     * @var array
     */
    const REFERENCE_IMPORT_FORMATS = [
        'excel', 'csv', 'text', 'xml', 'web_service'
    ];

    /**
     * @var null
     */
    protected static $legalEntityTypes = null;

    /**
     * @var array
     */
    public static $referenceRowsData = [];

    /**
     * Reference express_control type codes
     */
    const REFERENCE_EXPRESS_CONTROL_TYPE_ACCELERATED = '3';
    const REFERENCE_EXPRESS_CONTROL_TYPE_PERISHABLES = '1';

    /**
     * @var string
     */
    const REFERENCE_CLASSIFICATOR_WAYBILL_CODE = '9991';

    /**
     * @var string
     */
    const REFERENCE_TRANSPORT_PERMIT_TYPE_P1 = 'P-1';
    const REFERENCE_TRANSPORT_PERMIT_TYPE_P3 = 'P-3';
    const REFERENCE_TRANSPORT_PERMIT_TYPE_P_BP = 'P-BP';

    /**
     * @var string
     */
    public $table = 'reference_table';

    /**
     * @var bool
     */
    protected $hasAdminInfo = true;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'parent_id',
        'table_name',
        'manually_edit',
        'import_format',
        'api_url',
        'available_at',
        'available_to',
        'show_status'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'created_admin_id',
        'created_admin_ip',
        'updated_at',
        'updated_admin_id',
        'updated_admin_ip',
        'deleted_admin_id',
        'deleted_admin_ip'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'import_format' => 'array'
    ];

    /**
     * Function to relate with ReferenceTableMl
     *
     * @return HasMany
     */
    public function ml()
    {
        return $this->hasMany(ReferenceTableMl::class, 'reference_table_id', 'id');
    }

    /**
     * Function to relate with ReferenceTableMl current
     *
     * @return HasOne
     */
    public function current()
    {
        return $this->hasOne(ReferenceTableMl::class, 'reference_table_id', 'id')->notDeleted()->where('lng_id', cLng('id'))->first();
    }

    /**
     * Function to relate with ReferenceTableAgencies
     *
     * @return HasMany
     */
    public function agencies()
    {
        return $this->hasMany(ReferenceTableAgencies::class, 'reference_table_id', 'id');
    }

    /**
     * Function to relate with ReferenceTableStructure
     *
     * @return HasMany
     */
    public function structure()
    {
        return $this->hasMany(ReferenceTableStructure::class, 'reference_table_id', 'id')->orderByAsc('sort_order')->active();
    }

    /**
     * Function to relate with ReferenceTableStructure
     *
     * @return HasMany
     */
    public function structureSearchField()
    {
        return $this->structure()->where('search_filter', ReferenceTable::TRUE);
    }

    /**
     * Function to relate with ReferenceTableStructure
     *
     * @return HasMany
     */
    public function customStructure()
    {
        return $this->hasMany(ReferenceTableStructure::class, 'reference_table_id', 'id')->whereNotIn('reference_table_structures.field_name', ReferenceTable::DEFAULT_STRUCTURE_NAMES)->orderByAsc('sort_order');
    }

    /**
     * Function to get orderAble fields in structure
     *
     * @return array
     */
    public function orderAbleFields()
    {
        $orderAbleFields = [];
        foreach ($this->structure as $structure) {
            if ($structure->search_result && (int)$structure->sortable == 0) {
                $orderAbleFields[] = $structure->field_name;
            }
        }

        return $orderAbleFields;
    }

    /**
     * Function to get Legal entity values (id_card,tax_id,foreign_citizen)
     *
     * @return array
     */
    public static function legalEntityValues()
    {
        if (self::$legalEntityTypes == null) {
            self::setLegalEntityTypes();
        }

        return self::$legalEntityTypes;
    }

    /**
     * Function to check type of legal entity
     *
     * @param $type
     * @param $checkType
     * @return bool
     */
    public static function isLegalEntityType($type, $checkType)
    {
        $referenceLegalEntity = self::legalEntityValues();

        if (isset($referenceLegalEntity[$checkType]) && $referenceLegalEntity[$checkType] == $type) {
            return true;
        }

        return false;
    }

    /**
     * Function to check auth user can edit reference
     *
     * @return bool
     */
    public function authUserCanEditReference()
    {
        if ($this->show_status != ReferenceTable::STATUS_ACTIVE || in_array($this->table_name, ReferenceTable::NONE_EDITABLE_REFERENCE_TABLE_NAMES)) {
            return false;
        }

        if (Auth::user()->isSwAdmin()) {
            return true;
        }

        if (Auth::user()->isAgency()) {

            if (!Auth::user()->isLocalAdmin() && !Auth::user()->canCreate()) {
                return false;
            }

            $referenceAgency = ReferenceTableAgencies::select('access')->where(['reference_table_id' => $this->id, 'agency_id' => Auth::user()->getCurrentUserRole()->agency_id])->first();

            if (!is_null($referenceAgency) && $referenceAgency->access != 'edit') {
                return false;
            }

            return true;
        }

        return false;

    }

    /**
     * Function to return reference structure fields by ref id
     *
     * @param $referenceTableId
     * @return ReferenceTableStructure
     */
    public static function structureField($referenceTableId)
    {
        return ReferenceTableStructure::select('id', 'field_name')->where(['reference_table_id' => $referenceTableId])->orderByAsc()->get();
    }

    /**
     * Function to check reference has agency column
     *
     * @param $referenceId
     * @return bool|string
     */
    public static function hasAgencyColumnGetName($referenceId)
    {
        $refAgencyId = self::getReferenceAgencyId();

        $refStructureHasAgencyColumn = ReferenceTableStructure::select('id', 'field_name')->where(['reference_table_id' => $referenceId, 'show_status' => ReferenceTable::STATUS_ACTIVE])->whereIn('type', [ReferenceTableStructure::COLUMN_TYPE_SELECT, ReferenceTableStructure::COLUMN_TYPE_AUTOCOMPLETE])->where("parameters->source", '=', "$refAgencyId")->first();

        if (!is_null($refStructureHasAgencyColumn)) {
            return $refStructureHasAgencyColumn->field_name;
        }

        return false;
    }

    /**
     * Function to get all reference tables with agency column
     *
     * @return array
     */
    public static function getReferenceWithAgencyColumn()
    {
        $referenceList = [];
        $refAgencyId = self::getReferenceAgencyId();

        $refStructureHasAgencyColumn = ReferenceTableStructure::with(['referenceTable'])->select('reference_table_id', 'field_name')->where(['show_status' => ReferenceTable::STATUS_ACTIVE])
            ->whereIn('type', [ReferenceTableStructure::COLUMN_TYPE_SELECT, ReferenceTableStructure::COLUMN_TYPE_AUTOCOMPLETE])
            ->where("parameters->source", '=', "$refAgencyId")
            ->get();

        foreach ($refStructureHasAgencyColumn as $item) {
            if (!is_null($item->referenceTable)) {
                $referenceList[$item->referenceTable->table_name] = $item->field_name;
            }
        }

        return $referenceList;
    }

    /**
     * Function to get Reference Agency id
     *
     * @return int
     */
    public static function getReferenceAgencyId()
    {
        return ReferenceTable::where('table_name', self::REFERENCE_AGENCY_TABLE_NAME)->pluck('id')->first();
    }

    /**
     * Function to get Reference Agency id
     *
     * @return Collection
     */
    public static function getAuthUserAgencyRefRow()
    {
        return getReferenceRows(self::REFERENCE_AGENCY, false, Auth::user()->companyTaxId(), ['id', 'code', 'name', 'long_name']);
    }

    /**
     * Function to get Auth user ref agency Ids
     *
     * @return array|bool
     */
    public static function getUserReferenceAgencyIds()
    {
        if (Auth::user()->isAgency()) {
            return DB::table(self::REFERENCE_AGENCY)->where('code', Auth::user()->companyTaxId())->pluck('id')->all();
        }

        return false;
    }

    /**
     * Function to get inactive refs
     *
     * @return Collection
     */
    public static function getInactiveRef()
    {
        return ReferenceTable::joinMl()->select('id', 'table_name')->where('reference_table.show_status', ReferenceTable::STATUS_INACTIVE)->get();
    }

    /**
     * Function to get Reference Id By Table Name
     *
     * @param $tableName
     * @return int|null
     */
    public static function getReferenceIdByTableName($tableName)
    {
        if (substr($tableName, 0, 10) === 'reference_') {
            $tableName = substr($tableName, 10);
        }

        return ReferenceTable::select('id')->where('table_name', $tableName)->pluck('id')->first();
    }

    /**
     * Function to set Legal Entity Types
     */
    public static function setLegalEntityTypes()
    {
        $referenceLegalEntityValues = getReferenceRows(ReferenceTable::REFERENCE_LEGAL_ENTITY_FORM)->toArray();

        $resultData = [];
        if (count($referenceLegalEntityValues)) {
            foreach ($referenceLegalEntityValues as $value) {
                $resultData[$value->code] = $value->id;
            }
        }

        self::$legalEntityTypes = $resultData;
    }
}
