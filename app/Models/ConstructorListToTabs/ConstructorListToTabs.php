<?php

namespace App\Models\ConstructorListToTabs;

use App\Models\BaseModel;

/**
 * Class ConstructorListToTabs
 * @package App\Models\ConstructorListToTabs
 */
class ConstructorListToTabs extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'constructor_list_to_tabs';

    /**
     * @var bool
     */
    protected $hasAdminInfo = true;

    /**
     * @var array
     */
    protected $fillable = [
        'constructor_list_id',
        'tab_key'
    ];

}
