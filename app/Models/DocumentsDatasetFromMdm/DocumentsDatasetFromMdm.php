<?php

namespace App\Models\DocumentsDatasetFromMdm;

use App\Models\BaseModel;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\DataModel\DataModel;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

/**
 * Class DocumentsDatasetFromMdm
 * @package App\Models\DocumentsDatasetFromMdm
 */
class DocumentsDatasetFromMdm extends BaseModel
{
    public $table = 'documents_dataset_from_mdm';

    /**
     * @var array
     */
    protected $fillable = [
        'field_id',
        'order',
        'sort_order_id',
        'document_id',
        'mdm_field_id',
        'group',
        'xpath',
        'is_attribute',
        'is_hidden',
        'is_search_param',
        'is_unique',
        'show_in_search_results',
        'is_active',
    ];

    /**
     * Function to relate with DocumentsDatasetFromMdmML all
     *
     * @return HasMany
     */
    public function ml()
    {
        return $this->hasMany(DocumentsDatasetFromMdmML::class, 'documents_dataset_from_mdm_id', 'id');
    }

    /**
     * Function to relate with DocumentsDatasetFromMdmML current
     *
     * @return HasOne
     */
    public function current()
    {
        return $this->hasOne(DocumentsDatasetFromMdmML::class, 'documents_dataset_from_mdm_id', 'id')->where('lng_id', cLng('id'));
    }

    /**
     * Function to relate with DataModel
     *
     * @return HasOne
     */
    public function mdmField()
    {
        return $this->hasOne(DataModel::class, 'id', 'mdm_field_id');
    }

    /**
     * Function to relate with Data Model
     *
     * @return HasOne
     */
    public function mdm()
    {
        return $this->hasOne(DataModel::class, 'id', 'mdm_field_id');
    }

    /**
     * Function to check auth user has role
     *
     * @param $query
     * @return Builder
     */
    public function scopeConstructorDataSetMdmOwner($query)
    {
        $agencyDocumentIds = ConstructorDocument::getConstructorDocumentForAgency()->pluck('id')->toArray();

        return $query->whereIn($this->getTable() . '.document_id', $agencyDocumentIds ?? -1);
    }

    /**
     * Function to get mdm Field Data
     *
     * @param $documentID
     * @return DocumentsDatasetFromMdm
     */
    public static function mdmFieldData($documentID)
    {
        return self::select([
            'documents_dataset_from_mdm.*',
            'documents_dataset_from_mdm_ml.label',
            'documents_dataset_from_mdm_ml.tooltip',
            'data_model_excel.name as name',
            'data_model_excel_ml.description as description'
        ])
            ->where('document_id', $documentID)
            ->join('data_model_excel', DB::raw('data_model_excel.id::varchar'), '=', 'documents_dataset_from_mdm.mdm_field_id')
            ->join('data_model_excel_ml', function ($query) {
                $query->on('data_model_excel.id', '=', 'data_model_excel_ml.data_model_id')->where('data_model_excel_ml.lng_id', cLng('id'));
            })
            ->joinMl()
            ->orderBy('documents_dataset_from_mdm.id')
            ->get();
    }

    /**
     * Function to get mdm Field for export
     *
     * @param $documentID
     * @return DocumentsDatasetFromMdm
     */
    public static function mdmFieldsForExport($documentID)
    {
        return self::select([
            'documents_dataset_from_mdm_ml.*',
            'documents_dataset_from_mdm_ml.tooltip',
            'data_model_excel.id',
            'data_model_excel.name'
        ])
            ->where('document_id', $documentID)
            ->leftJoin('data_model_excel', DB::raw('data_model_excel.id::varchar'), '=', 'documents_dataset_from_mdm.mdm_field_id')
            ->leftJoin('documents_dataset_from_mdm_ml', 'documents_dataset_from_mdm_ml.documents_dataset_from_mdm_id', '=', 'documents_dataset_from_mdm.id')
            ->get();
    }
}
