<?php

namespace App\Models\DocumentsDatasetFromMdm;

use App\Models\BaseMlManager;
use App\Models\ConstructorDataSet\ConstructorDataSet;
use Illuminate\Support\Facades\DB;

/**
 * Class DocumentsDatasetFromMdmManager
 * @package App\Models\DocumentsDatasetFromMdm
 */
class DocumentsDatasetFromMdmManager
{
    use BaseMlManager;

    /**
     * Function to save data
     *
     * @param array $data
     */
    public function store(array $data)
    {
        DB::transaction(function () use ($data) {

            $insertData = $this->getInsertField($data);

            if (isset($insertData['data'])) {

                $docId = $data['documentID'];
                $dataSet = DocumentsDatasetFromMdm::where('document_id', $docId)->get();

                $fileName = "dataset_$docId.json";
                $fileDir = storage_path("dataset");

                if (!is_dir($fileDir)) {
                    mkdir($fileDir, 0775, true);
                }

                $fileName = $fileDir . '/' . $fileName;

                file_put_contents($fileName, $dataSet);

                //
                $documentDatasetFromMdmDeleteIds = DocumentsDatasetFromMdm::where('document_id', $docId)->pluck('id')->all();

                DocumentsDatasetFromMdm::where('document_id', $docId)->delete();
                DocumentsDatasetFromMdmML::whereIn('documents_dataset_from_mdm_id', $documentDatasetFromMdmDeleteIds)->delete();

                //
                $documentsDatasetFromMdmML = new DocumentsDatasetFromMdmML();

                foreach ($insertData['data'] as $item) {
                    $documentsDatasetFromMdm = new DocumentsDatasetFromMdm($item);
                    $documentsDatasetFromMdm->save();

                    $this->storeMl($insertData['ml'][$item['field_id']], $documentsDatasetFromMdm, $documentsDatasetFromMdmML);
                }
            }
        });
    }

    /**
     * Function to get array for insert
     *
     * @param $data
     * @return array
     */
    private function getInsertField($data)
    {
        $insertData = [];
        $groupPath = ConstructorDataSet::getGroupPaths();

        if (isset($data['paramsMDM']) && !empty($data['paramsMDM'])) {
            foreach ($data['paramsMDM'] as $fieldID => $item) {
                $sortOrderedId = preg_split('~_(?=[^_]*$)~', $fieldID);
                $insertData['data'][] = [
                    'field_id' => $fieldID,
                    'order' => isset($item['order']) ? $item['order'] : 0,
                    'sort_order_id' => (int)$sortOrderedId[1],
                    'document_id' => $data['documentID'],
                    'mdm_field_id' => $item['mdm_field_id'],
                    'group' => $item['group'],
                    'xpath' => $groupPath['groupXPath'][$item['group']],
                    'is_attribute' => isset($item['is_attribute']),
                    'is_hidden' => isset($item['is_hidden']),
                    'is_unique' => isset($item['is_unique']),
                    'is_search_param' => isset($item['is_search_param']),
                    'show_in_search_results' => isset($item['show_in_search_results']),
                    'is_active' => isset($item['is_active'])
                ];
                foreach (activeLanguages() as $lang) {
                    $insertData['ml'][$fieldID][$lang->id] = [
                        'label' => isset($item['field_label']) ? $item['field_label'][$lang->id] : '',
                        'tooltip' => isset($item['tooltip']) ? $item['tooltip'][$lang->id] : ''
                    ];
                }
            }
        }

        return $insertData;
    }
}