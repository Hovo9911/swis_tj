<?php

namespace App\Models\DocumentsDatasetFromMdm;

use App\Models\BaseModel;
use App\Models\Traits\HasCompositePrimaryKey;

/**
 * Class DocumentsDatasetFromMdmML
 * @package App\Models\DocumentsDatasetFromMdm
 */
class DocumentsDatasetFromMdmML extends BaseModel
{
    use HasCompositePrimaryKey;

    /**
     * @var string
     */
    public $table = 'documents_dataset_from_mdm_ml';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $primaryKey = ['documents_dataset_from_mdm_id', 'lng_id'];

    /**
     * @var array
     */
    protected $fillable = [
        'documents_dataset_from_mdm_id',
        'lng_id',
        'label',
        'tooltip'
    ];
}
