<?php

namespace App\Models\NotificationTemplatesSentAt;

use App\Models\BaseModel;
use Carbon\Carbon;

/**
 * Class NotificationTemplatesSentAt
 * @package App\Models\NotificationTemplatesSentAt
 */
class NotificationTemplatesSentAt extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'notification_templates_sent_at';

    /**
     * @var bool
     */
    public $hasAdminInfo = false;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'notification_templates_id',
        'sent_at',
    ];

    /**
     * Function to get certificate period validity
     *
     * @param $value
     * @return string|void
     */
    public function getSentAtAttribute($value)
    {
        if ($value) {
            return Carbon::parse($value)->format(config('swis.date_time_format_front'));
        }
    }
}