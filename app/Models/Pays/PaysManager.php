<?php

namespace App\Models\Pays;

use App\Models\PaymentProviders\PaymentProviders;

/**
 * Class PaysManager
 * @package App\Models\Pays
 */
class PaysManager
{
    /**
     * Function to get Provider
     *
     * @param $token
     * @return PaymentProviders
     */
    public function getProvider($token)
    {
        return PaymentProviders::select('id', 'ip_address')->where('token', $token)->first();
    }

    /**
     * Function to check is the ip address from provider
     *
     * @param $provider
     * @param $ipAddress
     * @return bool
     */
    public function checkIpAddressByProvider($provider, $ipAddress)
    {
        $requestId = request()->ip();
        $customerIps = explode(',', str_replace(' ', '', $provider->ip_address));

        $matched = false;
        foreach ($customerIps as $customerIp) {
            if (strpos($customerIp, "/") === false) {
                if ($requestId == $customerIp) {
                    $matched = true;
                }
            } else {
                if ($this->netMatch($customerIp, $requestId) === true) {
                    $matched = true;
                }
            }
        }

        return $matched;
    }

    /**
     * Function to check new matches
     *
     * @param $range
     * @param $ip
     * @return bool
     */
    private function netMatch($range, $ip)
    {
        if ( strpos( $range, '/' ) == false ) {
            $range .= '/32';
        }
        // $range is in IP/CIDR format eg 127.0.0.1/24
        list( $range, $netmask ) = explode( '/', $range, 2 );
        $range_decimal = ip2long( $range );
        $ip_decimal = ip2long( $ip );
        $wildcard_decimal = pow( 2, ( 32 - $netmask ) ) - 1;
        $netmask_decimal = ~ $wildcard_decimal;

        return ( ( $ip_decimal & $netmask_decimal ) == ( $range_decimal & $netmask_decimal ) );
    }
}

