<?php

namespace App\Models\Pays;

use App\Models\BaseModel;

/**
 * Class Pays
 * @package App\Models\Pays
 */
class Pays extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'payments';

    /**
     * @var array
     */
    protected $fillable = [
        'ip',
        'service_provider_id',
        'document_id',
        'payment_done_date',
        'amount',
        'beneficiary_name',
        'tax_id',
        'description',
        'status',
        'sended_into_balance'
    ];
}
