<?php

namespace App\Models\PaymentAndObligation;

use App\Http\Controllers\Core\DataTableSearch;
use App\Models\BaseModel;
use App\Models\Company\Company;
use App\Models\Menu\Menu;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\Role\Role;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure;
use App\Models\SingleApplicationObligations\SingleApplicationObligations;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use App\Models\User\User;
use App\Models\UserRoles\UserRoles;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class PaymentAndObligationSearch
 * @package App\Models\PaymentAndObligation
 */
class PaymentAndObligationSearch extends DataTableSearch
{
    /**
     * Function to get search rows count
     *
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * Function to get rows count
     *
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * Function to get query set order, limit and return all data
     *
     * @return Collection
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        $returnArray = $query->get();

        list($companyTaxId, $roleIds, $roles, $xml) = $this->getUserRoles();

        $getObligationSum = $getPaymentSum = $getBalanceSum = 0;

        foreach ($returnArray as &$item) {
            $item->sub_application_number = ($item->obligation_code == SingleApplicationObligations::SAF_OBLIGATIONS_FIX_CODE_FOR_SEND_APPLICATION_OBLIGATION) ? '-' : $item->sub_application_number;
            $item->obligation_payer_person = ($item->obligation_payer_person == '()  ') ? '' : $item->obligation_payer_person;

            $item['is_saf_link'] = false;
            $item['saf_link'] = '#';

            if (!is_null(SingleApplication::select('id')->where('regular_number', $item->saf_number)->safOwnerOrCreator()->first())) {
                $item['is_saf_link'] = true;
                $item['saf_link'] = urlWithLng("/single-application/edit/{$item['regime']}/{$item['saf_number']}");
            }

            $item->obligation_payer = $this->getObligationPayer($item);

            // -- start generate application link -- //

            $subApplication = SingleApplicationSubApplications::select('saf.id')
                ->join('saf', function ($join) use ($companyTaxId) {
                    $join->on('saf.regular_number', '=', 'saf_sub_applications.saf_number')->where('saf_sub_applications.company_tax_id', $companyTaxId);
                })
                ->join('saf_sub_application_states', function ($join) {
                    $join->on('saf_sub_application_states.saf_subapplication_id', '=', 'saf_sub_applications.id')->where('saf_sub_application_states.is_current', '1');
                })
                ->join('constructor_mapping', function ($join) use ($roleIds, $item) {
                    $join->on('saf_sub_application_states.state_id', '=', 'constructor_mapping.start_state')->whereIn('constructor_mapping.role', $roleIds)->where('constructor_mapping.document_id', $item->constructor_document_id);
                });

            if (!Auth::user()->isLocalAdmin()) {
                foreach ($roles as $role) {
                    foreach ($role as $roleGroup) {
                        $subApplication->where(function ($q) use ($roleGroup, $xml) {
                            foreach ($roleGroup as $role) {
                                if ($role['attribute_type'] == 'saf') {
                                    $name = str_replace("]", "", str_replace("[", "->", str_replace("][", "->", (string)getSafFieldById($xml, $role['attribute_field_id'])[0]->attributes()->name)));
                                    $attrValue = (string)$role['attribute_value'];

                                    if ($role['attribute_field_id'] == 'SAF_ID_1') {
                                        $attrValue = SingleApplication::REGIME_IMPORT;
                                        if ($role['attribute_value'] == 2) {
                                            $attrValue = SingleApplication::REGIME_EXPORT;
                                        } elseif ($role['attribute_value'] == 3) {
                                            $attrValue = SingleApplication::REGIME_TRANSIT;
                                        }
                                    }
                                    $q->orWhere("saf_sub_applications.data->{$name}", $attrValue);
                                } else {
                                    $name = $role['attribute_field_id'];
                                    $q->orWhere("saf_sub_applications.custom_data->{$name}", (string)$role['attribute_value']);
                                }
                            }
                        });
                    }
                }
            }

            if (!is_null($subApplication->first())) {
                $item['is_saf_application_link'] = true;
                $item['application_link'] = urlWithLng("/user-documents/{$item['constructor_document_id']}/application/{$item['sub_application_id']}/edit");
            }

            if ($item['obligation_pay_date']) {
                $item['obligation_pay_date'] = formattedDate(date(config('swis.date_time_format'), strtotime($item['obligation_pay_date'])), true);
            }

            if ($item['obligation_created_date']) {
                $item['obligation_created_date'] = formattedDate($item['obligation_created_date'], true);
            }

            $getObligationSum += $item['obligation'];
            $getPaymentSum += $item['payment'];
            $getBalanceSum += $item['balance'];
        }

        $returnArray[] = $this->returnTotalRow($getObligationSum, $getPaymentSum, $getBalanceSum);

        return $returnArray;
    }

    /**
     * Function to get total obligation sum
     *
     * @return Builder
     */
    public function searchTotal()
    {
        $query = $this->constructQuery();
        return $query->where(function ($query) {
            return $query->whereNull('payment')->orWhere('payment', '=', '0');
        })->sum('obligation');
    }

    /**
     * Function to set order
     *
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($this->orderByCol ?? 'saf_obligations.id', $this->orderByType ?? 'desc');
    }

    /**
     * Function to get query for get data
     *
     * @param array $data
     * @param array $select
     * @return Builder
     */
    protected function constructQuery($data = [], $select = [])
    {
        $data = !empty($data) ? $data : $this->searchData;
        $select = !empty($select) ? $select : $this->getSelectFields();

        // get by authorise person permissions
        $getRoles = [];
        $paymentAndObligationMenu = Menu::getMenuIdByName(Menu::PAYMENT_AND_OBLIGATIONS_MENU_NAME);
        $authUser = Auth::user();

        $getRoleWithoutAttribute = UserRoles::select('id')
            ->when(!$authUser->isLocalAdmin(), function ($q) use ($authUser) {
                $q->where('user_id', $authUser->id);
                $q->where('company_tax_id', $authUser->companyTaxId());
            })
            ->whereNull('attribute_field_id')
            ->where('role_status', BaseModel::STATUS_ACTIVE)
            ->where('menu_id', $paymentAndObligationMenu)
            ->first();

        if (!$authUser->isLocalAdmin() && is_null($getRoleWithoutAttribute)) {
            $roles = UserRoles::select('attribute_field_id', 'attribute_type', 'attribute_value', 'role_id')
                ->where('company_tax_id', $authUser->companyTaxId())
                ->when(!Auth::user()->isLocalAdmin(), function ($q) use ($authUser) {
                    $q->where('user_id', $authUser->id);
                    $q->where('company_tax_id', $authUser->companyTaxId());
                })
                ->where('role_status', BaseModel::STATUS_ACTIVE)
                ->where('menu_id', $paymentAndObligationMenu)
                ->get();

            foreach ($roles as $role) {
                $getRoles[$role->attribute_field_id][] = $role->attribute_value;
            }
        }

        // Any change in this script please change also in PaymentaAndObligationController@getTableTotalPay method
        $query = SingleApplicationObligations::select($select)
            ->join('saf_sub_applications', 'saf_sub_applications.id', '=', 'saf_obligations.subapplication_id')
            ->join('saf', 'saf.regular_number', '=', 'saf_obligations.saf_number')
            ->leftJoin('users as creator_user', 'creator_user.id', '=', 'saf.creator_user_id')
            ->leftJoin('users as payer_user', 'payer_user.id', '=', DB::raw("saf_obligations.payer_user_id::integer"))
            ->leftJoin('users', 'users.id', '=', 'saf.creator_user_id')
            ->join('agency', function ($q) {
                $q->on('agency.tax_id', '=', 'saf_sub_applications.company_tax_id')->where('agency.show_status', BaseModel::STATUS_ACTIVE);
            })
            ->join('agency_ml', function ($query) {
                $query->on('agency_ml.agency_id', '=', 'agency.id')->where('agency_ml.lng_id', cLng('id'));
            })
            ->join('constructor_documents', function ($query) {
                $query->on('constructor_documents.document_code', '=', 'saf_sub_applications.document_code')->where('constructor_documents.show_status', '1');
            })
            ->join('constructor_documents_ml', function ($query) {
                $query->on('constructor_documents_ml.constructor_documents_id', '=', 'constructor_documents.id')->where('constructor_documents_ml.lng_id', cLng('id'));
            })
            ->join('reference_obligation_budget_line', 'reference_obligation_budget_line.id', '=', 'saf_obligations.ref_budget_line_id')
            ->join('reference_obligation_budget_line_ml', function ($q) {
                $q->on('reference_obligation_budget_line_ml.reference_obligation_budget_line_id', '=', 'reference_obligation_budget_line.id')->where('reference_obligation_budget_line_ml.lng_id', cLng('id'));
            })
            ->join('reference_obligation_type', 'reference_obligation_type.id', '=', 'reference_obligation_budget_line.budget_line')
            ->join('reference_obligation_type_ml', function ($q) {
                $q->on('reference_obligation_type_ml.reference_obligation_type_id', '=', 'reference_obligation_type.id')->where('reference_obligation_type_ml.lng_id', cLng('id'));
            })
            ->leftJoin('saf_obligation_receipts', 'saf_obligation_receipts.saf_obligation_id', '=', 'saf_obligations.id')
            ->where('saf_obligations.obligation_code', '!=', SingleApplicationObligations::SAF_OBLIGATIONS_FIX_CODE_FOR_SEND_APPLICATION_OBLIGATION)
            ->when(!empty($getRoles), function ($q) use ($getRoles) {
                $subDivisionKey = Role::ROLE_ATTRIBUTES_PAYMENT_AND_OBLIGATION_SUBDIVISION;
                $documentKey = Role::ROLE_ATTRIBUTES_PAYMENT_AND_OBLIGATION_DOCUMENT;

                if (isset($getRoles[$subDivisionKey]) && $subDivisions = $getRoles[$subDivisionKey]) {
                    $q->where(function ($query) use ($subDivisions) {
                        foreach ($subDivisions as $item) {
                            $getCode = getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME, $item, false, ['code']);
                            $availableIds = getReferenceRows(ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS_TABLE_NAME, false, false, ['id'], 'get', false, false, ['code' => $getCode->code], true)->pluck('id')->all();
                            $query->orWhereIn('saf_sub_applications.agency_subdivision_id', $availableIds);
                        }
                    });
                }

                if (isset($getRoles[$documentKey]) && $documents = $getRoles[$documentKey]) {
                    $q->where(function ($query) use ($documents) {
                        foreach ($documents as $item) {
                            $query->orWhere('constructor_documents.id', $item);
                        }
                    });
                }

            });

        if (!Auth::user()->isSwAdmin()) {
            $query->where(function ($q) {
                $q->safOwnerOrCreator()->orWhere('saf_sub_applications.company_tax_id', Auth::user()->companyTaxId());
            });
        }

        // Start search by filters
        if (isset($data['saf_number'])) {
            $query->where('saf.regular_number', 'ILIKE', "%{$data['saf_number']}%");
        }

        if (isset($data['sub_application_number'])) {
            $query->where('saf_sub_applications.document_number', 'ILIKE', "%{$data['sub_application_number']}%");
        }

        if (isset($data['tin_importer_exporter'])) {
            $importerExporterTin = $data['tin_importer_exporter'];

            $query->where(function ($q) use ($importerExporterTin) {
                $q->orWhere('saf.importer_value', 'ILIKE', "%{$importerExporterTin}%")
                    ->orWhere('saf.exporter_value', 'ILIKE', "%{$importerExporterTin}%");
            });
        }

        if (isset($data['importer_exporter'])) {
            $importerExporter = $data['importer_exporter'];

            $query->where(function ($q) use ($importerExporter) {
                $q->where('saf.data->saf->sides->export->type_value', 'ilike', "%{$importerExporter}%")
                    ->where('saf.data->saf->sides->import->name', 'ilike', "%{$importerExporter}%");
            });
        }

        if (isset($data['applicant_tin'])) {
            $query->where('users.ssn', 'ILIKE', "%{$data['applicant_tin']}%");
        }

        if (isset($data['applicant'])) {
            $applicant = $data['applicant'];
            $query->where(function ($q) use ($applicant) {
                $q->orWhere('users.first_name', 'ILIKE', "%{$applicant}%")
                    ->orWhere('users.last_name', 'ILIKE', "%{$applicant}%");
            });
        }

        if (isset($data['agency'])) {
            $query->where('agency.tax_id', 'ILIKE', "%{$data['agency']}%");
        }

        if (isset($data['document'])) {
            $query->where('constructor_documents.document_code', 'ILIKE', "%{$data['document']}%");
        }

        if (isset($data['obligation_type'])) {
            $query->where('reference_obligation_type.code', 'ILIKE', "%{$data['obligation_type']}%");
        }

        if (isset($data['budget_line'])) {
            $budgetLine = $data['budget_line'];
            $query->where('reference_obligation_budget_line.account_number', 'ILIKE', "%{$budgetLine}%");
        }

        if (isset($data['pay'])) {
            if ($data['pay'] === "1") {
                $query->whereRaw('saf_obligations.obligation = saf_obligations.payment');
            } elseif ($data['pay'] === "0" && $data['pay'] !== "001") {
                $query->whereRaw('saf_obligations.obligation != saf_obligations.payment');
            }
        }

        if (isset($data['balance_start'])) {
            $query->whereRaw("CAST(saf_obligations.obligation - saf_obligations.payment AS DECIMAL(10,2)) >= {$data['balance_start']}");
        }

        if (isset($data['balance_end'])) {
            $query->whereRaw("CAST(saf_obligations.obligation - saf_obligations.payment AS DECIMAL(10,2)) <= {$data['balance_end']}");
        }

        if (isset($data['obligation_pay_date_start'])) {
            $query->where('saf_obligations.payment_date', '>=', "{$data['obligation_pay_date_start']} 00:00:00");
        }

        if (isset($data['obligation_pay_date_end'])) {
            $query->where('saf_obligations.payment_date', '<=', "{$data['obligation_pay_date_end']} 23:59:59");
        }

        if (isset($data['obligation_created_date_start'])) {
            $query->where('saf_obligations.created_at', '>=', "{$data['obligation_created_date_start']} 00:00:00");
        }

        if (isset($data['obligation_created_date_end'])) {
            $query->where('saf_obligations.created_at', '<=', "{$data['obligation_created_date_end']} 23:59:59");
        }

        if (isset($data['subdivision_id'])) {
            $query->where('saf_sub_applications.agency_subdivision_id', $data['subdivision_id']);
        }

        // End search by filters

        return $query;
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }

    /**
     * Function to call protected ConstructorQuery
     *
     * @param $data
     * @param $select
     * @return mixed
     */
    public function callToConstructorQuery($data, $select)
    {
        return $this->constructQuery($data, $select);
    }

    /**
     * Function to get roles and company tax id
     *
     * @param array $roles
     * @return array
     */
    public function getUserRoles($roles = [])
    {
        $companyTaxId = Auth::user()->companyTaxId();
        $roleIds = Auth::user()->getCurrentRoles(Menu::USER_DOCUMENTS_MENU_NAME);
        $getRoles = UserRoles::select('attribute_field_id', 'attribute_type', 'attribute_value', 'role_id')
            ->whereIn('role_id', $roleIds)
            ->where('company_tax_id', $companyTaxId)
            ->when(!Auth::user()->isLocalAdmin(), function ($q) {
                $q->where('user_id', Auth::id());
            });

        foreach ($getRoles->get()->toArray() as $role) {
            if (!is_null($role['attribute_field_id'])) {
                $roles[$role['role_id']][$role['attribute_field_id']][] = $role;
            }
        }
        $xml = SingleApplicationDataStructure::lastXml();

        return [$companyTaxId, $roleIds, $roles, $xml];
    }

    /**
     * Function to get obligation payer name
     *
     * @param $item
     * @return string
     */
    public function getObligationPayer($item)
    {
        $item->obligation_payer = '';
        if (!is_null($item->payer_user_type)) {
            if ($item->payer_user_type == SingleApplicationObligations::SAF_OBLIGATIONS_TYPE_NATURAL_PERSON) {
                $item->obligation_payer = "(" . $item->payer_company_tax_id . ') ' . User::find($item->payer_user_id)->name();
            } elseif ($item->payer_user_type == SingleApplicationObligations::SAF_OBLIGATIONS_TYPE_AGENCY || $item->payer_user_type == SingleApplicationObligations::SAF_OBLIGATIONS_TYPE_BROKER) {
                $item->obligation_payer = "(" . $item->payer_company_tax_id . ') ' . optional(Company::getCompanyByTaxID($item->payer_company_tax_id))->name;
            } else {
                $item->obligation_payer = "(" . $item->payer_company_tax_id . ') ' . trans("swis.saf_obligations.payer.{$item->payer_user_type}");
            }
        }
        return $item->obligation_payer;
    }

    /**
     * Function to return obligations total row (last row)
     *
     * @param $getObligationSum
     * @param $getPaymentSum
     * @param $getBalanceSum
     * @param bool $xlsReport
     * @return array
     */
    public function returnTotalRow($getObligationSum, $getPaymentSum, $getBalanceSum, $xlsReport = false)
    {
        $fields = [
            "id" => '',
            "print" => '',
            "regime" => '',
            "sub_application_id" => '',
            "constructor_document_id" => '',
            "obligation_created_date" => '',
            "obligation_pay_date" => '',
            "applicant_tin" => '',
            "saf_number" => '',
            "sub_application_number" => '',
            "agency" => '',
            "document" => '',
            'obligation_payer' => '',
            'obligation_payer_person' => '',
            "obligation_type" => '',
            "budget_line" => '',
            "importer_exporter_tin" => '',
            "saf_obligation_receipt" => '',
            "obligation" => number_format($getObligationSum, '2', '.', ''),
            "payment" => number_format($getPaymentSum, '2', '.', ''),
            "balance" => number_format($getBalanceSum, '2', '.', ''),
        ];

        if ($xlsReport) {
            unset($fields['id']);
            unset($fields['print']);
            unset($fields['regime']);
        }

        return $fields;
    }

    /**
     * Function to get select fields
     *
     * @return array
     */
    private function getSelectFields()
    {
        return [
            'saf_obligations.id as id',
            'saf_obligations.id as print',
            'saf_obligations.pdf_hash_key as pdf_hash_key',
            'saf.regime as regime',
            'saf_sub_applications.id as sub_application_id',
            'constructor_documents.id as constructor_document_id',
            'saf_obligations.created_at as obligation_created_date',
            'saf_obligations.payment_date as obligation_pay_date',
            'creator_user.ssn as applicant_tin',
            'saf.regular_number as saf_number',
            'saf_sub_applications.document_number as sub_application_number',
            'agency_ml.name as agency',
            'constructor_documents_ml.document_name as document',
            'saf_obligations.payer_user_id', 'saf_obligations.payer_user_type', 'saf_obligations.payer_company_tax_id',
            'saf.importer_value',
            'saf.exporter_value',
            'saf.creator_company_tax_id as saf_creator_company_tax_id',
            'saf.company_tax_id as saf_company_tax_id',
            'reference_obligation_budget_line.account_number as budget_line',
            'saf_obligations.obligation_code',
            'saf_obligation_receipts.file_name as saf_obligation_receipt',
            DB::raw("CONCAT('(', reference_obligation_type.code , ') ', reference_obligation_type_ml.name) AS obligation_type"),
            DB::raw("CONCAT('(', payer_user.ssn , ') ', payer_user.first_name, ' ', payer_user.last_name) AS obligation_payer_person"),
            DB::raw("(CASE
                WHEN saf.regime='import' THEN saf.importer_value
                WHEN saf.regime='export' THEN saf.exporter_value
                ELSE saf.company_tax_id END
            ) as importer_exporter_tin"),
            DB::raw('CAST(saf_obligations.obligation AS NUMERIC(10,2)) as obligation'),
            DB::raw('CAST(saf_obligations.payment AS NUMERIC(10,2)) as payment'),
            DB::raw('CAST(saf_obligations.obligation - saf_obligations.payment AS NUMERIC(10,2)) as balance')
        ];
    }
}
