<?php

namespace App\Models\ConstructorLists;

use App\Http\Controllers\Core\DataTableSearch;
use Illuminate\Support\Facades\DB;

/**
 * Class ConstructorListsSearch
 * @package App\Models\ConstructorLists
 */
class ConstructorListsSearch extends DataTableSearch
{
    /**
     * @return mixed
     */
    public function searchRowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function rowsCount()
    {
        return $this->constructQuery()->count();
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $query = $this->constructQuery();
        $this->constructOrder($query);
        $this->constructLimit($query);
        return $query->get();
    }

    /**
     * @param $query
     */
    protected function constructOrder($query)
    {
        $query->orderBy($this->orderByCol ?? 'document_id', $this->orderByType ?? 'desc');
    }

    /**
     * @return mixed
     */
    protected function constructQuery()
    {
        $id = isset($this->searchData['id']) ? $this->searchData['id'] : null;
        $constructorDocumentId = isset($this->searchData['constructor_document_id']) ? $this->searchData['constructor_document_id'] : null;

        return ConstructorLists::when($constructorDocumentId, function ($query) use ($constructorDocumentId) {
            $query->where('document_id', $constructorDocumentId);
        })->when($id, function ($query) use ($id) {
            $query->where(DB::raw('id::varchar'), $id);
        })->constructorListOwner()->notDeleted();
    }

    /**
     * @param $query
     */
    protected function constructLimit($query)
    {
        $query->skip($this->pagingStart)->take($this->pagingCount);
    }
}