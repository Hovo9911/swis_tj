<?php

namespace App\Models\ConstructorLists;

use App\Models\BaseModel;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorListToDataSetFromMdm\ConstructorListToDataSetFromMdm;
use App\Models\ConstructorListToDataSetFromSaf\ConstructorListToDataSetFromSaf;
use App\Models\ConstructorListToTabs\ConstructorListToTabs;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Query\Builder;

/**
 * Class ConstructorLists
 * @package App\Models\ConstructorLists
 */
class ConstructorLists extends BaseModel
{
    /**
     * @var int
     */
    const NON_VISIBLE = 1;
    const EDITABLE = 2;
    const MANDATORY = 3;

    /**
     * @var string
     */
    public $table = 'constructor_lists';

    /**
     * @var bool
     */
    protected $hasAdminInfo = true;

    /**
     * @var array
     */
    const LIST_TYPES = [
        'non_visible' => [],
        'editable' => [],
        'mandatory' => [],
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'document_id',
        'list_type',
        'description',
        'show_status'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'created_admin_id',
        'created_admin_ip',
        'updated_at',
        'updated_admin_id',
        'updated_admin_ip',
        'deleted_admin_id',
        'deleted_admin_ip'
    ];

    /**
     * Function to get saf fields
     *
     * @return HasMany
     */
    public function safFields()
    {
        return $this->hasMany(ConstructorListToDataSetFromSaf::class, 'constructor_list_id', 'id');
    }

    /**
     * Function to get mdm fields
     *
     * @return HasMany
     */
    public function mdmfields()
    {
        return $this->hasMany(ConstructorListToDataSetFromMdm::class, 'constructor_list_id', 'id');
    }

    /**
     * Function to get tabs
     *
     * @return HasMany
     */
    public function tabs()
    {
        return $this->hasMany(ConstructorListToTabs::class, 'constructor_list_id', 'id');
    }

    /**
     * Function to generate list array
     *
     * @param $documentId
     * @return array
     */
    public static function generateListsArray($documentId)
    {
        $data['nonVisibleList'] = self::getNonVisibleList($documentId);
        $data['editableList'] = self::getEditableList($documentId);
        $data['mandatoryList'] = self::getMandatorylist($documentId);

        return $data;
    }

    /**
     * Function to get non visible lists
     *
     * @param $documentId
     * @return ConstructorLists
     */
    public static function getNonVisibleList($documentId)
    {
        return self::getData(self::NON_VISIBLE, $documentId);
    }

    /**
     * Function to get editable lists
     *
     * @param $documentId
     * @return ConstructorLists
     */
    public static function getEditableList($documentId)
    {
        return self::getData(self::EDITABLE, $documentId);
    }

    /**
     * Function to get mandatory lists
     *
     * @param $documentId
     * @return ConstructorLists
     */
    public static function getMandatorylist($documentId)
    {
        return self::getData(self::MANDATORY, $documentId);
    }

    /**
     * Function to get lists data
     *
     * @param $listType
     * @param $documentId
     * @return mixed
     */
    protected static function getData($listType, $documentId)
    {
        return self::select(array_merge(["id"], (new self)->getFillable()))
            ->when($documentId, function ($query) use ($documentId) {
                $query->where('document_id', $documentId);
            })
            ->where('list_type', $listType)
            ->constructorListOwner()
            ->get();
    }

    /**
     * Function to check auth user has role
     *
     * @param $query
     * @return Builder
     */
    public function scopeConstructorListOwner($query)
    {
        $agencyDocumentIds = ConstructorDocument::getConstructorDocumentForAgency()->pluck('id')->toArray();

        return $query->whereIn($this->getTable() . '.document_id', $agencyDocumentIds ?? -1);
    }

    /**
     * Function to merge and return mapping lists
     *
     * @param $mappings
     * @param $mappingWithLists
     * @param bool $onlyNoneVisible
     * @return array
     */
    public static function margeMappingLists($mappings, $mappingWithLists, $onlyNoneVisible = false)
    {
        $merged = self::LIST_TYPES;
        $nonVisibleLists = [];
        $nonVisibleListCount = 0;

        foreach ($mappings as $mapping) {
            if (isset($mapping->non_visible_list)) {
                if (isset($mapping->nonVisibleFieldsSaf)) {
                    foreach ($mapping->nonVisibleFieldsSaf as $item) {
                        $nonVisibleLists[$item->saf_field_id][] = $mapping->non_visible_list;
                    }
                }

                if (isset($mapping->nonVisibleFieldsMdm)) {
                    foreach ($mapping->nonVisibleFieldsMdm as $item) {
                        $nonVisibleLists[$item->constructor_data_set_field_id][] = $mapping->non_visible_list;
                    }
                }
                $nonVisibleListCount++;
            }

            if (!$onlyNoneVisible) {
                if (isset($mapping->editable_list)) {
                    $merged['editable'] = self::getListFieldsArray($mapping->editableFieldsSaf, 'saf', $merged['editable']);
                    $merged['editable'] = self::getListFieldsArray($mapping->editableFieldsMdm, 'mdm', $merged['editable']);
                }

                if (isset($mapping->mandatory_list)) {
                    $merged['mandatory'] = self::getListFieldsArray($mapping->mandatoryFieldsSaf, 'saf', $merged['mandatory']);
                    $merged['mandatory'] = self::getListFieldsArray($mapping->mandatoryFieldsMdm, 'mdm', $merged['mandatory']);
                }
            }
        }

        $nonVisibleList = [];
        foreach ($nonVisibleLists as $key => &$item) {
            if (count($item) != $nonVisibleListCount) {
                unset($nonVisibleLists[$key]);
            } else {
                $nonVisibleList[] = $key;
            }
        }

        $merged['non_visible'] = (count($mappings) > 1 && count($mappingWithLists['non_visible']) == count($mappings)) ? array_unique($nonVisibleList) : $nonVisibleList;
        $merged['mandatory'] = (count($mappings) > 1 && count($mappingWithLists['mandatory']) == count($mappings)) ? array_unique(array_diff_assoc($merged['mandatory'], array_unique($merged['mandatory']))) : $merged['mandatory'];
        $merged['editable'] = array_unique($merged['editable']);

        return self::listFieldsForApplication($merged);
    }

    /**
     * Function to merge mapping list
     *
     * @param $mappings
     * @param $tabLists
     * @return array
     */
    public static function margeTabLists($mappings, $tabLists)
    {
        $merged = self::LIST_TYPES;
        $nonVisibleLists = $editableLists = [];
        $nonVisibleListCount = $editableListCount = 0;

        foreach ($mappings as $mapping) {

            if (isset($mapping->non_visible_list)) {
                if (isset($mapping->nonVisibleTabs)) {
                    foreach ($mapping->nonVisibleTabs as $item) {
                        $nonVisibleLists[$item->tab_key][] = $mapping->non_visible_list;
                    }
                }
                $nonVisibleListCount++;
            }

            if (isset($mapping->editable_list)) {
                if (isset($mapping->editableTabs)) {
                    foreach ($mapping->editableTabs as $item) {
                        $editableLists[$item->tab_key][] = $mapping->non_visible_list;
                    }
                }
                $editableListCount++;
            }
        }

        $nonVisibleList = [];
        foreach ($nonVisibleLists as $key => &$item) {
            if (count($item) != $nonVisibleListCount) {
                unset($nonVisibleLists[$key]);
            } else {
                $nonVisibleList[] = $key;
            }
        }

        $editableList = [];
        foreach ($editableLists as $key => &$item) {
            if (($key != 'instructions' && $key != 'lab_examination') && count($item) != $editableListCount) {
                unset($editableLists[$key]);
            } else {
                $editableList[] = $key;
            }
        }

        $merged['non_visible'] = (count($mappings) > 1 && count($tabLists['non_visible']) == count($mappings)) ? array_unique($nonVisibleList) : $nonVisibleList;
        $merged['editable'] = (count($mappings) > 1 && count($tabLists['editable']) == count($mappings)) ? array_unique($editableList) : $editableList;

        return self::listFieldsForApplication($merged);
    }

    /**
     * Function to get only editable lists
     *
     * @param $mappings
     * @return array
     */
    public static function margeMappingListsGetEditable($mappings)
    {
        $editableLists['editable'] = [];
        foreach ($mappings as $mapping) {
            if (isset($mapping->editable_list)) {
                $editableLists['editable'] = self::getListFieldsArray($mapping->editableFieldsSaf, 'saf', $editableLists['editable']);
                $editableLists['editable'] = self::getListFieldsArray($mapping->editableFieldsMdm, 'mdm', $editableLists['editable']);
            }
        }

        $editableLists = array_unique($editableLists);

        return self::listFieldsForApplication($editableLists);
    }

    /**
     * Function to get List Fields Array
     *
     * @param $objectData
     * @param $type
     * @param $merged
     * @return array
     */
    private static function getListFieldsArray($objectData, $type, $merged)
    {
        if (!empty($objectData)) {
            foreach ($objectData as $data) {
                $merged[] = ($type == 'saf') ? $data->saf_field_id : $data->constructor_data_set_field_id;
            }
        }

        return $merged;
    }

    /**
     * Function to get List Tabs Array
     *
     * @param $objectData
     * @param $merged
     * @return array
     */
    private static function getListTabsArray($objectData, $merged)
    {
        if (!empty($objectData)) {
            foreach ($objectData as $data) {
                $merged[] = $data->tab_key;
            }
        }

        return $merged;
    }

    /**
     * Function to get list Fields For Application
     *
     * @param $merged
     * @return array
     */
    private static function listFieldsForApplication($merged)
    {
        $returnArray = [];
        foreach ($merged as $listType => $fields) {
            foreach ($fields as $field) {
                $returnArray[$field][] = $listType;
                $returnArray[$field] = array_unique($returnArray[$field]);
            }
        }

        return $returnArray;
    }

    /**
     * Function to get mandatory lists for view
     *
     * @param $mappings
     * @return array
     */
    public static function generateMandatoryListForView($mappings)
    {
        $mandatory = [];
        $mandatoryListCount = 0;
        $constructorListsAllData = ConstructorLists::select('id')->active()->get()->keyBy('id');

        foreach ($mappings as $mapping) {
//            $checkMapping = ConstructorLists::select('id')->where('id', $mapping->mandatory_list)->where('show_status', '1')->first();

            if (isset($mapping->mandatory_list) && isset($constructorListsAllData[$mapping->mandatory_list])) {
                if (isset($mapping->mandatoryFieldsSaf)) {
                    foreach ($mapping->mandatoryFieldsSaf as $item) {
                        $mandatory[$item->saf_field_id][] = $mapping->mandatory_list;
                    }
                }

                if (isset($mapping->mandatoryFieldsMdm)) {
                    foreach ($mapping->mandatoryFieldsMdm as $item) {
                        $mandatory[$item->constructor_data_set_field_id][] = $mapping->mandatory_list;
                    }
                }

                $mandatoryListCount++;
            }
        }

        foreach ($mandatory as $key => &$item) {
            if (count($item) != $mandatoryListCount) {
                unset($mandatory[$key]);
            } else {
                $item = true;
            }
        }

        return $mandatory;
    }
}
