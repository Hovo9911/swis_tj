<?php

namespace App\Models\ConstructorLists;

use App\Models\ConstructorListToDataSetFromMdm\ConstructorListToDataSetFromMdm;
use App\Models\ConstructorListToDataSetFromSaf\ConstructorListToDataSetFromSaf;
use App\Models\ConstructorListToTabs\ConstructorListToTabs;
use Illuminate\Support\Facades\DB;

/**
 * Class ConstructorListsManager
 * @package App\Models\ConstructorLists
 */
class ConstructorListsManager
{
    /**
     * Function to Store data to DB
     *
     * @param array $data
     * @return ConstructorLists
     */
    public function store(array $data)
    {
        $safParams = isset($data['paramsSaf']) ? $data['paramsSaf'] : [];
        $mdmParams = isset($data['paramsMDM']) ? $data['paramsMDM'] : [];
        $tabsParams = isset($data['paramsTabs']) ? $data['paramsTabs'] : [];

        unset($data['paramsMDM']);
        unset($data['paramsSaf']);
        unset($data['paramsTabs']);

        $constructorLists = new ConstructorLists($data);

        return DB::transaction(function () use ($data, $constructorLists, $safParams, $mdmParams, $tabsParams) {
            $constructorLists->save();
            $safParams = $this->generateSafAndMdmRows($safParams, $constructorLists->id, 'saf');
            $mdmParams = $this->generateSafAndMdmRows($mdmParams, $constructorLists->id, 'mdm');
            $tabsParams = $this->generateTabsRows($tabsParams, $constructorLists->id);

            ConstructorListToDataSetFromSaf::insert($safParams);
            ConstructorListToDataSetFromMdm::insert($mdmParams);
            ConstructorListToTabs::insert($tabsParams);

            return $constructorLists;
        });
    }

    /**
     * Function to Update data
     *
     * @param array $data
     * @param int $id
     */
    public function update(array $data, int $id)
    {
        $safParams = isset($data['paramsSaf']) ? $data['paramsSaf'] : [];
        $mdmParams = isset($data['paramsMDM']) ? $data['paramsMDM'] : [];
        $tabsParams = isset($data['paramsTabs']) ? $data['paramsTabs'] : [];

        unset($data['paramsMDM']);
        unset($data['paramsSaf']);
        unset($data['tabsParams']);

        $constructorLists = ConstructorLists::where('id', $id)->constructorListOwner()->firstOrFail();

        DB::transaction(function () use ($data, $constructorLists, $safParams, $mdmParams, $tabsParams) {
            ConstructorListToDataSetFromSaf::where('constructor_list_id', $constructorLists->id)->delete();
            ConstructorListToDataSetFromMdm::where('constructor_list_id', $constructorLists->id)->delete();
            ConstructorListToTabs::where('constructor_list_id', $constructorLists->id)->delete();

            $constructorLists->update($data);
            $safParams = $this->generateSafAndMdmRows($safParams, $constructorLists->id, 'saf');
            $mdmParams = $this->generateSafAndMdmRows($mdmParams, $constructorLists->id, 'mdm');
            $tabsParams = $this->generateTabsRows($tabsParams, $constructorLists->id);

            ConstructorListToDataSetFromSaf::insert($safParams);
            ConstructorListToDataSetFromMdm::insert($mdmParams);
            ConstructorListToTabs::insert($tabsParams);
        });
    }

    /**
     * Function to generate insert array
     *
     * @param $data
     * @param $constructorListId
     * @param $type
     * @return array
     */
    private function generateSafAndMdmRows($data, $constructorListId, $type)
    {
        $insertData = [];

        foreach ($data as $field => $item) {
            $key = ($type == 'saf') ? 'saf_field_id' : 'constructor_data_set_field_id';
            $insertData[] = [
                'constructor_list_id' => $constructorListId,
                $key => $field
            ];
        }

        return $insertData;
    }

    /**
     * Function to generate insert array
     *
     * @param $tabsParams
     * @param $constructorListId
     * @param array $insertData
     * @return array
     */
    private function generateTabsRows($tabsParams, $constructorListId, $insertData = [])
    {
        foreach ($tabsParams as $tab => $item) {
            $insertData[] = [
                'constructor_list_id' => $constructorListId,
                'tab_key' => $tab
            ];
        }

        return $insertData;
    }

    /**
     * Function to get logged data
     *
     * @param $id
     * @param $id2
     * @return array
     */
    public function getLogData($id, $id2 = null)
    {
        $data = ConstructorLists::where('id', $id)->first()->toArray();

        $data['paramsSaf'] = ConstructorListToDataSetFromSaf::where('constructor_list_id', $id)->get()->toArray();
        $data['paramsMDM'] = ConstructorListToDataSetFromMdm::where('constructor_list_id', $id)->get()->toArray();
        $data['paramsTabs'] = ConstructorListToTabs::where('constructor_list_id', $id)->get()->toArray();

        return $data;
    }

}
