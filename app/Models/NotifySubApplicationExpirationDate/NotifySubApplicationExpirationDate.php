<?php

namespace App\Models\NotifySubApplicationExpirationDate;

use App\Models\BaseModel;

class NotifySubApplicationExpirationDate extends BaseModel
{
    /**
     * @var int
     */
    const NOTIFIED = 1;
    const NON_NOTIFIED = 0;
    const REQUESTED = 2;

    /**
     * @var string
     */
    public $table = 'notify_subapplication_expiration_date';

    /**
     * @var bool
     */
    protected $hasAdminInfo = true;

    /**
     * @var array
     */
    protected $fillable = [
        'subapplication_id',
        'date_of_notification',
        'date_of_subapplication_end',
        'is_notified',
        'working_minutes_in_requested_status',
        'auto_released_status'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'created_admin_id',
        'created_admin_ip',
        'updated_at',
        'updated_admin_id',
        'updated_admin_ip',
        'deleted_admin_id',
        'deleted_admin_ip'
    ];
}
