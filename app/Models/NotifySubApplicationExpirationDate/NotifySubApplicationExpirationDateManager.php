<?php

namespace App\Models\NotifySubApplicationExpirationDate;

use App\Models\Agency\Agency;
use App\Models\Calendar\Calendar;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class NotifySubApplicationExpirationDateManager
 * @package App\Models\NotifySubApplicationExpirationDate
 */
class NotifySubApplicationExpirationDateManager extends Model
{
    /**
     * Function to store data
     *
     * @param array $data
     */
    public function store(array $data)
    {
        // Sub Application created date
        $dateFrom = SingleApplicationSubApplications::where('id', $data['subapplication_id'])->pluck('created_at')->first();

        if (!is_null($dateFrom)) {
            $dateFrom = $dateFrom->toDateTimeString();
        }

        $dateTo = Calendar::nextDate($dateFrom, 'date_to', $data['agency_id']);
        $documentTypeId = ConstructorDocument::where('id', $data['document_id'])->pluck('document_type_id')->first();
        $documentTypeReferenceLastActiveId = optional(getReferenceItemLastActiveRow(ReferenceTable::REFERENCE_DOCUMENT_TYPE_REFERENCE, $documentTypeId))->id;

        $documentTypeReference = DB::table(ReferenceTable::REFERENCE_DOCUMENT_TYPE_REFERENCE)
            ->select('control_period_hours', 'auto_release', 'notify_hours_ago')
            ->where(['id' => $documentTypeReferenceLastActiveId])
            ->first();

        $controlPeriodHours = optional($documentTypeReference)->control_period_hours;
        $notifyHoursAgo = $documentTypeReference->notify_hours_ago ?? 0;
        $documentAutoRelease = optional($documentTypeReference)->auto_release;
        $autoReleasedStatus = 2;

        if (!is_null($documentAutoRelease)) {

            $documentAutoRelease = getReferenceRows(ReferenceTable::REFERENCE_BOOLEAN_VALUES, $documentAutoRelease);
            if ($documentAutoRelease->code == ReferenceTable::REFERENCE_BOOLEAN_CODE_TRUE) {
                $autoReleasedStatus = 0;
            }
        }

        if (!is_null($controlPeriodHours)) {

            //
            if ($controlPeriodHours > $notifyHoursAgo) {
                $diffControlPeriodAndNotifyHours = $controlPeriodHours - $notifyHoursAgo;
            }

            // Control period in minutes
            $controlPeriodMinutes = $controlPeriodHours * 60;

            // Difference between control period and notify hours in minutes
            $diffControlPeriodAndNotifyMinutes = ($diffControlPeriodAndNotifyHours ?? $controlPeriodHours) * 60;

            $notifySubApplicationExpirationDate = NotifySubApplicationExpirationDate::select('id', 'updated_at', 'working_minutes_in_requested_status')->where('subapplication_id', $data['subapplication_id'])->first();

            if (is_null($notifySubApplicationExpirationDate)) {

                $notifiedData = [
                    'subapplication_id' => $data['subapplication_id'],
                    'date_of_notification' => Agency::getNotificationDate($dateFrom, $dateTo, $diffControlPeriodAndNotifyMinutes, $data['agency_id']),
                    'date_of_subapplication_end' => Agency::getNotificationDate($dateFrom, $dateTo, $controlPeriodMinutes, $data['agency_id']),
                    'is_notified' => NotifySubApplicationExpirationDate::NON_NOTIFIED,
                    'auto_released_status' => $autoReleasedStatus
                ];

                NotifySubApplicationExpirationDate::create($notifiedData);

            } else {

                $workingMinutesInRequestedStatus = Agency::getWorkingHoursBetweenDates(Carbon::parse($notifySubApplicationExpirationDate->updated_at)->format(config('swis.date_time_format')), now()->toDateTimeString(), $data['agency_id']);
                $workingMinutesInRequestedStatus += (int)$notifySubApplicationExpirationDate->working_minutes_in_requested_status;

                $dateOfNotification = Agency::getNotificationDate($dateFrom, $dateTo, ($diffControlPeriodAndNotifyMinutes + $workingMinutesInRequestedStatus), $data['agency_id']);
                $dateOfSubApplicationEnd = Agency::getNotificationDate($dateFrom, $dateTo, ($controlPeriodMinutes + $workingMinutesInRequestedStatus), $data['agency_id']);

                NotifySubApplicationExpirationDate::where('subapplication_id', $data['subapplication_id'])
                    ->update([
                        'date_of_notification' => $dateOfNotification,
                        'date_of_subapplication_end' => $dateOfSubApplicationEnd,
                        'is_notified' => NotifySubApplicationExpirationDate::NON_NOTIFIED,
                        'working_minutes_in_requested_status' => $workingMinutesInRequestedStatus,
                    ]);
            }
        }
    }

    /**
     * Function to update data
     *
     * @param array $data
     * @return bool|void
     */
    public function updateData(array $data)
    {
        $notifySubApplicationExpirationDate = NotifySubApplicationExpirationDate::select('id')->where('subapplication_id', $data['subapplication_id'])->first();

        if (!is_null($notifySubApplicationExpirationDate)) {
            NotifySubApplicationExpirationDate::where('subapplication_id', $data['subapplication_id'])
                ->update(['is_notified' => NotifySubApplicationExpirationDate::REQUESTED]);
        }
    }
}

