<?php

namespace App\Models\SingleApplicationDataStructure;

use App\Models\BaseModel;
use SimpleXMLElement;

/**
 * Class SingleApplicationDataStructure
 * @package App\Models\SingleApplicationDataStructure
 */
class SingleApplicationDataStructure extends BaseModel
{
    /**
     * @var string
     */
    public $table = 'saf_data_structure';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'version',
        'xml',
        'show_status',
    ];

    /**
     * @var null
     */
    private static $instance = null;

    /**
     * @var null
     */
    protected $lastXml = null;

    /**
     * Function to gets the instance via lazy initialization (created on first usage)
     */
    public static function getInstance(): SingleApplicationDataStructure
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * @param $value
     * @return string
     */
    public function getXmlInternalAttribute($value)
    {
        return new SimpleXMLElement($value);
    }

    /**
     * @param $value
     * @return string
     */
    public function getXmlAttribute($value)
    {
        return new SimpleXMLElement($value);
    }

    /**
     * Function to get last XML version
     *
     * @param bool $type
     * @return bool|SimpleXMLElement
     */
    public static function lastXml($type = false)
    {
        if (is_null(SingleApplicationDataStructure::getInstance()->lastXml)) {
            $result = SingleApplicationDataStructure::where('show_status', SingleApplicationDataStructure::STATUS_ACTIVE)->orderByDesc()->first();
            SingleApplicationDataStructure::getInstance()->lastXml = $result;
        } else {
            $result = SingleApplicationDataStructure::getInstance()->lastXml;
        }

        if (!is_null($result)) {

            if (!$type) {
                return $result->xml;
            }

            if ($type == 'id') {
                return $result->id;
            }

        }

        return false;
    }

    /**
     * Function to get xml field id(key) mdm(value)
     *
     * @return array
     */
    public static function getXmlIdMdm()
    {
        $lastXml = self::lastXml();

        $xmlFields = [];
        foreach ($lastXml->xpath("//field") as $value){
            $xmlFields[(string)$value->attributes()->id] = (int)$value->attributes()->mdm;
        }

        return $xmlFields;
    }
}
