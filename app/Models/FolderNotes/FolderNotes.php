<?php

namespace App\Models\FolderNotes;

use App\Models\BaseModel;
use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

/**
 * Class FolderNotes
 * @package App\Models\FolderNotes
 */
class FolderNotes extends BaseModel
{
    /**
     * @var string
     */
    const FOLDER_NOTE_TRANS_KEY_SAVE_NOTE = 'swis.folder.notes.save_note';
    const FOLDER_NOTE_TRANS_KEY_SAVE = 'swis.folder.notes.save';
    const FOLDER_NOTE_TRANS_KEY_CREATE = 'swis.folder.notes.created';

    /**
     * @var string (In trans Variables list)
     */
    const NOTE_VARIABLE_NOTE = 'NOTE';
    const NOTE_VARIABLE_USER_NAME = 'USER_NAME';

    /**
     * @var string
     */
    public $table = 'folder_notes';

    /**
     * @var bool
     */
    protected $hasAdminInfo = false;

    /**
     * @var array
     */
    protected $fillable = [
        'version',
        'user_id',
        'folder_id',
        'trans_key',
        'note',
        'old_data',
        'new_data'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'old_data' => 'array',
        'new_data' => 'array'
    ];

    /**
     * Function to get created_at value
     *
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format(config('swis.date_time_format_front'));
    }

    /**
     * Function to store folder notes
     *
     * @param array $data
     */
    public static function storeNotes(array $data)
    {
        $defaultData = [
            'version' => 1,
            'user_id' => Auth::id(),
            'trans_key' => '',
            'note' => null,
            'old_data' => null,
            'new_data' => null,
        ];

        $insertData = array_merge($defaultData, $data);

        self::create($insertData);
    }

    /**
     * Function to get notes
     *
     * @param $id
     * @return string
     */
    public static function getNotes($id)
    {
        $notes = self::where('folder_id', $id)
            ->orderByDesc()
            ->get();

        return (new self)->generateWithTrans($notes);
    }

    /**
     * Function to get notes with trans
     *
     * @param $notes
     * @return string
     */
    public function generateWithTrans($notes)
    {
        foreach ($notes as &$note) {
            $note->note = self::getVariablesValues($note, $note->trans_key);
        }

        return $notes;
    }

    /**
     * Function to change variable name with value
     *
     * @param $note
     * @param $trans
     * @return array
     */
    private static function getVariablesValues($note, $trans)
    {
        $transValue = trans($trans);
        $dataVariables = getStringsBetween($transValue, '{', '}');
        $noteCreatedUserName = User::getUserNameByID($note->user_id);

        $dataVariablesValues = [
            self::NOTE_VARIABLE_USER_NAME => $noteCreatedUserName
        ];

        foreach ($dataVariables as $dataVariable) {
            switch ($dataVariable) {
                case self::NOTE_VARIABLE_NOTE:
                    $dataVariablesValues[self::NOTE_VARIABLE_NOTE] = $note->note ?? '-';
                    break;
            }
        }

        foreach ($dataVariablesValues as $variableKey => $variablesValue) {
            $variableKeyWith = "{{$variableKey}}";
            if (strpos($transValue, $variableKeyWith) !== false) {

                if ($variableKey == self::NOTE_VARIABLE_NOTE) {
                    $variablesValue = "<b>$variablesValue</b>";
                }

                $transValue = str_replace($variableKeyWith, trans($variablesValue), $transValue);
            }
        }

        return $transValue;
    }
}
