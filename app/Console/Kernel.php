<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\RemoveSessionIds',
        'App\Console\Commands\GenerateCrudCommand',
        'App\Console\Commands\SendPendingEmails',
        'App\Console\Commands\SendPendingSMS',
        'App\Console\Commands\InactivateDataModelRows',
        'App\Console\Commands\InactivateReferenceTables',
        'App\Console\Commands\InactivateLaboratories',
        'App\Console\Commands\InactivateBrokers',
        'App\Console\Commands\UpdateExchangeRates',
        'App\Console\Commands\InactivateUserRoles',
        'App\Console\Commands\InactivateRegionalOffices',
        'App\Console\Commands\UpdateHsCodesRecursive',
        'App\Console\Commands\RemoveUserRoleMenus',
        'App\Console\Commands\NotifyAboutMyApplicationExpirationDate',
        'App\Console\Commands\RemoveBlockedUsers',
        'App\Console\Commands\CheckPendingPayments',
        \jdavidbakr\MultiServerEvent\Commands\MultiServerMigrationService::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @return void
     */
    protected function defineConsoleSchedule()
    {
        $this->app->instance(
            Schedule::class,
            $schedule = new \jdavidbakr\MultiServerEvent\Scheduling\Schedule()
        );

        $this->schedule($schedule);
    }

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('remove:session')
            ->dailyAt('01:00')
            ->withoutOverlappingMultiServer()
            ->ensureFinishedMultiServer(10);
        $schedule->command('send:PendingEmails')
            ->everyMinute()
            ->withoutOverlapping()
            ->withoutOverlappingMultiServer()
            ->ensureFinishedMultiServer(10);
        $schedule->command('send:PendingSms')
            ->everyMinute()
            ->withoutOverlapping()
            ->withoutOverlappingMultiServer()
            ->ensureFinishedMultiServer(10);
        $schedule->command('Inactivating:DataModelRows')
            ->daily()
            ->withoutOverlapping()
            ->withoutOverlappingMultiServer()
            ->ensureFinishedMultiServer(10);
        $schedule->command('Inactivating:Laboratories')
            ->daily()
            ->withoutOverlapping()
            ->withoutOverlappingMultiServer()
            ->ensureFinishedMultiServer(10);
        $schedule->command('Inactivating:Brokers')
            ->daily()
            ->withoutOverlapping()
            ->withoutOverlappingMultiServer()
            ->ensureFinishedMultiServer(10);
        $schedule->command('Inactivating:UserRoles')
            ->daily()
            ->withoutOverlapping()
            ->withoutOverlappingMultiServer()
            ->ensureFinishedMultiServer(10);
        $schedule->command('Inactivating:RegionalOffice')
            ->daily()
            ->withoutOverlapping()
            ->withoutOverlappingMultiServer()
            ->ensureFinishedMultiServer(120);
        $schedule->command('update:hsCode_path_name')
            ->everyMinute()
            ->withoutOverlapping()
            ->withoutOverlappingMultiServer()
            ->ensureFinishedMultiServer(10);
        $schedule->command('update:ExchangeRatesRef')
            ->dailyAt('08:30')
            ->withoutOverlapping()
            ->withoutOverlappingMultiServer()
            ->ensureFinishedMultiServer(10);
        $schedule->command('update:ExchangeRatesRef')
            ->dailyAt('17:30')
            ->withoutOverlapping()
            ->withoutOverlappingMultiServer()
            ->ensureFinishedMultiServer(10);
        $schedule->command('remove:userRoleMenus')
            ->weekly()
            ->withoutOverlapping()
            ->withoutOverlappingMultiServer()
            ->ensureFinishedMultiServer(10);
        $schedule->command('command:NotifyAboutMyApplicationExpirationDate')
            ->everyMinute()
            ->withoutOverlapping()
            ->withoutOverlappingMultiServer()
            ->ensureFinishedMultiServer(10);
        $schedule->command('command:CheckSubApplicationAutoReleased')
            ->everyMinute()
            ->withoutOverlapping()
            ->withoutOverlappingMultiServer()
            ->ensureFinishedMultiServer(10);
        $schedule->command('command:RemoveBlockedUsers')
            ->everyMinute()
            ->withoutOverlapping()
            ->withoutOverlappingMultiServer()
            ->ensureFinishedMultiServer(10);
        $schedule->command('check:PendingPayments')
            ->hourly()
            ->withoutOverlapping()
            ->withoutOverlappingMultiServer()
            ->ensureFinishedMultiServer(10);
        $schedule->command('Inactivating:ReferenceTablesRows')
            ->dailyAt('00:05')
            ->withoutOverlapping()
            ->withoutOverlappingMultiServer()
            ->ensureFinishedMultiServer(10);

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
