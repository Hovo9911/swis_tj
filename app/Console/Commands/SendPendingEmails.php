<?php

namespace App\Console\Commands;

use App\Facades\Mailer;
use Illuminate\Console\Command;

/**
 * Class SendPendingEmails
 * @package App\Console\Commands
 */
class SendPendingEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:PendingEmails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send pending emails';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        Mailer::sendEmails();
    }
}
