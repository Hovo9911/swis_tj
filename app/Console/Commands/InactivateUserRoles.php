<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\AuthorizePerson\AuthorizePersonManager;

class InactivateUserRoles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Inactivating:UserRoles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for inactivating user roles';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $userRolesManager = new AuthorizePersonManager();
        $userRolesManager->inactivateUserRoles();
    }
}
