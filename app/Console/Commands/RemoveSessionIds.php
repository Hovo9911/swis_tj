<?php

namespace App\Console\Commands;

use App\Models\UserSession\UserSession;
use Carbon\Carbon;
use Illuminate\Console\Command;

class RemoveSessionIds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remove:session';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to remove old sessions.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        UserSession::where('created_at', '<=', Carbon::now()->subDay())->delete();
    }
}
