<?php

namespace App\Console\Commands;

use App\Models\UserAuthFail\UserAuthFail;
use Illuminate\Console\Command;

class RemoveBlockedUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:RemoveBlockedUsers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove blocked users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $blockTime = now()->subMinutes(config('global.user_login.block_time'))->toDateTimeString();

        $blockedUsers = UserAuthFail::getBlockedUsers($blockTime);

        foreach ($blockedUsers as $blockedUser) {
            UserAuthFail::where(['username' => $blockedUser->username])->delete();
        }
    }
}
