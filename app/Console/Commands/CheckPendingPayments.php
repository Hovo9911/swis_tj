<?php

namespace App\Console\Commands;

use App\Payments\PaymentsManager;
use Illuminate\Console\Command;

class CheckPendingPayments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:PendingPayments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to check pending Alif payments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $paymentManager = new PaymentsManager();
        $paymentManager->checkPendingPayments();
    }
}
