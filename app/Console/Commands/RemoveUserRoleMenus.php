<?php

namespace App\Console\Commands;

use App\Models\UserRolesMenus\UserRolesMenusManager;
use Illuminate\Console\Command;

class RemoveUserRoleMenus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remove:userRoleMenus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear user_role_menus table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $userRolesMenusManager = new UserRolesMenusManager();
        $userRolesMenusManager->removeUserRoleMenus();
    }
}
