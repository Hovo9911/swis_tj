<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class GenerateConfigs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:configs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generating default configs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *''
     * @return mixed
     */
    public function handle()
    {
        $xmlDoc = new \DOMDocument();
        $xmlDoc->load(env('CONFIG_XML_PATH').'/config.xml');

        $globalArr = [];
        if ($xmlDoc->schemaValidate(env('CONFIG_XML_PATH').'/config.xsd')) {

            $auth_verify = $xmlDoc->getElementsByTagName('auth_verify');

            if($auth_verify->length > 0){
                foreach ($auth_verify as $verify){

                    $email = $verify->getElementsByTagName("email");
                    $phone = $verify->getElementsByTagName("phone");

                    $globalArr['auth_verify'] = [
                        'email'=>  [
                            'mode'=>  $this->get_bool($email->item(0)->getAttribute("mode")),
                            'token_count'=>  (int)$email->item(0)->getAttribute("token_count"),
                            'send_again_count'=>  (int)$email->item(0)->getAttribute("send_again_count"),
                        ],
                        'phone'=>  [
                            'mode'=>  $this->get_bool($phone->item(0)->getAttribute("mode")),
                            'sms_count'=>  (int)$phone->item(0)->getAttribute("sms_count"),
                            'send_again_count'=>  (int)$phone->item(0)->getAttribute("send_again_count"),
                        ]
                    ];
                }
            }else{
                echo 'Auth Verify configs not defined!';
            }

            $tax_id = $xmlDoc->getElementsByTagName('tax_id');


            if($tax_id->length > 0){
                $globalArr['tax_id'] = [
                    'min'=>(int)$tax_id->item(0)->getAttribute('min'),
                    'max'=>(int)$tax_id->item(0)->getAttribute('max')
                ];
            }else{
                echo 'Tax Code configs not defined!';
            }
        } else {
            echo 'Not valid xmd!';
        }

        file_put_contents(config_path('global.php'), "<?php \r\nreturn " . $this->var_export54($globalArr) . ";");

        echo 'Successfully Generated'."\n";
    }

    /**
     * @param $var
     * @param string $indent
     * @return mixed|string
     */
    private function var_export54($var, $indent = "")
    {
        switch (gettype($var)) {
            case "string":
                return '"' . addcslashes($var, "\\\$\"\r\n\t\v\f") . '"';
            case "array":
                $indexed = array_keys($var) === range(0, count($var) - 1);
                $r = [];
                foreach ($var as $key => $value) {
                    $r[] = "$indent    "
                        . ($indexed ? "" : $this->var_export54($key) . " => ")
                        . $this->var_export54($value, "$indent    ");
                }
                return "[\n" . implode(",\n", $r) . "\n" . $indent . "]";
            case "boolean":
                return $var ? "TRUE" : "FALSE";
            default:
                return var_export($var, TRUE);
        }
    }

    /**
     * @param $value
     * @return bool|null
     */
    private function get_bool($value){
        switch( strtolower($value) ){
            case 'true': return true;
            case 'false': return false;
            default: return NULL;
        }
    }

}
