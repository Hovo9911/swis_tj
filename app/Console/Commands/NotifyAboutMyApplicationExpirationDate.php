<?php

namespace App\Console\Commands;

use App\Models\ConstructorStates\ConstructorStates;
use App\Models\NotificationTemplates\NotificationTemplates;
use App\Models\NotificationTemplates\NotificationTemplatesManager;
use App\Models\NotifySubApplicationExpirationDate\NotifySubApplicationExpirationDate;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

/**
 * Class NotifyAboutMyApplicationExpirationDate
 * @package App\Console\Commands
 */
class NotifyAboutMyApplicationExpirationDate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:NotifyAboutMyApplicationExpirationDate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for notify about My Application expiration date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $notifySubApplicationExpirationDates = NotifySubApplicationExpirationDate::select('subapplication_id', 'date_of_notification')
            ->where('is_notified', NotifySubApplicationExpirationDate::NON_NOTIFIED)
            ->get();

        $now = now()->toDateTimeString();

        foreach ($notifySubApplicationExpirationDates as $notifySubApplicationExpirationDate) {

            $dateOfNotification = Carbon::parse($notifySubApplicationExpirationDate->date_of_notification);

            // If date_of_notification less than now()
            if ($dateOfNotification->lte($now)) {

                //Get subApplications when current_state != requested and current_state != end state
                $subApplication = SingleApplicationSubApplications::select('saf_sub_applications.id', 'saf_sub_applications.document_number', 'constructor_documents.id as document_id', 'saf_sub_applications.saf_number')
                    ->join('constructor_documents', "constructor_documents.document_code", "=", "saf_sub_applications.document_code")
                    ->join('saf_sub_application_states', function ($q) {
                        $q->on('saf_sub_application_states.saf_subapplication_id', '=', 'saf_sub_applications.id')->where('is_current', '1');
                    })
                    ->join('constructor_states', function ($q) {
                        $q->on('constructor_states.id', '=', 'saf_sub_application_states.state_id')->where('state_type', '!=', ConstructorStates::END_STATE_TYPE);
                    })
                    ->where('saf_sub_applications.id', $notifySubApplicationExpirationDate->subapplication_id)
                    ->where('current_status', '!=', SingleApplicationSubApplications::STATUS_REQUESTED)
                    ->first();

                if (!is_null($subApplication)) {
                    //Get users group
                    $userTypes = User::USER_TYPES_FOR_NOTIFY_ABOUT_SUB_APPLICATION_EXP_DATE;
                    $usersGroup = [];

                    foreach ($userTypes as $userType) {
                        $usersGroup[$userType] = User::getUsersGroupForNotifications($userType, [], $subApplication->id);
                    }

                    DB::transaction(function () use ($usersGroup, $subApplication) {
                        //Send Notifications
                        foreach ($usersGroup as $userType => $userGroup) {
                            $this->sendNotification($userGroup, $userType, $subApplication);
                        }

                        //Update NotifySubApplicationExpirationDate table set status notified
                        NotifySubApplicationExpirationDate::where('subapplication_id', $subApplication->id)
                            ->update(['is_notified' => NotifySubApplicationExpirationDate::NOTIFIED]);
                    });
                }
            }
        }
    }

    /**
     * Function to send notifications
     *
     * @param $userGroup
     * @param $userType
     * @param $subApplication
     */
    private function sendNotification($userGroup, $userType, $subApplication)
    {
        switch ($userType) {
            case User::SAF_APPLICANT:
                $notificationTemplateId = NotificationTemplates::getNotificationIdByCode(NotificationTemplates::SUB_APPLICATION_EXPIRATION_DATE_TEMPLATE_FOR_SAF_APPLICANT);
                break;

            case User::SUB_APPLICATION_CURRENT_STATE_USERS:
                $notificationTemplateId = NotificationTemplates::getNotificationIdByCode(NotificationTemplates::SUB_APPLICATION_EXPIRATION_DATE_TEMPLATE_FOR_STATE_USERS);
                break;

            case User::AGENCY_HEADS:
                $notificationTemplateId = NotificationTemplates::getNotificationIdByCode(NotificationTemplates::SUB_APPLICATION_EXPIRATION_DATE_TEMPLATE_FOR_AGENCY_HEADS);
                break;

            case User::USER_TYPE_SW_ADMIN:
                $notificationTemplateId = NotificationTemplates::getNotificationIdByCode(NotificationTemplates::SUB_APPLICATION_EXPIRATION_DATE_TEMPLATE_FOR_SW_ADMINS);
                break;
        }

        if (isset($notificationTemplateId)) {
            (new NotificationTemplatesManager())->sendGlobalNotifications($userGroup, $userType, $notificationTemplateId, $subApplication);
        }
    }
}
