<?php

namespace App\Console\Commands;

use App\Models\Instructions\InstructionsManager;
use Illuminate\Console\Command;
use App\Models\ReferenceTableForm\ReferenceTableFormManager;

class InactivateReferenceTables extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Inactivating:ReferenceTablesRows';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for inactivating reference tables rows';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $referenceRowManager = new ReferenceTableFormManager();
        $referenceRowManager->inactivateReferenceTableRows();

        $instructionManager = new InstructionsManager();
        $instructionManager->inactivateTableRows();
    }
}
