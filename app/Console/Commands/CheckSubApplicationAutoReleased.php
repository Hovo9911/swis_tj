<?php

namespace App\Console\Commands;

use App\Models\ConstructorStates\ConstructorStates;
use App\Models\NotifySubApplicationExpirationDate\NotifySubApplicationExpirationDate;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CheckSubApplicationAutoReleased extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:CheckSubApplicationAutoReleased';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $autoReleasedSubApplications = NotifySubApplicationExpirationDate::select('subapplication_id', 'date_of_subapplication_end')
            ->where('auto_released_status', 0)
            ->get();

        $now = now()->toDateTimeString();

        foreach ($autoReleasedSubApplications as $autoReleasedSubApplication) {

            $dateOfNotification = Carbon::parse($autoReleasedSubApplication->date_of_subapplication_end);

            // If date_of_notification less than now()
            if ($dateOfNotification->lte($now)) {

                //Get subApplications when current_state != requested and current_state != end state
                $subApplication = SingleApplicationSubApplications::select('saf_sub_applications.id', 'saf_sub_applications.document_number', 'constructor_documents.id as document_id', 'saf_sub_applications.saf_number')
                    ->join('constructor_documents', "constructor_documents.document_code", "=", "saf_sub_applications.document_code")
                    ->join('saf_sub_application_states', function ($q) {
                        $q->on('saf_sub_application_states.saf_subapplication_id', '=', 'saf_sub_applications.id')->where('is_current', '1');
                    })
                    ->join('constructor_states', function ($q) {
                        $q->on('constructor_states.id', '=', 'saf_sub_application_states.state_id')->where('state_type', '!=', ConstructorStates::END_STATE_TYPE);
                    })
                    ->where('saf_sub_applications.id', $autoReleasedSubApplication->subapplication_id)
                    ->where('current_status', '!=', SingleApplicationSubApplications::STATUS_REQUESTED)
                    ->first();

                if (!is_null($subApplication)) {

                    DB::transaction(function () use ($subApplication) {

                        //Update saf_sub_applications table set auto_release status=1
                        SingleApplicationSubApplications::where('id', $subApplication->id)->update(['auto_released' => 1]);

                        //Update NotifySubApplicationExpirationDate table set auto_released_status=1
                        NotifySubApplicationExpirationDate::where('subapplication_id', $subApplication->id)->update(['auto_released_status' => 1]);
                    });
                }
            }
        }
    }
}
