<?php

namespace App\Console\Commands;

use App\Models\RegionalOffice\RegionalOfficeManager;
use Illuminate\Console\Command;

class InactivateRegionalOffices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Inactivating:RegionalOffice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for inactivating regional offices rows when accreditation_valid_until < today';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $regionalOfficeManager = new RegionalOfficeManager();
        $regionalOfficeManager->inactivateRegionalOffice();
    }
}
