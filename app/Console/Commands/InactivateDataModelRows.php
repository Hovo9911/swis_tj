<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\DataModel\DataModelManager;

class InactivateDataModelRows extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Inactivating:DataModelRows';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for inactivating data model rows when exp. date > today';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $dataModelManager = new DataModelManager();
        $dataModelManager->inactivateDataModelRows();
    }
}
