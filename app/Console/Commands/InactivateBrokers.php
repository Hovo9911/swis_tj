<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Broker\BrokerManager;

class InactivateBrokers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Inactivating:Brokers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for inactivating brokers when certification_period_validity < today';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $brokerManager = new BrokerManager();
        $brokerManager->inactivateBrokers();
    }
}
