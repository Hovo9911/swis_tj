<?php

namespace App\Console\Commands;

use App\Models\ReferenceTableForm\ReferenceTableFormManager;
use Illuminate\Console\Command;

class UpdateExchangeRates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:ExchangeRatesRef';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Exchange Rates Ref';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $referenceRowManager = new ReferenceTableFormManager();
        $referenceRowManager->updateExchangeRatesRefTable();
    }
}
