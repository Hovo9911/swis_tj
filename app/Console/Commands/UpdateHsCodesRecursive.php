<?php

namespace App\Console\Commands;

use App\Models\ReferenceTable\ReferenceTable;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UpdateHsCodesRecursive extends Command
{
    /**
     * @var
     */
    private $activeLanguages;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:hsCode_path_name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to update all or single Hs code path nam.';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now();
        ini_set('memory_limit', '128M');
        ini_set('max_execution_time', '900');

        try {
            $deleteJobs = [];
            $this->activeLanguages = activeLanguages();
            $jobs = DB::table('jobs')->select('id', 'payload')->get();

            foreach ($jobs as $job) {

                $payload = json_decode($job->payload);
                $data = unserialize($payload->data->command);

                $hsCodesData = $data->hsCodes;
                if (!is_null($hsCodesData)) {
                    if ($data->updateAllData) {
                        foreach ($hsCodesData as $item) {
                            $this->updateEachHsCode($item->id, $item->code);
                        }
                    } else {
                        $this->updateEachHsCode($hsCodesData->id, $hsCodesData->code);
                    }
                } elseif (isset($data->hsCodeId)) {
                    $item = DB::table(ReferenceTable::REFERENCE_HS_CODE_6)
                        ->select('id', 'code')
                        ->where('show_status', ReferenceTable::STATUS_ACTIVE)
                        ->where('id', $data->hsCodeId)
                        ->first();
                    if (!is_null($item)) {
                        $this->updateEachHsCode($item->id, $item->code);
                    }
                }
                $deleteJobs[] = $job->id;
            }

            DB::table('jobs')->whereIn('id', $deleteJobs)->delete();
        } catch (\Exception $exception) {

            Log::error("{$exception->getMessage()}, {$now}");
        }

    }

    /**
     * Function to update every hsCode
     *
     * @param $id
     * @param $code
     */
    private function updateEachHsCode($id, $code)
    {
        $hsCodeCodes = $this->getRelatedHsCodes($code);

        $refHsCodeTableName = ReferenceTable::REFERENCE_HS_CODE_6;
        $refHsCodeTableNameMl = $refHsCodeTableName . '_ml';
        $refFieldId = $refHsCodeTableName . '_id';

        foreach ($this->activeLanguages as $lng) {
            $getHsCodes = DB::table($refHsCodeTableName)
                ->select('code', 'name')
                ->join($refHsCodeTableNameMl, function ($q) use ($lng, $refHsCodeTableName, $refHsCodeTableNameMl, $refFieldId) {
                    $q->on($refHsCodeTableNameMl . '.' . $refFieldId, '=', $refHsCodeTableName . '.id')->where('lng_id', $lng->id);
                })
                ->whereIn('code', $hsCodeCodes)
                ->where("{$refHsCodeTableName}.show_status", ReferenceTable::STATUS_ACTIVE)
                ->pluck('name', 'code')
                ->all();
            ksort($getHsCodes);

            $pathName = $this->getPathName($getHsCodes);

            $pathName = rtrim(trim($pathName), ',');

            DB::table($refHsCodeTableNameMl)->where($refFieldId, $id)->where('lng_id', $lng->id)->update(['path_name' => $pathName]);
        }
    }

    /**
     * Function to get all related hsCodes
     *
     * @param $code
     * @param array $hsCodeCodes
     * @return array
     */
    private function getRelatedHsCodes($code, $hsCodeCodes = [])
    {
        $code = str_replace(" ", "", iconv(mb_detect_encoding($code), "UTF-8", $code));

        for ($i = 1; $i < strlen($code); $i++) {
            $hsCodeCodes[] = mb_substr($code, 0, $i);
        }
        $hsCodeCodes[] = $code;
        return $hsCodeCodes;
    }

    /**
     * Function to get path_name
     *
     * @param $getHsCodes
     * @param string $pathName
     * @return string
     */
    private function getPathName($getHsCodes, $pathName = '')
    {
        foreach ($getHsCodes as $code => $name) {
            $name = str_replace('-', '', $name);
            $pathName .= " {$name}, ";
        }

        return $pathName;
    }
}
