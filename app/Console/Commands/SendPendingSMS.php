<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Sms\SmsManager;

class SendPendingSMS extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:PendingSms';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send pending sms';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle()
    {
        $sms = new SmsManager();
        $sms->sendPendingSMS();
    }
}
