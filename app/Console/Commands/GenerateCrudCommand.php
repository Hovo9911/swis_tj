<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class GenerateCrudCommand extends Command
{
    /**
     * The name and signature of the console command.
     * Name parameter must be written in cameCase
     *
     * @var string
     */
    protected $signature = 'generate:crud {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to generate crud files with give name parameter';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $crudPrefix = $this->argument('name');

        $bar = $this->output->createProgressBar(4);

        $bar->start();

        // Make Model
        $this->call("make:model", ['name' => "Models/{$crudPrefix}/{$crudPrefix}"]);
        $bar->advance();

        // Make Model Manger
        $this->call("make:model", ['name' => "Models/{$crudPrefix}/{$crudPrefix}Manager"]);
        $bar->advance();

        // Make Model ML
        $this->call("make:model", ['name' => "Models/{$crudPrefix}/{$crudPrefix}Ml"]);
        $bar->advance();

        // Make Model Search
        $this->call("make:model", ['name' => "Models/{$crudPrefix}/{$crudPrefix}Search"]);
        $bar->advance();

        $bar->finish();
    }
}
