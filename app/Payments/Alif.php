<?php

namespace App\Payments;

/**
 * Class Alif
 * @package App\Payments
 */
class Alif
{
    /**
     * @var string
     */
    const RESPONSE_STATUS_OK = 'ok';
    const RESPONSE_STATUS_APPROVED = 'approved';
    const RESPONSE_STATUS_NOT_FOUND = 'not found';
    const RESPONSE_STATUS_FAILED = 'failed';

    /**
     * @var string
     */
    private $secretkey;
    public  $key;
    public  $amount;
    public  $info;
    public  $orderid;
    public  $callbackUrl;
    public  $returnUrl;
    public  $email;
    public  $phone;
    public  $status;
    public  $transactionId;

    /**
     * Alif constructor.
     *
     * @param $key
     * @param $pass
     */
    public function __construct($key, $pass)
    {
        $this->key = $key;
        $this->secretkey = hash_hmac('sha256', $pass, $key);
    }

    /**
     * @return string|void
     */
    function token()
    {
        $this->amount = str_replace(',', '.', sprintf('%.2f',$this->amount));
        if($this->amount !== '' && $this->key !== '' && $this->orderid !== '' && $this->callbackUrl !== ''){
            return hash_hmac('sha256', $this->key.$this->orderid.$this->amount.$this->callbackUrl, $this->secretkey);
        }
    }

    /**
     * @return string
     */
    function callback()
    {
        return hash_hmac('sha256', $this->orderid.$this->status.$this->transactionId, $this->secretkey);
    }

    /**
     * @return string
     */
    function checkOrderToken()
    {
        return hash_hmac('sha256', $this->key.$this->orderid, $this->secretkey);
    }

    /**
     * @param $jsn
     * @return string
     */
    function tokenInfo($jsn)
    {
        return hash_hmac('sha256', $jsn, $this->secretkey);
    }

}