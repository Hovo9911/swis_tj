<?php

namespace App\Payments;

use App\Models\OnlineTransactions\OnlineTransactions;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use GuzzleHttp\Client;
use Exception;
use Illuminate\Support\Facades\Log;

/**
 * Class PaymentsManager
 * @package App\Payments
 */
class PaymentsManager
{
    /**
     * Function to create Payment
     *
     * @return Alif
     */
    public static function createAlifPayment()
    {
        $key = config('payment.alif.key');
        $secret = config('payment.alif.secret');
        $alif = new Alif($key, $secret);

        $alif->callbackUrl = config('payment.alif.callback_url');
        $alif->returnUrl = config('payment.alif.return_url');
        $alif->info = 'Single window';

        return $alif;
    }

    /**
     * Function to get token from callback
     *
     * @param $content
     * @return string
     */
    public static function getCallbackToken($content)
    {
        $key = config('payment.alif.key');
        $secret = config('payment.alif.secret');
        $alif = new Alif($key, $secret);

        $alif->orderid = $content->orderId;
        $alif->status = $content->status;
        $alif->transactionId = $content->transactionId;

        return $alif->callback();
    }

    /**
     * Function to create Payment
     *
     * @param $amount
     * @param $orderId
     * @return string
     */
    public static function getAlifDynamicParams($amount, $orderId)
    {
        $key = config('payment.alif.key');
        $secret = config('payment.alif.secret');
        $callbackUrl = config('payment.alif.callback_url');

        $pay = new Alif($key, $secret);

        $pay->amount = sprintf('%.2f', $amount);
        $pay->orderid = $orderId;
        $pay->callbackUrl = $callbackUrl;
        $pay->token = $pay->token();

        return $pay->token;
    }

    /**
     * Function to check Alif payment status
     */
    public function checkPendingPayments()
    {

        $key = config('payment.alif.key');
        $secret = config('payment.alif.secret');
        $alif = new Alif($key, $secret);

        $pendingOnlineTransactions = OnlineTransactions::getPendingOnlineTransactions();

        foreach ($pendingOnlineTransactions as $onlineTransaction) {

            $alif->orderid = $onlineTransaction->id; //orderid
            $token = $alif->checkOrderToken();

            // The data to send to the API
            $postData = [
                'orderId' => "{$onlineTransaction->id}",
                'key' => $alif->key,
                'token' => $token
            ];

            $url = config('payment.alif.payment_check_url');

            // Setup cURL
            $ch = curl_init($url);

            curl_setopt_array($ch, [
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
                CURLOPT_POSTFIELDS => json_encode($postData)
            ]);

            // Send the request
            $response = curl_exec($ch);

            // Check for errors
            if ($response === FALSE) {
                Log::info('alif-payment-status-check-error: ' . __LINE__ . ' ' . curl_error($ch));
            }
            /**
             *
             * success response
             * {
             * "orderId": "12345678",
             * "transactionId": "92938922",
             * "status": "ok",
             * "token": "3792cujidwjkdj981uj918dj19djdad32d23f2f",
             * "amount": 10,
             * "phone": "+992931234455"
             * }
             * error response
             * {
             * "orderId": "12345678",
             * "status": "not found",
             * }
             * or
             * {
             * "orderId": "12345678",
             * "status": "failed",
             * }
             */
            // Decode the response
            $responseData = json_decode($response, true);


            $onlineTransaction = OnlineTransactions::where('id', $responseData['orderId'])->first();

            if (!is_null($onlineTransaction)) {
                switch ($responseData['status']) {
                    case Alif::RESPONSE_STATUS_OK:
                    case Alif::RESPONSE_STATUS_APPROVED:

                        try {

                            $client = new Client();
                            $request = $client->request("POST", urlWithLng(config('payment.api.add')), [
                                'form_params' => [
                                    'token' => config('payment.api.token'),
                                    'document_id' => $responseData['transactionId'],
                                    'payment_done_date' => time(),
                                    'amount' => $responseData['amount'],
                                    'beneficiary_name' => $responseData['phone'],
                                    'tax_id' => $onlineTransaction->company_tax_id,
                                    'user_id' => $onlineTransaction->user_id,
                                    'description' => ''
                                ]
                            ]);

                            $resultRaw = $request->getBody()->getContents();
                            $result = json_decode($resultRaw);

                            if ($result->status == OnlineTransactions::STATUS_OK) {
                                $onlineTransaction->status = OnlineTransactions::STATUS_OK;
                                $onlineTransaction->amount = $responseData['amount'];
                                $onlineTransaction->transaction_id = $responseData['transactionId'];
                            } else {

                                Log::info('alif-payment-status-check-error: ' . __LINE__ . ' ' . print_r($resultRaw . '', 1));
                                $onlineTransaction->status = OnlineTransactions::STATUS_OK;
                            }
                        } catch (Exception $exception) {

                            Log::info('alif-payment-status-check-error: ' . __LINE__ . ' ' . $exception->getMessage());
                        }

                        break;
                    case
                    Alif::RESPONSE_STATUS_FAILED:
                        $onlineTransaction->status = OnlineTransactions::STATUS_FAILED;
                        $onlineTransaction->amount = $responseData['amount'];
                        $onlineTransaction->transaction_id = $responseData['transactionId'];
                        break;
                    case Alif::RESPONSE_STATUS_NOT_FOUND:
                        $onlineTransaction->status = OnlineTransactions::STATUS_NOT_FOUND;
                        break;
                }

                $onlineTransaction->save();

            } else {
                Log::info('alif-payment-online-transaction-not-found: ' . __LINE__ .  print_r($response . '', 1));
            }
        }
    }
}