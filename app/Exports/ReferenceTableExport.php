<?php

namespace App\Exports;

use App\Models\ReferenceTable\ReferenceTable;
use App\Models\ReferenceTable\ReferenceTableStructure;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

/**
 * Class ReferenceTableExport
 * @package App\Exports
 */
class ReferenceTableExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    private $languages;
    private $exportData;
    private $activeLangs;
    private $exportFields;
    private $referenceTable;

    /**
     * ReferenceTableExport constructor.
     */
    public function __construct()
    {
    }

    /**
     * Function to Generate excel
     *
     * @return Collection
     */
    public function collection()
    {
        return collect($this->exportData);
    }

    /**
     * Function to set header in excel docs
     *
     * @return array
     */
    public function headings(): array
    {
        $keys = array_keys($this->exportFields);
        if ($this->exportData && count($this->exportData) > 0) {
            $keys = array_keys($this->exportData[0]);
        }

        return $keys;
    }

    /**
     * Function to get Data
     *
     * @param $exportFields
     * @param $languages
     * @param $referenceTable
     * @param $activeLangs
     * @return ReferenceTableExport
     */
    public function getData($exportFields, $languages, $referenceTable, $activeLangs)
    {
        $this->languages = $languages;
        $this->activeLangs = $activeLangs;
        $this->exportFields = $exportFields;
        $this->referenceTable = $referenceTable;

        $selectMLFields = ReferenceTableStructure::where('reference_table_id', $referenceTable['id'])->where('has_ml', ReferenceTableStructure::TRUE)
            ->active()
            ->pluck('field_name')
            ->all();

        $selectMLFields[] = 'lng_id';

        $rTable = 'reference_' . $this->referenceTable->table_name;
        $rTableMl = 'reference_' . $this->referenceTable->table_name . '_ml';

        $referenceTableData = DB::table($rTable)->where('show_status', ReferenceTable::STATUS_ACTIVE)->get();

        $exportData = [];
        foreach ($referenceTableData as $refKey => $tableItem) {
            foreach ($this->exportFields as $fields => $item) {
                if (array_key_exists($fields, $tableItem)) {
                    if ($rTable == ReferenceTable::REFERENCE_OBLIGATION_BUDGET_LINE && $fields == 'account_number') {
                        $exportData[$refKey][$fields] = isset($tableItem->{$fields}) ? (string)"'{$tableItem->{$fields}}'" : "";
                    } else {
                        $exportData[$refKey][$fields] = isset($tableItem->{$fields}) ? (string)"{$tableItem->{$fields}}" : "";
                    }
                } else {

                    $referenceId = "{$rTable}_id";
                    $langNames = DB::table($rTableMl)->select($selectMLFields)->where($referenceId, $tableItem->id)->get();

                    /*if ($item['type'] === 'select') {
                        if (array_key_exists('source', $item['parameters'])) {
                            $sourceTableParams = ReferenceTable::select('id', 'table_name')->find($item['parameters']['source']);
                            $sourceTable = DB::table("reference_{$sourceTableParams->table_name}")->where('id', $tableItem->{$item['relation_column']})->first();

                            if (is_null($sourceTable)) {
                                $exportData[$refKey][$fields] = "Not Found";
                                continue;
                            }

                            $sourceTableMl = DB::table("reference_{$sourceTableParams->table_name}_ml")->where("reference_{$sourceTableParams->table_name}_id", $sourceTable->id)->get();
                            $relationLabels = [];

                            foreach ($sourceTableMl as $source) {
                                $relationLabels[$source->lng_id] = $source->name;
                            }

                            foreach ($this->activeLangs as $lang) {
                                $exportData[$refKey]["{$item['relation_column']}_label_$lang->code"] = (string)$relationLabels[$lang->id];
                            }
                        }
                    } else {

                    }*/

                    foreach ($langNames as $lang) {
                        $exceptionList = [$referenceId, "show_status", "lng_id"];
                        foreach ($lang as $k => $val) {
                            if (!in_array($k, $exceptionList)) {
                                $exportData[$refKey][$this->languages[$lang->lng_id][$k]] = (string)$lang->{$k};
                            }
                        }
                    }
                }
            }
        }

        $this->exportData = $exportData;

        return $this;
    }
}
