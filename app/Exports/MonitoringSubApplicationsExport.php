<?php

namespace App\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithHeadings;

/**
 * Class MonitoringSubApplicationsExport
 * @package App\Exports
 */
class MonitoringSubApplicationsExport implements FromCollection, WithHeadings
{
    use Exportable, RegistersEventListeners;

    private $exportData;

    /**
     * Function to Generate excel
     *
     * @return Collection
     */
    public function collection()
    {
        return collect($this->exportData);
    }

    /**
     * Function to set header in excel docs
     *
     * @return array
     */
    public function headings(): array
    {
        return [trans('ID'),
            trans('swis.user_documents.state.label'),
            trans('swis.saf.status_type'),
            trans('swis.single_app.regular_number'),
            trans('swis.user_document.subapplication_submitting_date'),
            trans('swis.user_document.sub_application_number'),
            trans('swis.user_document.sub_application_type'),
            trans('swis.user_document.express_control_id'),
            trans('swis.sub_application.agency.title'),
            trans('swis.monitoring.user_roles.subdivision.label'),
            trans('swis.sub_application.registration_number.title'),
            trans('swis.sub_application.registration_date.title'),
            trans('swis.sub_application.permission_number.title'),
            trans('swis.sub_application.permission_date.title'),
            trans('swis.sub_application.expiration_date.title'),
            trans('swis.sub_application.saf_applicant_tin.title'),
            trans('swis.sub_application.saf_applicant_user_tin.title'),
        ];
    }

    /**
     * Function to get data for exporting report
     *
     * @param $data
     * @return $this
     */
    public function setData($data)
    {
        $this->exportData = $data;

        return $this;
    }
}