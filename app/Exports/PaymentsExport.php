<?php

namespace App\Exports;

use App\Models\Transactions\Transactions;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

/**
 * Class PaymentsExport
 * @package App\Exports
 */
class PaymentsExport implements FromView, WithEvents
{
    use Exportable, RegistersEventListeners;

    private $exportData;

    /**
     * Function to Generate excel
     *
     * @return Collection
     */
    public function collection()
    {
        return collect($this->exportData);
    }

    /**
     * @return View
     */
    public function view(): View
    {
        return view('exports.payments-report', [
            'data' => $this->exportData,
        ]);
    }

    /**
     * @param AfterSheet $event
     */
    public static function afterSheet(AfterSheet $event)
    {
        $event->sheet->getStyle('A1')->getAlignment()->setWrapText(true);
    }

    /**
     * Function to get data for exporting report
     *
     * @param $data
     * @return $this
     */
    public function setData($data)
    {
        foreach ($data as &$value) {

            if ($value->service_provider_name == ' - ') {
                $value->service_provider_name = trans('swis.core.single_window.title');
            }

            if (trim($value->obligation_type) == '()') {
                $value->obligation_type = '';
            }

            if (is_null($value->payment_id) && $value->type == Transactions::PAYMENT_TYPE_OUTPUT) {
                $value->payment_id = 'SW' . str_pad($value->id, 10, '0', STR_PAD_LEFT);
            }

            $value->amount = $value->amount . ' ' . config('swis.default_currency');
            $value->total_balance = $value->total_balance . ' ' . config('swis.default_currency');

            unset($value->type);
        }

        $this->exportData = $data;

        return $this;
    }
}