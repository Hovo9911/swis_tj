<?php

namespace App\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithHeadings;

/**
 * Class MonitoringPaymentsExport
 * @package App\Exports
 */
class MonitoringPaymentsExport implements FromCollection, WithHeadings
{
    use Exportable, RegistersEventListeners;

    private $exportData;

    /**
     * Function to Generate excel
     *
     * @return Collection
     */
    public function collection()
    {
        return collect($this->exportData);
    }

    /**
     * Function to set header in excel docs
     *
     * @return array
     */
    public function headings(): array
    {
        return [trans('ID'),
            trans('swis.payments.username.title'),
            trans('swis.payments.user_ssn.title'),
            trans('swis.payments.company_name.title'),
            trans('swis.payments.transaction_id.title'),
            trans('swis.payments.transaction_date.title'),
            trans('swis.payments.saf_number.title'),
            trans('swis.payments.sub_application_number.title'),
            trans('swis.payments.agency.title'),
            trans('swis.payments.subdivision.title'),
            trans('swis.payments.document.title'),
            trans('swis.payments.obligation_type.title'),
            trans('swis.payments.amount.title')];
    }

    /**
     * Function to get data for exporting report
     *
     * @param $data
     * @return $this
     */
    public function setData($data)
    {
        $this->exportData = $data;

        return $this;
    }
}