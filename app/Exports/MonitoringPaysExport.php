<?php

namespace App\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithHeadings;

/**
 * Class MonitoringPaysExport
 * @package App\Exports
 */
class MonitoringPaysExport implements FromCollection, WithHeadings
{
    use Exportable, RegistersEventListeners;

    private $exportData;

    /**
     * Function to Generate excel
     *
     * @return Collection
     */
    public function collection()
    {
        return collect($this->exportData);
    }

    /**
     * Function to set header in excel docs
     *
     * @return array
     */
    public function headings(): array
    {
        return [trans('ID'),
            trans('swis.pays.username.title'),
            trans('swis.pays.user_ssn.title'),
            trans('swis.pays.transaction_id.title'),
            trans('swis.pays.transaction_date.title'),
            trans('swis.pays.amount.title'),
            trans('swis.pays.provider.title'),
            trans('swis.pays.company_tax_id.title'),
            trans('swis.pays.company_name.title')
        ];
    }

    /**
     * Function to get data for exporting report
     *
     * @param $data
     * @return $this
     */
    public function setData($data)
    {
        $this->exportData = $data;

        return $this;
    }
}