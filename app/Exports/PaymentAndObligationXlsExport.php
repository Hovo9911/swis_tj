<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

/**
 * Class PaymentAndObligationXlsExport
 * @package App\Exports
 */
class PaymentAndObligationXlsExport implements FromView, WithEvents
{
    use Exportable, RegistersEventListeners;

    private $data;
    private $searchParams;

    /**
     * Function to Generate excel
     *
     * @return Collection
     */
    public function collection()
    {
        return collect($this->data);
    }

    /**
     * @return View
     */
    public function view(): View
    {
        return view('exports.payment-and-obligation-report', [
            'data' => $this->data,
            'searchParams' => $this->searchParams,
            'fieldTrans' => $this->getSearchParamsTrans()
        ]);
    }

    /**
     * @param AfterSheet $event
     */
    public static function afterSheet(AfterSheet $event)
    {
        $event->sheet->mergeCells('A1:O1');
        $event->sheet->mergeCells('A2:O2');
        $event->sheet->mergeCells('A3:O3');
        $event->sheet->getStyle('A1')->getAlignment()->setWrapText(true);
    }

    /**
     * Function to get select fields
     */
    public function getSelectField()
    {
        return [
            'saf_obligations.id as n',
            'saf_obligations.id as id',
            'creator_user.ssn as applicant_tin',
            DB::raw("(CASE
                WHEN saf.regime='import' THEN saf.importer_value
                WHEN saf.regime='export' THEN saf.exporter_value
                ELSE saf.company_tax_id END
            ) as importer_exporter_tin"),
            'saf.regular_number as saf_number',
            'saf_sub_applications.document_number as sub_application_number',
            'agency_ml.name as agency',
            'constructor_documents_ml.document_name as document',
            DB::raw("CONCAT('(', reference_obligation_type.code , ') ', reference_obligation_type_ml.name) AS obligation_type"),
            'reference_obligation_budget_line.account_number as budget_line',
            'saf_obligations.created_at as obligation_created_date',
            'saf_obligations.payment_date as obligation_pay_date',
            'saf_obligations.id as obligation_payer',        // fields will be get value after query
            'saf_obligations.id as obligation_payer_person', // fields will be get value after query
            DB::raw('CAST(saf_obligations.obligation AS NUMERIC(10,2)) as obligation'),
            DB::raw('CAST(saf_obligations.payment AS NUMERIC(10,2)) as payment'),
            DB::raw('CAST(saf_obligations.obligation - saf_obligations.payment AS NUMERIC(10,2)) as balance'),
            // fields under this row will be remove from fetching array
            'saf_obligations.payer_user_id',
            'saf_obligations.payer_user_type',
            'saf_obligations.payer_company_tax_id',
            DB::raw("CONCAT('(', payer_user.ssn , ') ', payer_user.first_name, ' ', payer_user.last_name) AS obligation_payer_person"),
        ];
    }

    /**
     * Function to get search Params trans
     *
     * @return array
     */
    public function getSearchParamsTrans()
    {
        return [
            'saf_number' => trans('swis.payment-and-obligations.saf_number'),
            'sub_application_number' => trans('swis.payment-and-obligations.sub_application_number'),
            'applicant_tin' => trans('swis.payment-and-obligations.applicant_tin'),
            'applicant' => trans('swis.payment-and-obligations.applicant'),
            'agency' => trans('swis.payment-and-obligations.agency'),
            'tin_importer_exporter' => trans('swis.payment-and-obligations.tin_importer_exporter'),
            'importer_exporter' => trans('swis.payment-and-obligations.importer_exporter'),
            'obligation_type' => trans('swis.payment-and-obligations.obligation_type'),
            'budget_line' => trans('swis.payment-and-obligations.budget_line'),
            'document' => trans('swis.payment-and-obligations.document'),
            'pay' => trans('swis.payment-and-obligations.pay'),
            'obligation_pay_date_start' => trans('swis.payment-and-obligations.obligation_pay_date_start'),
            'obligation_pay_date_end' => trans('swis.payment-and-obligations.obligation_pay_date_end'),
            'obligation_created_date_start' => trans('swis.payment-and-obligations.obligation_created_date_start'),
            'obligation_created_date_end' => trans('swis.payment-and-obligations.obligation_created_date_end'),
            'balance_start' => trans('swis.payment-and-obligations.balance_start'),
            'balance_end' => trans('swis.payment-and-obligations.balance_end')
        ];
    }

    /**
     * Function to get data for exporting report
     *
     * @param $data
     * @param $params
     * @return $this
     */
    public function setData($data, $params)
    {
        $this->data = $data;
        $this->searchParams = $params;

        return $this;
    }
}