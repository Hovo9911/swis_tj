<?php

namespace App\Exports\UserDocuments;

use App\Models\ConstructorStates\ConstructorStates;
use App\Models\DataModel\DataModel;
use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm;
use App\Models\DocumentsDatasetFromSaf\DocumentsDatasetFromSaf;
use App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure;
use App\Models\User\User;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Events\AfterSheet;

/**
 * Class UserDocumentsXlsExport
 * @package App\Exports\UserDocuments
 */
class UserDocumentsXlsExport implements WithEvents, ShouldAutoSize, WithMultipleSheets
{
    use Exportable, RegistersEventListeners;

    private $applications;
    private $products;
    private $searchParams;

    /**
     * @return array
     */
    public function sheets(): array
    {
        return [
            'applications' => new UserDocumentsXlsExportApplicationSheet($this->applications, $this->searchParams),
            'products' => new UserDocumentsXlsExportProductsSheet($this->products)
        ];
    }

    /**
     * @param AfterSheet $event
     */
    public static function afterSheet(AfterSheet $event)
    {
        $event->sheet->mergeCells('A1:O1');
        $event->sheet->mergeCells('A2:O2');
        $event->sheet->mergeCells('A3:O3');
        $event->sheet->getStyle('A1')->getAlignment()->setWrapText(true);
    }

    /**
     * Function to get data for exporting report
     *
     * @param $data
     * @param $params
     * @return void
     */
    public function setData($data, $params)
    {
        $this->applications = $data['applications'];
        $this->products = $data['products'];
        $this->searchParams = $params;
    }

    /**
     * Function to get xms search params label value ml
     *
     * @param $data
     * @param array $returnArray
     * @return array
     */
    public function getXlsFormSearchParams($data, $returnArray = [])
    {
        unset($data['search_by_superscribe_field']);
        foreach ($data as $field => $value) {

            if ($field == 'state_id') {
                $state = ConstructorStates::select('id')->with(['currentMl'])->where('id', $value)->first();
                $returnArray[] = [
                    'label' => trans('swis.constructor_document.state.label'),
                    'value' => optional($state->currentMl)->state_name,
                ];
            } elseif ($field == 'superscribe_id') {
                $user = User::select('id', 'first_name', 'last_name')->where('id', $value)->first();
                $returnArray[] = [
                    'label' => trans('swis.constructor_document.superscription.label'),
                    'value' => $user->name() ?? '',
                ];
            } elseif ($field == 'unsuperscribe_sub_applications') {
                $returnArray[] = [
                    'label' => trans('swis.constructor_document.unsuperscribe_sub_applications.label'),
                    'value' => trans('swis.user_documents.true.label'),
                ];
            } elseif (strpos($field, '_start_date_filter') !== false) {
                $returnArray[] = $this->getFieldXlsReport(str_replace('_start_date_filter', '', $field), $value);
            } elseif (strpos($field, '_end_date_filter') !== false) {
                $returnArray[] = $this->getFieldXlsReport(str_replace('_end_date_filter', '', $field), $value);
            } else {
                $returnArray[] = $this->getFieldXlsReport($field, $value);
            }
        }

        return $returnArray;
    }

    /**
     * Function to get field
     *
     * @param $field
     * @param $value
     * @param array $returnData
     * @return array
     */
    private function getFieldXlsReport($field, $value, $returnData = [])
    {
        $xml = SingleApplicationDataStructure::lastXml();

        $dataModelAllData = DataModel::allData();
        if (strpos($field, 'SAF_ID_') === false) {
            $dataSet = DocumentsDatasetFromMdm::select('id', 'mdm_field_id', 'label')->where('field_id', $field)->joinMl()->with('mdm')->first();

            if (!is_null($dataSet)) {
                $returnData['label'] = $dataSet->label;
                if (optional($dataSet)->mdm && $dataSet->mdm->reference) {
                    $refData = getReferenceRows($dataSet->mdm->reference, $value, false, ['id', 'code', 'name']);
                    $returnData['value'] = $refData->name ?? '';
                } else {
                    $returnData['value'] = $value;
                }
            }
        } else {
            $field = getSafFieldById($xml, $field)[0];
            $dataSet = DocumentsDatasetFromSaf::select('id', 'label')->where('saf_field_id', $field)->join('documents_dataset_from_saf_ml', 'documents_dataset_from_saf_ml.documents_dataset_from_saf_Id', '=', 'documents_dataset_from_saf.id')->first();
            $returnData['label'] = $dataSet->label ?? trans((string)$field->attributes()->title);

//            $mdm = DataModel::select('reference')->where(DB::raw('id::varchar'), (string)$field->attributes()->mdm)->first();
            if (isset($dataModelAllData[(string)$field->attributes()->mdm]) && $dataModelAllData[(string)$field->attributes()->mdm]['reference']) {
                $refData = getReferenceRows($dataModelAllData[(string)$field->attributes()->mdm]['reference'], $value, false, ['id', 'code', 'name']);
                $returnData['value'] = $refData->name ?? '';
            } else {
                $returnData['value'] = $value;
            }
        }

        return $returnData;
    }
}