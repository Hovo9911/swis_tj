<?php

namespace App\Exports\UserDocuments;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;

/**
 * Class UserDocumentsXlsExportProductsSheet
 * @package App\Exports\UserDocuments
 */
class UserDocumentsXlsExportProductsSheet implements FromView, WithEvents, ShouldAutoSize, WithTitle
{
    use Exportable, RegistersEventListeners;

    private $products;

    /**
     * UserDocumentsXlsExportApplicationSheet constructor.
     *
     * @param $products
     */
    public function __construct($products)
    {
        $this->products = $products;
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->mergeCells('P1:Q1');
                $event->sheet->mergeCells('R1:S1');
                $event->sheet->mergeCells('T1:U1');
                $event->sheet->getStyle('A1')->getAlignment()->setWrapText(true);
                $event->sheet->getStyle('A1:V1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
            },
        ];
    }

    /**
     * @return View
     */
    public function view(): View
    {
        return view('exports.user-documents.user-documents-products-sheet-report', [
            'products' => $this->products
        ]);
    }

    /**
     * @inheritDoc
     */
    public function title(): string
    {
        return "products";
    }
}