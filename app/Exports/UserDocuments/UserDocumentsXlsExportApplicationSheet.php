<?php

namespace App\Exports\UserDocuments;

use Closure;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;

/**
 * Class UserDocumentsXlsExportApplicationSheet
 * @package App\Exports\UserDocuments
 */
class UserDocumentsXlsExportApplicationSheet implements FromView, WithEvents, ShouldAutoSize, WithTitle
{
    use Exportable, RegistersEventListeners;

    private $applications;
    private $searchParams;

    /**
     * UserDocumentsXlsExportApplicationSheet constructor.
     *
     * @param $applications
     * @param $params
     */
    public function __construct($applications, $params)
    {
        $this->applications = $applications;
        $this->searchParams = $params;
    }

    /**
     * @return array|Closure[]
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:V4')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
            },
        ];
    }

    /**
     * @return View
     */
    public function view(): View
    {
        return view('exports.user-documents.user-documents-applications-sheet-report', [
            'searchParams' => $this->searchParams,
            'applications' => $this->applications
        ]);
    }

    /**
     * Function to after sheet
     *
     * @param AfterSheet $event
     */
    public static function afterSheet(AfterSheet $event)
    {
        $event->sheet->mergeCells('A1:O1');
        $event->sheet->mergeCells('A2:O2');
        $event->sheet->mergeCells('A3:O3');
        $event->sheet->getStyle('A1')->getAlignment()->setWrapText(true);
    }

    /**
     * @inheritDoc
     */
    public function title(): string
    {
        return "applications";
    }
}