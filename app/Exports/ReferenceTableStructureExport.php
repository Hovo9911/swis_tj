<?php

namespace App\Exports;

use App\Models\ReferenceTable\ReferenceTableStructure;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

/**
 * Class ReferenceTableStructureExport
 * @package App\Exports
 */
class ReferenceTableStructureExport implements FromCollection, WithHeadings
{
    use Exportable;

    private $referenceTable;
    private $exportData;

    /**
     * Function to Generate excel
     *
     * @return Collection
     */
    public function collection()
    {
        return collect($this->exportData);
    }

    /**
     * Function to set header in excel docs
     *
     * @return array
     */
    public function headings(): array
    {
        return [trans('swis.reference_table.field_name'),
            trans('swis.reference_table.type.label'),
            trans('swis.reference_table.parameter.label'),
            trans('swis.reference_table.required.label'),
            trans('swis.reference_table.search_filter.label'),
            trans('swis.reference_table.search_result.label'),
            trans('swis.reference_table.sortable.label'),
            trans('swis.reference_table.sort_order.label'),
            trans('swis.reference_table.ml.label'),
            trans('core.base.label.status')];
    }

    /**
     * Function to get data from reference_table_structure
     *
     * @param $referenceTable
     * @return $this
     */
    public function getData($referenceTable)
    {
        $this->referenceTable = $referenceTable;

        $exportData = ReferenceTableStructure::select('field_name', 'type', 'parameters', 'required', 'search_filter', 'search_result', 'sortable', 'sort_order', 'has_ml', 'show_status')
            ->where('reference_table_id', $referenceTable->id)
            ->get();

        foreach ($exportData as $data) {
            $data->required = ($data->required == 1) ? trans('swis.reference_table.checkbox.yes') : trans('swis.reference_table.checkbox.no');
            $data->search_filter = ($data->search_filter == 1) ? trans('swis.reference_table.checkbox.yes') : trans('swis.reference_table.checkbox.no');
            $data->search_result = ($data->search_result == 1) ? trans('swis.reference_table.checkbox.yes') : trans('swis.reference_table.checkbox.no');
            $data->sortable = ($data->sortable == 1) ? trans('swis.reference_table.checkbox.yes') : trans('swis.reference_table.checkbox.no');
            $data->has_ml = ($data->has_ml == 1) ? trans('swis.reference_table.checkbox.yes') : trans('swis.reference_table.checkbox.no');
            $data->show_status = ($data->show_status == 1) ? trans('core.base.label.status.active') : trans('core.base.label.status.inactive');
        }

        $this->exportData = $exportData;

        return $this;
    }
}
