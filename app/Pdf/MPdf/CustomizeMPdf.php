<?php

namespace App\Pdf\MPdf;

use Mpdf\Mpdf;

/**
 * Class CustomizeMPdf
 * @package App\Pdf\MPdf
 */
class CustomizeMPdf extends Mpdf
{
    /**
     * CustomizeMPdf constructor.
     * @param array $config
     * @throws \Mpdf\MpdfException
     */
    public function __construct(array $config = [])
    {
        $this->createTempDir(env('MPDF_TMP_PATH'));

        if (!empty($config)) {
            $configAll = array_merge(
                ['tempDir' => env('MPDF_TMP_PATH')],
                $config
            );
        } else {
            $configAll = ['tempDir' => env('MPDF_TMP_PATH')];
        }
        parent::__construct($configAll);
    }

    /**
     * Function to create temp dir for mPdf
     *
     * @param string $tempDir
     * @return bool
     */
    private function createTempDir(string $tempDir): bool
    {
        if (!file_exists($tempDir)) {
            mkdir($tempDir, 0775, true);
        }

        return true;
    }

}