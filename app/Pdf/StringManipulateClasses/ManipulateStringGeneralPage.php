<?php
namespace App\Pdf\StringManipulateClasses;


class ManipulateStringGeneralPage
{
    private $type = '';
    private $maxSymbolCount = 0;
    private $symbolCount = 0;
    private $addOtherPage = false;
    private $currentIndexOtherPage = 0;
    private $otherPageData = [];
    private $text = '';

    /**
     * ManipulateStringGeneralPage constructor.
     * @param string $type
     * @param int $maxSymbolCount
     */
    public function __construct(string $type, int $maxSymbolCount)
    {
        $this->setType($type);
        $this->setMaxSymbolCount($maxSymbolCount);
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    private function setType(string $type)
    {

        if(empty($type)) {
            throw new Exception('$type should not be empty');
        }

        $this->type = $type;
    }

    /**
     * @return array
     */
    public function getOtherPageData(): array
    {
        return $this->otherPageData;
    }

    /**
     * @param string $otherPageData
     */
    private function setOtherPageData(string $text, bool $hasKey)
    {
        $this->otherPageData[$this->currentIndexOtherPage]['text'] = $text;
        $this->otherPageData[$this->currentIndexOtherPage]['hasKey'] = $hasKey;
        $this->otherPageData[$this->currentIndexOtherPage]['type'] = $this->getType();
        $this->currentIndexOtherPage++;
    }

    /**
     * @return int
     */
    public function getMaxSymbolCount(): int
    {
        return $this->maxSymbolCount;
    }

    /**
     * @param int $maxSymbolCount
     */
    private function setMaxSymbolCount(int $maxSymbolCount)
    {
        if (empty($maxSymbolCount)) {
            throw new Exception('$maxSymbolCount should not be empty');
        }

        $this->maxSymbolCount = $maxSymbolCount;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    private function setText(string $text)
    {
        $this->text .= $text;
    }

    /**
     * @param string $text
     * @param bool $hasKey
     */
    public function addText (string $text, bool $hasKey) {
        $this->manipulateLogicGeneralPage($text, $hasKey);
    }

    /**
     * @param string $text
     * @param bool $hasKey
     */
    private function manipulateLogicGeneralPage(string $text, bool $hasKey) {
        $maxSymbolCount = $this->getMaxSymbolCount();
        $symbolCount = $this->symbolCount;
        if (!empty($text)) {
            if (!$this->addOtherPage) {
                $allowedSymbolCount = $maxSymbolCount - $symbolCount;
                $currentSymbolCount = mb_strlen($text, 'UTF-8');
                if ($allowedSymbolCount > $currentSymbolCount) {
                    $this->setText($text);
                } else {

                    if ($hasKey) {
                        $this->setOtherPageData($text, $hasKey);
                    } else {
                        $this->setText(mb_substr($text, 0, $allowedSymbolCount, 'UTF-8'));
                        $this->setOtherPageData(mb_substr($text, $allowedSymbolCount, null, 'UTF-8'), $hasKey);
                    }
                    $this->addOtherPage = true;
                }
            } else {
                $this->setOtherPageData($text, $hasKey);
            }
        }
    }

}