<?php
namespace App\Pdf\StringManipulateClasses;


class ManipulateStringOtherPages
{
    private $otherPagesText = [];
    private $otherPagesTextTitle = [];
    private $maxSymbolCountCurrentPage = 0;

    private $currentOtherPage = 0;
    private $returnOtherPagesText = [];

    /**
     * ManipulateStringOtherPages constructor.
     * @param array $otherPagesText
     * @param array $otherPagesTextTitle
     * @param int $maxSymbolCountCurrentPage
     */
    public function __construct(array $otherPagesText, array $otherPagesTextTitle, int $maxSymbolCountCurrentPage)
    {
        $this->otherPagesText = $otherPagesText;
        $this->otherPagesTextTitle = $otherPagesTextTitle;
        $this->maxSymbolCountCurrentPage = $maxSymbolCountCurrentPage;
    }

    /**
     * @return array
     */
    public function getOtherPagesText(): array
    {
        return $this->otherPagesText;
    }

    /**
     * @return array
     */
    public function getOtherPagesTextTitle(): array
    {
        return $this->otherPagesTextTitle;
    }

    /**
     * @param $text
     */
    private function setCurrentOtherPage($text)
    {
        if (isset($this->returnOtherPagesText[$this->currentOtherPage])) {
            $this->returnOtherPagesText[$this->currentOtherPage] .= $text;
        } else {
            $this->returnOtherPagesText[$this->currentOtherPage] = $text;
        }
    }

    /**
     * @param $text
     */
    private function setNextOtherPage($text)
    {
        $currentOtherPage = $this->currentOtherPage + 1;
        if (isset($this->returnOtherPagesText[$currentOtherPage])) {
            $this->returnOtherPagesText[$currentOtherPage] .= $text;
        } else {
            $this->returnOtherPagesText[$currentOtherPage] = $text;
        }
    }

    /**
     * create other pages
     */
    private function manipulateLogicOtherPages()
    {
        $arrayOtherPages = $this->getOtherPagesText();
        $otherPagesTextKeyTitle = $this->getOtherPagesTextTitle();
        $symbolCount = 0;
        $addKeyTitle = true;
        $maxSymbolCount = $this->maxSymbolCountCurrentPage;

        if (!empty($arrayOtherPages)) {
            foreach ($arrayOtherPages as $key => $value) {
                if (!empty($value['text'])) {

                    if ($key == 0 || $value['type'] != $arrayOtherPages[$key - 1]['type']) {
                        $this->setCurrentOtherPage($otherPagesTextKeyTitle[$value['type']] . " <br>");
                    }

                    $allowedSymbolCount = $maxSymbolCount - $symbolCount;

                    $currentSymbolCount = mb_strlen($value['text'], 'UTF-8');
                    $symbolCount += $currentSymbolCount;

                    if ($symbolCount > $maxSymbolCount) {
                        if ($value['hasKey']) {
                            $this->setNextOtherPage($otherPagesTextKeyTitle[$value['type']] . " <br>");
                            $this->setNextOtherPage($value['text']);
                            $symbolCount = mb_strlen($value['text'], 'UTF-8');
                        } else {
                            $this->setCurrentOtherPage(mb_substr($value['text'], 0, $allowedSymbolCount, 'UTF-8'));
                            $this->setNextOtherPage($otherPagesTextKeyTitle[$value['type']] . " <br>");
                            $textNextPage = mb_substr($value['text'], $allowedSymbolCount, null, 'UTF-8');
                            $this->setNextOtherPage($textNextPage);
                            $symbolCount = mb_strlen($textNextPage, 'UTF-8');
                        }
                        $this->currentOtherPage++;
                    } else {
                        $this->setCurrentOtherPage($value['text']);
                    }

                }
            }
        } else {
            $this->returnOtherPagesText = '';
        }
    }

    /**
     * @return array
     */
    public function selectOtherPagesText()
    {
        $this->manipulateLogicOtherPages();
        return $this->returnOtherPagesText;
    }

}