<?php

namespace App\Pdf;

use App\Models\Agency\Agency;
use App\Models\ConstructorDocument\ConstructorDocument;
use App\Models\ConstructorStates\ConstructorStates;
use App\Models\ConstructorTemplates\ConstructorTemplates;
use App\Models\DataModel\DataModel;
use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm;
use App\Models\SingleApplication\SingleApplication;
use App\Models\SingleApplicationNotes\SingleApplicationNotes;
use App\Models\SingleApplicationObligations\SingleApplicationObligations;
use App\Models\SingleApplicationProducts\SingleApplicationProducts;
use App\Models\SingleApplicationSubApplicationProducts\SingleApplicationSubApplicationProducts;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use App\Pdf\MPdf\CustomizeMPdf;
use App\Pdf\TcPdf\CustomizeTcPdf;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

/**
 * Class PdfManager
 * @package App\Pdf
 */
class PdfManager
{
    /**
     * Function to generate pdf
     *
     * @param $documentId
     * @param $applicationId
     * @param $template
     * @param null $products
     * @param null $pdfHashKey
     *
     * @throws \Throwable
     */
    public function generatePdf($documentId, $applicationId, $template, $products = null, $pdfHashKey = null)
    {
        $this->checkQRCodeAuthorization($template);
        $this->pdfTypeSwitcher($documentId, $applicationId, $template, $products, $pdfHashKey);
    }

    /**
     * Function to check pdf template type  tcPdf || mPdf
     *
     * @param int $documentId
     * @param int $applicationId
     * @param string $template
     * @param string $products
     *
     * @throws \Throwable
     */
    public function pdfTypeSwitcher($documentId, $applicationId, $template = null, $products = null, $pdfHashKey = null)
    {
        if ($template != ConstructorTemplates::DEFAULT_PDF && !is_null($template)) {
            $constructorTemplate = ConstructorTemplates::select('pdf_type')->where('code', $template)->firstOrFail();

            if ($constructorTemplate->pdf_type == ConstructorTemplates::PDF_TYPE_TCPDF) {
                $this->generateTcPdfTemplate($documentId, $applicationId, $template, $pdfHashKey, $products);
            } else {
                $this->generateMPdfTemplate($documentId, $applicationId, $template, $pdfHashKey, $products);
            }
        } else {
            // Default pdf
            $this->generateDefaultTcPdf($applicationId, $pdfHashKey, $documentId, $products);
        }
    }

    /**
     * Function to generate TcPdf file
     *
     * @param $subApplicationId
     * @param $pdfHashKey
     * @param null $documentId
     * @param null $productsIds
     * @param null $fileDir
     * @param null $fileName
     *
     * @throws \Throwable
     */
    private function generateDefaultTcPdf($subApplicationId, $pdfHashKey, $documentId = null, $productsIds = null, $fileDir = null, $fileName = null)
    {
        $subApplication = SingleApplicationSubApplications::where(['id' => $subApplicationId, 'pdf_hash_key' => $pdfHashKey])->firstOrFail();
        $saf = SingleApplication::where('regular_number', $subApplication->saf_number)->firstOrFail();
        $agency = Agency::where('id', $subApplication->agency_id)->firstOrFail();

        // Obligation part
        $obligations = (new SingleApplicationObligations())->getObligationsForPDF($saf, $subApplication);

        // Notes part
        if (!is_null($documentId)) {
            $notes = SingleApplicationNotes::getApplicationNotes($subApplicationId)->toArray();

        } else {
            $safNotes = SingleApplicationNotes::getSafNotes($subApplication->saf_number);
            $subApplicationNotes = SingleApplicationNotes::getSubApplicationNotesForPdf($subApplicationId);
            $notes = $subApplicationNotes->merge($safNotes)->sortByDesc('id');
        }

        $subAppNum = $subApplication->document_number ?? '';
        $products = !is_null($productsIds) ? explode(",", $productsIds) : null;

        //
        $subApplicationCurrentStateName = '';
        if (isset($subApplication->applicationCurrentState, $subApplication->applicationCurrentState->state) && !is_null($subApplication->applicationCurrentState->state->current())) {
            $subApplicationCurrentStateName = $subApplication->applicationCurrentState->state->current()->state_name;
        }

        //
        $constructorDocument = ConstructorDocument::where('document_code', optional($subApplication)->document_code)->active()->first();
        $documentName = !is_null($constructorDocument) ? optional($constructorDocument->current())->document_name : '';

        $pdfTitle = trans('swis.user_documents.saf_pdf_prefix') . '_' . $documentName . '_' . $subAppNum . '_' . optional($subApplication)->status_date;
        $pdf = new customizeTcPdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $this->tcPdfOptions($pdf, $pdfTitle, $template = null);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage('L');

        $isEndStateApproved = false;
        if (!empty($subApplication->applicationCurrentState->state)) {
            $isEndStateApproved = ConstructorStates::checkEndStateStatus($subApplication->applicationCurrentState->state->id, ConstructorStates::END_STATE_APPROVED);
        }

        if ($isEndStateApproved) {
            $this->generateQrCode($pdf, $documentId, $pdfHashKey, $subApplicationId, $productsIds, ConstructorTemplates::DEFAULT_PDF);
        }

        // Set some content to print
        $html = view('pdf-templates/saf')->with([
            'saf' => $saf,
            'subApplication' => $subApplication,
            'notes' => $notes,
            'obligations' => $obligations,
            'userFullname' => Auth::user() ? Auth::user()->name() : '',
            'applicationId' => $subApplicationId,
            'documentName' => $documentName,
            'subApplicationNumber' => $subAppNum,
            'subApplicationSubmittingDate' => formattedDate(optional($subApplication)->status_date) ?? '',
            'customData' => optional($subApplication)->custom_data,
            'products' => $products,
            'agency' => $agency])
            ->render();

        // Print text using writeHTMLCell()
        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

        $pdf->AddPage('L');

        $htmlNotes = view('pdf-templates/notes')->with(['notes' => $notes, 'saf' => $saf, 'subApplicationCurrentStateName' => $subApplicationCurrentStateName])->render();

        // Print text using writeHTMLCell()
        $pdf->writeHTMLCell(0, 0, '', '', $htmlNotes, 0, 1, 0, true, '', true);

        $this->tcPdfOutput($pdf, $fileDir, $fileName, $pdfTitle);
    }

    /**
     * Function to generate TcPdf file
     *
     * @param int $documentId
     * @param int $applicationId
     * @param string $template
     * @param string $pdfHashKey
     * @param string|null $products
     * @param array $options
     *
     * @throws \Throwable
     */
    private function generateTcPdfTemplate(int $documentId, int $applicationId, string $template, string $pdfHashKey, string $products = null, array $options = [])
    {
        $pdf = new CustomizeTcPdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $data = $this->getPdfData($documentId, $applicationId, $pdfHashKey, $template, $products);
        $this->tcPdfOptions($pdf, $data['pdfTitle'], $template);
        $this->tcPdfCustomOptions($pdf, $template);

        $templatesWithLandscapePage = [
            ConstructorTemplates::SEC_TEMPLATE_CODE
        ];

        in_array($template, $templatesWithLandscapePage) ? $pdf->AddPage('L', 'A4') : $pdf->AddPage();

        $templatesWithQRCode = $this->templatesWithQRCode($template);

        // Set some content to print
        $templatesWithoutProductsPart = $this->templatesWithoutProductsPart($template);

        $pdfData = [
            'agency' => $data['agency'],
            'subApplication' => $data['subApplication'],
            'applicationId' => $data['applicationId'],
            'documentId' => $data['documentId'],
            'saf' => $data['saf'],
            'subAppProducts' => $data['subAppProducts'],
            'customData' => $data['customData'],
            'productModalIds' => $data['productModalIds'],
            'template' => $data['template'],
            'availableProductsIds' => $data['availableProductsIds'],
            'pdf' => $pdf
        ];

        $html = view("pdf-templates/{$template}/index")->with($pdfData)->render();

        // Print text using writeHTMLCell()
        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

        if ($templatesWithQRCode) {
            $this->generateQrCode($pdf, $documentId, $pdfHashKey, $applicationId, $products, $template);
        }

        // Add a page
        // This method has several options, check the source code documentation for more information.

        if (!is_null($template) && count($data['subAppProducts']) > 1 && !$templatesWithoutProductsPart) {

            $this->generateProductsPage($template, $pdf, $pdfData);
        }

        $downloadablePdfName = $this->downloadablePdfName($template, $data['subApplication']);

        // Output tcPdf
        $this->tcPdfOutput($pdf, null, null, $downloadablePdfName);

    }

    /**
     * Function to generate mPdf file
     *
     * @param int $documentId
     * @param int $applicationId
     * @param string $template
     * @param string $pdfHashKey
     * @param string|null $products
     * @param array $options
     *
     * @throws \Mpdf\MpdfException
     * @throws \Throwable
     */
    private function generateMPdfTemplate(int $documentId, int $applicationId, string $template, string $pdfHashKey, string $products = null, array $options = [])
    {
        $data = $this->getPdfData($documentId, $applicationId, $pdfHashKey, $template, $products);
        $options = $this->getMpdfOptions($template);
        $pdf = new CustomizeMPdf($options);

        // set document information
        $pdf->SetCreator('MPDF');
        $pdf->SetAuthor('');
        $pdf->SetTitle($data['pdfTitle']);
        $pdf->SetSubject('Certificate');
        $pdf->SetKeywords('Certificate');


        $pdfData = [
            'agency' => $data['agency'],
            'subApplication' => $data['subApplication'],
            'applicationId' => $data['applicationId'],
            'documentId' => $data['documentId'],
            'saf' => $data['saf'],
            'subAppProducts' => $data['subAppProducts'],
            'customData' => $data['customData'],
            'productModalIds' => $data['productModalIds'],
            'template' => $data['template'],
            'availableProductsIds' => $data['availableProductsIds'],
            'pdf' => $pdf
        ];

        $html = view("pdf-templates/{$template}/index")->with($pdfData)->render();

        // write html
        $pdf->WriteHTML($html);

        $downloadablePdfName = $this->downloadablePdfName($template, $data['subApplication']);

        $pdf->Output($downloadablePdfName . '.pdf', 'I');
    }

    /**
     * Function to generate TcPdf for obligation
     *
     * @param $obligation
     * @param $pdfHashKey
     * @throws \Throwable
     */
    public function generateObligationPdf($obligation, $pdfHashKey)
    {
        $pdf = new CustomizeTcPdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $style = array(
            'border' => false,
            'vpadding' => 'auto',
            'hpadding' => 'auto',
            'fgcolor' => array(0, 0, 0),
            'bgcolor' => false, //array(255,255,255)
            'module_width' => 1, // width of a single module in points
            'module_height' => 1 // height of a single module in points
        );

        $pdfTitle = trans('swis.user_documents.saf_pdf_prefix') . '_' . trans('swis.print_form.obligation_title_info') . '_' . optional($obligation)->sub_application_number . '_' . $obligation->id;
        $this->tcPdfOptions($pdf, $pdfTitle, null);

        $pdf->AddPage('L');
        $pdf->SetAutoPageBreak(true, 0);
        $pdf->write2DBarcode(urlWithLng('payment-and-obligations/generatePdf/' . $obligation->id . '/' . $pdfHashKey), 'QRCODE,R', 255, 0, 35, 35, $style, '');

        // Set some content to print
        $html = view("pdf-templates/OBLIGATIONS/index")->with(['obligation' => $obligation])->render();

        // Print text using writeHTMLCell()
        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

        $this->tcPdfOutput($pdf, null, null, $pdfTitle);

    }

    /**
     * Function to generate PDF title
     *
     * @param $template
     * @param $customData
     * @param $subApplication
     * @param $documentId
     * @return string
     */
    private function generatePDFTitle($template, $customData, $subApplication, $documentId)
    {
        $templateName = $this->getPdfTemplateName($template);

        switch ($template) {
            case ConstructorTemplates::TAJIKSTANDART_TEMPLATE_CODE:
                $pdfTitle = trans('swis.user_documents.saf_pdf_prefix') . '_' . $templateName . '_' . $subApplication->document_number . '_' . (isset($customData['1645_5']) ? formattedDate($customData['1645_5']) : '');
                break;
            case ConstructorTemplates::TJMOH_TEMPLATE_CODE:
                $pdfTitle = trans('swis.user_documents.saf_pdf_prefix') . '_' . $templateName . '_' . $subApplication->document_number . '_' . (isset($customData['PHS_001_5']) ? formattedDate($customData['PHS_001_5']) : '');
                break;
            case ConstructorTemplates::TJCS_TEMPLATE_CODE:
                $pdfTitle = trans('swis.user_documents.saf_pdf_prefix') . '_' . $templateName . '_' . $subApplication->document_number . '_' . (isset($customData['CSCC_001_7']) ? formattedDate($customData['CSCC_001_7']) : '');
                break;
            case ConstructorTemplates::FSC_VET_PHARM_TEMPLATE_CODE:
                $pdfTitle = trans('swis.user_documents.saf_pdf_prefix') . '_' . $templateName . '_' . $subApplication->document_number . '_' . (isset($customData['CCVP_5']) ? formattedDate($customData['CCVP_5']) : '');
                break;
            case ConstructorTemplates::FSCVET5_TEMPLATE_CODE:
                $pdfTitle = trans('swis.user_documents.saf_pdf_prefix') . '_' . $templateName . '_' . $subApplication->document_number . '_' . (isset($customData['Veterinary_7']) ? formattedDate($customData['Veterinary_7']) : '');
                break;
            case ConstructorTemplates::SASS_TEMPLATE_CODE:
                $pdfTitle = trans('swis.user_documents.saf_pdf_prefix') . '_' . $templateName . '_' . $subApplication->document_number . '_' . (isset($customData['SAS_001_5']) ? formattedDate($customData['SAS_001_5']) : '');
                break;
            case ConstructorTemplates::SASS_EXAMINATION:
            case ConstructorTemplates::SASS_EXAMINATION_TYPE_ITEM:
                $pdfTitle = trans('swis.user_documents.saf_pdf_prefix') . '_' . $templateName . '_' . $subApplication->document_number . '_' . ($customData['SAS_001_53'] ?? '');
                break;
            case ConstructorTemplates::TJ_SWT_872:
                $pdfTitle = trans('swis.user_documents.saf_pdf_prefix') . '_' . $templateName . '_' . ($customData['1122_4'] ?? '') . '_' . ($customData['1122_5'] ?? '');
                break;
            case ConstructorTemplates::TJ_LICENSE_NRSL_SWT_902:
                $pdfTitle = trans('swis.user_documents.saf_pdf_prefix') . '_' . $templateName . '_' . ($customData['NaRSL_4'] ?? '') . '_' . ($customData['NaRSL_5'] ?? '');
                break;
            case ConstructorTemplates::TJ_SWT_756_CCI_V1:
            case ConstructorTemplates::TJ_SWT_756_CCI_FORM_A:
            case ConstructorTemplates::TJ_SWT_756_CCI_FORM_A_WITHOUT_REG_NUMBER:
            case ConstructorTemplates::TJ_SWT_756_CCI_V2:
                $pdfTitle = trans('swis.user_documents.saf_pdf_prefix') . '_' . $templateName . '_' . $subApplication->document_number . '_' . ($customData['CCI_001_5'] ?? '');
                break;
            case ConstructorTemplates::MOC_TEMPLATE_CODE:
            case ConstructorTemplates::MOC_TJ_TEMPLATE_CODE:
                $pdfTitle = trans('swis.user_documents.saf_pdf_prefix') . '_' . $templateName . '_' . ($customData['MC_001_4'] ?? '') . '_' . ($customData['MC_001_6'] ?? '');
                break;
            case ConstructorTemplates::KOOC:
            case ConstructorTemplates::FSC_ACT_1_TEMPLATE_CODE:

                $mdmValues = DocumentsDatasetFromMdm::select('mdm_field_id', 'field_id')->where('document_id', $documentId)
                    ->whereIn('mdm_field_id', [DataModel::getMdmFieldIDByConstant(DataModel::PERMISSION_DATE_MDM_ID),
                        DataModel::getMdmFieldIDByConstant(DataModel::PERMISSION_NUMBER_MDM_ID)])->get();
                foreach ($mdmValues as $mdmValue) {
                    switch ($mdmValue['mdm_field_id']) {
                        case DataModel::getMdmFieldIDByConstant(DataModel::PERMISSION_DATE_MDM_ID):
                            $approvedDate = $mdmValue['field_id'] ?? '';
                            break;
                        case DataModel::getMdmFieldIDByConstant(DataModel::PERMISSION_NUMBER_MDM_ID):
                            $approveNumber = $mdmValue['field_id'] ?? '';
                            break;
                    }
                }
                $approvedDate = isset($approvedDate, $customData[$approvedDate]) ? $customData[$approvedDate] : '';
                $approveNumber = isset($approveNumber, $customData[$approveNumber]) ? $customData[$approveNumber] : '';
                $pdfTitle = trans('swis.user_documents.saf_pdf_prefix') . '_' . $templateName . '_' . $approveNumber . '_' . $approvedDate;
                break;
            case ConstructorTemplates::TJMOH_PERMITION_RU:
            case ConstructorTemplates::TJMOH_PERMITION_EN:
            case ConstructorTemplates::TJMOH_RESOLUTION:
            case ConstructorTemplates::TJMOH_RESOLUTION_1:
                $pdfTitle = trans('swis.user_documents.saf_pdf_prefix') . '_' . $templateName . '_' . ($customData['6141_4'] ?? '') . '_' . ($customData['6141_5'] ?? '');
                break;
            default:
                $pdfTitle = '';
                break;
        }

        return $pdfTitle;
    }

    /**
     * Function to get data for pdf
     *
     * @param int $documentId
     * @param int $applicationId
     * @param string $pdfHashKey
     * @param string $template
     * @param string $products
     * @param array $options
     *
     * @return array
     */
    private function getPdfData(int $documentId, int $applicationId, string $pdfHashKey, string $template, string $products = null, array $options = []): array
    {
        //Check if document_id not exist return page 404
        ConstructorDocument::where('id', $documentId)->firstOrFail();

        $subApplication = SingleApplicationSubApplications::where(['id' => $applicationId, 'pdf_hash_key' => $pdfHashKey])->firstOrFail();
        $saf = SingleApplication::where('regular_number', $subApplication->saf_number)->firstOrFail();
        $agency = Agency::where('id', $subApplication->agency_id)->joinMl()->firstOrFail();
        $subAppProductsIds = SingleApplicationSubApplicationProducts::where(['subapplication_id' => $applicationId])->pluck('product_id')->all();

        $productsInfo = $productModalIds = $productsNumbers = [];

        if (!is_null($products)) {

            $products = explode(",", $products);
            foreach ($products as $key => $product) {
                $productsInfo[] = explode('-', $product);
            }

            if (count($productsInfo) > 0) {
                foreach ($productsInfo as $productInfo) {
                    $productsNumbers[] = $productInfo[0];
                    $productModalIds[] = $productInfo[1];
                }
            }
            $subAppProductsIds = array_intersect($subAppProductsIds, $productsNumbers);
        }

        $pdfTitle = '';
        $customData = [];
        if (!is_null($documentId)) {

            $subApplicationCustomData = SingleApplicationSubApplications::select('data', 'custom_data')->find($applicationId);

            $customData = optional($subApplicationCustomData)->custom_data;

            if (!is_null($template)) {
                $pdfTitle = $this->generatePDFTitle($template, $customData, $subApplication, $documentId);
            }
        }

        //
        $subAppProducts = SingleApplicationProducts::whereIn('id', $subAppProductsIds)->get();

        return [
            'agency' => $agency,
            'subApplication' => $subApplication,
            'applicationId' => $applicationId,
            'documentId' => $documentId,
            'saf' => $saf,
            'subAppProducts' => $subAppProducts,
            'customData' => $customData,
            'productModalIds' => $productModalIds,
            'template' => $template,
            'availableProductsIds' => $subAppProductsIds,
            'pdfTitle' => $pdfTitle
        ];
    }

    /**
     * Function to get tcPdfOptions
     *
     * @param $pdf
     * @param null $pdfTitle
     * @param null $template
     */
    public function tcPdfOptions($pdf, $pdfTitle = null, $template = null)
    {
        $userFullName = Auth::user() ? Auth::user()->name() : '';

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor($userFullName);
        $pdf->SetTitle($pdfTitle ?? '');
        $pdf->SetSubject('Certificate');
        $pdf->SetKeywords('Certificate');

        // set default header data
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
        //$pdf->setFooterData(array(0,64,0), array(0,64,128));

        // set header and footer fonts
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);


        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // ---------------------------------------------------------

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('freeserif', '', 14, '', true);


        // Set margins
        $pdf->SetMargins(5, 5, 5, true);

    }

    /**
     * Function to set tcPdf custom options
     *
     * @param $pdf
     * @param $template
     *
     * @throws \Throwable
     */
    private function tcPdfCustomOptions($pdf, $template)
    {
        $templatesWithSmallMargins = $this->templatesWithSmallMargins($template);

        switch ($template) {
            case ConstructorTemplates::FSCVET_TEMPLATE_CODE:
            case ConstructorTemplates::FSCVET3_TEMPLATE_CODE:
            case ConstructorTemplates::FSC_IMP_QUA2_TEMPLATE_CODE:
            case ConstructorTemplates::FSC_IMP_VET_TEMPLATE_CODE:
                $pdf->SetAutoPageBreak(true, 0);
                $pdf->SetMargins(16, 18, 16, false);
                break;
            case ConstructorTemplates::TJ_SWT_756_CCI_V1:
            case ConstructorTemplates::TJ_SWT_756_CCI_FORM_A:
            case ConstructorTemplates::TJ_SWT_756_CCI_FORM_A_WITHOUT_REG_NUMBER:
            case ConstructorTemplates::TJ_SWT_756_CCI_V2:
                $pdf->SetMargins(2, 5, 2, 2);
                $pdf->SetAutoPageBreak(true, 0);
                break;
            case ConstructorTemplates::TJ_LICENSE_NRSL_SWT_902:
                $pdf->SetMargins(0, 0, 0, false);
                $pdf->SetAutoPageBreak(true, 0);
                break;
            case ConstructorTemplates::SASS_EXAMINATION:
            case ConstructorTemplates::SASS_EXAMINATION_TYPE_ITEM:
                $pdf->SetMargins(10, 5, 10, false);
                $pdf->SetAutoPageBreak(true, 0);
                break;
            case ConstructorTemplates::TJMOH_TEMPLATE_CODE:
                $pdf->SetMargins(20, 18, 20, false);
                break;
            case ConstructorTemplates::FSCVET4_TEMPLATE_CODE :
                $pdf->SetMargins(10, 18, 10, false);
                $pdf->SetAutoPageBreak(true, 0);
                break;
            case ConstructorTemplates::FSCVET5_TEMPLATE_CODE :
                $pdf->SetMargins(10, 10, 10, false);
                break;
            case ConstructorTemplates::KOOC:
                $pdf->SetAutoPageBreak(true, 0);
                break;
            case ConstructorTemplates::FSC_IMP_QUA_TEMPLATE_CODE:
                $pdf->SetAutoPageBreak(true, 0);
                $pdf->SetMargins(16, 0, 0, false);
                break;
            case ConstructorTemplates::FSC1_TEMPLATE_CODE:
                $pdf->SetAutoPageBreak(true, 0);
                $pdf->SetMargins(5, 5, 5, false);
                break;
            default:
                $templatesWithSmallMargins ? $pdf->SetMargins(5, 5, 5, false) : $pdf->SetMargins(16, 18, 16, false);
                break;
        }

    }

    /**
     * Function to output tcPdf
     *
     * @param $pdf
     * @param $fileDir
     * @param $fileName
     * @param $pdfTitle
     *
     * @throws \Throwable
     */
    public function tcPdfOutput($pdf, $fileDir, $fileName, $pdfTitle)
    {
        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        if (is_null($fileDir)) {
            ob_end_clean();
            $pdf->Output($pdfTitle . '.pdf', 'I');

        } else {

            if (!is_dir($fileDir)) {
                mkdir($fileDir, 0775, true);
            }

            $pdfFile = $fileDir . '/' . $fileName;
            $pdf->Output($pdfFile, 'F');
        }
    }

    /**
     * Function to generate pdf template products page
     *
     * @param $template
     * @param $pdf
     * @param $pdfData
     *
     * @throws \Throwable
     */
    private function generateProductsPage($template, $pdf, $pdfData)
    {
        $headOfDepartment = $this->getHeadOfCompany($template, $pdfData['customData']);

        $templatesWithoutSignaturePart = [
            ConstructorTemplates::SASS_TEMPLATE_CODE
        ];

        $signatureHtml = '<table width="100%" border="0">
                                        <tr>
                                            <td style="height:10px"></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 10px;font-weight: bold;line-height: 20px;padding: 0;text-align: center;width: 1%;
                                            ">
                                            </td>
                                            <td style="
                                                font-size: 12px;
                                                font-weight: bold;
                                                padding: 0;
                                                text-align: left;
                                                width: 50%;
                                            ">Сардори раёсати баҳодиҳии мутобикат</td>
                                            <td style="
                                                font-size: 12px;
                                                font-weight: bold;
                                                line-height: 20px;
                                                padding: 0;
                                                text-align: right;
                                                width: 34%;
                                            "> ' . $headOfDepartment . '</td>
                                        </tr>
                                    </table>';

        $productsHtml = view("pdf-templates/{$template}/products")
            ->with([
                'agency' => $pdfData['agency'],
                'subApplication' => $pdfData['subApplication'],
                'subAppProducts' => $pdfData['subAppProducts'],
                'documentId' => $pdfData['documentId'],
                'applicationId' => $pdfData['applicationId'],
                'customData' => $pdfData['customData'],
                'signatureHtml' => $signatureHtml,
                'template' => $pdfData['template'],
                'saf' => $pdfData['saf'],
                'pdf' => $pdf])
            ->render();

        if (!empty($productsHtml)) {

            $pdf->AddPage('P', 'A4');
            $pdf->writeHTMLCell(0, 0, '', '', $productsHtml, 0, 1, 0, true, '', true);
            $totalPageCount = $pdf->getNumPages();

            if (($totalPageCount > 1) && !in_array($template, $templatesWithoutSignaturePart)) {
                for ($i = 2; $i <= $totalPageCount; $i++) {
                    $pdf->setPage($i, false);

                    if ($i != $totalPageCount) {

                        $pdf->SetAutoPageBreak(TRUE, 5);
                        $pdf->writeHTMLCell(0, 0, '', '265', $signatureHtml, 0, 1, 0, true, '', true);
                    }
                }
            }
        }
    }

    /**
     * Function to generate QR code
     *
     * @param $pdf
     * @param $documentId
     * @param $pdfHashKey
     * @param $subApplicationId
     * @param $productsIds
     * @param null $template
     */
    private function generateQrCode($pdf, $documentId, $pdfHashKey, $subApplicationId, $productsIds, $template = null)
    {
        $style = array(
            'border' => false,
            'vpadding' => 'auto',
            'hpadding' => 'auto',
            'fgcolor' => array(0, 0, 0),
            'bgcolor' => false,
            'module_width' => 1,
            'module_height' => 1
        );

        if (is_null($documentId)) {
            $pdf->write2DBarcode(urlWithLng('/application/' . $subApplicationId . '/' . $pdfHashKey), 'QRCODE,R', 255, 5, 38, 38, $style, '');
        } else {
            switch ($template) {
                case ConstructorTemplates::DEFAULT_PDF:
                    $pdf->write2DBarcode(urlWithLng('user-documents/' . $documentId . '/application/' . $subApplicationId . '/' . $pdfHashKey . '/' . $template . '/' . ($productsIds ?? '')), 'QRCODE,R', 255, 5, 38, 38, $style, '');
                    break;
            }
        }
    }

    /**
     * Function to get head of company
     *
     * @param $template
     * @param $customData
     *
     * @return string
     */
    private function getHeadOfCompany($template, $customData)
    {
        switch ($template) {
            case ConstructorTemplates::TAJIKSTANDART_TEMPLATE_CODE:
                $headOfDepartment = $customData['1645_27'] ?? '';
                break;
            case ConstructorTemplates::TJMOH_TEMPLATE_CODE:
                $headOfDepartment = $customData['PHS_001_27'] ?? '';
                break;
            case ConstructorTemplates::TJCS_TEMPLATE_CODE:
                $headOfDepartment = $customData['CSCC_001_28'] ?? '';
                break;
            case ConstructorTemplates::FSC_VET_PHARM_TEMPLATE_CODE:
                $headOfDepartment = $customData['CCVP_27'] ?? '';
                break;
            case ConstructorTemplates::SASS_TEMPLATE_CODE:
                $headOfDepartment = $customData['SAS_001_52'] ?? '';
                break;
            default:
                $headOfDepartment = '';
                break;
        }

        return $headOfDepartment;
    }

    /**
     * Function to return templates with small margins
     *
     * @param $template
     *
     * @return bool
     */
    private function templatesWithSmallMargins($template)
    {
        $templatesWithSmallMargins = [
            ConstructorTemplates::FSC1_TEMPLATE_CODE,
            ConstructorTemplates::SEC_TEMPLATE_CODE,
            ConstructorTemplates::SASS_EXAMINATION,
            ConstructorTemplates::SASS_EXAMINATION_TYPE_ITEM
        ];

        return in_array($template, $templatesWithSmallMargins) ? true : false;
    }

    /**
     * Function to return templates with products part
     *
     * @param $template
     *
     * @return bool
     */
    private function templatesWithoutProductsPart($template)
    {
        $templatesWithoutProductsPart = [
            ConstructorTemplates::FSC_TEMPLATE_CODE,
            ConstructorTemplates::FSC1_TEMPLATE_CODE,
            ConstructorTemplates::MOC_TEMPLATE_CODE,
            ConstructorTemplates::MOC_TJ_TEMPLATE_CODE,
            ConstructorTemplates::SEC_TEMPLATE_CODE,
            ConstructorTemplates::FSCVET_TEMPLATE_CODE,
            ConstructorTemplates::FSCVET2_TEMPLATE_CODE,
            ConstructorTemplates::FSCVET3_TEMPLATE_CODE,
            ConstructorTemplates::FSCVET4_TEMPLATE_CODE,
            ConstructorTemplates::FSCVET5_TEMPLATE_CODE,
            ConstructorTemplates::FSC_IMP_VET_TEMPLATE_CODE,
            ConstructorTemplates::FSC_IMP_QUA_TEMPLATE_CODE,
            ConstructorTemplates::FSC_IMP_QUA2_TEMPLATE_CODE,
            ConstructorTemplates::SASS_EXAMINATION,
            ConstructorTemplates::SASS_EXAMINATION_TYPE_ITEM,
            ConstructorTemplates::SASS_TEMPLATE_CODE,
            ConstructorTemplates::TJ_SWT_756_CCI_V1,
            ConstructorTemplates::TJ_SWT_756_CCI_FORM_A,
            ConstructorTemplates::TJ_SWT_756_CCI_FORM_A_WITHOUT_REG_NUMBER,
            ConstructorTemplates::TJ_SWT_756_CCI_V2,
            ConstructorTemplates::KOOC,
            ConstructorTemplates::TJ_LICENSE_NRSL_SWT_902,
            ConstructorTemplates::TJ_SWT_872,
            ConstructorTemplates::FSC_TJ
        ];

        return in_array($template, $templatesWithoutProductsPart) ? true : false;
    }

    /**
     * Function to return templates with QR code
     *
     * @param $template
     *
     * @return bool
     */
    private function templatesWithQRCode($template)
    {
        $templatesWithQRCode = [];

        return in_array($template, $templatesWithQRCode) ? true : false;
    }

    /**
     * Function to check QR code authorization
     *
     * @param $template
     *
     * @return RedirectResponse|void
     */
    private function checkQRCodeAuthorization($template)
    {
        $template = ConstructorTemplates::select(['qr_code', 'authorization_without_login'])->where('code', $template)->first();

        if (optional($template)->qr_code == ConstructorTemplates::TRUE && optional($template)->authorization_without_login == ConstructorTemplates::FALSE && Auth::guest()) {
            return redirect(urlWithLng('login'));
        }
    }

    /**
     * Function to return downloadable pdf name
     *
     * @param $template
     * @param $subApplication
     *
     * @return string
     */
    private function downloadablePdfName($template, $subApplication)
    {
        $templateName = $this->getPdfTemplateName($template);

        switch ($template) {
            default:
                $downloadablePdfName = trans('swis.user_documents.saf_pdf_prefix') . '_' . $templateName . '_' . (optional($subApplication)->document_number) . '_' . optional($subApplication)->status_date;
        }

        return $downloadablePdfName;
    }

    /**
     * Function to return pdf template name
     *
     * @param $template
     *
     * @return string
     */
    private function getPdfTemplateName($template)
    {
        if (!is_null($template)) {
            $constructorTemplate = ConstructorTemplates::has('ml')->select('id')->where('code', $template)->first();
            $templateName = !is_null($constructorTemplate) ? $constructorTemplate->current()->template_name : '';
        }

        return $templateName ?? '';
    }

    /**
     * Function to get mPdf options
     *
     * @param $template
     *
     * @return array
     */
    private function getMpdfOptions($template)
    {
        $options = [
            'default_font' => 'FreeSans',
        ];

        if ($template == ConstructorTemplates::FSC_2) {
            $options['orientation'] = 'L';
        }

        return $options;
    }
}