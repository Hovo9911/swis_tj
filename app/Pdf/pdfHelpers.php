<?php

use App\Http\Controllers\Core\Dictionary\DictionaryManager;
use App\Models\BaseModel;
use App\Models\DataModel\DataModel;
use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure;
use App\Models\UserDocuments\UserDocuments;

/**
 * Function to get array variables max symbols count
 *
 * @param $array
 *
 * @return bool|false|int
 */
if (!function_exists('getSymbolsCount')) {
    function getSymbolsCount($array)
    {
        $maxCount = 1;
        foreach ($array as $value) {
            $countTextSymbols = mb_strlen($value, 'UTF-8');
            if ($countTextSymbols > $maxCount) {
                $maxCount = $countTextSymbols;
            }
        }
        return $maxCount;
    }
}

/**
 * Function to create empty row for pdf and give height
 *
 * @param $emptyRowHeight
 *
 * @return string
 */
if (!function_exists('emptyRow')) {
    function emptyRow($emptyRowHeight)
    {
        return '<tr>
        <td class="border_show_r" style="height:' . $emptyRowHeight . 'px"></td>
        <td class="border_show_r"></td>
        <td class="border_show_r"></td>
        <td class="border_show_r"></td>
        <td class="border_show_r"></td>
        <td class="border_show_r"></td>
    </tr>';
    }
}

/**
 * Function to return months
 *
 * @param $monthId
 * @param bool $lngId
 *
 * @return mixed|string
 */
if (!function_exists('month')) {
    function month($monthId, $lngId = false)
    {
        if (!$monthId) {
            return '';
        }

        $dictionaryManager = new DictionaryManager();

        if(strlen($monthId) == 1 && (int)$monthId <= 9){
            $monthId = '0'.$monthId;
        }

        $months = array(
            '01' => $dictionaryManager->getTransByLngCode('swis.pdf.january', $lngId),
            '02' => $dictionaryManager->getTransByLngCode('swis.pdf.february', $lngId),
            '03' => $dictionaryManager->getTransByLngCode('swis.pdf.march', $lngId),
            '04' => $dictionaryManager->getTransByLngCode('swis.pdf.april', $lngId),
            '05' => $dictionaryManager->getTransByLngCode('swis.pdf.may', $lngId),
            '06' => $dictionaryManager->getTransByLngCode('swis.pdf.june', $lngId),
            '07' => $dictionaryManager->getTransByLngCode('swis.pdf.jule', $lngId),
            '08' => $dictionaryManager->getTransByLngCode('swis.pdf.august', $lngId),
            '09' => $dictionaryManager->getTransByLngCode('swis.pdf.september', $lngId),
            '10' => $dictionaryManager->getTransByLngCode('swis.pdf.october', $lngId),
            '11' => $dictionaryManager->getTransByLngCode('swis.pdf.november', $lngId),
            '12' => $dictionaryManager->getTransByLngCode('swis.pdf.december', $lngId)
        );

        return array_key_exists($monthId, $months) ? $months[$monthId] : '';
    }
}

/**
 * Function to return string
 *
 * @param array $data
 * @param string $field
 * @param string $otherText
 * @param string $otherTextInsertPosition
 *
 * @return string
 */
if (!function_exists('notEmptyForPdfFields')) {
    function notEmptyForPdfFields($data, string $field, string $otherText = '', string $otherTextInsertPosition = 'last', string $emptyValue = ''): string
    {
        if (!empty($data) && !empty($field) && !empty($data[$field])) {

            if (is_array($data[$field])) {

                $returnField = implode(', ', $data[$field]);

            } else {

                $returnField = $data[$field];

            }

            if (!empty($otherText)) {
                if ($otherTextInsertPosition == 'last') {
                    $returnField .= $otherText;
                } elseif ($otherTextInsertPosition == 'first') {
                    $returnField = $otherText . $returnField;
                }
            }
        } else {
            $returnField = $emptyValue;
        }

        return $returnField;
    }
}

/**
 * Function to get value in multiple rows
 *
 * @param $mapping
 * @param $firstRowCharactersCount
 * @param $otherRowsCharactersCount
 *
 * @return array
 */
if (!function_exists('getMappingValueInMultipleRows')) {
    function getMappingValueInMultipleRows($mapping, $firstRowCharactersCount, $otherRowsCharactersCount)
    {
        mb_internal_encoding("UTF-8");
        $result = [];

        if (mb_strlen($mapping) > $firstRowCharactersCount) {

            $length = mb_strpos(utf8Wordwrap($mapping, $firstRowCharactersCount), "\n", 0);
            $firstRowMapping = mb_substr($mapping, 0, $length);
            $result[] = $firstRowMapping;
            $mapping = mb_substr($mapping, $length);

            if (!empty($mapping)) {
                $length = mb_strpos(utf8Wordwrap($mapping, $otherRowsCharactersCount), "\n", 0);
                $secondRowMapping = mb_substr($mapping, 0, $length);
                $result[] = $secondRowMapping;

                while (mb_strlen($mapping) > $otherRowsCharactersCount) {
                    $mapping = mb_substr($mapping, $length);
                    $length = mb_strpos(utf8Wordwrap($mapping, $otherRowsCharactersCount), "\n", 0);
                    $otherRowMapping = mb_substr($mapping, 0, $length);
                    $result[] = $otherRowMapping;
                }
            }

        } else {
            $result[] = $mapping;
        }
        return $result;
    }
}

/**
 * Function to return wordwrap UTF-8
 *
 * @param $string
 * @param $width
 * @param string $break
 *
 * @return string|string[]|null
 */
if (!function_exists('utf8Wordwrap')) {
    function utf8Wordwrap($string, $width, $break = "\n")
    {
        $string = nl2br($string);
        // Anchor the beginning of the pattern with a lookahead
        // to avoid crazy backtracking when words are longer than $width
        $pattern = '/(?=\s)(.{1,' . $width . '})(?:\s|$)/uS';
        $replace = '$1' . $break;

        return preg_replace($pattern, $replace, $string);
    }
}

/**
 * Function to return custom data fields values
 *
 * @param $documentId
 * @param $customData
 * @param bool $lngId
 * @param string $refColumn
 *
 * @return array
 */
if (!function_exists('getFieldsValues')) {
    function getFieldsValues($documentId, $customData, $lngId = false, $refColumn = 'name')
    {
        $fieldsData = DocumentsDatasetFromMdm::select('field_id', 'mdm_field_id')->where('document_id', $documentId)->get();
        $dataModelAllData = DataModel::allData();
        $fields = [];

        if (!is_null($customData)) {
            foreach ($fieldsData as $fieldInfo) {

                $fieldValue = array_key_exists($fieldInfo['field_id'], $customData) ? $customData[$fieldInfo['field_id']] : '';

                if (is_null($fieldValue)) {
                    $fields[$fieldInfo['field_id']] = '';
                }

                if (!empty($fieldValue)) {

                    $mdmField = (object)$dataModelAllData[$fieldInfo['mdm_field_id']];

                    switch ($mdmField->type) {
                        case DataModel::MDM_ELEMENT_TYPE_MULTIPLE:
                            if (is_array($fieldValue) && !empty($mdmField->reference)) {
                                $mdmArray = [];
                                foreach ($fieldValue as $key => $referenceRowId) {
                                    $mdmArray[] = optional(getReferenceRows(ReferenceTable::REFERENCE_TABLE_PREFIX . $mdmField->reference, $referenceRowId, false, [$refColumn], 'get', false, false, [], false, $lngId))->$refColumn;
                                }
                                $fields[$fieldInfo['field_id']] = implode(', ', $mdmArray);
                            }
                            break;
                        case DataModel::MDM_ELEMENT_TYPE_DATE:
                            $fields[$fieldInfo['field_id']] = formattedDate($fieldValue);
                            break;
                        case DataModel::MDM_ELEMENT_TYPE_DATE_TIME:
                            $fields[$fieldInfo['field_id']] = formattedDate($fieldValue, true);
                            break;
                        default:
                            if (!empty($mdmField->reference)) {
                                $fields[$fieldInfo['field_id']] = optional(getReferenceRows(ReferenceTable::REFERENCE_TABLE_PREFIX . $mdmField->reference, $fieldValue, false, [$refColumn], 'get', false, false, [], false, $lngId))->$refColumn;
                            } else {
                                $fields[$fieldInfo['field_id']] = $fieldValue;
                            }
                            break;
                    }
                }
            }
        }
        return $fields;
    }
}

/**
 * Function to get saf data field_id value
 *
 * @param $saf
 * @param $subApplication
 * @param $products
 * @param $subAppProductsIds
 * @param null $lngId
 * @param string $refColumn
 *
 * @return array[]
 */
if (!function_exists("getSafDataFieldIdValue")) {
    function getSafDataFieldIdValue($saf, $subApplication, $products, $subAppProductsIds, $lngId = null, $refColumn = 'name')
    {
        $dataModel = DataModel::allData();
        $lngId = $lngId ?: cLng('id');
        $getSafIdNameArray = getSafIdNameArray();
        $returnMainData = $returnProductsData = $returnProductBatchesData = [];
        $lastXML = SingleApplicationDataStructure::lastXml();
        $getSafProductsIdNameArray = getSafProductsIdNameArray();
        $applicationDataByPrefix = prefixKey('', $subApplication->data);
        $getSafControlIdNameArray = array_keys(getSafControlFieldIdNameArray());
        // extract make array keys as variable name !!!!!! $routingValues, $routingProductValues
        extract(UserDocuments::getSafControlFieldsForPdf($subApplication, $saf, $subAppProductsIds));


        foreach ($getSafIdNameArray as $fieldId => $fieldName) {
            if (!in_array($fieldId, $getSafControlIdNameArray)) {
                $fieldName = str_replace('[', '.', str_replace(']', '', str_replace('][', '.', $fieldName)));
                $fieldValue = isset($applicationDataByPrefix[$fieldName]) ? $applicationDataByPrefix[$fieldName] : '';
            } else {
                $fieldValue = isset($routingValues[$fieldName]) ? $routingValues[$fieldName] : '';
            }

            $mdm = (int)$lastXML->xpath("//*[@id='{$fieldId}']")[0]->attributes()->mdm;
            if (!empty($mdm) && array_key_exists($mdm, $dataModel) && !empty($dataModel[$mdm]['reference']) && $reference = $dataModel[$mdm]['reference']) {
                $fieldValue = optional(getReferenceRows($reference, $fieldValue, false, [$refColumn], 'get', false, false, [], false, $lngId))->{$refColumn};
            }
            $returnMainData[$fieldId] = $fieldValue;
        }


        foreach ($products as $product) {
            foreach ($getSafProductsIdNameArray as $fieldId => $fieldName) {
                $fieldName = str_replace('[', '.', str_replace(']', '', str_replace('][', '.', $fieldName)));
                if (isset($routingProductValues[$product->id]) && in_array($fieldName, $routingProductValues[$product->id])) {
                    $fieldValue = $routingProductValues[$product->id][$fieldName];
                } else {
                    $fieldValue = $product->{$fieldName};
                }

                $mdm = (int)$lastXML->xpath("//*[@id='{$fieldId}']")[0]->attributes()->mdm;
                if (!empty($mdm) && array_key_exists($mdm, $dataModel) && !empty($dataModel[$mdm]['reference']) && $reference = $dataModel[$mdm]['reference']) {
                    $fieldValue = optional(getReferenceRows($reference, $fieldValue, false, [$refColumn], 'get', false, false, [], false, $lngId))->{$refColumn};
                }
                $returnProductsData[$product->id][$fieldId] = $fieldValue;
            }


            foreach ($product->batches as $batch) {
                foreach (getSafProductsBatchesIdNameArray() as $fieldId => $fieldName) {
                    $fieldValue = $batch[$fieldName];

                    $mdm = (int)$lastXML->xpath("//*[@id='{$fieldId}']")[0]->attributes()->mdm;
                    if (!empty($mdm) && array_key_exists($mdm, $dataModel) && !empty($dataModel[$mdm]['reference']) && $reference = $dataModel[$mdm]['reference']) {
                        $fieldValue = optional(getReferenceRows($reference, $fieldValue, false, [$refColumn], 'get', false, false, [], false, $lngId))->{$refColumn};
                    }
                    $returnProductBatchesData[$product->id][$batch->id][$fieldId] = $fieldValue;
                }
            }
        }

        return [$returnMainData, $returnProductsData, $returnProductBatchesData];
    }
}

/**
 * Function to str_split_unicode
 *
 * @param string $text
 * @param int $symbolCount
 * @param int $otherSymbolCount
 *
 * @return array|null
 */
if (!function_exists("str_split_unicode")) {
    function str_split_unicode($text, int $symbolCount = 0, int $otherSymbolCount = 0): array
    {
        if ($symbolCount > 0 && !empty($text)) {
            $result = array();
            $len = mb_strlen($text, "UTF-8");
            for ($i = 0; $i < $len;) {
                if ($otherSymbolCount > 0 && $i > 0) {
                    $symbolCount = $otherSymbolCount;
                }
                $mySymbolCount = $symbolCount;

                if ($i + $mySymbolCount < $len && isset($text[$i + $symbolCount - 1]) && $text[$i + $symbolCount - 1] != ' ') {
                    $myText = mb_substr($text, $i, $symbolCount, "UTF-8");
                    $mySymbolPosition = $i + mb_strrpos($myText, ' ', "UTF-8") + 1;

                    if ($mySymbolPosition == $i + 1) {
                        $mySymbolPosition = $i + $symbolCount;
                    }
                    $mySymbolCount = $mySymbolPosition - $i;

                }

                $result[] = mb_substr($text, $i, $mySymbolCount, "UTF-8");
                $i = $i + $mySymbolCount;
            }
            return $result;
        }

        return [];
    }
}

/*
 *
 * ------------------------------------------ START FUNCTIONS FOR TEST PDF ---------------------------------------------
 */

/**
 * Function to get keys in current file
 *
 * @param string $fileName
 * @param string $delimiterType
 */
if (!function_exists("getKeyInCurrentFile")) {
    function getKeyInCurrentFile($fileName = '', $delimiterType = 'trans')
    {
        $path = 'views/pdf-templates/';
        $filePath = $path . $fileName . '/edit.blade.php';

        $text = file_get_contents(resource_path($filePath));
        if ($delimiterType == 'trans') {
            $startDelimiter = 'trans("';
            $endDelimiter = '")';
        } else {
            $startDelimiter = '$dictionaryManager->getTransByLngCode("';
            $endDelimiter = '", BaseModel::LANGUAGE_CODE_';
        }

        $data = getStringsBetween($text, $startDelimiter, $endDelimiter);
        $valueText = [];

        foreach ($data as $value) {
            if (strstr($value, '{$template}')) {
                $valueText[] = str_replace('{$template}', $fileName, $value);
            }
        }

        $valueText = array_unique($valueText);
        $valueText = "\n gayane@helix.am jan " . "\n" . implode("\n", $valueText);

        file_put_contents(resource_path($filePath), $valueText, FILE_APPEND);

    }
}

/**
 * Function to get text for max symbols count
 *
 * @param null $maxSymbolCount
 * @param string $lngCode
 * @param bool $calculateSymbolCount
 *
 * @return false|float|string|null
 */
function getTextForMaxSymbolCount($maxSymbolCount = null, $lngCode = '', bool $calculateSymbolCount = false)
{
    if (empty($lngCode)) {
        $lngCode = cLng('code');
    }

    /*
        ru, tj, kg - 40%
        en - 75%
        hy - 25%
    */
    $percent = 40;

    if ($lngCode == BaseModel::LANGUAGE_CODE_RU) {
        $percent = 40;
    } else if ($lngCode == BaseModel::LANGUAGE_CODE_EN) {
        $percent = 75;
    } else if ($lngCode == BaseModel::LANGUAGE_CODE_TJ) {
        $percent = 40;
    }

    $text = 'ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖ ЖЖЖЖ, ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖ. ЖЖЖЖЖЖЖЖ, ЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖ ЖЖЖЖ.
     ЖЖЖЖ, ЖЖЖЖЖ ЖЖЖЖЖЖ ЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖЖЖЖЖЖЖЖ. ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖ ЖЖЖЖ, ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖ.
     ЖЖЖЖЖЖЖЖ, ЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖ ЖЖЖЖ. ЖЖЖЖ, ЖЖЖЖЖ ЖЖЖЖЖЖ ЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖЖЖЖЖЖЖЖ.ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖ ЖЖЖЖ, ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖ. ЖЖЖЖЖЖЖЖ, ЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖ ЖЖЖЖ.
     ЖЖЖЖ, ЖЖЖЖЖ ЖЖЖЖЖЖ ЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖЖЖЖЖЖЖЖ. ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖ ЖЖЖЖ, ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖ.
     ЖЖЖЖЖЖЖЖ, ЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖ ЖЖЖЖ. ЖЖЖЖ, ЖЖЖЖЖ ЖЖЖЖЖЖ ЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖЖЖЖЖЖЖЖ.ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖ ЖЖЖЖ, ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖ. ЖЖЖЖЖЖЖЖ, ЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖ ЖЖЖЖ.
     ЖЖЖЖ, ЖЖЖЖЖ ЖЖЖЖЖЖ ЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖЖЖЖЖЖЖЖ. ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖ ЖЖЖЖ, ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖ.
     ЖЖЖЖЖЖЖЖ, ЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖ ЖЖЖЖ. ЖЖЖЖ, ЖЖЖЖЖ ЖЖЖЖЖЖ ЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖЖЖЖЖЖЖЖ.ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖ ЖЖЖЖ, ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖ. ЖЖЖЖЖЖЖЖ, ЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖ ЖЖЖЖ.
     ЖЖЖЖ, ЖЖЖЖЖ ЖЖЖЖЖЖ ЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖЖЖЖЖЖЖЖ. ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖ ЖЖЖЖ, ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖ.
     ЖЖЖЖЖЖЖЖ, ЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖ ЖЖЖЖ. ЖЖЖЖ, ЖЖЖЖЖ ЖЖЖЖЖЖ ЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖЖЖЖЖЖЖЖ.ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖ ЖЖЖЖ, ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖ. ЖЖЖЖЖЖЖЖ, ЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖ ЖЖЖЖ.
     ЖЖЖЖ, ЖЖЖЖЖ ЖЖЖЖЖЖ ЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖЖЖЖЖЖЖЖ. ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖ ЖЖЖЖ, ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖ.
     ЖЖЖЖЖЖЖЖ, ЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖ ЖЖЖЖ. ЖЖЖЖ, ЖЖЖЖЖ ЖЖЖЖЖЖ ЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖЖЖЖЖЖЖЖ.ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖ ЖЖЖЖ, ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖ. ЖЖЖЖЖЖЖЖ, ЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖ ЖЖЖЖ.
     ЖЖЖЖ, ЖЖЖЖЖ ЖЖЖЖЖЖ ЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖЖЖЖЖЖЖЖ. ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖ ЖЖЖЖ, ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖ.
     ЖЖЖЖЖЖЖЖ, ЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖ ЖЖЖЖ. ЖЖЖЖ, ЖЖЖЖЖ ЖЖЖЖЖЖ ЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖЖЖЖЖЖЖЖ.ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖ ЖЖЖЖ, ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖ. ЖЖЖЖЖЖЖЖ, ЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖ ЖЖЖЖ.
     ЖЖЖЖ, ЖЖЖЖЖ ЖЖЖЖЖЖ ЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖЖЖЖЖЖЖЖ. ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖ ЖЖЖЖ, ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖ.
     ЖЖЖЖЖЖЖЖ, ЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖ ЖЖЖЖ. ЖЖЖЖ, ЖЖЖЖЖ ЖЖЖЖЖЖ ЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖЖЖЖЖЖЖЖ.ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖ ЖЖЖЖ, ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖ. ЖЖЖЖЖЖЖЖ, ЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖ ЖЖЖЖ.
     ЖЖЖЖ, ЖЖЖЖЖ ЖЖЖЖЖЖ ЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖЖЖЖЖЖЖЖ. ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖ ЖЖЖЖ, ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖ.
     ЖЖЖЖЖЖЖЖ, ЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖ ЖЖЖЖ. ЖЖЖЖ, ЖЖЖЖЖ ЖЖЖЖЖЖ ЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖЖЖЖЖЖЖЖ.ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖ ЖЖЖЖ, ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖ. ЖЖЖЖЖЖЖЖ, ЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖ ЖЖЖЖ.
     ЖЖЖЖ, ЖЖЖЖЖ ЖЖЖЖЖЖ ЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖЖЖЖЖЖЖЖ. ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖ ЖЖЖЖ, ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖ.
     ЖЖЖЖЖЖЖЖ, ЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖ ЖЖЖЖ. ЖЖЖЖ, ЖЖЖЖЖ ЖЖЖЖЖЖ ЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖЖЖЖЖЖЖЖ.ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖ ЖЖЖЖ, ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖ. ЖЖЖЖЖЖЖЖ, ЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖ ЖЖЖЖ.
     ЖЖЖЖ, ЖЖЖЖЖ ЖЖЖЖЖЖ ЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖЖЖЖЖЖЖЖ. ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖ ЖЖЖЖ, ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖЖЖ ЖЖЖЖ.
     ЖЖЖЖЖЖЖЖ, ЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖЖЖЖЖ ЖЖЖЖ. ЖЖЖЖ, ЖЖЖЖЖ ЖЖЖЖЖЖ ЖЖЖ ЖЖЖЖЖ ЖЖЖЖ ЖЖЖЖЖЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖ ЖЖЖЖЖЖЖЖЖЖ.';

    if ($calculateSymbolCount && !empty($maxSymbolCount)) {

        $maxSymbolCount = $maxSymbolCount + round(($maxSymbolCount * $percent) / 100, 0);
        return $maxSymbolCount;

    } else {

        return mb_substr($text, 0, $maxSymbolCount, 'UTF-8');

    }
}

/*
 *
 * ------------------------------------------ END FUNCTIONS FOR TEST PDF ---------------------------------------------
 */


