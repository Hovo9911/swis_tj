<?php

use App\Models\Agency\Agency;
use App\Models\BaseModel;
use App\Models\DataModel\DataModel;
use App\Models\DocumentsDatasetFromMdm\DocumentsDatasetFromMdm;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\SingleApplicationDataStructure\SingleApplicationDataStructure;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


/* ========================================================================================
                                Global Helper Functions - Start
 ======================================================================================== */

if (!function_exists("responseResult")) {
    function responseResult($status, $url = null, $data = null, $validationResult = null, $httpStatusCode = 200, $httpHeaders = ['Content-Type' => 'application/json;charset=utf8'])
    {
        if ($status === null && $validationResult !== null) {
            $status = $validationResult->isValid() ? 'OK' : 'INVALID_DATA';
        }
        $result = [
            'status' => $status,
        ];

        if ($validationResult !== null) {
            $result['errors'] = $validationResult;
        }

        if (!empty($data)) {
            $result['data'] = $data;
        }
        if ($url !== null) {
            $result['data']['backToUrl'] = $url;
        }
        try {
            return Response::json($result, $httpStatusCode, $httpHeaders);
        } catch (Exception $e) {
            dump($e);
        }
    }
}



if (!function_exists("deletePendingFile")) {
    function deleteFile($filePath, $removeDir = true)
    {
        if (file_exists($filePath)) {

            unlink($filePath);

            if ($removeDir) {
                rmdir(dirname($filePath));
            }
        }
    }
}

/* ========================================================================================
                                Global Helper Functions - ENd
 ======================================================================================== */


/* ========================================================================================
                                Route Helper Functions - Start
 ======================================================================================== */

if (!function_exists("currentRouteNameIs")) {
    function currentRouteNameIs($routeName)
    {

        if (!is_array($routeName) && Route::currentRouteName() == $routeName) {
            return true;
        }

        if (is_array($routeName) && in_array(Route::currentRouteName(), $routeName)) {
            return true;
        }

        return false;
    }
}

if (!function_exists("urlWithLng")) {
    function urlWithLng($url)
    {
        return url(cLng('code') . '/' . ltrim($url, '/'));
    }
}

if (!function_exists("routeWithLng")) {
    function routeWithLng($name, $params = [])
    {
        $params = array_merge($params, ['lngCode' => cLng('code')]);
        return route($name, $params);
    }
}

if (!function_exists("linkToModule")) {
    function linkToModule($moduleLink, $id = false)
    {
        if ($id) {
            return '<a target="_blank" class="fs12 link-to-module" href="' . urlWithLng($moduleLink . '/' . $id . '/edit') . '">' . trans('swis.link.to.module') . '</a>';
        }
    }
}

if (!function_exists("linkToModuleByHref")) {
    function linkToModuleByHref($moduleLink)
    {
        return '<a target="_blank" class="fs12 link-to-module" href="' . urlWithLng($moduleLink) . '">' . trans('swis.link.to.module') . '</a>';
    }
}

/* ========================================================================================
                                Route Helper Functions - End
 ======================================================================================== */


/* ========================================================================================
                                Date Helper Functions - Start
 ======================================================================================== */

if (!function_exists("setDatePlaceholder")) {
    function setDatePlaceholder($dateTime = false)
    {
        $langCode = cLng('code');

        if (!$dateTime) {
            switch ($langCode) {
                case 'am':
                    $placeHolder = 'օօ/աա/տտտտ';
                    break;
                case 'tj':
                    $placeHolder = 'рр/мм/сссс';
                    break;
                case 'ru':
                    $placeHolder = 'дд/мм/гггг';
                    break;
                case 'ky':
                    $placeHolder = 'кк/аа/жжжж';
                    break;
                default:
                    $placeHolder = 'dd/mm/yyyy';
                    break;
            }
        } else {

            switch ($langCode) {
                case 'am':
                    $placeHolder = 'օօ/աա/տտտտ ժժ:րր';
                    break;
                case 'tj':
                    $placeHolder = 'рр/мм/сссс сс:дд';
                    break;
                case 'ru':
                    $placeHolder = 'дд/мм/гггг чч:мм';
                    break;
                case 'ky':
                    $placeHolder = 'кк/аа/жжжж сс:мм';
                    break;
                default:
                    $placeHolder = 'dd/mm/yyyy hh:mm';
                    break;
            }

        }

        return $placeHolder;
    }
}

if (!function_exists("currentDate")) {
    function currentDate()
    {
        return date(config('swis.date_format'));
    }
}

if (!function_exists("currentDateFront")) {
    function currentDateFront()
    {
        return \Carbon\Carbon::now()->format(config('swis.date_format_front'));
    }
}

if (!function_exists("currentDateTimeFront")) {
    function currentDateTimeFront($endDay = false)
    {
        if ($endDay) {
            return \Carbon\Carbon::now()->endOfDay()->format(config('swis.date_time_format_front'));
        }

        return \Carbon\Carbon::now()->format(config('swis.date_time_format_front'));
    }
}

if (!function_exists("currentDateTime")) {
    function currentDateTime()
    {
        return \Carbon\Carbon::parse(date(config('swis.date_time_format')));
    }
}

if (!function_exists("currentDateTimeNameFormat")) {
    function currentDateTimeNameFormat()
    {
        return \Carbon\Carbon::parse(date(config('swis.date_time_format')))->format('Y-m-d-H-i-s');
    }
}

if (!function_exists("formattedDate")) {
    function formattedDate($value, $dateTime = false, $isReport = false)
    {
        if ($value && (isDateFormat($value) || isDateFormat($value, true))) {
            $format = config('swis.date_format_front');

            if ($dateTime) {
                $format = config('swis.date_time_format_front');
            }

            if ($isReport) {
                $format = config('swis.date_format_report');
            }

            return \Carbon\Carbon::parse($value)->format($format);
        }

        return $value;
    }
}

if (!function_exists("isDateFormat")) {
    function isDateFormat($dateValue, $dateTime = false)
    {
        $format = config('swis.date_format');

        if ($dateTime) {
            $format = config('swis.date_time_format');
        }

        if (DateTime::createFromFormat($format, $dateValue) !== false && DateTime::getLastErrors()['warning_count'] == '0') {
            return true;
        }

        return false;
    }
}

if (!function_exists("datesDifferenceInMinutes")) {
    function datesDifferenceInMinutes($startDate, $endDate)
    {
        $diff = 0;

        $diff += Carbon::parse($endDate)->diffInMinutes(Carbon::parse($startDate));

        return $diff;
    }
}

if (!function_exists("getMinutesDifferenceBetweenDates")) {
    function getMinutesDifferenceBetweenDates($dates)
    {
        $diff = 0;
        foreach ($dates as $date) {
            $diff += Carbon::parse($date['date_to'])->diffInMinutes(Carbon::parse($date['date_from']));
        }

        return $diff;
    }
}

/* ========================================================================================
                                Date Helper Functions - End
 ======================================================================================== */


/* ========================================================================================
                                Buttons Helper Functions - Start
 ======================================================================================== */

if (!function_exists("renderAddButton")) {
    function renderAddButton($specialKey = 'core.base.button.add')
    {
        if (App::environment('local') || App::environment('development')) {
            return '<button type="submit" class="btn btn-success mc-nav-button-add btn--plus"><i class="fa fa-plus"></i>' . trans($specialKey) . '</button>';
        } else {
            if (\Auth::user()->canCreate()) {
                return '<button type="submit" class="btn btn-success mc-nav-button-add btn--plus"><i class="fa fa-plus"></i>' . trans($specialKey) . '</button>';
            }
        }
    }
}

if (!function_exists("renderDeleteButton")) {
    function renderDeleteButton()
    {
        if (App::environment('local') || App::environment('development')) {
            return '<button type="button" class="btn--delete btn  btn-danger mc-nav-button-delete"><i class="fa fa-times"></i>' . trans('core.base.button.delete') . '</button>';
        } else {
            if (\Auth::user()->canDelete()) {
                return '<button type="button" class="btn--delete btn  btn-danger mc-nav-button-delete"><i class="fa fa-times"></i>' . trans('core.base.button.delete') . '</button>';
            }
        }
    }
}

if (!function_exists("renderEditButton")) {
    function renderEditButton()
    {
        if (App::environment('local') || App::environment('development')) {
            return '<button type="submit" class="btn mr-10 btn-success mc-nav-button-save">' . trans('core.base.button.save') . '</button>';
        } else {
            $routeUrl = \Illuminate\Support\Facades\Route::current()->uri();
            $createUrl = false;
            if (!empty($routeUrl)) {
                $exUrl = explode('/', $routeUrl);
                if (isset($exUrl['2']) && $exUrl['2'] == 'create') {
                    $createUrl = true;
                }
            }

            $closeBtn = false;
            $viewType = request()->route('viewType');
            if (!is_null($viewType) && $viewType == BaseModel::VIEW_MODE_VIEW) {
                $closeBtn = true;
            }

            if (($createUrl || \Auth::user()->canUpdate()) && !$closeBtn) {
                return '<button type="submit" class="btn mr-10 btn-success mc-nav-button-save">' . trans('core.base.button.save') . '</button>';
            }
        }

    }
}

if (!function_exists("renderCancelButton")) {
    function renderCancelButton()
    {
        return '<button type="button" class="btn  btn-default mc-nav-button-cancel">' . trans('core.base.button.back') . '</button>';
    }
}

if (!function_exists("renderWeekendDaysButton")) {
    function renderWeekendDaysButton()
    {
        return '<button type="button" class="btn  btn-success mc-nav-button weekend-days">' . trans('core.base.button.weekend_days') . '</button>';
    }
}

/* ========================================================================================
                                Buttons Helper Functions - End
 ======================================================================================== */


/* ========================================================================================
                                Language Helper Functions - Start
 ======================================================================================== */

if (!function_exists("cLng")) {
    function cLng($key = null)
    {
        $language = \App\Http\Controllers\Core\Language\ApplicationLanguageManager::getInstance();

        if (!empty($language->id)) {

            if ($key == 'code') {
                return $language->code;
            }

            return $language->id;
        }

        if ($key == 'id') {
            return \App\Models\Languages\Language::LANGUAGE_CODE_RU;
        }

        return env('DEFAULT_LANGUAGE_CODE', 'en');
    }
}

if (!function_exists("activeLanguages")) {
    function activeLanguages()
    {
        return \App\Http\Controllers\Core\Language\LanguageManager::getActiveLanguages();
    }
}

if (!function_exists("currentLanguage")) {
    function currentLanguage()
    {
        return \App\Models\Languages\Language::where('id', cLng('id'))->first();
    }
}

if (!function_exists("activeLanguagesIdCode")) {
    function activeLanguagesIdCode()
    {
        $application = \App\Models\Application\ApplicationLanguages::with(['language'])->where(['application_id' => 1])->get();

        $languages = [];
        foreach ($application as $value) {
            $languages[$value->language->id] = $value->language->code;
        }

        return $languages;
    }
}

/* ========================================================================================
                                Language Helper Functions - End
 ======================================================================================== */


/* ========================================================================================
                                Rules Helper Functions - Start
 ======================================================================================== */

if (!function_exists("validateSyntax")) {
    function validateSyntax($condition)
    {
        preg_match_all('/SAF_ID_[\w]*/ius', $condition, $variables); // match all variables inside the condition
        $script = '';

        // assign some values to the variables
        foreach ($variables[0] as $key => $variable) {
            $script .= '$' . $variable . ' = ' . $key . ";\r\n";
        }
        $condition = str_replace('SAF', '$SAF', $condition);

        // check is valid the syntax of the condition
        $script .= 'if(' . $condition . ') {  } return true;';
        $script = 'try {' . $script . '} catch(\Exception $e) { return $e->getMessage(); }';

        $result = false;
        try {
            $result = runScript($script);
        } catch (\ParseError $e) {
            $result = $e->getMessage();
        } catch (\Exception $e) {
            $result = $e->getMessage();
        }

        // return true if the syntax of the condition is valid or exception message
        return $result;
    }
}

if (!function_exists("checkCondition")) {
    function checkCondition($data, $condition, $type = 'saf', $subApplication = null, $stateId = null)
    {
        preg_match_all('/SAF_ID_[\w]*/ius', $condition, $variables); // match all variables inside the condition
        $script = '';

        $definedVariables = [];
        // assign filled values to the variables
        foreach ($variables[0] as $key => $variable) {
            if ($type == 'saf') {
                if (isset($data[$variable]) && !isset($definedVariables[$variable])) {
                    $definedVariables[$variable] = '';
                    $script .= '$' . $variable . ' = "' . $data[$variable] . "\";\r\n";
                }
            } else {
                if ((array_key_exists($variable, $data) && is_null($data[$variable])) || isset($data[$variable])) {
                    $script .= '$' . $variable . ' = "' . $data[$variable] . "\";\r\n";
                }
            }

        }
        $condition = str_replace('SAF', '$SAF', $condition);

        preg_match_all('/FIELD_ID_[\w]*/ius', $condition, $variables); // match all variables inside the condition

        // assign filled values to the variables
        foreach ($variables[0] as $key => $variable) {
            if ($type == 'saf') {
                if (isset($data[$variable]) && !isset($definedVariables[$variable])) {
                    $definedVariables[$variable] = '';
                    $script .= '$' . $variable . ' = "' . $data[$variable] . "\";\r\n";
                }
            } else {
                $script .= '$' . $variable . ' = null;';
                if ((array_key_exists($variable, $data) && is_null($data[$variable])) || isset($data[$variable])) {
                    $script .= '$' . $variable . ' = "' . $data[$variable] . "\";\r\n";
                }
            }
        }
        $condition = str_replace('FIELD', '$FIELD', $condition);

        if (!is_null($subApplication)) {

            $safAgencySubDivisionId = \App\Models\SingleApplication\SingleApplication::SAF_AGENCY_SUBDIVISION_ID;

            $state = !empty($subApplication->applicationCurrentState->state) ? $subApplication->applicationCurrentState->state->current()->state_name : '';
            $companyInfo = Agency::getAgencyInfo();
            $subDivisionRefId = isset($data[$safAgencySubDivisionId]) ? $data[$safAgencySubDivisionId] : null;
            $subDivisionInfo = \App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications::getSubDivisionInfo($subDivisionRefId);

            $userId = Auth::id();
            $script .= "if (!defined('CURRENT_STATE')) { define('CURRENT_STATE', '{$state}'); }";
            $script .= "if (!defined('IS_MANUAL')) { define('IS_MANUAL', '{$subApplication->manual_created}'); }";
            $script .= "if (!defined('SUB_APPLICATION_ID')) { define('SUB_APPLICATION_ID', '{$subApplication->id}'); }";
            $script .= "if (!defined('NEXT_STATE_ID')) { define('NEXT_STATE_ID', '{$stateId}'); }";
            $script .= "if (!defined('USER_ID')) { define('USER_ID', '{$userId}'); }";
            $script .= "if (!defined('COMPANY_INFO')) { define('COMPANY_INFO', '{$companyInfo}'); }";
            $script .= "if (!defined('SAF_TYPE')) { define('SAF_TYPE', '{$subApplication->regime}'); }";
            $script .= "if (!defined('SUBDIVISION_INFO')) { define('SUBDIVISION_INFO', '{$subDivisionInfo}'); }";
        }

        // check is matching the condition
        $script .= 'if (' . $condition . ') { return true; } else { return false; }';
        $script = 'try {' . $script . '} catch(\Exception $e) { return $e->getMessage(); }';
//    echo $script;

        $result = false;
        try {
            $result = runScript($script);
        } catch (\ParseError $e) {
            $result = $e->getMessage();
        } catch (\Exception $e) {
            $result = $e->getMessage();
        }
        // return true if the matches the condition, false if not matches or exception message
        return $result;
    }
}

if (!function_exists("matchRules")) {
    function matchRules($data, $rules, $subApplication, $stateId)
    {
        preg_match_all('/SAF_ID_[\w]*/ius', $rules, $variables); // match all variables inside the condition
        $script = '';

        // assign filled values to the variables
        foreach ($variables[0] as $key => $variable) {
            if (isset($data[$variable])) {
                $script .= '$' . $variable . ' = "' . $data[$variable] . "\";\r\n";
            }
        }
        $rules = str_replace('SAF', '$SAF', $rules);

        preg_match_all('/FIELD_ID_[\w]*/ius', $rules, $variables); // match all variables inside the condition

        // assign filled values to the variables
        foreach ($variables[0] as $key => $variable) {
            if (isset($data[$variable])) {
                if (is_array($data[$variable])) {
                    $data[$variable] = implode("|", $data[$variable]);
                }
                $script .= '$' . $variable . ' = "' . $data[$variable] . "\";\r\n";
            }
        }
        $rules = str_replace('FIELD', '$FIELD', $rules);

        preg_match_all('/\$SAF_ID_[\w]*[\=]/ius', $rules, $variablesAssign1);
        preg_match_all('/\$FIELD_ID_[\w]*[\=]/ius', $rules, $variablesAssign2);

        $variablesAssign = array_merge($variablesAssign1[0], $variablesAssign2[0]);
        foreach ($variablesAssign as $value) {
            $rules = str_replace($value, '$returnData["' . str_replace(['$', '='], '', $value) . '"]=', $rules);
        }

        $safAgencySubDivisionId = \App\Models\SingleApplication\SingleApplication::SAF_AGENCY_SUBDIVISION_ID;

        //set defines
        $state = !empty($subApplication->applicationCurrentState->state) ? $subApplication->applicationCurrentState->state->current()->state_name : '';
        $companyInfo = Agency::getAgencyInfo();
        $subDivisionRefId = isset($data[$safAgencySubDivisionId]) ? $data[$safAgencySubDivisionId] : null;
        $subDivisionInfo = \App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications::getSubDivisionInfo($subDivisionRefId);

        $userId = Auth::id();
        $script .= "if (!defined('CURRENT_STATE')) { define('CURRENT_STATE', '{$state}'); }";
        $script .= "if (!defined('IS_MANUAL')) { define('IS_MANUAL', '{$subApplication->manual_created}'); }";
        $script .= "if (!defined('SUB_APPLICATION_ID')) { define('SUB_APPLICATION_ID', '{$subApplication->id}'); }";
        $script .= "if (!defined('NEXT_STATE_ID')) { define('NEXT_STATE_ID', '{$stateId}'); }";
        $script .= "if (!defined('USER_ID')) { define('USER_ID', '{$userId}'); }";
        $script .= "if (!defined('COMPANY_INFO')) { define('COMPANY_INFO', '{$companyInfo}'); }";
        $script .= "if (!defined('SAF_TYPE')) { define('SAF_TYPE', '{$subApplication->regime}'); }";
        $script .= "if (!defined('SUBDIVISION_INFO')) { define('SUBDIVISION_INFO', '{$subDivisionInfo}'); }";

        // check is matching the condition
        if (strpos($rules, "setProductValues") !== false) {
            $script .= '$returnData = ' . $rules . ';';
        } else {
            $script .= '$returnData = [];' . $rules . ';';
        }

        $script .= 'return json_encode($returnData);';
        $script = 'try {' . $script . '} catch(\Exception $e) { return false; }';

        $result = false;
        try {
            $result = runScript($script);
        } catch (\ParseError $e) {
            $result = false;
        } catch (\Exception $e) {
            $result = false;
        }

        // return true if the matches the condition, false if not matches or exception message
        return $result;
    }
}

if (!function_exists("getFunctions")) {
    function getFunctions()
    {
        $functions = new \App\SwisRules\AbstractFunctions();

        return $functions->calculationAndObligationFunctions();
    }
}

if (!function_exists("runScript")) {
    function runScript($script)
    {
        return eval(getFunctions() . "\n" . $script);
    }
}

/* ========================================================================================
                                Rules Helper Functions - End
 ======================================================================================== */


/* ========================================================================================
                                Reference Helper Functions - Start
 ======================================================================================== */

if (!function_exists("getReferenceRows")) {
    function getReferenceRows($tableName, $byId = false, $byCode = false, $select = [], $getResult = 'get', $orderBy = false, $orderByType = false, $where = [], $allData = false, $lngId = false)
    {
        if ($byId === '0' || $byId === 0 || $byId === null || $byId === '' || empty($tableName)) {
            return null;
        }

        if ($getResult == '') {
            $getResult = 'get';
        }

        $referenceTableName = "reference_{$tableName}";

        if (substr($tableName, 0, 10) === 'reference_') {
            $referenceTableName = $tableName;
        }

        $referenceTableNameML = $referenceTableName . "_ml";

        if (empty($select)) {
            $select = ["{$referenceTableName}.id", "{$referenceTableName}.code", "{$referenceTableName}.show_status", "{$referenceTableNameML}.name"];
        }

        if ($referenceTableName == ReferenceTable::REFERENCE_HS_CODE_6 && $select != '*') {
            $select[] = $referenceTableNameML . '.path_name';
            $select[] = $referenceTableName . '.unit_of_measurement_1';
            $select[] = $referenceTableName . '.unit_of_measurement_2';
        }

        $language = ($lngId) ? $lngId : cLng('id');

        if ($language == 'en') {
            $language = 1;
        }

        $referenceTable = DB::table($referenceTableName)->select($select)
            ->join($referenceTableNameML, "{$referenceTableNameML}.{$referenceTableName}_id", "=", "{$referenceTableName}.id")
            ->where('lng_id', $language);

        if ($referenceTableName == ReferenceTable::REFERENCE_HS_CODE_6) {
            $referenceTable->whereRaw('LENGTH(code) = 10');
        }

        if ($byId) {

            $data = $referenceTable->where(DB::raw("{$referenceTableName}.id::varchar"), (int)$byId)->first();

        } else {
            if (!$allData) {
                $referenceTable->where($referenceTableName . '.show_status', '1');
            }

            if ($byCode) {

                $data = $referenceTable->where(DB::raw("{$referenceTableName}.code::varchar"), $byCode)->first();

            } else {

                $referenceTable->where($where);

                if ($orderBy) {
                    $referenceTable->orderBy($orderBy, $orderByType ? $orderByType : 'ASC');
                } else {
                    if ($referenceTableName == ReferenceTable::REFERENCE_COUNTRIES || $referenceTableName == ReferenceTable::REFERENCE_TRANSPORT_BORDER_CHECKPOINTS) {
                        $referenceTable->orderBy($referenceTableNameML . '.name', 'ASC');
                    } else {
                        $referenceTable->orderBy($referenceTableName . '.id', 'DESC');
                    }
                }

                $data = $referenceTable->{$getResult}();
            }
        }

        return $data;
    }
}

if (!function_exists("getReferenceRowsKeyValue")) {
    function getReferenceRowsKeyValue($tableName, $keyType = 'id', $onlyActive = false)
    {
        $referenceTableName = "reference_{$tableName}";

        if (substr($tableName, 0, 10) === 'reference_') {
            $referenceTableName = $tableName;
        }

        if ($referenceTableName == 'reference_hs_code_6') {
            $referenceTableName = ReferenceTable::REFERENCE_HS_CODE_6;
        }

        $data = [];
//    if (\Illuminate\Support\Facades\Schema::hasTable($referenceTableName)) {
        $referenceTableNameML = $referenceTableName . "_ml";

        $select = ["{$referenceTableName}.id", "{$referenceTableName}.code", "{$referenceTableName}.show_status", "{$referenceTableNameML}.name"];

        if ($referenceTableName == ReferenceTable::REFERENCE_HS_CODE_6) {
            $select[] = $referenceTableNameML . '.path_name';
        }

        if ($referenceTableName == ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS) {
            $select[] = $referenceTableNameML . '.short_name';
        }

        $referenceTable = \Illuminate\Support\Facades\DB::table($referenceTableName)->select($select)
            ->join($referenceTableNameML, "{$referenceTableNameML}.{$referenceTableName}_id", "=", "{$referenceTableName}.id")
            ->where('lng_id', cLng('id'));

        if ($referenceTableName == ReferenceTable::REFERENCE_HS_CODE_6) {
            $referenceTable->whereRaw('LENGTH(code) = 10');
        }

        if ($onlyActive || $keyType == 'code') {
            $referenceTable->where($referenceTableName . '.show_status', '1');
        }

        $referenceTable = $referenceTable->orderBy('id')->get();

        if ($referenceTable->count()) {
            foreach ($referenceTable as $value) {
                if (isset($value->{$keyType})) {
                    $data[$value->{$keyType}] = [
                        'id' => $value->id,
                        'name' => $value->name,
                        'code' => $value->code,
                        'show_status' => $value->show_status
                    ];

                    if ($referenceTableName == ReferenceTable::REFERENCE_HS_CODE_6) {
                        $data[$value->{$keyType}]['path_name'] = $value->path_name;
                    }

                    if ($referenceTableName == ReferenceTable::REFERENCE_AGENCY_SUBDIVISIONS) {
                        $data[$value->{$keyType}]['short_name'] = $value->short_name;
                    }

                }
            }
        }
//    }

        return $data;
    }
}

if (!function_exists("getReferenceItemLastActiveRow")) {
    function getReferenceItemLastActiveRow($tableName, $refId)
    {
        $referenceTableName = "reference_{$tableName}";

        if (substr($tableName, 0, 10) === 'reference_') {
            $referenceTableName = $tableName;
        }

        if (!is_null($refId)) {

            $refData = getReferenceRows($referenceTableName, $refId);

            if ($refData->show_status == ReferenceTable::STATUS_ACTIVE) {
                return $refData;
            }

            $refDataByCode = getReferenceRows($tableName, false, $refData->code);

            if (!is_null($refDataByCode)) {
                return $refDataByCode;
            }

            return $refData;
        }

        return null;
    }
}

if (!function_exists("getReferenceRowsWhereIn")) {
    function getReferenceRowsWhereIn($tableName, $byIds = [], $byCodes = [], $select = [], $getActives = true, $orderBy = false, $orderByType = false)
    {
        $referenceTableName = "reference_{$tableName}";

        if (substr($tableName, 0, 10) === 'reference_') {
            $referenceTableName = $tableName;
        }

//    if (\Illuminate\Support\Facades\Schema::hasTable($referenceTableName)) {
        $referenceTableNameML = $referenceTableName . "_ml";

        if (empty($select)) {
            $select = ["{$referenceTableName}.id", "{$referenceTableName}.code", "{$referenceTableNameML}.name"];
        }

        $referenceTable = \Illuminate\Support\Facades\DB::table($referenceTableName)->select($select)
            ->join($referenceTableNameML, "{$referenceTableNameML}.{$referenceTableName}_id", "=", "{$referenceTableName}.id")
            ->where('lng_id', cLng('id'));

        if ($byIds) {
            $referenceTable->whereIn('id', $byIds);
        }

        if ($byCodes) {
            $referenceTable->whereIn('code', $byCodes);
        }

        if ($getActives) {
            $referenceTable->where($referenceTableName . '.show_status', ReferenceTable::STATUS_ACTIVE);
        }

        if ($orderBy) {
            $referenceTable->orderBy($orderBy, $orderByType ? $orderByType : 'ASC');
        } else {
            $referenceTable->orderBy($referenceTableName . '.id', 'DESC');
        }

        return $referenceTable->get();
    }
}

if (!function_exists("getReferenceRowIdByCode")) {
    function getReferenceRowIdByCode($tableName, $code)
    {
        $referenceTableName = "reference_{$tableName}";

        if (substr($tableName, 0, 10) === 'reference_') {
            $referenceTableName = $tableName;
        }

        return DB::table($referenceTableName)->where('code', $code)->where('show_status', '1')->pluck('id')->first();
    }
}

if (!function_exists("getReferenceTableColumnLabel")) {
    function getReferenceTableColumnLabel($field, $table)
    {
        $arr = ['name', 'code', 'start_date', 'end_date'];

        if (in_array($field, $arr)) {
            return trans('swis.ref_table_form.' . $field . '.label');
        }

        return trans('swis.ref_table_form.' . $table . '.' . $field . '.label');
    }
}

if (!function_exists("getReferenceAllRowsById")) {
    function getReferenceAllRowsById($tableName, $id)
    {
        $referenceTableName = "reference_{$tableName}";

        if (substr($tableName, 0, 10) === 'reference_') {
            $referenceTableName = $tableName;
        }

        $byIdGetCode = DB::table($referenceTableName)->where('id', $id)->pluck('code')->first();

        if ($byIdGetCode) {
            return DB::table($referenceTableName)->select('id', 'code')->where('code', $byIdGetCode)->get();
        }

        return collect();
    }
}

if (!function_exists("getReferenceLastRowByCode")) {
    function getReferenceLastRowByCode($tableName, $code)
    {
        $referenceTableName = "reference_{$tableName}";

        if (substr($tableName, 0, 10) === 'reference_') {
            $referenceTableName = $tableName;
        }

        $referenceTableNameML = $referenceTableName . "_ml";

        return \Illuminate\Support\Facades\DB::table($referenceTableName)->select(['id', 'code', 'name', $referenceTableName . '.show_status'])
            ->join($referenceTableNameML, "{$referenceTableNameML}.{$referenceTableName}_id", "=", "{$referenceTableName}.id")
            ->where('lng_id', cLng('id'))
            ->where('code', $code)
            ->orderBy('id', 'DESC')
            ->first();
    }
}

/* ========================================================================================
                                Reference Helper Functions - End
 ======================================================================================== */


/* ========================================================================================
                                String Helper Functions - Start
 ======================================================================================== */

if (!function_exists("replaceToLatinCharacters")) {
    function replaceToLatinCharacters($value)
    {
        $notAlpha = array("№", "а", "А", "С", "О", "Е", "К", "Х", "Р", "Н");
        $changedAlpha = array("", "A", "A", "C", "O", "E", "K", "X", "P", "H");

        $value = str_replace($notAlpha, $changedAlpha, $value);

        return $value;
    }
}

if (!function_exists("generateID")) {
    function generateID($length = 16)
    {
        $str = "";
        $characters = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }
}

if (!function_exists("randomNumberDigits")) {
    function randomNumberDigits($length = 10)
    {
        $result = 1;
        for ($i = 1; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }

        return $result;
    }
}

if (!function_exists("prefixKey")) {
    function prefixKey($prefix, $array, $delimiter = '.')
    {
        $result = [];
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $result = array_merge($result, prefixKey($prefix . $key . $delimiter, $value, $delimiter));
            } else {
                $result[$prefix . $key] = $value;
            }
        }
        return $result;
    }
}

if (!function_exists("cutString")) {
    function cutString($string, $length = 40, $addDote = true)
    {
        $newString = mb_substr($string, 0, $length, 'UTF-8');

        $newString = trim($newString);
        if ($addDote && mb_strlen($string) > $length) {
            $newString .= '...';
        }

        return $newString;
    }
}

if (!function_exists("getStringsBetween")) {
    function getStringsBetween($str, $startDelimiter, $endDelimiter)
    {
        $contents = array();
        $startDelimiterLength = strlen($startDelimiter);
        $endDelimiterLength = strlen($endDelimiter);
        $startFrom = $contentStart = $contentEnd = 0;
        while (false !== ($contentStart = strpos($str, $startDelimiter, $startFrom))) {
            $contentStart += $startDelimiterLength;
            $contentEnd = strpos($str, $endDelimiter, $contentStart);
            if (false === $contentEnd) {
                break;
            }
            $contents[] = substr($str, $contentStart, $contentEnd - $contentStart);
            $startFrom = $contentEnd + $endDelimiterLength;
        }

        return $contents;
    }
}
/* ========================================================================================
                                String Helper Functions - End
 ======================================================================================== */


/* ========================================================================================
                                Array Helper Functions - Start
 ======================================================================================== */

if (!function_exists("strReplaceAssoc")) {
    function strReplaceAssoc(array $replace, $string)
    {
        return str_replace(array_keys($replace), array_values($replace), $string);
    }
}

if (!function_exists("recursiveRenderLog")) {
    function recursiveRenderLog($oldData, $newData, $drawFirst = true, $drawSecond = true)
    {
        $html = '';

        foreach ($oldData as $key => $value) {

            if ($drawFirst && array_key_exists($key, $newData) && $value == $newData[$key]) {
                continue;
            } elseif ($drawFirst && !array_key_exists($key, $newData)) {
                continue;
            }

            if ($drawSecond && array_key_exists($key, $newData) && $value == $newData[$key]) {
                continue;
            } elseif ($drawSecond && !array_key_exists($key, $newData)) {
                continue;
            }


            $drawArrayDiff = false;
            $oldSpecialFieldNames = $newSpecialFieldNames = [];
            if (array_key_exists($key, $oldData) && array_key_exists($key, $newData) && is_array($oldData[$key]) && is_array($newData[$key])) {
                if ($key === 'agencies') {
                    $drawArrayDiff = true;
                    $oldSpecialFieldNames = Agency::select('id', DB::raw("CONCAT('(', tax_id , ') ', name) AS name"))->joinMl()->whereIn('id', array_column($oldData[$key], 'agency_id'))->pluck('name', 'id')->all();
                    $newSpecialFieldNames = Agency::select('id', DB::raw("CONCAT('(', tax_id , ') ', name) AS name"))->joinMl()->whereIn('id', array_column($newData[$key], 'agency_id'))->pluck('name', 'id')->all();
                } elseif ($key === 'structure') {
                    $drawArrayDiff = true;
                }

            }

            $html .= "<tr><td><strong>{$key}</strong></td>";

            if ($drawFirst) {
                $html .= "<td>";
                if (is_array($value)) {
                    $html .= "<table class='table table-normal table-bordered table-condensed' style='width: 100%;'><tbody>";
                    if (array_key_exists($key, $oldData) && $drawArrayDiff) {
                        foreach ($oldData[$key] ?? [] as $itemKey => $item) {
                            if ($key === 'agencies') {
                                if (isset($oldSpecialFieldNames[$item['agency_id']])) {
                                    $html .= "<tr><td>" . $oldSpecialFieldNames[$item['agency_id']] . "</td></tr>";
                                }
                            } elseif ($key === 'structure') {
                                if (isset($newData[$key], $newData[$key][$itemKey]) && is_array($newData[$key]) && is_array($item) && is_array($newData[$key][$itemKey])) {
                                    if (!strcmp(json_encode($item), json_encode($newData[$key][$itemKey]))) {
                                        continue;
                                    }
                                }

                                $html .= drawStructureChanges($item);

                            }
                        }
                    } elseif (array_key_exists($key, $newData)) {
                        $html .= recursiveRenderLog($value, $newData[$key], true, false);
                    }
                    $html .= "</tbody></table>";
                } else {
                    $html .= $value;
                }
                $html .= "</td>";
            }

            if ($drawSecond) {
                if (array_key_exists($key, $newData)) {
                    if (is_array($newData[$key])) {
                        $html .= "<td>";
                        $html .= "<table class='table table-normal table-bordered table-condensed'><tbody>";

                        if (array_key_exists($key, $newData) && $drawArrayDiff) {
                            foreach ($newData[$key] ?? [] as $itemKey => $item) {

                                if ($key === 'agencies') {
                                    if (isset($newSpecialFieldNames[$item['agency_id']])) {
                                        $html .= "<tr><td>" . $newSpecialFieldNames[$item['agency_id']] . "</td></tr>";
                                    }
                                } elseif ($key === 'structure') {
                                    if (isset($oldData[$key], $oldData[$key][$itemKey]) && is_array($oldData[$key]) && is_array($item) && is_array($oldData[$key][$itemKey])) {
                                        if (!strcmp(json_encode($item), json_encode($oldData[$key][$itemKey]))) {
                                            continue;
                                        }
                                    }

                                    $html .= drawStructureChanges($item);

                                } else {
//                                    $html .= "Jujubulas First {$item['id']} <br/>";
                                }
                            }
                        } elseif (array_key_exists($key, $newData)) {
                            $html .= recursiveRenderLog($value, $newData[$key], false, true);
                        }
                        $html .= "</tbody></table>";
                    } else {
                        $html .= $value == $newData[$key] ? "<td>" . $newData[$key] : "<td class='changed'>" . $newData[$key];
                    }
                } else {
                    $html .= "<td>";
                }
            }

            $html .= "</td></tr>";

        }

        return $html;
    }

    /**
     * Function to draw structure changes
     *
     * @param $item
     * @return string
     */
    function drawStructureChanges($item)
    {
        $html = '';
        $html .= "<tr><td>" . $item['field_name'] . "</td></tr>";
        $html .= "<tr><td><table class='table table-normal table-bordered table-condensed'><tbody>";

        if ($item['type'] == \App\Models\ReferenceTable\ReferenceTableStructure::COLUMN_TYPE_DATE) {
            $html .= "<tr><td>" . trans('swis.reference_table.start.label') . "</td><td>{$item['parameters']['start']}</td></tr>";
            $html .= "<tr><td>" . trans('swis.reference_table.end.label') . "</td><td>{$item['parameters']['end']}</td></tr>";
        } elseif ($item['type'] == \App\Models\ReferenceTable\ReferenceTableStructure::COLUMN_TYPE_NUMBER || $item['type'] == \App\Models\ReferenceTable\ReferenceTableStructure::COLUMN_TYPE_TEXT) {
            $html .= "<tr><td>" . trans('swis.reference_table.min.label') . "</td><td>{$item['parameters']['min']}</td></tr>";
            $html .= "<tr><td>" . trans('swis.reference_table.max.label') . "</td><td>{$item['parameters']['max']}</td></tr>";
        } elseif ($item['type'] == \App\Models\ReferenceTable\ReferenceTableStructure::COLUMN_TYPE_SELECT || $item['type'] == \App\Models\ReferenceTable\ReferenceTableStructure::COLUMN_TYPE_AUTOCOMPLETE) {
            $html .= "<tr><td>" . trans('swis.reference_table.source.label') . "</td><td>{$item['parameters']['source']}</td></tr>";
        }
        $html .= "</tbody></table></td></tr>";

        return $html;
    }
}

if (!function_exists("arrayKeyExistsRecursive")) {
    function arrayKeyExistsRecursive($key, $array)
    {
        if (array_key_exists($key, $array)) {
            return true;
        }
        foreach ($array as $k => $value) {
            if (is_array($value) && arrayKeyExistsRecursive($key, $value)) {
                return true;
            }
        }
        return false;
    }
}

if (!function_exists("uniqueMultiDimensionArray")) {
    function uniqueMultiDimensionArray($my_array, $key)
    {
        $result = array();
        $i = 0;
        $key_array = array();

        foreach ($my_array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $result[$i] = $val;
            }
            $i++;
        }
        return $result;
    }
}

if (!function_exists("arrayMergeRecursiveDeep")) {
    function arrayMergeRecursiveDeep(array $array1, array $array2)
    {
        $merged = $array1;

        foreach ($array2 as $key => & $value) {
            if (is_array($value) && isset($merged[$key]) && is_array($merged[$key])) {
                $merged[$key] = arrayMergeRecursiveDeep($merged[$key], $value);
            } else if (is_numeric($key)) {
                if (!in_array($value, $merged)) {
                    $merged[] = $value;
                }
            } else {
                $merged[$key] = $value;
            }
        }

        return $merged;
    }
}

/* ========================================================================================
                                Array Helper Functions - End
 ======================================================================================== */


/* ========================================================================================
                                My Application Helper Functions - Start
 ======================================================================================== */

if (!function_exists("getUserDocuments")) {
    function getUserDocuments($datasetFromMdm, $mdmFields)
    {
        $fields = [];

        foreach ($datasetFromMdm as $key => $value) {

            preg_match("/tab\[@key='([^']+)'?/us", $value->xpath, $tab);
            preg_match("/\/block\[@name='([^']+)'?/ius", $value->xpath, $block);
            preg_match("/typeBlock\[@name='([^']+)'?/ius", $value->xpath, $typeBlock);

            $fields[] = [
                'dataSetMdmId' => $value->id,
                'field_id' => $value->field_id,
                'name' => $mdmFields->get($value->mdm_field_id)->name,
                'mdm_type' => $mdmFields->get($value->mdm_field_id)->type,
                'definition' => $mdmFields->get($value->mdm_field_id)->definition,
                'interface_format' => $mdmFields->get($value->mdm_field_id)->interface_format,
                'description' => $mdmFields->get($value->mdm_field_id)->description,
                'mdm_field_id' => $value->mdm_field_id,
                'validation' => getValidationRulesFromFormat($mdmFields->get($value->mdm_field_id)->interface_format, $mdmFields->get($value->mdm_field_id)->pattern, false, $mdmFields->get($value->mdm_field_id)),
                'xpath' => $value->xpath,
                'tab' => $tab[1] ?? '',
                'block' => $block[1] ?? '',
                'typeBlock' => $typeBlock[1] ?? '',
                'max_value' => $mdmFields->get($value->mdm_field_id)->client_max,
                'tooltip' => $value->tooltip,
                'label' => $value->label,
            ];
        }

        return $fields;
    }
}

if (!function_exists("getValidationRulesFromFormat")) {
    function getValidationRulesFromFormat($format, $pattern, $required, $mdmField)
    {
        $rule = '';
        if ($mdmField->type != DataModel::MDM_ELEMENT_TYPE_LINE && $mdmField->type != DataModel::MDM_ELEMENT_TYPE_MULTIPLE) {
            preg_match('/^([a,n]{1,2})([\.]{0,3})([\d]+)([\,]([\d]+))?$/', $format, $match);
            /*
             * 1 - type
             * 2 - between
             * 3 - length
             * 5 - floating point
             */
            if (empty($match)) {
                dd('column interface format is empty -- ' . $mdmField); //@fixme
            }

            $required = $required == true ? 'required|' : 'nullable|';

            $validationType = '';

            if ($match[1] == 'a') {
                $validationType = 'mdm_alpha';
            } elseif ($match[1] == 'an') {
                $validationType = 'mdm_alphanumeric';
            } elseif ($match[1] == 'n') {
                $validationType = 'mdm_numeric';
            }

            $between = false;
            if (isset($match[2]) && $match[2] == '..') {
                $between = true;
            }

            $length = (!empty($match[3])) ? $match[3] : 0;

            if (!empty($match[5])) {
                $validationType = 'double';
            }

            $rule = '';

            switch ($validationType) {
                case 'string':
                    $rule = $required . 'string';
                    if ($between) {
                        $rule .= '|max:' . $length;
                    } elseif ($length > 0) {
                        $rule .= '|size:' . $length;
                    }
                    break;
                case 'integer':
                    $rule = $required . 'integer';
                    if ($between) {
                        $rule .= '|digits_between:0,' . $length;
                    } elseif ($length > 0) {
                        $rule = $rule . 'digits:' . $length;
                    }
                    break;
                case 'double':
                    $rule = $rule . $required . 'regex:/^[+-]?[0-9]{1,' . $match[3] . '}(\.[0-9]{1,' . $match[5] . '})?$/';
                    break;
                case 'mdm_alpha':
                    $rule = $required . 'mdm_alpha';
                    if ($between) {
                        $rule .= '|max:' . $length;
                    } elseif ($length > 0) {
                        $rule .= '|size:' . $length;
                    }
                    break;
                case 'mdm_alphanumeric':
                    $rule = $required . 'mdm_alphanumeric';
                    if ($between) {
                        $rule .= '|max:' . $length;
                    } elseif ($length > 0) {
                        $rule .= '|size:' . $length;
                    }
                    break;
                case 'mdm_numeric':
                    $rule = $required . 'mdm_numeric';
                    if ($between) {
                        $rule .= '|numeric';
                    } elseif ($length > 0) {
                        $rule = $rule . 'digits:' . $length;
                    }
                    break;
            }

            if (!is_null($pattern)) {
                $regex = '|regex:' . $pattern;
                $rule = $rule . $regex;
            }
        }

        return $rule;
    }
}

if (!function_exists("getJsonDataAsArray")) {
    function getJsonDataAsArray($data, $customData, $safProducts, $safControlledData = [])
    {
        $getSaf = [];
        $returnArray = [];
        $getSafIdNameArray = getSafIdNameArray();

        foreach ($data as $tab => $tabValues) {
            foreach ($tabValues as $field => $value) {
                if (is_array($value)) {
                    foreach ($value as $key => $item) {
                        if (is_array($item)) {
                            $i = 1;
                            foreach ($item as $multipleFieldKey => $multipleFieldValue) {

                                if (is_array($multipleFieldValue)) {
                                    if ($key == "products") {
                                        foreach ($multipleFieldValue as $productSectionKey => $productSectionValue) {
                                            $getSaf["saf[{$field}][{$key}][{$multipleFieldValue['id']}][{$productSectionKey}]"] = $productSectionValue;
                                        }
                                    }

                                } else {
                                    $getSaf["saf[{$field}][{$key}][{$multipleFieldKey}]"] = $multipleFieldValue;
                                }
                            }
                            $i++;
                        } else {
                            $getSaf["saf[{$field}][{$key}]"] = $item;
                        }
                    }
                } else {
                    $getSaf["saf[{$field}]"] = $value;
                }
            }
        }

        if (!empty($safControlledData)) {
            foreach ($safControlledData as $tab => $tabValues) {
                if (is_array($tabValues)) {
                    foreach ($tabValues as $field => $value) {

                        if (is_array($value)) {
                            foreach ($value as $key => $item) {
                                if (is_array($item)) {
                                    foreach ($item as $multipleFieldKey => $multipleFieldValue) {
                                        if (is_array($multipleFieldValue)) {
                                            if ($key == "products") {
                                                foreach ($multipleFieldValue as $productSectionKey => $productSectionValue) {
                                                    $getSaf["saf_controlled_fields[{$field}][{$key}][{$multipleFieldValue['id']}][{$productSectionKey}]"] = $productSectionValue;
                                                }
                                            }

                                        } else {
                                            $getSaf["saf_controlled_fields[{$field}][{$key}][{$multipleFieldKey}]"] = $multipleFieldValue;
                                        }
                                    }
                                } else {
                                    $getSaf["saf_controlled_fields[{$field}][{$key}]"] = $item;
                                }
                            }
                        } else {
                            $getSaf["saf_controlled_fields[{$tab}][{$field}]"] = $value;
                        }
                    }
                }
            }
        }

        $getSafProductsIdNameArray = getSafProductsIdNameArray($safProducts);

        foreach ($getSaf as $field => $item) {
            if (strpos($field, "saf[product][products]") !== false) {
                if (array_key_exists($field, $getSafProductsIdNameArray)) {
                    $key = $getSafProductsIdNameArray[$field];
                    $returnArray[$key] = $item;
                }
            } else {
                $key = array_search($field, $getSafIdNameArray);
                $returnArray[$key] = $item;
            }
        }

        // custom data we are get always as array (Laravel casts method)
        if (isset($customData)) {
            foreach ($customData as $key => $item) {
                $returnArray["FIELD_ID_{$key}"] = $item;
            }
        }
        return $returnArray;
    }
}

if (!function_exists("getSafProductsIdNameArray")) {
    function getSafProductsIdNameArray($safProducts = [])
    {
        $returnArray = [];
        $lastXML = SingleApplicationDataStructure::lastXml();
        $elements = $lastXML->xpath("//*[@name='swis.single_app.products_list']");

        if (isset($elements[0])) {
            foreach ($elements[0]->fieldset as $element) {
                if (count($element->field) <= 1) {
                    $fieldId = (string)$element->field->attributes()->id;
                    $name = (string)$element->field->attributes()->name;
                    if ($name) {
                        if (!empty($safProducts)) {
                            foreach ($safProducts as $product) {
                                $nameArrayKey = "saf[product][products][{$product['id']}][{$name}]";
                                $returnArray[$nameArrayKey] = $fieldId;
                            }
                        } else {
                            $returnArray[$fieldId] = $name;
                        }
                    }
                } else {
                    foreach ($element->field as $field) {
                        $fieldId = (string)$field->attributes()->id;
                        $name = (string)$field->attributes()->name;
                        if ($name) {
                            if (!empty($safProducts)) {
                                foreach ($safProducts as $product) {
                                    $nameArrayKey = "saf[product][products][{$product['id']}][{$name}]";
                                    $returnArray[$nameArrayKey] = $fieldId;
                                }
                            } else {
                                $returnArray[$fieldId] = $name;
                            }
                        }
                    }
                }
            }
        }
        return $returnArray;
    }
}

if (!function_exists("getSafFieldCorrectName")) {
    function getSafFieldCorrectName($field, $isDate = false)
    {
        $field = str_replace(']', '', $field);
        return str_replace('[', '.', $field);
    }
}

if (!function_exists("getSafIdNameArray")) {
    function getSafIdNameArray($withPoints = false)
    {
        $returnArray = [];
        $lastXML = SingleApplicationDataStructure::lastXml();
        $elements = $lastXML->xpath("//field");
        foreach ($elements as $element) {
            $name = (string)$element->attributes()->name;
            if ($name) {
                if ($withPoints) {
                    $name = str_replace('[', '.', str_replace(']', '', str_replace('][', '.', $name)));
                    $returnArray[(string)$element->attributes()->id] = $name;
                } else {
                    $returnArray[(string)$element->attributes()->id] = $name;
                }
            }
        }
        return $returnArray;
    }
}

if (!function_exists("getSafControlFieldIdNameArray")) {
    function getSafControlFieldIdNameArray()
    {
        $returnArray = [];
        $lastXML = SingleApplicationDataStructure::lastXml();
        $elements = $lastXML->xpath("//field[@safControlledField='true']");
        foreach ($elements as $element) {
            $name = (string)$element->attributes()->name;
            if ($name) {
                $returnArray[(string)$element->attributes()->id] = $name;
            }
        }
        return $returnArray;
    }
}

if (!function_exists("getSafFieldById")) {
    function getSafFieldById($xml, $fieldId)
    {
        return $xml->xpath("//*[@id='{$fieldId}']");
    }
}

if (!function_exists("listControlled")) {
    function listControlled($fieldID, $globalHiddenFields, $lists, $type, $mandatoryListForView = [])
    {
        $data = [
            'requiredField' => '',
            'disabled' => 'readonly',
            'continue' => false,
        ];

        if (isset($globalHiddenFields, $globalHiddenFields[$type]) && in_array($fieldID, $globalHiddenFields[$type])) {
            $data['continue'] = true;
            return $data;
        } else {
            $checkedInList = (isset($lists) && array_key_exists($fieldID, $lists));
            if ($checkedInList && in_array("non_visible", $lists[$fieldID]) && !in_array("editable", $lists[$fieldID])) {
                $data['continue'] = true;
                return $data;
            } else {
                if ($checkedInList && isset($mandatoryListForView[$fieldID])) {
                    $data['requiredField'] = 'required';
                }
                if ($checkedInList && in_array("editable", $lists[$fieldID])) {
                    $data['disabled'] = '';
                }
            }
        }

        return $data;
    }
}

if (!function_exists('getFieldsValueByMDMID')) {
    function getFieldsValueByMDMID($documentId, $customData, $lngId = false, $refColumn = 'name')
    {
        $mdmValues = DocumentsDatasetFromMdm::select('field_id', 'mdm_field_id')->where('document_id', $documentId)->get();
        $fields = [];

        $dataModelAllData = DataModel::allData();
        foreach ($mdmValues as $mdmValue) {
            $fields[$mdmValue['mdm_field_id']] = $mdmValue['field_id'] ? isset($mdmValue['field_id'], $customData[$mdmValue['field_id']]) ? $customData[$mdmValue['field_id']] : '' : '';
            if (!empty($fields[$mdmValue['mdm_field_id']])) {
//                $mdmField = DataModel::select('id','type', 'reference')->where('id', $mdmValue['mdm_field_id'])->firstOrFail();
                $mdmField = (object)$dataModelAllData[$mdmValue['mdm_field_id']];

                switch ($mdmField->type) {
                    case DataModel::MDM_ELEMENT_TYPE_MULTIPLE:
                        if (!empty($mdmField->reference)) {
                            foreach ($fields[$mdmValue['mdm_field_id']] as $key => $referenceRowId) {
                                $fields[$mdmValue['mdm_field_id']][$key] = optional(getReferenceRows(ReferenceTable::REFERENCE_TABLE_PREFIX . $mdmField->reference, $referenceRowId, false, [$refColumn], 'get', false, false, [], false, $lngId))->$refColumn;
                            }
                        }
                        $fields[$mdmValue['mdm_field_id']] = implode(', ', $fields[$mdmValue['mdm_field_id']]);
                        break;
                    case DataModel::MDM_ELEMENT_TYPE_DATE:
                        $fields[$mdmValue['mdm_field_id']] = formattedDate($fields[$mdmValue['mdm_field_id']]);
                        break;
                    case DataModel::MDM_ELEMENT_TYPE_DATE_TIME:
                        $fields[$mdmValue['mdm_field_id']] = formattedDate($fields[$mdmValue['mdm_field_id']], true);
                        break;
                    default:
                        if (!empty($mdmField->reference)) {
                            $fields[$mdmValue['mdm_field_id']] = optional(getReferenceRows(ReferenceTable::REFERENCE_TABLE_PREFIX . $mdmField->reference, $fields[$mdmValue['mdm_field_id']], false, [$refColumn], 'get', false, false, [], false, $lngId))->$refColumn;
                        }
                        break;
                }

            }
        }

        return $fields;
    }
}

/* ========================================================================================
                                My Application Helper Functions - End
 ======================================================================================== */


/* ========================================================================================
                                SAF Helper Functions - Start
 ======================================================================================== */

if (!function_exists("getSafAttrPath")) {
    function getSafAttrPath($path)
    {
        $paths = explode('/', $path);
        foreach ($paths as &$item) {
            $item = trans(trim($item));
        }
        return implode(' / ', $paths);
    }
}

if (!function_exists("getSaFXmlFieldModuleName")) {
    function getSaFXmlFieldModuleName(string $field, $lastXML = null)
    {
        $lastXML = isset($lastXML) ? $lastXML : SingleApplicationDataStructure::lastXml();
        $parentBlock = $lastXML->xpath("//*[@id='{$field}']/ancestor::block");
        $parentBlock = isset($parentBlock[0]) ? $parentBlock[0] : null;
        $module = 'default';
        if ($parentBlock) {
            if (!empty($parentBlock->attributes()) && !empty($parentBlock->attributes()->module)) {
                $module = (string)$parentBlock->attributes()->module;
            } else {
                if (isset($parentBlock->subBlock)) {
                    if (!empty($parentBlock->subBlock->attributes()) && !empty($parentBlock->subBlock->attributes()->module)) {
                        $module = (string)$parentBlock->subBlock->attributes()->module;
                    }
                }
            }
        }
        return $module;
    }
}

if (!function_exists("getSafProductsBatchesIdNameArray")) {
    function getSafProductsBatchesIdNameArray()
    {
        $lastXML = SingleApplicationDataStructure::lastXml();
        $elements = $lastXML->xpath("//*[@name='swis.single_app.batches']");

        $returnArray = [];
        if (isset($elements[0])) {
            foreach ($elements[0]->fieldset as $element) {
                if ((string)$element->field->attributes()->name) {
                    $returnArray[(string)$element->field->attributes()->id] = (string)$element->field->attributes()->dataName;
                }
            }
        }

        return $returnArray;
    }
}

if (!function_exists("getSafXmlFieldByID")) {
    function getSafXmlFieldByID($id, $lastXML = false)
    {
        if (!$lastXML) {
            $lastXML = SingleApplicationDataStructure::lastXml();
        }

        return $lastXML->xpath("//field[@id='" . $id . "']");
    }
}

if (!function_exists("getSafXmlControlledFields")) {
    function getSafXmlControlledFields()
    {
        $lastXML = SingleApplicationDataStructure::lastXml();

        $safFields = $lastXML->xpath("//field[@safControlledField='true']");

        $safControlledFields = [];
        foreach ($safFields as $safField) {

            $safControlledFields[(string)$safField->attributes()->id] = (object)[
                'id' => (string)$safField->attributes()->id,
                'name' => (string)$safField->attributes()->name,
                'title' => (string)$safField->attributes()->title,
                'hideOnSaf' => (string)$safField->attributes()->hideOnSaf,
                'mdm' => (string)$safField->attributes()->mdm,
            ];
        }

        ksort($safControlledFields, SORT_NATURAL);

        return $safControlledFields;
    }
}

if (!function_exists("libxml_display_error")) {
    function libxml_display_error($error)
    {
        $return = "<br/>\n";
        switch ($error->level) {
            case LIBXML_ERR_WARNING:
                $return .= "<b>Warning $error->code</b>: ";
                break;
            case LIBXML_ERR_ERROR:
                $return .= "<b>Error $error->code</b>: ";
                break;
            case LIBXML_ERR_FATAL:
                $return .= "<b>Fatal Error $error->code</b>: ";
                break;
        }
        $return .= trim($error->message);
        if ($error->file) {
            $return .= " in <b>$error->file</b>";
        }
        $return .= " on line <b>$error->line</b>\n";

        return $return;
    }
}

if (!function_exists("libxml_display_errors")) {
    function libxml_display_errors()
    {
        $errorsList = [];
        $errors = libxml_get_errors();
        foreach ($errors as $error) {
            $errorsList[] = libxml_display_error($error);
        }
        libxml_clear_errors();
        return $errorsList;
    }
}

/* ========================================================================================
                                SAF Helper Functions - End
 ======================================================================================== */