<?php

namespace App\SwisRules;

/**
 * Class ValidationRules
 * @package App\SwisRules
 */
class ValidationRules extends AbstractFunctions
{
    /**
     * @var string
     */
    protected $script = '';
    protected $data = '';
    protected $condition = '';
    protected $rule = '';
    protected $type = '';
    protected $subApplication = '';
    protected $stateId = '';

    /**
     * ValidationRules constructor.
     *
     * @param $data
     * @param $condition
     * @param $rule
     * @param string $type
     * @param $subApplication
     * @param $stateId
     */
    public function __construct($data, $condition, $rule, $type = 'saf', $subApplication = null, $stateId = null)
    {
        $this->data = $data;
        $this->condition = $condition;
        $this->rule = $rule;
        $this->type = $type;
        $this->subApplication = $subApplication;
        $this->stateId = $stateId;
    }

    /**
     * @return bool|mixed
     */
    public function checkCondition()
    {
        $this->script = $this->getConditionRules($this->data, $this->condition, $this->type, $this->subApplication);

        return $this->run($this->calculationAndObligationFunctions());
    }

    /**
     * @return bool|mixed
     */
    public function matchRules()
    {
        $this->script = $this->getValidationMatchRules($this->data, $this->rule);

        return $this->run($this->validationFunctions());
    }

    /**
     * @param $functions
     * @return bool|mixed
     */
    private function run($functions)
    {
        try {
            return eval($functions . "\n" . $this->script);
        } catch (\ParseError $e) {
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }
}