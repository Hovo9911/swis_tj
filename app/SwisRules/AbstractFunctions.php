<?php

namespace App\SwisRules;

use App\Models\Agency\Agency;
use App\Models\SingleApplication\SingleApplication;
use Illuminate\Support\Facades\Auth;

class AbstractFunctions
{
    /**
     * Function To Get Functions for Calculation and Obligation Rules
     */
    public function calculationAndObligationFunctions()
    {
        $filePath = app_path('Rules/functions/calculation-obligation-functions.php');

        return 'require_once("' . $filePath . '");
        ';
    }

    /**
     * Function To Get Functions for Validation Rules
     */
    protected function validationFunctions()
    {
        $filePath = app_path('Rules/functions/validation-functions.php');

        return 'require_once("' . $filePath . '");
        ';
    }

    /**
     * Function to get Calculation match rules
     *
     * @param $data
     * @param $rules
     * @param $subApplication
     * @param $stateId
     * @return string
     */
    protected function getCalculationMatchRules($data, $rules, $subApplication, $stateId)
    {
        $variables = $this->getVariables('/SAF_ID_[\w]*/ius', $rules);
        $script = $this->generateMatchScriptString($data, $variables);
        $rules = str_replace('SAF', '$SAF', $rules);
        $safAgencySubDivisionId = SingleApplication::SAF_AGENCY_SUBDIVISION_ID;

        $variables = $this->getVariables('/FIELD_ID_[\w]*/ius', $rules);
        $script = $this->generateMatchScriptString($data, $variables, $script);
        $rules = str_replace('FIELD', '$FIELD', $rules);

        preg_match_all('/\$SAF_ID_[\w]*[\=]/ius', $rules, $variablesAssignSaf);
        preg_match_all('/\$FIELD_ID_[\w]*[\=]/ius', $rules, $variablesAssignMdm);

        $variablesAssign = array_merge($variablesAssignSaf[0], $variablesAssignMdm[0]);
        foreach ($variablesAssign as $value) {
            $rules = str_replace($value, '$returnData["' . str_replace(['$', '='], '', $value) . '"]=', $rules);
        }

        $state = !empty($subApplication->applicationCurrentState->state) ? $subApplication->applicationCurrentState->state->current()->state_name : '';
        $companyInfo = Agency::getAgencyInfo();
        $subDivisionRefId = isset($data[$safAgencySubDivisionId]) ? $data[$safAgencySubDivisionId] : null;
        $subDivisionInfo = \App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications::getSubDivisionInfo($subDivisionRefId);

        $userId = Auth::id();
        $script .= "if (!defined('CURRENT_STATE')) { define('CURRENT_STATE', '{$state}'); }";
        $script .= "if (!defined('IS_MANUAL')) { define('IS_MANUAL', '{$subApplication->manual_created}'); }";
        $script .= "if (!defined('SUB_APPLICATION_ID')) { define('SUB_APPLICATION_ID', '{$subApplication->id}'); }";
        $script .= "if (!defined('NEXT_STATE_ID')) { define('NEXT_STATE_ID', '{$stateId}'); }";
        $script .= "if (!defined('USER_ID')) { define('USER_ID', '{$userId}'); }";
        $script .= "if (!defined('COMPANY_INFO')) { define('COMPANY_INFO', '{$companyInfo}'); }";
        $script .= "if (!defined('SAF_TYPE')) { define('SAF_TYPE', '{$subApplication->regime}'); }";
        $script .= "if (!defined('SUBDIVISION_INFO')) { define('SUBDIVISION_INFO', '{$subDivisionInfo}'); }";

        // check is matching the condition
        $script .= '$returnData = [];' . $rules . ';';
        $script .= 'return json_encode($returnData);';
        return 'try {' . $script . '} catch(\Exception $e) { return false; }';
    }

    /**
     * Function to get validation match rules
     *
     * @param $data
     * @param $rules
     * @return string
     */
    protected function getValidationMatchRules($data, $rules)
    {
        return 'try { return ' . $rules . ';} catch(\Exception $e) { return $e->getMessage(); }';
    }

    /**
     * Function to get condition rules
     *
     * @param $data
     * @param $condition
     * @param string $type
     * @param $subApplication
     * @param $stateId
     * @return bool|mixed|string
     */
    protected function getConditionRules($data, $condition, $type = 'saf', $subApplication = null, $stateId = null)
    {
        $safAgencySubDivisionId = SingleApplication::SAF_AGENCY_SUBDIVISION_ID;
        $variables = $this->getVariables('/SAF_ID_[\w]*/ius', $condition);

        list($definedVariables, $script) = $this->generateConditionScriptString($data, $variables, [], $type);
        $condition = str_replace('SAF', '$SAF', $condition);

        $variables = $this->getVariables('/FIELD_ID_[\w]*/ius', $condition);
        list($definedVariables, $script) = $this->generateConditionScriptString($data, $variables, $definedVariables, $type, $script);
        $condition = str_replace('FIELD', '$FIELD', $condition);

        if (!is_null($subApplication)) {
            $state = !empty($subApplication->applicationCurrentState->state) ? $subApplication->applicationCurrentState->state->current()->state_name : '';
            $companyInfo = Agency::getAgencyInfo();
            $subDivisionRefId = isset($data[$safAgencySubDivisionId]) ? $data[$safAgencySubDivisionId] : null;
            $subDivisionInfo = \App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications::getSubDivisionInfo($subDivisionRefId);

            $userId = Auth::id();
            $script .= "if (!defined('CURRENT_STATE')) { define('CURRENT_STATE', '{$state}'); }";
            $script .= "if (!defined('IS_MANUAL')) { define('IS_MANUAL', '{$subApplication->manual_created}'); }";
            $script .= "if (!defined('SUB_APPLICATION_ID')) { define('SUB_APPLICATION_ID', '{$subApplication->id}'); }";
            $script .= "if (!defined('NEXT_STATE_ID')) { define('NEXT_STATE_ID', '{$stateId}'); }";
            $script .= "if (!defined('USER_ID')) { define('USER_ID', '{$userId}'); }";
            $script .= "if (!defined('COMPANY_INFO')) { define('COMPANY_INFO', '{$companyInfo}'); }";
            $script .= "if (!defined('SAF_TYPE')) { define('SAF_TYPE', '{$subApplication->regime}'); }";
            $script .= "if (!defined('SUBDIVISION_INFO')) { define('SUBDIVISION_INFO', '{$subDivisionInfo}'); }";
        }
        // check is matching the condition
        $script .= 'if (' . $condition . ') { return true; } else { return false; }';
        return 'try {' . $script . '} catch(\Exception $e) { return false; }';
    }

    /**
     * Function to get variables
     *
     * @param $regex
     * @param $rules
     * @return false|int
     */
    private function getVariables($regex, $rules)
    {
        preg_match_all($regex, $rules, $variables);

        return $variables;
    }

    /**
     * Function to generate match script string
     *
     * @param $data
     * @param $variables
     * @param string $script
     * @return string
     */
    private function generateMatchScriptString($data, $variables, $script = '')
    {
        foreach ($variables[0] as $key => $variable) {
            if (isset($data[$variable])) {
                $script .= '$' . $variable . ' = "' . $data[$variable] . "\";\r\n";
            }
        }

        return $script;
    }

    /**
     * @param $data
     * @param $variables
     * @param $definedVariables
     * @param $type
     * @param string $script
     * @return array
     */
    private function generateConditionScriptString($data, $variables, $definedVariables, $type, $script = '')
    {
        foreach ($variables[0] as $key => $variable) {
            if ($type == 'saf') {
                if (isset($data[$variable]) && !isset($definedVariables[$variable])) {
                    $definedVariables[$variable] = '';
                    $script .= '$' . $variable . ' = "' . $data[$variable] . "\";\r\n";
                }
            } else {
                if ((array_key_exists($variable, $data) && is_null($data[$variable])) || isset($data[$variable])) {
                    if (is_array($data[$variable])) {
                        $script .= '$' . $variable . ' = [' . implode(',', $data[$variable]) . "];\r\n";
                    } else {
                        $script .= '$' . $variable . ' = "' . $data[$variable] . "\";\r\n";
                    }
                } else {
                    $script .= '$' . $variable . ' = null;';
                }
            }
        }

        return [$definedVariables, $script];
    }

}