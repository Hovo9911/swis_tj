<?php

namespace App\Services\TaxService\Interfaces;

/**
 * Interface TaxServiceInterFace
 * @package App\Services\TaxService\Interfaces
 */
interface TaxServiceInterFace
{
    public function getCompanyDataByTaxId($taxId);

    public function getUserDataBySSN($ssn, $userInputPassport, $checkPassport);
}