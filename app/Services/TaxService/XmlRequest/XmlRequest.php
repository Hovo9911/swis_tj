<?php

namespace App\Services\TaxService\XmlRequest;

use SimpleXMLElement;

/**
 * Class XmlRequest
 */
class XmlRequest
{
    /**
     * Function to send request Tax Service and user/company info
     *
     * @param $tin
     * @return SimpleXMLElement
     */
    public function call($tin)
    {
        $curl = curl_init();

        $taxServiceUrl = config('services.tax_service.url');

        curl_setopt_array($curl, array(
            CURLOPT_URL => $taxServiceUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_CONNECTTIMEOUT => 12,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "<xml><info><requestType>4</requestType></info><request><inn>$tin</inn></request></xml>",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: text/plain"
            ),
        ));

        if (!empty($_GET['debug'])) {
            curl_setopt($curl, CURLOPT_VERBOSE, true);

            $verbose = fopen('php://temp', 'w+');
            curl_setopt($curl, CURLOPT_STDERR, $verbose);
        }

        $response = curl_exec($curl);

        curl_close($curl);

        if (!empty($_GET['debug'])) {
            echo "<pre>\n request data\n";
            echo "******************\n";

            rewind($verbose);
            $verboseLog = stream_get_contents($verbose);

            echo "Verbose information:\n<pre>", htmlspecialchars($verboseLog), "</pre>\n";
            echo "response data\n";
            print_r($response);
            die();
        }

        $json_encoded_str = preg_replace('/(<\?xml[^?]+?)utf-16/i', '$1utf-8', $response);

        return simplexml_load_string($json_encoded_str);
    }
}