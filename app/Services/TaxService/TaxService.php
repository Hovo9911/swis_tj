<?php

namespace App\Services\TaxService;

use App\Exceptions\TaxServiceTjCompanyInactiveException;
use App\Exceptions\TaxServiceTjInnNotFoundException;
use App\Exceptions\TaxServiceTjInvalidResponseException;
use App\Models\ReferenceTable\ReferenceTable;
use App\Services\TaxService\Interfaces\TaxServiceInterFace;
use App\Services\TaxService\XmlRequest\XmlRequest;
use Exception;

/**
 * Class TaxService
 */
class TaxService implements TaxServiceInterFace
{
    /**
     * @var XmlRequest
     */
    protected $xmlRequest;

    /**
     * TaxService constructor.
     *
     * @param XmlRequest $xmlRequest
     */
    public function __construct(XmlRequest $xmlRequest)
    {
        $this->xmlRequest = $xmlRequest;
    }

    /**
     * @var string
     */
    const STATUS_OK = 'OK';
    const STATUS_INN_NOT_FOUND = 'INN_NOT_FOUND';

    /**
     * @var string
     */
    const STATUS_ACTIVE = '1';
    const STATUS_INACTIVE = '2';

    /**
     * @var array
     */
    const COMPANY_TYPES = ['1', '2'];
    const USER_TYPES = ['2', '3'];

    /**
     * Function to get Company data in TJ
     *
     * @param $taxId
     * @return array|bool
     *
     * @throws TaxServiceTjCompanyInactiveException
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function getCompanyDataByTaxId($taxId)
    {
        try {
            $result = $this->xmlRequest->call($taxId);

            $resultStatus = (string)$result->response->errorCode;

            if ($resultStatus == '') {
                throw (new TaxServiceTjInvalidResponseException());
            }

            // If Company INN Not Found
            if ($resultStatus == self::STATUS_INN_NOT_FOUND) {
                throw (new TaxServiceTjInnNotFoundException());
            }

            // Status OK
            if ($resultStatus == self::STATUS_OK) {

                $resultData = $result->response->doc->physical_info ?? '';
                if (!$resultData) {
                    $resultData = $result->response->doc->company_info;
                }

                // If Company is inactive
                if ((string)$resultData->id_status != self::STATUS_ACTIVE) {
                    throw (new TaxServiceTjCompanyInactiveException());
                }

                $type = (string)$resultData->typeID;

                if ($resultData && in_array($type, self::COMPANY_TYPES)) {

                    $name = ($type == '1') ? (string)$resultData->FullName : trans('swis.base.individual_entrepreneur.title') . ' ' . (string)$resultData->FullName;

                    return [
                        'status' => $resultStatus,
                        'tax_id' => $taxId,
                        'name' => mb_substr($name, 0, 255),
                        'country' => getReferenceRowIdByCode(ReferenceTable::REFERENCE_COUNTRIES, config('swis.country_mode')),
                        'address' => trim((string)$resultData->adr_txt) ?: '-',
                        'phone_number' => (string)$resultData['company_phone'] ?? '',
                        'community' => trim(mb_substr((string)$resultData->structure_name, 0, 34)) ?: '-',
//                        'company_reg_date' => (string)$resultData->DateReg ?? '',
                    ];
                } else {
                    throw (new TaxServiceTjInnNotFoundException());
                }

            }
        } catch (TaxServiceTjCompanyInactiveException $e) {
            throw (new TaxServiceTjCompanyInactiveException());
        } catch (TaxServiceTjInnNotFoundException $e) {
            throw (new TaxServiceTjInnNotFoundException());
        } catch (Exception $e) {
            throw (new TaxServiceTjInvalidResponseException());
        }

        return false;
    }

    /**
     * Function to get user info
     *
     * @param $ssn
     * @param $userInputPassport
     * @param $checkPassport
     * @return array|bool
     *
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function getUserDataBySSN($ssn, $userInputPassport, $checkPassport)
    {
        try {
            $result = $this->xmlRequest->call($ssn);

            $resultStatus = (string)$result->response->errorCode;

            // If User INN Not Found
            if ($resultStatus == self::STATUS_INN_NOT_FOUND) {
                throw (new TaxServiceTjInnNotFoundException());
            }

            $resultData = $result->response->doc->physical_info ?? '';

            if ($resultData && in_array($resultData->typeID, self::USER_TYPES)) {

                $passport = '-';
                if (!empty((string)$resultData->Document_Num)) {
                    $passport = replaceToLatinCharacters((string)$resultData->Document_Num);
                }

                if ($checkPassport && $passport != $userInputPassport) {
                    throw (new TaxServiceTjInnNotFoundException());
                }

                $name = (string)$resultData->FullName;

                $name = trim(str_replace("   ", " ", $name));
                $name = str_replace("  ", " ", $name);

                $first_name = explode(' ', $name)[1] ?? '';
                $last_name = explode(' ', $name)[0] ?? '';

                $passportIssuanceDate = null;
                if (!empty($resultData->Document_Date)) {
                    $passportIssuanceDate = date(config('swis.date_format'), strtotime($resultData->Document_Date));
                }

                return [
                    'ssn' => $ssn,
                    'name' => $name,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'country' => getReferenceRowIdByCode(ReferenceTable::REFERENCE_COUNTRIES, config('swis.country_mode')),
                    'passport_type' => (string)$resultData->Document_type ?? '',
                    'passport' => $passport,
                    'passport_issue_date' => $passportIssuanceDate,
                    'passport_issued_by' => (string)$resultData->Document_Organ ?? '',
                    'address' => trim((string)$resultData->adr_txt) ?: '-',
                    'full_address' => '-',
                    'phone_number' => (string)$resultData->company_phone ?? '',
                    'community' => trim((string)$resultData->structure_name) ?: '-',
                ];

            } else {
                throw (new TaxServiceTjInnNotFoundException());
            }

        } catch (TaxServiceTjInnNotFoundException $e) {
            throw (new TaxServiceTjInnNotFoundException());
        } catch (Exception $e) {
            throw (new TaxServiceTjInvalidResponseException());
        }
    }

    /**
     * Function to get Tax Service info
     *
     * @param $tin
     * @return false|string
     *
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function getXmlResponse($tin)
    {
        try {
            $result = $this->xmlRequest->call($tin);

            $resultStatus = (string)$result->response->errorCode;

            $xmlDocument = new \DOMDocument('1.0');
            $xmlDocument->preserveWhiteSpace = false;
            $xmlDocument->formatOutput = true;

            if ($resultStatus == self::STATUS_INN_NOT_FOUND) {
                $xmlDocument->loadXML($result->asXML());

                return $xmlDocument->saveXML();
//                throw (new TaxServiceTjInnNotFoundException());
            }

            if ($resultStatus == '') {
                return (string)$result;
//                throw (new TaxServiceTjInvalidResponseException());
            }

            $xmlDocument->loadXML($result->asXML());

            return html_entity_decode($xmlDocument->saveXML(), ENT_NOQUOTES, 'UTF-8');

        } catch (TaxServiceTjInnNotFoundException $e) {
            throw (new TaxServiceTjInnNotFoundException());
        } catch (Exception $e) {
            throw (new TaxServiceTjInvalidResponseException());
        }
    }
}