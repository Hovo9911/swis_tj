<?php

namespace App;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use File;
use Input;

class GUploader
{
    protected $conf;

    public function __construct($name)
    {
        $this->conf = config('guploader.'.$name);
    }

    public function checkType($file)
    {
        $conf = $this->conf;
        $mimeType = $file->getMimeType();
        $attachmentType = null;
        foreach ($conf['allowed_types'] as $type => $mimeTypes) {
            if (in_array($mimeType, $mimeTypes)) {
                return $type;
                break;
            }
        }
        return false;
    }

    public function checkSize($file)
    {
        $size = $file->getClientSize();
        return ($size < $this->conf['max_size']);
    }

	public function storeTemp($file)
    {
        $conf = $this->conf;
        $originalName = $file->getClientOriginalName();
        $filename = basename($originalName, '.' . pathinfo($originalName, PATHINFO_EXTENSION));
        $filename = mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $filename);

        $extension = $file->getClientOriginalExtension();
        $folder = uniqid();
        $filePath = $this->getFilePath($this->conf['tmp_path'], $folder, $filename . '.' . $extension);
        $path = $conf['tmp_path'] . $filePath;
        Storage::disk($conf['disk'])->put($path, File::get($file));

        return [
            'file_path' => $path,
            'tmp_name' => $filePath,
            'file_name' => $filename . '.' . $extension,
        ];
    }

    public function getTempFilepath($tmpName)
    {
        $conf = $this->conf;
        if (!Storage::disk($conf['disk'])->exists($conf['tmp_path'].$tmpName)) {
            return false;
        }
        return Storage::disk($conf['disk'])->path($conf['tmp_path'].$tmpName);
    }

    private function getFilePath($pre, $folder, $file)
    {
        $filePath = $pre . $folder . '/' . $file;
        if (Storage::disk($this->conf['disk'])->exists($filePath)) {
            return $this->getFilePath($pre, $folder + 1, $file);
        }
        return $folder . '/' . $file;
    }

    public function storePerm($tmpName, $permCustomName = '')
    {
        $conf = $this->conf;
        $confPermPath = $conf['perm_path'];
        $confTmpPath = $conf['tmp_path'];
        $confDisk = $conf['disk'];

        $fileConfTmpPath = $confTmpPath . $tmpName;

        if (!Storage::disk($confDisk)->exists($fileConfTmpPath)) {
            Log::info('g-uploader-copy-not-exist: disk - ' . $conf['disk'] . ', tmp_path - ' . $tmpName);

            abort(404);
        }

        $permPath = $tmpName;
        if ($permCustomName) {
            $permPath = $permCustomName . '/' . basename($tmpName);
        }

        if (Storage::disk($confDisk)->exists($confPermPath . $permPath)) {
            list($folder, $file) = explode('/', $tmpName);

            if (!$permCustomName) {
                $permPath = $this->getFilePath($this->conf['perm_path'], $folder, $file);
            }

        } else {

            if (!Storage::disk($confDisk)->put($confPermPath . $permPath, Storage::disk($confDisk)->get($fileConfTmpPath))) {
                Log::info('g-uploader-copy-failed: tmp - ' . $fileConfTmpPath . ', perm - ' . $permPath);

                return "";
            }
        }

        unlink(Storage::disk($confDisk)->path($fileConfTmpPath));
        rmdir(Storage::disk($confDisk)->path($confTmpPath . dirname($tmpName)));

        return $permPath;
    }

    public function getAcceptTypes()
    {
        $types = $this->conf['allowed_types'];
        $groups = [];

        foreach ($types as $type) {
            $groups []= implode(',', $type);
        }
        return implode(',', $groups);
    }

    public function isMultiple()
    {
        return $this->conf['multiple'];
    }
}