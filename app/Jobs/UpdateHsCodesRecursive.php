<?php

namespace App\Jobs;

use App\Models\ReferenceTable\ReferenceTable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

/**
 * Class UpdateHsCodesRecursive
 * @package App\Jobs
 */
class UpdateHsCodesRecursive implements ShouldQueue
{
    use Dispatchable;

    // do not change to private or protected
    /**
     * @var mixed
     */
    public $hsCodes;

    /**
     * @var bool
     */
    public $updateAllData;
    public $hsCodeId;

    /**
     * Create a new job instance.
     *
     * @param $hsCodeData
     * @param bool $single
     * @param $updateAllData
     */
    public function __construct($hsCodeData, $single = false, $updateAllData = false)
    {
        $refHsCodeTableName = ReferenceTable::REFERENCE_HS_CODE_6;

        if (!$single) {
            $this->hsCodes = $hsCodeData;
        } else {
            $this->hsCodes = DB::table($refHsCodeTableName)
                ->select('id', 'code')
                ->where('show_status', ReferenceTable::STATUS_ACTIVE)
                ->where('id', $hsCodeData)
                ->first();
            $this->hsCodeId = $hsCodeData;
        }

        $this->updateAllData = $updateAllData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

    }
}
