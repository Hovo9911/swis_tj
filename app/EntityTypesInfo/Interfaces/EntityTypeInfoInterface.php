<?php


namespace App\EntityTypesInfo\Interfaces;

/**
 * Interface EntityTypeInfo
 * @package App\EntityTypesInfo\Interfaces
 */
interface EntityTypeInfoInterface
{
    public function getCompanyInfo($taxId, $saveDataInCompany);

    public function getUserInfo($ssn, $passport, $checkPassport);
}