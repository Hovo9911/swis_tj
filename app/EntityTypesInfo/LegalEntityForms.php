<?php

namespace App\EntityTypesInfo;

use App\EntityTypesInfo\Interfaces\EntityTypeInfoInterface;
use App\Exceptions\TaxServiceTjCompanyInactiveException;
use App\Exceptions\TaxServiceTjInnNotFoundException;
use App\Exceptions\TaxServiceTjInvalidResponseException;
use App\Models\Company\CompanyManager;
use App\Models\ReferenceTable\ReferenceTable;
use App\Models\User\User;
use App\Services\TaxService\TaxService;
use Illuminate\Support\Facades\Auth;

/**
 * Class LegalEntityForms
 * @package App\EntityTypesInfo
 */
class LegalEntityForms implements EntityTypeInfoInterface
{
    /**
     * @var TaxService
     */
    private $taxService;

    /**
     * @var CompanyManager
     */
    private $companyManager;

    /**
     * @var array|null
     */
    private $legalEntities;

    /**
     * @var null
     */
    protected static $instance = null;

    /**
     * LegalEntityForms constructor.
     *
     * @param CompanyManager $companyManager
     * @param TaxService $taxService
     */
    public function __construct(CompanyManager $companyManager, TaxService $taxService)
    {
        $this->companyManager = $companyManager;
        $this->taxService = $taxService;
        $this->legalEntities = ReferenceTable::legalEntityValues();
    }

    /**
     * Function to get Company Info
     *
     * @param $taxId
     * @param bool $saveDataInCompany
     * @return array|bool
     *
     * @throws TaxServiceTjCompanyInactiveException
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function getCompanyInfo($taxId, $saveDataInCompany = false)
    {
        $companyInfoFromTaxService = $this->taxService->getCompanyDataByTaxId($taxId);

        if ($companyInfoFromTaxService && $saveDataInCompany) {
            $this->companyManager->updateInfoOrCreate($companyInfoFromTaxService);
        }

        return $companyInfoFromTaxService;
    }

    /**
     * Function to get User Info
     *
     * @param $ssn
     * @param $passport
     * @param bool $checkPassport
     * @return array|bool
     *
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function getUserInfo($ssn, $passport, $checkPassport = true)
    {
        return $this->taxService->getUserDataBySSN($ssn, replaceToLatinCharacters($passport), $checkPassport);
    }

    /**
     * Function to get Holder data if broker get current user info, not broker from tax service get info
     *
     * @param $holderType
     * @param $holderValue
     * @param null $holderPassport
     * @param string $addKeyPrefix
     * @param bool $checkBrokerPart
     * @return array
     *
     * @throws TaxServiceTjCompanyInactiveException
     * @throws TaxServiceTjInnNotFoundException
     * @throws TaxServiceTjInvalidResponseException
     */
    public function holderDataByType($holderType, $holderValue, $holderPassport = null, $checkBrokerPart = true, $addKeyPrefix = 'holder_')
    {
        $holderData = $infoData = [];

        // Not Broker (Natural person, Only Company)
        if ($checkBrokerPart && !Auth::user()->isBroker()) {

            $authUserCreator = Auth::user()->getCreator();

            $holderData = [
                $addKeyPrefix . 'type' => $authUserCreator->type,
                $addKeyPrefix . 'type_value' => $authUserCreator->ssn,
                $addKeyPrefix . 'name' => $authUserCreator->name,
                $addKeyPrefix . 'passport' => $authUserCreator->passport,
                $addKeyPrefix . 'country' => $authUserCreator->country,
                $addKeyPrefix . 'community' => $authUserCreator->community,
                $addKeyPrefix . 'address' => $authUserCreator->address,
                $addKeyPrefix . 'phone_number' => $authUserCreator->phone_number,
                $addKeyPrefix . 'email' => $authUserCreator->email,
            ];

            return $holderData;
        }

        // Company
        if ($holderType == $this->legalEntities[User::ENTITY_TYPE_TAX_ID]) {
            $infoData = $this->getCompanyInfo($holderValue);
        }

        // User(Natural person)
        if ($holderType == $this->legalEntities[User::ENTITY_TYPE_USER_ID]) {
            $infoData = $this->getUserInfo($holderValue, $holderPassport, false);
        }

        if ($infoData) {
            $holderData = [
                $addKeyPrefix . 'name' => $infoData['name'],
                $addKeyPrefix . 'type' => $holderType,
//                $addKeyPrefix . 'passport' => $infoData['passport'] ?? null,
                $addKeyPrefix . 'type_value' => $holderValue,
                $addKeyPrefix . 'country' => $infoData['country'],
//                $addKeyPrefix . 'address' => $infoData['address'],
//                $addKeyPrefix . 'community' => $infoData['community'],
            ];
        }

        return $holderData;
    }

    /**
     * Function to get instance of class
     *
     * @return LegalEntityForms
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = app()->make(self::class);
        }

        return self::$instance;
    }

}