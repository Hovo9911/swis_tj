<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidateLunchHours implements Rule
{
    private $workingHoursStart;
    private $workingHoursEnd;
    private $lunchHoursStart;
    private $lunchHoursEnd;

    /**
     * ValidateLunchHours constructor.
     * @param $workingHoursStart
     * @param $workingHoursEnd
     * @param $lunchHoursStart
     * @param $lunchHoursEnd
     */
    public function __construct($workingHoursStart, $workingHoursEnd, $lunchHoursStart, $lunchHoursEnd)
    {
        $this->workingHoursStart = $workingHoursStart;
        $this->workingHoursEnd = $workingHoursEnd;
        $this->lunchHoursStart = $lunchHoursStart;
        $this->lunchHoursEnd = $lunchHoursEnd;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($this->workingHoursStart >= $this->lunchHoursStart) {
            return false;
        }

        if ($this->workingHoursEnd <= $this->lunchHoursEnd) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('swis.agency.lunch_time_range.error');
    }
}
