<?php

namespace App\Rules;

use App\Models\SingleApplicationProducts\SingleApplicationProducts;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

/**
 * Class ValidateDelimited
 * @package App\Rules
 */
class ValidateDelimited implements Rule
{
    public $validationType;
    public $safNumber;

    /**
     * @var string
     */
    const VALIDATION_TYPE_SAF_PRODUCTS = 'saf_products';
    const VALIDATION_TYPE_SAF_SUB_APPLICATIONS = 'saf_sub_applications';

    /**
     * ValidateDelimited constructor.
     * @param $validationType
     * @param null $safNumber
     */
    public function __construct($validationType, $safNumber = null)
    {
        $this->validationType = $validationType;
        $this->safNumber = $safNumber;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $result = true;
        switch ($this->validationType) {
            case self::VALIDATION_TYPE_SAF_PRODUCTS;
                    $result = $this->validateSafProducts($attribute, $value);
                break;
            case self::VALIDATION_TYPE_SAF_SUB_APPLICATIONS;
                    $result = $this->validateSafSubApplications($attribute, $value);
                break;
        }

        return $result;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('core.loading.invalid_data');
    }

    /**
     * Function to validate saf products
     *
     * @param $attribute
     * @param $value
     * @return bool
     */
    private function validateSafProducts($attribute, $value)
    {
        if (!is_null($this->safNumber)) {
            $products = explode(',', $value);

            foreach ($products as $productId) {
                $safProduct = SingleApplicationProducts::select('id')->where(['saf_number' => $this->safNumber])->where(DB::raw('id::varchar'), $productId)->first();

                if (is_null($safProduct)) {
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Function to validate saf sub applications
     *
     * @param $attribute
     * @param $value
     * @return bool
     */
    private function validateSafSubApplications($attribute, $value)
    {
        if (!is_null($this->safNumber)) {
            $subApplications = explode(',', $value);

            foreach ($subApplications as $subApplicationId) {
                $safSubApplication = SingleApplicationSubApplications::select('id')->where(['saf_number' => $this->safNumber])->where(DB::raw('id::varchar'), $subApplicationId)->first();

                if (is_null($safSubApplication)) {
                    return false;
                }
            }

            return true;
        }

        return false;
    }
}
