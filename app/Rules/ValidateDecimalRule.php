<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

/**
 * Class ValidateDecimalRule
 * @package App\Rules
 */
class ValidateDecimalRule implements Rule
{
    private $leftSide;
    private $rightSide;

    /**
     * Create a new rule instance.
     *
     * @param $leftSide
     * @param $rightSide
     */
    public function __construct($leftSide, $rightSide)
    {
        $this->leftSide = $leftSide;
        $this->rightSide = $rightSide;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $value = str_replace(',', '.', $value);
        $decimal = explode(".", $value);

        if(isset($decimal[0]) && !ctype_digit($decimal[0]) || (isset($decimal[1])) && !ctype_digit($decimal[1])){
            return false;
        }

        if (strlen($decimal[0]) > $this->leftSide or (isset($decimal[1]) && strlen($decimal[1]) > $this->rightSide)) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('swis.reference-table-form.decimal-number.error');
    }
}
