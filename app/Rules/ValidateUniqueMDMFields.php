<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\SingleApplicationSubApplications\SingleApplicationSubApplications;

class ValidateUniqueMDMFields implements Rule
{
    private $constructorDocument;
    private $applicationID;
    private $customData;
    private $uniqueDataSetMdm;

    /**
     * ValidateUniqueDataForMDMAndSaf constructor.
     *
     * @param $constructorDocument
     * @param $customData
     * @param $applicationID
     * @param $uniqueDataSetMdm
     */
    public function __construct($constructorDocument, $customData, $applicationID, $uniqueDataSetMdm)
    {
        $this->constructorDocument = $constructorDocument;
        $this->applicationID = $applicationID;
        $this->customData = $customData;
        $this->uniqueDataSetMdm = $uniqueDataSetMdm;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!is_null($this->uniqueDataSetMdm)) {
            if (!is_null($this->constructorDocument) && isset($this->customData[$this->uniqueDataSetMdm->field_id]) && !is_null($this->customData[$this->uniqueDataSetMdm->field_id])) {

                $subApplication = SingleApplicationSubApplications:: where('document_code', "{$this->constructorDocument->document_code}")
                    ->where('id', '!=', $this->applicationID)
                    ->where("custom_data->{$this->uniqueDataSetMdm->field_id}", '=', $this->customData[$this->uniqueDataSetMdm->field_id])
                    ->get();

                if (count($subApplication)>0) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('swis.user_documents.unique_field');
    }
}
