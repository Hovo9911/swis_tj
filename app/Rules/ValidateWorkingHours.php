<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidateWorkingHours implements Rule
{
    private $hoursStart;
    private $hoursEnd;

    /**
     * Create a new rule instance.
     *
     * ValidateWorkingHours constructor.
     * @param $hoursStart
     * @param $hoursEnd
     */
    public function __construct($hoursStart, $hoursEnd)
    {
        $this->hoursStart = $hoursStart;
        $this->hoursEnd = $hoursEnd;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($this->hoursStart >= $this->hoursEnd) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('swis.agency.end_time.error');
    }
}
