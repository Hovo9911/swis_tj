<?php

if (!function_exists("makeMandatory")) {
    function makeMandatory($fields) { return ["function" => "makeMandatory", "result" => $fields]; }
}
if (!function_exists("showError")) {
    function showError($key, $field = null) {
        $result = ["message" => trans($key), "fields" => []];
        if (!is_null($field)) {
            $result["fields"][] = $field;
        }
        return ["function" => "showError", "result" => $result];
    }
}
if (!function_exists("showWarning")) {
    function showWarning($key, $field = null) {
        $result = ["message" => trans($key), "fields" => []];
        if (!is_null($field)) {
            $result["fields"][] = $field;
        }
        return ["function" => "showWarning", "result" => $result];
    }
}
if (!function_exists("showWarning2")) {
    function showWarning2($key, $field = null) {
        $result = ["message" => trans($key), "fields" => []];
        if (!is_null($field)) {
            $result["fields"][] = $field;
        }
        return ["function" => "showWarning2", "result" => $result];
    }
}
if (!function_exists("makeLabMandatory")) {
    function makeLabMandatory() {
        return ["function" => "makeLabMandatory", "result" => []];
    }
}