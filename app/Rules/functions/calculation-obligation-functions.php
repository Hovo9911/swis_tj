<?php

use GuzzleHttp\Exception\ConnectException;
use Illuminate\Support\Facades\Log;

if (!function_exists("in")) {
    function in($value, $data) { return in_array($value, explode("|", $data)); }
}
if (!function_exists("roundingUp")) {
    function roundingUp($number, $fieldId) { return ceil($fieldId/$number); }
}
if (!function_exists("roundingDown")) {
    function roundingDown($number, $fieldId) { return floor($fieldId/$number); }
}
if (!function_exists("startWith")) {
    function startWith($data, $value) { return mb_strpos($data, $value) === 0; }
}
if (!function_exists("contains")) {
    function contains($data, $value) { return mb_strpos($data, $value) !== false; }
}
if (!function_exists("endWith")) {
    function endWith($data, $value) { return mb_stripos(strrev($data), strrev($value)) === 0; }
}
if (!function_exists("isEmpty")) {
    function isEmpty($data) { return empty($data); }
}
if (!function_exists("nowDate")) {
    function nowDate($format = "Y-m-d") { return date($format); }
}
if (!function_exists("currentUser")) {
    function currentUser() { return \Auth::user()->first_name . " " . \Auth::user()->last_name; }
}
if (!function_exists("concat")) {
    function concat() { return implode("", func_get_args()); }
}
if (!function_exists("subapplicationManuallyAdded")) {
    function subapplicationManuallyAdded() { return IS_MANUAL; }
}
if (!function_exists("currentUserShortName")) {
    function currentUserShortName() { return ucfirst(mb_substr(\Auth::user()->first_name, 0, 1)) . ". " . \Auth::user()->last_name; }
}
if (!function_exists("yearEnd")) {
    function yearEnd() { return date("Y-m-d", strtotime("12/31")); }
}
if (!function_exists("isSafType")) {
    function isSafType($type) { return ($type == SAF_TYPE); }
}
if (!function_exists("formatNumber")) {
    function formatNumber($number, $decimals) {
        $negation = ($number < 0) ? (-1) : 1;
        $coefficient = pow(10, $decimals);
        $number = $negation * floor((string)(abs($number) * $coefficient)) / $coefficient;
        return number_format($number, $decimals, ".", "");
    }
}
if (!function_exists("currentUserInfo")) {
    function currentUserInfo($key) {
        $userInfo = Auth::user()->toArray();
        if (isset($key) && array_key_exists($key, $userInfo)) {
            return $userInfo[$key];
        }
        return "";
    }
}
if (!function_exists("currentAgencyInfo")) {
    function currentAgencyInfo($key) {
        $companyInfo = json_decode(COMPANY_INFO, true);
        if (isset($key) && array_key_exists($key, $companyInfo)) {
            return $companyInfo[$key];
        }
        return "";
    }
}
if (!function_exists("currentSubdivisionInfo")) {
    function currentSubdivisionInfo($key) {
        $subDivisionInfo = json_decode(SUBDIVISION_INFO, true);
        if (isset($key) && array_key_exists($key, $subDivisionInfo)) {
            return $subDivisionInfo[$key];
        }
        return "";
    }
}
if (!function_exists("inRef")) {
    function inRef($tableName, $conditions, $returnField = null) {
        $params = array_combine(["table-name", "conditions", "return-field", "token"], [$tableName, json_encode($conditions), $returnField, "0B4C5866B70EC237DA40E5EDEEED291FFB123A86AB41A5A51DC1F640953C3F39"]);
        return apiCall(config("rules.functions.inRef"), $params);
    }
}
if (!function_exists("exchangeRate")) {
    function exchangeRate($field, $fromCurrency, $toCurrency) {
        $params = array_combine(["field", "from-currency", "to-currency", "sub-application-id", "token"], [$field, $fromCurrency, $toCurrency, SUB_APPLICATION_ID, "0B4C5866B70EC237DA40E5EDEEED291FFB123A86AB41A5A51DC1F640953C3F39"]);
        return apiCall(config("rules.functions.exchangeRate"), $params);
    }
}
if (!function_exists("hoursSpent")) {
    function hoursSpent() {
        return apiCall(config("rules.functions.hourSpend"), ["sub-application-id" => SUB_APPLICATION_ID, "token" => "0B4C5866B70EC237DA40E5EDEEED291FFB123A86AB41A5A51DC1F640953C3F39"]);
    }
}
if (!function_exists("hoursLeft")) {
    function hoursLeft() {
        return apiCall(config("rules.functions.hourLeft"), ["sub-application-id" => SUB_APPLICATION_ID, "token" => "0B4C5866B70EC237DA40E5EDEEED291FFB123A86AB41A5A51DC1F640953C3F39"]);
    }
}
if (!function_exists("getProducts")) {
    function getProducts($conditions, $function) {
        $params = array_combine(["conditions", "function", "sub-application-id", "token"], [json_encode($conditions), $function, SUB_APPLICATION_ID, "0B4C5866B70EC237DA40E5EDEEED291FFB123A86AB41A5A51DC1F640953C3F39"]);
        return apiCall(config("rules.functions.getProducts"), $params);
    }
}
if (!function_exists("setProductValues")) {
    function setProductValues($conditions, $values) {
        $params = array_combine(["conditions", "values", "sub-application-id", "token"], [json_encode($conditions), json_encode($values), SUB_APPLICATION_ID, "0B4C5866B70EC237DA40E5EDEEED291FFB123A86AB41A5A51DC1F640953C3F39"]);
        return apiCall(config("rules.functions.setProductValues"), $params);
    }
}
if (!function_exists("getBatches")) {
    function getBatches($conditions, $function) {
        $params = array_combine(["conditions", "function", "sub-application-id", "token"], [json_encode($conditions), $function, SUB_APPLICATION_ID, "0B4C5866B70EC237DA40E5EDEEED291FFB123A86AB41A5A51DC1F640953C3F39"]);
        return apiCall(config("rules.functions.getBatches"), $params);
    }
}
if (!function_exists("nextId")) {
    function nextId($field, $length = null) {
        $params = array_combine(["sub-application-id", "token", "field", "length"], [SUB_APPLICATION_ID, "0B4C5866B70EC237DA40E5EDEEED291FFB123A86AB41A5A51DC1F640953C3F39", $field, $length]);
        return apiCall(config("rules.functions.getNextId"), $params);
    }
}
if (!function_exists("generateObligation")) {
    function generateObligation($obligationCode, $sum) {
        $params = array_combine(["sub-application-id", "token", "obligation-code", "sum", "state_id", "user_id"], [SUB_APPLICATION_ID, "0B4C5866B70EC237DA40E5EDEEED291FFB123A86AB41A5A51DC1F640953C3F39", $obligationCode, $sum, NEXT_STATE_ID, USER_ID]);
        return apiCall(config("rules.functions.generateObligation"), $params);
    }
}
if (!function_exists("isDocAttached")) {
    function isDocAttached($documentCode) {
        $params = array_combine(["sub-application-id", "token", "document-code"], [SUB_APPLICATION_ID, "0B4C5866B70EC237DA40E5EDEEED291FFB123A86AB41A5A51DC1F640953C3F39", $documentCode]);
        return apiCall(config("rules.functions.isDocAttached"), $params);
    }
}
if (!function_exists("notRespondingInstruction")) {
    function notRespondingInstruction() {
        $params = array_combine(["sub-application-id", "token"], [SUB_APPLICATION_ID, "0B4C5866B70EC237DA40E5EDEEED291FFB123A86AB41A5A51DC1F640953C3F39"]);
        return apiCall(config("rules.functions.notRespondingInstruction"), $params);
    }
}
if (!function_exists("isIdentic")) {
    function isIdentic($fieldId, $hsCodeGroup = null) {
        $params = array_combine(["sub-application-id", "token", "field_id", "hs_code_group"], [SUB_APPLICATION_ID, "0B4C5866B70EC237DA40E5EDEEED291FFB123A86AB41A5A51DC1F640953C3F39", $fieldId, $hsCodeGroup]);
        return apiCall(config("rules.functions.isIdentic"), $params);
    }
}
if (!function_exists("unansweredInstructionType")) {
    function unansweredInstructionType($instructionType = null) {
        $params = array_combine(["sub-application-id", "token", "instruction_type"], [SUB_APPLICATION_ID, "0B4C5866B70EC237DA40E5EDEEED291FFB123A86AB41A5A51DC1F640953C3F39", $instructionType]);
        return apiCall(config("rules.functions.unansweredInstructionType"), $params);
    }
}
if (!function_exists("negativeAnsweredInstructionType")) {
    function negativeAnsweredInstructionType($instructionType = null) {
        $params = array_combine(["sub-application-id", "token", "instruction_type"], [SUB_APPLICATION_ID, "0B4C5866B70EC237DA40E5EDEEED291FFB123A86AB41A5A51DC1F640953C3F39", $instructionType]);
        return apiCall(config("rules.functions.negativeAnsweredInstructionType"), $params);
    }
}
if (!function_exists("getLabObligations")) {
    function getLabObligations() {
        $params = array_combine(["sub-application-id", "token"], [SUB_APPLICATION_ID, "0B4C5866B70EC237DA40E5EDEEED291FFB123A86AB41A5A51DC1F640953C3F39"]);
        return apiCall(config("rules.functions.getLabObligations"), $params);
    }
}
if (!function_exists("unansweredExamination")) {
    function unansweredExamination() {
        $params = array_combine(["sub-application-id", "token"], [SUB_APPLICATION_ID, "0B4C5866B70EC237DA40E5EDEEED291FFB123A86AB41A5A51DC1F640953C3F39"]);
        return apiCall(config("rules.functions.unansweredExamination"), $params);
    }
}
if (!function_exists("apiCall")) {
    function apiCall($url, $params) {
        $verify = (strpos($url, "https://staging.") !== false) ? ["verify" => false] : [];
        $client = new GuzzleHttp\Client($verify);
        $request = $client->request("POST", $url, [ "form_params" => $params ]);
        return json_decode($request->getBody()->getContents())->response;
    }
}