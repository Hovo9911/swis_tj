<?php

namespace App\Sms;

use App\Models\ReferenceTable\ReferenceTable;
use App\Models\Sms\Sms;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class SmsManager
 *
 * @property \Illuminate\Config\Repository|\Illuminate\Foundation\Application|mixed sms_send
 * @package App\Http\Controllers\Core\SmsManager\SmsManager
 */
class SmsManager
{

    /**
     * SmsManager constructor.
     */
    public function __construct()
    {
        $this->sms_send = config('swis.sms_send');
    }

    /**
     * @param $data
     */
    public function storeSms($data)
    {
        $phoneNumber = $this->getValidPhoneNumber(trim($data['phone']));

        if ($phoneNumber && $this->sms_send) {
            $data['phone'] = $phoneNumber;
            $data['status'] = Sms::STATUS_PENDING;
            $sms = new Sms($data);
            $sms->save();
        }
    }

    /**
     * Function to send sms
     *
     * @param $sms
     * @throws \Exception
     */
    public function sendSms($sms)
    {
        if ($this->sms_send) {

            try {
                $this->send($sms);
                $sms->status = Sms::STATUS_SENT;
            } catch (\Exception $e) {
                $sms->status = Sms::STATUS_FAILED;
                Log::info('sms-send-error: ' . __LINE__ . ' ' .$e->getMessage());
            }

            $sms->save();
        }
    }

    /**
     * Function to send pending sms
     */
    public function sendPendingSMS()
    {
        if ($this->sms_send) {
            $smss = Sms::where('status', Sms::STATUS_PENDING)->orderBy('id', 'desc')->get();

            foreach ($smss as $sms) {
                $this->sendSms($sms);
            }
        }
    }

    /**
     * Function to check phone_number is valid and has active operator in ref (clean from +,trim)
     *
     * @param $userPhoneNumber
     * @return bool|string
     */
    public function getValidPhoneNumber($userPhoneNumber)
    {
        $countryPhoneNumberPrefix = config('swis.sms_country_phone_number_prefix');
        $countryPhoneNumberCount = config('swis.sms_country_phone_number_count');
        $countryPhoneNumberCountWithoutPrefix = $countryPhoneNumberCount - 3;

        //
        $phoneNumber = trim(str_replace('+', '', $userPhoneNumber));

        if (strlen($phoneNumber) === $countryPhoneNumberCountWithoutPrefix) {
            $phoneNumber = $countryPhoneNumberPrefix . $phoneNumber;
        }

        if (strpos($phoneNumber, $countryPhoneNumberPrefix) !== 0) {
            return false;
        }

        $phoneNumberOperator = mb_substr($phoneNumber, 3, 2);
        $hasActiveOperator = DB::table(ReferenceTable::REFERENCE_MOBILE_OPERATORS_CODES)->select('id')->where(['code' => $phoneNumberOperator, 'show_status' => ReferenceTable::STATUS_ACTIVE])->first();

        if (!is_null($hasActiveOperator)) {
            return $phoneNumber;
        }

        return false;
    }

    private function send($sms)
    {
        $config = config('services.sms_gateway');
        header('Content-Type: text/plain');

        $src = $config['from'];

        $phone = $sms->phone;
        $dst = $phone;

        $s = new SMPP();
//        $s->debug = 1;

        // $host,$port,$system_id,$password
        $s->open($config['host'], $config['port'], $config['username'], $config['password']);

        // $source_addr,$destintation_addr,$short_message,$utf=0,$flash=0
//		$s->send_long($src, $dst, $sms->message);

//		/* To send unicode
        $utf = true;
        $message = iconv('UTF-8', 'UTF-16BE', $sms->message);

        $s->send_long($src, $dst, $message, $utf);
//        */

        $s->close();
    }
}